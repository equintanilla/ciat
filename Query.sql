
------------------------------------------------
--PRODUCTO TERMINAL DE MEDIANO PLAZO
------------------------------------------------

CREATE TABLE pterminal_oestrategico
(
  pt_id serial NOT NULL,
  obje_id integer NOT NULL,
  obje_codigo character varying(100) NOT NULL DEFAULT '0'::character varying,
  obje_objetivo character varying(2000) NOT NULL DEFAULT '0'::character varying,
  indi_id integer NOT NULL,
  obje_indicador character varying(500) NOT NULL DEFAULT '0'::character varying,
  obje_formula character varying(500) NOT NULL DEFAULT '0'::character varying,
  obje_linea_base numeric(18,2) NOT NULL DEFAULT 0,
  obje_meta numeric(18,2) NOT NULL DEFAULT 0,
  obje_fuente_verificacion character varying(2000) NOT NULL DEFAULT '0'::character varying,
  obje_metodo character varying(2000) NOT NULL DEFAULT '0'::character varying,
  obje_tendencia character varying(200) NOT NULL DEFAULT '0'::character varying,
  obje_supuestos character varying(2000) NOT NULL DEFAULT '0'::character varying,
  obje_presupuesto numeric(18,2) NOT NULL DEFAULT 0,
  obje_ponderacion numeric(18,2) NOT NULL DEFAULT 0,
  obje_gestiones integer NOT NULL DEFAULT 0,
  obje_gestion_curso integer NOT NULL DEFAULT 0,
  pdes_id integer NOT NULL DEFAULT 0,
  fun_id integer NOT NULL DEFAULT 0,
  ptdi_id integer DEFAULT 0,
  obje_archivo_adjunto character varying(20000),
  obje_eficacia character varying(600),
  obje_eficiencia character varying(600),
  obje_eficiencia_pe character varying(600),
  obje_eficiencia_fi character varying(600),
  obje_total_casos character varying(800),
  obje_casos_favorables character varying(800),
  obje_casos_desfavorables character varying(800),
  obje_denominador integer NOT NULL DEFAULT 0,
  CONSTRAINT pterminal_oestrategico_pt_id PRIMARY KEY (pt_id),
  CONSTRAINT fk_pterminal_oestrategico_pdes FOREIGN KEY (pdes_id)
      REFERENCES pdes (pdes_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_pterminal_oestrategico_ptdi FOREIGN KEY (ptdi_id)
      REFERENCES ptdi (ptdi_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_pterminal_oestrategico_indicador FOREIGN KEY (indi_id)
      REFERENCES indicador (indi_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT pterminal_oestrategico_fun_id_fkey FOREIGN KEY (fun_id)
      REFERENCES funcionario (fun_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT pterminal_oestrategico_obje_id_fkey FOREIGN KEY (obje_id)
      REFERENCES objetivosestrategicos (obje_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT pterminal_oestrategico_obje_gestiones_fkey FOREIGN KEY (obje_gestiones)
      REFERENCES gestion (g_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

-- El que le sigue
CREATE TABLE pt_prog_gestion
(
  opt_id serial NOT NULL,
  pt_id integer NOT NULL,
  g_id integer NOT NULL,
  opm_programado numeric(18,2) NOT NULL DEFAULT 0,
  CONSTRAINT pt_prog_gestion_pkey PRIMARY KEY (opt_id),
  CONSTRAINT pt_prog_gestion_g_id_fkey FOREIGN KEY (g_id)
      REFERENCES gestion (g_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT pt_prog_gestion_pt_id_fkey FOREIGN KEY (pt_id)
      REFERENCES pterminal_oestrategico (pt_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);


------------------------------------------------
--NUEVAS COLUMNAS EN UNIDADORGANIZACIONAL
--EMERSON
--26/11/2018
------------------------------------------------
	ALTER TABLE "public"."unidadorganizacional" ADD COLUMN uni_cod_da integer;
	ALTER TABLE "public"."unidadorganizacional" ADD COLUMN uni_cod_ue integer;

----------------------------------------------
--NUEVA COLUMNA EN TAPLA PTDI
--EMERSON
--30/11/2018
------------------------------------------------
	ALTER TABLE "public"."ptdi" ADD COLUMN ptdi_gestion_fin int4;


------------------------------------------------------------------------------
--NUEVA COLUMNA EN TABLA objetivosgestion PARA CONTROLAR LA FECHA
--EDDY
--05/12/2018
--------------------------------------------------------------------------------
  ALTER TABLE objetivosgestion ADD COLUMN fecha_mod timestamp without time zone DEFAULT now();

--------------------------------------------------------------------------------
--NUEVA TABLA  PARA CONTROLAR LAS MODIFICACIONES DE OBJETIVOS DE GESTION
--EDDY
--05/12/2018
--------------------------------------------------------------------------------
CREATE TABLE objetivosgestion_mod
(
  objetivosgestion_mod_id serial,
  o_id numeric(18,0),
  obje_id numeric(18,0) NOT NULL,
  o_codigo character varying(100) NOT NULL,
  o_objetivo character varying(2000) NOT NULL,
  indi_id integer NOT NULL,
  o_indicador character varying(500) NOT NULL DEFAULT '0'::character varying,
  o_formula character varying(500) NOT NULL DEFAULT '0'::character varying,
  o_linea_base numeric(18,2) DEFAULT 0,
  o_meta numeric(18,2) DEFAULT 0,
  o_fuente_verificacion character varying(2000) DEFAULT '0'::character varying,
  o_supuestos character varying(2000) DEFAULT '0'::character varying,
  o_ponderacion numeric(18,2) DEFAULT 0,
  aper_id numeric(18,0) DEFAULT 0,
  o_cierre integer DEFAULT 0,
  o_gestion integer,
  fun_id integer DEFAULT 0,
  o_archivo_adjunto character varying(1000) DEFAULT NULL::character varying,
  o_eficacia character varying(600),
  o_efinanciera character varying(600),
  o_epejecucion character varying(600),
  o_efisica character varying(600),
  fecha_creacion timestamp without time zone,
  o_estado integer DEFAULT 1,
  o_total_casos character varying(500),
  o_casos_favorables character varying(500),
  o_casos_desfavorables character varying(500),
  o_denominador integer DEFAULT 0,
  fecha_mod timestamp without time zone DEFAULT now(),
  CONSTRAINT objetivosgestion_mod_id_pk PRIMARY KEY (objetivosgestion_mod_id)
);


--------------------------------------------------------------------------------
--NUEVA TABLA  PARA CONTROLAR LAS MODIFICACIONES DE OBJETIVOS DE GESTION TEMPORIZADOR
--EDDY
--06/12/2018
--------------------------------------------------------------------------------
CREATE TABLE ogestion_prog_mes_mod
(
  ogestion_progm_mod serial,
  objetivosgestion_mod_id integer,
  opm_id integer,
  o_id integer,
  mes_id integer,
  opm_fis numeric(18,2) DEFAULT 0,
  CONSTRAINT ogestion_prog_mes_mod_pkey PRIMARY KEY (ogestion_progm_mod)
);


--------------------------------------------------------------------------------
--MODIFICANDO LA VISTA PARA RANGOS DE GESTION EN PRODUCTO TERMINAL
--EDDY
--10/12/2018
--------------------------------------------------------------------------------
CREATE OR REPLACE VIEW v_objetivos_estrategicos_pte AS 
 SELECT o.pt_id,
  o.obje_id,
    o.obje_codigo,
    o.obje_objetivo,
    o.indi_id,
    ( SELECT get_indicador(o.indi_id) AS get_indicador) AS indicador,
    ( SELECT get_indi_abrev(o.indi_id) AS get_indi_abrev) AS indi_abrev,
    o.obje_indicador,
    o.obje_linea_base,
    o.obje_meta,
    o.obje_ponderacion,
    o.obje_fuente_verificacion,
    o.obje_supuestos,
    o.obje_formula,
    o.obje_total_casos,
    o.obje_casos_favorables,
    o.obje_casos_desfavorables,
    o.fun_id,
    f.fun_nombre,
    f.fun_paterno,
    f.fun_materno,
    ( SELECT get_unidad(f.uni_id) AS get_unidad) AS get_unidad,
    o.obje_eficacia,
    o.obje_eficiencia,
    o.obje_eficiencia_pe,
    o.obje_eficiencia_fi,
    o.pdes_id,
    o.ptdi_id,
    o.obje_gestion_curso,
    o.obje_archivo_adjunto,
    p.pdes_descripcion AS pdes_accion,
    p.pdes_codigo AS pdes_acod,
    q.pdes_descripcion AS pdes_resultado,
    q.pdes_codigo AS pdes_rcod,
    r.pdes_descripcion AS pdes_meta,
    r.pdes_codigo AS pdes_mcod,
    s.pdes_descripcion AS pdes_pilar,
    s.pdes_codigo AS pdes_pcod,
    w.ptdi_descripcion AS ptdi_programa,
    w.ptdi_codigo AS ptdi_pcod,
    x.ptdi_descripcion AS ptdi_politica,
    x.ptdi_codigo AS ptdi_ocod,
    y.ptdi_descripcion AS ptdi_eje,
    y.ptdi_codigo AS ptdi_ecod
   FROM pterminal_oestrategico o
     JOIN funcionario f ON o.fun_id = f.fun_id
     JOIN pdes p ON o.pdes_id = p.pdes_id
     JOIN pdes q ON p.pdes_depende = q.pdes_codigo
     JOIN pdes r ON q.pdes_depende = r.pdes_codigo
     JOIN pdes s ON r.pdes_depende = s.pdes_codigo
     JOIN ptdi w ON o.ptdi_id = w.ptdi_id
     JOIN ptdi x ON w.ptdi_depende = x.ptdi_codigo
     JOIN ptdi y ON x.ptdi_depende = y.ptdi_codigo
  WHERE q.pdes_gestion = o.obje_gestion_curso AND r.pdes_gestion = o.obje_gestion_curso  AND s.pdes_gestion = o.obje_gestion_curso 
  AND (o.obje_gestion_curso between w.ptdi_gestion and w.ptdi_gestion_fin) AND (o.obje_gestion_curso between x.ptdi_gestion and x.ptdi_gestion_fin) AND (o.obje_gestion_curso between y.ptdi_gestion and y.ptdi_gestion_fin);


--------------------------------------------------------------------------------
--MODIFICANDO LA VISTA PARA RANGOS PARA LA MODIFICACION DE OBJETIVOS
--EDDY
--10/12/2018
--------------------------------------------------------------------------------
CREATE OR REPLACE VIEW v_objetivos_estrategicos AS 
 SELECT o.obje_id,
    o.obje_codigo,
    o.obje_objetivo,
    o.indi_id,
    ( SELECT get_indicador(o.indi_id) AS get_indicador) AS indicador,
    ( SELECT get_indi_abrev(o.indi_id) AS get_indi_abrev) AS indi_abrev,
    o.obje_indicador,
    o.obje_linea_base,
    o.obje_meta,
    o.obje_ponderacion,
    o.obje_fuente_verificacion,
    o.obje_supuestos,
    o.obje_formula,
    o.obje_total_casos,
    o.obje_casos_favorables,
    o.obje_casos_desfavorables,
    o.fun_id,
    f.fun_nombre,
    f.fun_paterno,
    f.fun_materno,
    ( SELECT get_unidad(f.uni_id) AS get_unidad) AS get_unidad,
    o.obje_eficacia,
    o.obje_eficiencia,
    o.obje_eficiencia_pe,
    o.obje_eficiencia_fi,
    o.pdes_id,
    o.ptdi_id,
    o.obje_gestion_curso,
    o.obje_archivo_adjunto,
    p.pdes_descripcion AS pdes_accion,
    p.pdes_codigo AS pdes_acod,
    q.pdes_descripcion AS pdes_resultado,
    q.pdes_codigo AS pdes_rcod,
    r.pdes_descripcion AS pdes_meta,
    r.pdes_codigo AS pdes_mcod,
    s.pdes_descripcion AS pdes_pilar,
    s.pdes_codigo AS pdes_pcod,
    w.ptdi_descripcion AS ptdi_programa,
    w.ptdi_codigo AS ptdi_pcod,
    x.ptdi_descripcion AS ptdi_politica,
    x.ptdi_codigo AS ptdi_ocod,
    y.ptdi_descripcion AS ptdi_eje,
    y.ptdi_codigo AS ptdi_ecod
   FROM objetivosestrategicos o
     JOIN funcionario f ON o.fun_id = f.fun_id
     JOIN pdes p ON o.pdes_id = p.pdes_id
     JOIN pdes q ON p.pdes_depende = q.pdes_codigo
     JOIN pdes r ON q.pdes_depende = r.pdes_codigo
     JOIN pdes s ON r.pdes_depende = s.pdes_codigo
     JOIN ptdi w ON o.ptdi_id = w.ptdi_id
     JOIN ptdi x ON w.ptdi_depende = x.ptdi_codigo
     JOIN ptdi y ON x.ptdi_depende = y.ptdi_codigo
  WHERE q.pdes_gestion = o.obje_gestion_curso AND r.pdes_gestion = o.obje_gestion_curso AND s.pdes_gestion = o.obje_gestion_curso 
 AND (o.obje_gestion_curso between w.ptdi_gestion and w.ptdi_gestion_fin) AND (o.obje_gestion_curso between x.ptdi_gestion and x.ptdi_gestion_fin) AND (o.obje_gestion_curso between y.ptdi_gestion and y.ptdi_gestion_fin);

------------------------------------------------------------------------------
--NUEVA COLUMNA EN TABLA objetivosestrategicos  y creacion de la tablas para controlar modificaciones
--EDDY
--21/12/2018
--------------------------------------------------------------------------------
  ALTER TABLE objetivosestrategicos ADD COLUMN fecha_mod timestamp without time zone DEFAULT now();

CREATE TABLE objetivosestrategicos_mod
(
  objetivosestrategicos_mod_id serial,
  obje_id numeric(18,0),
  obje_codigo character varying(100) NOT NULL DEFAULT '0'::character varying,
  obje_objetivo character varying(2000) NOT NULL DEFAULT '0'::character varying,
  indi_id integer NOT NULL,
  obje_indicador character varying(500) NOT NULL DEFAULT '0'::character varying,
  obje_formula character varying(500) NOT NULL DEFAULT '0'::character varying,
  obje_linea_base numeric(18,2) NOT NULL DEFAULT 0,
  obje_meta numeric(18,2) NOT NULL DEFAULT 0,
  obje_fuente_verificacion character varying(2000) NOT NULL DEFAULT '0'::character varying,
  obje_metodo character varying(2000) NOT NULL DEFAULT '0'::character varying,
  obje_tendencia character varying(200) NOT NULL DEFAULT '0'::character varying,
  obje_supuestos character varying(2000) NOT NULL DEFAULT '0'::character varying,
  obje_presupuesto numeric(18,2) NOT NULL DEFAULT 0,
  obje_ponderacion numeric(18,2) NOT NULL DEFAULT 0,
  obje_gestiones integer NOT NULL DEFAULT 0,
  obje_gestion_curso integer NOT NULL DEFAULT 0,
  pdes_id integer NOT NULL DEFAULT 0,
  fun_id integer NOT NULL DEFAULT 0,
  ptdi_id integer DEFAULT 0,
  obje_archivo_adjunto character varying(20000),
  obje_eficacia character varying(600),
  obje_eficiencia character varying(600),
  obje_eficiencia_pe character varying(600),
  obje_eficiencia_fi character varying(600),
  obje_total_casos character varying(800),
  obje_casos_favorables character varying(800),
  obje_casos_desfavorables character varying(800),
  obje_denominador integer NOT NULL DEFAULT 0,
  fecha_mod timestamp without time zone DEFAULT now(),
  CONSTRAINT objetivosestrategicos_mod_id_pk PRIMARY KEY (objetivosestrategicos_mod_id)
);

CREATE TABLE obje_prog_gestion_mod
(
  obje_prog_gestion_mod_id serial,
  objetivosestrategicos_mod_id integer,
  opm_id integer,
  obje_id integer NOT NULL,
  g_id integer NOT NULL,
  opm_programado numeric(18,2) NOT NULL DEFAULT 0,
  CONSTRAINT oobje_prog_gestion_mod_pkey PRIMARY KEY (obje_prog_gestion_mod_id)
);



--------------------------------------------------------------------------------
--FUNCION PARA CONTROLAR LAS MODIFICACIONES
--EDDY
--21/12/2018
--------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION modifica_ogestion(e_oid integer, e_obje integer, e_sw integer)
  RETURNS integer AS
$BODY$
DECLARE
 fila record;
 ogestion_mod_id integer;

BEGIN
  
  IF (e_sw = 2) THEN
      INSERT INTO objetivosestrategicos_mod( obje_id, obje_codigo, obje_objetivo, 
            indi_id, obje_indicador, obje_formula, obje_linea_base, obje_meta, 
            obje_fuente_verificacion, obje_metodo, obje_tendencia, obje_supuestos, 
            obje_presupuesto, obje_ponderacion, obje_gestiones, obje_gestion_curso, 
            pdes_id, fun_id, ptdi_id, obje_archivo_adjunto, obje_eficacia, 
            obje_eficiencia, obje_eficiencia_pe, obje_eficiencia_fi, obje_total_casos, 
            obje_casos_favorables, obje_casos_desfavorables, obje_denominador, 
            fecha_mod)
        SELECT obje_id, obje_codigo, obje_objetivo, indi_id, obje_indicador, 
       obje_formula, obje_linea_base, obje_meta, obje_fuente_verificacion, 
       obje_metodo, obje_tendencia, obje_supuestos, obje_presupuesto, 
       obje_ponderacion, obje_gestiones, obje_gestion_curso, pdes_id, 
       fun_id, ptdi_id, obje_archivo_adjunto, obje_eficacia, obje_eficiencia, 
       obje_eficiencia_pe, obje_eficiencia_fi, obje_total_casos, obje_casos_favorables, 
       obje_casos_desfavorables, obje_denominador, fecha_mod
  FROM objetivosestrategicos WHERE obje_id = e_obje;

        Select objetivosestrategicos_mod_id INTO ogestion_mod_id from objetivosestrategicos_mod order by objetivosestrategicos_mod_id desc limit 1;

        INSERT INTO obje_prog_gestion_mod(objetivosestrategicos_mod_id, opm_id, obje_id, g_id, opm_programado)
                select ogestion_mod_id, opm_id, obje_id, g_id, opm_programado from obje_prog_gestion where obje_id = e_obje;


  END IF;

  INSERT INTO objetivosgestion_mod(o_id, obje_id, o_codigo, o_objetivo, indi_id, o_indicador, o_formula, o_linea_base, o_meta, o_fuente_verificacion, o_supuestos, o_ponderacion, aper_id, 
                o_cierre, o_gestion, fun_id, o_archivo_adjunto, o_eficacia, o_efinanciera, o_epejecucion, o_efisica, fecha_creacion, o_estado, o_total_casos, o_casos_favorables, 
                o_casos_desfavorables, o_denominador, fecha_mod)
        SELECT o_id, obje_id, o_codigo, o_objetivo, indi_id, o_indicador, o_formula,  o_linea_base, o_meta, o_fuente_verificacion, o_supuestos, o_ponderacion, 
           aper_id, o_cierre, o_gestion, fun_id, o_archivo_adjunto, o_eficacia,    o_efinanciera, o_epejecucion, o_efisica, fecha_creacion, o_estado, 
           o_total_casos, o_casos_favorables, o_casos_desfavorables, o_denominador, fecha_mod FROM objetivosgestion WHERE o_id = e_oid;

    Select objetivosgestion_mod_id INTO ogestion_mod_id from objetivosgestion_mod order by objetivosgestion_mod_id desc limit 1;

    INSERT INTO ogestion_prog_mes_mod(objetivosgestion_mod_id, opm_id, o_id, mes_id, opm_fis)
                select ogestion_mod_id, opm_id, o_id, mes_id, opm_fis from ogestion_prog_mes where o_id = e_oid;

    
RETURN 1;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;


--------------------------------------------------------------------------------
--Modificando el titulo del menu de modificaciones
--EDDY
--24/12/2018
--------------------------------------------------------------------------------
UPDATE opciones set o_titulo = 'Modificaciones de Objetivos de Acción de Corto Plazo Terminales' where o_id = 30028;

--------------------------------------------------------------------------------
--Modificando el titulo del menu de modificaciones
--EDDY
--27/12/2018
--------------------------------------------------------------------------------
ALTER TABLE objetivosgestion ADD COLUMN justifica_m text;
ALTER TABLE objetivosgestion ADD COLUMN norma_m character varying(300);


--------------------------------------------------------------------------------
--AGREGANDO FUNCION PARA EL CONTEO DE PRODUCTO TERMINAL
--MODIFICANDO LA VISTA PARA RANGOS DE GESTION Y DEPENDENCIA DE PRODUCTO TERMINAL
--EDDY
--08/01/2019
--------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION get_cont_pt(id_obje numeric)
  RETURNS integer AS
$BODY$
declare
cantidad integer;
begin
SELECT count(*) INTO cantidad
    FROM pterminal_oestrategico
    WHERE obje_id = id_obje::integer;
    return cantidad;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE;



CREATE OR REPLACE VIEW v_objetivos_estrategicos_pt AS 
 SELECT o.obje_id,
    o.obje_codigo,
    o.obje_objetivo,
    o.indi_id,
    ( SELECT get_indicador(o.indi_id) AS get_indicador) AS indicador,
    ( SELECT get_indi_abrev(o.indi_id) AS get_indi_abrev) AS indi_abrev,
    o.obje_indicador,
    o.obje_linea_base,
    o.obje_meta,
    o.obje_ponderacion,
    o.obje_fuente_verificacion,
    o.obje_supuestos,
    o.obje_formula,
    o.obje_total_casos,
    o.obje_casos_favorables,
    o.obje_casos_desfavorables,
    o.fun_id,
    f.fun_nombre,
    f.fun_paterno,
    f.fun_materno,
    ( SELECT get_unidad(f.uni_id) AS get_unidad) AS get_unidad,
    o.obje_eficacia,
    o.obje_eficiencia,
    o.obje_eficiencia_pe,
    o.obje_eficiencia_fi,
    o.pdes_id,
    o.ptdi_id,
    o.obje_gestion_curso,
    o.obje_archivo_adjunto,
    p.pdes_descripcion AS pdes_accion,
    p.pdes_codigo AS pdes_acod,
    q.pdes_descripcion AS pdes_resultado,
    q.pdes_codigo AS pdes_rcod,
    r.pdes_descripcion AS pdes_meta,
    r.pdes_codigo AS pdes_mcod,
    s.pdes_descripcion AS pdes_pilar,
    s.pdes_codigo AS pdes_pcod,
    w.ptdi_descripcion AS ptdi_programa,
    w.ptdi_codigo AS ptdi_pcod,
    x.ptdi_descripcion AS ptdi_politica,
    x.ptdi_codigo AS ptdi_ocod,
    y.ptdi_descripcion AS ptdi_eje,
    y.ptdi_codigo AS ptdi_ecod,
    ( SELECT get_cont_pt(o.obje_id) AS get_cont_pt) AS pt_id
   FROM objetivosestrategicos o
     JOIN funcionario f ON o.fun_id = f.fun_id
     JOIN pdes p ON o.pdes_id = p.pdes_id
     JOIN pdes q ON p.pdes_depende = q.pdes_codigo
     JOIN pdes r ON q.pdes_depende = r.pdes_codigo
     JOIN pdes s ON r.pdes_depende = s.pdes_codigo
     JOIN ptdi w ON o.ptdi_id = w.ptdi_id
     JOIN ptdi x ON w.ptdi_depende = x.ptdi_codigo
     JOIN ptdi y ON x.ptdi_depende = y.ptdi_codigo
  WHERE q.pdes_gestion = o.obje_gestion_curso AND r.pdes_gestion = o.obje_gestion_curso AND s.pdes_gestion = o.obje_gestion_curso 
  AND (o.obje_gestion_curso between w.ptdi_gestion and w.ptdi_gestion_fin) AND (o.obje_gestion_curso between x.ptdi_gestion and x.ptdi_gestion_fin) 
  AND (o.obje_gestion_curso between y.ptdi_gestion and y.ptdi_gestion_fin);


/*******************************************************************/
--------------------------------------------------------------------------------
--Tablas, y vistas para la programación de insumos
--
--EMERSON
--17/01/2019
--------------------------------------------------------------------------------
create sequence insumos_partida_ins_id_seq
start with 1
  increment by 1
  minvalue 1;

CREATE TABLE "public"."insumos_partida" (
  "insp_id" numeric(18) NOT NULL DEFAULT nextval('insumos_partida_ins_id_seq'::regclass),
  "par_id" int4 NOT NULL DEFAULT 0,
  "ff_id" int4 NOT NULL DEFAULT 0,
  "of_id" int4 NOT NULL DEFAULT 0,
  "fun_id" int4 NOT NULL DEFAULT 0,
	"et_id" int4 NOT NULL DEFAULT 0,
  "insp_gestion" int4,
  "insp_estado" int4 NOT NULL DEFAULT 1,
  "insp_justificacion" varchar(5000) COLLATE "pg_catalog"."default",
  "insp_inicial" numeric(18,2) NOT NULL DEFAULT 0,
  "insp_modifi" numeric(18,2) NOT NULL DEFAULT 0,
  "insp_actual" numeric(18,2) NOT NULL DEFAULT 0,
  "insp_fecha_registro" timestamp(6) NOT NULL DEFAULT ('now'::text)::timestamp(0) with time zone,
  "insp_fecha_modif" timestamp(6) NOT NULL DEFAULT ('now'::text)::timestamp(0) with time zone,
  CONSTRAINT "insumos_partida_insp_id" PRIMARY KEY ("insp_id"),
  CONSTRAINT "fk_insumos_partida_partidas" FOREIGN KEY ("par_id") REFERENCES "public"."partidas" ("par_id") ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT "insumos_partida_fuentefinanciamiento_ff_id_fkey" FOREIGN KEY ("ff_id") REFERENCES "public"."fuentefinanciamiento" ("ff_id") ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT "insumos_partida_funcionario_fun_id_fkey" FOREIGN KEY ("fun_id") REFERENCES "public"."funcionario" ("fun_id") ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT "insumos_partida_gestion_insp_gestion_fkey" FOREIGN KEY ("insp_gestion") REFERENCES "public"."gestion" ("g_id") ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT "insumos_partida_organismo_of_id_fkey" FOREIGN KEY ("of_id") REFERENCES "public"."organismofinanciador" ("of_id") ON DELETE NO ACTION ON UPDATE NO ACTION,
	  CONSTRAINT "insumos_partida_entidadtransferencia_et_id_fkey" FOREIGN KEY ("et_id") REFERENCES "public"."entidadtransferencia" ("et_id") ON DELETE NO ACTION ON UPDATE NO ACTION
)
;

ALTER TABLE "public"."insumos_partida"
  OWNER TO "postgres";

CREATE TRIGGER "tbl_atributos_tg_audit" AFTER UPDATE OR DELETE ON "public"."insumos_partida"
FOR EACH ROW
EXECUTE PROCEDURE "audit"."fn_log_audit"();


ALTER TABLE insumos ADD COLUMN insp_id int4;

CREATE TABLE "public"."insumos_partida_actividad" (
  "act_id" numeric(18) NOT NULL DEFAULT 0,
  "insp_id" numeric(18) NOT NULL DEFAULT 0,
  CONSTRAINT "fk_insumopartida_actividad_act_id" FOREIGN KEY ("act_id") REFERENCES "public"."_actividades" ("act_id") ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT "fk_insumopartida_insp_id" FOREIGN KEY ("insp_id") REFERENCES "public"."insumos_partida" ("insp_id") ON DELETE NO ACTION ON UPDATE NO ACTION
)
;

ALTER TABLE "public"."insumos_partida_actividad"
  OWNER TO "postgres";

CREATE OR REPLACE VIEW "public"."vrelacion_insumo_partida_actividad" AS
	SELECT
	  insumos_partida.insp_id,
	  insumos_partida.par_id,
    partidas.par_codigo,
		partidas.par_nombre,
    insumos_partida.ff_id,
    fuentefinanciamiento.ff_codigo,
		fuentefinanciamiento.ff_descripcion,
    insumos_partida.of_id,
    organismofinanciador.of_codigo,
		organismofinanciador.of_descripcion,
    insumos_partida.et_id,
		entidadtransferencia.et_codigo,
		entidadtransferencia.et_descripcion,
    insumos_partida.insp_gestion,
    insumos_partida.insp_estado,
    insumos_partida.insp_justificacion,
    insumos_partida.insp_inicial,
    insumos_partida.insp_modifi,
    insumos_partida.insp_actual,
    insumos_partida.insp_fecha_registro,
    insumos_partida_actividad.act_id,
    _actividades.act_actividad
		FROM (((((insumos_partida
     JOIN insumos_partida_actividad ON ((insumos_partida_actividad.insp_id = insumos_partida.insp_id)))
     JOIN partidas ON ((insumos_partida.par_id = partidas.par_id)))
     JOIN fuentefinanciamiento ON ((insumos_partida.ff_id = fuentefinanciamiento.ff_id)))
     JOIN organismofinanciador ON ((insumos_partida.of_id = organismofinanciador.of_id))
		 JOIN entidadtransferencia ON ((insumos_partida.et_id = entidadtransferencia.et_id)))
     JOIN _actividades ON ((insumos_partida_actividad.act_id = _actividades.act_id)))
  WHERE (insumos_partida.insp_estado <> 3);

ALTER TABLE "public"."vrelacion_insumo_partida_actividad" OWNER TO "postgres";


CREATE OR REPLACE  VIEW "public"."vlista_insumos" AS
SELECT insumos.ins_id,
    insumos.ins_codigo,
    to_char(insumos.ins_fecha_requerimiento, 'DD-MM-YYYY'::text) AS ins_fecha_requerimiento,
    insumos.ins_perfil,
    insumos.ins_cant_requerida,
    insumos.ins_costo_unitario,
    insumos.ins_costo_total,
    to_char(insumos.ins_fecha_inicio, 'DD-MM-YYYY'::text) AS ins_fecha_inicio,
    to_char(insumos.ins_fecha_conclusion, 'DD-MM-YYYY'::text) AS ins_fecha_conclusion,
    insumos.ins_duracion,
    insumos.ins_detalle,
    insumos.ins_unidad_medida,
    insumos.ins_cargo,
    insumos.ins_objetivo,
    insumos.ins_actividades,
    insumos.ins_evaluador,
    insumos.ins_productos,
    insumos.ins_gestion,
    insumos.par_id,
    ( SELECT fnget_par.par_codigo
           FROM fnget_par(insumos.par_id) fnget_par(par_nombre, par_depende, par_codigo)) AS par_codigo,
    ( SELECT fnget_par.par_nombre
           FROM fnget_par(insumos.par_id) fnget_par(par_nombre, par_depende, par_codigo)) AS par_nombre,
    insumos.ins_tipo,
    ( SELECT fnget_tipo_ins.ti_abreviatura
           FROM fnget_tipo_ins(insumos.ins_tipo) fnget_tipo_ins(ti_nombre, ti_abreviatura)) AS ti_abreviatura,
    ( SELECT fnget_tipo_ins.ti_nombre
           FROM fnget_tipo_ins(insumos.ins_tipo) fnget_tipo_ins(ti_nombre, ti_abreviatura)) AS ti_nombre,
    insumos.fun_id,
    insumos.car_id,
    ( SELECT fnget_cargo.car_cargo
           FROM fnget_cargo(insumos.car_id) fnget_cargo(car_depende, car_cargo, car_sueldo)) AS car_cargo,
		insumos.insp_id
   FROM insumos
  WHERE (insumos.ins_estado = ANY (ARRAY[1, 2]));

ALTER TABLE "public"."vlista_insumos" OWNER TO "postgres";

CREATE OR REPLACE VIEW "public"."vrelacion_proy_prod_act_ins" AS  SELECT p.proy_id,
    p.pfec_fecha_inicio,
    p.pfec_fecha_fin,
    c.com_id,
    r.prod_id,
    a.act_id,
    (COALESCE(i.ins_id, (0)::numeric))::integer AS ins_id,
    i.ins_codigo,
    i.ins_fecha_requerimiento,
    i.ins_perfil,
    i.ins_cant_requerida,
    i.ins_costo_unitario,
    i.ins_costo_total,
    i.ins_fecha_inicio,
    i.ins_fecha_conclusion,
    i.ins_duracion,
    i.ins_detalle,
    i.ins_unidad_medida,
    i.ins_cargo,
    i.ins_objetivo,
    i.ins_actividades,
    i.ins_evaluador,
    i.ins_productos,
    i.ins_gestion,
    i.par_id,
    i.par_codigo,
    i.par_nombre,
    i.ins_tipo,
    i.ti_abreviatura,
    i.ti_nombre,
    i.fun_id,
    i.car_id,
    i.car_cargo,
		i.insp_id
   FROM (((((_proyectofaseetapacomponente p
     LEFT JOIN _componentes c ON ((p.pfec_id = c.pfec_id)))
     LEFT JOIN _productos r ON ((c.com_id = r.com_id)))
     LEFT JOIN _actividades a ON ((r.prod_id = a.prod_id)))
     LEFT JOIN _insumoactividad ia ON ((a.act_id = ia.act_id)))
     LEFT JOIN vlista_insumos i ON ((ia.ins_id = i.ins_id)))
  WHERE ((p.pfec_estado = 1) AND (p.estado = ANY (ARRAY[(1)::numeric, (2)::numeric])) AND (c.estado = ANY (ARRAY[(1)::numeric, (2)::numeric])) AND (r.estado = ANY (ARRAY[1, 2])) AND (a.estado = ANY (ARRAY[1, 2])));

ALTER TABLE "public"."vrelacion_proy_prod_act_ins" OWNER TO "postgres";

create sequence insumo_partida_gestion_id_seq
  start with 1
  increment by 1
  minvalue 1;

CREATE TABLE "public"."insumo_partida_gestion" (
  "inspg_id" numeric(18) NOT NULL DEFAULT nextval('insumo_partida_gestion_id_seq'::regclass),
  "insp_id" numeric(18) NOT NULL,
  "g_id" int4 NOT NULL,
  "inspg_monto_prog" numeric(18,2) NOT NULL,
  "inspg_estado" int4 DEFAULT 1,
  CONSTRAINT "insumo_partida_gestion_pkey" PRIMARY KEY ("inspg_id"),
  CONSTRAINT "insumo_partida_gestion_g_id_fkey" FOREIGN KEY ("g_id") REFERENCES "public"."gestion" ("g_id") ON DELETE NO ACTION ON UPDATE NO ACTION,
	CONSTRAINT "insumo_partida_gestion_insp_id_fkey" FOREIGN KEY ("insp_id") REFERENCES "public"."insumos_partida" ("insp_id") ON DELETE NO ACTION ON UPDATE NO ACTION
)
;

ALTER TABLE "public"."insumo_partida_gestion"
  OWNER TO "postgres";

CREATE TRIGGER "tbl_atributos_tg_audit" AFTER UPDATE OR DELETE ON "public"."insumo_partida_gestion"
FOR EACH ROW
EXECUTE PROCEDURE "audit"."fn_log_audit"();

--------------------------------------------------------------------------------
--Tablas para la relacion de insumos con componente (caso de programacion de insumos por componente)
--
--EMERSON
--18/01/2019
--------------------------------------------------------------------------------
CREATE TABLE "public"."insumos_partida_componente" (
  "com_id" numeric(18) NOT NULL DEFAULT 0,
  "insp_id" numeric(18) NOT NULL DEFAULT 0,
  CONSTRAINT "fk_insumopartida_componente_com_id" FOREIGN KEY ("com_id") REFERENCES "public"."_componentes" ("com_id") ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT "fk_insumopartida_componente_insp_id" FOREIGN KEY ("insp_id") REFERENCES "public"."insumos_partida" ("insp_id") ON DELETE NO ACTION ON UPDATE NO ACTION
)
;

ALTER TABLE "public"."insumos_partida_componente"
  OWNER TO "postgres";


CREATE VIEW "public"."vrelacion_insumo_partida_componente" AS  SELECT insumos_partida.insp_id,
    insumos_partida.par_id,
    partidas.par_codigo,
    partidas.par_nombre,
    insumos_partida.ff_id,
    fuentefinanciamiento.ff_codigo,
    fuentefinanciamiento.ff_descripcion,
    insumos_partida.of_id,
    organismofinanciador.of_codigo,
    organismofinanciador.of_descripcion,
    insumos_partida.et_id,
    entidadtransferencia.et_codigo,
    entidadtransferencia.et_descripcion,
    insumos_partida.insp_gestion,
    insumos_partida.insp_estado,
    insumos_partida.insp_justificacion,
    insumos_partida.insp_inicial,
    insumos_partida.insp_modifi,
    insumos_partida.insp_actual,
    insumos_partida.insp_fecha_registro,
    insumos_partida_componente.com_id,
    _componentes.com_componente
   FROM ((((((insumos_partida
     JOIN insumos_partida_componente ON ((insumos_partida_componente.insp_id = insumos_partida.insp_id)))
     JOIN partidas ON ((insumos_partida.par_id = partidas.par_id)))
     JOIN fuentefinanciamiento ON ((insumos_partida.ff_id = fuentefinanciamiento.ff_id)))
     JOIN organismofinanciador ON ((insumos_partida.of_id = organismofinanciador.of_id)))
     JOIN entidadtransferencia ON ((insumos_partida.et_id = entidadtransferencia.et_id)))
     JOIN _componentes ON ((insumos_partida_componente.com_id = _componentes.com_id)))
  WHERE (insumos_partida.insp_estado <> 3);

ALTER TABLE "public"."vrelacion_insumo_partida_actividad" OWNER TO "postgres";

--------------------------------------------------------------------------------
--Modificación de tabla insumo_partida_gestion
--
--EMERSON
--18/01/2019
--------------------------------------------------------------------------------
ALTER TABLE "public"."insumo_partida_gestion" ADD COLUMN "m_id" int4;

--------------------------------------------------------------------------------
--Incorporación de vistas para el regsitro de programación delegada
--
--EMERSON
--18/01/2019
--------------------------------------------------------------------------------
CREATE VIEW "public"."vifin_prog_mes" AS  SELECT i.ifin_id,
    i.insg_id,
    i.ff_id,
    i.of_id,
    i.et_id,
    i.ifin_monto,
    i.ifin_gestion,
    i.nro_if,
    i.ffofet_id,
    ff.ff_codigo,
    oof.of_codigo,
    et.et_codigo,
    sum(ip.ipm_fis) AS programado_total,
    to_number(COALESCE(string_agg(
        CASE
            WHEN (ip.mes_id = 1) THEN to_char(ip.ipm_fis, '999999999999.99'::text)
            ELSE NULL::text
        END, ''::text), '0'::text), '999999999999.99'::text) AS mes1,
    to_number(COALESCE(string_agg(
        CASE
            WHEN (ip.mes_id = 2) THEN to_char(ip.ipm_fis, '999999999999.99'::text)
            ELSE NULL::text
        END, ''::text), '0'::text), '999999999999.99'::text) AS mes2,
    to_number(COALESCE(string_agg(
        CASE
            WHEN (ip.mes_id = 3) THEN to_char(ip.ipm_fis, '999999999999.99'::text)
            ELSE NULL::text
        END, ''::text), '0'::text), '999999999999.99'::text) AS mes3,
    to_number(COALESCE(string_agg(
        CASE
            WHEN (ip.mes_id = 4) THEN to_char(ip.ipm_fis, '999999999999.99'::text)
            ELSE NULL::text
        END, ''::text), '0'::text), '999999999999.99'::text) AS mes4,
    to_number(COALESCE(string_agg(
        CASE
            WHEN (ip.mes_id = 5) THEN to_char(ip.ipm_fis, '999999999999.99'::text)
            ELSE NULL::text
        END, ''::text), '0'::text), '999999999999.99'::text) AS mes5,
    to_number(COALESCE(string_agg(
        CASE
            WHEN (ip.mes_id = 6) THEN to_char(ip.ipm_fis, '999999999999.99'::text)
            ELSE NULL::text
        END, ''::text), '0'::text), '999999999999.99'::text) AS mes6,
    to_number(COALESCE(string_agg(
        CASE
            WHEN (ip.mes_id = 7) THEN to_char(ip.ipm_fis, '999999999999.99'::text)
            ELSE NULL::text
        END, ''::text), '0'::text), '999999999999.99'::text) AS mes7,
    to_number(COALESCE(string_agg(
        CASE
            WHEN (ip.mes_id = 8) THEN to_char(ip.ipm_fis, '999999999999.99'::text)
            ELSE NULL::text
        END, ''::text), '0'::text), '999999999999.99'::text) AS mes8,
    to_number(COALESCE(string_agg(
        CASE
            WHEN (ip.mes_id = 9) THEN to_char(ip.ipm_fis, '999999999999.99'::text)
            ELSE NULL::text
        END, ''::text), '0'::text), '999999999999.99'::text) AS mes9,
    to_number(COALESCE(string_agg(
        CASE
            WHEN (ip.mes_id = 10) THEN to_char(ip.ipm_fis, '999999999999.99'::text)
            ELSE NULL::text
        END, ''::text), '0'::text), '999999999999.99'::text) AS mes10,
    to_number(COALESCE(string_agg(
        CASE
            WHEN (ip.mes_id = 11) THEN to_char(ip.ipm_fis, '999999999999.99'::text)
            ELSE NULL::text
        END, ''::text), '0'::text), '999999999999.99'::text) AS mes11,
    to_number(COALESCE(string_agg(
        CASE
            WHEN (ip.mes_id = 12) THEN to_char(ip.ipm_fis, '999999999999.99'::text)
            ELSE NULL::text
        END, ''::text), '0'::text), '999999999999.99'::text) AS mes12
   FROM ((((ifin_prog_mes ip
     JOIN insumo_financiamiento i ON ((i.ifin_id = ip.ifin_id)))
     JOIN fuentefinanciamiento ff ON ((i.ff_id = ff.ff_id)))
     JOIN organismofinanciador oof ON ((i.of_id = oof.of_id)))
     JOIN entidadtransferencia et ON ((i.et_id = et.et_id)))
  WHERE (i.ifin_estado = ANY (ARRAY[1, 2]))
  GROUP BY i.ifin_id, ff.ff_codigo, oof.of_codigo, et.et_codigo
  ORDER BY i.ifin_id;

ALTER TABLE "public"."vifin_prog_mes" OWNER TO "postgres";

CREATE VIEW "public"."vproy_insumo_delegado_programado" AS  SELECT p.proy_id,
    p.pfec_fecha_inicio,
    p.pfec_fecha_fin,
    c.com_id,
    c.com_componente,
    c.com_ponderacion,
    ur.uni_unidad,
    (COALESCE(i.ins_id, (0)::numeric))::integer AS ins_id,
    i.ins_codigo,
    i.ins_fecha_requerimiento,
    i.ins_perfil,
    i.ins_cant_requerida,
    i.ins_costo_unitario,
    i.ins_costo_total,
    i.ins_fecha_inicio,
    i.ins_fecha_conclusion,
    i.ins_duracion,
    i.ins_detalle,
    i.ins_unidad_medida,
    i.ins_cargo,
    i.ins_objetivo,
    i.ins_actividades,
    i.ins_evaluador,
    i.ins_productos,
    i.ins_gestion,
    i.par_id,
    i.par_codigo,
    i.par_nombre,
    i.ins_tipo,
    i.ti_abreviatura,
    i.ti_nombre,
    i.fun_id,
    i.car_id,
    i.car_cargo,
    ig.g_id,
    iprog.nro_if,
    iprog.ffofet_id,
    iprog.ff_codigo,
    iprog.ff_id,
    iprog.of_codigo,
    iprog.of_id,
    iprog.et_codigo,
    iprog.et_id,
    iprog.programado_total,
    iprog.mes1,
    iprog.mes2,
    iprog.mes3,
    iprog.mes4,
    iprog.mes5,
    iprog.mes6,
    iprog.mes7,
    iprog.mes8,
    iprog.mes9,
    iprog.mes10,
    iprog.mes11,
    iprog.mes12
   FROM ((((((_proyectofaseetapacomponente p
     LEFT JOIN _componentes c ON ((p.pfec_id = c.pfec_id)))
     JOIN unidadorganizacional ur ON ((c.uni_id = ur.uni_id)))
     LEFT JOIN insumocomponente ic ON ((c.com_id = ic.com_id)))
     JOIN vlista_insumos i ON ((ic.ins_id = i.ins_id)))
     JOIN insumo_gestion ig ON ((ig.ins_id = i.ins_id)))
     JOIN vifin_prog_mes iprog ON ((iprog.insg_id = ig.insg_id)))
  WHERE ((p.pfec_estado = 1) AND (p.estado = ANY (ARRAY[(1)::numeric, (2)::numeric])) AND (c.estado = ANY (ARRAY[(1)::numeric, (2)::numeric])));

ALTER TABLE "public"."vproy_insumo_delegado_programado" OWNER TO "postgres";


--------------------------------------------------------------------------------
--Creacion de la tabla 
--
--EMERSON
--08/02/2019
--------------------------------------------------------------------------------
CREATE TABLE mod_ogestion_adjuntos
(
  pa_id serial NOT NULL,
  pa_nombre character varying(500),
  pa_ruta_archivo character varying(500),
  fecha_creacion timestamp without time zone DEFAULT ('now'::text)::timestamp(0) with time zone,
  o_id integer NOT NULL,
  CONSTRAINT mod_ogestion_adjuntos_pkey PRIMARY KEY (pa_id),
  CONSTRAINT mod_ogestion_adjuntos_reg_id_fkey FOREIGN KEY (o_id)
      REFERENCES objetivosgestion (o_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);


---------------------------------------- WILMER -----------
create table mod_est_proy(
  mod_id serial not null,
  proy_id numeric(18,0) NOT NULL,
  r_id integer,
  primary key(mod_id)
 );



create table historial_proyectofaseetapacomponente(
  hpfec_id serial not null,
  pfec_id numeric(18,0),
  pfec_fecha_inicio_ddmmaaa timestamp without time zone NOT NULL,
  pfec_fecha_fin_ddmmaaa timestamp without time zone NOT NULL,
  num_ip character varying(50),
  nom_ip character varying(50),
  fecha timestamp without time zone DEFAULT ('now'::text)::timestamp(0) with time zone,
  fun_id integer NOT NULL DEFAULT 0,
  primary key(hpfec_id)
 );



-------- Tabla historial de productos modificados
create table _hproductos(
  hprod_id serial not null,
  prod_id numeric(18,0) NOT NULL,
  prod_producto character varying(2000),
  indi_id integer NOT NULL,
  prod_indicador character varying(500),
  prod_linea_base numeric(18,2) NOT NULL DEFAULT 0,
  prod_meta numeric(18,2) NOT NULL DEFAULT 0,
  fecha timestamp without time zone DEFAULT ('now'::text)::timestamp(0) with time zone,
  estado integer DEFAULT 1,
  fun_id integer NOT NULL DEFAULT 0,
  num_ip character varying(50),
  nom_ip character varying(50),
  primary key(hprod_id)
 );


-------- Tabla Historial de temporalidad de producto
create table hprod_programado_mensual(
  hpg_id serial not null,
  hprod_id integer NOT NULL,
  m_id integer NOT NULL DEFAULT 0,
  pg_fis numeric(18,2) NOT NULL DEFAULT 0,
  pg_fin numeric(18,2) NOT NULL DEFAULT 0,
  g_id integer NOT NULL,
  primary key(hpg_id)
 );



-------- Tabla historial de Actividad modificados
create table _hactividades(
  hact_id serial not null,
  act_id numeric(18,0) NOT NULL,
  act_linea_base numeric(18,3) NOT NULL DEFAULT 0,
  act_meta numeric(18,3) NOT NULL DEFAULT 0,
  fecha timestamp without time zone DEFAULT ('now'::text)::timestamp(0) with time zone,
  estado integer DEFAULT 1,
  fun_id integer NOT NULL DEFAULT 0,
  num_ip character varying(50),
  nom_ip character varying(50),
  primary key(hact_id)
 );


-------- Tabla Historial de temporalidad de Actividad
create table hact_programado_mensual(
  hag_id serial not null,
  hact_id integer NOT NULL,
  m_id integer NOT NULL DEFAULT 0,
  pg_fis numeric(18,2) NOT NULL DEFAULT 0,
  pg_fin numeric(18,2) NOT NULL DEFAULT 0,
  g_id integer NOT NULL,
  primary key(hag_id)
 );



-------- Tabla Ptto Fase modficado
create table _ptto_fase_modificado(
  pttom_id serial not null,
  pfec_id numeric(18,0) NOT NULL,
  num_ip character varying(50),
  nom_ip character varying(50),
  fecha timestamp without time zone DEFAULT ('now'::text)::timestamp(0) with time zone,
  fun_id integer NOT NULL DEFAULT 0,
  estado integer NOT NULL DEFAULT 1,
  primary key(pttom_id)
 );

-------- Tabla Ptto Fase Gestion
 create table _ptto_fase_gestion_historial(
  pttoh_id serial not null,
  pttom_id integer NOT NULL,
  ptofecg_id numeric(18,0) NOT NULL,
  pfecg_ppto_total numeric(18,2) DEFAULT 0,
  g_id integer NOT NULL DEFAULT 0,
  primary key(pttoh_id)
 );


create table insumo_partida_historial(
  insph_id serial not null,
  insp_id numeric(18,0),
  
  insp_inicial numeric(18,2) NOT NULL DEFAULT 0,
  insp_modifi numeric(18,2) NOT NULL DEFAULT 0,
  insp_actual numeric(18,2) NOT NULL DEFAULT 0,
  
  num_ip character varying(50),
  nom_ip character varying(50),
  fecha timestamp without time zone DEFAULT ('now'::text)::timestamp(0) with time zone,
  fun_id integer NOT NULL DEFAULT 0,
  primary key(insph_id)
 );


 create table insumo_partida_gestion_historial(
  inspgh_id serial not null,
  insph_id integer,
  
  g_id integer NOT NULL,
  inspg_monto_prog numeric(18,2) NOT NULL,
  inspg_estado integer DEFAULT 1,
  m_id integer,
  
  primary key(inspgh_id)
 );