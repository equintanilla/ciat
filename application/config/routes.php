<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$route['default_controller'] = "user";
$route['404_override'] = 'user/vaca_404';

$route['admin/logout'] = 'user/logout';
$route['admin/validate'] = 'user/validate_credentials';
$route['admin/dashboard'] = 'user/dashboard_index';
$route['admin/dm/(:any)'] = 'user/dashboard_menu/$1';

/*REPORTES TAQPACHA*/
$route['taqpacha'] = 'taqpacha';				//Pagina inicial: listado de reportes de TAQPACHA
$route['taqpacha/pip'] = 'taqpacha/pip';		//1. Presupuesto de inversion publica (proyectos) por año
$route['taqpacha/ppnr'] = 'taqpacha/ppnr';		//2. Presupuesto programas no recurrentes por año
$route['taqpacha/epip'] = 'taqpacha/epip';		//3. Estado de proyectos de inversion publica por año
$route['taqpacha/eppnr'] = 'taqpacha/eppnr';	//4. Estado de programas no recurrentes por año
$route['taqpacha/ipse'] = 'taqpacha/ipse';		//5. Inversion publica anual por sector economico
$route['taqpacha/ippnr'] = 'taqpacha/ippnr';	//6. Presupuesto programas no recurrentes por sector economico
$route['taqpacha/ipr'] = 'taqpacha/ipr';		//7. Inversion publica anual por region
$route['taqpacha/ipr_detalle'] = 'taqpacha/ipr_detalle';
$route['taqpacha/pnrr'] = 'taqpacha/pnrr';		//8. Presupuesto programas no recurrentes anual por region
$route['taqpacha/pnrr_detalle'] = 'taqpacha/pnrr_detalle';
$route['taqpacha/ipp'] = 'taqpacha/ipp';		//9. Inversion publica anual por provincia
$route['taqpacha/ipp_detalle'] = 'taqpacha/ipp_detalle';		//9. Inversion publica anual por provincia
$route['taqpacha/pnrp'] = 'taqpacha/pnrp';		//10. Presupuesto programas no recurrentes anual por provincia
$route['taqpacha/pnrp_detalle'] = 'taqpacha/pnrp_detalle';
$route['taqpacha/ipm'] = 'taqpacha/ipm';		//11. Inversion publica anual por municipio
$route['taqpacha/ipm_detalle'] = 'taqpacha/ipm_detalle';
$route['taqpacha/pnrm'] = 'taqpacha/pnrm';		//12. Presupuesto programas no recurrentes anual por municipio
$route['taqpacha/pnrm_detalle'] = 'taqpacha/pnrm_detalle';

/*REPORTES MAE*/
$route['reportes_mae/region'] = 'reportes_mae/reportes_mae/acciones_por_region';
$route['reportes_mae/valida_region'] = 'reportes_mae/reportes_mae/valida_acciones_por_region';
$route['reportes_mae/reporte_region/(:any)'] = 'reportes_mae/reportes_mae/reporte_acciones_por_region/$1/$2/$3/$4/$5/$6';
$route['reportes_mae/excel_region/(:any)'] = 'reportes_mae/reportes_mae/excel_acciones_por_region/$1/$2/$3/$4/$5/$6';

$route['reportes_mae/provincia'] = 'reportes_mae/reportes_mae/acciones_por_provincia';
$route['reportes_mae/valida_provincia'] = 'reportes_mae/reportes_mae/valida_acciones_por_provincia';
$route['reportes_mae/reporte_provincia/(:any)'] = 'reportes_mae/reportes_mae/reporte_acciones_por_provincia/$1/$2/$3/$4/$5/$6';

$route['reportes_mae/municipio'] = 'reportes_mae/reportes_mae/acciones_por_municipio';
$route['reportes_mae/municipio_valida'] = 'reportes_mae/reportes_mae/valida_acciones_por_muni';
$route['reportes_mae/reporte_municipio/(:any)'] = 'reportes_mae/reportes_mae/reporte_acciones_por_municipio/$1/$2/$3/$4/$5/$6';

$route['reportes_mae/unidad_ejec'] = 'reportes_mae/reportes_mae/acciones_por_unidad_ejecutora';
$route['reportes_mae/valida_datos']='reportes_mae/reportes_mae/validar_acciones_por_unidad_ejecutora';
$route['reportes_mae/reporte_ue/(:any)'] = 'reportes_mae/reportes_mae/reporte_acciones_por_unidad_ejecutora/$1/$2/$3/$4/$5/$6';

$route['reportes_mae/estado'] = 'reportes_mae/reportes_mae/acciones_operativas_por_estado';
$route['reportes_mae/valida_estado'] = 'reportes_mae/reportes_mae/valida_acciones_operativas_por_estado';
$route['reportes_mae/reporte_estado/(:any)'] = 'reportes_mae/reportes_mae/reporte_acciones_por_estado/$1/$2/$3/$4/$5/$6';

$route['reportes_mae/sector'] = 'reportes_mae/reportes_mae/acciones_operativas_por_sector';
$route['reportes_mae/valida_sector'] = 'reportes_mae/reportes_mae/valida_acciones_operativas_por_sector';



$route['reportes_mae/valida_datos_cod'] ='reportes_mae/creporte_mae/validar_seleccion_cod';
$route['reportes_mae/acc_operativas'] = 'reportes_mae/reportes_mae/mis_operaciones';
$route['reportes_mae/resumen_ejec/(:any)'] = 'reportes_mae/reportes_mae/resumen_ejecucion/$1/$2';


/*REPORTE AVANCE CARGADO DATOS*/
$route['taqpacha/cargado'] = 'taqpacha/cargado';//Listado de proyectos y programas que especifican 

/*REPORTES SGP*/
$route['sgp/(:num)'] = 'sgpp/sgp/reporte/$1';//Identificacion proyecto programa SGPP
$route['sgp/avance/(:num)'] = 'sgpp/sgp/fisico/$1';//Avance fisico financiero del SGPP

/*PROGRAMACION*/
$route['admin/combo_ubicacion'] = 'user/combo_ubicacion';
$route['admin/combo_fase_etapas'] = 'user/combo_fases_etapas';
$route['admin/combo_clasificador'] = 'user/combo_clasificador';

/*-------------------------PROGRAMACION ESTRATEGICA-----------------------------*/
$route['admin/pei/mision'] = 'user/mision';
$route['admin/pei/mision/accion/(:any)'] = 'user/pei_accion/$1';
$route['admin/pei/vision'] = 'user/vision';
$route['admin/pei/ayuda/acerca'] = 'user/acerca';

//-----------------------------------------------------ROY ----------------------------------------------------------
//================================= OBJETIVOS ESTRATEGICOS2  ===========================================================
$route['prog/me/objetivo'] = 'programacion/cme_objetivos/lista_objetivos'; ////// lista de objetivos estrategicos
$route['prog/me/n_obj'] = 'programacion/cme_objetivos/nuevo_objetivo'; ////// nuevo objetivo estrategico
$route['prog/me/add_obj'] = 'programacion/cme_objetivos/guardar_objetivo'; ////// nuevo objetivo estrategico
$route['prog/me/ind/(:any)'] = 'programacion/cme_objetivos/indicador_desempenio/$1';  //// Inidcador de desempenio
$route['prog/me/add_id'] = 'programacion/cme_objetivos/valida_indicador';  //// valida indicador de desempenio
$route['prog/me/add_pdf'] = 'programacion/cme_objetivos/guardar_archivo';  //// valida indicador de desempenio
$route['prog/me/mod_obje/(:any)'] = 'programacion/cme_objetivos/vista_modificar_objetivo/$1';  //// valida indicador de desempenio
$route['prog/me/mod_obj'] = 'programacion/cme_objetivos/modificar_objetivo'; ////// modificar objetivo estrategico

//-----------------------------------------------------EDDY ----------------------------------------------------------
//================================= PRODUCTO TERMINAL  ===========================================================
$route['prog/pterminalg/(:any)'] = 'programacion/cme_prod_terminal/lista_objetivos/$1';//LISTA DE PRODUCTO TERMINAL
$route['prog/ptg_nuevo/(:any)'] = 'programacion/cme_prod_terminal/nuevo_pterminal/$1';//NUEVO PRODUCTO TERMINAL
$route['prog/ptg_guardar'] = 'programacion/cme_prod_terminal/guardar_pterminal';//GUARDAR PRODUCTO TERMINAL
$route['prog/ptg_mod/(:any)'] = 'programacion/cme_prod_terminal/vista_modificar_objetivot/$1/$2';  //// MODIFICAR PRODUCTO TERMINAL
$route['prog/me/mod_objt'] = 'programacion/cme_prod_terminal/modificar_objetivot'; ////// modificar objetivo estrategico

$route['prog/add/ptg_pdf'] = 'programacion/cprog_prod_terminal/guardar_archivo';//GUARDAR ARCHIVO
// $route['prog/ptg_mod/(:any)'] = 'programacion/cprog_prod_terminal/form_mod_pterminal/$1/$2/$3/$4';//VISTA MODIFICAR 



//////////////////////////////////MANTENIMIENTO   2   /////////////////////////////////////////
//APERTURA PROGRAMATICA PADRES
$route['mnt/prog_p'] = 'mantenimiento/capertura_programatica/main_apertura_programatica_padres';
//APERTURA PROGRAMATICA HIJOS
$route['mnt/aper_prog'] = 'mantenimiento/capertura_programatica/main_apertura_programatica';
//analasis ver si habra abm de las aperturas
$route['admin/mantenimiento/add_aper'] = 'cmantenimiento/add_aper';
$route['admin/mantenimiento/get_aper'] = 'cmantenimiento/get_aper';
$route['admin/mantenimiento/del_aper'] = 'cmantenimiento/del_aper';
//POA
$route['mnt/poa'] = 'mantenimiento/cpoa';
$route['mnt/delete'] = 'mantenimiento/cpoa/eliminar_poa';

//ASIGNAR OBJ ESTRATEGICO  A CARPETA POA
$route['mnt/red_o'] = 'mantenimiento/cpoa/red_objetivos';
$route['mnt/asignar/(:any)'] = 'mantenimiento/cpoa/asignar_obj_poa/$1';


//================================= PROGRAMACION ================================
//PTOGRAMACION DEL POA - RED DE OBJETIVOS
$route['prog/redobj'] = 'programacion/cprog_red_objetivos';
$route['prog/obj/(:any)'] = 'programacion/cprog_red_objetivos/lista_objetivos/$1';//LISTA DE OBJETIVO ESTRATEGICOS
//--- OBJETIVO DE GESTION
$route['prog/obje_gestion/(:any)'] = 'programacion/cprog_objetivo_gestion/lista_ogestion/$1/$2';//LISTA DE GESTION
$route['prog/o_nuevo/(:any)'] = 'programacion/cprog_objetivo_gestion/nuevo_ogestion/$1/$2';//NUEVO OBJETIVO DE GESTION
$route['prog/o_guardar'] = 'programacion/cprog_objetivo_gestion/add_objetivo_gestion';//GUARDAR O MODIFICAR
$route['prog/add/o_pdf'] = 'programacion/cprog_objetivo_gestion/guardar_archivo';//GUARDAR ARCHIVO
$route['prog/o_mod/(:any)'] = 'programacion/cprog_objetivo_gestion/form_mod_ogestion/$1/$2/$3';//VISTA MODIFICAR OBJETIVO DE GESTION
//--- PRODUCTO TERMINAL
$route['prog/pterminal/(:any)'] = 'programacion/cprog_prod_terminal/lista_pterminal/$1/$2/$3';//LISTA DE PRODUCTO TERMINAL
$route['prog/pt_nuevo/(:any)'] = 'programacion/cprog_prod_terminal/nuevo_pterminal/$1/$2/$3';//NUEVO PRODUCTO TERMINAL
$route['prog/pt_guardar'] = 'programacion/cprog_prod_terminal/guardar_pterminal';//GUARDAR PRODUCTO TERMINAL
$route['prog/add/pt_pdf'] = 'programacion/cprog_prod_terminal/guardar_archivo';//GUARDAR ARCHIVO
$route['prog/pt_mod/(:any)'] = 'programacion/cprog_prod_terminal/form_mod_pterminal/$1/$2/$3/$4';//VISTA MODIFICAR PRODUCTO TERMINAL
//--- DIRECTO - PROGRAMACION DE INSUMOS
$route['prog/ins/(:any)'] = 'programacion/cprog_insumos/insumos/$1/$2';//--

// --- DIRECTO - PROGRAMACION DE INSUMOS / ACTIVIDADES
$route['prog/ins_act/(:any)'] = 'insumos/cprog_insumos_directo/prog_isumos_act/$1/$2/$3';//PROGRAMACION DE INSUMOS A NIVEL ACTIVIDADES
$route['prog/nuevo_ins_a/(:any)'] = 'insumos/cprog_insumos_directo/nuevo_insumo/$1/$2/$3/$4';//NUEVO INSUMO
$route['prog/mod_ins_a/(:any)'] = 'insumos/cprog_insumos_directo/mod_insumo/$1/$2/$3/$4/$5/$6';//MODIFICAR INSUMO
$route['prog/ins_a_prog/(:any)'] = 'insumos/cprog_insumos_directo/insumo_programado/$1/$2/$3/$4/$5/$6';//NUEVO INSUMOS A NIVEL COMPONENTE PROGRAMADO -w
$route['prog/del_ins_a/(:any)'] = 'insumos/cprog_insumos_directo/del_insumo/$1/$2/$3/$4';//ELIMINAR INSUMOS A NIVEL ACTIVIDADES
$route['prog/delete_ins_a'] = 'insumos/cprog_insumos_directo/delete_insumo';//ELIMINAR INSUMOS POR MODAL ACTIVIDADES

// --- INSUMO PARTIDA
$route['prog/ins_part/(:any)'] = 'insumos/cprog_insumos_partida/prog_isumos_partida/$1/$2/$3';//PROGRAMACION DE INSUMOS A PARTIDAS
$route['prog/nuevo_ins_part/(:any)'] = 'insumos/cprog_insumos_partida/nuevo_insumo_partida/$1/$2/$3';//NUEVO INSUMO PARTIDA
$route['prog/mod_ins_part/(:any)'] = 'insumos/cprog_insumos_partida/mod_insumo_partida/$1/$2/$3/$4';//MODIFICA INSUMO PARTIDA
$route['prog/del_ins_part'] = 'insumos/cprog_insumos_partida/delete_insumo_partida';//ELIMINAR INSUMOS A NIVEL ACTIVIDADES

$route['prog/ins_part_com/(:any)'] = 'insumos/cprog_insumos_partida/prog_isumos_partida_comp/$1/$2';
$route['prog/nuevo_ins_part_com/(:any)'] = 'insumos/cprog_insumos_partida/nuevo_insumo_partida_com/$1/$2';
$route['prog/mod_ins_part_com/(:any)'] = 'insumos/cprog_insumos_partida/mod_insumo_partida_com/$1/$2/$3';

//--- DELEGADO - PROGRAMACION DE INSUMOS/COMPONENTES Wilmer
$route['prog/ins_com/(:any)'] = 'insumos/cprog_insumos_delegado/prog_isumos_com/$1/$2/$3';//PROGRAMACION DE INSUMOS A NIVEL COMPONENTE LISTA -w
$route['prog/combo_partidas'] = 'insumos/cprog_insumos_delegado/combo_partidas_hijos';// COMBO PARTIDAS -w
$route['prog/nuevo_ins_c/(:any)'] = 'insumos/cprog_insumos_delegado/nuevo_insumo/$1/$2/$3/$4';//NUEVO INSUMOS A NIVEL COMPONENTE -w
$route['prog/ins_c_prog/(:any)'] = 'insumos/cprog_insumos_delegado/insumo_programado/$1/$2/$3/$4';//NUEVO INSUMOS A NIVEL COMPONENTE PROGRAMADO -w

$route['prog/mod_ins_com/(:any)'] = 'insumos/cprog_insumos_delegado/mod_insumo/$1/$2/$3/$4/$5';//MODIFICAR INSUMOS A NIVEL COMPONENTE -w
$route['prog/del_ins_com/(:any)'] = 'insumos/cprog_insumos_delegado/del_insumo/$1/$2/$3';//ELIMINAR INSUMOS A NIVEL COMPONENTE -w
$route['prog/delete_ins_com'] = 'insumos/cprog_insumos_delegado/delete_insumo';//ELIMINAR INSUMOS POR MODAL COMPONENTES
$route['prog/verif_ptto_gestion'] = 'insumos/cprog_insumos_delegado/verif_ptto_asignado'; ////// verificando Ptto Asignado por Gestiones

//--- PONDERACION
$route['prog/pond_o'] = 'programacion/cponderacion_ogestion/lista_red_objetivos_o';//objetivo de gestion - vista de red de objetivos para la ponderacion
$route['prog/pond_ogestion/(:any)'] = 'programacion/cponderacion_ogestion/lista_ogestion/$1';//objetivo de gestion pondracion
$route['prog/pond_pt'] = 'programacion/cponderacion_pterminal/lista_red_objetivos_pt';//producto terminal- vista de red de objetivos para la ponderacion
$route['prog/list_ogestion/(:any)'] = 'programacion/cponderacion_pterminal/lista_ogestion/$1';//lista de objetivo de gestion - producto terminal
$route['prog/pond_pterminal/(:any)'] = 'programacion/cponderacion_pterminal/lista_pterminal/$1/$2';//lista de objetivo de gestion - producto terminal
$route['prog/pond_prog'] = 'programacion/cponderacion_programas/ponderacion_programas';//objetivo de gestion - vista de red de objetivos para la ponderacion
$route['prog/pond_p'] = 'programacion/cponderacion_proyecto/lista_red_objetivos_p';//lista de programas para la ponderacion del proyecto
$route['prog/pond_proy/(:any)'] = 'programacion/cponderacion_proyecto/ponderacion_proyectos/$1';//lista de proyectos
//=========================================FIN DE PROGRAMACION ==============================================

//===================== REGISTRO DE EJECUCION
//--- REGISTRO EJECUCION POA
$route['reg/ejec_op'] = 'registro/cejec_ogestion_pterminal/lista_red_programas';
$route['reg/lo/(:any)'] = 'registro/cejec_ogestion_pterminal/lista_ogestion_pterminal/$1/$2/$3';
$route['reg/add_ejec_op'] = 'registro/cejec_ogestion_pterminal/add_ejec_op';
$route['reg/lm/(:any)'] = 'registro/cejec_ogestion_pterminal/mostrar_ejec_programa/$1/$2';
$route['reg/lmes/(:any)'] = 'registro/cejec_ogestion_pterminal/lista_mes_ejec/$1/$2';
$route['reg/ejec_mes/(:any)'] = 'registro/cejec_ogestion_pterminal/ejecucion_mes/$1/$2/$3';
$route['reg/arc/(:any)'] = 'registro/cejec_ogestion_pterminal/lista_archivos/$1/$2';
$route['reg/add_arc'] = 'registro/cejec_ogestion_pterminal/add_arc';
$route['reg/list_arc/(:any)'] = 'registro/cejec_ogestion_pterminal/l_archivos_mes/$1/$2/$3/4';
$route['admin/lmreporte/(:any)'] = 'registro/reporte_ejec/mostrar_ejec_programa/$1/$2';
//registro sigep
$route['reg/l_sigep'] = 'registro/cejec_pres_sigep/lista_proy_ejec';
$route['reg/c_sigep'] = 'registro/cejec_pres_sigep/lista_ejec_pres_csv';
$route['reg/add_sigep'] = 'registro/cejec_pres_sigep/subir_sigep';


//======================== REPORTES---------------------------------------

$route['rep/seg_pt'] = 'reportes/cseg_alerta_pterminal';//MENU SEGUIMIENTO DEL PRODUCTO TERMINAL
$route['rep/nivel_pt/(:any)'] = 'reportes/cseg_alerta_pterminal/lista_ogestion/$1';//lista de objetivo de gestion
$route['rep/lista_pt/(:any)'] = 'reportes/cseg_alerta_pterminal/lista_pterminal/$1/$2';//lista de productos terminales
$route['rep/graf_pt/(:any)'] = 'reportes/cseg_alerta_pterminal/grafico_pterminal/$1/$2/$3';//alerta temprana del producto terminal
$route['rep/nivel_pt_o/(:any)'] = 'reportes/cseg_alerta_pterminal/nivel_ogestion/$1';//nivel objetivo de gestion
$route['rep/graf_oges/(:any)'] = 'reportes/cseg_alerta_pterminal/grafico_nivel_ogestion/$1/$2';//nivel objetivo de gestion
$route['rep/nivel_pt_pr/(:any)'] = 'reportes/cseg_alerta_pterminal/nivel_programa/$1';//nivel programa
$route['rep/inst_pt'] = 'reportes/cseg_alerta_pterminal/nivel_institucion';//nivel institucion



/*============================= MODULO DE REPORTES =============================================*/
$route['admin/rep/at_acciones'] = 'reportes/reporte_w/alerta_temprana_acciones'; //// menu alerta temprara
/*-------------------- Seguimiento Temprana de acciones ----------------------*/
$route['admin/rep/productos'] = 'reportes/reporte_w/productos'; //// productos total por institucion
$route['admin/rep/list_productos'] = 'reportes/reporte_w/programas_productos'; //// productos a nivel programas
$route['admin/rep/list_productos_proy/(:any)'] = 'reportes/reporte_w/proyectos_productos/$1'; //// productos a nivel proyectos

$route['admin/rep/acciones'] = 'reportes/reporte_w/acciones';
$route['admin/rep/list_acciones'] = 'reportes/reporte_w/programas_acciones'; //// acciones a nivel programas
$route['admin/rep/list_acciones_proy/(:any)'] = 'reportes/reporte_w/proyectos_acciones/$1'; //// Acciones a nivel proyectos

/*------------------------------- Resumen de ejecucion fisica Financiera-------------------------------------*/
$route['admin/rep/rep_refis'] = 'reportes/reporte_w/seleccion_reporte_resumen_fis'; //// Seleccion de las Operaciones Fisica
$route['admin/rep/rep_refin'] = 'reportes/reporte_w/seleccion_reporte_resumen_fin'; //// Seleccion de las Operaciones Financiera
$route['admin/rep/valida_resfisfin'] = 'reportes/reporte_w/validar_seleccion_resumen_fis_fin'; //// valida seleccion
$route['admin/rep/gerencial_proyectos/(:any)'] = 'reportes/reporte_w/reporte_gerencial_proyectos/$1/$2/$3/$4/$5'; //// a nivel institucional EJECUCION FISICA Y FINANCIERA
$route['admin/rep/ver_proyectos_fis/(:any)'] = 'reportes/reporte_w/ver_reporte_gerencial_fis/$1/$2/$3/$4/$5/$6/$7'; ///// ver los proyectos gerenciales segun su tipo-	EJECUCION FISICA
$route['admin/rep/ver_proyectos_fin/(:any)'] = 'reportes/reporte_w/ver_reporte_gerencial_fin/$1/$2/$3/$4/$5'; ///// ver los proyectos gerenciales segun su tipo-	EJECUCION FINANCIERA

$route['rep/rep_avance_financiero/(:any)'] = 'reportes/reporte_w/reporte_avance_financiero/$1/$2/$3/$4/$5'; //// Imprimir Reporte Financiero
$route['rep/rep_avance_fisico/(:any)'] = 'reportes/reporte_w/reporte_avance_fisico/$1/$2/$3/$4/$5'; //// Imprimir Reporte Fisico
$route['rep/reporte_proyectos_fin/(:any)'] = 'reportes/reporte_w/reporte_gerencial_financiero/$1/$2/$3/$4/$5'; ///// ver los proyectos gerenciales segun su tipo-	EJECUCION FINANCIERA
$route['rep/reporte_proyectos_fis/(:any)'] = 'reportes/reporte_w/reporte_gerencial_fisica/$1/$2/$3/$4/$5'; ///// ver los proyectos gerenciales segun su tipo-	EJECUCION FISICA

/*------------------------------- Resumen de ejecucion fisica (Nuevo niveles de ejecucion) -------------------------------------*/
$route['rep/ejec_fis'] = 'reporte_niveles/reporte_nfisica/seleccion_reporte_fis'; //// Seleccion de las Operaciones para la ejecucion Fisica
$route['rep/valida_resfis'] = 'reporte_niveles/reporte_nfisica/validar_seleccion_resumen_fis'; //// valida seleccion avance fisico
$route['rep/reporte_nfisico/(:any)'] = 'reporte_niveles/reporte_nfisica/reporte_ejecucion_niveles/$1/$2/$3/$4/$5/$6/$7/$8'; //// Get Accion Proyecto en ejecucion

/*------------------------------- Resumen de ejecucion financiera (Nuevo niveles de ejecucion) -------------------------------------*/
$route['rep/ejec_fin'] = 'reporte_niveles/reporte_nfinanciero/seleccion_reporte_fin'; //// Seleccion de las Operaciones para la ejecucion Fisica
$route['rep/valida_resfin'] = 'reporte_niveles/reporte_nfinanciero/validar_seleccion_resumen_fin'; //// valida seleccion avance fisico
$route['rep/reporte_nfinanciero/(:any)'] = 'reporte_niveles/reporte_nfinanciero/reporte_ejecucion_niveles/$1/$2/$3/$4/$5/$6/$7/$8'; //// Reporte Financiero

//------------ REPORTE - EJECUCION PRESUPUESTARIA (Nuevo Reportes Tarija)-------------------
$route['rep/rep_ejecucion_presupuestaria'] = 'reportes_lpz/c_tgasto/menu_reporte'; //// Menu Ejecucion Presupuestaria 

$route['rep/ejec/tipo_gasto'] = 'reportes_lpz/c_tgasto/rubro_gasto'; //// Rubro de Gasto 1
$route['rep/ejec/rep_tipo_gasto'] = 'reportes_lpz/c_tgasto/reporte_rubro_gasto'; //// Rubro de Gasto REPORTE 1
$route['rep/ejec/rep_tipo_gasto_grafico'] = 'reportes_lpz/c_tgasto/reporte_rubro_gasto_grafico'; //// Rubro de Gasto REPORTE GRAFICO

/// ------------ EJECUCION PRESUPUESTARIA GENERAL POR UNIDAD EJECUTORA -------------------------------
$route['rep/ejec/unidad_ejec'] = 'reportes_lpz/c_uejec/unidad_ejecutora'; //// Por unidad Ejecutora 2
$route['rep/ejec/rep_unidad_ejec'] = 'reportes_lpz/c_uejec/reporte_unidad_ejecutora'; //// Por unidad Ejecutora REPORTE 2
$route['rep/ejec/rep_unidad_ejec_grafico'] = 'reportes_lpz/c_uejec/reporte_unidad_ejecutora_grafico'; //// Por unidad Ejecutora REPORTE GRAFICO
$route['rep/ejec/det_unidad_ejec/(:any)'] = 'reportes_lpz/c_uejec/detalle_unidad_ejecutora/$1';  /// Detalle Unidad Ejecutora
$route['rep/ejec/rep_det_unidad_ejec/(:any)'] = 'reportes_lpz/c_uejec/reporte_detalle_unidad_ejecutora/$1';  /// Detalle Unidad Ejecutora REPORTE

/// ------------ EJECUCION PRESUPUESTARIA POR SECTORES ECONOMICOS PI -------------------------------
$route['rep/ejec/unidad_ejec_proy'] = 'reportes_lpz/c_proysector/pinversion_unidad_ejecutora'; //// proy inversion Por unidad Ejecutora 3
$route['rep/ejec/rep_unidad_ejec_proy'] = 'reportes_lpz/c_proysector/reporte_pinversion_unidad_ejecutora'; //// proy inversion Por unidad Ejecutora REPORTE 3
$route['rep/ejec/det_unidad_ejec_proy/(:any)'] = 'reportes_lpz/c_proysector/detalle_pinversion_unidad_ejecutora/$1'; //// proy inversion Por unidad Ejecutora DETALLE 3
$route['rep/ejec/rep_det_unidad_ejec_proy/(:any)'] = 'reportes_lpz/c_proysector/reporte_detalle_pinversion_unidad_ejecutora/$1'; //// proy inversion Por unidad Ejecutora REPORTE DETALLE 3

$route['rep/ejec/sector_proy'] = 'reportes_lpz/c_proysector/pi_sector_economico'; //// proy inversion Por Sector Economico 3
$route['rep/ejec/rep_sector_proy'] = 'reportes_lpz/c_proysector/reporte_pi_sector_economico'; //// proy inversion Por Sector Economico REPORTE 3
$route['rep/ejec/rep_sector_proy_grafico'] = 'reportes_lpz/c_proysector/reporte_pi_sector_economico_grafico'; //// proy inversion Por Sector Economico REPORTE GRAFICO
$route['rep/ejec/detalle_sector_proy/(:any)'] = 'reportes_lpz/c_proysector/detalle_pi_sector_economico/$1'; //// proy inversion Por Sector Economico DETALLE 3
$route['rep/ejec/rep_detalle_sector_proy/(:any)'] = 'reportes_lpz/c_proysector/reporte_detalle_pi_sector_economico/$1'; //// proy inversion Por Sector Economico REPORTE DETALLE 3

$route['rep/ejec/unidad_ejec_prog'] = 'reportes_lpz/c_progsector/prog_unidad_ejecutora'; //// Programas Por unidad Ejecutora 4
$route['rep/ejec/rep_unidad_ejec_prog'] = 'reportes_lpz/c_progsector/reporte_prog_unidad_ejecutora'; //// programas Por unidad Ejecutora REPORTE 4
$route['rep/ejec/det_unidad_ejec_prog/(:any)'] = 'reportes_lpz/c_progsector/detalle_prog_unidad_ejecutora/$1'; //// programas Por unidad Ejecutora DETALLE 4
$route['rep/ejec/rep_det_unidad_ejec_prog/(:any)'] = 'reportes_lpz/c_progsector/reporte_detalle_prog_unidad_ejecutora/$1'; //// programas Por unidad Ejecutora REPORTE DETALLE 4

$route['rep/ejec/sector_prog'] = 'reportes_lpz/c_progsector/prog_sector_economico'; //// Programas Por Sector Economico 4
$route['rep/ejec/rep_sector_prog'] = 'reportes_lpz/c_progsector/reporte_prog_sector_economico'; //// Programas Por Sector Economico REPORTE 5
$route['rep/ejec/rep_sector_prog_grafico'] = 'reportes_lpz/c_progsector/reporte_prog_sector_economico_grafico'; //// Programas Por Sector Economico REPORTE GRAFICO 5
$route['rep/ejec/detalle_sector_prog/(:any)'] = 'reportes_lpz/c_progsector/detalle_prog_sector_economico/$1'; //// programas Por Sector Economico DETALLE 5
$route['rep/ejec/rep_detalle_sector_prog/(:any)'] = 'reportes_lpz/c_progsector/reporte_detalle_prog_sector_economico/$1'; //// programas Por Sector Economico REPORTE DETALLE 5

$route['rep/ejec/gasto_corriente'] = 'reportes_lpz/c_gcorriente/gasto_corriente'; //// Ejecucion Presupuestaria de Gasto Corriente 5
$route['rep/ejec/rep_gasto_corriente'] = 'reportes_lpz/c_gcorriente/reporte_gasto_corriente'; //// Ejecucion Presupuestaria de Gasto Corriente REPORTE 5
$route['rep/ejec/detalle_rep_gasto_corriente/(:any)'] = 'reportes_lpz/c_gcorriente/detalle_gasto_corriente/$1'; //// Ejecucion Presupuestaria de Gasto Corriente DETALLE 5
$route['rep/ejec/rep_detalle_rep_gasto_corriente/(:any)'] = 'reportes_lpz/c_gcorriente/reporte_detalle_prog_gasto_corriente/$1'; //// Ejecucion Presupuestaria de Gasto Corriente REPORTE DETALLE 5


$route['rep/ejec/transferencias'] = 'reportes_lpz/c_transferencias/transferencias_cap_municipales'; //// Transferencias Capitales Municipales
$route['rep/ejec/transferencias_corriente'] = 'reportes_lpz/c_transferencias/transferencias_corriente_municipales'; //// Transferencias Corrientes Municipales

$route['rep/ejec/rep_transferencias/(:any)'] = 'reportes_lpz/c_transferencias/rep_transferencias/$1'; //// Transferencias Municipales REPORTE
$route['rep/ejec/det_trans_muni/(:any)'] = 'reportes_lpz/c_transferencias/detalle_transferencias_municipales/$1/$2/$3'; //// Transferencias Municipales DETALLE
$route['rep/ejec/rep_det_trans_muni/(:any)'] = 'reportes_lpz/c_transferencias/reporte_detalle_transferencias_municipales/$1/$2/$3'; //// Transferencias Municipales DETALLE


$route['rep/ejec/presupuestaria'] = 'reportes_lpz/c_epresupuestaria/presupuestaria_general'; //// Ejecucion del Presupuesto General
$route['rep/ejec/rep_presupuestaria'] = 'reportes_lpz/c_epresupuestaria/rep_pres_general'; //// Ejecucion del Presupuesto General REPORTE
$route['rep/ejec/detalle_presupuestaria/(:any)'] = 'reportes_lpz/c_epresupuestaria/detalle_pres_general/$1/$2'; //// Ejecucion del Presupuesto General DETALLE
$route['rep/ejec/rep_detalle_presupuestaria/(:any)'] = 'reportes_lpz/c_epresupuestaria/reporte_detalle_pres_general/$1/$2'; //// Ejecucion del Presupuesto General DETALLE REPORTE

$route['rep/ejec/presupuestaria_pi'] = 'reportes_lpz/c_epresupuestaria/presupuestaria_pinversion'; //// Ejecucion del Presupuesto Proyecto de Inversion
$route['rep/ejec/rep_presupuestaria_pi'] = 'reportes_lpz/c_epresupuestaria/rep_pres_pinversion'; //// Ejecucion del Presupuesto Proyectos REPORTE
$route['rep/ejec/detalle_presupuestaria_pi/(:any)'] = 'reportes_lpz/c_epresupuestaria/detalle_pres_pinversion/$1/$2'; //// Ejecucion del Presupuesto Proyectos DETALLE
$route['rep/ejec/rep_detalle_presupuestaria_pi/(:any)'] = 'reportes_lpz/c_epresupuestaria/reporte_detalle_pres_pinversion/$1/$2'; //// Ejecucion del Presupuesto Proyectos DETALLE REPORTE

$route['rep/ejec/presupuestaria_prog'] = 'reportes_lpz/c_epresupuestaria/presupuestaria_prog'; //// Ejecucion del Presupuesto Programas
$route['rep/ejec/rep_presupuestaria_prog'] = 'reportes_lpz/c_epresupuestaria/rep_pres_prog'; //// Ejecucion del Presupuesto Programas REPORTE
$route['rep/ejec/detalle_presupuestaria_prog/(:any)'] = 'reportes_lpz/c_epresupuestaria/detalle_pres_prog/$1/$2'; //// Ejecucion del Presupuesto Programas DETALLE
$route['rep/ejec/rep_detalle_presupuestaria_prog/(:any)'] = 'reportes_lpz/c_epresupuestaria/reporte_detalle_pres_prog/$1/$2/$3'; //// Ejecucion del Presupuesto Programas DETALLE REPORTE
/*-----------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*========================================= REPORTES DAS ==================================*/
$route['rep/ejec/menu_das'] = 'reportes_das/cdas_total/menu_reporte_das'; //// Menu Reportes DAS
//------------------------------- REPORTE - DAS TOTAL----------------------------------------
$route['rep/ejec/das'] = 'reportes_das/cdas_total/direccion_administrativa_total'; //// Total ejecucion por Direcciones Administrativas
$route['rep/ejec/reporte_das'] = 'reportes_das/cdas_total/reporte_direccion_administrativa_total'; //// Total ejecucion por Direcciones Administrativas REPORTE
$route['rep/ejec/reporte_das_graf'] = 'reportes_das/cdas_total/reporte_direccion_administrativa_total_graf'; //// Total ejecucion por Direcciones Administrativas REPORTE GRAFICO

$route['rep/ejec/detalle_das/(:any)'] = 'reportes_das/cdas_total/detalle_direccion_administrativa_total/$1'; //// Total ejecucion por Direcciones Administrativas DETALLE
$route['rep/ejec/rep_detalle_das/(:any)'] = 'reportes_das/cdas_total/reporte_direccion_administrativa_total_detalle/$1'; //// Total ejecucion por Direcciones Administrativas REPORTE DETALLE
/*-------------------------------------------------------------------------------------------------------------------------*/

//------------------------------- REPORTE - DAS PROYECTOS DE INVERSION ---------------------------------------
$route['rep/ejec/das_pi'] = 'reportes_das/cdas_pi/direccion_administrativa_pi'; //// Total ejecucion por Direcciones Administrativas PI
$route['rep/ejec/reporte_das_pi'] = 'reportes_das/cdas_pi/reporte_direccion_administrativa_pi'; //// Total ejecucion por Direcciones Administrativas PI REPORTE
$route['rep/ejec/reporte_das_graf_pi'] = 'reportes_das/cdas_pi/reporte_direccion_administrativa_total_graf_pi'; //// Total ejecucion por Direcciones Administrativas PI REPORTE GRAFICO

$route['rep/ejec/detalle_das_pi/(:any)'] = 'reportes_das/cdas_pi/detalle_direccion_administrativa_total_pi/$1'; //// Total ejecucion por Direcciones Administrativas PI DETALLE
$route['rep/ejec/rep_detalle_das_pi/(:any)'] = 'reportes_das/cdas_pi/reporte_direccion_administrativa_pi_detalle/$1'; //// Total ejecucion por Direcciones Administrativas PI REPORTE DETALLE
/*-------------------------------------------------------------------------------------------------------------------------*/

//------------------------------- REPORTE - DAS PROGRAMAS ----------------------------------------
$route['rep/ejec/das_prog'] = 'reportes_das/cdas_prog/direccion_administrativa_prog'; //// Total ejecucion por Direcciones Administrativas PROG
$route['rep/ejec/reporte_das_prog'] = 'reportes_das/cdas_prog/reporte_direccion_administrativa_prog'; //// Total ejecucion por Direcciones Administrativas PROG REPORTE
$route['rep/ejec/reporte_das_graf_prog'] = 'reportes_das/cdas_prog/reporte_direccion_administrativa_total_graf_prog'; //// Total ejecucion por Direcciones Administrativas PROG REPORTE GRAFICO

$route['rep/ejec/detalle_das_prog/(:any)'] = 'reportes_das/cdas_prog/detalle_direccion_administrativa_total_prog/$1'; //// Total ejecucion por Direcciones Administrativas PROG  DETALLE
$route['rep/ejec/rep_detalle_das_prog/(:any)'] = 'reportes_das/cdas_prog/reporte_direccion_administrativa_prog_detalle/$1'; //// Total ejecucion por Direcciones Administrativas PROG REPORTE DETALLE
/*-------------------------------------------------------------------------------------------------------------------------*/

//------------------------------- REPORTE - DAS GASTO CORRIENTE ----------------------------------------
$route['rep/ejec/das_gcorriente'] = 'reportes_das/cdas_gcorriente/direccion_administrativa_gcorriente'; //// Total ejecucion por Direcciones Administrativas GASTO CORRIENTE
$route['rep/ejec/reporte_das_gcorriente'] = 'reportes_das/cdas_gcorriente/reporte_direccion_administrativa_gcorriente'; //// Total ejecucion por Direcciones Administrativas GASTO CORRIENTE REPORTE
$route['rep/ejec/reporte_das_graf_gcorriente'] = 'reportes_das/cdas_gcorriente/reporte_direccion_administrativa_total_graf_gcorriente'; //// Total ejecucion por Direcciones Administrativas GASTO CORRIENTE REPORTE GRAFICO

$route['rep/ejec/detalle_das_gcorriente/(:any)'] = 'reportes_das/cdas_gcorriente/detalle_direccion_administrativa_total_gcorriente/$1'; //// Total ejecucion por Direcciones Administrativas GASTO CORRIENTE  DETALLE
$route['rep/ejec/rep_detalle_das_gcorriente/(:any)'] = 'reportes_das/cdas_gcorriente/reporte_direccion_administrativa_gcorriente_detalle/$1'; //// Total ejecucion por Direcciones Administrativas GASTO CORRIENTE REPORTE DETALLE
/*-------------------------------------------------------------------------------------------------------------------------*/

//------------------------------- REPORTE - DAS PROVISIONES FINANCIERAS ----------------------------------------
$route['rep/ejec/das_pfinancieras'] = 'reportes_das/cdas_pfinancieras/direccion_administrativa_pfinancieras'; //// Total ejecucion por Direcciones Administrativas PROVISIONES FINANCIERAS
$route['rep/ejec/reporte_das_pfinancieras'] = 'reportes_das/cdas_pfinancieras/reporte_direccion_administrativa_pfinancieras'; //// Total ejecucion por Direcciones Administrativas PROVISIONES FINANCIERAS REPORTE
$route['rep/ejec/reporte_das_graf_pfinancieras'] = 'reportes_das/cdas_pfinancieras/reporte_direccion_administrativa_total_graf_pfinancieras'; //// Total ejecucion por Direcciones Administrativas PROVISIONES FINANCIERAS REPORTE GRAFICO

$route['rep/ejec/detalle_das_pfinancieras/(:any)'] = 'reportes_das/cdas_pfinancieras/detalle_direccion_administrativa_total_pfinancieras/$1'; //// Total ejecucion por Direcciones Administrativas PROVISIONES FINANCIERAS DETALLE
$route['rep/ejec/rep_detalle_das_pfinancieras/(:any)'] = 'reportes_das/cdas_pfinancieras/reporte_direccion_administrativa_pfinancieras_detalle/$1'; //// Total ejecucion por Direcciones Administrativas PROVISIONES REPORTE DETALLE
/*-------------------------------------------------------------------------------------------------------------------------*/

//------------------------------- REPORTE - DAS SERVICIO DE LA DEUDA ----------------------------------------
$route['rep/ejec/das_sdeuda'] = 'reportes_das/cdas_sdeuda/direccion_administrativa_sdeuda'; //// Total ejecucion por Direcciones Administrativas SERVISIO DE LA DEUDA
$route['rep/ejec/reporte_das_sdeuda'] = 'reportes_das/cdas_sdeuda/reporte_direccion_administrativa_sdeuda'; //// Total ejecucion por Direcciones Administrativas SERVICIO DE LA DEUDA REPORTE
$route['rep/ejec/reporte_das_graf_sdeuda'] = 'reportes_das/cdas_sdeuda/reporte_direccion_administrativa_total_graf_sdeuda'; //// Total ejecucion por Direcciones Administrativas SERVICIO DE LA DEUDA REPORTE GRAFICO

$route['rep/ejec/detalle_das_sdeuda/(:any)'] = 'reportes_das/cdas_sdeuda/detalle_direccion_administrativa_total_sdeuda/$1'; //// Total ejecucion por Direcciones Administrativas SERVICIO DE LA DEUDA DETALLE
$route['rep/ejec/rep_detalle_das_sdeuda/(:any)'] = 'reportes_das/cdas_sdeuda/reporte_direccion_administrativa_sdeuda_detalle/$1'; //// Total ejecucion por Direcciones Administrativas SERVICIO DE LA DEUDA DETALLE REPORTE DETALLE
/*-------------------------------------------------------------------------------------------------------------------------*/

//------------------------------- REPORTE - DAS TRANSFERENCIAS ----------------------------------------
$route['rep/ejec/das_transferencias'] = 'reportes_das/cdas_transferencia/direccion_administrativa_transferencias'; //// Total ejecucion por Direcciones Administrativas TRANSFERENCIA
$route['rep/ejec/reporte_das_transferencias'] = 'reportes_das/cdas_transferencia/reporte_direccion_administrativa_transferencias'; //// Total ejecucion por Direcciones Administrativas TRANSFERENCIA REPORTE
$route['rep/ejec/reporte_das_graf_transferencias'] = 'reportes_das/cdas_transferencia/reporte_direccion_administrativa_total_graf_transferencias'; //// Total ejecucion por Direcciones Administrativas TRANSFERENCIA REPORTE GRAFICO

$route['rep/ejec/detalle_das_transferencias/(:any)'] = 'reportes_das/cdas_transferencia/detalle_direccion_administrativa_total_transferencias/$1'; //// Total ejecucion por Direcciones Administrativas TRANSFERENCIA DETALLE
$route['rep/ejec/rep_detalle_das_transferencias/(:any)'] = 'reportes_das/cdas_transferencia/reporte_direccion_administrativa_transferencias_detalle/$1'; //// Total ejecucion por Direcciones Administrativas SERVICIO DE LA TRANSFERENCIA DETALLE REPORTE DETALLE
/*-------------------------------------------------------------------------------------------------------------------------*/

/*========================================= REPORTES COMPONENTES PTDI ==================================*/
$route['rep/ejec/comp_ptdi'] = 'reportes_cptdi/ccomp_ptdi/ptdi'; //// Ptdi
$route['rep/ejec/rep_grafico_comp_ptdi'] = 'reportes_cptdi/ccomp_ptdi/reporte_componente_estrategico_graf'; ////  Grafico Ptdi
$route['rep/ejec/rep_comp_ptdi'] = 'reportes_cptdi/ccomp_ptdi/reporte_ptdi'; ////  Reporte Ptdi


$route['rep/ejec/detalle_comp_ptdi/(:any)'] = 'reportes_cptdi/ccomp_ptdi/detalle_ptdi/$1'; //// Detalle Ptdi
$route['rep/ejec/rep_detalle_comp_ptdi/(:any)'] = 'reportes_cptdi/ccomp_ptdi/reporte_detalle_ptdi/$1'; //// Detalle REPORTE Ptdi


/*-----------------------------REPORTES SEGUIMIENTO --------------------------*/
/*-------------------------Reportes - Objetivos de Gestion --------------------*/
$route['admin/rep/rep_ogestion'] = 'reportes/reporte_ogestion/objetivos_gestion'; //// Menu Objetivos de Gestion
$route['admin/rep/rep_og_institucion'] = 'reportes/reporte_ogestion/objetivos_gestion_institucional'; ///// a nivel de intitucion
$route['admin/rep/rep_og_institucion_detalles'] = 'reportes/reporte_ogestion/objetivos_gestion_institucional_detalles'; ///// a nivel de intitucion detalle

$route['admin/rep/rep_og_programa/(:any)'] = 'reportes/reporte_ogestion/objetivos_gestion_programa/$1'; ///// a nivel por programa
$route['admin/rep/rep_og_programa_detalles/(:any)'] = 'reportes/reporte_ogestion/objetivos_gestion_programa_detalles/$1'; ///// a nivel de programas detalles

$route['admin/rep/rep_og_objetivo'] = 'reportes/reporte_ogestion/objetivos_gestion_objetivo'; ///// a nivel por objetivo
$route['admin/rep/rep_og_objetivo/og/(:any)'] = 'reportes/reporte_ogestion/objetivos_gestion_objetivo_detalle/$1/$2'; ///// a nivel por Objetivo a detalle
$route['admin/rep/rep_acciones']='reportes/reporte_w/seleccione_reporte';//reportes/reporte_w/seleccion_reporte_resumen_fis
/*-------------------------Reportes - Productos Terminales --------------------*/
$route['admin/rep/rep_pterminal'] = 'reportes/reporte_pterminal/productos_terminales'; //// Menu Producto Terminal
$route['admin/rep/rep_pt_institucion'] = 'reportes/reporte_pterminal/productos_terminales_institucional'; ///// a nivel de intitucion
$route['admin/rep/rep_pt_institucion_detalles'] = 'reportes/reporte_pterminal/productos_terminales_institucional_detalles'; ///// a nivel de intitucion detalle

$route['admin/rep/rep_pt_programa/(:any)'] = 'reportes/reporte_pterminal/productos_terminales_programa/$1'; ///// a nivel por programa
$route['admin/rep/rep_pt_programa_detalles/(:any)'] = 'reportes/reporte_pterminal/productos_terminales_programa_detalles/$1'; ///// a nivel de programas detalles

$route['admin/rep/rep_pt_oprog'] = 'reportes/reporte_pterminal/list_pterminales_oprog'; ///// lista por programas
$route['admin/rep/rep_pt_obj/og/(:any)'] = 'reportes/reporte_pterminal/productos_terminales_objetivo/$1/$2'; ///// a nivel por objetivo
$route['admin/rep/rep_pt_obj/detalles/(:any)'] = 'reportes/reporte_pterminal/productos_terminales_objetivo_detalle/$1/$2'; ///// a nivel por Objetivo a detalle

$route['admin/rep/rep_pt_pprog'] = 'reportes/reporte_pterminal/list_pterminales_ptprog'; ///// lista por programas
$route['admin/rep/rep_pt_pt/pt/(:any)'] = 'reportes/reporte_pterminal/productos_terminales_pt/$1/$2/$3'; ///// a nivel de producto terminal

/*----------------------- Reportes - Ejecucion del Presupuesto -----------------------------------*/
$route['admin/rep/rep_ejec_pres'] = 'reportes/reporte_ejec_presupuesto/ejecucion_presupuesto'; //// Menu Ejecucion Presupuesto
$route['admin/rep/rep_ejec_institucion'] = 'reportes/reporte_ejec_presupuesto/ejecucion_presupuesto_institucional'; ///// a nivel de intitucion
$route['admin/rep/rep_ejec_institucion_detalles'] = 'reportes/reporte_ejec_presupuesto/ejecucion_presupuesto_institucional_detalles'; ///// a nivel de intitucion detalle

$route['admin/rep/rep_ejec_programa/(:any)'] = 'reportes/reporte_ejec_presupuesto/ejecucion_presupuesto_programa/$1'; ///// a nivel por programa
$route['admin/rep/rep_ejec_programa_detalles/(:any)'] = 'reportes/reporte_ejec_presupuesto/ejecucion_presupuesto_programa_detalles/$1'; ///// a nivel de programas detalles

$route['admin/rep/rep_ejec_operacion'] = 'reportes/reporte_ejec_presupuesto/ejecucion_presupuesto_operacion'; ///// a nivel de Operacion
$route['admin/rep/rep_ejec_operacion/op/(:any)'] = 'reportes/reporte_ejec_presupuesto/ejecucion_presupuesto_operacion_detalle/$1/$2'; ///// a nivel de Operacion a detalle
/*--------------------------- REPORTES FORMULARIOS POA ----------------------------------*/
/*--------------------------- reportes Objetivos Estrategicos ---------------------------*/
$route['admin/reporte/obje'] = 'reportes/reporte_fpoa/iframe_objetivo_estrategico'; //// Iframe Objetivo Estrategico
$route['admin/reporte/rep_obj'] = 'reportes/reporte_fpoa/pdf_objetivo_estrategico'; //// Reporte Objetivo Estrategico
/*--------------------------- reportes Analisis de Situacion ---------------------------*/
$route['admin/reporte/anal_sit'] = 'reportes/reporte_fpoa/analisis_programas';
$route['admin/reporte_analisis_situacion/(:any)'] = 'reportes/analisis_situacion/pdf_analisis_situacion/$1';
/*--------------------------- reportes Objetivos Gestion-Pterminal ---------------------------*/
$route['admin/reporte/objges'] = 'reportes/reporte_fpoa/og_pterminal_programas'; ///// Lista Aperturas Programaticas
$route['admin/reporte/rep_og_pterminal/(:any)'] = 'reportes/reporte_fpoa/reporte_og_pterminal/$1'; //// Reporte Objetivo Gestion-Producto Terminal
/*--------------------------- reportes Acciones ---------------------------*/
$route['admin/rep/prog'] = 'reportes/reporte_fpoa/accion_programas'; ///// Lista de Programas para acciones
$route['admin/reporte/reporte_acciones/(:any)'] = 'reportes/reporte_fpoa/reporte_acciones/$1/$2'; //// Reporte Acciones


/*------------------------------- Evaluacion -------------------------------------*/
$route['admin/rep/evaluacion_acciones'] = 'reportes/reporte_w2/evaluacion_acciones'; //// Evaluacion de Acciones
$route['admin/rep/proyectos/(:any)'] = 'reportes/reporte_w2/list_proyectos/$1'; //// Evaluacion de Acciones por proyectos
$route['admin/rep/eficacia/(:any)'] = 'reportes/reporte_w2/evaluacion_eficacia/$1/$2'; //// Tabal de Eficacia
$route['admin/rep/valida_eficacia'] = 'reportes/reporte_w2/valida_evaluacion'; //// valida evaluacion Eficacia
$route['admin/rep/evaluacion/(:any)'] = 'reportes/reporte_w2/iframe_evaluacion/$1/$2'; //// Iframe Evaluacion
$route['admin/rep/rep_evaluacion/(:any)'] = 'reportes/reporte_w2/evaluacion_accion/$1/$2'; //// Evaluacion Proyecto


//REPORTE - PRESUPUESTO
$route['rep/pres_prog'] = 'reportes/crep_pres_prog';
$route['rep/prog_lproy/(:any)'] = 'reportes/crep_pres_prog/lista_proyectos/$1';
$route['rep/pres_prog_proy/(:any)'] = 'reportes/crep_pres_prog/presupuesto_programado/$1/$2/$3';
$route['rep/pres_ejec'] = 'reportes/crep_pres_ejec';
$route['rep/pres_ejec_lproy/(:any)'] = 'reportes/crep_pres_ejec/lista_proyectos/$1';
$route['rep/pres_ejec_proy/(:any)'] = 'reportes/crep_pres_ejec/presupuesto_ejecutado/$1/$2/$3';
//REPORTE- EVALUACION
$route['rep/eva_institucional'] = 'reportes/crep_eva_institucional';
$route['rep/eva_programacion'] = 'reportes/crep_eva_programacion';
$route['rep/ev_prog/(:any)'] = 'reportes/crep_eva_programacion/evaluacion_programa/$1';

$route['rep/eva_institucional'] = 'reportes/crep_eva_institucional';
$route['rep/eva_programacion'] = 'reportes/crep_eva_programacion';


/*======================= OBJETIVOS ESTRATEGICOS============================================*/
$route['admin/me/objetivos'] = 'objetivos/list_objetivos'; ////// lista de objetivos estrategicos
$route['admin/me/objetivo'] = 'objetivos/objetivo_estrategico'; ///// formulario de registro
$route['admin/me/add'] = 'objetivos/valida_objetivo'; ////// validar datos
$route['admin/me/modificar/(:any)'] = 'objetivos/modificar_objetivo/$1'; ////// formulario de editado
$route['admin/me/update'] = 'objetivos/update_objetivo'; ////// validar datos de editado

$route['admin/me/delet_obj'] = 'objetivos/delete_objetivo'; //// eliminar objetivo
$route['admin/me/indicador/(:any)'] = 'objetivos/indicador_d/$1/$2';  //// Inidcador de desempenio
$route['admin/me/add_id'] = 'objetivos/valida_indicador';  //// valida indicador de desempenio
/*---------------------------------- PDF --------------------------------------*/
$route['admin/me/pdf'] = 'objetivos/b_pdf';
$route['admin/me/ficha_tecnica'] = 'reportes/reporte/ficha_tecnica';

$route['admin/me/ver_pdf_obj'] = 'objetivos/ver_pdf_obj';
$route['admin/me/o_pdf'] = 'objetivos/subir_pdf_obj';

/*======================== VALIDADOR POA - FINANCIERO ============================*/
$route['admin/combo_unidad'] = 'proyecto/combo_unidad';
$route['admin/combo_fun_uni'] = 'programacion/proyecto/combo_funcionario_unidad';
$route['admin/proy/list_proy_poa'] = 'programacion/proyecto/list_proyectos_poa';  //// lista de proyectos validador POA
$route['admin/proy/list_proy_fin'] = 'programacion/proyecto/list_proyectos_financiero';  //// lista de proyectos validador POA
$route['admin/proy/mis_acciones'] = 'programacion/proyecto/mis_acciones';  //// lista de mis Acciones, donde se encuentran mis acciones
$route['admin/proy/list_proy_ok'] = 'programacion/proyecto/list_proyectos_aprobados';  //// lista de proyectos aprobados

$route['admin/proy/add_obs'] = 'programacion/proyecto/add_obs';  //// recupera datos del proyecto
$route['admin/proy/dev_poa/(:any)'] = 'proyecto/dev_validador_top/$1';  //// asignar proyectos al validador Financiero
$route['admin/proy/asig_proy'] = 'programacion/proyecto/asignar_proyecto';  //// asignar proyectos al validador POA y FINANCIERO


/*================================ MI PROGRAMACION ============================*/
$route['admin/proy/get_proy'] = 'programacion/proyecto/get_proyecto';  //// recupera datos del proyecto
$route['admin/proy/get_resp'] = 'programacion/proyecto/get_responsables';  //// recupera datos de los responsables
$route['admin/proy/get_meta'] = 'programacion/proyecto/get_meta';  //// recupera datos de la meta x
$route['admin/proy/prioridad'] = 'programacion/proyecto/prioridad_proyecto';  //// prioridad del proyecto

/*--------------------------------- TECNICO DE PLANIFICACION -------------------------------*/
$route['admin/proy/list_proy'] = 'programacion/proyecto/list_proyectos';  //// lista de proyectos
//$route['admin/proy/list_avance'] = 'programacion/proyecto/list_proy_xxx';  //// lista de avance en cargador de datos
$route['admin/proy/proyecto'] = 'programacion/proyecto/tecnico_operativo'; //// formularios de registro
$route['admin/proy/proyecto/(:any)'] = 'programacion/proyecto/tecnico_operativo_n/$1/$2'; //// formularios de registro
$route['admin/proy/add'] = 'programacion/proyecto/valida'; ////// validar datos del proyecto
$route['admin/proy/verif'] = 'programacion/proyecto/verif'; ////// verificando datos para la apertura programatica
$route['admin/proy/add_meta'] = 'programacion/metas/add_metas'; ////// validar metas 
$route['admin/proy/update_meta'] = 'programacion/metas/update_metas'; ////// validar metas 
$route['admin/proy/del_meta'] = 'programacion/metas/delete_meta'; ////// eliminar metas 

$route['admin/proy/verif_archivo'] = 'programacion/proyecto/verif_archivo'; ////// verificando extension del archivo
$route['admin/proy/add_arch'] = 'programacion/proyecto/subir_archivos'; //// subir archivos
$route['admin/proy/archivos/(:any)'] = 'programacion/proyecto/list_archivos/$1'; //// subir archivos
$route['admin/proy/get_arch/(:any)'] = 'programacion/proyecto/list_arch/$1/$2'; //// datos del archivo seleccionado
//$route['admin/proy/del_arch/(:any)'] = 'programacion/proyecto/eliminar_archivo/$1/$2'; //// eliminar archivo pdf
$route['proy/delete_arch'] = 'programacion/proyecto/delete_archivo'; //// eliminar archivo pdf

$route['admin/proy/delete'] = 'programacion/proyecto/delete_proyecto'; ////// eliminar proyectos 

/*-----------------------------  SGP - GERENCIA DE PROYECTOS -------------------------------*/
$route['admin/sgp/list_proy'] = 'programacion/gerencia/list_proyectos';  //// lista de proyectos en ejecucion SGP
$route['admin/sgp/proyecto'] = 'programacion/gerencia/tecnico_operativo'; //// formularios de registro SGP
$route['admin/sgp/proyecto/(:any)'] = 'programacion/gerencia/tecnico_operativo_n/$1/$2'; //// formularios de registro
$route['admin/sgp/add'] = 'programacion/gerencia/valida'; ////// validar datos del proyecto
$route['admin/sgp/add_arch'] = 'programacion/gerencia/subir_archivos'; //// subir archivos
$route['admin/sgp/archivos/(:any)'] = 'programacion/gerencia/list_archivos/$1'; //// subir archivos
$route['admin/sgp/get_arch/(:any)'] = 'programacion/gerencia/list_arch/$1/$2'; //// datos del archivo seleccionado SGP
$route['admin/sgp/get_arch/(:any)'] = 'programacion/gerencia/list_arch/$1/$2'; //// datos del archivo seleccionado SGP
$route['admin/sgp/del_arch/(:any)'] = 'programacion/gerencia/eliminar_archivo/$1/$2'; //// eliminar archivo pdf SGP
/*-----------------------------  FASES - GERENCIA DE PROYECTOS -------------------------------*/
$route['admin/sgp/fase_etapa/(:any)'] = 'programacion/faseetapa/list_fase_etapa_sgp/$1';  //// lista fase etapas  - id_proy
$route['admin/sgp/newfase/(:any)'] = 'programacion/faseetapa/nueva_fase_sgp/$1/$2/$3';  //// nueva fase SGP
$route['admin/sgp/add_fe'] = 'programacion/faseetapa/add_fase_sgp';  //// valida1 fase/etapa SGP
$route['admin/sgp/add_fe2'] = 'programacion/faseetapa/add_fase2_sgp';  //// valida2 fase/etapa SGP
$route['admin/sgp/update_f/(:any)'] = 'programacion/faseetapa/modificar_fase_sgp/$1/$2/$3';  //// opcion Modificar Fase SGP
$route['admin/sgp/fase_update'] = 'programacion/faseetapa/update_fase_etapa_sgp';  //// Valida  Modificar Fase SGP

/*-----------------------------  SGP - GERENCIA DE PROYECTOS -------------------------------*/
$route['admin/sgp/proy_cerrados'] = 'programacion/gerencia/list_proyectos_cerrados';  //// lista de proyectos cerrados SGP


/*-----------------------------  FASES  DEL PROYECTO -------------------------------*/
$route['admin/proy/fase_etapa/(:any)'] = 'programacion/faseetapa/list_fase_etapa/$1';  //// lista fase etapas  - id_proy
$route['admin/proy/newfase/(:any)'] = 'programacion/faseetapa/nueva_fase/$1/$2/$3';  //// nueva fase
$route['admin/proy/add_fe'] = 'programacion/faseetapa/add_fase';  //// valida1 fase/etapa
$route['admin/proy/add_fe2'] = 'programacion/faseetapa/add_fase2';  //// valida2 fase/etapa
$route['admin/proy/update_f/(:any)'] = 'programacion/faseetapa/modificar_fase/$1/$2/$3';  //// opcion Modificar Fase
$route['admin/proy/fase_update'] = 'programacion/faseetapa/update_fase_etapa';  //// Modificar Fase (controlador)
$route['admin/proy/off'] = 'programacion/faseetapa/encender_fase';  //// Encender Fase
$route['admin/proy/verif_fase'] = 'programacion/faseetapa/verif_fase'; //// Verificando las dependencia de la fase
$route['admin/proy/delete_fase'] = 'programacion/faseetapa/delete_fase'; //// Eliminando Fase Etapa
$route['admin/proy/get_fase'] = 'programacion/faseetapa/get_fase_activa';  //// Obitne datos de la fase para los indicadores de desemepenio
$route['admin/proy/add_indi'] = 'programacion/faseetapa/add_indicador';  //// Obitne datos de la fase para los indicadores de desemepenio
$route['admin/proy/asig_ptto/(:any)'] = 'programacion/faseetapa/asignar_presupuesto/$1'; ////// Asigan Presupuesto de la gestion vigente a la fase activa
$route['admin/proy/add_ptto'] = 'programacion/faseetapa/add_techo_presupuesto';  //// valida techo presupuestario
$route['admin/proy/ver_techo_ptto/(:any)'] = 'programacion/faseetapa/ver_techo_ptto/$1/$2';  //// ver techo presupuestario de la fase
$route['admin/proy/get_techo'] = 'programacion/faseetapa/get_techo_ptto';  //// recupera datos del techo presupuesto x
$route['admin/proy/add_ptto_techo'] = 'programacion/faseetapa/validar_techo_ptto';  //// Validar datos del techo presupuesto x
$route['admin/proy/update_techo'] = 'programacion/faseetapa/update_techo_ptto';  //// Update datos del techo presupuesto x
$route['admin/proy/delete_recurso/(:any)'] = 'programacion/faseetapa/delete_recurso/$1/$2/$3/$4';  //// Delete datos del techo presupuesto recurso x (Borrar)
$route['admin/proy/actualiza_techo_ptto'] = 'programacion/faseetapa/valida_techo_ptto';  //// ver techo presupuestario de la fase (Actualizando lo ultimo)
$route['admin/proy/verif_fase_existe'] = 'programacion/faseetapa/verif_existente'; ////// verifica si fase etapa ya se encuentra registrado

///
/*-----------------------------  MODIFICADO DEL PROYECTO -------------------------------*/
$route['admin/proy/edit/(:any)'] = 'programacion/proyecto/ruta_edit_proy/$1/$2'; //// ruta de formularios para el editado  id, form
$route['admin/proy/update_apertura'] = 'programacion/proyecto/actualizar_apertura'; ////// Actualizar apertura programatica
$route['admin/proy/update'] = 'programacion/proyecto/actualizar_datos'; ////// Actualizar datos proyecto
/*-----------------------------  MODIFICADO DEL PROYECTO SGP -------------------------------*/
$route['admin/sgp/edit/(:any)'] = 'programacion/gerencia/ruta_edit_proy/$1/$2'; //// ruta de formularios para el editado  id, form SGP
$route['admin/sgp/update'] = 'programacion/gerencia/actualizar_datos'; ////// Actualizar datos proyecto SGP
$route['admin/proy/history/(:any)'] = 'programacion/proyecto/historial_usuarios/$1'; /////// Historial de usuarios responsables

/*-----------------------------  PROGRAMACION DEL PROYECTO - DATOS GENERALES -------------------------------*/
$route['admin/proy/prog/(:any)'] = 'programacion/datosgenerales/dashboard_programacion/$1/$2';  //// Dashboard del componente - id_proy 
$route['admin/proy/mis_proyectos/(:any)'] = 'programacion/datosgenerales/mis_proyectos/$1';  //// lista de proyectos anual/multianual

//MODIFICADO PARA EVITAR ERRORES EN REGISTRO DE FASE ETAPA
$route['admin/proy/datos_proy/(:any)'] = 'programacion/datosgenerales/datos_generales/$1/$2/$3';  //// Datos generales
//$route['admin/proy/datos_proy/(:any)'] = 'admin/proy/edit/$1/1';  //// Datos generales

$route['admin/proy/edit_prog/(:any)'] = 'programacion/datosgenerales/ruta_edit_proy/$1/$2/$3'; //// ruta de formularios para el editado mod id, form
$route['admin/proy/update_prog'] = 'programacion/datosgenerales/actualizar_datos'; ////// Actualizar datos del proyecto 
$route['admin/proy/add_archivo'] = 'programacion/datosgenerales/subir_archivos'; //// subir archivos
$route['admin/prog/update_fase/(:any)'] = 'programacion/datosgenerales/modificar_fase/$1/$2/$3/$4';  //// opcion Modificar Fase
$route['admin/prog/fase_update'] = 'programacion/datosgenerales/update_fase_etapa';  //// Modificar Fase (controlador)

/*-----------------------------  PROGRAMACION DEL PROYECTO - PROGRAMACION FISICA  -------------------------------*/
$route['admin/prog/prog_fisica/(:any)'] = 'programacion/datosgenerales/programacion_fisica/$1/$2/$3';  //// programacion fisica del proyecto
$route['admin/prog/prog_financiera/(:any)'] = 'programacion/datosgenerales/programacion_fin/$1/$2/$3';  //// programacion financiera del proyecto

/*-----------------------------  PROGRAMACION DEL PROYECTO - COMPONENTES  -------------------------------*/
$route['admin/prog/list_comp/(:any)'] = 'programacion/componente/lista_componentes/$1/$2/$3';  //// listado componente de un proyecto
$route['admin/prog/add_comp'] = 'programacion/componente/valida_componente';  //// listado componente de un proyecto
$route['admin/prog/get_comp'] = 'programacion/componente/get_componente';  //// recupera datos del componente x
$route['admin/prog/update_comp'] = 'programacion/componente/update_componente'; ////// validar update componente 
$route['admin/prog/delete_comp/(:any)'] = 'programacion/componente/delete_componente/$1/$2/$3/$4'; //// Eliminando el componente
//$route['admin/prog/delete_comp'] = 'programacion/componente/delete_componente'; //// Eliminando el componente

/*-----------------------------  PROGRAMACION DEL PROYECTO - PRODUCTOS  -------------------------------*/
$route['admin/prog/list_prod/(:any)'] = 'programacion/producto/lista_productos/$1/$2/$3/$4';  //// lista de productos
$route['admin/prog/new_prod/(:any)'] = 'programacion/producto/new_productos/$1/$2/$3/$4';  //// formulario agregar productos
$route['admin/prog/add_prod'] = 'programacion/producto/valida_producto';  //// valida productos
$route['admin/prog/mod_prod/(:any)'] = 'programacion/producto/update/$1/$2/$3/$4/$5';  ////  formulario editado productos 
$route['admin/prog/update_prod'] = 'programacion/producto/modificar_producto';  //// valida modificar componente
$route['admin/prog/delete_prod/(:any)'] = 'programacion/producto/delete_producto/$1/$2/$3/$4/$5'; //// Eliminando el producto

/*-----------------------------  PROGRAMACION DEL PROYECTO - ACTIVIDADES  -------------------------------*/
$route['admin/prog/list_act/(:any)'] = 'programacion/actividades/lista_actividades/$1/$2/$3/$4/$5';  //// lista de actividades
//$route['admin/prog/upload_act/(:any)'] = 'programacion/actividades/archivo_actividad/$1/$2/$3/$4/$5';  //// lista de actividades con subida
$route['admin/prog/valida_upload_act'] = 'programacion/actividades/subir_actividad';  //// Subir Archivo Actividades
$route['admin/prog/plist_act/(:any)'] = 'programacion/actividades/pre_lista_actividades/$1/$2/$3/$4/$5/$6';  //// pre lista de actividades
$route['admin/prog/subir_act/(:any)'] = 'programacion/actividades/validar_pre_lista_actividades/$1/$2/$3/$4/$5/$6';  //// Validar pre lista de actividades
$route['admin/prog/delete_act_temp/(:any)'] = 'programacion/actividades/borrar_actividad_temporal/$1/$2/$3/$4/$5/$6';  //// Eliminar pre lista de actividades

$route['admin/prog/new_act/(:any)'] = 'programacion/actividades/new_actividad/$1/$2/$3/$4/$5';  ////  formulario de actividades
$route['admin/prog/add_act'] = 'programacion/actividades/valida_actividad';  //// valida actividades
$route['admin/prog/mod_act/(:any)'] = 'programacion/actividades/update/$1/$2/$3/$4/$5';  ////  formulario editado actividades 
$route['admin/prog/update_act'] = 'programacion/actividades/modificar_actividad';  //// modificar actividad
$route['admin/prog/delete_act/(:any)'] = 'programacion/actividades/delete_actividades/$1/$2/$3/$4/$5/$6'; //// Eliminando la actividad

/*-----------------------------  PROGRAMACION DEL PROYECTO - EJECUCION  -------------------------------*/
$route['admin/prog/efisica/(:any)'] = 'programacion/producto/lista_prod_efisica/$1/$2/$3';  //// lista de Productos ejecucion fisica
$route['admin/prog/ejec_prod/(:any)'] = 'programacion/producto/ejecucion_producto/$1/$2/$3';  //// ejecucion producto
$route['admin/prog/valida_ejec_prod'] = 'programacion/producto/valida_ejecucion_producto';  //// ejecucion producto
$route['admin/prog/efisica_a/(:any)'] = 'programacion/actividades/lista_act_efisica/$1/$2/$3/$4/$5';  //// lista de actividades ejecusion fisica
$route['admin/prog/ejec_act/(:any)'] = 'programacion/actividades/ejecucion_actividad/$1/$2/$3/$4/$5/$6';  //// ejecucion Actividad
$route['admin/prog/valida_ejec_act'] = 'programacion/actividades/valida_ejecucion_actividad';  //// ejecucion actividad

/*-----------------------------  PROGRAMACION DEL PROYECTO - EJECUCION FINANCIERA -------------------------------*/
$route['admin/prog/efinanciero/(:any)'] = 'programacion/datosgenerales/lista_partidas/$1/$2/$3';  //// Ejecucion Financiera segun tipo de ejecucion
$route['admin/prog/ejec_partida/(:any)'] = 'programacion/datosgenerales/ejecutar_partida/$1/$2/$3/$4';  //// Ejecutar Partida
$route['admin/prog/ejec_partida_m/(:any)'] = 'programacion/datosgenerales/ejecutar_partida_m/$1/$2/$3/$4/$5';  //// Ejecutar Partida
$route['admin/prog/valida_ejecucion'] = 'programacion/datosgenerales/add_ejecucion';  //// Valida Ejecucion



/*-----------------------------  PROGRAMACION DEL PROYECTO - REPORTES  -------------------------------*/
$route['admin/prog/reporte_proy/(:any)'] = 'programacion/reportes/reporte_proyecto/$1/$2/$3';  //// Iframe Identificacion del proyecto
$route['admin/prog/identificacion_proy/(:any)'] = 'programacion/reportes/identificacion_proyecto/$1/$2/$3';  //// Reporte Identificacion del proyecto

$route['admin/prog/reporte_fis/(:any)'] = 'programacion/reportes/reporte_fisico/$1/$2/$3';  //// Iframe Programacion Fisica
$route['admin/prog/ejecucion_pfisico/(:any)'] = 'programacion/reportes/reporte_programacion_fisica/$1/$2/$3';  //// Reporte Programacion ejecucion programacion fisica anual
$route['admin/prog/ejecucion_pfisico_m/(:any)'] = 'programacion/reportes/reporte_programacion_fisica_m/$1/$2/$3';  //// Reporte Programacion ejecucion programacion fisica multi anual

$route['admin/prog/reporte_fin/(:any)'] = 'programacion/reportes/reporte_financiero/$1/$2/$3';  //// iframe reporte Presupuestario
$route['admin/prog/rep_financiero/(:any)'] = 'programacion/reportes/reporte_programacion_financiera/$1/$2/$3';  //// reporte Presupuestario

$route['admin/prog/contrato/(:any)'] = 'programacion/reportes/reporte_contrato/$1/$2/$3';  //// Iframe Seguimiento del contrato
$route['admin/prog/reporte_contrato/(:any)'] = 'programacion/reportes/contrato_proyecto/$1/$2';  ////Reporte Seguimiento del contrato

$route['admin/prog/supervision/(:any)'] = 'programacion/reportes/reporte_supervision/$1/$2/$3';  //// Iframe Supervision y evaluacion
$route['admin/prog/reporte_supervision/(:any)'] = 'programacion/reportes/supervision_evaluacion/$1/$2';  //// Reporte Supervision y evaluacion

$route['admin/prog/curva_s/(:any)'] = 'programacion/reportes/grafico_fisico/$1/$2/$3';  //// Iframe Grafico Curva S
$route['admin/rep_curva_fis/(:any)']='programacion/reportes/grafico_programacion_fisica/$1/$2/$3';// Reporte S Programado Fisico
$route['admin/imprimir_curva_fis/(:any)']='programacion/reportes/imprimir_programacion_fisica/$1/$2/$3';// IMPRIMIR Curva S Programado Fisico

$route['prog/curva_sfin/(:any)'] = 'programacion/reportes/grafico_financiero/$1/$2/$3';  ////Grafico Curva S Financiero
$route['prog/rep_curva_fin/(:any)'] = 'programacion/reportes/grafico_programacion_financiero/$1/$2/$3'; /// Curva S Programado Financiero
$route['prog/imprimir_curva_fin/(:any)'] = 'programacion/reportes/imprimir_programacion_financiero/$1/$2/$3'; /// Curva S Programado Financiero

$route['prog/cierre_proyecto/(:any)'] = 'programacion/reportes/cierre_proyecto/$1/$2/$3';  //// Cierre del Proyecto

/*================================ REGISTRO DE EJECUCION ============================*/
/*----------------------------- EJECUCION SIGEP POR APRETURAS PROGRAMATICAS --------------------------------------*/
$route['admin/ejec/list_aper'] = 'ejecucion/ejecucion/list_aperturas'; //// Lista de Aperturas
$route['admin/ejec/l_sigep'] = 'ejecucion/ejecucion/cargar_archivo_sigep'; //// Cargar Archivo Sigep
$route['admin/ejec/ejec_sigep/(:any)'] = 'ejecucion/ejecucion/ejecucion_presupuesto/$1'; //// Form Ejecucion Presupuestaria


$route['admin/combo_estado'] = 'ejecucion/ejecucion/combo_estados_proy';

$route['admin/ejec/mis_operaciones'] = 'ejecucion/ejecucion/mis_operaciones';  //// Mis Operaciones
$route['admin/ejec/meses_operacion/(:any)'] = 'ejecucion/ejecucion/list_meses/$1/$2/$3/$4';  //// lista de meses por componentes para la ejecucion
$route['admin/ejec/proy/(:any)'] = 'ejecucion/ejecucion/proyecto_n/$1/$2/$3'; //// Formulario de Registro - Ejecucion
$route['admin/ejec/valida'] = 'ejecucion/ejecucion/valida'; //// valida datos del proyecto en su ejecucion
$route['admin/ejec/archivos/(:any)'] = 'ejecucion/ejecucion/list_archivos/$1/$2/$3';  //// Anexos de la Ejecucion
$route['admin/ejec/valida_arch'] = 'ejecucion/ejecucion/valida_archivo'; //// valida archivo
$route['admin/ejec/delete_anexo_ejec/(:any)'] = 'ejecucion/ejecucion/borrar_archivo_anexo_ejecucion/$1/$2/$3/$4/$5/$6'; //// Eliminar archivo
$route['admin/ejec/reporte_ejecucion/(:any)'] = 'ejecucion/ejecucion/reporte_ejecucion_mensual/$1/$2/$3/$4/$5/$6'; //// Reporte Mensual
$route['admin/cargar_partida_proy'] = 'ejecucion/ejecucion/fn_genera_prog_partidas';
$route['admin/ejec/verifi'] = 'ejecucion/ejecucion/verifica_registro';  //// Verifica si ya existe el regsitro para ese mes

//NUEVO ADICIONADO POR COCAE SIN NOTIFICAR
//INI
$route['ejec/partidas/(:any)'] = 'ejecucion/ejecucion/list_partidas/$1';  //// lista de partidas 
$route['ejec/ejec_fin/(:any)'] = 'ejecucion/ejecucion/ejec_presupuesto/$1/$2';  //// Ejecucion Presupuestaria
$route['ejec/valida_ejecucion'] = 'ejecucion/ejecucion/add_ejecucion';  //// Valida Ejecucion
//FIN


//$route['admin/ejec/prog'] = 'ejecucion/ejecucion/red_programas';  //// lista de programas
$route['admin/ejec/list_proy/(:any)'] = 'ejecucion/ejecucion/list_proyectos/$1';  //// lista de proyectos para el registro de la ejecucion
$route['admin/ejec/proy_comp/(:any)'] = 'ejecucion/ejecucion/proyecto_componente/$1/$2'; //// redireccionando a sus componentes
//$route['admin/ejec/proy/(:any)'] = 'ejecucion/ejecucion/proyecto_n/$1/$2/$3'; //// ejecucion del programa por el mes actual
//$route['admin/ejec/valida'] = 'ejecucion/ejecucion/valida'; //// valida datos del proyecto en su ejecucion
$route['admin/ejec/update_proy'] = 'ejecucion/ejecucion/update_proyecto'; //// update cerrar proyecto


$route['admin/ejec/ver/(:any)'] = 'ejecucion/ejecucion/verificando_proy/$1/$2'; //// verificando los meses del proyecto
$route['admin/ejec/ejecucion_mes/(:any)'] = 'ejecucion/ejecucion/ejecucion_mes/$1/$2/$3/$4'; //// mostrar mes de ejecucion
$route['admin/ejec/valida_obs'] = 'ejecucion/ejecucion/valida_observacion'; //// valida obervacion
$route['admin/ejec/get_obs'] = 'ejecucion/ejecucion/get_observacion'; //// valida obervacion
$route['admin/ejec/update_obs'] = 'ejecucion/ejecucion/update_observacion'; //// valida obervacion
$route['admin/ejec/delete_obs'] = 'ejecucion/ejecucion/delete_observacion'; //// delete contrato
$route['admin/ejec/proy_comp_meses/(:any)'] = 'ejecucion/ejecucion/proyecto_componente_meses/$1/$2';  //// componentes por meses
//$route['admin/ejec/meses/(:any)'] = 'ejecucion/ejecucion/list_meses/$1/$2/$3/$4';  //// lista de meses por meses
$route['admin/ejec/revertir'] = 'ejecucion/ejecucion/revertir_mes';  //// revertir ejecucion
$route['admin/ejec/verm/(:any)'] = 'ejecucion/ejecucion/verificando_proy_mes/$1/$2/$3'; //// verificando el mes x del proyecto
$route['admin/ejec/documentos/(:any)'] = 'ejecucion/ejecucion/archivos_mes/$1/$2/$3'; //// lista de archivos del mes x
$route['admin/ejec/get_archivo/(:any)'] = 'ejecucion/ejecucion/get_archivo/$1/$2/$3/$4'; //// datos del archivo seleccionado

/*----------------------------- EJECUCION - CONTRATOS -------------------------------*/
$route['admin/ejec/list_contratos/(:any)'] = 'ejecucion/ejecucion/lista_contratos/$1/$2/$3/$4/$5/$6'; //// lista de contrato
$route['admin/ejec/new_contrato/(:any)'] = 'ejecucion/ejecucion/contrato/$1/$2/$3/$4'; //// nuevocontrato
$route['admin/ejec/add_contrato'] = 'ejecucion/ejecucion/valida_contrato'; //// valida contrato
$route['admin/ejec/add_ctta'] = 'ejecucion/ejecucion/valida_contratista'; //// valida contrato
$route['admin/ejec/edit_contrato/(:any)'] = 'ejecucion/ejecucion/update_contrato/$1/$2/$3/$4'; //// update contrato
$route['admin/ejec/update_contrato'] = 'ejecucion/ejecucion/valida_update_contrato'; //// valida update contrato
$route['admin/ejec/delete_contrato'] = 'ejecucion/ejecucion/delete_contrato'; //// delete contrato

/*----------------------------- EJECUCION - GARANTIAS -------------------------------*/
$route['admin/ejec/garantias/(:any)'] = 'ejecucion/ejecucion/lista_garantias/$1/$2/$3/$4/$5'; //// lista de contrato
$route['admin/ejec/new_garantia/(:any)'] = 'ejecucion/ejecucion/garantia/$1/$2/$3/$4/$5'; //// nueva garantia
$route['admin/ejec/add_garantia'] = 'ejecucion/ejecucion/valida_garantia/'; //// valida garantia
$route['admin/ejec/edit_garantia/(:any)'] = 'ejecucion/ejecucion/update_garantia/$1/$2/$3/$4/$5/$6'; //// update garantia
$route['admin/ejec/update_garantia'] = 'ejecucion/ejecucion/valida_update_garantia'; //// valida update garantia
$route['admin/ejec/delete_garantia'] = 'ejecucion/ejecucion/delete_garantia'; //// delete garantia
/*----------------------------- EJECUCION - PEI -------------------------------*/
$route['admin/ejec/redobj'] = 'ejecucion/ejecucion/cprog_red_objetivos'; //// lista de red de objetivos
$route['admin/ejec/obj_est/(:any)'] = 'ejecucion/ejecucion/objetivo_estrategico/$1'; //// Objetivo a Ejecutar
$route['admin/ejec/valida_oe'] = 'ejecucion/ejecucion/valida_oe'; //// Valida Objetivo estrategico
$route['admin/ejec/obj_update/(:any)'] = 'ejecucion/ejecucion/objetivo_estrategico_update/$1'; //// Objetivo a Ejecutar
$route['admin/ejec/obj_cerrado/(:any)'] = 'ejecucion/ejecucion/objetivo_estrategico_cerrado/$1'; //// Objetivo a Ejecutar cerrado
$route['admin/ejec/valida_obs_oe'] = 'ejecucion/ejecucion/valida_observacion_oe'; //// valida obervacion objetivos estrategicos
$route['admin/ejec/get_obs_oe'] = 'ejecucion/ejecucion/get_observacioarchivos_oen_oe'; //// get observacion obj. est
$route['admin/ejec/update_obs_oe'] = 'ejecucion/ejecucion/update_observacion_oe'; //// valida obervacion oe
$route['admin/ejec/delete_obs_oe'] = 'ejecucion/ejecucion/delete_observacion_oe'; //// delete observacion oe
$route['admin/ejec/archivos_oe/(:any)'] = 'ejecucion/ejecucion/archivos_adjuntos_oe/$1'; //// archivos adjuntos
$route['admin/ejec/valida_arch_oe'] = 'ejecucion/ejecucion/valida_archivo_oe'; //// valida archivo oe
$route['admin/ejec/delete'] = 'ejecucion/ejecucion/delete_archivo'; ////// eliminar metas 


/*============================================= MODIFICACIONES =====================================================*/
$route['admin/mod/mis_acciones'] = 'modificaciones/modificaciones/acciones_aprobadas/'; //// Acciones aprobadas
$route['admin/mod/proyecto/(:any)'] = 'modificaciones/modificaciones/proyecto/$1'; //// proyecto
$route['admin/mod/modificar'] = 'modificaciones/modificaciones/modificar'; //// modificar accion 
$route['admin/mod/proyecto_mod/(:any)'] = 'modificaciones/modificaciones/redireccionar_modicacion/$1/$2/$3/$4'; //// redireccionar al tipo de formulario 
$route['admin/mod/producto/(:any)'] = 'modificaciones/modificaciones/modificar_producto/$1/$2/$3/$4'; //// modificar producto 
$route['admin/mod/valida_mp'] = 'modificaciones/modificaciones/valida_producto'; //// valida modificacion producto
$route['admin/mod/actividad/(:any)'] = 'modificaciones/modificaciones/modificar_actividad/$1/$2/$3/$4'; //// modificar producto 
$route['admin/mod/valida_ma'] = 'modificaciones/modificaciones/valida_actividad'; //// valida modificacion actividad
$route['admin/mod/valida_plazo'] = 'modificaciones/modificaciones/valida_plazo'; //// valida modificacion plazo de ejecucion
$route['admin/mod/presupuesto/(:any)'] = 'modificaciones/modificaciones/modificar_presupuesto/$1/$2/$3/$4'; //// modificar presupuesto 
$route['admin/mod/valida_pr'] = 'modificaciones/modificaciones/valida_presupuesto'; //// valida modificacion presupuesto
$route['admin/mod/techo_p/(:any)'] = 'modificaciones/modificaciones/modificar_techo_presupuesto/$1/$2/$3/$4'; //// modificar presupuesto 
$route['admin/mod/add_ptto_techo'] = 'modificaciones/modificaciones/validar_techo_ptto'; //// valida  presupuesto
$route['admin/mod/update_techo'] = 'modificaciones/modificaciones/update_techo_ptto'; //// valida modificacion presupuesto
$route['admin/lmes/(:any)'] = 'modificaciones/modificaciones/lista_mes_ejec/$1';


/*======================================= MODIFICACIONES ==========================================================*/
$route['admin/mod/redobj'] = 'modificaciones/modificaciones/cprog_red_objetivos'; /// Lista de red de objetivos por gestion
$route['admin/mod/objetivo/(:any)'] = 'modificaciones/modificaciones/objetivos_estrategicos/$1'; //// objetivos estrategicos
$route['admin/mod/obj_gest/(:any)'] = 'modificaciones/modificaciones/objetivo_gestion/$1/$2'; //// objetivos de gestion y productos terminales
$route['admin/mod/mod_obj_gest/(:any)'] = 'modificaciones/modificaciones/modificar_objetivo_gestion/$1/$2/$3'; //// Modificar Objetivo de Gestion
$route['admin/mod/valida_objetivo'] = 'modificaciones/modificaciones/valida_obj_gestion'; /// valida objetivo de gestion modificado
$route['admin/mod/mod_pt/(:any)'] = 'modificaciones/modificaciones/modificar_producto_terminal/$1/$2/$3'; //// Modificar producto terminal de Gestion
$route['admin/mod/valida_pt'] = 'modificaciones/modificaciones/valida_pt'; /// valida producto terminal modificado
$route['admin/mod/ver_mod/(:any)'] = 'modificaciones/modificaciones/ver_modificar_objetivo/$1/$2/$3'; //// Modificar Objetivo de Gestion
$route['admin/mod/rep_objetivo/(:any)'] = 'modificaciones/modificaciones/reporte_objetivos_estrategicos/$1'; //// objetivos estrategicos
















//========================    REPORTES    ==================================
//============================  SEGUIMIENTO OBJETIVO DE GESTION
$route['admin/seg/mo'] = 'reportes/seguimiento/seg_menu_ogestion';
$route['admin/seg/og/(:any)'] = 'reportes/seguimiento/seg_por_prog/$1';//seguimiento por programas
$route['admin/seg/obje_gestion/(:any)'] = 'reportes/seguimiento/seg_por_ogestion/$1'; //seguimiento por objetivo de gestion
$route['admin/seg/inst'] = 'reportes/seguimiento/seg_pe_institucion'; //seguimiento por institucion
//============================  SEGUIMIENTO PRODUCTO TERMINAL
$route['admin/seg/mpt'] = 'reportes/seguimiento/seg_menu_pt';
$route['admin/seg/pe_pt'] = 'reportes/seguimiento/seg_pe_pt';
$route['admin/seg/o_pt/(:any)'] = 'reportes/seguimiento/seg_ogestion_productot/$1';
$route['admin/seg/pt/(:any)'] = 'reportes/seguimiento/seg_por_pt/$1/$2'; //seguimiento por producto terminal
$route['admin/seg/n_o/(:any)'] = 'reportes/seguimiento/nivel_ogestion/$1';//seguimiento a nivel objetivo de gestion
$route['admin/seg/gopt/(:any)'] = 'reportes/seguimiento/grafico_por_gestionpt/$1/$2';//grafico nivel de objetivo de gestion producto terminal
$route['admin/seg/prog_pt/(:any)'] = 'reportes/seguimiento/nivel_programa_pt/$1';//seguimiento a nivel programas
$route['admin/seg/inst_pt'] = 'reportes/seguimiento/nivel_institucion_pt';//seguimiento a nivel institucion
//=========================== SEGUIMIENTO PRODUCTO DE LA OPERACION
$route['admin/seg/mop'] = 'reportes/seguimiento/menu_producto_operacion';//MENU de seguimiento producto de la opracion
$route['admin/seg/mop1'] = 'reportes/seguimiento/seg_menu_operacion';//seguimiento producto de la opracion por prog y ejec
$route['admin/seg/mop2'] = 'reportes/seguimiento/seg_menu_op_fisico';//seguimiento producto de la opracion
//$route['admin/seg/o_op/(:any)'] = 'reportes/seguimiento/lista_ogestion_ope/$1';//lista de objetivo de gestion para los productos de la opracion
$route['admin/seg/o_op/(:any)'] = 'reportes/seguimiento/lista_proyectos/$1';//lista de proyectos para los productos de la opracion
$route['admin/seg/pt_ope/(:any)'] = 'reportes/seguimiento/lista_pt_ope/$1';//lista de productos terminales para los productos de la opracion
$route['admin/seg/pe_po/(:any)'] = 'reportes/seguimiento/prog_ejec_ope/$1/$2/$3';//programacion y ejecucion de las operaciones
$route['admin/seg/graf_prod'] = 'reportes/seguimiento/grafico_prod_mes';//grafico de producto de la operacion por mes
$route['admin/seg/proy_po/(:any)'] = 'reportes/seguimiento/nivel_proyecto_po/$1';//seguimiento a nivel proyecto
$route['admin/seg/gproy/(:any)'] = 'reportes/seguimiento/grafico_por_proyecto_op/$1/$2/$3';//grafico nivel de proyecto de producto de la operacion
$route['admin/seg/prog_po/(:any)'] = 'reportes/seguimiento/nivel_programa_po/$1';//seguimiento a nivel programas
$route['admin/seg/inst_po'] = 'reportes/seguimiento/nivel_institucion_po';//seguimiento a nivel institucion producto de la operacion
//producto de la operacion fisico
$route['admin/seg/proy_pof/(:any)'] = 'reportes/seguimiento/nivel_proyecto_pof/$1';//seguimiento a nivel de proyecto
$route['admin/seg/graf_proyf'] = 'reportes/seguimiento/grafico_proy_fisico';//seguimiento a nivel de proyecto
$route['admin/seg/prog_pof/(:any)'] = 'reportes/seguimiento/nivel_programa_pof/$1';//seguimiento a nivel programas FISICO
$route['admin/seg/inst_pof'] = 'reportes/seguimiento/nivel_institucion_pof';//seguimiento a nivel institucion producto de la operacion FISICO
//REPORTES GERENCIALES
$route['admin/rg/seg'] = 'reportes/reportes_gerenciales/menu_reporte_gerencial';//reportes gerenciales



/*========================================= CONTROL SOCIAL ======================================*/
$route['admin/validate_invitado'] = 'user/validate_invitado';// validar al control social
$route['admin/control_social'] = 'control_social/c_social/menu';// menu control social

$route['admin/mis_operaciones'] = 'control_social/c_social/mis_operaciones';// Mis Operaciones Institucionales
$route['admin/imprimir_operaciones/(:any)'] = 'control_social/c_social/imprimir_mis_operaciones/$1';// Imprimir Operaciones Institucionales
$route['admin/exportar_operaciones/(:any)'] = 'control_social/c_social/exportar_mis_operaciones/$1';// Exportar Excel Operaciones Institucionales

$route['admin/select_ejecfis'] = 'control_social/c_social/ejecucion_fisica';// Resumen de Ejecucion Fisica
$route['admin/select_ejecfin'] = 'control_social/c_social/ejecucion_financiera';// Resumen de Ejecucion Financiera
$route['rep/valida_resfisfin'] = 'reportes/reporte_w/validar_seleccion_resumen_fis_fin_control'; //// valida seleccion
$route['rep/gerencial_proyectos/(:any)'] = 'reportes/reporte_w/reporte_gerencial_proyectos_control/$1/$2/$3/$4/$5'; //// a nivel institucional EJECUCION FISICA Y FINANCIERA
$route['rep/ver_proyectos_fis/(:any)'] = 'reportes/reporte_w/ver_reporte_gerencial_fis_control/$1/$2/$3/$4/$5/$6/$7'; ///// ver los proyectos gerenciales segun su tipo-	EJECUCION FISICA
$route['rep/ver_proyectos_fin/(:any)'] = 'reportes/reporte_w/ver_reporte_gerencial_fin_control/$1/$2/$3/$4/$5'; ///// ver los proyectos gerenciales segun su tipo-	EJECUCION FINANCIERA

$route['admin/comp_est'] = 'reportes_cptdi/ccomp_ptdi/ptdi_control'; //// Ptdi Control Social
$route['rep/comp_est/detalle_comp_ptdi/(:any)'] = 'reportes_cptdi/ccomp_ptdi/detalle_ptdi_control/$1'; //// Detalle Ptdi Control Social



//=========================  PRESUPUESTO ===============================
$route['admin/fp/pg'] = 'reportes/presupuesto/presupuesto_gasto';// presupuesto de gasto
$route['admin/fp/lp/(:any)'] = 'reportes/presupuesto/lista_proyectos/$1';// lista de proyectos
$route['admin/fp/lpar/(:any)'] = 'reportes/presupuesto/lista_partidas/$1/$2/$3';// lista de proyectos
$route['admin/pr/lproy/(:any)'] = 'reportes/presupuesto/lista_proy_por_mes/$1/';// lista de presupuesto de proyectos por mes
$route['admin/pr/lpar_mes/(:any)'] = 'reportes/presupuesto/lista_par_por_mes/$1/$2/$3';// lista de presupuesto de proyectos por mes
$route['admin/pr/inst'] = 'reportes/presupuesto/pres_por_institucion';//PRESUPUESTO POR INSTITUCION
$route['admin/pr/graf_proy'] = 'reportes/presupuesto/grafico_proyecto_mes';//GRAFICOS PROYECTO
$route['admin/pr/graf_prog'] = 'reportes/presupuesto/grafico_programa_mes';//GRAFICOS PROGRAMA



/////////////////////////////////////pdf//////////////////////////////////////////////
$route['admin/mantenimiento/hardy'] = 'a_pdf/b_pdf';
//////////////////////////////////////////////////////
$route['admin/pdf/obj_pdf/(:any)'] = 'a_pdf/obj_pdf/$1/$2';
$route['admin/pdf/objs_pdf/(:any)'] = 'a_pdf/obj_dompdf/$1/$2';
///////////////////////////////////////REPORTES////////////////////////////////
$route['admin/reportes/objes'] = 'reportes/reporte/ficha_tecnica';
$route['admin/reportes/objgest'] = 'reportes/reporte/ficha_obge';
$route['admin/reportes/prter'] = 'reportes/reporte/ficha_proter';
$route['admin/reportes/productot'] = 'reportes/reporte/ficha_productot';
$route['admin/reportes/productots'] = 'reportes/reporte/repor_protot';
$route['admin/reportes/report_ges/(:any)'] = 'reportes/reporte/reporte_gestion/$1/$2';
//////////////////////////////////////////////////////////////////////////////
$route['admin/reportes/objgest_f'] = 'reportes/reporte/rep_estra';
$route['admin/reportes/lista_g'] = 'reportes/reporte/rep_gest';
$route['admin/reportes/lista_t'] = 'reportes/reporte/rep_productot';
$route['admin/reportes/lista_pordtot/(:any)'] = 'reportes/reporte/ficha_pdfpt/$1/$2/$3';
////titulos///
$route['admin/reportes/objgest_titulo'] = 'reportes/reporte/titulo_estra';
$route['admin/reportes/gest_titulo'] = 'reportes/reporte/titulo_gestion';
$route['admin/reportes/term_titulo'] = 'reportes/reporte/titulo_terminal';
//////////reporte gestion
$route['admin/reportes/reptn_gestionn/(:any)'] = 'reportes/reporte/report_gestion/$1/$2';
$route['admin/reportes/reptn_terminal/(:any)'] = 'reportes/reporte/report_pord_terminal/$1';
$route['admin/reportes/rep_terminal/(:any)'] = 'reportes/reporte/ficha_productoterminal/$1';
//////////////////////////////conotrol social/////////////////////////////////
$route['admin/controls'] = 'controlsocial/vista';
////////////////////////////login session/////////////////////////////////////
$route['admin/logins'] = 'user/login_exit';



//////////////////////////////////////////funcionario/////////////////////////////////////
/*$route['admin/mnt/list_usu'] = 'mantenimiento/funcionario/list_usuarios';  //// lista de usuarios
$route['admin/funcionario/add_fun'] = 'mantenimiento/funcionario/add_funcionario'; // add funcionario
$route['admin/mantenimiento/del_fun'] = 'mantenimiento/funcionario/del_fun';//aliminar funcionario
$route['admin/mantenimiento/get_fun'] = 'mantenimiento/funcionario/mod_funs';//modificar funcionario


$route['admin/mnt/modificar_fun'] = 'mantenimiento/funcionario/mod_funcionario';//modificar funcionario segundo*/

/*------------------------------ FUNCIONARIO (NUEVO)--------------------------------*/
$route['admin/mnt/list_usu'] = 'mantenimiento/cfuncionario/list_usuarios';  //// lista de usuarios
$route['admin/funcionario/new_fun'] = 'mantenimiento/cfuncionario/new_funcionario'; // new funcionario
$route['funcionario/verif_usuario'] = 'mantenimiento/cfuncionario/verif_usuario'; // verifica usuario
$route['admin/funcionario/add_fun'] = 'mantenimiento/cfuncionario/add_funcionario'; // valida funcionario
$route['admin/funcionario/update_fun/(:any)'] = 'mantenimiento/cfuncionario/update_funcionario/$1'; // Update Funcionario
$route['admin/funcionario/add_update_fun'] = 'mantenimiento/cfuncionario/add_update_funcionario'; // valida Update funcionario
$route['admin/funcionario/delete_fun/(:any)'] = 'mantenimiento/cfuncionario/del_fun/$1';//eliminar funcionario
$route['admin/mantenimiento/get_fun'] = 'mantenimiento/cfuncionario/mod_funs';//modificar funcionario

$route['admin/funcionario/verif_ci'] = 'mantenimiento/cfuncionario/verif_ci';//verif ci
$route['admin/funcionario/verif_usuario'] = 'mantenimiento/cfuncionario/verif_user';//verif user

//$route['admin/funcionario/activar'] = 'mantenimiento/cfuncionario/activar_estado';// activar
$route['funcionario/activar'] = 'mantenimiento/cfuncionario/activar';//activar


$route['admin/mod_contra'] = 'mantenimiento/funcionario/nueva_contra';//cambiar sontrase�a funcionario
$route['admin/mods_contras'] = 'mantenimiento/funcionario/mod_cont';//cambiar contrase�a funcionario
//====================roles==================//



$route['rol'] = 'mantenimiento/roles/roles_menu';//menu roloes
$route['rol_op'] = 'mantenimiento/roles/opciones';//menu roles
$route['mod_opc']='mantenimiento/roles/mod_rol';//modificaciones y adiciones eliminar roles
//================escala salarial===============//
$route['escala_salarial']='mantenimiento/cescala_salarial/lista_escala';//escala salarial
$route['admin/escala_salarial_ver']='mantenimiento/cescala_salarial/verif_codigo_escala';//escala salarial verificar
$route['admin/escala_salarial_add']='mantenimiento/cescala_salarial/valida_cargo';//escala salarial add
$route['admin/escala_salarial_update']='mantenimiento/cescala_salarial/update_cargo';//escala salarial update
$route['admin/escala_salarial_mod']='mantenimiento/escala_salarial/get_car';//escala salarial mod
$route['admin/escala_salarial_del']='mantenimiento/cescala_salarial/del_car';//escala salarial del
$route['admin/rep_escala_salarial']='mantenimiento/cescala_salarial/rep_escala_salarial';// Reporte Escala Salarial

//==================cofiguracion===============//prog/ins_part
$route['Configuracion']='mantenimiento/cconfiguracion/main_configuracion';// main configuracion
$route['Configuracion_mod']='mantenimiento/configuracion/mod_conf';// configuracion modificar a�o
$route['Configuracion_mod_mes']='mantenimiento/configuracion/mod_conf_mes';// configuracion modificar mes


//=================== Estructura organizacional (Nuevo) ======================//
$route['estructura_org']='mantenimiento/cestructura_organizacional/list_unidad_organizacional';// lista de unidades Responsables,Ejecutoras,Direcciones Administrativas
$route['admin/add_unidad']='mantenimiento/cestructura_organizacional/form_unidad_organizacional';// vista estructura organizacional add
$route['admin/valida_unidad']='mantenimiento/cestructura_organizacional/valida_unidad_organizacional';// valida unidad organizacional
$route['admin/estructura_org_verificar']='mantenimiento/cestructura_organizacional/verificar_cod_uni';// vista estructura organizacional verificar codigo
$route['admin/mod_unidad/(:any)'] = 'mantenimiento/cestructura_organizacional/update_unidad_organizacional/$1'; /// Vista Modificar
$route['admin/update_unidad']='mantenimiento/cestructura_organizacional/valida_update_unidad_organizacional';// valida update unidad organizacional

$route['admin/del_unidad'] = 'mantenimiento/cestructura_organizacional/del_unidad'; // Eliminar Unidad

$route['rep_estructura_org']='mantenimiento/cestructura_organizacional/rep_list_unidad_organizacional';// Reporte lista de unidades Responsables,Ejecutoras,Direcciones Administrativas


//===================partidas==========================//
$route['partidas']='mantenimiento/partidas/lista_partidas';// vista partidas
$route['admin/verificar_par']='mantenimiento/partidas/verificar_cod_par';// vista partidas verificar codigo partida
$route['admin/partidas_add']='mantenimiento/partidas/add_par';// vista partidas adicionar partidas
$route['admin/partidas_mod']='mantenimiento/partidas/get_par';// vista partidas modificar partidas
$route['admin/partidas_del']='mantenimiento/partidas/del_par';// vista partidas eliminar partidas
//===================organismo financiador============//
$route['organismo_financiador']='mantenimiento/organismo_fin/lista_organismo_fin';// vista organismo  financiador
$route['admin/organismo_fin_verif']='mantenimiento/organismo_fin/verificar_cod_of';// vista organismo  financiador verificar codigo
$route['admin/organismo_fin_add']='mantenimiento/organismo_fin/add_organismofinanciador';// vista organismo  financiador add
$route['admin/organismo_fin_mod']='mantenimiento/organismo_fin/get_of';// vista organismo  financiador mod
$route['admin/organismo_fin_del']='mantenimiento/organismo_fin/del_organismofinanciador';// vista organismo  financiador mod
//============================fuente financiamiento=============================//
$route['fuente_financiamiento']='mantenimiento/fuente_financiamiento/lista_fuente_fin';// vista fuente financiamiento
$route['admin/fuente_financiamiento_verif']='mantenimiento/fuente_financiamiento/verificar_cod_ff';// vista fuente financiamiento verificar
$route['fuente_financiamiento_add']='mantenimiento/fuente_financiamiento/add_ff';// vista fuente financiamiento add
$route['fuente_financiamiento_mod']='mantenimiento/fuente_financiamiento/get_ff';// vista fuente financiamiento mod
$route['fuente_financiamiento_del']='mantenimiento/fuente_financiamiento/del_ff';// vista fuente financiamiento mod
//================================entidad de transferencia=====================//
$route['entidad_transferencia']='mantenimiento/entidad_transferencia/lista_enditada_transferencia';// vista entidad trasferencia
$route['admin/entidad_transferencia_ver']='mantenimiento/entidad_transferencia/verificar_cod_et';// vista entidad trasferencia verificar
$route['admin/entidad_transferencia_add']='mantenimiento/entidad_transferencia/add_et';// vista entidad trasferencia add
$route['admin/entidad_transferencia_mod']='mantenimiento/entidad_transferencia/get_et';// vista entidad trasferencia mod
$route['admin/entidad_transferencia_del']='mantenimiento/entidad_transferencia/del_et';// vista entidad trasferencia del
//==================================pdes====================================//
$route['pdes']='mantenimiento/pdes/lista_pdes';// vista pdes
$route['pdes_verificar']='mantenimiento/pdes/verificar_pdes';// verificar pdes
$route['pdes_add']='mantenimiento/pdes/add_pdes_pilar';// add pdes 
$route['pdes_mostrar']='mantenimiento/pdes/mostrar_pilar';// mostrar pdes pilar
$route['pdes_mod_pilar']='mantenimiento/pdes/mod_pilar_pdes';// mostrar pdes pilar
//=================================ptdi=====================================//
$route['ptdi']='mantenimiento/ptdi/lista_ptdi';// vista ptdi


//============================dictaamen=========================//
$route['admin/dictamen_proyecto/(:any)']='reportes/dictamen/pdf_dictamen_proyecto/$1/$2';// dictamen
//==============================correo===============================//
$route['admin/correo']='mantenimiento/entidad_transferencia/enviar';// correo
//pruebas
$route['admin/reportes/objess'] = 'reportes/reporte/prueba';
///////////////////////////programacion//////////////////////
//========================mision y vision=========================//
$route['mision'] = 'programacion/mision/vista_mision';
$route['vision'] = 'programacion/vision/vista_vision';
//======================cambiar gestion=================//
$route['cambiar_gestion'] = 'mantenimiento/cambiar_gestion/listar_c_gestion';//vista de cambiar gestion
$route['cambiar'] = 'nueva_session/cambiar_gestion';//cambiar contralador


//==============Reportes Para Red De Objetivos===========================//
$route['admin/objetivos_estrategicos/(:any)'] = 'reportes/red_objetivos/objetivo_estrategico/$1';
$route['admin/objetivos_gestion/(:any)'] = 'reportes/red_objetivos/objetivo_gestion/$1/$2';
$route['admin/producto_terminal/(:any)'] = 'reportes/red_objetivos/producto_terminal/$1/$2/$3';
$route['admin/reporte_red_obj/(:any)'] = 'reportes/red_objetivos/reporte_red_objetivos/$1/$2';
//=============REPORTES/FRM. POA PLANIFICACION=================//
//reportes/frm. poa planificacion/operaciones//
/*$route['admin/rep/prog'] = 'reportes/reportes_vistas/reporte_acciones';
$route['admin/reporte_operaciones/(:any)'] = 'reportes/operaciones/reporte_operacion/$1';
$route['admin/reporte_acciones/(:any)'] = 'reportes/red_objetivos/reporte_acciones/$1/$2';*/
//reportes/frm. poa ejecucion/
$route['admin/rep/frm_poa_ejec'] = 'reportes/reportes_vistas/objetivo_estrategicos/2'; //// ----------------- a eliminar
$route['admin/rep/pdf/obj_ac'] = 'reportes/reporte/ficha_tecnica_ejecucion';
//Mantenimiento Clasificacion Sectorial
$route['clasificacion_sectorial'] = 'mantenimiento/clasificacion_sectorial/lista_clasificadores_sectores';
$route['subsector/(:any)'] = 'mantenimiento/clasificacion_sectorial/lista_clasificadores_subsectores/$1/$2';


//$route['admin/reporte/obje'] = 'reportes/reportes_vistas/objetivo_estrategicos/1'; //// ----------------- a eliminar
$route['admin/analisis_sit'] = 'programacion/analisis_situacion';
$route['admin/prog/analisis/(:any)'] = 'programacion/analisis_situacion/ver_analisis_situacion/$1';


$route['trabajando'] = 'trabajando/vista';
$route['admin/reporte/objges_eje'] = 'reportes/reportes_vistas/objetivo_gestion_producto_terminal_ejecutado';


/*----------------------- MANTENIMIENTO -- CIERRE DE PROGRAMACION -------------------*/
//$route['mnt/cierre_programacion'] = 'mantenimiento/cierre_programacion/cierre';
$route['mnt/cierre_programacion'] = 'mantenimiento/cierre_programacion/cierre_proyectos';
$route['mnt/validar_cierre_prog'] = 'mantenimiento/cierre_programacion/validar_cierre_proyectos';


//////PDES
$route['pdes_meta/(:any)'] = 'mantenimiento/pdes_meta/lista_pdes_meta/$1';
$route['pdes_resultados/(:any)'] = 'mantenimiento/pdes_resultados/lista_pdes_resultados/$1';
$route['pdes_accion/(:any)'] = 'mantenimiento/pdes_accion/lista_pdes_accion/$1';
/////PTDI
$route['ptdi_meta/(:any)'] = 'mantenimiento/ptdi_meta/lista_ptdi_meta/$1';
$route['ptdi_resultados/(:any)'] = 'mantenimiento/ptdi_resultados/lista_ptdi_resultados/$1';
$route['ptdi_accion/(:any)'] = 'mantenimiento/ptdi_accion/lista_ptdi_accion/$1';


/*----------------------- VERSION 1.1. Enviar Mails -------------------*/
$route['sendmail'] = 'mailer';

/*======== MODIFICACIONES OPERACIONES =========*/
$route['mod/mis_operaciones'] = 'modificaciones/cmodificaciones/mis_operaciones'; //// Lista de Operaciones
$route['mod/opcion_fis/(:any)'] = 'modificaciones/cmodificaciones/opcion_modificacion_fisica/$1/$2/$3'; //// Opcion de Modificaciones
$route['mod/derivar_proy'] = 'modificaciones/cmodificaciones/derivar_proyecto';  //// Derivar a TOP, POA, FIN

$route['mod/tue'] = 'modificaciones/cmodificaciones/lista_tue'; //// TOP
$route['mod/vpoa'] = 'modificaciones/cmodificaciones/lista_vpoa'; //// POA

$route['mod/opciones/(:any)'] = 'modificaciones/cmod_opciones/opciones_modificar/$1/$2/$3'; //// OPCIONES A MODIFICAR 
$route['mod/prod_mtotal/(:any)'] = 'modificaciones/cmod_opciones/mod_prod_mtotal/$1/$2'; //// MODIFICAR META TOTAL - PRODUCTOS

$route['mod/act_mtotal/(:any)'] = 'modificaciones/cmod_opciones/mod_act_mtotal/$1/$2'; //// MODIFICAR META TOTAL - ACTIVIDADES

$route['mod/partidas/(:any)'] = 'modificaciones/cmod_opciones/mod_partidas/$1'; //// MODIFICAR ADICION FUENTES, ORGANISMOS
$route['mod/mod_par/(:any)'] = 'insumos/cprog_insumos_partida/modificar_partida/$1'; //// MODIFICAR DE PARTIDA