<?php
class model_funcionario extends CI_Model {
  
    /**
    * Responsable for auto load the database
    * @return void
    */
    public function __construct()
    {
        $this->load->database();
    }
   

    public function get_funcionarios()
    {
        $this->db->select("u.uni_unidad,c.car_cargo,f.*");
        $this->db->from('funcionario f,unidadorganizacional u,cargo c');
        $this->db->where('f.fun_estado', 1);
        $this->db->where('f.uni_id = u.uni_id');
        $this->db->where('f.car_id = c.car_id');
        $this->db->ORDER_BY('f.fun_id');
        $query = $this->db->get();
        
        return $query->result_array();
    }
	
	public function get_estructuras()
    {
        $this->db->select("*");
        $this->db->from('unidadorganizacional');
        $this->db->where('uni_estado', 1);
        $query = $this->db->get();
        
        return $query->result_array();
    }
    public function get_funcargos()
    {
        $this->db->select('*');
        $this->db->from('cargo');
        $this->db->where('car_estado', 1);
        $query1 = $this->db->get();
        return $query1->result_array();
    }
    function add_funcionario($fun_nombre,$fun_paterno,$fun_materno,$fun_dni,$fun_telefono,
                             $fun_domicilio,$fun_usuario,$fun_password,$uni_id,$car_id,$fun_rol,$fun_cargo){
        $data = array(
            'fun_nombre' => strtoupper( $fun_nombre),
            'fun_paterno' => strtoupper( $fun_paterno),
            'fun_materno' => strtoupper( $fun_materno),
            'fun_cargo' => strtoupper( $fun_cargo),
            'fun_dni' => strtoupper($fun_dni),
            'fun_telefono' => $fun_telefono,
            'fun_domicilio' => strtoupper( $fun_domicilio),
            'fun_usuario' => strtoupper($fun_usuario),
            'fun_password' => strtoupper($fun_password),
            'uni_id' => $uni_id,
            'car_id' => $car_id,
            'fun_rol'.$fun_rol =>$fun_rol*10,

        );
        $this->db->INSERT('funcionario',$data);
    }
    public function get_fun($id)
    {
        $this->db->select("*");
        $this->db->from('funcionario');
        $this->db->where('fun_estado',1);
        $this->db->where('fun_id',$id);
        $query = $this->db->get();
        return $query->result_array();
    }
    function mod_funcionario($fun_id,$fun_nombre,$fun_paterno,$fun_materno,$fun_dni,$fun_telefono,
                             $fun_domicilio,$fun_usuario,$fun_password,$uni_id,$car_id,$fun_rol,$fun_cargo){
        $data = array(
            'fun_nombre' => strtoupper( $fun_nombre),
            'fun_paterno' => strtoupper( $fun_paterno),
            'fun_materno' => strtoupper( $fun_materno),
            'fun_cargo' => strtoupper( $fun_cargo),
            'fun_dni' => strtoupper($fun_dni),
            'fun_telefono' => $fun_telefono,
            'fun_domicilio' => strtoupper( $fun_domicilio),
            'fun_usuario' => strtoupper($fun_usuario),
            'fun_password' => strtoupper($fun_password),
            'uni_id' => $uni_id,
            'car_id' => $car_id,
        );
        $cont =1;
        //son 10 roles
        while( $cont <= 10){
            if($cont == $fun_rol){
                $data['fun_rol'.$cont]= $fun_rol*10;
            }else{
                $data['fun_rol'.$cont]= 0;
            }
            $cont++;
        }


        $this->db->WHERE('fun_id',$fun_id);
        $this->db->UPDATE('funcionario',$data);
    }


	///cambiar contraseņa
	function mod_password($fun_id,$usuario,$password)
    {
            $query = $this->db->query("
                                UPDATE funcionario
                                   SET  fun_usuario='".$usuario."',
                                        fun_password='".$password."'
                                 WHERE fun_id=".$fun_id."
                         ");

        $this->session->sess_destroy();
        redirect('admin/dashboard');

    }

	
	







}
?>  
