<?php

//alerta temprana del producto terminal
class Malerta_pterminal extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    //obtener programacion del producto terminal
    function prog_mensual_pterminal($pt_id)
    {
        $this->db->SELECT('*');
        $this->db->FROM('vpterminal_prog_mensual_fis');
        $this->db->WHERE('pt_id', $pt_id);
        $query = $this->db->get();
        return $query->result_array();
    }

    //obtener ejecucion absoluta del producto terminal
    function ejec_abs_pterminal($pt_id)
    {
        $this->db->SELECT('*');
        $this->db->FROM('vpterminal_ejec_mensual_absoluto');
        $this->db->WHERE('pt_id', $pt_id);
        $query = $this->db->get();
        return $query->result_array();
    }

    //obtener ejecucion relativa del producto terminal
    function ejec_rel_pterminal($pt_id)
    {
        $this->db->SELECT('*');
        $this->db->FROM('vpterminal_ejec_mensual_relativo');
        $this->db->WHERE('pt_id', $pt_id);
        $query = $this->db->get();
        return $query->result_array();
    }
}