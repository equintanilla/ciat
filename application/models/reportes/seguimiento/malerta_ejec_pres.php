<?php

class malerta_ejec_pres extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    //ejecucion presupuestaria a nivel institucional
    function get_eje_pres($mes_id, $gestion)
    {
        $sql = ' SELECT SUM(pem_ppto_inicial) AS ppto_inicial, SUM(pem_modif_aprobadas) AS modif_aprobadas, SUM(pem_ppto_vigente) AS ppto_vigente,
        (SELECT get_programacion(' . $gestion . ', ' . $mes_id . ')) AS programado, SUM(pem_devengado) AS ejecutado,
        CASE WHEN (SELECT get_programacion(' . $gestion . ',' . $mes_id . ')) = 0 THEN 0 ELSE
        ((SUM(pem_devengado) / (SELECT get_programacion(' . $gestion . ', ' . $mes_id . '))) * 100)  END AS ejec_respecto_prog,
        CASE WHEN (SUM(pem_ppto_vigente)) = 0 THEN 0 ELSE
        (SUM(pem_devengado) / SUM(pem_ppto_vigente)) * 100 END AS ejec_respecto_total
         FROM proy_ejec_mes
         WHERE gestion = ' . $gestion . ' AND mes_id = ' . $mes_id;
        $query = $this->db->query($sql);
        return $query->row();
    }

    //ejecucion presupuestaria del proyecto
    function get_ejec_proy($aper_programa, $gestion, $mes_id)
    {
        $sql = "
            SELECT DISTINCT a.aper_id, a.aper_gestion, a.aper_programa, a.aper_proyecto, a.aper_actividad, a.aper_descripcion,
            ap.proy_id, pf.pfec_ejecucion, p.proy_nombre,
            COALESCE( (SELECT get_programacion_acumulada_proy(CAST(ap.proy_id AS integer), " . $mes_id . ", pf.pfec_ejecucion)), 0) AS programado_acumulado,
            COALESCE((SELECT ppto_inicial FROM fnget_ejecucion_acumulada_sigep( CAST(ap.proy_id AS integer)," . $mes_id . "," . $gestion . ")), 0) AS ppto_inicial,
            COALESCE((SELECT modificaciones FROM fnget_ejecucion_acumulada_sigep( CAST(ap.proy_id AS integer)," . $mes_id . "," . $gestion . ")), 0) AS modificaciones,
            COALESCE((SELECT ppto_vigente FROM fnget_ejecucion_acumulada_sigep( CAST(ap.proy_id AS integer)," . $mes_id . "," . $gestion . ")), 0) AS ppto_vigente,
            COALESCE((SELECT ejecucion_acumulada FROM fnget_ejecucion_acumulada_sigep( CAST(ap.proy_id AS integer)," . $mes_id . "," . $gestion . ")), 0) AS ejecucion_acumulada
            FROM aperturaprogramatica a
            INNER JOIN aperturaproyectos ap ON a.aper_id = ap.aper_id
            LEFT JOIN _proyectofaseetapacomponente pf ON ap.proy_id = pf.proy_id
            INNER JOIN _proyectos p ON ap.proy_id = p.proy_id
            WHERE  a.aper_programa = '" . $aper_programa . "' AND  (a.aper_proyecto <> '0000' OR a.aper_actividad <> '000') AND a.aper_estado IN (1,2)
            AND a.aper_gestion = " . $gestion . " AND p.estado IN (1,2) AND (" . $gestion . " BETWEEN p.proy_gestion_inicio AND p.proy_gestion_fin)
            ORDER BY a.aper_programa, a.aper_proyecto, a.aper_actividad
        ";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    //obtener programacion ejecucion por programa
    function get_prog_ejec2($gestion, $mes_id)
    {
        $sql = "
        SELECT  a.aper_programa,
        SUM( (SELECT get_programacion_acumulada_proy(CAST(ap.proy_id AS integer), " . $mes_id . ", pf.pfec_ejecucion)) )AS programado_acumulado,
        SUM(COALESCE((SELECT ppto_inicial FROM fnget_ejecucion_acumulada_sigep( CAST(ap.proy_id AS integer)," . $mes_id . "," . $gestion . ")), 0) )AS ppto_inicial,
        SUM(COALESCE((SELECT modificaciones FROM fnget_ejecucion_acumulada_sigep( CAST(ap.proy_id AS integer)," . $mes_id . "," . $gestion . ")), 0) )AS modificaciones,
        SUM(COALESCE((SELECT ppto_vigente FROM fnget_ejecucion_acumulada_sigep( CAST(ap.proy_id AS integer)," . $mes_id . "," . $gestion . ")), 0) )AS ppto_vigente,
        SUM(COALESCE((SELECT ejecucion_acumulada FROM fnget_ejecucion_acumulada_sigep( CAST(ap.proy_id AS integer)," . $mes_id . "," . $gestion . ")), 0)) AS ejecucion_acumulada
        FROM aperturaprogramatica a
        INNER JOIN aperturaproyectos ap ON a.aper_id = ap.aper_id
        LEFT JOIN _proyectofaseetapacomponente pf ON ap.proy_id = pf.proy_id
        INNER JOIN _proyectos p ON ap.proy_id = p.proy_id
        WHERE (a.aper_proyecto <> '0000' OR a.aper_actividad <> '000') AND a.aper_estado IN (1,2)
        AND a.aper_gestion = " . $gestion . "AND p.estado IN (1,2) AND (" . $gestion . " BETWEEN p.proy_gestion_inicio AND p.proy_gestion_fin)
        GROUP BY a.aper_programa
        ORDER BY a.aper_programa
        ";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    //Obtener programacion y ejecucion financiera del proyecto a nivel unidad ejecutora
    function prog_ejec_uni_proy($uni_id, $mes, $gestion)
    {
        $sql = "
        SELECT DISTINCT t.proy_id,pf.pfec_ejecucion, p.proy_nombre,p.proy_codigo,
        COALESCE((SELECT ppto_inicial FROM fnget_ejecucion_acumulada_sigep( CAST(t.proy_id AS integer)," . $mes . "," . $gestion . ")), 0) AS ppto_inicial,
        COALESCE((SELECT modificaciones FROM fnget_ejecucion_acumulada_sigep( CAST(t.proy_id AS integer)," . $mes . "," . $gestion . ")), 0) AS modificaciones,
        COALESCE((SELECT ppto_vigente FROM fnget_ejecucion_acumulada_sigep( CAST(t.proy_id AS integer)," . $mes . "," . $gestion . ")), 0) AS ppto_vigente,
        COALESCE( (SELECT get_programacion_acumulada_proy(CAST(t.proy_id AS integer), " . $mes . ", pf.pfec_ejecucion)), 0) AS programado_acumulado,
        COALESCE((SELECT ejecucion_acumulada FROM fnget_ejecucion_acumulada_sigep( CAST(t.proy_id AS integer)," . $mes . "," . $gestion . ")), 0) AS ejecucion_acumulada
        FROM (select proy_id
        FROM _proyectofuncionario
        WHERE uni_ejec = " . $uni_id . "
        GROUP BY proy_id, uni_ejec)t
        LEFT JOIN _proyectofaseetapacomponente pf ON t.proy_id = pf.proy_id
        INNER JOIN _proyectos p ON t.proy_id = p.proy_id
        WHERE p.estado IN (1,2) and (" . $gestion . " BETWEEN p.proy_gestion_inicio AND p.proy_gestion_fin) ORDER BY t.proy_id
        ";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    //lista de unidad ejecutora
    function prog_ejec_unidad()
    {
        $sql = '
        SELECT u.uni_id, u.uni_unidad
        FROM (
        SELECT uni_ejec FROM _proyectofuncionario
        WHERE pfun_estado IN (1,2) GROUP BY uni_ejec) t
        INNER JOIN unidadorganizacional u ON t.uni_ejec = u.uni_id
        ';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    //Programacion y ejecucion financiera a nivel provincia lista de proyectos
    function prog_ejec_prov_proy($prov_id, $mes, $gestion)
    {
        $sql = "
        SELECT DISTINCT  p.proy_id,  p.proy_nombre,p.proy_codigo,
	    COALESCE((SELECT ppto_inicial FROM fnget_ejecucion_acumulada_sigep( CAST(p.proy_id AS integer)," . $mes . "," . $gestion . ")), 0) AS ppto_inicial,
        COALESCE((SELECT modificaciones FROM fnget_ejecucion_acumulada_sigep( CAST(p.proy_id AS integer)," . $mes . "," . $gestion . ")), 0) AS modificaciones,
        COALESCE((SELECT ppto_vigente FROM fnget_ejecucion_acumulada_sigep( CAST(p.proy_id AS integer)," . $mes . "," . $gestion . ")), 0) AS ppto_vigente,
        COALESCE( (SELECT get_programacion_acumulada_proy(CAST(p.proy_id AS integer)," . $mes . ", pf.pfec_ejecucion)), 0) AS programado_acumulado,
        COALESCE((SELECT ejecucion_acumulada FROM fnget_ejecucion_acumulada_sigep( CAST(p.proy_id AS integer)," . $mes . "," . $gestion . ")), 0) AS ejecucion_acumulada
        FROM _proyectosprovincias pv
        INNER JOIN _proyectos p ON pv.proy_id = p.proy_id
        LEFT JOIN _proyectofaseetapacomponente pf ON pv.proy_id = pf.proy_id
        WHERE pv.estado IN (1,2) AND pv.prov_id = " . $prov_id . " AND p.estado IN (1,2)
        AND (" . $gestion . " BETWEEN p.proy_gestion_inicio AND p.proy_gestion_fin) ORDER BY p.proy_id
        ";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    //dato de provincia
    function get_provincia($prov_id)
    {
        $this->db->SELECT('d.dep_departamento, p.prov_id, p.prov_provincia');
        $this->db->FROM('_provincias p');
        $this->db->JOIN('_departamentos d','p.dep_id = d.dep_id', 'LEFT');
        $this->db->WHERE('d.dep_estado IN(1,2)');
        $this->db->WHERE('p.prov_estado IN(1,2)');
        $this->db->WHERE('p.prov_id', $prov_id);
        $query = $this->db->get();
        return $query->row();
    }
    //lista provincias
    function lista_provincia(){
        $this->db->SELECT('d.dep_departamento, p.prov_id, p.prov_provincia');
        $this->db->FROM('_provincias p');
        $this->db->JOIN('_departamentos d','p.dep_id = d.dep_id', 'LEFT');
        $this->db->WHERE('d.dep_estado IN(1,2)');
        $this->db->WHERE('p.prov_estado IN(1,2)');
        $this->db->ORDER_BY ('p.dep_id','ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

}