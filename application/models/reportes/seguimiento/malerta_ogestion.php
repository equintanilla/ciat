<?php
//alerta temprana de objetivo de gestion
class Malerta_ogestion extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }
    //obtener programacion del objetivo de gestion
    function prog_mensual_ogestion($o_id){
        $this->db->SELECT('*');
        $this->db->FROM('vogestion_prog_mensual_fis');
        $this->db->WHERE('o_id',$o_id);
        $query = $this->db->get();
        return $query->result_array();
    }
    //obtener ejecucion absoluta del objetivo de gestion
    function ejec_abs_ogestion($o_id){
        $this->db->SELECT('*');
        $this->db->FROM('vogestion_ejec_mensual_absoluto');
        $this->db->WHERE('o_id',$o_id);
        $query = $this->db->get();
        return $query->result_array();
    }
    //obtener ejecucion relativa del objetivo de gestion
    function ejec_rel_ogestion($o_id){
        $this->db->SELECT('*');
        $this->db->FROM('vogestion_ejec_mensual_relativo');
        $this->db->WHERE('o_id',$o_id);
        $query = $this->db->get();
        return $query->result_array();
    }
}