<?php
class Model_taqpacha extends CI_Model {
    public function __construct()
    {
        $this->load->database();
    }

    public function get_sectores()
    {
        $query = "select codsectorial,sector from _clasificadorsectorial where codsectorialduf='Sector'";
        $query = $this->db->query($query);
        return $query->result_array();
    }

    public function get_regiones()
    {
        $query = "select * from _regiones";
        $query = $this->db->query($query);
        return $query->result_array();
    }
    public function get_regiones_sinMulti()
    {
        $query = "select * from _regiones where reg_id<>8 and reg_id<>0 order by reg_region";
        $query = $this->db->query($query);
        return $query->result_array();
    }
	public function get_region($reg_id){
        $query = "select * from _regiones where reg_id=".$reg_id;
        $query = $this->db->query($query);
        return $query->result_array();
    }

    public function get_provincias()
    {
        $query = "select * from _provincias order by prov_provincia";
        $query = $this->db->query($query);
        return $query->result_array();
    }
    public function get_provincias_sinMulti()
    {
        $query = "select * from _provincias where prov_id<>906 order by prov_provincia";
        $query = $this->db->query($query);
        return $query->result_array();
    }
    public function get_provincia($prov_id)
    {
        $query = "select upper(prov_provincia) as prov_provincia from _provincias where prov_id=".$prov_id;
        $query = $this->db->query($query);
        return $query->result_array();
    }

    public function get_municipios()
    {
        $query = "select * from _municipios order by muni_municipio";
        $query = $this->db->query($query);
        return $query->result_array();
    }
    public function get_municipios_sinMulti()
    {
        $query = "select * from _municipios where muni_id<>90504 order by muni_municipio";
        $query = $this->db->query($query);
        return $query->result_array();
    }
    public function get_municipio($mun_id)
    {
        $query = "select upper(muni_municipio) as muni_municipio from _municipios where muni_id=".$mun_id;
        $query = $this->db->query($query);
        return $query->result_array();
    }

	public function get_unidad_ejec($ue_id){
		$query = "select upper(uni_unidad) as uni_unidad from unidadorganizacional where uni_id=".$ue_id;
		$query = $this->db->query($query);
		return $query->result_array();
	}
	public function get_estado($est_id){
		$query = "select upper(ep_descripcion) as ep_descripcion from _estadoproyecto where ep_id=".$est_id;
		$query = $this->db->query($query);
		return $query->result_array();
	}
	/*
	1. Presupuesto de inversion publica por gestión
	*/
    public function get_reporte1($gestion)
    {
        $query = "select fu.uni_ejec,fu.ue,coalesce(fn_nro_proyectos_fase(fu.uni_ejec,".$gestion.",1,2),0) as inv,
			 coalesce(fn_nro_proyectos_fase(fu.uni_ejec,".$gestion.",1,1),0) as pre_inv,
			 fn_nro_proyectos_presup(fu.uni_ejec,".$gestion.",1) as presup,
			 fn_nro_proyectos_presup_ejecutado(fu.uni_ejec,".$gestion.",1) as presup_ejec
                from _proyectos as p
                Inner Join _tipoproyecto as tp On p.tp_id=tp.tp_id
                Inner Join (select apy.*,apg.* from aperturaprogramatica as apg, aperturaproyectos as apy where apy.aper_id=apg.aper_id) as tap On p.proy_id=tap.proy_id
                Inner Join (select pf.\"proy_id\",pf.\"uni_ejec\",u.\"uni_unidad\" as ue
                        from _proyectofuncionario pf
                        Inner Join unidadorganizacional as u On pf.\"uni_ejec\"=u.\"uni_id\"
                where pf.pfun_tp=1) as fu On fu.proy_id=p.proy_id
                where tap.aper_programa in (
								select aper_programa from aperturaprogramatica
								where aper_proyecto='0000'
								and aper_actividad='000'
								and aper_gestion=".$gestion."
								and aper_asignado=1							
								)
								and p.estado!=3 
								and tap.aper_gestion=".$gestion."
								and proy_estado!=4 
								and tp.tp_id =1
								group by fu.ue,fu.uni_ejec
								order by fu.ue
		";
        $query = $this->db->query($query);
        return $query->result_array();
    }

	/*
	2. Presupuesto de programas no recurrentes por gestión
	*/
    public function get_reporte2($gestion)
    {
        $query = "
				select fu.uni_ejec,fu.ue,coalesce(fn_nro_proyectos(fu.uni_ejec,".$gestion.",3),0) as nro_prog_rec,
							 fn_nro_proyectos_presup(fu.uni_ejec,".$gestion.",3) as presup,
							 fn_nro_proyectos_presup_ejecutado(fu.uni_ejec,".$gestion.",3) as presup_ejec
								from _proyectos as p
								Inner Join _tipoproyecto as tp On p.tp_id=tp.tp_id
								Inner Join (select apy.*,apg.* from aperturaprogramatica as apg, aperturaproyectos as apy where apy.aper_id=apg.aper_id) as tap On p.proy_id=tap.proy_id
								Inner Join (select pf.proy_id,pf.uni_ejec,u.uni_unidad as ue from _proyectofuncionario pf Inner Join unidadorganizacional as u On pf.uni_ejec=u.uni_id
								where pf.pfun_tp=1) as fu On fu.proy_id=p.proy_id
								where tap.aper_programa in (select aper_programa from aperturaprogramatica where aper_proyecto='0000'and aper_actividad='000'and aper_gestion=".$gestion." and aper_asignado=1)
												and p.estado!=3
					and tap.aper_gestion=".$gestion."
					and proy_estado!=4
					and tp.tp_id =3
				group by fu.ue,fu.uni_ejec
				order by fu.ue
		";
        $query = $this->db->query($query);
        return $query->result_array();
    }

	/*
	3. ESTADO DE PROYECTOS DE INVERSIÓN PUBLICA ANUAL
	*/
    public function get_reporte3($gestion)
    {
        $query = "
			select
			_est_proy,
			count(*) as cantidad,
			sum(_presupuesto) as presup,
			sum(_ejecutado) as ejecutado
			from
			(select coalesce(fn_nro_proyectos_estado(p.proy_id,".$gestion."),'Sin reporte de estado') as _est_proy,
			coalesce(fn_nro_proyectos_presup(p.proy_id,".$gestion."),0) as _presupuesto,
			coalesce(fn_nro_proyectos_presup_ejecutado(p.proy_id,".$gestion."),0) as _ejecutado
			from _proyectos as p
				 Inner Join (select apy.*,apg.* from aperturaprogramatica as apg, aperturaproyectos as apy where apy.aper_id=apg.aper_id) as tap On p.proy_id=tap.proy_id
				 Inner Join (select pf.proy_id,pf.uni_ejec,u.uni_unidad as ue from _proyectofuncionario pf Inner Join unidadorganizacional as u On pf.uni_ejec=u.uni_id where pf.pfun_tp=1) as fu On fu.proy_id=p.proy_id
					 where tap.aper_programa in (select aper_programa from aperturaprogramatica where aper_proyecto='0000'and aper_actividad='000' and aper_gestion=".$gestion." and aper_asignado=1)
								and p.estado!=3
			and tap.aper_gestion=".$gestion."
			and proy_estado!=4
			and p.tp_id =1) as previo
								group by previo._est_proy
		";
        $query = $this->db->query($query);
        return $query->result_array();
    }

	/*
	4. ESTADO DE PROGRAMAS NO RECURRENTES ANUAL
	*/
    public function get_reporte4($gestion)
    {
        $query = "
			select
			_est_proy,
			count(*) as cantidad,
			sum(_presupuesto) as presup,
			sum(_ejecutado) as ejecutado
			from
			(select coalesce(fn_nro_proyectos_estado(p.proy_id,".$gestion."),'Sin reporte de estado') as _est_proy,
			coalesce(fn_nro_proyectos_presup(p.proy_id,".$gestion."),0) as _presupuesto,
			coalesce(fn_nro_proyectos_presup_ejecutado(p.proy_id,".$gestion."),0) as _ejecutado
			from _proyectos as p
				 Inner Join (select apy.*,apg.* from aperturaprogramatica as apg, aperturaproyectos as apy where apy.aper_id=apg.aper_id) as tap On p.proy_id=tap.proy_id
				 Inner Join (select pf.proy_id,pf.uni_ejec,u.uni_unidad as ue from _proyectofuncionario pf Inner Join unidadorganizacional as u On pf.uni_ejec=u.uni_id where pf.pfun_tp=1) as fu On fu.proy_id=p.proy_id
					 where tap.aper_programa in (select aper_programa from aperturaprogramatica where aper_proyecto='0000'and aper_actividad='000' and aper_gestion=".$gestion." and aper_asignado=1)
								and p.estado!=3
			and tap.aper_gestion=".$gestion."
			and proy_estado!=4
			and p.tp_id =3) as previo
			group by previo._est_proy
		";
        $query = $this->db->query($query);
        return $query->result_array();
    }
	
	
	/*
	5. INVERSION PUBLICA ANUAL POR SECTOR ECONOMICO
	*/
    public function get_reporte5($gestion, $sec_id)
    {
		$sec_id = substr($sec_id, 0, 2);
        $query = "
			select _sector,sum(_total) as cantidad,sum(_presup) as presup,sum(_ejecutado) as ejecutado from
			(select csec.sector as _sector,
						 coalesce(count(p.*),0) as _total,
						 coalesce(fn_nro_proyectos_presup_sector(csec.codsectorial,".$gestion.",1),0) as _presup,
						 coalesce(fn_nro_proyectos_presup_sector_ejecutado(csec.codsectorial,".$gestion.",1),0) as _ejecutado
			from _proyectos as p
			inner join _clasificadorsectorial as csec on p.codsectorial=csec.codsectorial
				 Inner Join (select apy.*,apg.* from aperturaprogramatica as apg, aperturaproyectos as apy where apy.aper_id=apg.aper_id) as tap On p.proy_id=tap.proy_id
				 Inner Join (select pf.proy_id,pf.uni_ejec,u.uni_unidad as ue from _proyectofuncionario pf Inner Join unidadorganizacional as u On pf.uni_ejec=u.uni_id where pf.pfun_tp=1) as fu On fu.proy_id=p.proy_id
					 where tap.aper_programa in (select aper_programa from aperturaprogramatica where aper_proyecto='0000'and aper_actividad='000' and aper_gestion=".$gestion." and aper_asignado=1)
								and p.estado!=3
			and tap.aper_gestion=".$gestion."
			and proy_estado!=4
			and p.tp_id =1
			and (substring(csec.codsectorial,1,2) like '%".$sec_id."%' or ".$sec_id." = '-1')
				group by csec.codsectorial,csec.sector
				order by csec.sector) as previo
				GROUP BY previo._sector
		";
        $query = $this->db->query($query);
        return $query->result_array();
    }

	/*
	6. INVERSION ANUAL PROGRAMAS NO RECURRENTES POR SECTOR ECONOMICO
	*/
    public function get_reporte6($gestion, $sec_id)
    {
		$sec_id = substr($sec_id, 0, 2);
        $query = "
			select _sector,sum(_total) as cantidad,sum(_presup) as presup, sum(_ejecutado) as ejecutado from
			(select csec.sector as _sector,
						 coalesce(count(p.*),0) as _total,
						 coalesce(fn_nro_proyectos_presup_sector(csec.codsectorial,".$gestion.",3),0) as _presup,
						 coalesce(fn_nro_proyectos_presup_sector_ejecutado(csec.codsectorial,".$gestion.",3),0) as _ejecutado
			from _proyectos as p
			inner join _clasificadorsectorial as csec on p.codsectorial=csec.codsectorial
				 Inner Join (select apy.*,apg.* from aperturaprogramatica as apg, aperturaproyectos as apy where apy.aper_id=apg.aper_id) as tap On p.proy_id=tap.proy_id
				 Inner Join (select pf.proy_id,pf.uni_ejec,u.uni_unidad as ue from _proyectofuncionario pf Inner Join unidadorganizacional as u On pf.uni_ejec=u.uni_id where pf.pfun_tp=1) as fu On fu.proy_id=p.proy_id
					 where tap.aper_programa in (select aper_programa from aperturaprogramatica where aper_proyecto='0000'and aper_actividad='000' and aper_gestion=".$gestion." and aper_asignado=1)
								and p.estado!=3
			and tap.aper_gestion=".$gestion."
			and proy_estado!=4
			and p.tp_id =3
			and (substring(csec.codsectorial,1,2) like '%".$sec_id."%' or ".$sec_id." = '-1')
				group by csec.codsectorial,csec.sector
				order by csec.sector) as previo
				GROUP BY previo._sector
		";
        $query = $this->db->query($query);
        return $query->result_array();
    }
	
	
	/*
	7. INVERSION PUBLICA ANUAL POR REGION
	*/
    public function get_reporte7($gestion, $reg_id)
    {
        $query = "
			select _regiones.reg_id,
						 reg_region,
						 count(p.*) as cantidad,
						 fn_nro_proyectos_presup_region(_regiones.reg_id,".$gestion.",1) as presup
			from _proyectos as p
				 Inner Join _proyectosmunicipios as pmun On p.proy_id=pmun.proy_id
					 Inner Join _municipios as mun on pmun.muni_id= mun.muni_id
					 Inner join _regiones on mun.reg_id=_regiones.reg_id
				 Inner Join (select apy.*,apg.* from aperturaprogramatica as apg, aperturaproyectos as apy where apy.aper_id=apg.aper_id) as tap On p.proy_id=tap.proy_id
				 Inner Join (select pf.proy_id,pf.uni_ejec,u.uni_unidad as ue from _proyectofuncionario pf Inner Join unidadorganizacional as u On pf.uni_ejec=u.uni_id where pf.pfun_tp=1) as fu On fu.proy_id=p.proy_id
					 where tap.aper_programa in (select aper_programa from aperturaprogramatica where aper_proyecto='0000'and aper_actividad='000' and aper_gestion=".$gestion." and aper_asignado=1)
								and p.estado!=3
			and tap.aper_gestion=".$gestion."
			and proy_estado!=4
			and p.tp_id =1
			and (_regiones.reg_id=".$reg_id." or ".$reg_id."=-1)
				group by _regiones.reg_id,reg_region
				order by count(p.*) desc
		";
        $query = $this->db->query($query);
        return $query->result_array();
    }
    public function get_reporte7_detalle($gestion,$reg_id){
        $query = "
			select _regiones.reg_id,
			reg_region,
			p.proy_nombre as proyecto,
			p.proy_sisin,
			p.proy_gestion_inicio_ddmmaaaa,
			p.proy_gestion_fin_ddmmaaaa,
			p.proy_poblac_beneficiaria
			from _proyectos as p
				 Inner Join _proyectosmunicipios as pmun On p.proy_id=pmun.proy_id
					 Inner Join _municipios as mun on pmun.muni_id= mun.muni_id
					 Inner join _regiones on mun.reg_id=_regiones.reg_id
				 Inner Join (select apy.*,apg.* from aperturaprogramatica as apg, aperturaproyectos as apy where apy.aper_id=apg.aper_id) as tap On p.proy_id=tap.proy_id
				 Inner Join (select pf.proy_id,pf.uni_ejec,u.uni_unidad as ue from _proyectofuncionario pf Inner Join unidadorganizacional as u On pf.uni_ejec=u.uni_id where pf.pfun_tp=1) as fu On fu.proy_id=p.proy_id
					 where tap.aper_programa in (select aper_programa from aperturaprogramatica where aper_proyecto='0000'and aper_actividad='000' and aper_gestion=".$gestion." and aper_asignado=1)
								and p.estado!=3
			and tap.aper_gestion=".$gestion."
			and proy_estado!=4
			and p.tp_id =1
			and (_regiones.reg_id=".$reg_id.")
			order by p.proy_nombre desc
		";
        $query = $this->db->query($query);
        return $query->result_array();
    }

	/*
	8. INVERSION ANUAL EN PROGRAMAS NO RECURRENTES POR REGION
	*/
    public function get_reporte8($gestion, $reg_id)
    {
        $query = "
			select _regiones.reg_id,
						 reg_region,
						 count(p.*) as cantidad,
						 fn_nro_proyectos_presup_region(_regiones.reg_id,".$gestion.",3) as presup
			from _proyectos as p
				 Inner Join _proyectosmunicipios as pmun On p.proy_id=pmun.proy_id
					 Inner Join _municipios as mun on pmun.muni_id= mun.muni_id
					 Inner join _regiones on mun.reg_id=_regiones.reg_id
				 Inner Join (select apy.*,apg.* from aperturaprogramatica as apg, aperturaproyectos as apy where apy.aper_id=apg.aper_id) as tap On p.proy_id=tap.proy_id
				 Inner Join (select pf.proy_id,pf.uni_ejec,u.uni_unidad as ue from _proyectofuncionario pf Inner Join unidadorganizacional as u On pf.uni_ejec=u.uni_id where pf.pfun_tp=1) as fu On fu.proy_id=p.proy_id
					 where tap.aper_programa in (select aper_programa from aperturaprogramatica where aper_proyecto='0000'and aper_actividad='000' and aper_gestion=".$gestion." and aper_asignado=1)
								and p.estado!=3
			and tap.aper_gestion=".$gestion."
			and proy_estado!=4
			and p.tp_id =3
			and (_regiones.reg_id=".$reg_id." or ".$reg_id."=-1)
				group by _regiones.reg_id,reg_region
				order by reg_region
		";
        $query = $this->db->query($query);
        return $query->result_array();
    }
	public function get_reporte8_detalle($gestion, $reg_id){
        $query = "
			select _regiones.reg_id,
			reg_region,
			p.proy_nombre as proyecto,
			p.proy_sisin,
			p.proy_gestion_inicio_ddmmaaaa,
			p.proy_gestion_fin_ddmmaaaa,
			p.proy_poblac_beneficiaria
			from _proyectos as p
				 Inner Join _proyectosmunicipios as pmun On p.proy_id=pmun.proy_id
					 Inner Join _municipios as mun on pmun.muni_id= mun.muni_id
					 Inner join _regiones on mun.reg_id=_regiones.reg_id
				 Inner Join (select apy.*,apg.* from aperturaprogramatica as apg, aperturaproyectos as apy where apy.aper_id=apg.aper_id) as tap On p.proy_id=tap.proy_id
				 Inner Join (select pf.proy_id,pf.uni_ejec,u.uni_unidad as ue from _proyectofuncionario pf Inner Join unidadorganizacional as u On pf.uni_ejec=u.uni_id where pf.pfun_tp=1) as fu On fu.proy_id=p.proy_id
					 where tap.aper_programa in (select aper_programa from aperturaprogramatica where aper_proyecto='0000'and aper_actividad='000' and aper_gestion=".$gestion." and aper_asignado=1)
								and p.estado!=3
			and tap.aper_gestion=".$gestion."
			and proy_estado!=4
			and p.tp_id =3
			and (_regiones.reg_id=".$reg_id.")
			order by p.proy_nombre desc
		";
        $query = $this->db->query($query);
        return $query->result_array();
    }
	
	/*
	9. INVERSION PUBLICA ANUAL POR PROVINCIA
	*/
    public function get_reporte9($gestion, $prov_id)
    {
        $query = "
			select prov.prov_id,
						 prov.prov_provincia,
						 count(p.*) as cantidad,
						 fn_nro_proyectos_presup_prov(prov.prov_id,".$gestion.",1) as presup,
						 fn_nro_proyectos_presup_prov_ejecutado(prov.prov_id,".$gestion.",1) as ejecutado
			from _proyectos as p
				 Inner Join _proyectosprovincias as pp On p.proy_id=pp.proy_id
					 Inner Join _provincias as prov on pp.prov_id= prov.prov_id
				 Inner Join (select apy.*,apg.* from aperturaprogramatica as apg, aperturaproyectos as apy where apy.aper_id=apg.aper_id) as tap On p.proy_id=tap.proy_id
				 Inner Join (select pf.proy_id,pf.uni_ejec,u.uni_unidad as ue from _proyectofuncionario pf Inner Join unidadorganizacional as u On pf.uni_ejec=u.uni_id where pf.pfun_tp=1) as fu On fu.proy_id=p.proy_id
					 where tap.aper_programa in (select aper_programa from aperturaprogramatica where aper_proyecto='0000'and aper_actividad='000' and aper_gestion=".$gestion." and aper_asignado=1)
								and p.estado!=3
			and tap.aper_gestion=".$gestion."
			and proy_estado!=4
			and p.tp_id =1
			and (prov.prov_id=".$prov_id." or ".$prov_id."=-1)
				group by prov.prov_id,prov_provincia
				order by prov_provincia
		";
        $query = $this->db->query($query);
        return $query->result_array();
    }
    public function get_reporte9_detalle($gestion, $prov_id){
        $query = "
        select prov.prov_id,
            prov.prov_provincia,p.proy_nombre as proyecto,
			p.proy_sisin,
			p.proy_gestion_inicio_ddmmaaaa,
			p.proy_gestion_fin_ddmmaaaa,
			p.proy_poblac_beneficiaria
			from _proyectos as p
				 Inner Join _proyectosprovincias as pp On p.proy_id=pp.proy_id
					 Inner Join _provincias as prov on pp.prov_id= prov.prov_id
				 Inner Join (select apy.*,apg.* from aperturaprogramatica as apg, aperturaproyectos as apy where apy.aper_id=apg.aper_id) as tap On p.proy_id=tap.proy_id
				 Inner Join (select pf.proy_id,pf.uni_ejec,u.uni_unidad as ue from _proyectofuncionario pf Inner Join unidadorganizacional as u On pf.uni_ejec=u.uni_id where pf.pfun_tp=1) as fu On fu.proy_id=p.proy_id
					 where tap.aper_programa in (select aper_programa from aperturaprogramatica where aper_proyecto='0000'and aper_actividad='000' and aper_gestion=".$gestion." and aper_asignado=1)
								and p.estado!=3
			and tap.aper_gestion=".$gestion."
			and proy_estado!=4
			and p.tp_id =1
			and (prov.prov_id=".$prov_id.") order by p.proy_nombre";
        $query = $this->db->query($query);
        return $query->result_array();
    }
	/*
	10. INVERSION ANUAL EN PROGRAMAS NO RECURRENTES POR PROVINCIA
	*/
    public function get_reporte10($gestion, $prov_id)
    {
        $query = "
			select prov.prov_id,
						 prov.prov_provincia,
						 count(p.*) as cantidad,
						 fn_nro_proyectos_presup_prov(prov.prov_id,".$gestion.",3) as presup,
						 fn_nro_proyectos_presup_prov_ejecutado(prov.prov_id,".$gestion.",3) as ejecutado
			from _proyectos as p
				 Inner Join _proyectosprovincias as pp On p.proy_id=pp.proy_id
					 Inner Join _provincias as prov on pp.prov_id= prov.prov_id
				 Inner Join (select apy.*,apg.* from aperturaprogramatica as apg, aperturaproyectos as apy where apy.aper_id=apg.aper_id) as tap On p.proy_id=tap.proy_id
				 Inner Join (select pf.proy_id,pf.uni_ejec,u.uni_unidad as ue from _proyectofuncionario pf Inner Join unidadorganizacional as u On pf.uni_ejec=u.uni_id where pf.pfun_tp=1) as fu On fu.proy_id=p.proy_id
					 where tap.aper_programa in (select aper_programa from aperturaprogramatica where aper_proyecto='0000'and aper_actividad='000' and aper_gestion=".$gestion." and aper_asignado=1)
								and p.estado!=3
			and tap.aper_gestion=".$gestion."
			and proy_estado!=4
			and p.tp_id =3
			and (prov.prov_id=".$prov_id." or ".$prov_id."=-1)
				group by prov.prov_id,prov_provincia
				order by prov_provincia
		";
        $query = $this->db->query($query);
        return $query->result_array();
    }
    public function get_reporte10_detalle($gestion, $prov_id){
        $query = "
        select prov.prov_id,
            prov.prov_provincia,p.proy_nombre as proyecto,
			p.proy_sisin,
			p.proy_gestion_inicio_ddmmaaaa,
			p.proy_gestion_fin_ddmmaaaa,
			p.proy_poblac_beneficiaria
			from _proyectos as p
				 Inner Join _proyectosprovincias as pp On p.proy_id=pp.proy_id
					 Inner Join _provincias as prov on pp.prov_id= prov.prov_id
				 Inner Join (select apy.*,apg.* from aperturaprogramatica as apg, aperturaproyectos as apy where apy.aper_id=apg.aper_id) as tap On p.proy_id=tap.proy_id
				 Inner Join (select pf.proy_id,pf.uni_ejec,u.uni_unidad as ue from _proyectofuncionario pf Inner Join unidadorganizacional as u On pf.uni_ejec=u.uni_id where pf.pfun_tp=1) as fu On fu.proy_id=p.proy_id
					 where tap.aper_programa in (select aper_programa from aperturaprogramatica where aper_proyecto='0000'and aper_actividad='000' and aper_gestion=".$gestion." and aper_asignado=1)
								and p.estado!=3
			and tap.aper_gestion=".$gestion."
			and proy_estado!=4
			and p.tp_id =3
			and (prov.prov_id=".$prov_id.") order by p.proy_nombre";
        $query = $this->db->query($query);
        return $query->result_array();
    }
	/*
	11. INVERSION PUBLICA ANUAL POR MUNICIPIO
	*/
    public function get_reporte11($gestion, $mun_id)
    {
        $query = "
			select mun.muni_id,
						 mun.muni_municipio,
						 count(p.*) as cantidad,
						 fn_nro_proyectos_presup_muni(mun.muni_id,".$gestion.",1) as presup,
						 fn_nro_proyectos_presup_muni_ejecutado(mun.muni_id,".$gestion.",1) as ejecutado
			from _proyectos as p
				 Inner Join _proyectosmunicipios as pmun On p.proy_id=pmun.proy_id
					 Inner Join _municipios as mun on pmun.muni_id= mun.muni_id
				 Inner Join (select apy.*,apg.* from aperturaprogramatica as apg, aperturaproyectos as apy where apy.aper_id=apg.aper_id) as tap On p.proy_id=tap.proy_id
				 Inner Join (select pf.proy_id,pf.uni_ejec,u.uni_unidad as ue from _proyectofuncionario pf Inner Join unidadorganizacional as u On pf.uni_ejec=u.uni_id where pf.pfun_tp=1) as fu On fu.proy_id=p.proy_id
					 where tap.aper_programa in (select aper_programa from aperturaprogramatica where aper_proyecto='0000'and aper_actividad='000' and aper_gestion=".$gestion." and aper_asignado=1)
								and p.estado!=3
			and tap.aper_gestion=".$gestion."
			and proy_estado!=4
			and p.tp_id =1
			and (mun.muni_id=".$mun_id." or ".$mun_id."=-1)
				group by mun.muni_id,muni_municipio
				order by muni_municipio
		";
        $query = $this->db->query($query);
        return $query->result_array();
    }
	public function get_reporte11_detalle($gestion,$mun_id){
        $query = "
			select mun.muni_id,
			mun.muni_municipio,
			p.proy_nombre as proyecto,
			p.proy_sisin,
			p.proy_gestion_inicio_ddmmaaaa,
			p.proy_gestion_fin_ddmmaaaa,
			p.proy_poblac_beneficiaria
			from _proyectos as p
				 Inner Join _proyectosmunicipios as pmun On p.proy_id=pmun.proy_id
					 Inner Join _municipios as mun on pmun.muni_id= mun.muni_id
				 Inner Join (select apy.*,apg.* from aperturaprogramatica as apg, aperturaproyectos as apy where apy.aper_id=apg.aper_id) as tap On p.proy_id=tap.proy_id
				 Inner Join (select pf.proy_id,pf.uni_ejec,u.uni_unidad as ue from _proyectofuncionario pf Inner Join unidadorganizacional as u On pf.uni_ejec=u.uni_id where pf.pfun_tp=1) as fu On fu.proy_id=p.proy_id
					 where tap.aper_programa in (select aper_programa from aperturaprogramatica where aper_proyecto='0000'and aper_actividad='000' and aper_gestion=".$gestion." and aper_asignado=1)
								and p.estado!=3
			and tap.aper_gestion=".$gestion."
			and proy_estado!=4
			and p.tp_id =1
			and (mun.muni_id=".$mun_id.")
				order by p.proy_nombre
		";
        $query = $this->db->query($query);
        return $query->result_array();
    }
	/*
	12. INVERSION EN PROGRAMAS NO RECURRENTES POR MUNICIPIO
	*/
    public function get_reporte12($gestion, $mun_id)
    {
        $query = "
			select mun.muni_id,
						 mun.muni_municipio,
						 count(p.*) as cantidad,
						 fn_nro_proyectos_presup_prov(mun.muni_id,".$gestion.",3) as presup
			from _proyectos as p
				 Inner Join _proyectosmunicipios as pmun On p.proy_id=pmun.proy_id
					 Inner Join _municipios as mun on pmun.muni_id= mun.muni_id
				 Inner Join (select apy.*,apg.* from aperturaprogramatica as apg, aperturaproyectos as apy where apy.aper_id=apg.aper_id) as tap On p.proy_id=tap.proy_id
				 Inner Join (select pf.proy_id,pf.uni_ejec,u.uni_unidad as ue from _proyectofuncionario pf Inner Join unidadorganizacional as u On pf.uni_ejec=u.uni_id where pf.pfun_tp=1) as fu On fu.proy_id=p.proy_id
					 where tap.aper_programa in (select aper_programa from aperturaprogramatica where aper_proyecto='0000'and aper_actividad='000' and aper_gestion=".$gestion." and aper_asignado=1)
								and p.estado!=3
			and tap.aper_gestion=".$gestion."
			and proy_estado!=4
			and p.tp_id =3
			and (mun.muni_id=".$mun_id." or ".$mun_id."=-1)
				group by mun.muni_id,muni_municipio
				order by muni_municipio
		";
        $query = $this->db->query($query);
        return $query->result_array();
    }
    public function get_reporte12_detalle($gestion, $mun_id){
        $query = "select mun.muni_id,
			mun.muni_municipio,
			p.proy_nombre as proyecto,
			p.proy_sisin,
			p.proy_gestion_inicio_ddmmaaaa,
			p.proy_gestion_fin_ddmmaaaa,
			p.proy_poblac_beneficiaria
			from _proyectos as p
				 Inner Join _proyectosmunicipios as pmun On p.proy_id=pmun.proy_id
					 Inner Join _municipios as mun on pmun.muni_id= mun.muni_id
				 Inner Join (select apy.*,apg.* from aperturaprogramatica as apg, aperturaproyectos as apy where apy.aper_id=apg.aper_id) as tap On p.proy_id=tap.proy_id
				 Inner Join (select pf.proy_id,pf.uni_ejec,u.uni_unidad as ue from _proyectofuncionario pf Inner Join unidadorganizacional as u On pf.uni_ejec=u.uni_id where pf.pfun_tp=1) as fu On fu.proy_id=p.proy_id
					 where tap.aper_programa in (select aper_programa from aperturaprogramatica where aper_proyecto='0000'and aper_actividad='000' and aper_gestion=".$gestion." and aper_asignado=1)
								and p.estado!=3
			and tap.aper_gestion=".$gestion."
			and proy_estado!=4
			and p.tp_id =3
			and (mun.muni_id=".$mun_id.")
			order by p.proy_nombre
		";
        $query = $this->db->query($query);
        return $query->result_array();
    }
/**Lista de proyectos con porcentaje de cargado de datos**/
    public function list_proyectos($prog,$est_proy,$tpf,$gestion)
    {
        if($this->session->userdata('rol_id')==1) ////// LISTA PARA EL SUPER ADMINISTRADOR
        {
            $sql = 'select tap.*,p.*,tp.*,fu.*,
                    fn_proyectos_registro_dg(p.proy_id,'.$gestion.') as _datos_grales,
                    fn_proyectos_registro_prog_f(p.proy_id,'.$gestion.') as _prog_fis,
                    fn_proyectos_registro_prog_fin(p.proy_id,'.$gestion.') as _prog_fin,
                    fn_proyectos_registro_ejec_fis(p.proy_id,'.$gestion.','.$this->session->userdata("mes").') as _ejec_fis,
                    fn_proyectos_registro_ejec_fin(p.proy_id,'.$gestion.','.$this->session->userdata("mes").') as _ejec_fin
                    from _proyectos as p
                    Inner Join _tipoproyecto as tp On p.tp_id=tp.tp_id
                    Inner Join (select apy.*,apg.* from aperturaprogramatica as apg, aperturaproyectos as apy where apy.aper_id=apg.aper_id) as tap On p.proy_id=tap.proy_id
                    Inner Join (select pf."proy_id",f."fun_id",f."fun_nombre",f."fun_paterno",f."fun_materno",u."uni_unidad" as ue,ur."uni_unidad" as ur
                        from _proyectofuncionario pf
                        Inner Join funcionario as f On pf.fun_id=f.fun_id
                        Inner Join unidadorganizacional as u On pf."uni_ejec"=u."uni_id"
                        Inner Join unidadorganizacional as ur On pf.uni_resp=ur.uni_id
                    where pf.pfun_tp='.$tpf.') as fu On fu.proy_id=p.proy_id
                    where tap.aper_programa=\''.$prog.'\' and p.estado!=\'3\' and tap.aper_gestion='.$this->session->userdata("gestion").' and proy_estado='.$est_proy.' 
                    ORDER BY tap.aper_proyecto,tap.aper_actividad  asc';
        }
        $query = $this->db->query($sql);
        return $query->result_array();
    }
	/**********Numero de proyectos por municipio************/
	public function proyectos_x_municipio($mun_id,$gestion,$mes_id,$sql_in){
        $query = 'select p.*,pf.*,a.*,pe.*,u.*
                from aperturaprogramatica a 
                Inner Join unidadorganizacional as u On a.uni_id = u.uni_id 
                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id 
                Inner Join _proyectos as p On p.proy_id = ap.proy_id 
                Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id 
                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
                Inner Join _tipoproyecto as pt On p.tp_id=pt.tp_id 
                Inner Join _proyectosmunicipios as pm On p.proy_id = pm.proy_id 
                where (a.aper_estado = \'1\' OR a.aper_estado = \'2\') and 
                a.aper_gestion='.$gestion.' and 
                (a.aper_proyecto <>  \'0000\' or a.aper_actividad <>  \'000\') and 
                pe.m_id='.$mes_id.' and 
                p.tp_id '.$sql_in.' and 
                pf.pfec_estado=1 and pm.muni_id='.$mun_id.'
                order by p.proy_id asc';
        $query = $this->db->query($query);
        return $query->result_array();
    }
    public function get_proyectos($gestion,$mes_id,$sql_in){
        $query = 'select p.*
                from aperturaprogramatica a 
                Inner Join unidadorganizacional as u On a.uni_id = u.uni_id 
                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id 
                Inner Join _proyectos as p On p.proy_id = ap.proy_id 
                Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id 
                Inner Join ptto_fase_gestion as pfg On pfg.pfec_id = pf.pfec_id 
                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
                Inner Join _tipoproyecto as pt On p.tp_id=pt.tp_id 
                Inner Join _proyectosmunicipios as pm On p.proy_id = pm.proy_id 
                where (a.aper_estado = \'1\' OR a.aper_estado = \'2\') and 
                a.aper_gestion='.$gestion.' and 
                (a.aper_proyecto <>  \'0000\' or a.aper_actividad <>  \'000\') and 
                pe.m_id='.$mes_id.' and 
                p.tp_id '.$sql_in.' and 
                pf.pfec_estado=1 and 
                pfg.g_id='.$gestion;
        $query = $this->db->query($query);
        return $query->result_array();
    }
    public function municipios_en_proyecto($proy_id,$gestion,$mes_id){
        $query='select count(muni_id) as total_muni
			from (
			select distinct pm.muni_id
            from aperturaprogramatica a 
            Inner Join unidadorganizacional as u On a.uni_id = u.uni_id 
            Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id 
            Inner Join _proyectos as p On p.proy_id = ap.proy_id 
            Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id
            Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
            Inner Join _tipoproyecto as pt On p.tp_id=pt.tp_id 
            Inner Join _proyectosmunicipios as pm On p.proy_id = pm.proy_id 
            where (a.aper_estado = \'1\' OR a.aper_estado = \'2\') and 
            a.aper_gestion='.$gestion.' and 
            (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and 
            pe.m_id='.$mes_id.' and 
            pf.pfec_estado=\'1\' and p.proy_id='.$proy_id.') as _municipios_proy';
        $query = $this->db->query($query);
        return $query->result_array();
    }

    /**********Numero de proyectos por provincia************/
    public function proyectos_x_provincia($prov_id,$gestion,$mes_id,$sql_in){
        $query = 'select p.*,a.*,pe.*,u.*
                from aperturaprogramatica a 
                Inner Join unidadorganizacional as u On a.uni_id = u.uni_id 
                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id 
                Inner Join _proyectos as p On p.proy_id = ap.proy_id 
                Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id 
                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
                Inner Join _tipoproyecto as pt On p.tp_id=pt.tp_id 
                Inner Join _proyectosprovincias as pp On p.proy_id = pp.proy_id 
                where (a.aper_estado = \'1\' OR a.aper_estado = \'2\') and 
                a.aper_gestion='.$gestion.' and 
                (a.aper_proyecto <>  \'0000\' or a.aper_actividad <>  \'000\') and 
                pe.m_id='.$mes_id.' and 
                p.tp_id '.$sql_in.' and 
                pf.pfec_estado=1 and pp.prov_id='.$prov_id.'
                order by p.proy_id asc';
        $query = $this->db->query($query);
        return $query->result_array();
    }
    public function provincias_en_proyecto($proy_id,$gestion,$mes_id){
        $query='select count(prov_id) as total_prov 
				from (select distinct pp.prov_id
            from aperturaprogramatica a 
            Inner Join unidadorganizacional as u On a.uni_id = u.uni_id 
            Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id 
            Inner Join _proyectos as p On p.proy_id = ap.proy_id 
            Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id 
            Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
            Inner Join _tipoproyecto as pt On p.tp_id=pt.tp_id 
            Inner Join _proyectosprovincias as pp On p.proy_id = pp.proy_id 
            where (a.aper_estado = \'1\' OR a.aper_estado = \'2\') and 
            a.aper_gestion='.$gestion.' and 
            (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and 
            pe.m_id='.$mes_id.' and 
            pf.pfec_estado=\'1\' and p.proy_id='.$proy_id.'
            ) as _provincias_proy';
        $query = $this->db->query($query);
        return $query->result_array();
    }

    /**********Numero de proyectos por región************/
    public function proyectos_x_region($reg_id,$gestion,$mes_id,$sql_in){
        $query = 'select distinct p.*,a.*,pe.*,u.*
                from aperturaprogramatica a 
                Inner Join unidadorganizacional as u On a.uni_id = u.uni_id 
                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id 
                Inner Join _proyectos as p On p.proy_id = ap.proy_id 
                Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id 
                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
                Inner Join _tipoproyecto as pt On p.tp_id=pt.tp_id 
                Inner Join _proyectosmunicipios as pm On p.proy_id = pm.proy_id 
                Inner Join _municipios as mun On pm.muni_id = mun.muni_id
                Inner Join _regiones as reg On mun.reg_id=reg.reg_id
                where (a.aper_estado = \'1\' OR a.aper_estado = \'2\') and 
                a.aper_gestion='.$gestion.' and 
                (a.aper_proyecto <>  \'0000\' or a.aper_actividad <>  \'000\') and 
                pe.m_id='.$mes_id.' and 
                p.tp_id '.$sql_in.' and 
                pf.pfec_estado=1 and reg.reg_id='.$reg_id.'
                order by p.tp_id asc';
        $query = $this->db->query($query);
        return $query->result_array();
    }
    public function regiones_en_proyecto($proy_id,$gestion,$mes_id){
        $query='select count(reg_id) as total_muni 
			from (select distinct reg.reg_id
            from aperturaprogramatica a 
            Inner Join unidadorganizacional as u On a.uni_id = u.uni_id 
            Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id 
            Inner Join _proyectos as p On p.proy_id = ap.proy_id 
            Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id 
            Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
            Inner Join _tipoproyecto as pt On p.tp_id=pt.tp_id 
            Inner Join _proyectosmunicipios as pm On p.proy_id = pm.proy_id
            Inner Join _municipios as mun On pm.muni_id = mun.muni_id
            Inner Join _regiones as reg On mun.reg_id=reg.reg_id
            where (a.aper_estado = \'1\' OR a.aper_estado = \'2\') and 
            a.aper_gestion='.$gestion.' and 
            (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and 
            pe.m_id='.$mes_id.' and 
            pf.pfec_estado=\'1\' and p.proy_id='.$proy_id.') as _regiones_proy';
        $query = $this->db->query($query);
        return $query->result_array();
    }
}
	