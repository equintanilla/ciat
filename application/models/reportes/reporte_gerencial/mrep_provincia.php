<?php

//reporte global a nivel provincia
class Mrep_provincia extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    function proyecto_inversion($mes, $gestion)
    {
        $sql = "
            SELECT x.prov_id, x.prov_provincia,
            to_number(COALESCE(string_agg(case when x.fas_id = 2 then to_char(x.cantidad, '99999999')end,''),'0'), '99999999') as cant2,
            to_number(COALESCE(string_agg(case when x.fas_id = 1 then to_char(x.cantidad, '99999999')end,''),'0'), '99999999') as cant1,
            SUM(x.p_vig) AS ppto_vigente, SUM(x.dev) AS devengado
            FROM (
            SELECT t.prov_id, t.prov_provincia, t.fas_id, COUNT(t.proy_id)as cantidad, SUM(t.pem_ppto_vigente) AS p_vig, SUM(t.pem_devengado) AS dev
            FROM (
            SELECT p.prov_id, p.prov_provincia, q.proy_id, r.tp_id, f.fas_id, e.pem_ppto_vigente, e.pem_devengado
            FROM _provincias p
            INNER JOIN _proyectosprovincias q ON p.prov_id = q.prov_id
            INNER JOIN _proyectos r ON q.proy_id = r.proy_id
            INNER JOIN _proyectofaseetapacomponente f ON f.proy_id = r.proy_id
            INNER JOIN proy_ejec_mes e ON f.proy_id = e.proy_id
            WHERE p.prov_estado IN (1,2) AND r.proy_estado IN (1,2) AND r.tp_id = 1 AND (" . $gestion . " BETWEEN r.proy_gestion_inicio AND r.proy_gestion_fin)
            AND f.pfec_estado = 1 AND f.estado IN (1,2) AND f.fas_id IN (1,2)
            AND e.mes_id = " . $mes . " AND e.gestion = " . $gestion . "
            )t
            GROUP BY t.prov_id, t.prov_provincia, t.fas_id
            )x
            GROUP BY x.prov_id, x.prov_provincia
            ORDER BY x.prov_provincia;
        ";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function programa_no_recurrentes($mes, $gestion)
    {
        $sql = "
        SELECT x.prov_id, x.prov_provincia,
        to_number(COALESCE(string_agg(case when x.tp_id = 3 then to_char(x.cantidad, '99999999')end,''),'0'), '99999999') as cant2,
        to_number(COALESCE(string_agg(case when x.tp_id = 2 then to_char(x.cantidad, '99999999')end,''),'0'), '99999999') as cant1,
        SUM(x.p_vig) AS ppto_vigente, SUM(x.dev) AS devengado
        FROM (
        SELECT t.prov_id, t.prov_provincia, t.tp_id, COUNT(t.proy_id)as cantidad, SUM(t.pem_ppto_vigente) AS p_vig, SUM(t.pem_devengado) AS dev
        FROM (
        SELECT p.prov_id, p.prov_provincia, q.proy_id, r.tp_id, e.pem_ppto_vigente, e.pem_devengado
        FROM _provincias p
        INNER JOIN _proyectosprovincias q ON p.prov_id = q.prov_id
        INNER JOIN _proyectos r ON q.proy_id = r.proy_id
        INNER JOIN proy_ejec_mes e ON r.proy_id = e.proy_id
        WHERE p.prov_estado IN (1,2) AND r.proy_estado IN (1,2) AND r.tp_id IN (2,3) AND ($gestion BETWEEN r.proy_gestion_inicio AND r.proy_gestion_fin)
        AND e.mes_id = $mes AND e.gestion = $gestion
        )t
        GROUP BY t.prov_id, t.prov_provincia, t.tp_id
        )x
        GROUP BY x.prov_id, x.prov_provincia
        ORDER BY x.prov_provincia
        ";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    //proyecto de inversion + programa no recurrentes
    function reporte_pi_pnr($mes, $gestion)
    {
          $sql = "
            SELECT x.prov_id, x.prov_provincia,
            to_number(COALESCE(string_agg(case when x.tp_id = 1 then to_char(x.cantidad, '99999999')end,''),'0'), '99999999') as cant2,
            to_number(COALESCE(string_agg(case when x.tp_id = 3 then to_char(x.cantidad, '99999999')end,''),'0'), '99999999') as cant1,
            SUM(x.p_vig) AS ppto_vigente, SUM(x.dev) AS devengado
            FROM (
            SELECT t.prov_id, t.prov_provincia, t.tp_id, COUNT(t.proy_id)as cantidad, SUM(t.pem_ppto_vigente) AS p_vig, SUM(t.pem_devengado) AS dev
            FROM (
            SELECT p.prov_id, p.prov_provincia, q.proy_id, r.tp_id, e.pem_ppto_vigente, e.pem_devengado
            FROM _provincias p
            INNER JOIN _proyectosprovincias q ON p.prov_id = q.prov_id
            INNER JOIN _proyectos r ON q.proy_id = r.proy_id
            INNER JOIN proy_ejec_mes e ON r.proy_id = e.proy_id
            WHERE p.prov_estado IN (1,2) AND r.proy_estado IN (1,2) AND r.tp_id IN (1,3) AND (".$gestion." BETWEEN r.proy_gestion_inicio AND r.proy_gestion_fin)
            AND e.mes_id = ".$mes." AND e.gestion = ".$gestion."
            )t
            GROUP BY t.prov_id, t.prov_provincia, t.tp_id
            )x
            GROUP BY x.prov_id, x.prov_provincia
            ORDER BY x.prov_provincia
          ";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
}