<?php

// reporte global - por municipio
class Mrep_region extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    public function proyecto_inversion($mes_id, $gestion)
    {
        $sql = "
            SELECT x.reg_id, x.reg_region,
            to_number(COALESCE(string_agg(case when x.fas_id = 2 then to_char(x.cantidad, '99999999')end,''),'0'), '99999999') as cantx,
            to_number(COALESCE(string_agg(case when x.fas_id = 1 then to_char(x.cantidad, '99999999')end,''),'0'), '99999999') as canty,
            SUM(x.p_vig) AS ppto_vigente, SUM(x.dev) AS devengado
            FROM (
            SELECT t.reg_id, t.reg_region, t.fas_id, COUNT(t.proy_id)as cantidad, SUM(t.pem_ppto_vigente) AS p_vig, SUM(t.pem_devengado) AS dev
            FROM (
            SELECT r.reg_id, r.reg_region, p.proy_id, f.fas_id, e.pem_ppto_vigente, e.pem_devengado
            FROM _regiones r
            INNER JOIN _municipios m ON r.reg_id = m.reg_id
            INNER JOIN _proyectosmunicipios p ON m.muni_id = p.muni_id
            INNER JOIN _proyectos q ON p.proy_id = q.proy_id
            INNER JOIN _proyectofaseetapacomponente f ON f.proy_id = q.proy_id
            INNER JOIN proy_ejec_mes e ON f.proy_id = e.proy_id
            WHERE r.reg_estado IN (1,2) AND m.muni_estado IN (1,2) AND q.proy_estado IN (1,2) AND q.tp_id = 1
            AND (" . $gestion . " BETWEEN q.proy_gestion_inicio AND q.proy_gestion_fin) AND f.pfec_estado = 1 AND f.estado IN (1,2) AND f.fas_id IN (1,2)
            AND e.mes_id = " . $mes_id . " AND e.gestion = " . $gestion . "
            )t
            GROUP BY t.reg_id, t.reg_region, t.fas_id
            )x
            GROUP BY x.reg_id, x.reg_region
            ORDER BY x.reg_region
        ";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function programa_no_recurrente($mes_id, $gestion)
    {
        $sql = "
            SELECT x.reg_id, x.reg_region,
            to_number(COALESCE(string_agg(case when x.tp_id = 3 then to_char(x.cantidad, '99999999')end,''),'0'), '99999999') as cantx,
            to_number(COALESCE(string_agg(case when x.tp_id = 2 then to_char(x.cantidad, '99999999')end,''),'0'), '99999999') as canty,
            SUM(x.p_vig) AS ppto_vigente, SUM(x.dev) AS devengado
            FROM (
            SELECT t.reg_id, t.reg_region, t.tp_id, COUNT(t.proy_id)as cantidad, SUM(t.pem_ppto_vigente) AS p_vig, SUM(t.pem_devengado) AS dev
            FROM (
            SELECT r.reg_id, r.reg_region, p.proy_id, q.tp_id, e.pem_ppto_vigente, e.pem_devengado
            FROM _regiones r
            INNER JOIN _municipios m ON r.reg_id = m.reg_id
            INNER JOIN _proyectosmunicipios p ON m.muni_id = p.muni_id
            INNER JOIN _proyectos q ON p.proy_id = q.proy_id
            INNER JOIN proy_ejec_mes e ON p.proy_id = e.proy_id
            WHERE r.reg_estado IN (1,2) AND m.muni_estado IN (1,2) AND q.proy_estado IN (1,2) AND q.tp_id IN (2,3)
            AND (" . $gestion . " BETWEEN q.proy_gestion_inicio AND q.proy_gestion_fin) AND e.mes_id = " . $mes_id . " AND e.gestion = " . $gestion . "
            )t
            GROUP BY t.reg_id, t.reg_region, t.tp_id
            )x
            GROUP BY x.reg_id, x.reg_region
            ORDER BY x.reg_region
        ";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    //proyecto de inversion + programa no recurrente
    public function reporte_pi_pnr($mes_id, $gestion)
    {
        $sql = "
                SELECT x.reg_id, x.reg_region,
                to_number(COALESCE(string_agg(case when x.tp_id = 1 then to_char(x.cantidad, '99999999')end,''),'0'), '99999999') as cantx,
                to_number(COALESCE(string_agg(case when x.tp_id = 3 then to_char(x.cantidad, '99999999')end,''),'0'), '99999999') as canty,
                SUM(x.p_vig) AS ppto_vigente, SUM(x.dev) AS devengado
                FROM (
                SELECT t.reg_id, t.reg_region, t.tp_id, COUNT(t.proy_id)as cantidad, SUM(t.pem_ppto_vigente) AS p_vig, SUM(t.pem_devengado) AS dev
                FROM (
                SELECT r.reg_id, r.reg_region, p.proy_id, q.tp_id, e.pem_ppto_vigente, e.pem_devengado
                FROM _regiones r
                INNER JOIN _municipios m ON r.reg_id = m.reg_id
                INNER JOIN _proyectosmunicipios p ON m.muni_id = p.muni_id
                INNER JOIN _proyectos q ON p.proy_id = q.proy_id
                INNER JOIN proy_ejec_mes e ON p.proy_id = e.proy_id
                WHERE r.reg_estado IN (1,2) AND m.muni_estado IN (1,2) AND q.proy_estado IN (1,2) AND q.tp_id IN (1,3)
                AND (" . $gestion . " BETWEEN q.proy_gestion_inicio AND q.proy_gestion_fin) AND e.mes_id = " . $mes_id . " AND e.gestion = " . $gestion . "
                )t
                GROUP BY t.reg_id, t.reg_region, t.tp_id
                )x
                GROUP BY x.reg_id, x.reg_region
                ORDER BY x.reg_region
          ";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
}