<?php

//reporte global - nivel estado de proyectos
class Mestado_proyectos extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    //por tipo de proyecto
    function tipo_proyecto_estado($mes, $gestion, $tipo_proy)
    {
        $sql = '
            SELECT t.ep_id, t.ep_descripcion, COUNT(t.proy_id) AS nro_proy, SUM(t.p_ini) AS ppto_inicial, SUM(t.modi) AS modif_aprobadas,
            SUM(t.p_vig) AS ppto_vigente, SUM(t.p_ejec) AS ppto_ejecutado
            FROM(
                SELECT p.proy_id, p.ep_id, e.ep_descripcion,SUM(pe.pem_ppto_inicial) AS p_ini, SUM(pe.pem_modif_aprobadas) AS modi,
              SUM(pe.pem_ppto_vigente) AS p_vig, SUM(pe.pem_devengado) as p_ejec
              FROM _proyectos p
              INNER JOIN _estadoproyecto e ON p.ep_id = e.ep_id
              INNER JOIN proy_ejec_mes pe ON p.proy_id = pe.proy_id
              WHERE	p.estado IN (1, 2) AND p.tp_id = ' . $tipo_proy . ' AND (' . $gestion . ' BETWEEN P .proy_gestion_inicio AND P .proy_gestion_fin)
              AND pe.mes_id = ' . $mes . ' AND pe.gestion = ' . $gestion . '
              GROUP BY p.proy_id, p.proy_codigo, p.proy_nombre, p.ep_id, e.ep_descripcion
            )t
            GROUP BY t.ep_id, t.ep_descripcion
            ORDER BY t.ep_descripcion
         ';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    //proyecto de inversion  + programa no recurrente
    function proy_inv_prog_norec($mes, $gestion)
    {
        $sql = '
            SELECT t.ep_id, t.ep_descripcion, COUNT(t.proy_id) AS nro_proy, SUM(t.p_ini) AS ppto_inicial, SUM(t.modi) AS modif_aprobadas,
            SUM(t.p_vig) AS ppto_vigente, SUM(t.p_ejec) AS ppto_ejecutado
            FROM(
                SELECT p.proy_id, p.ep_id, e.ep_descripcion,SUM(pe.pem_ppto_inicial) AS p_ini, SUM(pe.pem_modif_aprobadas) AS modi,
              SUM(pe.pem_ppto_vigente) AS p_vig, SUM(pe.pem_devengado) as p_ejec
              FROM _proyectos p
              INNER JOIN _estadoproyecto e ON p.ep_id = e.ep_id
              INNER JOIN proy_ejec_mes pe ON p.proy_id = pe.proy_id
              WHERE	p.estado IN (1, 2) AND p.tp_id IN (1,3) AND (' . $gestion . ' BETWEEN P .proy_gestion_inicio AND P .proy_gestion_fin)
              AND pe.mes_id = ' . $mes . ' AND pe.gestion = ' . $gestion . '
              GROUP BY p.proy_id, p.proy_codigo, p.proy_nombre, p.ep_id, e.ep_descripcion
            )t
            GROUP BY t.ep_id, t.ep_descripcion
            ORDER BY t.ep_descripcion
         ';
        $query = $this->db->query($sql);
        return $query->result_array();
    }
}