<?php

//reporte globsl - nivel municipio
class Mrep_municipio extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    function proyecto_inversion($mes_id, $gestion)
    {
        $sql = "
            SELECT x.muni_id, x.muni_municipio,
            to_number(COALESCE(string_agg(case when x.fas_id = 2 then to_char(x.cantidad, '99999999')end,''),'0'), '99999999') as cantx,
            to_number(COALESCE(string_agg(case when x.fas_id = 1 then to_char(x.cantidad, '99999999')end,''),'0'), '99999999') as canty,
            SUM(x.p_ini) AS ppto_inicial, SUM(x.modi) AS modif_aprobadas, SUM(x.p_vig) AS ppto_vigente, SUM(x.dev) AS devengado
            FROM (
            SELECT t.muni_id, t.muni_municipio, t.fas_id, COUNT(t.proy_id)as cantidad, SUM(pem_ppto_inicial) AS p_ini, SUM(pem_modif_aprobadas) AS modi,
            SUM(t.pem_ppto_vigente) AS p_vig, SUM(t.pem_devengado) AS dev
            FROM (
            SELECT m.muni_id, m.muni_municipio, p.proy_id, r.tp_id, f.fas_id, e.pem_ppto_vigente, e.pem_devengado, e.pem_ppto_inicial, e.pem_modif_aprobadas
            FROM _municipios m
            INNER JOIN _proyectosmunicipios p ON m.muni_id = p.muni_id
            INNER JOIN _proyectos r ON p.proy_id = r.proy_id
            INNER JOIN _proyectofaseetapacomponente f ON f.proy_id = r.proy_id
            INNER JOIN proy_ejec_mes e ON f.proy_id = e.proy_id
            WHERE m.muni_estado IN (1,2) AND r.proy_estado IN (1,2) AND r.tp_id = 1 AND (" . $gestion . " BETWEEN r.proy_gestion_inicio AND r.proy_gestion_fin)
            AND f.pfec_estado = 1 AND f.estado IN (1,2) AND f.fas_id IN (1,2)
            AND e.mes_id = " . $mes_id . " AND e.gestion = " . $gestion . "
            )t
            GROUP BY t.muni_id, t.muni_municipio, t.fas_id
            )x
            GROUP BY x.muni_id, x.muni_municipio
            ORDER BY x.muni_municipio
        ";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function prog_no_recurrente($mes_id, $gestion)
    {
        $sql = "
            SELECT x.muni_id, x.muni_municipio,
            to_number(COALESCE(string_agg(case when x.tp_id = 3 then to_char(x.cantidad, '99999999')end,''),'0'), '99999999') as cantx,
            to_number(COALESCE(string_agg(case when x.tp_id = 2 then to_char(x.cantidad, '99999999')end,''),'0'), '99999999') as canty,
            SUM(x.p_ini) AS ppto_inicial, SUM(x.modi) AS modif_aprobadas, SUM(x.p_vig) AS ppto_vigente, SUM(x.dev) AS devengado
            FROM (
            SELECT t.muni_id, t.muni_municipio, t.tp_id, COUNT(t.proy_id)as cantidad, SUM(pem_ppto_inicial) AS p_ini, SUM(pem_modif_aprobadas) AS modi,
            SUM(t.pem_ppto_vigente) AS p_vig, SUM(t.pem_devengado) AS dev
            FROM (
            SELECT m.muni_id, m.muni_municipio, p.proy_id, r.tp_id, e.pem_ppto_inicial, e.pem_modif_aprobadas,e.pem_ppto_vigente, e.pem_devengado
            FROM _municipios m
            INNER JOIN _proyectosmunicipios p ON m.muni_id = p.muni_id
            INNER JOIN _proyectos r ON p.proy_id = r.proy_id
            INNER JOIN proy_ejec_mes e ON r.proy_id = e.proy_id
            WHERE m.muni_estado IN (1,2) AND r.proy_estado IN (1,2) AND r.tp_id IN (2,3) AND (" . $gestion . " BETWEEN r.proy_gestion_inicio AND r.proy_gestion_fin)
            AND e.mes_id = " . $mes_id . " AND e.gestion = " . $gestion . "
            )t
            GROUP BY t.muni_id, t.muni_municipio, t.tp_id
            )x
            GROUP BY x.muni_id, x.muni_municipio
            ORDER BY x.muni_municipio
        ";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    //reporte de proyecto de inversion + programa no recurrente
    function reporte_pi_pnr($mes_id, $gestion)
    {
        $sql = "
        SELECT x.muni_id, x.muni_municipio,
        to_number(COALESCE(string_agg(case when x.tp_id = 3 then to_char(x.cantidad, '99999999')end,''),'0'), '99999999') as cantx,
        to_number(COALESCE(string_agg(case when x.tp_id = 2 then to_char(x.cantidad, '99999999')end,''),'0'), '99999999') as canty,
        SUM(x.p_ini) AS ppto_inicial, SUM(x.modi) AS modif_aprobadas, SUM(x.p_vig) AS ppto_vigente, SUM(x.dev) AS devengado
        FROM (
        SELECT t.muni_id, t.muni_municipio, t.tp_id, COUNT(t.proy_id)as cantidad, SUM(pem_ppto_inicial) AS p_ini, SUM(pem_modif_aprobadas) AS modi,
        SUM(t.pem_ppto_vigente) AS p_vig, SUM(t.pem_devengado) AS dev
        FROM (
        SELECT m.muni_id, m.muni_municipio, p.proy_id, r.tp_id, e.pem_ppto_inicial, e.pem_modif_aprobadas,e.pem_ppto_vigente, e.pem_devengado
        FROM _municipios m
        INNER JOIN _proyectosmunicipios p ON m.muni_id = p.muni_id
        INNER JOIN _proyectos r ON p.proy_id = r.proy_id
        INNER JOIN proy_ejec_mes e ON r.proy_id = e.proy_id
        WHERE m.muni_estado IN (1,2) AND r.proy_estado IN (1,2) AND r.tp_id IN (1,3) AND (".$gestion." BETWEEN r.proy_gestion_inicio AND r.proy_gestion_fin)
        AND e.mes_id = ".$mes_id." AND e.gestion = ".$gestion."
        )t
        GROUP BY t.muni_id, t.muni_municipio, t.tp_id
        )x
        GROUP BY x.muni_id, x.muni_municipio
        ORDER BY x.muni_municipio
        ";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
}