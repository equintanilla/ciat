<?php

//modelo de eje programatica
class Meje_programatica extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    //reporte por tipo de proyecto por eje programatica
    function reporte_proy_ejeprog($mes, $gestion, $tipo_proy)
    {
        $sql = '
        SELECT t.ptdi, x.ptdi_codigo, x.ptdi_descripcion, COUNT(t.proy_id) AS nro_proy, SUM(t.p_ini) AS ppto_inicial, SUM(t.modi) AS modif_aprobadas,
        SUM(t.p_vig) AS ppto_vigente, SUM(t.ejec) AS ppto_ejecutado
        FROM(
              SELECT p.proy_id, p.tp_id, COALESCE((SELECT get_primer_caracter(p.ptdi_id ,' . $gestion . ' )),0) AS ptdi,
              SUM(e.pem_ppto_inicial) AS p_ini, SUM(e.pem_ppto_vigente) AS p_vig, SUM(e.pem_modif_aprobadas) AS modi, SUM(e.pem_devengado) AS ejec
              FROM _proyectos p
              INNER JOIN proy_ejec_mes e ON p.proy_id = e.proy_id
              WHERE p.tp_id = ' . $tipo_proy . ' AND p.proy_estado IN (1,2) AND (' . $gestion . ' BETWEEN p.proy_gestion_inicio AND p.proy_gestion_fin)
              AND e.mes_id = ' . $mes . ' AND e.gestion = ' . $gestion . '
              GROUP BY p.proy_id, p.tp_id, ptdi
        )t
        INNER JOIN ptdi x ON t.ptdi = x.ptdi_id
        GROUP BY t.ptdi, x.ptdi_codigo, x.ptdi_descripcion
        ORDER BY x.ptdi_codigo
        ';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function proy_inv_prog_norec($mes, $gestion)
    {
        $sql = '
        SELECT t.ptdi, x.ptdi_codigo, x.ptdi_descripcion, COUNT(t.proy_id) AS nro_proy, SUM(t.p_ini) AS ppto_inicial, SUM(t.modi) AS modif_aprobadas,
        SUM(t.p_vig) AS ppto_vigente, SUM(t.ejec) AS ppto_ejecutado
        FROM(
              SELECT p.proy_id, p.tp_id, COALESCE((SELECT get_primer_caracter(p.ptdi_id ,' . $gestion . ' )),0) AS ptdi,
              SUM(e.pem_ppto_inicial) AS p_ini, SUM(e.pem_ppto_vigente) AS p_vig, SUM(e.pem_modif_aprobadas) AS modi, SUM(e.pem_devengado) AS ejec
              FROM _proyectos p
              INNER JOIN proy_ejec_mes e ON p.proy_id = e.proy_id
              WHERE p.tp_id in (1,3) AND p.proy_estado IN (1,2) AND (' . $gestion . ' BETWEEN p.proy_gestion_inicio AND p.proy_gestion_fin)
              AND e.mes_id = ' . $mes . ' AND e.gestion = ' . $gestion . '
              GROUP BY p.proy_id, p.tp_id, ptdi
        )t
        INNER JOIN ptdi x ON t.ptdi = x.ptdi_id
        GROUP BY t.ptdi, x.ptdi_codigo, x.ptdi_descripcion
        ORDER BY x.ptdi_codigo
        ';
        $query = $this->db->query($sql);
        return $query->result_array();
    }


}