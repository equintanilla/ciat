<?php
class model_pei extends CI_Model {
  
    /**
    * Responsable for auto load the database
    * @return void
    */
    public function __construct()
    {
        $this->load->database();
    }
   
   

    /////FUNCION PARA EL REGISTRO DE LOS USUARIOS
    function pei_mision_get()
    {
        $query=$this->db->query('SELECT * FROM "public"."configuracion" AS BEN
                        WHERE BEN."conf_estado"=\'1\'
                           ');
        //$query=$query->row_array();
        return $query->row_array();
    }
    /////FUNCION PARA EL REGISTRO DE LOS USUARIOS
    function pei_mision_edit()
    {
         
       $opts = array(
            'conf_mision' => $this->input->post('mision'),
            );
        $this->db->where('conf_estado',1);
        $this->db->update('public.configuracion',$opts);

    }
    /////FUNCION PARA EL REGISTRO DE LOS USUARIOS
    function pei_vision_get()
    {
        $query=$this->db->query('SELECT * FROM "public"."configuracion" AS BEN
                        WHERE BEN."conf_estado"=\'1\'
                           ');
        //$query=$query->row_array();
        return $query->row_array();
    }
    /////FUNCION PARA EL REGISTRO DE LOS USUARIOS
    function pei_vision_edit()
    {
         
       $opts = array(
            'conf_vision' => $this->input->post('mision'),
            );
        $this->db->where('conf_estado',1);
        $this->db->update('public.configuracion',$opts);

    }

    public function historial_vision($ide){
         $sql = 'select *
                 from historial_vision v
                 Inner Join funcionario as f On v.fun_id=f.fun_id
                 where v.ide='.$ide.'
                 order by v.hv_id asc';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function historial_mision($ide){
         $sql = 'select *
                 from historial_mision m
                 Inner Join funcionario as f On m.fun_id=f.fun_id
                 where m.ide='.$ide.'
                 order by m.hm_id asc';
        $query = $this->db->query($sql);
        return $query->result_array();
    }
}
?>  
