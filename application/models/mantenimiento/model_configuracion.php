<?php
class Model_configuracion extends CI_Model {
    /**
    * Responsable for auto load the database
    * @return void
    */
    public function __construct()
    {
        $this->load->database();
    }

    public function get_configuracion()
    {
         $sql = 'select * 
         from configuracion c
         Inner Join mes as m On m.m_id=c.conf_mes
         where c.conf_estado=\'1\'';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function get_mes()
    {
         $sql = 'select * from mes';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

        public function get_gestion()
    {
         $sql = 'select *
                from gestion
                where g_id!=\'0\'';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function verifica_gestion($ide)
    {
        $query = "SELECT count(*) n
        FROM gestion
        WHERE g_id = $ide";
        $query = $this->db->query($query);
        $query = $query->row();
        $bool = ($query->n >= 1) ? true : false ;
        return $bool;
    }
    public function activa_funcionario($ide)
    {
        if(!$this->verifica_gestion($ide)){
            $this->db->insert('gestion', array(
                'g_id' => $ide,
                'g_descripcion' => $ide
            ));
        }
        $update = array(
            'estado' => 1
        );
        $this->db->where('ide', $ide);
        return $this->db->update('configuracion', $update);
    }
    public function desactiva_funcionario($ide)
    {
        $update = array(
            'estado' => 0
        );
        $this->db->where('ide', $ide);
        return $this->db->update('configuracion', $update);
    }
    public function lista_gestion()
    {
        $sql = 'SELECT *FROM configuracion where estado = 1 order by ide';
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    public function get_gestion_todo()
    {
        $sql = 'SELECT * from configuracion order by ide desc';
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    public function act_ges($ide)
    {
        $q = "SELECT * FROM configuracion WHERE conf_estado = 1";
        $q = $this->db->query($q);
        $q = $q->row();
        $mes = $q->conf_mes;
        $sql = 'UPDATE public.configuracion
                SET conf_estado = 0';
        $query = $this->db->query($sql);
        $update = array(
            'conf_estado' => 1,
            'conf_mes' => $mes
        );
        $this->db->where('ide', $ide);
        $this->db->update('configuracion', $update);
        // $sql = 'UPDATE public.configuracion SET conf_estado = 1, conf_mes = $mes WHERE ide='.$ide.'';
        // $query = $this->db->query($sql);
        redirect('Configuracion');
    }
    public function act_ges_mes($conf_mes)
    {
        // $sql = 'UPDATE public.configuracion SET conf_mes=0';
        // $query = $this->db->query($sql);
        $sql = 'UPDATE public.configuracion
               SET conf_mes='.$conf_mes.'
               WHERE conf_estado = 1';
        $query = $this->db->query($sql);
        redirect('Configuracion');
    }
    public function gestion_actual()
    {
       $sql = 'SELECT ide, conf_mes from configuracion
                where conf_estado=1';
        $query = $this->db->query($sql);
        return $query->result_array(); 
    }
}
?>  
