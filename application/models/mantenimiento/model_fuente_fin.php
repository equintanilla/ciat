<?php
class model_fuente_fin extends CI_Model {
    /**
    * Responsable for auto load the database
    * @return void
    */
    public function __construct()
    {
        $this->load->database();
    }
    public function lista_fuente_fin()
    {
        $sql = 'SELECT *from fuentefinanciamiento where ff_estado=1 order by ff_id';
        $query = $this->db->query($sql);
        return $query->result_array();
        redirect('partidas');
    } 
    function verificar_ffcod($cod,$gestion){
        //empezamos una transacciÃ³n
        $this->db->trans_begin();
        $this->db->WHERE('ff_codigo',$cod);
        $this->db->WHERE('ff_estado',1);
        $this->db->WHERE('ff_gestion',$gestion);
        $this->db->FROM('fuentefinanciamiento');
        $query = $this->db->get();
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }else{
            //correcto
            $this->db->trans_commit();
            return $query->result_array();
        }
    }
     function mod_ff($ffid,$ffdescripcion,$ffsigla,$ffgestion){
        $ffdescripcion = strtoupper($ffdescripcion);
        $ffsigla = strtoupper($ffsigla);
        $sql = "UPDATE fuentefinanciamiento SET ff_descripcion='".$ffdescripcion."',ff_sigla='".$ffsigla."',ff_gestion=".$ffgestion." WHERE ff_id=".$ffid;
        $this->db->query($sql);
    }
    function add_ff($ffdescripcion,$ffsigla,$ffcodigo,$ffgestion){
        $id_antes = $this->generar_id('fuentefinanciamiento','ff_id');
        $nuevo_id = $id_antes[0]['id_antes'];
        $nuevo_id++;
        $data = array(
            'ff_id' => $nuevo_id,
            'ff_descripcion' => strtoupper( $ffdescripcion),
            'ff_sigla' => strtoupper($ffsigla),
            'ff_estado' => 1,
            'ff_codigo' => $ffcodigo,
            'ff_gestion' => $ffgestion,
        );
        $this->db->insert('fuentefinanciamiento',$data);
    }
    function dato_ff($id)
    {
        $this->db->WHERE('ff_id',$id);
        $this->db->from('fuentefinanciamiento');
        $query = $this->db->get();
        return $query->result_array();
    }
    function generar_id($tabla,$id){
        $query =$this->db->query('SELECT MAX('.$id.') AS id_antes FROM '.$tabla);
        return $query->result_array();
       // return $query->row_array();
    }

    
}
?>  
