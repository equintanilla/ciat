<?php

class Mpoa extends CI_Model
{
    var $gestion;

    public function __construct()
    {
        $this->load->database();
        $this->gestion = $this->session->userData('gestion');
    }

    //=======================================  POA  =================================================
    function lista_poa()
    {
        //FILTRADO POR GESTION DESDE POSTGRES
        $this->db->SELECT('*');
        $this->db->FROM('vista_poa');
        $this->db->WHERE('poa_gestion', $this->gestion);
        $this->db->ORDER_BY("aper_programa,aper_proyecto,aper_actividad", "ASC");
        $query = $this->db->get();
        return $query->result_array();
    }

    //GUARDAR POA Y ACTUALIZAR EL ESTADO DE ASIGNADO DE LA APERTURA PROGRAMATICA
    function guardar_poa($aper_id, $poa_fecha)
    {
        $fecha = $this->edita_fecha($poa_fecha);
        $fun_id = $this->session->userData('id_usuario');
        $gestion = $this->gestion;
        //EMPEZAR TRANSACCION
        $this->db->trans_begin();
        //-----------crear codigo --------------------------
        $cont = $this->get_cont();
        $cont = $cont[0]['conf_poa'];
        $cont++;
        $codigo = 'SIIP/POA/' . $this->gestion . '/00' . $cont;
        //--------- fin crear codigo -----------------------
        $data = array(
            'poa_codigo' => $codigo,
            'aper_id' => $aper_id,
            'poa_fecha_creacion' => $fecha,
            'poa_gestion' => $gestion,
            'fun_id' => $fun_id
        );
        $this->db->insert('poa', $data);
        //-----------actualizar mi conf poa mi contador de la tabla configuracion
        $this->actualizar_confpoa($cont, $gestion);
        //----------- actualizar el estado de asignado de mi apertura
        $this->asignar_aper($aper_id);
        //comprobamos si se han llevado a cabo correctamente todas
        //las consultas
        if ($this->db->trans_status() === FALSE) {
            //si ha habido algún error lo debemos mostrar aquí
            $this->db->trans_rollback();

        } else {
            //en otro caso todo ha ido bien
            return $this->db->trans_commit();
        }
    }

    //CONVERTIR LA FECHA DE ENTRADA EN FECHA POSTGRES
    function edita_fecha($fecha)
    {
        $fecha = str_replace("/", "-", $fecha);
        $vec = explode("-", $fecha);
        $fecha = $vec[2] . "-" . $vec[1] . "-" . $vec[0];
        return $fecha;
    }

    // OBTIENE EL CONTADOR DE CODIGO POA DE LA TABLA CONFIGURACION
    function get_cont()
    {
        $this->db->SELECT('*');
        $this->db->FROM('configuracion');
        $this->db->WHERE('ide',$this->gestion);
        $query = $this->db->get();
        return $query->result_array();
    }

    //ACTUALIZA EL CONTADOR DE MI CODIGO POA DE LA TABLA CONFIGURACION
    function actualizar_confpoa($cont, $gestion)
    {
        $data = array(
            'conf_poa' => $cont,
        );
        $this->db->WHERE('ide', $gestion);
        $this->db->UPDATE('configuracion ', $data);
    }

    //CAMBIAR EL ESTADO DE ASIGNADO DE MI PAERTURA
    function asignar_aper($aper_id)
    {
        $data = array(
            'aper_asignado' => 1,
        );
        $this->db->WHERE("aper_id", $aper_id);
        $this->db->UPDATE('aperturaprogramatica', $data);
    }

    //FUNCION QUE RETORNA DATOS DEL POA FILTRADO POR ID, gestion
    function dato_poa($poa_id, $gestion)
    {
        $this->db->SELECT('*');
        $this->db->FROM('vista_poa');
        $this->db->WHERE('poa_id', $poa_id);
        $this->db->WHERE('poa_gestion', $gestion);
        $query = $this->db->get();
        return $query->result_array();
    }

    //FUNCION QUE RETORNA DATOS DEL POA FILTRADO POR ID
    function dato_poa_id($poa_id)
    {
        $this->db->SELECT('*');
        $this->db->FROM('vista_poa');
        $this->db->WHERE('poa_id', $poa_id);
        $query = $this->db->get();
        return $query->result_array();
    }

    //MODIFICAR MI CARPETA POA
    function modificar_poa($poa_id, $fecha)
    {
        $fecha = $this->edita_fecha($fecha);
        $data = array(
            'poa_fecha_creacion' => $fecha,
            'poa_estado' => 2
        );
        $this->db->WHERE("poa_id", $poa_id);
        return $this->db->UPDATE('poa', $data);
    }

    //ELIMINAR CARPETA (CAMBIAR DE ESTADO 3)
    function eliminar_poa($poa_id, $gestion)
    {
        $this->db->trans_begin();

        //============  ELIMINAR POA cambiar estado y poner a 3 la apertura prog
        //VERIFICAR SI CAMBIAR APER ID = 0
        $sql1 = 'UPDATE poa SET poa_estado = 3 WHERE poa_id = ' . $poa_id;
        //===================== MODIFICAR APERTURA PROGRAMATICA ========================
        $aper_id = $this->dato_poa($poa_id, $gestion);
        $aper_id = $aper_id[0]['aper_id'];
        $sql2 = 'UPDATE aperturaprogramatica SET aper_asignado= 0 WHERE aper_id=' . $aper_id;
        //================== ELIMINAR LAS RELACIONES EN objetivosestrategicos
        $sql3 = 'DELETE FROM objetivosestrategicos WHERE obje_id in(select obje_id from poaobjetivosestrategicos where poa_id='.$poa_id.')';
        $this->db->query($sql1);
        $this->db->query($sql2);
        $this->db->query($sql3);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
            return $this->db->trans_commit();
        }
    }

    //LISTA DE OBJETIVOS ESTRATEGICOS ASIGNADOS AL POA
    function get_list_objetivos_estrategicos($poa)
    {
        $sql = "select tmp.*
               from (select p.obje_id AS asignar,o.* FROM  v_objetivos_estrategicos o
               LEFT JOIN poaobjetivosestrategicos p ON o.obje_id = p.obje_id AND p.poa_id = " . $poa . ")tmp
               where " . $this->gestion. " between tmp.obje_gestion_curso and (tmp.obje_gestion_curso+5) ORDER BY tmp.obje_id ASC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    //ASIGNAR OBJETIVOS ESTRATEGICO
    function asignar_obj($poa_id, $obj_id)
    {
        $data = array(
            'poa_id' => $poa_id,
            'obje_id' => $obj_id,
        );
        $this->db->insert('poaobjetivosestrategicos', $data);
    }

    //QUITAR OBJETIVO ESTRATEGICO
    function quitar_obj($poa_id, $obj_id)
    {
        $this->db->where('poa_id', $poa_id);
        $this->db->where('obje_id', $obj_id);
        $this->db->delete('poaobjetivosestrategicos');
    }

    function verificar_objetivo_estartegico($obje_id,$aper_id)
    {
        $this->db->FROM('objetivosgestion');
        $this->db->WHERE('obje_id', $obje_id);
        $this->db->WHERE('aper_id', $aper_id);
        $this->db->WHERE("(o_estado = 1 OR o_estado = 2 OR o_estado = 4)");
        $query = $this->db->get();
        return $query->num_rows();
    }

}