<?php
class Model_clasificacionsectorial extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }
    public function modificar_datos_subsectores($subsectores,$codsec,$gestion)
    {
        $codsec = $codsec.'';
        $this->db->where('codsec', $codsec);
        $this->db->where('gestion', $gestion);
        return $this->db->update('_clasificadorsectorial', $subsectores);
    }
    public function modificar_datos_sector($data,$codsectorial,$gestion)
    {
        $codsectorial = $codsectorial.'';
        $this->db->where('codsectorial', $codsectorial);
        $this->db->where('gestion', $gestion);
        return $this->db->update('_clasificadorsectorial', $data);
    }
    public function get_clasificador_sectorial($codsectorial,$gestion)
    {
        $query = "SELECT *
        FROM _clasificadorsectorial
        WHERE codsectorial = '$codsectorial' and gestion = $gestion";
        $query = $this->db->query($query);
        $query = $query->row();
        return $query;
    }
    public function inserta_nuevosector($data)
    {
        $this->db->INSERT('_clasificadorsectorial',$data);
    }
    public function verfica_nuevosector($nuevo_pk, $gestion)
    {
        $query = "SELECT count(*) n
        FROM _clasificadorsectorial
        WHERE codsectorial = '$nuevo_pk' and gestion = $gestion";
        $query = $this->db->query($query);
        $query = $query->row();
        $bool = ($query->n > 0) ? true : false ;
        return $bool;
    }
    public function get_sectores_gestion($gestion)
    {
        $query = "SELECT *
        FROM _clasificadorsectorial
        WHERE nivel = 1 and gestion = $gestion
        ORDER BY codsectorial";
        $query = $this->db->query($query);
        return $query->result_array();
    }
    public function get_subsectores_gestion($gestion,$codsector)
    {
        $query = "SELECT *
        FROM _clasificadorsectorial
        WHERE gestion = $gestion and codsec like '$codsector' and (nivel = 2 or nivel = 3)
        order by codsectorial";
        $query = $this->db->query($query);
        return $query->result_array();
    }
    public function elimina_sector($data)
    {
        $update = array(
            'estado' => 0
        );
        $data['codsec'] = $data['codsec'].'';
        $this->db->where('codsec', $data['codsec']);
        $this->db->where('gestion', $data['gestion']);
        return $this->db->update('_clasificadorsectorial', $update);
    }
    public function datos_sector($gestion,$codsector)
    {
        $query = "SELECT *
        from _clasificadorsectorial
        where codsec like '$codsector' and gestion = $gestion and nivel = 1";
        $query = $this->db->query($query);
        return $query->row();
    }
}