<?php
class model_estructura_org extends CI_Model {
    /**
     * Responsable for auto load the database
     * @return void
     */
    public function __construct()
    {
        $this->load->database();
    }

    public function list_unidad_organizacional()
    {
        $sql = 'select * from vista_unidad_organizacional';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function list_tipo_ue()
    {
        $sql = ' select * from tp_unidad_ejecutora where tue_estado!=\'3\'';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function estructura_org()
    {
        $sql = 'SELECT *
        FROM unidadorganizacional where uni_estado!=\'3\';';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function cod_ultimo()
    {
        $sql = ' select MAX(uni_id+1) as codigo
                from unidadorganizacional;';
        $query = $this->db->query($sql);
        return $query->result_array();
    }


    function verificar_unicod($cod){
        $this->db->trans_begin();
        $this->db->WHERE('uni_id',$cod);
        $this->db->FROM('unidadorganizacional');
        $query = $this->db->get();
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }else{
            $this->db->trans_commit();
            return $query->result_array();
        }
    }
    function mod_uni($uni_id,$uni_unidad,$padre){
        $data = array(
            'uni_id' => $uni_id,
            'uni_unidad' => strtoupper( $uni_unidad),
            'uni_depende' => $padre,
        );
        $this->db->WHERE('uni_id',$uni_id);
        $this->db->UPDATE('unidadorganizacional',$data);
    }
    function add_uni_independiente($uni_codigo,$uni_unidad){
        $data = array(
            'uni_id' => $uni_codigo,
            'uni_unidad' => strtoupper( $uni_unidad),
            'uni_depende' => 0,
        );
        $this->db->insert('unidadorganizacional',$data);
    }
    function add_uni_dependiente($uni_codigo,$uni_unidad,$padre){
        $data = array(
            'uni_id' => $uni_codigo,
            'uni_unidad' => strtoupper( $uni_unidad),
            'uni_depende' => $padre,
        );
        $this->db->insert('unidadorganizacional',$data);
    }
    function get_uni($id){
        $this->db->select("*");
        $this->db->from('unidadorganizacional');
        $this->db->where('uni_id',$id);
        $query = $this->db->get();

        return $query->result_array();
    }

    public function codigo_da_ue($cod_da,$cod_ue)
    {
        $this->db->select("*");
        $this->db->from('unidadorganizacional');
        $this->db->where('uni_cod_da', $cod_da);
        $this->db->where('uni_cod_ue', $cod_ue);
        $this->db->where('uni_estado !=',3);

        $consulta = $this->db->get();
        $cantidad_encontrados = $consulta->num_rows();

        if($cantidad_encontrados>='1')
        {return true;}
        else
        {return false;}
    }

}
?>
