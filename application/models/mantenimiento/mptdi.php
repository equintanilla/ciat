<?php
class Mptdi extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }
    //obtener mi lista de pilares
    public function lista_pilar($gestion)
    {
        /*$this->db->SELECT('*');
        $this->db->FROM('ptdi');
        $this->db->WHERE('ptdi_gestion', $gestion);
        $this->db->WHERE('ptdi_jerarquia', 1);
        $this->db->ORDER_BY('ptdi_id', 'ASC');
        $query = $this->db->get();
        return $query->result_array();*/
        $query=$this->db->query(
            'SELECT * FROM ptdi
            WHERE ('.$gestion.'>=ptdi_gestion and '.$gestion.'<=ptdi_gestion_fin) 
                    and ptdi_jerarquia=1  
            ORDER BY ptdi_id ASC');
        return $query->result_array();
    }
    // LISTA DE METAS FILTRADO POR DEPENDENCIA
    public function lista_combo($gestion,$id_depende){
        // $this->db->SELECT('*');
        // $this->db->FROM('ptdi');
        // $this->db->WHERE('ptdi_gestion', $gestion);
        // $this->db->WHERE('ptdi_depende', $id_depende);
        // $this->db->ORDER_BY('ptdi_codigo', 'ASC');
        // $query = $this->db->get();
        // return $query->result_array();

        $query=$this->db->query(
            'SELECT * FROM ptdi
            WHERE ('.$gestion.'>=ptdi_gestion and '.$gestion.'<=ptdi_gestion_fin) 
                    and ptdi_depende='.$id_depende.' 
            ORDER BY ptdi_codigo ASC');
        return $query->result_array();
    }
    //obtener mi lista de ptdi
    public function lista_ptdi($gestion)
    {
        // $this->db->SELECT('*');
        // $this->db->FROM('ptdi');
        // $this->db->WHERE('ptdi_gestion', $gestion);
        // $this->db->ORDER_BY('ptdi_codigo', 'ASC');
        // $query = $this->db->get();
        // return $query->result_array();

        $query=$this->db->query(
            'SELECT * FROM ptdi
            WHERE ('.$gestion.'>=ptdi_gestion and '.$gestion.'<=ptdi_gestion_fin) 
            ORDER BY ptdi_codigo ASC');
        return $query->result_array();
    }

    public function listar_ptdi_pilar()
    {
       
        $sql = 'SELECT *from ptdi
        where ptdi_jerarquia=1 and ptdi_estado !=0';
        $query = $this->db->query($sql);
        return $query->result_array();
        redirect('mantenimiento/vlista_ptdi');
    }
    public function listar_ptdi_metas($ptdi_codigo)
    {
       
        $sql = 'SELECT *from ptdi
        where ptdi_jerarquia=2 and ptdi_estado !=0 and  ptdi_depende='.$ptdi_codigo.'';
        $query = $this->db->query($sql);
        return $query->result_array();
        redirect('mantenimiento/vlista_ptdi');
    }
    public function listar_ptdi_resultado($ptdi_codigo)
    {
       
        $sql = 'SELECT *from ptdi
        where ptdi_jerarquia=3 and ptdi_estado !=0 and  ptdi_depende='.$ptdi_codigo.'';
        $query = $this->db->query($sql);
        return $query->result_array();
        redirect('mantenimiento/vlista_ptdi');
    }
    public function listar_ptdi_accion($ptdi_codigo)
    {
       
        $sql = 'SELECT *from ptdi
        where ptdi_jerarquia=4 and ptdi_estado !=0 and  ptdi_depende='.$ptdi_codigo.'';
        $query = $this->db->query($sql);
        return $query->result_array();
        redirect('mantenimiento/vlista_ptdi');
    }





}
?>  
