<?php
class Model_analisis_situacion extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }
    
    public function get_tipo_foda()
    {
        $query = "SELECT *
        FROM tipofoda
        WHERE tfoda_estado = 1";
        $query = $this->db->query($query);
        return $query->result_array();
    }
    public function insertar_foda($data)
    {
        $this->db->INSERT('foda', $data);
    }
    public function get_foda_por_poa_id($poa_id)
    {
        $query = "SELECT *
        from ((select f.*, t.tfoda_descripcion, t.tfoda_analisis,
            CASE WHEN t.tfoda_id = 'F' THEN 1
                WHEN t.tfoda_id = 'O' THEN 2
                WHEN t.tfoda_id = 'D' THEN 3
                WHEN t.tfoda_id = 'A' THEN 4
                ELSE 0
            END id_orden
        FROM (select *
        from foda
        where estado = 1 or estado =2) f left join tipofoda t
        on (f.tfoda_id = t.tfoda_id and t.tfoda_estado = 1) or (f.tfoda_id = t.tfoda_id and t.tfoda_estado = 2))) tmp
        where poa_id = $poa_id
        ORDER BY tmp.id_orden";
        $query = $this->db->query($query);
        return $query->result_array();
    }
    public function get_foda_para_editar($foda_id)
    {
        $query = "SELECT *
        FROM foda
        WHERE foda_id = $foda_id";
        $query = $this->db->query($query);
        return $query;
    }
    public function edita_foda($data,$foda_id)
    {
        $this->db->WHERE('foda_id',$foda_id);
        return $this->db->UPDATE('foda',$data);
    }
    public function elimina_foda($foda_id)
    {
        $data = array(
            'estado' => 0
        );
        $this->db->WHERE('foda_id',$foda_id);
        return $this->db->UPDATE('foda',$data);
    }
    public function get_datos_poa_para_pdf($poa_id,$gestion)
    {
        $query = "SELECT *
        FROM vista_poa
        WHERE poa_id = $poa_id and poa_gestion = $gestion";
        $query = $this->db->query($query);
        return $query;
    }
}
