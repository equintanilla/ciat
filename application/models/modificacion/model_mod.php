<?php
class Model_mod extends CI_Model{
    var $gestion;
    public function __construct(){
        $this->load->database();
        $this->load->model('programacion/model_proyecto');
        $this->load->model('programacion/model_actividad');
        $this->load->model('programacion/model_producto');
        $this->load->model('programacion/model_componente');
        $this->gestion = $this->session->userData('gestion');
        $this->fun_id = $this->session->userData('fun_id');
        $this->rol = $this->session->userData('rol_id');
        $this->adm = $this->session->userData('adm');
        $this->dist = $this->session->userData('dist');
        $this->dist_tp = $this->session->userData('dist_tp');
    }
    
    //tipo de estado
    public function get_tp_estado($proy_id){
        $sql = ' select *
                 from mod_est_proy
                 where proy_id='.$proy_id.'';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*-------- Lista de Operaciones Aprobados -----*/
    public function operaciones_aprobados($prog,$tp){
        $sql = 'select tap.*,p.*,tp.*,fu.*
                    from _proyectos as p
                    Inner Join _tipoproyecto as tp On p.tp_id=tp.tp_id
                    Inner Join (select apy.*,apg.* from aperturaprogramatica as apg, aperturaproyectos as apy where apy.aper_id=apg.aper_id) as tap On p.proy_id=tap.proy_id
                    Inner Join (select pf."proy_id",f."fun_id",f."fun_nombre",f."fun_paterno",f."fun_materno",u."uni_unidad" as ue,ur."uni_unidad" as ur
                        from _proyectofuncionario pf
                        Inner Join funcionario as f On pf.fun_id=f.fun_id
                        Inner Join unidadorganizacional as u On pf."uni_ejec"=u."uni_id"
                        Inner Join unidadorganizacional as ur On pf.uni_resp=ur.uni_id
                    where pf.pfun_tp=\'1\') as fu On fu.proy_id=p.proy_id
                    where tap.aper_programa=\''.$prog.'\' and p.estado!=\'3\' and tap.aper_gestion='.$this->session->userData('gestion').' and proy_estado=\'4\' and p.tp_id='.$tp.' 
                    ORDER BY tap.aper_proyecto,tap.aper_actividad  asc';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*-------- Lista de Operaciones Aprobados -----*/
    public function tp_operaciones($tp,$rol_id){
        $sql = 'select tap.*,p.*,tp.*,fu.*
                    from _proyectos as p
                    Inner Join _tipoproyecto as tp On p.tp_id=tp.tp_id
                    Inner Join mod_est_proy as et On et.proy_id=p.proy_id
                    Inner Join (select apy.*,apg.* from aperturaprogramatica as apg, aperturaproyectos as apy where apy.aper_id=apg.aper_id) as tap On p.proy_id=tap.proy_id
                    Inner Join (select pf."proy_id",f."fun_id",f."fun_nombre",f."fun_paterno",f."fun_materno",u."uni_unidad" as ue,ur."uni_unidad" as ur
                        from _proyectofuncionario pf
                        Inner Join funcionario as f On pf.fun_id=f.fun_id
                        Inner Join unidadorganizacional as u On pf."uni_ejec"=u."uni_id"
                        Inner Join unidadorganizacional as ur On pf.uni_resp=ur.uni_id
                    where pf.pfun_tp=\'1\') as fu On fu.proy_id=p.proy_id
                    where p.estado!=\'3\' and tap.aper_gestion='.$this->session->userData('gestion').' and proy_estado=\'4\' and p.tp_id='.$tp.' and et.r_id='.$rol_id.' 
                    ORDER BY tap.aper_proyecto,tap.aper_actividad  asc';
        $query = $this->db->query($sql);
        return $query->result_array();
    }


    /*-------- historial Plazo --------*/
    public function historial_plazo($pfec_id){
        $sql = 'select *
                from historial_proyectofaseetapacomponente
                where pfec_id='.$pfec_id.'
                order by hpfec_id';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*-------- Lista de Partidas Actividad,Componente --------*/
    public function list_partida_insumo($tp_act_comp,$tp_ejec){
        if($tp_ejec==1){
           $sql = 'select *
                from insumos_partida_actividad ia
                Inner Join insumos_partida as ip On ip.insp_id=ia.insp_id
                Inner Join fuentefinanciamiento as ff On ff.ff_id=ip.ff_id
                Inner Join organismofinanciador as oof On oof.of_id=ip.of_id
                Inner Join partidas as par On par.par_id=ip.par_id
                where ia.act_id='.$tp_act_comp.'
                order by ia.insp_id asc'; 
        }
        else{
            $sql = 'select *
                from insumos_partida_componente ic
                Inner Join insumos_partida as ip On ip.insp_id=ic.insp_id
                Inner Join fuentefinanciamiento as ff On ff.ff_id=ip.ff_id
                Inner Join organismofinanciador as oof On oof.of_id=ip.of_id
                Inner Join partidas as par On par.par_id=ip.par_id
                where ic.com_id='.$tp_act_comp.'
                order by ic.insp_id asc';
        }

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*-------- Suma monto Partidas - Actividad,Componente --------*/
    public function suma_monto_partida($tp_act_comp,$tp_ejec){
        if($tp_ejec==1){
            $sql = 'select ip.insp_id,sum(ip.insp_inicial) minicial, sum(ip.insp_actual) mactual
                from insumos_partida_actividad ia
                Inner Join insumos_partida as ip On ip.insp_id=ia.insp_id
                Inner Join partidas as par On par.par_id=ip.par_id
                where ia.act_id='.$tp_act_comp.'
                group by ip.insp_id';
        }
        else{
            $sql = ' select ip.insp_id,sum(ip.insp_inicial) minicial, sum(ip.insp_actual) mactual
                from insumos_partida_componente ic
                Inner Join insumos_partida as ip On ip.insp_id=ic.insp_id
                Inner Join partidas as par On par.par_id=ip.par_id
                where ic.com_id='.$tp_act_comp.'
                group by ip.insp_id';
        }
        
        $query = $this->db->query($sql);
        return $query->result_array();
    }

}
