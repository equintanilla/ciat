<?php
class Model_dasgcorriente extends CI_Model {
    public function __construct()
    {
        $this->load->database();
    }

    /*-------------- Lista de direcciones administrativa ------------*/
    public function direcciones_administrativas()
    {
        $sql = 'select *
                from unidadorganizacional
                where uni_adm=\'1\' and uni_estado!=\'3\'
                order by uni_id asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*======================= EJECUCION PRESUPUESTARIA DAS ID UNI TOTAL =====================*/
    public function suma_total_lista_programas_gcorriente($gestion,$mes)
    {
        $sql = 'select SUM(pfecg_ppto_total) as costo,SUM(pe.pe_pi) as pi, SUM(pe_pm) as pm ,SUM(pe_pv) as pv ,SUM(pe_pe) as pe
                from aperturaprogramatica a
                Inner Join unidadorganizacional as u On a.uni_id = u.uni_id
                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
                Inner Join _proyectos as p On ap.proy_id = p.proy_id
                Inner Join _proyectofuncionario as pfun On p.proy_id = pfun.proy_id
                Inner Join unidadorganizacional as uad On uad.uni_id = pfun.uni_adm
                Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id
                Inner Join ptto_fase_gestion as pfg On pfg.pfec_id = pf.pfec_id
                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
                where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and a.aper_gestion='.$gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and  pe.m_id='.$mes.' and pf.pfec_estado=\'1\' and pfg.g_id='.$gestion.' and pfun.pfun_tp=\'1\' and p.tg_id=\'2\' and uad.uni_adm=\'1\'';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*======================= EJECUCION PRESUPUESTARIA DAS ID UNI=====================*/
    public function lista_programas_gcorriente($gestion,$mes,$uni_id)
    {
        $sql = 'select a.*,pe.*,u.*,p.*,pfg.*,pfun.*,uad.*
                from aperturaprogramatica a
                Inner Join unidadorganizacional as u On a.uni_id = u.uni_id
                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
                Inner Join _proyectos as p On ap.proy_id = p.proy_id
                Inner Join _proyectofuncionario as pfun On p.proy_id = pfun.proy_id
                Inner Join unidadorganizacional as uad On uad.uni_id = pfun.uni_adm
                Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id
                Inner Join ptto_fase_gestion as pfg On pfg.pfec_id = pf.pfec_id
                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
                where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and a.aper_gestion='.$gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and  pe.m_id='.$mes.' and pf.pfec_estado=\'1\' and pfg.g_id='.$gestion.' and pfun.pfun_tp=\'1\' and p.tg_id=\'2\' and uad.uni_adm=\'1\' and uad.uni_id='.$uni_id.'
                order by a.aper_programa,a.aper_proyecto,a.aper_actividad asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*======================= EJECUCION PRESUPUESTARIA DAS ID UNI EJEC =====================*/
    public function lista_programas_gcorriente_detalle($gestion,$mes,$uni_id_das, $uni_ejec)
    {
        $sql = 'select a.*,pe.*,u.*,p.*,pfg.*,pfun.*,uad.*
                from aperturaprogramatica a
                Inner Join unidadorganizacional as u On a.uni_id = u.uni_id
                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
                Inner Join _proyectos as p On ap.proy_id = p.proy_id
                Inner Join _proyectofuncionario as pfun On p.proy_id = pfun.proy_id
                Inner Join unidadorganizacional as uad On uad.uni_id = pfun.uni_adm
                Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id
                Inner Join ptto_fase_gestion as pfg On pfg.pfec_id = pf.pfec_id
                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
                where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and a.aper_gestion='.$gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and  pe.m_id='.$mes.' and pf.pfec_estado=\'1\' and pfg.g_id='.$gestion.' and pfun.pfun_tp=\'1\' and p.tg_id=\'2\' and uad.uni_adm=\'1\' and uad.uni_id='.$uni_id_das.' and pfun.uni_ejec='.$uni_ejec.'
                order by a.aper_programa,a.aper_proyecto,a.aper_actividad asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*======================= EJECUCION PRESUPUESTARIA DAS ID UNI TOTAL =====================*/
    public function suma_lista_programas_gcorriente($gestion,$mes,$uni_id)
    {
        $sql = 'select uad.uni_id,SUM(pfecg_ppto_total) as costo,SUM(pe.pe_pi) as pi, SUM(pe_pm) as pm ,SUM(pe_pv) as pv ,SUM(pe_pe) as pe
                from aperturaprogramatica a
                Inner Join unidadorganizacional as u On a.uni_id = u.uni_id
                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
                Inner Join _proyectos as p On ap.proy_id = p.proy_id
                Inner Join _proyectofuncionario as pfun On p.proy_id = pfun.proy_id
                Inner Join unidadorganizacional as uad On uad.uni_id = pfun.uni_adm
                Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id
                Inner Join ptto_fase_gestion as pfg On pfg.pfec_id = pf.pfec_id
                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
                where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and a.aper_gestion='.$gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and  pe.m_id='.$mes.' and pf.pfec_estado=\'1\' and pfg.g_id='.$gestion.' and pfun.pfun_tp=\'1\' and p.tg_id=\'2\' and uad.uni_adm=\'1\' and uad.uni_id='.$uni_id.'
                group by uad.uni_id
                order by uad.uni_id asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

}