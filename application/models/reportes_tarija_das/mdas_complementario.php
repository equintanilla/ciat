<?php
class Mdas_complementario extends CI_Model {
    public function __construct()
    {
        $this->load->database();
    }

    /*-------------- Get Unidad Administrativa ------------*/
    public function get_unidad_organizacional($uni_id)
    {
        $sql = 'select *
                from unidadorganizacional
                where uni_id='.$uni_id.'';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*-------------- Lista de unidades Ejecutoras ------------*/
    public function unidades_ejecutoras()
    {
        $sql = 'select *
                from unidadorganizacional
                where uni_ejecutora=\'1\' and uni_estado!=\'3\'
                order by uni_id asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*-------------- Suma Gestiones Ejecuciones ------------*/
    public function ejecuciones($pfec_id,$gestion)
    {
        $sql = 'select SUM(pfecg_ppto_ejecutado) as total_ejecutado 
                from ptto_fase_gestion
                where pfec_id='.$pfec_id.' and g_id<'.$gestion.' and estado!=\'3\'';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*-------------- Suma Gestiones Programado ------------*/
    public function costo_total_programado($pfec_id)
    {
        $sql = 'select SUM(pfecg_ppto_total) as total_programado
                 from ptto_fase_gestion
                 where pfec_id='.$pfec_id.' and estado!=\'3\'';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*-------------- Estado de la Operacion ------------*/
    public function estado_operacion($pfec_id,$mes_id,$gestion)
    {
        $sql = ' select *
                 from fase_ejecucion fe
                 Inner Join _estadoproyecto as est On est.ep_id = fe.estado
                 where pfec_id='.$pfec_id.' and m_id='.$mes_id.' and g_id='.$gestion.'';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*-------------- Lista de direcciones administrativa ------------*/
    public function direcciones_administrativas()
    {
        $sql = 'select *
                from unidadorganizacional
                where uni_adm=\'1\' and uni_estado!=\'3\'
                order by uni_id asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }


    /*-------------- Get Componente Estrategico------------*/
    public function get_componente_estrategico($ptdi_id)
    {
        $sql = 'select *
                from ptdi
                where ptdi_id='.$ptdi_id.'';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*-------------- Costo total del proyecto ------------*/
    public function costo_total_proyecto($pfec_id)
    {
        $sql = 'select SUM(pfec_ptto_fase) as total_programado
                 from _proyectofaseetapacomponente
                 where pfec_id='.$pfec_id.' and estado!=\'3\'';

        $query = $this->db->query($sql);
        return $query->result_array();
        //select sum(pfec_ptto_fase) from _proyectofaseetapacomponente
    }

    /*-------------- Suma Total de modificaciones a costo total del proyecto ------------*/
    public function modificaciones_costo_total($pfec_id)
    {
        $sql = 'select sum(pfec_variacion) as total_modif
                from _ptto_audi 
                where pfec_id='.$pfec_id;

        $query = $this->db->query($sql);
        return $query->result_array();
    }

}