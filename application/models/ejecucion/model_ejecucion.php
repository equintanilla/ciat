<?php

class Model_ejecucion extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }
    
    /*--------------------------- AVANCE FINANCIERO --------------------------------*/
    public function datos_programa_presupuesto($proy_id,$mes_id,$gestion)
    {
        $sql = 'select a.*,pe.*,u.*,p.*,pfg.*
                from aperturaprogramatica a
                Inner Join unidadorganizacional as u On a.uni_id = u.uni_id
                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
                Inner Join _proyectos as p On ap.proy_id = p.proy_id
                Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id
                Inner Join ptto_fase_gestion as pfg On pfg.pfec_id = pf.pfec_id
                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
                where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and a.aper_gestion='.$gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and  pe.m_id='.$mes_id.' and pf.pfec_estado=\'1\' and pfg.g_id='.$gestion.' and p.proy_id='.$proy_id.'';

        $query = $this->db->query($sql);
        return $query->result_array();
    }
    //====Verifica si la ejecución que se registra no fue hecha anteriormente
    public function verificar_registro($pfec_id,$com_id,$mes_id,$gest)
    {
        $this->db->select("*");
        $this->db->from('fase_ejecucion');
        $this->db->where('pfec_id', $pfec_id);
        $this->db->where('com_id', $com_id);
        $this->db->where('m_id', $mes_id);
        $this->db->where('g_id', $gest);
        $this->db->where('estado !=', 3);

        $consulta = $this->db->get();
        $cantidad_encontrados = $consulta->num_rows();

        if($cantidad_encontrados>='1')
        {return true;}
        else
        {return false;}
    }

    /*--------------------- vista aperturas programaticas -----------------*/
    public function aperturasprogramaticas($programa,$proyecto,$actividad,$gestion)
    {
        $sql = 'select *
                from vaperturas_programaticas
                where programa='.$programa.' and proyecto='.$proyecto.' and actividad='.$actividad.' and aper_gestion='.$gestion.'';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*--------------------- ptto proyecto sigep -----------------*/
    public function operacion_ptto_sigep($aper_id,$mes)
    {
        $sql = 'select *
                from _proy_ejec_ppto
                where aper_id='.$aper_id.' and m_id='.$mes.'';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*--------------------- Insert/Update ptto sigep -----------------*/
    public function storage_ptto($aper_id,$mes,$tp,$pi,$pm,$pv,$pe)
    {
        if($tp==1){
            $query=$this->db->query('set datestyle to DMY');
            $update_pres = array( 
                'pe_pi' => $pi,
                'pe_pm' => $pm,
                'pe_pv' => $pv,
                'pe_pe' => $pe,
                'fun_id' => $this->session->userdata("fun_id"),
                'pe_estado' => 2,
            );

            $this->db->where('aper_id', $aper_id);
            $this->db->where('m_id', $mes);
            $this->db->update('_proy_ejec_ppto', $update_pres);
        }
        else{
            for ($i=1; $i <=12 ; $i++) { 
               if($mes==$i)
                {
                    $query=$this->db->query('set datestyle to DMY');
                    $data_to_store = array( 
                        'aper_id' => $aper_id,
                        'm_id' => $mes,
                        'pe_pi' => $pi,
                        'pe_pm' => $pm,
                        'pe_pv' => $pv,
                        'pe_pe' => $pe,
                        'fun_id' => $this->session->userdata("fun_id"),
                    );
                    $this->db->insert('_proy_ejec_ppto', $data_to_store);
                }
                else{
                    $query=$this->db->query('set datestyle to DMY');
                    $data_to_store = array( 
                        'aper_id' => $aper_id,
                        'm_id' => $i,
                        'fun_id' => $this->session->userdata("fun_id"),
                        'pe_estado' => 2,
                    );
                    $this->db->insert('_proy_ejec_ppto', $data_to_store);
                }
            }
        }

    }
    /*--------Generar log de proyectos nuevos detectados al importar datos del sigep*/
    public function set_proyectos_nuevos($cat_prog,$cat_nombre,$pi,$pm,$pv,$pe,$mes,$gestion){
        $query=$this->db->query('set datestyle to DMY');
        $data_to_store = array(
            'cat_prog' => $cat_prog,
            'cat_nombre'=> $cat_nombre,
            'pe_pi' => $pi,
            'pe_pm' => $pm,
            'pe_pv' => $pv,
            'pe_pe' => $pe,
            'm_id' => $mes,
            'g_id' => $gestion,
            'fun_id' => $this->session->userdata("fun_id"),
            'estado'=>'N'
        );
        $this->db->insert('audit.proyecto_sin_cat', $data_to_store);

    }
    public function proyectos_existente($cat_prog,$gestion)
    {
        $sql = 'select *
                from audit.proyecto_sin_cat
                where cat_prog=\''.$cat_prog.'\' and g_id='.$gestion;
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    public function proyectos_nuevos($mes_id,$gestion){
        $sql = 'select *
                from audit.proyecto_sin_cat
                where m_id='.$mes_id.' and g_id='.$gestion;
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    /*---------------------tabla meses ejecucion sigep -----------------*/
    public function meses_sigep($mes,$gestion)
    {
        $sql = 'select *
                from mes_ejecucion_sigep
                where m_id='.$mes.' and g_id='.$gestion.' and sg_estado!=\'3\'';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*---------------------tabla lista meses ejecucion sigep -----------------*/
    public function list_meses_sigep($gestion)
    {
        $sql = 'select *
                from mes_ejecucion_sigep ms
                Inner Join mes as m On m.m_id=ms.m_id
                where ms.g_id='.$gestion.' and ms.sg_estado!=\'3\'';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*---------------------- MESES -----------*/
    public function list_meses()
    {
        $sql = 'select *
                from mes
                where m_id!=\'0\'
                order by m_id asc';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*---------------------- GET PROYECTO EJECUCION -----------*/
    public function get_proyecto_ejec_ptto($aper_id)
    {
        $sql = 'select *
                from _proy_ejec_ppto
                where aper_id='.$aper_id.'';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function get_proyecto_ejec_ptto_mes($aper_id,$mes)
    {
        $sql = 'select *
                from _proy_ejec_ppto
                where aper_id='.$aper_id.' and m_id='.$mes.'';
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    /*----------------------------------------------------------*/
    /*======================================================= LISTA DE PROYECTOS SEGUN SU TIPO PROY PARA EL ADMINISTRADOR ====================================*/
    public function proyecto_n($tp,$gestion)
    {
        $sql = 'select count(p.*)as nro, p.*,tp.*,fu.*,tap.*,f.*,fg.*,fu.*
            from _proyectos as p
            Inner Join _tipoproyecto as tp On p.tp_id=tp.tp_id
            Inner Join (select apg.aper_id ,apy.proy_id ,apg.aper_programa,apg.aper_gestion,apg.aper_proyecto,apg.aper_actividad from aperturaprogramatica as apg, aperturaproyectos as apy where apy.aper_id=apg.aper_id) as tap On p.proy_id=tap.proy_id
            Inner Join (select pf.fun_id,pf.proy_id,f.fun_nombre,f.fun_paterno,f.fun_materno,u.uni_id,u.uni_unidad
            from _proyectofuncionario pf
            Inner Join funcionario as f On pf.fun_id=f.fun_id
            Inner Join unidadorganizacional as u On pf.uni_ejec=u.uni_id
            where pf.pfun_tp=1) as fu On fu.proy_id=p.proy_id
        Inner Join (select pfec_id,proy_id, pfec_fecha_inicio,pfec_fecha_fin,pfec_descripcion,pfec_ptto_fase from _proyectofaseetapacomponente where pfec_estado=\'1\')as f On f.proy_id=p.proy_id
        Inner Join (select pfec_id,pfecg_ppto_total, pfecg_ppto_ejecutado from ptto_fase_gestion where g_id='.$gestion.')as fg On fg.pfec_id=f.pfec_id
        Inner Join _componentes as c On c.pfec_id=f.pfec_id
        Inner Join _productos as pr On pr.com_id=c.com_id
        Inner Join _actividades as ac On ac.prod_id=pr.prod_id
            where p.tp_id='.$tp.' and p.proy_estado=\'5\' and p.estado!=\'3\' and tap.aper_gestion='.$gestion.' GROUP BY p.proy_id,tp.tp_id,tap.aper_id,tap.aper_gestion,tap.proy_id,tap.aper_programa,tap.aper_proyecto,tap.aper_actividad,f.pfec_id,f.proy_id,
            f.pfec_fecha_inicio,f.pfec_fecha_fin,f.pfec_descripcion,f.pfec_ptto_fase,fg.pfec_id,fg.pfecg_ppto_total,fg.pfecg_ppto_ejecutado,fu.fun_id,fu.proy_id,fu.fun_nombre,fu.fun_paterno,fu.fun_materno,
            fu.uni_id,fu.uni_unidad';
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    /*============================================================================================== ===========================================*/
  
 /*======================================================= LISTA DE PROYECTOS SEGUN SU TIPO PROY PARA EL FUNCIONARIO ====================================*/
    public function proyecto_n_fun_id($tp,$prog,$fun_id,$gestion)
    {
        $sql = 'SELECT count(p.*)as nro, p.*,tp.*,fu.*,tap.*,f.*,fg.*,fu.*
            from _proyectos as p
            Inner Join _tipoproyecto as tp On p."tp_id"=tp."tp_id"
            Inner Join (select apg."aper_id" ,apy."proy_id" ,apg."aper_programa",apg."aper_gestion",apg."aper_proyecto",apg."aper_actividad" from aperturaprogramatica as apg, aperturaproyectos as apy where apy."aper_id"=apg."aper_id") as tap On p."proy_id"=tap."proy_id"
            Inner Join (select pf."fun_id",pf."proy_id",f."fun_nombre",f."fun_paterno",f."fun_materno",u."uni_id",u."uni_unidad"
            from _proyectofuncionario pf
            Inner Join funcionario as f On pf."fun_id"=f."fun_id"
            Inner Join unidadorganizacional as u On pf."uni_ejec"=u."uni_id"
            where pf."pfun_tp"=\'1\') as fu On fu."proy_id"=p."proy_id"
        Inner Join (select pfec_id,proy_id, pfec_fecha_inicio,pfec_fecha_fin,pfec_descripcion,pfec_ptto_fase from _proyectofaseetapacomponente where pfec_estado=\'1\')as f On f."proy_id"=p."proy_id"
        Inner Join (select pfec_id,pfecg_ppto_total, pfecg_ppto_ejecutado from ptto_fase_gestion where g_id='.$gestion.')as fg On fg.pfec_id=f.pfec_id
        Inner Join _componentes as c On c.pfec_id=f.pfec_id
        Inner Join _productos as pr On pr.com_id=c.com_id
        Inner Join _actividades as ac On ac.prod_id=pr.prod_id
            where p.tp_id='.$tp.' and tap.aper_programa=\''.$prog.'\' and fu.fun_id='.$fun_id.' and p.proy_estado=\'4\' and p.estado!=\'3\' and tap.aper_gestion='.$gestion.' GROUP BY p.proy_id,tp.tp_id,tap.aper_id,tap.proy_id,tap.aper_programa,tap.aper_proyecto,tap.aper_actividad,f.pfec_id,f.proy_id,
            f.pfec_fecha_inicio,f.pfec_fecha_fin,f.pfec_descripcion,f.pfec_ptto_fase,fg.pfec_id,fg.pfecg_ppto_total,fg.pfecg_ppto_ejecutado,fu.fun_id,fu.proy_id,fu.fun_nombre,fu.fun_paterno,fu.fun_materno,
            fu.uni_id,fu.uni_unidad,tap.aper_gestion';
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    /*============================================================================================== ===========================================*/

    /*================================ ESTADO DEL PROYECTO ==============================*/  
    public function proy_estado()
    {
        $sql = 'select *
                from _estadoproyecto
                order by ep_id asc';
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    /*=====================================================================================*/
    /*================================ DEPENDENCIAS DEL ESTADO  ==============================*/  
    public function dependencias()
    {
        $this->db->from('dp_estados');
        $this->db->order_by("st_id","asc");
        $query = $this->db->get();
        return $query->result_array();
    }
    /*=====================================================================================*/
    /*============================ BORRA DATOS DE PRODUCTO MES EJECUTADO =================================*/
    public function delete_prod_ejec_mes_id($id_prod,$id_m,$gest){ 
        $this->db->where('prod_id', $id_prod);
        $this->db->where('m_id', $id_m);
        $this->db->where('g_id', $gest);
        $this->db->delete('prod_ejecutado_mensual'); 
    }
    /*====================================================================================================*/
        /*============================ BORRA DATOS DE LA ACTIVIDAD MES EJECUTADO =================================*/
    public function delete_act_ejec_mes_id($id_act,$id_m,$gest){ 
        $this->db->where('act_id', $id_act);
        $this->db->where('m_id', $id_m);
        $this->db->where('g_id', $gest);
        $this->db->delete('act_ejecutado_mensual'); 
    }
    /*====================================================================================================*/
    /*=================================== AGREGAR EJECUCION FASE DEL PROYECTO ====================================*/
    function ejecucion_fase_mes($accion,$id_fase,$id_com,$id_m,$id_fun,$prob,$efectos,$causas,$sol,$m_estado,$gestion,$estado_proy,$dep_est,$fip,$ffp)
    {
        if($accion==1) /// Crear nuevo registro de Ejecucion mensual
        {
            if($this->verificar_registro($id_fase,$id_com,$id_m,$gestion)==false) {
                $query = $this->db->query('set datestyle to DMY');
                $data_to_store = array(
                    'pfec_id' => $id_fase,
                    'com_id' => $id_com,
                    'm_id' => $id_m,
                    'id_fun' => $id_fun,
                    'ejec_prob' => strtoupper($prob),
                    'ejec_efect' => '',
                    'ejec_causas' => '',
                    'ejec_sol' => strtoupper($sol),
                    'ejec_est' => $m_estado,
                    'g_id' => $gestion,
                    'estado' => $estado_proy,
                    'st_id' => $dep_est,
                    'f_inicio_pinforme' => $fip,
                    'f_final_pinforme' => $ffp,
                    'f_archivo_sgp' =>'',
                );

                $this->db->insert('fase_ejecucion', $data_to_store);
            }
        }
        elseif($accion==2) //// Modificar ejecucion
        {   $query=$this->db->query('set datestyle to DMY');        
            $update_ejec = array(
                    'id_fun' => $id_fun,
                    'ejec_prob' => strtoupper($prob),
                    'ejec_efect' => '',
                    'ejec_causas' => '',
                    'ejec_sol' => strtoupper($sol),
                    'ejec_est' => $m_estado,
                    'g_id' => $gestion,
                    'estado' => $estado_proy,
                    'f_inicio_pinforme' => $fip,
                    'f_final_pinforme' => $ffp,
                    'st_id' => $dep_est
                    );

            $this->db->where('pfec_id', $id_fase);
            $this->db->where('m_id', $id_m);
            $this->db->where('com_id', $id_com);
            $this->db->where('g_id', $gestion);
            $this->db->update('fase_ejecucion', $update_ejec);
        }
    }
    

    /*=================================== AGREGAR EL SGP EN PDF A LA EJECUCIÓN MENSUAL DE LA FASE ====================================*/
    function guardar_pdf_bd($id_fase, $id_com, $id_m, $gestion, $nombre_documento)
    {
        $query=$this->db->query('set datestyle to DMY');
        $update_ejec = array(
            'f_archivo_sgp' => $nombre_documento
        );

        $this->db->where('pfec_id', $id_fase);
        $this->db->where('m_id', $id_m);
        $this->db->where('com_id', $id_com);
        $this->db->where('g_id', $gestion);
        $this->db->update('fase_ejecucion', $update_ejec);
    }

    
    /*==============================================================================================================*/
    /*================================= VERIFICANDO EJECUCION MENSUAL ======================================*/
    public function verif_ejecucion_fase_mes($id_f,$id_c,$id_m,$gestion)
    {
        $this->db->from('fase_ejecucion');
        $this->db->where('pfec_id', $id_f);
        $this->db->where('com_id', $id_c);
        $this->db->where('m_id', $id_m);
        $this->db->where('g_id', $gestion);
        $query = $this->db->get();
        return $query->num_rows();
    }
    /*================================================================================================*/  
    /*================================= DATOS EJECUCION FASE MENSUAL ======================================*/
    public function datos_ejecucion_fase_mes($id_f,$id_c,$id_m,$gestion)
    {
        $this->db->from('fase_ejecucion');
        $this->db->where('pfec_id', $id_f);
        $this->db->where('com_id', $id_c);
        $this->db->where('m_id', $id_m);
        $this->db->where('g_id', $gestion);
        $query = $this->db->get();
        return $query->result_array();
    }
    /*================================================================================================*/  
    /*========================== LISTA DE ARCHIVOS ADJUNTOS MENSUALES ================================*/  
    public function lista_archivos_mes($id_f,$id_m)
    {
        $this->db->select("
                            pdm.fa_id,
                            pdm.pfec_id,
                            pdm.m_id,
                            pdm.id_fun,
                            f.fun_nombre,
                            f.fun_paterno,
                            f.fun_materno,
                            pdm.archivo,
                            pdm.ejec_fecha,
                            pdm.tip_doc,
                            pdm.g_id,
                            pdm.documento");
        $this->db->from('fase_ejecucion_adjuntos pdm');
        $this->db->join('funcionario f', 'pdm.id_fun = f.fun_id', 'left');
        $this->db->where('pdm.pfec_id',$id_f);
        $this->db->where('pdm.m_id',$id_m);
        $this->db->where('pdm.g_id',$this->session->userdata('gestion'));
        $this->db->order_by("pdm.fa_id","asc");
        $query = $this->db->get();
        return $query->result_array();
    }
    /*================================================================================================*/ 
    /*================================= DATOS OBSERVACION EJECUCION ======================================*/
    public function observaciones_fase($id_f,$id_m,$id_c)
    {
        $this->db->from('fase_observacion');
        $this->db->where('pfec_id', $id_f);
        $this->db->where('m_id', $id_m);
        $this->db->where('g_id', $this->session->userdata('gestion'));
        $this->db->where('com_id', $id_c);
        $this->db->order_by('fo_id',"asc");
        $query = $this->db->get();
        return $query->result_array();
    }
    /*================================================================================================*/ 
    /*================================= DATOS OBSERVACION EJECUCION ======================================*/
    public function observaciones_id($id_obs)
    {
        $this->db->from('fase_observacion');
        $this->db->where('fo_id', $id_obs);
        $query = $this->db->get();
        return $query->result_array();
    }
    /*================================================================================================*/ 

    /*==================================== LISTA MESES EJECUCION ====================================*/
    public function list_meses_ejec($id_fase,$id_c)  
    {   
        $sql = 'SELECT fe.*,f.*
                from fase_ejecucion as fe
                Inner Join funcionario as f On fe."id_fun"=f."fun_id"
                where fe."pfec_id"='.$id_fase.' and fe."com_id"='.$id_c.' and g_id='.$this->session->userdata('gestion').' ORDER BY fe.fe_id  asc';
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    /*================================================================================================*/ 
    /*==================================== LISTA MESES EJECUCION ====================================*/
    public function list_archivos_mes($id_fase,$id_c,$id_m)  
    {   
        $sql = 'SELECT fe.*,f.*
                from fase_ejecucion_adjuntos as fe
                Inner Join funcionario as f On fe."id_fun"=f."fun_id"
                where fe."pfec_id"='.$id_fase.' and fe."com_id"='.$id_c.' and m_id='.$id_m.' and g_id='.$this->session->userdata('gestion').' ORDER BY fe.fa_id  asc';
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    /*================================================================================================*/ 
    /*================================= ARCHIVO  ID ======================================*/
    public function get_archivo_mes_proy($fa_id)
    {
        $sql = 'SELECT *
                from fase_ejecucion_adjuntos
                where fa_id='.$fa_id.'';
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    /*================================================================================================*/
    /*================================= MODALIDADES ======================================*/
    public function modalidades()
    {
        $this->db->from('_modalidades');
        $this->db->where('mod_estado', 1);
        $query = $this->db->get();
        return $query->result_array();
    }
    /*================================= TIPO DE CONTRATO ======================================*/
    public function tipo_contrato()
    {
        $this->db->from('_contratos_tipo');
        $this->db->where('ctto_tip_estado', 1);
        $query = $this->db->get();
        return $query->result_array();
    }
    /*================================= TIPO DE GARANTÍA ======================================*/
    public function tipo_garantia()
    {
        $this->db->from('boleta_garantia_tipo');
        $this->db->where('gtia_tip_estado', 1);
        $query = $this->db->get();
        return $query->result_array();
    }
    /*================================================================================================*/
        /*================================= CONTRATISTA ======================================*/
    public function contratista()
    {
        $this->db->from('_contratistas');
        $this->db->where('ctta_estado', 1);
        $query = $this->db->get();
        return $query->result_array();
    }
    /*================================================================================================*/
    /*================================= LISTA DE CONTRATOS ======================================*/
    public function list_contratos($id_f,$id_c)
    {
        $sql = 'SELECT c.*,m.*,ct.*,tip.ctto_tip_nombre
                from _contratos as c
                Inner Join _modalidades as m On m."mod_id"=c."mod_id"
                Inner Join _contratistas as ct On ct."ctta_id"=c."ctta_id"
                Inner Join _contratos_tipo as tip On c.ctto_tip_id=tip.ctto_tip_id 
                where c."pfec_id"='.$id_f.' and c."com_id"='.$id_c.' ORDER BY c.ctto_id  asc';
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    /*================================================================================================*/
    /*================================= NRO DE CONTRATOS ======================================*/
    public function nro_contratos($id_f)
    {
        $this->db->from('_contratos');
        $this->db->where('pfec_id', $id_f);
        $query = $this->db->get();
        return $query->num_rows();
    }
    /*================================================================================================*/
    /*================================= CONTRATO X ======================================*/
    public function contrato_id($ctto_id)
    {
        $sql = 'SELECT c.*,m.*,ct.*
                from _contratos as c
                Inner Join _modalidades as m On m."mod_id"=c."mod_id"
                Inner Join _contratistas as ct On ct."ctta_id"=c."ctta_id"
                where c."ctto_id"='.$ctto_id.' ORDER BY c.ctto_id  asc';
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    /*================================================================================================*/
    /*================================= LISTA DE GARANTIAS ======================================*/
    public function lista_garantias($ctto_id)
    {
        $sql = 'SELECT g.*,tip.gtia_tip_nombre
                from boleta_garantia as g
                Inner Join boleta_garantia_tipo as tip On g.gtia_tip_id=tip.gtia_tip_id
                where g."ctto_id"='.$ctto_id.' ORDER BY g.gtia_id  asc';
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    /*================================================================================================*/
    /*================================= GARANTIA ID ======================================*/
    public function garantia_id($gtia_id)
    {
        $sql = 'SELECT g.*
                from boleta_garantia as g
                Inner Join boleta_garantia_tipo as tip On g.gtia_tip_id=tip.gtia_tip_id
                where g."gtia_id"='.$gtia_id.' ORDER BY g.gtia_id  asc';
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    /*================================================================================================*/
    /*================================= NRO SUPERVISION Y EVALUACION DE LA FASE ACTIVA ======================================*/
    public function nro_ejecucion_fase_mes($id_f)
    {
        $this->db->from('fase_ejecucion');
        $this->db->where('pfec_id', $id_f);
        $this->db->where('g_id', $this->session->userdata("gestion"));
        $query = $this->db->get();
        return $query->num_rows();
    }
    /*================================================================================================*/ 
    /*================================= NRO SUPERVISION Y EVALUACION ======================================*/
    public function nro_ejecucion_componente($id_c)
    {
        $this->db->from('fase_ejecucion');
        $this->db->where('com_id', $id_c);
        $this->db->where('g_id', $this->session->userdata("gestion"));
        $query = $this->db->get();
        return $query->num_rows();
    }
    /*================================================================================================*/ 
    /*================================= SUPERVISION Y EVALUACIO ======================================*/
    public function ejecucion_componente($id_c)
    {
        $this->db->from('fase_ejecucion');
        $this->db->where('com_id', $id_c);
        $this->db->where('g_id', $this->session->userdata("gestion"));
        $this->db->order_by("m_id","asc");
        $query = $this->db->get();
        return $query->result_array();
    }
    /*================================================================================================*/

    /*=================================== LISTA DE OBJETIVOS ESTRATEGICOS PROGRAMADO ====================================*/
    public function oe_programado($obje_id,$gestion)
    {
        $sql = 'SELECT *
            from obje_prog_gestion
            where obje_id='.$obje_id.' and g_id='.$gestion.''; 
        $query = $this->db->query($sql);
        return $query->result_array();

    }
    /*==============================================================================================================*/ 
    /*=================================== LISTA DE OBJETIVOS ESTRATEGICOS PROGRAMADO ====================================*/
    public function verif($obje_id,$gestion)
    {
        $this->db->from('oe_observacion');
        $this->db->where('obje_id', $obje_id);
        $this->db->where('g_id', $gestion);
        $query = $this->db->get();
        
        if($query->num_rows()!=0)
        {
            return 1;
        }
        else
        {
            return 0;
        }

    }
    /*==============================================================================================================*/ 
    /*=================================== LOBJETIVO ESTRATEGICO ====================================*/
    public function oe($obje_id)
    {
        $this->db->from('objetivosestrategicos');
        $this->db->where('obje_id', $obje_id);
        $query = $this->db->get();
        return $query->result_array();
    }
    /*=================================== LISTA DE PRODUCTOS EJECUTADO GESTION  ====================================*/
    public function oe_ejecutado($obje_id,$gestion)
    {
        $this->db->from('obje_ejec_gestion');
        $this->db->where('obje_id', $obje_id);
        $this->db->where('g_id', $gestion);
        $query = $this->db->get();
        return $query->result_array();
    }
    /*=================================== LISTA DE OBJETIVOS ESTRATEGICOS PROGRAMADO ====================================*/
    public function nro_oe_programado($obje_id,$gestion)
    {
        $this->db->from('obje_prog_gestion');
        $this->db->where('obje_id', $obje_id);
        $this->db->where('g_id', $gestion);
        $query = $this->db->get();
        return $query->num_rows();

    }
    /*==============================================================================================================*/ 
    /*=================================== LISTA DE PRODUCTOS EJECUTADO GESTION  ====================================*/
    public function nro_oe_ejecutado($obje_id,$gestion)
    {
        $this->db->from('obje_ejec_gestion');
        $this->db->where('obje_id', $obje_id);
        $this->db->where('g_id', $gestion);
        $query = $this->db->get();
        return $query->num_rows();
    }

    /*============================ BORRA DATOS DE OBJETIVOS GESTION =================================*/
    public function delete_oe_id($obje_id,$gest){ 
        $this->db->where('obje_id', $obje_id);
        $this->db->where('g_id', $gest);
        $this->db->delete('obje_ejec_gestion'); 
    }
    /*====================================================================================================*/
    /*=================================== AGREGAR OE  EJECUTADO GESTION ====================================*/
    public function add_oe_ejec($obje_id,$gestion,$e,$ea,$eb)
    {
        $data = array(
            'obje_id' => $obje_id,
            'oem_ejecutado' => $e,
            'oem_ejecutado_a' => $ea,
            'oem_ejecutado_b' => $eb,
            'g_id' => $gestion,
        );
        $this->db->insert('obje_ejec_gestion',$data);
    }
    /*==============================================================================================================*/
    /*================================= GET EJECUCION OE ======================================*/
    public function get_ejecucion_oe($obje_id,$gestion)
    {
        $this->db->from('oe_ejecucion');
        $this->db->where('obje_id', $obje_id);
        $this->db->where('g_id', $gestion);
        $query = $this->db->get();
        return $query->result_array();
    }
    /*================================================================================================*/  
    /*================================= SUPERVISION Y EVALUACIO ======================================*/
    public function oe_observacion($obje_id,$gestion)
    {
        $this->db->from('oe_observacion');
        $this->db->where('obje_id', $obje_id);
        $this->db->where('g_id', $gestion);
        $query = $this->db->get();
        return $query->result_array();
    }
    /*================================================================================================*/    
    /*================================= DATOS OBSERVACION EJECUCION ======================================*/
    public function observaciones_id_oe($obs_id)
    {
        $this->db->from('oe_observacion');
        $this->db->where('obs_id', $obs_id);
        $query = $this->db->get();
        return $query->result_array();
    }
    /*================================================================================================*/ 
    /*================================= ARCHIVOS EJECUCION OE ======================================*/
    public function archivos_oe($obje_id,$gest)
    {
        $this->db->from('oe_adjuntos');
        $this->db->where('obje_id', $obje_id);
        $this->db->where('g_id', $gest);
        $query = $this->db->get();
        return $query->result_array();
    }
    /*================================================================================================*/ 
    /*================================= ARCHIVO ID ======================================*/
    public function archivos_id_oe($oe_id)
    {
        $this->db->from('oe_adjuntos');
        $this->db->where('oe_id', $oe_id);
        $query = $this->db->get();
        return $query->result_array();
    }
    /*================================================================================================*/ 

}
