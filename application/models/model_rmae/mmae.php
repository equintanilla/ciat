<?php
class Mmae extends CI_Model {
    public function __construct(){
        $this->load->database();
        $this->gestion = $this->session->userData('gestion');
        $this->mes = $this->session->userData('mes');
    }

    
    /*---------- lista de unidad ejecutora ------------*/
    public function unidades_ejecutoras(){
        $sql = 'select *
                from unidadorganizacional
                where uni_ejecutora=\'1\' and uni_estado!=\'3\'
                order by uni_id asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*-------------- Get Unidad Ejecutora ------------*/
    public function get_unidad_organizacional($uni_id){
        $sql = 'select *
                from unidadorganizacional
                where uni_id='.$uni_id.'';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*-------------- Get monto sigep por aperturas y mes actual ------------*/
    public function get_monto_sigep_mes($aper_id,$mes_id){
        $sql = 'select *
                from _proy_ejec_ppto
                where aper_id='.$aper_id.' and m_id='.$mes_id.' and pe_estado!=\'3\'';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*--------------- Lista de Proyectos por Unidad Ejecutora ------------------*/
    public function get_proyectos_ue($ue_id,$mes_id,$sql_in){

        $sql = 'select ue.uni_id as ue_id,ue.uni_unidad,p.proy_id,p.proy_nombre,tp.tp_id,tp.tp_tipo,pf.*,fun.*,pe.*,fun_paterno|| \' \' ||fun_materno|| \' \' ||fun_nombre as responsable,fun_telefono
            from aperturaprogramatica a
            Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
            Inner Join _proyectos as p On ap.proy_id = p.proy_id
            Inner Join _tipoproyecto as tp On p.tp_id=tp.tp_id
            Inner Join _proyectofuncionario as pfun On p.proy_id = pfun.proy_id
            Inner Join funcionario as fun On fun.fun_id = pfun.fun_id
            Inner Join unidadorganizacional as ue On ue.uni_id = pfun.uni_ejec
            Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id
            Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
            where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and a.aper_gestion='.$this->gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') 
            and pf.pfec_estado=\'1\' and p.tp_id '.$sql_in.' and pfun.pfun_tp=\'1\' and ue.uni_ejecutora=\'1\' and ue.uni_id='.$ue_id.'
            and pe.m_id='.$mes_id.' order by p.tp_id asc';


        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*--------------------- lista de proyectos (todos) ----------------------*/
    public function lista_proyectos_todos($f1,$f2,$p1,$p2,$p3,$p4){
        $fe1=0;$fe2=0;$tp1=0;$tp2=0;$tp3=0;$tp4=0;
        if($f1==1){$fe1=1;}
        if($f2==1){$fe2=2;}

        if($p1==1){$tp1=1;}
        if($p2==1){$tp2=2;}
        if($p3==1){$tp3=3;}
        if($p4==1){$tp4=4;}

        $sql = 'select ue.uni_id as ue_id,ue.uni_unidad as ue,p.proy_id,p.proy_nombre,tp.tp_tipo,pf.*,fun.*
            from aperturaprogramatica a
            Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
            Inner Join _proyectos as p On ap.proy_id = p.proy_id
            Inner Join _tipoproyecto as tp On p.tp_id=tp.tp_id
            Inner Join _proyectofuncionario as pfun On p.proy_id = pfun.proy_id
            Inner Join funcionario as fun On fun.fun_id = pfun.fun_id
            Inner Join unidadorganizacional as ue On ue.uni_id = pfun.uni_ejec
            Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id
            where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and a.aper_gestion='.$this->gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and pf.pfec_estado=\'1\' and (pf.pfec_ejecucion='.$fe1.' or pf.pfec_ejecucion='.$fe2.')and (p.tp_id='.$tp1.' or p.tp_id='.$tp2.' or p.tp_id='.$tp3.' or p.tp_id='.$tp4.') and pfun.pfun_tp=\'1\' and ue.uni_ejecutora=\'1\'
            order by ue.uni_id , p.tp_id asc';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

	/*--------------- Lista de Proyectos por Sector ------------------*/
	public function get_proyectos_sector($cod_sect,$mes_id,$sql_in){
		$sql = 'select ue.uni_id as ue_id,ue.uni_unidad,clas.codsectorial as cod_sec,clas.sector as sector,p.proy_id,p.proy_nombre,tp.tp_id,tp.tp_tipo,pf.*,fun.*,pe.*,fun_paterno|| \' \' ||fun_materno|| \' \' ||fun_nombre as responsable,fun_telefono
            from aperturaprogramatica a
            Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
            Inner Join _proyectos as p On ap.proy_id = p.proy_id
            Inner Join _tipoproyecto as tp On p.tp_id=tp.tp_id
            Inner Join _proyectofuncionario as pfun On p.proy_id = pfun.proy_id
            Inner Join funcionario as fun On fun.fun_id = pfun.fun_id
            Inner Join unidadorganizacional as ue On ue.uni_id = pfun.uni_ejec
            Inner Join (select * from _clasificadorsectorial where codsec in(select codsec from _clasificadorsectorial where codsectorial=\''.$cod_sect.'\')) as clas On clas.codsectorial = p.codsectorial 
            Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id
            Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id
            where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and a.aper_gestion='.$this->gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') 
            and pf.pfec_estado=\'1\' and p.tp_id '.$sql_in.' and pfun.pfun_tp=\'1\' and ue.uni_ejecutora=\'1\'
            and pe.m_id='.$mes_id.' order by p.tp_id asc';
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	/*--------------- Lista de Proyectos por Estado ------------------*/
	public function get_proyectos_estado($ep_id,$mes_id,$sql_in){
		$sql = 'select distinct ue.uni_id as ue_id,ue.uni_unidad, est.ep_id,est.ep_descripcion, p.proy_id,p.proy_nombre,tp.tp_id,tp.tp_tipo,pf.*,fun.*,pe.*,fun_paterno|| \' \' ||fun_materno|| \' \' ||fun_nombre as responsable,fun_telefono
            from aperturaprogramatica a
            Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
            Inner Join _proyectos as p On ap.proy_id = p.proy_id
            Inner Join _tipoproyecto as tp On p.tp_id=tp.tp_id
            Inner Join _proyectofuncionario as pfun On p.proy_id = pfun.proy_id
            Inner Join funcionario as fun On fun.fun_id = pfun.fun_id
            Inner Join unidadorganizacional as ue On ue.uni_id = pfun.uni_ejec
            Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id
            Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
            Inner Join (select * from fase_ejecucion fe
            Inner Join _estadoproyecto as est On est.ep_id = fe.estado where estado='.$ep_id.' and m_id='.$mes_id.') as est On est.pfec_id = pf.pfec_id 
            where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and a.aper_gestion='.$this->gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') 
            and pf.pfec_estado=\'1\' and p.tp_id '.$sql_in.' and pfun.pfun_tp=\'1\' and ue.uni_ejecutora=\'1\'
            and pe.m_id='.$mes_id.' and pe.pe_estado!=\'3\'';
		$query = $this->db->query($sql);
		return $query->result_array();
	}
   /*================== SECTOR ECONOMICO ==================*/
    /*-------------- Lista de Sectores ------------*/
    public function list_sectores(){
        $sql = 'select *
                from _clasificadorsectorial
                where nivel=\'1\' and estado!=\'3\' 
                order by codsectorial asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }
	/*-------------- Get Sector ------------*/
	public function get_sector($cod_sector){
		$sql = 'select *
                from _clasificadorsectorial
                where codsectorial=\''.$cod_sector.'\'';

		$query = $this->db->query($sql);
		return $query->result_array();
	}

	/*-------------Estado del proyecto-------*/
	public function get_estado($ep_id){
		$sql = 'select *
                from _estadoproyecto
                where ep_id='.$ep_id;

		$query = $this->db->query($sql);
		return $query->result_array();
	}

	/*-------------- Lista de Sectores ------------*/
	public function list_estados(){
		$sql = 'select *
                from _estadoproyecto
                where ep_estado=\'1\' 
                order by ep_descripcion desc';

		$query = $this->db->query($sql);
		return $query->result_array();
	}
	/*-----------Municipios por proyecto---------*/
	public function proyecto_municipio($proy_id,$prov_id)
	{
		$sql = 'select mun.* 
			from _proyectosmunicipios pm
			inner join _municipios as mun on mun.muni_id=pm.muni_id
			where pm.proy_id='.$proy_id.' and mun.prov_id='.$prov_id.'
			order by mun.muni_municipio';
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	public function proyecto_solo_municipio($proy_id)
	{
		$sql = 'select mun.* 
			from _proyectosmunicipios pm
			inner join _municipios as mun on mun.muni_id=pm.muni_id
			where pm.proy_id='.$proy_id.' 
			order by mun.muni_municipio';
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	public function proyecto_provincia($proy_id){
		$sql = 'select p.*
			from _proyectosprovincias as pp
			inner join _provincias as p on pp.prov_id=p.prov_id
			where pp.proy_id='.$proy_id.'
			order by prov_provincia';
		$query=$this->db->query($sql);
		return $query->result_array();
	}
}