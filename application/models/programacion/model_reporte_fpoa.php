<?php

class Model_reporte_fpoa extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    /*======================== OBJETIVO ESTRATEGICO =========================*/
    /*---------------------Get Objetivo id  ---------------------------*/
    public function get_objetivo($obje_id)
    {
         $sql = 'select *
                 from objetivosestrategicos
                 where obje_id='.$obje_id.'';

        $query = $this->db->query($sql);
        return $query->result_array();
    }
        /*---------------------Og programado ---------------------------*/
    public function oestrategico_programado($obje_id)
    {
         $sql = 'select *
                 from obje_prog_gestion
                 where obje_id='.$obje_id.'';

        $query = $this->db->query($sql);
        return $query->result_array();
    }
    /*---------------------Og ejecutado absoluto ---------------------------*/
    public function oestrategico_ejecutado($obje_id)
    {
         $sql = 'select *
                 from obje_ejec_gestion
                 where obje_id='.$obje_id.'';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

}