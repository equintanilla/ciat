<?php

class Mponderacion extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    //RELACION POA OBJETIVOS ESTRATEGICOS
    function poa_obje_estrategico($poa_id)
    {
        $aper_id = $this->get_poa($poa_id)->aper_id;
        $this->db->select('o.o_id,o.o_codigo,o.o_objetivo,o.o_ponderacion,f.fun_nombre,f.fun_paterno,f.fun_materno,
        (SELECT get_unidad(f.uni_id)) as uni_unidad');
        $this->db->from('poaobjetivosestrategicos po');
        $this->db->join('objetivosgestion o ', 'o.obje_id  = po.obje_id','INNER');
        $this->db->join('funcionario f ', 'f.fun_id  = o.fun_id', 'left');
        $this->db->where('po.poa_id ', $poa_id);
        $this->db->where('o.aper_id', $aper_id);
        $this->db->where('o.o_estado IN (1,2)');
        $this->db->where('o.o_gestion', $this->session->userData('gestion'));
        $this->db->ORDER_BY('o_id', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    //GUARDAR LA PONDERACION
    function add_pond_ogestion($o_id, $ponderacion)
    {
        $data['o_ponderacion'] = $ponderacion;
        $this->db->WHERE('o_id ', $o_id);
        return $this->db->UPDATE('objetivosgestion', $data);
    }

    //OBTENER DATOS DEL POA
    function get_poa($poa_id)
    {
        $this->db->SELECT('*');
        $this->db->FROM('vista_poa');
        $this->db->WHERE('poa_id', $poa_id);
        $query = $this->db->get();
        return $query->row();
    }

    //GUARDAR PONDERACION DE PRODUCTO TERMINAL
    function add_pond_pterminal($pt_id, $ponderacion)
    {
        $data['pt_ponderacion'] = $ponderacion;
        $this->db->WHERE('pt_id ', $pt_id);
        return $this->db->UPDATE('_productoterminal', $data);
    }

    //FUNCION QUE RETORNA DATOS DEL POA FILTRADO POR ID, gestion
    function dato_poa($aper_id)
    {
        $this->db->SELECT('*');
        $this->db->FROM('vista_poa');
        $this->db->WHERE('aper_id', $aper_id);
        $query = $this->db->get();
        return $query->row();
    }

    //GUARDAR PONDERACION DEL PROGRAMA
    function add_pond_programa($aper_id, $ponderacion)
    {
        $data['aper_ponderacion'] = $ponderacion;
        $this->db->WHERE('aper_id ', $aper_id);
        return $this->db->UPDATE('aperturaprogramatica', $data);
    }

    //SUMAR EL PRESUPUESTO POR APERTURA HIJAS Y PROYECTO
    function suma_aper_proy($aper_programa)
    {
        $gestion = $this->session->userdata('gestion');
        $sql = "
        SELECT COALESCE (SUM(q.pfecg_ppto_total),0) AS suma
        FROM aperturaprogramatica a
        INNER JOIN aperturaproyectos ap ON a.aper_id = ap.aper_id
        INNER JOIN _proyectofaseetapacomponente p ON ap.proy_id = p.proy_id
        INNER JOIN ptto_fase_gestion q ON q.pfec_id = p.pfec_id
        WHERE a.aper_programa = '" . $aper_programa . "' AND (a.aper_proyecto <> '0000' OR  a.aper_actividad <> '000') AND a.aper_gestion = " . $gestion . "
        AND p.pfec_estado = 1 AND a.aper_estado IN (1,2) AND p.estado IN (1,2) AND q.g_id = " . $gestion . " AND q.estado IN (1,2) AND a.aper_estado IN (1,2)
        GROUP BY aper_programa";
        $query = $this->db->query($sql);
        return $query->row();
    }

    //obtener suma total del presupuesto por programa y proyecto
    function sum_total_prog_proy()
    {
        $gestion = $this->session->userdata('gestion');
        $sql = '
        SELECT SUM(q.pfecg_ppto_total) AS suma_asignado
        FROM aperturaprogramatica a
        INNER JOIN aperturaproyectos ap ON a.aper_id = ap.aper_id
        INNER JOIN _proyectofaseetapacomponente p ON ap.proy_id = p.proy_id
        INNER JOIN ptto_fase_gestion q ON q.pfec_id = p.pfec_id
        WHERE a.aper_gestion = ' . $gestion . ' AND a.aper_estado IN (1,2) AND p.pfec_estado = 1 AND p.estado IN (1,2) AND q.g_id = ' . $gestion . ' AND q.estado IN (1,2)
        GROUP BY a.aper_gestion';
        $query = $this->db->query($sql);
        return $query->row();
    }

    //LISTA DE PROYECTOS
    function lista_proyectos($aper_programa)
    {
        $gestion = $this->session->userdata('gestion');
        $sql = "
            SELECT a.aper_programa,a.aper_proyecto,a.aper_actividad,ap.proy_id,r.proy_codigo,r.proy_nombre,r.proy_sisin,r.proy_ponderacion,q.pfecg_ppto_total
            FROM aperturaprogramatica a
            INNER JOIN aperturaproyectos ap ON a.aper_id = ap.aper_id
            INNER JOIN _proyectofaseetapacomponente p ON ap.proy_id = p.proy_id
            INNER JOIN ptto_fase_gestion q ON q.pfec_id = p.pfec_id
            INNER JOIN _proyectos r ON r.proy_id = ap.proy_id
            WHERE a.aper_programa = '" . $aper_programa . "' AND a.aper_gestion = " . $gestion . " AND a.aper_estado IN (1,2) AND p.pfec_estado = 1
            AND p.estado IN (1,2) AND q.g_id = " . $gestion . " AND q.estado IN (1,2) AND r.estado IN (1,2)
            ORDER BY a.aper_programa,a.aper_proyecto,a.aper_actividad";
        $query = $this->db->query($sql);
        return $query->result_array();

    }

    //OBTENER DATOS DEL PROYETO
    function dato_proyecto($proy_id)
    {
        $this->db->select('*');
        $this->db->from('_proyectos');
        $this->db->where('proy_id', $proy_id);
        $query = $this->db->get();
        return $query->row();

    }

    //GUARDAR PONDERACION DEL PROYECTO
    function add_pond_proy($proy_id, $ponderacion)
    {
        $data['proy_ponderacion'] = $ponderacion;
        $this->db->where('proy_id ', $proy_id);
        $this->db->update('_proyectos', $data);
    }
}