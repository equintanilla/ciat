<?php

class Mobjetivo_gestion extends CI_Model
{
    var $gestion ;
    public function __construct()
    {
        $this->load->database();
        $this->gestion = $this->session->userData('gestion');
    }

    //LISTA DE OBJETIVO DE GESTION
    function lista_ogestion($obje_id, $aper_id)
    {
        $this->db->SELECT('o.*,f.fun_nombre,f.fun_paterno,f.fun_materno,(SELECT get_unidad(f.uni_id)) AS unidad,i.indi_abreviacion');
        $this->db->FROM('objetivosgestion o');
        $this->db->JOIN('funcionario f', 'o.fun_id = f.fun_id','INNER');
        $this->db->JOIN('indicador i', 'i.indi_id = o.indi_id','INNER');
        //$this->db->WHERE('f.fun_id',$this->session->userdata('fun_id'));
        $this->db->WHERE('(o.o_estado = 1 OR o.o_estado = 2)');
        $this->db->WHERE('obje_id', $obje_id);
        $this->db->WHERE('aper_id', $aper_id);
        $this->db->ORDER_BY('o.o_id', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    //META INICIAL
    function get_mbefore($o_id)
    {
        $this->db->SELECT('o.o_meta');
        $this->db->FROM('objetivosgestion_mod o');
        $this->db->WHERE('o_id', $o_id);
        $this->db->WHERE('o_estado = 1');
        $query = $this->db->get();
        return $query->result_array();
    }

    //OBETENER DATO DE OBJETIVO DE GESTION
    function get_ogestion($o_id)
    {
        $this->db->SELECT('o.*,f.fun_nombre,f.fun_paterno,f.fun_materno,(SELECT get_unidad(f.uni_id)) AS unidad,i.indi_abreviacion');
        $this->db->FROM('objetivosgestion o');
        $this->db->JOIN('funcionario f', 'o.fun_id = f.fun_id','LEFT');
        $this->db->JOIN('indicador i', 'i.indi_id = o.indi_id','LEFT');
        $this->db->WHERE('o.o_id', $o_id);
        $query = $this->db->get();
        return $query->result_array();
    }

    //OBTENER PROGRAMACION MENSUAL
    function get_prog_ogestion($o_id, $mes)
    {
        $no_existe[0]['opm_fis'] = '0';
        $this->db->SELECT('*');
        $this->db->FROM('ogestion_prog_mes');
        $this->db->WHERE('o_id', $o_id);
        $this->db->WHERE('mes_id', $mes);
        $query = $this->db->get();
        if (($query->num_rows()) == 0) {
            return $no_existe;
        } else {
            return $query->result_array();
        }
    }

    //GUARDAR OBJETIVOS DE GESTION
    function guardar_objgestion($data)
    {
        //EMPEZAR TRANSACCION
        $this->db->trans_begin();
        //GUARDAR EN TABLA OBJETIVO OBJETIVO DE GESTION
        $o_id = $this->guardar_tabla_ogestion($data,$this->gestion);//retorna mi id
        //GUARDAR EN PROGRAMACION MENSUAL - OBJETIVO DE GESTION
        $this->guardar_prog_objgestion($data, $o_id);

        if ($this->db->trans_status() === FALSE) {
            //si ha habido algún error lo debemos mostrar aquí
            $this->db->trans_rollback();

        } else {
            //en otro caso todo ha ido bien
            return $this->db->trans_commit();
        }
    }

    //GUARDAR PROGRAMACION MENSUAL DEL OBJETIVO DE GESTION
    function guardar_prog_objgestion($dato, $o_id)
    {
        for ($i = 1; $i <= 12; $i++) {
            if ($dato[('mes' . $i)] != 0) {
                $data_prog = array(
                    'o_id' => $o_id,
                    'mes_id' => $i,
                    'opm_fis' => $dato[('mes' . $i)]
                );
                $this->db->insert('ogestion_prog_mes', $data_prog);
            }
        }
    }

    //CONTADOR DE MI CODIGO DE OBJETIVO DE GESTION
    function get_cont()
    {
        $this->db->WHERE('ide', $this->gestion);
        $this->db->FROM('configuracion');
        $query = $this->db->get();
        return $query->result_array();
    }

    function guardar_tabla_ogestion($data, $gestion)
    {
        //CREAR MI CODIGO DE OBJETIVO DE GESTION
        $cont = $this->get_cont();
        $cont = $cont[0]['conf_obj_gestion'];
        $cont++;
        //------------------------------------------------
        $codigo = $this->session->userdata('name').'/OG/' . $this->session->userData('gestion') . '/00' . $cont;
        $data = array(
            'obje_id' => $data['obje_id'],
            'o_codigo' => $codigo,
            'o_objetivo' => strtoupper(trim($data['objetivo'])),
            'indi_id' => $data['tipo_indicador'],
            'o_indicador' => strtoupper(trim($data['oindicador'])),
            'o_formula' => strtoupper(trim($data['oformula'])),
            'o_linea_base' => $data['olineabase'],
            'o_meta' => $data['ometa'],
            'o_fuente_verificacion' => strtoupper(trim($data['ofuenteverificacion'])),
            'o_supuestos' => strtoupper(trim($data['osupuesto'])),
            'o_ponderacion' => $data['oponderacion'],
            'aper_id' => $data['aper_id'],
            'o_total_casos' => strtoupper(trim($data['relativoa'])),
            'o_casos_favorables' => strtoupper(trim($data['relativob'])),
            'o_casos_desfavorables' => strtoupper(trim($data['relativoc'])),
            'o_gestion' => $gestion,
            'fun_id' => $data['fun_id'],
            'o_denominador' => $data['o_denominador']
        );
        $this->db->insert('objetivosgestion', $data);
        $obje_id = $this->db->insert_id();
        //actualizar en configuracion conf objetivo de gestion
        $this->update_conf_objegestion($cont);
        return $obje_id;
    }

    //ACTUALIZAR LA CANTIDAD DE ID DE MI CONFIGURACION
    function update_conf_objegestion($cont)
    {
        $sql = "UPDATE configuracion SET conf_obj_gestion=" . $cont . " WHERE ide=".$this->gestion;
        $this->db->query($sql);
    }

    //GUARDAR INDICADOR DE DESEMPEÑO
    function add_indicador_ogestion($o_id, $data)
    {
        $this->db->WHERE('o_id', $o_id);
        return $this->db->UPDATE('objetivosgestion', $data);
    }

    //MODIFICAR OBJETIVO DE GESTION
    function mod_ogestion($data, $o_id, $gestion)
    {
        //EMPEZAR TRANSACCION
        $this->db->trans_begin();
        //MODIFICAR LA TABLA OBJETIVO OBJETIVO DE GESTION
        $this->modificar_tabla_ogestion($o_id, $data, $gestion);//retorna mi id
        //MODIFICAR PROGRAMACION MENSUAL - OBJETIVO DE GESTION
        $this->modificar_prog_objgestion($data, $o_id);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
            return $this->db->trans_commit();
        }
    }

    //MODIFICAR TABLA DE OBJETIVO DE GESTION
    function modificar_tabla_ogestion($o_id, $data, $gestion)
    {
        $data = array(
            'o_objetivo' => strtoupper(trim($data['objetivo'])),
            'indi_id' => $data['tipo_indicador'],
            'o_indicador' => strtoupper(trim($data['oindicador'])),
            'o_formula' => strtoupper(trim($data['oformula'])),
            'o_linea_base' => $data['olineabase'],
            'o_meta' => $data['ometa'],
            'o_fuente_verificacion' => strtoupper(trim($data['ofuenteverificacion'])),
            'o_supuestos' => strtoupper(trim($data['osupuesto'])),
            'o_ponderacion' => $data['oponderacion'],
            'o_total_casos' => strtoupper(trim($data['relativoa'])),
            'o_casos_favorables' => strtoupper(trim($data['relativob'])),
            'o_casos_desfavorables' => strtoupper(trim($data['relativoc'])),
            'o_gestion' => $gestion,
            'fun_id' => $data['fun_id'],
            'o_denominador' => $data['o_denominador']
        );
        $this->db->WHERE('o_id', $o_id);
        $this->db->UPDATE('objetivosgestion', $data);
    }
    //MODIFICAR PROGRAMACION DE OBJETIVO DE GESTION
    function modificar_prog_objgestion($dato, $o_id)
    {   //ELIMINAR LOS DATOS DE LA PROGRAMACION
        $data = array('o_id' => $o_id);
        $this->db->DELETE('ogestion_prog_mes', $data);
        for ($i = 1; $i <= 12; $i++) {
            if ($dato[('mes' . $i)] != 0) {
                $data_prog = array(
                    'o_id' => $o_id,
                    'mes_id' => $i,
                    'opm_fis' => $dato[('mes' . $i)]
                );
                $this->db->insert('ogestion_prog_mes', $data_prog);
            }
        }
    }
    //ELIMINAR OBJETIVO DE GESTION
    function eliminar_ogestion($o_id){
        if($this->existe_ogestion_pt($o_id) == 0 ){
            $this->db->trans_begin();
            //eliminar
            $data = array('o_id' => $o_id);
            $this->db->delete('ogestion_prog_mes', $data);
            $this->db->delete('ogestion_ejec_relativo', $data);
            $this->db->delete('ogestion_ejec_mes', $data);
            $this->db->delete('objetivosgestion', $data);
            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
            }else{
                $this->db->trans_commit();
                $respuesta = $o_id;
            }

        }else{
            $respuesta = -1;
        }
        return $respuesta;
    }
    //verificar si existe el objetivo de gestion en la tabla _producoterminal
    function existe_ogestion_pt($o_id){
        $this->db->SELECT('*');
        $this->db->FROM('_productoterminal');
        $this->db->WHERE('o_id',$o_id);
        $q = $this->db->get();
        $q = $q->num_rows();
        return $q;
    }

    //eliminar archivos de objetivo de gestion
    public function del_archivo($o_id){
        $data = array(
            'o_archivo_adjunto' => ""
        );
        $this->db->where('o_id',$o_id);
        $this->db->update('objetivosgestion', $data);
    }
}

?>
