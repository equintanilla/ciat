<?php

class Mp_terminal extends CI_Model
{
    var $gestion;
    public function __construct()
    {
        $this->load->database();
        $this->gestion = $this->session->userData('gestion');
    }

    //lista de productos terminales
    function lista_pterminal($o_id)
    {
        $this->db->SELECT('p.*,f.fun_nombre,f.fun_paterno,f.fun_materno,(SELECT get_unidad(f.uni_id)) AS unidad,i.indi_abreviacion');
        $this->db->FROM('_productoterminal p');
        $this->db->JOIN('funcionario f', 'p.fun_id = f.fun_id','LEFT');
        $this->db->JOIN('indicador i', 'i.indi_id = p.indi_id','LEFT');
        //$this->db->WHERE('f.fun_id',$this->session->userdata('fun_id'));
        $this->db->WHERE('(p.pt_estado = 1 OR p.pt_estado = 2)');
        $this->db->WHERE('p.o_id', $o_id);
        $this->db->WHERE('p.pt_gestion', $this->gestion);
        $this->db->ORDER_BY('p.pt_id', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_pterminal($pt_id)
    {
        $this->db->SELECT('p.*,f.fun_nombre,f.fun_paterno,f.fun_materno,(SELECT get_unidad(f.uni_id)) AS unidad,i.indi_abreviacion');
        $this->db->FROM('_productoterminal p');
        $this->db->JOIN('funcionario f', 'p.fun_id = f.fun_id','LEFT');
        $this->db->JOIN('indicador i', 'i.indi_id = p.indi_id','LEFT');
        $this->db->WHERE('p.pt_id', $pt_id);
        $query = $this->db->get();
        return $query->result_array();
    }

    //OBTENER PROGRAMADO MENSUAL DEL PRODUCTO TERMINAL
    function get_prog_pterminal($pt_id, $mes)
    {
        $no_existe[0]['ppm_fis'] = '0';
        $this->db->SELECT('*');
        $this->db->FROM('pt_prog_mes');
        $this->db->WHERE('pt_id', $pt_id);
        $this->db->WHERE('mes_id', $mes);
        $query = $this->db->get();
        if (($query->num_rows()) == 0) {
            return $no_existe;
        } else {
            return $query->result_array();
        }
    }

    //GUARDAR PRODUCTO TERMINAL
    function guardar_pterminal($dato_pt, $dato_prog)
    {
        //EMPEZAR TRANSACCION
        $this->db->trans_begin();
        //GUARDAR EN TABLA PRODUCTO TERMINAL
        $pt_id = $this->guardar_tabla_pterminal($dato_pt, $this->gestion);//retorna mi id
        //GUARDAR EN PROGRAMACION MENSUAL - PRODUCTO TERMINAL
        $this->guardar_prog_pterminal($dato_prog, $pt_id);
        if ($this->db->trans_status() === FALSE) {
            //si ha habido algún error lo debemos mostrar aquí
            $this->db->trans_rollback();

        } else {
            //en otro caso todo ha ido bien
            return $this->db->trans_commit();
        }
    }

    //guardar producto terminal
    function guardar_tabla_pterminal($dato_pt, $gestion)
    {
        //generar mi codigo de producto terminal
        $cont = $this->get_cont();
        $cont = $cont[0]['conf_producto_terminal'];
        $cont++;
        $codigo = $this->session->userdata('name').'/PT/' . $gestion . '/00' . $cont;
        $dato_pt['pt_codigo'] = $codigo;
        //-------------------------------------------------
        $this->db->insert('_productoterminal', $dato_pt);
        $pt_id = $this->db->insert_id();//obtner mi id
        //actualizar en configuracion conf objetivo de gestion
        $this->update_conf_productot($cont);
        return $pt_id;
    }

    //guardar la programacion mensual de producto terminal
    function guardar_prog_pterminal($dato, $pt_id)
    {
        for ($i = 1; $i <= 12; $i++) {
            if ($dato[('mes' . $i)] != 0) {
                $data_prog = array(
                    'pt_id' => $pt_id,
                    'mes_id' => $i,
                    'ppm_fis' => $dato[('mes' . $i)]
                );
                $this->db->insert('pt_prog_mes', $data_prog);
            }
        }
    }

    //ACTUALIZAR EL CONTADOR DE MI CODIGO
    function update_conf_productot($cont)
    {
        $sql = "UPDATE configuracion SET conf_producto_terminal=" . $cont . " WHERE ide=".$this->gestion;
        $this->db->query($sql);
    }

    //CONTADOR DE ID
    function get_cont()
    {
        $this->db->WHERE('ide',$this->gestion);
        $this->db->FROM('configuracion');
        $query = $this->db->get();
        return $query->result_array();
    }

    //GUARDAR INDICADOR DE DESEMPEÑO
    function add_indicador_pt($pt_id, $data)
    {
        $this->db->WHERE('pt_id', $pt_id);
        return $this->db->UPDATE('_productoterminal', $data);
    }

    //MODIFICAR PRODUCTO TERMINAL
    function mod_pterminal($pt_id, $dato_pt, $dato_prog)
    {
        $this->db->trans_begin();
        //MODIFICAR LA TABLA PRODUCTO TERMINAL
        $this->modificar_tabla_pterminal($pt_id, $dato_pt);
        //MODIFICAR PROGRAMACION MENSUAL - PRODUCTO TERMINAL
        $this->modificar_prog_pterminal($pt_id, $dato_prog);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
            return $this->db->trans_commit();
        }
    }

    //MODIFICAR TABLA DE PRODUCTO TERMINAL
    function modificar_tabla_pterminal($pt_id, $dato_pt)
    {
        $this->db->WHERE('pt_id', $pt_id);
        $this->db->UPDATE('_productoterminal', $dato_pt);
    }

    //MODIFICAR PROGRAMACION MENSUAL PRODUCTO TERMINAL
    function  modificar_prog_pterminal($pt_id, $dato_prog)
    {   //ELIMINAR LOS DATOS DE LA PROGRAMACION
        $data = array('pt_id' => $pt_id);
        $this->db->DELETE('pt_prog_mes', $data);
        for ($i = 1; $i <= 12; $i++) {
            if ($dato_prog[('mes' . $i)] != 0) {
                $data_prog = array(
                    'pt_id' => $pt_id,
                    'mes_id' => $i,
                    'ppm_fis' => $dato_prog[('mes' . $i)]
                );
                $this->db->insert('pt_prog_mes', $data_prog);
            }
        }
    }

    //ELIMINAR PRODUCTO TERMINAL
    function eliminar_pterminal($pt_id){
        if($this->existe_pterminal_productos($pt_id) == 0 ){
            $this->db->trans_begin();
            //eliminar
            $data = array('pt_id' => $pt_id);
            $this->db->delete('pt_prog_mes', $data);
            $this->db->delete('pt_ejec_relativo', $data);
            $this->db->delete('pt_ejec_mes', $data);
            $this->db->delete('_productoterminal', $data);
            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
            }else{
                $this->db->trans_commit();
                $respuesta = $pt_id;
            }

        }else{
            $respuesta = -1;
        }
        return $respuesta;
    }
    //VERIFICAR SI EXISTE EL PRODUCTO TERMINAL EN LATABLA PRODUCTOS
    function existe_pterminal_productos($pt_id){
        $this->db->SELECT('*');
        $this->db->FROM('_productos');
        $this->db->WHERE('pt_id',$pt_id);
        $q = $this->db->get();
        $q = $q->num_rows();
        return $q;
    }

    //eliminar archivos de objetivo de gestion
    public function del_archivo($pt_id){
        $data = array(
            'pt_archivo_adjunto' => ""
        );
        $this->db->where('pt_id',$pt_id);
        $this->db->update('_productoterminal', $data);
    }
}

?>
