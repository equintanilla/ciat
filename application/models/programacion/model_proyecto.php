<?php
class Model_proyecto extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
        $this->gestion = $this->session->userData('gestion');
        $this->fun_id = $this->session->userData('fun_id');

    }
    //lista de organismo financiador

    public function verif_cod($cod)
    {
        $consulta = $this->db->get_where('_proyectos',array('proy_codigo'=>$cod));
        if($consulta->num_rows()=='1')
        {return true;}
        else
        {return false;}
    }

    public function store_proyecto($data,$data1) ///// Inserta datos a tabla proyectos
    {
        $insert = $this->db->insert('_proyectos', $data);
        $insert = $this->db->insert('aperturaprogramatica', $data1);
        return $insert; 
    }

    public function update_proyecto($data,$data1,$id,$cod,$id_aper) ///// 
    {
        $this->db->where('proy_id', $id);
        $this->db->where('proy_codigo', $cod);
        $this->db->update('_proyectos', $data);


        $this->db->where('aper_id', $id_aper);
        $this->db->update('aperturaprogramatica', $data1);

        $report = array();
        $report['error'] = $this->db->_error_number();
        $report['message'] = $this->db->_error_message();
        if($report !== 0){
            return true;
        }else{
            return false;
        }
    }

     /*-------------------- LISTA TIPO CAPITALZIABLE -----------------*/
    public function list_cap()
    {
        $sql = 'select * from tb_cap';
        $query = $this->db->query($sql);
        return $query->result_array();
    }   

    /*==================== LISTA NUEVA DE ACCIONES =============================*/
    public function list_proyectos($prog,$est_proy,$tpf,$tp){
        if($this->session->userdata('rol_id')==1) ////// LISTA PARA EL SUPER ADMINISTRADOR
        {
            $sql = 'select tap.*,p.*,tp.*,fu.*
                    from _proyectos as p
                    Inner Join _tipoproyecto as tp On p.tp_id=tp.tp_id
                    Inner Join (select apy.*,apg.* from aperturaprogramatica as apg, aperturaproyectos as apy where apy.aper_id=apg.aper_id) as tap On p.proy_id=tap.proy_id
                    Inner Join (select pf."proy_id",f."fun_id",f."fun_nombre",f."fun_paterno",f."fun_materno",u."uni_unidad" as ue,ur."uni_unidad" as ur
                        from _proyectofuncionario pf
                        Inner Join funcionario as f On pf.fun_id=f.fun_id
                        Inner Join unidadorganizacional as u On pf."uni_ejec"=u."uni_id"
                        Inner Join unidadorganizacional as ur On pf.uni_resp=ur.uni_id
                    where pf.pfun_tp='.$tpf.') as fu On fu.proy_id=p.proy_id
                    where tap.aper_programa=\''.$prog.'\' and p.estado!=\'3\' and tap.aper_gestion='.$this->gestion.' and p.proy_estado='.$est_proy.' and p.tp_id='.$tp.' and tap.aper_estado!=\'3\'
                    ORDER BY tap.aper_proyecto,tap.aper_actividad  asc';
        }
        else
        {
            if(count($this->verif_componente($this->session->userdata('fun_id'),$est_proy))!=0){
                //Columna quitada de la consulta por generar duplicidad en vista en caso de que proyecto tenga más de una fase ,pfe.pfec_id
                $sql = 'select tap.aper_programa,tap.aper_proyecto,tap.aper_actividad,p.proy_id,p.proy_nombre,p.proy_sisin,p.tp_id,p.proy_pr,p.t_obs,p.proy_observacion,tp.tp_tipo,fu.fun_nombre,fu.fun_paterno,fu.fun_materno,fu.ue,fu.ur
                        from _proyectos as p
                        Inner Join _tipoproyecto as tp On p.tp_id=tp.tp_id
                        Inner Join (select apy.*,apg.* from aperturaprogramatica as apg, aperturaproyectos as apy where apy.aper_id=apg.aper_id) as tap On p.proy_id=tap.proy_id
                        Inner Join (select pf.proy_id,f.fun_id,f.fun_nombre,f.fun_paterno,f.fun_materno,u.uni_unidad as ue,ur.uni_unidad as ur
                            from _proyectofuncionario pf
                                Inner Join funcionario as f On pf.fun_id=f.fun_id
                                Inner Join unidadorganizacional as u On pf.uni_ejec=u.uni_id
                                Inner Join unidadorganizacional as ur On pf.uni_resp=ur.uni_id
                                where pf.pfun_tp='.$tpf.') as fu On fu.proy_id=p.proy_id
                        Inner Join _proyectofaseetapacomponente as pfe On p.proy_id=pfe.proy_id 
                        Inner Join _componentes as com On com.pfec_id=pfe.pfec_id       
                        where tap.aper_programa=\''.$prog.'\' and p.estado!=\'3\' and tap.aper_gestion='.$this->gestion.' and proy_estado='.$est_proy.' and (com.resp_id='.$this->fun_id.' or fu.fun_id='.$this->fun_id.') and p.tp_id='.$tp.' and tap.aper_estado!=\'3\'
                        GROUP BY tap.aper_programa,tap.aper_proyecto,tap.aper_actividad,p.proy_id,p.proy_nombre,p.proy_sisin,p.tp_id,p.proy_pr,p.t_obs,p.proy_observacion,tp.tp_tipo,fu.fun_nombre,fu.fun_paterno,fu.fun_materno,fu.ue,fu.ur
                        ORDER BY tap.aper_proyecto,tap.aper_actividad  asc'; 
            }
            else{
               $sql = 'select tap.*,p.*,tp.*,fu.*
                    from _proyectos as p
                    Inner Join _tipoproyecto as tp On p.tp_id=tp.tp_id
                    Inner Join (select apy.*,apg.* from aperturaprogramatica as apg, aperturaproyectos as apy where apy.aper_id=apg.aper_id) as tap On p.proy_id=tap.proy_id
                    Inner Join (select pf."proy_id",f."fun_id",f."fun_nombre",f."fun_paterno",f."fun_materno",u."uni_unidad" as ue,ur."uni_unidad" as ur
                        from _proyectofuncionario pf
                        Inner Join funcionario as f On pf.fun_id=f.fun_id
                        Inner Join unidadorganizacional as u On pf."uni_ejec"=u."uni_id"
                        Inner Join unidadorganizacional as ur On pf.uni_resp=ur.uni_id
                    where pf.pfun_tp='.$tpf.') as fu On fu.proy_id=p.proy_id
                    where tap.aper_programa=\''.$prog.'\' and p.estado!=\'3\' and tap.aper_gestion='.$this->gestion.' and proy_estado='.$est_proy.' and fu.fun_id='.$this->fun_id.' and p.tp_id='.$tp.' and tap.aper_estado!=\'3\'
                    ORDER BY tap.aper_proyecto,tap.aper_actividad  asc';
            }
            
        }

        $query = $this->db->query($sql);
        return $query->result_array();
    }


    /*----------------- lISTA OPERACIONES APROBADAS (ORIGINAL) ---------------*/
    public function list_proyectos_aprobados($mod_id,$prog,$est_proy,$tpf,$tp){
        if($this->session->userdata('rol_id')==1){
            $sql = 'select tap.*,p.*,tp.*,fu.*
                    from _proyectos as p
                    Inner Join _tipoproyecto as tp On p.tp_id=tp.tp_id
                    Inner Join (select apy.*,apg.* from aperturaprogramatica as apg, aperturaproyectos as apy where apy.aper_id=apg.aper_id) as tap On p.proy_id=tap.proy_id
                    Inner Join (select pf."proy_id",f."fun_id",f."fun_nombre",f."fun_paterno",f."fun_materno",u."uni_unidad" as ue,ur."uni_unidad" as ur
                        from _proyectofuncionario pf
                        Inner Join funcionario as f On pf.fun_id=f.fun_id
                        Inner Join unidadorganizacional as u On pf."uni_ejec"=u."uni_id"
                        Inner Join unidadorganizacional as ur On pf.uni_resp=ur.uni_id
                    where pf.pfun_tp='.$tpf.') as fu On fu.proy_id=p.proy_id
                    where tap.aper_programa=\''.$prog.'\' and p.estado!=\'3\' and tap.aper_gestion='.$this->gestion.' and proy_estado='.$est_proy.' and p.tp_id='.$tp.' and tap.aper_estado!=\'3\'
                    ORDER BY tap.aper_proyecto,tap.aper_actividad  asc';
        }
        else{
            $sql = 'select tap.*,p.*,tp.*,fu.*
                    from _proyectos as p
                    Inner Join _tipoproyecto as tp On p.tp_id=tp.tp_id
                    Inner Join (select apy.*,apg.* from aperturaprogramatica as apg, aperturaproyectos as apy where apy.aper_id=apg.aper_id) as tap On p.proy_id=tap.proy_id
                    Inner Join (select pf."proy_id",f."fun_id",f."fun_nombre",f."fun_paterno",f."fun_materno",u."uni_unidad" as ue,ur."uni_unidad" as ur
                        from _proyectofuncionario pf
                        Inner Join funcionario as f On pf.fun_id=f.fun_id
                        Inner Join unidadorganizacional as u On pf."uni_ejec"=u."uni_id"
                        Inner Join unidadorganizacional as ur On pf.uni_resp=ur.uni_id
                    where pf.pfun_tp='.$tpf.') as fu On fu.proy_id=p.proy_id
                    where tap.aper_programa=\''.$prog.'\' and p.estado!=\'3\' and tap.aper_gestion='.$this->gestion.' and proy_estado='.$est_proy.' and fu.fun_id='.$this->fun_id.' and p.tp_id='.$tp.' and tap.aper_estado!=\'3\'
                    ORDER BY tap.aper_proyecto,tap.aper_actividad  asc'; 
        }

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*----------------- lISTA OPERACIONES (EJECUCION) ---------------*/
    public function lista_proyectos($mod,$tp_id){
        $sql = 'select p.*,apg.*, f.*,ue.*
                from _proyectos p
                Inner Join aperturaproyectos as ap On ap.proy_id=p.proy_id
                Inner Join aperturaprogramatica as apg On apg.aper_id=ap.aper_id
                Inner Join _proyectofuncionario as pf On pf.proy_id=p.proy_id
                Inner Join funcionario as f On f.fun_id=pf.fun_id
                Inner Join unidadorganizacional as ue On pf.uni_ejec=ue.uni_id
                Inner Join unidadorganizacional as ur On pf.uni_resp=ur.uni_id

                where p.estado!=\'3\' and apg.aper_gestion='.$this->gestion.' and apg.aper_estado!=\'3\' and pf.pfun_tp=\'1\' and p.tp_id='.$tp_id.'
                order by apg.aper_programa, apg.aper_proyecto, apg.aper_proyecto asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }



    /*----------------------- VERIF PROGRAMACION DEL POA -------------------*/
    public function verif_componente($fun_id,$proy_estado)
    {
         $sql = 'select *
                from _componentes com
                Inner Join _proyectofaseetapacomponente as pfe On pfe.pfec_id=com.pfec_id
                Inner Join _proyectos as p On p.proy_id=pfe.proy_id
                where com.resp_id='.$fun_id.' and com.estado!=\'3\' and pfe.pfec_estado=\'1\' and p.proy_estado='.$proy_estado.'';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*----------------------- VERIF PROGRAMACION DEL POA -------------------*/
    public function verif_componente_pluri($fun_id)
    {
         $sql = 'select *
                from _componentes com
                Inner Join _proyectofaseetapacomponente as pfe On pfe.pfec_id=com.pfec_id
                Inner Join _proyectos as p On p.proy_id=pfe.proy_id
                where com.resp_id='.$fun_id.' and com.estado!=\'3\' and pfe.pfec_estado=\'1\' and (p.proy_estado=\'1\' or p.proy_estado=\'2\' )';
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    /*==================== OPERACIONES APROBADAS =============================*/
    public function list_operaciones_aprobadas()
    {
         $sql = 'select tap.*,p.*,pf.*,pfg.*,tp.*,fu.*
                    from _proyectos as p
                    Inner Join _tipoproyecto as tp On p.tp_id=tp.tp_id
                    Inner Join _proyectofaseetapacomponente as pf On pf.proy_id=p.proy_id
                    Inner Join ptto_fase_gestion as pfg On pfg.pfec_id=pf.pfec_id
                    Inner Join (select apy.*,apg.* from aperturaprogramatica as apg, aperturaproyectos as apy where apy.aper_id=apg.aper_id) as tap On p.proy_id=tap.proy_id
                    Inner Join (select pf.proy_id,f.fun_id,f.fun_nombre,f.fun_paterno,f.fun_materno,u.uni_unidad as ue,ur.uni_unidad as ur
                        from _proyectofuncionario pf
                        Inner Join funcionario as f On pf.fun_id=f.fun_id
                        Inner Join unidadorganizacional as u On pf.uni_ejec=u.uni_id
                        Inner Join unidadorganizacional as ur On pf.uni_resp=ur.uni_id
                    where pf.pfun_tp=\'1\') as fu On fu.proy_id=p.proy_id
                    where p.estado!=\'3\' and tap.aper_gestion='.$this->gestion.' and (proy_estado=\'4\' or proy_estado=\'5\') and pf.pfec_estado=\'1\' and g_id='.$this->gestion.'
                    ORDER BY tap.aper_programa,tap.aper_proyecto,tap.aper_actividad  asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }
    public function proyecto_funcionario($proy_id){
        $sql = 'select *
                from _proyectofuncionario
                where proy_id='.$proy_id.' and fun_id='.$this->fun_id.'';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function proyecto_funcionario_proceso($proy_id){
        $sql = 'select *
                    from _proyectos p
                    Inner Join _proyectofaseetapacomponente as pfe On p.proy_id=pfe.proy_id
                    Inner Join _componentes as com On com.pfec_id=pfe.pfec_id
                    where p.proy_id='.$proy_id.' and com.resp_id='.$this->fun_id.'';
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    public function list_proyectos_poa($prog,$est_proy,$tp)  //////////// REPORTE ACCIONES POA 
    {
         $sql = 'select tap.*,p.*,tp.*,fu.*
                    from _proyectos as p
                    Inner Join _tipoproyecto as tp On p.tp_id=tp.tp_id
                    Inner Join (select apy.*,apg.* from aperturaprogramatica as apg, aperturaproyectos as apy where apy.aper_id=apg.aper_id) as tap On p.proy_id=tap.proy_id
                    Inner Join (select pf."proy_id",f."fun_id",f."fun_nombre",f."fun_paterno",f."fun_materno",u."uni_unidad" as ue,ur."uni_unidad" as ur
                        from _proyectofuncionario pf
                        Inner Join funcionario as f On pf.fun_id=f.fun_id
                        Inner Join unidadorganizacional as u On pf."uni_ejec"=u."uni_id"
                        Inner Join unidadorganizacional as ur On pf.uni_resp=ur.uni_id
                    where pf.pfun_tp=\'1\') as fu On fu.proy_id=p.proy_id
                    where tap.aper_programa=\''.$prog.'\' and p.estado!=\'3\' and p.tp_id='.$tp.' and tap.aper_gestion='.$this->gestion.' and proy_estado='.$est_proy.' 
                    ORDER BY tap.aper_proyecto,tap.aper_actividad  asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function proy_actividades($prog,$gestion){
        $sql = 'select tap.*,p.*,tp.*,fu.*
                from _proyectos as p
                Inner Join _tipoproyecto as tp On p.tp_id=tp.tp_id
                Inner Join (select apy.*,apg.* from aperturaprogramatica as apg, aperturaproyectos as apy where apy.aper_id=apg.aper_id) as tap On p.proy_id=tap.proy_id
                Inner Join (select pf."proy_id",f."fun_id",f."fun_nombre",f."fun_paterno",f."fun_materno",u."uni_unidad" as ue,ur."uni_unidad" as ur
                        from _proyectofuncionario pf
                        Inner Join funcionario as f On pf.fun_id=f.fun_id
                        Inner Join unidadorganizacional as u On pf."uni_ejec"=u."uni_id"
                        Inner Join unidadorganizacional as ur On pf.uni_resp=ur.uni_id
                where pf.pfun_tp=\'1\') as fu On fu.proy_id=p.proy_id
                where tap.aper_programa=\''.$prog.'\' and p.estado!=\'3\' and tap.aper_gestion='.$gestion.' and proy_estado!=\'4\' and tap.aper_estado!=\'3\'
                ORDER BY tap.aper_proyecto,tap.aper_actividad  asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

     public function proy_actividades_g($prog,$gestion) ///// LISTA DE PROGRAMAS NO RECURRENTES - GERENCIA DE PROYECTOS
    {
        if($this->session->userdata('rol_id')==1) ////// LISTA PARA EL SUPER ADMINISTRADOR
        {
            $sql = 'select tap.*,p.*,tp.*,fu.*
                    from _proyectos as p
                    Inner Join _tipoproyecto as tp On p.tp_id=tp.tp_id
                    Inner Join (select apy.*,apg.* from aperturaprogramatica as apg, aperturaproyectos as apy where apy.aper_id=apg.aper_id) as tap On p.proy_id=tap.proy_id
                    Inner Join (select pf."proy_id",f."fun_id",f."fun_nombre",f."fun_paterno",f."fun_materno",u."uni_unidad" as ue,ur."uni_unidad" as ur
                        from _proyectofuncionario pf
                        Inner Join funcionario as f On pf.fun_id=f.fun_id
                        Inner Join unidadorganizacional as u On pf."uni_ejec"=u."uni_id"
                        Inner Join unidadorganizacional as ur On pf.uni_resp=ur.uni_id
                    where pf.pfun_tp=\'1\') as fu On fu.proy_id=p.proy_id
                    where tap.aper_programa=\''.$prog.'\' and p.estado!=\'3\' and tap.aper_gestion='.$this->gestion.' 
                    ORDER BY tap.aper_proyecto,tap.aper_actividad  asc';
        }
        else
        {
            if(count($this->verif_componente_pluri($this->fun_id))!=0){
                $sql = 'select tap.aper_programa,tap.aper_proyecto,tap.aper_actividad,p.proy_id,p.proy_nombre,p.proy_sisin,p.tp_id,p.proy_pr,p.t_obs,p.proy_observacion,tp.tp_tipo,fu.fun_nombre,fu.fun_paterno,fu.fun_materno,fu.ue,fu.ur,pfe.pfec_id
                        from _proyectos as p
                        Inner Join _tipoproyecto as tp On p.tp_id=tp.tp_id
                        Inner Join (select apy.*,apg.* from aperturaprogramatica as apg, aperturaproyectos as apy where apy.aper_id=apg.aper_id) as tap On p.proy_id=tap.proy_id
                        Inner Join (select pf.proy_id,f.fun_id,f.fun_nombre,f.fun_paterno,f.fun_materno,u.uni_unidad as ue,ur.uni_unidad as ur
                            from _proyectofuncionario pf
                                Inner Join funcionario as f On pf.fun_id=f.fun_id
                                Inner Join unidadorganizacional as u On pf.uni_ejec=u.uni_id
                                Inner Join unidadorganizacional as ur On pf.uni_resp=ur.uni_id
                                where pf.pfun_tp=\'1\') as fu On fu.proy_id=p.proy_id
                        Inner Join _proyectofaseetapacomponente as pfe On p.proy_id=pfe.proy_id 
                        Inner Join _componentes as com On com.pfec_id=pfe.pfec_id       
                        where tap.aper_programa=\''.$prog.'\' and p.estado!=\'3\' and tap.aper_gestion='.$this->gestion.' and pfe.pfec_estado=\'1\' and (p.proy_estado=\'1\' or p.proy_estado=\'2\' or p.proy_estado=\'3\') and (com.resp_id='.$this->session->userdata('fun_id').' or fu.fun_id='.$this->session->userdata('fun_id').')
                        GROUP BY tap.aper_programa,tap.aper_proyecto,tap.aper_actividad,p.proy_id,p.proy_nombre,p.proy_sisin,p.tp_id,p.proy_pr,p.t_obs,p.proy_observacion,tp.tp_tipo,fu.fun_nombre,fu.fun_paterno,fu.fun_materno,fu.ue,fu.ur,pfe.pfec_id
                        ORDER BY tap.aper_proyecto,tap.aper_actividad  asc'; 
            }
            else{
              $sql = 'select tap.*,p.*,tp.*,fu.*
                    from _proyectos as p
                    Inner Join _tipoproyecto as tp On p.tp_id=tp.tp_id
                    Inner Join (select apy.*,apg.* from aperturaprogramatica as apg, aperturaproyectos as apy where apy.aper_id=apg.aper_id) as tap On p.proy_id=tap.proy_id
                    Inner Join (select pf."proy_id",f."fun_id",f."fun_nombre",f."fun_paterno",f."fun_materno",u."uni_unidad" as ue,ur."uni_unidad" as ur
                        from _proyectofuncionario pf
                        Inner Join funcionario as f On pf.fun_id=f.fun_id
                        Inner Join unidadorganizacional as u On pf."uni_ejec"=u."uni_id"
                        Inner Join unidadorganizacional as ur On pf.uni_resp=ur.uni_id
                    where pf.pfun_tp=\'1\') as fu On fu.proy_id=p.proy_id
                    where tap.aper_programa=\''.$prog.'\' and p.estado!=\'3\' and tap.aper_gestion='.$this->gestion.' and fu.fun_id='.$this->fun_id.'
                    ORDER BY tap.aper_proyecto,tap.aper_actividad  asc'; 
            }
            
        }

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*============================= LISTA DE PROYECTOS-PROGRAMAS-OPERACIONES -- CONTROL SOCIAL =============================*/
    public function list_proy_prog_ope($prog,$tp)
    {
       if($tp==0){
        $sql = 'select tap.*,p.*,tp.*,fu.*
                from _proyectos as p
                Inner Join _tipoproyecto as tp On p.tp_id=tp.tp_id
                Inner Join (select apy.*,apg.* from aperturaprogramatica as apg, aperturaproyectos as apy where apy.aper_id=apg.aper_id) as tap On p.proy_id=tap.proy_id
                Inner Join (select pf."proy_id",f."fun_id",f."fun_nombre",f."fun_paterno",f."fun_materno",u."uni_unidad" as ue,ur."uni_unidad" as ur
                        from _proyectofuncionario pf
                        Inner Join funcionario as f On pf.fun_id=f.fun_id
                        Inner Join unidadorganizacional as u On pf."uni_ejec"=u."uni_id"
                        Inner Join unidadorganizacional as ur On pf.uni_resp=ur.uni_id
                where pf.pfun_tp=\'1\') as fu On fu.proy_id=p.proy_id
                where tap.aper_programa=\''.$prog.'\' and p.estado!=\'3\' and tap.aper_gestion='.$this->gestion.' and proy_estado!=\'4\' 
                ORDER BY tap.aper_proyecto,tap.aper_actividad  asc';
       }
       else{
        $sql = 'select tap.*,p.*,tp.*,fu.*
                from _proyectos as p
                Inner Join _tipoproyecto as tp On p.tp_id=tp.tp_id
                Inner Join (select apy.*,apg.* from aperturaprogramatica as apg, aperturaproyectos as apy where apy.aper_id=apg.aper_id) as tap On p.proy_id=tap.proy_id
                Inner Join (select pf."proy_id",f."fun_id",f."fun_nombre",f."fun_paterno",f."fun_materno",u."uni_unidad" as ue,ur."uni_unidad" as ur
                        from _proyectofuncionario pf
                        Inner Join funcionario as f On pf.fun_id=f.fun_id
                        Inner Join unidadorganizacional as u On pf."uni_ejec"=u."uni_id"
                        Inner Join unidadorganizacional as ur On pf.uni_resp=ur.uni_id
                where pf.pfun_tp=\'1\') as fu On fu.proy_id=p.proy_id
                where tap.aper_programa=\''.$prog.'\' and p.estado!=\'3\' and tap.aper_gestion='.$this->gestion.' and proy_estado!=\'4\' and p.tp_id='.$tp.'
                ORDER BY tap.aper_proyecto,tap.aper_actividad  asc';
       }
       
        $query = $this->db->query($sql);
        return $query->result_array();
    }

   /*============================================== CONTROL SOCIAL ====================================================*/
    public function programas_proy_control($prog,$est,$gestion)
    {
         $sql = 'SELECT p.*,tp.*,tap.*,fu.*
            from _proyectos as p
            Inner Join _tipoproyecto as tp On p."tp_id"=tp."tp_id"
            Inner Join (select apy."proy_id", apg."aper_id",apg."aper_programa", apg."aper_proyecto", apg."aper_actividad",apg."aper_descripcion",apg."aper_ponderacion" from aperturaprogramatica as apg, aperturaproyectos as apy where apy."aper_id"=apg."aper_id" and apg."aper_gestion"='.$gestion.') as tap On p."proy_id"=tap."proy_id"
            Inner Join (select pf."proy_id",f."fun_id",f."fun_nombre",f."fun_paterno",f."fun_materno",u."uni_unidad"
                        from _proyectofuncionario pf
                        Inner Join funcionario as f On pf."fun_id"=f."fun_id"
                        Inner Join unidadorganizacional as u On pf."uni_ejec"=u."uni_id"
                        where pf."pfun_tp"=\'1\') as fu On fu."proy_id"=p."proy_id"
            where tap.aper_programa=\''.$prog.'\' and proy_estado='.$est.' and estado!=\'3\' and (tap.aper_actividad=\'000\' or tap.aper_actividad=\'\') ORDER BY tap.aper_proyecto  asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }


    /*==================================== NUMERO DE RESPONSABLES DE PROYECTOS================================*/
    public function nro_resp($proy_id)
    {
        $this->db->from('_proyectofuncionario');
        $this->db->where('proy_id', $proy_id);
        $query = $this->db->get();
        return $query->num_rows();
    }
    /*======================================================================================================*/
    /*============================================================ FUNCIONARIOS RESPONSABLES ===================================================*/
    public function responsable_proy($proy_id,$tp){
        
         $sql = 'SELECT pf."proy_id",pf."fun_id",f."fun_nombre",f."fun_paterno",f."fun_materno",f."fun_cargo",f."fun_telefono",pf."uni_ejec",u1."uni_unidad" as uejec, pf."uni_resp",u2."uni_unidad" as uresp, pf."uni_adm",u3."uni_unidad" as uadm
                from _proyectofuncionario as pf
                Inner Join funcionario as f On pf."fun_id"=f."fun_id" 
                Inner Join unidadorganizacional as u1 On u1."uni_id"=pf."uni_ejec" 
                Inner Join unidadorganizacional as u2 On u2."uni_id"=pf."uni_resp"
                Inner Join unidadorganizacional as u3 On u3."uni_id"=pf."uni_adm"
                where pf."pfun_tp"='.$tp.' and pf.proy_id='.$proy_id.'';
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    /*============================================================================================== ===========================================*/

    /*========== NRO DE PROYECTOS ==========*/
    public function nro_proyectos($tp)
    {
        $this->db->from('_proyectos');
        $this->db->where('tp_id', $tp);
        $query = $this->db->get();
        return $query->num_rows();
    }

    /*=========================  HISTORIAL DE USUARIOS - PLANIFICACION ===================*/  
    public function historial_usuario($id_p)
    {
        $this->db->select("*");
        $this->db->from('_proyectofuncionario fp');
        $this->db->join('funcionario f', 'fp.fun_id = f.fun_id', 'left');
        $this->db->where('fp.proy_id',$id_p);
        $this->db->where('fp.pfun_tp',1);
        $this->db->order_by("pfun_id","asc");
        $query = $this->db->get();
        return $query->result_array();
    }

    /*============= FUNCIONARIO A ASIGNAR TOP, POA, FINANCIERO ===========*/  
     public function asig_responsables($tp){
        $sql = 'SELECT f.*,fr.*,r.*
                from funcionario f
                Inner Join fun_rol as fr On fr."fun_id"=f."fun_id" 
                Inner Join rol as r On r."r_id"=fr."r_id" 
                where r."r_id"='.$tp.' and f.fun_estado!=\'3\' and f.fun_estado!=\'0\'';
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    /*=====================================================================*/ 
    /*============= APERTURA BUSCADA  ===========*/  
     public function aper_id($proy_id,$gestion){
        $sql = 'SELECT apy."proy_id", apg."aper_id" 
                from aperturaprogramatica as apg, aperturaproyectos as apy 
                where apy."aper_id"=apg."aper_id" and apg."aper_gestion"='.$gestion.' and apy.proy_id='.$proy_id.'';
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    /*======================================================================*/ 
    public function unidades_ejecu() 
    {
        $sql = 'select *
                from unidadorganizacional
                where uni_ejecutora=\'1\' and uni_estado!=\'0\' order by uni_id';
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    
    ////// Direcciones Administrativas
    public function direcciones_administrativas() 
    {
        $sql = 'select *
                from unidadorganizacional
                where uni_adm=\'1\' and uni_estado!=\'3\' order by uni_id';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function list_unidad_org()
    {
        $this->db->from('unidadorganizacional');
        $this->db->order_by("uni_id","asc");
        $query = $this->db->get();
        return $query->result_array();
    }
    /*=========================  DATOS UNIDAD X =================== ok */
    public function get_unidad($uni_id)
    {
        $this->db->select("*");
        $this->db->from('unidadorganizacional');
        $this->db->where('uni_id',$uni_id);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function get_estado($ep_id)
    {
        $this->db->select("*");
        $this->db->from('_estadoproyecto');
        $this->db->where('ep_id',$ep_id);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function get_eje_programatica($ptdi_id)
    {
        $sql = 'select *
                from ptdi
                where ptdi_id='.$ptdi_id.' and ptdi_jerarquia=\'1\' and ptdi_gestion='.$this->session->userdata('gestion').'';
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    public function get_region($reg_id)
    {
        $sql = ' select *
                 from _regiones
                 where reg_id='.$reg_id.' and reg_estado!=\'0\'';
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    public function get_municipios($muni_id)
    {
        $sql = 'select mun.*
                from _provincias as prov
                Inner Join _municipios as mun On prov.prov_id=mun.prov_id
                where mun.muni_id='.$muni_id.' and prov.dep_id=\'2\' and prov.prov_estado=\'1\' and mun.muni_estado=\'1\'';
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    public function get_provincia($prov_id)
    {
        $sql = ' select *
                 from _provincias as prov
                 where prov.prov_id='.$prov_id.' and prov.dep_id=\'2\'';
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    public function tip_proy()
    {
        $sql = '
            select tp.*
            from _tipoproyecto as tp
            where tp.tp_estado!=\'0\'';
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    /*==================================== ACTIVAR FASE ETAPA ================================================*/
    public function clear_usu($id_p) 
    {
      $update= pg_query("UPDATE _proyectofuncionario SET pfun_estado = '0' WHERE proy_id='".$id_p."'");
      
    }

   /*=========================  PARA LA APERTURA PROGRAMATICA ===================*/
    public function apertura_p($gest,$prog,$proy,$act)  ///// para la lista de proyectos
    {

        $this->db->from('aperturaprogramatica');
        $this->db->where('aper_gestion',$gest);
        $this->db->where('aper_programa',$prog);
        $this->db->where('aper_asignado',1);
        $this->db->where('aper_proyecto',$proy);
        $this->db->where('aper_actividad',$act);
        $this->db->where('aper_gestion',$this->session->userdata("gestion"));
        $this->db->order_by("aper_id","asc");
        $query = $this->db->get();
        return $query->result_array();
    }

 /*=========================  METAS DEL PROYECTO =================== ok*/ 
    public function metas_p($id_p)
    {
        $query=$this->db->query('SELECT * FROM _metas WHERE proy_id = '.$id_p.' AND (estado = \'1\' OR estado = \'2\') ORDER BY meta_rp ASC ');
        return $query->result_array(); 
    }
    /*========================= END  METAS DEL PROYECTO ===================*/

    /*=========================  DATOS DE LA META X =================== ok */
    public function metas_id($met_id)
    {
        $this->db->select("*");
        $this->db->from('_metas');
        $this->db->where('meta_id',$met_id);
        $query = $this->db->get();
        return $query->result_array();
    }
    /*========================= END DATOS DE LA META X  ===================*/

    /*=========================  PRIORIDAD X =================== ok */
    public function prioridad()
    {
        $this->db->from('prioridad');
        $query = $this->db->get();
        return $query->result_array();
    }
    /*========================= END DATOS DE LA META X  ===================*/

    /*========================= DATOS DEL PROYECTO X ===================*/
    function get_id_proyecto($id_p) ///// todo el proyecto x
    {

        $query=$this->db->query('SELECT p."proy_id", 
                                        p."proy_nombre",
                                        p."proy_codigo",
                                        p."proy_sisin",
                                        p."proy_estado",
                                        p."proy_gestion_inicio_ddmmaaaa" as f_inicial,    
                                        p."proy_gestion_fin_ddmmaaaa" as f_final,
                                        p."proy_gestion_impacto" as duracion,

                                        p."proy_desc_problema" as desc_prob,
                                        p."proy_desc_solucion" as desc_sol,
                                        p."proy_obj_general" as obj_gral,
                                        p."proy_obj_especifico" as obj_esp,

                                        p."proy_gestion_fin" as fin,
                                        p."proy_gestion_inicio" as inicio,

                                      
                                        p."proy_descripcion_proyecto",
                                        p."proy_observacion",
                                        p."proy_poblac_beneficiaria",
                                        p."area_id",
                                        p."proy_poblac_beneficiada",
                                        p."proy_porcent_poblac_beneficiada",
                                        p.t_obs,
                                        p."codsectorial",
                                        p."proy_ponderacion",
                                        p."cp_id",
                                        p."comp",
                                        p."proy_pr",
                                        tp."tp_id",
                                        tp."tp_tipo" as tipo,
                                        tp."tp_tipo",
                                        tg."tg_id",
                                        tg."tg_descripcion",
                                        
                                        tap."aper_id",
                                        tap."aper_programa", 
                                        tap."aper_proyecto", 
                                        tap."aper_actividad", 
                                        tap."aper_descripcion", 

                                        p."fifu_id",
                                        p."pdes_id",
                                        p."ptdi_id",
                                        p."proy_nro_hombres",
                                        p."proy_nro_mujeres",
                                        fu.*
                                        
        FROM "public"."_proyectos" as p
        Inner Join "public"."_tipoproyecto" as tp On p."tp_id"=tp."tp_id"
        Inner Join "public"."tipo_gasto" as tg On p."tg_id"=tg."tg_id"
        Inner Join (select apy."proy_id", apg."aper_id",apg."aper_programa", apg."aper_proyecto", apg."aper_actividad",apg."aper_descripcion" from "public"."aperturaprogramatica" as apg, "public"."aperturaproyectos" as apy where apy."aper_id"=apg."aper_id" and apg."aper_gestion"='.$this->session->userdata("gestion").') as tap On p."proy_id"=tap."proy_id"
        Inner Join (select pf."proy_id",f."fun_id",f."fun_nombre",f."fun_paterno",f."fun_materno",u."uni_unidad" as ue,ur."uni_unidad" as ur
                        from _proyectofuncionario pf
                        Inner Join funcionario as f On pf."fun_id"=f."fun_id"
                        Inner Join unidadorganizacional as u On pf."uni_ejec"=u."uni_id"
                        Inner Join unidadorganizacional as ur On pf.uni_resp=ur.uni_id
                        where pf."pfun_tp"=\'1\') as fu On fu."proy_id"=p."proy_id"
        where p."proy_id"='.$id_p.'');

        return $query->result_array();
    }

    /*-------------------------------------------- CONSULTA PARA BUSCAR PEDES X ---------------------------------------*/
    function datos_pedes($id_pd) 
    {
        $query=$this->db->query('SELECT p."pdes_id",
                                        pdes."id1",
                                        pdes."pilar",
                                        pdes."id2",
                                        pdes."meta",
                                        pdes."id3",
                                        pdes."resultado",
                                        pdes."id4",
                                        pdes."accion"
                                        
        FROM "public"."pdes" as p
        Inner Join (SELECT p1."pdes_codigo" as id1, p1."pdes_descripcion" as pilar ,p2."pdes_codigo" as id2, p2."pdes_descripcion" as meta, p3."pdes_codigo" as id3, p3."pdes_descripcion" as resultado, p4."pdes_codigo" as id4, p4."pdes_descripcion" as accion, p4."pdes_id"
        FROM "public"."pdes" as p3
        Inner Join (select pdes_depende,pdes_descripcion, pdes_codigo,pdes_id from pdes where pdes_jerarquia=\'4\' and pdes_estado=\'1\') as p4 On p3."pdes_codigo"=p4."pdes_depende"
        Inner Join (select pdes_depende,pdes_descripcion, pdes_codigo from pdes where pdes_jerarquia=\'2\' and pdes_estado=\'1\') as p2 On p2."pdes_codigo"=p3."pdes_depende"
        Inner Join (select pdes_depende,pdes_descripcion, pdes_codigo from pdes where pdes_jerarquia=\'1\' and pdes_estado=\'1\') as p1 On p1."pdes_codigo"=p2."pdes_depende"
        ) as pdes On p."pdes_id"=pdes."pdes_id"
        where p."pdes_id"='.$id_pd.'');
        return $query->result_array();
    }

     /*-------------------------------------------- CONSULTA PARA BUSCAR PEDES X ---------------------------------------*/
    function datos_ptdi($id_ptdi) 
    {
        $query=$this->db->query('SELECT p."ptdi_id",
                                        ptdi."id1",
                                        ptdi."pilar",
                                        ptdi."id2",
                                        ptdi."meta",
                                        ptdi."id3",
                                        ptdi."resultado"
                                        
        FROM "public"."ptdi" as p
        Inner Join (SELECT p1."ptdi_codigo" as id1, p1."ptdi_descripcion" as pilar ,p2."ptdi_codigo" as id2, p2."ptdi_descripcion" as meta, p3."ptdi_codigo" as id3, p3."ptdi_descripcion" as resultado, p3."ptdi_id"
        FROM "public"."ptdi" as p3
        Inner Join (select ptdi_depende,ptdi_descripcion, ptdi_codigo from ptdi where ptdi_jerarquia=\'2\' and ptdi_estado=\'1\') as p2 On p2."ptdi_codigo"=p3."ptdi_depende"
        Inner Join (select ptdi_depende,ptdi_descripcion, ptdi_codigo from ptdi where ptdi_jerarquia=\'1\' and ptdi_estado=\'1\') as p1 On p1."ptdi_codigo"=p2."ptdi_depende"
        ) as ptdi On p."ptdi_id"=ptdi."ptdi_id"

        where p."ptdi_id"='.$id_ptdi);
        return $query->result_array();
    }

    /*-------------------------------------------- CONSULTA PARA BUSCAR FINALIDAD FUNCION X ---------------------------------------*/
    function datos_finalidad($fifu_id) 
    {
        $query=$this->db->query('SELECT f1."fifu_id" as f1,f1."fifu_finalidad_funcion" as finalidad,f2."fifu_id" as f2,f2."fifu_finalidad_funcion" as funcion,f3."fifu_id" as f3,f3."fifu_finalidad_funcion" as accion
        from "public"."_finalidadfuncion" as f3
        Inner Join (select fifu_id,fifu_nivel,fifu_finalidad_funcion,fifu_depende from _finalidadfuncion)as f2 On f2."fifu_id"=f3."fifu_depende" 
        Inner Join (select fifu_id,fifu_nivel,fifu_finalidad_funcion,fifu_depende from _finalidadfuncion)as f1 On f1."fifu_id"=f2."fifu_depende" 
        where f3."fifu_id"='.$fifu_id.'');

        return $query->result_array();
    }

    public function get_fifu($fifu_id)
    {
        $sql = 'select *
                from _finalidadfuncion
                where fifu_id='.$fifu_id;
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    /*-------------------------------------------- CLASIFICACION SECTORIAL ---------------------------------------*/
    function codigo_sectorial($codsec,$n) 
    { 
        $query=$this->db->query('SELECT cd3."codsectorial" as cod3, cd3."codsectorialduf" as actividad,cd3."descclasificadorsectorial" as desc3, cd2."codsectorial" as cod2,cd2."codsectorialduf" as subsector,cd2."descclasificadorsectorial" as desc2, cd1."codsectorial" as cod1,cd1."codsectorialduf" as sector,cd1."descclasificadorsectorial" as desc1
        from _clasificadorsectorial cd3
        Inner Join (SELECT codsectorial,codsectorialduf,descclasificadorsectorial,codsubsec from _clasificadorsectorial where nivel=\'2\') as cd2 On cd2."codsubsec"=cd3."codsubsec"
        Inner Join (SELECT codsectorial,codsectorialduf,descclasificadorsectorial,codsec from _clasificadorsectorial where nivel=\'1\') as cd1 On cd1."codsec"=cd3."codsec"
        where cd3."codsectorial"=\''.$codsec.'\' and cd3."nivel"='.$n.'');

        return $query->result_array();
    }

    /*=================================== AGREGAR APERTURA PROGRAMATICA ====================================*/
    public function add_apertura($ip_proy,$gestion,$prog,$proy,$act,$desc,$fun_id)
    {
             $data_to_store1 = array(
                    'aper_gestion' => $gestion,
                    'aper_programa' => $prog,
                    'aper_proyecto' => $proy,
                    'aper_actividad' => $act,
                    'aper_descripcion' => strtoupper($desc),
                    'fun_id' => $fun_id,
                );
            $this->db->insert('aperturaprogramatica', $data_to_store1);
            $id_aper=$this->db->insert_id();

            $data_to_store2 = array(
                'aper_id' => $id_aper,
                'proy_id' => $ip_proy,
            );
            $this->db->insert('aperturaproyectos', $data_to_store2);
    }

    /*=================================== AGREGAR PRODUCTO  PROGRAMADO GESTION ====================================*/
    /*=================================== UPDATE APERTURA PROGRAMATICA ====================================*/
    public function update_apertura($ip_aper,$prog,$proy,$act,$desc,$fun_id)
    {
            $update_aper = array(
                'aper_programa' => $prog,
                'aper_proyecto' => $proy,
                'aper_actividad' => $act,
                'aper_descripcion' => $desc,
                'fun_id' => $fun_id);

            $this->db->where('aper_id', $ip_aper);
            $this->db->update('aperturaprogramatica', $update_aper);
    }

    /*=================================== AGREGAR PRODUCTO  PROGRAMADO GESTION ====================================*/
    public function add_resp_proy($proy_id,$fid1,$fid2,$fid3,$uni,$ejec,$adm)
    {
                /*==============  FUNCIONARIO PROYECTO TECNICO DE PLANIFICACION ================*/
                  $data_to_store = array(
                              'proy_id' => $proy_id,
                              'fun_id' => $fid1,
                              'pfun_descripcion' => 'TECNICO DE PLANIFICACION',
                              'pfun_fecha' => date('d/m/Y h:i:s'),
                              'pfun_estado' => '1',
                              'uni_ejec' => $uni,
                              'uni_resp' => $ejec,
                              'uni_adm' => $adm,
                              'pfun_tp' => '1',
                          );
                  $this->db->insert('_proyectofuncionario', $data_to_store);
                /*============= END  FUNCIONARIO PROYECTO TECNICO DE PLANIFICACION ================*/

                  /*==============  FUNCIONARIO PROYECTO TECNICO VALIDADOR POA ================*/
                  $data_to_store = array(
                              'proy_id' => $proy_id,
                              'fun_id' => $fid2,
                              'pfun_descripcion' => 'TECNICO VALIDADOR POA',
                              'pfun_fecha' => date('d/m/Y h:i:s'),
                              'uni_ejec' => $uni,
                              'uni_resp' => $ejec,
                              'pfun_estado' => '1',
                              'uni_adm' => $adm,
                              'pfun_tp' => '2',
                          );
                  $this->db->insert('_proyectofuncionario', $data_to_store);
                /*============= END  FUNCIONARIO PROYECTO TECNICO VALIDADOR POA ================*/

                /*==============  FUNCIONARIO PROYECTO TECNICO VALIDADOR FINANCIERO ================*/
                  $data_to_store = array(
                              'proy_id' => $proy_id,
                              'fun_id' => $fid3,
                              'pfun_descripcion' => 'TECNICO VALIDADOR FINANCIERO',
                              'pfun_fecha' => date('d/m/Y h:i:s'),
                              'uni_ejec' => $uni,
                              'uni_resp' => $ejec,
                              'pfun_estado' => '1',
                              'uni_adm' => $adm,
                              'pfun_tp' => '3',
                          );
                  $this->db->insert('_proyectofuncionario', $data_to_store);
    }
    /*==============================================================================================================*/

        /*=================================== AGREGAR PRODUCTO  PROGRAMADO GESTION ====================================*/
    public function update_resp_proy($proy_id,$fid1,$fid2,$fid3,$uni,$ejec,$uni_adm)
    {
                /*==============  FUNCIONARIO PROYECTO TECNICO DE PLANIFICACION ================*/
                $update_top = array(
                                  'fun_id' => $fid1,
                                  'uni_ejec' => $uni,
                                  'uni_resp' => $ejec,
                                  'uni_adm' => $uni_adm,
                                  'pfun_estado' => '2',
                              );
                    $this->db->where('proy_id', $proy_id);
                    $this->db->where('pfun_tp', 1);
                    $this->db->update('_proyectofuncionario', $update_top);
                /*============= END  FUNCIONARIO PROYECTO TECNICO DE PLANIFICACION ================*/

                /*==============  FUNCIONARIO PROYECTO TECNICO VALIDADOR POA ================*/
                    $update_poa = array(
                                  'fun_id' => $fid2,
                                  'uni_ejec' => $uni,
                                  'uni_resp' => $ejec,
                                  'uni_adm' => $uni_adm,
                                  'pfun_estado' => '2',
                              );
                      $this->db->where('proy_id', $proy_id);
                      $this->db->where('pfun_tp', 2);
                      $this->db->update('_proyectofuncionario', $update_poa);
                /*============= END  FUNCIONARIO PROYECTO TECNICO VALIDADOR POA ================*/

                /*==============  FUNCIONARIO PROYECTO TECNICO VALIDADOR FINANCIERO ================*/
                      $update_fin = array(
                                  'fun_id' => $fid3,
                                  'uni_ejec' => $uni,
                                  'uni_resp' => $ejec,
                                  'uni_adm' => $uni_adm,
                                  'pfun_estado' => '2',
                              );
                      $this->db->where('proy_id', $proy_id);
                      $this->db->where('pfun_tp', 3);
                      $this->db->update('_proyectofuncionario', $update_fin);


                 /*==============  FUNCIONARIO PROYECTO TECNICO VALIDADOR FINANCIERO ================*/
                      $update_aper = array(
                                  'fun_id' => $fid3,
                                  'uni_ejec' => $uni,
                                  'uni_resp' => $ejec,
                                  'uni_adm' => $uni_adm,
                                  'pfun_estado' => '2',
                              );
                      $this->db->where('proy_id', $proy_id);
                      $this->db->where('pfun_tp', 3);
                      $this->db->update('_proyectofuncionario', $update_aper);
    }
    /*==============================================================================================================*/

    function fechas_proyecto($id_p) ////// para calcular las fechas - tabla proyectos
    {

        $this->db->select(' extract(years from (proy_gestion_inicio_ddmmaaaa))as inicio,
                            extract(years from (proy_gestion_fin_ddmmaaaa))as final');
        $this->db->from('_proyectos ');
        $this->db->where('proy_id', $id_p);
        $query = $this->db->get();
        return $query->result_array();
    }

    /*============================ END BORRA DATOS DE F/E GESTION =================================*/

    /*============================ BORRA LA META DEL PROYECTO=================================*/
    public function delete_meta($id_m){ 
        $this->db->where('meta_id', $id_m);
        $this->db->delete('_metas');

        $this->db->from('_metas');
        $this->db->where('meta_id', $id_m);
        $query = $this->db->get();
        return $query->num_rows(); 
    }
 
 /*======================================================= VERIFICANDO QUE EL PROYECTO TENGA DATOS HASTA ACTIVIDADES ========================*/
    public function verif_proy($proy_id)
    {
        $sql = 'SELECT p.*
            from _proyectos as p
            Inner Join (select * from _proyectofaseetapacomponente where pfec_estado=\'1\')as f On f."proy_id"=p."proy_id"
        Inner Join (select * from ptto_fase_gestion where g_id='.$this->session->userdata("gestion").')as fg On fg.pfec_id=f.pfec_id
        Inner Join _componentes as c On c.pfec_id=f.pfec_id
        Inner Join _productos as pr On pr.com_id=c.com_id
        Inner Join _actividades as ac On ac.prod_id=pr.prod_id
        where p.proy_id='.$proy_id.'';
        $query = $this->db->query($sql);
        return $query->num_rows(); 
    }
    /*============================================================================================== ===========================================*/
    /*======================================================= VERIFICANDO QUE EL PROYECTO TENGA DATOS HASTA PRODUCTOS ========================*/
    public function verif_proy_prod($proy_id)
    {
        $sql = 'SELECT p.*
            from _proyectos as p
            Inner Join (select * from _proyectofaseetapacomponente where pfec_estado=\'1\')as f On f."proy_id"=p."proy_id"
        Inner Join (select * from ptto_fase_gestion where g_id='.$this->session->userdata("gestion").')as fg On fg.pfec_id=f.pfec_id
        Inner Join _componentes as c On c.pfec_id=f.pfec_id
        Inner Join _productos as pr On pr.com_id=c.com_id
        where p.proy_id='.$proy_id.'';
        $query = $this->db->query($sql);
        return $query->num_rows(); 
    }
    /*============================================================================================== ===========================================*/
    
    /*============================ BORRA PROGRAMAS RECURRENTES =================================*/
    public function delete_proyecto($id_p,$id_ap,$id_fe,$id_c,$tp){ 

        $this->db->where('proy_id', $id_p);
        $this->db->where('aper_id', $id_ap);
        $this->db->delete('aperturaproyectos'); 

        $this->db->where('aper_id', $id_ap);
        $this->db->delete('aperturaprogramatica'); 

        if($tp==2 || $tp==3)
        {
        $this->db->where('pfec_id', $id_fe);
        $this->db->delete('_proyectofaseetapacomponentegestion');

        $this->db->where('proy_id', $id_p);
        $this->db->delete('_proyectofaseetapacomponente'); 
        }
        if($tp==4)
        {

        $this->db->where('com_id', $id_c);
        $this->db->delete('_componentesgestion'); 

        $this->db->where('pfec_id', $id_fe);
        $this->db->delete('_componentes'); 

        $this->db->where('pfec_id', $id_fe);
        $this->db->delete('_proyectofaseetapacomponentegestion');

        $this->db->where('proy_id', $id_p);
        $this->db->delete('_proyectofaseetapacomponente'); 
        }

        $this->db->where('proy_id', $id_p);
        $this->db->delete('_proyectofuncionario'); 

        $this->db->where('proy_id', $id_p);
        $this->db->delete('_metas'); 

        $this->db->where('proy_id', $id_p);
        $this->db->delete('_proyectos'); 

    }
    /*============================ BORRA PROGRAMAS RECURRENTES =================================*/
  
    /*============================ END BORRA PROYECTO  INVERSION =================================*/
    public function datos_proyecto($id_p)  //////////// lista de fases y etapa del proyecto x 
    {
        $this->db->select('*');
        $this->db->from('_proyectos ');
        $this->db->where('proy_id', $id_p);
        $query = $this->db->get();
        return $query->result_array();
    }

    /*============================ LISTA DE PTDI =================================*/
    public function lista_ptdi()
    {
        /*$this->db->from('ptdi');
        //$this->db->where('ptdi_gestion',$this->session->userdata("gestion"));
        $this->db->where('ptdi_jerarquia',1);
        $this->db->order_by("ptdi_id","asc");
        $query = $this->db->get();
        return $query->result_array();*/
        $query=$this->db->query(
            'SELECT * FROM ptdi
            WHERE ('.$this->session->userdata("gestion").'>=ptdi_gestion and '.$this->session->userdata("gestion").'<=ptdi_gestion_fin) 
                    and ptdi_jerarquia=1  
            ORDER BY ptdi_id ASC');
        return $query->result_array();
    }

    /*============================ BORRA LA APERTURA ID =================================*/
    public function delete_aper_id($aper_id){ 
        
        $this->db->where('aper_id', $aper_id);
        $this->db->delete('aperturaproyectos');

        $this->db->where('aper_id', $aper_id);
        $this->db->delete('aperturaprogramatica');
    }
    /*=========================================================================================*/
    /*=========================== END VERIFICA SI SI LA FASE ESTA ENCENDIDO/APAGADO ======================*/

    public function get_doc($id_adj) ///// obtiene datos del documento seleccionado
    {
        $this->db->select("*");
        $this->db->from('_proyecto_adjuntos');
        $this->db->where('adj_id', $id_adj);
        $query = $this->db->get();
        
        return $query->result_array();  
    }

    public function get_archivos($id_p) ////// lista de archivos del proyecto x
    {
        $query=$this->db->query(
            'SELECT * FROM _proyecto_adjuntos pa
            Inner Join funcionario as f On f."fun_id"=pa."fun_id" 
            WHERE pa."proy_id" = '.$id_p.' AND adj_estado = \'1\'AND adj_adjunto != \'null\' 
            ORDER BY proy_id ASC ');
        return $query->result_array(); 
    }

    public function nro_aperturas_hijos($prog,$proy,$act,$gest)  ///// para verificar la apertura programatica
    {
        $this->db->select("*");
        $this->db->from('aperturaprogramatica');
        $this->db->where('aper_programa', $prog);
        $this->db->where('aper_proyecto', $proy);
        $this->db->where('aper_actividad', $act);
        $this->db->where('aper_gestion', $gest);
        $this->db->where('aper_estado', 1);

        $consulta = $this->db->get();
        $cantidad_encontrados = $consulta->num_rows();

        if($cantidad_encontrados>='1')
        {return true;}
        else
        {return false;}
    }

    public function get_id_pdes($cod)  
    {
        $this->db->select("pdes_id");
        $this->db->from('pdes');
        $this->db->where('pdes_codigo', $cod);
        $query = $this->db->get();
        
        return $query->result_array();  
    }

    public function get_id_ptdi($cod)  
    {
        $this->db->select("ptdi_id");
        $this->db->from('ptdi');
        $this->db->where('ptdi_codigo', $cod);
        $query = $this->db->get();
        
        return $query->result_array();  
    }

    public function cod_proy()
    {
        $query=$this->db->query('SELECT proy_codigo FROM _proyectos ORDER BY proy_id DESC LIMIT 1');
        return $query->result_array(); 
    }

    public function get_indicador()  /// lista de indicadores
    {
        $this->db->select('*');
        $this->db->from('indicador ');
        $query = $this->db->get();
        return $query->result_array();
    }
    /*============== LISTA DE APERTURAS PROGRAMAS PADRES ============*/
    public function list_prog()  
    {   $proy='0000';
        $act='000';

        $this->db->from('aperturaprogramatica');
        $this->db->where('aper_proyecto', $proy);
        $this->db->where('aper_actividad', $act);
        $this->db->where('aper_gestion', $this->session->userdata("gestion"));
        $this->db->where('aper_asignado', 1);
        $this->db->ORDER_BY ('aper_gestion,aper_programa,aper_proyecto,aper_actividad','ASC');
        $query = $this->db->get();
        
        return $query->result_array();  
    }
    /*==================================================================*/

        /*============== LISTA DE APERTURAS PROGRAMAS PADRES CONTROL SOCIAL ============*/
    public function list_prog2($gestion)  
    {   $proy='0000';
        $act='000';

        $this->db->from('aperturaprogramatica');
        $this->db->where('aper_proyecto', $proy);
        $this->db->where('aper_actividad', $act);
        $this->db->where('aper_gestion', $gestion);
        $this->db->where('aper_asignado', 1);
        $this->db->ORDER_BY ('aper_gestion,aper_programa,aper_proyecto,aper_actividad','ASC');
        $query = $this->db->get();
        
        return $query->result_array();  
    }
    /*==================================================================*/

    /*================ ESTADO DEL PROYECTO==============*/  
    public function proy_estado()
    {
        $this->db->from('_estadoproyecto');
        $query = $this->db->get();
        return $query->result_array();
    }

    /*================ ARCHIVO DEL PROYECTO X ==============*/  
    public function get_archivo_proy($id)
    {
        $this->db->select("*");
        $this->db->from('_proyecto_adjuntos');
        $this->db->where('adj_id',$id);
        $query = $this->db->get();
        return $query->result_array();
    }
    /*================ ARCHIVO MES DEL PROYECTO X ==============*/  
    public function get_archivo_mes_proy($id)
    {
        $this->db->select("*");
        $this->db->from('fase_ejecucion_adjuntos');
        $this->db->where('fa_id',$id);
        $query = $this->db->get();
        return $query->result_array();
    }
    /*================ ARCHIVO MENSUAL X ==============*/  
    public function get_archivo_mes($id)
    {
        $this->db->select("*");
        $this->db->from('fase_ejecucion_adjuntos');
        $this->db->where('fa_id',$id);
        $query = $this->db->get();
        return $query->result_array();
    }
  /*============================ NRO DE ARCHIVOS POR MESES =================================*/
    public function nro_arch_meses($id_pr,$gest)
    {
        $this->db->from('fase_ejecucion_adjuntos');
        $this->db->where('proy_id', $id_pr);
        $this->db->where('ejec_gestion', $gest);
        $query = $this->db->get();
        return $query->num_rows();
    }

  /*============================= LOCALIZACION DEL PROYECTO =======================*/
    /*========== NRO DE DEPARATAMENTOS DEL PROYECTO X ==========*/
    public function nro_proy_dep($id)
    {
        $this->db->from('_proyectosdepartamentos');
        $this->db->where('proy_id', $id);
        $query = $this->db->get();
        return $query->num_rows();
    }
    /*================ DEPARTAMENTOS DEL PROYECTO X ==============*/  
    public function proy_dep($id)
    {
        $this->db->select("
                pd.proy_id,
                pd.dep_id,
                dep.dep_departamento
                        ");
        $this->db->from('_proyectosdepartamentos pd');
        $this->db->join('_departamentos dep', 'pd.dep_id = dep.dep_id', 'left');
        $this->db->where('pd.proy_id',$id);
        $query = $this->db->get();
        return $query->result_array();
    }
    /*========== NRO DE PROVINCIAS DEL PROYECTO X ==========*/
    public function nro_proy_prov($id)
    {
        $this->db->from('_proyectosprovincias');
        $this->db->where('proy_id', $id);
        $query = $this->db->get();
        return $query->num_rows();
    }
    /*================ PROVINCIAS DEL PROYECTO X ==============*/  
    public function proy_prov($id)
    {
        $this->db->select("
                pp.proy_id,
                pp.prov_id,
                prov.dep_id,
                prov.prov_provincia
                        ");
        $this->db->from('_proyectosprovincias pp');
        $this->db->join('_provincias prov', 'pp.prov_id = prov.prov_id', 'left');
        $this->db->where('pp.proy_id',$id);
        $query = $this->db->get();
        return $query->result_array();
    }
    /*========== NRO DE MUNICIPIOS DEL PROYECTO X ==========*/
    public function nro_proy_mun($id)
    {
        $this->db->from('_proyectosmunicipios');
        $this->db->where('proy_id', $id);
        $query = $this->db->get();
        return $query->num_rows();
    }
    /*================ MINICPIOS DEL PROYECTO X ==============*/  
    public function proy_mun($id)
    {
        $this->db->select("
                pm.proy_id,
                pm.muni_id,
                pm.pm_pondera,
                rg.reg_region,
                mun.muni_municipio,
                mun.prov_id,
                mun.muni_poblacion_hombres,
                mun.muni_polacion_mujeres
                        ");
        $this->db->from('_proyectosmunicipios pm');
        $this->db->join('_municipios mun', 'pm.muni_id = mun.muni_id', 'left');
        $this->db->join('_regiones rg', 'rg.reg_id = mun.reg_id', 'left');
        $this->db->where('pm.proy_id',$id);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function provincia_municipio($proy_id,$prov_id)
    {
        $sql = 'select *
        from _proyectosmunicipios pm
        Inner Join _municipios as m On m.muni_id=pm.muni_id
        Inner Join _regiones as rg On rg.reg_id=m.reg_id
        where pm.proy_id='.$proy_id.' and m.prov_id='.$prov_id.'';

        $query = $this->db->query($sql);
        return $query->result_array();
    }
    /*========== NRO DE CANTONES DEL PROYECTO X ==========*/
    public function nro_proy_cant($id)
    {
        $this->db->from('_proyectoscantones');
        $this->db->where('proy_id', $id);
        $query = $this->db->get();
        return $query->num_rows();
    }
    /*================ MINICPIOS DEL PROYECTO X ==============*/  
    public function proy_cant($id)
    {
        $this->db->select("
                pc.proy_id,
                pc.can_id,
                ct.muni_id,
                ct.can_canton
                        ");
        $this->db->from('_proyectoscantones pc');
        $this->db->join('_cantones ct', 'pc.can_id = ct.can_id', 'left');
        $this->db->where('pc.proy_id',$id);
        $query = $this->db->get();
        return $query->result_array();
    }

    /*================================= INDICADOR  ======================================*/
    public function indicador()
    {
        $this->db->from('indicador');
        $this->db->where('indi_estado', 1);
        $query = $this->db->get();
        return $query->result_array();
    }
    /*================================================================================================*/

    /*================================= APERTURA DE PROGRAMAS  ======================================*/
    public function mis_programas($proy_id)
    {
        $this->db->from('aperturaproyectos');
        $this->db->where('proy_id', $proy_id);
        $query = $this->db->get();
        return $query->result_array();
    }
    /*================================================================================================*/
    /*============== MI UBICACION - REGION-PROVINCIA-MUNICIPIO===================*/
    public function ubicacion_proy($proy_id)
    {
        $sql = '
        SELECT m.region,prov.prov_provincia as provincia,m.municipio
        FROM _proyectosprovincias as pp
        Inner Join _provincias as prov On pp.prov_id=prov.prov_id
        Inner Join (SELECT pm.proy_id,mun.muni_municipio as municipio, r.reg_region as region
        FROM _proyectosmunicipios as pm
        Inner Join _municipios as mun On pm.muni_id=mun.muni_id
        Inner Join _regiones as r On r.reg_id=mun.reg_id
        WHERE pm.proy_id='.$proy_id.' ORDER BY pm.pm_id ASC LIMIT \'1\') as m On m.proy_id=pp.proy_id
        WHERE pp.proy_id='.$proy_id.'
        ORDER BY pp.pp_id ASC LIMIT \'1\'';
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    /*================================================================================================*/ 
/*===================================================================== PROGRAMAS PROYECTO - REPORTES ==========================================*/
     public function programas_proyecto($prog,$gestion)
    {

            $sql = 'SELECT p.*,tp.*,tap.*,fu.*
            from _proyectos as p
            Inner Join _tipoproyecto as tp On p."tp_id"=tp."tp_id"
            Inner Join (select apy."proy_id", apg."aper_id",apg."aper_programa", apg."aper_proyecto", apg."aper_actividad",apg."aper_descripcion",apg."aper_ponderacion" from aperturaprogramatica as apg, aperturaproyectos as apy where apy."aper_id"=apg."aper_id" and apg."aper_gestion"='.$gestion.') as tap On p."proy_id"=tap."proy_id"
            Inner Join (select pf."proy_id",f."fun_id",f."fun_nombre",f."fun_paterno",f."fun_materno",u."uni_unidad"
                        from _proyectofuncionario pf
                        Inner Join funcionario as f On pf."fun_id"=f."fun_id"
                        Inner Join unidadorganizacional as u On pf."uni_ejec"=u."uni_id"
                        where pf."pfun_tp"=\'1\') as fu On fu."proy_id"=p."proy_id"
            where tap.aper_programa=\''.$prog.'\' and estado!=\'3\'  ORDER BY tap.aper_proyecto  asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }
    /*============================================================================================== ===========================================*/
    /*======================================= TIPO DE GASTO ========================================*/
    public function tip_gasto()
    {
        $sql = '
            select tg.*
            from tipo_gasto as tg
            where tg.tg_estado!=\'0\' ORDER BY tg.tg_id asc';
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    public function configuracion() 
    {
        $this->db->from('configuracion');
        $this->db->where('ide', $this->session->userdata("gestion"));
        $query = $this->db->get();
        return $query->result_array();
    }

    public function localizacion($proy_id) 
    {
        $this->db->from('vista_localizacion_dictamen');
        $this->db->where('proy_id', $proy_id);
        $query = $this->db->get();
        return $query->result_array();
    }
    /*==============================================================================================*/

    /*----------------------- OPERACIONES A NIVEL DEL ADMINISTRADOR ----------------------*/
    public function operaciones($prog,$est_proy,$tpf,$tp){
        $sql = 'select tap.*,p.*,tp.*,fu.*
                    from _proyectos as p
                    Inner Join _tipoproyecto as tp On p.tp_id=tp.tp_id
                    Inner Join (select apy.*,apg.* from aperturaprogramatica as apg, aperturaproyectos as apy where apy.aper_id=apg.aper_id) as tap On p.proy_id=tap.proy_id
                    Inner Join (select pf."proy_id",f."fun_id",f."fun_nombre",f."fun_paterno",f."fun_materno",u."uni_unidad" as ue,ur."uni_unidad" as ur
                        from _proyectofuncionario pf
                        Inner Join funcionario as f On pf.fun_id=f.fun_id
                        Inner Join unidadorganizacional as u On pf."uni_ejec"=u."uni_id"
                        Inner Join unidadorganizacional as ur On pf.uni_resp=ur.uni_id
                    where pf.pfun_tp='.$tpf.') as fu On fu.proy_id=p.proy_id
                    where tap.aper_programa=\''.$prog.'\' and p.estado!=\'3\' and tap.aper_gestion='.$this->session->userdata("gestion").' and proy_estado='.$est_proy.' and p.tp_id='.$tp.'
                    ORDER BY tap.aper_proyecto,tap.aper_actividad asc';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*================================================================================================*/
    /*============== reporte de objetivos de CORTO PLAZO===================*/
    public function reporte_objetivos_cp($gest)
    {
        $sql = '
        SELECT o.o_id,o.o_objetivo, o.o_linea_base, o.o_meta, o.aper_id
        FROM objetivosgestion as o
        WHERE o.o_gestion='.$gest.' ORDER BY o.o_id';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*================================================================================================*/
    /*============== Obtener programado ===================*/
    public function reporte_obtener_pg($j, $id)
    {
        $sql = '
        SELECT o.opm_fis
        FROM ogestion_prog_mes as o
        WHERE o.o_id='.$id.' and o.mes_id = '. $j;
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*================================================================================================*/
    /*============== Obtener ejecutado ===================*/
    public function reporte_obtener_ej($j, $id)
    {
        $sql = '
        SELECT o.oem_fis
        FROM ogestion_ejec_mes as o
        WHERE o.o_id='.$id.' and o.mes_id = '. $j;
        $query = $this->db->query($sql);
        return $query->result_array();
    }

}