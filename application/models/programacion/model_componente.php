<?php
class Model_componente extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }
    //lista de organismo financiador

    /*------------ Relacion Insumo Componente -------*/
    function imsumo_componente($com_id)
    {
        $sql = 'select *
                from insumocomponente
                where com_id='.$com_id.''; 
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    /*================================= LISTA DE COMPONENTES ======================================*/
    public function componentes_id($id_f)
    {
        $sql = 'select c.*,f.fun_id as resp_id,f.fun_id as resp_id, f.fun_nombre,f.fun_paterno,f.fun_materno,u.*
                from _componentes as c
                Inner Join funcionario as f On f.fun_id=c.resp_id
                Inner Join unidadorganizacional as u On u.uni_id=c.uni_id
                where c.pfec_id='.$id_f.' and c.estado!=\'3\' 
                order by c.com_id  asc'; 
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function componentes_fun_id($id_f,$fun_id)
    {
        $sql = 'select c.*,f.fun_id as resp_id,f.fun_id as resp_id, f.fun_nombre,f.fun_paterno,f.fun_materno,u.*
                from _componentes as c
                Inner Join funcionario as f On f.fun_id=c.resp_id
                Inner Join unidadorganizacional as u On u.uni_id=c.uni_id
                where c.pfec_id='.$id_f.' and c.estado!=\'3\' 
                order by c.com_id  asc'; //and resp_id='.$fun_id.'
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    /*================================================================================================*/
    /*================================= NRO DE COMPONENTES ======================================*/
    public function componentes_nro($id_f)
    {
        $this->db->from('_componentes');
        $this->db->where('pfec_id', $id_f);
        $query = $this->db->get();
        return $query->num_rows();
    }
    /*================================================================================================*/
    /*================================= COMPONENTE X ======================================*/
    public function get_componente($id_c)
    {
        $this->db->from('_componentes');
        $this->db->where('com_id', $id_c);
        $query = $this->db->get();
        return $query->result_array();
    }
/*================================================================================================*/

    /*============================ BORRA DATOS F/E PTTO =================================*/
    public function delete_comp($id_c){ 

        $this->db->where('com_id', $id_c);
        $this->db->delete('_componentes');
    }
    /*======================================================================================*/

/*==============================================================================================================*/
}