<?php

class Model_reporte_tipogasto extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }
    /*======================================= TIPO DE GASTO ========================================*/
    public function tip_gasto()
    {
        $sql = '
            select tg.*
            from tipo_gasto as tg
            where tg.tg_estado!=\'0\' and tg.tg_id!=0 ORDER BY tg.tg_id asc';
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    /*==============================================================================================*/
    /*================================= LISTA DE UNIDADES E CON CLASIFICACION TIPO DE GASTO ======================================*/
    public function unidades_ejecutoras($tg_id,$p1,$p2,$p3,$p4)
    {
        $tp1=0;$tp2=0;$tp3=0;$tp4=0;
        if($p1==1){$tp1=1;}
        if($p2==1){$tp2=2;}
        if($p3==1){$tp3=3;}
        if($p4==1){$tp4=4;}

        $sql = 'select ue.uni_id,ue.uni_unidad, count(ue.uni_id)as nro
                from unidadorganizacional as ue
                Inner Join _proyectofuncionario as pf On pf.uni_ejec=ue.uni_id
                Inner Join _proyectos as p On p.proy_id=pf.proy_id
                Inner Join tipo_gasto as tg On p.tg_id=tg.tg_id
                Inner Join _proyectofaseetapacomponente as fe On p.proy_id=fe.proy_id
                Inner Join (select apy.*, apg.* from aperturaprogramatica as apg, aperturaproyectos as apy where apy.aper_id=apg.aper_id and apg.aper_gestion='.$this->session->userdata("gestion").') as tap On p.proy_id=tap.proy_id
                where ue.uni_ejecutora=\'1\' and pf.pfun_tp=\'1\' and p.estado!=\'3\' and fe.pfec_estado=\'1\' and p.tg_id='.$tg_id.' and (p.tp_id='.$tp1.' or p.tp_id='.$tp2.' or p.tp_id='.$tp3.' or p.tp_id='.$tp4.')
                group by ue.uni_id
                order by ue.uni_id';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*================================= PROYECTOS SEGUN SU UNIDAD EJECUTORA Y TIPO DE GASTO ======================================*/
    public function proyectos_ue_tpgasto($uni_id,$tg_id,$p1,$p2,$p3,$p4)
    {
        $tp1=0;$tp2=0;$tp3=0;$tp4=0;
        if($p1==1){$tp1=1;}
        if($p2==1){$tp2=2;}
        if($p3==1){$tp3=3;}
        if($p4==1){$tp4=4;}

        $sql = 'select p.*,fe.*,tap.*,pf.*,f.*,tp.*,tg.*
                from unidadorganizacional as ue
                Inner Join _proyectofuncionario as pf On pf.uni_ejec=ue.uni_id
                Inner Join funcionario as f On pf.fun_id=f.fun_id
                Inner Join _proyectos as p On p.proy_id=pf.proy_id
                Inner Join _tipoproyecto as tp On p.tp_id=tp.tp_id
                Inner Join tipo_gasto as tg On p.tg_id=tg.tg_id
                Inner Join _proyectofaseetapacomponente as fe On p.proy_id=fe.proy_id
                Inner Join (select apy.*, apg.* from aperturaprogramatica as apg, aperturaproyectos as apy where apy.aper_id=apg.aper_id and apg.aper_gestion='.$this->session->userdata("gestion").') as tap On p.proy_id=tap.proy_id
                where ue.uni_id='.$uni_id.' and pf.pfun_tp=\'1\' and p.estado!=\'3\' and fe.pfec_estado=\'1\' and pf.pfun_tp=\'1\' and p.tg_id='.$tg_id.' and (p.tp_id='.$tp1.' or p.tp_id='.$tp2.' or p.tp_id='.$tp3.' or p.tp_id='.$tp4.')';
        $query = $this->db->query($sql);
        return $query->result_array();
    }
}