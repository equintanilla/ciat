<?php
class Mobjetivos extends CI_Model {
    var $gestion;
    public function __construct()
    {
        $this->load->database();
        $this->gestion = $this->session->userData('gestion');
    }
    //lista de objetivos estrategicos
    function lista_objetivos($gestion){
        $fecha_inicio = $this->get_fec_inicio();
        $fecha_inicio = $fecha_inicio->conf_gestion_desde;
        $fecha_final = $fecha_inicio + 4;
        $this->db->SELECT('*');
        $this->db->FROM('v_objetivos_estrategicos');
        $this->db->WHERE('(obje_gestion_curso BETWEEN '.$fecha_inicio.' AND '.$fecha_final.')');
        $this->db->ORDER_BY('obje_id', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    //lista de objetivos estrategicos con pt
    function lista_objetivos1($gestion){
        $fecha_inicio = $this->get_fec_inicio();
        $fecha_inicio = $fecha_inicio->conf_gestion_desde;
        $fecha_final = $fecha_inicio + 4;
        $this->db->SELECT('*');
        $this->db->FROM('v_objetivos_estrategicos_pt');
        $this->db->WHERE('(obje_gestion_curso BETWEEN '.$fecha_inicio.' AND '.$fecha_final.')');
        $this->db->ORDER_BY('obje_id', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    //lista de producto terminal
    function lista_prod_terminal($obje_id){
        $fecha_inicio = $this->get_fec_inicio();
        $fecha_inicio = $fecha_inicio->conf_gestion_desde;
        $fecha_final = $fecha_inicio + 4;
        $this->db->SELECT('*');
        $this->db->FROM('v_objetivos_estrategicos_pte');
        $this->db->WHERE('obje_id',$obje_id);
        $this->db->WHERE('(obje_gestion_curso BETWEEN '.$fecha_inicio.' AND '.$fecha_final.')');
        $this->db->ORDER_BY('obje_id', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    function lista_obje_poa($poa_id){
        $fecha_inicio = $this->get_fec_inicio();
        $fecha_inicio = $fecha_inicio->conf_gestion_desde;
        $fecha_final = $fecha_inicio + 4;
        $this->db->SELECT('v.*');
        $this->db->FROM('poaobjetivosestrategicos p');
        $this->db->JOIN('v_objetivos_estrategicos v ', 'p.obje_id = v.obje_id','INNER');
        $this->db->WHERE('p.poa_id',$poa_id);
        $this->db->WHERE('(obje_gestion_curso BETWEEN '.$fecha_inicio.' AND '.$fecha_final.')');
        $this->db->ORDER_BY('obje_id', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    function lista_obje_poa_total(){
        $fecha_inicio = $this->get_fec_inicio();
        $fecha_inicio = $fecha_inicio->conf_gestion_desde;
        $fecha_final = $fecha_inicio + 4;
        $this->db->SELECT('v.*');
        $this->db->FROM('v_objetivos_estrategicos v');
        $this->db->WHERE('(obje_gestion_curso BETWEEN '.$fecha_inicio.' AND '.$fecha_final.')');
        $this->db->ORDER_BY('obje_id', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }
    //DATOS DE OBJETIVOS ESTRATEGICOS FILTRADO POR ID
    function dato_objetivo($obje_id){
        $this->db->SELECT('*');
        $this->db->FROM('v_objetivos_estrategicos');
        $this->db->WHERE('obje_id', $obje_id);
        $query = $this->db->get();
        return $query->result_array();
    }
    //DATOS DE OBJETIVOS TERMINALES ESTRATEGICOS FILTRADO POR ID
    function dato_objetivot($obje_id){
        $this->db->SELECT('*');
        $this->db->FROM('v_objetivos_estrategicos_pte');
        $this->db->WHERE('pt_id', $obje_id);
        $query = $this->db->get();
        return $query->result_array();
    }
    //DATOS DE OBJETIVOS TERMINALES ESTRATEGICOS SIN VISTA 
    function dato_objetivo_terminal($obje_id){
        
        $this->db->SELECT('*');
        $this->db->FROM('pterminal_oestrategico');
        $this->db->WHERE('obje_id', $obje_id);
        $query = $this->db->get();
        return $query->result_array();
    }
    //LISTA DE PROGRAMACION DE OBJETIVOS
    function lista_pro_obj($obje_id){
        $this->db->SELECT('*');
        $this->db->FROM('obje_prog_gestion');
        $this->db->WHERE('obje_id', $obje_id);
        $query = $this->db->get();
        return $query->result_array();
    }
    //dato de programacion de objetivos filtrado por gestion
    function get_prog_obj($obje_id,$gestion){
        $no_existe[0]['opm_programado'] = '0';
        $this->db->SELECT('*');
        $this->db->FROM('obje_prog_gestion');
        $this->db->WHERE('obje_id', $obje_id);
        $this->db->WHERE('g_id', $gestion);
        $query = $this->db->get();
        if(($query->num_rows()) == 0){
            return $no_existe;
        }else{
            return $query->result_array();
        }
    }

    //dato de programacion de objetivos terminales filtrado por gestion
    function get_prog_pt_obj($obje_id,$gestion){
        $no_existe[0]['opm_programado'] = '0';
        $this->db->SELECT('*');
        $this->db->FROM('pt_prog_gestion');
        $this->db->WHERE('pt_id', $obje_id);
        $this->db->WHERE('g_id', $gestion);
        $query = $this->db->get();
        if(($query->num_rows()) == 0){
            return $no_existe;
        }else{
            return $query->result_array();
        }
    }
    //guardar objetivo estrategico
    function guardar_obj($dato){
        //EMPEZAR TRANSACCION
        $this->db->trans_begin();
        //GUARDAR EN TABLA OBJETIVO ESTRATEGICO
        $obje_id = $this->guardar_tabla_obje($dato);//retorna mi id
        //GUARDAR EN PROGRAMACION GESTION - OBJETIVO ESTRATEGICO
        $this->guardar_prog_obje($dato,$obje_id);
        if ($this->db->trans_status() === FALSE) {
            //si ha habido algún error lo debemos mostrar aquí
            $this->db->trans_rollback();

        } else {
            //en otro caso todo ha ido bien
            return $this->db->trans_commit();
        }
    }
    //guardar producto terminal
    function guardar_productot($dato){
        //EMPEZAR TRANSACCION
        $this->db->trans_begin();
        //GUARDAR EN TABLA OBJETIVO ESTRATEGICO
        $pt_id = $this->guardar_tabla_terminal($dato);//retorna mi id
        //GUARDAR EN PROGRAMACION GESTION - OBJETIVO ESTRATEGICO
        $this->guardar_prog_terminal($dato,$pt_id);
        if ($this->db->trans_status() === FALSE) {
            //si ha habido algún error lo debemos mostrar aquí
            $this->db->trans_rollback();

        } else {
            //en otro caso todo ha ido bien
            return $this->db->trans_commit();
        }
    }
    //GUARDAR PROGRAMACION ANUAL DEL PRODUCTO TERMINAL
    function guardar_prog_terminal($dato,$obje_id){
        $puntero = $this->get_fec_inicio();
        $puntero = $puntero->conf_gestion_desde;
        for($i = 1 ; $i <= 5 ; $i++){
            if($dato[$puntero] != 0){
                $data_prog = array(
                    'pt_id' => $obje_id,
                    'g_id' => $puntero,
                    'opm_programado' => $dato[$puntero]
                );
                $this->db->insert('pt_prog_gestion', $data_prog);
                //$this->db->insert_batch('otra_tabla', $data);
            }
            $puntero++;
            //$nueva_gestion++;
        }
    }
    //GUARDAR PROGRAMACION ANUAL DEL OBJETIVO ESTRATEGICO
    function guardar_prog_obje($dato,$obje_id){
        $puntero = $this->get_fec_inicio();
        $puntero = $puntero->conf_gestion_desde;
        for($i = 1 ; $i <= 5 ; $i++){
            if($dato[$puntero] != 0){
                $data_prog = array(
                    'obje_id' => $obje_id,
                    'g_id' => $puntero,
                    'opm_programado' => $dato[$puntero]
                );
                $this->db->insert('obje_prog_gestion', $data_prog);
                //$this->db->insert_batch('otra_tabla', $data);
            }
            $puntero++;
            //$nueva_gestion++;
        }
    }
    //GUARDAR EJECUCION ABSOLUTO DEL OBJETIVO ESTRATEGICO
    function g_ejec_absoluto($gestion,$obje_id){
        $nueva_gestion = $gestion;
        for($i = 1 ; $i <= 5 ; $i++){
            $data_ejec = array(
                'obje_id' => $obje_id,
                'g_id' => $nueva_gestion
            );
            $this->db->insert('obje_ejec_gestion', $data_ejec);
            $nueva_gestion++;
        }
    }

    function g_ejec_relativo($dato,$gestion,$obje_id){
        $nueva_gestion = $gestion;
        for($i = 1 ; $i <= 5 ; $i++){
            $data_ejec = array(
                'obje_id' => $obje_id,
                'g_id' => $nueva_gestion,
                'oer_denominador' => $dato['denominador']
            );
            $this->db->insert('obje_ejec_relativo', $data_ejec);
            $nueva_gestion++;
        }
    }
    //GUARDAR EN TABLA OBJETIVO ESTRATEGICO
    function guardar_tabla_obje($dato){
        //-----------crear codigo --------------------------
        $cont = $this->get_cont();
        $cont = $cont[0]['conf_obj_estrategico'];
        $cont++;
        $codigo = 'OBJ/SIIP/' . $this->gestion. '/00' . $cont;
        //--------- fin crear codigo -----------------------
        ///FALTA O PREGUNTAR UNIDAD ORGANIZACIONAL
        $data = array(
            'obje_codigo' => $codigo,
            'obje_objetivo' => $dato['obj'],
            'indi_id' => $dato['tipo_i'],
            'obje_indicador' => $dato['indicador'],
            'obje_formula' => $dato['formula'],
            'obje_linea_base' => $dato['lb'],
            'obje_meta' => $dato['meta'],
            'obje_fuente_verificacion' => $dato['verificacion'],
            'obje_supuestos' => $dato['supuestos'],
            'obje_ponderacion' => $dato['pn_cion'],
            'obje_gestion_curso' => $this->gestion,
            'pdes_id' => $dato['pdes4'],//guardar el ultimo pdes
            'ptdi_id' => $dato['ptdi3'],//guardar el ultimo ptdi
            'fun_id' => $dato['fun_id'],
            'obje_total_casos' => $dato['c_a'],
            'obje_casos_favorables' => $dato['c_b'],
            'obje_casos_desfavorables' => $dato['c_c'],
            'obje_denominador' => $dato['denominador']
        );
        $this->db->insert('objetivosestrategicos', $data);
        $obje_id = $this->db->insert_id();
        //-----------actualizar mi conf objetivos estrategicos mi contador de la tabla configuracion
        $this->actualizar_confobje_estrategico($cont, $this->gestion);
        return $obje_id;
    }
    //GUARDAR EN TABLA PRODUTO TERMINAL
    function guardar_tabla_terminal($dato){
        //-----------crear codigo --------------------------
        $cont = $this->get_cont();
        $cont = $cont[0]['conf_obj_estrategico'];
        $cont++;
        $codigo = 'PTM/SIIP/' . $this->gestion. '/00' . $cont;
        //--------- fin crear codigo -----------------------
        ///FALTA O PREGUNTAR UNIDAD ORGANIZACIONAL
        $data = array(
            'obje_codigo' => $codigo,
            'obje_objetivo' => $dato['obj'],
            'indi_id' => $dato['tipo_i'],
            'obje_id' => $dato['obje_id'],
            'obje_indicador' => $dato['indicador'],
            'obje_formula' => $dato['formula'],
            'obje_linea_base' => $dato['lb'],
            'obje_meta' => $dato['meta'],
            'obje_fuente_verificacion' => $dato['verificacion'],
            'obje_supuestos' => $dato['supuestos'],
            'obje_ponderacion' => $dato['pn_cion'],
            'obje_gestion_curso' => $this->gestion,
            'pdes_id' => $dato['pdes4'],//guardar el ultimo pdes
            'ptdi_id' => $dato['ptdi3'],//guardar el ultimo ptdi
            'fun_id' => $dato['fun_id'],
            'obje_total_casos' => $dato['c_a'],
            'obje_casos_favorables' => $dato['c_b'],
            'obje_casos_desfavorables' => $dato['c_c'],
            'obje_denominador' => $dato['denominador']
        );
        $this->db->insert('pterminal_oestrategico', $data);
        $pt_id = $this->db->insert_id();
        //-----------actualizar mi conf objetivos estrategicos mi contador de la tabla configuracion
        $this->actualizar_confobje_estrategico($cont, $this->gestion);
        return $pt_id;
    }
    // OBTIENE EL CONTADOR DE CODIGO POA DE LA TABLA CONFIGURACION
    function get_cont()
    {
        $this->db->SELECT('*');
        $this->db->FROM('configuracion');
        $this->db->WHERE('ide',$this->gestion);
        $query = $this->db->get();
        return $query->result_array();
    }
    //ACTUALIZA EL CONTADOR DE MI CODIGO OBJETIVO ESTRATEGICO DE LA TABLA CONFIGURACION
    function actualizar_confobje_estrategico($cont, $gestion)
    {
        $data = array(
            'conf_obj_estrategico' => $cont,
        );
        $this->db->WHERE('ide', $gestion);
        $this->db->UPDATE('configuracion ', $data);
    }
    //guardar objetivo estrategico
    function modificar_obj($dato, $obje_id){
        //EMPEZAR TRANSACCION
        $this->db->trans_begin();
        //GUARDAR EN TABLA OBJETIVO ESTRATEGICO
        $this->modificar_tabla_obje($dato,$obje_id);//retorna mi id
        //GUARDAR EN PROGRAMACION GESTION - OBJETIVO ESTRATEGICO
        $this->mod_prog_obje($dato,$obje_id);
        if ($this->db->trans_status() === FALSE) {
            //si ha habido algún error lo debemos mostrar aquí
            $this->db->trans_rollback();

        } else {
            //en otro caso todo ha ido bien
            return $this->db->trans_commit();
        }
    }
    //guardar objetivo estrategico terminal
    function modificar_objt($dato, $obje_id){
        //EMPEZAR TRANSACCION
        $this->db->trans_begin();
        //GUARDAR EN TABLA OBJETIVO ESTRATEGICO
        $this->modificar_tabla_obje_terminal($dato,$obje_id);//retorna mi id
        //GUARDAR EN PROGRAMACION GESTION - OBJETIVO ESTRATEGICO
        $this->mod_prog_objet($dato,$obje_id);
        if ($this->db->trans_status() === FALSE) {
            //si ha habido algún error lo debemos mostrar aquí
            $this->db->trans_rollback();

        } else {
            //en otro caso todo ha ido bien
            return $this->db->trans_commit();
        }
    }
    //MODIFICAR EN TABLA OBJETIVO ESTRATEGICO
    function modificar_tabla_obje($dato,$obje_id){
        $data = array(
            'obje_objetivo' => $dato['obj'],
            'indi_id' => $dato['tipo_i'],
            'obje_indicador' => $dato['indicador'],
            'obje_formula' => $dato['formula'],
            'obje_linea_base' => $dato['lb'],
            'obje_meta' => $dato['meta'],
            'obje_fuente_verificacion' => $dato['verificacion'],
            'obje_supuestos' => $dato['supuestos'],
            'obje_ponderacion' => $dato['pn_cion'],
            'pdes_id' => $dato['pdes4'],//guardar el ultimo pdes
            'ptdi_id' => $dato['ptdi3'],//guardar el ultimo pdi
            'fun_id' => $dato['fun_id'],
            'obje_total_casos' => $dato['c_a'],
            'obje_casos_favorables' => $dato['c_b'],
            'obje_casos_desfavorables' => $dato['c_c'],
            'obje_denominador' => $dato['denominador']
        );
        $this->db->where('obje_id',$obje_id);
        $this->db->update('objetivosestrategicos', $data);
    }
    //MODIFICAR EN TABLA OBJETIVO ESTRATEGICO
    function modificar_tabla_obje_terminal($dato,$obje_id){
        $data = array(
            'obje_objetivo' => $dato['obj'],
            'indi_id' => $dato['tipo_i'],
            'obje_id' => $dato['obje_id_d'],
            'obje_indicador' => $dato['indicador'],
            'obje_formula' => $dato['formula'],
            'obje_linea_base' => $dato['lb'],
            'obje_meta' => $dato['meta'],
            'obje_fuente_verificacion' => $dato['verificacion'],
            'obje_supuestos' => $dato['supuestos'],
            'obje_ponderacion' => $dato['pn_cion'],
            'pdes_id' => $dato['pdes4'],//guardar el ultimo pdes
            'ptdi_id' => $dato['ptdi3'],//guardar el ultimo pdi
            'fun_id' => $dato['fun_id'],
            'obje_total_casos' => $dato['c_a'],
            'obje_casos_favorables' => $dato['c_b'],
            'obje_casos_desfavorables' => $dato['c_c'],
            'obje_denominador' => $dato['denominador']
        );
        $this->db->where('pt_id',$obje_id);
        $this->db->update('pterminal_oestrategico', $data);
    }
    //MODIFICAR PROGRAMACION GESTION
    function mod_prog_obje($dato,$obje_id){
        //borrar todos sus programados
        $this->db->where('obje_id',$obje_id);
        $this->db->delete('obje_prog_gestion');
        //insertar los nuevos programados
        $puntero = $this->get_fec_inicio();
        $puntero = $puntero->conf_gestion_desde;
        for($i = 1 ; $i <= 5 ; $i++){
            if($dato[$puntero] != 0){
                $data_prog = array(
                    'obje_id' => $obje_id,
                    'g_id' => $puntero,
                    'opm_programado' => $dato[$puntero]
                );
                $this->db->insert('obje_prog_gestion', $data_prog);
            }
            $puntero++;
        }
    }
    //MODIFICAR PROGRAMACION TERMINAL DE GESTION
    function mod_prog_objet($dato,$obje_id){
        //borrar todos sus programados
        $this->db->where('pt_id',$obje_id);
        $this->db->delete('pt_prog_gestion');
        //insertar los nuevos programados
        $puntero = $this->get_fec_inicio();
        $puntero = $puntero->conf_gestion_desde;
        for($i = 1 ; $i <= 5 ; $i++){
            if($dato[$puntero] != 0){
                $data_prog = array(
                    'pt_id' => $obje_id,
                    'g_id' => $puntero,
                    'opm_programado' => $dato[$puntero]
                );
                $this->db->insert('pt_prog_gestion', $data_prog);
            }
            $puntero++;
        }
    }
    //eliminar objtivos estrategicos
    function eliminar_obje($obje_id){
        if($this->existe_obje_poa($obje_id) == 0 && $this->existe_obje_ogestion($obje_id) == 0){
            $this->db->trans_begin();
            //eliminar
            $data = array('obje_id' => $obje_id);
            $this->db->delete('obje_prog_gestion', $data);
            $this->db->delete('obje_ejec_gestion', $data);
            $this->db->delete('obje_ejec_relativo', $data);
            $this->db->delete('objetivosestrategicos', $data);
            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
            }else{
                $this->db->trans_commit();
                $respuesta = $obje_id;
            }

        }else{
            $respuesta = -1;
        }
        return $respuesta;
    }
    //eliminar productos terminales
    function eliminar_objet($obje_id){  //TODO preguntar si se verificara primero si existen productos terminales de testión relacionados
        //if($this->existe_obje_poa($obje_id) == 0 && $this->existe_obje_ogestion($obje_id) == 0){
            $this->db->trans_begin();
            //eliminar
            $data = array('pt_id' => $obje_id);
            $this->db->delete('pt_prog_gestion', $data);
            // $this->db->delete('obje_ejec_gestion', $data);
            // $this->db->delete('obje_ejec_relativo', $data);
            $this->db->delete('pterminal_oestrategico', $data);
            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
            }else{
                $this->db->trans_commit();
                $respuesta = $obje_id;
            }

        // }else{
        //     $respuesta = -1;
        // }
        return $respuesta;
    }

    //verificar si existe el objetivo estrategico en la tabla poaobjetivos estrategicos
    function existe_obje_poa($obje_id){
        $this->db->SELECT('*');
        $this->db->FROM('poaobjetivosestrategicos');
        $this->db->WHERE('obje_id',$obje_id);
        $q = $this->db->get();
        $q = $q->num_rows();
        return $q;
    }
    //verificar si existe el objetivo estrategico en la tabla poaobjetivos estrategicos
    function existe_obje_ogestion($obje_id){
        $this->db->SELECT('*');
        $this->db->FROM('objetivosgestion');
        $this->db->WHERE('obje_id',$obje_id);
        $q = $this->db->get();
        $q = $q->num_rows();
        return $q;
    }
    //OBTENER FECHA DE INICIO DE MANDATO
    function get_fec_inicio(){
        $this->db->SELECT('ide,conf_gestion_desde');
        $this->db->FROM('configuracion');
        $this->db->WHERE('ide',$this->gestion);
        $query = $this->db->get();
        return $query->row();
    }

    // PARA REPORTES DE EJECUCION OBJETIVO GESTION Y PROD TERMINAL
    public function get_prod_terminal_ejecutado($pt_id)
    {
        $query = "select *
                    from vogestion_ejec_mensual_absoluto
                    where o_id = ".$pt_id;
        $query = $this->db->query($query);

        return false;
    }
    public function get_obj_gestion_ejecutado($o_id)
    {
        $query = "SELECT *
        from vogestion_ejec_mensual_absoluto
        where o_id = ".$o_id;
        $query = $this->db->query($query);
    }

    //eliminar archivos de objetivo estrategico
    public function del_archivo($obje_id){
        $data = array(
            'obje_archivo_adjunto' => ""
        );
        $this->db->where('obje_id',$obje_id);
        $this->db->update('objetivosestrategicos', $data);
    }
}
?>
