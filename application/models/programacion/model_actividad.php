<?php
class model_actividad extends CI_Model {
  
    /**
    * Responsable for auto load the database
    * @return void
    */
    public function __construct()
    {
        $this->load->database();
    }
     
    /*------------ Relacion Insumo Actividad -------*/
    function imsumo_actividad($act_id)
    {
        $sql = 'select *
                from _insumoactividad
                where act_id='.$act_id.''; 
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    /*=================================== SUMA PROGRAMADO  ====================================*/
    public function suma_programado($act_id)
    {
        $sql = 'select SUM(pg_fis) as suma
                from act_programado_mensual
                where act_id='.$act_id.''; 
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function update_actividad_archivo($id)
    {
        $sql = 'update _actividad_archivos set estado=0 WHERE id = '.$id.'';  
        $this->db->query($sql);
    }

    function get_actividad_archivo($id)
    {
        $sql = 'select * from _actividad_archivos where id = '.$id.'';   
        $this->db->query($sql);
    }

    /*=================================== LISTA DE PRODUCTOS ANUAL ====================================*/
    function list_act_anual($id_prod)
    {
        $sql = 'SELECT a.*,tp.*
            from _actividades as a
            Inner Join indicador as tp On a."indi_id"=tp."indi_id"
            where a."prod_id"='.$id_prod.' and a."estado"!=\'3\' ORDER BY a.act_id  asc'; 
        $query = $this->db->query($sql);
        return $query->result_array();

    }
    /*==============================================================================================================*/

    /*=================================== AGREGAR ACTIVIDAD  PROGRAMADO GESTION ====================================*/
    function add_act_gest($id_act,$gestion,$m_id,$pgfis,$pgfin)
    {
        $data = array(
            'act_id' => $id_act,
            'm_id' => $m_id,
            'pg_fis' => $pgfis,
            'pg_fin' => $pgfin,
            'g_id' => $gestion,
        );
        $this->db->insert('act_programado_mensual',$data);
    }
    /*==============================================================================================================*/

    /*=================================== NRO DE ACTIVIDADES REGISTRADOS ====================================*/
    public function nro_actividades_gest($id_pr)
    {
        $this->db->from('_actividades');
        $this->db->where('prod_id', $id_pr);
        $query = $this->db->get();
        return $query->num_rows();
    }
    /*==============================================================================================================*/
    /*=================================== LISTA DE ACTIVIDADESGESTION ANUAL ====================================*/
    function list_actgest_anual($id_act)
    {
        $sql = 'SELECT *
            from act_programado_mensual
            where act_id='.$id_act.' and g_id='.$this->session->userdata("gestion").''; 
        $query = $this->db->query($sql);
        return $query->result_array();

    }
    /*==============================================================================================================*/
    /*=================================== GET DATOS  ACTIVIDAD  ====================================*/
    function get_actividad_id($id_act)
    {
        $sql = 'SELECT a.*,tp.* 
            from _actividades as a
            Inner Join indicador as tp On a."indi_id"=tp."indi_id"
            where a."act_id"='.$id_act.''; 
        $query = $this->db->query($sql);
        return $query->result_array();

    }
    /*==============================================================================================================*/
    /*=================================== META GESTION ACTIVIDAD ====================================*/
    public function meta_act_gest($id_act)
    {
        $sql = 'SELECT SUM(pg_fis) as meta_gest
            from act_programado_mensual
            where act_id='.$id_act.' AND g_id='.$this->session->userdata("gestion").' '; 
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    /*==============================================================================================================*/
    /*=================================== LISTA DE ACTIVIDADES PROGRAMADO GESTION  ====================================*/
    public function act_prog_mensual($id_act,$gest)
    {
        $this->db->from('act_programado_mensual');
        $this->db->where('act_id', $id_act);
        $this->db->where('g_id', $gest);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function act_prog_total($id_act){
        $sql='select sum(pg_fis) as total from act_programado_mensual where act_id='.$id_act;
        $query = $this->db->query($sql);
        return $query->row();
    }
    public function act_prog_acumulada($id_act,$gest){
        $sql='select sum(pg_fis) as total_acum from act_programado_mensual where act_id='.$id_act.' and g_id<'.$gest;
        $query = $this->db->query($sql);
        return $query->row();
    }
    /*==============================================================================================================*/
    /*=================================== LISTA DE ACTIVIDAD EJECUTADO GESTION  ====================================*/
    public function act_ejec_mensual($id_act,$gest)
    {
        $this->db->from('act_ejecutado_mensual');
        $this->db->where('act_id', $id_act);
        $this->db->where('g_id', $gest);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function act_ejec_total($id_act){
        $sql='select sum(ejec_fis) as total from act_ejecutado_mensual where act_id='.$id_act;
        $query = $this->db->query($sql);
        return $query->row();
    }
    public function act_ejec_acumulada($id_act,$gest){
        $sql='select sum(ejec_fis) as total_acum from act_ejecutado_mensual where act_id='.$id_act.' and g_id<'.$gest;
        $query = $this->db->query($sql);
        return $query->row();
    }
    public function nro_act_ejec_mensual($id_act,$gest)
    {
        $this->db->from('act_ejecutado_mensual');
        $this->db->where('act_id', $id_act);
        $this->db->where('g_id', $gest);
        $query = $this->db->get();
        return $query->num_rows();
    } 
    /*==============================================================================================================*/
    /*=================================== AGREGAR PRODUCTO  EJECUTADO GESTION ====================================*/
    function add_act_ejec_gest($id_act,$gestion,$m_id,$efis,$a,$b,$efin)
    {
        $data = array(
            'act_id' => $id_act,
            'm_id' => $m_id,
            'ejec_fis' => $efis,
            'ejec_fin' => $efin,
            'ejec_fis_a' => $a,
            'ejec_fis_b' => $b,
            'g_id' => $gestion,
        );
        $this->db->insert('act_ejecutado_mensual',$data);
    }
    /*==============================================================================================================*/
    /*============================ BORRA DATOS DE LA ACTIVIDAD PROGRAMADO GESTION =================================*/
    public function delete_act_gest($id_act){ 
        $this->db->where('act_id', $id_act);
        $this->db->delete('act_programado_mensual'); 
    }
    /*=================================================================================================*/
    /*============================ BORRA DATOS DE LA ACTIVIDAD EJECUTADO GESTION =================================*/
    public function delete_act_ejec_gest($act_id,$gest){ 
        $this->db->where('act_id', $act_id);
        $this->db->where('g_id', $gest);
        $this->db->delete('act_ejecutado_mensual'); 

        $this->db->where('act_id', $act_id);
        $this->db->where('g_id', $gest);
        $this->db->delete('act_ejecutado_mensual_relativo'); 
    }
    /*=================================================================================================*/
     /*=================================== META GESTION ACTIVIDAD ====================================*/
    public function suma_monto_ponderado_total($id_prod)
    {
        $sql = 'SELECT SUM(act_pres_p) as monto_total
            from _actividades
            where prod_id='.$id_prod.' '; 
        $query = $this->db->query($sql);
        return $query->result_array();

    }
    /*==============================================================================================================*/
    /*================================= NRO DE ACTIVIDADES ======================================*/
    public function actividades_nro($id_p)
    {
        $this->db->from('_actividades');
        $this->db->where('prod_id', $id_p);
        $query = $this->db->get();
        return $query->num_rows();
    }
    /*================================================================================================*/    


/*========================================== NRO DE ACTIVIDADES EN LOS INSUMOS ============================*/
    public function nro_act_insumos($id_act)
    {
        $this->db->from('_insumoactividad');
        $this->db->where('act_id', $id_act);
        $query = $this->db->get();
        return $query->num_rows();
    }


    /*================================ CALCULA LOS DIAS DE LA ACTIVIDAD =========================*/
    public function calcula_dias($id_act)
    {
        $this->db->select('extract(days from (act_fecha_final - act_fecha_inicio)) as dias');
        $this->db->from('_actividades ');
        $this->db->where('act_id', $id_act);
        $query = $this->db->get();
        return $query->result_array();
    }
    /*============================================================================================*/



    public function calcula_monto($id_prod) ////// para calcular las suma de los montos de la actividad
    {
        $query=$this->db->query('SELECT SUM(a."act_presupuesto") as costo, SUM(a."act_pres_p") as costo_p
                                        
        FROM "public"."_actividades" as a
        where a."prod_id"='.$id_prod.'');
        return $query->result_array();
    }

    /*------------------------------- para las dependencias -----------*/
    public function nro_act($id_prod)
    {
        $query=$this->db->query('SELECT * FROM _actividades WHERE prod_id='.$id_prod.' ORDER BY act_id DESC LIMIT 1');
        return $query->result_array(); 
    }

    /*============================ BORRA DATOS ACTIVIDAD =================================*/
    public function delete_actividad($id_a){ 

        $this->db->where('act_id', $id_a);
        $this->db->delete('act_programado_mensual');

        $this->db->where('act_id', $id_a);
        $this->db->delete('act_ejecutado_mensual');

        $this->db->where('act_id', $id_a);
        $this->db->delete('_actividades');
    }
    /*======================================================================================*/

    /*=================================== PROGRAMADO ACTIVIDAD  POR GESTIONES ====================================*/
    public function programado_actividad($id_act,$gest)
    {
        $this->db->from('vista_actividades_temporalizacion_programado_dictamen');
        $this->db->where('act_id', $id_act);
        $this->db->where('g_id', $gest);
        $query = $this->db->get();
        return $query->result_array();
    }
    /*==============================================================================================*/
    /*=================================== PROGRAMADO ACTIVIDAD  ====================================*/
    public function get_programado_actividad($id_act)
    {
        $this->db->from('vista_actividades_temporalizacion_programado_dictamen');
        $this->db->where('act_id', $id_act);
        $query = $this->db->get();
        return $query->result_array();
    }
    /*==============================================================================================*/
    /*=================================== EJECUTADO ACTIVIDAD POR GESTIONES ====================================*/
    public function ejecutado_actividad($id_act,$gest)
    {
        $this->db->from('vista_actividades_temporalizacion_ejecutado_dictamen');
        $this->db->where('act_id', $id_act);
        $this->db->where('g_id', $gest);
        $query = $this->db->get();
        return $query->result_array();
    }
    /*==============================================================================================*/
    /*=================================== EJECUTADO ACTIVIDAD ====================================*/
    public function get_ejecutado_actividad($id_act)
    {
        $this->db->from('vista_actividades_temporalizacion_ejecutado_dictamen');
        $this->db->where('act_id', $id_act);
        $query = $this->db->get();
        return $query->result_array();
    }
    /*==============================================================================================*/



    /*=================================== LISTA DE ACTIVIDADES PROGRAMADO GESTION (Nuevo) ====================================*/
    public function actividad_programado($act_id,$gest)
    {
        $this->db->from('vista_actividades_temporalizacion_programado_dictamen');
        $this->db->where('act_id', $act_id);
        $this->db->where('g_id', $gest);
        $query = $this->db->get();
        return $query->result_array();
    }

    /*=================================== LISTA DE ACTIVIDADES EJECUTADO GESTION (Nuevo) ====================================*/
    public function actividad_ejecutado($act_id,$gest)
    {
        $this->db->from('vista_actividades_temporalizacion_ejecutado_dictamen');
        $this->db->where('act_id', $act_id);
        $this->db->where('g_id', $gest);
        $query = $this->db->get();
        return $query->result_array();
    }
}
?>  
