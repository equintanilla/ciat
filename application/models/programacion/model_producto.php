<?php
class model_producto extends CI_Model {
  
    /**
    * Responsable for auto load the database
    * @return void
    */
    public function __construct()
    {
        $this->load->database();
    }

/*=================================== LISTA DE PRODUCTOS  ====================================*/
    function list_prod($id_c)
    {
        $sql = 'SELECT p.*,tp.*
            from _productos as p
            Inner Join indicador as tp On p."indi_id"=tp."indi_id"
            where p."com_id"='.$id_c.' and p."estado"!=\'3\' ORDER BY p.prod_id  asc'; 
        $query = $this->db->query($sql);
        return $query->result_array();

    }
/*==============================================================================================================*/
/*=================================== META GESTION ACTUAL PRODUCTO ====================================*/
    public function meta_prod_gest($id_prod)
    {
        $sql = 'SELECT SUM(pg_fis) as meta_gest
            from prod_programado_mensual
            where prod_id='.$id_prod.' AND g_id='.$this->session->userdata("gestion").' '; 
        $query = $this->db->query($sql);
        return $query->result_array();
    }
/*==============================================================================================================*/
/*=================================== LISTA DE PRODUCTOS ANUAL ====================================*/
    function get_producto_id($id_prod)
    {
        $sql = 'SELECT p.*,tp.*
            from _productos as p
            Inner Join indicador as tp On p."indi_id"=tp."indi_id"
            where p."prod_id"='.$id_prod.''; 
        $query = $this->db->query($sql);
        return $query->result_array();

    }
/*==============================================================================================================*/

/*=================================== LISTA DE PRODUCTOS PROGRAMADO GESTION  ====================================*/
    public function prod_prog_mensual($id_pr,$gest)
    {
        $this->db->from('prod_programado_mensual');
        $this->db->where('prod_id', $id_pr);
        $this->db->where('g_id', $gest);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function nro_prod_prog_mensual($id_pr,$gest)
    {
        $this->db->from('prod_programado_mensual');
        $this->db->where('prod_id', $id_pr);
        $this->db->where('g_id', $gest);
        $query = $this->db->get();
        return $query->num_rows();
    } 
    /*==============================================================================================================*/
    /*=================================== LISTA DE PRODUCTOS EJECUTADO GESTION  ====================================*/
    public function prod_ejec_mensual($id_pr,$gest)
    {
        $this->db->from('prod_ejecutado_mensual');
        $this->db->where('prod_id', $id_pr);
        $this->db->where('g_id', $gest);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function nro_prod_ejec_mensual($id_pr,$gest)
    {
        $this->db->from('prod_ejecutado_mensual');
        $this->db->where('prod_id', $id_pr);
        $this->db->where('g_id', $gest);
        $query = $this->db->get();
        return $query->num_rows();
    } 
    /*==============================================================================================================*/
    /*=================================== LISTA DE PRODUCTOS EJECUTADO RELATIVO GESTION  ====================================*/
    public function prod_ejecr_mensual($id_pr,$gest)
    {
        $this->db->from('prod_ejecutado_mensual_relativo');
        $this->db->where('prod_id', $id_pr);
        $this->db->where('g_id', $gest);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function nro_prod_ejecr_mensual($id_pr,$gest)
    {
        $this->db->from('prod_ejecutado_mensual_relativo');
        $this->db->where('prod_id', $id_pr);
        $this->db->where('g_id', $gest);
        $query = $this->db->get();
        return $query->num_rows();
    } 
    /*==============================================================================================================*/
    /*=================================== LISTA DE PRODUCTOSGESTION ANUAL ====================================*/
    function list_prodgest_anual($id_prod)
    {
        $sql = 'SELECT *
            from prod_programado_mensual
            where prod_id='.$id_prod.' and g_id='.$this->session->userdata("gestion").''; 
        $query = $this->db->query($sql);
        return $query->result_array();

    }
    /*==============================================================================================================*/
    /*=================================== AGREGAR PRODUCTO  PROGRAMADO GESTION ====================================*/
    function add_prod_gest($id_prod,$gestion,$m_id,$valor)
    {
        $data = array(
            'prod_id' => $id_prod,
            'm_id' => $m_id,
            'pg_fis' => $valor,
            'g_id' => $gestion,
        );
        $this->db->insert('prod_programado_mensual',$data);
    }
    /*==============================================================================================================*/
    /*=================================== AGREGAR PRODUCTO  EJECUTADO GESTION ====================================*/
    function add_prod_ejec_gest($id_prod,$gestion,$m_id,$valor,$a,$b)
    {
        $data = array(
            'prod_id' => $id_prod,
            'm_id' => $m_id,
            'pejec_fis' => $valor,
            'pejec_fis_a' => $a,
            'pejec_fis_b' => $b,
            'g_id' => $gestion,
        );
        $this->db->insert('prod_ejecutado_mensual',$data);
    }
    /*==============================================================================================================*/
    /*=================================== AGREGAR PRODUCTO  EJECUTADO RELATIVO GESTION ====================================*/
    function add_prod_ejecr_gest($id_prod,$gestion,$m_id,$va,$vb)
    {
        $data = array(
            'prod_id' => $id_prod,
            'm_id' => $m_id,
            'pejec_favorable' => $va,
            'pejec_desfavorable' => $vb,
            'g_id' => $gestion,
        );
        $this->db->insert('prod_ejecutado_mensual_relativo',$data);
    }
    /*==============================================================================================================*/
    /*============================ BORRA DATOS DE PRODUCTO PROGRAMADO GESTION =================================*/
    public function delete_prod_gest($id_prod){ 
        $this->db->where('prod_id', $id_prod);
        $this->db->delete('prod_programado_mensual'); 
    }
    /*=================================================================================================*/
    /*============================ BORRA DATOS DE PRODUCTO PROGRAMADO GESTION =================================*/
    public function delete_prod_ejec_gest($id_prod,$gest){ 
        $this->db->where('prod_id', $id_prod);
        $this->db->where('g_id', $gest);
        $this->db->delete('prod_ejecutado_mensual'); 

        $this->db->where('prod_id', $id_prod);
        $this->db->where('g_id', $gest);
        $this->db->delete('prod_ejecutado_mensual_relativo'); 
    }
    /*=================================================================================================*/
    /*==================================== NUMERO DE RESPONSABLES DE PROYECTOS================================*/
    public function prod_terminal111()
    {
        $this->db->from('_productoterminal');
        $this->db->where('pt_gestion', $this->session->userdata("gestion"));
        $this->db->order_by("pt_id","asc");
        $query = $this->db->get();
        return $query->result_array();
    }
    /*======================================================================================================*/
    /*=================================== LISTA DE PRODUCTOS  ====================================*/
    public function prod_terminal($prog)
    {
        $sql = 'SELECT pt.*
                from aperturaprogramatica as ap
                Inner Join objetivosgestion as og On og."aper_id"=ap."aper_id"
                Inner Join _productoterminal as pt On pt."o_id"=og."o_id"
                where ap."aper_gestion"='.$this->session->userdata("gestion").' and ap.aper_programa=\''.$prog.'\' and ap.aper_proyecto=\'0000\' and ap.aper_actividad=\'000\' and pt.pt_gestion='.$this->session->userdata("gestion").' ORDER BY pt.pt_id  asc'; 
        $query = $this->db->query($sql);
        return $query->result_array();

    }
    /*==============================================================================================================*/
    /*================================= NRO DE PRODUCTOS ======================================*/
    public function productos_nro($id_c)
    {
        $this->db->from('_productos');
        $this->db->where('com_id', $id_c);
        $query = $this->db->get();
        return $query->num_rows();
    }
    /*================================================================================================*/    
    /*============================ BORRA DATOS PRODUCTOS =================================*/
    public function delete_producto_p($id_p){ 

        $this->db->where('prod_id', $id_p);
        $this->db->delete('prod_programado_mensual');
    }

    public function delete_producto_e($id_p){ 

        $this->db->where('prod_id', $id_p);
        $this->db->delete('prod_ejecutado_mensual');

        $this->db->where('prod_id', $id_p);
        $this->db->delete('_productos');
    }

    public function delete_producto($id_p){ 

        $this->db->where('prod_id', $id_p);
        $this->db->delete('_productos');
    }
    /*======================================================================================*/


        public function producto_programado($prod_id,$gestion){
        $sql = 'select *
                from vista_productos_temporalizacion_programado_dictamen
                where prod_id='.$prod_id.' and g_id='.$gestion.''; 
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function list_temporalidad($prod_id){
        $sql = 'select *
                from prod_programado_mensual
                where prod_id='.$prod_id.'
                order by g_id asc'; 
        $query = $this->db->query($sql);
        return $query->result_array();
    }


    public function list_productos_actividad($com_id){
        $sql = 'select *
                from _productos pr
                Inner Join _actividades as a On a.prod_id=pr.prod_id
                where pr.com_id='.$com_id.' and pr.estado!=\'3\' and a.estado!=\'3\'
                order by pr.prod_id, a.act_id asc'; 
        $query = $this->db->query($sql);
        return $query->result_array();
    }
}
?>  
