<?php
class Model_epresupuestaria extends CI_Model {
    public function __construct()
    {
        $this->load->database();
    }

    /*-------------- Lista de tipos de ejecucion 1 ------------*/
    public function tp_unidad_ejec1()
    {
        $sql = 'select *
                from tp_unidad_ejecutora
                where tue_estado!=\'3\'  and tue_id=\'1\'
                order by tue_id asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function tp_unidad_ejec2()
    {
        $sql = 'select *
                from tp_unidad_ejecutora
                where tue_estado!=\'3\' and tue_id!=\'0\' and tue_id!=\'1\'
                order by tue_id asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*-------------- Lista de tipos de ejecucion (ORGANO EJECUTIVO) ------------*/
    public function tp_unidad_ejec_oe()
    {
        $sql = 'select *
                from tp_unidad_ejecutora
                where tue_estado!=\'3\'  and tue_id!=\'0\'
                order by tue_id asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*-------------- lista de unidades Ejecutoras ------------*/
    public function unidades_ejecutoras($tp_ue)
    {
        $sql = 'select *
                from unidadorganizacional
                where uni_ejecutora=\'1\' and uni_estado!=\'3\' and tp_ue='.$tp_ue.'';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*-------------- Get Unidad Ejecutora ------------*/
    public function get_unidad_ejecutora($uni_id)
    {
        $sql = 'select *
                from unidadorganizacional uo
                Inner Join tp_unidad_ejecutora as ue On ue.tue_id = uo.tp_ue
                where uni_id='.$uni_id.'';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*---------------------- Programas General -----------------------*/
    public function programas_hijos_general($aper_programa,$gestion,$mes,$uni_id)
    {
        $sql = 'select p.*,a.*,pe.*,u.*,pfg.*
                from aperturaprogramatica a
                Inner Join unidadorganizacional as u On a.uni_id = u.uni_id
                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
                Inner Join _proyectos as p On p.proy_id = ap.proy_id
                Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id
                Inner Join ptto_fase_gestion as pfg On pfg.pfec_id = pf.pfec_id
                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
                where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\')  and a.aper_programa=\''.$aper_programa.'\' and a.aper_gestion='.$gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and pe.m_id='.$mes.' and pf.pfec_estado=\'1\' and pfg.g_id='.$gestion.' and u.uni_id='.$uni_id.'
                order by a.aper_programa,a.aper_proyecto,a.aper_actividad asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*---------------------- Programas General Detalles -----------------------*/
    public function programas_hijos_general_detalles($gestion,$mes,$uni_id,$uni_ejec)
    {
        $sql = 'select p.*,a.*,pe.*,u.*,pfg.*
                from aperturaprogramatica a
                Inner Join unidadorganizacional as u On a.uni_id = u.uni_id
                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
                Inner Join _proyectos as p On p.proy_id = ap.proy_id
                Inner Join _proyectofuncionario as pfun On p.proy_id = pfun.proy_id
                Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id
                Inner Join ptto_fase_gestion as pfg On pfg.pfec_id = pf.pfec_id
                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
                where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and a.aper_gestion='.$gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and pe.m_id='.$mes.' and pf.pfec_estado=\'1\' and pfg.g_id='.$gestion.' and pfun.pfun_tp=\'1\' and u.uni_id='.$uni_id.' and pfun.uni_ejec='.$uni_ejec.'
                order by a.aper_programa,a.aper_proyecto,a.aper_actividad asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function programas_hijos_general_total($gestion,$mes,$uni_id,$uni_ejec){
        $sql = 'select count(p.*) as total
                from aperturaprogramatica a
                Inner Join unidadorganizacional as u On a.uni_id = u.uni_id
                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
                Inner Join _proyectos as p On p.proy_id = ap.proy_id
                Inner Join _proyectofuncionario as pfun On p.proy_id = pfun.proy_id
                Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id
                Inner Join ptto_fase_gestion as pfg On pfg.pfec_id = pf.pfec_id
                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
                where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and a.aper_gestion='.$gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and pe.m_id='.$mes.' and pf.pfec_estado=\'1\' and pfg.g_id='.$gestion.' and pfun.pfun_tp=\'1\' and u.uni_id='.$uni_ejec;

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function suma_programas_hijos_general($gestion,$mes,$uni_id)
    {
        $sql = 'select u.uni_id,SUM(pfecg_ppto_total) as costo,SUM(pe.pe_pi) as pi, SUM(pe_pm) as pm ,SUM(pe_pv) as pv ,SUM(pe_pe) as pe
                from aperturaprogramatica a
                Inner Join unidadorganizacional as u On a.uni_id = u.uni_id
                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
                Inner Join _proyectos as p On p.proy_id = ap.proy_id
                Inner Join _proyectofuncionario as pfun On p.proy_id = pfun.proy_id
                Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id
                Inner Join ptto_fase_gestion as pfg On pfg.pfec_id = pf.pfec_id
                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
                where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and a.aper_gestion='.$gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and pe.m_id='.$mes.' and pf.pfec_estado=\'1\' and pfg.g_id='.$gestion.' and pfun.pfun_tp=\'1\' and u.uni_id='.$uni_id.'
                group by u.uni_id
                order by u.uni_id asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }
    /*-----------------------------------------------------------------------------*/

    /*------------------- Programas Proyectos de Inversion -----------------------*/
    public function programas_hijos_pinversion($aper_programa,$gestion,$mes,$uni_id)
    {
        $sql = 'select p.*,a.*,pe.*,u.*,pfg.*
                from aperturaprogramatica a
                Inner Join unidadorganizacional as u On a.uni_id = u.uni_id
                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
                Inner Join _proyectos as p On p.proy_id = ap.proy_id
                Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id
                Inner Join ptto_fase_gestion as pfg On pfg.pfec_id = pf.pfec_id
                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
                where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\')  and a.aper_programa=\''.$aper_programa.'\' and a.aper_gestion='.$gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and  pe.m_id='.$mes.' and p.tp_id=\'1\' and pf.pfec_estado=\'1\' and pfg.g_id='.$gestion.' and u.uni_id='.$uni_id.'
                order by a.aper_programa,a.aper_proyecto,a.aper_actividad asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }
    public function programas_hijos_pinversion_totales($gestion,$mes,$uni_ejec){
        $sql = 'select p.*,a.*,pe.*,u.*,pfg.*
                from aperturaprogramatica a
                Inner Join unidadorganizacional as u On a.uni_id = u.uni_id
                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
                Inner Join _proyectos as p On p.proy_id = ap.proy_id
                Inner Join _proyectofuncionario as pfun On p.proy_id = pfun.proy_id
                Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id
                Inner Join ptto_fase_gestion as pfg On pfg.pfec_id = pf.pfec_id
                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
                where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and a.aper_gestion='.$gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and  pe.m_id='.$mes.' and p.tp_id=\'1\' and pf.pfec_estado=\'1\' and pfg.g_id='.$gestion.' and pfun.pfun_tp=\'1\' and u.uni_id='.$uni_ejec;

        $query = $this->db->query($sql);
        return $query->num_rows();
    }




    /*------------------- Programas Proyectos de Inversion Detalles -----------------------*/
    public function programas_hijos_pinversion_detalles($gestion,$mes,$uni_id,$uni_ejec)
    {
        $sql = 'select p.*,a.*,pe.*,u.*,pfg.*
                from aperturaprogramatica a
                Inner Join unidadorganizacional as u On a.uni_id = u.uni_id
                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
                Inner Join _proyectos as p On p.proy_id = ap.proy_id
                Inner Join _proyectofuncionario as pfun On p.proy_id = pfun.proy_id
                Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id
                Inner Join ptto_fase_gestion as pfg On pfg.pfec_id = pf.pfec_id
                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
                where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and a.aper_gestion='.$gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and  pe.m_id='.$mes.' and p.tp_id=\'1\' and pf.pfec_estado=\'1\' and pfg.g_id='.$gestion.' and pfun.pfun_tp=\'1\' and u.uni_id='.$uni_id.' and pfun.uni_ejec='.$uni_ejec.'
                order by a.aper_programa,a.aper_proyecto,a.aper_actividad asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }
        public function suma_programas_hijos_pinversion($gestion,$mes,$uni_id)
    {
        $sql = 'select u.uni_id,SUM(pfecg_ppto_total) as costo,SUM(pe.pe_pi) as pi, SUM(pe_pm) as pm ,SUM(pe_pv) as pv ,SUM(pe_pe) as pe
                from aperturaprogramatica a
                Inner Join unidadorganizacional as u On a.uni_id = u.uni_id
                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
                Inner Join _proyectos as p On p.proy_id = ap.proy_id
                Inner Join _proyectofuncionario as pfun On p.proy_id = pfun.proy_id
                Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id
                Inner Join ptto_fase_gestion as pfg On pfg.pfec_id = pf.pfec_id
                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
                where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and a.aper_gestion='.$gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and  pe.m_id='.$mes.' and p.tp_id=\'1\' and pf.pfec_estado=\'1\' and pfg.g_id='.$gestion.' and pfun.pfun_tp=\'1\' and u.uni_id='.$uni_id.'
                group by u.uni_id
                order by u.uni_id asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }
    /*----------------------------------------------------------------------------------------*/

    /*------------------- Programas Recurrentes , no Recurrentes -----------------------*/
    public function programas_hijos_programas($aper_programa,$gestion,$mes,$uni_id)
    {
        $sql = 'select p.*,pfg.*,a.*,pe.*,u.*
                from aperturaprogramatica a
                Inner Join unidadorganizacional as u On a.uni_id = u.uni_id
                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
                Inner Join _proyectos as p On p.proy_id = ap.proy_id
                Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id
                Inner Join ptto_fase_gestion as pfg On pfg.pfec_id = pf.pfec_id
                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
                where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\')  and a.aper_programa=\''.$aper_programa.'\' and a.aper_gestion='.$gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and  pe.m_id='.$mes.' and (p.tp_id=\'2\' or p.tp_id=\'3\') and pf.pfec_estado=\'1\' and pfg.g_id='.$gestion.' and u.uni_id='.$uni_id.'
                order by a.aper_programa,a.aper_proyecto,a.aper_actividad asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*------------------- Programas Recurrentes , no Recurrentes Detalles -----------------------*/
    public function programas_hijos_programas_detalles($gestion,$mes,$uni_id,$uni_ejec)
    {
        $sql = 'select p.*,pfg.*,a.*,pe.*,u.*
                from aperturaprogramatica a
                Inner Join unidadorganizacional as u On a.uni_id = u.uni_id
                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
                Inner Join _proyectos as p On p.proy_id = ap.proy_id
                Inner Join _proyectofuncionario as pfun On p.proy_id = pfun.proy_id
                Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id
                Inner Join ptto_fase_gestion as pfg On pfg.pfec_id = pf.pfec_id
                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
                where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and a.aper_gestion='.$gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and  pe.m_id='.$mes.' and (p.tp_id=\'2\' or p.tp_id=\'3\') and pf.pfec_estado=\'1\' and pfg.g_id='.$gestion.' and pfun.pfun_tp=\'1\' and u.uni_id='.$uni_id.'  and pfun.uni_ejec='.$uni_ejec.'
                order by a.aper_programa,a.aper_proyecto,a.aper_actividad asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function programas_hijos_programas_total($gestion,$mes,$uni_ejec)
    {
        $sql = 'select p.*,pfg.*,a.*,pe.*,u.*
                from aperturaprogramatica a
                Inner Join unidadorganizacional as u On a.uni_id = u.uni_id
                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
                Inner Join _proyectos as p On p.proy_id = ap.proy_id
                Inner Join _proyectofuncionario as pfun On p.proy_id = pfun.proy_id
                Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id
                Inner Join ptto_fase_gestion as pfg On pfg.pfec_id = pf.pfec_id
                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
                where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and a.aper_gestion='.$gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and  pe.m_id='.$mes.' and (p.tp_id=\'2\' or p.tp_id=\'3\') and pf.pfec_estado=\'1\' and pfg.g_id='.$gestion.' and pfun.pfun_tp=\'1\' and u.uni_id='.$uni_ejec;

        $query = $this->db->query($sql);
        return $query->num_rows();
    }
    public function suma_programas_hijos_programas($gestion,$mes,$uni_id)
    {
        $sql = 'select u.uni_id,SUM(pfecg_ppto_total) as costo,SUM(pe.pe_pi) as pi, SUM(pe_pm) as pm ,SUM(pe_pv) as pv ,SUM(pe_pe) as pe
                from aperturaprogramatica a
                Inner Join unidadorganizacional as u On a.uni_id = u.uni_id
                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
                Inner Join _proyectos as p On p.proy_id = ap.proy_id
                Inner Join _proyectofuncionario as pfun On p.proy_id = pfun.proy_id
                Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id
                Inner Join ptto_fase_gestion as pfg On pfg.pfec_id = pf.pfec_id
                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
                where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and a.aper_gestion='.$gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and  pe.m_id='.$mes.' and (p.tp_id=\'2\' or p.tp_id=\'3\') and pf.pfec_estado=\'1\' and pfg.g_id='.$gestion.' and pfun.pfun_tp=\'1\' and u.uni_id='.$uni_id.'
                group by u.uni_id
                order by u.uni_id asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }
    /*-----------------------------------------------------------------------------------------------------------------*/

    /*------------------------------------------- Suma Total General --------------------------------------------------*/
    public function suma_id_ue($gestion,$mes,$tue_id)
    {
        $sql = 'select tue.tue_id,SUM(pr.pe_pi) as pi,SUM(pr.pe_pm) as pm,SUM(pr.pe_pv) as pv,SUM(pr.pe_pe) as pe
                from tp_unidad_ejecutora tue
                Inner Join unidadorganizacional as u On u.tp_ue = tue.tue_id
                Inner Join (select p.proy_id,p.proy_nombre,a.uni_id,pe.pe_pi,pe_pm,pe.pe_pv,pe.pe_pe
                                from aperturaprogramatica a
                                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
                                Inner Join _proyectos as p On p.proy_id = ap.proy_id
                                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
                                where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and a.aper_gestion='.$gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and  pe.m_id='.$mes.'
                                order by a.aper_programa,a.aper_proyecto,a.aper_actividad asc) as pr On pr.uni_id = u.uni_id
                where tue.tue_estado!=\'3\'  and tue.tue_id!=\'0\' and tue.tue_id='.$tue_id.'
                group by tue.tue_id
                order by tue.tue_id asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function suma_total($gestion,$mes)
    {
        $sql = 'select SUM(pr.pe_pi) as pi,SUM(pr.pe_pm) as pm,SUM(pr.pe_pv) as pv,SUM(pr.pe_pe) as pe
                from tp_unidad_ejecutora tue
                Inner Join unidadorganizacional as u On u.tp_ue = tue.tue_id
                Inner Join (select p.proy_id,p.proy_nombre,a.uni_id,pe.pe_pi,pe_pm,pe.pe_pv,pe.pe_pe
                                from aperturaprogramatica a
                                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
                                Inner Join _proyectos as p On p.proy_id = ap.proy_id
                                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
                                where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and a.aper_gestion='.$gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and  pe.m_id='.$mes.'
                                order by a.aper_programa,a.aper_proyecto,a.aper_actividad asc) as pr On pr.uni_id = u.uni_id
                where tue.tue_estado!=\'3\'  and tue.tue_id!=\'0\'';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function suma_pretotal($gestion,$mes)
    {
        $sql = 'select SUM(pr.pe_pi) as pi,SUM(pr.pe_pm) as pm,SUM(pr.pe_pv) as pv,SUM(pr.pe_pe) as pe
                from tp_unidad_ejecutora tue
                Inner Join unidadorganizacional as u On u.tp_ue = tue.tue_id
                Inner Join (select p.proy_id,p.proy_nombre,a.uni_id,pe.pe_pi,pe_pm,pe.pe_pv,pe.pe_pe
                                from aperturaprogramatica a
                                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
                                Inner Join _proyectos as p On p.proy_id = ap.proy_id
                                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
                                where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and a.aper_gestion='.$gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and  pe.m_id='.$mes.'
                                order by a.aper_programa,a.aper_proyecto,a.aper_actividad asc) as pr On pr.uni_id = u.uni_id
                where tue.tue_estado!=\'3\' and tue.tue_id!=\'0\' and tue.tue_id!=\'1\'';

        $query = $this->db->query($sql);
        return $query->result_array();
    }
    /*-----------------------------------------------------------------------------------------------------------------*/

    /*--------------------------------- Suma Total Proyectos de Inversion ---------------------------------------------*/
    public function suma_id_ue_ip($gestion,$mes,$tue_id)
    {
        $sql = 'select tue.tue_id,SUM(pr.pe_pi) as pi,SUM(pr.pe_pm) as pm,SUM(pr.pe_pv) as pv,SUM(pr.pe_pe) as pe
                from tp_unidad_ejecutora tue
                Inner Join unidadorganizacional as u On u.tp_ue = tue.tue_id
                Inner Join (select p.proy_id,p.proy_nombre,a.uni_id,pe.pe_pi,pe_pm,pe.pe_pv,pe.pe_pe
                                from aperturaprogramatica a
                                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
                                Inner Join _proyectos as p On p.proy_id = ap.proy_id
                                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
                                where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and a.aper_gestion='.$gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and  pe.m_id='.$mes.' and p.tp_id=\'1\'
                                order by a.aper_programa,a.aper_proyecto,a.aper_actividad asc) as pr On pr.uni_id = u.uni_id
                where tue.tue_estado!=\'3\'  and tue.tue_id!=\'0\' and tue.tue_id='.$tue_id.'
                group by tue.tue_id
                order by tue.tue_id asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function suma_total_pi($gestion,$mes)
    {
        $sql = 'select SUM(pr.pe_pi) as pi,SUM(pr.pe_pm) as pm,SUM(pr.pe_pv) as pv,SUM(pr.pe_pe) as pe
                from tp_unidad_ejecutora tue
                Inner Join unidadorganizacional as u On u.tp_ue = tue.tue_id
                Inner Join (select p.proy_id,p.proy_nombre,a.uni_id,pe.pe_pi,pe_pm,pe.pe_pv,pe.pe_pe
                                from aperturaprogramatica a
                                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
                                Inner Join _proyectos as p On p.proy_id = ap.proy_id
                                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
                                where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and a.aper_gestion='.$gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and  pe.m_id='.$mes.' and p.tp_id=\'1\'
                                order by a.aper_programa,a.aper_proyecto,a.aper_actividad asc) as pr On pr.uni_id = u.uni_id
                where tue.tue_estado!=\'3\'  and tue.tue_id!=\'0\'';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function suma_pretotal_pi($gestion,$mes)
    {
        $sql = 'select SUM(pr.pe_pi) as pi,SUM(pr.pe_pm) as pm,SUM(pr.pe_pv) as pv,SUM(pr.pe_pe) as pe
                from tp_unidad_ejecutora tue
                Inner Join unidadorganizacional as u On u.tp_ue = tue.tue_id
                Inner Join (select p.proy_id,p.proy_nombre,a.uni_id,pe.pe_pi,pe_pm,pe.pe_pv,pe.pe_pe
                                from aperturaprogramatica a
                                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
                                Inner Join _proyectos as p On p.proy_id = ap.proy_id
                                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
                                where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and a.aper_gestion='.$gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and  pe.m_id='.$mes.' and p.tp_id=\'1\'
                                order by a.aper_programa,a.aper_proyecto,a.aper_actividad asc) as pr On pr.uni_id = u.uni_id
                where tue.tue_estado!=\'3\' and tue.tue_id!=\'0\' and tue.tue_id!=\'1\'';

        $query = $this->db->query($sql);
        return $query->result_array();
    }
    /*-----------------------------------------------------------------------------------------------------------------*/

    /*--------------------------------- Suma Total Programas Recurrente, No Recurrentes ---------------------------------------------*/
    public function suma_id_ue_pr($gestion,$mes,$tue_id)
    {
        $sql = 'select tue.tue_id,SUM(pr.pe_pi) as pi,SUM(pr.pe_pm) as pm,SUM(pr.pe_pv) as pv,SUM(pr.pe_pe) as pe
                from tp_unidad_ejecutora tue
                Inner Join unidadorganizacional as u On u.tp_ue = tue.tue_id
                Inner Join (select p.proy_id,p.proy_nombre,a.uni_id,pe.pe_pi,pe_pm,pe.pe_pv,pe.pe_pe
                                from aperturaprogramatica a
                                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
                                Inner Join _proyectos as p On p.proy_id = ap.proy_id
                                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
                                where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and a.aper_gestion='.$gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and  pe.m_id='.$mes.' and (p.tp_id=\'2\' or p.tp_id=\'3\')
                                order by a.aper_programa,a.aper_proyecto,a.aper_actividad asc) as pr On pr.uni_id = u.uni_id
                where tue.tue_estado!=\'3\'  and tue.tue_id!=\'0\' and tue.tue_id='.$tue_id.'
                group by tue.tue_id
                order by tue.tue_id asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function suma_total_pr($gestion,$mes)
    {
        $sql = 'select SUM(pr.pe_pi) as pi,SUM(pr.pe_pm) as pm,SUM(pr.pe_pv) as pv,SUM(pr.pe_pe) as pe
                from tp_unidad_ejecutora tue
                Inner Join unidadorganizacional as u On u.tp_ue = tue.tue_id
                Inner Join (select p.proy_id,p.proy_nombre,a.uni_id,pe.pe_pi,pe_pm,pe.pe_pv,pe.pe_pe
                                from aperturaprogramatica a
                                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
                                Inner Join _proyectos as p On p.proy_id = ap.proy_id
                                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
                                where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and a.aper_gestion='.$gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and  pe.m_id='.$mes.' and (p.tp_id=\'2\' or p.tp_id=\'3\')
                                order by a.aper_programa,a.aper_proyecto,a.aper_actividad asc) as pr On pr.uni_id = u.uni_id
                where tue.tue_estado!=\'3\'  and tue.tue_id!=\'0\'';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function suma_pretotal_pr($gestion,$mes)
    {
        $sql = 'select SUM(pr.pe_pi) as pi,SUM(pr.pe_pm) as pm,SUM(pr.pe_pv) as pv,SUM(pr.pe_pe) as pe
                from tp_unidad_ejecutora tue
                Inner Join unidadorganizacional as u On u.tp_ue = tue.tue_id
                Inner Join (select p.proy_id,p.proy_nombre,a.uni_id,pe.pe_pi,pe_pm,pe.pe_pv,pe.pe_pe
                                from aperturaprogramatica a
                                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
                                Inner Join _proyectos as p On p.proy_id = ap.proy_id
                                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
                                where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and a.aper_gestion='.$gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and  pe.m_id='.$mes.' and (p.tp_id=\'2\' or p.tp_id=\'3\')
                                order by a.aper_programa,a.aper_proyecto,a.aper_actividad asc) as pr On pr.uni_id = u.uni_id
                where tue.tue_estado!=\'3\' and tue.tue_id!=\'0\' and tue.tue_id!=\'1\'';

        $query = $this->db->query($sql);
        return $query->result_array();
    }
    /*-----------------------------------------------------------------------------------------------------------------*/
    public function ejecucion_proyecto_programas_x_municipio($sql_in,$gestion,$mes,$muni_id)
    {
        if($muni_id!=90504){
	        $sql = 'select p.*,pf.*,a.*,pe.*,u.*,pt.tp_tipo,fun_paterno|| \' \' ||fun_materno|| \' \' ||fun_nombre as responsable,fun_telefono
	                from aperturaprogramatica a 
	                Inner Join unidadorganizacional as u On a.uni_id = u.uni_id 
	                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id 
	                Inner Join _proyectos as p On p.proy_id = ap.proy_id 
	                Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id  
	                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id
	                Inner Join _tipoproyecto as pt On p.tp_id=pt.tp_id
	                Inner Join _proyectosmunicipios as pm On p.proy_id = pm.proy_id
	                Inner Join _proyectofuncionario as pfun On p.proy_id = pfun.proy_id
					Inner Join funcionario as fun on pfun.fun_id= fun.fun_id
	                where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and 
	                a.aper_gestion='.$gestion.' and pfun.pfun_tp=1 and
	                (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and  
	                pe.m_id='.$mes.' and p.tp_id '.$sql_in.' and 
	                pf.pfec_estado=\'1\' and 
	                pm.muni_id='.$muni_id.'
	                order by pt.tp_id,uni_unidad asc';
        }else{
	        $sql = 'select * from (select p.*,pf.*,a.*,pe.*,u.*,pt.tp_tipo,fun_paterno|| \' \' ||fun_materno|| \' \' ||fun_nombre as responsable,fun_telefono,90504 as municipio
	                from aperturaprogramatica a 
	                Inner Join unidadorganizacional as u On a.uni_id = u.uni_id 
	                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id 
	                Inner Join _proyectos as p On p.proy_id = ap.proy_id 
	                Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id  
	                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id
	                Inner Join _tipoproyecto as pt On p.tp_id=pt.tp_id
	                Inner Join _proyectosmunicipios as pm On p.proy_id = pm.proy_id
	                Inner Join _proyectofuncionario as pfun On p.proy_id = pfun.proy_id
					Inner Join funcionario as fun on pfun.fun_id= fun.fun_id
	                where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and 
	                a.aper_gestion='.$gestion.' and pfun.pfun_tp=1 and
	                (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and  
	                pe.m_id='.$mes.' and p.tp_id '.$sql_in.' and 
	                pf.pfec_estado=\'1\' and 
	                pm.muni_id='.$muni_id.'
	                union
	                select p.*,pf.*,a.*,pe.*,u.*,pt.tp_tipo,fun_paterno|| \' \' ||fun_materno|| \' \' ||fun_nombre as responsable,fun_telefono,0 as municipio
	                from aperturaprogramatica a 
	                Inner Join unidadorganizacional as u On a.uni_id = u.uni_id 
	                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id 
	                Inner Join _proyectos as p On p.proy_id = ap.proy_id 
	                Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id  
	                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id
	                Inner Join _tipoproyecto as pt On p.tp_id=pt.tp_id
	                Inner Join _proyectosmunicipios as pm On p.proy_id = pm.proy_id
	                Inner Join _proyectofuncionario as pfun On p.proy_id = pfun.proy_id
					Inner Join funcionario as fun on pfun.fun_id= fun.fun_id
	                where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and 
	                a.aper_gestion='.$gestion.' and pfun.pfun_tp=1 and
	                (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and  
	                pe.m_id='.$mes.' and p.tp_id '.$sql_in.' and 
	                pf.pfec_estado=\'1\' and 
	                pm.muni_id<>'.$muni_id.' ) as _general
	                order by tp_id,uni_unidad asc';
        }
    	$query = $this->db->query($sql);
        return $query->result_array();
    }

    public function ejecucion_proyecto_programas_x_provincia($sql_in,$gestion,$mes,$prov_id)
    {
        if($prov_id!=906){
        	$sql = 'select p.*,pf.*,a.*,pe.*,u.*,pt.tp_tipo,fun_paterno|| \' \' ||fun_materno|| \' \' ||fun_nombre as responsable,fun_telefono 
                from aperturaprogramatica a 
                Inner Join unidadorganizacional as u On a.uni_id = u.uni_id 
                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id 
                Inner Join _proyectos as p On p.proy_id = ap.proy_id 
                Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id 
                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id
                Inner Join _tipoproyecto as pt On p.tp_id=pt.tp_id
                Inner Join _proyectosprovincias as pp On p.proy_id = pp.proy_id
                Inner Join _proyectofuncionario as pfun On p.proy_id = pfun.proy_id
				Inner Join funcionario as fun on pfun.fun_id= fun.fun_id
                where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and 
                a.aper_gestion='.$gestion.' and pfun.pfun_tp=1 and
                (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and  
                pe.m_id='.$mes.' and p.tp_id '.$sql_in.' and 
                pf.pfec_estado=\'1\' and 
                pp.prov_id='.$prov_id.'
                order by pt.tp_id,uni_unidad asc';
        }else{
	        $sql = 'select * from (select p.*,pf.*,a.*,pe.*,u.*,pt.tp_tipo,fun_paterno|| \' \' ||fun_materno|| \' \' ||fun_nombre as responsable,fun_telefono, 906 as provincia 
                from aperturaprogramatica a 
                Inner Join unidadorganizacional as u On a.uni_id = u.uni_id 
                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id 
                Inner Join _proyectos as p On p.proy_id = ap.proy_id 
                Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id 
                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id
                Inner Join _tipoproyecto as pt On p.tp_id=pt.tp_id
                Inner Join _proyectosprovincias as pp On p.proy_id = pp.proy_id
                Inner Join _proyectofuncionario as pfun On p.proy_id = pfun.proy_id
				Inner Join funcionario as fun on pfun.fun_id= fun.fun_id
                where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and 
                a.aper_gestion='.$gestion.' and pfun.pfun_tp=1 and
                (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and  
                pe.m_id='.$mes.' and p.tp_id '.$sql_in.' and 
                pf.pfec_estado=\'1\' and 
                pp.prov_id='.$prov_id.'
                union
                select p.*,pf.*,a.*,pe.*,u.*,pt.tp_tipo,fun_paterno|| \' \' ||fun_materno|| \' \' ||fun_nombre as responsable,fun_telefono, 0 as provincia 
                from aperturaprogramatica a 
                Inner Join unidadorganizacional as u On a.uni_id = u.uni_id 
                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id 
                Inner Join _proyectos as p On p.proy_id = ap.proy_id 
                Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id 
                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id
                Inner Join _tipoproyecto as pt On p.tp_id=pt.tp_id
                Inner Join _proyectosprovincias as pp On p.proy_id = pp.proy_id
                Inner Join _proyectofuncionario as pfun On p.proy_id = pfun.proy_id
				Inner Join funcionario as fun on pfun.fun_id= fun.fun_id
                where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and 
                a.aper_gestion='.$gestion.' and pfun.pfun_tp=1 and
                (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and  
                pe.m_id='.$mes.' and p.tp_id '.$sql_in.' and 
                pf.pfec_estado=\'1\' and 
                pp.prov_id<>'.$prov_id.') as _general
                order by tp_id,uni_unidad asc';
        }
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function ejecucion_proyecto_programas_x_region($sql_in,$gestion,$mes,$reg_id)
    {
        if($reg_id!=8){
    	$sql = 'select distinct p.*,pf.*,a.*,pe.*,u.*,pt.tp_tipo,fun_paterno|| \' \' ||fun_materno|| \' \' ||fun_nombre as responsable,fun_telefono
                from aperturaprogramatica a 
                Inner Join unidadorganizacional as u On a.uni_id = u.uni_id 
                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id 
                Inner Join _proyectos as p On p.proy_id = ap.proy_id 
                Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id
                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id
                Inner Join _tipoproyecto as pt On p.tp_id=pt.tp_id
                Inner Join _proyectosmunicipios as pm On p.proy_id = pm.proy_id
                Inner Join _proyectofuncionario as pfun On p.proy_id = pfun.proy_id
				Inner Join funcionario as fun on pfun.fun_id= fun.fun_id
				Inner Join _municipios as mun On pm.muni_id = mun.muni_id
                Inner Join _regiones as reg On mun.reg_id=reg.reg_id
                where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and 
                a.aper_gestion='.$gestion.' and pfun.pfun_tp=1 and
                (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and  
                pe.m_id='.$mes.' and p.tp_id '.$sql_in.' and 
                pf.pfec_estado=\'1\' and reg.reg_id='.$reg_id.'
                order by p.tp_id,uni_unidad asc';
        }else{
	        $sql = 'select * from (select distinct p.*,pf.*,a.*,pe.*,u.*,pt.tp_tipo,fun_paterno|| \' \' ||fun_materno|| \' \' ||fun_nombre as responsable,fun_telefono, 8 as region
                from aperturaprogramatica a 
                Inner Join unidadorganizacional as u On a.uni_id = u.uni_id 
                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id 
                Inner Join _proyectos as p On p.proy_id = ap.proy_id 
                Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id 
                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id
                Inner Join _tipoproyecto as pt On p.tp_id=pt.tp_id
                Inner Join _proyectosmunicipios as pm On p.proy_id = pm.proy_id
                Inner Join _proyectofuncionario as pfun On p.proy_id = pfun.proy_id
				Inner Join funcionario as fun on pfun.fun_id= fun.fun_id
				Inner Join _municipios as mun On pm.muni_id = mun.muni_id
                Inner Join _regiones as reg On mun.reg_id=reg.reg_id
                where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and 
                a.aper_gestion='.$gestion.' and pfun.pfun_tp=1 and
                (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and  
                pe.m_id='.$mes.' and p.tp_id '.$sql_in.' and 
                pf.pfec_estado=\'1\' and reg.reg_id='.$reg_id.'
                union
                select distinct p.*,pf.*,a.*,pe.*,u.*,pt.tp_tipo,fun_paterno|| \' \' ||fun_materno|| \' \' ||fun_nombre as responsable,fun_telefono,0 as region
                from aperturaprogramatica a 
                Inner Join unidadorganizacional as u On a.uni_id = u.uni_id 
                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id 
                Inner Join _proyectos as p On p.proy_id = ap.proy_id 
                Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id 
                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id
                Inner Join _tipoproyecto as pt On p.tp_id=pt.tp_id
                Inner Join _proyectosmunicipios as pm On p.proy_id = pm.proy_id
                Inner Join _proyectofuncionario as pfun On p.proy_id = pfun.proy_id
				Inner Join funcionario as fun on pfun.fun_id= fun.fun_id
				Inner Join _municipios as mun On pm.muni_id = mun.muni_id
                Inner Join _regiones as reg On mun.reg_id=reg.reg_id
                where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and 
                a.aper_gestion='.$gestion.' and pfun.pfun_tp=1 and
                (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and  
                pe.m_id='.$mes.' and p.tp_id '.$sql_in.' and 
                pf.pfec_estado=\'1\' and reg.reg_id<>'.$reg_id.' ) as _general
                order by tp_id,uni_unidad asc';
        }
        $query = $this->db->query($sql);
        return $query->result_array();
    }
}