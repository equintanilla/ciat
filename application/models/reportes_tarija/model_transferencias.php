<?php
class Model_transferencias extends CI_Model {
    public function __construct()
    {
        $this->load->database();
    }

    /*-------------- Lista de Municipios ------------*/
    public function list_municipios()
    {
        $sql = 'select *
                from _provincias p
                Inner Join _municipios as m On m.prov_id = p.prov_id
                where p.dep_id=\'6\' and m.muni_estado!=\'3\'';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*-------------- get Municipio ------------*/
    public function get_municipio($muni_id)
    {
        $sql = 'select *
                from _provincias p
                Inner Join _municipios as m On m.prov_id = p.prov_id
                where m.muni_id='.$muni_id.' and p.dep_id=\'6\' and m.muni_estado!=\'3\'';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*---------------------- PROGRAMAS SEGUN TIPO DE TRANSFERENCIA --------------------*/
    public function programas_hijos_transferencia($aper_programa,$gestion,$mes,$tp_trans,$muni_id)
    {
        $sql = 'select a.*,pe.*,u.*,p.*,pfg.*,m.*
                from aperturaprogramatica a
                Inner Join unidadorganizacional as u On a.uni_id = u.uni_id
                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
                Inner Join _proyectos as p On p.proy_id = ap.proy_id
                Inner Join _proyectosmunicipios as pm On pm.proy_id = ap.proy_id
                Inner Join _municipios as m On m.muni_id = pm.muni_id
                Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id
                Inner Join ptto_fase_gestion as pfg On pfg.pfec_id = pf.pfec_id
                where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and a.aper_programa=\''.$aper_programa.'\' and a.aper_gestion='.$gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and  pe.m_id='.$mes.' and p.tg_id='.$tp_trans.' and pf.pfec_estado=\'1\' and pfg.g_id='.$gestion.' and p.tp_id=\'2\' and m.muni_id='.$muni_id.'
                order by a.aper_programa,a.aper_proyecto,a.aper_actividad asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*---------------------- DETALLES PROGRAMAS SEGUN TIPO DE TRANSFERENCIA --------------------*/
    public function programas_hijos_transferencia_detalles($gestion,$mes,$tp_trans,$muni_id,$uni_ejec)
    {
        $sql = 'select a.*,pe.*,u.*,p.*,pfg.*,m.*
                from aperturaprogramatica a
                Inner Join unidadorganizacional as u On a.uni_id = u.uni_id
                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
                Inner Join _proyectos as p On p.proy_id = ap.proy_id
                Inner Join _proyectofuncionario as pfun On p.proy_id = pfun.proy_id
                Inner Join _proyectosmunicipios as pm On pm.proy_id = ap.proy_id
                Inner Join _municipios as m On m.muni_id = pm.muni_id
                Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id
                Inner Join ptto_fase_gestion as pfg On pfg.pfec_id = pf.pfec_id
                where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and a.aper_gestion='.$gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and  pe.m_id='.$mes.' and p.tg_id='.$tp_trans.' and pf.pfec_estado=\'1\' and pfg.g_id='.$gestion.' and (p.tp_id=\'2\' or p.tp_id=\'3\')  and pfun.pfun_tp=\'1\' and m.muni_id='.$muni_id.' and pfun.uni_ejec='.$uni_ejec.' 
                order by a.aper_programa,a.aper_proyecto,a.aper_actividad asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*---------------------- SUMA TOTAL PROGRAMAS SEGUN EL TIPO DE TRANSFERENCIA --------------------*/
    public function suma_total_programas_hijos_transferencia($gestion,$mes,$tp_trans)
    {
        $sql = 'select p.tg_id,SUM(pfecg_ppto_total) as costo,SUM(pe.pe_pi) as pi, SUM(pe_pm) as pm ,SUM(pe_pv) as pv ,SUM(pe_pe) as pe
                from aperturaprogramatica a
                Inner Join unidadorganizacional as u On a.uni_id = u.uni_id
                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
                Inner Join _proyectos as p On p.proy_id = ap.proy_id
                Inner Join _proyectofuncionario as pfun On p.proy_id = pfun.proy_id
                Inner Join _proyectosmunicipios as pm On pm.proy_id = ap.proy_id
                Inner Join _municipios as m On m.muni_id = pm.muni_id
                Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id
                Inner Join ptto_fase_gestion as pfg On pfg.pfec_id = pf.pfec_id
                where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and a.aper_gestion='.$gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and pe.m_id='.$mes.' and p.tg_id='.$tp_trans.' and pf.pfec_estado=\'1\' and pfg.g_id='.$gestion.' and (p.tp_id=\'2\' or p.tp_id=\'3\') 
                group by p.tg_id
                order by p.tg_id asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*---------------------- SUMA PROGRAMAS SEGUN EL TIPO DE TRANSFERENCIA SEGUN EL MUNICIPIO --------------------*/
    public function suma_programas_hijos_transferencia($gestion,$mes,$tp_trans,$muni_id)
    {
        $sql = 'select p.tg_id,SUM(pfecg_ppto_total) as costo,SUM(pe.pe_pi) as pi, SUM(pe_pm) as pm ,SUM(pe_pv) as pv ,SUM(pe_pe) as pe
                from aperturaprogramatica a
                Inner Join unidadorganizacional as u On a.uni_id = u.uni_id
                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
                Inner Join _proyectos as p On p.proy_id = ap.proy_id
                Inner Join _proyectosmunicipios as pm On pm.proy_id = ap.proy_id
                Inner Join _municipios as m On m.muni_id = pm.muni_id
                Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id
                Inner Join ptto_fase_gestion as pfg On pfg.pfec_id = pf.pfec_id
                where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and a.aper_gestion='.$gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and pe.m_id='.$mes.' and p.tg_id='.$tp_trans.' and pf.pfec_estado=\'1\' and pfg.g_id='.$gestion.' and (p.tp_id=\'2\' or p.tp_id=\'3\') and m.muni_id='.$muni_id.'
                group by p.tg_id
                order by p.tg_id asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }


}