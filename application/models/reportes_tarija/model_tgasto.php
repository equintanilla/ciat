<?php
class Model_tgasto extends CI_Model {
    public function __construct()
    {
        $this->load->database();
    }
    
    /*------------- tipo de gasto --------------*/
    public function tp_gasto($tp)
    {
        $sql = 'select *
                from tipo_gasto
                where tg_estado!=\'3\' and tg_id!=\'0\' and tg_tp='.$tp.'';

        $query = $this->db->query($sql);
        return $query->result_array();
    }
    /*----------- otros gastos --------------*/
    public function otros_gastos()
    {
        $sql = 'select *
                from tipo_gasto
                where tg_estado!=\'3\' and tg_id!=\'0\' and tg_tp!=\'1\' and tg_tp!=\'2\'
                order by tg_id asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }
    /*------------- tipo de gasto --------------*/
    public function tipo_gasto($tp)
    {
        $sql = 'select *
                from tipo_gasto
                where tg_estado!=\'3\' and tg_id!=\'0\' and tg_tp='.$tp.'
                order by tg_id asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }


    public function suma_tp_total($gestion,$mes)
    {
        $sql = 'select SUM(pfecg_ppto_total) as costo,SUM(pe.pe_pi) as pi, SUM(pe_pm) as pm ,SUM(pe_pv) as pv ,SUM(pe_pe) as pe
                from aperturaprogramatica a
                Inner Join unidadorganizacional as u On a.uni_id = u.uni_id
                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
                Inner Join _proyectos as p On p.proy_id = ap.proy_id
                Inner Join tipo_gasto as tg On tg.tg_id = p.tg_id
                Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id
                Inner Join ptto_fase_gestion as pfg On pfg.pfec_id = pf.pfec_id
                where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and a.aper_gestion='.$gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and  pe.m_id='.$mes.' and pf.pfec_estado=\'1\' and pfg.g_id='.$gestion.' 
        ';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function suma_tp_gasto($gestion,$mes,$tg_tp)
    {
        $sql = 'select tg.tg_tp,SUM(pfecg_ppto_total) as costo,SUM(pe.pe_pi) as pi, SUM(pe_pm) as pm ,SUM(pe_pv) as pv ,SUM(pe_pe) as pe
                from aperturaprogramatica a
                Inner Join unidadorganizacional as u On a.uni_id = u.uni_id
                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
                Inner Join _proyectos as p On p.proy_id = ap.proy_id
                Inner Join tipo_gasto as tg On tg.tg_id = p.tg_id
                Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id
                Inner Join ptto_fase_gestion as pfg On pfg.pfec_id = pf.pfec_id
                where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and a.aper_gestion='.$gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and  pe.m_id='.$mes.' and pf.pfec_estado=\'1\' and pfg.g_id='.$gestion.' and tg.tg_tp='.$tg_tp.'
                group by tg.tg_tp
                order by tg.tg_tp asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function suma_gasto_id($gestion,$mes,$tg_id)
    {
        $sql = 'select tg.tg_id,SUM(pfecg_ppto_total) as costo,SUM(pe.pe_pi) as pi, SUM(pe_pm) as pm ,SUM(pe_pv) as pv ,SUM(pe_pe) as pe
                from aperturaprogramatica a
                Inner Join unidadorganizacional as u On a.uni_id = u.uni_id
                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
                Inner Join _proyectos as p On p.proy_id = ap.proy_id
                Inner Join tipo_gasto as tg On tg.tg_id = p.tg_id
                Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id
                Inner Join ptto_fase_gestion as pfg On pfg.pfec_id = pf.pfec_id
                where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and a.aper_gestion='.$gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and pe.m_id='.$mes.' and pf.pfec_estado=\'1\' and pfg.g_id='.$gestion.' and tg.tg_id='.$tg_id.'
                group by tg.tg_id
                order by tg.tg_id asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function programas_hijos($aper_programa,$gestion,$mes,$tp_gasto)
    {
        $sql = 'select p.*,a.*,pe.*,pfg.*
                from aperturaprogramatica a
                Inner Join unidadorganizacional as u On a.uni_id = u.uni_id
                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
                Inner Join _proyectos as p On p.proy_id = ap.proy_id
                Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id
                Inner Join ptto_fase_gestion as pfg On pfg.pfec_id = pf.pfec_id
                where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\')  and a.aper_programa=\''.$aper_programa.'\' and a.aper_gestion='.$gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and  pe.m_id='.$mes.' and pf.pfec_estado=\'1\' and pfg.g_id='.$gestion.' and p.tg_id='.$tp_gasto.'
                order by a.aper_programa,a.aper_proyecto,a.aper_actividad asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

}