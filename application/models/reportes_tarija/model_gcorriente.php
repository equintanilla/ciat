<?php
class Model_gcorriente extends CI_Model {
    public function __construct()
    {
        $this->load->database();
    }

    /*-------------- Lista de unidades Ejecutoras todas ------------*/
    public function tp_unidad_ejec()
    {
        $sql = 'select *
                from tp_unidad_ejecutora
                where tue_estado!=\'3\'  and tue_id!=\'0\'
                order by tue_id asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*---------------------- EJECUCION PRESUPUESTARIA POR GASTO CORRIENTE --------------------*/
    public function programas_hijos_gcorriente($aper_programa,$gestion,$mes,$tue_id)
    {
        $sql = 'select a.*,pe.*,u.*,p.*,pfg.*
                from aperturaprogramatica a
                Inner Join unidadorganizacional as u On a.uni_id = u.uni_id
                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
                Inner Join _proyectos as p On p.proy_id = ap.proy_id
                Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id
                Inner Join ptto_fase_gestion as pfg On pfg.pfec_id = pf.pfec_id
                where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\')  and a.aper_programa=\''.$aper_programa.'\' and a.aper_gestion='.$gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and  pe.m_id='.$mes.' and p.tg_id=\'2\' and u.uni_ejecutora=\'1\' and pf.pfec_estado=\'1\' and pfg.g_id='.$gestion.' and u.tp_ue='.$tue_id.'
                order by a.aper_programa,a.aper_proyecto,a.aper_actividad asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*---------------------- DETALLES --------------------*/
    public function programas_hijos_gcorriente_detalles($gestion,$mes,$tue_id,$uni_ejec)
    {
        $sql = 'select a.*,pe.*,u.*,p.*,pfg.*
                from aperturaprogramatica a
                Inner Join unidadorganizacional as u On a.uni_id = u.uni_id
                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
                Inner Join _proyectos as p On p.proy_id = ap.proy_id
                Inner Join _proyectofuncionario as pfun On p.proy_id = pfun.proy_id
                Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id
                Inner Join ptto_fase_gestion as pfg On pfg.pfec_id = pf.pfec_id
                where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and a.aper_gestion='.$gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and  pe.m_id='.$mes.' and p.tg_id=\'2\' and u.uni_ejecutora=\'1\' and pf.pfec_estado=\'1\' and pfg.g_id='.$gestion.' and pfun.pfun_tp=\'1\' and u.tp_ue='.$tue_id.' and pfun.uni_ejec='.$uni_ejec.' 
                order by a.aper_programa,a.aper_proyecto,a.aper_actividad asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*---------------------- SUMA EJECUCION PRESUPUESTARIA POR GASTO CORRIENTE --------------------*/
    public function suma_programas_hijos_gcorriente($gestion,$mes,$tue_id)
    {
        $sql = 'select u.tp_ue,a.aper_programa,SUM(pfecg_ppto_total) as costo,SUM(pe.pe_pi) as pi, SUM(pe_pm) as pm ,SUM(pe_pv) as pv ,SUM(pe_pe) as pe
                from aperturaprogramatica a
                Inner Join unidadorganizacional as u On a.uni_id = u.uni_id
                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
                Inner Join _proyectos as p On p.proy_id = ap.proy_id
                Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id
                Inner Join ptto_fase_gestion as pfg On pfg.pfec_id = pf.pfec_id
                where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and a.aper_gestion='.$gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and  pe.m_id='.$mes.' and p.tg_id=\'2\' and u.uni_ejecutora=\'1\' and pf.pfec_estado=\'1\' and pfg.g_id='.$gestion.' and u.tp_ue='.$tue_id.'
                group by u.tp_ue, a.aper_programa
                order by u.tp_ue, a.aper_programa asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }


}