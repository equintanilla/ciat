<?php
class Model_codsec extends CI_Model {
    public function __construct()
    {
        $this->load->database();
    }

    /*-------------- Lista de Sectores ------------*/
    public function list_sectores()
    {
        $sql = 'select *
                from _clasificadorsectorial
                where nivel=\'1\' and estado!=\'3\' 
                order by codsectorial asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /*-------------- Get Codigo Sectorial ------------*/
    public function get_codigo_sectorial($codsec)
    {
        $sql = 'select *
                from _clasificadorsectorial
                where codsectorial=\''.$codsec.'\'';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    //////////// Proyectos inversion por codigo sectorial
    public function programas_hijos_inversionp($aper_programa,$gestion,$mes,$cod_sec)
    {
        $sql = 'select p.*,pfg.*,a.*,pe.*,csec.*
                from aperturaprogramatica a
                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
                Inner Join _proyectos as p On p.proy_id = ap.proy_id
                Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id
                Inner Join ptto_fase_gestion as pfg On pfg.pfec_id = pf.pfec_id
                Inner Join (
                    SELECT cd3.codsectorial as cod3, cd1.codsectorial as cod1
                    from _clasificadorsectorial cd3
                    Inner Join (SELECT codsectorial,codsectorialduf,descclasificadorsectorial,codsubsec from _clasificadorsectorial where nivel=\'2\') as cd2 On cd2.codsubsec=cd3.codsubsec
                    Inner Join (SELECT codsectorial,codsectorialduf,descclasificadorsectorial,codsec from _clasificadorsectorial where nivel=\'1\') as cd1 On cd1.codsec=cd3.codsec
                    ) as csec On csec.cod3 = p.codsectorial 
                    where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\')  and a.aper_programa=\''.$aper_programa.'\' and a.aper_gestion='.$gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and  pe.m_id='.$mes.' and p.tp_id=\'1\' and pf.pfec_estado=\'1\' and pfg.g_id='.$gestion.' and csec.cod1=\''.$cod_sec.'\'
                    order by a.aper_programa,a.aper_proyecto,a.aper_actividad asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    /////  Detalles
    public function programas_hijos_inversionp_detalles($gestion,$mes,$cod_sec,$uni_ejec)
    {
        $sql = 'select p.*,pfg.*,a.*,pe.*,csec.*
                from aperturaprogramatica a
                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
                Inner Join _proyectos as p On p.proy_id = ap.proy_id
                Inner Join _proyectofuncionario as pfun On p.proy_id = pfun.proy_id
                Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id
                Inner Join ptto_fase_gestion as pfg On pfg.pfec_id = pf.pfec_id
                Inner Join (
                    SELECT cd3.codsectorial as cod3, cd1.codsectorial as cod1
                    from _clasificadorsectorial cd3
                    Inner Join (SELECT codsectorial,codsectorialduf,descclasificadorsectorial,codsubsec from _clasificadorsectorial where nivel=\'2\') as cd2 On cd2.codsubsec=cd3.codsubsec
                    Inner Join (SELECT codsectorial,codsectorialduf,descclasificadorsectorial,codsec from _clasificadorsectorial where nivel=\'1\') as cd1 On cd1.codsec=cd3.codsec
                    ) as csec On csec.cod3 = p.codsectorial 
                    where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and a.aper_gestion='.$gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and  pe.m_id='.$mes.' and p.tp_id=\'1\' and pf.pfec_estado=\'1\' and pfg.g_id='.$gestion.' and pfun.pfun_tp=\'1\' and csec.cod1=\''.$cod_sec.'\' and pfun.uni_ejec='.$uni_ejec.'
                    order by a.aper_programa,a.aper_proyecto,a.aper_actividad asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function suma_programas_hijos_inversionp($aper_programa,$gestion,$mes,$cod_sec)
    {
        $sql = 'select csec.cod1,a.aper_programa,SUM(pfecg_ppto_total) as costo, SUM(pe.pe_pi) as pi, SUM(pe_pm) as pm ,SUM(pe_pv) as pv ,SUM(pe_pe) as pe
                from aperturaprogramatica a
                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
                Inner Join _proyectos as p On p.proy_id = ap.proy_id
                Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id
                Inner Join ptto_fase_gestion as pfg On pfg.pfec_id = pf.pfec_id
                Inner Join (
                    SELECT cd3.codsectorial as cod3, cd1.codsectorial as cod1
                    from _clasificadorsectorial cd3
                    Inner Join (SELECT codsectorial,codsectorialduf,descclasificadorsectorial,codsubsec from _clasificadorsectorial where nivel=\'2\') as cd2 On cd2.codsubsec=cd3.codsubsec
                    Inner Join (SELECT codsectorial,codsectorialduf,descclasificadorsectorial,codsec from _clasificadorsectorial where nivel=\'1\') as cd1 On cd1.codsec=cd3.codsec
                    ) as csec On csec.cod3 = p.codsectorial 
                    where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and a.aper_programa=\''.$aper_programa.'\' and a.aper_gestion='.$gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and  pe.m_id='.$mes.' and p.tp_id=\'1\' and pf.pfec_estado=\'1\' and pfg.g_id='.$gestion.' and csec.cod1=\''.$cod_sec.'\'
           group by csec.cod1, a.aper_programa
           order by csec.cod1, a.aper_programa asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }
    /*=======================================================================================*/

    //////////// Programas Recurrentes y No Recurrentes por codigo sectorial
    public function programas_hijos_programas($aper_programa,$gestion,$mes,$cod_sec)
    {
        $sql = 'select p.*,pfg.*,a.*,pe.*,csec.*
                from aperturaprogramatica a
                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
                Inner Join _proyectos as p On p.proy_id = ap.proy_id
                Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id
                Inner Join ptto_fase_gestion as pfg On pfg.pfec_id = pf.pfec_id
                Inner Join (
                    SELECT cd3.codsectorial as cod3, cd1.codsectorial as cod1
                    from _clasificadorsectorial cd3
                    Inner Join (SELECT codsectorial,codsectorialduf,descclasificadorsectorial,codsubsec from _clasificadorsectorial where nivel=\'2\') as cd2 On cd2.codsubsec=cd3.codsubsec
                    Inner Join (SELECT codsectorial,codsectorialduf,descclasificadorsectorial,codsec from _clasificadorsectorial where nivel=\'1\') as cd1 On cd1.codsec=cd3.codsec
                    ) as csec On csec.cod3 = p.codsectorial 
                    where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\')  and a.aper_programa=\''.$aper_programa.'\' and a.aper_gestion='.$gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and  pe.m_id='.$mes.' and (p.tp_id=\'1\' or p.tp_id=\'2\') and pf.pfec_estado=\'1\' and pfg.g_id='.$gestion.' and csec.cod1=\''.$cod_sec.'\'
                    order by a.aper_programa,a.aper_proyecto,a.aper_actividad asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    ///// Detalles
    public function programas_hijos_programas_detalles($gestion,$mes,$cod_sec,$uni_ejec)
    {
        $sql = 'select p.*,pfg.*,a.*,pe.*,csec.*
                from aperturaprogramatica a
                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
                Inner Join _proyectos as p On p.proy_id = ap.proy_id
                Inner Join _proyectofuncionario as pfun On p.proy_id = pfun.proy_id
                Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id
                Inner Join ptto_fase_gestion as pfg On pfg.pfec_id = pf.pfec_id
                Inner Join (
                    SELECT cd3.codsectorial as cod3, cd1.codsectorial as cod1
                    from _clasificadorsectorial cd3
                    Inner Join (SELECT codsectorial,codsectorialduf,descclasificadorsectorial,codsubsec from _clasificadorsectorial where nivel=\'2\') as cd2 On cd2.codsubsec=cd3.codsubsec
                    Inner Join (SELECT codsectorial,codsectorialduf,descclasificadorsectorial,codsec from _clasificadorsectorial where nivel=\'1\') as cd1 On cd1.codsec=cd3.codsec
                    ) as csec On csec.cod3 = p.codsectorial 
                    where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\') and a.aper_gestion='.$gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and  pe.m_id='.$mes.' and (p.tp_id=\'1\' or p.tp_id=\'2\') and pf.pfec_estado=\'1\' and pfg.g_id='.$gestion.' and pfun.pfun_tp=\'1\' and csec.cod1=\''.$cod_sec.'\' and pfun.uni_ejec='.$uni_ejec.' 
                    order by a.aper_programa,a.aper_proyecto,a.aper_actividad asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }
    public function suma_programas_hijos_programas($aper_programa,$gestion,$mes,$cod_sec)
    {
        $sql = 'select csec.cod1,a.aper_programa,SUM(pfecg_ppto_total) as costo, SUM(pe.pe_pi) as pi, SUM(pe_pm) as pm ,SUM(pe_pv) as pv ,SUM(pe_pe) as pe
                from aperturaprogramatica a
                Inner Join aperturaproyectos as ap On ap.aper_id = a.aper_id
                Inner Join _proy_ejec_ppto as pe On pe.aper_id = a.aper_id 
                Inner Join _proyectos as p On p.proy_id = ap.proy_id
                Inner Join _proyectofaseetapacomponente as pf On pf.proy_id = p.proy_id
                Inner Join ptto_fase_gestion as pfg On pfg.pfec_id = pf.pfec_id
                Inner Join (
                    SELECT cd3.codsectorial as cod3, cd1.codsectorial as cod1
                    from _clasificadorsectorial cd3
                    Inner Join (SELECT codsectorial,codsectorialduf,descclasificadorsectorial,codsubsec from _clasificadorsectorial where nivel=\'2\') as cd2 On cd2.codsubsec=cd3.codsubsec
                    Inner Join (SELECT codsectorial,codsectorialduf,descclasificadorsectorial,codsec from _clasificadorsectorial where nivel=\'1\') as cd1 On cd1.codsec=cd3.codsec
                    ) as csec On csec.cod3 = p.codsectorial 
                    where (a.aper_estado = \'1\' OR  a.aper_estado = \'2\')  and a.aper_programa=\''.$aper_programa.'\' and a.aper_gestion='.$gestion.' and (a.aper_proyecto <> \'0000\' or a.aper_actividad <> \'000\') and  pe.m_id='.$mes.' and (p.tp_id=\'1\' or p.tp_id=\'2\') and pf.pfec_estado=\'1\' and pfg.g_id='.$gestion.' and csec.cod1=\''.$cod_sec.'\'
           group by csec.cod1, a.aper_programa
           order by csec.cod1, a.aper_programa asc';

        $query = $this->db->query($sql);
        return $query->result_array();
    }

}