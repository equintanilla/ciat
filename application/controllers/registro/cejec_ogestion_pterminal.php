<?php

class Cejec_ogestion_pterminal extends CI_Controller
{
    var $gestion;
    public $rol = array('1' => '2','2' => '6');
    function __construct()
    {
        parent::__construct();
        $this->load->model('Users_model','',true);
        $this->load->model('registro_ejec/mejec_ogestion_pterminal');
        $this->load->model('mantenimiento/mpoa');
        //llamar a mi menu
        $this->load->library('menu');
        if($this->rolfun($this->rol)){
            $this->menu->const_menu(3);
            $this->gestion = $this->session->userData('gestion');
        }
        else{
          redirect('admin/dashboard');
        } 
        
    }

    //LISTA RED DE PROGRAMAS
    function lista_red_programas()
    {
        
        $data['lista_poa'] = $this->mejec_ogestion_pterminal->lista_prog();
        $ruta = 'registro_ejec/ejec_ogestion_pterminal/vlista_regpoa';
        $this->construir_vista($ruta, $data);
        
    }

    //LISTA OBJETIVOS DE GESTION - PRODUCTO TERMINAL
    function lista_ogestion_pterminal($poa_id, $aper_id, $o_id)
    {
        $data['aper_id'] = $aper_id;
        $data['poa_id'] = $poa_id;
        $data['dato_poa'] = $this->mpoa->dato_poa($poa_id, $this->gestion);
        $mes_id = $this->session->UserData('mes');//mes de creacion
        $data['mes_nombre'] = $this->obtener_mes();
        $data['mes_id'] = $mes_id;
        $data['atras'] = site_url("") . '/reg/ejec_op/';//para el boton de atras
        $lista_objgestion = $this->mejec_ogestion_pterminal->lista_ogestion($poa_id);
        $cont = 1;
        $tabla = '';
        foreach ($lista_objgestion as $row) {
            $nombres=$row['fun_nombre']. ' ' .$row['fun_paterno'] . ' ' . $row['fun_materno'];
            $tabla .= '<tr>';
            $tabla .= '<td >' . $cont . '</td>';
            $tabla .= '<td >' . $row['o_codigo'] . '</td>';
            $tabla .= '<td >' . $row['o_objetivo'] . '</td>';
            $tabla .= '<td >' . $row['nombre_indi'] . '</td>';
            $tabla .= '<td>' . $this->get_tabla_ogestion($row, $mes_id) . '</td>';
            $tabla .= '</tr >';
            $tabla .= $this->tabla_pterminal($row['o_id'], $cont, $mes_id,$nombres);
            $cont++;
            // $tabla .= $this->header_ogestion($cont);
        }
        $data['tabla_ejecucion'] = $tabla;
        $data['reg_ejec_programa_js'] = '<SCRIPT src="' . base_url() . 'mis_js/registro_ejecucion/reg_ejec_programa.js" type="text/javascript"></SCRIPT>';
        $ruta = 'registro_ejec/ejec_ogestion_pterminal/vejec_ogestion_pterminal';
        $this->construir_vista($ruta, $data);
    }

    //GENERA TABLA DE EJECUCION DEL OBJETIVO DE GESTION
    function get_tabla_ogestion($data, $mes)
    {
        $estilo = ' style="background-color: #568A89;color: white;font-weight: bold;"';//estilo para la tabla
        $o_id = $data['o_id'];
        $prog = $this->mejec_ogestion_pterminal->get_prog_ogestion($mes, $o_id);
        $p = ($data['indi_id'] == 1) ? '' : '%';//porcentaje para el caso de absoluto
        $num_p = (count($prog) == 0) ? 0 : $prog->opm_fis;//verificar si existe programacion fisica
        $tabla_o = '<table border="1" id="dt_basic1" class="table table-bordered">';
        //--- PROGRAMACION
        $tabla_o .= '<tr>';
        $tabla_o .= '<td' . $estilo . '>P.</td>';
        $tabla_o .= '<td><input type="text" value="' . $num_p . $p . '" disabled="disabled"></td>';

        if ($data['indi_id'] == 1) {
            //--- CASO ABSOLUTO
            $ejec = $this->mejec_ogestion_pterminal->get_eje_oabsoluto($mes, $o_id);//obtener ejecucion de tipo absoluto
            $num_e = (count($ejec) == 0) ? 0 : $ejec->oem_fis;//verificar si existe la ejecucion fisica
            $tabla_o .= '<td ' . $estilo . '> E.</td>';
            $tabla_o .= '<td title="EJECUCIÓN"><input type="text" name="o_id-' . $o_id . '-1" value="' . $num_e . '"
                            onkeypress="if (this.value.length < 15) { return numerosDecimales(event);}else{return false; }" required></td>';
            $tabla_o .= '</tr>';
        }else{
            //CASO RELATIVO
            $ejec = $this->mejec_ogestion_pterminal->get_eje_orelativo($mes, $o_id);//obtener ejecucion de tipo absoluto
            $num_f = (count($ejec) == 0) ? 0 : $ejec->oer_favorable;//verificar si existe la ejecucion favorable
            $num_d = (count($ejec) == 0) ? 0 : $ejec->oer_desfavorable;//verificar si existe la ejecucion desfavorable
            $tabla_o .= '<tr>';
            $tabla_o .= '<td ' . $estilo . '> NUMERADOR</td>';
            $tabla_o .= '<td title="EJECUCIÓN FAVORABLE"><input type="text" name="o_id-' . $o_id . '-2" id="o_id-' . $o_id . '-2" value="' . $num_f . '"
            onkeypress="if (this.value.length < 15) { return numerosDecimales(event);}else{return false; }" required></td>';
            $tabla_o .= '<td ' . $estilo . '> DENOMINADOR</td>';
            $tabla_o .= '<td title="EJECUCIÓN DESFAVORABLE"><input type="text" name="o_id-' . $o_id . '-3" id="o_id-' . $o_id . '-3" value="' . $num_d . '"
             onkeypress="if (this.value.length < 15) { return numerosDecimales(event);}else{return false; }" required></td>';
            $tabla_o .= '</tr>';
        }
        $tabla_o .= '</table>';
        return $tabla_o;
    }

    function header_ogestion($cont)
    {
        $estilo = ' style="background-color: #568A89;color: white;font-weight: bold;"';//estilo para la tabla
        $cabecera = '<tr>';
        $cabecera .= '<td ' . $estilo . '>' . $cont . '</td>';
        $cabecera .= '<td ' . $estilo . '>CÓDIGO</td>';
        $cabecera .= '<td ' . $estilo . '>ACCIÓN DE CORTO PLAZO</td>';
        $cabecera .= '<td ' . $estilo . '>T.I</td>';
        $cabecera .= '<td ' . $estilo . '>EJECUCIÓN - ' . $this->gestion . '</td > ';
        $cabecera .= '</tr>';
        return $cabecera;
    }

    function obtener_mes()
    {
        $mes [1] = 'ENERO';
        $mes [2] = 'FEBRERO';
        $mes [3] = 'MARZO';
        $mes [4] = 'ABRIL';
        $mes [5] = 'MAYO';
        $mes [6] = 'JUNIO';
        $mes [7] = 'JULIO';
        $mes [8] = 'AGOSTO';
        $mes [9] = 'SEPTIEMBRE';
        $mes [10] = 'OCTUBRE';
        $mes [11] = 'NOVIEMBRE';
        $mes [12] = 'DICIEMBRE';
        return $mes;
    }

    //genera tabla de producto terminal
    function tabla_pterminal($o_id, $cont_ogestion, $mes_id, $nombres)
    {
        $lista_pterminal = $this->mejec_ogestion_pterminal->lista_pterminal($o_id);

        $tabla = '';
        $cont_pterminal = 1;
        $c = 1;
        foreach ($lista_pterminal as $row) {
            ($c == 0) ? $tabla .= '' : $tabla .= $this->header_pterminal($cont_ogestion, $cont_pterminal);
            $c = 0;
            $tabla .= '<tr>';
            $tabla .= '<td >' . $cont_ogestion . '.' . $cont_pterminal . '</td>';
            $tabla .= '<td >' . $row['pt_codigo'] . '</td>';
            $tabla .= '<td ><b>' . $row['pt_objetivo'] . '<br>Indicador&nbsp;&nbsp;&nbsp;&nbsp;:</b> '. $row['pt_indicador'] .' <br><b>Linea Base :</b> 
            ' . $row['pt_linea_base'] .' <br><b>Meta Anual&nbsp;:</b> ' . $row['pt_meta'] .' <br><b>Medio Verif.:</b> ' . $row['pt_fuente_verificacion'] . '</td>';
            $tabla .= '<td >' . $nombres. '</td>';
            $tabla .= '<td >' . $row['nombre_indi'] . '</td>';
            $tabla .= '<td>' . $this->get_tabla_pterminal_ejec($row, $mes_id) . '</td>';
            $tabla .= '</tr >';
            $cont_pterminal++;
        }
        return $tabla;
    }

    function header_pterminal($cont_ogestion, $cont_pterminal)
    {
        $estilo = ' style="background-color: #618661;color: white;font-weight: bold;"';//estilo para la tabla
        $cabecera = '<tr>';
        $cabecera .= '<td ' . $estilo . '>' . $cont_ogestion . '</td>';
        $cabecera .= '<td ' . $estilo . '>CÓDIGO</td>';
        $cabecera .= '<td ' . $estilo . '>PRODUCTO TERMINAL</td>';
        $cabecera .= '<td ' . $estilo . '>RESPONSABLE</td>';
        $cabecera .= '<td ' . $estilo . '>T.I</td>';
        $cabecera .= '<td ' . $estilo . '>EJECUCIÓN - ' . $this->gestion . '</td > ';
        $cabecera .= '</tr>';
        return $cabecera;
    }

    //GENERA TABLA DE EJECUCION DEL PRODUCTO TERMINAL
    function get_tabla_pterminal_ejec($data, $mes_id)
    {
        $estilo = ' style="background-color: #618661;color: white;font-weight: bold;"';//estilo para la tabla
        $pt_id = $data['pt_id'];
        $prog = $this->mejec_ogestion_pterminal->get_prog_pterminal($mes_id, $pt_id);
        $p = ($data['indi_id'] == 1) ? '' : '%';//porcentaje para el caso de absoluto
        $num_p = (count($prog) == 0) ? 0 : $prog->ppm_fis;//verificar si existe programacion fisica
        $tabla_pt = '<table border="1" id="dt_basic1" class="table">';
        //--- PROGRAMACION
        $tabla_pt .= '<tr>';
        $tabla_pt .= '<td' . $estilo . '>P.</td>';
        $tabla_pt .= '<td><input type="text" value="' . $num_p . $p . '" disabled="disabled"></td>';

        if ($data['indi_id'] == 1) {
            //--- CASO ABSOLUTO
            $ejec = $this->mejec_ogestion_pterminal->get_eje_ptabsoluto($mes_id, $pt_id);//obtener ejecucion de tipo absoluto
            $num_e = (count($ejec) == 0) ? 0 : $ejec->pem_fis;//verificar si existe la ejecucion fisica
            $tabla_pt .= '<td' . $estilo . '> E.</td>';
            $tabla_pt .= '<td><input type="text" name="pt_id-' . $pt_id . '-1" value="' . $num_e . '"
                            onkeypress="if (this.value.length < 15) { return numerosDecimales(event);}else{return false; }" required></td>';
            $tabla_pt .= '</tr>';
        }else{
            //CASO VARIABLE
            $ejec = $this->mejec_ogestion_pterminal->get_eje_ptrelativo($mes_id, $pt_id);//obtener ejecucion de tipo absoluto
            $num_f = (count($ejec) == 0) ? 0 : $ejec->per_favorable;//verificar si existe la ejecucion favorable
            $num_d = (count($ejec) == 0) ? 0 : $ejec->per_desfavorable;//verificar si existe la ejecucion desfavorable
            $tabla_pt .= '<tr>';
            $tabla_pt .= '<td ' . $estilo . '> E.F</td>';
            $tabla_pt .= '<td><input type="text" name="pt_id-' . $pt_id . '-2" value="' . $num_f . '"
            onkeypress="if (this.value.length < 15) { return numerosDecimales(event);}else{return false; }" required></td>';
            $tabla_pt .= '<td ' . $estilo . '> E.D</td>';
            $tabla_pt .= '<td><input type="text" name="pt_id-' . $pt_id . '-3" value="' . $num_d . '"
             onkeypress="if (this.value.length < 15) { return numerosDecimales(event);}else{return false; }" required></td>';
            $tabla_pt .= '</tr>';
        }

        $tabla_pt .= '</table>';
        return $tabla_pt;
    }

    //mostrar ejecuciones del programa
    function mostrar_ejec_programa($poa_id, $aper_id)
    {
        $mes_id = $this->session->UserData('mes');
        $gestion = $this->session->UserData('gestion');
        $data['mes_nombre'] = $this->obtener_mes();
        $data['mes_id'] = $mes_id;
        $data['atras'] = site_url("") . '/reg/ejec_op/';//para el boton de atras
        $data['aper_id'] = $aper_id;
        $data['poa_id'] = $poa_id;
        $data['dato_poa'] = $this->mpoa->dato_poa($poa_id, $gestion);
        // var_dump($data['dato_poa']);  die;
        $data['dato_ejec_prog'] = $this->mejec_ogestion_pterminal->dato_ejec_prog($aper_id, $mes_id, $gestion);//dato de la ejecucion del programa
        $lista_objgestion = $this->mejec_ogestion_pterminal->lista_ogestion($poa_id);//obtener los datos de la ejecucion del programa
        // var_dump($lista_objgestion);  die;
        $data['lista_gestion'] = $lista_objgestion;
        $cont = 1;
        $tabla = '';
        foreach ($lista_objgestion as $row) {
            $nombres=$row['fun_nombre']. ' ' .$row['fun_paterno'] . ' ' . $row['fun_materno'];
            $tabla .= '<tr>';
            $tabla .= '<td >' . $cont . '</td>';
            $tabla .= '<td >' . $row['o_codigo'] . '</td>';
            $tabla .= '<td ><b>' . $row['o_objetivo'] . '<br>Indicador&nbsp;&nbsp;&nbsp;&nbsp;:</b> '. $row['o_indicador'] .' <br><b>Linea Base :</b> 
            ' . $row['o_linea_base'] .' <br><b>Meta Anual&nbsp;:</b> ' . $row['o_meta'] .' <br><b>Medio Verif.:</b> ' . $row['o_fuente_verificacion'] . '</td>';
            $tabla .= '<td >' . $row['fun_nombre'] .$row['fun_paterno'] . $row['fun_materno'] . '</td>';
            $tabla .= '<td >' . $row['nombre_indi'] . '</td>';
            $tabla .= '<td>' . $this->get_tabla_ogestion($row, $mes_id) . '</td>';
            $tabla .= '</tr >';
            $tabla .= $this->tabla_pterminal($row['o_id'], $cont, $mes_id, $nombres);
            $cont++;
            // $tabla .= $this->header_ogestion($cont);
        }
        $data['tabla_ejecucion'] = $tabla;
        $data['reg_ejec_programa_js'] = '<SCRIPT src="' . base_url() . 'mis_js/registro_ejecucion/reg_ejec_programa.js" type="text/javascript"></SCRIPT>';
        $ruta = 'registro_ejec/ejec_ogestion_pterminal/vejec_ogestion_pterminal';
        $this->construir_vista($ruta, $data);
    }

    //GUARDAR EJECUCION DEL OBJETIVO DE GESTION Y PRODUCTO TERMINAL
    function add_ejec_op()
    {
        if ($this->input->post()) {
            $post = $this->input->post();
            $mes_id = $post['mes_id'];
            if (isset($post['modificar'])) {
                $peticion = $this->mejec_ogestion_pterminal->guardar_ejec_op($post, $mes_id);
            } else {
                $reg_id = $this->mejec_ogestion_pterminal->guardar_ejec_op($post, $mes_id);
                //--- SUBIR ARCHIVO
                if ($_FILES["userfile"]["size"] == 0) {
                    $peticion = true;
                } else {
                    $filename = $_FILES["userfile"]["name"];
                    $file_basename = substr($filename, 0, strripos($filename, '.')); // get file extention
                    $file_ext = substr($filename, strripos($filename, '.')); // get file name
                    $filesize = $_FILES["userfile"]["size"];
                    $allowed_file_types = array('.jpg', '.doc', '.pdf', '.png', '.JPEG','.xlsx','.docx','.odt','.ods');
                    if (in_array($file_ext, $allowed_file_types) && ($filesize < 80000000)) { // Rename file
                        $newfilename = 'EP-' . $this->gestion . '-' . substr(md5(uniqid(rand())), 0, 5) . $file_ext;
                        //--------------------------------$data_to_store['pa_ruta_archivo'] = "" . $newfilename;
                        if (file_exists("archivos/ejec_programa/" . $newfilename)) {
                            echo "<script>alert('Ya existe este archivo')</script>";
                        } else {
                            move_uploaded_file($_FILES["userfile"]["tmp_name"], "archivos/ejec_programa/" . $newfilename);
                            //guardar mi area urbana despues de las validaciones
                            $data['pa_ruta_archivo'] = $newfilename;
                            $data['reg_id'] = $reg_id;
                            $data['pa_nombre'] = $post['nombre_archivo'];
                            $peticion = $this->db->INSERT('programa_mes_adjuntos', $data);
                        }
                    } elseif (empty($file_basename)) {
                        echo "Selecciona un archivo para cargarlo.";
                    } elseif ($filesize > 100000000) {
                        //redirect('');
                    } else {
                        $mensaje = "Sólo estos tipos de archivo se permiten para la carga: " . implode(', ', $allowed_file_types);
                        echo '<script>alert("' . $mensaje . '")</script>';
                        // unlink($_FILES["userfile"]["tmp_name"]);
                    }
                }
            }

            //----------------------------------------------------------------------
            if ($peticion) {
                $url = site_url("") . '/reg/ejec_op';
                echo '<script>
                            alert("EL REGISTRO SE GUARDO CORRECTAMENTE !!!")
                            window.location.href="' . $url . '";
                      </script>';
            } else {
                echo '<script>
                            alert("ERROR AL GUARDAR!!!")
                            window.location.reload(true);
                     </script>';
            }

        } else {
            show_404();
        }
    }

    //LISTA DE EJECUCIONES POR MES
    function lista_mes_ejec($poa_id, $aper_id)
    {
        $data['aper_id'] = $aper_id;
        $data['poa_id'] = $poa_id;
        $data['dato_poa'] = $this->mpoa->dato_poa($poa_id, $this->gestion)[0];
        $gestion = $this->gestion;
        $lista_mes_ejec = $this->mejec_ogestion_pterminal->lista_mes_ejec($aper_id, $gestion);
        $cont = 1;
        $tabla = '';
        $ruta_reg = site_url("") . '/reg/ejec_mes/' . $poa_id . '/' . $aper_id . '/';//ruta mostrar ejecucion
        $ruta_img_reg = base_url() . 'assets/ifinal/modificar.png';//ruta imagen registro
        $ruta_img_arc = base_url() . 'assets/ifinal/doc.jpg';//ruta imagen ARCHIVOS
        $ruta_arc = site_url("") . '/reg/list_arc/';//ruta de archivos
        $ruta_img_rev = base_url() . 'assets/ifinal/rever.png';
        $ruta_img_no_rev = base_url() . 'assets/ifinal/4.png';//para el caso de no revertir
        foreach ($lista_mes_ejec AS $row) {
            $tabla .= '<tr>';
            $tabla .= '<td>' . $cont . '</td>';
            $tabla .= '<td>';
            $tabla .= '<a href="' . $ruta_reg . $row['reg_mes'] . '" title="REGISTRO DE EJECUCIÓN">
						    <img src="' . $ruta_img_reg . '" width="30" height="30" class="img-responsive" title="REGISTRO DE EJECUCIÓN">
					   </a>';
            $tabla .= '</td>';
            $tabla .= '<td>';
            $tabla .= '<a href="' . $ruta_arc . $poa_id . '/' . $aper_id . '/' . $row['reg_id'] . '/' . $row['reg_mes'] . '" title="LISTA DE ARCHIVOS">
						    <img src="' . $ruta_img_arc . '" width="30" height="30" class="img-responsive" title="LISTA DE ARCHIVOS">
					   </a>';
            $tabla .= '</td>';
            $tabla .= '<td>';
            if ($row['reg_validar'] == 2) {
                $tabla .= '<a title="NO PUEDE REVERTIR EL REGISTRO" >
						    <img src="' . $ruta_img_no_rev . '" width="30" height="30" class="img-responsive" title="NO PUEDE REVERTIR EL REGISTRO">
					   </a>';
            } else {
                $tabla .= '<a title="REVERTIR REGISTRO DE EJECUCIÓN" id="' . $row['reg_mes'] . '" name="' . $row['reg_id'] . '" class="revertir">
						    <img src="' . $ruta_img_rev . '" width="30" height="30" class="img-responsive" title="REVERTIR REGISTRO DE EJECUCIÓN">
					   </a>';
            }

            $tabla .= '</td>';
            $tabla .= '<td>' . $row['m_descripcion'] . '</td>';
            $tabla .= '<td>' . $row['reg_problemas'] . '</td>';
            $tabla .= '<td>' . $row['reg_causas'] . '</td>';
            $tabla .= '<td>' . $row['reg_soluciones'] . '</td>';
            $tabla .= '<td>' . $row['fun_nombre'] . $row['fun_paterno'] . $row['fun_materno'] . '</td>';
            $tabla .= '<td>' . date('d-m-Y', strtotime($row['fecha_creacion'])) . '</td>';
            $tabla .= '<td>' . $row['estado'] . '</td>';
            $tabla .= '</tr>';
            $cont++;
        }
        $data['tabla_ejec_mes'] = $tabla;
        $data['mes_nombre'] = $this->obtener_mes();
        $data['reg_ejec_programa_js'] = '<SCRIPT src="' . base_url() . 'mis_js/registro_ejecucion/reg_ejec_programa.js" type="text/javascript"></SCRIPT>';
        $ruta = 'registro_ejec/ejec_ogestion_pterminal/vlista_meses_ejec';
        $this->construir_vista($ruta, $data);
    }

    //EJECUCION DEL PROGRAMA POR MES
    function ejecucion_mes($poa_id, $aper_id, $mes_id)
    {
        $gestion = $this->session->UserData('gestion');
        $data['mes_nombre'] = $this->obtener_mes();
        $data['mes_id'] = $mes_id;
        $data['aper_id'] = $aper_id;
        $data['poa_id'] = $poa_id;
        $data['dato_poa'] = $this->mpoa->dato_poa($poa_id, $gestion);
        $data['atras'] = site_url("") . '/reg/lmes/' . $poa_id . '/' . $aper_id;//para el boton de atras
        $data['dato_ejec_prog'] = $this->mejec_ogestion_pterminal->dato_ejec_prog($aper_id, $mes_id, $gestion);//dato de la ejecucion del programa
        $lista_objgestion = $this->mejec_ogestion_pterminal->lista_ogestion($poa_id);//obtener los datos de la ejecucion del programa
        $cont = 1;
        $tabla = '';
        foreach ($lista_objgestion as $row) {
            $nombres=$row['fun_nombre']. ' ' .$row['fun_paterno'] . ' ' . $row['fun_materno'];
            $tabla .= '<tr>';
            $tabla .= '<td >' . $cont . '</td>';
            $tabla .= '<td >' . $row['o_codigo'] . '</td>';
            $tabla .= '<td >' . $row['o_objetivo'] . '</td>';
            $tabla .= '<td >' . $row['nombre_indi'] . '</td>';
            $tabla .= '<td>' . $this->get_tabla_ogestion($row, $mes_id) . '</td>';
            $tabla .= '</tr >';
            $tabla .= $this->tabla_pterminal($row['o_id'], $cont, $mes_id,$nombres);
            $cont++;
            $tabla .= $this->header_ogestion($cont);
        }
        $data['tabla_ejecucion'] = $tabla;
        $data['reg_ejec_programa_js'] = '<SCRIPT src="' . base_url() . 'mis_js/registro_ejecucion/reg_ejec_programa.js" type="text/javascript"></SCRIPT>';
        $ruta = 'registro_ejec/ejec_ogestion_pterminal/vejec_ogestion_pterminal';
        $this->construir_vista($ruta, $data);
    }

    //REVERTIR EJECUCION DEL PROGRAMA
    function revertir_ejec_programa()
    {
        if ($this->input->post() && $this->input->is_ajax_request()) {
            $post = $this->input->post();
            $poa_id = $post['poa_id'];
            $reg_id = $post['reg_id'];
            $mes_id = $post['mes_id'];
            //revertir ejecucion del programa
            $peticion = $this->mejec_ogestion_pterminal->revertir_programa($poa_id, $reg_id, $mes_id);
            if ($peticion) {
                $data['peticion'] = 'verdadero';
            } else {
                $data['peticion'] = 'falso';
            }
            echo json_encode($data);
        } else {
            show_404();
        }


    }

    //LISTA DE ARCHIVOS POR MES
    function lista_archivos($poa_id, $aper_id)
    {
        $mes_id = $this->session->UserData('mes');
        $gestion = $this->session->UserData('gestion');
        $data['mes_nombre'] = $this->obtener_mes();
        $data['poa_id'] = $poa_id;
        $data['aper_id'] = $aper_id;
        $data['mes_id'] = $mes_id;
        $data['atras'] = site_url("") . '/reg/ejec_op/';//para el boton de atras
        $dato_ejec_prog = $this->mejec_ogestion_pterminal->dato_ejec_prog($aper_id, $mes_id, $gestion);//dato de ejecucion de programa
        $data['reg_id'] = $dato_ejec_prog->reg_id;
        $data['lista_archivos'] = $this->mejec_ogestion_pterminal->lista_archivos($dato_ejec_prog->reg_id);
        $data['dato_poa'] = $this->mpoa->dato_poa($poa_id, $gestion);
        $data['principal'] = 1;
        $data['reg_ejec_programa_js'] = '<SCRIPT src="' . base_url() . 'mis_js/registro_ejecucion/reg_ejec_programa.js" type="text/javascript"></SCRIPT>';
        $ruta = 'registro_ejec/ejec_ogestion_pterminal/vlista_archivo';
        $this->construir_vista($ruta, $data);
    }

    //lista de archivos
    function l_archivos_mes($poa_id, $aper_id, $reg_id, $mes_id)
    {
        $data['mes_nombre'] = $this->obtener_mes();
        $data['poa_id'] = $poa_id;
        $data['aper_id'] = $aper_id;
        $data['mes_id'] = $mes_id;
        $data['atras'] = site_url("") . '/reg/lmes/' . $poa_id . '/' . $aper_id;//para el boton de atras
        $data['reg_id'] = $reg_id;
        $data['principal'] = 2;
        $data['lista_archivos'] = $this->mejec_ogestion_pterminal->lista_archivos($reg_id);
        $data['dato_poa'] = $this->mpoa->dato_poa($poa_id, $this->gestion);
        $data['reg_ejec_programa_js'] = '<SCRIPT src="' . base_url() . 'mis_js/registro_ejecucion/reg_ejec_programa.js" type="text/javascript"></SCRIPT>';
        $ruta = 'registro_ejec/ejec_ogestion_pterminal/vlista_archivo';
        $this->construir_vista($ruta, $data);
    }

    //ADICIONAR ARCHIVOS
    function add_arc()
    {
        if ($this->input->post()) {
            $post = $this->input->post();
            $reg_id = $post['reg_id'];
            $mes_id = $this->mejec_ogestion_pterminal->get_prog_ejec($reg_id);
            $mes_id = $mes_id->reg_mes;
            $aper_id = $post['aper_id'];
            $poa_id = $post['poa_id'];
            //--- SUBIR ARCHIVO
            $filename = $_FILES["file"]["name"];
            $file_basename = substr($filename, 0, strripos($filename, '.')); // get file extention
            $file_ext = substr($filename, strripos($filename, '.')); // get file name
            $filesize = $_FILES["file"]["size"];
            $allowed_file_types = array('.pdf', '.doc', '.jpg', '.JPG', '.png', '.JPEG');
            if (in_array($file_ext, $allowed_file_types) && ($filesize < 80000000)) { // Rename file
                $newfilename = 'EP-' . $this->gestion . '-' . substr(md5(uniqid(rand())), 0, 5) . $file_ext;
                //--------------------------------$data_to_store['pa_ruta_archivo'] = "" . $newfilename;
                if (file_exists("archivos/ejec_programa/" . $newfilename)) {
                    echo "<script>alert('Ya existe este archivo')</script>";
                } else {
                    move_uploaded_file($_FILES["file"]["tmp_name"], "archivos/ejec_programa/" . $newfilename);
                    //guardar mi area urbana despues de las validaciones
                    $dato['pa_ruta_archivo'] = $newfilename;
                    $dato['reg_id'] = $reg_id;
                    $dato['pa_nombre'] = $post['nombre_archivo'];
                    $this->db->INSERT('programa_mes_adjuntos', $dato);
                    if ($post['principal'] == 1) {
                        redirect(site_url() . '/reg/arc/' . $poa_id . '/' . $aper_id);
                    } else {
                        redirect(site_url() . '/reg/list_arc/' . $poa_id . '/' . $aper_id . '/' . $reg_id . '/' . $mes_id);
                    }

                }
            } elseif (empty($file_basename)) {
                echo "Selecciona un archivo para cargarlo.";
            } elseif ($filesize > 100000000) {
                //redirect('');
            } else {
                $mensaje = "Sólo estos tipos de archivo se permiten para la carga: " . implode(', ', $allowed_file_types);
                echo '<script>alert("' . $mensaje . '")</script>';
            }

        } else {
            show_404();
        }
    }

    function eliminar_archivo()
    {
        if ($this->input->post() && $this->input->is_ajax_request()) {
            $pa_id = $this->input->post('p_id');
            $ruta = $this->mejec_ogestion_pterminal->lista_arch_prog_arch($pa_id);
            $file = "archivos/ejec_programa/" . $ruta->pa_ruta_archivo;
            unlink($file);
            $this->db->WHERE('pa_id', $pa_id);
            $peticion = $this->db->DELETE('programa_mes_adjuntos');
            if ($peticion) {
                $data['peticion'] = 'verdadero';
            } else {
                $data['peticion'] = 'falso';
            }
            echo json_encode($data);
        } else {
            show_404();
        }
    }

    //FUNCION PARA CONSTRUIR MI VISTA
    function construir_vista($ruta, $data)
    {
        //----------------------------------- MENU-------------------------------
        $menu['enlaces'] = $this->menu->get_enlaces();
        $menu['subenlaces'] = $this->menu->get_sub_enlaces();
        $menu['titulo'] = 'REGISTRO DE EJECUCIÓN';
        //-----------------------------------------------------------------------
        //armar vista
        $this->load->view('includes/header');
        $this->load->view('includes/menu_lateral', $menu);
        $this->load->view($ruta, $data);//contenido
        $this->load->view('includes/footer');
    }

    function rolfun($rol)
    {
      $valor=false;
      for ($i=1; $i <=count($rol) ; $i++) { 
        $data = $this->Users_model->get_datos_usuario_roles($this->session->userdata('fun_id'),$rol[$i]);
        if(count($data)!=0){
          $valor=true;
          break;
        }
      }
      return $valor;
    }
}