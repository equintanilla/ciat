<?php
define("DOMPDF_ENABLE_REMOTE", true);
define("DOMPDF_TEMP_DIR", "/tmp");
class Reporte_ejec extends CI_Controller
{
    private $estilo_vertical = '<style>
        body{
            font-family: sans-serif;
            }
        @page {
        margin: 40px 40px;
        }
        header {
            position: fixed;
            left: 0px;
            top: -20px;
            right: 0px;
            height: 20px;
            background-color: #fff;
            }
        header h1{
            margin: 5px 0;
        }
        header h2{
            margin: 0 0 10px 0;
        }
        footer {
            position: fixed;
            left: 0px;
            bottom: -82px;
            right: 0px;
            height: 100px;
            border-bottom: 1px solid #ddd;
        }
        footer .page:after {
            content: counter(page) ;
        }
        footer table {
            width: 100%;
        }
        footer p {
            text-align: right;
        }
        footer .izq {
            text-align: left;
        }
        table{
            font-size: 7px;
            width: 100%;
            background-color:#fff;
        }
        th, td {
                padding: 1.5px;
                text-align: center;
            }
        .mv{font-size:10px;}
        .verde{ width:100%; height:5px; background-color:#3B7F2A;}
        .rojo{ width:100%; height:5px; background-color:#D03437;}
        .siipp{width:180px;}
        .header_table {
            color: #ffffff;
            text-align: center;
            align-items: center;
            align-self: center;
            font-weight: bold;
            background-color: #696969;
            align-content: center;
        }
        .header_subtable {
            color: #ffffff;
            text-align: center;
            align-items: center;
            align-self: center;
            font-weight: bold;
            background-color: #a59393;
            align-content: center;
        }
        .titulo_pdf {
            text-align: left;
            font-size: 8px;
        }
        .datos_principales {
            text-align: center;
            font-size: 8px;
        }
        .pdes_titulo{
            text-decoration: underline;
            font-weight: bold;
            text-transform: uppercase;
        }
        .td_pdes{
            text-align: left;  
        }
        .indi_desemp{
            font-size: 9px;
            border: 1px solid #CCC;
            background-color: #E0E0E0;
            padding: .5em;
        }
        table.fixed { table-layout:fixed; }
        table.fixed td { overflow: hidden; }
        .sub_table{
            border-bottom:1px solid black;
        }
        .contenedor_principal{
            border-style: solid;
            border-width:1px;
        }
        .titulo_dictamen{
            padding:5px;
            background-color: #454545;
            border-style: solid;margin: 2px;
            border-width: 1px;
            color: white;
            font-size: 7px;
            border-color: black;
        }
        .contenedor_datos{
            padding:0px;
            margin:1px;
        }
        .table_contenedor{
            border-style: solid;
            margin: 0px;
            padding: 3px;
            border-width: 1px;
            border-collapse: collapse;
        }
        .collapse_t td{
            border-style: solid;
            margin: 0px;
            padding: 3px;
            border-width: 1px;
            border-collapse: collapse;
        }
        .fila_unitaria{
            text-align:left;
            padding: 8;
        }
        .lista{
            text-align:left;
            padding-left: 8;
            list-style-type:square;
            margin:2px;
        }
    </style>';
    public function __construct()
    {
        parent::__construct();
        $this->load->library('pdf');
        $this->load->library('pdf2');
        $this->load->model('menu_modelo');
        $this->load->model('reportes/model_objetivo');
        $this->load->model('Users_model', '', true);

        $this->load->model('registro_ejec/mejec_ogestion_pterminal');
        $this->load->model('mantenimiento/mpoa');

        $this->gestion = $this->session->userData('gestion');
        // echo 'suuun';  die;
    }
    
    //GENERA TABLA DE EJECUCION DEL OBJETIVO DE GESTION
    function get_tabla_ogestion1($data, $mes)
    {
        $estilo = ' style="background-color: #568A89;color: white;font-weight: bold;"';//estilo para la tabla
        $o_id = $data['o_id'];
        $prog = $this->mejec_ogestion_pterminal->get_prog_ogestion($mes, $o_id);
        $p = ($data['indi_id'] == 1) ? '' : '%';//porcentaje para el caso de absoluto
        $num_p = (count($prog) == 0) ? 0 : $prog->opm_fis;//verificar si existe programacion fisica
        $tabla_o = '<table border="1" id="dt_basic1" class="table table-bordered">';
        //--- PROGRAMACION
        $tabla_o .= '<tr>';
        $tabla_o .= '<td' . $estilo . '>P.</td>';
        $tabla_o .= '<td><input type="text" value="' . $num_p . $p . '" disabled="disabled"></td>';

        if ($data['indi_id'] == 1) {
            //--- CASO ABSOLUTO
            $ejec = $this->mejec_ogestion_pterminal->get_eje_oabsoluto($mes, $o_id);//obtener ejecucion de tipo absoluto
            $num_e = (count($ejec) == 0) ? 0 : $ejec->oem_fis;//verificar si existe la ejecucion fisica
            $tabla_o .= '<td ' . $estilo . '> E.</td>';
            $tabla_o .= '<td title="EJECUCIÓN"><input type="text" name="o_id-' . $o_id . '-1" value="' . $num_e . '"
                            onkeypress="if (this.value.length < 15) { return numerosDecimales(event);}else{return false; }" required></td>';
            $tabla_o .= '</tr>';
        }else{
            //CASO RELATIVO
            $ejec = $this->mejec_ogestion_pterminal->get_eje_orelativo($mes, $o_id);//obtener ejecucion de tipo absoluto
            $num_f = (count($ejec) == 0) ? 0 : $ejec->oer_favorable;//verificar si existe la ejecucion favorable
            $num_d = (count($ejec) == 0) ? 0 : $ejec->oer_desfavorable;//verificar si existe la ejecucion desfavorable
            $tabla_o .= '<tr>';
            $tabla_o .= '<td ' . $estilo . '> E.F</td>';
            $tabla_o .= '<td title="EJECUCIÓN FAVORABLE"><input type="text" name="o_id-' . $o_id . '-2" id="o_id-' . $o_id . '-2" value="' . $num_f . '"
            onkeypress="if (this.value.length < 15) { return numerosDecimales(event);}else{return false; }" required></td>';
            $tabla_o .= '<td ' . $estilo . '> E.D</td>';
            $tabla_o .= '<td title="EJECUCIÓN DESFAVORABLE"><input type="text" name="o_id-' . $o_id . '-3" id="o_id-' . $o_id . '-3" value="' . $num_d . '"
             onkeypress="if (this.value.length < 15) { return numerosDecimales(event);}else{return false; }" required></td>';
            $tabla_o .= '</tr>';
        }
        $tabla_o .= '</table>';
        return $tabla_o;
    }

    //genera tabla de producto terminal
    function tabla_pterminal1($o_id, $cont_ogestion, $mes_id)
    {
        $lista_pterminal = $this->mejec_ogestion_pterminal->lista_pterminal($o_id);
        $tabla = '';
        $cont_pterminal = 1;
        $c = 1;
        foreach ($lista_pterminal as $row) {
            ($c == 0) ? $tabla .= '' : $tabla .= $this->header_pterminal($cont_ogestion, $cont_pterminal);
            $c = 0;
            $tabla .= '<tr>';
            $tabla .= '<td >' . $cont_ogestion . '.' . $cont_pterminal . '</td>';
            $tabla .= '<td >' . $row['pt_codigo'] . '</td>';
            $tabla .= '<td ><b>' . $row['pt_objetivo'] . '<br>Indicador&nbsp;&nbsp;&nbsp;&nbsp;:</b> '. $row['pt_indicador'] .' <br><b>Linea Base :</b> 
            ' . $row['pt_linea_base'] .' <br><b>Meta Anual&nbsp;:</b> ' . $row['pt_meta'] .' <br><b>Medio Verif.:</b> ' . $row['pt_fuente_verificacion'] . '</td>';
            $tabla .= '<td >' . $row['nombre_indi'] . '</td>';
            $tabla .= '<td>' . $this->get_tabla_pterminal_ejec($row, $mes_id) . '</td>';
            $tabla .= '</tr >';
            $cont_pterminal++;
        }
        return $tabla;
    }

    function get_tabla_pterminal_ejec($data, $mes_id)
    {
        $estilo = ' style="background-color: #618661;color: white;font-weight: bold;"';//estilo para la tabla
        $pt_id = $data['pt_id'];
        $prog = $this->mejec_ogestion_pterminal->get_prog_pterminal($mes_id, $pt_id);
        $p = ($data['indi_id'] == 1) ? '' : '%';//porcentaje para el caso de absoluto
        $num_p = (count($prog) == 0) ? 0 : $prog->ppm_fis;//verificar si existe programacion fisica
        $tabla_pt = '<table border="1" id="dt_basic1" class="table">';
        //--- PROGRAMACION
        $tabla_pt .= '<tr>';
        $tabla_pt .= '<td' . $estilo . '>P.</td>';
        $tabla_pt .= '<td><input type="text" value="' . $num_p . $p . '" disabled="disabled"></td>';

        if ($data['indi_id'] == 1) {
            //--- CASO ABSOLUTO
            $ejec = $this->mejec_ogestion_pterminal->get_eje_ptabsoluto($mes_id, $pt_id);//obtener ejecucion de tipo absoluto
            $num_e = (count($ejec) == 0) ? 0 : $ejec->pem_fis;//verificar si existe la ejecucion fisica
            $tabla_pt .= '<td' . $estilo . '> E.</td>';
            $tabla_pt .= '<td><input type="text" name="pt_id-' . $pt_id . '-1" value="' . $num_e . '"
                            onkeypress="if (this.value.length < 15) { return numerosDecimales(event);}else{return false; }" required></td>';
            $tabla_pt .= '</tr>';
        }else{
            //CASO VARIABLE
            $ejec = $this->mejec_ogestion_pterminal->get_eje_ptrelativo($mes_id, $pt_id);//obtener ejecucion de tipo absoluto
            $num_f = (count($ejec) == 0) ? 0 : $ejec->per_favorable;//verificar si existe la ejecucion favorable
            $num_d = (count($ejec) == 0) ? 0 : $ejec->per_desfavorable;//verificar si existe la ejecucion desfavorable
            $tabla_pt .= '<tr>';
            $tabla_pt .= '<td ' . $estilo . '> E.F</td>';
            $tabla_pt .= '<td><input type="text" name="pt_id-' . $pt_id . '-2" value="' . $num_f . '"
            onkeypress="if (this.value.length < 15) { return numerosDecimales(event);}else{return false; }" required></td>';
            $tabla_pt .= '<td ' . $estilo . '> E.D</td>';
            $tabla_pt .= '<td><input type="text" name="pt_id-' . $pt_id . '-3" value="' . $num_d . '"
             onkeypress="if (this.value.length < 15) { return numerosDecimales(event);}else{return false; }" required></td>';
            $tabla_pt .= '</tr>';
        }

        $tabla_pt .= '</table>';
        return $tabla_pt;
    }

    function header_pterminal($cont_ogestion, $cont_pterminal)
    {
        $estilo = ' style="background-color: #618661;color: white;font-weight: bold;"';//estilo para la tabla
        $cabecera = '<tr>';
        $cabecera .= '<td ' . $estilo . '>' . $cont_ogestion . '</td>';
        $cabecera .= '<td ' . $estilo . '>CÓDIGO</td>';
        $cabecera .= '<td ' . $estilo . '>PRODUCTO TERMINAL</td>';
        $cabecera .= '<td ' . $estilo . '>T.I</td>';
        $cabecera .= '<td ' . $estilo . '>EJECUCIÓN - ' . $this->gestion . '</td > ';
        $cabecera .= '</tr>';
        return $cabecera;
    }

    function header_ogestion1($cont)
    {
        $estilo = ' style="background-color: #568A89;color: white;font-weight: bold;"';//estilo para la tabla
        $cabecera = '<tr>';
        $cabecera .= '<td ' . $estilo . '>' . $cont . '</td>';
        $cabecera .= '<td ' . $estilo . '>CÓDIGO</td>';
        $cabecera .= '<td ' . $estilo . '>ACCIÓN DE CORTO PLAZO</td>';
        $cabecera .= '<td ' . $estilo . '>T.I</td>';
        $cabecera .= '<td ' . $estilo . '>EJECUCIÓN - ' . $this->gestion . '</td > ';
        $cabecera .= '</tr>';
        return $cabecera;
    }

    function obtener_mes1()
    {
        $mes [1] = 'ENERO';
        $mes [2] = 'FEBRERO';
        $mes [3] = 'MARZO';
        $mes [4] = 'ABRIL';
        $mes [5] = 'MAYO';
        $mes [6] = 'JUNIO';
        $mes [7] = 'JULIO';
        $mes [8] = 'AGOSTO';
        $mes [9] = 'SEPTIEMBRE';
        $mes [10] = 'OCTUBRE';
        $mes [11] = 'NOVIEMBRE';
        $mes [12] = 'DICIEMBRE';
        return $mes;
    }
  
    /////////*******RED DE OBJETIVOS --> OBJETIVOS GESTION*******////////
    public function mostrar_ejec_programa($poa_id, $aper_id)
    {
        $mes_id = $this->session->UserData('mes');
        $gestion = $this->session->UserData('gestion');
        $data['mes_nombre'] = $this->obtener_mes1();
        $data['mes_id'] = $mes_id;
        $data['atras'] = site_url("") . '/reg/ejec_op/';//para el boton de atras
        $data['aper_id'] = $aper_id;
        $data['poa_id'] = $poa_id;
        $data['dato_poa'] = $this->mpoa->dato_poa($poa_id, $gestion);
        // var_dump($data['dato_poa']);  die;
        $data['dato_ejec_prog'] = $this->mejec_ogestion_pterminal->dato_ejec_prog($aper_id, $mes_id, $gestion);//dato de la ejecucion del programa
        $lista_objgestion = $this->mejec_ogestion_pterminal->lista_ogestion($poa_id);//obtener los datos de la ejecucion del programa
        $data['lista_gestion'] = $lista_objgestion;
        $cont = 1;
        $tabla = '';
        foreach ($lista_objgestion as $row) {
            $tabla .= '<tr>';
            $tabla .= '<td >' . $cont . '</td>';
            $tabla .= '<td >' . $row['o_codigo'] . '</td>';
            $tabla .= '<td ><b>' . $row['o_objetivo'] . '<br>Indicador&nbsp;&nbsp;&nbsp;&nbsp;:</b> '. $row['o_indicador'] .' <br><b>Linea Base :</b> 
            ' . $row['o_linea_base'] .' <br><b>Meta Anual&nbsp;:</b> ' . $row['o_meta'] .' <br><b>Medio Verif.:</b> ' . $row['o_fuente_verificacion'] . '</td>';
            $tabla .= '<td >' . $row['nombre_indi'] . '</td>';
            $tabla .= '<td>' . $this->get_tabla_ogestion1($row, $mes_id) . '</td>';
            $tabla .= '</tr >';
            $tabla .= $this->tabla_pterminal1($row['o_id'], $cont, $mes_id);
            $cont++;
            // $tabla .= $this->header_ogestion1($cont);
        }
        


        $html = '';

        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.'</head>
            <body>
                <header>
                </header>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/logo.jpg" class="siipp"/>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata('entidad').'<br>
                            <b>POA - PLAN OPERATIVO POR RESULTADOS - </b> '.$gestion.'<br>
                            <b>FORMULARIO :</b> REGISTRO DE EJECUCION<br>
                        </td>
                    </tr>
                </table>
                <hr>

             


                <table width="100%">
                    ' . $tabla . '
                </table>
                <div class="row">
                    <label><font size="2"><b>PROBLEMAS</b></font></label>
                    <textarea id="reg_problemas">
                    '. $data['dato_ejec_prog']->reg_problemas .'</textarea>
                    <label><font size="2"><b>CAUSAS</b></font></label>
                    <textarea id="reg_problemas">
                    '. $data['dato_ejec_prog']->reg_causas .'</textarea>
                    <label><font size="2"><b>SOLUCIONES</b></font></label>
                    <textarea id="reg_problemas">
                    '. $data['dato_ejec_prog']->reg_soluciones .'</textarea>
                </div>

                <footer>
                    <table class="table table-bordered">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata("sistema_pie").'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>
                <p>
                </p>
            </body>
        </html>';

        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'landscape');
        $dompdf->render();
        $dompdf->stream("objetivo_gestion.pdf", array("Attachment" => false));
    }
   
    public function acciones_red_objetivo($obje_id,$aper_id,$gestion,$n_obj,$poa_id,$caso)
    {
        
    }

}