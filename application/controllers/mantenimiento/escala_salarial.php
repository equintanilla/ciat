<?php

class escala_salarial extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Users_model','',true);
        $this->load->model('menu_modelo');
        $this->load->model('mantenimiento/model_escala_salarial');
        $this->load->model('mantenimiento/model_funcionario');
         //llamar a mi menu
        $this->load->library('menu');
        $this->menu->const_menu(9);
   	 }
   
    public function escala_s()
    {
        $data['cargos'] = $this->model_escala_salarial->lista_escala_salarial();
        $data['list_car_padre'] = $this->model_escala_salarial->list_car_padre(); ///// la consulta
        $ruta = 'mantenimiento/vlista_escala_salarial';
        $this->construir_vista($ruta,$data);
    }

   function construir_vista($ruta,$data){

        //----------------------------------- MENU-------------------------------
        $menu['enlaces'] = $this->menu->get_enlaces();
        $menu['subenlaces'] = $this->menu->get_sub_enlaces();
        $menu['titulo'] = 'MANTENIMIENTO';
        //-----------------------------------------------------------------------
        //armar vista
        $this->load->view('includes/header');
        $this->load->view('includes/menu_lateral',$menu);
        $this->load->view($ruta,$data);//contenido
        //$this->load->view('admin/mantenimiento/vprueba');//contenido
        $this->load->view('includes/footer');

    }
    function verificar_cod_car(){
            if($this->input->is_ajax_request() && $this->input->post('car_codigo'))
            {
                $post = $this->input->post();
                $cod = $post['car_codigo'];
                $cod = $this->security->xss_clean($cod);
                $data = $this->model_escala_salarial->verificar_carcod($cod);
                if(count($data)== 0){
                    echo '1';
                }else{
                    echo '0';
                }
            }else{
                show_404();
            }
    }
    function add_cargo(){
        if($this->input->is_ajax_request() && $this->input->post()){
            $this->form_validation->set_rules('car_nombre', 'Nombre', 'required|trim');
            $this->form_validation->set_rules('car_sueldo', 'sueldo', 'required|trim');
            $this->form_validation->set_rules('car_codigo', 'codigo', 'required|trim|integer');
           // $this->form_validation->set_rules('padre', 'Seleccione una opcion', 'required|trim');
            //=========================== mensajes =========================================
            $this->form_validation->set_message('required', 'El campo es es obligatorio');
            if ($this->form_validation->run() ) {
                $car_tp = $this->input->post('car_tp');
                $car_nombre = $this->input->post('car_nombre');
                $car_sueldo=  $this->input->post('car_sueldo');
                $car_codigo=  $this->input->post('car_codigo');
                $padre =  $this->input->post('padre');
                //=================enviar  evitar codigo malicioso ==========
                $car_nombre= $this->security->xss_clean(trim($car_nombre));
                $car_sueldo = $this->security->xss_clean($car_sueldo);
       
                if($car_tp==1){

                    

                    /*if($padre == ''){
                        $this->model_escala_salarial->add_car_independiente($car_codigo,$car_nombre,$car_sueldo);
                    }else{
                        $this->model_escala_salarial->add_car_dependiente($car_codigo,$car_nombre,$padre,$car_sueldo);
                    }*/
                    echo "true";
                }
                elseif ($car_tp==2) {
                    /*$car_id = $this->input->post('modificar');
                    if($padre == ''){
                        $this->model_escala_salarial->mod_car_independiente($car_id,$car_nombre,$car_sueldo);
                    }else{
                        $this->model_escala_salarial->mod_car_dependiente($car_id,$car_nombre,$padre,$car_sueldo);
                    }*/
                    echo "true";
                }
                else{
                    echo "false";
                }
                
            } else {
                echo'DATOS ERRONEOS';
            }
        }else{
            show_404();
        }
    }
    function get_car(){
        if($this->input->is_ajax_request() && $this->input->post())
        {
            $post = $this->input->post();
            $cod = $post['id_car'];
            $id = $this->security->xss_clean($cod);
            $dato_car = $this->model_escala_salarial->get_car($id);
            $padre='NINGUNO';
            if($dato_car[0]['car_depende']!=0){
                $car_padre = $this->model_escala_salarial->get_car($dato_car[0]['car_depende']);
                $padre =$car_padre[0]['car_cargo'];
            }

                $result = array(
                    'car_id' => $dato_car[0]['car_id'],
                    "car_cargo" =>$dato_car[0]['car_cargo'],
                    "car_sueldo" =>$dato_car[0]['car_sueldo'],
                    "padre" => $padre,
                    "car_depende" => $dato_car[0]['car_depende'],
                    "car_codigo" => $dato_car[0]['car_id'],
                );
            echo json_encode($result);
        }else{
            show_404();
        }
    }
function del_car(){
        if($this->input->is_ajax_request() && $this->input->post()){
            $post = $this->input->post();
            $postid = $post['postid'];
            $sql = 'UPDATE cargo SET car_estado = 0 WHERE car_id = '.$postid;
            if($this->db->query($sql)){
                echo $postid;
            }else{
                echo false;
            }
        }else{
            show_404();
        }
    }
    

   
    

	}