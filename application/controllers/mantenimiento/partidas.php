<?php

class partidas extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Users_model','',true);
        $this->load->model('menu_modelo');
        $this->load->model('mantenimiento/model_escala_salarial');
        $this->load->model('mantenimiento/model_partidas');
         //llamar a mi menu
        $this->load->library('menu');
        $this->menu->const_menu(9);
   	 }
   
    public function lista_partidas()
    {
        $data['list_par_padres'] = $this->model_partidas->lista_padres();
        $data['lista_p'] = $this->model_partidas->lista_partidas();
        $ruta = 'mantenimiento/vlista_partidas';
        $this->construir_vista($ruta,$data);
    }
    
  
   function construir_vista($ruta,$data){

        //----------------------------------- MENU-------------------------------
        $menu['enlaces'] = $this->menu->get_enlaces();
        $menu['subenlaces'] = $this->menu->get_sub_enlaces();
        $menu['titulo'] = 'MANTENIMIENTO';
        //-----------------------------------------------------------------------
        //armar vista
        $this->load->view('includes/header');
        $this->load->view('includes/menu_lateral',$menu);
        $this->load->view($ruta,$data);//contenido
        //$this->load->view('admin/mantenimiento/vprueba');//contenido
        $this->load->view('includes/footer');

    }
    function verificar_cod_par(){
        if($this->input->is_ajax_request() && $this->input->post('par_codigo'))
        {
            $post = $this->input->post();
            $cod = $post['par_codigo'];
            $gestion = $post['par_gestion'];
            $cod = $this->security->xss_clean($cod);
            $gestion = $this->security->xss_clean($gestion);
            //$gestion = $post['aper_gestion'];
            $data = $this->model_partidas->verificar_parcod($cod,$gestion);
            if(count($data)== 0){
                echo '1';
            }else{
                echo '0';
            }
        }else{
            show_404();
        }
    }
     function add_par(){
        if($this->input->is_ajax_request() && $this->input->post()){
            $this->form_validation->set_rules('par_nombre', 'Nombre', 'required|trim');
            $this->form_validation->set_rules('par_codigo', 'codigo', 'required|trim|integer');
            $this->form_validation->set_rules('par_gestion', 'gestion', 'required|trim|integer');
            //=========================== mensajes =========================================
            $this->form_validation->set_message('required', 'El campo es es obligatorio');
            $this->form_validation->set_message('integer', 'El campo  debe poseer solo numeros enteros');
            if ($this->form_validation->run() ) {
                $par_nombre = $this->input->post('par_nombre');
                $par_gestion =  $this->input->post('par_gestion');
                $par_codigo =  $this->input->post('par_codigo');
                //=================enviar  evitar codigo malicioso ==========
                $par_nombre= $this->security->xss_clean(trim($par_nombre));
                $par_codigo = $this->security->xss_clean($par_codigo);
                $par_gestion = $this->security->xss_clean($par_gestion);
                //======================= MODIFICAR=
                if(isset($_REQUEST['modificar'])){
                    $par_id = $this->input->post('modificar');
                    $this->model_partidas->mod_par($par_id,$par_nombre,$par_gestion,$par_codigo);
                }else{
                    $this->form_validation->set_rules('dependiente', 'Seleccione una opcion', 'required|trim');
                    $this->form_validation->set_rules('padre', 'Seleccione una opcion', 'required|trim');
                    //=========================== mensajes =========================================
                    $this->form_validation->set_message('required', 'El campo es es obligatorio');
                    $padre =  $this->input->post('padre');
                    $codigo_padre =  $this->model_partidas->dato_par($padre);
                    if($padre ==2){
                        $this->model_partidas->add_par_independiente($par_nombre,$par_codigo,$par_gestion);
                    }else{
                        $this->model_partidas->add_par_dependiente($par_nombre,$codigo_padre[0]['par_codigo'],$par_codigo,$par_gestion);
                    }
                }
                echo 'true';
            } else {
                echo'DATOS ERRONEOS';
            }
        }else{
            show_404();
        }

    }
    function get_par(){
        if($this->input->is_ajax_request() && $this->input->post())
        {
            $post = $this->input->post();
            $cod = $post['id_par'];
            $id = $this->security->xss_clean($cod);
            $dato_par = $this->model_partidas->dato_par($id);
            foreach($dato_par as $row){
                $padre_par = $this->model_partidas->dato_par_codigo($row['par_depende']);
                foreach($padre_par as $fila){
                    $padre = $fila['par_nombre'];
                }
                $result = array(
                    'par_id' => $row['par_id'],
                    "par_nombre" =>$row['par_nombre'],
                    "par_codigo" =>$row['par_codigo'],
                    "par_gestion" =>$row['par_gestion'],
                    "padre" => $padre
                );
            }
            echo json_encode($result);
        }else{
            show_404();
        }
    }

function del_par(){
        if($this->input->is_ajax_request() && $this->input->post()){
            $post = $this->input->post();
            $postid = $post['postid'];
            $sql = 'DELETE FROM  partidas WHERE par_id ='.$postid.' AND par_depende != 0';
            if($this->db->query($sql)){
                echo $postid;
            }else{
                echo false;
            }
        }else{
            show_404();
        }

    }

   
    

	}