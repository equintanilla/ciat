<?php
define("DOMPDF_ENABLE_REMOTE", true);
define("DOMPDF_TEMP_DIR", "/tmp");
class Cfuncionario extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('fun_id')!=null){
            $this->load->library('encrypt');
            $this->load->model('Users_model','',true);
            $this->load->model('menu_modelo');
            $this->load->model('mantenimiento/model_funcionario');
            $this->load->model('programacion/model_proyecto');
            $this->gestion = $this->session->userData('gestion');

        }else{
            redirect('/','refresh');
        }
    }
    
    public function roles_html($id)
    {
        $html_roles_fun = '';
        $lista_rol = $this->model_funcionario->get_rol($id);
        foreach ($lista_rol as $fila) {
            $html_roles_fun .= '
                <li>'.$fila['r_nombre'].'</li>
            ';
        }
        return $html_roles_fun;
    }


    public function list_usuarios()
    {
        $data['menu']=$this->menu(9);
        $data['resp']=$this->session->userdata('funcionario');

        $data['usuarios']=$this->list_funcionarios();

        $this->load->view('admin/mantenimiento/vfuncionario/vlist_fun', $data);
    }

    public function new_funcionario()
    {
        $data['menu']=$this->menu(9);

        $data['uni_org']=$this->model_funcionario->get_uni_o(); /// Unidad Organizacional
        $data['list_escala'] = $this->model_funcionario->get_cargo(); //// Escala Salarial
        $data['listas_rol'] =$this->model_funcionario->get_add_rol();
        $this->load->view('admin/mantenimiento/vfuncionario/vnew_fun', $data);
    }

    public function update_funcionario($fun_id)
    {
        $data['menu']=$this->menu(9);
        $data['fun']=$this->model_funcionario->get_funcionario($fun_id); /// Get Usuario
        $data['uni_org']=$this->model_funcionario->get_uni_o();
        $data['listas_rol'] =$this->model_funcionario->get_add_rol();
        $data['list_escala'] = $this->model_funcionario->get_cargo(); //// Escala Salarial
        $data['edit_pass'] = $this->encrypt->decode($data['fun'][0]['fun_password']);
        $this->load->view('admin/mantenimiento/vfuncionario/update_fun', $data);
    }

    public function list_funcionarios()
    {
        $funcionarios=$this->model_funcionario->get_funcionarios();
        $tabla ='';
        $nro=0;
        foreach($funcionarios  as $row)
        {
            $nro++;
            $dato_activo = ''; $color='';
            if ($row['fun_estado'] == 1 or $row['fun_estado'] == 2) {
                $color="#cfefcf";
                $dato_activo = '
                    <a href="#" data-toggle="modal" data-target="#modal_act_ff" class="btn btn-success act_ff" title="RESPONSABLE ACTIVO"  name="'.$row['fun_id'].'" id="0">
                        <i class="fa fa-check"></i>ACTIVO
                    </a>'
                ;
            } else {
                $color="#f5aabb";
                $dato_activo = '
                    <a href="#" data-toggle="modal" data-target="#modal_act_ff" class="btn btn-danger act_ff" title="RESPONSABLE INACTIVO"  name="'.$row['fun_id'].'" id="1">
                        <i class="fa fa-check"></i>INACTIVO
                    </a>'
                ;
            }

            $tabla .='<tr bgcolor='.$color.'>';
                $tabla .='<td align=center>'.$nro.'</td>';
                $tabla .='<td>'.$row['fun_nombre'].'</td>';
                $tabla .='<td>'.$row['fun_paterno'].'</td>';
                $tabla .='<td>'.$row['fun_materno'].'</td>';
                $tabla .='<td>'.$row['fun_ci'].'</td>';
                $tabla .='<td>'.$row['fun_domicilio'].'</td>';
                $tabla .='<td>'.$row['fun_telefono'].'</td>';
                $tabla .='<td>'.$row['uni_unidad'].'</td>';
                $tabla .='<td>'.$row['fun_usuario'].'</td>';
                $tabla .='<td>
                            <ul style="text-align:left; padding-left: 1;list-style-type:square; margin:2px;">
                                '.$this->roles_html($row['fun_id']).'
                            </ul>
                        </td>';
                if($row['fun_id']==0){
                    $tabla .='<td></td>';
                }
                else{
                   $tabla .='<td align=center>'.$dato_activo.'</td>'; 
                }
                
                $tabla .='<td>';
                $tabla .='<div class="btn-group">
                 <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-list"></i> ACCIONES <span class="caret"></span></button>
                 <ul class="dropdown-menu" role="menu">
                     <li><a href="'.site_url("admin").'/funcionario/update_fun/'.$row['fun_id'].'" class="mod_aper" title="MODIFICAR USUARIO"><i class="glyphicon glyphicon-pencil"></i> Modificar</a></li>
                     <li><a href="#" data-toggle="modal" data-target="#modal_act_ff" class="act_ff" title="ELIMINAR USUARIO" id="3" name="'.$row['fun_id'].'"><i class="glyphicon glyphicon-trash"></i> Eliminar</a></li>
                 </ul>
                 </div>';
                 $tabla .='</td>';
            $tabla .='</tr>';
        }

        return $tabla;
    }

    /*-------------------------- VALIDA USUARIO ---------------------*/
    public function add_funcionario()
    {
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {
            $this->form_validation->set_rules('nombre', 'Nombre', 'required|trim');
            $this->form_validation->set_rules('ap', 'Apellido Paterno', 'required|trim');
            $this->form_validation->set_rules('usuario', 'Usuario', 'required|trim');
            $this->form_validation->set_rules('password', 'Password', 'required|trim');

            if ($this->form_validation->run())
            {
                $data_to_store = array( 
                    'uni_id' => $this->input->post('uni_id'),
                    'car_id' => $this->input->post('esc_sal'),
                    'fun_nombre' => strtoupper($this->input->post('nombre')),
                    'fun_paterno' => strtoupper($this->input->post('ap')),
                    'fun_materno' => strtoupper($this->input->post('am')),
                    'fun_cargo' => strtoupper($this->input->post('crgo')),
                    'fun_ci' => $this->input->post('ci'),
                    'fun_domicilio' => strtoupper($this->input->post('domicilio')),
                    'fun_telefono' => $this->input->post('fono'),
                    'fun_usuario' => $this->input->post('usuario'),
                    'fun_password' => $this->encrypt->encode($this->input->post('password')),
                );
                $this->db->insert('funcionario', $this->security->xss_clean($data_to_store));
                $fun_id=$this->db->insert_id();

                for ($i=1; $i <=10 ; $i++) {
                   if($this->input->post('rol'.$i.'')!=''){
                    $data_to_store2 = array( 
                    'fun_id' => $fun_id,
                    'r_id' => $this->input->post('rol'.$i.''),
                    );
                    $this->db->insert('fun_rol', $this->security->xss_clean($data_to_store2));
                }
               }

                $this->session->set_flashdata('success','SE REGISTRO CORRECTAMENTE');
                redirect('admin/mnt/list_usu');
            }
            else{
            $this->session->set_flashdata('danger','ERROR AL REGISTRAR DATOS, VERIFIQUE INFORMACION');
            redirect('admin/vfuncionario/new_fun');
            }
        }
        else{
            $this->session->set_flashdata('danger','ERROR EN EL SERVIDOR, Contactese con el Administrador');
            redirect('admin/vfuncionario/new_fun');
        }
    }

    /*---------------------------------- VALIDA UPDATE USUARIO -------------------------------*/
    public function add_update_funcionario()
    {
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {
            $this->form_validation->set_rules('nombre', 'Nombre', 'required|trim');
            $this->form_validation->set_rules('ap', 'Apellido Paterno', 'required|trim');
            $this->form_validation->set_rules('usuario', 'Usuario', 'required|trim');
            $this->form_validation->set_rules('password', 'Password', 'required|trim');

            if ($this->form_validation->run())
            {
                $update_fun = array(
                    'uni_id' => $this->input->post('uni_id'),
                    'car_id' => $this->input->post('esc_sal'),
                    'fun_nombre' => strtoupper($this->input->post('nombre')),
                    'fun_paterno' => strtoupper($this->input->post('ap')),
                    'fun_materno' => strtoupper($this->input->post('am')),
                    'fun_cargo' => strtoupper($this->input->post('crgo')),
                    'fun_ci' => $this->input->post('ci'),
                    'fun_domicilio' => strtoupper($this->input->post('domicilio')),
                    'fun_telefono' => $this->input->post('fono'),
                    'fun_usuario' => $this->input->post('usuario'),
                    'fun_password' => $this->encrypt->encode($this->input->post('password')),
                    );
                    $this->db->where('fun_id', $this->input->post('fun_id'));
                    $this->db->update('funcionario', $this->security->xss_clean($update_fun));

                
                
                if($this->input->post('fun_id')!=0){

                    $this->model_funcionario->elimina_roles($this->input->post('fun_id'));
                    for ($i=1; $i <=10 ; $i++) {
                       if($this->input->post('rol'.$i.'')!=''){
                        $data_to_store2 = array( 
                        'fun_id' => $this->input->post('fun_id'),
                        'r_id' => $this->input->post('rol'.$i.''),
                        );
                        $this->db->insert('fun_rol', $this->security->xss_clean($data_to_store2));
                    }
                   } 
                }   
                
                $this->session->set_flashdata('success','EL RESPONSABLE SE MODIFICO CORRECTAMENTE');
                redirect('admin/mnt/list_usu');
            }
            else{
            $this->session->set_flashdata('danger','ERROR AL REGISTRAR DATOS, VERIFIQUE INFORMACION');
            redirect('admin/funcionario/new_fun');
            }
        }
        else{
            $this->session->set_flashdata('danger','ERROR EN EL SERVIDOR, Contactese con el Administrador');
            redirect('admin/funcionario/new_fun');
        }
    }
   
    public function verif_usuario()
    {
        if($this->input->is_ajax_request()) 
        {
            $post = $this->input->post();
            $user = $post['user'];
            $usuario=$this->model_funcionario->verificar_fun($user);
             if($usuario == 0){
             echo "true"; ///// no existe un CI registrado
             }
             else{
              echo "false"; //// existe el CI ya registrado
             } 
        }else{
            show_404();
        }
    }

    function verif_ci(){
        if($this->input->is_ajax_request()) 
        {
            $post = $this->input->post();
            $ci = $post['ci'];

            $variable= $this->model_funcionario->fun_ci($ci);
             if(count($variable)!=0){
             echo "false"; ///// Ya existe CI
             }
             else{
              echo "true"; //// No existe CI
             } 
        }else{
          show_404();
      }
    }

    /*------------------------- Activar - desactivar Responsables ----------------------*/
    function activar()
    {
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $post = $this->input->post();
            $fun_id = $post['fun_id'];
            $tipo = $post['tp'];

            $peticion=$this->update_estado($fun_id,$tipo);
            if($peticion){
                $result = array(
                    'respuesta' => 'correcto'
                );
            }
            else{
                $result = array(
                    'respuesta' => 'error'
                );
            }

            echo json_encode($result);

        } else {
            echo 'DATOS ERRONEOS';
        }
    }

    function update_estado($fun_id,$tp)
    {
        $update_proy = array
                    ('fun_estado' => $tp);

                    $this->db->where('fun_id', $fun_id);
                    $this->db->update('funcionario', $update_proy);


        if(count($this->model_funcionario->verif_activo($fun_id,$tp))==1){
            return true;
        }
        else{
            return false;
        }
    }

    function verif_user(){
        if($this->input->is_ajax_request()) 
        {
            $post = $this->input->post();
            $user = $post['user'];

            $variable= $this->model_funcionario->fun_usuario($user);
             if(count($variable)!=0){
             echo "false"; ///// Ya existe CI
             }
             else{
              echo "true"; //// No existe CI
             } 
        }else{
          show_404();
      }
    }


    public function del_fun($fun_id)
    {
        $fun=$this->model_funcionario->del_fun($fun_id);
        $this->session->set_flashdata('success','EL REGISTRO SE ELIMINO CORRECTAMENTE');
        redirect('admin/mnt/list_usu');
    }
    
    function nueva_contra()
    {
            $this->load->view('admin/mod_contrase');
    }

    function mod_cont()
    {
        $fun_id = $this->input->post('fun_id');
        $apassword = $this->input->post('apassword');
        $password = $this->input->post('password');
        $password = $this->encrypt->encode($password);
        $verifica = (($this->encrypt->decode($this->model_funcionario->verificar_password($fun_id))) == $apassword) ? true : false ;
        if($verifica){
            $this->model_funcionario->mod_password($fun_id,$password);
            echo "
                <script>
                    alert('Se Cambio la Contraseña Correctamente');
                </script>
            ";
            $this->vista();
        }
        else{
            echo "
                <script>
                    alert('La Contraseña Anterior No Coincide');
                </script>
            ";
            $this->nueva_contra();
        }
    }

        /*------------------------------------- MENU -----------------------------------*/
        function menu($mod){
            $enlaces=$this->menu_modelo->get_Modulos($mod);
            for($i=0;$i<count($enlaces);$i++)
            {
              $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
            }

            $tabla ='';
            for($i=0;$i<count($enlaces);$i++)
            {
                if(count($subenlaces[$enlaces[$i]['o_child']])>0)
                {
                    $tabla .='<li>';
                        $tabla .='<a href="#">';
                            $tabla .='<i class="'.$enlaces[$i]['o_image'].'"></i> <span class="menu-item-parent">'.$enlaces[$i]['o_titulo'].'</span></a>';    
                            $tabla .='<ul>';    
                                foreach ($subenlaces[$enlaces[$i]['o_child']] as $item) {
                                $tabla .='<li><a href="'.base_url($item['o_url']).'">'.$item['o_titulo'].'</a></li>';
                            }
                            $tabla .='</ul>';
                    $tabla .='</li>';
                }
            }

            return $tabla;
        }

        public function get_mes($mes_id)
        {
          $mes[1]='ENERO';
          $mes[2]='FEBRERO';
          $mes[3]='MARZO';
          $mes[4]='ABRIL';
          $mes[5]='MAYO';
          $mes[6]='JUNIO';
          $mes[7]='JULIO';
          $mes[8]='AGOSTO';
          $mes[9]='SEPTIEMBRE';
          $mes[10]='OCTUBRE';
          $mes[11]='NOVIEMBRE';
          $mes[12]='DICIEMBRE';

          $dias[1]='31';
          $dias[2]='28';
          $dias[3]='31';
          $dias[4]='30';
          $dias[5]='31';
          $dias[6]='30';
          $dias[7]='31';
          $dias[8]='31';
          $dias[9]='30';
          $dias[10]='31';
          $dias[11]='30';
          $dias[12]='31';

          $valor[1]=$mes[$mes_id];
          $valor[2]=$dias[$mes_id];

          return $valor;
        }
	}