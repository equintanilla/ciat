<?php

class pdes_meta extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Users_model','',true);
        $this->load->model('menu_modelo');
        $this->load->model('mantenimiento/mpdes');   
         //llamar a mi menu
        $this->load->library('menu');
        $this->menu->const_menu(9);
   	 }
   
    public function lista_pdes_meta($pdes_codigo)
    {
        //$data['list_pdes_pilar'] =$this->mpdes->listar_pedes_pilar();
        $data['list_pdes_meta'] =$this->mpdes->listar_pedes_meta($pdes_codigo);
        //$data['list_pdes_resultado'] =$this->mpdes->listar_pedes_resultado();
        //$data['list_pdes_accion'] =$this->mpdes->listar_pedes_accion();
        $data['datos']= $pdes_codigo;
        $ruta = 'mantenimiento/vlista_pdes_meta';
        $this->construir_vista($ruta,$data);
    }

   function construir_vista($ruta,$data){

        //----------------------------------- MENU-------------------------------
        $menu['enlaces'] = $this->menu->get_enlaces();
        $menu['subenlaces'] = $this->menu->get_sub_enlaces();
        $menu['titulo'] = 'MANTENIMIENTO';
        //-----------------------------------------------------------------------
        //armar vista
        $this->load->view('includes/header');
        $this->load->view('includes/menu_lateral',$menu);
        $this->load->view($ruta,$data);//contenido
        //$this->load->view('admin/mantenimiento/vprueba');//contenido
        $this->load->view('includes/footer');

    } 

	}