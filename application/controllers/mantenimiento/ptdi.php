<?php

class ptdi extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Users_model','',true);
        $this->load->model('menu_modelo');
        $this->load->model('mantenimiento/mptdi');
       
         //llamar a mi menu
        $this->load->library('menu');
        $this->menu->const_menu(9);
   	 }
   
    public function lista_ptdi()
    {
        $data['list_ptdi_pilar'] =$this->mptdi->listar_ptdi_pilar();
        //$data['list_ptdi_metas'] =$this->mptdi->listar_ptdi_metas();
        //$data['list_ptdi_resultado']=$this->mptdi->listar_ptdi_resultado();
        //$data['list_ptdi_accion']=$this->mptdi->listar_ptdi_accion();
        $ruta = 'mantenimiento/vlista_ptdi';
        $this->construir_vista($ruta,$data);
    }

   function construir_vista($ruta,$data){

        //----------------------------------- MENU-------------------------------
        $menu['enlaces'] = $this->menu->get_enlaces();
        $menu['subenlaces'] = $this->menu->get_sub_enlaces();
        $menu['titulo'] = 'MANTENIMIENTO';
        //-----------------------------------------------------------------------
        //armar vista
        $this->load->view('includes/header');
        $this->load->view('includes/menu_lateral',$menu);
        $this->load->view($ruta,$data);//contenido
        //$this->load->view('admin/mantenimiento/vprueba');//contenido
        $this->load->view('includes/footer');

    }

   
    

	}