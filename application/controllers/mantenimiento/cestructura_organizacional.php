<?php
define("DOMPDF_ENABLE_REMOTE", true);
define("DOMPDF_TEMP_DIR", "/tmp");
class cestructura_organizacional extends CI_Controller {
    public $rol = array('1' => '1');
    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('fun_id')!=null){
            $this->load->model('Users_model','',true);
            if($this->rolfun($this->rol)){
                $this->load->library('pdf');
                $this->load->library('pdf2');
                $this->load->model('Users_model','',true);
                $this->load->model('menu_modelo');
                $this->load->model('mantenimiento/model_configuracion');
                $this->load->model('mantenimiento/model_estructura_org');
                $this->load->library("security");
            }
            else{
                redirect('admin/dashboard');
            }
        }else{
            redirect('/','refresh');
        }
    }

    /*-------------------- LISTA DE UNIDADES ORGANIZACIONALES ------------------*/
    public function list_unidad_organizacional()
    {
        $enlaces=$this->menu_modelo->get_Modulos(9);
        $data['enlaces'] = $enlaces;
        for($i=0;$i<count($enlaces);$i++)
        {
            $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        $list_unidad = $this->model_estructura_org->list_unidad_organizacional();

        $mod = site_url("admin") . '/mod_unidad';
        $del = site_url("admin") . '/del_unidad';

        $tabla = '';
        $nro=0;
        $color='';
        foreach($list_unidad as $row)
        {
            if($row['uni_resp']==1){$color=''; $tipo1='<center><img src="' . base_url() . 'assets/ifinal/ok.png" WIDTH="25" HEIGHT="25"/></center>';}else{$tipo1='';}
            if($row['uni_ejecutora']==1){$color=''; $tipo2='<center><img src="' . base_url() . 'assets/ifinal/ok.png" WIDTH="25" HEIGHT="25"/></center>';}else{$tipo2='';}
            if($row['uni_adm']==1){$color=''; $tipo3='<center><img src="' . base_url() . 'assets/ifinal/ok.png" WIDTH="25" HEIGHT="25"/></center>';}else{$tipo3='';}

            $nro++;
            $tabla .= '<tr bgcolor="'.$color.'">';
            $tabla .= '<td>'.$row['uni_id'].'</td>';
            $tabla .= '<td>'.$row['unidad_dependiente'].'</td>';
            $tabla .= '<td>'.$row['unidad_organizacional'].'</td>';
            $tabla .= '<td>'.$tipo1.'</td>';
            $tabla .= '<td>'.$tipo2.'</td>';
            $tabla .= '<td>'.$tipo3.'</td>';
            $tabla .= '<td>
<div class="btn-group">
<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-list"></i> ACCIONES <span class="caret"></span></button>
<ul class="dropdown-menu" role="menu">
<li><a href="' . $mod . '/'.$row['uni_id'].'" title="MODIFICAR ESTRUCTURA" " title="Modificar Estructura"><i class="glyphicon glyphicon-pencil"></i> Modificar</a></li>';
            if($row['uni_depende']!=0){
                $tabla .= '<li>
                                <a href="#" data-toggle="modal" data-target="#modal_act_ff" class="del_ff" title="ELIMINAR RESPONSABLE"  name="'.$row['uni_id'].'">
                                    <i class="glyphicon glyphicon-trash"></i> Eliminar
                                </a></li>';
            }
            $tabla .='
</ul>
</div>
                           </td>';
            $tabla .= '</tr>';
        }

        $data['list_unidades']=$tabla;
        $this->load->view('admin/mantenimiento/unidad_organizacional/list_unidad', $data);
    }

    /*-------------------- NUEVA UNIDAD ORGANIZACIONAL -----------------------------*/
    public function form_unidad_organizacional()
    {
        $enlaces=$this->menu_modelo->get_Modulos(9);
        $data['enlaces'] = $enlaces;
        for($i=0;$i<count($enlaces);$i++)
        {
            $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        $data['estructura_organizacional'] = $this->model_estructura_org->estructura_org();
        $data['list_tipo_ue'] = $this->model_estructura_org->list_tipo_ue();
        $data['cod']=$this->model_estructura_org->cod_ultimo();

        $this->load->view('admin/mantenimiento/unidad_organizacional/form_unidad', $data);
    }

    /*-------------------- VALIDA UNIDAD ORGANIZACIONAL -----------------------------*/
    function valida_unidad_organizacional()
    {
        if($this->input->server('REQUEST_METHOD') === 'POST')
        {
            $this->form_validation->set_rules('cod_uni', 'Codigo', 'required|trim');
            $this->form_validation->set_rules('unidad', 'Unidad Organizacional', 'required|trim');

            if ($this->form_validation->run())
            {
                if($this->input->post('tunidad1')=='on'){$p1=1;}else{$p1=0;}
                if($this->input->post('tunidad2')=='on'){$p2=1;}else{$p2=0;}
                if($this->input->post('tunidad3')=='on'){$p3=1;}else{$p3=0;}

                if($this->input->post('dep')==0){ $uni_dep=0;}else{$uni_dep=$this->input->post('unidad_dep');}

                /*-------------- Registrando a la tabla unidadorganizacional ------------*/
                $query=$this->db->query('set datestyle to DMY');
                $data_to_store = array(
                    'uni_id' => $this->input->post('cod_uni'), /// Codigo
                    'uni_depende' => $uni_dep, /// unidad depende
                    'uni_unidad' => strtoupper($this->input->post('unidad')), ///  Unidad
                    'fun_id' => $this->session->userdata("fun_id"), /// Fun id
                    'uni_resp' => $p1, /// Unidad Responsable
                    'uni_ejecutora' => $p2, /// Unidad Ejecutora
                    'uni_adm' => $p3, /// Direccion Administrativa
                    'tp_ue' => $this->input->post('tp_ue'), /// Tipo de Unidad Ejecutora
                    'uni_cod_da'=> $this->input->post('cod_da'),
                    'uni_cod_ue'=> $this->input->post('cod_ue'),
                );
                $this->db->insert('unidadorganizacional', $this->security->xss_clean($data_to_store)); ///// Guardar en Tabla Insumos

                echo '<script> alert("LA UNIDAD SE REGISTRO CORRECTAMENTE")</script>';
                redirect(site_url("") . '/estructura_org');

            }
            else{
                redirect('admin/add_unidad');
                show_404();
            }

        }
        else{
            show_404();
        }
    }

    /*-------------------- MODIFICA UNIDAD ORGANIZACIONAL -----------------------------*/
    public function update_unidad_organizacional($uni_id)
    {
        $enlaces=$this->menu_modelo->get_Modulos(9);
        $data['enlaces'] = $enlaces;
        for($i=0;$i<count($enlaces);$i++)
        {
            $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        $data['estructura_organizacional'] = $this->model_estructura_org->estructura_org();
        $data['unidad']=$this->model_estructura_org->get_uni($uni_id);

        if($data['unidad'][0]['uni_resp']==1){$cheked1="checked";}else{$cheked1="";}
        if($data['unidad'][0]['uni_ejecutora']==1){$cheked2="checked";}else{$cheked2="";}
        if($data['unidad'][0]['uni_adm']==1){$cheked3="checked";}else{$cheked3="";}

        $data['cheked1'] = $cheked1;
        $data['cheked2'] = $cheked2;
        $data['cheked3'] = $cheked3;
        $data['list_tipo_ue'] = $this->model_estructura_org->list_tipo_ue();

        $this->load->view('admin/mantenimiento/unidad_organizacional/update_unidad', $data);
    }

    /*-------------------- VALIDA UPDATE UNIDAD ORGANIZACIONAL -----------------------------*/
    function valida_update_unidad_organizacional()
    {
        if($this->input->server('REQUEST_METHOD') === 'POST')
        {
            $this->form_validation->set_rules('uni_id', 'Unidad Organizacional Id', 'required|trim');
            $this->form_validation->set_rules('unidad', 'Unidad Organizacional', 'required|trim');

            if ($this->form_validation->run())
            {
                if($this->input->post('tunidad1')=='on'){$p1=1;}else{$p1=0;}
                if($this->input->post('tunidad2')=='on'){$p2=1;}else{$p2=0;}
                if($this->input->post('tunidad3')=='on'){$p3=1;}else{$p3=0;}

                if($this->input->post('dep')==0){ $uni_dep=0;}else{$uni_dep=$this->input->post('unidad_dep');}
                /*-------------- Registrando a la tabla unidadorganizacional ------------*/
                $query=$this->db->query('set datestyle to DMY');
                $update_uni = array(
                    'uni_depende' => $uni_dep, /// unidad depende
                    'uni_unidad' => strtoupper($this->input->post('unidad')), ///  Unidad
                    'fun_id' => $this->session->userdata("fun_id"), /// Fun id
                    'uni_estado' => 2, /// Estado
                    'uni_resp' => $p1, /// Unidad Responsable
                    'uni_ejecutora' => $p2, /// Unidad Ejecutora
                    'uni_adm' => $p3, /// Direccion Administrativa
                    'tp_ue' => $this->input->post('tp_ue'), /// Tipo de Unidad Ejecutora
                    'uni_cod_da' => $this->input->post('cod_da'), ///
                    'uni_cod_ue' => $this->input->post('cod_ue') ///
                );
                $this->db->where('uni_id', $this->input->post('uni_id'));
                $this->db->update('unidadorganizacional', $this->security->xss_clean($update_uni));

                echo '<script> alert("LA UNIDAD SE MODIFICO CORRECTAMENTE")</script>';
                redirect(site_url("") . '/estructura_org');

            }
            else{
                redirect('admin/mod_unidad/'.$this->input->post('uni_id').'/false');
                show_404();
            }

        }
        else{
            show_404();
        }
    }
    function valida_update_unidad_organizacional2()
    {
        if ($this->input->post()) {
            $post = $this->input->post();
            $uni_id = $post['uni_id'];  /// Uni id
            $unidad = $post['unidad'];  /// Unidad
            $dep = $post['dep'];//tipo de dependencia
            $tipo_u = $post['tipo_u'];//Tipo de Unidad
            $unidad_dep = $post['unidad_dep'];//id unidad dependiente

            $uni_id = $this->security->xss_clean($uni_id);
            $unidad= $this->security->xss_clean(trim($unidad));
            $dep = $this->security->xss_clean($dep);
            $tipo_u = $this->security->xss_clean($tipo_u);
            $unidad_dep = $this->security->xss_clean($unidad_dep);

            if($dep==0){$unidad_dep=0;}
            /*-------------- Registrando a la tabla Insumos ------------*/
            $query=$this->db->query('set datestyle to DMY');
            $update_uni = array(
                'uni_depende' => $unidad_dep, /// unidad depende
                'uni_unidad' => strtoupper($unidad), ///  Unidad
                'fun_id' => $this->session->userdata("fun_id"), /// Fun id
                'uni_estado' => 2, /// Estado
                'uni_ejecutora' => $tipo_u /// Tipo de Unidad
            );
            $this->db->where('uni_id', $uni_id);
            $this->db->update('unidadorganizacional', $this->security->xss_clean($update_uni));

            echo '<script> alert("EL REGISTRO SE MODIFICO CORRECTAMENTE")</script>';
            redirect(site_url("") . '/estructura_org');

        } else {
            show_404();
        }
    }

    function verificar_cod_uni(){
        if($this->input->is_ajax_request() && $this->input->post('uni_codigo'))
        {
            $post = $this->input->post();
            $cod = $post['uni_codigo'];
            $cod = $this->security->xss_clean($cod);
            $data = $this->model_estructura_org->verificar_unicod($cod);
            if(count($data)== 0){
                echo '1';
            }else{
                echo '0';
            }
        }else{
            show_404();
        }
    }

    /*------------------------- Eliminar Unidad Organizacional ----------------------*/
    function del_unidad()
    {
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $post = $this->input->post();
            $uni_id = $post['uni_id'];

            $update_uni = array(
                'uni_estado' => 3, /// Eliminado
                'fun_id' => $this->session->userdata("fun_id")
            );
            $this->db->where('uni_id', $uni_id);
            $this->db->update('unidadorganizacional', $update_uni);

            $result = array(
                'respuesta' => 'correcto'
            );

            echo json_encode($result);

        } else {
            echo 'DATOS ERRONEOS';
        }
    }

    //////leiminar///////
    function del_uni(){
        if($this->input->is_ajax_request() && $this->input->post()){
            $post = $this->input->post();
            $postid = $post['postid'];
            $sql = 'UPDATE unidadorganizacional SET uni_estado = 0 WHERE uni_id = '.$postid;
            if($this->db->query($sql)){
                echo $postid;
            }else{
                echo false;
            }
        }else{
            show_404();
        }
    }

    private $estilo_vertical = '<style>
        body{
            font-family: sans-serif;
            }
        @page {
        margin: 40px 40px;
        }
        header {
            position: fixed;
            left: 0px;
            top: -20px;
            right: 0px;
            height: 20px;
            background-color: #fff;
            }
        header h1{
            margin: 5px 0;
        }
        header h2{
            margin: 0 0 10px 0;
        }
        th{
                padding: 1px;
                text-align: center;
                font-size: 9px;
                color: #ffffff;
            }
        footer {
            position: fixed;
            left: 0px;
            bottom: -82px;
            right: 0px;
            height: 100px;
            border-bottom: 1px solid #ddd;
        }
        footer .page:after {
            content: counter(page) ;
        }
        footer table {
            width: 100%;
        }
        footer p {
            text-align: right;
        }
        footer .izq {
            text-align: left;
        }
        table{
            font-size: 7px;
            width: 100%;
            background-color:#fff;
        }
        th, td {
                padding: 1px;
                text-align: center;
                font-size: 7px;
            }
        .mv{font-size:10px;}
        .siipp{width:120px;}
        .header_table {
            color: #ffffff;
            text-align: center;
            align-items: center;
            align-self: center;
            font-weight: bold;
            background-color: #696969;
            align-content: center;
        }
        .header_subtable {
            color: #ffffff;
            text-align: center;
            align-items: center;
            align-self: center;
            font-weight: bold;
            background-color: #a59393;
            align-content: center;
        }
        .titulo_pdf {
            text-align: left;
            font-size: 10px;
        }
        .datos_principales {
            text-align: center;
            font-size: 9px;
        }
        .pdes_titulo{
            text-decoration: underline;
            font-weight: bold;
            text-transform: uppercase;
        }
        .td_pdes{
            text-align: left;  
        }

        .indi_desemp{
            font-size: 14px;
            border: 1px solid #CCC;
            background-color: #E0E0E0;
            padding: .5em;
        }
        table.fixed { table-layout:fixed; }
        table.fixed td { overflow: hidden; }
        .sub_table{
            border-bottom:1px solid black;
        }
        .contenedor_principal{
            border-style: solid;
            border-width:1px;
        }
        .titulo_dictamen{
            padding:4px;
            background-color: #454545;
            border-style: solid;margin: 2px;
            border-width: 1px;
            color: white;
            font-size: 9px;
            border-color: black;
        }
        .contenedor_datos{
            padding:0px;
            margin:1px;
        }
        .table_contenedor{
            border-style: solid;
            margin: 0px;
            padding: 1px;
            border-width: 1px;
            border-collapse: collapse;
        }
        .collapse_t td{
            border-style: solid;
            margin: 0px;
            padding: 3px;
            border-width: 1px;
            border-collapse: collapse;
        }
        .fila_unitaria{
            text-align:left;
        }
        .lista{
            text-align:left;
            padding-left: 8;
            list-style-type:square;
            margin:2px;
        }
    </style>';

    /*==================================== REPORTE - APERTURA PROGRAMATICA ===================================================*/
    public function rep_list_unidad_organizacional()
    {
        $gestion = $this->session->userdata('gestion');
        $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/logo.jpg" class="siipp"/>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata('entidad').'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$gestion.'<br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                            <b>REPORTE : </b> ESTRUCTURA ORGANIZACIONAL<br>
                        </td>
                        <td width=20%;>
                           <img src="'.base_url().'assets/img/escudo.jpg" alt="" width="80px">
                        </td>
                    </tr>
                </table>

                
                <footer>
                    <table class="table table-bordered" width="100%">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata('sistema_pie').'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>
                
                <br>
                <div class="contenedor_principal">
                    <table class="table-bordered" style="table-layout:fixed;" border="1">
                      <tr class="titulo_dictamen">
                        <th style="width:2%">C&Oacute;DIGO</th>
                        <th style="width:15%">UNIDAD DEPENDIENTE</th>
                        <th style="width:15%">UNIDAD ORGANIZACIONAL</th>
                        <th style="width:4%">UNIDAD RESPONSABLE</th>
                        <th style="width:4%">UNIDAD EJECUTORA</th>
                        <th style="width:4%">DIRECCI&Oacute;N ADMINISTRATIVA</th>
                      </tr>';
        $list_unidad = $this->model_estructura_org->list_unidad_organizacional();
        $cont = 1;
        foreach ($list_unidad as $row) {

            if($row['uni_resp']==1){$color=''; $tipo1='<center><img src="assets/ifinal/ok.png" WIDTH="20" HEIGHT="20"/></center>';}else{$tipo1='';}
            if($row['uni_ejecutora']==1){$color=''; $tipo2='<center><img src="assets/ifinal/ok.png" WIDTH="20" HEIGHT="20"/></center>';}else{$tipo2='';}
            if($row['uni_adm']==1){$color=''; $tipo3='<center><img src="assets/ifinal/ok.png" WIDTH="20" HEIGHT="20"/></center>';}else{$tipo3='';}

            $html .= '<tr >';
            $html .= '<td >'.$row['uni_id'].'</td>';
            $html .= '<td >'.$row['unidad_dependiente'].'</td>';
            $html .= '<td >'.$row['unidad_organizacional'].'</td>';
            $html .= '<td >'.$tipo1.'</td>';
            $html .= '<td >'.$tipo2.'</td>';
            $html .= '<td >'.$tipo3.'</td>';
            $html .= '</tr>';
        }
        $html .= '
                    </table>

                    
                </div>
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'portrait');
        $dompdf->render();
        $dompdf->stream("dictamen_proyecto.pdf", array("Attachment" => false));
    }

    function rolfun($rol)
    {
        $valor=false;
        for ($i=1; $i <=count($rol) ; $i++) {
            $data = $this->Users_model->get_datos_usuario_roles($this->session->userdata('fun_id'),$rol[$i]);
            if(count($data)!=0){
                $valor=true;
                break;
            }
        }
        return $valor;
    }

    function verifica_da_ue(){
        if($this->input->is_ajax_request())
        {
            $post = $this->input->post();
            $cod_da = $post['cod_da'];
            $cod_ue = $post['cod_ue'];
            $variable= $this->model_estructura_org->codigo_da_ue($cod_da,$cod_ue);
            if($variable == true)
            {
                echo 1; ///// existe
            }
            else
            {
                echo 0; //// no existe
            }
        }else{
            show_404();
        }
    }
}



