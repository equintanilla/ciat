<?php

class fuente_financiamiento extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Users_model','',true);
        $this->load->model('menu_modelo');
        $this->load->model('mantenimiento/model_escala_salarial');
        $this->load->model('mantenimiento/model_fuente_fin');
         //llamar a mi menu
        $this->load->library('menu');
        $this->menu->const_menu(9);
   	 }
   
    public function lista_fuente_fin()
    {
        $data['lista_ff'] = $this->model_fuente_fin->lista_fuente_fin();
        $ruta = 'mantenimiento/vlista_fuente_fin';
        $this->construir_vista($ruta,$data);
    }
    function construir_vista($ruta,$data){

        //----------------------------------- MENU-------------------------------
        $menu['enlaces'] = $this->menu->get_enlaces();
        $menu['subenlaces'] = $this->menu->get_sub_enlaces();
        $menu['titulo'] = 'MANTENIMIENTO';
        //-----------------------------------------------------------------------
        //armar vista
        $this->load->view('includes/header');
        $this->load->view('includes/menu_lateral',$menu);
        $this->load->view($ruta,$data);//contenido
        //$this->load->view('admin/mantenimiento/vprueba');//contenido
        $this->load->view('includes/footer');

    }
    function verificar_cod_ff(){
        //si no es una peticiÃ³n ajax mostramos un error 404
        if($this->input->is_ajax_request() && $this->input->post('ff_codigo'))
        {
            //en otro caso procesamos la peticiÃ³n
            $post = $this->input->post();
            $cod = $post['ff_codigo'];
            $ffgestion = $post['ff_gestion'];
            $cod = $this->security->xss_clean($cod);
            $ffgestion = $this->security->xss_clean($ffgestion);
            //$gestion = $post['aper_gestion'];
            $data = $this->model_fuente_fin->verificar_ffcod($cod,$ffgestion);
            //=========== SI EL MODELO responde correctamente SEGUIMOS
            if(count($data)== 0){
                echo '1';
            }else{
                echo '0';
            }
        }else{
            show_404();
        }
    }
     function add_ff(){
        if($this->input->is_ajax_request() && $this->input->post()){
            $this->form_validation->set_rules('ffdescripcion', 'descripcion', 'required|trim');
            $this->form_validation->set_rules('ffsigla', 'sigla', 'required|trim');
            $this->form_validation->set_rules('ffcodigo', 'codigo', 'required|trim|integer');
            $this->form_validation->set_rules('ffgestion', 'sigla', 'required|trim|integer');
            //=========================== mensajes =========================================
            $this->form_validation->set_message('required', 'El campo es es obligatorio');
            $this->form_validation->set_message('integer', 'El campo  debe poseer solo numeros enteros');
            if ($this->form_validation->run() ) {
                $ffdescripcion=  $this->input->post('ffdescripcion');
                $ffsigla =  $this->input->post('ffsigla');
                $ffcodigo =  $this->input->post('ffcodigo');
                $ffgestion =  $this->input->post('ffgestion');
                //=================enviar  evitar codigo malicioso ==========
                $ffdescripcion = $this->security->xss_clean(trim($ffdescripcion));
                $ffsigla = $this->security->xss_clean(trim($ffsigla));
                $ffcodigo = $this->security->xss_clean($ffcodigo);
                $ffgestion = $this->security->xss_clean($ffgestion);
                //======================= MODIFICAR=
                if(isset($_REQUEST['modificar'])){
                    $ffid = $this->input->post('modificar');
                    $verificar = $this->model_fuente_fin->mod_ff($ffid,$ffdescripcion,$ffsigla,$ffgestion,$ffcodigo);
                }else{
                    $verificar =  $this->model_fuente_fin->add_ff($ffdescripcion,$ffsigla,$ffcodigo,$ffgestion);
                }
                echo 'true';
            } else {
                echo'DATOS ERRONEOS';
            }
        }else{
            show_404();
        }

    }
    function get_ff(){
        if($this->input->is_ajax_request() && $this->input->post())
        {
            $post = $this->input->post();
            $cod = $post['id_ff'];
            $id = $this->security->xss_clean($cod);
            $dato_ff = $this->model_fuente_fin->dato_ff($id);
            foreach($dato_ff as $row){
                $result = array(
                    'ff_id' => $row['ff_id'],
                    "ff_descripcion" =>$row['ff_descripcion'],
                    "ff_sigla" =>$row['ff_sigla'],
                    "ff_codigo" =>$row['ff_codigo'],
                    "ff_gestion" =>$row['ff_gestion']
                );
            }
            echo json_encode($result);

        }else{
            show_404();
        }
    }
    function del_ff(){
        if($this->input->is_ajax_request() && $this->input->post()){
            $post = $this->input->post();
            $postid = $post['postid'];
            $sql = 'UPDATE fuentefinanciamiento SET ff_estado = 0 WHERE ff_id = '.$postid;
            if($this->db->query($sql)){
                echo $postid;
            }else{
                echo false;
            }
        }else{
            show_404();
        }
    }


   
    

	}