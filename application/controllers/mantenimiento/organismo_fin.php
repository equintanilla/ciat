<?php

class organismo_fin extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Users_model','',true);
        $this->load->model('menu_modelo');
        $this->load->model('mantenimiento/model_escala_salarial');
        $this->load->model('mantenimiento/model_organismo_fin');
         //llamar a mi menu
        $this->load->library('menu');
        $this->menu->const_menu(9);
   	 }
   
    public function lista_organismo_fin()
    {
        $data['lista_of'] = $this->model_organismo_fin->lista_orga_fin();
        $ruta = 'mantenimiento/vlista_organismo_fin';
        $this->construir_vista($ruta,$data);
    }
   
  
   function construir_vista($ruta,$data){

        //----------------------------------- MENU-------------------------------
        $menu['enlaces'] = $this->menu->get_enlaces();
        $menu['subenlaces'] = $this->menu->get_sub_enlaces();
        $menu['titulo'] = 'MANTENIMIENTO';
        //-----------------------------------------------------------------------
        //armar vista
        $this->load->view('includes/header');
        $this->load->view('includes/menu_lateral',$menu);
        $this->load->view($ruta,$data);//contenido
        //$this->load->view('admin/mantenimiento/vprueba');//contenido
        $this->load->view('includes/footer');

    }
    function verificar_cod_of(){
        //si no es una peticiÃ³n ajax mostramos un error 404
        if($this->input->is_ajax_request() && $this->input->post('of_codigo'))
        {
            //en otro caso procesamos la peticiÃ³n
            $post = $this->input->post();
            $cod = $post['of_codigo'];
            $ofgestion = $post['of_gestion'];
            $cod = $this->security->xss_clean($cod);
            $ofgestion = $this->security->xss_clean($ofgestion);
            $data = $this->model_organismo_fin->verificar_ofcod($cod,$ofgestion);
            //=========== SI EL MODELO responde correctamente SEGUIMOS
            if(count($data)== 0){
                echo '1';
            }else{
                echo '0';
            }
        }else{
            show_404();
        }
    }
    function add_organismofinanciador(){
        if($this->input->is_ajax_request() && $this->input->post()){
            $this->form_validation->set_rules('ofdescripcion', 'descripcion', 'required|trim');
            $this->form_validation->set_rules('ofsigla', 'sigla', 'required|trim');
            $this->form_validation->set_rules('ofcodigo', 'codigo', 'required|trim|integer');
            $this->form_validation->set_rules('ofgestion', 'sigla', 'required|trim|integer');
            //=========================== mensajes =========================================
            $this->form_validation->set_message('required', 'El campo es es obligatorio');
            $this->form_validation->set_message('integer', 'El campo  debe poseer solo numeros enteros');
            if ($this->form_validation->run() ) {
                $ofdescripcion=  $this->input->post('ofdescripcion');
                $ofsigla =  $this->input->post('ofsigla');
                $ofcodigo =  $this->input->post('ofcodigo');
                $ofgestion =  $this->input->post('ofgestion');
                //=================enviar  evitar codigo malicioso ==========
                $ofdescripcion = $this->security->xss_clean(trim($ofdescripcion));
                $ofsigla = $this->security->xss_clean(trim($ofsigla));
                $ofcodigo = $this->security->xss_clean($ofcodigo);
                $ofgestion = $this->security->xss_clean($ofgestion);
                //======================= MODIFICAR=
                if(isset($_REQUEST['modificar'])){
                    $ofid = $this->input->post('modificar');
                    $verificar = $this->model_organismo_fin->mod_of($ofid,$ofdescripcion,$ofsigla,$ofgestion,$ofcodigo);
                }else{
                    $verificar =  $this->model_organismo_fin->add_of($ofdescripcion,$ofsigla,$ofcodigo,$ofgestion);
                }
                echo 'true';
            } else {
               echo'DATOS ERRONEOS';
            }
        }else{
            show_404();
        }

    }
     function get_of(){
        if($this->input->is_ajax_request() && $this->input->post())
        {
            $post = $this->input->post();
            $cod = $post['id_of'];
            $id = $this->security->xss_clean($cod);
            $dato_of = $this->model_organismo_fin->dato_of($id);
                foreach($dato_of as $row){
                    $result = array(
                        'of_id' => $row['of_id'],
                        "of_descripcion" =>$row['of_descripcion'],
                        "of_sigla" =>$row['of_sigla'],
                        "of_codigo" =>$row['of_codigo'],
                        "of_gestion" =>$row['of_gestion']
                        );
                }
                echo json_encode($result);

        }else{
            show_404();
        }
    }
    function del_organismofinanciador(){
        if($this->input->is_ajax_request() && $this->input->post()){
            $post = $this->input->post();
            $postid = $post['postid'];
            //echo $i;
            $sql = 'UPDATE organismofinanciador SET of_estado = 0 WHERE of_id = '.$postid;
            if($this->db->query($sql)){
                echo $postid;
            }else{
                echo false;
            }
        }else{
            show_404();
        }
    }


   
    

	}