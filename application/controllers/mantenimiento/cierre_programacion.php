<?php

class Cierre_programacion extends CI_Controller
{
    public $rol = array('1' => '1');
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Users_model','',true);
        if($this->rolfun($this->rol)){
        if($this->session->userdata('fun_id')!=null){
        $this->load->model('menu_modelo');
        $this->load->model('Users_model','',true);
        $this->load->model('programacion/model_proyecto');
        $this->load->model('programacion/model_faseetapa');
        $this->gestion = $this->session->userData('gestion');
        }else{
            redirect('/','refresh');
        }
      }
      else{
          redirect('admin/dashboard');
      }
    }


    /*--------------- Select Opciones -----------------*/
    public function cierre()
    {
        $data['menu']=$this->menu(9);
        $this->load->view('admin/mantenimiento/cierre/cierre_programacion', $data); 
    }

    /*--------------- Cierre Proyectos -----------------*/
    public function cierre_proyectos()
    {
        $data['menu']=$this->menu(9);
        $data['programas']=$this->proyectos_aprobados();
        $this->load->view('admin/mantenimiento/cierre/cierre_proyectos', $data); 
    }

    public function proyectos_aprobados()
    {
        $operaciones=$this->model_proyecto->list_operaciones_aprobadas();
        $tabla ='';
        $nro=0;
        foreach($operaciones  as $row)
        {
            if($row['proy_estado']==4){
                $color='#b8f3b2';$ti='PROYECTOS_APROBADOS'; $estado='OPERACI&Oacute;N APROBADO';
            }
            elseif ($row['proy_estado']==5) {
                $color='#ec7d7d';$ti='PROYECTOS_CERRADOS'; $estado='OPERACI&Oacute;N CERRADO';
            }
            $nro++;
            $tabla .='<tr bgcolor='.$color.' title='.$ti.'>';
                $tabla .='<td>'.$nro.'</td>';
                $tabla .='<td>'.$row['aper_programa'].' '.$row['aper_proyecto'].' '.$row['aper_actividad'].'</td>';
                $tabla .='<td>'.$row['proy_nombre'].'</td>';
                $tabla .='<td>'.$row['proy_sisin'].'</td>';
                $tabla .='<td>'.$row['tp_tipo'].'</td>';
                $tabla .='<td>'.$row['fun_nombre'].' '.$row['fun_paterno'].' '.$row['fun_materno'].'</td>';
                $tabla .='<td>'.$row['ue'].'</td>';
                $nc=$this->model_faseetapa->calcula_nc($row['pfec_fecha_inicio']); //// calcula nuevo/continuo
                $ap=$this->model_faseetapa->calcula_ap($row['pfec_fecha_inicio'],$row['pfec_fecha_fin']); //// calcula Anual/Plurianual
                $tabla .='<td>'.$nc.'</td>';
                $tabla .='<td>'.$ap.'</td>';
                $tabla .='<td>'.number_format($row['pfec_ptto_fase'], 2, ',', '.').'</td>';
                $tabla .='<td>'.number_format($row['pfecg_ppto_total'], 2, ',', '.').'</td>';
                $tabla .='<td>'.$estado.'</td>';
            $tabla .='</tr>';
        }

        return $tabla;
    }





    function validar_cierre_proyectos()
    {
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $post = $this->input->post();
            $tipo = $post['tp'];
            $operaciones=$this->model_proyecto->list_operaciones_aprobadas();
            foreach($operaciones  as $row)
            {
                $update_proy = array(
                    'proy_estado' => 5,
                    'fun_id' => $this->session->userdata("fun_id")
                );

                $this->db->where('proy_id', $row['proy_id']);
                $this->db->update('_proyectos', $update_proy);
            }
            $result = array(
                    'respuesta' => 'correcto'
                );
            echo json_encode($result);

        } else {
            echo 'DATOS ERRONEOS';
        }

    }





    function menu($mod){
    $enlaces=$this->menu_modelo->get_Modulos($mod);
    for($i=0;$i<count($enlaces);$i++)
    {
      $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
    }

    $tabla ='';
    for($i=0;$i<count($enlaces);$i++)
    {
        if(count($subenlaces[$enlaces[$i]['o_child']])>0)
        {
            $tabla .='<li>';
                $tabla .='<a href="#">';
                    $tabla .='<i class="'.$enlaces[$i]['o_image'].'"></i> <span class="menu-item-parent">'.$enlaces[$i]['o_titulo'].'</span></a>';    
                    $tabla .='<ul>';    
                        foreach ($subenlaces[$enlaces[$i]['o_child']] as $item) {
                        $tabla .='<li><a href="'.base_url($item['o_url']).'">'.$item['o_titulo'].'</a></li>';
                    }
                    $tabla .='</ul>';
            $tabla .='</li>';
        }
    }
    return $tabla;
    }

    function rolfun($rol)
    {
      $valor=false;
      for ($i=1; $i <=count($rol) ; $i++) { 
        $data = $this->Users_model->get_datos_usuario_roles($this->session->userdata('fun_id'),$rol[$i]);
        if(count($data)!=0){
          $valor=true;
          break;
        }
      }
      return $valor;
    }

    function rolfunn($tp_rol)
    {
      $valor=false;
      $data = $this->Users_model->get_datos_usuario_roles($this->session->userdata('fun_id'),$tp_rol);
      if(count($data)!=0){
        $valor=true;
      }
      return $valor;
    }

}