<?php

class Cconfiguracion extends CI_Controller {
    public $rol = array('1' => '1');
    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('fun_id')!=null){
            $this->load->model('Users_model','',true);
            if($this->rolfun($this->rol)){ 
                $this->load->library('pdf');
                $this->load->library('pdf2');
                $this->load->model('Users_model','',true);
                $this->load->model('menu_modelo');
                $this->load->model('mantenimiento/model_configuracion');
                $this->load->model('mantenimiento/model_estructura_org');
                $this->load->library("security");
            }
            else{
                redirect('admin/dashboard');
            }
        }
        else{
                redirect('/','refresh');
        }
    }


    public function main_configuracion()
    { 
      $data['menu']=$this->menu(9);
      $data['conf'] = $this->model_configuracion->get_configuracion();
      $data['mes'] = $this->model_configuracion->get_mes();
      $data['gestion'] = $this->model_configuracion->get_gestion();

      $this->load->view('admin/mantenimiento/configuracion/vmain_configuracion', $data);
    }

    public function update_conf()
    { 
        if ($this->input->server('REQUEST_METHOD') === 'POST') ///// UPDATE
        {
            $this->form_validation->set_rules('ide', 'id configuracion', 'required|trim');
            $this->form_validation->set_rules('tp', 'tipo', 'required|trim');
           
            if ($this->form_validation->run() )
            {
                if($this->input->post('tp')==2){
                    $update_conf = array
                            (   'conf_mes' => $this->input->post('mes_id'),
                                'fun_id' => $this->session->userdata("fun_id"));
                    $this->db->where('ide', $this->input->post('ide'));
                    $this->db->update('configuracion', $update_conf);

                    $this->session->set_flashdata('success','SE MODIFICO CORRECTAMENTE EL MES ACTIVO');
                    redirect('Configuracion');
                }
                elseif($this->input->post('tp')==1){
                    $update_conf = array
                            (   'conf_estado' => 0,
                                'fun_id' => $this->session->userdata("fun_id"));
                    $this->db->where('ide', $this->input->post('ide'));
                    $this->db->update('configuracion', $update_conf);


                    $update_conf = array
                            (   'conf_estado' => 1,
                                'fun_id' => $this->session->userdata("fun_id"));
                    $this->db->where('ide', $this->input->post('gest_id'));
                    $this->db->update('configuracion', $update_conf);

                    redirect('/','refresh');
                }
                elseif($this->input->post('tp')==3){
                    $update_conf = array
                            (   'conf_sigla_entidad' => $this->input->post('sigla'),
                                'fun_id' => $this->session->userdata("fun_id"));
                    $this->db->where('ide', $this->input->post('ide'));
                    $this->db->update('configuracion', $update_conf);

                    $this->session->set_flashdata('success','SE MODIFICO CORRECTAMENTE LA SIGLA INSTITUCIONAL');
                    redirect('Configuracion');
                }
                elseif($this->input->post('tp')==4){
                    $update_conf = array
                            (   'conf_nombre_entidad' => $this->input->post('entidad'),
                                'fun_id' => $this->session->userdata("fun_id"));
                    $this->db->where('ide', $this->input->post('ide'));
                    $this->db->update('configuracion', $update_conf);

                    $this->session->set_flashdata('success','SE MODIFICO CORRECTAMENTE EL NOMBRE DE LA ENTIDAD');
                    redirect('Configuracion');
                }
                elseif($this->input->post('tp')==5){
                    $update_conf = array
                            (   'conf_gestion_desde' => $this->input->post('gi'),
                                'conf_gestion_hasta' => $this->input->post('gf'),
                                'fun_id' => $this->session->userdata("fun_id"));
                    $this->db->where('ide', $this->input->post('ide'));
                    $this->db->update('configuracion', $update_conf);

                    $this->session->set_flashdata('success','SE MODIFICO CORRECTAMENTE LAS GESTIONES PARA LOS OBETIVOS ESTRATEGICOS');
                    redirect('Configuracion');
                }
            }
        }
        else{

        }
    }



    public function activar_gestion()
    {
        if($this->input->is_ajax_request()){
            $post = $this->input->post();
            $ide = $post['ide'];
            $bool = $this->model_configuracion->activa_funcionario($ide);
            // $bool = true;
            if($bool){
                $data = array(
                    'resultado' => 'Operación Correcta',
                    'ide' => $ide
                    );
                echo json_encode($data);
            }
            else{
                $data = array(
                    'resultado' => 'Operación Incorrecto'
                    );
                echo json_encode($data);
            }
        } else {
            show_404();
        }
    }
    public function desactivar_gestion()
    {
        if($this->input->is_ajax_request()){
            $post = $this->input->post();
            $ide = $post['ide'];
            $bool = $this->model_configuracion->desactiva_funcionario($ide);
            // $bool = true;
            if($bool){
                $data = array(
                    'resultado' => 'Operación Correcta',
                    'ide' => $ide
                    );
                echo json_encode($data);
            }
            else{
                $data = array(
                    'resultado' => 'Operación Incorrecto'
                    );
                echo json_encode($data);
            }
        } else {
            show_404();
        }
    }
   
    public function configuracion_gestion()
    {
        //////////////////////////////gestion y mes actual/////////
        $gestion= $this->model_configuracion->gestion_actual();
        // $data['ges'] = $gestion;
        $data['mes_db'] = $this->mes_texto($gestion[0]['conf_mes']);
        $data['gestion_db'] = $gestion[0]['ide'];
        $data['gestion_lista'] = $this->model_configuracion->get_gestion_todo();
        //////////////////////vista gestion/////////////////////////
        $listar_gestion = $this->model_configuracion->lista_gestion();
        $tabla = '';
        $tabla.='';
        $tabla.='<form   method="post" action="'.base_url().'index.php/Configuracion_mod">
                <select name="actu_gest" class="form-control" required>
                 <option value="'.$data['gestion_db'].'">Seleccionar gestion</option>'; 
        foreach ($listar_gestion as $row) {
        $tabla.='<option value="'.$row['ide'].'" >'.$row['ide'].'</option>';
              };
        $tabla.='  </select>
                    <br>
                    <BUTTON class="btn btn-xs btn-success">
                        <div class="btn-hover-postion1">
                           Modificar
                        </div>
                    </BUTTON>
            </form>';
        ////////////////////end vista gestion/////////////////////////
        ///////////////////////vista mes/////////////////////////
            $tablas='';
        $tablas.='';
        $tablas.='<form   method="post" action="'.base_url().'index.php/Configuracion_mod_mes">
                <select name="actu_gest_mes" class="form-control">
                 <option value="1">Seleccionar Mes</option>
                 <option value="1">Enero</option> 
                 <option value="2">Febrero</option> 
                 <option value="3">Marzo</option> 
                 <option value="4">Abril</option> 
                 <option value="5">Mayo</option> 
                 <option value="6">Junio</option> 
                 <option value="7">Julio</option> 
                 <option value="8">Agosto</option> 
                 <option value="9">Septiembre</option> 
                 <option value="10">Octubre</option> 
                 <option value="11">Noviembre</option> 
                 <option value="12">Diciembre</option>  
                </select>
                <br>
                    <BUTTON class="btn btn-xs btn-success">
                        <div class="btn-hover-postion1">
                           Modificar
                        </div>
                    </BUTTON>
            </form>';
        ///////////////////////end mes///////////////////////////
        $data['mes']=$tablas;
        $data['gestion']=$tabla;
        $ruta = 'mantenimiento/vlista_configuracion';
        $this->construir_vista($ruta,$data);
    }
    function mod_conf()
    {
        $ide=$this->input->post('actu_gest');
        $this->model_configuracion->act_ges($ide); 
    }
    function mod_conf_mes()
    {
        echo $conf_mes=$this->input->post('actu_gest_mes');
        $this->model_configuracion->act_ges_mes($conf_mes); 
    }

   
   function construir_vista($ruta,$data)
   {
       //----------------------------------- MENU-------------------------------
        $menu['enlaces'] = $this->menu->get_enlaces();
        $menu['subenlaces'] = $this->menu->get_sub_enlaces();
        $menu['titulo'] = 'MANTENIMIENTO';
        //-----------------------------------------------------------------------
        //armar vista
        $this->load->view('includes/header');
        $this->load->view('includes/menu_lateral',$menu);
        $this->load->view($ruta,$data);//contenido
        //$this->load->view('admin/mantenimiento/vprueba');//contenido
        $this->load->view('includes/footer');

    }
    public function mes_texto($mes)
    {
        switch ($mes) {
            case '1':
                $texto = 'Enero';
                break;
            case '2':
                $texto = 'Febrero';
                break;
            case '3':
                $texto = 'Marzo';
                break;
            case '4':
                $texto = 'Abril';
                break;
            case '5':
                $texto = 'Mayo';
                break;
            case '6':
                $texto = 'Junio';
                break;
            case '7':
                $texto = 'Julio';
                break;
            case '8':
                $texto = 'Agosto';
                break;
            case '9':
                $texto = 'Septiembre';
                break;
            case '10':
                $texto = 'Octubre';
                break;
            case '11':
                $texto = 'Noviembre';
                break;
            case '12':
                $texto = 'Diciembre';
                break;
            default:
                $texto = 'Sin Mes asignado';
                break;
        }
        return $texto;
    }

        function menu($mod){
            $enlaces=$this->menu_modelo->get_Modulos($mod);
            for($i=0;$i<count($enlaces);$i++)
            {
              $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
            }

            $tabla ='';
            for($i=0;$i<count($enlaces);$i++)
            {
                if(count($subenlaces[$enlaces[$i]['o_child']])>0)
                {
                    $tabla .='<li>';
                        $tabla .='<a href="#">';
                            $tabla .='<i class="'.$enlaces[$i]['o_image'].'"></i> <span class="menu-item-parent">'.$enlaces[$i]['o_titulo'].'</span></a>';    
                            $tabla .='<ul>';    
                                foreach ($subenlaces[$enlaces[$i]['o_child']] as $item) {
                                $tabla .='<li><a href="'.base_url($item['o_url']).'">'.$item['o_titulo'].'</a></li>';
                            }
                            $tabla .='</ul>';
                    $tabla .='</li>';
                }
            }

            return $tabla;
        }

    function rolfun($rol)
    {
      $valor=false;
      for ($i=1; $i <=count($rol) ; $i++) { 
        $data = $this->Users_model->get_datos_usuario_roles($this->session->userdata('fun_id'),$rol[$i]);
        if(count($data)!=0){
          $valor=true;
          break;
        }
      }
      return $valor;
    }
}