<?php

class configuracion extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Users_model','',true);
        $this->load->model('menu_modelo');
        $this->load->model('mantenimiento/model_configuracion');
        $this->load->model('mantenimiento/model_funcionario');
         //llamar a mi menu
        $this->load->library('menu');
        $this->menu->const_menu(9);
    }

    public function activar_gestion()
    {
        if($this->input->is_ajax_request()){
            $post = $this->input->post();
            $ide = $post['ide'];
            $bool = $this->model_configuracion->activa_funcionario($ide);
            // $bool = true;
            if($bool){
                $data = array(
                    'resultado' => 'Operación Correcta',
                    'ide' => $ide
                    );
                echo json_encode($data);
            }
            else{
                $data = array(
                    'resultado' => 'Operación Incorrecto'
                    );
                echo json_encode($data);
            }
        } else {
            show_404();
        }
    }
    public function desactivar_gestion()
    {
        if($this->input->is_ajax_request()){
            $post = $this->input->post();
            $ide = $post['ide'];
            $bool = $this->model_configuracion->desactiva_funcionario($ide);
            // $bool = true;
            if($bool){
                $data = array(
                    'resultado' => 'Operación Correcta',
                    'ide' => $ide
                    );
                echo json_encode($data);
            }
            else{
                $data = array(
                    'resultado' => 'Operación Incorrecto'
                    );
                echo json_encode($data);
            }
        } else {
            show_404();
        }
    }
   
    public function configuracion_gestion()
    {
        //////////////////////////////gestion y mes actual/////////
        $gestion= $this->model_configuracion->gestion_actual();
        // $data['ges'] = $gestion;
        $data['mes_db'] = $this->mes_texto($gestion[0]['conf_mes']);
        $data['gestion_db'] = $gestion[0]['ide'];
        $data['gestion_lista'] = $this->model_configuracion->get_gestion_todo();
        //////////////////////vista gestion/////////////////////////
        $listar_gestion = $this->model_configuracion->lista_gestion();
        $tabla = '';
        $tabla.='';
        $tabla.='<form   method="post" action="'.base_url().'index.php/Configuracion_mod">
                <select name="actu_gest" class="form-control" required>
                 <option value="'.$data['gestion_db'].'">Seleccionar gestion</option>'; 
        foreach ($listar_gestion as $row) {
        $tabla.='<option value="'.$row['ide'].'" >'.$row['ide'].'</option>';
              };
        $tabla.='  </select>
                    <br>
                    <BUTTON class="btn btn-xs btn-success">
                        <div class="btn-hover-postion1">
                           Modificar
                        </div>
                    </BUTTON>
            </form>';
        ////////////////////end vista gestion/////////////////////////
        ///////////////////////vista mes/////////////////////////
            $tablas='';
        $tablas.='';
        $tablas.='<form   method="post" action="'.base_url().'index.php/Configuracion_mod_mes">
                <select name="actu_gest_mes" class="form-control">
                 <option value="1">Seleccionar Mes</option>
                 <option value="1">Enero</option> 
                 <option value="2">Febrero</option> 
                 <option value="3">Marzo</option> 
                 <option value="4">Abril</option> 
                 <option value="5">Mayo</option> 
                 <option value="6">Junio</option> 
                 <option value="7">Julio</option> 
                 <option value="8">Agosto</option> 
                 <option value="9">Septiembre</option> 
                 <option value="10">Octubre</option> 
                 <option value="11">Noviembre</option> 
                 <option value="12">Diciembre</option>  
                </select>
                <br>
                    <BUTTON class="btn btn-xs btn-success">
                        <div class="btn-hover-postion1">
                           Modificar
                        </div>
                    </BUTTON>
            </form>';
        ///////////////////////end mes///////////////////////////
        $data['mes']=$tablas;
        $data['gestion']=$tabla;
        $ruta = 'mantenimiento/vlista_configuracion';
        $this->construir_vista($ruta,$data);
    }
    function mod_conf()
    {
        $ide=$this->input->post('actu_gest');
        $this->model_configuracion->act_ges($ide); 
    }
    function mod_conf_mes()
    {
        echo $conf_mes=$this->input->post('actu_gest_mes');
        $this->model_configuracion->act_ges_mes($conf_mes); 
    }

   
   function construir_vista($ruta,$data)
   {
       //----------------------------------- MENU-------------------------------
        $menu['enlaces'] = $this->menu->get_enlaces();
        $menu['subenlaces'] = $this->menu->get_sub_enlaces();
        $menu['titulo'] = 'MANTENIMIENTO';
        //-----------------------------------------------------------------------
        //armar vista
        $this->load->view('includes/header');
        $this->load->view('includes/menu_lateral',$menu);
        $this->load->view($ruta,$data);//contenido
        //$this->load->view('admin/mantenimiento/vprueba');//contenido
        $this->load->view('includes/footer');

    }
    public function mes_texto($mes)
    {
        switch ($mes) {
            case '1':
                $texto = 'Enero';
                break;
            case '2':
                $texto = 'Febrero';
                break;
            case '3':
                $texto = 'Marzo';
                break;
            case '4':
                $texto = 'Abril';
                break;
            case '5':
                $texto = 'Mayo';
                break;
            case '6':
                $texto = 'Junio';
                break;
            case '7':
                $texto = 'Julio';
                break;
            case '8':
                $texto = 'Agosto';
                break;
            case '9':
                $texto = 'Septiembre';
                break;
            case '10':
                $texto = 'Octubre';
                break;
            case '11':
                $texto = 'Noviembre';
                break;
            case '12':
                $texto = 'Diciembre';
                break;
            default:
                $texto = 'Sin Mes asignado';
                break;
        }
        return $texto;
    }
}