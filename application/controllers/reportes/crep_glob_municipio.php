<?php

//reporte global - nivel municipio
class Crep_glob_municipio extends CI_Controller
{
    function __construct()
    {
        parent:: __construct();
        $this->load->model('reportes/reporte_gerencial/mrep_municipio');
        //llamar a mi menu
        $this->load->library('menu');
        $this->menu->const_menu(7);
        $this->gestion = $this->session->userData('gestion');
        $this->mes = $this->session->userData('mes');
    }

    function index()
    {
        $mes_id = $this->session->userData('mes');
        $lista = $this->mrep_municipio->proyecto_inversion($mes_id, $this->gestion);
        $data_tabla = $this->tabla_nivel_proyecto($lista, '#A3CBF2');
        $data['tabla_proy_inv'] = $data_tabla['tabla_proy'];
        $data['pie_proy_inv'] = $data_tabla['pie_proy'];
        $lista = $this->mrep_municipio->prog_no_recurrente($mes_id, $this->gestion);
        $data_tabla = $this->tabla_nivel_proyecto($lista, '#B1F2A4');
        $data['tabla_prog_norec'] = $data_tabla['tabla_proy'];
        $data['pie_prog_norec'] = $data_tabla['pie_proy'];
        $lista = $this->mrep_municipio->reporte_pi_pnr($mes_id, $this->gestion);
        $data_tabla = $this->tabla_nivel_proyecto($lista, '#E7BADE');
        $data['tabla_pi_pnr'] = $data_tabla['tabla_proy'];
        $data['pie_pi_pnr'] = $data_tabla['pie_proy'];

        $ruta = 'reportes/reportes_gerenciales/reporte_global/vpor_municipio';
        $this->construir_vista($ruta, $data);
    }

    function tabla_nivel_proyecto($lista, $color)
    {
        $mat['tabla_proy'] = '';
        $mat['pie_proy'] = '';
        $total_ppto_vigente = 0;
        foreach ($lista as $item) {
            $total_ppto_vigente += $item['ppto_vigente'];
        }
        $sum_xin = 0;
        $sum_ypr = 0;
        $sum_total = 0;
        $sum_ppto_inicial = 0;
        $sum_modif_aprobadas = 0;
        $sum_ppto_vig = 0;
        $sum_por = 0;
        $sum_ppto_ejec = 0;
        foreach ($lista as $item) {
            $mat['tabla_proy'] .= '<tr>';
            $mat['tabla_proy'] .= '<td class="descripcion">' . $item['muni_municipio'] . '</td>';
            $mat['tabla_proy'] .= '<td>' . $item['cantx'] . '</td>';
            $mat['tabla_proy'] .= '<td>' . $item['canty'] . '</td>';
            $mat['tabla_proy'] .= '<td>' . ($item['cantx'] + $item['cantx']) . '</td>';
            $mat['tabla_proy'] .= '<td>' . number_format($item['ppto_inicial'], 2, '.', ' ') . '</td>';
            $mat['tabla_proy'] .= '<td>' . number_format($item['modif_aprobadas'], 2, '.', ' ') . '</td>';
            $mat['tabla_proy'] .= '<td>' . number_format($item['ppto_vigente'], 2, '.', ' ') . '</td>';
            $porc = ($total_ppto_vigente == 0) ? 0 : number_format((($item['ppto_vigente'] / $total_ppto_vigente) * 100), 2, '.', '');
            $mat['tabla_proy'] .= '<td>' . $porc . '%</td>';
            $mat['tabla_proy'] .= '<td>' . number_format($item['devengado'], 2, ',', '.') . '</td>';
            $mat['tabla_proy'] .= '</tr>';
            //para los reportes graficos
            $mat['pie_proy'] .= "['" . $item['muni_municipio'] . "'," . $porc . "],";
            $sum_xin += $item['cantx'];
            $sum_ypr += $item['canty'];
            $sum_total += ($item['cantx'] + $item['canty']);
            $sum_ppto_inicial += $item['ppto_inicial'];
            $sum_modif_aprobadas  += $item['modif_aprobadas'];
            $sum_ppto_vig += $item['ppto_vigente'];
            $sum_por += $porc;
            $sum_ppto_ejec += $item['devengado'];
        }
        $mat['tabla_proy'] .= '<tr bgcolor="'.$color.'">';
        $mat['tabla_proy'] .= '<td style="text-align: center">TOTAL</td>';
        $mat['tabla_proy'] .= '<td>' . $sum_xin . '</td>';
        $mat['tabla_proy'] .= '<td>' . $sum_ypr . '</td>';
        $mat['tabla_proy'] .= '<td>' . $sum_total . '</td>';
        $mat['tabla_proy'] .= '<td>' . number_format($sum_ppto_inicial, 2, ',', '.') . '</td>';
        $mat['tabla_proy'] .= '<td>' . number_format($sum_modif_aprobadas, 2, ',', '.') . '</td>';
        $mat['tabla_proy'] .= '<td>' . number_format($sum_ppto_vig, 2, ',', '.') . '</td>';
        $mat['tabla_proy'] .= '<td>' . number_format($sum_por, 2, '.', '' ). '%</td>';
        $mat['tabla_proy'] .= '<td>' . number_format($sum_ppto_ejec, 2, ',', '.') . '</td>';
        $mat['tabla_proy'] .= '</tr>';
        return $mat;
    }

    function construir_vista($ruta, $data)
    {
        //----------------------------------- MENU-------------------------------
        $menu['enlaces'] = $this->menu->get_enlaces();
        $menu['subenlaces'] = $this->menu->get_sub_enlaces();
        $menu['titulo'] = 'REPORTES';
        //-----------------------------------------------------------------------
        //armar vista
        $this->load->view('includes/header');
        $this->load->view('includes/menu_lateral', $menu);
        $this->load->view($ruta, $data);//contenido
        $this->load->view('includes/footer');
    }
}