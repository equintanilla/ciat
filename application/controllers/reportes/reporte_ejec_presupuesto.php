<?php
define("DOMPDF_ENABLE_REMOTE", true);
define("DOMPDF_TEMP_DIR", "/tmp");
class Reporte_ejec_presupuesto extends CI_Controller { 
  public function __construct ()
    { 
        parent::__construct();
        if($this->session->userdata('fun_id')!=null){
        $this->load->library('pdf');
        $this->load->library('pdf2');

        $this->load->model('programacion/model_reporte');
        $this->load->model('reportes/seguimiento/model_reporte_seguimiento');
        $this->load->model('programacion/model_proyecto');
        $this->load->model('programacion/model_faseetapa');
        $this->load->model('programacion/insumos/minsumos');
        $this->load->model('programacion/insumos/minsumos_delegado');

        $this->load->model('menu_modelo');
        $this->load->model('Users_model','',true);
        $this->gestion = $this->session->userData('gestion');
        }else{
            redirect('/','refresh');
        }
    }
    
    /*---------------------------------------- MENU EJECUCION DEL PRESUPUESTO ----------------------------------*/
    public function ejecucion_presupuesto()
    { 
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++)
        {
             $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        
        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];
        $data['programas'] = $this->model_reporte_seguimiento->list_programas(); ///// lista de programas
        
        $this->load->view('admin/reportes/seguimiento/ejecucion_presupuesto/menu_ejec_presupuesto', $data);
    }
   /*----------------------------- EJECUCION PRESUPUESTO A NIVEL INSTITUCION ------------------------------*/
    public function ejecucion_presupuesto_institucional()
    { 
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++)
        {
            $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        
        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];

        $data['nro']=count($this->model_reporte_seguimiento->list_programas());
        $data['total'] = $this->tabla_matriz_total_institucional(); //// Matriz Total institucional
        $data['tabla'] = $this->insumo_total_institucional($data['total']); //// Matriz Total institucional

        $this->load->view('admin/reportes/seguimiento/ejecucion_presupuesto/pejecucion1', $data);
    }

    public function tabla_matriz_total_institucional()
    {
        $ms[0]='total';$ms[1]='mes1';$ms[2]='mes2';$ms[3]='mes3';$ms[4]='mes4';$ms[5]='mes5';$ms[6]='mes6';
        $ms[7]='mes7';$ms[8]='mes8';$ms[9]='mes9';$ms[10]='mes10';$ms[11]='mes11';$ms[12]='mes12';

        for ($i=0; $i <=12 ; $i++) { 
            $prog[1][$i]=0;
            $prog[2][$i]=0;
        }
        $programado_directo=$this->minsumos->sum_total_programado_institucional_directo($this->gestion); /// suma programa Directo
        $ejecutado_directo=$this->minsumos->sum_total_ejecutado_institucional_directo($this->gestion); /// suma Ejecutado Directo
        $programado_delegado=$this->minsumos_delegado->sum_total_programado_institucional_delegado($this->gestion); /// suma programa Delegado
        $ejecutado_delegado=$this->minsumos_delegado->sum_total_ejecutado_institucional_delegado($this->gestion); /// suma Ejecutado Delegado
        
        for ($i=0; $i <=12 ; $i++) { 
            $prog[1][$i]=$programado_directo[0][$ms[$i]]+$programado_delegado[0][$ms[$i]];
            $prog[2][$i]=$ejecutado_directo[0][$ms[$i]]+$ejecutado_delegado[0][$ms[$i]];
        }

        for($i=1;$i<=12;$i++)
        {
          $totalp[1][$i]=0; //// i
          $totalp[2][$i]=0; //// p
          $totalp[3][$i]=0; //// pa
          $totalp[4][$i]=0; //// %pa
          $totalp[5][$i]=0; //// e
          $totalp[6][$i]=0; //// ea
          $totalp[7][$i]=0; //// %ea
          $totalp[8][$i]=0; //// eficacia
          $totalp[9][$i]=0; //// Menor
          $totalp[10][$i]=0; //// Entre
          $totalp[11][$i]=0; //// Mayor
        }

        $suma_pfin=0; $suma_efin=0;
        for($i=1;$i<=12;$i++)
        {
            $totalp[1][$i]=$i; //// i
            $totalp[2][$i]=$prog[1][$i]; /// programado
            $suma_pfin=$suma_pfin+$prog[1][$i];
            $totalp[3][$i]=$suma_pfin; ///// Programado acumulado

            if($prog[1][0]!=0)
            {
                $totalp[4][$i]=round((($totalp[3][$i]/$prog[1][0])*100),2); ////// Programado Acumulado %
            }

            $totalp[5][$i]=$prog[2][$i]; /// Ejecutado
            $suma_efin=$suma_efin+$prog[2][$i];
            $totalp[6][$i]=$suma_efin; ///// Ejecutado acumulado
              
             if($prog[1][0]!=0)
            {
                $totalp[7][$i]=round((($totalp[6][$i]/$prog[1][0])*100),2); ////// Ejecutado Acumulado %
            }

            if($totalp[4][$i]!=0)
            {
                $totalp[8][$i]=round((($totalp[7][$i]/$totalp[4][$i])*100),2); ////// Eficacia
            }

            /*------------------------- eficacia -------------------------*/
            if($totalp[8][$i]<=75){$totalp[9][$i] = $totalp[8][$i];}
            if ($totalp[8][$i] >= 76 && $totalp[8][$i] <= 90.9) {$totalp[10][$i] = $totalp[8][$i];}
            if($totalp[8][$i] >= 91){$totalp[11][$i] = $totalp[8][$i];}
        }

        return $totalp;
    }

    /*----------------------------- EJECUCION PRESUPUESTO A NIVEL INSTITUCION DETALLES------------------------------*/
    public function ejecucion_presupuesto_institucional_detalles()
    { 
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++)
        {
             $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        
        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];

        $nro_ep=0;
        $tabla = '';
        $programas = $this->model_reporte_seguimiento->list_programas(); ///// lista de programas
        for($j = 0; $j<=12; $j++){$pres_p[$j]=0;$pres_e[$j]=0;$pres_efi[$j]=0;$efi_menor[$j]=0;$efi_entre[$j]=0;$efi_mayor[$j]=0;} //// vector programas programado , Ejecutado vacio
        foreach ($programas as $rowp) 
        {
            $tabla .= '<tr bgcolor=#bdd8ef>';
                $tabla .= '<td>'.$nro_ep.'</td>';
                $tabla .= '<td>'.$rowp['aper_programa'].''.$rowp['aper_proyecto'].''.$rowp['aper_actividad'].'</td>';
                $tabla .= '<td>'.$rowp['aper_descripcion'].'</td>';
                $tabla .= '<td>'.$rowp['uni_unidad'].'</td>';
                $tabla .= '<td>'.$rowp['aper_ponderacion'].' %</td>';
                $tabla .= '<td>';

            $tabla .= '</tr>';

        $nro_ep++;
        }
        $data['togestion'] = $tabla;
        $this->load->view('admin/reportes/seguimiento/ejecucion_presupuesto/pejecucion1_detalle', $data);
    }
    /*---------------------------------------------------- OBJETIVOS DE GESTION A NIVEL PROGRAMAS -------------------------------------*/
    public function ejecucion_presupuesto_programa($poa_id)
    { 
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++)
        {
             $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        
        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];

        $data['poa_id'] = $poa_id;
        $data['programa']=$this->model_reporte_seguimiento->get_programa($poa_id); /// get programa

        $proyectos=$this->model_proyecto->proy_actividades($data['programa'][0]['aper_programa'],$this->session->userdata('gestion'));
        for($i=0;$i<=12;$i++)
        {
          $mp[1][$i]=0; //// programado
          $mp[2][$i]=0; //// ejecutado
        }

        $nro=0;
        foreach ($proyectos as $row) 
        {
            $fase = $this->model_faseetapa->get_id_fase($row['proy_id']);//DATOS DE LA FASE
            if(count($fase)!=0)
            {   
                $nro++;
                $suma_prog = $this->suma_programa($row['proy_id'],$this->gestion); //// Matriz Total
                for ($i=0; $i <=12 ; $i++) { 
                    $mp[1][$i]=$mp[1][$i]+$suma_prog[1][$i]; //// programado
                    $mp[2][$i]=$mp[2][$i]+$suma_prog[2][$i]; //// ejecutado
                }
            }
        }

        $data['nro']=$nro;
        $data['total'] = $this->tabla_total_programas($mp); //// Matriz Total a nivel de programas
        $data['tabla'] = $this->insumo_total_programas($data['total'],$poa_id); //// tabla Total a nivel de programas
        $this->load->view('admin/reportes/seguimiento/ejecucion_presupuesto/pejecucion2', $data);
    }


    public function ejecucion_presupuesto_programa_detalles($poa_id)
    { 
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++)
        {
             $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        
        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];
        $data['poa_id'] = $poa_id;
        $data['programa']=$this->model_reporte_seguimiento->get_programa($poa_id); /// get programa
        $nro_pr=1;

        $tabla = '';
        $proyectos=$this->model_proyecto->proy_actividades($data['programa'][0]['aper_programa'],$this->session->userdata('gestion'));
        $sp=0;
        foreach ($proyectos as $row) 
        {
            $fase = $this->model_faseetapa->get_id_fase($row['proy_id']);//DATOS DE LA FASE
            if(count($fase)!=0)
            {   
                $tabla .= '<tr bgcolor=#bdd8ef>';
                    $tabla .= '<td>'.$nro_pr.'</td>';
                    $tabla .= '<td>'.$row['aper_programa'].''.$row['aper_proyecto'].''.$row['aper_actividad'].'</td>';
                    $tabla .= '<td>'.$row['proy_nombre'].'</td>';
                    $tabla .= '<td>'.$row['tp_tipo'].'</td>';
                    $tabla .= '<td>'.$row['proy_ponderacion'].' %</td>';
                    $tabla .= '<td>';
                        $tabla .= ''.$this->insumo_total($row['proy_id'],$this->gestion).'';
                    $tabla .= '</td>';
                    $tabla .= '</tr>';
                $nro_pr++;
                
            }
        }
        $data['togestion'] = $tabla;
        $this->load->view('admin/reportes/seguimiento/ejecucion_presupuesto/pejecucion2_detalle', $data);
    }
    /*---------------------------------------------------- OBJETIVOS DE GESTION A NIVEL DE OPERACIONES -------------------------------------*/
    public function ejecucion_presupuesto_operacion()
    { 
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++)
        {
             $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        
        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];

        $data['programas'] = $this->model_reporte_seguimiento->list_programas(); ///// lista de programas
        
        $this->load->view('admin/reportes/seguimiento/ejecucion_presupuesto/pejecucion3', $data);
    }

    public function ejecucion_presupuesto_operacion_detalle($poa_id,$proy_id)
    { 
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++)
        {
             $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        
        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];
        $data['programa']=$this->model_reporte_seguimiento->get_programa($poa_id); /// get programa
        for($j = 0; $j<=12; $j++){$prog1[$j]=0;$prog2[$j]=0;$ejec1[$j]=0;$ejec2[$j]=0;$proy_efi[$j]=0;}
        $proy=$this->model_proyecto->get_id_proyecto($proy_id); /// get proyecto
        $data['proy']=$proy;

        $tabla = '';
            $tabla .= '<table  class="table table-bordered" width="100%">';
            $tabla .= '<tr bgcolor=#474544>';
                $tabla .= '<th>CATEGORIA PROGRAM&Aacute;TICA</th>';
                $tabla .= '<th>PROYECTO_PROGRAMA_OPERACI&Oacute;N</th>';
                $tabla .= '<th>TIPO DE OPERACI&Oacute;N</th>';
                $tabla .= '<th>CRONOGRAMA DE PROGRAMACI&Oacute;N Y EJECUCI&Oacute;N</th>';
            $tabla .= '</tr>';
            $tabla .= '<tr>';
                $tabla .= '<td>'.$proy[0]['aper_programa'].''.$proy[0]['aper_proyecto'].''.$proy[0]['aper_actividad'].'</td>';
                $tabla .= '<td>'.$proy[0]['proy_nombre'].'</td>';
                $tabla .= '<td>'.$proy[0]['tipo'].'</td>';
                $tabla .= '<td>';
                    $tabla .= ''.$this->insumo_total($proy_id,$this->gestion).'';
                $tabla .= '</td>';
            $tabla .= '</tr>';
            $tabla .= '</table>';


        $data['total'] = $this->tabla_total($proy_id,$this->gestion); //// Matriz Total
        $data['tprograma'] = $tabla;

        $this->load->view('admin/reportes/seguimiento/ejecucion_presupuesto/pejecucion3_detalle', $data);
    }
    /*==========================================================================================*/
    function suma_programa($proy_id,$gestion)
    {
        $fase = $this->model_faseetapa->get_id_fase($proy_id);
        $ms[0]='total';$ms[1]='mes1';$ms[2]='mes2';$ms[3]='mes3';$ms[4]='mes4';$ms[5]='mes5';$ms[6]='mes6';
        $ms[7]='mes7';$ms[8]='mes8';$ms[9]='mes9';$ms[10]='mes10';$ms[11]='mes11';$ms[12]='mes12';
        
        for($i=0;$i<=12;$i++)
        {
          $totalp[1][$i]=0; //// programado
          $totalp[2][$i]=0; //// ejecutado
        }

        if($fase[0]['pfec_ejecucion']==1){
            $programado = $this->minsumos->sum_total_insumo_prog($proy_id,$gestion);
            $ejecutado = $this->minsumos->sum_total_insumo_ejec($proy_id,$gestion);
        }
        elseif ($fase[0]['pfec_ejecucion']==2) {
           $programado = $this->minsumos_delegado->sum_total_insumo_prog($proy_id,$gestion);
           $ejecutado = $this->minsumos_delegado->sum_total_insumo_ejec($proy_id,$gestion);
        }
        
        if(count($programado)!=0)
        {
            for($i=0;$i<=12;$i++)
            {
                $totalp[1][$i]=$programado[0][$ms[$i]]; /// programado
                if(count($ejecutado)!=0){
                $totalp[2][$i]=$ejecutado[0][$ms[$i]]; /// Ejecutado
              }
            }
        }
        
        return $totalp;
    }
    /*=================================== TABLA TOTAL ANUAL =======================================*/
    function tabla_total($proy_id,$gestion)
    {
        $fase = $this->model_faseetapa->get_id_fase($proy_id);
        $ms[0]='total';$ms[1]='mes1';$ms[2]='mes2';$ms[3]='mes3';$ms[4]='mes4';$ms[5]='mes5';$ms[6]='mes6';
        $ms[7]='mes7';$ms[8]='mes8';$ms[9]='mes9';$ms[10]='mes10';$ms[11]='mes11';$ms[12]='mes12';
        
        for($i=1;$i<=12;$i++)
        {
          $totalp[1][$i]=0; //// i
          $totalp[2][$i]=0; //// p
          $totalp[3][$i]=0; //// pa
          $totalp[4][$i]=0; //// %pa
          $totalp[5][$i]=0; //// e
          $totalp[6][$i]=0; //// ea
          $totalp[7][$i]=0; //// %ea
          $totalp[8][$i]=0; //// eficacia
          $totalp[9][$i]=0; //// Menor
          $totalp[10][$i]=0; //// Entre
          $totalp[11][$i]=0; //// Mayor
        }

        if($fase[0]['pfec_ejecucion']==1){
            $programado = $this->minsumos->sum_total_insumo_prog($proy_id,$gestion);
            $ejecutado = $this->minsumos->sum_total_insumo_ejec($proy_id,$gestion);
        }
        elseif ($fase[0]['pfec_ejecucion']==2) {
           $programado = $this->minsumos_delegado->sum_total_insumo_prog($proy_id,$gestion);
           $ejecutado = $this->minsumos_delegado->sum_total_insumo_ejec($proy_id,$gestion);
        }
        
        if(count($programado)!=0)
        {
            $suma_pfin=0; $suma_efin=0;
            for($i=1;$i<=12;$i++)
            {
                $totalp[1][$i]=$i; //// i
                $totalp[2][$i]=$programado[0][$ms[$i]]; /// programado
                $suma_pfin=$suma_pfin+$programado[0][$ms[$i]];
                $totalp[3][$i]=$suma_pfin; ///// Programado acumulado

              if($programado[0][$ms[0]]!=0)
              {
                $totalp[4][$i]=round((($totalp[3][$i]/$programado[0][$ms[0]])*100),2); ////// Programado Acumulado %
              }
              
              if(count($ejecutado)!=0){
                  $totalp[5][$i]=$ejecutado[0][$ms[$i]]; /// Ejecutado
                  $suma_efin=$suma_efin+$ejecutado[0][$ms[$i]];
                  $totalp[6][$i]=$suma_efin; ///// Ejecutado acumulado
                  
                  if($programado[0][$ms[0]]!=0)
                  {
                    $totalp[7][$i]=round((($totalp[6][$i]/$programado[0][$ms[0]])*100),2); ////// Ejecutado Acumulado %
                  }

                  if($totalp[4][$i]!=0)
                  {
                    $totalp[8][$i]=round((($totalp[7][$i]/$totalp[4][$i])*100),2); ////// Eficacia
                  }

                  /*------------------------- eficacia -------------------------*/
                  if($totalp[8][$i]<=75){$totalp[9][$i] = $totalp[8][$i];}
                  if ($totalp[8][$i] >= 76 && $totalp[8][$i] <= 90.9) {$totalp[10][$i] = $totalp[8][$i];}
                  if($totalp[8][$i] >= 91){$totalp[11][$i] = $totalp[8][$i];}
              }
            }
        }
        
        return $totalp;
    }

    /*=================================== TABLA TOTAL ANUAL  NIVEL PROGRAMAS =======================================*/
    function tabla_total_programas($mp)
    {
        for($i=1;$i<=12;$i++)
        {
          $totalp[1][$i]=0; //// i
          $totalp[2][$i]=0; //// p
          $totalp[3][$i]=0; //// pa
          $totalp[4][$i]=0; //// %pa
          $totalp[5][$i]=0; //// e
          $totalp[6][$i]=0; //// ea
          $totalp[7][$i]=0; //// %ea
          $totalp[8][$i]=0; //// eficacia
          $totalp[9][$i]=0; //// Menor
          $totalp[10][$i]=0; //// Entre
          $totalp[11][$i]=0; //// Mayor
        }

        $suma_pfin=0; $suma_efin=0;
        for($i=1;$i<=12;$i++)
        {
            $totalp[1][$i]=$i; //// i
            $totalp[2][$i]=$mp[1][$i]; /// programado
            $suma_pfin=$suma_pfin+$mp[1][$i];
            $totalp[3][$i]=$suma_pfin; ///// Programado acumulado

              if($mp[1][0]!=0)
              {
                $totalp[4][$i]=round((($totalp[3][$i]/$mp[1][0])*100),2); ////// Programado Acumulado %
              }
          
              $totalp[5][$i]=$mp[2][$i]; /// Ejecutado
              $suma_efin=$suma_efin+$mp[2][$i];
              $totalp[6][$i]=$suma_efin; ///// Ejecutado acumulado
              
              if($mp[1][0]!=0)
              {
                $totalp[7][$i]=round((($totalp[6][$i]/$mp[1][0])*100),2); ////// Ejecutado Acumulado %
              }

              if($totalp[4][$i]!=0)
              {
                $totalp[8][$i]=round((($totalp[7][$i]/$totalp[4][$i])*100),2); ////// Eficacia
              }

              /*------------------------- eficacia -------------------------*/
              if($totalp[8][$i]<=75){$totalp[9][$i] = $totalp[8][$i];}
              if ($totalp[8][$i] >= 76 && $totalp[8][$i] <= 90.9) {$totalp[10][$i] = $totalp[8][$i];}
              if($totalp[8][$i] >= 91){$totalp[11][$i] = $totalp[8][$i];}

        }
        
        return $totalp;
    }


    public function insumo_total($proy_id,$gestion)
    {
        $total= $this->tabla_total($proy_id,$gestion);
        $tabla = '';   
        $tabla .='<div class="contenedor_datos">
                    <table class="table table-bordered" width="100%">
                        <tr bgcolor=#474544>
                            <th colspan=13>GESTI&Oacute;N '.$gestion.'</th>
                        </tr>
                        <tr bgcolor=#474544>
                            <th style="width:2%;">P/E</th>
                            <th style="width:5%;">ENERO</th>
                            <th style="width:5%;">FEBRERO</th>
                            <th style="width:5%;">MARZO</th>
                            <th style="width:5%;">ABRIL</th>
                            <th style="width:5%;">MAYO</th>
                            <th style="width:5%;">JUNIO</th>
                            <th style="width:5%;">JULIO</th>
                            <th style="width:5%;">AGOSTO</th>
                            <th style="width:5%;">SEPTIEMBRE</th>
                            <th style="width:5%;">OCTUBRE</th>
                            <th style="width:5%;">NOVIEMBRE</th>
                            <th style="width:5%;">DICIEMBRE</th>
                        </tr>
                        <tr class="odd_row">
                            <td>P.</td>';
                            for ($i=1; $i <=12 ; $i++) 
                            { 
                               $tabla .='<td>'.number_format($total[2][$i], 2, ',', '.').'</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="odd_row">
                            <td>P.A.</td>';
                            for ($i=1; $i <=12 ; $i++) 
                            { 
                               $tabla .='<td>'.number_format($total[3][$i], 2, ',', '.').'</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="odd_row">
                            <td>%P.A.</td>';
                            for ($i=1; $i <=12 ; $i++) 
                            { 
                               $tabla .='<td>'.$total[4][$i].'%</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="odd_row">
                            <td>E</td>';
                            for ($i=1; $i <=12 ; $i++) 
                            { 
                               $tabla .='<td>'.number_format($total[5][$i], 2, ',', '.').'</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="odd_row">
                            <td>E.A.</td>';
                            for ($i=1; $i <=12 ; $i++) 
                            { 
                               $tabla .='<td>'.number_format($total[6][$i], 2, ',', '.').'</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="odd_row">
                            <td>%E.A.</td>';
                            for ($i=1; $i <=12 ; $i++) 
                            { 
                               $tabla .='<td>'.$total[7][$i].'%</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="odd_row">
                            <td>EFICACIA</td>';
                            for ($i=1; $i <=12 ; $i++) 
                            { 
                               $tabla .='<td>'.$total[8][$i].'%</td>'; 
                            }
                        $tabla .='
                        </tr>';
                    $tabla .='
                    </table>
                </div>';
     
        return $tabla;
    }

    public function insumo_total_programas($total,$poa_id)
    {
        $tabla = '';   
        $tabla .='<div class="contenedor_datos">
                    <table class="table table-bordered" width="100%">
                        <tr bgcolor=#474544>
                            <th colspan=13>GESTI&Oacute;N '.$this->session->userData('gestion').'</th>
                        </tr>
                        <tr bgcolor=#474544>
                            <th style="width:2%;">P/E</th>
                            <th style="width:5%;">ENERO</th>
                            <th style="width:5%;">FEBRERO</th>
                            <th style="width:5%;">MARZO</th>
                            <th style="width:5%;">ABRIL</th>
                            <th style="width:5%;">MAYO</th>
                            <th style="width:5%;">JUNIO</th>
                            <th style="width:5%;">JULIO</th>
                            <th style="width:5%;">AGOSTO</th>
                            <th style="width:5%;">SEPTIEMBRE</th>
                            <th style="width:5%;">OCTUBRE</th>
                            <th style="width:5%;">NOVIEMBRE</th>
                            <th style="width:5%;">DICIEMBRE</th>
                        </tr>
                        <tr class="odd_row">
                            <td><a href="'.base_url().'index.php/admin/rep/rep_ejec_programa_detalles/'.$poa_id.'" title="DETALLES">PROGRAMADO</td>';
                            for ($i=1; $i <=12 ; $i++) 
                            { 
                               $tabla .='<td>'.number_format($total[2][$i], 2, ',', '.').'</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="odd_row">
                            <td><a href="'.base_url().'index.php/admin/rep/rep_ejec_programa_detalles/'.$poa_id.'" title="DETALLES">PROGRAMADO ACUMULADO</td>';
                            for ($i=1; $i <=12 ; $i++) 
                            { 
                               $tabla .='<td>'.number_format($total[3][$i], 2, ',', '.').'</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="odd_row">
                            <td><a href="'.base_url().'index.php/admin/rep/rep_ejec_programa_detalles/'.$poa_id.'" title="DETALLES">%PROGRAMADO ACUMULADO</td>';
                            for ($i=1; $i <=12 ; $i++) 
                            { 
                               $tabla .='<td>'.$total[4][$i].'%</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="odd_row">
                            <td><a href="'.base_url().'index.php/admin/rep/rep_ejec_programa_detalles/'.$poa_id.'" title="DETALLES">EJECUTADO</td>';
                            for ($i=1; $i <=12 ; $i++) 
                            { 
                               $tabla .='<td>'.number_format($total[5][$i], 2, ',', '.').'</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="odd_row">
                            <td><a href="'.base_url().'index.php/admin/rep/rep_ejec_programa_detalles/'.$poa_id.'" title="DETALLES">EJECUTADO ACUMULADO</td>';
                            for ($i=1; $i <=12 ; $i++) 
                            { 
                               $tabla .='<td>'.number_format($total[6][$i], 2, ',', '.').'</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="odd_row">
                            <td><a href="'.base_url().'index.php/admin/rep/rep_ejec_programa_detalles/'.$poa_id.'" title="DETALLES">%EJECUTADO ACUMULADO</td>';
                            for ($i=1; $i <=12 ; $i++) 
                            { 
                               $tabla .='<td>'.$total[7][$i].'%</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="odd_row">
                            <td><a href="'.base_url().'index.php/admin/rep/rep_ejec_programa_detalles/'.$poa_id.'" title="DETALLES">EFICACIA</td>';
                            for ($i=1; $i <=12 ; $i++) 
                            { 
                               $tabla .='<td>'.$total[8][$i].'%</td>'; 
                            }
                        $tabla .='
                        </tr>';
                    $tabla .='
                    </table>
                </div>';
     
        return $tabla;
    }

    public function insumo_total_institucional($total)
    {
        $tabla = '';   
        $tabla .='<div class="contenedor_datos">
                    <table class="table table-bordered" width="100%">
                        <tr bgcolor=#474544>
                            <th colspan=13>GESTI&Oacute;N '.$this->session->userData('gestion').'</th>
                        </tr>
                        <tr bgcolor=#474544>
                            <th style="width:2%;">P/E</th>
                            <th style="width:5%;">ENERO</th>
                            <th style="width:5%;">FEBRERO</th>
                            <th style="width:5%;">MARZO</th>
                            <th style="width:5%;">ABRIL</th>
                            <th style="width:5%;">MAYO</th>
                            <th style="width:5%;">JUNIO</th>
                            <th style="width:5%;">JULIO</th>
                            <th style="width:5%;">AGOSTO</th>
                            <th style="width:5%;">SEPTIEMBRE</th>
                            <th style="width:5%;">OCTUBRE</th>
                            <th style="width:5%;">NOVIEMBRE</th>
                            <th style="width:5%;">DICIEMBRE</th>
                        </tr>
                        <tr class="odd_row">
                            <td><a href="'.base_url().'index.php/admin/rep/rep_ejec_institucion_detalles" title="DETALLES">PROGRAMADO</td>';
                            for ($i=1; $i <=12 ; $i++) 
                            { 
                               $tabla .='<td>'.number_format($total[2][$i], 2, ',', '.').'</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="odd_row">
                            <td><a href="'.base_url().'index.php/admin/rep/rep_ejec_institucion_detalles" title="DETALLES">PROGRAMADO ACUMULADO</td>';
                            for ($i=1; $i <=12 ; $i++) 
                            { 
                               $tabla .='<td>'.number_format($total[3][$i], 2, ',', '.').'</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="odd_row">
                            <td><a href="'.base_url().'index.php/admin/rep/rep_ejec_institucion_detalles" title="DETALLES">%PROGRAMADO ACUMULADO</td>';
                            for ($i=1; $i <=12 ; $i++) 
                            { 
                               $tabla .='<td>'.$total[4][$i].'%</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="odd_row">
                            <td><a href="'.base_url().'index.php/admin/rep/rep_ejec_institucion_detalles" title="DETALLES">EJECUTADO</td>';
                            for ($i=1; $i <=12 ; $i++) 
                            { 
                               $tabla .='<td>'.number_format($total[5][$i], 2, ',', '.').'</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="odd_row">
                            <td><a href="'.base_url().'index.php/admin/rep/rep_ejec_institucion_detalles" title="DETALLES">EJECUTADO ACUMULADO</td>';
                            for ($i=1; $i <=12 ; $i++) 
                            { 
                               $tabla .='<td>'.number_format($total[6][$i], 2, ',', '.').'</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="odd_row">
                            <td><a href="'.base_url().'index.php/admin/rep/rep_ejec_institucion_detalles" title="DETALLES">%EJECUTADO ACUMULADO</td>';
                            for ($i=1; $i <=12 ; $i++) 
                            { 
                               $tabla .='<td>'.$total[7][$i].'%</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="odd_row">
                            <td><a href="'.base_url().'index.php/admin/rep/rep_ejec_institucion_detalles" title="DETALLES">EFICACIA</td>';
                            for ($i=1; $i <=12 ; $i++) 
                            { 
                               $tabla .='<td>'.$total[8][$i].'%</td>'; 
                            }
                        $tabla .='
                        </tr>';
                    $tabla .='
                    </table>
                </div>';
     
        return $tabla;
    }
    /*=======================================================================================================================*/


    /*------------------------------- PROGRAMADO FINANCIERO ANTERIOR -------------------------------------*/
    public function programado_fin($proy_id,$gestion)
    {
        $fase = $this->model_faseetapa->get_id_fase($proy_id);//DATOS DE LA FASE
          $mes[0]='monto';
          $mes[1]='enero';
          $mes[2]='febrero';
          $mes[3]='marzo';
          $mes[4]='abril';
          $mes[5]='mayo';
          $mes[6]='junio';
          $mes[7]='julio';
          $mes[8]='agosto';
          $mes[9]='septiembre';
          $mes[10]='octubre';
          $mes[11]='noviembre';
          $mes[12]='diciembre';

        for($i=0;$i<=12;$i++){ $pfin[$i]=0;}

        if ($fase[0]['pfec_ejecucion'] == 1) ///// EJECUCION DIRECTA
        {
            $prog_fin= $this->model_reporte->programado_financiero_directo($gestion, $proy_id); 
        }
        elseif ($fase[0]['pfec_ejecucion'] == 2) ///// EJECUCION DELEGADA
        {
            $prog_fin= $this->model_reporte->programado_financiero_delegado($gestion, $proy_id);
        }

        if(count($prog_fin)!=0)
        {
            for($i=0;$i<=12;$i++)
            {
                $pfin[$i]=$prog_fin[0][$mes[$i]];
            } 
        }

        return $pfin;
    }

    /*------------------------------- EJECUTADO FINANCIERO -------------------------------------*/
    public function ejecutado_fin($proy_id,$gestion)
    {
        $fase = $this->model_faseetapa->get_id_fase($proy_id);//DATOS DE LA FASE

          $mes[1]='ejec1';
          $mes[2]='ejec2';
          $mes[3]='ejec3';
          $mes[4]='ejec4';
          $mes[5]='ejec5';
          $mes[6]='ejec6';
          $mes[7]='ejec7';
          $mes[8]='ejec8';
          $mes[9]='ejec9';
          $mes[10]='ejec10';
          $mes[11]='ejec11';
          $mes[12]='ejec12';

        for($i=1;$i<=12;$i++){ $efin[$i]=0;}
        $ejec_fin= $this->model_reporte->ejecutado_financiero($proy_id,$gestion); 

        if(count($ejec_fin)!=0)
        {
            for($i=1;$i<=12;$i++)
            {
                $efin[$i]=$ejec_fin[0][$mes[$i]];
            } 
        }

        return $efin;
    }
  /*===================================================================================================*/
  /*===================================================================================================*/
    public function get_mes($mes_id)
    {
      $mes[1]='Enero';
      $mes[2]='Febrero';
      $mes[3]='Marzo';
      $mes[4]='Abril';
      $mes[5]='Mayo';
      $mes[6]='Junio';
      $mes[7]='Julio';
      $mes[8]='Agosto';
      $mes[9]='Septiembre';
      $mes[10]='Octubre';
      $mes[11]='Noviembre';
      $mes[12]='Diciembre';

      $dias[1]='31';
      $dias[2]='28';
      $dias[3]='31';
      $dias[4]='30';
      $dias[5]='31';
      $dias[6]='30';
      $dias[7]='31';
      $dias[8]='31';
      $dias[9]='30';
      $dias[10]='31';
      $dias[11]='30';
      $dias[12]='31';

      $valor[1]=$mes[$mes_id];
      $valor[2]=$dias[$mes_id];

      return $valor;
    }
}