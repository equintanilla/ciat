<?php

//presupuesto ejecutado
class Crep_pres_ejec extends CI_Controller
{
    var $gestion;
    var $mes;

    function __construct()
    {
        parent:: __construct();
        $this->load->model('reportes/presupuesto/mreporte_pres_prog');
        $this->load->model('reportes/presupuesto/mreporte_pres_ejec');
        $this->load->model('mantenimiento/mpoa');
        //llamar a mi menu
        $this->load->library('menu');
        $this->menu->const_menu(7);
        $this->gestion = $this->session->userData('gestion');
        $this->mes = $this->session->userData('mes');
    }

    function index()
    {
        $data['lista_poa'] = $this->mpoa->lista_poa();
        $data['ruta_proy'] = 'rep/pres_ejec_lproy';
        $ruta = 'reportes/presupuesto/vlista_programas';
        $this->construir_vista($ruta, $data);
    }

    function lista_proyectos($programa)
    {
        $data['atras'] = site_url() . '/rep/pres_ejec';
        $data['ruta'] = site_url() . '/rep/pres_ejec_proy/'.$programa.'/';
        $data['lista_proy'] = $this->mreporte_pres_prog->lista_proyectos($programa, $this->gestion);
        $ruta = 'reportes/presupuesto/vlista_proyectos';
        $this->construir_vista($ruta, $data);
    }

    function presupuesto_ejecutado($programa, $proy_id, $tipo_ejec)
    {
        if ($tipo_ejec == 1) {//directo
            $prog_ejec = $this->mreporte_pres_ejec->proy_prog_ejec_directo($proy_id, $this->gestion);
            $data_tabla = $this->pres_prog_ejec($prog_ejec);
            $data['tabla_prog_ejec'] = $data_tabla['tabla'];
            $data['footer'] = $data_tabla['footer'];
        } elseif ($tipo_ejec == 2) {//delegado
            $prog_ejec = $this->mreporte_pres_ejec->proy_prog_ejec_delegado($proy_id, $this->gestion);
            $data_tabla = $this->pres_prog_ejec($prog_ejec);
            $data['tabla_prog_ejec'] = $data_tabla['tabla'];
            $data['footer'] = $data_tabla['footer'];
        } else {//default
            $data['tabla_pres_prog'] = 'DATOS ERRONEOS';
            $data['footer'] = '';
        }
        $data['atras_main'] = site_url() . '/rep/pres_ejec';
        $data['atras'] = site_url() . '/rep/pres_ejec_lproy/'.$programa;
        $ruta = 'reportes/presupuesto/vpres_ejec_proy';
        $this->construir_vista($ruta, $data);
    }

    //presupuesto programado ejecutado
    function pres_prog_ejec($prog_ejec)
    {
        if (count($prog_ejec) != 0) {
            $tabla = '';
            $sum_ppto_inicial = 0;
            $sum_ppto_vigente = 0;
            $sum_mod_aprobadas = 0;
            foreach ($prog_ejec as $item) {
                $tabla .= '<tr>';
                $tabla .= '<td>' . $item['par_codigo'] . '</td>';
                $tabla .= '<td>' . $item['par_nombre'] . '</td>';
                $tabla .= '<td>' . $item['ff_codigo'] . '</td>';
                $tabla .= '<td>' . $item['of_codigo'] . '</td>';
                $tabla .= '<td>' . $item['et_codigo'] . '</td>';
                $tabla .= '<td>' . number_format($item['ppto_inicial'], 2, ',', '.') . '</td>';
                $tabla .= '<td>' . number_format($item['modif_aprobadas'], 2, ',', '.') . '</td>';
                $tabla .= '<td>' . number_format($item['ppto_vigente'], 2, ',', '.') . '</td>';
                $tabla .= '<td>';//---------------------------------------------------
                $tabla .= '<table class="table table-bordered">';
                $tabla .= '<thead>';
                $tabla .= '<tr>'; //cabecera
                $tabla .= '<td style="width: 5px;background-color: #568A89;color: white"> P/E</td>';
                for ($i = 1; $i <= 12; $i++) {
                    $tabla .= '<td style="width: 5px;background-color: #568A89;color: white">' . $this->get_mes($i) . '</td>';
                }
                $tabla .= '</tr>';
                $tabla .= '</thead>';
                $tabla .= '<tr>';
                $tabla .= '<td style="width: 5px;background-color: #568A89;color: white">P.</td>';
                $tabla .= '<td>' . number_format($item['enero'], 2, ',', '.') . '</td>';
                $tabla .= '<td>' . number_format($item['febrero'], 2, ',', '.') . '</td>';
                $tabla .= '<td>' . number_format($item['marzo'], 2, ',', '.') . '</td>';
                $tabla .= '<td>' . number_format($item['abril'], 2, ',', '.') . '</td>';
                $tabla .= '<td>' . number_format($item['mayo'], 2, ',', '.') . '</td>';
                $tabla .= '<td>' . number_format($item['junio'], 2, ',', '.') . '</td>';
                $tabla .= '<td>' . number_format($item['julio'], 2, ',', '.') . '</td>';
                $tabla .= '<td>' . number_format($item['agosto'], 2, ',', '.') . '</td>';
                $tabla .= '<td>' . number_format($item['septiembre'], 2, ',', '.') . '</td>';
                $tabla .= '<td>' . number_format($item['octubre'], 2, ',', '.') . '</td>';
                $tabla .= '<td>' . number_format($item['noviembre'], 2, ',', '.') . '</td>';
                $tabla .= '<td>' . number_format($item['diciembre'], 2, ',', '.') . '</td>';
                $tabla .= '</tr>';
                $tabla .= '<tr>';
                $tabla .= '<td style="width: 5px;background-color: #568A89;color: white">E.</td>';
                for ($i = 1; $i <= 12; $i++) {
                    $tabla .= '<td>' . number_format($item[('ejec' . $i)], 2, ',', '.') . '</td>';
                }
                $tabla .= '</tr>';
                $tabla .= '</table>';
                $tabla .= '</td>';//---------------------------------------------------
                $tabla .= '</tr>';
                $sum_ppto_inicial += $item['ppto_inicial'];
                $sum_mod_aprobadas += $item['modif_aprobadas'];
                $sum_ppto_vigente += $item['ppto_vigente'];
            }
            $data['tabla'] = $tabla;
            $data['footer'] = '<tr>';
            $data['footer'] .= '<th colspan="5"><center>TOTAL</center></th>';
            $data['footer'] .= '<th>'.number_format($sum_ppto_inicial, 2, ',', '.').'</th>';
            $data['footer'] .= '<th>'.number_format($sum_mod_aprobadas, 2, ',', '.').'</th>';
            $data['footer'] .= '<th>'.number_format($sum_ppto_vigente, 2, ',', '.').'</th>';
            $data['footer'] .= '<th></th>';
            $data['footer'] .= '</tr>';
        } else {
            //NO EXISTE DATOS
            $data['tabla'] = '';
            $data['footer'] = '';
        }
        return $data;
    }

    function get_mes($num)
    {
        $mes[1] = 'ENERO';
        $mes[2] = 'FEBRERO';
        $mes[3] = 'MARZO';
        $mes[4] = 'ABRIL';
        $mes[5] = 'MAYO';
        $mes[6] = 'JUNIO';
        $mes[7] = 'JULIO';
        $mes[8] = 'AGOSTO';
        $mes[9] = 'SEPTIEMBRE';
        $mes[10] = 'OCTUBRE';
        $mes[11] = 'NOVIEMBRE';
        $mes[12] = 'DICIEMBRE';
        return $mes[$num];
    }

    function construir_vista($ruta, $data)
    {
        //----------------------------------- MENU-------------------------------
        $menu['enlaces'] = $this->menu->get_enlaces();
        $menu['subenlaces'] = $this->menu->get_sub_enlaces();
        $menu['titulo'] = 'REPORTES';
        //-----------------------------------------------------------------------
        //armar vista
        $this->load->view('includes/header');
        $this->load->view('includes/menu_lateral', $menu);
        $this->load->view($ruta, $data);//contenido
        $this->load->view('includes/footer');
    }

}