<?php
require_once APPPATH.'controllers/reportes/dictamen.php';

class Operaciones extends Dictamen
{
    private $estilo = '<style>
        body{
            font-family: sans-serif;
            }
        @page {
        margin: 40px 40px;
        }
        header {
            position: fixed;
            left: 0px;
            top: -20px;
            right: 0px;
            height: 20px;
            background-color: #fff;
            }
        header h1{
            margin: 5px 0;
        }
        header h2{
            margin: 0 0 10px 0;
        }
        footer {
            position: fixed;
            left: 0px;
            bottom: -82px;
            right: 0px;
            height: 100px;
            border-bottom: 1px solid #ddd;
        }
        footer .page:after {
            content: counter(page) ;
        }
        footer table {
            width: 100%;
        }
        footer p {
            text-align: right;
        }
        footer .izq {
            text-align: left;
        }
        table{
            font-size: 9px;
            width: 100%;
            background-color:#fff;
        }
        th, td {
                padding: 1.5px;
                text-align: center;
            }
        .mv{font-size:10px;}
        .siipp{width:200px;}
        .header_table {
            color: #ffffff;
            text-align: center;
            align-items: center;
            align-self: center;
            font-weight: bold;
            background-color: #696969;
            align-content: center;
        }
        .header_subtable {
            color: #ffffff;
            text-align: center;
            align-items: center;
            align-self: center;
            font-weight: bold;
            background-color: #a59393;
            align-content: center;
        }
        .titulo_pdf {
            text-align: left;
            font-size: 11px;
        }
        .datos_principales {
            text-align: center;
            font-size: 11px;
        }
        .pdes_titulo{
            text-decoration: underline;
            font-weight: bold;
            text-transform: uppercase;
        }
        .td_pdes{
            text-align: left;  
        }
        .indi_desemp{
            font-size: 14px;
            border: 1px solid #CCC;
            background-color: #E0E0E0;
            padding: .5em;
        }
        table.fixed { table-layout:fixed; }
        table.fixed td { overflow: hidden; }
        .sub_table{
            border-bottom:1px solid black;
        }
        .contenedor_principal{
            border-style: solid;
            border-width:1px;
        }
        .titulo_dictamen{
            padding:5px;
            background-color: #454545;
            border-style: solid;margin: 2px;
            border-width: 1px;
            color: white;
            font-size: 10px;
            border-color: black;
        }
        .contenedor_datos{
            padding:0px;
            margin:1px;
        }
        .table_contenedor{
            border-style: solid;
            margin: 0px;
            padding: 3px;
            border-width: 1px;
            border-collapse: collapse;
        }
        .collapse_t td{
            border-style: solid;
            margin: 0px;
            padding: 3px;
            border-width: 1px;
            border-collapse: collapse;
        }
        .fila_unitaria{
            text-align:left;
            padding: 8;
        }
        .lista{
            text-align:left;
            padding-left: 8;
            list-style-type:square;
            margin:2px;
        }
    </style>';
    public function __construct()
    {
        parent::__construct();
        $this->load->library('pdf');
        $this->load->library('pdf2');
        $this->load->model('reportes/model_objetivo');
    }

    public function reporte_operacion($aper_id)
    {
        $gestion = $this->session->userdata('gestion');
        $datos_aper = $this->model_objetivo->get_datos_apertura($aper_id,$gestion);
        $datos_aper = $datos_aper->row();
        $aper_programa = $datos_aper->aper_programa;
        $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/logo.jpg" class="siipp"/>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata('entidad').'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$gestion.'<br>
                            <b>'.$this->session->userdata('sistema').' </b><br>
                            <b>FORMULARIO : </b> OPERACIONES DE INVERSIÓN Y FUNCIONAMIENTO '.$aper_programa.'<br>
                        </td>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/escudo.jpg" alt="" width="100px">
                        </td>
                    </tr>
                </table>
                <footer>
                    <table class="table table-bordered">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata("sistema_pie").'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>
                <table class="datos_principales" style="table-layout:fixed;" border="1">
                    <tr>
                        <td class="fila_unitaria">
                            <b> PROGRAMA: </b>'.$datos_aper->aper_programatica.'
                        </td>
                    </tr>
                    <tr>
                        <td class="fila_unitaria">
                            <b> DESCRIPCIÓN: </b>'.$datos_aper->aper_descripcion.'
                        </td>
                    </tr>
                </table>
                <br>
                <div class="contenedor_principal">
                    '.$this->proyectos_para_operaciones($aper_programa,$gestion).'
                </div>
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'landscape');
        $dompdf->render();
        $dompdf->stream("reporte_operaciones.pdf", array("Attachment" => false));
    }

    public function proyectos_para_operaciones($aper_programa,$gestion)
    {
        $html_proy_operacion = '';
        $proy_operaciones = $this->model_objetivo->get_reportes_operaciones($aper_programa,$gestion);
        $proy_operaciones = $proy_operaciones->result_array();
        $n=0;
        $html_inversion = '<div class="titulo_dictamen"> PROYECTOS DE INVERIÓN </div>';
        $html_recurrente = '<div class="titulo_dictamen"> PROGRAMAS RECURRENTES </div>';
        $html_norecurrente = '<div class="titulo_dictamen"> PROGRAMAS NO RECURRENTES </div>';
        $html_op_funcionamiento = '<div class="titulo_dictamen"> OPERACIÓN DE FUNCIONAMIENTO </div>';
        foreach ($proy_operaciones as $fila) {
            switch ($fila['tp_id']) {
                case 1:
                    $html_proy_operacion .= $html_inversion;
                    $n++;
                    break;
                case 2:
                    $html_proy_operacion .= $html_recurrente;
                    $n++;
                    break;
                case 3:
                    $html_proy_operacion .= $html_norecurrente;
                    $n++;
                    break;
                case 4:
                    $html_proy_operacion .= $html_op_funcionamiento;
                    $n++;
                    break;
                default:
                    $html_proy_operacion .= '';
                    $n++;
                    break;
            }
        };
        // $html_proy_operacion .= '
        //     <div class="contenedor_datos">
        //         <table class="table_contenedor">
        //             <tr class="collapse_t">
        //                 <td style="width:18%"  class="fila_unitaria">Tipo</td>
        //                 <td class="fila_unitaria">Generales</td>
        //             </tr>
        //         </table>
        //     </div>
        //     <div class="contenedor_datos">
        //         <table class="table_contenedor">
        //             <tr class="collapse_t">
        //                 <td style="width:18%"  class="fila_unitaria">Tipo</td>
        //                 <td class="fila_unitaria">Generales</td>
        //             </tr>
        //         </table>
        //     </div>
        //     <div class="contenedor_datos">
        //         <table class="table_contenedor">
        //             <tr class="collapse_t">
        //                 <td style="width:18%"  class="fila_unitaria">Tipo</td>
        //                 <td class="fila_unitaria">Generales</td>
        //             </tr>
        //         </table>
        //     </div>
        //     <div class="contenedor_datos">
        //         <table class="table_contenedor">
        //             <tr class="collapse_t">
        //                 <td style="width:18%"  class="fila_unitaria">Tipo</td>
        //                 <td class="fila_unitaria">Generales</td>
        //             </tr>
        //         </table>
        //     </div>
        // ';
        return $html_proy_operacion;
    }

}