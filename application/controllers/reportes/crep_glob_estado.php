<?php

///Reporte global - nivel estado de proyectos
class Crep_glob_estado extends CI_Controller
{
    function __construct()
    {
        parent:: __construct();
        $this->load->model('reportes/reporte_gerencial/mestado_proyectos');
        //llamar a mi menu
        $this->load->library('menu');
        $this->menu->const_menu(7);
        $this->gestion = $this->session->userData('gestion');
        $this->mes = $this->session->userData('mes');
    }

    function index()
    {
        $mes = $this->mes;
        $lista = $this->mestado_proyectos->tipo_proyecto_estado($mes, $this->gestion, 1);//proyecto de inversion
        $data_proy_inversion = $this->data_proy_estado($lista, '#A3CBF2');
        $data['tabla_proy_inv'] = $data_proy_inversion['tabla'];
        $data['graf_col_pi'] = $data_proy_inversion['graf_col_pi'];
        $data['graf_col_titulo'] = $data_proy_inversion['graf_col_titulo'];
        $lista = $this->mestado_proyectos->tipo_proyecto_estado($mes, $this->gestion, 3);//programa no recurrente
        $data_prog_no_recurrente = $this->data_proy_estado($lista, '#B1F2A4');
        $data['tabla_prog_norec'] = $data_prog_no_recurrente['tabla'];
        $data['graf_col_prog_norec'] = $data_prog_no_recurrente['graf_col_pi'];
        $data['graf_col_tit_prog_norec'] = $data_prog_no_recurrente['graf_col_titulo'];
        $lista = $this->mestado_proyectos->proy_inv_prog_norec($mes, $this->gestion);//proyecto de inversion + programa no recurrente
        $data_prog_no_recurrente = $this->data_proy_estado($lista, '#ebbf40');
        $data['tabla_pi_pnr'] = $data_prog_no_recurrente['tabla'];
        $data['graf_col_pi_pnr'] = $data_prog_no_recurrente['graf_col_pi'];
        $data['graf_col_titulo_pi_pnr'] = $data_prog_no_recurrente['graf_col_titulo'];
        $ruta = 'reportes/reportes_gerenciales/reporte_global/vestados_proyectos';
        $this->construir_vista($ruta, $data);
    }

    function data_proy_estado($lista, $color)
    {
        $data_pi['tabla'] = '<tr>';
        $data_pi['graf_col_pi'] = '';
        $data_pi['graf_col_titulo'] = '';
        $sum_cant_proy = 0;
        $sum_ppto_ini = 0;
        $sum_modif = 0;
        $sum_ppto_vig = 0;
        $sum_ppto_ejec = 0;
        foreach ($lista as $item) {
            $data_pi['tabla'] .= '<td style="text-align: left">' . $item['ep_descripcion'] . '</td>';
            $data_pi['tabla'] .= '<td>' . $item['nro_proy'] . '</td>';
            $data_pi['tabla'] .= '<td>' . number_format($item['ppto_inicial'], 2, '.', ' ') . '</td>';
            $data_pi['tabla'] .= '<td>' . number_format($item['modif_aprobadas'], 2, '.', ' ') . '</td>';
            $data_pi['tabla'] .= '<td>' . number_format($item['ppto_vigente'], 2, '.', ' ') . '</td>';
            $data_pi['tabla'] .= '<td>' . number_format($item['ppto_ejecutado'], 2, '.', ' ') . '</td>';
            $porc_ejec = ($item['ppto_vigente'] == 0) ? 0.00 : number_format(($item['ppto_ejecutado'] / $item['ppto_vigente']) * 100, 2, '.', '');
            $data_pi['tabla'] .= '<td>' . $porc_ejec . '%</td>';
            //grafico
            $data_pi['graf_col_titulo'] .= "'" . $item['ep_descripcion'] . "',";
            $data_pi['graf_col_pi'] .= $porc_ejec . ',';
            //sumas
            $sum_cant_proy += $item['nro_proy'];
            $sum_ppto_ini += $item['ppto_inicial'];
            $sum_modif += $item['modif_aprobadas'];
            $sum_ppto_vig += $item['ppto_vigente'];
            $sum_ppto_ejec += $item['ppto_ejecutado'];
        }
        $data_pi['tabla'] .= '<tr bgcolor="'.$color.'">';
        $data_pi['tabla'] .= '<td>TOTAL</td>';
        $data_pi['tabla'] .= '<td>'.$sum_cant_proy.'</td>';
        $data_pi['tabla'] .= '<td>'.number_format($sum_ppto_ini, 2, '.', ' ').'</td>';
        $data_pi['tabla'] .= '<td>'.number_format($sum_modif, 2, '.', ' ').'</td>';
        $data_pi['tabla'] .= '<td>'.number_format($sum_ppto_vig, 2, '.', ' ').'</td>';
        $data_pi['tabla'] .= '<td>'.number_format($sum_ppto_ejec, 2, '.', ' ').'</td>';
        $porc_ejec = ($sum_ppto_vig == 0) ? 0.00 : number_format(($sum_ppto_ejec/ $sum_ppto_vig) * 100, 2, '.', ' ');
        $data_pi['tabla'] .= '<td>'.$porc_ejec.'%</td>';
        $data_pi['tabla'] .= '</tr>';

        return $data_pi;
    }

    function construir_vista($ruta, $data)
    {
        //----------------------------------- MENU-------------------------------
        $menu['enlaces'] = $this->menu->get_enlaces();
        $menu['subenlaces'] = $this->menu->get_sub_enlaces();
        $menu['titulo'] = 'REPORTES';
        //-----------------------------------------------------------------------
        //armar vista
        $this->load->view('includes/header');
        $this->load->view('includes/menu_lateral', $menu);
        $this->load->view($ruta, $data);//contenido
        $this->load->view('includes/footer');
    }
}