<?php
//Reporte evaluacion institucional
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Crep_eva_institucional extends CI_Controller
{
    var $gestion;
    var $mes;

    function __construct()
    {
        parent:: __construct();
        $this->load->model('mantenimiento/mpoa');
        $this->load->model('registro_ejec/mejec_ogestion_pterminal');
        $this->load->model('programacion/prog_poa/mobjetivo_gestion');
        $this->load->model('reportes/seguimiento/malerta_ogestion');
        $this->load->model('programacion/prog_poa/mp_terminal');//PTERMINAL
        $this->load->model('reportes/seguimiento/malerta_pterminal');
        $this->load->model('reportes/seguimiento/malerta_ejec_pres');//PRES
        $this->load->model('registro_ejec/mejec_ogestion_pterminal');
        $this->load->model('programacion/prog_poa/mobjetivo_gestion');
        //llamar a mi menu
        $this->load->library('menu');
        $this->menu->const_menu(7);
        $this->gestion = $this->session->userData('gestion');
        $this->mes = $this->session->userData('mes');
    }

    function index()
    {
        $data_ogestion = $this->tabla_eva_ogestion();
        $data['eficacia_ogestion'] = $data_ogestion['tabla'];
        $data['o_menor'] = $data_ogestion['ef_menor'];
        $data['o_entre'] = $data_ogestion['ef_entre'];
        $data['o_mayor'] = $data_ogestion['ef_mayor'];
        $data_pterminal = $this->tabla_eva_pterminal();
        $data['eficacia_pterminal'] = $data_pterminal['tabla'];
        $data['pt_menor'] = $data_pterminal['ef_menor'];
        $data['pt_entre'] = $data_pterminal['ef_entre'];
        $data['pt_mayor'] = $data_pterminal['ef_mayor'];
        $data['lista_ejec_pres'] = $this->malerta_ejec_pres->get_eje_pres($this->mes, $this->gestion);
        $data['mes'] = $this->mes_nombre();
        $ruta = 'reportes/evaluacion/veva_institucional';
        $this->construir_vista($ruta, $data);
    }

    function tabla_eva_ogestion()
    {
        $lista_poa = $this->mejec_ogestion_pterminal->lista_prog();
        $dato['ef_menor'] = '';//vector para el grafico de eficacia
        $dato['ef_entre'] = '';//vector para el grafico de eficacia
        $dato['ef_mayor'] = '';//vector para el grafico de eficacia
        for ($i = 1; $i <= 12; $i++) { //iniciar matriz suma total
            $sum_mat[0][$i] = 0;//total programacion
            $sum_mat[1][$i] = 0;//total ejecucion
        }
        foreach ($lista_poa as $item) {
            $aper_ponderacion = $item['aper_ponderacion'];
            $lista_ogestion = $this->mejec_ogestion_pterminal->lista_ogestion($item['poa_id']);
            for ($i = 1; $i <= 12; $i++) { //iniciar matriz
                $mat[0][$i] = 0;
                $mat[1][$i] = 0;
            }
            foreach ($lista_ogestion as $row) {
                $dato_ogestion = $this->mobjetivo_gestion->get_ogestion($row['o_id'])[0];
                $o_id = $dato_ogestion['o_id'];
                $meta = $dato_ogestion['o_meta'];
                $linea_base = $dato_ogestion['o_linea_base'];
                $indicador = $dato_ogestion['indi_id'];
                $denominador = $dato_ogestion['o_denominador'];
                $ponderacion = $dato_ogestion['o_ponderacion'];
                $mat_prog = $this->mat_prog_mes_ogestion($o_id, $meta, $linea_base); //matriz de la programacion
                $mat_ejec = $this->mat_ejec_mes_ogestion($o_id, $meta, $linea_base, $indicador, $denominador, $mat_prog); //matriz de la programacion
                for ($j = 1; $j <= 12; $j++) {
                    $mat[0][$j] += (count($mat_prog) == 0) ? 0 : ($mat_prog[2][$j] * $ponderacion);
                    $mat[1][$j] += (count($mat_ejec) == 0) ? 0 : ($indicador == 1) ? ($mat_ejec[2][$j] * $ponderacion) : ($mat_ejec[4][$j] * $ponderacion);
                }
            }
            for ($i = 1; $i <= 12; $i++) {
                $sum_mat[0][$i] += ($mat[0][$i]) * $aper_ponderacion;//suma programacion
                $sum_mat[1][$i] += ($mat[1][$i]) * $aper_ponderacion;//suma ejecucion
            }
        }
        $dato['tabla'] = '<tr>';
        for ($i = 1; $i <= 12; $i++) {//EFICACIA
            $val = ($sum_mat[0][$i] == 0) ? 0 : ($sum_mat[1][$i] / $sum_mat[0][$i]) * 100;
            $dato['tabla'] .= '<td>' . number_format($val, 2, ',', '.') . '%</td>';
            $data_efi = $this->get_eficacia(round($val));
            $dato['ef_menor'] .= $data_efi['menor'];
            $dato['ef_entre'] .= $data_efi['entre'];
            $dato['ef_mayor'] .= $data_efi['mayor'];
        }
        $dato['tabla'] .= '</tr>';
        return $dato;
    }

    //matriz de la programacion mensual del pbjetiv de gestion
    function mat_prog_mes_ogestion($o_id, $meta, $linea_base)
    {
        $lista_prog = $this->malerta_ogestion->prog_mensual_ogestion($o_id);
        if (count($lista_prog) != 0) {
            $lista_prog = $this->malerta_ogestion->prog_mensual_ogestion($o_id)[0];
            $prog_acumulado = $linea_base;
            $mat_prog[0][0] = 'Programación';
            $mat_prog[1][0] = 'Programación Acumulada';
            $mat_prog[2][0] = 'Programación Acumulada Porcentual [%]';
            for ($i = 1; $i <= 12; $i++) {
                $puntero_bd = 'fis' . $i;
                $prog_fisica = $lista_prog[$puntero_bd];
                $mat_prog[0][$i] = $prog_fisica;//programacion fisica
                $prog_acumulado += $prog_fisica;
                $mat_prog[1][$i] = $prog_acumulado;//programacion acumulada
                $mat_prog[2][$i] = ($meta == 0) ? 0 : (($prog_acumulado / $meta) * 100);//programacion acumulada en porcentaje
            }
            return $mat_prog;
        } else {
            return 0;
        }

    }

    //verificar si la ejecucion es absoluto o relativo
    function mat_ejec_mes_ogestion($o_id, $meta, $linea_base, $indicador, $denominador, $mat_prog)
    {
        if ($indicador == 1) {
            $matr = $this->mat_ejec_abs_o($o_id, $linea_base, $meta, $mat_prog);
        } else {
            $matr = $this->mat_ejec_rel_o($o_id, $linea_base, $meta, $denominador, $mat_prog);
        }
        return $matr;
    }

    //objetivo de gestion ejecucion mensual de tipo absoluto
    function mat_ejec_abs_o($o_id, $linea_base, $meta, $mat_prog)
    {
        $lista_ejec = $this->malerta_ogestion->ejec_abs_ogestion($o_id);
        if (count($lista_ejec) != 0) {
            $lista_ejec = $this->malerta_ogestion->ejec_abs_ogestion($o_id)[0];
            $ejec_acumulado = $linea_base;
            $mat_ejec[0][0] = 'Ejecución';
            $mat_ejec[1][0] = 'Ejecución Acumulada';
            $mat_ejec[2][0] = 'Ejecución Acumulada Porcentual [%]';
            $mat_ejec[3][0] = 'EFICACIA';
            for ($i = 1; $i <= 12; $i++) {
                $puntero_bd = 'fis' . $i;
                $ejec_fisica = $lista_ejec[$puntero_bd];
                $mat_ejec[0][$i] = $ejec_fisica;//ejecucion fisica
                $ejec_acumulado += $ejec_fisica;
                $mat_ejec[1][$i] = $ejec_acumulado;//ejecucion acumulada
                $mat_ejec[2][$i] = ($meta == 0) ? 0 : (($ejec_acumulado / $meta) * 100);//ejecucion acumulada en porcentaje
                $mat_ejec[3][$i] = ($mat_prog[2][$i] == 0) ? 0 : ($mat_ejec[2][$i] / $mat_prog[2][$i]) * 100;//EFICACIA
            }
            return $mat_ejec;
        } else {
            return 0;
        }

    }

    //objetivo de gestion ejecucion mensual de tipo relativo
    function mat_ejec_rel_o($o_id, $linea_base, $meta, $denominador, $mat_prog)
    {
        $lista_ejec = $this->malerta_ogestion->ejec_rel_ogestion($o_id);
        if (count($lista_ejec) != 0) {
            $lista_ejec = $this->malerta_ogestion->ejec_rel_ogestion($o_id)[0];
            $ejec_acumulado = $linea_base;
            $mat_ejec[0][0] = 'CASO FAVORABLE';
            $mat_ejec[1][0] = 'CASO DESFAVORABLE';
            $mat_ejec[2][0] = 'EJECUCIÓN';
            $mat_ejec[3][0] = 'EJECUCIÓN ACUMUALADA';
            $mat_ejec[4][0] = 'EJECUCIÓN ACUMUALADA PORCENTUAL [%]';
            $mat_ejec[5][0] = 'EFICACIA';
            if ($denominador == 0) {//variable
                for ($i = 1; $i <= 12; $i++) {
                    $punt_fav = 'fav' . $i; //puntero
                    $punt_des = 'des' . $i;
                    $favorable = $lista_ejec[$punt_fav];
                    $desfavorable = $lista_ejec[$punt_des];
                    $mat_ejec[0][$i] = $favorable;
                    $mat_ejec[1][$i] = $desfavorable;
                    $ejec_fisica = ($desfavorable <= 0) ? 0 : ($favorable / $desfavorable) * $mat_prog[0][$i];
                    $mat_ejec[2][$i] = $ejec_fisica;
                    $ejec_acumulado += $ejec_fisica;
                    $mat_ejec[3][$i] = $ejec_acumulado;//ejecucion acumulada
                    $mat_ejec[4][$i] = ($meta == 0) ? 0 : (($ejec_acumulado / $meta) * 100);//ejecucion acumulada en porcentaje
                    $mat_ejec[5][$i] = ($mat_prog[2][$i] == 0) ? 0 : ($mat_ejec[4][$i] / $mat_prog[2][$i]) * 100;//EFICACIA
                }
            } else {//fijo
                for ($i = 1; $i <= 12; $i++) {
                    $punt_fav = 'fav' . $i; //puntero
                    $punt_des = 'des' . $i;
                    $favorable = $lista_ejec[$punt_fav];
                    $desfavorable = $lista_ejec[$punt_des];
                    $mat_ejec[0][$i] = $favorable;
                    $mat_ejec[1][$i] = $desfavorable;
                    $ejec_fisica = ($desfavorable <= 0) ? 0 : ($favorable / $desfavorable) * 100;
                    $mat_ejec[2][$i] = $ejec_fisica;
                    $ejec_acumulado += $ejec_fisica;
                    $mat_ejec[3][$i] = $ejec_acumulado;//ejecucion acumulada
                    $mat_ejec[4][$i] = ($meta == 0) ? 0 : (($ejec_acumulado / $meta) * 100);//ejecucion acumulada en porcentaje
                    $mat_ejec[5][$i] = ($mat_prog[2][$i] == 0) ? 0 : ($mat_ejec[4][$i] / $mat_prog[2][$i]) * 100;//EFICACIA
                }
            }
            return $mat_ejec;
        } else {
            return 0;
        }
    }

    function tabla_eva_pterminal()
    {
        $lista_poa = $this->mejec_ogestion_pterminal->lista_prog();
        $dato['ef_menor'] = '';//vector para el grafico de eficacia
        $dato['ef_entre'] = '';//vector para el grafico de eficacia
        $dato['ef_mayor'] = '';//vector para el grafico de eficacia
        for ($i = 1; $i <= 12; $i++) { //iniciar matriz suma total
            $sum_inst[0][$i] = 0;//total programacion
            $sum_inst[1][$i] = 0;//total ejecucion
        }
        foreach ($lista_poa as $aper_item) {
            $lista_objgestion = $this->mejec_ogestion_pterminal->lista_ogestion($aper_item['poa_id']);
            for ($i = 1; $i <= 12; $i++) { //iniciar matriz  para sumas de prog y ejec del objetivo de gestion
                $o_sum[0][$i] = 0;
                $o_sum[1][$i] = 0;
            }
            foreach ($lista_objgestion as $o_item) {
                $lista_pterminal = $this->mp_terminal->lista_pterminal($o_item['o_id']);
                for ($i = 1; $i <= 12; $i++) { //iniciar matriz
                    $mat[0][$i] = 0;
                    $mat[1][$i] = 0;
                }
                foreach ($lista_pterminal as $row) {
                    $dato_pterminal = $this->mp_terminal->get_pterminal($row['pt_id'])[0];
                    $pt_id = $dato_pterminal['pt_id'];
                    $meta = $dato_pterminal['pt_meta'];
                    $linea_base = $dato_pterminal['pt_linea_base'];
                    $indicador = $dato_pterminal['indi_id'];
                    $denominador = $dato_pterminal['pt_denominador'];
                    $ponderacion = $dato_pterminal['pt_ponderacion'];
                    $mat_prog = $this->mat_prog_mes_pterminal($pt_id, $meta, $linea_base); //matriz de la programacion
                    $mat_ejec = $this->mat_ejec_mes_pterminal($pt_id, $meta, $linea_base, $indicador, $denominador, $mat_prog); //matriz de la ejecucion
                    for ($j = 1; $j <= 12; $j++) {
                        $mat[0][$j] += (count($mat_prog) == 0) ? 0 : ($mat_prog[2][$j] * $ponderacion);
                        $mat[1][$j] += (count($mat_ejec) == 0) ? 0 : ($indicador == 1) ? ($mat_ejec[2][$j] * $ponderacion) : ($mat_ejec[4][$j] * $ponderacion);
                    }
                }
                for ($j = 1; $j <= 12; $j++) { //sumar prog y ejec
                    $o_sum[0][$j] += $mat[0][$j] * $o_item['o_ponderacion'];
                    $o_sum[1][$j] += $mat[1][$j] * $o_item['o_ponderacion'];
                }
            }
            for ($j = 1; $j <= 12; $j++) { //sumar prog y ejec
                $sum_inst[0][$j] += $o_sum[0][$j] * $aper_item['aper_ponderacion'];
                $sum_inst[1][$j] += $o_sum[1][$j] * $aper_item['aper_ponderacion'];
            }
        }

        $dato['tabla'] = '<tr>';
        for ($i = 1; $i <= 12; $i++) {//EFICACIA
            $val = ($sum_inst[0][$i] == 0) ? 0 : ($sum_inst[1][$i] / $sum_inst[0][$i]) * 100;
            $dato['tabla'] .= '<td>' . number_format($val, 2, ',', '.') . '%</td>';
            $data_efi = $this->get_eficacia(round($val));
            $dato['ef_menor'] .= $data_efi['menor'];
            $dato['ef_entre'] .= $data_efi['entre'];
            $dato['ef_mayor'] .= $data_efi['mayor'];
        }
        $dato['tabla'] .= '</tr>';
        return $dato;
    }

    //matriz de la programacion mensual del producto terminal
    function mat_prog_mes_pterminal($pt_id, $meta, $linea_base)
    {
        $lista_prog = $this->malerta_pterminal->prog_mensual_pterminal($pt_id);
        if (count($lista_prog) != 0) {
            $lista_prog = $this->malerta_pterminal->prog_mensual_pterminal($pt_id)[0];
            $prog_acumulado = $linea_base;
            $mat_prog[0][0] = 'Programación';
            $mat_prog[1][0] = 'Programación Acumulada';
            $mat_prog[2][0] = 'Programación Acumulada Porcentual [%]';
            for ($i = 1; $i <= 12; $i++) {
                $puntero_bd = 'fis' . $i;
                $prog_fisica = $lista_prog[$puntero_bd];
                $mat_prog[0][$i] = $prog_fisica;//programacion fisica
                $prog_acumulado += $prog_fisica;
                $mat_prog[1][$i] = $prog_acumulado;//programacion acumulada
                $mat_prog[2][$i] = ($meta == 0) ? 0 : (($prog_acumulado / $meta) * 100);//programacion acumulada en porcentaje
            }
            return $mat_prog;
        } else {
            return 0;
        }

    }

    //verificar si la ejecucion es absoluto o relativo
    function mat_ejec_mes_pterminal($pt_id, $meta, $linea_base, $indicador, $denominador, $mat_prog)
    {
        if ($indicador == 1) {
            $matr = $this->mat_ejec_abs($pt_id, $linea_base, $meta, $mat_prog);
        } else {
            $matr = $this->mat_ejec_rel($pt_id, $linea_base, $meta, $denominador, $mat_prog);
        }
        return $matr;
    }

    //ejecucion mensual de tipo absoluto
    function mat_ejec_abs($pt_id, $linea_base, $meta, $mat_prog)
    {
        $lista_ejec = $this->malerta_pterminal->ejec_abs_pterminal($pt_id);
        if (count($lista_ejec) != 0) {
            $lista_ejec = $this->malerta_pterminal->ejec_abs_pterminal($pt_id)[0];
            $ejec_acumulado = $linea_base;
            $mat_ejec[0][0] = 'Ejecución';
            $mat_ejec[1][0] = 'Ejecución Acumulada';
            $mat_ejec[2][0] = 'Ejecución Acumulada Porcentual [%]';
            $mat_ejec[3][0] = 'EFICACIA';
            for ($i = 1; $i <= 12; $i++) {
                $puntero_bd = 'fis' . $i;
                $ejec_fisica = $lista_ejec[$puntero_bd];
                $mat_ejec[0][$i] = $ejec_fisica;//ejecucion fisica
                $ejec_acumulado += $ejec_fisica;
                $mat_ejec[1][$i] = $ejec_acumulado;//ejecucion acumulada
                $mat_ejec[2][$i] = ($meta == 0) ? 0 : (($ejec_acumulado / $meta) * 100);//ejecucion acumulada en porcentaje
                $mat_ejec[3][$i] = ($mat_prog[2][$i] == 0) ? 0 : ($mat_ejec[2][$i] / $mat_prog[2][$i]) * 100;//EFICACIA
            }
            return $mat_ejec;
        } else {
            return 0;
        }

    }

    //ejecucion mensual de tipo relativo
    function mat_ejec_rel($pt_id, $linea_base, $meta, $denominador, $mat_prog)
    {
        $lista_ejec = $this->malerta_pterminal->ejec_rel_pterminal($pt_id);
        if (count($lista_ejec) != 0) {
            $lista_ejec = $this->malerta_pterminal->ejec_rel_pterminal($pt_id)[0];
            $ejec_acumulado = $linea_base;
            $mat_ejec[0][0] = 'CASO FAVORABLE';
            $mat_ejec[1][0] = 'CASO DESFAVORABLE';
            $mat_ejec[2][0] = 'EJECUCIÓN';
            $mat_ejec[3][0] = 'EJECUCIÓN ACUMUALADA';
            $mat_ejec[4][0] = 'EJECUCIÓN ACUMUALADA PORCENTUAL [%]';
            $mat_ejec[5][0] = 'EFICACIA';
            if ($denominador == 0) {//variable
                for ($i = 1; $i <= 12; $i++) {
                    $punt_fav = 'fav' . $i; //puntero
                    $punt_des = 'des' . $i;
                    $favorable = $lista_ejec[$punt_fav];
                    $desfavorable = $lista_ejec[$punt_des];
                    $mat_ejec[0][$i] = $favorable;
                    $mat_ejec[1][$i] = $desfavorable;
                    $ejec_fisica = ($desfavorable <= 0) ? 0 : ($favorable / $desfavorable) * $mat_prog[0][$i];
                    $mat_ejec[2][$i] = $ejec_fisica;
                    $ejec_acumulado += $ejec_fisica;
                    $mat_ejec[3][$i] = $ejec_acumulado;//ejecucion acumulada
                    $mat_ejec[4][$i] = ($meta == 0) ? 0 : (($ejec_acumulado / $meta) * 100);//ejecucion acumulada en porcentaje
                    $mat_ejec[5][$i] = ($mat_prog[2][$i] == 0) ? 0 : ($mat_ejec[4][$i] / $mat_prog[2][$i]) * 100;//EFICACIA
                }
            } else {//fijo
                for ($i = 1; $i <= 12; $i++) {
                    $punt_fav = 'fav' . $i; //puntero
                    $punt_des = 'des' . $i;
                    $favorable = $lista_ejec[$punt_fav];
                    $desfavorable = $lista_ejec[$punt_des];
                    $mat_ejec[0][$i] = $favorable;
                    $mat_ejec[1][$i] = $desfavorable;
                    $ejec_fisica = ($desfavorable <= 0) ? 0 : ($favorable / $desfavorable) * 100;
                    $mat_ejec[2][$i] = $ejec_fisica;
                    $ejec_acumulado += $ejec_fisica;
                    $mat_ejec[3][$i] = $ejec_acumulado;//ejecucion acumulada
                    $mat_ejec[4][$i] = ($meta == 0) ? 0 : (($ejec_acumulado / $meta) * 100);//ejecucion acumulada en porcentaje
                    $mat_ejec[5][$i] = ($mat_prog[2][$i] == 0) ? 0 : ($mat_ejec[4][$i] / $mat_prog[2][$i]) * 100;//EFICACIA
                }
            }
            return $mat_ejec;
        } else {
            return 0;
        }
    }

    function get_eficacia($efi)
    {
        if ($efi <= 75) {
            $d['menor'] = "{y: " . $efi . ", color: 'red'},";
            $d['entre'] = "{y: 0, color: 'yellow'},";
            $d['mayor'] = "{y: 0, color: 'green'},";
        }
        if ($efi >= 76 && $efi <= 90) {
            $d['entre'] = "{y: " . $efi . ", color: 'yellow'},";
            $d['menor'] = "{y: 0, color: 'red'},";
            $d['mayor'] = "{y: 0, color: 'green'},";
        }
        if ($efi >= 91) {
            $d['mayor'] = "{y: " . $efi . ", color: 'green'},";
            $d['entre'] = "{y: 0, color: 'yellow'},";
            $d['menor'] = "{y: 0, color: 'red'},";
        }
        return $d;
    }

    function mes_nombre(){
        $mes[1] = 'ENERO';
        $mes[2] = 'FEBRERO';
        $mes[3] = 'MARZO';
        $mes[4] = 'ABRIL';
        $mes[5] = 'MAYO';
        $mes[6] = 'JUNIO';
        $mes[7] = 'JULIO';
        $mes[8] = 'AGOSTO';
        $mes[9] = 'SEPTIEMBRE';
        $mes[10] = 'OCTUBRE';
        $mes[11] = 'NOVIEMBRE';
        $mes[12] = 'DICIEMBRE';
        return $mes;
    }

    //FUNCION PARA CONSTRUIR MI VISTA
    function construir_vista($ruta, $data)
    {
        //----------------------------------- MENU-------------------------------
        $menu['enlaces'] = $this->menu->get_enlaces();
        $menu['subenlaces'] = $this->menu->get_sub_enlaces();
        $menu['titulo'] = 'REPORTES';
        //-----------------------------------------------------------------------
        //armar vista
        $this->load->view('includes/header');
        $this->load->view('includes/menu_lateral', $menu);
        $this->load->view($ruta, $data);//contenido
        $this->load->view('includes/footer');
    }
}