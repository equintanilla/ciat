<?php

//reporte global a nivel provincia
class Crep_glob_provincia extends CI_Controller
{
    function __construct()
    {
        parent:: __construct();
        $this->load->model('reportes/reporte_gerencial/mrep_provincia');
        //llamar a mi menu
        $this->load->library('menu');
        $this->menu->const_menu(7);
        $this->gestion = $this->session->userData('gestion');
        $this->mes = $this->session->userData('mes');
    }

    function index()
    {
        $mes_id = $this->session->userData('mes');
        $lista = $this->mrep_provincia->proyecto_inversion($mes_id, $this->gestion);
        $data_tabla = $this->tabla_nivel_proyecto($lista,'#A3CBF2');
        $data['tabla_proy_inv'] = $data_tabla['tabla_proy'];
        $data['pie_proy_inv'] = $data_tabla['pie_proy'];
        $lista = $this->mrep_provincia->programa_no_recurrentes($mes_id, $this->gestion);
        $data_tabla = $this->tabla_nivel_proyecto($lista,'#B1F2A4');
        $data['tabla_prog_norec'] = $data_tabla['tabla_proy'];
        $data['pie_prog_norec'] = $data_tabla['pie_proy'];
        $lista = $this->mrep_provincia->reporte_pi_pnr($mes_id, $this->gestion);
        $data_tabla = $this->tabla_nivel_proyecto($lista,'#E7BADE');
        $data['tabla_pi_pnr'] = $data_tabla['tabla_proy'];
        $data['pie_pi_pnr'] = $data_tabla['pie_proy'];
        $ruta = 'reportes/reportes_gerenciales/reporte_global/vpor_provincia';
        $this->construir_vista($ruta, $data);
    }

    //tabla por el tipo de proyecto
    function tabla_nivel_proyecto($lista, $color)
    {
        $mat['tabla_proy'] = '';
        $mat['pie_proy'] = '';
        $total_ppto_vigente = 0;
        foreach ($lista as $item) {
            $total_ppto_vigente += $item['ppto_vigente'];
        }
        $sum_inv = 0;
        $sum_pinv = 0;
        $sum_total = 0;
        $sum_pres_vig = 0;
        $sum_por = 0;
        $sum_ppto_ejec = 0;
        foreach ($lista as $item) {
            $mat['tabla_proy'] .= '<tr>';
            $mat['tabla_proy'] .= '<td class="descripcion">' . $item['prov_provincia'] . '</td>';
            $mat['tabla_proy'] .= '<td>' . $item['cant2'] . '</td>';
            $mat['tabla_proy'] .= '<td>' . $item['cant1'] . '</td>';
            $mat['tabla_proy'] .= '<td>' . ($item['cant2'] + $item['cant1']) . '</td>';
            $mat['tabla_proy'] .= '<td>' . number_format($item['ppto_vigente'], 2, '.', ' ') . '</td>';
            $porc = ($total_ppto_vigente == 0) ? 0 : number_format((($item['ppto_vigente'] / $total_ppto_vigente) * 100), 2, '.', '');
            $mat['tabla_proy'] .= '<td>' . $porc . '%</td>';
            $mat['tabla_proy'] .= '<td>' . number_format($item['devengado'], 2, ',', '.') . '</td>';
            $mat['tabla_proy'] .= '</tr>';
            //para los reportes graficos
            $mat['pie_proy'] .= "['" . $item['prov_provincia'] . "'," . $porc . "],";
            $sum_total += ($item['cant2'] + $item['cant1']);
            $sum_pres_vig += $item['ppto_vigente'];
            $sum_por += $porc;
            $sum_ppto_ejec += $item['devengado'];
            $sum_inv += $item['cant2'];
            $sum_pinv += $item['cant1'];
        }
        $mat['tabla_proy'] .= '<tr bgcolor="'.$color.'">';
        $mat['tabla_proy'] .= '<td style="text-align: center">TOTAL</td>';
        $mat['tabla_proy'] .= '<td>' . $sum_inv . '</td>';
        $mat['tabla_proy'] .= '<td>' . $sum_pinv . '</td>';
        $mat['tabla_proy'] .= '<td>' . $sum_total . '</td>';
        $mat['tabla_proy'] .= '<td>' . number_format($sum_pres_vig, 2, ',', '.') . '</td>';
        $mat['tabla_proy'] .= '<td>' . number_format($sum_por, 2, '.', '' ). '%</td>';
        $mat['tabla_proy'] .= '<td>' . number_format($sum_ppto_ejec, 2, ',', '.') . '</td>';
        $mat['tabla_proy'] .= '</tr>';
        return $mat;
    }

    function construir_vista($ruta, $data)
    {
        //----------------------------------- MENU-------------------------------
        $menu['enlaces'] = $this->menu->get_enlaces();
        $menu['subenlaces'] = $this->menu->get_sub_enlaces();
        $menu['titulo'] = 'REPORTES';
        //-----------------------------------------------------------------------
        //armar vista
        $this->load->view('includes/header');
        $this->load->view('includes/menu_lateral', $menu);
        $this->load->view($ruta, $data);//contenido
        $this->load->view('includes/footer');
    }
}