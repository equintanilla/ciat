<?php
define("DOMPDF_ENABLE_REMOTE", true);
define("DOMPDF_TEMP_DIR", "/tmp");
class Reporte_ogestion extends CI_Controller { 
  public function __construct ()
    { 
        parent::__construct();
        if($this->session->userdata('fun_id')!=null){
        $this->load->library('pdf');
        $this->load->library('pdf2');

        $this->load->model('reportes/model_objetivo');
        $this->load->model('reportes/seguimiento/model_reporte_seguimiento');

        $this->load->model('menu_modelo');
        $this->load->model('Users_model','',true);
        }else{
            redirect('/','refresh');
        }
    }
        /*============================== FICHA TECNICA CABECERA =================================*/
       private $estilo_vertical = '<style>
        body{
            font-family: sans-serif;
            }
        @page {
        margin: 40px 40px;
        }
        header {
            position: fixed;
            left: 0px;
            top: -20px;
            right: 0px;
            height: 20px;
            background-color: #fff;
            }
        header h1{
            margin: 5px 0;
        }
        header h2{
            margin: 0 0 10px 0;
        }
        th{
                padding: 1px;
                text-align: center;
                font-size: 9px;
                color: #ffffff;
            }
        footer {
            position: fixed;
            left: 0px;
            bottom: -82px;
            right: 0px;
            height: 100px;
            border-bottom: 1px solid #ddd;
        }
        footer .page:after {
            content: counter(page) ;
        }
        footer table {
            width: 100%;
        }
        footer p {
            text-align: right;
        }
        footer .izq {
            text-align: left;
        }
        table{
            font-size: 7px;
            width: 100%;
            background-color:#fff;
        }
        th, td {
                padding: 1px;
                text-align: center;
                font-size: 7px;
            }
        .mv{font-size:10px;}
        .siipp{width:200px;}
        .header_table {
            color: #ffffff;
            text-align: center;
            align-items: center;
            align-self: center;
            font-weight: bold;
            background-color: #696969;
            align-content: center;
        }
        .header_subtable {
            color: #ffffff;
            text-align: center;
            align-items: center;
            align-self: center;
            font-weight: bold;
            background-color: #a59393;
            align-content: center;
        }
        .titulo_pdf {
            text-align: left;
            font-size: 10px;
        }
        .datos_principales {
            text-align: center;
            font-size: 9px;
        }
        .pdes_titulo{
            text-decoration: underline;
            font-weight: bold;
            text-transform: uppercase;
        }
        .td_pdes{
            text-align: left;  
        }

        .indi_desemp{
            font-size: 14px;
            border: 1px solid #CCC;
            background-color: #E0E0E0;
            padding: .5em;
        }
        table.fixed { table-layout:fixed; }
        table.fixed td { overflow: hidden; }
        .sub_table{
            border-bottom:1px solid black;
        }
        .contenedor_principal{
            border-style: solid;
            border-width:1px;
        }
        .titulo_dictamen{
            padding:4px;
            background-color: #454545;
            border-style: solid;margin: 2px;
            border-width: 1px;
            color: white;
            font-size: 10px;
            border-color: black;
        }
        .contenedor_datos{
            padding:0px;
            margin:1px;
        }
        .table_contenedor{
            border-style: solid;
            margin: 0px;
            padding: 1px;
            border-width: 1px;
            border-collapse: collapse;
        }
        .collapse_t td{
            border-style: solid;
            margin: 0px;
            padding: 3px;
            border-width: 1px;
            border-collapse: collapse;
        }
        .fila_unitaria{
            text-align:left;
        }
        .lista{
            text-align:left;
            padding-left: 8;
            list-style-type:square;
            margin:2px;
        }
    </style>';
 
    /*=========================================================================================================================*/
    /*================================================= OBJETIVOS DE GESTION =================================================*/
    /*---------------------------------------------------- MENU OBJETIVOS DE GESTION -------------------------------------*/
    public function objetivos_gestion()
    { 
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++)
        {
             $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        
        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];

        $data['programas'] = $this->model_reporte_seguimiento->list_programas(); ///// lista de programas
        
        $this->load->view('admin/reportes/seguimiento/objetivos_gestion/menu_ogestion', $data);
    }
    /*---------------------------------------------------- OBJETIVOS DE GESTION A NIVEL INSTITUCION -------------------------------------*/
    public function objetivos_gestion_institucional()
    { 
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++)
        {
             $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        
        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];

        $nro_og=0;
        $programas = $this->model_reporte_seguimiento->list_programas(); ///// lista de programas
        for($j = 1; $j<=12; $j++){$poa_p[$j]=0;$poa_e[$j]=0;$poa_efi[$j]=0;$efi_menor[$j]=0;$efi_entre[$j]=0;$efi_mayor[$j]=0;} //// vector programas programado , Ejecutado vacio
        foreach ($programas as $rowp) 
        {
            if($rowp['ejecutado']!=0)
            {
                //echo $rowp['aper_descripcion'].'<br>';
                $ogestion = $this->model_reporte_seguimiento->list_ogestion($rowp['poa_id']); ///// lista de ogestion
                for($j = 1; $j<=12; $j++){$og_p[$j]=0;$og_e[$j]=0;} //// vector Objetivo de Gestion programado , Ejecutado vacio
                foreach ($ogestion as $rowo) 
                {
                   //echo $rowo['o_objetivo'].'<br>';
                    $programado=$this->model_reporte_seguimiento->ogestion_programado($rowo['o_id']); /// programado
                    $nro=0;
                    foreach($programado as $row)
                    {
                      $nro++;
                      $matriz [1][$nro]=$row['mes_id'];
                      $matriz [2][$nro]=$row['opm_fis'];
                    }
                     /*---------------- llenando la matriz vacia --------------*/
                    for($j = 1; $j<=12; $j++)
                    {
                      $matriz_r[1][$j]=$j;
                      $matriz_r[2][$j]='0';
                      $matriz_r[3][$j]='0';
                      $matriz_r[4][$j]='0';
                      $matriz_r[5][$j]='0';
                      $matriz_r[6][$j]='0';
                      $matriz_r[7][$j]='0';
                      $matriz_r[8][$j]='0';
                    }
                    /*--------------------------------------------------------*/
                    /*--------------------- asignando en la matriz P, PA, %PA ----------------------*/
                      for($i = 1 ;$i<=$nro ;$i++)
                      {
                        for($j = 1 ;$j<=12 ;$j++)
                        {
                          if($matriz[1][$i]==$matriz_r[1][$j])
                          {
                            $matriz_r[2][$j]=round($matriz[2][$i],2);
                          }
                        }
                      }

                    $pa=0;
                    for($j = 1 ;$j<=12 ;$j++){
                        $pa=$pa+$matriz_r[2][$j];
                        $matriz_r[3][$j]=$pa+$rowo['o_linea_base'];
                        if($rowo['o_meta']!=0)
                        {
                            $matriz_r[4][$j]=round(((($pa+$rowo['o_linea_base'])/$rowo['o_meta'])*100),2);
                        }
                        
                      }
                    /*--------------------------------------------------------------------------------*/
                    /*-------------------------------- EJECUTADO -----------------------------------*/
                    if($rowo['indi_id']==1){
                        $ejecutado=$this->model_reporte_seguimiento->ogestion_ejecutado_absoluto($rowo['o_id']); /// ejecutado abs
                        $nro_e=0;
                        foreach($ejecutado as $row)
                        {
                            $nro_e++;
                            $matriz_e [1][$nro_e]=$row['mes_id'];
                            $matriz_e [2][$nro_e]=$row['oem_fis'];
                        }
                    }
                    else{
                        $ejecutado=$this->model_reporte_seguimiento->ogestion_ejecutado_relativo($rowo['o_id']); /// ejecutado rel
                        $nro_e=0;
                        foreach($ejecutado as $row)
                        {
                            $nro_e++;
                            if($rowo['o_denominador']==0){
                                $valor=(($row['oer_desfavorable']/$row['oer_favorable'])*$matriz_r[2][$nro_e]);
                            }
                            else{
                                $valor=(($row['oer_desfavorable']/$row['oer_favorable'])*100);
                            }
                            
                            $matriz_e [1][$nro_e]=$row['mes_id'];
                            $matriz_e [2][$nro_e]=$valor;
                        }
                    }
                    /*--------------------matriz E,AE,%AE gestion ------------------*/
                        for($i = 1 ;$i<=$nro_e ;$i++){
                          for($j = 1 ;$j<=12 ;$j++)
                          {
                            if($matriz_e[1][$i]==$matriz_r[1][$j])
                            {
                              $matriz_r[5][$j]=round($matriz_e[2][$i],1);
                            }
                          }
                        }

                      $pe=0;
                      for($j = 1 ;$j<=12 ;$j++){
                        $pe=$pe+$matriz_r[5][$j];
                        $matriz_r[6][$j]=$pe+$rowo['o_linea_base'];
                        if($rowo['o_meta'])
                        {
                           $matriz_r[7][$j]=round(((($pe+$rowo['o_linea_base'])/$rowo['o_meta'])*100),2); 
                        }
                        
                        if($matriz_r[4][$j]!=0)
                        {$matriz_r[10][$j]=round((($matriz_r[7][$j]/$matriz_r[4][$j])*100),1);}
                    }
                    /*------------------------------------------------------------------------------*/
/*                      for($i = 1 ;$i<=7 ;$i++)
                      {
                        for($j = 1 ;$j<=12 ;$j++)
                        {
                          echo "[".$matriz_r[$i][$j]."]";
                        }
                        echo "<br>";
                      }*/

                    for($i = 1 ;$i<=12 ;$i++)
                    {
                        $og_p[$i]=$og_p[$i]+(($matriz_r[4][$i]*$rowo['o_ponderacion'])/100);
                        $og_e[$i]=$og_e[$i]+(($matriz_r[7][$i]*$rowo['o_ponderacion'])/100);
                    }
                }

/*                for($i = 1 ;$i<=12 ;$i++)
                    {
                        echo "[".$og_p[$i]."]";
                    }*/

                for($i = 1 ;$i<=12 ;$i++)
                    {
                        $poa_p[$i]=round(($poa_p[$i]+(($og_p[$i]*$rowp['aper_ponderacion'])/100)),2);
                        $poa_e[$i]=round(($poa_e[$i]+(($og_e[$i]*$rowp['aper_ponderacion'])/100)),2);
                    }
            $nro_og++;
            }
            
        }

        /*------------------ eficacia ------------------*/
        for($i = 1 ;$i<=12 ;$i++)
        {
            if($poa_e[$i]!=0)
            {$poa_efi[$i]=round((($poa_e[$i]/$poa_p[$i])*100),2);}

            if($poa_efi[$i]<=75){$efi_menor[$i] = $poa_efi[$i];}
            if ($poa_efi[$i] >= 76 && $poa_efi[$i] <= 90.9) {$efi_entre[$i] = $poa_efi[$i];}
            if($poa_efi[$i] >= 91){$efi_mayor[$i] = $poa_efi[$i];}
        }


/*                    for($i = 1 ;$i<=12 ;$i++)
                    {
                        echo "[".$poa_p[$i]."]";
                    }
                    echo "<br>";
                    for($i = 1 ;$i<=12 ;$i++)
                    {
                        echo "[".$poa_e[$i]."]";
                    }
                    echo "<br>";
                    for($i = 1 ;$i<=12 ;$i++)
                    {
                        echo "[".$poa_efi[$i]."]";
                    }*/
        $data['prog'] = $poa_p;
        $data['ejec'] = $poa_e;
        $data['efi'] = $poa_efi;
        $data['menor'] = $efi_menor;
        $data['entre'] = $efi_entre;
        $data['mayor'] = $efi_mayor;
        $data['nro_og'] = $nro_og;
        $this->load->view('admin/reportes/seguimiento/objetivos_gestion/ogestion1', $data);
    }
    /*----------------------------------- detalle institucion ---------------------------------*/
    public function objetivos_gestion_institucional_detalles()
    { 
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++)
        {
             $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        
        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];

        $tabla = '';
        $nro_og=1;
        $programas = $this->model_reporte_seguimiento->list_programas(); ///// lista de programas
        foreach ($programas as $rowp) 
        {
            if($rowp['ejecutado']!=0)
            {
                $tabla .= '<tr bgcolor=#bdd8ef>';
                    $tabla .= '<td>'.$nro_og.'</td>';
                    $tabla .= '<td>'.$rowp['aper_programa'].''.$rowp['aper_proyecto'].''.$rowp['aper_actividad'].'</td>';
                    $tabla .= '<td>'.$rowp['aper_descripcion'].'</td>';
                    $tabla .= '<td>'.$rowp['uni_unidad'].'</td>';
                    $tabla .= '<td>'.$rowp['aper_ponderacion'].' %</td>';
                    
                $tabla .= '<td>';
                $tabla .= '<table  class="table table table-bordered" width="100%">';
                $tabla .= '<tr bgcolor=#474544>';
                    $tabla .= '<th>Nro.</th>';
                    $tabla .= '<th>OBJETIVO</th>';
                    $tabla .= '<th>TIPO DE INDICADOR</th>';
                    $tabla .= '<th>PONDERACI&Oacute;N</th>';
                    $tabla .= '<th>CRONOGRAMA DE EJECUCI&Oacute;N</th>';
                $tabla .= '</tr>';
                $nro_obj=1;
                $ogestion = $this->model_reporte_seguimiento->list_ogestion($rowp['poa_id']); ///// lista de ogestion
                foreach ($ogestion as $rowo) 
                {
                   $tabla .= '<tr>';
                    $tabla .= '<td>'.$nro_obj.'</td>';
                    $tabla .= '<td>'.$rowo['o_objetivo'].'</td>';
                    $tabla .= '<td>'.$rowo['indi_descripcion'].'</td>';
                    $tabla .= '<td>'.$rowo['o_ponderacion'].'</td>';
                    $tabla .= '<td>';
                        $tabla .= '<table  class="table table table-bordered" width="100%">';
                        $tabla .= '<tr bgcolor=#474544>';
                            $tabla .= '<th>P/E</th>';
                            $tabla .= '<th>ENERO</th>';
                            $tabla .= '<th>FEBRERO</th>';
                            $tabla .= '<th>MARZO</th>';
                            $tabla .= '<th>ABRIL</th>';
                            $tabla .= '<th>MAYO</th>';
                            $tabla .= '<th>JUNIO</th>';
                            $tabla .= '<th>JULIO</th>';
                            $tabla .= '<th>AGOSTO</th>';
                            $tabla .= '<th>SEPTIEMBRE</th>';
                            $tabla .= '<th>OCTUBRE</th>';
                            $tabla .= '<th>NOVIEMBRE</th>';
                            $tabla .= '<th>DICIEMBRE</th>';
                        $tabla .= '</tr>';

                    $programado=$this->model_reporte_seguimiento->ogestion_programado($rowo['o_id']); /// programado
                    $nro=0;
                    foreach($programado as $row)
                    {
                      $nro++;
                      $matriz [1][$nro]=$row['mes_id'];
                      $matriz [2][$nro]=$row['opm_fis'];
                    }
                     /*---------------- llenando la matriz vacia --------------*/
                    for($j = 1; $j<=12; $j++)
                    {
                      $matriz_r[1][$j]=$j;
                      $matriz_r[2][$j]='0';
                      $matriz_r[3][$j]='0';
                      $matriz_r[4][$j]='0';
                      $matriz_r[5][$j]='0';
                      $matriz_r[6][$j]='0';
                      $matriz_r[7][$j]='0';
                      $matriz_r[8][$j]='0';
                    }
                    /*--------------------------------------------------------*/
                    /*--------------------- asignando en la matriz P, PA, %PA ----------------------*/
                      for($i = 1 ;$i<=$nro ;$i++)
                      {
                        for($j = 1 ;$j<=12 ;$j++)
                        {
                          if($matriz[1][$i]==$matriz_r[1][$j])
                          {
                            $matriz_r[2][$j]=round($matriz[2][$i],2);
                          }
                        }
                      }

                    $pa=0;
                    for($j = 1 ;$j<=12 ;$j++){
                        $pa=$pa+$matriz_r[2][$j];
                        $matriz_r[3][$j]=$pa+$rowo['o_linea_base'];
                        $matriz_r[4][$j]=round(((($pa+$rowo['o_linea_base'])/$rowo['o_meta'])*100),2);
                      }
                    /*--------------------------------------------------------------------------------*/
                    /*-------------------------------- EJECUTADO -----------------------------------*/
                    if($rowo['indi_id']==1){
                        $ejecutado=$this->model_reporte_seguimiento->ogestion_ejecutado_absoluto($rowo['o_id']); /// ejecutado abs
                        $nro_e=0;
                        foreach($ejecutado as $row)
                        {
                            $nro_e++;
                            $matriz_e [1][$nro_e]=$row['mes_id'];
                            $matriz_e [2][$nro_e]=$row['oem_fis'];
                        }
                    }
                    else{
                        $ejecutado=$this->model_reporte_seguimiento->ogestion_ejecutado_relativo($rowo['o_id']); /// ejecutado rel
                        $nro_e=0;
                        foreach($ejecutado as $row)
                        {
                            $nro_e++;
                            if($rowo['o_denominador']==0){
                                $valor=(($row['oer_desfavorable']/$row['oer_favorable'])*$matriz_r[2][$nro_e]);
                            }
                            else{
                                $valor=(($row['oer_desfavorable']/$row['oer_favorable'])*100);
                            }
                            
                            $matriz_e [1][$nro_e]=$row['mes_id'];
                            $matriz_e [2][$nro_e]=$valor;
                        }
                    }
                    /*--------------------matriz E,AE,%AE gestion ------------------*/
                        for($i = 1 ;$i<=$nro_e ;$i++){
                          for($j = 1 ;$j<=12 ;$j++)
                          {
                            if($matriz_e[1][$i]==$matriz_r[1][$j])
                            {
                              $matriz_r[5][$j]=round($matriz_e[2][$i],1);
                            }
                          }
                        }

                      $pe=0;
                      for($j = 1 ;$j<=12 ;$j++){
                        $pe=$pe+$matriz_r[5][$j];
                        $matriz_r[6][$j]=$pe+$rowo['o_linea_base'];
                        $matriz_r[7][$j]=round(((($pe+$rowo['o_linea_base'])/$rowo['o_meta'])*100),2);
                        if($matriz_r[4][$j]!=0)
                        {$matriz_r[8][$j]=round((($matriz_r[7][$j]/$matriz_r[4][$j])*100),1);}
                    }
                    /*------------------------------------------------------------------------------*/
                      
                    $tabla .= '<tr>';
                    $tabla .= '<td>PROGRAMADO</td>';
                    for($i = 1 ;$i<=12 ;$i++)
                      {
                        $tabla .= '<td>'.$matriz_r[2][$i].'</td>';
                      }
                    $tabla .= '</tr>';

                    $tabla .= '<tr>';
                    $tabla .= '<td>PROGRAMADO ACUMULADO</td>';
                    for($i = 1 ;$i<=12 ;$i++)
                      {
                        $tabla .= '<td>'.$matriz_r[3][$i].'</td>';
                      }
                    $tabla .= '</tr>';
                    $tabla .= '<tr bgcolor=#d6e4f1>';
                    $tabla .= '<td>PROGRAMADO ACUMULADO %</td>';
                    for($i = 1 ;$i<=12 ;$i++)
                      {
                        $tabla .= '<td>'.$matriz_r[4][$i].'%</td>';
                      }
                    $tabla .= '</tr>';
                    $tabla .= '<tr>';
                    $tabla .= '<td>EJECUTADO</td>';
                    for($i = 1 ;$i<=12 ;$i++)
                      {
                        $tabla .= '<td>'.$matriz_r[5][$i].'%</td>';
                      }
                    $tabla .= '</tr>';
                    $tabla .= '<tr>';
                    $tabla .= '<td>EJECUTADO ACUMULADO</td>';
                    for($i = 1 ;$i<=12 ;$i++)
                      {
                        $tabla .= '<td>'.$matriz_r[6][$i].'%</td>';
                      }
                    $tabla .= '</tr>';
                    $tabla .= '<tr bgcolor=#d6e4f1>';
                    $tabla .= '<td>EJECUTADO ACUMULADO %</td>';
                    for($i = 1 ;$i<=12 ;$i++)
                      {
                        $tabla .= '<td>'.$matriz_r[7][$i].'%</td>';
                      }
                    $tabla .= '</tr>';
                    $tabla .= '<tr>';
                    $tabla .= '<td>EFICACIA</td>';
                    for($i = 1 ;$i<=12 ;$i++)
                      {
                        $tabla .= '<td>'.$matriz_r[8][$i].'%</td>';
                      }
                    $tabla .= '</tr>';
                      
                
                    $tabla .= '</table>';
                $tabla .= '</td>';
                $nro_obj++;
                }
            $nro_og++;
            $tabla .= '</tr>';
            $tabla .= '</table>';
            $tabla .= '</td>';
            $tabla .= '</tr>';
            }
            
        }

        $data['togestion'] = $tabla;
        $this->load->view('admin/reportes/seguimiento/objetivos_gestion/ogestion1_detalle', $data);
    }

    /*---------------------------------------------------- OBJETIVOS DE GESTION A NIVEL PROGRAMAS -------------------------------------*/
    public function objetivos_gestion_programa($poa_id)
    { 
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++)
        {
             $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        
        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];

        $data['poa_id'] = $poa_id;
        $data['programa']=$this->model_reporte_seguimiento->get_programa($poa_id); /// get programa

        $ogestion = $this->model_reporte_seguimiento->list_ogestion($poa_id); ///// lista de ogestion
        for($j = 1; $j<=12; $j++){$og_p[$j]=0;$og_p2[$j]=0;$og_p3[$j]=0;$og_e[$j]=0;$og_efi[$j]=0;$efi_menor[$j]=0;$efi_entre[$j]=0;$efi_mayor[$j]=0;} //// vector Objetivo de Gestion programado , Ejecutado vacio
        $nro_og=0;
        foreach ($ogestion as $rowo) 
        {
            $programado=$this->model_reporte_seguimiento->ogestion_programado($rowo['o_id']); /// programado
            $nro=0;
            foreach($programado as $row)
            {
              $nro++;
              $matriz [1][$nro]=$row['mes_id'];
              $matriz [2][$nro]=$row['opm_fis'];
            }
             /*---------------- llenando la matriz vacia --------------*/
            for($j = 1; $j<=12; $j++)
            {
              $matriz_r[1][$j]=$j;
              $matriz_r[2][$j]='0';
              $matriz_r[3][$j]='0';
              $matriz_r[4][$j]='0';
              $matriz_r[5][$j]='0';
              $matriz_r[6][$j]='0';
              $matriz_r[7][$j]='0';
              $matriz_r[8][$j]='0';
            }
            /*--------------------------------------------------------*/
            /*--------------------- asignando en la matriz P, PA, %PA ----------------------*/
              for($i = 1 ;$i<=$nro ;$i++)
              {
                for($j = 1 ;$j<=12 ;$j++)
                {
                  if($matriz[1][$i]==$matriz_r[1][$j])
                  {
                    $matriz_r[2][$j]=round($matriz[2][$i],2);
                  }
                }
              }

            $pa=0;
            for($j = 1 ;$j<=12 ;$j++){
                $pa=$pa+$matriz_r[2][$j];
                $matriz_r[3][$j]=$pa+$rowo['o_linea_base'];
                $matriz_r[4][$j]=round(((($pa+$rowo['o_linea_base'])/$rowo['o_meta'])*100),2);
              }
            /*--------------------------------------------------------------------------------*/
            /*-------------------------------- EJECUTADO -----------------------------------*/
            if($rowo['indi_id']==1){
                $ejecutado=$this->model_reporte_seguimiento->ogestion_ejecutado_absoluto($rowo['o_id']); /// ejecutado abs
                $nro_e=0;
                foreach($ejecutado as $row)
                {
                    $nro_e++;
                    $matriz_e [1][$nro_e]=$row['mes_id'];
                    $matriz_e [2][$nro_e]=$row['oem_fis'];
                }
            }
            else{
                $ejecutado=$this->model_reporte_seguimiento->ogestion_ejecutado_relativo($rowo['o_id']); /// ejecutado rel
                $nro_e=0;
                foreach($ejecutado as $row)
                {
                    $nro_e++;
                    if($rowo['o_denominador']==0){
                        $valor=(($row['oer_desfavorable']/$row['oer_favorable'])*$matriz_r[2][$nro_e]);
                    }
                    else{
                        $valor=(($row['oer_desfavorable']/$row['oer_favorable'])*100);
                    }
                    
                    $matriz_e [1][$nro_e]=$row['mes_id'];
                    $matriz_e [2][$nro_e]=$valor;
                }
            }
            /*--------------------matriz E,AE,%AE gestion ------------------*/
                for($i = 1 ;$i<=$nro_e ;$i++){
                  for($j = 1 ;$j<=12 ;$j++)
                  {
                    if($matriz_e[1][$i]==$matriz_r[1][$j])
                    {
                      $matriz_r[5][$j]=round($matriz_e[2][$i],1);
                    }
                  }
                }

              $pe=0;
              for($j = 1 ;$j<=12 ;$j++){
                $pe=$pe+$matriz_r[5][$j];
                $matriz_r[6][$j]=$pe+$rowo['o_linea_base'];
                $matriz_r[7][$j]=round(((($pe+$rowo['o_linea_base'])/$rowo['o_meta'])*100),2);
                if($matriz_r[4][$j]!=0)
                {$matriz_r[8][$j]=round((($matriz_r[7][$j]/$matriz_r[4][$j])*100),1);}
            }
            /*------------------------------------------------------------------------------*/
            for($i = 1 ;$i<=12 ;$i++)
            {
                $og_p[$i]=$og_p[$i]+(($matriz_r[4][$i]*$rowo['o_ponderacion'])/100);
                $og_e[$i]=$og_e[$i]+(($matriz_r[7][$i]*$rowo['o_ponderacion'])/100);
            }

            $nro_og++;
        }

        /*------------------ eficacia ------------------*/
        for($i = 1 ;$i<=12 ;$i++)
        {
            if($og_e[$i]!=0)
            {$og_efi[$i]=round((($og_e[$i]/$og_p[$i])*100),2);}

            if($og_efi[$i]<=75){$efi_menor[$i] = $og_efi[$i];}
            if ($og_efi[$i] >= 76 && $og_efi[$i] <= 90.9) {$efi_entre[$i] = $og_efi[$i];}
            if($og_efi[$i] >= 91){$efi_mayor[$i] = $og_efi[$i];}
        }

        $data['prog'] = $og_p;
        $data['ejec'] = $og_e;
        $data['efi'] = $og_efi;
        $data['menor'] = $efi_menor;
        $data['entre'] = $efi_entre;
        $data['mayor'] = $efi_mayor;
        $data['nro_og'] = $nro_og;
        
        $this->load->view('admin/reportes/seguimiento/objetivos_gestion/ogestion2', $data);
    }
    /*----------------------------------- detalle programa ---------------------------------*/
    public function objetivos_gestion_programa_detalles($poa_id)
    { 
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++)
        {
             $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        
        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];

        $data['poa_id'] = $poa_id;
        $data['programa']=$this->model_reporte_seguimiento->get_programa($poa_id); /// get programa

        $tabla = '';
                $nro_obj=1;
                $ogestion = $this->model_reporte_seguimiento->list_ogestion($poa_id); ///// lista de ogestion
                foreach ($ogestion as $rowo) 
                {
                   $tabla .= '<tr>';
                    $tabla .= '<td>'.$nro_obj.'</td>';
                    $tabla .= '<td>'.$rowo['o_objetivo'].'</td>';
                    $tabla .= '<td>'.$rowo['indi_descripcion'].'</td>';
                    $tabla .= '<td>'.$rowo['o_ponderacion'].' %</td>';
                    $tabla .= '<td>';
                        $tabla .= '<table  class="table table table-bordered" width="100%">';
                        $tabla .= '<tr bgcolor=#474544>';
                            $tabla .= '<th>P/E</th>';
                            $tabla .= '<th>ENERO</th>';
                            $tabla .= '<th>FEBRERO</th>';
                            $tabla .= '<th>MARZO</th>';
                            $tabla .= '<th>ABRIL</th>';
                            $tabla .= '<th>MAYO</th>';
                            $tabla .= '<th>JUNIO</th>';
                            $tabla .= '<th>JULIO</th>';
                            $tabla .= '<th>AGOSTO</th>';
                            $tabla .= '<th>SEPTIEMBRE</th>';
                            $tabla .= '<th>OCTUBRE</th>';
                            $tabla .= '<th>NOVIEMBRE</th>';
                            $tabla .= '<th>DICIEMBRE</th>';
                        $tabla .= '</tr>';

                    $programado=$this->model_reporte_seguimiento->ogestion_programado($rowo['o_id']); /// programado
                    $nro=0;
                    foreach($programado as $row)
                    {
                      $nro++;
                      $matriz [1][$nro]=$row['mes_id'];
                      $matriz [2][$nro]=$row['opm_fis'];
                    }
                     /*---------------- llenando la matriz vacia --------------*/
                    for($j = 1; $j<=12; $j++)
                    {
                      $matriz_r[1][$j]=$j;
                      $matriz_r[2][$j]='0';
                      $matriz_r[3][$j]='0';
                      $matriz_r[4][$j]='0';
                      $matriz_r[5][$j]='0';
                      $matriz_r[6][$j]='0';
                      $matriz_r[7][$j]='0';
                      $matriz_r[8][$j]='0';
                    }
                    /*--------------------------------------------------------*/
                    /*--------------------- asignando en la matriz P, PA, %PA ----------------------*/
                      for($i = 1 ;$i<=$nro ;$i++)
                      {
                        for($j = 1 ;$j<=12 ;$j++)
                        {
                          if($matriz[1][$i]==$matriz_r[1][$j])
                          {
                            $matriz_r[2][$j]=round($matriz[2][$i],2);
                          }
                        }
                      }

                    $pa=0;
                    for($j = 1 ;$j<=12 ;$j++){
                        $pa=$pa+$matriz_r[2][$j];
                        $matriz_r[3][$j]=$pa+$rowo['o_linea_base'];
                        $matriz_r[4][$j]=round(((($pa+$rowo['o_linea_base'])/$rowo['o_meta'])*100),2);
                      }
                    /*--------------------------------------------------------------------------------*/
                    /*-------------------------------- EJECUTADO -----------------------------------*/
                    if($rowo['indi_id']==1){
                        $ejecutado=$this->model_reporte_seguimiento->ogestion_ejecutado_absoluto($rowo['o_id']); /// ejecutado abs
                        $nro_e=0;
                        foreach($ejecutado as $row)
                        {
                            $nro_e++;
                            $matriz_e [1][$nro_e]=$row['mes_id'];
                            $matriz_e [2][$nro_e]=$row['oem_fis'];
                        }
                    }
                    else{
                        $ejecutado=$this->model_reporte_seguimiento->ogestion_ejecutado_relativo($rowo['o_id']); /// ejecutado rel
                        $nro_e=0;
                        foreach($ejecutado as $row)
                        {
                            $nro_e++;
                            if($rowo['o_denominador']==0){
                                $valor=(($row['oer_desfavorable']/$row['oer_favorable'])*$matriz_r[2][$nro_e]);
                            }
                            else{
                                $valor=(($row['oer_desfavorable']/$row['oer_favorable'])*100);
                            }
                            
                            $matriz_e [1][$nro_e]=$row['mes_id'];
                            $matriz_e [2][$nro_e]=$valor;
                        }
                    }
                    /*--------------------matriz E,AE,%AE gestion ------------------*/
                        for($i = 1 ;$i<=$nro_e ;$i++){
                          for($j = 1 ;$j<=12 ;$j++)
                          {
                            if($matriz_e[1][$i]==$matriz_r[1][$j])
                            {
                              $matriz_r[5][$j]=round($matriz_e[2][$i],1);
                            }
                          }
                        }

                      $pe=0;
                      for($j = 1 ;$j<=12 ;$j++){
                        $pe=$pe+$matriz_r[5][$j];
                        $matriz_r[6][$j]=$pe+$rowo['o_linea_base'];
                        $matriz_r[7][$j]=round(((($pe+$rowo['o_linea_base'])/$rowo['o_meta'])*100),2);
                        if($matriz_r[4][$j]!=0)
                        {$matriz_r[8][$j]=round((($matriz_r[7][$j]/$matriz_r[4][$j])*100),1);}
                    }
                    /*------------------------------------------------------------------------------*/
                      
                    $tabla .= '<tr>';
                    $tabla .= '<td>PROGRAMADO</td>';
                    for($i = 1 ;$i<=12 ;$i++)
                      {
                        $tabla .= '<td>'.$matriz_r[2][$i].'</td>';
                      }
                    $tabla .= '</tr>';

                    $tabla .= '<tr>';
                    $tabla .= '<td>PROGRAMADO ACUMULADO</td>';
                    for($i = 1 ;$i<=12 ;$i++)
                      {
                        $tabla .= '<td>'.$matriz_r[3][$i].'</td>';
                      }
                    $tabla .= '</tr>';
                    $tabla .= '<tr bgcolor=#d6e4f1>';
                    $tabla .= '<td>PROGRAMADO ACUMULADO %</td>';
                    for($i = 1 ;$i<=12 ;$i++)
                      {
                        $tabla .= '<td>'.$matriz_r[4][$i].'%</td>';
                      }
                    $tabla .= '</tr>';
                    $tabla .= '<tr>';
                    $tabla .= '<td>EJECUTADO</td>';
                    for($i = 1 ;$i<=12 ;$i++)
                      {
                        $tabla .= '<td>'.$matriz_r[5][$i].'%</td>';
                      }
                    $tabla .= '</tr>';
                    $tabla .= '<tr>';
                    $tabla .= '<td>EJECUTADO ACUMULADO</td>';
                    for($i = 1 ;$i<=12 ;$i++)
                      {
                        $tabla .= '<td>'.$matriz_r[6][$i].'%</td>';
                      }
                    $tabla .= '</tr>';
                    $tabla .= '<tr bgcolor=#d6e4f1>';
                    $tabla .= '<td>EJECUTADO ACUMULADO %</td>';
                    for($i = 1 ;$i<=12 ;$i++)
                      {
                        $tabla .= '<td>'.$matriz_r[7][$i].'%</td>';
                      }
                    $tabla .= '</tr>';
                    $tabla .= '<tr>';
                    $tabla .= '<td>EFICACIA</td>';
                    for($i = 1 ;$i<=12 ;$i++)
                      {
                        $tabla .= '<td>'.$matriz_r[8][$i].'%</td>';
                      }
                    $tabla .= '</tr>';

                    $tabla .= '</table>';
                $tabla .= '</td>';
                $nro_obj++;
                }
            $tabla .= '</tr>';


        $data['togestion'] = $tabla;
        $this->load->view('admin/reportes/seguimiento/objetivos_gestion/ogestion2_detalle', $data);
    }

    /*---------------------------------------------------- OBJETIVOS DE GESTION A NIVEL OBJETIVOS -------------------------------------*/
    public function objetivos_gestion_objetivo()
    { 
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++)
        {
             $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        
        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];

        $data['programas'] = $this->model_reporte_seguimiento->list_programas(); ///// lista de programas
        
        $this->load->view('admin/reportes/seguimiento/objetivos_gestion/ogestion3', $data);
    }
    /*---------------------------------------------------- OBJETIVOS DE GESTION A NIVEL OBJETIVOS DETALLES -------------------------------------*/
    public function objetivos_gestion_objetivo_detalle($poa_id,$o_id)
    { 
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++)
        {
             $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        
        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];
        $data['programa']=$this->model_reporte_seguimiento->get_programa($poa_id); /// get programa
        
        $obj=$this->model_reporte_seguimiento->get_objetivo($o_id); /// get objetivo
        $data['objetivo']=$obj;

        $tabla = '';
            $tabla .= '<table  class="table table table-bordered" width="100%">';
            $tabla .= '<tr bgcolor=#474544>';
                $tabla .= '<th>OBJETIVO DE GESTI&Oacute;N</th>';
                $tabla .= '<th>TIPO DE INDICADOR</th>';
                $tabla .= '<th>LINEA/BASE</th>';
                $tabla .= '<th>META</th>';
                $tabla .= '<th>TIPO DE INDICADOR</th>';
                $tabla .= '<th>CRONOGRAMA DE EJECUCI&Oacute;N</th>';
            $tabla .= '</tr>';
            $tabla .= '<tr>';
                $tabla .= '<td>'.$obj[0]['o_objetivo'].'</td>';
                $tabla .= '<td>'.$obj[0]['indi_descripcion'].'</td>';
                $tabla .= '<td>'.$obj[0]['o_linea_base'].'</td>';
                $tabla .= '<td>'.$obj[0]['o_meta'].'</td>';
                $tabla .= '<td>'.$obj[0]['o_ponderacion'].' %</td>';
                $tabla .= '<td>';
                    $tabla .= '<table  class="table table table-bordered" width="100%">';
                    $tabla .= '<tr bgcolor=#474544>';
                        $tabla .= '<th>P/E</th>';
                        $tabla .= '<th>ENERO</th>';
                        $tabla .= '<th>FEBRERO</th>';
                        $tabla .= '<th>MARZO</th>';
                        $tabla .= '<th>ABRIL</th>';
                        $tabla .= '<th>MAYO</th>';
                        $tabla .= '<th>JUNIO</th>';
                        $tabla .= '<th>JULIO</th>';
                        $tabla .= '<th>AGOSTO</th>';
                        $tabla .= '<th>SEPTIEMBRE</th>';
                        $tabla .= '<th>OCTUBRE</th>';
                        $tabla .= '<th>NOVIEMBRE</th>';
                        $tabla .= '<th>DICIEMBRE</th>';
                    $tabla .= '</tr>';

                    $programado=$this->model_reporte_seguimiento->ogestion_programado($obj[0]['o_id']); /// programado
                    $nro=0;
                    foreach($programado as $row)
                    {
                      $nro++;
                      $matriz [1][$nro]=$row['mes_id'];
                      $matriz [2][$nro]=$row['opm_fis'];
                    }
                     /*---------------- llenando la matriz vacia --------------*/
                    for($j = 1; $j<=12; $j++)
                    {
                      $matriz_r[1][$j]=$j;
                      $matriz_r[2][$j]='0';
                      $matriz_r[3][$j]='0';
                      $matriz_r[4][$j]='0';
                      $matriz_r[5][$j]='0';
                      $matriz_r[6][$j]='0';
                      $matriz_r[7][$j]='0';
                      $matriz_r[8][$j]='0';
                    }
                    /*--------------------------------------------------------*/
                    /*--------------------- asignando en la matriz P, PA, %PA ----------------------*/
                      for($i = 1 ;$i<=$nro ;$i++)
                      {
                        for($j = 1 ;$j<=12 ;$j++)
                        {
                          if($matriz[1][$i]==$matriz_r[1][$j])
                          {
                            $matriz_r[2][$j]=round($matriz[2][$i],2);
                          }
                        }
                      }

                    $pa=0;
                    for($j = 1 ;$j<=12 ;$j++){
                        $pa=$pa+$matriz_r[2][$j];
                        $matriz_r[3][$j]=$pa+$obj[0]['o_linea_base'];
                        $matriz_r[4][$j]=round(((($pa+$obj[0]['o_linea_base'])/$obj[0]['o_meta'])*100),2);
                      }
                    /*--------------------------------------------------------------------------------*/
                    /*-------------------------------- EJECUTADO -----------------------------------*/
                    if($obj[0]['indi_id']==1){
                        $ejecutado=$this->model_reporte_seguimiento->ogestion_ejecutado_absoluto($obj[0]['o_id']); /// ejecutado abs
                        $nro_e=0;
                        foreach($ejecutado as $row)
                        {
                            $nro_e++;
                            $matriz_e [1][$nro_e]=$row['mes_id'];
                            $matriz_e [2][$nro_e]=$row['oem_fis'];
                        }
                    }
                    else{
                        $ejecutado=$this->model_reporte_seguimiento->ogestion_ejecutado_relativo($obj[0]['o_id']); /// ejecutado rel
                        $nro_e=0;
                        foreach($ejecutado as $row)
                        {
                            $nro_e++;
                            if($obj[0]['o_denominador']==0){
                                $valor=(($row['oer_desfavorable']/$row['oer_favorable'])*$matriz_r[2][$nro_e]);
                            }
                            else{
                                $valor=(($row['oer_desfavorable']/$row['oer_favorable'])*100);
                            }
                            
                            $matriz_e [1][$nro_e]=$row['mes_id'];
                            $matriz_e [2][$nro_e]=$valor;
                        }
                    }
                    /*--------------------matriz E,AE,%AE gestion ------------------*/
                        for($i = 1 ;$i<=$nro_e ;$i++){
                          for($j = 1 ;$j<=12 ;$j++)
                          {
                            if($matriz_e[1][$i]==$matriz_r[1][$j])
                            {
                              $matriz_r[5][$j]=round($matriz_e[2][$i],1);
                            }
                          }
                        }

                      $pe=0;
                      for($j = 1 ;$j<=12 ;$j++){
                        $pe=$pe+$matriz_r[5][$j];
                        $matriz_r[6][$j]=$pe+$obj[0]['o_linea_base'];
                        $matriz_r[7][$j]=round(((($pe+$obj[0]['o_linea_base'])/$obj[0]['o_meta'])*100),2);
                        if($matriz_r[4][$j]!=0)
                        {$matriz_r[8][$j]=round((($matriz_r[7][$j]/$matriz_r[4][$j])*100),1);}
                    }
                    /*------------------------------------------------------------------------------*/
                      
                    $tabla .= '<tr>';
                    $tabla .= '<td>PROGRAMADO</td>';
                    for($i = 1 ;$i<=12 ;$i++)
                      {
                        $tabla .= '<td>'.$matriz_r[2][$i].'</td>';
                      }
                    $tabla .= '</tr>';

                    $tabla .= '<tr>';
                    $tabla .= '<td>PROGRAMADO ACUMULADO</td>';
                    for($i = 1 ;$i<=12 ;$i++)
                      {
                        $tabla .= '<td>'.$matriz_r[3][$i].'</td>';
                      }
                    $tabla .= '</tr>';
                    $tabla .= '<tr bgcolor=#add1f3>';
                    $tabla .= '<td>PROGRAMADO ACUMULADO %</td>';
                    for($i = 1 ;$i<=12 ;$i++)
                      {
                        $tabla .= '<td>'.$matriz_r[4][$i].'%</td>';
                      }
                    $tabla .= '</tr>';
                    $tabla .= '<tr>';
                    $tabla .= '<td>EJECUTADO</td>';
                    for($i = 1 ;$i<=12 ;$i++)
                      {
                        $tabla .= '<td>'.$matriz_r[5][$i].'%</td>';
                      }
                    $tabla .= '</tr>';
                    $tabla .= '<tr>';
                    $tabla .= '<td>EJECUTADO ACUMULADO</td>';
                    for($i = 1 ;$i<=12 ;$i++)
                      {
                        $tabla .= '<td>'.$matriz_r[6][$i].'%</td>';
                      }
                    $tabla .= '</tr>';
                    $tabla .= '<tr bgcolor=#b9b5b3>';
                    $tabla .= '<td>EJECUTADO ACUMULADO %</td>';
                    for($i = 1 ;$i<=12 ;$i++)
                      {
                        $tabla .= '<td>'.$matriz_r[7][$i].'%</td>';
                      }
                    $tabla .= '</tr>';
                    $tabla .= '<tr>';
                    $tabla .= '<td><b>EFICACIA</b></td>';
                    for($i = 1 ;$i<=12 ;$i++)
                      {
                        $tabla .= '<td><b>'.$matriz_r[8][$i].'%</b></td>';
                      }
                    $tabla .= '</tr>';

                    $tabla .= '</table>';
                $tabla .= '</td>';
            $tabla .= '</tr>';
            $tabla .= '</table>';



            /*------------------ Semaforo de Eficacia ------------------*/
            for($i = 1 ;$i<=12 ;$i++){$efi_menor[$i]=0;$efi_entre[$i]=0;$efi_mayor[$i]=0;}

            for($i = 1 ;$i<=12 ;$i++)
            {
                if($matriz_r[8][$i]<=75){$efi_menor[$i] = $matriz_r[8][$i];}
                if ($matriz_r[8][$i] >= 76 && $matriz_r[8][$i] <= 90.9) {$efi_entre[$i] = $matriz_r[8][$i];}
                if($matriz_r[8][$i] >= 91){$efi_mayor[$i] = $matriz_r[8][$i];}
            }

        $data['togestion'] = $tabla;

        $data['pe'] = $matriz_r;

        $data['menor'] = $efi_menor;
        $data['entre'] = $efi_entre;
        $data['mayor'] = $efi_mayor;

        $this->load->view('admin/reportes/seguimiento/objetivos_gestion/ogestion3_detalle', $data);
    }
    /*==================================================================================================================*/ 
    
  /*=========================================================================================================================*/
    public function get_mes($mes_id)
    {
      $mes[1]='Enero';
      $mes[2]='Febrero';
      $mes[3]='Marzo';
      $mes[4]='Abril';
      $mes[5]='Mayo';
      $mes[6]='Junio';
      $mes[7]='Julio';
      $mes[8]='Agosto';
      $mes[9]='Septiembre';
      $mes[10]='Octubre';
      $mes[11]='Noviembre';
      $mes[12]='Diciembre';

      $dias[1]='31';
      $dias[2]='28';
      $dias[3]='31';
      $dias[4]='30';
      $dias[5]='31';
      $dias[6]='30';
      $dias[7]='31';
      $dias[8]='31';
      $dias[9]='30';
      $dias[10]='31';
      $dias[11]='30';
      $dias[12]='31';

      $valor[1]=$mes[$mes_id];
      $valor[2]=$dias[$mes_id];

      return $valor;
    }
}