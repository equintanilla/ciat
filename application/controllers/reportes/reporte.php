<?php
define("DOMPDF_ENABLE_REMOTE", true);
define("DOMPDF_TEMP_DIR", "/tmp");
class Reporte extends CI_Controller
{
    private $estilo_vertical = '<style>
        body{
            font-family: sans-serif;
            }
        @page {
        margin: 40px 40px;
        }
        header {
            position: fixed;
            left: 0px;
            top: -20px;
            right: 0px;
            height: 20px;
            background-color: #fff;
            }
        header h1{
            margin: 5px 0;
        }
        header h2{
            margin: 0 0 10px 0;
        }
        footer {
            position: fixed;
            left: 0px;
            bottom: -70px;
            right: 0px;
            height: 100px;
            border-bottom: 2px solid #ddd;
        }
        footer .page:after {
            content: counter(page) ;
        }
        footer table {
            width: 100%;
        }
        footer p {
            text-align: right;
        }
        footer .izq {
            text-align: left;
        }
        table{
            font-size: 8px;
            width: 100%;
            background-color:#fff;
        }
        th, td {
                padding: 1.5px;
                text-align: center;
            }
        .mv{font-size:10px;}
        .siipp{width:180px;}
        .header_table {
            color: #ffffff;
            text-align: center;
            align-items: center;
            align-self: center;
            font-weight: bold;
            background-color: #696969;
            align-content: center;
        }
        .header_subtable {
            color: #ffffff;
            text-align: center;
            align-items: center;
            align-self: center;
            font-weight: bold;
            background-color: #a59393;
            align-content: center;
        }
        .titulo_pdf {
            text-align: left;
            font-size: 9px;
        }
        .pdes_titulo{
            text-decoration: underline;
            font-weight: bold;
            text-transform: uppercase;
        }
        .td_pdes{
            text-align: left;  
        }
        .indi_desemp{
            font-size: 11px;
            border: 1px solid #CCC;
            background-color: #E0E0E0;
            padding: .5em;
        }
        table.fixed { table-layout:fixed; }
        table.fixed td { overflow: hidden; }
        .sub_table{
            border-bottom:1px solid black;
        }
    </style>';
    public function __construct()
    {
        parent::__construct();
        $this->load->library('pdf');
        $this->load->library('pdf2');
        $this->load->model('menu_modelo');
        $this->load->model('reportes/model_objetivo');
        $this->load->model('Users_model', '', true);
        $this->load->model('programacion/marco_estrategico/mobjetivos');
        $this->load->model('mantenimiento/mpoa');
        $this->load->model('model_pei');
        $this->load->library('menu');
        $this->menu->const_menu(1);
    }
    public function ficha_tecnica_ejecucion()
    {
        $n = 2;
        $gestion = $this->session->userdata('gestion');
        $lista_objetivos = $this->mobjetivos->lista_objetivos($gestion);
        $mision_vision = $this->model_objetivo->get_dato_configuracion($gestion);
        $mision_vision = $mision_vision->row();
        $mision = $mision_vision->conf_mision;
        $vision = $mision_vision->conf_vision;
        $gestion_base = $mision_vision->conf_gestion_base;
        $rango = $mision_vision->conf_gestion_desde.' - '.$mision_vision->conf_gestion_hasta;
        $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/logo.jpg" class="siipp"/>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata('entidad').'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$gestion.'<br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                            <b>FORMULARIO : </b> OBJETIVOS ESTRATEGICOS '.$mision_vision->conf_gestion_inicio.'-'.$mision_vision->conf_gestion_hasta.' <br>
                        </td>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/escudo.jpg" alt="" width="100px">
                        </td>
                    </tr>
                </table>
                <div class="mv" style="text-align:justify">
                    <b>MISI&Oacute;N: </b>'.$mision.'
                </div><br>
                <div class="mv" style="text-align:justify">
                    <b>VISI&Oacute;N: </b>'.$vision.'
                </div>
                <footer>
                    <table class="table table-bordered">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata('sistema_pie').'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>
                <p>
                    <table style="table-layout:fixed;">
                        <tbody>
                            '.$this->contenido_obj_estrategicos($lista_objetivos,$gestion,$n,$mision_vision->conf_gestion_inicio,$mision_vision->conf_gestion_hasta).'
                        </tbody>
                    </table>
                </p>
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'landscape');
        $dompdf->render();
        $dompdf->stream("Obj_estartegicos_ejecucion.pdf", array("Attachment" => false));
    }
    // Reporte PDF Ficha Tecnica 
    public function ficha_tecnica()
    {
        $fecha_creacion_pdf = date('d/m/Y - H:i');
        $n = 1;
        $gestion = $this->session->userdata('gestion');
        $lista_objetivos = $this->mobjetivos->lista_objetivos($gestion);
        $mision_vision = $this->model_objetivo->get_dato_configuracion($gestion);
        $mision_vision = $mision_vision->row();
        $mision = $mision_vision->conf_mision;
        $vision = $mision_vision->conf_vision;
        $gestion_base = $mision_vision->conf_gestion_base;
        $rango = $mision_vision->conf_gestion_desde.' - '.$mision_vision->conf_gestion_hasta;
        $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="verde"></div>
                <div class="white"></div>
                <table width="100%">
                        <tr>
                            <td class="titulo_pdf" style="width:620px !important; border-right:1px solid #aaa; padding-right:7px;">
								<b>ENTIDAD : </b> '.$this->session->userdata('entidad').'<br>
                                <b>POA - PLAN OPERATIVO POR RESULTADOS  - </b> '.$gestion.'<br>
                                <b>FORMULARIO PEI-01:</b> ACCIONES ESTRATÉGICAS EN EL MEDIANO PLAZO<br>
                                <b>Fecha de Impresi&oacute;n : </b> '.$fecha_creacion_pdf.'<br>
                            </td>
                            <td>
                                <img style="width:310px !important; padding:0 !important; margin:0 !important;" src="'.base_url().'assets/img/logo.jpg" class="siipp"/>
                            </td>
                        </tr>
				
                </table>
				<hr style="border:0; border-bottom:1px solid #aaa;">

                <footer>
                    <table class="table table-bordered">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata('sistema_pie').'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>
                <p>
                    <table style="table-layout:fixed;">
                        <tbody>
                            '.$this->contenido_obj_estrategicos($lista_objetivos,$gestion,$n,$mision_vision->conf_gestion_inicio,$mision_vision->conf_gestion_hasta).'
                        </tbody>
                    </table>
                </p>
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'landscape');
        $dompdf->render();
        $dompdf->stream("Obj_estrategicos_programacion.pdf", array("Attachment" => false));
    }

    public function ficha_tecnica_prueba()
    {
        $datos = $this->model_pei->pei_mision_get();
        // $list_obj = $this->model_objetivos->get_list_objetivos_estrategicos($this->session->userdata("gestion"));
        // Creación del objeto de la clase heredada
        $var = base_url();
        $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div id="content">
                <div class="rojo"></div>
                <div class="verde"></div>
                <table style="border-bottom:1pt solid black;">
                    <tr>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/logo.jpg" class="siipp"/>
                        </td>
                        <td width=60%;>
                            <table style="width:100%;">
                                <tr>
                                    <td style="width:85%;">
                                        <p class="izq">
                                            <font size=1>
                                                <b>ENTIDAD : </b>'.$this->session->userdata('entidad').'<br>
                                                <b>POA - PLAN OPERATIVO ANUAL : </b> 2016<br>
                                                <b>'.$this->session->userdata('sistema').' </b><br>
                                                <b>FORMULARIO : </b> OBJETIVOS ESTRATEGICOS 2016 - 2020
                                                <b>BASE URL: </b>'.$var.'
                                            </font>
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/escudo.jpg" alt="" width="100px">
                        </td>
                    </tr>
                </table>
                <div class="mv" style="text-align:justify">
                    <b>MISI&Oacute;N:</b> '.$this->session->userdata('entidad').' es una entidad-........
                </div><br>
                <div class="mv" style="text-align:justify">
                    <b>VISI&Oacute;N: </b> '.$this->session->userdata('entidad').' es una entidad-........
                </div>
                <p>
                    <table>
                        <thead>
                            <tr bgcolor="#696969">
                                <td style="width:1%;"><center><b><font color="#ffffff">COD.</font></b></center></td>
                                <td style="width:5%;"><center><b><font color="#ffffff">PDES</font></b></center></td>
                                <td style="width:3%;"><center><b><font color="#ffffff">Objetivo Estrat&eacute;gico</font></b></center></td>
                                <td style="width:1%;"><center><b><font color="#ffffff">Tipo de Indicador</font></b></center></td>
                                <td style="width:3%;"><center><b><font color="#ffffff">Indicador</font></b></center></td>
                                <td style="width:1%;"><center><b><font color="#ffffff">Linea Base</font></b></center></td>
                                <td style="width:1%;"><center><b><font color="#ffffff">Meta</font></b></center></td>
                                <td style="width:1%;"><center><b><font color="#ffffff">Ponderaci&oacute;n</font></b></center></td>
                                <td style="width:2%;"><center><b><font color="#ffffff">Responsable</font></b></center></td>
                                <td style="width:2%;"><center><b><font color="#ffffff">Unidad Responsable</font></b></center></td>
                                <td style="width:7%;"><center><b><font color="#ffffff">Temporalizai&oacute;n 2016-2020</font></b></center></td>
                            </tr>
                        </thead>

                    </table>
                </p>
                <footer>
                    <table class="table table-bordered">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata("sistema_pie").'</p></td>
                            <td><p class="page">P&aacute;gina</p></td>
                        </tr>
                    </table>
                </footer>
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'landscape');
        $dompdf->render();
        $dompdf->stream("reporte_matriz_resultado_final.pdf", array("Attachment" => false));
    }

    public function programacion_gestion($obje_id, $gestion_inicial, $linea_base, $meta)
    {
        $cont = 1;
        $prog_acumulado = $linea_base; //variable de programacion acumulada
        for ($i = $gestion_inicial; $i <= ($gestion_inicial + 4); $i++) {
            $puntero_prog = 'prog' . $cont;//PUNTERO PARA LISTAR LA PROGRAMACION
            $dat_prog = $this->mobjetivos->get_prog_obj($obje_id, $i);
            $dato_programado = $dat_prog[0]['opm_programado'];
            $temporalizacion[$puntero_prog] = $dato_programado;//matriz
            //PROGRAMADO ACUMULADO
            $puntero_prog_acumulado = 'p_acumulado' . $cont;//PUNTERO PARA LA PROGRAMACION ACUMULADA
            $prog_acumulado += $dato_programado;
            $temporalizacion[$puntero_prog_acumulado] = $prog_acumulado;//matriz
            //PROGRAMA ACUMULADO PORCENTUAL
            $puntero_pa_porcentual = 'pa_porc' . $cont;//PUNTERO PARA LA PROGRAMACION ACUMULADA PORCENTUAL
            if($meta == 0){
                $pa_porcentual = 0;
            }else{
                $pa_porcentual = (($prog_acumulado / $meta) * 100);
            }
            $temporalizacion[$puntero_pa_porcentual] = $pa_porcentual;//matriz
            $cont++;
        }
        return $temporalizacion;
    }

    public function programacion_gestion_pt($obje_id, $gestion_inicial, $linea_base, $meta)
    {
        $cont = 1;
        $prog_acumulado = $linea_base; //variable de programacion acumulada
        for ($i = $gestion_inicial; $i <= ($gestion_inicial + 4); $i++) {
            $puntero_prog = 'prog' . $cont;//PUNTERO PARA LISTAR LA PROGRAMACION
            $dat_prog = $this->mobjetivos->get_prog_pt_obj($obje_id, $i);
            $dato_programado = $dat_prog[0]['opm_programado'];
            $temporalizacion[$puntero_prog] = $dato_programado;//matriz
            //PROGRAMADO ACUMULADO
            $puntero_prog_acumulado = 'p_acumulado' . $cont;//PUNTERO PARA LA PROGRAMACION ACUMULADA
            $prog_acumulado += $dato_programado;
            $temporalizacion[$puntero_prog_acumulado] = $prog_acumulado;//matriz
            //PROGRAMA ACUMULADO PORCENTUAL
            $puntero_pa_porcentual = 'pa_porc' . $cont;//PUNTERO PARA LA PROGRAMACION ACUMULADA PORCENTUAL
            if($meta == 0){
                $pa_porcentual = 0;
            }else{
                $pa_porcentual = (($prog_acumulado / $meta) * 100);
            }
            $temporalizacion[$puntero_pa_porcentual] = $pa_porcentual;//matriz
            $cont++;
        }
        return $temporalizacion;
    }

    function retorna_porcentajes($dato,$gestion,$temporalizacion,$n)
    {
        $var = '
            <table>
                <thead>
                    <tr bgcolor="#696969">
                        <th class="header_subtable"> Tipo '.$dato.'</th>
                        <th class="header_subtable">'.$gestion.'</th>
                        <th class="header_subtable">'.($gestion+1).'</th>
                        <th class="header_subtable">'.($gestion+2).'</th>
                        <th class="header_subtable">'.($gestion+3).'</th>
                        <th class="header_subtable">'.($gestion+4).'</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="header_subtable"> P. </td>
                        <td bgcolor="#F5F5F5">'.round($temporalizacion[$dato]['prog1'],0).'%</td>
                        <td bgcolor="#F5F5F5">'.round($temporalizacion[$dato]['prog2'],0).'%</td>
                        <td bgcolor="#F5F5F5">'.round($temporalizacion[$dato]['prog3'],0).'%</td>
                        <td bgcolor="#F5F5F5">'.round($temporalizacion[$dato]['prog4'],0).'%</td>
                        <td bgcolor="#F5F5F5">'.round($temporalizacion[$dato]['prog5'],0).'%</td>
                    </tr>
                    <tr>
                        <td class="header_subtable"> P.A. </td>
                        <td bgcolor="#F5F5F5">'.round($temporalizacion[$dato]['p_acumulado1'],0).'%</td>
                        <td bgcolor="#F5F5F5">'.round($temporalizacion[$dato]['p_acumulado2'],0).'%</td>
                        <td bgcolor="#F5F5F5">'.round($temporalizacion[$dato]['p_acumulado3'],0).'%</td>
                        <td bgcolor="#F5F5F5">'.round($temporalizacion[$dato]['p_acumulado4'],0).'%</td>
                        <td bgcolor="#F5F5F5">'.round($temporalizacion[$dato]['p_acumulado5'],0).'%</td>
                    </tr>
                    <tr>
                        <td class="header_subtable"> %P.A. </td>
                        <td bgcolor="#F5F5F5">'.round($temporalizacion[$dato]['pa_porc1'],0).'%</td>
                        <td bgcolor="#F5F5F5">'.round($temporalizacion[$dato]['pa_porc2'],0).'%</td>
                        <td bgcolor="#F5F5F5">'.round($temporalizacion[$dato]['pa_porc3'],0).'%</td>
                        <td bgcolor="#F5F5F5">'.round($temporalizacion[$dato]['pa_porc4'],0).'%</td>
                        <td bgcolor="#F5F5F5">'.round($temporalizacion[$dato]['pa_porc5'],0).'%</td>
                    </tr>
                    '.$this->retorna_porcentajes_ejecucion($n,$dato,$temporalizacion,$gestion).'
                </tbody>
            </table>
        ';
        return $var;
    }

    public function retorna_porcentajes_ejecucion($n,$obje_id,$temporalizacion,$gestion)
    {
        $tr_return = '';
        if($n == 1){
            $tr_return = '
                <tr>
                    <td class="header_subtable"> E. </td>
                    <td bgcolor="#F5F5F5">'.round($temporalizacion[$obje_id]['prog1'],0).'%</td>
                    <td bgcolor="#F5F5F5">'.round($temporalizacion[$obje_id]['prog2'],0).'%</td>
                    <td bgcolor="#F5F5F5">'.round($temporalizacion[$obje_id]['prog3'],0).'%</td>
                    <td bgcolor="#F5F5F5">'.round($temporalizacion[$obje_id]['prog4'],0).'%</td>
                    <td bgcolor="#F5F5F5">'.round($temporalizacion[$obje_id]['prog5'],0).'%</td>
                </tr>
                <tr>
                    <td class="header_subtable"> E.A. </td>
                    <td bgcolor="#F5F5F5">'.round($temporalizacion[$obje_id]['p_acumulado1'],0).'%</td>
                    <td bgcolor="#F5F5F5">'.round($temporalizacion[$obje_id]['p_acumulado2'],0).'%</td>
                    <td bgcolor="#F5F5F5">'.round($temporalizacion[$obje_id]['p_acumulado3'],0).'%</td>
                    <td bgcolor="#F5F5F5">'.round($temporalizacion[$obje_id]['p_acumulado4'],0).'%</td>
                    <td bgcolor="#F5F5F5">'.round($temporalizacion[$obje_id]['p_acumulado5'],0).'%</td>
                </tr>
                <tr>
                    <td class="header_subtable"> %E.A. </td>
                    <td bgcolor="#F5F5F5">'.round($temporalizacion[$obje_id]['pa_porc1'],0).'%</td>
                    <td bgcolor="#F5F5F5">'.round($temporalizacion[$obje_id]['pa_porc2'],0).'%</td>
                    <td bgcolor="#F5F5F5">'.round($temporalizacion[$obje_id]['pa_porc3'],0).'%</td>
                    <td bgcolor="#F5F5F5">'.round($temporalizacion[$obje_id]['pa_porc4'],0).'%</td>
                    <td bgcolor="#F5F5F5">'.round($temporalizacion[$obje_id]['pa_porc5'],0).'%</td>
                </tr>
                <tr>
                    <td class="header_subtable">Eficacia</td>
                    <td bgcolor="#F5F5F5"></td>
                    <td bgcolor="#F5F5F5"></td>
                    <td bgcolor="#F5F5F5"></td>
                    <td bgcolor="#F5F5F5"></td>
                    <td bgcolor="#F5F5F5"></td>
                </tr>
            ';
        }
        return $tr_return;
    }

    public function contenido_obj_estrategicos($lista_objetivos,$gestion,$n,$g1,$g2)
    {
        $temporalizacion = array();
        foreach ($lista_objetivos as $row1) {
            $obje_id = $row1['obje_id'];// id de mi objetivo estrategico
            $gestion_inicial = $this->mobjetivos->get_fec_inicio()->conf_gestion_desde;// id de mi objetivo estrategico
            $linea_base = $row1['obje_linea_base'];
            $meta = $row1['obje_meta']; //variable meta
            $temporalizacion[$obje_id] = $this->programacion_gestion($obje_id, $gestion_inicial, $linea_base, $meta);
        }
        // var_dump($temporalizacion);  die;
        $campos_objetivos = '';
        foreach ($lista_objetivos as $fila) {

            //GENERANDO LOS PRODUCTOS TERMINALES
            $lista_pt = $this->mobjetivos->lista_prod_terminal($fila['obje_id']);
            $temporalizacion_pt = array();
            foreach ($lista_pt as $row1) {
                $obje_id = $row1['pt_id'];// id de mi objetivo estrategico
                $gestion_inicial = $this->mobjetivos->get_fec_inicio()->conf_gestion_desde;// id de mi objetivo estrategico
                $linea_base = $row1['obje_linea_base'];
                $meta = $row1['obje_meta']; //variable meta
                $temporalizacion_pt[$obje_id] = $this->programacion_gestion_pt($obje_id, $gestion_inicial, $linea_base, $meta);
            }


            $campos_objetivos .= '
            <tr>
                <td class="sub_table">
                    <table>
                        <tr bgcolor="#F5F5F5" style="width:80%;">
                            <td style="width:7%;" class="header_table"> PDES: </td>
                            <td style="text-align: left; width:9%;" ><span class="pdes_titulo">Pilar :</span> '.$fila['pdes_pilar'].'</td>
                            <td style="text-align: left;"><span class="pdes_titulo">Meta :</span> '.$fila['pdes_meta'].'<br>
                                            <span class="pdes_titulo">Resultado :</span> '.$fila['pdes_resultado'].'<br>
                                            <span class="pdes_titulo">Acción :</span> '.$fila['pdes_accion'].'</td>
                        </tr>
                        <tr bgcolor="#F5F5F5" style="width:80%;">
                            <td style="width:7%;" class="header_table"> PTDI: </td>
                            <td style="text-align: left; width:18%;" ><span class="pdes_titulo">Eje Program&aacute;tico :</span> '.$fila['ptdi_eje'].'</td>
                            <td style="text-align: left;"><span class="pdes_titulo">Pol&iacute;tica :</span> '.$fila['ptdi_politica'].'<br>
                                            <span class="pdes_titulo">Programa :</span> '.$fila['ptdi_programa'].'</td>
                        </tr>
                    </table>
                    <table>
                        <tr bgcolor="#696969">
                            <td style="width:3.7%;" class="header_table">COD. '.$fila['obje_id'].'</td>
                            <td style="width:10%;" class="header_table">ACCIONES DE MEDIANO PLAZO (PEI)</td>
                            <td style="width:10%;" class="header_table">INDICADORES</td>
                            <td style="width:3.7%;" class="header_table">FORMULA</td>
                            <td style="width:3.7%;" class="header_table">L&Iacute;NEA BASE</td>
                            <td style="width:3.7%;" class="header_table">META INICIAL</td>
                            <td style="width:3.7%;" class="header_table">MODIFICA- CIONES</td>
                            <td style="width:3.7%;" class="header_table">META ACTUAL</td>
                            <td style="width:3.7%;" class="header_table">PONDARACI&Oacute;N</td>
                            <td class="header_table">GESTI&Oacute;N '.$g1.'-'.$g2.'</td>
                            <td style="width:7.4%;" class="header_table">FUENTES DE VERIFICACI&Oacute;N</td>
                            <td style="width:3.4%;" class="header_table">RESPONSABLE</td>
                        </tr>
                        <tr bgcolor="#F5F5F5" style="width:100%;">            
                            <td style="width:3.7%; font-size:8px;">'.$fila['obje_codigo'].'</td>
                            <td style="width:10%;">'.$fila['obje_objetivo'].'</td>
                            <td style="width:10%;">'.$fila['obje_indicador'].'</td>
                            <td style="width:3.7%;">'.$fila['obje_formula'].'</td>
                            <td style="width:3.7%;">'.$fila['obje_linea_base'].'</td>
                            <td style="width:3.7%;">'.$fila['obje_meta'].'%</td>
                            <td style="width:3.7%;">%</td>
                            <td style="width:3.7%;">%</td>
                            <td style="width:3.7%;">'.$fila['obje_ponderacion'].'%</td>
                            
                            <td>'.$this->retorna_porcentajes($fila['obje_id'],$g1,$temporalizacion,$n).'</td>
                            <td style="width:7.4%;">'.$fila['obje_fuente_verificacion'].'</td>
                            <td style="width:3.4%;">'.$fila['fun_nombre'].' '.$fila['fun_paterno'].'</td>
                        </tr>
                    </table> <br><b><p align="left">PRODUCTOS TERMINALES</p><b>';
                    foreach ($lista_pt as $fila) {
                         $campos_objetivos .= '
                        <table>
                            <tr bgcolor="#696969">
                                <td style="width:3.7%;" class="header_table">COD. </td>
                                <td style="width:10%;" class="header_table">PRODUCTO TERMINAL DE MEDIANO PLAZO (PEI)</td>
                                <td style="width:10%;" class="header_table">INDICADORES</td>
                                <td style="width:3.7%;" class="header_table">FORMULA</td>
                                <td style="width:3.7%;" class="header_table">L&Iacute;NEA BASE</td>
                                <td style="width:3.7%;" class="header_table">META INICIAL</td>
                                <td style="width:3.7%;" class="header_table">MODIFICA- CIONES</td>
                                <td style="width:3.7%;" class="header_table">META ACTUAL</td>
                                <td style="width:3.7%;" class="header_table">PONDARACI&Oacute;N</td>
                                <td class="header_table">GESTI&Oacute;N '.$g1.'-'.$g2.'</td>
                                <td style="width:7.4%;" class="header_table">FUENTES DE VERIFICACI&Oacute;N</td>
                                <td style="width:3.4%;" class="header_table">RESPONSABLE</td>
                            </tr>
                            <tr bgcolor="#F5F5F5" style="width:100%;">            
                                <td style="width:3.7%; font-size:8px;">'.$fila['obje_codigo'].'</td>
                                <td style="width:10%;">'.$fila['obje_objetivo'].'</td>
                                <td style="width:10%;">'.$fila['obje_indicador'].'</td>
                                <td style="width:3.7%;">'.$fila['obje_formula'].'</td>
                                <td style="width:3.7%;">'.$fila['obje_linea_base'].'</td>
                                <td style="width:3.7%;">'.$fila['obje_meta'].'%</td>
                                <td style="width:3.7%;">%</td>
                                <td style="width:3.7%;">%</td>
                                <td style="width:3.7%;">'.$fila['obje_ponderacion'].'%</td>
                                
                                <td>'.$this->retorna_porcentajes($fila['pt_id'],$g1,$temporalizacion_pt,$n).'</td>
                                <td style="width:7.4%;">'.$fila['obje_fuente_verificacion'].'</td>
                                <td style="width:3.4%;">'.$fila['fun_nombre'].' '.$fila['fun_paterno'].'</td>
                            </tr>
                        </table>';

                    }

$campos_objetivos .= '
                </td>
            </tr>';
        }
        return $campos_objetivos;
    }

    public function prueba()
    {
        $menu['enlaces'] = $this->menu->get_enlaces();
        $menu['subenlaces'] = $this->menu->get_sub_enlaces();
        $menu['titulo'] = 'PRUEBA DE GRAFICOS';
        $this->load->view('includes/header');
        $this->load->view('includes/menu_lateral', $menu);
        $this->load->view('includes/footer');
        $this->load->view('vista');
        $this->jsongraf();
        $v = $this->pdf_graficos();
        ?>
        <h1>gragragasaas</h1>
        <h1>gragragasaas</h1>
        <h1>gragragasaas</h1>
        <h1><?php echo $v ?></h1>
        <div id="container">
        </div>
        <?php
    }

    //FUNCION PARA CONSTRUIR MI VISTA
    function construir_vista($ruta, $data)
    {
        $menu['enlaces'] = $this->menu->get_enlaces();
        $menu['subenlaces'] = $this->menu->get_sub_enlaces();
        $menu['titulo'] = 'PRUEBA DE GRAFICOS';
        $this->load->view('includes/header');
        $this->load->view('includes/menu_lateral', $menu);
        $this->load->view($ruta, $data);
        $this->load->view('includes/footer');
    }

    public function pdf_grafico()
    {
        $ci = $_GET['var'];
        $script = '';
        $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <h1>'.$ci.'000 asd</h1>
                <footer>
                    <table class="table table-bordered">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata("sistema_pie").'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>
                <p>'.$this->jsongraf.'
                <div id="container"></div>
                </p>
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'landscape');
        $dompdf->render();
        $dompdf->stream("grafico_pdf.pdf", array("Attachment" => false));
    }

    public function pdf_graficos()
    {
        ?>
        <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <h1> Grafico </h1>
        <button id="bs">GUARDAR</button>
        <div class="col-md-4" style="height: 350px; width: 33.3%;">
            <div class="box" id="graf">
                <script type="text/javascript">
                    $(function() {
                        Highcharts.chart('graf', {
                            title: {
                            text: 'Monthly Average Temperature',
                            x: -20 //center
                            },
                            subtitle: {
                            text: 'Source: WorldClimate.com',
                            x: -20
                            },
                            xAxis: {
                            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                                'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
                            ]
                            },
                            yAxis: {
                            title: {
                                text: 'Temperature (°C)'
                            },
                            plotLines: [{
                                value: 0,
                                width: 1,
                                color: '#808080'
                            }]
                            },
                            tooltip: {
                            valueSuffix: '°C'
                            },
                            legend: {
                            layout: 'vertical',
                            align: 'right',
                            verticalAlign: 'middle',
                            borderWidth: 0
                            },
                            series: [{
                            name: 'Tokyo',
                            data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
                            }, {
                            name: 'New York',
                            data: [-0.2, 0.8, 5.7, 11.3, 17.0, 22.0, 24.8, 24.1, 20.1, 14.1, 8.6, 2.5]
                            }, {
                            name: 'Berlin',
                            data: [-0.9, 0.6, 3.5, 8.4, 13.5, 17.0, 18.6, 17.9, 14.3, 9.0, 3.9, 1.0]
                            }, {
                            name: 'London',
                            data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
                            }]
                        });
                        <?php 
                            $url = 1231;
                            ?>
                        var exportUrl = 'http://export.highcharts.com/';
                        $("#bs").click(testPOSTi);
                        function testPOSTi() {
                            var vector = [20];
                            var optionsStr = JSON.stringify({
                                "xAxis": {
                                    "categories": ["Dato1", "Dato2", "Dato3", "Dato4"]
                                },
                                "yAxis": {
                                    "title": {
                                        "text": "PORCENTAJES (%)"
                                    }
                                },
                                "title": {
                                    "text": 'PROGRAMACIÓN OBJETIVO ESTRATÉGICO',
                                    "x": -90
                                },
                                "series": [{
                                    "data": [29.9, 71.5, 106.4, 150]
                                }],
                                "name": "Programacion acumulada en %"
                            });
                            dataString = encodeURI('async=true&type=jpeg&width=400&options=' + optionsStr);
                            if (window.XDomainRequest) {
                                var xdr = new XDomainRequest();
                                xdr.open("post", exportUrl+ '?' + dataString);
                                xdr.onload = function () {
                                    console.log(xdr.responseText);
                                    $('#container').html('<img src="' + exporturl + xdr.responseText + '"/>');
                                };
                                xdr.send();
                            } else {
                                i++;
                                $.ajax({
                                    type: 'POST',
                                    data: dataString,
                                    url: exportUrl,
                                    success: function (data) {
                                        console.log('URL Imagen guardada: ', data);
                                        var gl = exportUrl + data;
                                        $('#containers').html('<img id="imgs" value="'+gl+'" src="' + exportUrl + data + '"/>');
                                    }
                                });
                            }
                        }
                    });
                </script>      
            </div>
        </div>
        <?php
        return $url;
    }

    public function jsongraf()
    {
        ?>
        <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script type="text/javascript">
            $(function() {
                var exportUrl = 'http://export.highcharts.com/';
                var vector = [20];
                var optionsStr = JSON.stringify({
                    title: {
                            text: 'Monthly Average Temperature',
                            x: -20 //center
                            },
                            subtitle: {
                            text: 'Source: WorldClimate.com',
                            x: -20
                            },
                            xAxis: {
                            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                                'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
                            ]
                            },
                            yAxis: {
                            title: {
                                text: 'Temperature (°C)'
                            },
                            plotLines: [{
                                value: 0,
                                width: 1,
                                color: '#808080'
                            }]
                            },
                            tooltip: {
                            valueSuffix: '°C'
                            },
                            legend: {
                            layout: 'vertical',
                            align: 'right',
                            verticalAlign: 'middle',
                            borderWidth: 0
                            },
                            series: [{
                            name: 'Tokyo',
                            data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
                            }, {
                            name: 'New York',
                            data: [-0.2, 0.8, 5.7, 11.3, 17.0, 22.0, 24.8, 24.1, 20.1, 14.1, 8.6, 2.5]
                            }, {
                            name: 'Berlin',
                            data: [-0.9, 0.6, 3.5, 8.4, 13.5, 17.0, 18.6, 17.9, 14.3, 9.0, 3.9, 1.0]
                            }, {
                            name: 'London',
                            data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
                            }]
                });
                dataString = encodeURI('async=true&type=jpeg&width=400&options=' + optionsStr);
                $.ajax({
                    type: 'POST',
                    data: dataString,
                    url: exportUrl,
                    success: function (data) {
                        console.log('URL Imagen guardada: ', data);
                        $('#container').html('<img  src="' + exportUrl + data + '"/>');

                    }
                });
            });
        </script>
        <?php
    }

    public function mision_vision()
    {
        $fecha_creacion_pdf = date('d/m/Y - H:i');
        $gestion = $this->session->userdata('gestion');
        $mision_vision = $this->model_objetivo->get_dato_configuracion($gestion);
        $mision_vision = $mision_vision->row();
        $mision = $mision_vision->conf_mision;
        $vision = $mision_vision->conf_vision;
        $gestion_base = $mision_vision->conf_gestion_base;
        $rango = $mision_vision->conf_gestion_desde.' - '.$mision_vision->conf_gestion_hasta;
        $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                        <tr>
                            <td style="border-bottom:1px solid #aaa; padding-bottom:10px;" colspan="2">
                                <img src="'.base_url().'assets/img/escudo.jpg" alt="" width="100px">
								<br>
								<span style="font-size:1.8em;">'.$this->session->userdata('entidad').'</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="titulo_pdf" style="width:620px !important; border-right:1px solid #aaa; padding-right:7px;">
								<span style="font-size:1.6em;">
								<b>FORMULARIO : </b> MISIÓN Y VISIÓN '.$rango.' 
								</span><br>
                                <b>PLAN OPERATIVO ANUAL : </b> '.$gestion.'<br>
                                <b>Fecha de Impresi&oacute;n : </b> '.$fecha_creacion_pdf.'<br>
                            </td>
                            <td>
                                <img style="width:310px !important; padding:0 !important; margin:0 !important;" src="'.base_url().'assets/img/logo.jpg" class="siipp"/>
                            </td>
                        </tr>

                </table>
				<hr style="border:0; border-bottom:1px solid #aaa;">
                <div class="mv" style="text-align:justify">
                    <b style="font-size:1.5em;">MISI&Oacute;N: </b> <br>
                    '.$mision.'
                </div><br>
                <div class="mv" style="text-align:justify">
                    <b style="font-size:1.5em;">VISI&Oacute;N: </b> <br>
                    '.$vision.'
                </div>
                <footer>
                    <table class="table table-bordered">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata("sistema_pie").'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'landscape');
        $dompdf->render();
        $dompdf->stream("reporte_matriz_resultado_final.pdf", array("Attachment" => false));
    }


    public function historial_mision_vision($tp)
    {
        if($tp==1){
            $titulo='MISI&Oacute;N INSTITUCIONAL';
            $historial=$this->model_pei->historial_mision($this->session->userdata('ide'));
        }
        else{
            $titulo='VISI&Oacute;N INSTITUCIONAL';
            $historial=$this->model_pei->historial_vision($this->session->userdata('ide'));
        }
        $fecha_creacion_pdf = date('d/m/Y - H:i');
        $gestion = $this->session->userdata('gestion');
        $mision_vision = $this->model_objetivo->get_dato_configuracion($gestion);
        $mision_vision = $mision_vision->row();
        $mision = $mision_vision->conf_mision;
        $vision = $mision_vision->conf_vision;
        $gestion_base = $mision_vision->conf_gestion_base;
        $rango = $mision_vision->conf_gestion_desde.' - '.$mision_vision->conf_gestion_hasta;
        $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                        <tr>
                            <td style="border-bottom:1px solid #aaa; padding-bottom:10px;" colspan="2">
                                <img src="'.base_url().'assets/img/escudo.jpg" alt="" width="100px">
								<br>
								<span style="font-size:1.8em;">'.$this->session->userdata('entidad').'</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="titulo_pdf" style="width:620px !important; border-right:1px solid #aaa; padding-right:7px;">
								<span style="font-size:1.6em;">
								<b>HISTORIAL : </b> '.$titulo.' '.$rango.'
								</span><br>
                                <b>PLAN OPERATIVO ANUAL : </b> '.$gestion.'<br>
                                <b>Fecha de Impresi&oacute;n : </b> '.$fecha_creacion_pdf.'<br>
                            </td>
                            <td>
                                <img style="width:310px !important; padding:0 !important; margin:0 !important;" src="'.base_url().'assets/img/logo.jpg" class="siipp"/>
                            </td>
                        </tr>

                </table>
				<hr style="border:0; border-bottom:1px solid #aaa;">
				';

                if($tp==1){
                    $nro=0;
                    foreach($historial  as $row)
                    {
                        $nro++;
                        $html .= '
                        '.$nro.'.- '.$row['fun_nombre'].' '.$row['fun_paterno'].' | '.$row['fecha'].' 
                        <div class="mv" style="text-align:justify">
                            <b>MISI&Oacute;N INSTITUCIONAL: </b> <br>
                            '.$row['mision'].'
                        </div><br>';
                    }
                }
                else{
                    $nro=0;
                    foreach($historial  as $row)
                    {
                        $nro++;
                        $html .= '
                        '.$nro.'.- '.$row['fun_nombre'].' '.$row['fun_paterno'].' | '.$row['fecha'].' 
                        <div class="mv" style="text-align:justify">
                            <b>VISI&Oacute;N INSTITUCIONAL: </b> <br>
                            '.$row['vision'].'
                        </div><br>';
                    }
                }
                
                $html .= '
                <footer>
                    <table class="table table-bordered">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata('sistema_pie').'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'landscape');
        $dompdf->render();
        $dompdf->stream("reporte_matriz_resultado_final.pdf", array("Attachment" => false));
    }
}