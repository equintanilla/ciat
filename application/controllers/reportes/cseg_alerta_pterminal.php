<?php

class Cseg_alerta_pterminal extends CI_Controller
{
    function __construct()
    {
        parent:: __construct();
        $this->load->model('mantenimiento/mpoa');
        $this->load->model('programacion/prog_poa/mobjetivo_gestion');
        $this->load->model('programacion/prog_poa/mp_terminal');
        $this->load->model('registro_ejec/mejec_ogestion_pterminal');
        $this->load->model('programacion/prog_poa/mp_terminal');
        $this->load->model('reportes/seguimiento/malerta_pterminal');
        //llamar a mi menu
        $this->load->library('menu');
        $this->menu->const_menu(7);
        $this->gestion = $this->session->userData('gestion');
        $this->mes = $this->session->userData('mes');
    }

    function index()
    {
        $data['lista_poa'] = $this->mejec_ogestion_pterminal->lista_prog();
        $ruta = 'reportes/seguimiento/prod_terminal/vlista_poa';
        $this->construir_vista($ruta, $data);
    }

    //LISTA DE OBJETIVO DE GESTION
    function lista_ogestion($poa_id)
    {
        $data['dato_poa'] = $this->mpoa->dato_poa_id($poa_id);
        $data['lista_ogestion'] = $this->tabla_ogestion($poa_id);

        $meses=$this->get_mes($this->session->userdata("mes"));
        $mes = $meses[1];
        $dias = $meses[2];

        $data['id_mes'] = $this->session->userdata("mes");
        $data['mes'] = $mes;

        $ruta = 'reportes/seguimiento/prod_terminal/vlista_ogestion';
        $this->construir_vista($ruta, $data);
    }

    //GENERAR TABLA DE OBJETIVO DE GESTION
    function tabla_ogestion($poa_id)
    {
        $lista_objgestion = $this->mejec_ogestion_pterminal->lista_ogestion($poa_id);
        $cont = 1;
        $tabla = '';
        $ruta = site_url() . '/rep/lista_pt/' . $poa_id . '/';
        $ruta_img = base_url() . 'assets/ifinal/folder.png';
        foreach ($lista_objgestion as $row) {
            $tabla .= '<tr>';
            $tabla .= '<td >' . $cont . '</td>';
            $tabla .= '<td >
                            <a href="' . $ruta . $row['o_id'] . '" title="LISTA DE PRODUCTOS TERMINALES">
                                <img src="' . $ruta_img . '" width="50" height="50" class="img-responsive" title="LISTA DE PRODUCTOS TERMINALES">
							</a>
					   </td>';
            $tabla .= '<td >' . $row['o_codigo'] . '</td>';
            $tabla .= '<td >' . $row['o_objetivo'] . '</td>';
            $tabla .= '<td >' . $row['fun_nombre'] . ' ' . $row['fun_paterno'] . ' ' . $row['fun_materno'] . '</td>';
            $tabla .= '<td >' . $row['o_ponderacion'] . '</td>';
            $tabla .= '</tr >';
            $cont++;
        }
        return $tabla;
    }

    //LISTA DE PRODUCTOS TERMINALES
    function lista_pterminal($poa_id, $o_id)
    {
        $data['dato_poa'] = $this->mpoa->dato_poa_id($poa_id);
        $data['poa_id'] = $poa_id;
        $data['dato_ogestion'] = $this->mobjetivo_gestion->get_ogestion($o_id)[0];
        $data['lista_pterminal'] = $this->tabla_pterminal($poa_id, $o_id);

        $meses=$this->get_mes($this->session->userdata("mes"));
        $mes = $meses[1];
        $dias = $meses[2];

        $data['id_mes'] = $this->session->userdata("mes");
        $data['mes'] = $mes;

        $ruta = 'reportes/seguimiento/prod_terminal/vlista_pterminal';
        $this->construir_vista($ruta, $data);
    }

    function tabla_pterminal($poa_id, $o_id)
    {
        $lista_pterminal = $this->mp_terminal->lista_pterminal($o_id);
        $cont = 1;
        $tabla = '';
        $ruta = site_url() . '/rep/graf_pt/' . $poa_id . '/' . $o_id . '/';
        $ruta_img = base_url() . 'assets/ifinal/grafico.png';
        foreach ($lista_pterminal as $row) {
            $tabla .= '<tr>';
            $tabla .= '<td >' . $cont . '</td>';
            $tabla .= '<td >
                            <a href="' . $ruta . $row['pt_id'] . '" title="VER PROGRAMACIÓN Y EJECUCIÓN">
                                <img src="' . $ruta_img . '" width="50" height="50" class="img-responsive" title="VER PROGRAMACIÓN Y EJECUCIÓN">
							</a>
					   </td>';
            $tabla .= '<td >' . $row['pt_codigo'] . '</td>';
            $tabla .= '<td >' . $row['pt_objetivo'] . '</td>';
            $tabla .= '<td >' . $row['fun_nombre'] . ' ' . $row['fun_paterno'] . ' ' . $row['fun_materno'] . '</td>';
            $tabla .= '<td >' . $row['pt_ponderacion'] . '</td>';
            $tabla .= '</tr >';
            $cont++;
        }
        return $tabla;
    }

    //ALERTA TERMPRANA DEL PRODUCTO TERMINAL
    function grafico_pterminal($poa_id, $o_id, $pt_id)
    {
        $data['poa_id'] = $poa_id;
        $data['dato_poa'] = $this->mpoa->dato_poa_id($poa_id)[0];
        $data['dato_ogestion'] = $this->mobjetivo_gestion->get_ogestion($o_id)[0];//objetivo de gestion
        $dato_pterminal = $this->mp_terminal->get_pterminal($pt_id)[0];//producto terminal
        $data['dato_pterminal'] = $dato_pterminal;//producto terminal
        $dato_tabla = $this->tabla_prog_ejec_pterminal($pt_id, $dato_pterminal);
        $data['tabla_prog_ejec'] = $dato_tabla['tabla'];
        $data['programacion'] = $dato_tabla['graf_prog'];
        $data['ejecucion'] = $dato_tabla['graf_ejec'];
        $data['ef_menor'] = $dato_tabla['ef_menor'];
        $data['ef_entre'] = $dato_tabla['ef_entre'];
        $data['ef_mayor'] = $dato_tabla['ef_mayor'];

        $meses=$this->get_mes($this->session->userdata("mes"));
        $mes = $meses[1];
        $dias = $meses[2];

        $data['id_mes'] = $this->session->userdata("mes");
        $data['mes'] = $mes;

        $ruta = 'reportes/seguimiento/prod_terminal//vgrafico_pterminal';
        $this->construir_vista($ruta, $data);
    }

    //programacion y ejecucion del produto terminal
    function tabla_prog_ejec_pterminal($pt_id, $dato)
    {
        $meta = $dato['pt_meta'];
        $linea_base = $dato['pt_linea_base'];
        $indicador = $dato['indi_id'];
        $denominador = $dato['pt_denominador'];
        $mat_prog = $this->mat_prog_mes_pterminal($pt_id, $meta, $linea_base); //matriz de la programacion
        $mat_ejec = $this->mat_ejec_mes_pterminal($pt_id, $meta, $linea_base, $indicador, $denominador, $mat_prog); //matriz de ejecucion
        $graf_prog = '';//vector para el grafico de programacion
        $graf_ejec = '';//vector para el grafico de ejecucion
        $ef_menor = '';//vector para el grafico de eficacia
        $ef_entre = '';//vector para el grafico de eficacia
        $ef_mayor = '';//vector para el grafico de eficacia
        $tabla = '';
        for ($i = 0; $i < count($mat_prog); $i++) {
            $tabla .= '<tr>';
            for ($j = 0; $j <= 12; $j++) {
                $tabla .= '<td style="background-color: #F5F5DC">';
                if ($j == 0) {
                    $tabla .= $mat_prog[$i][$j];
                } elseif ($i == 2) {
                    $tabla .= number_format(($mat_prog[$i][$j]), 2, ',', '.') . '%';
                    $graf_prog .= round($mat_prog[$i][$j]) . ',';
                } else {
                    $tabla .= number_format(($mat_prog[$i][$j]), 2, ',', '.');
                }
                $tabla .= '</td>';
            }
            $tabla .= '</tr>';
        }
        if ($indicador == 1) {//absoluto
            for ($i = 0; $i < count($mat_ejec) - 1; $i++) {
                $tabla .= '<tr>';
                for ($j = 0; $j <= 12; $j++) {
                    $tabla .= '<td style="background:#98FB98 ">';
                    if ($j == 0) {
                        $tabla .= $mat_ejec[$i][$j];
                    } elseif ($i == 2) {
                        $tabla .= number_format(($mat_ejec[$i][$j]), 2, ',', '.') . '%';
                        $graf_ejec .= round($mat_ejec[$i][$j]) . ',';
                    } else {
                        $tabla .= number_format(($mat_ejec[$i][$j]), 2, ',', '.');
                    }
                    $tabla .= '</td>';
                }
                $tabla .= '</tr>';
            }
            $cont = 3;//puntero para eficacia
        } else {//relativo
            for ($i = 0; $i < count($mat_ejec) - 1; $i++) {
                $tabla .= '<tr>';
                for ($j = 0; $j <= 12; $j++) {
                    $tabla .= '<td style="background:#98FB98 ">';
                    if ($j == 0) {
                        $tabla .= $mat_ejec[$i][$j];
                    } elseif ($i == 4) {
                        $tabla .= number_format(($mat_ejec[$i][$j]), 2, ',', '.') . '%';
                        $graf_ejec .= round($mat_ejec[$i][$j]) . ',';
                    } else {
                        $tabla .= number_format(($mat_ejec[$i][$j]), 2, ',', '.');
                    }
                    $tabla .= '</td>';
                }
                $tabla .= '</tr>';
            }
            $cont = 5;//contador para eficacia
        }
        //EFICACIA
        $tabla .= '<tr>';
        $tabla .= '<td style="background:#B0E0E6">' . $mat_ejec[$cont][0] . '</td>';
        for ($j = 1; $j <= 12; $j++) {
            $tabla .= '<td style="background:#B0E0E6">';
            $tabla .= number_format(($mat_ejec[$cont][$j]), 2, ',', '.') . '%';
            $efi = round($mat_ejec[$cont][$j]);//EFICACIA
            $tabla .= '</td>';
            $data_efi = $this->get_eficacia($efi);
            $ef_menor .= $data_efi['menor'];
            $ef_entre .= $data_efi['entre'];
            $ef_mayor .= $data_efi['mayor'];

        }
        $tabla .= '</tr>';
        $dato['tabla'] = $tabla;
        $dato['graf_prog'] = $graf_prog;
        $dato['graf_ejec'] = $graf_ejec;
        $dato['ef_menor'] = $ef_menor;
        $dato['ef_entre'] = $ef_entre;
        $dato['ef_mayor'] = $ef_mayor;
        return $dato;
    }

    //matriz de la programacion mensual del producto terminal
    function mat_prog_mes_pterminal($pt_id, $meta, $linea_base)
    {
        $lista_prog = $this->malerta_pterminal->prog_mensual_pterminal($pt_id);
        if (count($lista_prog) != 0) {
            $lista_prog = $this->malerta_pterminal->prog_mensual_pterminal($pt_id)[0];
            $prog_acumulado = $linea_base;
            $mat_prog[0][0] = 'Programación';
            $mat_prog[1][0] = 'Programación Acumulada';
            $mat_prog[2][0] = 'Programación Acumulada Porcentual [%]';
            for ($i = 1; $i <= 12; $i++) {
                $puntero_bd = 'fis' . $i;
                $prog_fisica = $lista_prog[$puntero_bd];
                $mat_prog[0][$i] = $prog_fisica;//programacion fisica
                $prog_acumulado += $prog_fisica;
                $mat_prog[1][$i] = $prog_acumulado;//programacion acumulada
                $mat_prog[2][$i] = ($meta == 0) ? 0 : (($prog_acumulado / $meta) * 100);//programacion acumulada en porcentaje
            }
            return $mat_prog;
        } else {
            return 0;
        }

    }

    //verificar si la ejecucion es absoluto o relativo
    function mat_ejec_mes_pterminal($pt_id, $meta, $linea_base, $indicador, $denominador, $mat_prog)
    {
        if ($indicador == 1) {
            $matr = $this->mat_ejec_abs($pt_id, $linea_base, $meta, $mat_prog);
        } else {
            $matr = $this->mat_ejec_rel($pt_id, $linea_base, $meta, $denominador, $mat_prog);
        }
        return $matr;
    }

    //ejecucion mensual de tipo absoluto
    function mat_ejec_abs($pt_id, $linea_base, $meta, $mat_prog)
    {
        $lista_ejec = $this->malerta_pterminal->ejec_abs_pterminal($pt_id);
        if (count($lista_ejec) != 0) {
            $lista_ejec = $this->malerta_pterminal->ejec_abs_pterminal($pt_id)[0];
            $ejec_acumulado = $linea_base;
            $mat_ejec[0][0] = 'Ejecución';
            $mat_ejec[1][0] = 'Ejecución Acumulada';
            $mat_ejec[2][0] = 'Ejecución Acumulada Porcentual [%]';
            $mat_ejec[3][0] = 'EFICACIA';
            for ($i = 1; $i <= 12; $i++) {
                $puntero_bd = 'fis' . $i;
                $ejec_fisica = $lista_ejec[$puntero_bd];
                $mat_ejec[0][$i] = $ejec_fisica;//ejecucion fisica
                $ejec_acumulado += $ejec_fisica;
                $mat_ejec[1][$i] = $ejec_acumulado;//ejecucion acumulada
                $mat_ejec[2][$i] = ($meta == 0) ? 0 : (($ejec_acumulado / $meta) * 100);//ejecucion acumulada en porcentaje
                $mat_ejec[3][$i] = ($mat_prog[2][$i] == 0) ? 0 : ($mat_ejec[2][$i] / $mat_prog[2][$i]) * 100;//EFICACIA
            }
            return $mat_ejec;
        } else {
            return 0;
        }

    }

    //ejecucion mensual de tipo relativo
    function mat_ejec_rel($pt_id, $linea_base, $meta, $denominador, $mat_prog)
    {
        $lista_ejec = $this->malerta_pterminal->ejec_rel_pterminal($pt_id);
        if (count($lista_ejec) != 0) {
            $lista_ejec = $this->malerta_pterminal->ejec_rel_pterminal($pt_id)[0];
            $ejec_acumulado = $linea_base;
            $mat_ejec[0][0] = 'CASO FAVORABLE';
            $mat_ejec[1][0] = 'CASO DESFAVORABLE';
            $mat_ejec[2][0] = 'EJECUCIÓN';
            $mat_ejec[3][0] = 'EJECUCIÓN ACUMUALADA';
            $mat_ejec[4][0] = 'EJECUCIÓN ACUMUALADA PORCENTUAL [%]';
            $mat_ejec[5][0] = 'EFICACIA';
            if ($denominador == 0) {//variable
                for ($i = 1; $i <= 12; $i++) {
                    $punt_fav = 'fav' . $i; //puntero
                    $punt_des = 'des' . $i;
                    $favorable = $lista_ejec[$punt_fav];
                    $desfavorable = $lista_ejec[$punt_des];
                    $mat_ejec[0][$i] = $favorable;
                    $mat_ejec[1][$i] = $desfavorable;
                    $ejec_fisica = ($desfavorable <= 0) ? 0 : ($favorable / $desfavorable) * $mat_prog[0][$i];
                    $mat_ejec[2][$i] = $ejec_fisica;
                    $ejec_acumulado += $ejec_fisica;
                    $mat_ejec[3][$i] = $ejec_acumulado;//ejecucion acumulada
                    $mat_ejec[4][$i] = ($meta == 0) ? 0 : (($ejec_acumulado / $meta) * 100);//ejecucion acumulada en porcentaje
                    $mat_ejec[5][$i] = ($mat_prog[2][$i] == 0) ? 0 : ($mat_ejec[4][$i] / $mat_prog[2][$i]) * 100;//EFICACIA
                }
            } else {//fijo
                for ($i = 1; $i <= 12; $i++) {
                    $punt_fav = 'fav' . $i; //puntero
                    $punt_des = 'des' . $i;
                    $favorable = $lista_ejec[$punt_fav];
                    $desfavorable = $lista_ejec[$punt_des];
                    $mat_ejec[0][$i] = $favorable;
                    $mat_ejec[1][$i] = $desfavorable;
                    $ejec_fisica = ($desfavorable <= 0) ? 0 : ($favorable / $desfavorable) * 100;
                    $mat_ejec[2][$i] = $ejec_fisica;
                    $ejec_acumulado += $ejec_fisica;
                    $mat_ejec[3][$i] = $ejec_acumulado;//ejecucion acumulada
                    $mat_ejec[4][$i] = ($meta == 0) ? 0 : (($ejec_acumulado / $meta) * 100);//ejecucion acumulada en porcentaje
                    $mat_ejec[5][$i] = ($mat_prog[2][$i] == 0) ? 0 : ($mat_ejec[4][$i] / $mat_prog[2][$i]) * 100;//EFICACIA
                }
            }
            return $mat_ejec;
        } else {
            return 0;
        }
    }

    function get_eficacia($efi)
    {
        if ($efi <= 75) {
            $d['menor'] = "{y: " . $efi . ", color: 'red'},";
            $d['entre'] = "{y: 0, color: 'yellow'},";
            $d['mayor'] = "{y: 0, color: 'green'},";
        }
        if ($efi >= 76 && $efi <= 90) {
            $d['entre'] = "{y: " . $efi . ", color: 'yellow'},";
            $d['menor'] = "{y: 0, color: 'red'},";
            $d['mayor'] = "{y: 0, color: 'green'},";
        }
        if ($efi >= 91) {
            $d['mayor'] = "{y: " . $efi . ", color: 'green'},";
            $d['entre'] = "{y: 0, color: 'yellow'},";
            $d['menor'] = "{y: 0, color: 'red'},";
        }
        return $d;
    }

    //seguimiento del producto terminal a nivel objetivo de gestion
    function nivel_ogestion($poa_id)
    {
        $data['dato_poa'] = $this->mpoa->dato_poa_id($poa_id);
        $data['lista_ogestion'] = $this->tabla_nivel_ogestion($poa_id);

        $meses=$this->get_mes($this->session->userdata("mes"));
        $mes = $meses[1];
        $dias = $meses[2];

        $data['id_mes'] = $this->session->userdata("mes");
        $data['mes'] = $mes;

        $ruta = 'reportes/seguimiento/prod_terminal/vnivel_ogestion';
        $this->construir_vista($ruta, $data);
    }

    //nivel objetivo de egstion
    function tabla_nivel_ogestion($poa_id)
    {
        $lista_objgestion = $this->mejec_ogestion_pterminal->lista_ogestion($poa_id);
        $cont = 1;
        $tabla = '';
        $ruta = site_url() . '/rep/graf_oges/' . $poa_id . '/';
        $ruta_img = base_url() . 'assets/ifinal/grafico.png';
        foreach ($lista_objgestion as $row) {
            $tabla .= '<tr>';
            $tabla .= '<td >' . $cont . '</td>';
            $tabla .= '<td >
                            <a href="' . $ruta . $row['o_id'] . '" title="VER PROGRAMACIÓN Y EJECUCIÓN">
                                <img src="' . $ruta_img . '" width="50" height="50" class="img-responsive" title="VER PROGRAMACIÓN Y EJECUCIÓN">
							</a>
					   </td>';
            $tabla .= '<td >' . $row['o_codigo'] . '</td>';
            $tabla .= '<td >' . $row['o_objetivo'] . '</td>';
            $tabla .= '<td >' . $row['fun_nombre'] . ' ' . $row['fun_paterno'] . ' ' . $row['fun_materno'] . '</td>';
            $tabla .= '<td >' . $row['o_ponderacion'] . '</td>';
            $tabla .= '</tr >';
            $cont++;
        }
        return $tabla;
    }

    //grafico a nivel objetivo de gestion
    function grafico_nivel_ogestion($poa_id, $o_id)
    {
        $data['poa_id'] = $poa_id;
        $data['dato_poa'] = $this->mpoa->dato_poa_id($poa_id)[0];
        $data['dato_ogestion'] = $this->mobjetivo_gestion->get_ogestion($o_id)[0];//objetivo de gestion
        $dato_tabla = $this->tabla_prog_ejec_ogestion($o_id);
        $data['tabla_prog_ejec'] = $dato_tabla['tabla'];
        $data['programacion'] = $dato_tabla['graf_prog'];
        $data['ejecucion'] = $dato_tabla['graf_ejec'];
        $data['ef_menor'] = $dato_tabla['ef_menor'];
        $data['ef_entre'] = $dato_tabla['ef_entre'];
        $data['ef_mayor'] = $dato_tabla['ef_mayor'];

        $meses=$this->get_mes($this->session->userdata("mes"));
        $mes = $meses[1];
        $dias = $meses[2];

        $data['id_mes'] = $this->session->userdata("mes");
        $data['mes'] = $mes;

        $ruta = 'reportes/seguimiento/prod_terminal//vgrafico_nivel_ogestion';
        $this->construir_vista($ruta, $data);
    }

    //tabla de programacion y ejecucion del prod. terminal a nivel objetivo de gestion
    function tabla_prog_ejec_ogestion($o_id )
    {
        $lista_pterminal = $this->mp_terminal->lista_pterminal($o_id);
        $mat[0][0] = 'PROGRAMACIÓN ACUMULADA EN PORCENTAJE [%]';
        $mat[1][0] = 'EJECUCIÓN ACUMULADA EN PORCENTAJE [%]';
        $graf[0] = '';//grafico programacion
        $graf[1] = '';//grafico ejecucion
        $dato['ef_menor'] = '';//vector para el grafico de eficacia
        $dato['ef_entre'] = '';//vector para el grafico de eficacia
        $dato['ef_mayor'] = '';//vector para el grafico de eficacia
        for ($i = 1; $i <= 12; $i++) { //iniciar matriz
            $mat[0][$i] = 0;
            $mat[1][$i] = 0;
        }
        foreach ($lista_pterminal as $row) {
            $dato_pterminal = $this->mp_terminal->get_pterminal($row['pt_id'])[0];
            $pt_id = $dato_pterminal['pt_id'];
            $meta = $dato_pterminal['pt_meta'];
            $linea_base = $dato_pterminal['pt_linea_base'];
            $indicador = $dato_pterminal['indi_id'];
            $denominador = $dato_pterminal['pt_denominador'];
            $ponderacion = $dato_pterminal['pt_ponderacion'];
            $mat_prog = $this->mat_prog_mes_pterminal($pt_id, $meta, $linea_base); //matriz de la programacion
            $mat_ejec = $this->mat_ejec_mes_pterminal($pt_id, $meta, $linea_base, $indicador, $denominador, $mat_prog); //matriz de la ejecucion
            for ($j = 1; $j <= 12; $j++) {
                $mat[0][$j] += (count($mat_prog) == 0) ? 0 : ($mat_prog[2][$j] * $ponderacion);
                $mat[1][$j] += (count($mat_ejec) == 0) ? 0 : ($indicador == 1) ? ($mat_ejec[2][$j] * $ponderacion) : ($mat_ejec[4][$j] * $ponderacion);
            }
        }
        $dato['tabla'] = '';
        for ($i = 0; $i <= 1; $i++) {
            $dato['tabla'] .= '<tr>';
            $dato['tabla'] .= '<td style="background-color: #F5F5DC;">' . $mat[$i][0] . '</td>';
            for ($j = 1; $j <= 12; $j++) {
                $dato['tabla'] .= '<td style="background-color: #F5F5DC">' . number_format(($mat[$i][$j] ), 2, ',', '.') . '%</td>';
                $graf[$i] .= round($mat[$i][$j]) . ',';
            }
            $dato['tabla'] .= '</tr>';
        }
        //EFICACIA
        $dato['tabla'] .= '<tr>';
        $dato['tabla'] .= '<td style="background-color: #B0E0E6" >EFICACIA</td>';
        for ($i = 1; $i <= 12; $i++) {//EFICACIA
            $val = ($mat[0][$i] == 0) ? 0 : ($mat[1][$i] / $mat[0][$i]) * 100;
            $dato['tabla'] .= '<td style="background-color: #B0E0E6">' . number_format($val, 2, ',', '.') . '%</td>';
            $data_efic = $this->get_eficacia(round($val) );
            $dato['ef_menor'] .= $data_efic['menor'];
            $dato['ef_entre'] .= $data_efic['entre'];
            $dato['ef_mayor'] .= $data_efic['mayor'];
        }
        $dato['tabla'] .= '</tr>';
        $dato['graf_prog'] = $graf[0];
        $dato['graf_ejec'] = $graf[1];
        return $dato;

    }

    //seguimiento a nivel programa
    function nivel_programa($poa_id){
        $data['dato_poa'] = $this->mpoa->dato_poa_id($poa_id)[0];
        $dato_tabla = $this->tabla_seg_programa($poa_id);
        $data['tabla_prog_ejec'] = $dato_tabla['tabla'];
        $data['programacion'] = $dato_tabla['graf_prog'];
        $data['ejecucion'] = $dato_tabla['graf_ejec'];
        $data['ef_menor'] = $dato_tabla['ef_menor'];
        $data['ef_entre'] = $dato_tabla['ef_entre'];
        $data['ef_mayor'] = $dato_tabla['ef_mayor'];

        $meses=$this->get_mes($this->session->userdata("mes"));
        $mes = $meses[1];
        $dias = $meses[2];

        $data['id_mes'] = $this->session->userdata("mes");
        $data['mes'] = $mes;

        $ruta = 'reportes/seguimiento/prod_terminal/vgrafico_nivel_programa';
        $this->construir_vista($ruta, $data);
    }

    //SEGUIMIENTO DEL PRODUCTO TERMINAL A  NIVEL PROGRAMA
    function tabla_seg_programa($poa_id)
    {
        $lista_objgestion = $this->mejec_ogestion_pterminal->lista_ogestion($poa_id);
        $o_sum[0][0] = 'PROGRAMACIÓN ACUMULADA EN PORCENTAJE [%]';
        $o_sum[1][0] = 'EJECUCIÓN ACUMULADA EN PORCENTAJE [%]';
        $graf[0]= '';//grafico programacion
        $graf[1]= '';//grafico ejecucion
        $dato['ef_menor'] = '';//vector para el grafico de eficacia
        $dato['ef_entre'] = '';//vector para el grafico de eficacia
        $dato['ef_mayor'] = '';//vector para el grafico de eficacia
        for ($i = 1; $i <= 12; $i++) { //iniciar matriz  para sumas de prog y ejec del objetivo de gestion
            $o_sum[0][$i] = 0;
            $o_sum[1][$i] = 0;
        }
        foreach ($lista_objgestion as $o_item) {
            $lista_pterminal = $this->mp_terminal->lista_pterminal($o_item['o_id']);
            for ($i = 1; $i <= 12; $i++) { //iniciar matriz
                $mat[0][$i] = 0;
                $mat[1][$i] = 0;
            }
            foreach ($lista_pterminal as $row) {
                $dato_pterminal = $this->mp_terminal->get_pterminal($row['pt_id'])[0];
                $pt_id = $dato_pterminal['pt_id'];
                $meta = $dato_pterminal['pt_meta'];
                $linea_base = $dato_pterminal['pt_linea_base'];
                $indicador = $dato_pterminal['indi_id'];
                $denominador = $dato_pterminal['pt_denominador'];
                $ponderacion = $dato_pterminal['pt_ponderacion'];
                $mat_prog = $this->mat_prog_mes_pterminal($pt_id, $meta, $linea_base); //matriz de la programacion
                $mat_ejec = $this->mat_ejec_mes_pterminal($pt_id, $meta, $linea_base, $indicador, $denominador, $mat_prog); //matriz de la ejecucion
                for ($j = 1; $j <= 12; $j++) {
                    $mat[0][$j] += (count($mat_prog) == 0) ? 0 : ($mat_prog[2][$j] * $ponderacion);
                    $mat[1][$j] += (count($mat_ejec) == 0) ? 0 : ($indicador == 1) ? ($mat_ejec[2][$j] * $ponderacion) : ($mat_ejec[4][$j] * $ponderacion);
                }
            }
            for ($j = 1; $j <= 12; $j++) { //sumar prog y ejec
                $o_sum[0][$j] += $mat[0][$j] * $o_item['o_ponderacion'];
                $o_sum[1][$j] += $mat[1][$j] * $o_item['o_ponderacion'];
            }
        }
        $dato['tabla'] = '';
        for ($i = 0; $i <= 1; $i++) {
            $dato['tabla'] .= '<tr>';
            $dato['tabla'] .= '<td style="background-color: #F5F5DC;">' . $o_sum[$i][0] . '</td>';
            for ($j = 1; $j <= 12; $j++) {
                $dato['tabla'] .= '<td style="background-color: #F5F5DC">' . number_format(($o_sum[$i][$j]), 2, ',', '.') . '%</td>';
                $graf[$i] .= round($o_sum[$i][$j]) . ',';
            }
            $dato['tabla'] .= '</tr>';
        }
        $dato['tabla'] .= '<tr>';
        $dato['tabla'] .= '<td style="background-color: #B0E0E6" >EFICACIA</td>';
        for ($i = 1; $i <= 12; $i++) {//EFICACIA
            $val = ($o_sum[0][$i] == 0)? 0 : ($o_sum[1][$i] / $o_sum[0][$i])*100;
            $dato['tabla'] .= '<td style="background-color: #B0E0E6">' . number_format($val, 2, ',', '.') . '%</td>';
            $data_efi = $this->get_eficacia(round($val));
            $dato['ef_menor'] .= $data_efi['menor'];
            $dato['ef_entre'] .= $data_efi['entre'];
            $dato['ef_mayor'] .= $data_efi['mayor'];
        }
        $dato['tabla'] .= '</tr>';
        $dato['graf_prog'] = $graf[0];
        $dato['graf_ejec'] = $graf[1];
        return $dato;
    }

    //seguimiento a nivel institucion
    function nivel_institucion(){
        $dato_tabla = $this->tabla_seg_institucion();
        $data['tabla_prog_ejec'] = $dato_tabla['tabla'];
        $data['programacion'] = $dato_tabla['graf_prog'];
        $data['ejecucion'] = $dato_tabla['graf_ejec'];
        $data['ef_menor'] = $dato_tabla['ef_menor'];
        $data['ef_entre'] = $dato_tabla['ef_entre'];
        $data['ef_mayor'] = $dato_tabla['ef_mayor'];

        $meses=$this->get_mes($this->session->userdata("mes"));
        $mes = $meses[1];
        $dias = $meses[2];

        $data['id_mes'] = $this->session->userdata("mes");
        $data['mes'] = $mes;

        $ruta = 'reportes/seguimiento/prod_terminal/vgrafico_nivel_institucion';
        $this->construir_vista($ruta, $data);
    }

    function tabla_seg_institucion(){
        $lista_poa = $this->mejec_ogestion_pterminal->lista_prog();
        $sum_inst[0][0] = 'PROGRAMACIÓN ACUMULADA EN PORCENTAJE [%]';
        $sum_inst[1][0] = 'EJECUCIÓN ACUMULADA EN PORCENTAJE [%]';
        $graf[0]= '';//grafico programacion
        $graf[1]= '';//grafico ejecucion
        $dato['ef_menor'] = '';//vector para el grafico de eficacia
        $dato['ef_entre'] = '';//vector para el grafico de eficacia
        $dato['ef_mayor'] = '';//vector para el grafico de eficacia
        for ($i = 1; $i <= 12; $i++) { //iniciar matriz suma total
            $sum_inst[0][$i] = 0;//total programacion
            $sum_inst[1][$i] = 0;//total ejecucion
        }
        foreach ($lista_poa as $aper_item) {
            $lista_objgestion = $this->mejec_ogestion_pterminal->lista_ogestion($aper_item['poa_id']);
            for ($i = 1; $i <= 12; $i++) { //iniciar matriz  para sumas de prog y ejec del objetivo de gestion
                $o_sum[0][$i] = 0;
                $o_sum[1][$i] = 0;
            }
            foreach ($lista_objgestion as $o_item) {
                $lista_pterminal = $this->mp_terminal->lista_pterminal($o_item['o_id']);
                for ($i = 1; $i <= 12; $i++) { //iniciar matriz
                    $mat[0][$i] = 0;
                    $mat[1][$i] = 0;
                }
                foreach ($lista_pterminal as $row) {
                    $dato_pterminal = $this->mp_terminal->get_pterminal($row['pt_id'])[0];
                    $pt_id = $dato_pterminal['pt_id'];
                    $meta = $dato_pterminal['pt_meta'];
                    $linea_base = $dato_pterminal['pt_linea_base'];
                    $indicador = $dato_pterminal['indi_id'];
                    $denominador = $dato_pterminal['pt_denominador'];
                    $ponderacion = $dato_pterminal['pt_ponderacion'];
                    $mat_prog = $this->mat_prog_mes_pterminal($pt_id, $meta, $linea_base); //matriz de la programacion
                    $mat_ejec = $this->mat_ejec_mes_pterminal($pt_id, $meta, $linea_base, $indicador, $denominador, $mat_prog); //matriz de la ejecucion
                    for ($j = 1; $j <= 12; $j++) {
                        $mat[0][$j] += (count($mat_prog) == 0) ? 0 : ($mat_prog[2][$j] * $ponderacion);
                        $mat[1][$j] += (count($mat_ejec) == 0) ? 0 : ($indicador == 1) ? ($mat_ejec[2][$j] * $ponderacion) : ($mat_ejec[4][$j] * $ponderacion);
                    }
                }
                for ($j = 1; $j <= 12; $j++) { //sumar prog y ejec
                    $o_sum[0][$j] += $mat[0][$j] * $o_item['o_ponderacion'];
                    $o_sum[1][$j] += $mat[1][$j] * $o_item['o_ponderacion'];
                }
            }
            for ($j = 1; $j <= 12; $j++) { //sumar prog y ejec
                $sum_inst[0][$j] += $o_sum[0][$j] * $aper_item['aper_ponderacion'];
                $sum_inst[1][$j] += $o_sum[1][$j] * $aper_item['aper_ponderacion'];
            }
        }
        $dato['tabla'] = '';
        for ($i = 0; $i <= 1; $i++) {
            $dato['tabla'] .= '<tr>';
            $dato['tabla'] .= '<td style="background-color: #F5F5DC;">' . $sum_inst[$i][0] . '</td>';
            for ($j = 1; $j <= 12; $j++) {
                $dato['tabla'] .= '<td style="background-color: #F5F5DC">' . number_format(($sum_inst[$i][$j]), 2, ',', '.') . '%</td>';
                $graf[$i] .= round($sum_inst[$i][$j]) . ',';
            }
            $dato['tabla'] .= '</tr>';
        }
        $dato['tabla'] .= '<tr>';
        $dato['tabla'] .= '<td style="background-color: #B0E0E6" >EFICACIA</td>';
        for ($i = 1; $i <= 12; $i++) {//EFICACIA
            $val = ($sum_inst[0][$i] == 0)? 0 : ($sum_inst[1][$i] / $sum_inst[0][$i])*100;
            $dato['tabla'] .= '<td style="background-color: #B0E0E6">' . number_format($val, 2, ',', '.') . '%</td>';
            $data_efi = $this->get_eficacia(round($val));
            $dato['ef_menor'] .= $data_efi['menor'];
            $dato['ef_entre'] .= $data_efi['entre'];
            $dato['ef_mayor'] .= $data_efi['mayor'];
        }
        $dato['tabla'] .= '</tr>';
        $dato['graf_prog'] = $graf[0];
        $dato['graf_ejec'] = $graf[1];
        return $dato;
    }

    //FUNCION PARA CONSTRUIR MI VISTA
    function construir_vista($ruta, $data)
    {
        //----------------------------------- MENU-------------------------------
        $menu['enlaces'] = $this->menu->get_enlaces();
        $menu['subenlaces'] = $this->menu->get_sub_enlaces();
        $menu['titulo'] = 'REPORTES';
        //-----------------------------------------------------------------------
        //armar vista
        $this->load->view('includes/header');
        $this->load->view('includes/menu_lateral', $menu);
        $this->load->view($ruta, $data);//contenido
        $this->load->view('includes/footer');
    }
    public function get_mes($mes_id)
    {
      $mes[1]='ENERO';
      $mes[2]='FEBRERO';
      $mes[3]='MARZO';
      $mes[4]='ABRIL';
      $mes[5]='MAYO';
      $mes[6]='JUNIO';
      $mes[7]='JULIO';
      $mes[8]='AGOSTO';
      $mes[9]='SEPTIEMBRE';
      $mes[10]='OCTUBRE';
      $mes[11]='NOVIEMBRE';
      $mes[12]='DICIEMBRE';

      $dias[1]='31';
      $dias[2]='28';
      $dias[3]='31';
      $dias[4]='30';
      $dias[5]='31';
      $dias[6]='30';
      $dias[7]='31';
      $dias[8]='31';
      $dias[9]='30';
      $dias[10]='31';
      $dias[11]='30';
      $dias[12]='31';

      $valor[1]=$mes[$mes_id];
      $valor[2]=$dias[$mes_id];

      return $valor;
    }

}