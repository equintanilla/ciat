<?php
class Reportes_vistas extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->model('reportes/model_para_vistas');
        $this->load->model('mantenimiento/mpoa');
        $this->load->model('programacion/marco_estrategico/mobjetivos');
        //llamar a mi menu
        $this->load->library('menu');
        $this->menu->const_menu(7);
    }

    public function frm_analisis_ejecucion($value='')
    {
        $data['lista_poa'] = $this->mpoa->lista_poa();
        $meses=$this->get_mes($this->session->userdata("mes"));
        $mes = $meses[1];
        $dias = $meses[2];

        $data['id_mes'] = $this->session->userdata("mes");
        $data['mes'] = $mes;
        $ruta = 'reportes/frm_poa_ejecucion/analisis_situacion_reporte';
        $this->construir_vista($ruta, $data);
    }

    public function objetivo_estrategicos($n)
    {
        $data['n'] = $n;
        if($n==1){
            // Objetivo Estrategico (POA PROGRAMACION)
            $data['titulo'] = 'Formularios POA Programación Objetivos Estrategicos';
            $ruta = 'reportes/frm_poa_programacion/obj_estrategicos';
            $this->construir_vista($ruta, $data);
        }
        else{
            if ($n==2) {
                // Objetivo Estrategico (POA PROGRAMACION EJECUCION)
                $data['titulo'] = 'Formularios POA Ejecución Objetivos Estrategicos';
                $ruta = 'reportes/frm_poa_programacion/obj_estrategicos';
                $this->construir_vista($ruta, $data);
            }
            else{
                redirect('admin/dm/7/','refresh');
            }
        }
    }
    public function operaciones()
    {
        $gestion = $this->session->userdata('gestion');
        $operaciones = $this->model_para_vistas->get_operaciones($gestion); 
        $data['operaciones'] = $operaciones->result_array();
        $data['titulo']='OPERACIONES';
        $ruta = 'reportes/frm_poa_programacion/operaciones';
        $this->construir_vista($ruta, $data);
    }
    public function reporte_acciones()
    {
        $data['lista_poa'] = $this->mpoa->lista_poa();
        $data['n'] = 0;
        $ruta = 'reportes/frm_poa_programacion/acciones';
        $this->construir_vista($ruta, $data);
    }
    //FUNCION PARA CONSTRUIR MI VISTA
    function construir_vista($ruta, $data)
    {
        //----------------------------------- MENU-------------------------------
        $menu['enlaces'] = $this->menu->get_enlaces();
        $menu['subenlaces'] = $this->menu->get_sub_enlaces();
        $menu['titulo'] = 'REPORTES';
        //-----------------------------------------------------------------------
        //armar vista
        $this->load->view('includes/header');
        $this->load->view('includes/menu_lateral', $menu);
        $this->load->view($ruta, $data);//contenido
        $this->load->view('includes/footer');
    }
    //PARA VISTA DE OBJETIVOS DE GESTION Y PRODUCTOS TERMINAL
    function objetivo_gestion_producto_terminal(){
        $data['lista_poa'] = $this->mpoa->lista_poa();
        $data['n'] = 0;
        $ruta = 'reportes/frm_poa_programacion/obj_gest_prod_term';
        $this->construir_vista($ruta, $data);
    }
    function objetivo_gestion_producto_terminal_ejecutado(){
        $data['lista_poa'] = $this->mpoa->lista_poa();
        $data['n'] = 1;
        $ruta = 'reportes/frm_poa_programacion/obj_gest_prod_term';
        $this->construir_vista($ruta, $data);
    }
    //LISTA DE OBJETIVOS ESTRATEGICOS DEL POA
    function lista_objetivos($poa_id){
        //-----------------------------------------------------------------------
        $data['dato_poa'] = $this->mpoa->dato_poa_id($poa_id);
        $lista_objetivos = $this->mobjetivos->lista_obje_poa($poa_id);
        $temporalizacion = array();
        foreach ($lista_objetivos as $row1) {
            $obje_id = $row1['obje_id'];// id de mi objetivo estrategico
            $gestion_inicial = $row1['obje_gestion_curso'];// id de mi objetivo estrategico
            $linea_base = $row1['obje_linea_base'];
            $meta = $row1['obje_meta']; //variable meta
            $temporalizacion[$obje_id] = $this->progracion_gestion($obje_id, $gestion_inicial, $linea_base, $meta);
        }
        $data['lista_objetivos'] = $lista_objetivos;
        $data['temporalizacion'] = $temporalizacion;

        $ruta = 'programacion/prog_poa/red_objetivos/vlista_objetivos_poa';
        $this->construir_vista($ruta, $data);
    }
    //OBTENER TEMPORALIZACION DE PROGRMACION
    function progracion_gestion($obje_id, $gestion_inicial, $linea_base, $meta)
    {
        $cont = 1;
        $prog_acumulado = $linea_base; //variable de programacion acumulada
        for ($i = $gestion_inicial; $i <= ($gestion_inicial + 4); $i++) {
            $puntero_prog = 'prog' . $cont;//PUNTERO PARA LISTAR LA PROGRAMACION
            $dat_prog = $this->mobjetivos->get_prog_obj($obje_id, $i);
            $dato_programado = $dat_prog[0]['opm_programado'];
            $temporalizacion[$puntero_prog] = $dato_programado;//matriz
            //PROGRAMADO ACUMULADO
            $puntero_prog_acumulado = 'p_acumulado' . $cont;//PUNTERO PARA LA PROGRAMACION ACUMULADA
            $prog_acumulado += $dato_programado;
            $temporalizacion[$puntero_prog_acumulado] = $prog_acumulado;//matriz
            //PROGRAMA ACUMULADO PORECENTUAL
            $puntero_pa_porcentual = 'pa_porc' . $cont;//PUNTERO PARA LA PROGRAMACION ACUMULADA PORCENTUAL
	      if($meta == 0){
                $pa_porcentual = 0;
            }else{
                $pa_porcentual = (($prog_acumulado / $meta) * 100);
            }	           
            $temporalizacion[$puntero_pa_porcentual] = $pa_porcentual;//matriz
            $cont++;
        }
        return $temporalizacion;
    }

    public function get_mes($mes_id)
    {
      $mes[1]='ENERO';
      $mes[2]='FEBRERO';
      $mes[3]='MARZO';
      $mes[4]='ABRIL';
      $mes[5]='MAYO';
      $mes[6]='JUNIO';
      $mes[7]='JULIO';
      $mes[8]='AGOSTO';
      $mes[9]='SEPTIEMBRE';
      $mes[10]='OCTUBRE';
      $mes[11]='NOVIEMBRE';
      $mes[12]='DICIEMBRE';

      $dias[1]='31';
      $dias[2]='28';
      $dias[3]='31';
      $dias[4]='30';
      $dias[5]='31';
      $dias[6]='30';
      $dias[7]='31';
      $dias[8]='31';
      $dias[9]='30';
      $dias[10]='31';
      $dias[11]='30';
      $dias[12]='31';

      $valor[1]=$mes[$mes_id];
      $valor[2]=$dias[$mes_id];

      return $valor;
    }
}