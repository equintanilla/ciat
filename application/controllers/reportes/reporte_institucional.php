<?php
define("DOMPDF_ENABLE_REMOTE", true);
define("DOMPDF_TEMP_DIR", "/tmp");
class Reporte_institucional extends CI_Controller { 
  public function __construct ()
    { 
        parent::__construct();
        if($this->session->userdata('fun_id')!=null){
        $this->load->library('pdf');
        $this->load->library('pdf2');

        $this->load->model('reportes/model_objetivo');
        $this->load->model('programacion/model_proyecto');
        $this->load->model('mantenimiento/mpoa');
        $this->load->model('programacion/model_reporte');
        $this->load->model('programacion/model_reporte_global');
        $this->load->model('programacion/mreporte_proy');
        $this->load->model('menu_modelo');
        $this->load->model('Users_model','',true);
        }else{
            redirect('/','refresh');
        }
    }
        /*============================== FICHA TECNICA CABECERA =================================*/
       private $estilo_vertical = '<style>
        body{
            font-family: sans-serif;
            }
        @page {
        margin: 40px 40px;
        }
        header {
            position: fixed;
            left: 0px;
            top: -20px;
            right: 0px;
            height: 20px;
            background-color: #fff;
            }
        header h1{
            margin: 5px 0;
        }
        header h2{
            margin: 0 0 10px 0;
        }
        th{
                padding: 1px;
                text-align: center;
                font-size: 9px;
                color: #ffffff;
            }
        footer {
            position: fixed;
            left: 0px;
            bottom: -82px;
            right: 0px;
            height: 100px;
            border-bottom: 1px solid #ddd;
        }
        footer .page:after {
            content: counter(page) ;
        }
        footer table {
            width: 100%;
        }
        footer p {
            text-align: right;
        }
        footer .izq {
            text-align: left;
        }
        table{
            font-size: 7px;
            width: 100%;
            background-color:#fff;
        }
        th, td {
                padding: 1px;
                text-align: center;
                font-size: 7px;
            }
        .mv{font-size:10px;}
        .siipp{width:200px;}
        .header_table {
            color: #ffffff;
            text-align: center;
            align-items: center;
            align-self: center;
            font-weight: bold;
            background-color: #696969;
            align-content: center;
        }
        .header_subtable {
            color: #ffffff;
            text-align: center;
            align-items: center;
            align-self: center;
            font-weight: bold;
            background-color: #a59393;
            align-content: center;
        }
        .titulo_pdf {
            text-align: left;
            font-size: 10px;
        }
        .datos_principales {
            text-align: center;
            font-size: 9px;
        }
        .pdes_titulo{
            text-decoration: underline;
            font-weight: bold;
            text-transform: uppercase;
        }
        .td_pdes{
            text-align: left;  
        }

        .indi_desemp{
            font-size: 14px;
            border: 1px solid #CCC;
            background-color: #E0E0E0;
            padding: .5em;
        }
        table.fixed { table-layout:fixed; }
        table.fixed td { overflow: hidden; }
        .sub_table{
            border-bottom:1px solid black;
        }
        .contenedor_principal{
            border-style: solid;
            border-width:1px;
        }
        .titulo_dictamen{
            padding:4px;
            background-color: #454545;
            border-style: solid;margin: 2px;
            border-width: 1px;
            color: white;
            font-size: 10px;
            border-color: black;
        }
        .contenedor_datos{
            padding:0px;
            margin:1px;
        }
        .table_contenedor{
            border-style: solid;
            margin: 0px;
            padding: 1px;
            border-width: 1px;
            border-collapse: collapse;
        }
        .collapse_t td{
            border-style: solid;
            margin: 0px;
            padding: 3px;
            border-width: 1px;
            border-collapse: collapse;
        }
        .fila_unitaria{
            text-align:left;
        }
        .lista{
            text-align:left;
            padding-left: 8;
            list-style-type:square;
            margin:2px;
        }
    </style>';
 
  /*=========================================================================================================================*/
  
    /*========= SELECCION DE OPCIONES - REPORTE INSTITUCIONAL =================*/
    public function seleccion_reporte_institucional()
    { 
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++)
        {
             $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        
        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];
        $verif = $this->model_reporte->verif_ejecucion($this->session->userdata('mes'),$this->session->userdata('gestion'));
        $data['verif'] = $verif;
        
        $this->load->view('admin/reportes/reporte_institucional/select_accion', $data);
    }
    /*========================================= VALIDAR SELECCION DE ACCIONES================================================================================*/
    public function validar_seleccion()
    { 
        if($this->input->server('REQUEST_METHOD') === 'POST') 
        {
            if($this->input->post('p1')=='on'){$p1=1;}else{$p1=0;}
            if($this->input->post('p2')=='on'){$p2=1;}else{$p2=0;}
            if($this->input->post('p3')=='on'){$p3=1;}else{$p3=0;}
            if($this->input->post('p4')=='on'){$p4=1;}else{$p4=0;}

            redirect('admin/rep/mis_acciones/'.$p1.'/'.$p2.'/'.$p3.'/'.$p4.'');
        }
    }

    /*====================================== LISTA DE ACCIONES SELECCIONADAS ========================================*/
    public function mis_acciones_seleccionadas($p1,$p2,$p3,$p4)
    { 
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++)
        {
             $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;

        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];
        $data['p1'] = $p1; //// proyectos de inversion
        $data['p2'] = $p2; //// programas recurrentes
        $data['p3'] = $p3; //// programas no recurrentes
        $data['p4'] = $p4; //// accion de funcionamiento

        $pr1='';$pr2='';$pr3='';$pr4='';
        if($p1==1){$pr1='PROYECTO DE INVERSI&Oacute;N';}
        if($p2==1){$pr2=' - PROGRAMA RECURRENTE';}
        if($p3==1){$pr3=' - PROGRAMA NO RECURRENTE';}
        if($p4==1){$pr4=' - ACCI&Oacute;N DE FUNCIONAMIENTO';}
        
        $data['pr1']=$pr1;
        $data['pr2']=$pr2;
        $data['pr3']=$pr3;
        $data['pr4']=$pr4;

        $ppto_total=$this->suma_total_ptto(0,$p1,$p2,$p3,$p4); //// suma total de pptos

        $tp_accion = $this->model_reporte_global->tip_proy($p1,$p2,$p3,$p4);//lista de acciones
        $inv=0;$pinv=0;$total=0;$ppto_i=0;$ppto_m=0;$ppto_v=0;$p=0;$ppto_e=0;$ppto_s=0;$e=0;
        $tabla1 = '';
        foreach($tp_accion  as $row)
        {   
            $tabla1 .= '<tr>';
            $tabla1 .= '<td>'.$row['tp_tipo'].'</td>';
            $ctdad=$this->cantidad_accion($row['tp_id']);
            if($p1==1)
            {
            $tabla1 .= '<td>'.$ctdad[1].'</td>'; //// INVERSION
            $tabla1 .= '<td>'.$ctdad[2].'</td>'; //// PREINVERSION  
            }
            $tabla1 .= '<td>'.$ctdad[4].'</td>'; //// TOTAL
            $tabla1 .= '<td>'.number_format($ctdad[5], 2, ',', '.').'</td>'; //// PPTO INICIAL
            $tabla1 .= '<td>'.number_format($ctdad[6], 2, ',', '.').'</td>'; //// PPTO MOD
            $tabla1 .= '<td>'.number_format($ctdad[7], 2, ',', '.').'</td>'; //// PPTO VIG
            $tabla1 .= '<td>'.round((($ctdad[7]/$ppto_total[3])*100),2).' %</td>'; //// %P
            $tabla1 .= '<td>'.number_format($ctdad[9], 2, ',', '.').'</td>'; //// EJECUTADO
            $tabla1 .= '<td>'.number_format($ctdad[10], 2, ',', '.').'</td>'; //// SALDO
            $tabla1 .= '<td>'.$ctdad[11].' %</td>';  //// %E
            $tabla1 .= '</tr>';
           
           $inv=$inv+$ctdad[1];
           $pinv=$pinv+$ctdad[2];
           $total=$total+$ctdad[4];

           $ppto_i=$ppto_i+$ctdad[5];
           $ppto_m=$ppto_m+$ctdad[6];
           $ppto_v=$ppto_v+$ctdad[7];
           $p=$p+round((($ctdad[7]/$ppto_total[3])*100),2);

           $ppto_e=$ppto_e+$ctdad[9];
           $ppto_s=$ppto_s+$ctdad[10];
        }

        $data['inv'] = $inv;
        $data['pinv'] = $pinv;
        $data['total'] = $total;
        $data['ppto_i'] = number_format($ppto_i, 2, ',', '.');
        $data['ppto_m'] = number_format($ppto_m, 2, ',', '.');
        $data['ppto_v'] = number_format($ppto_v, 2, ',', '.');
        $data['p'] = $p;
        $data['ppto_e'] = number_format($ppto_e, 2, ',', '.');
        $data['ppto_s'] = number_format($ppto_s, 2, ',', '.');
        if($ppto_v!=0){$e=round((($ppto_e/$ppto_v)*100),2);}
        $data['e'] = $e;

        $data['accion'] = $tabla1;
        $this->load->view('admin/reportes/reporte_institucional/acciones', $data);
    }
  /*====================================== LISTA DE ACCIONES  ===========================================*/
  public function cantidad_accion($tp_id)
  {
    $accion = $this->model_reporte_global->cant_accion($tp_id,$this->session->userdata("gestion"));
    for($i=1;$i<=12;$i++){$valor[$i]=0;} 
    
    $cont_i=0;$cont_pi=0; $cont_total=0;$ptto_i=0;$ptto_mod=0;$ptto_v=0;$ptto_e=0;
    if(count($accion)!=0)
    {
      foreach($accion as $row)
      {
          if($tp_id==1)
          {
            if($row['fas_id']==2){$cont_i++;}
            elseif($row['fas_id']==1){$cont_pi++;}
          }
          $cont_total++;

          $pr = $this->model_reporte_global->cant_presupuesto_ejecucion($row['proy_id'],$this->session->userdata("mes"),$this->session->userdata("gestion"));
          if(count($pr)!=0)
          {
            $ptto_i=$ptto_i+$pr[0]['inicial']; //// suma presupuesto inicial
            $ptto_mod=$ptto_mod+$pr[0]['modificado']; //// suma presupuesto modificado
            $ptto_v=$ptto_v+$pr[0]['vigente']; //// suma presupuesto vigente
          }


          $pd_m=0;
          for($j=1;$j<=$this->session->userdata("mes");$j++)
          {
            $ejec = $this->model_reporte_global->cant_presupuesto_ejecucion($row['proy_id'],$j,$this->session->userdata("gestion"));
            if(count($ejec)!=0)
            {
              $pd_m=$pd_m+$ejec[0]['ejecutado']; /// suma devengado por meses
            }
          }
          $ptto_e=$ptto_e+$pd_m; //// suma devengado
      }
    }

    $valor[1]=$cont_i; //// INVERSION
    $valor[2]=$cont_pi; //// P INVERSION
    $valor[4]=$cont_total; //// TOTAL

    $valor[5]=$ptto_i; //// PPTO INICIAL
    $valor[6]=$ptto_mod; //// PPTO MODIFICADO
    $valor[7]=$ptto_v; //// PPTO VIGENTE
    if($valor[7]!=0){$valor[8]=round((($valor[5]/$valor[7])*100),2);} //// %P
    $valor[9]=$ptto_e; //// PPTO EJECUTADO
    $valor[10]=$valor[7]-$valor[9]; //// PPTO SALDO
    if($valor[7]!=0){$valor[11]=round((($valor[9]/$valor[7])*100),2);} //// %E

    return $valor;
  }
 /*==================================== SUMA TOTAL DE ACCIONES =================================*/
  public function suma_total_ptto($tipo,$p1,$p2,$p3,$p4)
  {
    for($i=1;$i<=10;$i++){$valor[$i]=1;}
    $ppto_i=0;$ppto_m=0;$ppto_v=0;$ppto_e=0;$ppto_s=0;
    if($tipo==0)////// SUMA REPORTE GLOBAL TIPO DE ACCIONES
    {
        $tp_accion = $this->model_reporte_global->tip_proy($p1,$p2,$p3,$p4);//lista de acciones
        foreach($tp_accion  as $row)
        {
          $cantidad=$this->cantidad_accion($row['tp_id']);
          $ppto_i=$ppto_i+$cantidad[5];$ppto_m=$ppto_m+$cantidad[6];$ppto_v=$ppto_v+$cantidad[7];$ppto_e=$ppto_e+$cantidad[9];$ppto_s=$ppto_s+$cantidad[10];
        }   

    }
    elseif($tipo==1) //// ACCIONES POR UNIDADES
    {
      $u_ejecutoras = $this->model_reporte_global->unidades_ejecutoras_global($this->session->userdata('gestion'),$p1,$p2,$p3,$p4);//lista de unidades ejecutoras 
      foreach($u_ejecutoras  as $row)
      {   
        $cantidad=$this->cantidad_ptto_ejec(1,$row['uni_id'],$this->session->userdata('gestion'),$this->session->userdata('mes'),$p1,$p2,$p3,$p4);
        $ppto_i=$ppto_i+$cantidad[9];$ppto_m=$ppto_m+$cantidad[10];$ppto_v=$ppto_v+$cantidad[11];$ppto_e=$ppto_e+$cantidad[12];$ppto_s=$ppto_s+$cantidad[13];
      }
    }
    elseif($tipo==2) //// ACCIONES POR ESTADOS DEL PROYECTO
    {
      $est_proy = $this->model_reporte_global->estado_proyecto();//lista de estados
      foreach($est_proy  as $row)
      {  
        $cantidad=$this->cantidad_ptto_ejec(2,$row['ep_id'],$this->session->userdata('gestion'),$this->session->userdata('mes'),$p1,$p2,$p3,$p4);
        $ppto_i=$ppto_i+$cantidad[9];$ppto_m=$ppto_m+$cantidad[10];$ppto_v=$ppto_v+$cantidad[11];$ppto_e=$ppto_e+$cantidad[12];$ppto_s=$ppto_s+$cantidad[13];
      }
    }
    elseif($tipo==3) //// ACCIONES POR EJE PROGRAMATICA
    {
      $eje_prog = $this->model_reporte_global->eje_programatica();//lista de ejes programaticos
      foreach($eje_prog  as $row)
      {  
        $cantidad=$this->cantidad_ptto_ejec(3,$row['ptdi_id'],$this->session->userdata('gestion'),$this->session->userdata('mes'),$p1,$p2,$p3,$p4);
        $ppto_i=$ppto_i+$cantidad[9];$ppto_m=$ppto_m+$cantidad[10];$ppto_v=$ppto_v+$cantidad[11];$ppto_e=$ppto_e+$cantidad[12];$ppto_s=$ppto_s+$cantidad[13];
      }
    }
    elseif($tipo==4) //// ACCIONES POR REGIONES
    {
      $region = $this->model_reporte_global->regiones();//lista de Regiones
      foreach($region  as $row)
      {  
        $cantidad=$this->cantidad_ptto_ejec(4,$row['reg_id'],$this->session->userdata('gestion'),$this->session->userdata('mes'),$p1,$p2,$p3,$p4);
        $ppto_i=$ppto_i+$cantidad[9];$ppto_m=$ppto_m+$cantidad[10];$ppto_v=$ppto_v+$cantidad[11];$ppto_e=$ppto_e+$cantidad[12];$ppto_s=$ppto_s+$cantidad[13];
      }
    }
    elseif($tipo==5) //// ACCIONES POR MUNICIPIOS
    {
      $muni = $this->model_reporte_global->municipios();//lista de municipios
      foreach($muni  as $row)
      {  
        $cantidad=$this->cantidad_ptto_ejec(5,$row['muni_id'],$this->session->userdata('gestion'),$this->session->userdata('mes'),$p1,$p2,$p3,$p4);
        $ppto_i=$ppto_i+$cantidad[9];$ppto_m=$ppto_m+$cantidad[10];$ppto_v=$ppto_v+$cantidad[11];$ppto_e=$ppto_e+$cantidad[12];$ppto_s=$ppto_s+$cantidad[13];
      }
    }
    elseif($tipo==6) //// ACCIONES POR PROVINCIAS
    {
      $prov = $this->model_reporte_global->provincias();//lista de Provincias
      foreach($prov  as $row)
      {  
        $cantidad=$this->cantidad_ptto_ejec(6,$row['prov_id'],$this->session->userdata('gestion'),$this->session->userdata('mes'),$p1,$p2,$p3,$p4);
        $ppto_i=$ppto_i+$cantidad[9];$ppto_m=$ppto_m+$cantidad[10];$ppto_v=$ppto_v+$cantidad[11];$ppto_e=$ppto_e+$cantidad[12];$ppto_s=$ppto_s+$cantidad[13];
      }
    }

    if($ppto_i!=0){$valor[1]=$ppto_i;} ////// PPTO INICIAL
    if($ppto_m!=0){$valor[2]=$ppto_m;} ///// PPTO MODIFICADO
    if($ppto_v!=0){$valor[3]=$ppto_v;} ///// PPTO VIGENTE
    if($ppto_e!=0){$valor[4]=$ppto_e;} ///// PPTO EJECUTADO
    if($ppto_s!=0){$valor[5]=$ppto_s;} ///// SALDO

    return $valor;
  }
  public function cantidad_ptto_ejec($tipo,$id,$gestion,$mes,$p1,$p2,$p3,$p4)
    {
      if($tipo==1) ///// UNIDADES EJECUTORAS
      {
        $accion = $this->model_reporte_global->proyectos_ue_global($id,$gestion,$p1,$p2,$p3,$p4);//lista de acciones
      }
      elseif($tipo==2) ///// ESTADO DE PROYECTOS
      {
        $accion = $this->model_reporte_global->proyectos_est_global($id,$gestion,$p1,$p2,$p3,$p4);//lista de acciones en ejeucion
      }
      elseif($tipo==3) ////// EJE PROGRAMATICO
      {
        $accion = $this->model_reporte_global->proyectos_eje_global($id,$gestion,$p1,$p2,$p3,$p4);//lista de acciones en ejeucion
      }
      elseif($tipo==4)  //// POR REGION
      {
        $accion = $this->model_reporte_global->proyectos_reg_global($id,$gestion,$p1,$p2,$p3,$p4);//lista de acciones en ejeucion
      }
      elseif($tipo==5) ///// POR MUNICIPIO
      {
        $accion = $this->model_reporte_global->proyectos_muni_global($id,$gestion,$p1,$p2,$p3,$p4);//lista de acciones en ejeucion
      }
      elseif($tipo==6) ///// POR PROVINCIA
      {
        $accion = $this->model_reporte_global->proyectos_prov_global($id,$gestion,$p1,$p2,$p3,$p4);//lista de acciones en ejeucion
      }
      
      for($i=1;$i<=14;$i++){$valor[$i]=0;}
      
      $cont_i=0;$cont_pi=0;
      $cont_pri=0;$cont_pr=0;$cont_pnr=0;$cont_af=0;$total=0;$ptto_i=0;$ptto_mod=0;$ptto_v=0;$ptto_e=0;
      if(count($accion)!=0)
      { 
        foreach($accion  as $row)
        {
          if($row['tp_id']==1)
          {
            if($row['fas_id']==2){$cont_i++;}
            elseif($row['fas_id']==1){$cont_pi++;}

            $cont_pri++;
          }
          elseif ($row['tp_id']==2) {$cont_pr++;}
          elseif ($row['tp_id']==3) {$cont_pnr++;}
          elseif ($row['tp_id']==4) {$cont_af++;}
          $total++;

          $pr = $this->model_reporte_global->cant_presupuesto_ejecucion($row['proy_id'],$mes,$gestion); ///// valores financieros del mes actual
          if(count($pr)!=0)
          {
            $ptto_i=$ptto_i+$pr[0]['inicial']; //// suma presupuesto inicial
            $ptto_mod=$ptto_mod+$pr[0]['modificado']; //// suma presupuesto modificado
            $ptto_v=$ptto_v+$pr[0]['vigente']; //// suma presupuesto vigente
          }


          $pd_m=0;
          for($j=1;$j<=$mes;$j++)
          {
            $ejec = $this->model_reporte_global->cant_presupuesto_ejecucion($row['proy_id'],$j,$gestion); //// valor ejecuicon a los meses
            if(count($ejec)!=0)
            {
              $pd_m=$pd_m+$ejec[0]['ejecutado']; /// suma devengado por meses
            }
          }
          $ptto_e=$ptto_e+$pd_m; //// suma devengado

        }
      }
      
      $valor[1]=$cont_i;
      $valor[2]=$cont_pi;
      $valor[3]=$cont_pri;

      $valor[4]=$cont_pri;
      $valor[5]=$cont_pr;
      $valor[6]=$cont_pnr;
      $valor[7]=$cont_af;
      $valor[8]=$total;

      $valor[9]=$ptto_i; //// PPTO INICIAL
      $valor[10]=$ptto_mod; //// PPTO MODIFICADO
      $valor[11]=$ptto_v; //// PPTO VIGENTE
      $valor[12]=$ptto_e; //// PPTO EJECUTADO
      $valor[13]=$valor[11]-$valor[12]; //// PPTO SALDO
      if($valor[11]!=0){$valor[14]=round((($valor[12]/$valor[11])*100),2);} //// %E

      return $valor;
    }

    /*--------------------------- DATOS POR ACCIONES ------------------------*/
    public function ptto_ejec_por_accion($proy_id,$mes,$gestion)
    {
        for($i=1;$i<=10;$i++){$valor[$i]=0;}

        $pr = $this->model_reporte_global->cant_presupuesto_ejecucion($proy_id,$mes,$gestion); ///// valores financieros del mes actual
          if(count($pr)!=0)
          {
            $valor[1]=$pr[0]['inicial']; //// presupuesto inicial
            $valor[2]=$pr[0]['modificado']; //// presupuesto modificado
            $valor[3]=$pr[0]['vigente']; //// presupuesto vigente
          }

          $pd_m=0;
          for($j=1;$j<=$mes;$j++)
          {
            $ejec = $this->model_reporte_global->cant_presupuesto_ejecucion($proy_id,$j,$gestion); //// valor ejecuicon a los meses
            if(count($ejec)!=0)
            {
              $pd_m=$pd_m+$ejec[0]['ejecutado']; /// suma devengado por meses
            }
          }
          $valor[4]=$pd_m; //// PPTO EJECUTADO DEVENGADO

        $valor[5]=$valor[3]-$valor[4];
        if($valor[3]!=0){$valor[6]=round((($valor[4]/$valor[3])*100),2);} //// %E

        return $valor;
    }
 /*==================================================================================================*/

  /*========================== SELECCION DE OPCIONES - UNIDAD EJECUTORA ===============================*/
    public function unidad_ejecutora($p1,$p2,$p3,$p4)
    { 
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++)
        {
             $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;

        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];

        $data['p1'] = $p1; //// proyectos de inversion
        $data['p2'] = $p2; //// programas recurrentes
        $data['p3'] = $p3; //// programas no recurrentes
        $data['p4'] = $p4; //// accion de funcionamiento

        $pr1='';$pr2='';$pr3='';$pr4='';
        if($p1==1){$pr1='PROYECTO DE INVERSI&Oacute;N';}
        if($p2==1){$pr2=' - PROGRAMA RECURRENTE';}
        if($p3==1){$pr3=' - PROGRAMA NO RECURRENTE';}
        if($p4==1){$pr4=' - ACCI&Oacute;N DE FUNCIONAMIENTO';}
        
        $data['pr1']=$pr1;
        $data['pr2']=$pr2;
        $data['pr3']=$pr3;
        $data['pr4']=$pr4;

        /*---------------------------------------------------------------------------*/
        $ppto_total=$this->suma_total_ptto(1,$p1,$p2,$p3,$p4); //// suma total de pptos
        $u_ejecutoras = $this->model_reporte_global->unidades_ejecutoras_global($this->session->userdata('gestion'),$p1,$p2,$p3,$p4);//lista de unidades ejecutoras 
        
        $inv=0;$pinv=0;$cont_inv=0;
        $cont_pr=0;$cont_pnr=0;$cont_af=0;$cont_total=0;
        $cont_ppto_i=0;$cont_ppto_m=0;$cont_ppto_v=0;$cont_ppto_e=0;$cont_ppto_s=0;$p=0;$e=0;
        $tabla = '';
        foreach($u_ejecutoras  as $row)
        {   
          $tabla .= '<tr>';
            $tabla .= '<td><center><a href="' . site_url("") . '/admin/rep/accion_por_accion/1/'.$row['uni_id'].'/'.$p1.'/'.$p2.'/'.$p3.'/'.$p4.'" >' . $row['uni_unidad'] . '</a></center></td>';
            $cantidad=$this->cantidad_ptto_ejec(1,$row['uni_id'],$this->session->userdata('gestion'),$this->session->userdata('mes'),$p1,$p2,$p3,$p4);
            if($p1==1){
            $tabla .= '<td>'.$cantidad[1].'</td>'; //// INV
            $tabla .= '<td>'.$cantidad[2].'</td>'; //// PINV                
            }
            if($p2==1){
            $tabla .= '<td>'.$cantidad[5].'</td>'; //// PROG REC                
            }
            if($p3==1){
            $tabla .= '<td>'.$cantidad[6].'</td>'; //// PROG NO REC              
            }
            if($p4==1){
            $tabla .= '<td>'.$cantidad[7].'</td>'; //// AC. FUN          
            }            

            $tabla .= '<td>'.$cantidad[8].'</td>'; //// TOTAL PROG
            $tabla .= '<td>'.number_format($cantidad[9], 2, ',', '.').'</td>';    //// PPTO INICIAL
            $tabla .= '<td>'.number_format($cantidad[10], 2, ',', '.').'</td>';   //// PPTO MOD
            $tabla .= '<td>'.number_format($cantidad[11], 2, ',', '.').'</td>';   //// PPTO VIG
            $tabla .= '<td> '.round((($cantidad[11]/$ppto_total[3])*100),2).'%</td>'; //// % P
            $tabla .= '<td>'.number_format($cantidad[12], 2, ',', '.').'</td>';   //// PPTO EJEC
            $tabla .= '<td>'.$cantidad[14].' %</td>';    //// %E
            $tabla .= '<td>'.number_format($cantidad[13], 2, ',', '.').'</td>';   //// PPTO SALDO
            
          $tabla .= '</tr>';

          $inv=$inv+$cantidad[1];
          $pinv=$pinv+$cantidad[2];

          $cont_inv=$cont_inv+$cantidad[4];
          $cont_pr=$cont_pr+$cantidad[5];
          $cont_pnr=$cont_pnr+$cantidad[6];
          $cont_af=$cont_af+$cantidad[7];
          $cont_total=$cont_total+$cantidad[8];

          $cont_ppto_i=$cont_ppto_i+$cantidad[9];
          $cont_ppto_m=$cont_ppto_m+$cantidad[10];
          $cont_ppto_v=$cont_ppto_v+$cantidad[11];
          $p=$p+round((($cantidad[11]/$ppto_total[3])*100),2);
          $cont_ppto_e=$cont_ppto_e+$cantidad[12];
          $cont_ppto_s=$cont_ppto_s+$cantidad[13];
        }
        /*---------------------------------------------------------------------------*/
        $data['tunidad'] = $tabla;

        $data['inv'] = $inv;
        $data['pinv'] = $pinv;
        $data['total'] = $cont_inv;

        $data['prinv'] = $cont_inv;
        $data['pr'] = $cont_pr;
        $data['pnr'] = $cont_pnr;
        $data['af'] = $cont_af;
        $data['totalp'] = $cont_total;

        $data['ppto_i'] = number_format($cont_ppto_i, 2, ',', '.');
        $data['ppto_m'] = number_format($cont_ppto_m, 2, ',', '.');
        $data['ppto_v'] = number_format($cont_ppto_v, 2, ',', '.');
        $data['p'] = $p;
        $data['ppto_e'] = number_format($cont_ppto_e, 2, ',', '.');
        $data['ppto_s'] = number_format($cont_ppto_s, 2, ',', '.');
        if($cont_ppto_v!=0){$e=round((($cont_ppto_e/$cont_ppto_v)*100),2);}
        $data['e'] = $e;

        if($p1==1){$p1=2;}
        $data['valor']=$p1+$p2+$p3+$p4+1;
        $this->load->view('admin/reportes/reporte_institucional/global/unidad', $data);
    }
    /*---------------------- REPORTE POR UNIDAD EJECUTORA --------------------------*/
    public function rep_unidad_ejecutora($p1,$p2,$p3,$p4)
    {  
        $gestion=$this->session->userdata('gestion');
        $meses=$this->get_mes($this->session->userdata("mes"));
        $mes = $meses[1];
        $dias = $meses[2];

        $ppto_total=$this->suma_total_ptto(1,$p1,$p2,$p3,$p4); //// suma total de pptos
        
        $pr1='';$pr2='';$pr3='';$pr4='';
        if($p1==1){$pr1='PROYECTO DE INVERSI&Oacute;N';}
        if($p2==1){$pr2=' - PROGRAMA RECURRENTE';}
        if($p3==1){$pr3=' - PROGRAMA NO RECURRENTE';}
        if($p4==1){$pr4=' - ACCI&Oacute;N DE FUNCIONAMIENTO';}
        
        $inv=0;$pinv=0;$cont_inv=0;
        $cont_pr=0;$cont_pnr=0;$cont_af=0;$cont_total=0;
        $cont_ppto_i=0;$cont_ppto_m=0;$cont_ppto_v=0;$cont_ppto_e=0;$cont_ppto_s=0;$p=0;$e=0;

        if($p1==1){$p=2;}
        $valor=$p+$p2+$p3+$p4+1;

        $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/logo.jpg" class="siipp"/>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata('entidad').'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$gestion.'<br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                            <b>REPORTE : </b> RESUMEN DE EJECUCI&Oacute;N PRESUPUESTARIA POR UNIDAD EJECUTORA<br>
                        </td>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/escudo.jpg" alt="" width="100px">
                        </td>
                    </tr>
                </table>
                <footer>
                    <table class="table table-bordered" width="100%">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata("sistema_pie").'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>';

                  $html .= '
                  <font size=1><b>REPORTES DE : '.$pr1.''.$pr2.''.$pr3.''.$pr4.'</b></font>
                  <table class="datos_principales" style="table-layout:fixed;">
                    <tr>
                      <th colspan="1" bgcolor="#000000" style="width:15%;"></th>
                      <th colspan='.$valor.' bgcolor="#000000" style="width:28.3%;" >CANTIDAD</th>
                      <th colspan="4" bgcolor="#000000" style="width:28.3%;" >PRESUPUESTO</th>
                      <th colspan="3" bgcolor="#000000" style="width:28.3%;" >EJECUCI&Oacute;N PRESUPUESTARIA</th> 
                    </tr>
                    <tr>
                      <th bgcolor="#000000">UNIDAD EJECUTORA</th>';
                      if($p1==1){
                        $html .= '
                      <th bgcolor="#000000">INV.</th>
                      <th bgcolor="#000000">PINV.</th>';
                        }
                      if($p2==1){
                        $html .= '
                      <th bgcolor="#000000">P.R.</th>';
                        }
                      if($p3==1){
                        $html .= '
                      <th bgcolor="#000000">P.N.R.</th>';
                        }
                       if($p4==1){
                        $html .= '
                      <th bgcolor="#000000">A.F.</th>';
                        }
                      $html .= '

                      <th bgcolor="#000000">TOTAL</th>

                      <th bgcolor="#000000">INICIAL</th>
                      <th bgcolor="#000000">MODIFICACIONES</th>
                      <th bgcolor="#000000">VIGENTE</th>
                      <th bgcolor="#000000">% PROPORCI&Oacute;N</th>

                      <th bgcolor="#000000">EJECUTADO</th>
                      <th bgcolor="#000000">% DE EJECUCI&Oacute;N</th>
                      <th bgcolor="#000000">SALDO</th>
                    </tr>

                    </thead>
                    <tbody id="bdi">';
                    $u_ejecutoras = $this->model_reporte_global->unidades_ejecutoras_global($this->session->userdata('gestion'),$p1,$p2,$p3,$p4);//lista de unidades ejecutoras 
                     foreach($u_ejecutoras  as $row)
                      {   
                        $html .= '<tr>';
                          $html .= '<td>'.$row['uni_unidad'].'</td>';
                          $cantidad=$this->cantidad_ptto_ejec(1,$row['uni_id'],$this->session->userdata('gestion'),$this->session->userdata('mes'),$p1,$p2,$p3,$p4);
                          if($p1==1){
                          $html .= '<td>'.$cantidad[1].'</td>'; //// INV
                          $html .= '<td>'.$cantidad[2].'</td>'; //// PINV                            
                          }
                          if($p2==1){
                          $html .= '<td>'.$cantidad[5].'</td>'; //// PROG REC
                          }
                          if($p3==1){
                          $html .= '<td>'.$cantidad[6].'</td>'; //// PROG NO REC
                          }
                          if($p4==1){
                          $html .= '<td>'.$cantidad[7].'</td>'; //// AC. FUN
                          }

                          $html .= '<td>'.$cantidad[8].'</td>'; //// TOTAL PROG
                          $html .= '<td>'.number_format($cantidad[9], 2, ',', '.').'</td>';    //// PPTO INICIAL
                          $html .= '<td>'.number_format($cantidad[10], 2, ',', '.').'</td>';   //// PPTO MOD
                          $html .= '<td>'.number_format($cantidad[11], 2, ',', '.').'</td>';   //// PPTO VIG
                          $html .= '<td>'.round((($cantidad[11]/$ppto_total[3])*100),2).'%</td>'; //// % P
                          $html .= '<td>'.number_format($cantidad[12], 2, ',', '.').'</td>';   //// PPTO EJEC
                          $html .= '<td>'.$cantidad[14].' %</td>';    //// %E
                          $html .= '<td>'.number_format($cantidad[13], 2, ',', '.').'</td>';   //// PPTO SALDO
                          
                        $html .= '</tr>';

                          $inv=$inv+$cantidad[1];
                          $pinv=$pinv+$cantidad[2];

                          $cont_inv=$cont_inv+$cantidad[4];
                          $cont_pr=$cont_pr+$cantidad[5];
                          $cont_pnr=$cont_pnr+$cantidad[6];
                          $cont_af=$cont_af+$cantidad[7];
                          $cont_total=$cont_total+$cantidad[8];

                          $cont_ppto_i=$cont_ppto_i+$cantidad[9];
                          $cont_ppto_m=$cont_ppto_m+$cantidad[10];
                          $cont_ppto_v=$cont_ppto_v+$cantidad[11];
                          $p=$p+round((($cantidad[11]/$ppto_total[3])*100),2);
                          $cont_ppto_e=$cont_ppto_e+$cantidad[12];
                          $cont_ppto_s=$cont_ppto_s+$cantidad[13];
                          $e=$e+$cantidad[14];
                        
                      }
                        if($cont_ppto_v!=0){$e=round((($cont_ppto_e/$cont_ppto_v)*100),2);}

                      $html .= '
                      <tr>
                            <th bgcolor="#404040">TOTAL</th>';
                            if($p1==1){
                            $html .= '
                            <th bgcolor="#404040">'.$inv.'</th>
                            <th bgcolor="#404040">'.$pinv.'</th>';   
                            }
                            if($p2==1){
                            $html .= '
                            <th bgcolor="#404040">'.$cont_pr.'</th>';  
                            }
                            if($p3==1){
                            $html .= '
                            <th bgcolor="#404040">'.$cont_pnr.'</th>';  
                            }
                            if($p4==1){
                            $html .= '
                            <th bgcolor="#404040">'.$cont_af.'</th>';  
                            }

                            $html .= '
                            <th bgcolor="#404040">'.$cont_total.'</th>

                            <th bgcolor="#404040">'.number_format($cont_ppto_i, 2, ',', '.').'</th>
                            <th bgcolor="#404040">'.number_format($cont_ppto_m, 2, ',', '.').'</th>
                            <th bgcolor="#404040">'.number_format($cont_ppto_v, 2, ',', '.').'</th>
                            <th bgcolor="#404040">'.round($p,1).' %</th>

                            <th bgcolor="#404040">'.number_format($cont_ppto_e, 2, ',', '.').'</th>
                            <th bgcolor="#404040">'.round($e,1).' %</th>
                            <th bgcolor="#404040">'.number_format($cont_ppto_s, 2, ',', '.').'</th>
                            
                          </tr>
                    </tbody>
                    </table>';
                $html .= '
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream("dictamen_proyecto.pdf", array("Attachment" => false));
    }
    /*------------------------------------ REPORTE ACCIONES POR UNIDAD SELECCIONADA ---------------------------*/
    public function proyecto_por_accion($tipo,$id,$p1,$p2,$p3,$p4)
    { 
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++)
        {
             $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        
        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];
        
        $data['p1'] = $p1; //// proyectos de inversion
        $data['p2'] = $p2; //// programas recurrentes
        $data['p3'] = $p3; //// programas no recurrentes
        $data['p4'] = $p4; //// accion de funcionamiento

        $cols=$p1+$p2+$p3+$p4;
        if($cols>1)
        {
            $data['cols'] = 3;
        }
        else
        {
            $data['cols'] = 2;
        }

        $pr1='';$pr2='';$pr3='';$pr4='';
        if($p1==1){$pr1='PROYECTOS DE INVERSI&Oacute;N';}
        if($p2==1){$pr2=' - PROGRAMAS RECURRENTES';}
        if($p3==1){$pr3=' - PROGRAMAS NO RECURRENTES';}
        if($p4==1){$pr4=' - ACCI&Oacute;N DE FUNCIONAMIENTO';}
        
        $data['pr1']=$pr1;
        $data['pr2']=$pr2;
        $data['pr3']=$pr3;
        $data['pr4']=$pr4;

        if($tipo==1) //// POR UNIDAD
        {
            $unidad=$this->model_proyecto->get_unidad($id);
            $data['caracteristica']= 'Por Unidad Ejecutora / '.$unidad[0]['uni_unidad'];

            $acciones=$this->model_reporte_global->proyectos_ue_global($id,$this->session->userdata('gestion'),$p1,$p2,$p3,$p4);
        }
        elseif($tipo==2) /// POR ESTADO
        {
            $estado=$this->model_proyecto->get_estado($id);
            $data['caracteristica']='Estado de Situacion / '.$estado[0]['ep_descripcion'];
            $acciones = $this->model_reporte_global->proyectos_est_global($id,$this->session->userdata('gestion'),$p1,$p2,$p3,$p4);
        }

        elseif($tipo==3) ////// EJE PROGRAMATICO
        {
            $eje=$this->model_proyecto->get_eje_programatica($id);
            $data['caracteristica']='Por Eje Programatica / '.$eje[0]['ptdi_descripcion'];
            $acciones = $this->model_reporte_global->proyectos_eje_global($id,$this->session->userdata('gestion'),$p1,$p2,$p3,$p4);
        }
        elseif($tipo==4)  //// POR REGION
        {
            $reg=$this->model_proyecto->get_region($id);
            $data['caracteristica']='Por region / '.$reg[0]['reg_region'];
            $acciones = $this->model_reporte_global->proyectos_reg_global($id,$this->session->userdata('gestion'),$p1,$p2,$p3,$p4);
        }
        elseif($tipo==5) ///// POR MUNICIPIO
        {
            $mun=$this->model_proyecto->get_municipios($id);
            $data['caracteristica']='Por Municipio / '.$mun[0]['muni_municipio'];
            $acciones = $this->model_reporte_global->proyectos_muni_global($id,$this->session->userdata('gestion'),$p1,$p2,$p3,$p4);
        }
        elseif($tipo==6) ///// POR PROVINCIA
        {
            $prov=$this->model_proyecto->get_provincia($id);
            $data['caracteristica']='Por Provincia / '.$prov[0]['prov_provincia'];
            $acciones = $this->model_reporte_global->proyectos_prov_global($id,$this->session->userdata('gestion'),$p1,$p2,$p3,$p4);
        }
        
            $sum=0;
            foreach($acciones  as $row)
            { $ppto=$this->ptto_ejec_por_accion($row['proy_id'],$this->session->userdata('mes'),$this->session->userdata('gestion'));
                $sum=$sum+$ppto[3];
            }
            
        /*-----------------------------------------------------------*/
        if($sum==0){$sum=1;}
        $ppto_i=0;$ppto_mod=0;$ppto_v=0;$p=0;$ppto_e=0;$ppto_s=0;$e=0;
        $tabla = '';
        foreach($acciones  as $row)
        {   
          $tabla .= '<tr>';
            $tabla .= '<td><center><a href="' . site_url("") . '/admin/rep/get_accion/'.$row['proy_id'].'" title="REPORTE DE EJECUCION DEL PROYECTO" target="black"><img src="'.base_url().'assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"></a></td>';
            $tabla .= '<td>'.$row['proy_nombre'].'</td>';
            if($cols>1)
            {
                $tabla .= '<td>'.$row['tp_tipo'].'</td>';
            }
            
            $ppto=$this->ptto_ejec_por_accion($row['proy_id'],$this->session->userdata('mes'),$this->session->userdata('gestion'));
            $tabla .= '<td>'.number_format($ppto[1], 2, ',', '.').'</td>';
            $tabla .= '<td>'.number_format($ppto[2], 2, ',', '.').'</td>';
            $tabla .= '<td>'.number_format($ppto[3], 2, ',', '.').'</td>';
            $tabla .= '<td>'.round((($ppto[3]/$sum)*100),2).'</td>';
            $tabla .= '<td>'.number_format($ppto[4], 2, ',', '.').'</td>';
            $tabla .= '<td>'.number_format($ppto[6], 2, ',', '.').'</td>';
            $tabla .= '<td>'.number_format($ppto[5], 2, ',', '.').'</td>';
            $tabla .= '<td>'.$row['uni_unidad'].'</td>';
          $tabla .= '</tr>';

          $ppto_i=$ppto_i+$ppto[1];
          $ppto_mod=$ppto_mod+$ppto[2];
          $ppto_v=$ppto_v+$ppto[3];
          $p=$p+round((($ppto[3]/$sum)*100),2);
          $ppto_e=$ppto_e+$ppto[4];
          $ppto_s=$ppto_s+$ppto[5];
        }
        /*-----------------------------------------------------------*/
        $data['ppto_i']=$ppto_i;
        $data['ppto_mod']=$ppto_mod;
        $data['ppto_v']=$ppto_v;
        $data['p']=$p;
        $data['ppto_e']=$ppto_e;
        $data['ppto_s']=$ppto_s;
        if($ppto_v!=0){$e=round((($ppto_e/$ppto_v)*100),2);}
        $data['e']=$e;
        
        $data['accion']=$tabla;
        $this->load->view('admin/reportes/reporte_institucional/global/proyecto_x_accion', $data);
    } 
    /*==================================================================================================================*/ 

    /*========================== SELECCION DE OPCIONES - ESTADO DEL PROYECTO ===============================*/
    public function estado_proyecto($p1,$p2,$p3,$p4)
    { 
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++)
        {
             $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;

        $data['p1'] = $p1; //// proyectos de inversion
        $data['p2'] = $p2; //// programas recurrentes
        $data['p3'] = $p3; //// programas no recurrentes
        $data['p4'] = $p4; //// accion de funcionamiento

        $pr1='';$pr2='';$pr3='';$pr4='';
        if($p1==1){$pr1='PROYECTOS DE INVERSI&Oacute;N';}
        if($p2==1){$pr2=' - PROGRAMAS RECURRENTES';}
        if($p3==1){$pr3=' - PROGRAMAS NO RECURRENTES';}
        if($p4==1){$pr4=' - ACCI&Oacute;N DE FUNCIONAMIENTO';}
        
        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];

        $data['pr1']=$pr1;
        $data['pr2']=$pr2;
        $data['pr3']=$pr3;
        $data['pr4']=$pr4;

        $ppto_total=$this->suma_total_ptto(2,$p1,$p2,$p3,$p4); //// suma total de pptos
        $est_proy = $this->model_reporte_global->estado_proyecto();//lista de estados
        /*---------------------------------------------------------------------------*/
        $inv=0;$pinv=0;$cont_inv=0;
        $cont_pr=0;$cont_pnr=0;$cont_af=0;$cont_total=0;
        $cont_ppto_i=0;$cont_ppto_m=0;$cont_ppto_v=0;$cont_ppto_e=0;$cont_ppto_s=0;$p=0;$e=0;

        $tabla = '';
        foreach($est_proy  as $row)
        {   
          $tabla .= '<tr>';
            $tabla .= '<td><a href="' . site_url("") . '/admin/rep/accion_por_accion/2/'.$row['ep_id'].'/'.$p1.'/'.$p2.'/'.$p3.'/'.$p4.'" >' . strtoupper($row['ep_descripcion']) . '</a></td>';
            $cantidad=$this->cantidad_ptto_ejec(2,$row['ep_id'],$this->session->userdata('gestion'),$this->session->userdata('mes'),$p1,$p2,$p3,$p4);

            if($p1==1){
            $tabla .= '<td>'.$cantidad[1].'</td>'; //// INV
            $tabla .= '<td>'.$cantidad[2].'</td>'; //// PINV                
            }
            if($p2==1){
            $tabla .= '<td>'.$cantidad[5].'</td>'; //// PROG REC                
            }
            if($p3==1){
            $tabla .= '<td>'.$cantidad[6].'</td>'; //// PROG NO REC              
            }
            if($p4==1){
            $tabla .= '<td>'.$cantidad[7].'</td>'; //// AC. FUN          
            }   

            $tabla .= '<td>'.$cantidad[8].'</td>'; //// TOTAL PROG
            $tabla .= '<td>'.number_format($cantidad[9], 2, ',', '.').'</td>';    //// PPTO INICIAL
            $tabla .= '<td>'.number_format($cantidad[10], 2, ',', '.').'</td>';   //// PPTO MOD
            $tabla .= '<td>'.number_format($cantidad[11], 2, ',', '.').'</td>';   //// PPTO VIG
            $tabla .= '<td>'.round((($cantidad[11]/$ppto_total[3])*100),2).'%</td>'; //// % P
            $tabla .= '<td>'.number_format($cantidad[12], 2, ',', '.').'</td>';   //// PPTO EJEC
            $tabla .= '<td>'.$cantidad[14].' %</td>';    //// %E
            $tabla .= '<td>'.number_format($cantidad[13], 2, ',', '.').'</td>';   //// PPTO SALDO
            
          $tabla .= '</tr>';

          $inv=$inv+$cantidad[1];
          $pinv=$pinv+$cantidad[2];

          $cont_inv=$cont_inv+$cantidad[4];
          $cont_pr=$cont_pr+$cantidad[5];
          $cont_pnr=$cont_pnr+$cantidad[6];
          $cont_af=$cont_af+$cantidad[7];
          $cont_total=$cont_total+$cantidad[8];

          $cont_ppto_i=$cont_ppto_i+$cantidad[9];
          $cont_ppto_m=$cont_ppto_m+$cantidad[10];
          $cont_ppto_v=$cont_ppto_v+$cantidad[11];
          $p=$p+round((($cantidad[11]/$ppto_total[3])*100),2);
          $cont_ppto_e=$cont_ppto_e+$cantidad[12];
          $cont_ppto_s=$cont_ppto_s+$cantidad[13];
          
        }
        /*---------------------------------------------------------------------------*/

        $data['inv'] = $inv;
        $data['pinv'] = $pinv;
        $data['total'] = $cont_inv;

        $data['prinv'] = $cont_inv;
        $data['pr'] = $cont_pr;
        $data['pnr'] = $cont_pnr;
        $data['af'] = $cont_af;
        $data['totalp'] = $cont_total;

        $data['ppto_i'] = number_format($cont_ppto_i, 2, ',', '.');
        $data['ppto_m'] = number_format($cont_ppto_m, 2, ',', '.');
        $data['ppto_v'] = number_format($cont_ppto_v, 2, ',', '.');
        $data['p'] = $p;
        $data['ppto_e'] = number_format($cont_ppto_e, 2, ',', '.');
        $data['ppto_s'] = number_format($cont_ppto_s, 2, ',', '.');
        if($cont_ppto_v!=0){$e=round((($cont_ppto_e/$cont_ppto_v)*100),2);}
        $data['e'] = $e;

        if($p1==1){$p1=2;}
        $data['valor']=$p1+$p2+$p3+$p4+1;

        $data['testado'] = $tabla;
        $this->load->view('admin/reportes/reporte_institucional/global/estado', $data);
    }
    /*---------------------- REPORTE POR ESTADO DEL PROYECTO --------------------------*/
    public function rep_estado_proyecto($p1,$p2,$p3,$p4)
    {  
        $gestion=$this->session->userdata('gestion');
        $meses=$this->get_mes($this->session->userdata("mes"));
        $mes = $meses[1];
        $dias = $meses[2];

        $ppto_total=$this->suma_total_ptto(2,$p1,$p2,$p3,$p4); //// suma total de pptos
        
        $pr1='';$pr2='';$pr3='';$pr4='';
        if($p1==1){$pr1='PROYECTOS DE INVERSI&Oacute;N';}
        if($p2==1){$pr2=' - PROGRAMAS RECURRENTES';}
        if($p3==1){$pr3=' - PROGRAMAS NO RECURRENTES';}
        if($p4==1){$pr4=' - ACCI&Oacute;N DE FUNCIONAMIENTO';}
        
        $inv=0;$pinv=0;$cont_inv=0;
        $cont_pr=0;$cont_pnr=0;$cont_af=0;$cont_total=0;
        $cont_ppto_i=0;$cont_ppto_m=0;$cont_ppto_v=0;$cont_ppto_e=0;$cont_ppto_s=0;$p=0;$e=0;

        if($p1==1){$p=2;}
        $valor=$p+$p2+$p3+$p4+1;

        $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/logo.jpg" class="siipp"/>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata('entidad').'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$gestion.'<br>
                            <b>'.$this->session->userdata('sistema').' </b><br>
                            <b>REPORTE : </b> RESUMEN DE EJECUCI&Oacute;N PRESUPUESTARIA POR ESTADO DE ACCIONES<br>
                        </td>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/escudo.jpg" alt="" width="100px">
                        </td>
                    </tr>
                </table>
                <footer>
                    <table class="table table-bordered" width="100%">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata("sistema_pie").'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>';

                  $html .= '
                  <font size=1><b>REPORTES DE : '.$pr1.''.$pr2.''.$pr3.''.$pr4.'</b></font>
                  <table class="datos_principales" style="table-layout:fixed;">
                    <tr>
                      <th colspan="1" bgcolor="#000000" style="width:15%;"></th>
                      <th colspan='.$valor.' bgcolor="#000000" style="width:28.3%;" >CANTIDAD</th>
                      <th colspan="4" bgcolor="#000000" style="width:28.3%;" >PRESUPUESTO</th>
                      <th colspan="3" bgcolor="#000000" style="width:28.3%;" >EJECUCI&Oacute;N PRESUPUESTARIA</th> 
                    </tr>
                    <tr>
                      <th bgcolor="#000000">ESTADOS DE ACCIONES</th>';
                      if($p1==1){
                        $html .= '
                      <th bgcolor="#000000">INV.</th>
                      <th bgcolor="#000000">PINV.</th>';
                        }
                      if($p2==1){
                        $html .= '
                      <th bgcolor="#000000">P.R.</th>';
                        }
                      if($p3==1){
                        $html .= '
                      <th bgcolor="#000000">P.N.R.</th>';
                        }
                       if($p4==1){
                        $html .= '
                      <th bgcolor="#000000">A.F.</th>';
                        }
                      $html .= '
                      <th bgcolor="#000000">TOTAL</th>

                      <th bgcolor="#000000">INICIAL</th>
                      <th bgcolor="#000000">MODIFICACIONES</th>
                      <th bgcolor="#000000">VIGENTE</th>
                      <th bgcolor="#000000">% PROPORCI&Oacute;N</th>

                      <th bgcolor="#000000">EJECUTADO</th>
                      <th bgcolor="#000000">% DE EJECUCI&Oacute;N</th>
                      <th bgcolor="#000000">SALDO</th>
                      
                    </tr>

                    </thead>
                    <tbody id="bdi">';

                    $est_proy = $this->model_reporte_global->estado_proyecto();//lista de estados
                     foreach($est_proy  as $row)
                      {   
                        $html .= '<tr>';
                          $html .= '<td><b>'.$row['ep_descripcion'].'</b></td>';
                          $cantidad=$this->cantidad_ptto_ejec(2,$row['ep_id'],$this->session->userdata('gestion'),$this->session->userdata('mes'),$p1,$p2,$p3,$p4);
                          if($p1==1){
                          $html .= '<td>'.$cantidad[1].'</td>'; //// INV
                          $html .= '<td>'.$cantidad[2].'</td>'; //// PINV                            
                          }
                          if($p2==1){
                          $html .= '<td>'.$cantidad[5].'</td>'; //// PROG REC
                          }
                          if($p3==1){
                          $html .= '<td>'.$cantidad[6].'</td>'; //// PROG NO REC
                          }
                          if($p4==1){
                          $html .= '<td>'.$cantidad[7].'</td>'; //// AC. FUN
                          }
                          $html .= '<td>'.$cantidad[8].'</td>'; //// TOTAL PROG
                          $html .= '<td>'.number_format($cantidad[9], 2, ',', '.').'</td>';    //// PPTO INICIAL
                          $html .= '<td>'.number_format($cantidad[10], 2, ',', '.').'</td>';   //// PPTO MOD
                          $html .= '<td>'.number_format($cantidad[11], 2, ',', '.').'</td>';   //// PPTO VIG
                          $html .= '<td>'.round((($cantidad[11]/$ppto_total[3])*100),2).'%</td>'; //// % P
                          $html .= '<td>'.number_format($cantidad[12], 2, ',', '.').'</td>';   //// PPTO EJEC
                          $html .= '<td>'.$cantidad[14].' %</td>';    //// %E
                          $html .= '<td>'.number_format($cantidad[13], 2, ',', '.').'</td>';   //// PPTO SALDO
                          
                        $html .= '</tr>';

                          $inv=$inv+$cantidad[1];
                          $pinv=$pinv+$cantidad[2];

                          $cont_inv=$cont_inv+$cantidad[4];
                          $cont_pr=$cont_pr+$cantidad[5];
                          $cont_pnr=$cont_pnr+$cantidad[6];
                          $cont_af=$cont_af+$cantidad[7];
                          $cont_total=$cont_total+$cantidad[8];

                          $cont_ppto_i=$cont_ppto_i+$cantidad[9];
                          $cont_ppto_m=$cont_ppto_m+$cantidad[10];
                          $cont_ppto_v=$cont_ppto_v+$cantidad[11];
                          $p=$p+round((($cantidad[11]/$ppto_total[3])*100),2);
                          $cont_ppto_e=$cont_ppto_e+$cantidad[12];
                          $cont_ppto_s=$cont_ppto_s+$cantidad[13];
                        
                      }
                      if($cont_ppto_v!=0){$e=round((($cont_ppto_e/$cont_ppto_v)*100),2);}
                      $html .= '
                      <tr>
                            <th bgcolor="#404040">TOTAL</th>';
                            if($p1==1){
                            $html .= '
                            <th bgcolor="#404040">'.$inv.'</th>
                            <th bgcolor="#404040">'.$pinv.'</th>';   
                            }
                            if($p2==1){
                            $html .= '
                            <th bgcolor="#404040">'.$cont_pr.'</th>';  
                            }
                            if($p3==1){
                            $html .= '
                            <th bgcolor="#404040">'.$cont_pnr.'</th>';  
                            }
                            if($p4==1){
                            $html .= '
                            <th bgcolor="#404040">'.$cont_af.'</th>';  
                            }

                            $html .= '
                            <th bgcolor="#404040">'.$cont_total.'</th>

                            <th bgcolor="#404040">'.number_format($cont_ppto_i, 2, ',', '.').'</th>
                            <th bgcolor="#404040">'.number_format($cont_ppto_m, 2, ',', '.').'</th>
                            <th bgcolor="#404040">'.number_format($cont_ppto_v, 2, ',', '.').'</th>
                            <th bgcolor="#404040">'.round($p,1).' %</th>

                            <th bgcolor="#404040">'.number_format($cont_ppto_e, 2, ',', '.').'</th>
                            <th bgcolor="#404040">'.$e.'</th>
                            <th bgcolor="#404040">'.number_format($cont_ppto_s, 2, ',', '.').'</th>
                            
                          </tr>
                    </tbody>
                    </table>';
                $html .= '
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream("dictamen_proyecto.pdf", array("Attachment" => false));
    } 
    /*==================================================================================================================*/ 
    /*========================== SELECCION DE OPCIONES - EJE PROGRAMATICO ===============================*/
    public function eje_programatico($p1,$p2,$p3,$p4)
    { 
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++)
        {
             $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;

        $data['p1'] = $p1; //// proyectos de inversion
        $data['p2'] = $p2; //// programas recurrentes
        $data['p3'] = $p3; //// programas no recurrentes
        $data['p4'] = $p4; //// accion de funcionamiento

        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];

        $pr1='';$pr2='';$pr3='';$pr4='';
        if($p1==1){$pr1='PROYECTOS DE INVERSI&Oacute;N';}
        if($p2==1){$pr2=' - PROGRAMAS RECURRENTES';}
        if($p3==1){$pr3=' - PROGRAMAS NO RECURRENTES';}
        if($p4==1){$pr4=' - ACCI&Oacute;N DE FUNCIONAMIENTO';}
        
        $data['pr1']=$pr1;
        $data['pr2']=$pr2;
        $data['pr3']=$pr3;
        $data['pr4']=$pr4;

        $ppto_total=$this->suma_total_ptto(3,$p1,$p2,$p3,$p4); //// suma total de pptos
        $eje_prog = $this->model_reporte_global->eje_programatica();//lista de ejes programaticos
        /*---------------------------------------------------------------------------*/
        $inv=0;$pinv=0;$cont_inv=0;
        $cont_pr=0;$cont_pnr=0;$cont_af=0;$cont_total=0;
        $cont_ppto_i=0;$cont_ppto_m=0;$cont_ppto_v=0;$cont_ppto_e=0;$cont_ppto_s=0;$p=0;$e=0;

        $tabla = '';
        foreach($eje_prog  as $row)
        {   
          $tabla .= '<tr>';
            $tabla .= '<td><a href="' . site_url("") . '/admin/rep/accion_por_accion/3/'.$row['ptdi_id'].'/'.$p1.'/'.$p2.'/'.$p3.'/'.$p4.'" >' . strtoupper($row['ptdi_descripcion']) . '</a></td>';
            $cantidad=$this->cantidad_ptto_ejec(3,$row['ptdi_id'],$this->session->userdata('gestion'),$this->session->userdata('mes'),$p1,$p2,$p3,$p4);
            if($p1==1){
            $tabla .= '<td>'.$cantidad[1].'</td>'; //// INV
            $tabla .= '<td>'.$cantidad[2].'</td>'; //// PINV                
            }
            if($p2==1){
            $tabla .= '<td>'.$cantidad[5].'</td>'; //// PROG REC                
            }
            if($p3==1){
            $tabla .= '<td>'.$cantidad[6].'</td>'; //// PROG NO REC              
            }
            if($p4==1){
            $tabla .= '<td>'.$cantidad[7].'</td>'; //// AC. FUN          
            } 
            $tabla .= '<td>'.$cantidad[8].'</td>'; //// TOTAL PROG
            $tabla .= '<td>'.number_format($cantidad[9], 2, ',', '.').'</td>';    //// PPTO INICIAL
            $tabla .= '<td>'.number_format($cantidad[10], 2, ',', '.').'</td>';   //// PPTO MOD
            $tabla .= '<td>'.number_format($cantidad[11], 2, ',', '.').'</td>';   //// PPTO VIG
            $tabla .= '<td>'.round((($cantidad[11]/$ppto_total[3])*100),2).'%</td>'; //// % P
            $tabla .= '<td>'.number_format($cantidad[12], 2, ',', '.').'</td>';   //// PPTO EJEC
            $tabla .= '<td>'.$cantidad[14].' %</td>';    //// %E
            $tabla .= '<td>'.number_format($cantidad[13], 2, ',', '.').'</td>';   //// PPTO SALDO
            
          $tabla .= '</tr>';

          $inv=$inv+$cantidad[1];
          $pinv=$pinv+$cantidad[2];

          $cont_inv=$cont_inv+$cantidad[4];
          $cont_pr=$cont_pr+$cantidad[5];
          $cont_pnr=$cont_pnr+$cantidad[6];
          $cont_af=$cont_af+$cantidad[7];
          $cont_total=$cont_total+$cantidad[8];

          $cont_ppto_i=$cont_ppto_i+$cantidad[9];
          $cont_ppto_m=$cont_ppto_m+$cantidad[10];
          $cont_ppto_v=$cont_ppto_v+$cantidad[11];
          $p=$p+round((($cantidad[11]/$ppto_total[3])*100),2);
          $cont_ppto_e=$cont_ppto_e+$cantidad[12];
          $cont_ppto_s=$cont_ppto_s+$cantidad[13];
        }
        /*---------------------------------------------------------------------------*/
       

        $data['inv'] = $inv;
        $data['pinv'] = $pinv;
        $data['total'] = $cont_inv;

        $data['prinv'] = $cont_inv;
        $data['pr'] = $cont_pr;
        $data['pnr'] = $cont_pnr;
        $data['af'] = $cont_af;
        $data['totalp'] = $cont_total;

        $data['ppto_i'] = number_format($cont_ppto_i, 2, ',', '.');
        $data['ppto_m'] = number_format($cont_ppto_m, 2, ',', '.');
        $data['ppto_v'] = number_format($cont_ppto_v, 2, ',', '.');
        $data['p'] = $p;
        $data['ppto_e'] = number_format($cont_ppto_e, 2, ',', '.');
        $data['ppto_s'] = number_format($cont_ppto_s, 2, ',', '.');
        if($cont_ppto_v!=0){$e=round((($cont_ppto_e/$cont_ppto_v)*100),2);}
        $data['e'] = $e;

        if($p1==1){$p1=2;}
        $data['valor']=$p1+$p2+$p3+$p4+1;

        $data['teje'] = $tabla;
        $this->load->view('admin/reportes/reporte_institucional/global/eje_prog', $data);
    }
     /*---------------------- REPORTE POR EJE PROGRAMATICA --------------------------*/
    public function rep_eje_programatica($p1,$p2,$p3,$p4)
    {  
        $gestion=$this->session->userdata('gestion');
        $meses=$this->get_mes($this->session->userdata("mes"));
        $mes = $meses[1];
        $dias = $meses[2];

        $ppto_total=$this->suma_total_ptto(3,$p1,$p2,$p3,$p4); //// suma total de pptos
        
        $pr1='';$pr2='';$pr3='';$pr4='';
        if($p1==1){$pr1='PROYECTOS DE INVERSI&Oacute;N';}
        if($p2==1){$pr2=' - PROGRAMAS RECURRENTES';}
        if($p3==1){$pr3=' - PROGRAMAS NO RECURRENTES';}
        if($p4==1){$pr4=' - ACCI&Oacute;N DE FUNCIONAMIENTO';}
        
        $inv=0;$pinv=0;$cont_inv=0;
        $cont_pr=0;$cont_pnr=0;$cont_af=0;$cont_total=0;
        $cont_ppto_i=0;$cont_ppto_m=0;$cont_ppto_v=0;$cont_ppto_e=0;$cont_ppto_s=0;$p=0;$e=0;

        if($p1==1){$p=2;}
        $valor=$p+$p2+$p3+$p4+1;

        $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/logo.jpg" class="siipp"/>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata('entidad').'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$gestion.'<br>
                            <b>'.$this->session->userdata('sistema').' </b><br>
                            <b>REPORTE : </b> RESUMEN DE EJECUCI&Oacute;N PRESUPUESTARIA POR EJE PROGRAMATICA<br>
                        </td>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/escudo.jpg" alt="" width="100px">
                        </td>
                    </tr>
                </table>
                <footer>
                    <table class="table table-bordered" width="100%">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata("sistema_pie").'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>';

                  $html .= '
                  <font size=1><b>REPORTES DE : '.$pr1.''.$pr2.''.$pr3.''.$pr4.'</b></font>
                  <table class="datos_principales" style="table-layout:fixed;">
                    <tr>
                      <th colspan="1" bgcolor="#000000" style="width:15%;"></th>
                      <th colspan='.$valor.' bgcolor="#000000" style="width:28.3%;" >CANTIDAD</th>
                      <th colspan="4" bgcolor="#000000" style="width:28.3%;" >PRESUPUESTO</th>
                      <th colspan="3" bgcolor="#000000" style="width:28.3%;" >EJECUCI&Oacute;N FINANCIERA</th> 
                    </tr>
                    <tr>
                      <th bgcolor="#000000">EJE PROGRAM&Aacute;TICA</th>';
                      if($p1==1){
                        $html .= '
                      <th bgcolor="#000000">INV.</th>
                      <th bgcolor="#000000">PINV.</th>';
                        }
                      if($p2==1){
                        $html .= '
                      <th bgcolor="#000000">P.R.</th>';
                        }
                      if($p3==1){
                        $html .= '
                      <th bgcolor="#000000">P.N.R.</th>';
                        }
                       if($p4==1){
                        $html .= '
                      <th bgcolor="#000000">A.F.</th>';
                        }
                      $html .= '
                      <th bgcolor="#000000">TOTAL</th>

                      <th bgcolor="#000000">INICIAL</th>
                      <th bgcolor="#000000">MODIFICACIONES</th>
                      <th bgcolor="#000000">VIGENTE</th>
                      <th bgcolor="#000000">% PROPORCI&Oacute;N</th>

                      <th bgcolor="#000000">EJECUTADO</th>
                      <th bgcolor="#000000">% DE EJECUCI&Oacute;N</th>
                      <th bgcolor="#000000">SALDO</th>
                    </tr>

                    </thead>
                    <tbody id="bdi">';

                    $eje_prog = $this->model_reporte_global->eje_programatica();//lista de ejes programaticos
                     foreach($eje_prog  as $row)
                      {   
                        $html .= '<tr>';
                          $html .= '<td>'.$row['ptdi_descripcion'].'</td>';
                          $cantidad=$this->cantidad_ptto_ejec(3,$row['ptdi_id'],$this->session->userdata('gestion'),$this->session->userdata('mes'),$p1,$p2,$p3,$p4);
                          if($p1==1){
                          $html .= '<td>'.$cantidad[1].'</td>'; //// INV
                          $html .= '<td>'.$cantidad[2].'</td>'; //// PINV                            
                          }
                          if($p2==1){
                          $html .= '<td>'.$cantidad[5].'</td>'; //// PROG REC
                          }
                          if($p3==1){
                          $html .= '<td>'.$cantidad[6].'</td>'; //// PROG NO REC
                          }
                          if($p4==1){
                          $html .= '<td>'.$cantidad[7].'</td>'; //// AC. FUN
                          }

                          $html .= '<td>'.$cantidad[8].'</td>'; //// TOTAL PROG
                          $html .= '<td>'.number_format($cantidad[9], 2, ',', '.').'</td>';    //// PPTO INICIAL
                          $html .= '<td>'.number_format($cantidad[10], 2, ',', '.').'</td>';   //// PPTO MOD
                          $html .= '<td>'.number_format($cantidad[11], 2, ',', '.').'</td>';   //// PPTO VIG
                          $html .= '<td>'.round((($cantidad[11]/$ppto_total[3])*100),2).'%</td>'; //// % P
                          $html .= '<td>'.number_format($cantidad[12], 2, ',', '.').'</td>';   //// PPTO EJEC
                          $html .= '<td>'.$cantidad[14].' %</td>';    //// %E
                          $html .= '<td>'.number_format($cantidad[13], 2, ',', '.').'</td>';   //// PPTO SALDO
                          
                        $html .= '</tr>';

                          $inv=$inv+$cantidad[1];
                          $pinv=$pinv+$cantidad[2];

                          $cont_inv=$cont_inv+$cantidad[4];
                          $cont_pr=$cont_pr+$cantidad[5];
                          $cont_pnr=$cont_pnr+$cantidad[6];
                          $cont_af=$cont_af+$cantidad[7];
                          $cont_total=$cont_total+$cantidad[8];

                          $cont_ppto_i=$cont_ppto_i+$cantidad[9];
                          $cont_ppto_m=$cont_ppto_m+$cantidad[10];
                          $cont_ppto_v=$cont_ppto_v+$cantidad[11];
                          $p=$p+round((($cantidad[11]/$ppto_total[3])*100),2);
                          $cont_ppto_e=$cont_ppto_e+$cantidad[12];
                          $cont_ppto_s=$cont_ppto_s+$cantidad[13];
                        
                      }
                      if($cont_ppto_v!=0){$e=round((($cont_ppto_e/$cont_ppto_v)*100),2);}
                      $html .= '
                      <tr>
                            <th bgcolor="#404040">TOTAL</th>';
                            if($p1==1){
                            $html .= '
                            <th bgcolor="#404040">'.$inv.'</th>
                            <th bgcolor="#404040">'.$pinv.'</th>';   
                            }
                            if($p2==1){
                            $html .= '
                            <th bgcolor="#404040">'.$cont_pr.'</th>';  
                            }
                            if($p3==1){
                            $html .= '
                            <th bgcolor="#404040">'.$cont_pnr.'</th>';  
                            }
                            if($p4==1){
                            $html .= '
                            <th bgcolor="#404040">'.$cont_af.'</th>';  
                            }

                            $html .= '
                            <th bgcolor="#404040">'.$cont_total.'</th>

                            <th bgcolor="#404040">'.number_format($cont_ppto_i, 2, ',', '.').'</th>
                            <th bgcolor="#404040">'.number_format($cont_ppto_m, 2, ',', '.').'</th>
                            <th bgcolor="#404040">'.number_format($cont_ppto_v, 2, ',', '.').'</th>
                            <th bgcolor="#404040">'.round($p,1).' %</th>

                            <th bgcolor="#404040">'.number_format($cont_ppto_e, 2, ',', '.').'</th>
                            <th bgcolor="#404040">'.$e.'%</th>
                            <th bgcolor="#404040">'.number_format($cont_ppto_s, 2, ',', '.').'</th>
                            
                          </tr>
                    </tbody>
                    </table>';
                $html .= '
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream("dictamen_proyecto.pdf", array("Attachment" => false));
    } 
    /*==================================================================================================================*/ 
    /*========================== SELECCION DE OPCIONES - REGIONES ===============================*/
    public function por_region($p1,$p2,$p3,$p4)
    { 
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++)
        {
             $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;

        $data['p1'] = $p1; //// proyectos de inversion
        $data['p2'] = $p2; //// programas recurrentes
        $data['p3'] = $p3; //// programas no recurrentes
        $data['p4'] = $p4; //// accion de funcionamiento

        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];

        $pr1='';$pr2='';$pr3='';$pr4='';
        if($p1==1){$pr1='PROYECTOS DE INVERSI&Oacute;N';}
        if($p2==1){$pr2=' - PROGRAMAS RECURRENTES';}
        if($p3==1){$pr3=' - PROGRAMAS NO RECURRENTES';}
        if($p4==1){$pr4=' - ACCI&Oacute;N DE FUNCIONAMIENTO';}
        
        $data['pr1']=$pr1;
        $data['pr2']=$pr2;
        $data['pr3']=$pr3;
        $data['pr4']=$pr4;

        $ppto_total=$this->suma_total_ptto(4,$p1,$p2,$p3,$p4); //// suma total de pptos
        $region = $this->model_reporte_global->regiones();//lista de Regiones
        /*---------------------------------------------------------------------------*/
        $inv=0;$pinv=0;$cont_inv=0;
        $cont_pr=0;$cont_pnr=0;$cont_af=0;$cont_total=0;
        $cont_ppto_i=0;$cont_ppto_m=0;$cont_ppto_v=0;$cont_ppto_e=0;$cont_ppto_s=0;$p=0;$e=0;

        $tabla = '';
        foreach($region  as $row)
        {   
          $tabla .= '<tr>';
            $tabla .= '<td><a href="' . site_url("") . '/admin/rep/accion_por_accion/4/'.$row['reg_id'].'/'.$p1.'/'.$p2.'/'.$p3.'/'.$p4.'" >' . strtoupper($row['reg_region']) . '</a></td>';
            $cantidad=$this->cantidad_ptto_ejec(4,$row['reg_id'],$this->session->userdata('gestion'),$this->session->userdata('mes'),$p1,$p2,$p3,$p4);
            if($p1==1){
            $tabla .= '<td>'.$cantidad[1].'</td>'; //// INV
            $tabla .= '<td>'.$cantidad[2].'</td>'; //// PINV                
            }
            if($p2==1){
            $tabla .= '<td>'.$cantidad[5].'</td>'; //// PROG REC                
            }
            if($p3==1){
            $tabla .= '<td>'.$cantidad[6].'</td>'; //// PROG NO REC              
            }
            if($p4==1){
            $tabla .= '<td>'.$cantidad[7].'</td>'; //// AC. FUN          
            } 
            $tabla .= '<td>'.$cantidad[8].'</td>'; //// TOTAL PROG
            $tabla .= '<td>'.number_format($cantidad[9], 2, ',', '.').'</td>';    //// PPTO INICIAL
            $tabla .= '<td>'.number_format($cantidad[10], 2, ',', '.').'</td>';   //// PPTO MOD
            $tabla .= '<td>'.number_format($cantidad[11], 2, ',', '.').'</td>';   //// PPTO VIG
            $tabla .= '<td>'.round((($cantidad[11]/$ppto_total[3])*100),2).'%</td>'; //// % P
            $tabla .= '<td>'.number_format($cantidad[12], 2, ',', '.').'</td>';   //// PPTO EJEC
            $tabla .= '<td>'.$cantidad[14].' %</td>';    //// %E
            $tabla .= '<td>'.number_format($cantidad[13], 2, ',', '.').'</td>';   //// PPTO SALDO
            
          $tabla .= '</tr>';

          $inv=$inv+$cantidad[1];
          $pinv=$pinv+$cantidad[2];

          $cont_inv=$cont_inv+$cantidad[4];
          $cont_pr=$cont_pr+$cantidad[5];
          $cont_pnr=$cont_pnr+$cantidad[6];
          $cont_af=$cont_af+$cantidad[7];
          $cont_total=$cont_total+$cantidad[8];

          $cont_ppto_i=$cont_ppto_i+$cantidad[9];
          $cont_ppto_m=$cont_ppto_m+$cantidad[10];
          $cont_ppto_v=$cont_ppto_v+$cantidad[11];
          $p=$p+round((($cantidad[11]/$ppto_total[3])*100),2);
          $cont_ppto_e=$cont_ppto_e+$cantidad[12];
          $cont_ppto_s=$cont_ppto_s+$cantidad[13];
        }
        /*---------------------------------------------------------------------------*/

        $data['inv'] = $inv;
        $data['pinv'] = $pinv;
        $data['total'] = $cont_inv;

        $data['prinv'] = $cont_inv;
        $data['pr'] = $cont_pr;
        $data['pnr'] = $cont_pnr;
        $data['af'] = $cont_af;
        $data['totalp'] = $cont_total;

        $data['ppto_i'] = number_format($cont_ppto_i, 2, ',', '.');
        $data['ppto_m'] = number_format($cont_ppto_m, 2, ',', '.');
        $data['ppto_v'] = number_format($cont_ppto_v, 2, ',', '.');
        $data['p'] = $p;
        $data['ppto_e'] = number_format($cont_ppto_e, 2, ',', '.');
        $data['ppto_s'] = number_format($cont_ppto_s, 2, ',', '.');
        if($cont_ppto_v!=0){$e=round((($cont_ppto_e/$cont_ppto_v)*100),2);}
        $data['e'] = $e;

        if($p1==1){$p1=2;}
        $data['valor']=$p1+$p2+$p3+$p4+1;

        $data['tregion'] = $tabla;
        $this->load->view('admin/reportes/reporte_institucional/global/region', $data);
    }
    /*---------------------- REPORTE POR REGION --------------------------*/
    public function rep_por_region($p1,$p2,$p3,$p4)
    {  
        $gestion=$this->session->userdata('gestion');
        $meses=$this->get_mes($this->session->userdata("mes"));
        $mes = $meses[1];
        $dias = $meses[2];

        $ppto_total=$this->suma_total_ptto(4,$p1,$p2,$p3,$p4); //// suma total de pptos
        
        $pr1='';$pr2='';$pr3='';$pr4='';
        if($p1==1){$pr1='PROYECTOS DE INVERSI&Oacute;N';}
        if($p2==1){$pr2=' - PROGRAMAS RECURRENTES';}
        if($p3==1){$pr3=' - PROGRAMAS NO RECURRENTES';}
        if($p4==1){$pr4=' - ACCI&Oacute;N DE FUNCIONAMIENTO';}
        
        $inv=0;$pinv=0;$cont_inv=0;
        $cont_pr=0;$cont_pnr=0;$cont_af=0;$cont_total=0;
        $cont_ppto_i=0;$cont_ppto_m=0;$cont_ppto_v=0;$cont_ppto_e=0;$cont_ppto_s=0;$p=0;$e=0;

        if($p1==1){$p=2;}
        $valor=$p+$p2+$p3+$p4+1;

        $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/logo.jpg" class="siipp"/>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata('entidad').'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$gestion.'<br>
                            <b>'.$this->session->userdata('sistema').' </b><br>
                            <b>REPORTE : </b> RESUMEN DE EJECUCI&Oacute;N PRESUPUESTARIA POR REGI&Oacute;N<br>
                        </td>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/escudo.jpg" alt="" width="100px">
                        </td>
                    </tr>
                </table>
                <footer>
                    <table class="table table-bordered" width="100%">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata("sistema_pie").'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>';

                  $html .= '
                  <font size=1><b>REPORTES DE : '.$pr1.''.$pr2.''.$pr3.''.$pr4.'</b></font>
                  <table class="datos_principales" style="table-layout:fixed;">
                    <tr>
                      <th colspan="1" bgcolor="#000000" style="width:15%;"></th>
                      <th colspan='.$valor.' bgcolor="#000000" style="width:28.3%;" >CANTIDAD</th>
                      <th colspan="4" bgcolor="#000000" style="width:28.3%;" >PRESUPUESTO</th>
                      <th colspan="3" bgcolor="#000000" style="width:28.3%;" >POR REGI&Oacute;N</th> 
                    </tr>
                    <tr>
                      <th bgcolor="#000000">REGI&Oacute;N</th>';
                      if($p1==1){
                        $html .= '
                      <th bgcolor="#000000">INV.</th>
                      <th bgcolor="#000000">PINV.</th>';
                        }
                      if($p2==1){
                        $html .= '
                      <th bgcolor="#000000">P.R.</th>';
                        }
                      if($p3==1){
                        $html .= '
                      <th bgcolor="#000000">P.N.R.</th>';
                        }
                       if($p4==1){
                        $html .= '
                      <th bgcolor="#000000">A.F.</th>';
                        }
                      $html .= '
                      <th bgcolor="#000000">TOTAL</th>

                      <th bgcolor="#000000">INICIAL</th>
                      <th bgcolor="#000000">MODIFICACIONES</th>
                      <th bgcolor="#000000">VIGENTE</th>
                      <th bgcolor="#000000">% PROPORCI&Oacute;N</th>

                      <th bgcolor="#000000">EJECUTADO</th>
                      <th bgcolor="#000000">% DE EJECUCI&Oacute;N</th>
                      <th bgcolor="#000000">SALDO</th>
                    </tr>

                    </thead>
                    <tbody id="bdi">';

                     $region = $this->model_reporte_global->regiones();//lista de Regiones
                     foreach($region  as $row)
                      {   
                        $html .= '<tr>';
                          $html .= '<td>'.$row['reg_region'].'</td>';
                          $cantidad=$this->cantidad_ptto_ejec(4,$row['reg_id'],$this->session->userdata('gestion'),$this->session->userdata('mes'),$p1,$p2,$p3,$p4);
                          if($p1==1){
                          $html .= '<td>'.$cantidad[1].'</td>'; //// INV
                          $html .= '<td>'.$cantidad[2].'</td>'; //// PINV                            
                          }
                          if($p2==1){
                          $html .= '<td>'.$cantidad[5].'</td>'; //// PROG REC
                          }
                          if($p3==1){
                          $html .= '<td>'.$cantidad[6].'</td>'; //// PROG NO REC
                          }
                          if($p4==1){
                          $html .= '<td>'.$cantidad[7].'</td>'; //// AC. FUN
                          }
                          $html .= '<td>'.$cantidad[8].'</td>'; //// TOTAL PROG
                          $html .= '<td>'.number_format($cantidad[9], 2, ',', '.').'</td>';    //// PPTO INICIAL
                          $html .= '<td>'.number_format($cantidad[10], 2, ',', '.').'</td>';   //// PPTO MOD
                          $html .= '<td>'.number_format($cantidad[11], 2, ',', '.').'</td>';   //// PPTO VIG
                          $html .= '<td>'.round((($cantidad[11]/$ppto_total[3])*100),2).'%</td>'; //// % P
                          $html .= '<td>'.number_format($cantidad[12], 2, ',', '.').'</td>';   //// PPTO EJEC
                          $html .= '<td>'.$cantidad[14].' %</td>';    //// %E
                          $html .= '<td>'.number_format($cantidad[13], 2, ',', '.').'</td>';   //// PPTO SALDO
                          
                        $html .= '</tr>';

                          $inv=$inv+$cantidad[1];
                          $pinv=$pinv+$cantidad[2];

                          $cont_inv=$cont_inv+$cantidad[4];
                          $cont_pr=$cont_pr+$cantidad[5];
                          $cont_pnr=$cont_pnr+$cantidad[6];
                          $cont_af=$cont_af+$cantidad[7];
                          $cont_total=$cont_total+$cantidad[8];

                          $cont_ppto_i=$cont_ppto_i+$cantidad[9];
                          $cont_ppto_m=$cont_ppto_m+$cantidad[10];
                          $cont_ppto_v=$cont_ppto_v+$cantidad[11];
                          $p=$p+round((($cantidad[11]/$ppto_total[3])*100),2);
                          $cont_ppto_e=$cont_ppto_e+$cantidad[12];
                          $cont_ppto_s=$cont_ppto_s+$cantidad[13];
                      }
                      if($cont_ppto_v!=0){$e=round((($cont_ppto_e/$cont_ppto_v)*100),2);}
                      $html .= '
                      <tr>
                            <th bgcolor="#404040">TOTAL</th>';
                            if($p1==1){
                            $html .= '
                            <th bgcolor="#404040">'.$inv.'</th>
                            <th bgcolor="#404040">'.$pinv.'</th>';   
                            }
                            if($p2==1){
                            $html .= '
                            <th bgcolor="#404040">'.$cont_pr.'</th>';  
                            }
                            if($p3==1){
                            $html .= '
                            <th bgcolor="#404040">'.$cont_pnr.'</th>';  
                            }
                            if($p4==1){
                            $html .= '
                            <th bgcolor="#404040">'.$cont_af.'</th>';  
                            }

                            $html .= '
                            <th bgcolor="#404040">'.$cont_total.'</th>

                            <th bgcolor="#404040">'.number_format($cont_ppto_i, 2, ',', '.').'</th>
                            <th bgcolor="#404040">'.number_format($cont_ppto_m, 2, ',', '.').'</th>
                            <th bgcolor="#404040">'.number_format($cont_ppto_v, 2, ',', '.').'</th>
                            <th bgcolor="#404040">'.round($p,1).' %</th>

                            <th bgcolor="#404040">'.number_format($cont_ppto_e, 2, ',', '.').'</th>
                            <th bgcolor="#404040">'.$e.'%</th>
                            <th bgcolor="#404040">'.number_format($cont_ppto_s, 2, ',', '.').'</th>
                            
                          </tr>
                    </tbody>
                    </table>';
                $html .= '
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream("dictamen_proyecto.pdf", array("Attachment" => false));
    } 
    /*==================================================================================================================*/ 
    /*========================== SELECCION DE OPCIONES - POR MUNICIPIO ===============================*/
    public function por_municipio($p1,$p2,$p3,$p4)
    { 
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++)
        {
             $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;

        $data['p1'] = $p1; //// proyectos de inversion
        $data['p2'] = $p2; //// programas recurrentes
        $data['p3'] = $p3; //// programas no recurrentes
        $data['p4'] = $p4; //// accion de funcionamiento

        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];

        $pr1='';$pr2='';$pr3='';$pr4='';
        if($p1==1){$pr1='PROYECTOS DE INVERSI&Oacute;N';}
        if($p2==1){$pr2=' - PROGRAMAS RECURRENTES';}
        if($p3==1){$pr3=' - PROGRAMAS NO RECURRENTES';}
        if($p4==1){$pr4=' - ACCI&Oacute;N DE FUNCIONAMIENTO';}
        
        $data['pr1']=$pr1;
        $data['pr2']=$pr2;
        $data['pr3']=$pr3;
        $data['pr4']=$pr4;

        $ppto_total=$this->suma_total_ptto(5,$p1,$p2,$p3,$p4); //// suma total de pptos
        $muni = $this->model_reporte_global->municipios();//lista de Municipios
        /*---------------------------------------------------------------------------*/
        $inv=0;$pinv=0;$cont_inv=0;
        $cont_pr=0;$cont_pnr=0;$cont_af=0;$cont_total=0;
        $cont_ppto_i=0;$cont_ppto_m=0;$cont_ppto_v=0;$cont_ppto_e=0;$cont_ppto_s=0;$p=0;$e=0;

        $tabla = '';
        foreach($muni  as $row)
        {   
          $tabla .= '<tr>';
            $tabla .= '<td><a href="' . site_url("") . '/admin/rep/accion_por_accion/5/'.$row['muni_id'].'/'.$p1.'/'.$p2.'/'.$p3.'/'.$p4.'" >' . strtoupper($row['muni_municipio']) . '</a></td>';
            $cantidad=$this->cantidad_ptto_ejec(5,$row['muni_id'],$this->session->userdata('gestion'),$this->session->userdata('mes'),$p1,$p2,$p3,$p4);
            if($p1==1){
            $tabla .= '<td>'.$cantidad[1].'</td>'; //// INV
            $tabla .= '<td>'.$cantidad[2].'</td>'; //// PINV                
            }
            if($p2==1){
            $tabla .= '<td>'.$cantidad[5].'</td>'; //// PROG REC                
            }
            if($p3==1){
            $tabla .= '<td>'.$cantidad[6].'</td>'; //// PROG NO REC              
            }
            if($p4==1){
            $tabla .= '<td>'.$cantidad[7].'</td>'; //// AC. FUN          
            } 
            $tabla .= '<td>'.$cantidad[8].'</td>'; //// TOTAL PROG
            $tabla .= '<td>'.number_format($cantidad[9], 2, ',', '.').'</td>';    //// PPTO INICIAL
            $tabla .= '<td>'.number_format($cantidad[10], 2, ',', '.').'</td>';   //// PPTO MOD
            $tabla .= '<td>'.number_format($cantidad[11], 2, ',', '.').'</td>';   //// PPTO VIG
            $tabla .= '<td>'.round((($cantidad[11]/$ppto_total[3])*100),2).'%</td>'; //// % P
            $tabla .= '<td>'.number_format($cantidad[12], 2, ',', '.').'</td>';   //// PPTO EJEC
            $tabla .= '<td>'.$cantidad[14].' %</td>';    //// %E
            $tabla .= '<td>'.number_format($cantidad[13], 2, ',', '.').'</td>';   //// PPTO SALDO
            
          $tabla .= '</tr>';

          $inv=$inv+$cantidad[1];
          $pinv=$pinv+$cantidad[2];

          $cont_inv=$cont_inv+$cantidad[4];
          $cont_pr=$cont_pr+$cantidad[5];
          $cont_pnr=$cont_pnr+$cantidad[6];
          $cont_af=$cont_af+$cantidad[7];
          $cont_total=$cont_total+$cantidad[8];

          $cont_ppto_i=$cont_ppto_i+$cantidad[9];
          $cont_ppto_m=$cont_ppto_m+$cantidad[10];
          $cont_ppto_v=$cont_ppto_v+$cantidad[11];
          $p=$p+round((($cantidad[11]/$ppto_total[3])*100),2);
          $cont_ppto_e=$cont_ppto_e+$cantidad[12];
          $cont_ppto_s=$cont_ppto_s+$cantidad[13];
        }
        /*---------------------------------------------------------------------------*/

        $data['inv'] = $inv;
        $data['pinv'] = $pinv;
        $data['total'] = $cont_inv;

        $data['prinv'] = $cont_inv;
        $data['pr'] = $cont_pr;
        $data['pnr'] = $cont_pnr;
        $data['af'] = $cont_af;
        $data['totalp'] = $cont_total;

        $data['ppto_i'] = number_format($cont_ppto_i, 2, ',', '.');
        $data['ppto_m'] = number_format($cont_ppto_m, 2, ',', '.');
        $data['ppto_v'] = number_format($cont_ppto_v, 2, ',', '.');
        $data['p'] = $p;
        $data['ppto_e'] = number_format($cont_ppto_e, 2, ',', '.');
        $data['ppto_s'] = number_format($cont_ppto_s, 2, ',', '.');
        if($cont_ppto_v!=0){$e=round((($cont_ppto_e/$cont_ppto_v)*100),2);}
        $data['e'] = $e;

        if($p1==1){$p1=2;}
        $data['valor']=$p1+$p2+$p3+$p4+1;

        $data['tmuni'] = $tabla;
        $this->load->view('admin/reportes/reporte_institucional/global/municipio', $data);
    }
    /*---------------------- REPORTE POR MUNICIPIO --------------------------*/
    public function rep_por_municipio($p1,$p2,$p3,$p4)
    {  
        $gestion=$this->session->userdata('gestion');
        $meses=$this->get_mes($this->session->userdata("mes"));
        $mes = $meses[1];
        $dias = $meses[2];

        $ppto_total=$this->suma_total_ptto(5,$p1,$p2,$p3,$p4); //// suma total de pptos
        
        $pr1='';$pr2='';$pr3='';$pr4='';
        if($p1==1){$pr1='PROYECTOS DE INVERSI&Oacute;N';}
        if($p2==1){$pr2=' - PROGRAMAS RECURRENTES';}
        if($p3==1){$pr3=' - PROGRAMAS NO RECURRENTES';}
        if($p4==1){$pr4=' - ACCI&Oacute;N DE FUNCIONAMIENTO';}
        
        $inv=0;$pinv=0;$cont_inv=0;
        $cont_pr=0;$cont_pnr=0;$cont_af=0;$cont_total=0;
        $cont_ppto_i=0;$cont_ppto_m=0;$cont_ppto_v=0;$cont_ppto_e=0;$cont_ppto_s=0;$p=0;$e=0;

        if($p1==1){$p=2;}
        $valor=$p+$p2+$p3+$p4+1;

        $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/logo.jpg" class="siipp"/>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata('entidad').'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$gestion.'<br>
                            <b>'.$this->session->userdata('sistema').' </b><br>
                            <b>REPORTE : </b> RESUMEN DE EJECUCI&Oacute;N PRESUPUESTARIA POR MUNICIPIO<br>
                        </td>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/escudo.jpg" alt="" width="100px">
                        </td>
                    </tr>
                </table>
                <footer>
                    <table class="table table-bordered" width="100%">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata("sistema_pie").'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>';

                  $html .= '
                  <font size=1><b>REPORTES DE : '.$pr1.''.$pr2.''.$pr3.''.$pr4.'</b></font>
                  <table class="datos_principales" style="table-layout:fixed;">
                    <tr>
                      <th colspan="1" bgcolor="#000000" style="width:15%;"></th>
                      <th colspan='.$valor.' bgcolor="#000000" style="width:28.3%;" >CANTIDAD</th>
                      <th colspan="4" bgcolor="#000000" style="width:28.3%;" >PRESUPUESTO</th>
                      <th colspan="3" bgcolor="#000000" style="width:28.3%;" >POR REGI&Oacute;N</th> 
                    </tr>
                    <tr>
                      <th bgcolor="#000000">MUNICIPIO</th>';
                      if($p1==1){
                        $html .= '
                      <th bgcolor="#000000">INV.</th>
                      <th bgcolor="#000000">PINV.</th>';
                        }
                      if($p2==1){
                        $html .= '
                      <th bgcolor="#000000">P.R.</th>';
                        }
                      if($p3==1){
                        $html .= '
                      <th bgcolor="#000000">P.N.R.</th>';
                        }
                       if($p4==1){
                        $html .= '
                      <th bgcolor="#000000">A.F.</th>';
                        }
                      $html .= '
                      <th bgcolor="#000000">TOTAL</th>

                      <th bgcolor="#000000">INICIAL</th>
                      <th bgcolor="#000000">MODIFICACIONES</th>
                      <th bgcolor="#000000">VIGENTE</th>
                      <th bgcolor="#000000">% PROPORCI&Oacute;N</th>

                      <th bgcolor="#000000">EJECUTADO</th>
                      <th bgcolor="#000000">% DE EJECUCI&Oacute;N</th>
                      <th bgcolor="#000000">SALDO</th>
                    </tr>

                    </thead>
                    <tbody id="bdi">';

                     $muni = $this->model_reporte_global->municipios();//lista de Municipios
                     foreach($muni  as $row)
                      {   
                        $html .= '<tr>';
                          $html .= '<td>'.$row['muni_municipio'].'</td>';
                          $cantidad=$this->cantidad_ptto_ejec(5,$row['muni_id'],$this->session->userdata('gestion'),$this->session->userdata('mes'),$p1,$p2,$p3,$p4);
                          
                          if($p1==1){
                          $html .= '<td>'.$cantidad[1].'</td>'; //// INV
                          $html .= '<td>'.$cantidad[2].'</td>'; //// PINV                            
                          }
                          if($p2==1){
                          $html .= '<td>'.$cantidad[5].'</td>'; //// PROG REC
                          }
                          if($p3==1){
                          $html .= '<td>'.$cantidad[6].'</td>'; //// PROG NO REC
                          }
                          if($p4==1){
                          $html .= '<td>'.$cantidad[7].'</td>'; //// AC. FUN
                          }

                          $html .= '<td>'.$cantidad[8].'</td>'; //// TOTAL PROG
                          $html .= '<td>'.number_format($cantidad[9], 2, ',', '.').'</td>';    //// PPTO INICIAL
                          $html .= '<td>'.number_format($cantidad[10], 2, ',', '.').'</td>';   //// PPTO MOD
                          $html .= '<td>'.number_format($cantidad[11], 2, ',', '.').'</td>';   //// PPTO VIG
                          $html .= '<td>'.round((($cantidad[11]/$ppto_total[3])*100),2).'%</td>'; //// % P
                          $html .= '<td>'.number_format($cantidad[12], 2, ',', '.').'</td>';   //// PPTO EJEC
                          $html .= '<td>'.$cantidad[14].' %</td>';    //// %E
                          $html .= '<td>'.number_format($cantidad[13], 2, ',', '.').'</td>';   //// PPTO SALDO
                          
                        $html .= '</tr>';

                          $inv=$inv+$cantidad[1];
                          $pinv=$pinv+$cantidad[2];

                          $cont_inv=$cont_inv+$cantidad[4];
                          $cont_pr=$cont_pr+$cantidad[5];
                          $cont_pnr=$cont_pnr+$cantidad[6];
                          $cont_af=$cont_af+$cantidad[7];
                          $cont_total=$cont_total+$cantidad[8];

                          $cont_ppto_i=$cont_ppto_i+$cantidad[9];
                          $cont_ppto_m=$cont_ppto_m+$cantidad[10];
                          $cont_ppto_v=$cont_ppto_v+$cantidad[11];
                          $p=$p+round((($cantidad[11]/$ppto_total[3])*100),2);
                          $cont_ppto_e=$cont_ppto_e+$cantidad[12];
                          $cont_ppto_s=$cont_ppto_s+$cantidad[13];
                      }
                      if($cont_ppto_v!=0){$e=round((($cont_ppto_e/$cont_ppto_v)*100),2);}
                      $html .= '
                      <tr>
                            <th bgcolor="#404040">TOTAL</th>';
                            if($p1==1){
                            $html .= '
                            <th bgcolor="#404040">'.$inv.'</th>
                            <th bgcolor="#404040">'.$pinv.'</th>';   
                            }
                            if($p2==1){
                            $html .= '
                            <th bgcolor="#404040">'.$cont_pr.'</th>';  
                            }
                            if($p3==1){
                            $html .= '
                            <th bgcolor="#404040">'.$cont_pnr.'</th>';  
                            }
                            if($p4==1){
                            $html .= '
                            <th bgcolor="#404040">'.$cont_af.'</th>';  
                            }

                            $html .= '
                            <th bgcolor="#404040">'.$cont_total.'</th>

                            <th bgcolor="#404040">'.number_format($cont_ppto_i, 2, ',', '.').'</th>
                            <th bgcolor="#404040">'.number_format($cont_ppto_m, 2, ',', '.').'</th>
                            <th bgcolor="#404040">'.number_format($cont_ppto_v, 2, ',', '.').'</th>
                            <th bgcolor="#404040">'.round($p,1).' %</th>

                            <th bgcolor="#404040">'.number_format($cont_ppto_e, 2, ',', '.').'</th>
                            <th bgcolor="#404040">'.$e.'</th>
                            <th bgcolor="#404040">'.number_format($cont_ppto_s, 2, ',', '.').'</th>
                            
                          </tr>
                    </tbody>
                    </table>';
                $html .= '
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream("dictamen_proyecto.pdf", array("Attachment" => false));
    } 
    /*==================================================================================================================*/ 
    /*========================== SELECCION DE OPCIONES - POR PROVINCIA ===============================*/
    public function por_provincia($p1,$p2,$p3,$p4)
    { 
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++)
        {
             $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;

        $data['p1'] = $p1; //// proyectos de inversion
        $data['p2'] = $p2; //// programas recurrentes
        $data['p3'] = $p3; //// programas no recurrentes
        $data['p4'] = $p4; //// accion de funcionamiento

        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];

        $pr1='';$pr2='';$pr3='';$pr4='';
        if($p1==1){$pr1='PROYECTOS DE INVERSI&Oacute;N';}
        if($p2==1){$pr2=' - PROGRAMAS RECURRENTES';}
        if($p3==1){$pr3=' - PROGRAMAS NO RECURRENTES';}
        if($p4==1){$pr4=' - ACCI&Oacute;N DE FUNCIONAMIENTO';}
        
        $data['pr1']=$pr1;
        $data['pr2']=$pr2;
        $data['pr3']=$pr3;
        $data['pr4']=$pr4;

        $ppto_total=$this->suma_total_ptto(6,$p1,$p2,$p3,$p4); //// suma total de pptos
        $prov = $this->model_reporte_global->provincias();//lista de Provincias
        /*---------------------------------------------------------------------------*/
        $inv=0;$pinv=0;$cont_inv=0;
        $cont_pr=0;$cont_pnr=0;$cont_af=0;$cont_total=0;
        $cont_ppto_i=0;$cont_ppto_m=0;$cont_ppto_v=0;$cont_ppto_e=0;$cont_ppto_s=0;$p=0;$e=0;

        $tabla = '';
        foreach($prov  as $row)
        {   
          $tabla .= '<tr>';
            $tabla .= '<td><a href="' . site_url("") . '/admin/rep/accion_por_accion/6/'.$row['prov_id'].'/'.$p1.'/'.$p2.'/'.$p3.'/'.$p4.'" >' . strtoupper($row['prov_provincia']) . '</a></td>';
            $cantidad=$this->cantidad_ptto_ejec(6,$row['prov_id'],$this->session->userdata('gestion'),$this->session->userdata('mes'),$p1,$p2,$p3,$p4);

            if($p1==1){
            $tabla .= '<td>'.$cantidad[1].'</td>'; //// INV
            $tabla .= '<td>'.$cantidad[2].'</td>'; //// PINV                
            }
            if($p2==1){
            $tabla .= '<td>'.$cantidad[5].'</td>'; //// PROG REC                
            }
            if($p3==1){
            $tabla .= '<td>'.$cantidad[6].'</td>'; //// PROG NO REC              
            }
            if($p4==1){
            $tabla .= '<td>'.$cantidad[7].'</td>'; //// AC. FUN          
            }

            $tabla .= '<td>'.$cantidad[8].'</td>'; //// TOTAL PROG
            $tabla .= '<td>'.number_format($cantidad[9], 2, ',', '.').'</td>';    //// PPTO INICIAL
            $tabla .= '<td>'.number_format($cantidad[10], 2, ',', '.').'</td>';   //// PPTO MOD
            $tabla .= '<td>'.number_format($cantidad[11], 2, ',', '.').'</td>';   //// PPTO VIG
            $tabla .= '<td>'.round((($cantidad[11]/$ppto_total[3])*100),2).'%</td>'; //// % P
            $tabla .= '<td>'.number_format($cantidad[12], 2, ',', '.').'</td>';   //// PPTO EJEC
            $tabla .= '<td>'.$cantidad[14].' %</td>';    //// %E
            $tabla .= '<td>'.number_format($cantidad[13], 2, ',', '.').'</td>';   //// PPTO SALDO
            
          $tabla .= '</tr>';

          $inv=$inv+$cantidad[1];
          $pinv=$pinv+$cantidad[2];

          $cont_inv=$cont_inv+$cantidad[4];
          $cont_pr=$cont_pr+$cantidad[5];
          $cont_pnr=$cont_pnr+$cantidad[6];
          $cont_af=$cont_af+$cantidad[7];
          $cont_total=$cont_total+$cantidad[8];

          $cont_ppto_i=$cont_ppto_i+$cantidad[9];
          $cont_ppto_m=$cont_ppto_m+$cantidad[10];
          $cont_ppto_v=$cont_ppto_v+$cantidad[11];
          $p=$p+round((($cantidad[11]/$ppto_total[3])*100),2);
          $cont_ppto_e=$cont_ppto_e+$cantidad[12];
          $cont_ppto_s=$cont_ppto_s+$cantidad[13];
        }
        /*---------------------------------------------------------------------------*/

        $data['inv'] = $inv;
        $data['pinv'] = $pinv;
        $data['total'] = $cont_inv;

        $data['prinv'] = $cont_inv;
        $data['pr'] = $cont_pr;
        $data['pnr'] = $cont_pnr;
        $data['af'] = $cont_af;
        $data['totalp'] = $cont_total;

        $data['ppto_i'] = number_format($cont_ppto_i, 2, ',', '.');
        $data['ppto_m'] = number_format($cont_ppto_m, 2, ',', '.');
        $data['ppto_v'] = number_format($cont_ppto_v, 2, ',', '.');
        $data['p'] = $p;
        $data['ppto_e'] = number_format($cont_ppto_e, 2, ',', '.');
        $data['ppto_s'] = number_format($cont_ppto_s, 2, ',', '.');
        if($cont_ppto_v!=0){$e=round((($cont_ppto_e/$cont_ppto_v)*100),2);}
        $data['e'] = $e;

        if($p1==1){$p1=2;}
        $data['valor']=$p1+$p2+$p3+$p4+1;

        $data['tprov'] = $tabla;
        $this->load->view('admin/reportes/reporte_institucional/global/provincia', $data);
    }
    /*---------------------- REPORTE POR MUNICIPIO --------------------------*/
    public function rep_por_provincia($p1,$p2,$p3,$p4)
    {  
        $gestion=$this->session->userdata('gestion');
        $meses=$this->get_mes($this->session->userdata("mes"));
        $mes = $meses[1];
        $dias = $meses[2];

        $ppto_total=$this->suma_total_ptto(6,$p1,$p2,$p3,$p4); //// suma total de pptos
        
        $pr1='';$pr2='';$pr3='';$pr4='';
        if($p1==1){$pr1='PROYECTOS DE INVERSI&Oacute;N';}
        if($p2==1){$pr2=' - PROGRAMAS RECURRENTES';}
        if($p3==1){$pr3=' - PROGRAMAS NO RECURRENTES';}
        if($p4==1){$pr4=' - ACCI&Oacute;N DE FUNCIONAMIENTO';}
        
        $inv=0;$pinv=0;$cont_inv=0;
        $cont_pr=0;$cont_pnr=0;$cont_af=0;$cont_total=0;
        $cont_ppto_i=0;$cont_ppto_m=0;$cont_ppto_v=0;$cont_ppto_e=0;$cont_ppto_s=0;$p=0;$e=0;

        if($p1==1){$p=2;}
        $valor=$p+$p2+$p3+$p4+1;

        $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/logo.jpg" class="siipp"/>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata('entidad').'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$gestion.'<br>
                            <b>'.$this->session->userdata('sistema').' </b><br>
                            <b>REPORTE : </b> RESUMEN DE EJECUCI&Oacute;N PRESUPUESTARIA POR PROVINCIA<br>
                        </td>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/escudo.jpg" alt="" width="100px">
                        </td>
                    </tr>
                </table>
                <footer>
                    <table class="table table-bordered" width="100%">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata("sistema_pie").'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>';

                  $html .= '
                  <font size=1><b>REPORTES DE : '.$pr1.''.$pr2.''.$pr3.''.$pr4.'</b></font>
                  <table class="datos_principales" style="table-layout:fixed;">
                    <tr>
                      <th colspan="1" bgcolor="#000000" style="width:15%;"></th>
                      <th colspan='.$valor.' bgcolor="#000000" style="width:28.3%;" >CANTIDAD</th>
                      <th colspan="4" bgcolor="#000000" style="width:28.3%;" >PRESUPUESTO</th>
                      <th colspan="3" bgcolor="#000000" style="width:28.3%;" >POR REGI&Oacute;N</th> 
                    </tr>
                    <tr>
                      <th bgcolor="#000000">MUNICIPIO</th>';
                      if($p1==1){
                        $html .= '
                      <th bgcolor="#000000">INV.</th>
                      <th bgcolor="#000000">PINV.</th>';
                        }
                      if($p2==1){
                        $html .= '
                      <th bgcolor="#000000">P.R.</th>';
                        }
                      if($p3==1){
                        $html .= '
                      <th bgcolor="#000000">P.N.R.</th>';
                        }
                       if($p4==1){
                        $html .= '
                      <th bgcolor="#000000">A.F.</th>';
                        }
                      $html .= '
                      <th bgcolor="#000000">TOTAL</th>

                      <th bgcolor="#000000">INICIAL</th>
                      <th bgcolor="#000000">MODIFICACIONES</th>
                      <th bgcolor="#000000">VIGENTE</th>
                      <th bgcolor="#000000">% PROPORCI&Oacute;N</th>

                      <th bgcolor="#000000">EJECUTADO</th>
                      <th bgcolor="#000000">% DE EJECUCI&Oacute;N</th>
                      <th bgcolor="#000000">SALDO</th>
                    </tr>

                    </thead>
                    <tbody id="bdi">';

                     $prov = $this->model_reporte_global->provincias();//lista de Provincias
                     foreach($prov  as $row)
                      {   
                        $html .= '<tr>';
                          $html .= '<td>'.strtoupper($row['prov_provincia']).'</td>';
                          $cantidad=$this->cantidad_ptto_ejec(6,$row['prov_id'],$this->session->userdata('gestion'),$this->session->userdata('mes'),$p1,$p2,$p3,$p4);
                          if($p1==1){
                          $html .= '<td>'.$cantidad[1].'</td>'; //// INV
                          $html .= '<td>'.$cantidad[2].'</td>'; //// PINV                            
                          }
                          if($p2==1){
                          $html .= '<td>'.$cantidad[5].'</td>'; //// PROG REC
                          }
                          if($p3==1){
                          $html .= '<td>'.$cantidad[6].'</td>'; //// PROG NO REC
                          }
                          if($p4==1){
                          $html .= '<td>'.$cantidad[7].'</td>'; //// AC. FUN
                          }

                          $html .= '<td>'.$cantidad[8].'</td>'; //// TOTAL PROG
                          $html .= '<td>'.number_format($cantidad[9], 2, ',', '.').'</td>';    //// PPTO INICIAL
                          $html .= '<td>'.number_format($cantidad[10], 2, ',', '.').'</td>';   //// PPTO MOD
                          $html .= '<td>'.number_format($cantidad[11], 2, ',', '.').'</td>';   //// PPTO VIG
                          $html .= '<td>'.round((($cantidad[11]/$ppto_total[3])*100),2).'%</td>'; //// % P
                          $html .= '<td>'.number_format($cantidad[12], 2, ',', '.').'</td>';   //// PPTO EJEC
                          $html .= '<td>'.$cantidad[14].' %</td>';    //// %E
                          $html .= '<td>'.number_format($cantidad[13], 2, ',', '.').'</td>';   //// PPTO SALDO
                          
                        $html .= '</tr>';

                          $inv=$inv+$cantidad[1];
                          $pinv=$pinv+$cantidad[2];

                          $cont_inv=$cont_inv+$cantidad[4];
                          $cont_pr=$cont_pr+$cantidad[5];
                          $cont_pnr=$cont_pnr+$cantidad[6];
                          $cont_af=$cont_af+$cantidad[7];
                          $cont_total=$cont_total+$cantidad[8];

                          $cont_ppto_i=$cont_ppto_i+$cantidad[9];
                          $cont_ppto_m=$cont_ppto_m+$cantidad[10];
                          $cont_ppto_v=$cont_ppto_v+$cantidad[11];
                          $p=$p+round((($cantidad[11]/$ppto_total[3])*100),2);
                          $cont_ppto_e=$cont_ppto_e+$cantidad[12];
                          $cont_ppto_s=$cont_ppto_s+$cantidad[13];
                      }
                      if($cont_ppto_v!=0){$e=round((($cont_ppto_e/$cont_ppto_v)*100),2);}
                      $html .= '
                      <tr>
                            <th bgcolor="#404040">TOTAL</th>';
                            if($p1==1){
                            $html .= '
                            <th bgcolor="#404040">'.$inv.'</th>
                            <th bgcolor="#404040">'.$pinv.'</th>';   
                            }
                            if($p2==1){
                            $html .= '
                            <th bgcolor="#404040">'.$cont_pr.'</th>';  
                            }
                            if($p3==1){
                            $html .= '
                            <th bgcolor="#404040">'.$cont_pnr.'</th>';  
                            }
                            if($p4==1){
                            $html .= '
                            <th bgcolor="#404040">'.$cont_af.'</th>';  
                            }

                            $html .= '

                            <th bgcolor="#404040">'.$cont_total.'</th>

                            <th bgcolor="#404040">'.number_format($cont_ppto_i, 2, ',', '.').'</th>
                            <th bgcolor="#404040">'.number_format($cont_ppto_m, 2, ',', '.').'</th>
                            <th bgcolor="#404040">'.number_format($cont_ppto_v, 2, ',', '.').'</th>
                            <th bgcolor="#404040">'.round($p,1).' %</th>

                            <th bgcolor="#404040">'.number_format($cont_ppto_e, 2, ',', '.').'</th>
                            <th bgcolor="#404040">'.$e.' %</th>
                            <th bgcolor="#404040">'.number_format($cont_ppto_s, 2, ',', '.').'</th>
                            
                          </tr>
                    </tbody>
                    </table>';
                $html .= '
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream("dictamen_proyecto.pdf", array("Attachment" => false));
    } 
    /*==================================================================================================================*/ 
    
  /*=========================================================================================================================*/
    public function get_mes($mes_id)
    {
      $mes[1]='Enero';
      $mes[2]='Febrero';
      $mes[3]='Marzo';
      $mes[4]='Abril';
      $mes[5]='Mayo';
      $mes[6]='Junio';
      $mes[7]='Julio';
      $mes[8]='Agosto';
      $mes[9]='Septiembre';
      $mes[10]='Octubre';
      $mes[11]='Noviembre';
      $mes[12]='Diciembre';

      $dias[1]='31';
      $dias[2]='28';
      $dias[3]='31';
      $dias[4]='30';
      $dias[5]='31';
      $dias[6]='30';
      $dias[7]='31';
      $dias[8]='31';
      $dias[9]='30';
      $dias[10]='31';
      $dias[11]='30';
      $dias[12]='31';

      $valor[1]=$mes[$mes_id];
      $valor[2]=$dias[$mes_id];

      return $valor;
    }
}