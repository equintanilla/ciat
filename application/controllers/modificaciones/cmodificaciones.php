<?php
define("DOMPDF_ENABLE_REMOTE", true);
define("DOMPDF_TEMP_DIR", "/tmp");
class Cmodificaciones extends CI_Controller {
  public $rol = array('1' => '3','2' => '4','3' => '5');  
  public function __construct (){
        parent::__construct();
        if($this->session->userdata('fun_id')!=null){
        $this->load->library('pdf2');
        $this->load->model('programacion/model_proyecto');
        $this->load->model('programacion/model_faseetapa');
        $this->load->model('programacion/model_actividad');
        $this->load->model('programacion/model_producto');
        $this->load->model('programacion/model_componente');
        $this->load->model('programacion/model_mantenimiento');
        $this->load->model('mantenimiento/mindicador');
        $this->load->model('ejecucion/model_ejecucion');
        $this->load->model('modificacion/model_mod');
        $this->load->model('mantenimiento/model_funcionario');
        $this->load->model('programacion/marco_estrategico/mobjetivos');
        $this->load->model('programacion/prog_poa/mp_terminal');
        $this->load->model('menu_modelo');

        }else{
            redirect('/','refresh');
        }
    }

    /*---------- MIS OPE----------*/
    public function mis_operaciones(){
        $data['menu']=$this->menu(2); //// genera menu
        $data['proyecto']=$this->lista_operaciones(1);
        $data['precurrente']=$this->lista_operaciones(2);
        $data['pnrecurrente']=$this->lista_operaciones(3);
        $data['ofuncionamiento']=$this->lista_operaciones(4);
        //load the view
        $this->load->view('admin/modificacion/poa/mis_operaciones', $data);
    }

    /*---------- TUE ----------*/
    public function lista_tue(){
        $data['menu']=$this->menu(2); //// genera menu
        $data['proyecto']=$this->lista_tp_operaciones(3,1);
        $data['precurrente']=$this->lista_tp_operaciones(3,2);
        $data['pnrecurrente']=$this->lista_tp_operaciones(3,3);
        $data['ofuncionamiento']=$this->lista_tp_operaciones(3,4);
        //load the view
        $this->load->view('admin/modificacion/poa/mis_operaciones_tue', $data);
    }

    /*---------- V POA ----------*/
    public function lista_vpoa(){
        $data['menu']=$this->menu(2); //// genera menu
        $data['proyecto']=$this->lista_tp_operaciones(4,1);
        $data['precurrente']=$this->lista_tp_operaciones(4,2);
        $data['pnrecurrente']=$this->lista_tp_operaciones(4,3);
        $data['ofuncionamiento']=$this->lista_tp_operaciones(4,4);
        //load the view
        $this->load->view('admin/modificacion/poa/mis_operaciones_vpoa', $data);
    }

    /*----- LISTA DE OPERACIONES TUE -----*/
    public function lista_tp_operaciones($rol_id,$tp_id){
        $tabla='';
        $proyectos=$this->model_mod->tp_operaciones($tp_id,$rol_id);

        $nro=0;
        foreach($proyectos  as $row){
            $fase = $this->model_faseetapa->get_id_fase($row['proy_id']);
            $nc=$this->model_faseetapa->calcula_nc($fase[0]['pfec_fecha_inicio']); //// calcula nuevo/continuo
            $ap=$this->model_faseetapa->calcula_ap($fase[0]['pfec_fecha_inicio'],$fase[0]['pfec_fecha_fin']);

            $nro_fg_act = $this->model_faseetapa->nro_fasegestion_actual($fase[0]['id'],$this->session->userdata('gestion'));
            $fase_gest = $this->model_faseetapa->fase_etapa_gestion($fase[0]['id'],$this->session->userdata("gestion"));
            $nro++;
            $tabla.='<tr style="height:40px;" bgcolor="#e7fbf6">';
                $tabla.='<td>'.$nro.'</td>';
                $tabla.='<td>';
                    if($rol_id==3){
                        $tabla .= '
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-list"></i> OPCIONES <span class="caret"></span></button><ul class="dropdown-menu" role="menu">';
                            $tabla .= '<li><a href="' . site_url("").'/mod/opcion_fis/1/' . $fase[0]['id'] . '/' . $row['proy_id'] . '" title="MODIFICAR DATOS"><i class="glyphicon glyphicon-align-left"></i>&nbsp;MODIFICAR</a></li>';
                            $tabla .= '<li><a href="#" data-toggle="modal" data-target="#modal_derivar" class="btn btn-default modal_derivar" name="'.$row['proy_id'].'" id="2" title="ASIGNAR LA OPERACION A ANALISTA POA"><i class="glyphicon glyphicon-envelope"></i>&nbsp;DERIVAR A ANALISTA POA</li>';
                            $tabla .= '</ul></div>';
                    }
                    elseif($rol_id==4){
                         $tabla .= '
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-list"></i> OPCIONES <span class="caret"></span></button><ul class="dropdown-menu" role="menu">';
                            $tabla .= '<li><a href="#" data-toggle="modal" data-target="#modal_derivar" class="btn btn-default modal_derivar" name="'.$row['proy_id'].'" id="1" title="DEVOLVER OPERACI&Oacute;N A TECNICO OPERATIVO"><i class="glyphicon glyphicon-envelope"></i>&nbsp;DEVOLVER A T.U.E.</li>';
                            $tabla .= '<li><a href="#" data-toggle="modal" data-target="#modal_derivar" class="btn btn-default modal_derivar" name="'.$row['proy_id'].'" id="0" title="APROBAR OPERACI&Oacute;N"><i class="glyphicon glyphicon-envelope"></i>&nbsp;APROBAR OPERACI&Oacute;N</li>';
                            $tabla .= '</ul></div>';
                    }
                    else{
                        $tabla.='No definido';
                    }
                $tabla.='</td>';
                $tabla.='<td align=center>'.$row['aper_programa'].''.$row['aper_proyecto'].''.$row['aper_actividad'].'</td>';
                $tabla.='<td>'.$row['proy_nombre'].'</td>
                         <td>'.$row['tp_tipo'].'</td>
                         <td>'.$row['proy_sisin'].'</td>
                         <td>'.$row['fun_nombre'].' '.$row['fun_paterno'].' '.$row['fun_materno'].'</td>
                         <td>'.$row['ue'].'</td>
                         <td>'.$row['ur'].'</td>
                         <td>* '.$fase[0]['fase'].'<br>* '.$fase[0]['etapa'].'</td>
                         <td>'.$nc.'</td>
                         <td>'.$ap.'</td>
                         <td>'.number_format($fase[0]['pfec_ptto_fase'], 2, ',', '.').' Bs.</td>';
                        $tabla .='<td>';
                        if($nro_fg_act!=0 && ($fase_gest[0]['estado']==1 || $fase_gest[0]['estado']==2)){
                            $tabla .=''.number_format($fase_gest[0]['pfecg_ppto_total'], 2, ',', '.').' Bs.';
                        }
                        elseif ($nro_fg_act==0) {
                            $tabla .= '<font color="red">la gestion no esta en curso</font>';
                        }
                        $tabla .='</td>';
                        $tabla .='<td>';
                        if($this->model_faseetapa->verif_fase_etapa_gestion($fase[0]['id'],$this->session->userdata("gestion"))!=0){
                            if($this->model_faseetapa->nro_ffofet($fase_gest[0]['ptofecg_id'])!=0){
                                $techo=$this->model_faseetapa->techo_presupuestario($fase_gest[0]['ptofecg_id']);
                                $tabla .= ''.number_format($techo[0]['suma_techo'], 2, ',', '.').' Bs.';
                            }
                            else{$tabla .= "<font color=red>S/T</font>";}
                        }
                        $tabla .='</td>';
            $tabla.='</tr>';
        }

        return $tabla;
    }

    /*----- LISTA DE OPERACIONES APROBADAS -----*/
    public function lista_operaciones($tp){
        $tabla='';
        $lista_aper_padres = $this->model_proyecto->list_prog();//lista de aperturas padres

        foreach($lista_aper_padres  as $rowa){
            $proyectos=$this->model_mod->operaciones_aprobados($rowa['aper_programa'],$tp);
            if(count($proyectos)!=0){
                $tabla .='<tr bgcolor="#99DDF0" style="height:40px;">';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td align=center>'.$rowa['aper_programa'].''.$rowa['aper_proyecto'].''.$rowa['aper_actividad'].'</td>';
                $tabla .='<td>'.$rowa['aper_descripcion'].'</td>';
                $tabla .='<td>'.$rowa['aper_sisin'].'</td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='</tr>';
                $nro=0;
                foreach($proyectos  as $row){
                    if($row['tp_id']!=1){$color='#D7FCF9';}else{$color='#ffffff';}
                    $fase = $this->model_faseetapa->get_id_fase($row['proy_id']);
                    $nc=$this->model_faseetapa->calcula_nc($fase[0]['pfec_fecha_inicio']); //// calcula nuevo/continuo
                    $ap=$this->model_faseetapa->calcula_ap($fase[0]['pfec_fecha_inicio'],$fase[0]['pfec_fecha_fin']);

                    $nro_fg_act = $this->model_faseetapa->nro_fasegestion_actual($fase[0]['id'],$this->session->userdata('gestion'));
                    $fase_gest = $this->model_faseetapa->fase_etapa_gestion($fase[0]['id'],$this->session->userdata("gestion"));
                    $nro++;
                    $tabla.='<tr style="height:40px;">';
                        $tabla.='<td>'.$nro.'</td>';
                        if(count($this->model_mod->get_tp_estado($row['proy_id']))!=0){
                            $tabla.='<td title="LOS DATOS DE LA OPERACI&Oacute;N / PROYECTO ESTAN SIENDO MODIFICADOS"><a class="btn btn-danger">MODIFICANDO ..</a></td>';
                        }
                        else{
                            $tabla .= '<td><center><a href="#" data-toggle="modal" data-target="#modal_derivar" class="btn btn-default modal_derivar" name="'.$row['proy_id'].'" id="1" title="APROBAR OPERACI&Oacute;N - PROYECTO DE INVERSI&Oacute;N"> <img src="'.base_url().'assets/ifinal/ok1.jpg" WIDTH="35" HEIGHT="35"/></a></center></td>';
                        }
                        $tabla.='
                                <td align=center>'.$row['aper_programa'].''.$row['aper_proyecto'].''.$row['aper_actividad'].'</td>
                                <td>'.$row['proy_nombre'].'</td>
                                <td>'.$row['tp_tipo'].'</td>
                                <td>'.$row['proy_sisin'].'</td>
                                <td>'.$row['fun_nombre'].' '.$row['fun_paterno'].' '.$row['fun_materno'].'</td>
                                <td>'.$row['ue'].'</td>
                                <td>'.$row['ur'].'</td>
                                <td>* '.$fase[0]['fase'].'<br>* '.$fase[0]['etapa'].'</td>
                                <td>'.$nc.'</td>
                                <td>'.$ap.'</td>
                                <td>'.number_format($fase[0]['pfec_ptto_fase'], 2, ',', '.').' Bs.</td>';
                                $tabla .='<td>';
                                if($nro_fg_act!=0 && ($fase_gest[0]['estado']==1 || $fase_gest[0]['estado']==2)){
                                    $tabla .=''.number_format($fase_gest[0]['pfecg_ppto_total'], 2, ',', '.').' Bs.';
                                }
                                elseif ($nro_fg_act==0) {
                                    $tabla .= '<font color="red">la gestion no esta en curso</font>';
                                }
                                $tabla .='</td>';
                                $tabla .='<td>';
                                if($this->model_faseetapa->verif_fase_etapa_gestion($fase[0]['id'],$this->session->userdata("gestion"))!=0){
                                    if($this->model_faseetapa->nro_ffofet($fase_gest[0]['ptofecg_id'])!=0){
                                        $techo=$this->model_faseetapa->techo_presupuestario($fase_gest[0]['ptofecg_id']);
                                        $tabla .= ''.number_format($techo[0]['suma_techo'], 2, ',', '.').' Bs.';
                                    }
                                    else{$tabla .= "<font color=red>S/T</font>";}
                                }
                                $tabla .='</td>';

                    $tabla.='</tr>';
                }
            }
        }

        return $tabla;
    }

    /*----- OPCION - MODIFICACION OPERACION ---------*/
    public function opcion_modificacion_fisica($mod,$fase_id,$proy_id){
        $enlaces=$this->menu_modelo->get_Modulos_programacion(1);
        $data['enlaces'] = $enlaces;
        $data['nro_fase']= $this->model_faseetapa->nro_fase($proy_id); /// nro de fases y etapas registrados
        $data['proyecto'] = $this->model_proyecto->get_id_proyecto($proy_id); ////// DATOS DEL PROYECTO
        $data['id_f'] = $this->model_faseetapa->get_id_fase($proy_id); //// DATOS DE LA FASE ACTIVA
        $data['mod']=1;
        if(count($data['proyecto'])!=0 and count($data['id_f'])!=0){

            $this->load->view('admin/modificacion/poa/opcion_mod_fisica', $data);
        }
        else{
            redirect('admin/dashboard');
        }
    }

    /*----- OPCION - DERIVAR OPERACION PARA SU MODIFICACION ---------*/
    function derivar_proyecto(){
        if($this->input->is_ajax_request() && $this->input->post()){
            $this->form_validation->set_rules('id_p', 'id del proyecto', 'required|trim');
            //=========================== mensajes =========================================
            $this->form_validation->set_message('required', 'El campo es es obligatorio');
            /*if ($this->form_validation->run() ) {*/
            $post = $this->input->post();
            $id = $this->security->xss_clean($post['id_p']);
            $est_proy = $this->security->xss_clean($post['tp']);

            if($est_proy==0){
                $this->db->where('proy_id', $id);
                $this->db->delete('mod_est_proy');
            }
            else{
                if($est_proy==1){
                    $rol_id=3;
                }
                elseif ($est_proy==2) {
                    $rol_id=4;
                }

                if(count($this->model_mod->get_tp_estado($id))==0){
                    $data_to_store = array(
                        'proy_id' => $id,
                        'r_id' => $rol_id,
                    );
                    $this->db->insert('mod_est_proy', $data_to_store);
                }
                else{
                    $update_estado = array(
                        'r_id' => $rol_id
                    );
                    $this->db->where('proy_id', $id);
                    $this->db->update('mod_est_proy', $update_estado);
                }
            }

        }else{
            show_404();
        }
    }

    /*------------ GENERAR MENU --------------*/
    function menu($mod){
      $enlaces=$this->menu_modelo->get_Modulos($mod);
      for($i=0;$i<count($enlaces);$i++) {
        $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
      }

      $tabla ='';
      for($i=0;$i<count($enlaces);$i++){
          if(count($subenlaces[$enlaces[$i]['o_child']])>0){
              $tabla .='<li>';
                  $tabla .='<a href="#">';
                      $tabla .='<i class="'.$enlaces[$i]['o_image'].'"></i> <span class="menu-item-parent">'.$enlaces[$i]['o_titulo'].'</span></a>';    
                      $tabla .='<ul>';    
                          foreach ($subenlaces[$enlaces[$i]['o_child']] as $item) {
                          $tabla .='<li><a href="'.base_url($item['o_url']).'">'.$item['o_titulo'].'</a></li>';
                      }
                      $tabla .='</ul>';
              $tabla .='</li>';
          }
      }
      return $tabla;
    }
}