<?php
define("DOMPDF_ENABLE_REMOTE", true);
define("DOMPDF_TEMP_DIR", "/tmp");
class Reporte_nfisica extends CI_Controller { 
  public function __construct ()
    { 
        parent::__construct();
        if($this->session->userdata('fun_id')!=null){
        $this->load->library('pdf');
        $this->load->library('pdf2');
        $this->load->model('reportes_nfisfin/model_nfisfin');
        $this->load->model('reportes_tarija_das/mdas_complementario');

        $this->load->model('programacion/model_proyecto');
        $this->load->model('programacion/model_producto');
        $this->load->model('programacion/model_faseetapa');
        $this->load->model('programacion/model_componente');
        $this->load->model('programacion/model_reporte_global');
        $this->load->model('programacion/model_actividad');
        $this->load->model('ejecucion/model_ejecucion');
        $this->load->model('menu_modelo');
        $this->load->model('Users_model','',true);
        $this->gestion = $this->session->userData('gestion');
        $this->mes = $this->session->userData('mes');
        }else{
            redirect('/','refresh');
        }
    }


  /*=========================================== REPORTE GERENCIAL =======================================================*/
    /*------------------------------- SELECCION DE PROYECTOS FISICA-----------------------------------*/
    public function seleccion_reporte_fis()
    { 
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++)
        {
             $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        
        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];
        $data['das'] = $this->model_nfisfin->direcciones_administrativas();

        $this->load->view('admin/reportes/gerencial/nfisica/select_operaciones', $data);
    }
   
    public function validar_seleccion_resumen_fis()
    { 
        if($this->input->server('REQUEST_METHOD') === 'POST') 
        {
            if($this->input->post('p1')=='on'){$p1=1;}else{$p1=0;}
            if($this->input->post('p2')=='on'){$p2=1;}else{$p2=0;}
            if($this->input->post('p3')=='on'){$p3=1;}else{$p3=0;}
            if($this->input->post('p4')=='on'){$p4=1;}else{$p4=0;}
            if($this->input->post('p5')=='on'){$p5=1;}else{$p5=0;}

            $enlaces = $this->menu_modelo->get_Modulos(7);
            $data['enlaces'] = $enlaces;
            for ($i = 0; $i < count($enlaces); $i++) {
                $subenlaces[$enlaces[$i]['o_child']] = $this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
            }
            $data['subenlaces'] = $subenlaces;

            $meses=$this->get_mes($this->session->userdata("mes"));
            $mes = $meses[1];
            $dias = $meses[2];
            $mes_id=$this->session->userdata("mes");
            $data['id_mes'] = $mes_id;
            $data['mes'] = $mes;
            $data['dias'] = $dias;

            $pr1='';$pr2='';$pr3='';$pr4='';$pr5='';
            if($p1==1){$pr1='PROYECTO DE INVERSI&Oacute;N DIRECTA';}
            if($p2==1){$pr2=' - PROYECTO DE INVERSI&Oacute;N DELEGADA';}
            if($p3==1){$pr3=' - PROGRAMA RECURRENTE';}
            if($p4==1){$pr4=' - PROGRAMA NO RECURRENTE';}
            if($p5==1){$pr5=' - OPERACI&Oacute;N DE FUNCIONAMIENTO';}
            
            $data['pr1']=$pr1;
            $data['pr2']=$pr2;
            $data['pr3']=$pr3;
            $data['pr4']=$pr4;
            $data['pr5']=$pr5;

            $data['p1']=$p1;
            $data['p2']=$p2;
            $data['p3']=$p3;
            $data['p4']=$p4;
            $data['p5']=$p5;

            $data['v1']=$this->input->post('v1');
            $data['v2']=$this->input->post('v2');
            $data['da_id']=$this->input->post('dea_id');

            $v=$this->datos_tp($p1,$p2,$p3,$p4,$p5); /// validando para la consulta sql

            if($this->input->post('dea_id')==0){
              $data['proyectos']=$this->list_ejecucion_fisica_todos($v[1],$v[2],$v[3],$v[4],$v[5],$v[6],$this->input->post('v1'),$this->input->post('v2'),1);
            // echo $v[1].''.$v[2].''.$v[3].''.$v[4].''.$v[5].''.$v[6].''.$this->input->post('v1').''.$this->input->post('v2');
              $data['title']=' Todas las Das';
            }
            else{
              $data['proyectos']=$this->list_ejecucion_fisica_da($v[1],$v[2],$v[3],$v[4],$v[5],$v[6],$this->input->post('v1'),$this->input->post('v2'),$this->input->post('dea_id'),1);
              $data['title']=' Por Das';
            }

           $this->load->view('admin/reportes/gerencial/nfisica/operaciones', $data);   
        }
        else{
          $this->session->set_flashdata('danger','Problemas al realizar la consulta, contactese con el Web Master');
          redirect('rep/ejec_fis');
        }
    }

    /*--------------------------- LISTA DE PROYECTOS DE TODAS LAS DAS ---------------*/
     function list_ejecucion_fisica_todos($f1,$f2,$p1,$p3,$p4,$p5,$valor1,$valor2,$tp_rep){
        $das = $this->model_nfisfin->direcciones_administrativas();
        if($tp_rep==1){
        $tb='id="dt_basic" class="table table table-bordered" width="100%"';
        }
        elseif ($tp_rep==2) {
          $tb='border="0" cellpadding="0" cellspacing="0" class="tabla"';
        }

        $nro=0;
        $tabla ='';
        $tabla .='<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <h2 class="alert alert-success"><center>EJECUCI&Oacute;N F&Iacute;SICA '.$valor1.' - '.$valor2.'% </center></h2>
                  </article>';

          $proy = $this->model_nfisfin->lista_programas($f1,$f2,$p1,$p3,$p4,$p5);
          if(count($proy)!=0){
            $nro++;
            $tabla .='
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-darken" >
                        <header>
                          <span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
                          <h2 class="font-md"><strong>TODAS LAS DIRECCIONES ADMINISTRATIVAS</strong></h2>  
                        </header>
                <div>
                  <div class="widget-body no-padding">
                  <div class="table-responsive">';
            $tabla .='<table '.$tb.'>';
            $tabla .='<thead>';
            $tabla .='<tr class="modo1">';
              $tabla .='<th style="width:1%;">#</th>';
              $tabla .='<th style="width:15%;">DIRECCI&Oacute;N ADMINISTRATIVA</th>';
              $tabla .='<th style="width:15%;">UNIDAD EJECUTORA</th>';
              $tabla .='<th style="width:15%;">PROYECTO_PROGRAMA_OPERACI&Oacute;N</th>';
              $tabla .='<th style="width:10%;">TIPO DE OPERACI&Oacute;N</th>';
              $tabla .='<th>PROGRAMADO</th>';
              $tabla .='<th>EJECUC&Iacute;N f&Iacute;SICA ACUMULADO</th>';
              $tabla .='<th>EFICACIA</th>';
              $tabla .='<th>AVANCE FINANCIERO</th>';
              $tabla .='<th>ESTADO</th>';
            $tabla .='</tr>';
            $tabla .='</thead>';
            $nrop=0;
            $tabla .='<tbody>';
              foreach($proy as $rowp)
              {
                $fis=$this->avance_fisico($rowp['proy_id'],$this->mes);
                $fin=$this->avance_financiero($rowp['proy_id']);
                $estado=$this->estado_proyecto($rowp['proy_id']);
                if($fis[2]>=$valor1 & $fis[2]<=$valor2){
                    $nrop++;
                    $tabla .='<tr class="modo1">';
                      $tabla .='<td>'.$nrop.'</td>';
                      $tabla .='<td>'.$rowp['das'].'</td>';
                      $tabla .='<td>'.$rowp['u_ejec'].'</td>';
                      $tabla .='<td>'.$rowp['proy_nombre'].'</td>';
                      $tabla .='<td>'.$rowp['tp_tipo'].'</td>';
                      $tabla .='<td>'.$fis[1].'%</td>';
                      $tabla .='<td bgcolor="#c9eac9">'.$fis[2].'%</td>';
                      $tabla .='<td>'.$fis[3].'%</td>';
                      $tabla .='<td>'.$fin.'%</td>';
                      $tabla .='<td>'.$estado.'</td>';
                    $tabla .='</tr>';
                }
              }
              $tabla .='</tbody>';
                $tabla .='<tr bgcolor="#f5f5f5">';
                  $tabla .='<td></td>';
                  $tabla .='<td><b>TOTAL : '.$nrop.'</b></td>';
                  $tabla .='<td></td>';
                  $tabla .='<td></td>';
                  $tabla .='<td></td>';
                  $tabla .='<td></td>';
                  $tabla .='<td></td>';
                  $tabla .='<td></td>';
                  $tabla .='<td></td>';
                  $tabla .='<td></td>';
                $tabla .='</tr>';
              $tabla .='</table>';
              $tabla .='</div>

                  </div>
                </div>
              </div>
            </article>';
          }

      return $tabla;
     }

      /*------------------- LISTA PROYECTOS SEGUN LA DA SELECCIONADO -------------------*/
      function list_ejecucion_fisica_da($f1,$f2,$p1,$p3,$p4,$p5,$valor1,$valor2,$da_id,$tp_rep){
        $da = $this->model_nfisfin->get_unidad_organizacional($da_id);
        if($tp_rep==1){
        $tb='id="dt_basic" class="table table table-bordered" width="100%"';
        }
        elseif ($tp_rep==2) {
          $tb='border="0" cellpadding="0" cellspacing="0" class="tabla"';
        }

        $nro=0;
        $tabla ='';
        $tabla .='<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <h2 class="alert alert-success"><center>EJECUCI&Oacute;N F&Iacute;SICA '.$valor1.' - '.$valor2.'% </center></h2>
                  </article>';

          $proy = $this->model_nfisfin->get_proyectos_prog_fis($f1,$f2,$p1,$p3,$p4,$p5,$da_id);
          if(count($proy)!=0){
            $nro++;
            $tabla .='
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-darken" >
                        <header>
                          <span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
                          <h2 class="font-md"><strong>'.$da[0]['uni_unidad'].'</strong></h2>  
                        </header>
                <div>
                  <div class="widget-body no-padding">
                  <div class="table-responsive">';
            $tabla .='<table '.$tb.'>';
            $tabla .='<thead>';
            $tabla .='<tr class="modo1">';
              $tabla .='<th style="width:1%;">#</th>';
              $tabla .='<th style="width:15%;">DIRECCI&Oacute;N ADMINISTRATIVA</th>';
              $tabla .='<th style="width:15%;">UNIDAD EJECUTORA</th>';
              $tabla .='<th style="width:15%;">PROYECTO_PROGRAMA_OPERACI&Oacute;N</th>';
              $tabla .='<th style="width:10%;">TIPO DE OPERACI&Oacute;N</th>';
              $tabla .='<th>PROGRAMADO</th>';
              $tabla .='<th>EJECUTADO</th>';
              $tabla .='<th>EFICACIA</th>';
              $tabla .='<th>AVANCE FINANCIERO</th>';
              $tabla .='<th>ESTADO</th>';
            $tabla .='</tr>';
            $tabla .='</thead>';
            $nrop=0;
            $tabla .='<tbody>';
              foreach($proy as $rowp)
              {
                $fis=$this->avance_fisico($rowp['proy_id'],$this->mes);
                $fin=$this->avance_financiero($rowp['proy_id']);
                $estado=$this->estado_proyecto($rowp['proy_id']);
                if($fis[2]>=$valor1 & $fis[2]<=$valor2){
                    $nrop++;
                    $tabla .='<tr class="modo1">';
                      $tabla .='<td>'.$nrop.'</td>';
                      $tabla .='<td>'.$rowp['das'].'</td>';
                      $tabla .='<td>'.$rowp['u_ejec'].'</td>';
                      $tabla .='<td>'.$rowp['proy_nombre'].'</td>';
                      $tabla .='<td>'.$rowp['tp_tipo'].'</td>';
                      $tabla .='<td>'.$fis[1].'%</td>';
                      $tabla .='<td bgcolor="#c9eac9">'.$fis[2].'%</td>';
                      $tabla .='<td>'.$fis[3].'%</td>';
                      $tabla .='<td>'.$fin.'%</td>';
                      $tabla .='<td>'.$estado.'</td>';
                    $tabla .='</tr>';
                }
              }
              $tabla .='</tbody>';
                $tabla .='<tr bgcolor="#f5f5f5">';
                  $tabla .='<td></td>';
                  $tabla .='<td><b>TOTAL : '.$nrop.'</b></td>';
                  $tabla .='<td></td>';
                  $tabla .='<td></td>';
                  $tabla .='<td></td>';
                  $tabla .='<td></td>';
                  $tabla .='<td></td>';
                  $tabla .='<td></td>';
                  $tabla .='<td></td>';
                  $tabla .='<td></td>';
                $tabla .='</tr>';
              $tabla .='</table>';
              $tabla .='</div>

                  </div>
                </div>
              </div>
            </article>';
          }
      return $tabla;
     }

     function list_ejecucion_fisica_todos2($f1,$f2,$p1,$p3,$p4,$p5,$valor1,$valor2,$tp_rep){
        $das = $this->model_nfisfin->direcciones_administrativas();
        $nro=0;
        $tabla ='';
        $tabla .='<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <h2 class="alert alert-success"><center>PROYECTOS ENTRE '.$valor1.' - '.$valor2.'% de Avance F&iacute;sico</center></h2>
                  </article>';
        foreach($das as $rowd)
        {
          $proy = $this->model_nfisfin->get_proyectos_prog_fis($f1,$f2,$p1,$p3,$p4,$p5,$rowd['uni_id']);
          if(count($proy)!=0){
            $nro++;
            $tabla .='
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-darken" >
                        <header>
                          <span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
                          <h2 class="font-md"><strong>'.$rowd['uni_unidad'].'</strong></h2>  
                        </header>
                <div>
                  <div class="widget-body no-padding">
                  <div class="table-responsive">';
            $tabla .='<table id="dt_basic'.$nro.'" class="table table table-bordered" width="100%">';
            $tabla .='<thead>';
            $tabla .='<tr>';
              $tabla .='<th style="width:1%;">#</th>';
              $tabla .='<th style="width:15%;">DIRECCI&Oacute;N ADMINISTRATIVA</th>';
              $tabla .='<th style="width:15%;">UNIDAD EJECUTORA</th>';
              $tabla .='<th style="width:15%;">PROYECTO_PROGRAMA_OPERACI&Oacute;N</th>';
              $tabla .='<th style="width:10%;">TIPO DE OPERACI&Oacute;N</th>';
              $tabla .='<th>PROGRAMADO</th>';
              $tabla .='<th>EJECUTADO</th>';
              $tabla .='<th>EFICACIA</th>';
              $tabla .='<th>AVANCE FINANCIERO</th>';
              $tabla .='<th>ESTADO</th>';
            $tabla .='</tr>';
            $tabla .='</thead>';
            $nrop=0;
            $tabla .='<tbody>';
              foreach($proy as $rowp)
              {
                $fis=$this->avance_fisico($rowp['proy_id'],$this->mes);
                $fin=$this->avance_financiero($rowp['proy_id']);
                $estado=$this->estado_proyecto($rowp['proy_id']);
                if($fis[2]>=$valor1 & $fis[2]<=$valor2){
                    $nrop++;
                    $tabla .='<tr>';
                      $tabla .='<td>'.$nrop.'</td>';
                      $tabla .='<td>'.$rowp['das'].'</td>';
                      $tabla .='<td>'.$rowp['u_ejec'].'</td>';
                      $tabla .='<td>'.$rowp['proy_nombre'].'</td>';
                      $tabla .='<td>'.$rowp['tp_tipo'].'</td>';
                      $tabla .='<td>'.$fis[1].'%</td>';
                      $tabla .='<td>'.$fis[2].'%</td>';
                      $tabla .='<td bgcolor="#c9eac9">'.$fis[3].'%</td>';
                      $tabla .='<td>'.$fin.'%</td>';
                      $tabla .='<td>'.$estado.'</td>';
                    $tabla .='</tr>';
                }
              }
              $tabla .='</tbody>';
                $tabla .='<tr bgcolor="#f5f5f5">';
                  $tabla .='<td></td>';
                  $tabla .='<td><b>TOTAL : '.$nrop.'</b></td>';
                  $tabla .='<td></td>';
                  $tabla .='<td></td>';
                  $tabla .='<td></td>';
                  $tabla .='<td></td>';
                  $tabla .='<td></td>';
                  $tabla .='<td></td>';
                  $tabla .='<td></td>';
                  $tabla .='<td></td>';
                $tabla .='</tr>';
              $tabla .='</table>';
              $tabla .='</div>

                  </div>
                </div>
              </div>
            </article>';
          }
        }

      return $tabla;
     }

    /*-------------------- Avance Financiero -------------------*/
    public function avance_financiero($proy_id)
    {  
        $efin = $this->model_ejecucion->datos_programa_presupuesto($proy_id,$this->mes,$this->gestion);
        $fin=0;
        if(count($efin)!=0){
            if($efin[0]['pe_pv']!=0){
              $fin=round((($efin[0]['pe_pe']/$efin[0]['pe_pv'])*100),2);  
            }
        }
      return $fin;
    }

    /*-------------------- Avance Fisico -------------------*/
    public function avance_fisico($proy_id,$id_mes)
    {  
      $fase = $this->model_faseetapa->get_id_fase($proy_id);
      $años=$fase[0]['pfec_fecha_fin']-$fase[0]['pfec_fecha_inicio']+1;
      $meses=$años*12;

      $ms[0]='programado_total';$ms[1]='enero';$ms[2]='febrero';$ms[3]='marzo';$ms[4]='abril';$ms[5]='mayo';$ms[6]='junio';
      $ms[7]='julio';$ms[8]='agosto';$ms[9]='septiembre';$ms[10]='octubre';$ms[11]='noviembre';$ms[12]='diciembre';

      $componentes = $this->model_componente->componentes_id($fase[0]['id']);
      for($p=1;$p<=$meses;$p++){$cp[1][$p]=0; $cp[2][$p]=0; $cp[3][$p]=0;}
      foreach ($componentes as $rowc)
      {
        $productos = $this->model_producto->list_prod($rowc['com_id']);
        for($p=1;$p<=$meses;$p++){$pp[1][$p]=0; $pp[2][$p]=0;}
       // echo "COMPONENTE : ".$rowc['com_componente']."<br>";
        foreach ($productos as $rowp)
        {
            $actividad = $this->model_actividad->list_act_anual($rowp['prod_id']);
          //  echo "PRODUCTO : ".$rowp['prod_producto']."<br>";
            for($p=1;$p<=$meses;$p++){$ap[1][$p]=0; $ap[2][$p]=0;}
            foreach ($actividad as $rowa)
            {   
              $variablep=1;$variablee=1; $v=12; $nro=1; $sump=0;$sume=0;
              for($p=1;$p<=$meses;$p++){$a[1][$p]=0; $a[2][$p]=0;$a[3][$p]=0;$a[4][$p]=0;$a[5][$p]=0;$a[6][$p]=0;} //// vector vacio para sumar a nivel de actividades programacion , ejecutado
              for($i=$fase[0]['pfec_fecha_inicio'];$i<=$fase[0]['pfec_fecha_fin'];$i++)
              {
                $aprog = $this->model_actividad->actividad_programado($rowa['act_id'],$i);
                if(count($aprog)!=0){
                  for ($j=1; $j <=12 ; $j++) { 
                    $a[1][$variablep]=$aprog[0][$ms[$j]];
                    $variablep++;
                  }
                }
                else{
                  $variablep=($v*$nro)+1;
                }

                $aejec = $this->model_actividad->actividad_ejecutado($rowa['act_id'],$i);
                if(count($aejec)!=0){
                  for ($j=1; $j <=12 ; $j++) { 
                    $a[4][$variablee]=$aejec[0][$ms[$j]];
                    $variablee++;
                  }
                }
                else{
                  $variablee=($v*$nro)+1;
                }

                $nro++;
              }

              for ($i=1; $i <=$meses ; $i++) { 
                $sump=$sump+$a[1][$i];
                $a[2][$i]=$sump+$rowa['act_linea_base'];
                
                if($rowa['act_meta']!=0)
                {
                  $a[3][$i]=round(((($sump+$rowa['act_linea_base'])/$rowa['act_meta'])*100),1);
                }
                $ap[1][$i]=$ap[1][$i]+(($a[3][$i]*$rowa['act_ponderacion'])/100);

                $sume=$sume+$a[4][$i];
                $a[5][$i]=$sume+$rowa['act_linea_base'];

                if($rowa['act_meta']!=0)
                {
                  $a[6][$i]=round(((($sume+$rowa['act_linea_base'])/$rowa['act_meta'])*100),1);
                }
                $ap[2][$i]=$ap[2][$i]+(($a[6][$i]*$rowa['act_ponderacion'])/100);
              }

            }

              for ($i=1; $i <=$meses ; $i++) {
                $pp[1][$i]=$pp[1][$i]+(($ap[1][$i]*$rowp['prod_ponderacion'])/100);
                $pp[2][$i]=$pp[2][$i]+(($ap[2][$i]*$rowp['prod_ponderacion'])/100);
              } 
              
        }
          for ($i=1; $i <=$meses ; $i++) {
                $cp[1][$i]=$cp[1][$i]+(($pp[1][$i]*$rowc['com_ponderacion'])/100);
                $cp[2][$i]=$cp[2][$i]+(($pp[2][$i]*$rowc['com_ponderacion'])/100);
              }

      }

      for ($i=1; $i <=$meses ; $i++) {
        if($cp[1][$i]!=0){
          $cp[3][$i]=round((($cp[2][$i]/$cp[1][$i])*100),1);
        }
      }

      for ($i=1; $i <=12 ; $i++) { 
        $tefic[1][$i]=0;$tefic[2][$i]=0;$tefic[3][$i]=0;
      }
      $i=1;$vmeses=12;$gestion=$fase[0]['pfec_fecha_inicio'];$nro=1;
      for($p=$fase[0]['pfec_fecha_inicio'];$p<=$fase[0]['pfec_fecha_fin'];$p++)
      {
          $variable=0;
          for($pos=$i;$pos<=$vmeses;$pos++)
          {
            $variable++;
            $tefic[1][$variable]=$cp[1][$pos];
            $tefic[2][$variable]=$cp[2][$pos];
            $tefic[3][$variable]=$cp[3][$pos];
          }
          if($p==$this->session->userdata('gestion'))
          {
              $ejecucion[1]=round($tefic[1][$id_mes],2);
              $ejecucion[2]=round($tefic[2][$id_mes],2);
              $ejecucion[3]=round($tefic[3][$id_mes],2);
          }

            $i=$vmeses+1;
            $vmeses=$vmeses+12;
            $gestion++; $nro++;
      }

      return $ejecucion;
  }

    /*-------------------- Estado Actual del proyecto -------------------*/
    public function estado_proyecto($proy_id)
    { 
      $fase = $this->model_faseetapa->get_id_fase($proy_id);
      $estado=$this->mdas_complementario->estado_operacion($fase[0]['id'],$this->mes,$this->gestion);
      $estado_proy='-----';
        if(count($estado)!=0){
            $estado_proy=$estado[0]['ep_descripcion'];
        }

      return $estado_proy;
    }

    function datos_tp($p1,$p2,$p3,$p4,$p5){
        if($p1==1 & $p2==1 & $p3==0 & $p4==0 & $p5==0){
          $v[1]=1;$v[2]=1;$v[3]=1;$v[4]=0;$v[5]=0;$v[6]=0;
         // echo "1,1,1,0,0,0";
        }
        elseif($p1==1 & $p2==0 & $p3==0 & $p4==0 & $p5==0){
          $v[1]=1;$v[2]=0;$v[3]=1;$v[4]=0;$v[5]=0;$v[6]=0;
         // echo "1,0,1,0,0,0";
        }
        elseif($p1==0 & $p2==1 & $p3==0 & $p4==0 & $p5==0){
          $v[1]=0;$v[2]=1;$v[3]=1;$v[4]=0;$v[5]=0;$v[6]=0;
         // echo "0,1,1,0,0,0";
        }
        elseif($p1==0 & $p2==0 & $p3==1 & $p4==0 & $p5==0){
          $v[1]=1;$v[2]=0;$v[3]=0;$v[4]=1;$v[5]=0;$v[6]=0;
         // echo "1,0,0,1,0,0";
        }
        elseif($p1==0 & $p2==0 & $p3==0 & $p4==1 & $p5==0){
          $v[1]=1;$v[2]=0;$v[3]=0;$v[4]=0;$v[5]=1;$v[6]=0;
         // echo "1,0,0,0,1,0";
        }
        elseif($p1==0 & $p2==0 & $p3==0 & $p4==0 & $p5==1){
          $v[1]=1;$v[2]=0;$v[3]=0;$v[4]=0;$v[5]=0;$v[6]=1;
         // echo "1,0,0,0,0,1";
        }
        elseif($p1==1 & $p2==0 & ($p3==1 || $p4=1 || $p5==1)){
          $v[1]=1;$v[2]=0;$v[3]=1;$v[4]=$p3;$v[5]=$p4;$v[6]=$p5;
         // echo "1,0,1,".$p3.",".$p4.",".$p5."";
        }
        elseif($p1==0 & $p2==1 & ($p3==1 || $p4==1 || $p5==1)){
          $v[1]=1;$v[2]=1;$v[3]=1;$v[4]=$p3;$v[5]=$p4;$v[6]=$p5;
         // echo "1,1,1,".$p3.",".$p4.",".$p5."";
        }
        elseif($p1==1 & $p2==1 & ($p3==1 || $p4==1 || $p5==1)){
          $v[1]=1;$v[2]=1;$v[3]=1;$v[4]=$p3;$v[5]=$p4;$v[6]=$p5;
         // echo "1,1,1,".$p3.",".$p4.",".$p5."";
        }
        if($p1==1 & $p2==1 & $p3==1 & $p4==1 & $p5==1){
          $v[1]=1;$v[2]=1;$v[3]=1;$v[4]=1;$v[5]=1;$v[6]=1;
         // echo "1,1,1,1,1,1";
        }
        return $v;
     }



  public function reporte_ejecucion_niveles($p1,$p2,$p3,$p4,$p5,$v1,$v2,$da_id)
   {
    $mess=$this->get_mes($this->session->userdata("mes"));
    $mes = $mess[1];
    $dias = $mess[2];

    $pr1='';$pr2='';$pr3='';$pr4='';$pr5='';
    if($p1==1){$pr1='PROYECTO DE INVERSI&Oacute;N DIRECTA';}
    if($p2==1){$pr2=' - PROYECTO DE INVERSI&Oacute;N DELEGADA';}
    if($p3==1){$pr3=' - PROGRAMA RECURRENTE';}
    if($p4==1){$pr4=' - PROGRAMA NO RECURRENTE';}
    if($p5==1){$pr5=' - OPERACI&Oacute;N DE FUNCIONAMIENTO';}

    $v=$this->datos_tp($p1,$p2,$p3,$p4,$p5); /// validando para la consulta sql
    if($da_id==0){
      $tabla=$this->list_ejecucion_fisica_todos($v[1],$v[2],$v[3],$v[4],$v[5],$v[6],$v1,$v2,2);
    }
    else{
      $tabla=$this->list_ejecucion_fisica_da($v[1],$v[2],$v[3],$v[4],$v[5],$v[6],$v1,$v2,$da_id,2);
    }

    $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr >
                        <td width=20%;>
                            <center><img src="'.base_url().'assets/img/logo.jpg" class="siipp"/></center>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata("entidad").'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$this->gestion.'<br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                            <b>REPORTE : </b> Ejecuci&oacute;n F&iacute;sica a nivel de '.$pr1.''.$pr2.''.$pr3.''.$pr4.''.$pr5.' <br>
                            (De 1 de ENERO Al de '.$dias.' de '.$mes.' de '.$this->gestion.') (Expresado en Bolivianos)
                        </td>
                        <td width=20%;>
                            <center><img src="'.base_url().'assets/img/escudo.jpg" alt="" width="90px"></center>
                        </td>
                    </tr>
                </table>
                <footer>
                    <table class="table table-bordered" width="100%">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata('sistema_pie').'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>
                <div>
                  '.$tabla.'
                </div>
            </body>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'landscape');
        ini_set('memory_limit','-1');
        ini_set('max_execution_time', 0);
        $dompdf->render();
        $dompdf->stream("reporte_ejecucion.pdf", array("Attachment" => false));
   }


  private $estilo_vertical = 
    '<style>
        body{
            font-family: sans-serif;
            }
        @page {
        margin: 40px 40px;
        }
        header {
            position: fixed;
            left: 0px;
            top: -20px;
            right: 0px;
            height: 20px;
            background-color: #fff;
            }
        header h1{
            margin: 5px 0;
        }
        header h2{
            margin: 0 0 10px 0;
        }

        footer {
            position: fixed;
            left: 0px;
            bottom: -82px;
            right: 0px;
            height: 100px;
            border-bottom: 1px solid #ddd;
        }
        footer .page:after {
            content: counter(page) ;
        }
        footer table {
            width: 100%;
        }
        footer p {
            text-align: right;
        }
        footer .izq {
            text-align: left;
        }
      table{
            font-size: 7px;
            width: 100%;
            background-color:#fff;
        }
        .mv{font-size:10px;}
        .siipp{width:120px;}

      .titulo_pdf {
            text-align: left;
            font-size: 10px;
        }
        .tabla {
      font-family: Verdana, Arial, Helvetica, sans-serif;
      font-size: 7px;
                  width: 100%;

      }
              .tabla th {
      padding: 5px;
      font-size: 7px;
      background-color: #83aec0;
      background-image: url(fondo_th.png);
      background-repeat: repeat-x;
      color: #FFFFFF;
      border-right-width: 1px;
      border-bottom-width: 1px;
      border-right-style: solid;
      border-bottom-style: solid;
      border-right-color: #558FA6;
      border-bottom-color: #558FA6;
      font-family: "Trebuchet MS", Arial;
      text-transform: uppercase;
      }
      .tabla .modo1 {
      font-size: 7px;
      font-weight:bold;

      background-image: url(fondo_tr01.png);
      background-repeat: repeat-x;
      color: #34484E;
      font-family: "Trebuchet MS", Arial;
      }
      .tabla .modo1 td {
      padding: 5px;
      border-right-width: 1px;
      border-bottom-width: 1px;
      border-right-style: solid;
      border-bottom-style: solid;
      border-right-color: #A4C4D0;
      border-bottom-color: #A4C4D0;

      }
    </style>';

  /*=========================================================================================================================*/
    public function get_mes($mes_id)
    {
      $mes[1]='ENERO';
      $mes[2]='FEBRERO';
      $mes[3]='MARZO';
      $mes[4]='ABRIL';
      $mes[5]='MAYO';
      $mes[6]='JUNIO';
      $mes[7]='JULIO';
      $mes[8]='AGOSTO';
      $mes[9]='SEPTIEMBRE';
      $mes[10]='OCTUBRE';
      $mes[11]='NOVIEMBRE';
      $mes[12]='DICIEMBRE';

      $dias[1]='31';
      $dias[2]='28';
      $dias[3]='31';
      $dias[4]='30';
      $dias[5]='31';
      $dias[6]='30';
      $dias[7]='31';
      $dias[8]='31';
      $dias[9]='30';
      $dias[10]='31';
      $dias[11]='30';
      $dias[12]='31';

      $valor[1]=$mes[$mes_id];
      $valor[2]=$dias[$mes_id];

      return $valor;
    }
}