<?php

class Sgp extends CI_Controller
{
//	public $rol = array('1' => '3','2' => '4','3' => '5');
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Users_model', '', true);
        $this->load->model('reportes/taqpacha/model_taqpacha','model_taqpacha');
        $this->load->model('menu_modelo');
        $this->load->model('programacion/model_faseetapa');
        $this->load->model('programacion/model_proyecto');
        $this->load->model('reportes/model_objetivo');
        $this->load->model('programacion/model_componente');
        $this->load->model('programacion/model_producto');
        $this->load->model('programacion/model_actividad');
        $this->load->model('programacion/insumos/mfinanciera');
    }

    public function reporte($proy_id)
    {
/*		if($proy_id == NULL){
			redirect('error_404');
		}*/
//		$proy_id = 617;//PROYECTO IXIAMAS
//		$proy_id = 860;//PROGRAMA CAMBIO CLIMATICO
        if($this->session->userdata('logged')){

		  $data['gestion'] = $this->session->userdata('gestion');
		  $data['mes_id'] = $this->session->userdata('mes');
		  
		  $resp_dictamen = $this->model_objetivo->get_dictamen2($proy_id, $this->session->userdata('gestion'));
		  $data['dictamen'] = $resp_dictamen->row_array();
          $data['unidad_responsable']=$this->model_proyecto->responsable_proy($proy_id,2); //// unidad responsable
          $data['unidad_ejecutora']=$this->model_proyecto->responsable_proy($proy_id,1);  //// unidad ejecutora
		  $localizacion = $this->model_objetivo->get_localizacion_dictamen($proy_id);
		  $data['localizacion'] = $localizacion->result_array();
		  $res_metas = $this->model_objetivo->get_indicadores_metas($proy_id);
		  $data['metas'] = $res_metas->result_array();
          $res_componentes = $this->model_objetivo->get_componentes_dictamen($proy_id);
          $data['componentes'] = $res_componentes->result_array();
		  
		  
          $fase=$this->model_faseetapa->fase_etapa($data['dictamen']['pfec_id'],$proy_id);
		  $data['presupuesto_total'] = number_format($fase[0]['pfec_ptto_fase'], 2, ',', '.');
		  
		  
		  $enlaces=$this->menu_modelo->get_Modulos(7);
		  $data['enlaces'] = $enlaces;

		  $mod = 4;
		  $id_p = $proy_id;
		  $id_f = $fase[0]['id'];// ID DE LA FASE ACTIVA DEL PROYECTO / PROGRAMA
		  
//		  $data['datos_generales'] = $this->datos_generales($resp_dictamen->row_array(), $this->model_proyecto->responsable_proy($proy_id,1), $localizacion->result_array(), $res_metas->result_array(), $res_componentes->result_array());
		  $data['pe_fisica']=$this->programacion_fisica_pluri($mod,$id_f,$id_p); //programacion ejecucion fisica
		  $data['pe_financiera']=$this->prog_financiera($mod,$id_f,$id_p);//programacion ejecucion financiera
		  
		  
/*		  for($i=0;$i<count($enlaces);$i++)
		  {
			$subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
		  }
		  */
//		  $data['subenlaces'] = $subenlaces;
//		  $html = $this->load->view('reportes/sgp/sgp1', $data, TRUE);
//		  $html = $this->output->get_output(); 
		  return $data;
		}
		else{
		  return 'ERROR';	
//		  redirect('admin/dashboard');
		} 
    }
/*	
	public function datos_generales($dictamen,$unidad_ejecutora, $localizacion, $metas, $componentes){
		$data = '';
		$data .= '
<style>
		.jarviswidget > div {
			font-size:1.3em;
		}
		.titulo, .titulo_s{
			margin:15px 0 15px 0;
			padding:0 0 5px 0;
			border-bottom:2px solid #ccc;
			text-align:center;
		}
		.titulo_s{
		border-bottom:0;
		}
		table{
			margin:5px 0;
			border:3px solid #bbb;
			font-size:1.1em !important;
		}
		table.tabla-indicadores tr th, 
		table.tabla-indicadores tr td, 
		table.tabla-modalidad tr th, 
		table.tabla-modalidad tr td, 
		table.tabla-metas tr th, 
		table.tabla-metas tr td{
			font-size:15px !important;
		}

		table td, table th{
			border:2px solid #ccc;
		}
		table td{
			background-color:rgb(245,245,255);
		}
		table th{
			background-color:rgb(235,235,240);
		}
		th{
			text-align:center;
		}
		.border1{
			border:2px solid #bbb;
			background-color:rgb(245,245,255);
			padding-top:5px;
			padding-bottom:5px;
		}
		.bg-green{
			background-color:#b9e5a9;
		}
		.bg-warning{
			background-color:#ffe88c;
		}
		.bg-info{
			background-color:#9bc1ff;
		}
		.bg-gray{
			background-color:#ccc;
		}
		.bg-gray2{
			background-color:#ddd;
		}
		.bg-primary{
			background-color:#4786ad;
		}
</style>		
<!-- IDENTIFICACIÓN DE LA ACCIÓN INSTITUCIONAL -->
												<h2 class="titulo_s">
													<!--span style="font-family:Edward; font-size:2.2em;">Centro de Investigación Agrícola Tropical</span>
													<br><br>-->
													<span class="text-primary">SISTEMA INTEGRADO DE PLANIFICACIÓN, SEGUIMIENTO Y EVALUACIÓN</span>
													<br>
													REPORTE SGPP
												</h2>
											
											<h2 class="titulo"><b>1. IDENTIFICACIÓN DEL PROYECTO - PROGRAMA</b></h2>
											<div class="row">
												<div class="col-md-2 text-primary text-right"><b>Nombre de la operación:</b></div>
												<div class="col-md-5 border1">'.$dictamen['proy_nombre'].'</div>
												<div class="col-md-2 text-primary text-right"><b>Código SISIN:</b></div>
												<div class="col-md-3 border1" style="font-size:1.4em; font-weight:bold;">
												';
												
							 if($dictamen['proy_sisin'] == ''){
								 $tabla.= '-'; 
							 }else{ 
								 $tabla.= ''.$dictamen['proy_sisin'];
							 }
							 
$tabla.= '							 </div>
											</div>
											<br>
											<div class="row">
												<div class="col-md-2 text-primary text-right"><b>Funcionario responsable:</b></div>
												<div class="col-md-5 border1">'.$unidad_ejecutora[0]['fun_nombre'].' '.$unidad_ejecutora[0]['fun_paterno'].' '.$unidad_ejecutora[0]['fun_materno'].'</div>
												<div class="col-md-2 text-primary text-right"><b>Cargo:</b></div>
												<div class="col-md-3 border1">'.$unidad_ejecutora[0]['fun_cargo'].'</div>
											</div>
											<h4 class="titulo"><b>CLASIFICACIÓN SECTORIAL</b></h4>
											<div class="row">
												<div class="col-md-2 text-primary text-right"><b>Código Sectorial:</b></div>
												<div class="col-md-2 border1" style="font-size:1.4em; font-weight:bold;">'.$dictamen['codsectorial'].'</div>
												<div class="col-md-8">
													<div class="row">
														<div class="col-md-4 text-primary text-right"><b>Sector:</b></div>
														<div class="col-md-8 border1">'.explode("-",$dictamen['codsectorial'])[0].'.'.$dictamen['c_sect_sector'].'</div>
													</div>
													<div class="row">
														<div class="col-md-4 text-primary text-right"><b>Subsector:</b></div>
														<div class="col-md-8 border1">'.explode("-",$dictamen['codsectorial'])[1].'.'.$dictamen['c_sect_subsector'].'</div>
													</div>
													<div class="row">
														<div class="col-md-4 text-primary text-right"><b>Actividad Económica:</b></div>
														<div class="col-md-8 border1">'.explode("-",$dictamen['codsectorial'])[2].'.'.$dictamen['c_sect_actividad'].'</div>
													</div>
												</div>
											</div>
											<h4 class="titulo"><b>LOCALIZACIÓN</b></h4>
											<div class="row">
												<div class="col-md-3 text-center text-primary">
													<b>Departamento:</b>
												</div>
												<div class="col-md-3 text-center text-primary">
													<b>Provincia:</b>
												</div>
												<div class="col-md-3 text-center text-primary">
													<b>Municipio:</b>
												</div>
												<div class="col-md-3 text-center text-primary">
													<b>Comunidad / Distritos / Cantones:</b>
												</div>
											</div>';	
											foreach($localizacion as $item){
											$tabla.= '<div class="row">
												<div class="col-md-3 text-center">
														<div class="border1">'.$item['dep_departamento'].'</div>
												</div>
												<div class="col-md-3 text-center">
														<div class="border1">'.$item['prov_provincia'].'</div>
												</div>
												<div class="col-md-3 text-center">
														<div class="border1">'.$item['muni_municipio'].'</div>
												</div>
												<div class="col-md-3 text-center">
														<div class="border1">'.$item['cantones'].'</div>
												</div>
											</div>';	
											}
											
											$tabla.='<h4 class="titulo"><b>DESCRIPCIÓN DEL PROYECTO - PROGRAMA</b></h4>
											<div class="row">
												<div class="col-md-12 border1">
												'.$dictamen['proy_descripcion_proyecto'].'
												</div>
											</div>
											<br>
											<div class="row">
												<div class="col-md-2 text-primary text-right"><b>FASE A EJECUTAR</b></div>
												<div class="col-md-4 text-primary text-center"><b>COSTO DE LA FASE A EJECUTAR</b></div>
												<div class="col-md-6 text-primary text-center" style="text-transform:uppercase;"><b>DURACIÓN DE LA FASE DE ';
												if($dictamen['fas_fase'] == 'Ninguno'){
													$tabla.= 'Inversión'; 
												}else{ 
													$tabla.= ''.$dictamen['fas_fase'];
												}
											$tabla.='</b></div>
											</div>
											<br>
											<div class="row">
												<div class="col-md-2 text-primary text-right"><b>';
											if($dictamen['fas_fase'] == 'Ninguno'){ 
												$tabla.= 'Inversión'; 
											}else{ 
												$tabla.= $dictamen['fas_fase'];
											}
											$tabla.=':</b></div>
												<div class="col-md-4 border1"><?= $presupuesto_total;?></div>
												<div class="col-md-2 text-primary text-right"><b>Fecha de inicio:</b></div>
												<div class="col-md-4 border1">'.$dictamen['fecha_inicial'].'</div>
											</div>
											<br>
											<div class="row">
												<div class="col-md-2 col-md-offset-6 text-primary text-right"><b>Fecha de término:</b></div>
												<div class="col-md-4 border1">'.$dictamen['fecha_final'].'</div>
											</div>
											<br>
											<div class="row">
												<div class="col-md-6">
													<div class="text-primary text-center"><b>Metas del Proyecto - Programa:</b></div>
													<table class="table table-borderded table-condensed tabla-metas">
														<tr>
															<th class="text-center">Unidad</th>
															<th class="text-center">Cantidad</th>
														</tr>';
														foreach($metas as $item){
														$tabla.='<tr>
															<td>'.$item['meta_descripcion'].'</td>
															<td>'.$item['meta_meta'].'</td>
														</tr>';
														}
											$tabla.='</table>
												</div>
												<div class="col-md-6">
													<div class="text-primary text-center"><b>Indicadores de evaluación:</b></div>
													<table class="table table-borderded table-condensed tabla-indicadores">
														<tr>
															<th class="text-center">Unidad</th>
															<th class="text-center">Valor</th>
														</tr>
														<tr>
															<td>VANP</td>
															<td>-</td>
														</tr>
														<tr>
															<td>VANS</td>
															<td>- </td>
														</tr>
														<tr>
															<td>CAE</td>
															<td>- </td>
														</tr>
													</table>
												</div>
											</div>
											<br>
											<div class="row">
												<div class="col-md-10 col-md-offset-1">
													<div class="text-primary text-center"><b>Determinación de los componentes de la operación y su modalidad de ejecución:</b></div>
													<table class="table table-borderded table-condensed tabla-modalidad">
														<tr>
															<th width="60%" class="text-center">Componentes</th>
															<th class="text-center">Modalidad de Ejecución <br><span style="font-size:0.8em;">(Administración Propia / Contratación de terceros)</span></th>
														</tr>';
														foreach($componentes as $item){
														$tabla.='<tr>
															<td>'.$item['com_componente'].'</td>
															<td>';
														if($item['com_modalidad'] == ''){ 
															$tabla.= 'No especificado'; 
														}else{ 
															$tabla.=''.$item['com_modalidad'];
														}
														$tabla.='</td>
														</tr>';
														}
														
													$tabla.='</table>
												</div>
											</div>
		
		';
		return $tabla;
	}
*/	
	public function fisico($proy_id){
/*		if($proy_id == NULL){
			redirect('error_404');
		}*///
	//	$proy_id = 617; // ixiamas

//		$proy_id = 860; // 
	//	$id_f = 751;

		$mod = 4;
		$id_p = $proy_id;
        if($this->session->userdata('logged')){

		  $data['gestion'] = $this->session->userdata('gestion');
		  $data['mes_id'] = $this->session->userdata('mes');
		  
		  $resp_dictamen = $this->model_objetivo->get_dictamen2($proy_id, $this->session->userdata('gestion'));
		  $data['dictamen'] = $resp_dictamen->row_array();
          $data['unidad_responsable']=$this->model_proyecto->responsable_proy($proy_id,2); //// unidad responsable
          $data['unidad_ejecutora']=$this->model_proyecto->responsable_proy($proy_id,1);  //// unidad ejecutora
		  
          $fase=$this->model_faseetapa->fase_etapa($data['dictamen']['pfec_id'],$proy_id);
		  $data['presupuesto_total'] = number_format($fase[0]['pfec_ptto_fase'], 2, ',', '.');

		  $id_f = $fase[0]['id'];// ID DE LA FASE ACTIVA DEL PROYECTO / PROGRAMA
		  
		  $enlaces=$this->menu_modelo->get_Modulos(7);
		  $data['enlaces'] = $enlaces;
		  
		  $data['pe_fisica']=$this->programacion_fisica_pluri($mod,$id_f,$id_p); //programacion ejecucion fisica
		  $data['pe_financiera']=$this->prog_financiera($mod,$id_f,$id_p);//programacion ejecucion financiera

		  for($i=0;$i<count($enlaces);$i++)
		  {
			$subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
		  }
		  $data['subenlaces'] = $subenlaces;
		  $this->load->view('reportes/sgp/sgp_fisico', $data);
		}
		else{
		  redirect('admin/dashboard');
		} 
	}

	
	public function formatea_fecha($fecha){
		$dia = substr($fecha,8,2);
		$nmes = intval(substr($fecha,5,2));
		$anio = substr($fecha,0,4);
		$mes = '';
		switch ($nmes) {
			case 1:	$mes = 'Ene'; break;
			case 2:	$mes = 'Feb'; break;
			case 3:	$mes = 'Mar'; break;
			case 4:	$mes = 'Abr'; break;
			case 5:	$mes = 'May'; break;
			case 6:	$mes = 'Jun'; break;
			case 7:	$mes = 'Jul'; break;
			case 8:	$mes = 'Ago'; break;
			case 9:	$mes = 'Sep'; break;
			case 10:$mes = 'Oct'; break;
			case 11:$mes = 'Nov'; break;
			case 12:$mes = 'Dic'; break;
		}
		return $dia.' '.$mes.' '.$anio;
	}
	
    public function programacion_fisica_pluri($mod,$id_f,$proy_id){
     	$gestion = $this->session->userdata('gestion');
	
		$proyecto = $this->model_proyecto->get_id_proyecto($proy_id);
        $fase = $this->model_faseetapa->get_id_fase($proy_id);
        $años=$fase[0]['pfec_fecha_fin']-$fase[0]['pfec_fecha_inicio']+1;
        $meses=$años*12;
        for($p=1;$p<=$meses;$p++){$app[$p]=0; $aee[$p]=0;} //// vector vacio para sumar a nivel de actividades programacion , ejecutado

        $tabla ='';
        $tabla .='
       <style>
        .table{
		  border-color:#777 !important;
        }
table.tabla_avance.table-condensed.table > tbody > tr > td, .table-condensed.table > tbody > tr > th, .table-condensed.table > tfoot > tr > td, .table-condensed.table > tfoot > tr > th, .table-condensed.table > thead > tr > td, .table-condensed.table > thead > tr > th {
    padding: 2px 4px !important;
}		
        th{
		  font-weight:bold;
		  font-size:12px;
		  border-color:#777 !important;
        }
		td{
		  font-size:11px;
		  border-color:#777 !important;
		}
		tr.head_componente th{
			background-color:#ccc;
			color:#333;
		  border-color:#777 !important;
		}
		tr.head_producto td{
			background-color:#eee;
			color:#333;
			font-weight:bold;
		  border-color:#777 !important;
		}
		tr.row_actividad > td{
			background-color:#fff;
			color:#333;
		  border-color:#777 !important;
		}
		tr.head_tabla > th{
			background-color:#aaa;
			color:#fff;
		  border-color:#777 !important;
		}
      </style>';
      $tabla .='';
        for($p=1;$p<=$meses;$p++){$cpp[$p]=0; $cpe[$p]=0;} //// vector vacio para sumar a nivel de productos programacion , ejecutado
        for($p=1;$p<=$meses;$p++){$ppp[$p]=0; $ppe[$p]=0;} //// vector vacio para sumar a nivel de productos programacion , ejecutado
        for($p=1;$p<=$meses;$p++){$app[$p]=0; $aee[$p]=0;} //// vector vacio para sumar a nivel de actividades programacion , ejecutado
        $nro_c = $this->model_componente->componentes_nro($id_f); //// nro de componente
        if ($nro_c>0){
            for($p=1;$p<=$meses;$p++){$cpp[$p]=0; $cpe[$p]=0;} //// vector vacio para sumar a nivel de productos programacion , ejecutado
                $componentes = $this->model_componente->componentes_id($id_f);
                $nro_c=1;
				
                foreach ($componentes as $rowc){
					$pres_comp = 0;
                    $tabla .='
                        <table class="table table-condensed change_order_items" border="1" style="width:100%;">
                        <thead>
                            <tr class="modo1 head_componente" align=left>
                              <th class="lead" colspan=12>COMPONENTE: <b>'.$rowc['com_componente'].'</b></th>
                            </tr>
                            <tr class="modo1 head_tabla">
                              <th><center>Nº</center></th>
                              <th><center>PRODUCTO</center></th>
                              <th><center>ACTIVIDAD</center></th>
                              <th><center>INDICADOR</center></th>
                              <th><center>LINEA BASE</center></th>
                              <th><center>META</center></th>
                              <th><center>PONDERACI&Oacute;N</center></th>
                              <th><center>Fecha Inicio</center></th>
                              <th><center>Fecha Final</center></th>
                              <th><center>DURACIÓN (d.c.)</center></th>
                              <th><center>COSTO (Bs)</center></th>
                              <th><center>PROGRAMACION / EJECUCIÓN</center></th>
                            </tr>
                        </thead>
                      <tbody>';
                        $nrop=0;
                        $productos = $this->model_producto->list_prod($rowc['com_id']);
                        for($p=1;$p<=$meses;$p++){$ppp[$p]=0; $ppe[$p]=0;} //// vector vacio para sumar a nivel de productos programacion , ejecutado
                        /*--------------------------- Producto --------------------------*/
                        foreach ($productos as $rowp){
                            $nrop++;
							$tabla.='
                            <tr class="modo1 head_producto" align=left>
							  <td>'.$nrop.'</td>
                              <td style="text-transform:uppercase;">'.$rowp['prod_producto'].'</td>
							  <td>-</td>
							  <td><center>'.$rowp['prod_indicador'].'</center></td>
							  <td><center>'.$rowp['prod_linea_base'].'</center></td>
							  <td><center>'.$rowp['prod_meta'].'</center></td>
							  <td><center>'.$rowp['prod_ponderacion'].'</center></td>
							  <td>-</td>
							  <td>-</td>
							  <td>-</td>
							  <td>-</td>
							  <td>-</td>
                            </tr>';
                            /*--------------------------- Actividad --------------------------*/
                            for($p=1;$p<=$meses;$p++){$app[$p]=0; $aee[$p]=0;} //// vector vacio para sumar a nivel de actividades programacion , ejecutado
                                $nroa=0;
                                $actividad = $this->model_actividad->list_act_anual($rowp['prod_id']);
                                foreach ($actividad as $rowa){
                                    $nroa++;
                                    $ti='';if($rowa['indi_id']==2){ $ti='%';}
                                    $meta_gestion=$this->model_actividad->meta_act_gest($rowa['act_id']);
                                    $meta_gest=$meta_gestion[0]['meta_gest']+$rowa['act_linea_base'];
									
									$pres_comp = $pres_comp + $rowa['act_presupuesto'];

                                    $tabla .='<tr class="modo1 row_actividad">
                                        <td>'.$nrop.'.'.$nroa.'</td>
                                        <td>-</td>
                                        <td>'.$rowa['act_actividad'].'</td>
                                        <td><center>'.$rowa['act_indicador'].'</center></td>
                                        <td><center>'.$rowa['act_linea_base'].'</center></td>
                                        <td><center>'.$rowa['act_meta'].'</center></td>
                                        <td><center>'.$rowa['act_ponderacion'].'%</center></td>
                                        <td><center>'.$this->formatea_fecha($rowa['act_fecha_inicio']).'</center></td>
                                        <td><center>'.$this->formatea_fecha($rowa['act_fecha_final']).'</center></td>
                                        <td><center>'.$rowa['act_duracion'].'</center></td>
                                        <td class="text-right">'.$rowa['act_presupuesto'].'</td>
                                        <td style="padding:3px;">';
                                    $pa=0; $pe=0; $gestion=$fase[0]['pfec_fecha_inicio']; $a=1;$e=1;
                                      for($k=1;$k<=$años;$k++){
                                        $programado=$this->model_actividad->act_prog_mensual($rowa['act_id'],$gestion); /// programado
                                        $ejecutado=$this->model_actividad->act_ejec_mensual($rowa['act_id'],$gestion); /// ejecutado
                                        $nro=0;
                                        foreach($programado as $row){
                                          $nro++;
                                          $matriz [1][$nro]=$row['m_id'];
                                          $matriz [2][$nro]=$row['pg_fis'];
                                        }
                                        /*---------------- llenando la matriz vacia --------------*/
                                        for($j = 1; $j<=12; $j++){
                                          $matriz_r[1][$j]=$j;
                                          $matriz_r[2][$j]='0';  //// P
                                          $matriz_r[3][$j]='0';  //// PA
                                          $matriz_r[4][$j]='0';  //// %PA
                                          $matriz_r[5][$j]='0';  //// A
                                          $matriz_r[6][$j]='0';  //// B
                                          $matriz_r[7][$j]='0';  //// E
                                          $matriz_r[8][$j]='0';  //// EA
                                          $matriz_r[9][$j]='0';  //// %EA
                                          $matriz_r[10][$j]='0'; //// EFICACIA
                                        }
                                        /*--------------------------------------------------------*/
                                       /*--------------------ejecutado gestion ------------------*/
                                        $nro_e=0;
                                        foreach($ejecutado as $row){
                                          $nro_e++;
                                          $matriz_e [1][$nro_e]=$row['m_id'];
                                          $matriz_e [2][$nro_e]=$row['ejec_fis'];
                                          $matriz_e [3][$nro_e]=$row['ejec_fis_a'];
                                          $matriz_e [4][$nro_e]=$row['ejec_fis_b'];
                                        }
                                        /*--------------------------------------------------------*/
                                        /*------- asignando en la matriz P, PA, %PA ----------*/
                                        for($i = 1 ;$i<=$nro ;$i++){
                                          for($j = 1 ;$j<=12 ;$j++){
                                            if($matriz[1][$i]==$matriz_r[1][$j]){
                                                $matriz_r[2][$j]=round($matriz[2][$i],2);
                                            }
                                          }
                                        }

                                        for($j = 1 ;$j<=12 ;$j++){
                                          $pa=$pa+$matriz_r[2][$j];
                                          $matriz_r[3][$j]=$pa+$rowa['act_linea_base'];
                                          if($rowa['act_meta']!=0){
                                            $matriz_r[4][$j]=round(((($pa+$rowa['act_linea_base'])/$rowa['act_meta'])*100),2);
                                          }
                                          
                                        }
                                        /*-------------------------------------------------*/
                                        /*--------------- EJECUCION ----------------------------------*/
                                        if($rowa['indi_id']==1){
                                          for($i = 1 ;$i<=$nro_e ;$i++){
                                            for($j = 1 ;$j<=12 ;$j++){
                                              if($matriz_e[1][$i]==$matriz_r[1][$j]){
                                                  $matriz_r[7][$j]=round($matriz_e[2][$i],2);
                                              }
                                            }
                                          }
                                        }
                                        elseif ($rowa['indi_id']==2){
                                          for($i = 1 ;$i<=$nro_e ;$i++){
                                              for($j = 1 ;$j<=12 ;$j++){
                                                if($matriz_e[1][$i]==$matriz_r[1][$j]){
                                                  $matriz_r[5][$j]=round($matriz_e[3][$i],2);
                                                  $matriz_r[6][$j]=round($matriz_e[4][$i],2);
                                                  $matriz_r[7][$j]=round($matriz_e[2][$i],2);
                                                }
                                              }
                                            }
                                          /*--------------------------------------------------------*/
                                          }
                                          /*--------------------matriz E,AE,%AE gestion ------------------*/
                                            for($j = 1 ;$j<=12 ;$j++){
                                              $pe=$pe+$matriz_r[7][$j];
                                              $matriz_r[8][$j]=$pe+$rowa['act_linea_base'];
                                              if($rowa['act_meta']!=0){
                                                $matriz_r[9][$j]=round(((($pe+$rowa['act_linea_base'])/$rowa['act_meta'])*100),2);
                                              }
                                              
                                              if($matriz_r[4][$j]==0)
                                                {$matriz_r[10][$j]=round((($matriz_r[9][$j])*100),2);}
                                                else{
                                                $matriz_r[10][$j]=round((($matriz_r[9][$j]/$matriz_r[4][$j])*100),2);
                                                }
                                            }
                                          
                                           // $ap[$n]=$matriz_r[2][1]*$rowa['act_ponderacion']; $n++;
                                            $ap[$a]=(($matriz_r[4][1]*$rowa['act_ponderacion'])/100); $a++;
                                            $ae[$e]=(($matriz_r[9][1]*$rowa['act_ponderacion'])/100); $e++;

                                            $ap[$a]=(($matriz_r[4][2]*$rowa['act_ponderacion'])/100); $a++;
                                            $ae[$e]=(($matriz_r[9][2]*$rowa['act_ponderacion'])/100); $e++;

                                            $ap[$a]=(($matriz_r[4][3]*$rowa['act_ponderacion'])/100); $a++;
                                            $ae[$e]=(($matriz_r[9][3]*$rowa['act_ponderacion'])/100); $e++;

                                            $ap[$a]=(($matriz_r[4][4]*$rowa['act_ponderacion'])/100); $a++;
                                            $ae[$e]=(($matriz_r[9][4]*$rowa['act_ponderacion'])/100); $e++;

                                            $ap[$a]=(($matriz_r[4][5]*$rowa['act_ponderacion'])/100); $a++;
                                            $ae[$e]=(($matriz_r[9][5]*$rowa['act_ponderacion'])/100); $e++;

                                            $ap[$a]=(($matriz_r[4][6]*$rowa['act_ponderacion'])/100); $a++;
                                            $ae[$e]=(($matriz_r[9][6]*$rowa['act_ponderacion'])/100); $e++;

                                            $ap[$a]=(($matriz_r[4][7]*$rowa['act_ponderacion'])/100); $a++;
                                            $ae[$e]=(($matriz_r[9][7]*$rowa['act_ponderacion'])/100); $e++;

                                            $ap[$a]=(($matriz_r[4][8]*$rowa['act_ponderacion'])/100); $a++;
                                            $ae[$e]=(($matriz_r[9][8]*$rowa['act_ponderacion'])/100); $e++;

                                            $ap[$a]=(($matriz_r[4][9]*$rowa['act_ponderacion'])/100); $a++;
                                            $ae[$e]=(($matriz_r[9][9]*$rowa['act_ponderacion'])/100); $e++;

                                            $ap[$a]=(($matriz_r[4][10]*$rowa['act_ponderacion'])/100); $a++;
                                            $ae[$e]=(($matriz_r[9][10]*$rowa['act_ponderacion'])/100); $e++;

                                            $ap[$a]=(($matriz_r[4][11]*$rowa['act_ponderacion'])/100); $a++;
                                            $ae[$e]=(($matriz_r[9][11]*$rowa['act_ponderacion'])/100); $e++;

                                            $ap[$a]=(($matriz_r[4][12]*$rowa['act_ponderacion'])/100); $a++;
                                            $ae[$e]=(($matriz_r[9][12]*$rowa['act_ponderacion'])/100); $e++;
                                            
                                            $tabla .= ' 
                                             <table class="table table-bordered table-condensed change_order_items tabla_avance" border="1" style="margin:3px 3px 3px 3px;">
                                                <tr class="modo1" bgcolor=#504c49>
                                                    <th style="background-color:#ccc;" style colspan="13">Gesti&oacute;n '.$gestion.'</th>
                                                </tr>
                                                <tr class="modo1" bgcolor=#504c49>
                                                    <th>-</th>
                                                    <th>Ene</th>
                                                    <th>Feb</th>
                                                    <th>Mar</th>
                                                    <th>Abr</th>
                                                    <th>May</th>
                                                    <th>Jun</th>
                                                    <th>Jul</th>
                                                    <th>Ago</th>
                                                    <th>Sep</th>
                                                    <th>Oct</th>
                                                    <th>Nov</th>
                                                    <th>Dic</th>
                                                </tr>
                                                <tr class="modo1">
                                                    <td style="width:1%;">P</td>
                                                    <td >'.$matriz_r[2][1].' '.$ti.'</td>
                                                    <td >'.$matriz_r[2][2].' '.$ti.'</td>
                                                    <td >'.$matriz_r[2][3].' '.$ti.'</td>
                                                    <td >'.$matriz_r[2][4].' '.$ti.'</td>
                                                    <td >'.$matriz_r[2][5].' '.$ti.'</td>
                                                    <td >'.$matriz_r[2][6].' '.$ti.'</td>
                                                    <td >'.$matriz_r[2][7].' '.$ti.'</td>
                                                    <td >'.$matriz_r[2][8].' '.$ti.'</td>
                                                    <td >'.$matriz_r[2][9].' '.$ti.'</td>
                                                    <td >'.$matriz_r[2][10].' '.$ti.'</td>
                                                    <td >'.$matriz_r[2][11].' '.$ti.'</td>
                                                    <td >'.$matriz_r[2][12].' '.$ti.'</td>
                                                </tr>
<!--                                                <tr bgcolor="#F5F5DC" class="modo1">
                                                    <td style="width:1%;">%P.A.</td>
                                                    <td >'.$matriz_r[4][1].'%</td>
                                                    <td >'.$matriz_r[4][2].'%</td>
                                                    <td >'.$matriz_r[4][3].'%</td>
                                                    <td >'.$matriz_r[4][4].'%</td>
                                                    <td >'.$matriz_r[4][5].'%</td>
                                                    <td >'.$matriz_r[4][6].'%</td>
                                                    <td >'.$matriz_r[4][7].'%</td>
                                                    <td >'.$matriz_r[4][8].'%</td>
                                                    <td >'.$matriz_r[4][9].'%</td>
                                                    <td >'.$matriz_r[4][10].'%</td>
                                                    <td >'.$matriz_r[4][11].'%</td>
                                                    <td >'.$matriz_r[4][12].'%</td>
                                                </tr>-->

                                                <tr bgcolor="#F5F5DC" class="modo1">
                                                    <td style="width:1%;">E</td>
                                                    <td >'.$matriz_r[7][1].' '.$ti.'</td>
                                                    <td >'.$matriz_r[7][2].' '.$ti.'</td>
                                                    <td >'.$matriz_r[7][3].' '.$ti.'</td>
                                                    <td >'.$matriz_r[7][4].' '.$ti.'</td>
                                                    <td >'.$matriz_r[7][5].' '.$ti.'</td>
                                                    <td >'.$matriz_r[7][6].' '.$ti.'</td>
                                                    <td >'.$matriz_r[7][7].' '.$ti.'</td>
                                                    <td >'.$matriz_r[7][8].' '.$ti.'</td>
                                                    <td >'.$matriz_r[7][9].' '.$ti.'</td>
                                                    <td >'.$matriz_r[7][10].' '.$ti.'</td>
                                                    <td >'.$matriz_r[7][11].' '.$ti.'</td>
                                                    <td >'.$matriz_r[7][12].' '.$ti.'</td>
                                                </tr>
<!--                                                <tr bgcolor="#F5F5DC" class="modo1">
                                                    <td style="width:1%;">%E.A.</td>
                                                    <td >'.$matriz_r[9][1].'%</td>
                                                    <td >'.$matriz_r[9][2].'%</td>
                                                    <td >'.$matriz_r[9][3].'%</td>
                                                    <td >'.$matriz_r[9][4].'%</td>
                                                    <td >'.$matriz_r[9][5].'%</td>
                                                    <td >'.$matriz_r[9][6].'%</td>
                                                    <td >'.$matriz_r[9][7].'%</td>
                                                    <td >'.$matriz_r[9][8].'%</td>
                                                    <td >'.$matriz_r[9][9].'%</td>
                                                    <td >'.$matriz_r[9][10].'%</td>
                                                    <td >'.$matriz_r[9][11].'%</td>
                                                    <td >'.$matriz_r[9][12].'%</td>
                                                </tr>-->
                                            </table>';
                                            $gestion++;
                                        }

                                        for($k=1;$k<=$meses;$k++){
                                           $app[$k]=round(($app[$k]+$ap[$k]),1); //// sumando a nivel de las actividades
                                           $aee[$k]=round(($aee[$k]+$ae[$k]),1); //// sumando a nivel de las actividades
                                        }
                                        $tabla .= '';
                                        $tabla .='</td>';
                                    $tabla .'</tr>';
                                }
								
								//PRESUPUESTO ANUAL
								$tabla.= '
								<tr>
									<td colspan="10" class="text-right">PRESUPUESTO TOTAL DEL COMPONENTE</td>
									<td style="font-size:14px;"><b>'.$pres_comp.'</b></td>
									<td></td>
								</tr>
								';
								
                                for($kp=1;$kp<=$meses;$kp++){
                                    $ppp[$kp]=round(($ppp[$kp]+($app[$kp]*$rowp['prod_ponderacion'])/100),1); //// sumando a nivel de los productos
                                    $ppe[$kp]=round(($ppe[$kp]+($aee[$kp]*$rowp['prod_ponderacion'])/100),1); //// sumando a nivel de los productos
                                }
                            /*--------------------------- End Actividad --------------------------*/
                        }
                        for($k=1;$k<=$meses;$k++){
                            $cpp[$k]=round(($cpp[$k]+($ppp[$k]*$rowc['com_ponderacion'])/100),0); //// sumando a nivel de los Componentes
                            $cpe[$k]=round(($cpe[$k]+($ppe[$k]*$rowc['com_ponderacion'])/100),0); //// sumando a nivel de los Componentes
                        }
                        /*--------------------------- End Producto --------------------------*/

                    $tabla .='</tbody>
                    </table>';
                }
                $i=1;$meses=12;$gestion=$fase[0]['pfec_fecha_inicio'];
                $tabla .='<hr><b>EFICACIA EN EJECUCIÓN FÍSICA (ANUAL)</b>';
                for($kp=1;$kp<=$años;$kp++){
                        $tabla .='
                        <table class="table table-condensed change_order_items tabla_eficacia" border=1>
                          <thead>
                            <tr class="modo1 head_tabla" align=center bgcolor="#504c49">
                                <th colspan="13"><center>GESTI&Oacute;N '.$gestion.'</center></th>
                            </tr>
                            <tr class="modo1 head_tabla" align=center bgcolor="#504c49">
                                <th width="20%">-</th>
                                <th width="6%"><center>Ene</center></th>
                                <th width="6%"><center>Feb</center></th>
                                <th width="6%"><center>Mar</center></th>
                                <th width="6%"><center>Abr</center></th>
                                <th width="6%"><center>May</center></th>
                                <th width="6%"><center>Jun</center></th>
                                <th width="6%"><center>Jul</center></th>
                                <th width="6%"><center>Ago</center></th>
                                <th width="6%"><center>Sep</center></th>
                                <th width="6%"><center>Oct</center></th>
                                <th width="6%"><center>Nov</center></th>
                                <th width="6%"><center>Dic</center></th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr class="modo1">
                                <td><b>PROGRAMACI&Oacute;N ACUMULADA EN %</b></td>';
                                for($pos=$i;$pos<=$meses;$pos++){
                                    $tabla .= '<td class="pac text-center">'.$cpp[$pos].'%</td>';
                                }
                                $tabla .= '
                            </tr>
                            <tr class="modo1">
                                <td><b>EJECUCI&Oacute;N ACUMULADA EN %</b></td>';
                                for($pos=$i;$pos<=$meses;$pos++){
                                    $tabla .= '<td class="eac text-center" bgcolor="#98FB98">'.$cpe[$pos].'%</td>';
                                }
                                $tabla .= '
                            </tr>
                            <tr class="modo1">
                                <td><b>EFICACIA POR MES</b></td>';
                                for($pos=$i;$pos<=$meses;$pos++){
                                    if($cpp[$pos]==0){$cpp[$pos]=1;}
                                    $tabla .= '<td style="font-weight:bold;" class="efic text-center" bgcolor="#B0E0E6">'.round((($cpe[$pos]/$cpp[$pos])*100),1).'%</td>';
                                }
                                $tabla .= '
                            </tr>
                          </tbody>
                          </table>';
                    $i=$meses+1;
                    $meses=$meses+12;
                    $gestion++; 
                }

        }

        return $tabla;
    }

    public function suma_total_partidas($proy_id,$gestion){
        $fase = $this->model_faseetapa->get_id_fase($proy_id);
        $pprog = $this->mfinanciera->vlista_partidas_total_proy_programado($proy_id,$gestion,$fase[0]['pfec_ejecucion']);
        $eprog = $this->mfinanciera->vlista_partidas_total_sigep_proy_programado($proy_id,$gestion);
        
        $m[0]='total';
        $m[1]='enero';
        $m[2]='febrero';
        $m[3]='marzo';
        $m[4]='abril';
        $m[5]='mayo';
        $m[6]='junio';
        $m[7]='julio';
        $m[8]='agosto';
        $m[9]='septiembre';
        $m[10]='octubre';
        $m[11]='noviembre';
        $m[12]='diciembre';

        for($i=1;$i<=12;$i++){
          $totalp[1][$i]=0; //// i
          $totalp[2][$i]=0; //// p
          $totalp[3][$i]=0; //// pa
          $totalp[4][$i]=0; //// %pa
          $totalp[5][$i]=0; //// e
          $totalp[6][$i]=0; //// ea
          $totalp[7][$i]=0; //// %ea
          $totalp[8][$i]=0; //// eficacia
          $totalp[9][$i]=0; //// Menor
          $totalp[10][$i]=0; //// Entre
          $totalp[11][$i]=0; //// Mayor
        }

        if(count($pprog)!=0){
            $suma_pfin=0; $suma_efin=0;
            for($i=1;$i<=12;$i++){
                $totalp[1][$i]=$i; //// i
                $totalp[2][$i]=$pprog[0][$m[$i]]; /// programado
                $suma_pfin=$suma_pfin+$pprog[0][$m[$i]];
                $totalp[3][$i]=$suma_pfin; ///// Programado acumulado

                if($pprog[0][$m[0]]!=0){
                    $totalp[4][$i]=round((($totalp[3][$i]/$pprog[0][$m[0]])*100),2); ////// Programado Acumulado %
                }

                if(count($eprog)!=0){
                  $totalp[5][$i]=$eprog[0][$m[$i]]; /// Ejecutado
                  $suma_efin=$suma_efin+$eprog[0][$m[$i]];
                  $totalp[6][$i]=$suma_efin; ///// Ejecutado acumulado
                  
                  if($pprog[0][$m[0]]!=0){
                    $totalp[7][$i]=round((($totalp[6][$i]/$pprog[0][$m[0]])*100),2); ////// Ejecutado Acumulado %
                  }

                  if($totalp[4][$i]!=0){
                    $totalp[8][$i]=round((($totalp[7][$i]/$totalp[4][$i])*100),2); ////// Eficacia
                  }

                  /*------------------------- eficacia -------------------------*/
                  if($totalp[8][$i]<=75){$totalp[9][$i] = $totalp[8][$i];}
                  if ($totalp[8][$i] >= 76 && $totalp[8][$i] <= 90.9) {$totalp[10][$i] = $totalp[8][$i];}
                  if($totalp[8][$i] >= 91){$totalp[11][$i] = $totalp[8][$i];}
              }
            }
        }

    return $totalp;
    }  
	
    public function prog_financiera($mod,$id_f,$proy_id){
     	$gestion = $this->session->userdata('gestion');

		$proyecto = $this->model_proyecto->get_id_proyecto($proy_id);
        $fase = $this->model_faseetapa->get_id_fase($proy_id);
        $tabla ='';
        $tabla .='
           <style>
          </style>';

            //    $this->mis_partidas($mod,$proy_id,$id_f,$this->gestion,$fase[0]['pfec_ejecucion']);
            //    $tabla .=''.$proy_id.'--'.$this->gestion.'-'.$fase[0]['pfec_ejecucion'];
                $proyecto_programado= $this->mfinanciera->lista_partidas_proy_programado($proy_id,$gestion,$fase[0]['pfec_ejecucion']);
                $nro=0;
                foreach ($proyecto_programado as $pp){
                    $nro++; 
                    $ff= $this->mfinanciera->get_ff($pp['ff_id']);
                    $of= $this->mfinanciera->get_of($pp['of_id']);

                    $tabla .='<table class="table table-condensed change_order_items" style="width:100%;" align="center">';
                    $tabla .='<tr class="head_tabla" style="background-color:#ac9fae;font-weight: bold;">';
                      $tabla .='<th style="width:3%;"><center>Nº</center></th>';
                      $tabla .='<th style="width:5%;"><center>PARTIDA</center></th>';
                      $tabla .='<th style="width:5%;"><center>FUENTE</center></th>';
                      $tabla .='<th style="width:5%;"><center>ORGANISMO</center></th>';
                      $tabla .='<th style="width:82%;"><center>PROGRAMADO - EJECUTADO</center></th>';
                    $tabla .='</tr>';      
                    $tabla .='<tr>';
                        $tabla .='<td><center>'.$nro.'</center></td>';
                        $tabla .='<td><center>'.$pp['par_codigo'].'</center></td>';
                        $tabla .='<td><center>'.$ff[0]['ff_codigo'].'</center></td>';
                        $tabla .='<td><center>'.$of[0]['of_codigo'].'</center></td>';
                        $tabla .='<td>';
                        $tabla .='<table class="table table-condensed change_order_items tabla_partidas" style="width:100%;" align="center">';
                        $tabla .='<tr class="modo1 head_componente">';
                          $tabla .='<th bgcolor="#504c49" style="width:7.6%;"><center>P/E</center></th>';
                          $tabla .='<th bgcolor="#504c49" style="width:7.6%;"><center>ENE</center></th>';
                          $tabla .='<th bgcolor="#504c49" style="width:7.6%;"><center>FEB</center></th>';
                          $tabla .='<th bgcolor="#504c49" style="width:7.6%;"><center>MAR</center></th>';
                          $tabla .='<th bgcolor="#504c49" style="width:7.6%;"><center>ABR</center></th>';
                          $tabla .='<th bgcolor="#504c49" style="width:7.6%;"><center>MAY</center></th>';
                          $tabla .='<th bgcolor="#504c49" style="width:7.6%;"><center>JUN</center></th>';
                          $tabla .='<th bgcolor="#504c49" style="width:7.6%;"><center>JUL</center></th>';
                          $tabla .='<th bgcolor="#504c49" style="width:7.6%;"><center>AGO</center></th>';
                          $tabla .='<th bgcolor="#504c49" style="width:7.6%;"><center>SEP</center></th>';
                          $tabla .='<th bgcolor="#504c49" style="width:7.6%;"><center>OCT</center></th>';
                          $tabla .='<th bgcolor="#504c49" style="width:7.6%;"><center>NOV</center></th>';
                          $tabla .='<th bgcolor="#504c49" style="width:7.6%;"><center>DIC</center></th>';
                        $tabla .='</tr>';
                         $tabla .='<tr class="modo1">';
                          $tabla .='<td>PROGRAMADO</td>';
                          $tabla .='<td class="text-right">'.number_format($pp['enero'], 2, ',', '.').'</td>';
                          $tabla .='<td class="text-right">'.number_format($pp['febrero'], 2, ',', '.').'</td>';
                          $tabla .='<td class="text-right">'.number_format($pp['marzo'], 2, ',', '.').'</td>';
                          $tabla .='<td class="text-right">'.number_format($pp['abril'], 2, ',', '.').'</td>';
                          $tabla .='<td class="text-right">'.number_format($pp['mayo'], 2, ',', '.').'</td>';
                          $tabla .='<td class="text-right">'.number_format($pp['junio'], 2, ',', '.').'</td>';
                          $tabla .='<td class="text-right">'.number_format($pp['julio'], 2, ',', '.').'</td>';
                          $tabla .='<td class="text-right">'.number_format($pp['agosto'], 2, ',', '.').'</td>';
                          $tabla .='<td class="text-right">'.number_format($pp['septiembre'], 2, ',', '.').'</td>';
                          $tabla .='<td class="text-right">'.number_format($pp['octubre'], 2, ',', '.').'</td>';
                          $tabla .='<td class="text-right">'.number_format($pp['noviembre'], 2, ',', '.').'</td>';
                          $tabla .='<td class="text-right">'.number_format($pp['diciembre'], 2, ',', '.').'</td>';
                        $tabla .='</tr>';
                        $pe= $this->mfinanciera->get_partida_proy_ejecutado($pp['pr_id'],$gestion);
                        if(count($pe)!=0){
                          $tabla .='<tr class="modo1">';
                            $tabla .='<td>EJECUTADO</td>';
                            $tabla .='<td class="text-right">'.number_format($pe[0]['enero'], 2, ',', '.').'</td>';
                            $tabla .='<td class="text-right">'.number_format($pe[0]['febrero'], 2, ',', '.').'</td>';
                            $tabla .='<td class="text-right">'.number_format($pe[0]['marzo'], 2, ',', '.').'</td>';
                            $tabla .='<td class="text-right">'.number_format($pe[0]['abril'], 2, ',', '.').'</td>';
                            $tabla .='<td class="text-right">'.number_format($pe[0]['mayo'], 2, ',', '.').'</td>';
                            $tabla .='<td class="text-right">'.number_format($pe[0]['junio'], 2, ',', '.').'</td>';
                            $tabla .='<td class="text-right">'.number_format($pe[0]['julio'], 2, ',', '.').'</td>';
                            $tabla .='<td class="text-right">'.number_format($pe[0]['agosto'], 2, ',', '.').'</td>';
                            $tabla .='<td class="text-right">'.number_format($pe[0]['septiembre'], 2, ',', '.').'</td>';
                            $tabla .='<td class="text-right">'.number_format($pe[0]['octubre'], 2, ',', '.').'</td>';
                            $tabla .='<td class="text-right">'.number_format($pe[0]['noviembre'], 2, ',', '.').'</td>';
                            $tabla .='<td class="text-right">'.number_format($pe[0]['diciembre'], 2, ',', '.').'</td>';
                          $tabla .='</tr>';
                        }
                        $tabla .='</table>';
                        $tabla .='</td>';

                    $tabla .='</tr>';
                    $tabla .='</table>';
                }
                $tabla .='<hr>';
                $total=$this->suma_total_partidas($proy_id,$gestion);
                $tabla .='<hr><b>EFICACIA EN EJECUCIÓN FINANCIERA (ANUAL)</b>';
                $tabla .='
                    <table class="table tabla_eficacia change_order_items" style="width:100%;" align="center">
                        <tr class="modo1 head_tabla">
                            <th colspan=13><center><b>GESTI&Oacute;N '.$gestion.'</b></th>
                        </tr>
                        <tr class="modo1 head_tabla">
                            <th bgcolor="#504c49" style="width:12%;"><center>P/E</center></th>
                            <th bgcolor="#504c49" style="width:5%;"><center>ENERO</center></th>
                            <th bgcolor="#504c49" style="width:5%;"><center>FEBRERO</center></th>
                            <th bgcolor="#504c49" style="width:5%;"><center>MARZO</center></th>
                            <th bgcolor="#504c49" style="width:5%;"><center>ABRIL</center></th>
                            <th bgcolor="#504c49" style="width:5%;"><center>MAYO</center></th>
                            <th bgcolor="#504c49" style="width:5%;"><center>JUNIO</center></th>
                            <th bgcolor="#504c49" style="width:5%;"><center>JULIO</center></th>
                            <th bgcolor="#504c49" style="width:5%;"><center>AGOSTO</center></th>
                            <th bgcolor="#504c49" style="width:5%;"><center>SEPTIEMBRE</center></th>
                            <th bgcolor="#504c49" style="width:5%;"><center>OCTUBRE</center></th>
                            <th bgcolor="#504c49" style="width:5%;"><center>NOVIEMBRE</center></th>
                            <th bgcolor="#504c49" style="width:5%;"><center>DICIEMBRE</center></th>
                        </tr>
                        <tr class="modo1">
                            <td class=""><b>PROGRAMADO</b></td>';
                            for ($i=1; $i <=12 ; $i++){ 
                               $tabla .='<td class="fpa text-right">'.number_format($total[2][$i], 2, ',', '.').'</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="modo1">
                            <td><b>PROGRAMADO ACUMULADO</b></td>';
                            for ($i=1; $i <=12 ; $i++){ 
                               $tabla .='<td class="fep text-right">'.number_format($total[3][$i], 2, ',', '.').'</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="modo1" bgcolor="#c5bcf3">
                            <td><b>% PROGRAMADO ACUMULADO</b></td>';
                            for ($i=1; $i <=12 ; $i++){ 
                               $tabla .='<td class="fea text-right">'.$total[4][$i].'%</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="modo1">
                            <td class=""><b>EJECUTADO</b></td>';
                            for ($i=1; $i <=12 ; $i++) 
                            { 
                               $tabla .='<td class="fpa text-right">'.number_format($total[5][$i], 2, ',', '.').'</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="modo1">
                            <td><b>EJECUTADO ACUMULADO</b></td>';
                            for ($i=1; $i <=12 ; $i++){ 
                               $tabla .='<td class="fep text-right">'.number_format($total[6][$i], 2, ',', '.').'</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="modo1" bgcolor="#c5bcf3">
                            <td><b>% EJECUTADO ACUMULADO</b></td>';
                            for ($i=1; $i <=12 ; $i++){ 
                               $tabla .='<td class="fea text-right">'.$total[7][$i].'%</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="modo1" bgcolor="#bbf3cf">
                            <td><b>EFICACIA POR MES</b></td>';
                            for ($i=1; $i <=12 ; $i++){ 
                               $tabla .='<td style="font-weight:bold;" class="efic text-right">'.$total[8][$i].'%</td>'; 
                            }
                        $tabla .='
                        </tr>';
                    $tabla .='
                    </table>';


        return $tabla;
    }
    

	
	
}