<?php
mb_internal_encoding('UTF-8');
mb_http_output('UTF-8');
header('Content-Type: text/html; charset=utf-8');

class Auditoria_proyecto extends CI_Controller 
{
    private $estilo_vertical = 'Prueba';
    public function __construct()
    {
        parent::__construct();
        $this->load->model('mantenimiento/model_auditoria');
        $this->load->model('Users_model','',true);
        $this->load->model('menu_modelo');
        $this->load->model('mantenimiento/mpdes');
        $this->load->model('mantenimiento/model_configuracion');
         //llamar a mi menu
        $this->load->library('menu');
        $this->menu->const_menu(9);
    }

    public function lista_auditoria()
    {
        $data['auditoria'] = $this->model_auditoria->get_auditoria();
        $ruta = 'mantenimiento/vlista_auditoria';
        $this->construir_vista($ruta,$data);
    }

    function construir_vista($ruta,$data)
    {
        $menu['enlaces'] = $this->menu->get_enlaces();
        $menu['subenlaces'] = $this->menu->get_sub_enlaces();
        $menu['titulo'] = 'AUDITORIA DE PROYECTOS';
        $this->load->view('includes/header');
        $this->load->view('includes/menu_lateral',$menu);
        $this->load->view($ruta,$data);
        $this->load->view('includes/footer');
    }
    public function ver_auditoria()
    {
        mb_internal_encoding('utf-8');
        $data = $this->model_auditoria->get_auditoria_id(3190);
        $dato1 = $data->old;
        $dato2 = $data->new;
        $json1 = json_encode($dato2);
        $json2 = json_decode($dato1);
        echo $dato1."<br>";
        echo $dato2."<br>";
    }

    public function ver_auditorias()
    {
        $id = $this->input->post('id');
        $tabla = trim($this->input->post('tabla'));
        $auditoria1 = $this->model_auditoria->get_auditoria_id($id);
        $auditoria2 = $this->model_auditoria->get_auditoria_id2($id);
        $dato_tabla = $this->model_auditoria->get_datos_tabla($tabla);
        // $columna = array();
        $i = 0;
        foreach ($dato_tabla as $fila) {
            $columna[$i] = $fila['column_name'];
            $i++;
        }
        $result = array(
            'id' => $id,
            'tabla' => $tabla,
            'old' => $auditoria1->old,
            'new' => $auditoria1->new,
            'columna' => $columna,
            'n' => $i
            );
        echo json_encode($result);
    }
}