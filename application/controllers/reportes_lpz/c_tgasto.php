<?php
define("DOMPDF_ENABLE_REMOTE", true);
define("DOMPDF_TEMP_DIR", "/tmp");
class c_tgasto extends CI_Controller { 
    var $gestion;
    var $mes;
    var $rol;
    var $fun_id;

  public function __construct ()
    { 
        parent::__construct();
        if($this->session->userdata('fun_id')!=null){
        $this->load->library('pdf');
        $this->load->library('pdf2');

        $this->load->model('reportes/model_objetivo');
        $this->load->model('programacion/model_proyecto');
        $this->load->model('mantenimiento/mapertura_programatica');
        $this->load->model('mantenimiento/munidad_organizacional');
        $this->load->model('programacion/model_reporte_tipogasto');
        $this->load->model('reportes_tarija/model_tgasto');
        $this->load->model('menu_modelo');
        $this->load->model('Users_model','',true);

        $this->gestion = $this->session->userData('gestion');
        $this->mes = $this->session->userData('mes');
        $this->rol = $this->session->userData('rol');
        $this->fun_id = $this->session->userData('fun_id');

        }else{
            redirect('/','refresh');
        }
    }
       
    public function menu_reporte()
    { 
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++)
        {
             $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];
    
        $this->load->view('admin/reportes/ejecucion_presupuestaria/menu_reporte_ejecucion', $data);
    }

    public function rubro_gasto()
    { 
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++)
        {
             $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];

        $data['tgasto']=$this->get_matriz_tgasto();
        $data['tabla']=$this->genera_tabla_tgasto($data['tgasto']);
    
        $this->load->view('admin/reportes/ejecucion_presupuestaria/rubro_gasto/rubro_gasto', $data);
    }

    public function reporte_rubro_gasto_grafico()
    { 
        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];

        $data['tgasto']=$this->get_matriz_tgasto();
        $data['tabla']=$this->genera_tabla_tgasto($data['tgasto']);

        $this->load->view('admin/reportes/ejecucion_presupuestaria/rubro_gasto/rubro_gasto_imprimir', $data);
    }

    public function get_matriz_tgasto()
    {
        for ($i=1; $i <=7 ; $i++) { 
          for ($j=0; $j <=8 ; $j++) { 
            $monto[$i][$j]=0;
          }
        }

        $i=1;
        $tipo_gasto = $this->model_tgasto->tp_gasto(1);
        $suma_gasto_total = $this->model_tgasto->suma_tp_total($this->gestion,$this->mes);
        $suma_gasto = $this->model_tgasto->suma_tp_gasto($this->gestion,$this->mes,1);
        if(count($suma_gasto)!=0){
          $monto[1][0]=1;
          $monto[1][1]=0;
          $monto[1][2]='INVERSION PUBLICA';
          $monto[1][3]=$suma_gasto[0]['pi'];
          $monto[1][4]=$suma_gasto[0]['pm'];
          $monto[1][5]=$suma_gasto[0]['pv'];
          $monto[1][6]=$suma_gasto[0]['pe'];
          $ejec=0;
          if($monto[1][5]!=0){
            $ejec=round((($monto[1][6]/$monto[1][5])*100),2);
          }
          $monto[1][7]=$ejec;
          $part=0;
          if($suma_gasto_total[0]['pe']!=0){
            $part=round((($monto[1][6]/$suma_gasto_total[0]['pe'])*100),2);
          }
          $monto[1][8]=$part;
        }
        else{
          $monto[1][0]=1;
          $monto[1][1]=0;
          $monto[1][2]='INVERSION PUBLICA';
        }
        

        $suma_gasto_total = $this->model_tgasto->suma_tp_total($this->gestion,$this->mes);
        $suma_gasto = $this->model_tgasto->suma_tp_gasto($this->gestion,$this->mes,2);
        if(count($suma_gasto)!=0){
          $monto[2][0]=2;
          $monto[2][1]=0;
          $monto[2][2]='PROGRAMAS';
          $monto[2][3]=$suma_gasto[0]['pi'];
          $monto[2][4]=$suma_gasto[0]['pm'];
          $monto[2][5]=$suma_gasto[0]['pv'];
          $monto[2][6]=$suma_gasto[0]['pe'];
          $ejec=0;
          if($monto[2][5]!=0){
            $ejec=round((($monto[2][6]/$monto[2][5])*100),2);
          }
          $monto[2][7]=$ejec;
          $part=0;
          if($suma_gasto_total[0]['pe']!=0){
            $part=round((($monto[2][6]/$suma_gasto_total[0]['pe'])*100),2);
          }
          $monto[2][8]=$part;
        }
        else{
          $monto[2][0]=2;
          $monto[2][1]=0;
          $monto[2][2]='PROGRAMAS';
        }
        

        $i=3;
        $otros_gasto = $this->model_tgasto->otros_gastos();
        foreach($otros_gasto as $rowg)
        {
          $lista_aper_padres = $this->mapertura_programatica->lista_aperturas_padres();
          $suma_pi=0;$suma_pm=0;$suma_pv=0;$suma_pe=0;
          foreach($lista_aper_padres as $rowap)
          {
            $lista_aper_hijas = $this->model_tgasto->programas_hijos($rowap['aper_programa'],$rowap['aper_gestion'],$this->mes,$rowg['tg_id']);
            foreach($lista_aper_hijas as $row2)
            {
              //echo "----- HIJO : ".$row2['proy_nombre']."- tg : ".$row2['tg_id']."- PI :".$row2['pe_pi']."- PM :".$row2['pe_pm']."- PV :".$row2['pe_pv']."- PE :".$row2['pe_pe']."<br>";  
              $suma_pi=$suma_pi+$row2['pe_pi'];
              $suma_pm=$suma_pm+$row2['pe_pm'];
              $suma_pv=$suma_pv+$row2['pe_pv'];
              $suma_pe=$suma_pe+$row2['pe_pe'];
            }
            $monto[$i][0]=0;
            $monto[$i][1]=$rowg['tg_id'];
            $monto[$i][2]=$rowg['tg_descripcion'];
            $monto[$i][3]=$suma_pi;
            $monto[$i][4]=$suma_pm;
            $monto[$i][5]=$suma_pv;
            $monto[$i][6]=$suma_pe;
            $ejec=0;
            if ($monto[$i][5]!=0) {
              $ejec=round((($monto[$i][6]/$monto[$i][5])*100),2);
            }
            $monto[$i][7]=$ejec;
            $part=0;
            if($suma_gasto_total[0]['pe']!=0){
              $part=round((($monto[$i][6]/$suma_gasto_total[0]['pe'])*100),2);
            }
            $monto[$i][8]=$part;
          }
          $i++;
        }

        /*---------------------- Ordenamiento ------------------------*/
        $aux_tp=0;
        $aux_id=0;
        $aux_gasto=0;
        $aux_pi=0;
        $aux_pm=0;
        $aux_pv=0;
        $aux_pe=0;
        $aux_e=0;
        $aux_part=0;

        for ($i=1; $i <=7 ; $i++) {    
            for ($j=$i+1; $j <=6 ; $j++) {
              if($monto[$j][8]>$monto[$i][8])
              {
                  $aux_tp=$monto[$i][0];
                  $aux_id=$monto[$i][1];
                  $aux_gasto=$monto[$i][2];
                  $aux_pi=$monto[$i][3];
                  $aux_pm=$monto[$i][4];
                  $aux_pv=$monto[$i][5];
                  $aux_pe=$monto[$i][6];
                  $aux_e=$monto[$i][7];
                  $aux_part=$monto[$i][8];
              // echo "aux tp ".$aux_tp."- PI".$aux_pi."- PM".$aux_pm;

                  $monto[$i][0]=$monto[$j][0];
                  $monto[$i][1]=$monto[$j][1];
                  $monto[$i][2]=$monto[$j][2];
                  $monto[$i][3]=$monto[$j][3];
                  $monto[$i][4]=$monto[$j][4];
                  $monto[$i][5]=$monto[$j][5];
                  $monto[$i][6]=$monto[$j][6];
                  $monto[$i][7]=$monto[$j][7];
                  $monto[$i][8]=$monto[$j][8];

                  $monto[$j][0]=$aux_tp;
                  $monto[$j][1]=$aux_id;
                  $monto[$j][2]=$aux_gasto;
                  $monto[$j][3]=$aux_pi;
                  $monto[$j][4]=$aux_pm;
                  $monto[$j][5]=$aux_pv;
                  $monto[$j][6]=$aux_pe;
                  $monto[$j][7]=$aux_e;
                  $monto[$j][8]=$aux_part;
              }
            }
        }

        return $monto;
    }
  /*=========================================================================================================================*/
    public function genera_tabla_tgasto($monto)
    {
        $suma_gasto_total = $this->model_tgasto->suma_tp_total($this->gestion,$this->mes);
        $tabla = '';
      //  $tabla .= '<table>';
        $cont=0;
        for ($i=1; $i <=7 ; $i++) { 
          $cont++;
          $mue[$cont][0]=$monto[$i][0];
          $mue[$cont][1]=$monto[$i][1];
          $mue[$cont][2]=$monto[$i][2];
          $mue[$cont][3]=$monto[$i][3];
          $mue[$cont][4]=$monto[$i][4];
          $mue[$cont][5]=$monto[$i][5];
          $mue[$cont][6]=$monto[$i][6];
          $mue[$cont][7]=$monto[$i][7];
          $mue[$cont][8]=$monto[$i][8];
          $tabla .= '<tr bgcolor="#e5e8ec" class="modo1">';
            $tabla .= '<td><b>'.$monto[$i][2].'</b></td>';
            $tabla .= '<td>'.number_format($monto[$i][3], 2, ',', '.').'</td>';
            $tabla .= '<td>'.number_format($monto[$i][4], 2, ',', '.').'</td>';
            $tabla .= '<td>'.number_format($monto[$i][5], 2, ',', '.').'</td>';
            $tabla .= '<td>'.number_format($monto[$i][6], 2, ',', '.').'</td>';
            $tabla .= '<td>'.$monto[$i][7].'%</td>';
            $tabla .= '<td>'.$monto[$i][8].'%</td>';
          $tabla .= '</tr>';

          if($monto[$i][0]==1 || $monto[$i][0]==2)
          {
            $gasto = $this->model_tgasto->tipo_gasto($monto[$i][0]);
            foreach($gasto as $row2)
            {
              $suma = $this->model_tgasto->suma_gasto_id($this->gestion,$this->mes,$row2['tg_id']);
              $cont++;
              $pi=0;$pm=0;$pv=0;$pe=0;$e=0;$part=0;
              if(count($suma)!=0){
                $pi=$suma[0]['pi'];$pm=$suma[0]['pm'];$pv=$suma[0]['pv'];$pe=$suma[0]['pe'];
                if($pv!=0){
                  $e=round((($pe/$pv)*100),2);
                }
                if($suma_gasto_total[0]['pe']!=0){
                  $part=round((($pe/$suma_gasto_total[0]['pe'])*100),2);
                }             
              }
              $mue[$cont][0]=0;
              $mue[$cont][1]=$row2['tg_id'];
              $mue[$cont][2]=$row2['tg_descripcion'];
              $mue[$cont][3]=$pi;
              $mue[$cont][4]=$pm;
              $mue[$cont][5]=$pv;
              $mue[$cont][6]=$pe;
              $mue[$cont][7]=$e;
              $mue[$cont][8]=$part;
              $tabla .= '<tr class="modo1">';
                $tabla .= '<td><b>'.$row2['tg_descripcion'].'</b></td>';
                $tabla .= '<td>'.number_format($pi, 2, ',', '.').'</td>';
                $tabla .= '<td>'.number_format($pm, 2, ',', '.').'</td>';
                $tabla .= '<td>'.number_format($pv, 2, ',', '.').'</td>';
                $tabla .= '<td>'.number_format($pe, 2, ',', '.').'</td>';
                $tabla .= '<td>'.$e.'%</td>';
                $tabla .= '<td>'.$part.'%</td>';
              $tabla .= '</tr>';
            }
          }
        }
              $tabla .= '<tr class="modo1">';
                $tabla .= '<td><b>TOTAL</b></td>';
                $tabla .= '<td>'.number_format($suma_gasto_total[0]['pi'], 2, ',', '.').'</td>';
                $tabla .= '<td>'.number_format($suma_gasto_total[0]['pm'], 2, ',', '.').'</td>';
                $tabla .= '<td>'.number_format($suma_gasto_total[0]['pv'], 2, ',', '.').'</td>';
                $tabla .= '<td>'.number_format($suma_gasto_total[0]['pe'], 2, ',', '.').'</td>';
                $ejec=0;
                if($suma_gasto_total[0]['pv']!=0){
                  $ejec=round((($suma_gasto_total[0]['pe']/$suma_gasto_total[0]['pv'])*100),2);
                }
                $tabla .= '<td>'.$ejec.'%</td>';

                $tabla .= '<td>100%</td>';
              $tabla .= '</tr>';
      //  $tabla .= '</table>';
     
        return $tabla;
    }

  /*============ REPORTE TABLA DETALLES POR SECTORES ECONOMICOS ============*/
   public function reporte_rubro_gasto()
   {
    $mess=$this->get_mes($this->session->userdata("mes"));
    $mes = $mess[1];
    $dias = $mess[2];

    $data['tgasto']=$this->get_matriz_tgasto();
    $tabla=$this->genera_tabla_tgasto($data['tgasto']);

    $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                          <center><img src="'.base_url().'assets/img/logo.jpg" class="siipp"/></center>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata("entidad").'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$this->gestion.'<br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                            <b>REPORTE : </b> EJECUCI&Oacute;N PRESPUESTARIA GENERAL POR RUBRO DE GASTO (Al de '.$dias.' de '.$mes.' de '.$this->gestion.') <br>
                            (Al de '.$dias.' de '.$mes.' de '.$this->gestion.') (Expresado en Bolivianos)
                        </td>
                        <td width=20%;>
                            <center><img src="'.base_url().'assets/img/escudo.jpg" alt="" width="90px"></center>
                        </td>
                    </tr>
                </table>
                <footer>
                    <table class="table table-bordered" width="100%">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata('sistema_pie').'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>
                <div>
                  <table border="0" cellpadding="0" cellspacing="0" class="tabla">
                    <thead>
                        <tr class="modo1" bgcolor="#e5e8ec">
                        <th>TIPO DE GASTO</th>
                        <th>PRESUPUESTO INICIAL</th>
                        <th>MODIFICACIONES</th>
                        <th>PRESUPUESTO VIGENTE</th>
                        <th>EJECUCI&Oacute;N</th>
                        <th>% EJECUCI&Oacute;N</th>
                        <th>% PART.</th>
                      </tr>
                    </thead>
                    <tbody>
                      '.$tabla.'
                    </tbody>
                  </table>
                </div>
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'landscape');
        ini_set('memory_limit','-1');
        ini_set('max_execution_time', 0);
        $dompdf->render();
        $dompdf->stream("reporte_rubro_gasto.pdf", array("Attachment" => false));
   }
    public function get_mes($mes_id)
    {
      $mes[1]='Enero';
      $mes[2]='Febrero';
      $mes[3]='Marzo';
      $mes[4]='Abril';
      $mes[5]='Mayo';
      $mes[6]='Junio';
      $mes[7]='Julio';
      $mes[8]='Agosto';
      $mes[9]='Septiembre';
      $mes[10]='Octubre';
      $mes[11]='Noviembre';
      $mes[12]='Diciembre';

      $dias[1]='31';
      $dias[2]='28';
      $dias[3]='31';
      $dias[4]='30';
      $dias[5]='31';
      $dias[6]='30';
      $dias[7]='31';
      $dias[8]='31';
      $dias[9]='30';
      $dias[10]='31';
      $dias[11]='30';
      $dias[12]='31';

      $valor[1]=$mes[$mes_id];
      $valor[2]=$dias[$mes_id];

      return $valor;
    }

  private $estilo_vertical = 
    '<style>
        body{
            font-family: sans-serif;
            }
        @page {
        margin: 40px 40px;
        }
        header {
            position: fixed;
            left: 0px;
            top: -20px;
            right: 0px;
            height: 20px;
            background-color: #fff;
            }
        header h1{
            margin: 5px 0;
        }
        header h2{
            margin: 0 0 10px 0;
        }

        footer {
            position: fixed;
            left: 0px;
            bottom: -82px;
            right: 0px;
            height: 100px;
            border-bottom: 1px solid #ddd;
        }
        footer .page:after {
            content: counter(page) ;
        }
        footer table {
            width: 100%;
        }
        footer p {
            text-align: right;
        }
        footer .izq {
            text-align: left;
        }
      table{
            font-size: 7px;
            width: 100%;
            background-color:#fff;
        }
        .mv{font-size:10px;}
        .siipp{width:120px;}

      .titulo_pdf {
            text-align: left;
            font-size: 10px;
        }
        .tabla {
      font-family: Verdana, Arial, Helvetica, sans-serif;
      font-size: 7px;
                  width: 100%;

      }
              .tabla th {
      padding: 5px;
      font-size: 7px;
      background-color: #83aec0;
      background-image: url(fondo_th.png);
      background-repeat: repeat-x;
      color: #FFFFFF;
      border-right-width: 1px;
      border-bottom-width: 1px;
      border-right-style: solid;
      border-bottom-style: solid;
      border-right-color: #558FA6;
      border-bottom-color: #558FA6;
      font-family: "Trebuchet MS", Arial;
      text-transform: uppercase;
      }
      .tabla .modo1 {
      font-size: 7px;
      font-weight:bold;

      background-image: url(fondo_tr01.png);
      background-repeat: repeat-x;
      color: #34484E;
      font-family: "Trebuchet MS", Arial;
      }
      .tabla .modo1 td {
      padding: 5px;
      border-right-width: 1px;
      border-bottom-width: 1px;
      border-right-style: solid;
      border-bottom-style: solid;
      border-right-color: #A4C4D0;
      border-bottom-color: #A4C4D0;

      }
    </style>';
}