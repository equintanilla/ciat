<?php
define("DOMPDF_ENABLE_REMOTE", true);
define("DOMPDF_TEMP_DIR", "/tmp");
class c_transferencias extends CI_Controller { 
    var $gestion;
    var $mes;
    var $rol;
    var $fun_id;

  public function __construct ()
    { 
        parent::__construct();
        if($this->session->userdata('fun_id')!=null){
        $this->load->library('pdf');
        $this->load->library('pdf2');

        $this->load->model('reportes_tarija_das/mdas_complementario');

        $this->load->model('programacion/model_proyecto');
        $this->load->model('programacion/model_producto');
        $this->load->model('programacion/model_faseetapa');
        $this->load->model('programacion/model_componente');
        $this->load->model('programacion/model_actividad');

        $this->load->model('mantenimiento/mapertura_programatica');
        $this->load->model('mantenimiento/munidad_organizacional');
        $this->load->model('reportes_tarija/model_transferencias');
        $this->load->model('menu_modelo');
        $this->load->model('Users_model','',true);

        $this->gestion = $this->session->userData('gestion');
        $this->mes = $this->session->userData('mes');
        $this->rol = $this->session->userData('rol');
        $this->fun_id = $this->session->userData('fun_id');

        }else{
            redirect('/','refresh');
        }
    }

    /*================ TRANSFERENCIAS CAPITAL ==================*/
    public function transferencias_cap_municipales()
    { 
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++)
        {
          $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];

        //// tg_id=8 -> Transaccion de capital
        $data['nro']=count($this->model_transferencias->list_municipios())+1;

        $data['muni']=$this->get_matriz_transaccion(8);
        $data['tabla']=$this->genera_tabla_trans($data['muni'],$data['nro'],8,1);

        $this->load->view('admin/reportes/ejecucion_presupuestaria/transferencias/trans_muni_capital', $data);
    }

    /*================== TRANSFERENCIAS CORRIENTES ================*/
    public function transferencias_corriente_municipales()
    { 
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++)
        {
          $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];

        //// tg_id=8 -> Transaccion de capital
        $data['nro']=count($this->model_transferencias->list_municipios())+1;

        $data['muni']=$this->get_matriz_transaccion(11);
        $data['tabla']=$this->genera_tabla_trans($data['muni'],$data['nro'],11,2);

        $this->load->view('admin/reportes/ejecucion_presupuestaria/transferencias/trans_muni_corriente', $data);
    }

    /*============== reporte Transferencias Municipales ==================*/
    public function rep_transferencias($tp)
    { 
        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];

        $data['nro']=count($this->model_transferencias->list_municipios())+1;
        $data['muni']=$this->get_matriz_transaccion($tp);

        if($tp==8){
          $data['title']='TRANSFERENCIAS DE CAPITALES A GOBIERNOS MUNICIPALES';
          $data['tabla']=$this->genera_tabla_trans($data['muni'],$data['nro'],$tp,1);
        }
        else{
          $data['title']='TRANSFERENCIAS CORRIENTES A GOBIERNOS MUNICIPALES';
          $data['tabla']=$this->genera_tabla_trans($data['muni'],$data['nro'],$tp,2);
        }
        

        $this->load->view('admin/reportes/ejecucion_presupuestaria/transferencias/trans_muni_imprimir', $data);
    }

    public function get_matriz_transaccion($tp)
    {
        $i=1;
        $municipios=$this->model_transferencias->list_municipios();
        $nro=count($municipios); 
        $spi=0;$spm=0;$spv=0;$spe=0;$se=0;$spar=0;
        $suma_total=$this->model_transferencias->suma_total_programas_hijos_transferencia($this->gestion,$this->mes,$tp); 
        if (count($suma_total)!=0) {
            $spi=$suma_total[0]['pi'];
            $spm=$suma_total[0]['pm'];
            $spv=$suma_total[0]['pv'];
            $spe=$suma_total[0]['pe'];
            if($spv!=0){
               $se=round((($spe/$spv)*100),2); 
            }
        }
        foreach($municipios as $rowm)
        {
          $suma=$this->model_transferencias->suma_programas_hijos_transferencia($this->gestion,$this->mes,$tp,$rowm['muni_id']); 
          $pi=0;$pm=0;$pv=0;$pe=0;$ejec=0;$part=0;
          if(count($suma)!=0){
            $pi=$suma[0]['pi'];
            $pm=$suma[0]['pm'];
            $pv=$suma[0]['pv'];
            $pe=$suma[0]['pe'];
            $ejec=0;
            if($pv!=0){
                $ejec=round((($pe/$pv)*100),2);
            }
            $part=0;
            if($suma_total[0]['pe']!=0){
                $part=round((($pe/$suma_total[0]['pe'])*100),2);
            }
          }

          $muni[$i][1]=$rowm['muni_id'];
          $muni[$i][2]=$rowm['muni_municipio'];
          $muni[$i][3]=$pi;
          $muni[$i][4]=$pm;
          $muni[$i][5]=$pv;
          $muni[$i][6]=$pe;
          $muni[$i][7]=$ejec;
          $muni[$i][8]=$part;
          $i++;
        }

        $muni[$i][1]='';
        $muni[$i][2]='TOTAL';
        $muni[$i][3]=$spi;
        $muni[$i][4]=$spm;
        $muni[$i][5]=$spv;
        $muni[$i][6]=$spe;
        $ejec=0;
        if($spv!=0){
            $ejec=round((($spe/$spv)*100),2);
        }
        $muni[$i][7]=$ejec;
        $muni[$i][8]=100;

        return $muni;

    }

    /*==================== GENERA TABLA TRANSFERENCIAS MUNICIPALES ================*/
    public function genera_tabla_trans($muni,$nro,$tp,$tp_rep)
    {
        $tabla = '';
        //$tabla .= '<table>';
          for ($p=1; $p <= $nro-1; $p++) { 
            $tabla .= '<tr class="even_row">';
            for ($k=2; $k <=8; $k++) { 
              
              if($k==3 || $k==4 || $k==5 || $k==6){
                $tabla .= '<td>'.number_format($muni[$p][$k], 2, ',', '.').'</td>';
              }
              elseif($k==7 || $k==8){
                $tabla .= '<td>'.$muni[$p][$k].'%</td>';
              }
              else{
                $tabla .= '<td><b><a href="' . site_url("") . '/rep/ejec/det_trans_muni/'.$tp.'/'.$muni[$p][1].'/'.$tp_rep.'" title="DETALLE : '.$muni[$p][$k].'">'.strtoupper($muni[$p][$k]).'</a></b></td>';
              }
              
            }
            $tabla .= '</tr>';
          }
          $tabla .= '<tr bgcolor="#f4f4f4">';
            for ($k=2; $k <=8; $k++) { 
              if($k==3 || $k==4 || $k==5 || $k==6){
                $tabla .= '<td>'.number_format($muni[$p][$k], 2, ',', '.').'</td>';
              }
              elseif($k==7 || $k==8){
                $tabla .= '<td>'.$muni[$p][$k].'%</td>';
              }
              else{
                $tabla .= '<td><b>'.$muni[$p][$k].'</b></td>';
              }
            }
        $tabla .= '</tr>';
        //$tabla .= '</table>';

      return $tabla;
    }

    public function detalle_transferencias_municipales($tp,$muni_id,$tp_rep)
    {
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(7);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++)
        {
             $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];

        $data['muni_id'] = $muni_id;
        $data['municipio'] = $this->model_transferencias->get_municipio($muni_id);

        
        if($tp_rep==1){
            $data['atras']= '' . base_url() . 'index.php/rep/ejec/transferencias';
            $data['titulo']='Transferencias de Capital a Gobiernos Municipales';
            $data['tp_trans']=8;
        }
        elseif ($tp_rep==2) {
            $data['atras']= '' . base_url() . 'index.php/rep/ejec/transferencias_corriente';
            $data['titulo']='Transferencias Corrientes a Gobiernos Municipales';
            $data['tp_trans']=11;
        }
        
        $data['tp_rep']=$tp_rep;
        
        $data['detalle']=$this->tabla_detalle_transferencias($muni_id,1,$tp);
        $this->load->view('admin/reportes/ejecucion_presupuestaria/transferencias/trans_muni_detalle', $data); 
    }

    public function tabla_detalle_transferencias($muni_id,$tp,$tp_trans)
    {
        if($tp==1){
        $tb='id="dt_basic" class="table table table-bordered" width="100%"';
        }
        elseif ($tp==2) {
          $tb='border="0" cellpadding="0" cellspacing="0" class="tabla"';
        }

        $unidades = $this->mdas_complementario->unidades_ejecutoras();
        $sum=0;
        $tabla ='';
        $tabla.='<table '.$tb.'>';
            $tabla.='<thead>';
            $tabla.='<tr class="modo1">';
              $tabla.='<th style="width:15%;">UNIDAD EJECUTORA</td>';
              $tabla.='<th style="width:15%;">PROYECTO-PROGRAMA.OPERACI&Oacute;N</th>';
              $tabla.='<th style="width:10%;">COSTO TOTAL DE PROYECTO</th>';
              $tabla.='<th style="width:10%;">COSTO TOTAL GESTI&Oacute;N '.$this->session->userdata('gestion').'</th>';
              $tabla.='<th style="width:10%;">PRESUPUESTO INICIAL '.$this->session->userdata('gestion').'</th>';
              $tabla.='<th style="width:10%;">PRESUPUESTO MODIFICADO '.$this->session->userdata('gestion').'</th>';
              $tabla.='<th style="width:10%;">PRESUPUESTO VIGENTE '.$this->session->userdata('gestion').'</th>';
              $tabla.='<th style="width:10%;">EJEC. FINANCIERA DE GESTI&Oacute;N '.$this->session->userdata('gestion').'</th>';
              $tabla.='<th style="width:10%;">% EJEC. FINANCIERA '.$this->session->userdata('gestion').'</th>';
              $tabla.='<th style="width:10%;">% EJEC. FINANCIERA DE PROYECTO </th>';
              $tabla.='<th style="width:10%;">% EJEC. F&Iacute;SICA ACUMULADA </th>';
              $tabla.='<th style="width:10%;">ESTADO</th>';
            $tabla.='</tr>';
            $tabla.='</thead>';
            $tabla.='<tbody>';
        foreach($unidades as $rowue)
        {
          $programas = $this->model_transferencias->programas_hijos_transferencia_detalles($this->gestion,$this->mes,$tp_trans,$muni_id,$rowue['uni_id']);
          
          if(count($programas)!=0){
          //  echo "UNI ID ".$rowue['uni_id']." UNIDAD EJECUTORA : ".$rowue['uni_unidad']."<br>";
          //  echo "NRO : ".count($programas)."<br>";
            foreach($programas as $rowp)
            {
              $ejec=0;
              if($rowp['pe_pv']!=0){
                $ejec=round((($rowp['pe_pe']/$rowp['pe_pv'])*100),2);
              }

              $se=$rowp['pe_pe'];
              $suma=$this->mdas_complementario->ejecuciones($rowp['pfec_id'],$this->gestion);
              if($suma[0]['total_ejecutado']!='' || $suma[0]['total_ejecutado']!=0){
                $se=$se+$suma[0]['total_ejecutado'];
              }

              $suma_prog=$this->mdas_complementario->costo_total_programado($rowp['pfec_id']);

              $tabla.='<tr class="modo1">';
                $tabla.='<td>'.$rowue['uni_unidad'].'</td>';
                $tabla.='<td>'.$rowp['proy_nombre'].'</td>';
                $tabla.='<td>'.number_format($suma_prog[0]['total_programado'], 2, ',', ' ').'</td>';
                $tabla.='<td>'.number_format($rowp['pfecg_ppto_total'], 2, ',', ' ').'</td>';
                $tabla.='<td>'.number_format($rowp['pe_pi'], 2, ',', ' ').'</td>';
                $tabla.='<td>'.number_format($rowp['pe_pm'], 2, ',', ' ').'</td>';
                $tabla.='<td>'.number_format($rowp['pe_pv'], 2, ',', ' ').'</td>';
                $tabla.='<td>'.number_format($rowp['pe_pe'], 2, ',', ' ').'</td>';
                $tabla.='<td>'.$ejec.'%</td>';
                $efin=0;
                if($suma_prog[0]['total_programado']!=0){
                  $efin=round((($se/$suma_prog[0]['total_programado'])*100),2);
                }
                $tabla.='<td>'.$efin.'%</td>';

                $efis=$this->avance_fisico($rowp['proy_id'],$this->mes);
                $tabla.='<td bgcolor="#cbe0cb">'.round($efis,2).'%</td>';
                $estado=$this->mdas_complementario->estado_operacion($rowp['pfec_id'],$this->mes,$this->gestion);
                $estado_proy='-----';
                if(count($estado)!=0){
                  $estado_proy=$estado[0]['ep_descripcion'];
                }
                $tabla.='<td>'.$estado_proy.'</td>';
              $tabla.='</tr>';

            }
          }
        }
      //  $total = $this->model_transferencias->suma_programas_hijos_transferencia($this->gestion,$this->mes,$tp_trans,$muni_id);
        $tabla.='</tbody>';
         /* $tabla.='<tr class="modo1" bgcolor="#5f6367">';
            $tabla.='<th style="width:10%;"><font color="#ffffff">TOTAL</font></td>';
            $tabla.='<th style="width:10%;"></th>';
            $tabla.='<th style="width:10%;"></th>';
            $tabla.='<th style="width:10%;"></th>';
            if(count($total)!=0){
            $tabla.='<th style="width:10%;"><font color="#ffffff">'.number_format($total[0]['pe_pi'], 2, ',', ' ').'</font></th>';
            $tabla.='<th style="width:10%;"><font color="#ffffff">'.number_format($total[0]['pe_pm'], 2, ',', ' ').'</font></th>';
            $tabla.='<th style="width:10%;"><font color="#ffffff">'.number_format($total[0]['pe_pv'], 2, ',', ' ').'</font></th>';
            }
            else{
            $tabla.='<th style="width:10%;"><font color="#ffffff">0.00</font></th>';
            $tabla.='<th style="width:10%;"><font color="#ffffff">0.00</font></th>';
            $tabla.='<th style="width:10%;"><font color="#ffffff">0.00</font></th>';
            }
           
            $tabla.='<th style="width:10%;"></th>';
            $tabla.='<th style="width:10%;"></th>';
            $tabla.='<th style="width:10%;"></th>';
            $tabla.='<th style="width:10%;"></th>';
            $tabla.='<th style="width:10%;"></th>';
          $tabla.='</tr>';*/
        $tabla.='</table><br>';
        /*echo "<br><br>";
        echo "SUMA TOTAL PI : ".$sum."";*/
        return $tabla;
    }

      /*======================= EJECUCION FISICA DEL PROYECTO AL MES ACTUAL =============================*/
  public function avance_fisico($proy_id,$id_mes)
  {  
      $fase = $this->model_faseetapa->get_id_fase($proy_id);
      $años=$fase[0]['pfec_fecha_fin']-$fase[0]['pfec_fecha_inicio']+1;
      $meses=$años*12;

      $ms[0]='programado_total';$ms[1]='enero';$ms[2]='febrero';$ms[3]='marzo';$ms[4]='abril';$ms[5]='mayo';$ms[6]='junio';
      $ms[7]='julio';$ms[8]='agosto';$ms[9]='septiembre';$ms[10]='octubre';$ms[11]='noviembre';$ms[12]='diciembre';

      $componentes = $this->model_componente->componentes_id($fase[0]['id']);
      for($p=1;$p<=$meses;$p++){$cp[1][$p]=0; $cp[2][$p]=0; $cp[3][$p]=0;}
      foreach ($componentes as $rowc)
      {
        $productos = $this->model_producto->list_prod($rowc['com_id']);
        for($p=1;$p<=$meses;$p++){$pp[1][$p]=0; $pp[2][$p]=0;}
       // echo "COMPONENTE : ".$rowc['com_componente']."<br>";
        foreach ($productos as $rowp)
        {
            $actividad = $this->model_actividad->list_act_anual($rowp['prod_id']);
          //  echo "PRODUCTO : ".$rowp['prod_producto']."<br>";
            for($p=1;$p<=$meses;$p++){$ap[1][$p]=0; $ap[2][$p]=0;}
            foreach ($actividad as $rowa)
            {   
              $variablep=1;$variablee=1; $v=12; $nro=1; $sump=0;$sume=0;
              for($p=1;$p<=$meses;$p++){$a[1][$p]=0; $a[2][$p]=0;$a[3][$p]=0;$a[4][$p]=0;$a[5][$p]=0;$a[6][$p]=0;} //// vector vacio para sumar a nivel de actividades programacion , ejecutado
              for($i=$fase[0]['pfec_fecha_inicio'];$i<=$fase[0]['pfec_fecha_fin'];$i++)
              {
                $aprog = $this->model_actividad->actividad_programado($rowa['act_id'],$i);
                if(count($aprog)!=0){
                  for ($j=1; $j <=12 ; $j++) { 
                    $a[1][$variablep]=$aprog[0][$ms[$j]];
                    $variablep++;
                  }
                }
                else{
                  $variablep=($v*$nro)+1;
                }

                $aejec = $this->model_actividad->actividad_ejecutado($rowa['act_id'],$i);
                if(count($aejec)!=0){
                  for ($j=1; $j <=12 ; $j++) { 
                    $a[4][$variablee]=$aejec[0][$ms[$j]];
                    $variablee++;
                  }
                }
                else{
                  $variablee=($v*$nro)+1;
                }

                $nro++;
              }

              for ($i=1; $i <=$meses ; $i++) { 
                $sump=$sump+$a[1][$i];
                $a[2][$i]=$sump+$rowa['act_linea_base'];
                
                if($rowa['act_meta']!=0)
                {
                  $a[3][$i]=round(((($sump+$rowa['act_linea_base'])/$rowa['act_meta'])*100),1);
                }
                $ap[1][$i]=$ap[1][$i]+(($a[3][$i]*$rowa['act_ponderacion'])/100);

                $sume=$sume+$a[4][$i];
                $a[5][$i]=$sume+$rowa['act_linea_base'];

                if($rowa['act_meta']!=0)
                {
                  $a[6][$i]=round(((($sume+$rowa['act_linea_base'])/$rowa['act_meta'])*100),1);
                }
                $ap[2][$i]=$ap[2][$i]+(($a[6][$i]*$rowa['act_ponderacion'])/100);
              }

            }

              for ($i=1; $i <=$meses ; $i++) {
                $pp[1][$i]=$pp[1][$i]+(($ap[1][$i]*$rowp['prod_ponderacion'])/100);
                $pp[2][$i]=$pp[2][$i]+(($ap[2][$i]*$rowp['prod_ponderacion'])/100);
              } 
              
        }
          for ($i=1; $i <=$meses ; $i++) {
                $cp[1][$i]=$cp[1][$i]+(($pp[1][$i]*$rowc['com_ponderacion'])/100);
                $cp[2][$i]=$cp[2][$i]+(($pp[2][$i]*$rowc['com_ponderacion'])/100);
              }

      }

      for ($i=1; $i <=$meses ; $i++) {
        if($cp[1][$i]!=0){
          $cp[3][$i]=round((($cp[2][$i]/$cp[1][$i])*100),1);
        }
      }

      for ($i=1; $i <=12 ; $i++) { 
        $tefic[1][$i]=0;$tefic[2][$i]=0;$tefic[3][$i]=0;
      }
      $i=1;$vmeses=12;$gestion=$fase[0]['pfec_fecha_inicio'];$nro=1;
      for($p=$fase[0]['pfec_fecha_inicio'];$p<=$fase[0]['pfec_fecha_fin'];$p++)
      {
          $variable=0;
          for($pos=$i;$pos<=$vmeses;$pos++)
          {
            $variable++;
            $tefic[1][$variable]=$cp[1][$pos];
            $tefic[2][$variable]=$cp[2][$pos];
            $tefic[3][$variable]=$cp[3][$pos];
          }
          if($p==$this->session->userdata('gestion'))
          {
              $ejecucion=$tefic[2][$id_mes];
          }

            $i=$vmeses+1;
            $vmeses=$vmeses+12;
            $gestion++; $nro++;
      }

      return $ejecucion;
  }
  /*=================================================================================================*/
    /*============ REPORTE TABLA DETALLES TRANSFERENCIAS ============*/
   public function reporte_detalle_transferencias_municipales($muni_id,$tp_rep,$tp_trans)
   {
    $mess=$this->get_mes($this->session->userdata("mes"));
    $mes = $mess[1];
    $dias = $mess[2];

    $municipio = $this->model_transferencias->get_municipio($muni_id);
    $detalles=$this->tabla_detalle_transferencias($muni_id,2,$tp_trans);

    if($tp_rep==1){
            $titulo='TRANSFERENCIAS DE CAPITAL A GOBIERNOS MUNICIPALES';
        }
        elseif ($tp_rep==2) {
            $titulo='TRANSFERENCIAS CORRIENTES A GOBIERNOS MUNICIPALES';
        }

    $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                            <center><img src="'.base_url().'assets/img/logo.jpg" class="siipp"/></center>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata("entidad").'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$this->gestion.'<br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                            <b>REPORTE : </b> '.$titulo.' / '.$municipio[0]['muni_municipio'].'<br>
                            (Al de '.$dias.' de '.$mes.' de '.$this->gestion.') (Expresado en Bolivianos)
                        </td>
                        <td width=20%;>
                            <center><img src="'.base_url().'assets/img/escudo.jpg" alt="" width="90px"></center>
                        </td>
                    </tr>
                </table>
                <footer>
                    <table class="table table-bordered" width="100%">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata('sistema_pie').'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>
                '.$detalles.'
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'landscape');
        ini_set('memory_limit','-1');
        ini_set('max_execution_time', 0);
        $dompdf->render();
        $dompdf->stream("reporte_detalle_prog_gasto_corriente.pdf", array("Attachment" => false));
   }
    public function get_mes($mes_id)
    {
      $mes[1]='Enero';
      $mes[2]='Febrero';
      $mes[3]='Marzo';
      $mes[4]='Abril';
      $mes[5]='Mayo';
      $mes[6]='Junio';
      $mes[7]='Julio';
      $mes[8]='Agosto';
      $mes[9]='Septiembre';
      $mes[10]='Octubre';
      $mes[11]='Noviembre';
      $mes[12]='Diciembre';

      $dias[1]='31';
      $dias[2]='28';
      $dias[3]='31';
      $dias[4]='30';
      $dias[5]='31';
      $dias[6]='30';
      $dias[7]='31';
      $dias[8]='31';
      $dias[9]='30';
      $dias[10]='31';
      $dias[11]='30';
      $dias[12]='31';

      $valor[1]=$mes[$mes_id];
      $valor[2]=$dias[$mes_id];

      return $valor;
    }
     private $estilo_vertical = 
    '<style>
        body{
            font-family: sans-serif;
            }
        @page {
        margin: 40px 40px;
        }
        header {
            position: fixed;
            left: 0px;
            top: -20px;
            right: 0px;
            height: 20px;
            background-color: #fff;
            }
        header h1{
            margin: 5px 0;
        }
        header h2{
            margin: 0 0 10px 0;
        }

        footer {
            position: fixed;
            left: 0px;
            bottom: -82px;
            right: 0px;
            height: 100px;
            border-bottom: 1px solid #ddd;
        }
        footer .page:after {
            content: counter(page) ;
        }
        footer table {
            width: 100%;
        }
        footer p {
            text-align: right;
        }
        footer .izq {
            text-align: left;
        }
      table{
            font-size: 7px;
            width: 100%;
            background-color:#fff;
        }
        .mv{font-size:10px;}
        .siipp{width:120px;}

      .titulo_pdf {
            text-align: left;
            font-size: 10px;
        }
        .tabla {
      font-family: Verdana, Arial, Helvetica, sans-serif;
      font-size: 7px;
                  width: 100%;

      }
              .tabla th {
      padding: 5px;
      font-size: 7px;
      background-color: #83aec0;
      background-image: url(fondo_th.png);
      background-repeat: repeat-x;
      color: #FFFFFF;
      border-right-width: 1px;
      border-bottom-width: 1px;
      border-right-style: solid;
      border-bottom-style: solid;
      border-right-color: #558FA6;
      border-bottom-color: #558FA6;
      font-family: "Trebuchet MS", Arial;
      text-transform: uppercase;
      }
      .tabla .modo1 {
      font-size: 7px;
      font-weight:bold;

      background-image: url(fondo_tr01.png);
      background-repeat: repeat-x;
      color: #34484E;
      font-family: "Trebuchet MS", Arial;
      }
      .tabla .modo1 td {
      padding: 5px;
      border-right-width: 1px;
      border-bottom-width: 1px;
      border-right-style: solid;
      border-bottom-style: solid;
      border-right-color: #A4C4D0;
      border-bottom-color: #A4C4D0;

      }
    </style>';
}