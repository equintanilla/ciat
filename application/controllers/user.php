<?php

class User extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Users_model', '', true);
        $this->load->model('mantenimiento/model_funcionario');
        $this->load->model('programacion/model_faseetapa');
        $this->load->model('programacion/model_proyecto');
        $this->load->model('model_pei');
        $this->load->model('model_control_menus');
        $this->load->library('session');
        $this->load->library('encrypt');
    }
    public function index()
    {
        if ($this->session->userdata('is_logged_in')) {
            redirect('admin/dashboard');
        } else {
            $this->load->view('admin/login');
        }
    }
    public function vaca_404()
    {
        $this->load->view('rewriten_404');
    }

    public function html_menu_opciones($o_filtro)
    {
        switch ($o_filtro) {
            case '1':
                $enlace = '
                    <li>
                        <a href="'.base_url().'index.php/admin/dm/11/" class="jarvismetro-tile big-cubes bg-color-greenLight">
                            <span class="iconbox"> 
                                <img class="img-circle" src="'.base_url().'assets/img/pei.png"/>
                                <span>
                                    <center>P.E.I.</center>
                                </span>
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="'.base_url().'index.php/admin/dm/1/" class="jarvismetro-tile big-cubes bg-color-greenLight">
                            <span class="iconbox"> 
                                <img class="img-circle" src="'.base_url().'assets/img/programacion.png"/>
                                <span>
                                    <center>Programación del POA</center>
                                </span>
                            </span>
                        </a>
                    </li>';
                break;
            case '2':
                $enlace = '
                    <li>
                        <a href="'.base_url().'index.php/admin/dm/2/" class="jarvismetro-tile big-cubes bg-color-blueDark"> 
                            <span class="iconbox">
                                <img class="img-circle" src="'.base_url().'assets/img/registro1.png" />
                                <span>
                                    <center>Modificaciones del POA</center>
                                </span>
                            </span>
                        </a>
                    </li>
                ';
                break;
            case '3':
                $enlace = '
                    <li>
                        <a href="'.base_url().'index.php/admin/dm/3/" class="jarvismetro-tile big-cubes bg-color-purple">
                            <span class="iconbox">
                                <img class="img-circle" src="'.base_url().'assets/img/trabajo_social.png" />
                                <span>
                                    <center>Registro de Ejecución </center>
                                </span>
                            </span>
                        </a>
                    </li>
                ';
                break;
            case '4':
                $enlace = '
                    <li>
                        <a href="'.base_url().'index.php/admin/dm/4/" class="jarvismetro-tile big-cubes bg-color-orangeDark">
                            <span class="iconbox"> 
                                <img class="img-circle" src="'.base_url().'assets/img/gerencia.png" />
                                <span>
                                    <center>Gerencia Proyectos</center>
                                </span>
                            </span> 
                        </a>
                    </li>
                ';
                break;
            case '5':
                $enlace = '
                    <li>
                        <a href="https://geo.gobernacionlapaz.gob.bo" class="jarvismetro-tile big-cubes bg-color-greenLight" title="Sistema de Información Geográfica"> 
                            <span class="iconbox">
                                <img class="img-circle" src="'.base_url().'assets/img/sig.png" />
                                <span>
                                    <center>S.I.G.</center>
                                </span>
                            </span>
                        </a>
                    </li>
                ';
                break;
            case '6':
                $enlace = '';
                break;
            case '7':
                $enlace = '
                    <li>
                        <a href="https://geo.gobernacionlapaz.gob.bo" target="_blank" class="jarvismetro-tile big-cubes bg-color-greenLight" title="Sistema de Información Geográfica"> 
                            <span class="iconbox">
                                <img class="img-circle" src="'.base_url().'assets/img/sig.png" />
                                <span>
                                    <center>S.I.G.</center>
                                </span>
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="'.base_url().'index.php/admin/dm/7/" class="jarvismetro-tile big-cubes bg-color-pinkDark">
                            <span class="iconbox">
                                <img class="img-circle" src="'.base_url().'assets/img/impresora.png" />
                                <span>
                                    <center>Reportes</center>
                                </span>
                            </span>
                        </a>
                    </li>
                ';
                break;
            case '8':
                $enlace = '';
                break;
            case '9':
                $enlace = '
                    <li>
                        <a href="'.base_url().'index.php/admin/dm/9/" class="jarvismetro-tile big-cubes bg-color-blueDark">
                            <span class="iconbox">
                                <img class="img-circle" src="'.base_url().'assets/img/mantenimiento1.png" />
                                <span>
                                    <center>Mantenimiento</center>
                                </span>
                            </span>
                        </a>
                    </li>
                ';
                break;
            case '10':
                $enlace = '
                    <li>
                        <a href="'.base_url().'index.php/admin/dm/10/" class="jarvismetro-tile big-cubes bg-color-pinkDark">
                            <span class="iconbox">
                                <img class="img-circle" src="'.base_url().'assets/img/impresora.png" />
                                <span>
                                    <center>Reportes MAE</center>
                                </span>
                            </span>
                        </a>
                    </li>
                ';
                break;
            default:
                $enlace = '';
                break;
        }
        return $enlace;
    }
    public function menu_principal_roles()
    {
        $fun_id = $this->session->userdata('fun_id');
        $menus = $this->model_control_menus->menu_segun_roles($fun_id);
        $vector;
        $n = 0;
        foreach ($menus as $fila) {
            $vector[$n] = $this->html_menu_opciones($fila['o_filtro']);
            $n++;
        }
		if(isset($vector)){
			return $vector;
		}else{
			redirect('user');
		}
    }
    public function dashboard_index()
    {
//		echo $this->session->userdata('is_logged_in');
        if($this->session->userdata('usuario')){
            $data['vector_menus'] = $this->menu_principal_roles();
            $this->load->view('admin/dashboard',$data);
        } else{
            $this->load->view('admin/login');
        }
    }

    public function dashboard_menu($opcion)
    {
        if($this->session->userdata('logged')){
            $this->load->model('menu_modelo');
            $dato_menu['se'] = '1900';//sesion
            $dato_menu['ay'] = '1900';//ayuda
            switch ($opcion) {
                case 1:
                    $enlaces = $this->menu_modelo->get_Modulos(1);
                    //GUARDA LOS ENLACES PADRES
                    $data['enlaces'] = $enlaces;
                    for ($i = 0; $i < count($enlaces); $i++) {
                        $subenlaces[$enlaces[$i]['o_child']] = $this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
                    }
                    $data['subenlaces'] = $subenlaces;
                    $data['datos'] = $this->model_pei->pei_mision_get();
                    $data['titulo'] = 'PROGRAMACION';
                    break;
                case 2:
                    $enlaces = $this->menu_modelo->get_Modulos(2);
                    //GUARDA LOS ENLACES PADRES
                    $data['enlaces'] = $enlaces;
                    for ($i = 0; $i < count($enlaces); $i++) {
                        $subenlaces[$enlaces[$i]['o_child']] = $this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
                    }
                    $data['subenlaces'] = $subenlaces;
                    $data['datos'] = $this->model_pei->pei_mision_get();
                    $data['titulo'] = 'MODIFICACIONES';
                    break;
                case 3:
                $enlaces = $this->menu_modelo->get_Modulos(3);
                    //GUARDA LOS ENLACES PADRES
                    $data['enlaces'] = $enlaces;
                    for ($i = 0; $i < count($enlaces); $i++) {
                        $subenlaces[$enlaces[$i]['o_child']] = $this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
                    }
                    $data['subenlaces'] = $subenlaces;
                    $data['datos'] = $this->model_pei->pei_mision_get();
                    $data['titulo'] = 'REGISTRO DE EJECUCIÓN';
                    break;
                case 4:

                $enlaces = $this->menu_modelo->get_Modulos(4);
                    //GUARDA LOS ENLACES PADRES
                    $data['enlaces'] = $enlaces;
                    for ($i = 0; $i < count($enlaces); $i++) {
                        $subenlaces[$enlaces[$i]['o_child']] = $this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
                    }
                    $data['subenlaces'] = $subenlaces;
                    $data['datos'] = $this->model_pei->pei_mision_get();
                    $data['titulo'] = 'GERENCIA DE PROYECTOS';
                    break;
                case 6:

                    $enlaces = $this->menu_modelo->get_Modulos(6);
                    //GUARDA LOS ENLACES PADRES
                    $data['enlaces'] = $enlaces;
                    for ($i = 0; $i < count($enlaces); $i++) {
                        $subenlaces[$enlaces[$i]['o_child']] = $this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
                    }
                    $data['subenlaces'] = $subenlaces;
                    $data['datos'] = $this->model_pei->pei_mision_get();
                    $data['titulo'] = 'SIG';
                    break;

                case 7:

                    $enlaces = $this->menu_modelo->get_Modulos(7);
                    //GUARDA LOS ENLACES PADRES
                    $data['enlaces'] = $enlaces;
                    for ($i = 0; $i < count($enlaces); $i++) {
                        $subenlaces[$enlaces[$i]['o_child']] = $this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
                    }
                    $data['subenlaces'] = $subenlaces;
                    $data['datos'] = $this->model_pei->pei_mision_get();
                    $data['titulo'] = 'REPORTES';
                    break;
                case 9:

                    $enlaces = $this->menu_modelo->get_Modulos(9);
                    //GUARDA LOS ENLACES PADRES
                    $data['enlaces'] = $enlaces;
                    for ($i = 0; $i < count($enlaces); $i++) {
                        $subenlaces[$enlaces[$i]['o_child']] = $this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
                    }
                    $data['subenlaces'] = $subenlaces;
                    $data['datos'] = $this->model_pei->pei_mision_get();
                    $data['titulo'] = 'MANTENIMIENTO';
                    break;
                case 10:
                    $enlaces = $this->menu_modelo->get_Modulos(10);
                    //GUARDA LOS ENLACES PADRES
                    $data['enlaces'] = $enlaces;
                    for ($i = 0; $i < count($enlaces); $i++) {
                        $subenlaces[$enlaces[$i]['o_child']] = $this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
                    }
                    $data['subenlaces'] = $subenlaces;
                    $data['datos'] = $this->model_pei->pei_mision_get();
                    $data['titulo'] = 'REPORTES MAE';
                    break;
                case 11:
                    $enlaces = $this->menu_modelo->get_Modulos(11);
                    //GUARDA LOS ENLACES PADRES
                    $data['enlaces'] = $enlaces;
                    for ($i = 0; $i < count($enlaces); $i++) {
                        $subenlaces[$enlaces[$i]['o_child']] = $this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
                    }
                    $data['subenlaces'] = $subenlaces;
                    $data['datos'] = $this->model_pei->pei_mision_get();
                    $data['titulo'] = 'P.E.I.';
                    break;

            }
            //$data['main_content'] = 'admin/menu_opcion';
            //$this->load->view('includes/template', $data);
            $this->load->view('includes/header');
            $this->load->view('includes/menu_lateral',$data);
            $this->load->view('admin/menu_opcion');
            $this->load->view('includes/footer');
        } else {
            redirect('admin/dashboard');
        }
    }
    function __encrip_password($password)
    {
        return md5($password);
    }
	/*
	*ESTA FUNCION ESTA OBSOLETA EN LA VERSION 1.1
	*/
    function validate_credentials_h()
    {

        $this->load->model('Users_model');

        $user_name = strtoupper($this->input->post('user_name'));
        $password = strtoupper(md5($this->input->post('password')));

        $is_valid = $this->Users_model->validate($user_name, $password);
        $data = $this->Users_model->datos_usuario($user_name, $password);
        $gestion = $this->Users_model->obtener_gestion();
        $entidad = $this->Users_model->get_entidad($gestion[0]['ide']);
        $entidad = $entidad->conf_nombre_entidad;
        // $mes_texto => $gestion[0]['conf_mes'],
        if ($is_valid == true) {
            $data = array(
                'user_name' => $user_name,
                'funcionario' => $data[0]['fun_nombre'] . " " . $data[0]['fun_apellido'],
                'usuario' => $data[0]['fun_usuario'],
                'fun_id' => $data[0]['fun_id'],
                'uni_id' => $data[0]['uni_id'],
                'unidad' => $data[0]['uni_unidad'],
                'boton_id' => $data[0]['b_id'],
                'rol_id' => $data[0]['r_id'],
                'gestion' => $gestion[0]['ide'],
                'mes' => $gestion[0]['conf_mes'],
                'entidad' => $entidad
            );

            $this->session->set_userdata($data);
            redirect('admin/dashboard');
        } else // incorrect username or password
        {
            $data['message_error'] = 'USuario / Contrase�a Incorrecto';
            $this->load->view('admin/login', $data);
        }
    }
    function validate_credentials()
    {
        $this->load->model('Users_model');
        $user_name = strtoupper($this->input->post('user_name'));
        $password = $this->input->post('password');
        $is_valid = $this->model_funcionario->verificar_loggin($user_name, $password);
        if($is_valid['bool']){
            //$this->session->sess_destroy();
            $data    = $this->Users_model->get_datos_usuario($is_valid['fun_id']);
            $gestion = $this->Users_model->obtener_gestion();
            $entidad = $this->Users_model->get_entidad($gestion[0]['ide']);
            $ent_sigla = $entidad->conf_sigla_entidad;
            $ent_nombre = $entidad->conf_nombre_entidad;
            $data = array(
                'user_name' => $user_name,
                'funcionario' => $data[0]['fun_nombre'] . " " . $data[0]['fun_paterno'],
                'usuario' => $data[0]['fun_usuario'],
                'fun_id' => $data[0]['fun_id'],
                'uni_id' => $data[0]['uni_id'],
                'unidad' => $data[0]['uni_unidad'],
                'rol_id' => $data[0]['r_id'],
                'ide' => $gestion[0]['ide'],
                'gestion' => $gestion[0]['conf_gestion'],
                'mes' => $gestion[0]['conf_mes'],
                'name' => 'SIPSyE V1.1',
                'sistema' => 'SISTEMA INTEGRADO DE PLANIFICACI&Oacute;N, MONITOREO, SEGUIMIENTO Y EVALUACI&OacuteN; INSTITUCIONAL - SIPSyE V1.1',
                'sistema_pie' => '&copy; SIPSyE V1.1 - Centro de Investigación Agrícola Tropical',
                'desc_mes' => $this->mes_texto($gestion[0]['conf_mes']),
                'entidad' => $ent_nombre,
                'entidad_sigla' => $ent_sigla,
                'logged' => true,
                'franja_decorativa'=>'<style>.verde{ width:100%; height:5px; background-color:#3B7F2A;}
        .rojo{ width:100%; height:5px; background-color:#D03437;}</style>'
            );
            $this->session->set_userdata($data);
            if($data['funcionario']=='CONTROL SOCIAL'){
                redirect('default_controller', 'refresh');
            }
            else{
              redirect('admin/dashboard');
            }
        }else{
            $this->session->set_flashdata('error', 'ERROR: Usuario o Contrase&ntilde;a Incorrectos. Intentelo Otra Vez');
            redirect('/', 'refresh');
        }
    }
    function logout()
    {
        // echo "1231231";
        $this->session->sess_destroy();
        redirect('admin/dashboard');
        // $this->index();
        // redirect('default_controller','refresh');
        // $this->load->view('admin/login');
    }
    function logout_control_social()
    {
        $this->session->sess_destroy();
        redirect('admin/logins');
    }
    function tasks()
    {
        $this->load->view('ajax/notify/tasks');
    }

    function notifications()
    {
        $this->load->view('ajax/notify/notifications');
    }
    function mail()
    {
        $this->load->view('ajax/notify/mail');
    }
    function menu()
    {
        $this->load->model('menu_modelo');
        $enlaces = $this->menu_modelo->get_Modulos();
        $data['enlaces'] = $enlaces;
        for ($i = 0; $i < count($enlaces); $i++) {
            $subenlaces[$enlaces[$i]['idchild']] = $this->menu_modelo->get_Enlaces($enlaces[$i]['idchild'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;/**/
        //$this->load->view('menu',$data);
    }
    function menu_enlace($sup)
    {
        $this->load->model('menu_modelo');
        $data['enlaces'] = $this->menu_modelo->get_Enlaces(0);
    }
    function mision()
    {
        $this->load->model('menu_modelo');
        $enlaces = $this->menu_modelo->get_Modulos(1);
        $data['enlaces'] = $enlaces;
        for ($i = 0; $i < count($enlaces); $i++) {
            $subenlaces[$enlaces[$i]['idchild']] = $this->menu_modelo->get_Enlaces($enlaces[$i]['idchild'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        $data['datos'] = $this->model_pei->pei_mision_get();
        $data['titulo'] = 'PROGRAMACI�N';

        //load the view
        $data['main_content'] = 'admin/marco_institucional/mision/mision';
        $this->load->view('includes/template', $data);
    }
    function vision()
    {

        $this->load->model('menu_modelo');
        $enlaces = $this->menu_modelo->get_Modulos(1);
        $data['enlaces'] = $enlaces;
        for ($i = 0; $i < count($enlaces); $i++) {
            $subenlaces[$enlaces[$i]['idchild']] = $this->menu_modelo->get_Enlaces($enlaces[$i]['idchild'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        $data['datos'] = $this->model_pei->pei_mision_get();
        $data['titulo'] = 'PROGRAMACI�N';
        //load the view
        $data['main_content'] = 'admin/marco_institucional/vision/vision';
        $this->load->view('includes/template', $data);
    }
    /*------------------------------------------------------*/
    /* 	mision registrar o editar
    /*------------------------------------------------------*/
    function pei_accion($accion)
    {
        $json = array();
        switch ($accion) {
            case 'mision_editar':
                $json['msj'] = 'mision editado';
                $json['success'] = true;
                //$data['material']=$this->model_pei->proveedores_editar();

                echo json_encode($json);
                break;
            case 'mision_guardar':
                $json['msj'] = 'mision Agregado';
                $json['success'] = true;
                $data['material'] = $this->model_pei->pei_mision_edit();
                echo json_encode($json);
                break;
            case 'vision_editar':
                $json['msj'] = 'vision editado';
                $json['success'] = true;
                //$data['material']=$this->model_pei->proveedores_editar();

                echo json_encode($json);
                break;
            case 'vision_guardar':
                $json['msj'] = 'vision Agregado';
                $json['success'] = true;
                $data['material'] = $this->model_pei->pei_vision_edit();
                echo json_encode($json);
                break;
        }
    }
    /*------------------------------------------------------*/
    /* 	acerca de ..
    /*------------------------------------------------------*/
    function acerca()
    {

        $this->load->model('menu_modelo');
        $enlaces = $this->menu_modelo->get_Modulos(1);
        $data['enlaces'] = $enlaces;
        for ($i = 0; $i < count($enlaces); $i++) {
            $subenlaces[$enlaces[$i]['idchild']] = $this->menu_modelo->get_Enlaces($enlaces[$i]['idchild'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        $data['datos'] = $this->model_pei->pei_mision_get();
        //load the view
        $data['main_content'] = 'admin/pei/ayuda/acerca_de';
        $this->load->view('includes/template', $data);
    }

    public function combo_fases_etapas() /////// COMBO DE FASES
    {
        //echo "urbanizaciones";
        $salida = "";
        $id_pais = $_POST["elegido"];
        // construimos el combo de ciudades deacuerdo al pais seleccionado
        $combog = pg_query("SELECT * FROM _etapas WHERE eta_clase=$id_pais");
        $salida .= "<option value=''>" . mb_convert_encoding('SELECCIONE FASE', 'cp1252', 'UTF-8') . "</option>";
        while ($sql_p = pg_fetch_row($combog)) {
            $salida .= "<option value='" . $sql_p[0] . "'>" . $sql_p[1] . "</option>";
        }
        echo $salida;
    }

    public function combo_clasificador($accion = '')
    {
        $salida = "";
        $accion = $_POST["accion"];


        switch ($accion) {
            case 'cl2':
                $salida = "";
                $id_pais = $_POST["elegido"];

                $combog = pg_query('SELECT * FROM _clasificadorsectorial WHERE nivel=\'2\' AND codsec=\'' . $id_pais . '\'');
                $salida .= "<option value=''>" . mb_convert_encoding('Seleccione Sub Sector', 'cp1252', 'UTF-8') . "</option>";
                while ($sql_p = pg_fetch_row($combog)) {
                    $salida .= "<option value='" . $sql_p[5] . "'>" . mb_convert_encoding($sql_p[0] . " - " . $sql_p[9] . " - " . $sql_p[6], 'cp1252', 'UTF-8') . "</option>";
                }

                echo $salida;
                //return $salida;
                break;

            case 'cl3':
                $salida = "";
                $id_pais = $_POST["elegido"];

                $combog = pg_query('SELECT * FROM _clasificadorsectorial WHERE nivel=\'3\' AND codsubsec=\'' . $id_pais . '\'');
                $salida .= "<option value=''>" . mb_convert_encoding('Seleccione Actividad Economica', 'cp1252', 'UTF-8') . "</option>";
                while ($sql_p = pg_fetch_row($combog)) {
                    $salida .= "<option value='" . $sql_p[0] . "'>" . $sql_p[0] . " - " . $sql_p[9] . " - " . $sql_p[1] . "</option>";
                }
                echo $salida;
                //return $salida;
                break;

            case 'funcion':
                $salida = "";
                $id_pais = $_POST["elegido"];

                $combog = pg_query('SELECT * FROM _finalidadfuncion WHERE  fifu_depende=\'' . $id_pais . '\'');
                $salida .= "<option value=''>" . mb_convert_encoding('Seleccione Funcion', 'cp1252', 'UTF-8') . "</option>";
                while ($sql_p = pg_fetch_row($combog)) {
                    $salida .= "<option value='" . $sql_p[0] . "'>" . $sql_p[0] . " - " . $sql_p[2] . " - " . $sql_p[4] . "</option>";
                }
                echo $salida;
                //return $salida;
                break;

            case 'clase_fn':
                $salida = "";
                $id_pais = $_POST["elegido"];

                $combog = pg_query('SELECT * FROM _finalidadfuncion WHERE  fifu_depende=\'' . $id_pais . '\'');
                $salida .= "<option value=''>" . mb_convert_encoding('Seleccione', 'cp1252', 'UTF-8') . "</option>";
                while ($sql_p = pg_fetch_row($combog)) {
                    $salida .= "<option value='" . $sql_p[0] . "'>" . $sql_p[0] . " - " . $sql_p[2] . " - " . $sql_p[4] . "</option>";
                }
                echo $salida;
                //return $salida;
                break;

            ////////////////////// PEDES//////////////////////////////////

            case 'pedes_2':
                $salida = "";
                $id_pais = $_POST["elegido"];

                $combog = pg_query('SELECT * FROM pdes WHERE pdes_depende=\'' . $id_pais . '\'');
                $salida .= "<option value=''>" . mb_convert_encoding('Seleccione', 'cp1252', 'UTF-8') . "</option>";
                while ($sql_p = pg_fetch_row($combog)) {
                    $salida .= "<option value='" . $sql_p[7] . "'>" . $sql_p[7] . " - " . $sql_p[2] . " - " . $sql_p[3] . "</option>";
                }
                echo $salida;
                //return $salida;
                break;

            case 'pedes_3':
                $salida = "";
                $id_pais = $_POST["elegido"];

                $combog = pg_query('SELECT * FROM pdes WHERE pdes_depende=\'' . $id_pais . '\'');
                $salida .= "<option value=''>" . mb_convert_encoding('Seleccione', 'cp1252', 'UTF-8') . "</option>";
                while ($sql_p = pg_fetch_row($combog)) {
                    $salida .= "<option value='" . $sql_p[7] . "'>" . $sql_p[7] . " - " . $sql_p[2] . " - " . $sql_p[3] . "</option>";
                }
                echo $salida;
                //return $salida;
                break;

            case 'pedes_4':
                $salida = "";
                $id_pais = $_POST["elegido"];

                $combog = pg_query('SELECT pdes_id FROM pdes WHERE pdes_depende=\'' . $id_pais . '\'');
                $salida .= "<option value=''>" . mb_convert_encoding('Seleccioneeeee', 'cp1252', 'UTF-8') . "</option>";
                while ($sql_p = pg_fetch_row($combog)) {
                    $salida .= "<option value='" . $sql_p[0] . "'>" . $sql_p[7] . " - " . $sql_p[2] . " - " . $sql_p[3] . "</option>";
                }
                echo $salida;
                //return $salida;
                break;

            case 'ptdi_2':
                $salida = "";
                $id_pais = $_POST["elegido"];

                $combog = pg_query('SELECT * FROM ptdi WHERE ptdi_depende=\'' . $id_pais . '\'');
                $salida .= "<option value=''>" . mb_convert_encoding('Seleccione', 'cp1252', 'UTF-8') . "</option>";
                while ($sql_p = pg_fetch_row($combog)) {
                    $salida .= "<option value='" . $sql_p[7] . "'>" . $sql_p[7] . " - " . $sql_p[2] . " - " . $sql_p[3] . "</option>";
                }
                echo $salida;
                //return $salida;
                break;

            case 'ptdi_3':
                $salida = "";
                $id_pais = $_POST["elegido"];

                $combog = pg_query('SELECT * FROM ptdi WHERE ptdi_depende=\'' . $id_pais . '\'');
                $salida .= "<option value=''>" . mb_convert_encoding('Seleccione', 'cp1252', 'UTF-8') . "</option>";
                while ($sql_p = pg_fetch_row($combog)) {
                    $salida .= "<option value='" . $sql_p[7] . "'>" . $sql_p[7] . " - " . $sql_p[2] . " - " . $sql_p[3] . "</option>";
                }
                echo $salida;
                //return $salida;
                break;

        }
        /**/
    }
    function login_exit()
    {
        $this->load->view('admin/login');
    }
    public function mes_texto($mes)
    {
        switch ($mes) {
            case '1':
                $texto = 'Enero';
                break;
            case '2':
                $texto = 'Febrero';
                break;
            case '3':
                $texto = 'Marzo';
                break;
            case '4':
                $texto = 'Abril';
                break;
            case '5':
                $texto = 'Mayo';
                break;
            case '6':
                $texto = 'Junio';
                break;
            case '7':
                $texto = 'Julio';
                break;
            case '8':
                $texto = 'Agosto';
                break;
            case '9':
                $texto = 'Septiembre';
                break;
            case '10':
                $texto = 'Octubre';
                break;
            case '11':
                $texto = 'Noviembre';
                break;
            case '12':
                $texto = 'Diciembre';
                break;
            default:
                $texto = 'Sin Mes asignado';
                break;
        }
        return $texto;
    }

    function validate_invitado()
    {
        $this->load->model('Users_model');
        if(TRUE){
            $gestion = $this->Users_model->obtener_gestion();//obtiene la gestion en la cual esta configurada el sistema
            $entidad = $this->Users_model->get_entidad($gestion[0]['ide']);//retorna VECTOR/OBJETO donde estan los datos de configuracion de la entidad
            $entidad = $entidad->conf_nombre_entidad;//retorna Centro de Investigación Agrícola Tropical
            $data = array(
                'user_name' => "CONTROL SOCIAL",//CONTROL SOCIAL
                'usuario' => "",//vacio
                'fun_id' => 430,//IDENTIFICADOR DEL USUARIO C.CONTROL "EL CONTROL SOCIAL"
                'uni_id' => "",//vacio
                'unidad' => "", //vacio
                'rol_id' => 1, //1
                'gestion' => $this->input->post('gestion'),
                'mes' => $this->input->post('mes'),
                'name' => 'SIPSyE V1.1',
                'sistema' => 'Sistema Integrado de Planificación, Seguimiento y Evaluación - SIPSyE V1.1',
                'sistema_pie' => '&copy; SIPSyE V1.1 - Centro de Investigación Agrícola Tropical',
                'desc_mes' => $this->mes_texto($gestion[0]['conf_mes']),
				'entidad' => $entidad,
                'logged' => true
            );
            $this->session->set_userdata($data);
			
            redirect('admin/control_social');
        }else{
            $this->session->sess_destroy();
            $this->session->set_flashdata('error', 'Datos Incorrectos Intentelo Otra Vez');
            redirect('default_controller', 'refresh');
        }
    }
    public function verificar_menu()
    {
        $url = $this->input->post('url');
        $parcial = substr($url, 0, 19);
        if( $parcial == 'index.php/admin/dm/' ) {
            $boolean = true;
        } else {
            if ($this->session->userdata('rol_id') == 1) {
                $boolean = true;
            } else {
                $verificar = $this->Users_model->verificar_menu($url);
                if($verificar) {
                    $boolean = $this->Users_model->verificar_menu_1($url);
                } else{
                    $boolean = true;
                }
            }
        }
        $data = array(
            'boolean' => $boolean,
            'url' => $url,
            'fun_id' => $this->session->userdata('fun_id')
        );
        echo json_encode($data);
    }
}
