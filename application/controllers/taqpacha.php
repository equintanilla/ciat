<?php

class Taqpacha extends CI_Controller
{
	public $rol = array('1' => '3','2' => '4','3' => '5');
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Users_model', '', true);
        $this->load->model('reportes/taqpacha/model_taqpacha','model_taqpacha');
        $this->load->model('menu_modelo');
        $this->load->model('programacion/model_faseetapa');
        $this->load->model('programacion/model_proyecto');
    }

    public function index()
    {
        if($this->session->userdata('logged')){

		  $data['gestion'] = $this->session->userdata('gestion');
		  $data['mes_id'] = $this->session->userdata('mes');
		  
		  $data['sectores'] = $this->model_taqpacha->get_sectores();
		  $data['regiones'] = $this->model_taqpacha->get_regiones();
		  $data['provincias'] = $this->model_taqpacha->get_provincias();
		  $data['municipios'] = $this->model_taqpacha->get_municipios();

		  $enlaces=$this->menu_modelo->get_Modulos(7);
		  $data['enlaces'] = $enlaces;

		  for($i=0;$i<count($enlaces);$i++)
		  {
			$subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
		  }
		  $data['subenlaces'] = $subenlaces;
		  $this->load->view('reportes/taqpacha/lista_reportes', $data);
		}
		else{
		  redirect('admin/dashboard');
		} 
    }

	//1. PRESUPUESTO DE INVERSIÓN PUBLICA GESTIÓN X
    public function pip()
    {
        if($this->session->userdata('logged')){

		  $data['gestion'] = $this->session->userdata('gestion');
		  $data['mes_id'] = $this->session->userdata('mes');
		  $data['reporte1'] = $this->model_taqpacha->get_reporte1($this->session->userdata('gestion'));
		  
		  $enlaces=$this->menu_modelo->get_Modulos(7);
		  $data['enlaces'] = $enlaces;

		  for($i=0;$i<count($enlaces);$i++)
		  {
			$subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
		  }
		  $data['subenlaces'] = $subenlaces;
		  $this->load->view('reportes/taqpacha/reporte1', $data);
		}
		else{
		  redirect('admin/dashboard');
		} 
    }

	//2. PRESUPUESTO PROGRAMAS NO RECURRENTES GESTIÓN X
    public function ppnr()
    {
        if($this->session->userdata('logged')){
		  $data['gestion'] = $this->session->userdata('gestion');
		  $data['mes_id'] = $this->session->userdata('mes');
		  $data['reporte1'] = $this->model_taqpacha->get_reporte2($this->session->userdata('gestion'));
		  
		  $enlaces=$this->menu_modelo->get_Modulos(7);
		  $data['enlaces'] = $enlaces;

		  for($i=0;$i<count($enlaces);$i++)
		  {
			$subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
		  }
		  $data['subenlaces'] = $subenlaces;
		  $this->load->view('reportes/taqpacha/reporte2', $data);
		}
		else{
		  redirect('admin/dashboard');
		} 
    }

	//3. ESTADO DE PROYECTOS DE INVERSION PUBLICA GESTIÓN X
    public function epip()
    {
        if($this->session->userdata('logged')){
		  $data['gestion'] = $this->session->userdata('gestion');
		  $data['mes_id'] = $this->session->userdata('mes');
		  $data['reporte1'] = $this->model_taqpacha->get_reporte3($this->session->userdata('gestion'));
		  
		  $enlaces=$this->menu_modelo->get_Modulos(7);
		  $data['enlaces'] = $enlaces;

		  for($i=0;$i<count($enlaces);$i++)
		  {
			$subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
		  }
		  $data['subenlaces'] = $subenlaces;
		  $this->load->view('reportes/taqpacha/reporte3', $data);
		}
		else{
		  redirect('admin/dashboard');
		} 
    }

	//3. ESTADO DE PROGRAMAS NO RECURRENTES GESTIÓN X
    public function eppnr()
    {
        if($this->session->userdata('logged')){
		  $data['gestion'] = $this->session->userdata('gestion');
		  $data['mes_id'] = $this->session->userdata('mes');
		  $data['reporte1'] = $this->model_taqpacha->get_reporte4($this->session->userdata('gestion'));
		  
		  $enlaces=$this->menu_modelo->get_Modulos(7);
		  $data['enlaces'] = $enlaces;

		  for($i=0;$i<count($enlaces);$i++)
		  {
			$subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
		  }
		  $data['subenlaces'] = $subenlaces;
		  $this->load->view('reportes/taqpacha/reporte4', $data);
		}
		else{
		  redirect('admin/dashboard');
		} 
    }
	
	
	//5. INVERSION PUBLICA ANUAL POR SECTOR ECONOMICO
    public function ipse($sec_id = -1)
    {
        if($this->session->userdata('logged')){

		  $data['gestion'] = $this->session->userdata('gestion');
		  $data['mes_id'] = $this->session->userdata('mes');
		  $data['reporte1'] = $this->model_taqpacha->get_reporte5($this->session->userdata('gestion'), $sec_id);
		  
		  $enlaces=$this->menu_modelo->get_Modulos(7);
		  $data['enlaces'] = $enlaces;

		  for($i=0;$i<count($enlaces);$i++)
		  {
			$subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
		  }
		  $data['subenlaces'] = $subenlaces;
		  $this->load->view('reportes/taqpacha/reporte5', $data);
		}
		else{
		  redirect('admin/dashboard');
		} 
    }

	//6. INVERSION ANUAL PROGRAMAS NO RECURRENTES POR SECTOR ECONOMICO
    public function ippnr($sec_id = -1)
    {
        if($this->session->userdata('logged')){

		  $data['gestion'] = $this->session->userdata('gestion');
		  $data['mes_id'] = $this->session->userdata('mes');
		  $data['reporte1'] = $this->model_taqpacha->get_reporte6($this->session->userdata('gestion'), $sec_id);
		  
		  $enlaces=$this->menu_modelo->get_Modulos(7);
		  $data['enlaces'] = $enlaces;

		  for($i=0;$i<count($enlaces);$i++)
		  {
			$subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
		  }
		  $data['subenlaces'] = $subenlaces;
		  $this->load->view('reportes/taqpacha/reporte6', $data);
		}
		else{
		  redirect('admin/dashboard');
		} 
    }	
	
	//7. INVERSION PUBLICA ANUAL POR REGION
    public function ipr($reg_id = -1)
    {
        if($this->session->userdata('logged')){

		  $data['gestion'] = $this->session->userdata('gestion');
		  $data['mes_id'] = $this->session->userdata('mes');
		  $data['reporte1'] = $this->model_taqpacha->get_reporte7($this->session->userdata('gestion'), $reg_id);
		  
		  $enlaces=$this->menu_modelo->get_Modulos(7);
		  $data['enlaces'] = $enlaces;

		  for($i=0;$i<count($enlaces);$i++)
		  {
			$subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
		  }
		  $data['subenlaces'] = $subenlaces;
		  $this->load->view('reportes/taqpacha/reporte7', $data);
		}
		else{
		  redirect('admin/dashboard');
		} 
    }
    public function ipr_detalle($reg_id)
    {
        if($this->session->userdata('logged')){

            $data['gestion'] = $this->session->userdata('gestion');
            $data['mes_id'] = $this->session->userdata('mes');
            $data['reporte1'] = $this->model_taqpacha->get_reporte7_detalle($this->session->userdata('gestion'), $reg_id);
            $titulo='INVERSIÓN PUBLICA GESTIÓN '.$this->session->userdata('gestion').' REGIÓN : '.$data['reporte1'][0]['reg_region'];
            $data['titulo']=$titulo;
            $enlaces=$this->menu_modelo->get_Modulos(7);
            $data['enlaces'] = $enlaces;

            for($i=0;$i<count($enlaces);$i++)
            {
                $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
            }
            $data['subenlaces'] = $subenlaces;
            $this->load->view('reportes/taqpacha/reporte9_detalle', $data);
        }
        else{
            redirect('admin/dashboard');
        }
    }
	//8. INVERSION PROGRAMAS NO RECURRENTES POR REGION
    public function pnrr($reg_id = -1)
    {
        if($this->session->userdata('logged')){

		  $data['gestion'] = $this->session->userdata('gestion');
		  $data['mes_id'] = $this->session->userdata('mes');
		  $data['reporte1'] = $this->model_taqpacha->get_reporte8($this->session->userdata('gestion'), $reg_id);
		  
		  $enlaces=$this->menu_modelo->get_Modulos(7);
		  $data['enlaces'] = $enlaces;

		  for($i=0;$i<count($enlaces);$i++)
		  {
			$subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
		  }
		  $data['subenlaces'] = $subenlaces;
		  $this->load->view('reportes/taqpacha/reporte8', $data);
		}
		else{
		  redirect('admin/dashboard');
		} 
    }
    public function pnrr_detalle($reg_id)
    {
        if($this->session->userdata('logged')){

            $data['gestion'] = $this->session->userdata('gestion');
            $data['mes_id'] = $this->session->userdata('mes');
            $data['reporte1'] = $this->model_taqpacha->get_reporte8_detalle($this->session->userdata('gestion'), $reg_id);
            $titulo='PROGRAMAS NO RECURRENTES '.$this->session->userdata('gestion').' REGIÓN : '.$data['reporte1'][0]['reg_region'];
            $data['titulo']=$titulo;
            $enlaces=$this->menu_modelo->get_Modulos(7);
            $data['enlaces'] = $enlaces;

            for($i=0;$i<count($enlaces);$i++)
            {
                $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
            }
            $data['subenlaces'] = $subenlaces;
            $this->load->view('reportes/taqpacha/reporte9_detalle', $data);
        }
        else{
            redirect('admin/dashboard');
        }
    }
	
	//9. INVERSION PUBLICA ANUAL POR PROVINCIA
    public function ipp($prov_id = -1)
    {
        if($this->session->userdata('logged')){

		  $data['gestion'] = $this->session->userdata('gestion');
		  $data['mes_id'] = $this->session->userdata('mes');
		  $data['reporte1'] = $this->model_taqpacha->get_reporte9($this->session->userdata('gestion'), $prov_id);
		  
		  $enlaces=$this->menu_modelo->get_Modulos(7);
		  $data['enlaces'] = $enlaces;

		  for($i=0;$i<count($enlaces);$i++)
		  {
			$subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
		  }
		  $data['subenlaces'] = $subenlaces;
		  $this->load->view('reportes/taqpacha/reporte9', $data);
		}
		else{
		  redirect('admin/dashboard');
		} 
    }

    public function ipp_detalle($prov_id)
    {
        if($this->session->userdata('logged')){

            $data['gestion'] = $this->session->userdata('gestion');
            $data['mes_id'] = $this->session->userdata('mes');
            $data['reporte1'] = $this->model_taqpacha->get_reporte9_detalle($this->session->userdata('gestion'), $prov_id);
            $titulo='INVERSIÓN PUBLICA GESTION '.$this->session->userdata('gestion').' PROVINCIA : '.$data['reporte1'][0]['prov_provincia'];
            $data['titulo']=$titulo;
            $enlaces=$this->menu_modelo->get_Modulos(7);
            $data['enlaces'] = $enlaces;

            for($i=0;$i<count($enlaces);$i++)
            {
                $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
            }
            $data['subenlaces'] = $subenlaces;
            $this->load->view('reportes/taqpacha/reporte9_detalle', $data);
        }
        else{
            redirect('admin/dashboard');
        }
    }
	//10. INVERSION ANUAL EN PROGRAMAS NO RECURRENTES POR PROVINCIA
    public function pnrp($prov_id = -1)
    {
        if($this->session->userdata('logged')){
		  $data['gestion'] = $this->session->userdata('gestion');
		  $data['mes_id'] = $this->session->userdata('mes');
		  $data['reporte1'] = $this->model_taqpacha->get_reporte10($this->session->userdata('gestion'), $prov_id);
		  
		  $enlaces=$this->menu_modelo->get_Modulos(7);
		  $data['enlaces'] = $enlaces;

		  for($i=0;$i<count($enlaces);$i++)
		  {
			$subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
		  }
		  $data['subenlaces'] = $subenlaces;
		  $this->load->view('reportes/taqpacha/reporte10', $data);
		}
		else{
		  redirect('admin/dashboard');
		} 
    }
    public function pnrp_detalle($prov_id)
    {
        if($this->session->userdata('logged')){

            $data['gestion'] = $this->session->userdata('gestion');
            $data['mes_id'] = $this->session->userdata('mes');
            $data['reporte1'] = $this->model_taqpacha->get_reporte10_detalle($this->session->userdata('gestion'), $prov_id);
            $titulo='PROGRAMAS NO RECURRENTES GESTIÓN '.$this->session->userdata('gestion').' PROVINCIA : '.$data['reporte1'][0]['prov_provincia'];
            $data['titulo']=$titulo;
            $enlaces=$this->menu_modelo->get_Modulos(7);
            $data['enlaces'] = $enlaces;

            for($i=0;$i<count($enlaces);$i++)
            {
                $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
            }
            $data['subenlaces'] = $subenlaces;
            $this->load->view('reportes/taqpacha/reporte9_detalle', $data);
        }
        else{
            redirect('admin/dashboard');
        }
    }
	//11. INVERSION PUBLICA ANUAL POR MUNICIPIO
    public function ipm($mun_id = -1)
    {
        if($this->session->userdata('logged')){
		  $data['gestion'] = $this->session->userdata('gestion');
		  $data['mes_id'] = $this->session->userdata('mes');
		  $data['reporte1'] = $this->model_taqpacha->get_reporte11($this->session->userdata('gestion'), $mun_id);
		  
		  $enlaces=$this->menu_modelo->get_Modulos(7);
		  $data['enlaces'] = $enlaces;

		  for($i=0;$i<count($enlaces);$i++)
		  {
			$subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
		  }
		  $data['subenlaces'] = $subenlaces;
		  $this->load->view('reportes/taqpacha/reporte11', $data);
		}
		else{
		  redirect('admin/dashboard');
		} 
    }
    public function ipm_detalle($mun_id)
    {
        if($this->session->userdata('logged')){

            $data['gestion'] = $this->session->userdata('gestion');
            $data['mes_id'] = $this->session->userdata('mes');
            $data['reporte1'] = $this->model_taqpacha->get_reporte11_detalle($this->session->userdata('gestion'), $mun_id);
            $titulo='INVERSIÓN PUBLICA GESTIÓN '.$this->session->userdata('gestion').' MUNICIPIO : '.$data['reporte1'][0]['muni_municipio'];
            $data['titulo']=$titulo;
            $enlaces=$this->menu_modelo->get_Modulos(7);
            $data['enlaces'] = $enlaces;

            for($i=0;$i<count($enlaces);$i++)
            {
                $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
            }
            $data['subenlaces'] = $subenlaces;
            $this->load->view('reportes/taqpacha/reporte9_detalle', $data);
        }
        else{
            redirect('admin/dashboard');
        }
    }
	//12. INVERSION ANUAL EN PROGRAMAS NO RECURRENTES POR MUNICIPIO
    public function pnrm($mun_id = -1)
    {
        if($this->session->userdata('logged')){
		  $data['gestion'] = $this->session->userdata('gestion');
		  $data['mes_id'] = $this->session->userdata('mes');
		  $data['reporte1'] = $this->model_taqpacha->get_reporte12($this->session->userdata('gestion'), $mun_id);
		  
		  $enlaces=$this->menu_modelo->get_Modulos(7);
		  $data['enlaces'] = $enlaces;

		  for($i=0;$i<count($enlaces);$i++)
		  {
			$subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
		  }
		  $data['subenlaces'] = $subenlaces;
		  $this->load->view('reportes/taqpacha/reporte12', $data);
		}
		else{
		  redirect('admin/dashboard');
		} 
    }
	public function pnrm_detalle($mun_id){
        if($this->session->userdata('logged')){

            $data['gestion'] = $this->session->userdata('gestion');
            $data['mes_id'] = $this->session->userdata('mes');
            $data['reporte1'] = $this->model_taqpacha->get_reporte12_detalle($this->session->userdata('gestion'), $mun_id);
            $titulo='PROGRAMAS NO RECURRENTES GESTIÓN '.$this->session->userdata('gestion').' MUNICIPIO : '.$data['reporte1'][0]['muni_municipio'];
            $data['titulo']=$titulo;
            $enlaces=$this->menu_modelo->get_Modulos(7);
            $data['enlaces'] = $enlaces;

            for($i=0;$i<count($enlaces);$i++)
            {
                $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
            }
            $data['subenlaces'] = $subenlaces;
            $this->load->view('reportes/taqpacha/reporte9_detalle', $data);
        }
        else{
            redirect('admin/dashboard');
        }
    }

    /*------------------------------------DATOS DE CARGADO DE DATOS---------------------------*/
    public function cargado()
    {
        if($this->session->userdata('logged')){
            $enlaces=$this->menu_modelo->get_Modulos(7);
            $data['enlaces'] = $enlaces;
            $data['lista_aper_padres'] = $this->model_proyecto->list_prog();//lista de aperturas padres
            for($i=0;$i<count($enlaces);$i++)
            {
                $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
            }
            $data['subenlaces'] = $subenlaces;
            //load the view
            $data['mod']=1;
            $data['programas']=$this->list_programas_xxx($data['mod']);
            $this->load->view('reportes/taqpacha/avance_cargado_datos', $data);
        }
        else{
            redirect('admin/dashboard');
        }
    }
	
    public function list_programas_xxx($mod)
    {
        $lista_aper_padres = $this->model_proyecto->list_prog();//lista de aperturas padres
        $tabla ='';
        foreach($lista_aper_padres  as $rowa)
        {
            $proyectos=$this->model_taqpacha->list_proyectos($rowa['aper_programa'],1,1,$this->session->userdata('gestion'));
            if(count($proyectos)!=0)
            {
                $tabla .='<tr bgcolor="#99DDF0">';
//                $tabla .='<td></td>';
                //          $tabla .='<td></td>';
                $tabla .='<td><center>' .$rowa['aper_programa'].''.$rowa['aper_proyecto'].''.$rowa['aper_actividad'].'</center></td>';
                $tabla .='<td>'.$rowa['aper_descripcion'].'</td>';
                $tabla .='<td>'.$rowa['aper_sisin'].'</td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='</tr>';
                foreach($proyectos  as $row)
                {
                    if($row['t_obs']==2){$obs='POA';}
                    elseif($row['t_obs']==3){$obs='FINANCIERO';}
                    elseif($row['t_obs']==4){$obs='TECNICO OPERATIVO';}
                    if($row['tp_id']!=1){$color='#D7FCF9';}else{$color='#ffffff';}
                    $fase = $this->model_faseetapa->get_id_fase($row['proy_id']);
                    $tabla .= '<tr bgcolor='.$color.'>';
//              $tabla .= '<td>XXX';
                    //            $tabla .= '</td>';
                    $tabla .= '<td class="text-center">';
                    if($row['aper_proyecto']!=''){
                        $tabla .= ''.$row['aper_programa'].''.$row['aper_proyecto'].''.$row['aper_actividad'].'<br><br>';
                    }
                    else{
                        $tabla .=  '<button class="btn btn-danger" title="RE ASIGNAR CATEGORIA PROGRAMATICA PARA LA GESTION '.$this->session->userdata('gestion').'"><i class="glyphicon glyphicon-fire"></i>&nbsp;ASIGNAR APERTURA<br>PROGRAM&Aacute;TICA<br>'.$this->session->userdata('gestion').'</button><br><br>';
                    }
                    if($row['t_obs']!=0 & $row['t_obs']!=1){
                        $tabla .=  '<a data-toggle="modal" data-target="#modal_mod_aper2" class="btn btn-warning mod_aper2" name="'.$row['proy_observacion'].'" id="'.$row['t_obs'].'"><i class="glyphicon glyphicon-info-sign"></i> OBSERVADO<br>'.$obs.'</a><br><br>';
                    }

                    $tabla .= '</td>';
                    $tabla .= '<td>'.$row['proy_nombre'].'</td>';
                    $tabla .= '<td>'.$row['tp_tipo'].'</td>';
                    $tabla .= '<td>'.$row['proy_sisin'].'</td>';
                    $tabla .= '<td>'.$row['fun_nombre'].' '.$row['fun_paterno'].' '.$row['fun_materno'].'</td>';
                    $tabla .= '<td>'.$row['ue'].'</td>';
                    $tabla .= '<td>'.$row['ur'].'</td>';
                    $tabla .= '<td class="text-center">'.round($row['_datos_grales'],2).'</td>';
                    $tabla .= '<td class="text-center">'.round($row['_prog_fis'],2).'</td>';
                    $tabla .= '<td class="text-center">'.round($row['_prog_fin'],2).'</td>';
                    $tabla .= '<td class="text-center">'.round($row['_ejec_fis'],2).'</td>';
                    $tabla .= '<td class="text-center">'.round($row['_ejec_fin'],2).'</td>';
                    $pr=$this->model_proyecto->prioridad();
                    $tabla .='<td>';
                    $prov=$this->model_proyecto->proy_prov($row['proy_id']);
                    if(count($prov)!=0){
                        foreach($prov as $locali)
                        {
                            $tabla .='<li><b>'.strtoupper($locali['prov_provincia']).' </b>';
                            $muni=$this->model_proyecto->provincia_municipio($row['proy_id'],$locali['prov_id']);
                            $tabla .='(';
                            foreach($muni as $muni)
                            {
                                $tabla .=''.$muni['muni_municipio'].',';
                            }
                            $tabla .=')';
                            $tabla .='</li>';
                        }
                    }

                    $tabla .='</td>';
                    $tabla .= '</tr>';
                }
            }
        }
        return $tabla;
    }

	
    function rolfun($rol)
    {
      $valor=false;
      for ($i=1; $i <=count($rol) ; $i++) { 
        $data = $this->Users_model->get_datos_usuario_roles($this->session->userdata('fun_id'),$rol[$i]);
        if(count($data)!=0){
          $valor=true;
          break;
        }
      }
      return $valor;
    }

	
}