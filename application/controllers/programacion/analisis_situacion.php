<?php
class Analisis_situacion extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        if($this->session->userdata('fun_id')!=null){
        $this->load->model('analisis_situacion/model_analisis_situacion');
        $this->load->model('mantenimiento/mpoa');
        $this->load->model('programacion/marco_estrategico/mobjetivos');
        $this->load->library('menu');
        $this->menu->const_menu(1);
        }else{
            redirect('/','refresh');
        }
    }
    function construir_vista($ruta, $data)
    {
        //Llamar Menus
        $menu['enlaces'] = $this->menu->get_enlaces();
        $menu['subenlaces'] = $this->menu->get_sub_enlaces();
        $menu['titulo'] = 'PROGRAMACIÓN';
        //Incluir vistas y Rutas
        $this->load->view('includes/header');
        $this->load->view('includes/menu_lateral', $menu);
        $this->load->view($ruta, $data);
        $this->load->view('includes/footer');
    }

    function index()
    {
        $data['lista_poa'] = $this->mpoa->lista_poa();
        $ruta = 'programacion/prog_poa/red_objetivos/vlista_analisis_situacion';
        $this->construir_vista($ruta, $data);
    }

    public function ver_analisis_situacion($poa_id)
    {
        $data['tipo_foda'] = $this->model_analisis_situacion->get_tipo_foda();
        $data['lista_foda'] = $this->model_analisis_situacion->get_foda_por_poa_id($poa_id);
        $data['poa_id'] = $poa_id;
        $ruta = 'programacion/prog_poa/red_objetivos/analisis_situacion';
        $this->construir_vista($ruta, $data);
    }

    public function agrega_foda()
    {
        $post = $this->input->post();
        $data = array(
            'poa_id' => $post['poa_id'],
            'foda_variables' => $post['descripcion'],
            'foda_incidencia' => $post['incidencia'],
            'tfoda_id' => $post['tipo_foda']
        );
        $this->model_analisis_situacion->insertar_foda($data);
        $this->ver_analisis_situacion($data['poa_id']);
    }
    public function get_foda_editar()
    {
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $post = $this->input->post();
            $id = $post['foda_id'];
            $dato = $this->model_analisis_situacion->get_foda_para_editar($id);
            $dato = $dato->row();
            $result = array(
                'foda_id' => $dato->foda_id,
                "poa_id" => $dato->poa_id,
                "foda_variables" => $dato->foda_variables,
                "foda_incidencia" => $dato->foda_incidencia,
                "tfoda_id" => $dato->tfoda_id
            );
            echo json_encode($result);
        } else {
            show_404();
        }
    }

    public function editar_foda()
    {
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $post = $this->input->post();
            $data = array(
                'foda_variables'=> $post['foda_variables'],
                'foda_incidencia'=> $post['foda_incidencia'],
                'tfoda_id'=> $post['tfoda_id']
            );
            $foda_id = $post['foda_id'];
            $peticion = $this->model_analisis_situacion->edita_foda($data,$foda_id);
            if ($peticion) {
                $result = array(
                    'respuesta' => 'correcto'
                );
            } else {
                $result = array(
                    'respuesta' => 'error'
                );
            }
            echo json_encode($result);
        } else {
            echo 'DATOS ERRONEOS';
        }
    }
    public function eliminar_foda()
    {
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $post = $this->input->post();
            $foda_id = $post['foda_id'];
            $peticion = $this->model_analisis_situacion->elimina_foda($foda_id);
            if ($peticion) {
                $result = array(
                    'respuesta' => 'correcto',
                    'foda_id' => $foda_id
                );
            } else {
                $result = array(
                    'respuesta' => 'error'
                );
            }
            echo json_encode($result);
        } else {
            echo 'DATOS ERRONEOS';
        }
    }
}