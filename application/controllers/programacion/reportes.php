<?php
define("DOMPDF_ENABLE_REMOTE", true);
define("DOMPDF_TEMP_DIR", "/tmp");
class Reportes extends CI_Controller {
    public $rol = array('1' => '3','2' => '4');  
    public function __construct ()
    { 
        parent::__construct();
        if($this->session->userdata('fun_id')!=null){
            $this->load->model('Users_model','',true);
        if($this->rolfun($this->rol)){ 
            $this->load->library('pdf');
            $this->load->library('pdf2');
            $this->load->model('programacion/model_reporte');
            $this->load->model('reportes/model_objetivo');
            $this->load->model('programacion/model_proyecto');
            $this->load->model('programacion/model_producto');
            $this->load->model('programacion/model_faseetapa');
            $this->load->model('programacion/model_componente');
            $this->load->model('programacion/model_actividad');
            $this->load->model('ejecucion/model_ejecucion');
            $this->load->model('programacion/insumos/minsumos');
            $this->load->model('programacion/insumos/minsumos_partida');
            $this->load->model('programacion/insumos/mfinanciera');
            $this->load->model('menu_modelo');
            $this->gestion = $this->session->userData('gestion');
            }else{
                redirect('admin/dashboard');
            }
        }else{
            redirect('/','refresh');
        }
    }

/*===================================== REPORTES CONTRATOS =============================================*/
  public function cierre_proyecto($mod,$id_f,$id_p)
  { 
    if($mod!='' & $id_f!='' & $id_p!=''){
        $enlaces=$this->menu_modelo->get_Modulos_programacion(1);
        $data['enlaces'] = $enlaces;
        $data['nro_fase']= $this->model_faseetapa->nro_fase($id_p); /// nro de fases y etapas registrados
        $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p); 
        $data['id_f'] = $this->model_faseetapa->get_id_fase($id_p); //// recupera datos de la tabla fase activa
        $data['mod']=$mod;
        $data['componente'] = $this->model_componente->get_componente($data['id_f'][0]['id']);

        $titulo_proy=strtoupper($data['proyecto'][0]['tipo']);
        $data['titulo_proy'] = $titulo_proy;

        $this->load->view('admin/programacion/reportes/cierre/cierre_proyecto', $data);
    }
    else{
      redirect('admin/dashboard');
    }
  }
 /*===================================== IDENTIFICACION DEL PROYECTO=============================================*/
 public function reporte_proyecto($mod,$id_f,$id_p)
  { 
    if($mod!='' & $id_f!='' & $id_p!=''){
        $enlaces=$this->menu_modelo->get_Modulos_programacion(1);
        $data['enlaces'] = $enlaces;
        $data['nro_fase']= $this->model_faseetapa->nro_fase($id_p); /// nro de fases y etapas registrados
        $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p); 
        $data['id_f'] = $this->model_faseetapa->get_id_fase($id_p); //// recupera datos de la tabla fase activa
        $data['mod']=$mod;

        $titulo_proy=strtoupper($data['proyecto'][0]['tipo']);
        $data['titulo_proy'] = $titulo_proy;
        $this->load->view('admin/programacion/reportes/proyecto/identificacion_proyecto', $data);
    }
    else{
      redirect('admin/dashboard');
    }
  }
 /*==============================================================================================================*/

 /*===================================== REPORTES EJECUCION FISICA =============================================*/
 public function reporte_fisico($mod,$id_f,$id_p){ 
    if($mod!='' & $id_f!='' & $id_p!=''){
        $enlaces=$this->menu_modelo->get_Modulos_programacion(1);
        $data['enlaces'] = $enlaces;
        $data['nro_fase']= $this->model_faseetapa->nro_fase($id_p); /// nro de fases y etapas registrados
        $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p); 
        $data['id_f'] = $this->model_faseetapa->get_id_fase($id_p); //// recupera datos de la tabla fase activa
        $data['mod']=$mod;
        $data['componente'] = $this->model_componente->get_componente($data['id_f'][0]['id']);
        $a�os=$data['id_f'][0]['pfec_fecha_fin']-$data['id_f'][0]['pfec_fecha_inicio']+1;
        $data['gest']=$a�os;
        $titulo_proy=strtoupper($data['proyecto'][0]['tipo']);
        $data['titulo_proy'] = $titulo_proy;

        $this->load->view('admin/programacion/reportes/efisica/pe_fisica', $data);
    }
    else{
      redirect('admin/dashboard');
    }
  }
 /*==============================================================================================================*/
  /*===================================== REPORTES EJECUCION FINANCIERA =============================================*/
    public function reporte_financiero($mod,$id_f,$id_p){ 
    if($mod!='' & $id_f!='' & $id_p!=''){
        $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p); 
        if(count($data['proyecto'])!=0){
            $enlaces=$this->menu_modelo->get_Modulos_programacion(1);
            $data['enlaces'] = $enlaces;
            $data['nro_fase']= $this->model_faseetapa->nro_fase($id_p); /// nro de fases y etapas registrados
            
            $data['id_f'] = $this->model_faseetapa->get_id_fase($id_p); //// recupera datos de la tabla fase activa
            $data['mod']=$mod;
            $data['componente'] = $this->model_componente->get_componente($data['id_f'][0]['id']);
            $a�os=$data['id_f'][0]['pfec_fecha_fin']-$data['id_f'][0]['pfec_fecha_inicio']+1;
            $data['gest']=$a�os;
            $titulo_proy=strtoupper($data['proyecto'][0]['tipo']);
            $data['titulo_proy'] = $titulo_proy;

            $this->load->view('admin/programacion/reportes/efinanciera/pe_financiera', $data);
        }
        else{
            redirect('admin/dashboard');
        }
    }
    else{
      redirect('admin/dashboard');
    }
  }
    
    public function reporte_programacion_financiera($mod,$id_f,$id_p){
        $fase = $this->model_faseetapa->get_id_fase($id_p);

        if(count($fase)>0){
            if($fase[0]['pfec_ejecucion']==1){
                //Listar Productos
                $data['pfinanciero']=$this->prog_financiera_directo($mod,$id_f,$id_p);
            }else{
                //Delegado
                $data['pfinanciero']=$this->prog_financiera_delegado($mod,$id_f,$id_p);
            }
        }
        $this->load->view('admin/programacion/reportes/efinanciera/prog_fin', $data);
    }
    
    public function prog_financiera_delegado($mod,$id_f,$proy_id){
        $proyecto = $this->model_proyecto->get_id_proyecto($proy_id);
        $fase = $this->model_faseetapa->get_id_fase($proy_id);
        $tabla ='';
        $tabla .='
           <style>
            .table{font-size: 10px;
                  width: 100%;
                  max-width:1550px;;
            overflow-x: scroll;
            }
            th{
              padding: 1.4px;
              text-align: center;
              font-size: 10px;
            }
          </style>';
        $tabla .='
                <div class="rojo"></div>
                <div class="verde"></div>
                <table class="change_order_items" style="width:100%;" align="center">
                    <tr>
                        <td width=20%; align=center>
                            <img src="'.base_url().'assets/img/logo.jpg" width="80%"/>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata('entidad').'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$this->gestion.'<br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                            <b>FORMULARIO : </b> PROGRAMACI&Oacute;N Y EJECUCI&Oacute;N FINANCIERA - ANUAL<br>
                            <b>ACCI&Oacute;N OPERATIVA : </b> '.$proyecto[0]['proy_nombre'].'<br>
                            <b>APERTURA PROGRAMATICA : </b> '.$proyecto[0]['aper_programa'].' '.$proyecto[0]['aper_proyecto'].' '.$proyecto[0]['aper_actividad'].'<br>
                        </td>
                        <td width=30%; align=center>
                            <img src="'.base_url().'assets/img/escudo.jpg" alt="" width="40%">
                        </td>
                    </tr>
                </table><hr>';
                $data['fase'] = $this->model_faseetapa->get_id_fase($proy_id); //// DATOS DE LA FASE ACTIVA
                $componentes=$this->model_componente->componentes_id($data['fase'][0]['id']);  //// COMPONENTES DE LA FASE ACTIVA
        if($mod==1){
                foreach ($componentes as $row_com){
                    $lista_insumos = $this->minsumos_partida->lista_insumos_com($row_com['com_id']);
                    $tabla .='
                    <table class="change_order_items" border="1" style="width:100%;" align="center">
                        <tr class="modo1">
                            <th colspan=9 bgcolor="#504c49" style="width:2%;"><font color=#fff>COMPONENTE : '.$row_com['com_componente'].'</font></th>
                        </tr>
                        <tr bgcolor="#504c49">
                        <th><font color=#fff>PARTIDA</font></th>
                        <th><font color=#fff>DESCRIPCIÓN</font></th>
                        <th><font color=#fff>FF</font></th>
                                                <th><font color=#fff>OF</font></th>
                                                <th><font color=#fff>ET</font></th>
                                                <th><font color=#fff>P. INICIAL</font></th>
                                                <th><font color=#fff>MODIF.</font></th>
                                                <th><font color=#fff>P. VIGENTE</font></th>
		                                        <th><font color=#fff>PROGRAMACIÓN ANUAL </font></th></tr>';
                    $total_vigente=0;
                    foreach ($lista_insumos as $row) {
                        $pres_anual=$this->minsumos_partida->insumos_partida_pres_anual($row['insp_id'],$this->gestion);
                        $tabla .= '<tr>';
                        $tabla .= '<td>'.strtoupper($row['par_codigo']) .'</td>';
                        $tabla .= '<td>'.$row['par_nombre'].'</td>';
                        $tabla .= '<td>'.$row['ff_codigo'].' - '.$row['ff_descripcion'].'</td>';
                        $tabla .= '<td>'.$row['of_codigo'].' - '.$row['of_descripcion'].'</td>';
                        $tabla .= '<td>'.$row['et_codigo'].' - '.$row['et_descripcion'].'</td>';
                        $tabla .= '<td>'.number_format($pres_anual[0]['p_total'], 2, ',', '.').'</td>';
                        $tabla .= '<td>'.number_format($row['insp_modifi'], 2, ',', '.').'</td>';
                        $tabla .= '<td>'.number_format($pres_anual[0]['p_total'], 2, ',', '.').'</td>';
                        $tabla .= '<td>'.$this->generar_prog_anual($row['par_id'],$proy_id,$row['insp_id'],$pres_anual[0]['p_total']).'</td>';
                        $total_vigente=$total_vigente+$row['insp_actual'];
                        $tabla .= '</tr>';
                    }
                    $tabla.='</table>';
                }

                $tabla .='<hr>';
                $total=$this->suma_total_partidas($proy_id,$this->gestion);
                $tabla .='
                    <table class="change_order_items" style="width:100%;" align="center">
                        <tr class="modo1">
                            <td colspan=13 bgcolor="#504c49" style="width:2%;"><font color=#fff>GESTI&Oacute;N '.$this->gestion.'</font></td>
                        </tr>
                        <tr class="modo1">
                            <td bgcolor="#504c49" style="width:2%;"><font color=#fff>P/E</font></td>
                            <td bgcolor="#504c49" style="width:5%;"><font color=#fff>ENERO</font></td>
                            <td bgcolor="#504c49" style="width:5%;"><font color=#fff>FEBRERO</font></td>
                            <td bgcolor="#504c49" style="width:5%;"><font color=#fff>MARZO</font></td>
                            <td bgcolor="#504c49" style="width:5%;"><font color=#fff>ABRIL</font></td>
                            <td bgcolor="#504c49" style="width:5%;"><font color=#fff>MAYO</font></td>
                            <td bgcolor="#504c49" style="width:5%;"><font color=#fff>JUNIO</font></td>
                            <td bgcolor="#504c49" style="width:5%;"><font color=#fff>JULIO</font></td>
                            <td bgcolor="#504c49" style="width:5%;"><font color=#fff>AGOSTO</font></td>
                            <td bgcolor="#504c49" style="width:5%;"><font color=#fff>SEPTIEMBRE</font></td>
                            <td bgcolor="#504c49" style="width:5%;"><font color=#fff>OCTUBRE</font></td>
                            <td bgcolor="#504c49" style="width:5%;"><font color=#fff>NOVIEMBRE</font></td>
                            <td bgcolor="#504c49" style="width:5%;"><font color=#fff>DICIEMBRE</font></td>
                        </tr>
                        <tr class="modo1">
                            <td>P.</td>';
                            for ($i=1; $i <=12 ; $i++){ 
                               $tabla .='<td>'.number_format($total[2][$i], 2, ',', '.').'</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="modo1">
                            <td>P.A.</td>';
                            for ($i=1; $i <=12 ; $i++){ 
                               $tabla .='<td>'.number_format($total[3][$i], 2, ',', '.').'</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="modo1" bgcolor="#c5bcf3">
                            <td>%P.A.</td>';
                            for ($i=1; $i <=12 ; $i++){ 
                               $tabla .='<td>'.$total[4][$i].'%</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="modo1">
                            <td>E</td>';
                            for ($i=1; $i <=12 ; $i++) 
                            { 
                               $tabla .='<td>'.number_format($total[5][$i], 2, ',', '.').'</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="modo1">
                            <td>E.A.</td>';
                            for ($i=1; $i <=12 ; $i++){ 
                               $tabla .='<td>'.number_format($total[6][$i], 2, ',', '.').'</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="modo1" bgcolor="#c5bcf3">
                            <td>%E.A.</td>';
                            for ($i=1; $i <=12 ; $i++){ 
                               $tabla .='<td>'.$total[7][$i].'%</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="modo1" bgcolor="#bbf3cf">
                            <td>EFICACIA</td>';
                            for ($i=1; $i <=12 ; $i++){ 
                               $tabla .='<td>'.$total[8][$i].'%</td>'; 
                            }
                        $tabla .='
                        </tr>';
                    $tabla .='</table>';
        }
        elseif($mod==4){
            foreach ($componentes as $row_com){
                $lista_insumos = $this->minsumos_partida->lista_insumos_com($row_com['com_id']);
                $tabla .='
                    <table class="change_order_items" border="1" style="width:100%;" align="center">
                        <tr class="modo1">
                            <th colspan=9 bgcolor="#504c49" style="width:2%;"><font color=#fff>COMPONENTE : '.$row_com['com_componente'].'</font></th>
                        </tr>
                        <tr bgcolor="#504c49">
                        <th><font color=#fff>PARTIDA</font></th>
                        <th><font color=#fff>DESCRIPCIÓN</font></th>
                        <th><font color=#fff>FF</font></th>
                        <th><font color=#fff>OF</font></th>
                        <th><font color=#fff>ET</font></th>
                        <th><font color=#fff>P. INICIAL</font></th>
                        <th><font color=#fff>MODIF.</font></th>
                        <th><font color=#fff>P. VIGENTE</font></th>
		                <th style="width:40%;"><font color=#fff>PROGRAMACIÓN ANUAL </font></th></tr>';
                $total_vigente=0;
                foreach ($lista_insumos as $row) {
                    $pres_anual=$this->minsumos_partida->insumos_partida_pres_total($row['insp_id']);
                    $tabla .= '<tr>';
                    $tabla .= '<td>'.strtoupper($row['par_codigo']) .'</td>';
                    $tabla .= '<td>'.$row['par_nombre'].'</td>';
                    $tabla .= '<td>'.$row['ff_codigo'].' - '.$row['ff_descripcion'].'</td>';
                    $tabla .= '<td>'.$row['of_codigo'].' - '.$row['of_descripcion'].'</td>';
                    $tabla .= '<td>'.$row['et_codigo'].' - '.$row['et_descripcion'].'</td>';
                    $tabla .= '<td>'.number_format($pres_anual[0]['p_total'], 2, ',', '.').'</td>';
                    $tabla .= '<td>'.number_format($row['insp_modifi'], 2, ',', '.').'</td>';
                    $tabla .= '<td>'.number_format($pres_anual[0]['p_total'], 2, ',', '.').'</td>';
                    $tabla .= '<td>'.$this->generar_prog_multi_anual($row['par_id'],$proy_id,$row['insp_id'],$pres_anual[0]['p_total']).'</td>';
                    $total_vigente=$total_vigente+$row['insp_actual'];
                    $tabla .= '</tr>';
                }
                $tabla.='</table>';
            }
            /**Tabla resumen***/
            $fase = $this->model_faseetapa->get_id_fase($proy_id);
            $anios=$fase[0]['pfec_fecha_fin']-$fase[0]['pfec_fecha_inicio']+1;
            $gestion=$fase[0]['pfec_fecha_inicio'];
            $gestion_tbl_aux=$gestion;
            $ges=$gestion;
            $matriz_gest[1][$gestion-1]='P.';
            $matriz_gest[2][$gestion-1]='P.A.';
            $matriz_gest[3][$gestion-1]='%P.A.';
            $matriz_gest[4][$gestion-1]='E';
            $matriz_gest[5][$gestion-1]='E.A.';
            $matriz_gest[6][$gestion-1]='%E.A.';
            $matriz_gest[7][$gestion-1]='EFICACIA';

            $tabla.='<table class="change_order_items" border="1" style="width: 100%">
                <thead>                        
                <tr bgcolor="#504c49">
                    <td style="width:1%;"><font color="#ffffff">P/E</font></td>';
            $gran_total=0;

            for($k=1;$k<=$anios;$k++) {
                $tabla.='<td style = "width:1%;" ><font color = "#ffffff" ><center >'.$gestion.'</center ></font ></td >';
                $pprog = $this->mfinanciera->vlista_partidas_total_proy_programado_global($proy_id,$gestion,$fase[0]['pfec_ejecucion']);
                if(count($pprog)!=0)
                    $gran_total=$gran_total+$pprog[0]['total'];
                $gestion++;
            }
            $tabla .='</tr></thead>';
            $prog_acum=0;
            $porc_acom=0;
            $ejec_acum=0;
            $porc_e_acum=0;
            for($k=1;$k<=$anios;$k++){
                $total=$this->suma_total_partidas_multi($proy_id,$ges,$gran_total);
                $matriz_gest[1][$ges]=number_format($total[2][1], 2, ',', '.');
                $prog_acum=$prog_acum+$total[3][1];
                $matriz_gest[2][$ges]=number_format($prog_acum, 2, ',', '.');
                $porc_acom=$porc_acom+$total[4][1];
                $matriz_gest[3][$ges]=number_format($porc_acom, 2, ',', '.');
                $matriz_gest[4][$ges]=number_format($total[5][1], 2, ',', '.');
                $ejec_acum=$ejec_acum+$total[6][1];
                $matriz_gest[5][$ges]=number_format($ejec_acum, 2, ',', '.');
                $porc_e_acum=$porc_e_acum+$total[7][1];
                $matriz_gest[6][$ges]=number_format($porc_e_acum, 2, ',', '.');
                $matriz_gest[7][$ges]=number_format($total[8][1], 2, ',', '.');
                $ges++;
            }
            $gestion_tbl=$gestion_tbl_aux;
            $columna=$gestion_tbl-1;
            $h=1;
            $color='';
            for($j=1;$j<=7;$j++){
                if($j==3 || $j==6){$color=' bgcolor="#c5bcf3"';}elseif($j==7){$color='bgcolor="#bbf3cf"';}else{$color='';}
                $tabla .= '<tr class="modo1"'.$color.' >
                        <td>'.$matriz_gest[$j][$columna].'</td>';
                for($k=1;$k<=$anios;$k++){
                    $tabla .= '<td>'.$matriz_gest[$h][$gestion_tbl].'</td>';
                    $gestion_tbl++;
                }
                $h++;
                $tabla .= '</tr>';
                $gestion_tbl=$gestion_tbl_aux;
            }
            $tabla .='</table>';
        }
        return $tabla;
    }
    public function prog_financiera_directo($mod,$id_f,$proy_id){
        $proyecto = $this->model_proyecto->get_id_proyecto($proy_id);
        $fase = $this->model_faseetapa->get_id_fase($proy_id);
        $tabla ='';
        $tabla .='
           <style>
            .table{font-size: 10px;
                  width: 100%;
                  max-width:1550px;;
            overflow-x: scroll;
            }
            th{
              padding: 1.4px;
              text-align: center;
              font-size: 10px;
            }
          </style>';
        $tabla .='
                <div class="rojo"></div>
                <div class="verde"></div>
                <table class="change_order_items" style="width:100%;" align="center">
                    <tr>
                        <td width=20%; align=center>
                            <img src="'.base_url().'assets/img/logo.jpg" width="80%"/>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata('entidad').'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$this->gestion.'<br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                            <b>FORMULARIO : </b> PROGRAMACI&Oacute;N Y EJECUCI&Oacute;N FINANCIERA - ANUAL<br>
                            <b>ACCI&Oacute;N OPERATIVA : </b> '.$proyecto[0]['proy_nombre'].'<br>
                            <b>APERTURA PROGRAMATICA : </b> '.$proyecto[0]['aper_programa'].' '.$proyecto[0]['aper_proyecto'].' '.$proyecto[0]['aper_actividad'].'<br>
                        </td>
                        <td width=30%; align=center>
                            <img src="'.base_url().'assets/img/escudo.jpg" alt="" width="40%">
                        </td>
                    </tr>
                </table><hr>';

        //    $this->mis_partidas($mod,$proy_id,$id_f,$this->gestion,$fase[0]['pfec_ejecucion']);
        //    $tabla .=''.$proy_id.'--'.$this->gestion.'-'.$fase[0]['pfec_ejecucion'];
        $proyecto_programado= $this->mfinanciera->lista_partidas_proy_programado($proy_id,$this->gestion,$fase[0]['pfec_ejecucion']);
        $nro=0;
        foreach ($proyecto_programado as $pp){
            $nro++;
            $ff= $this->mfinanciera->get_ff($pp['ff_id']);
            $of= $this->mfinanciera->get_of($pp['of_id']);

            $tabla .='<table class="change_order_items" style="width:100%;" align="center">';
            $tabla .='<tr style="background-color:#ac9fae;font-weight: bold;">';
            $tabla .='<td style="width:3%;">Nro</td>';
            $tabla .='<td style="width:5%;">PARTIDA</td>';
            $tabla .='<td style="width:5%;">DESCRIPCI&Oacute;N</td>';
            $tabla .='<td style="width:5%;">FUENTE</td>';
            $tabla .='<td style="width:5%;">ORGANISMO</td>';
            $tabla .='<td style="width:82%;">PROGRAMADO - EJECUTADO </td>';
            $tabla .='</tr>';
            $tabla .='<tr>';
            $tabla .='<td>'.$nro.'</td>';
            $tabla .='<td>'.$pp['par_codigo'].'</td>';
            $tabla .='<td>'.$pp['par_nombre'].'</td>';
            $tabla .='<td>'.$ff[0]['ff_codigo'].'</td>';
            $tabla .='<td>'.$of[0]['of_codigo'].'</td>';
            $tabla .='<td>';
            $tabla .='<table class="change_order_items" style="width:100%;" align="center">';
            $tabla .='<tr class="modo1">';
            $tabla .='<td bgcolor="#504c49" style="width:7.6%;"><font color=#fff>P/E</font></td>';
            $tabla .='<td bgcolor="#504c49" style="width:7.6%;"><font color=#fff>ENE.</font></td>';
            $tabla .='<td bgcolor="#504c49" style="width:7.6%;"><font color=#fff>FEB.</font></td>';
            $tabla .='<td bgcolor="#504c49" style="width:7.6%;"><font color=#fff>MAR.</font></td>';
            $tabla .='<td bgcolor="#504c49" style="width:7.6%;"><font color=#fff>ABR.</font></td>';
            $tabla .='<td bgcolor="#504c49" style="width:7.6%;"><font color=#fff>MAY.</font></td>';
            $tabla .='<td bgcolor="#504c49" style="width:7.6%;"><font color=#fff>JUN.</font></td>';
            $tabla .='<td bgcolor="#504c49" style="width:7.6%;"><font color=#fff>JUL.</font></td>';
            $tabla .='<td bgcolor="#504c49" style="width:7.6%;"><font color=#fff>AGOST.</font></td>';
            $tabla .='<td bgcolor="#504c49" style="width:7.6%;"><font color=#fff>SEPT.</font></td>';
            $tabla .='<td bgcolor="#504c49" style="width:7.6%;"><font color=#fff>OCT.</font></td>';
            $tabla .='<td bgcolor="#504c49" style="width:7.6%;"><font color=#fff>NOV.</font></td>';
            $tabla .='<td bgcolor="#504c49" style="width:7.6%;"><font color=#fff>DIC.</font></td>';
            $tabla .='</tr>';
            $tabla .='<tr class="modo1">';
            $tabla .='<td>PROGRAMADO</td>';
            $tabla .='<td>'.number_format($pp['enero'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pp['febrero'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pp['marzo'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pp['abril'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pp['mayo'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pp['junio'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pp['julio'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pp['agosto'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pp['septiembre'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pp['octubre'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pp['noviembre'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pp['diciembre'], 2, ',', '.').'</td>';
            $tabla .='</tr>';
            $pe= $this->mfinanciera->get_partida_proy_ejecutado($pp['pr_id'],$this->gestion);
            if(count($pe)!=0){
                $tabla .='<tr class="modo1">';
                $tabla .='<td>EJECUTADO</td>';
                $tabla .='<td>'.number_format($pe[0]['enero'], 2, ',', '.').'</td>';
                $tabla .='<td>'.number_format($pe[0]['febrero'], 2, ',', '.').'</td>';
                $tabla .='<td>'.number_format($pe[0]['marzo'], 2, ',', '.').'</td>';
                $tabla .='<td>'.number_format($pe[0]['abril'], 2, ',', '.').'</td>';
                $tabla .='<td>'.number_format($pe[0]['mayo'], 2, ',', '.').'</td>';
                $tabla .='<td>'.number_format($pe[0]['junio'], 2, ',', '.').'</td>';
                $tabla .='<td>'.number_format($pe[0]['julio'], 2, ',', '.').'</td>';
                $tabla .='<td>'.number_format($pe[0]['agosto'], 2, ',', '.').'</td>';
                $tabla .='<td>'.number_format($pe[0]['septiembre'], 2, ',', '.').'</td>';
                $tabla .='<td>'.number_format($pe[0]['octubre'], 2, ',', '.').'</td>';
                $tabla .='<td>'.number_format($pe[0]['noviembre'], 2, ',', '.').'</td>';
                $tabla .='<td>'.number_format($pe[0]['diciembre'], 2, ',', '.').'</td>';
                $tabla .='</tr>';
            }
            $tabla .='</table>';
            $tabla .='</td>';

            $tabla .='</tr>';
            $tabla .='</table>';
        }
        $tabla .='<hr>';
        $total=$this->suma_total_partidas($proy_id,$this->gestion);
        $tabla .='
                    <table class="change_order_items" style="width:100%;" align="center">
                        <tr class="modo1">
                            <td colspan=13 bgcolor="#504c49" style="width:2%;"><font color=#fff>GESTI&Oacute;N '.$this->gestion.'</font></td>
                        </tr>
                        <tr class="modo1">
                            <td bgcolor="#504c49" style="width:2%;"><font color=#fff>P/E</font></td>
                            <td bgcolor="#504c49" style="width:5%;"><font color=#fff>ENERO</font></td>
                            <td bgcolor="#504c49" style="width:5%;"><font color=#fff>FEBRERO</font></td>
                            <td bgcolor="#504c49" style="width:5%;"><font color=#fff>MARZO</font></td>
                            <td bgcolor="#504c49" style="width:5%;"><font color=#fff>ABRIL</font></td>
                            <td bgcolor="#504c49" style="width:5%;"><font color=#fff>MAYO</font></td>
                            <td bgcolor="#504c49" style="width:5%;"><font color=#fff>JUNIO</font></td>
                            <td bgcolor="#504c49" style="width:5%;"><font color=#fff>JULIO</font></td>
                            <td bgcolor="#504c49" style="width:5%;"><font color=#fff>AGOSTO</font></td>
                            <td bgcolor="#504c49" style="width:5%;"><font color=#fff>SEPTIEMBRE</font></td>
                            <td bgcolor="#504c49" style="width:5%;"><font color=#fff>OCTUBRE</font></td>
                            <td bgcolor="#504c49" style="width:5%;"><font color=#fff>NOVIEMBRE</font></td>
                            <td bgcolor="#504c49" style="width:5%;"><font color=#fff>DICIEMBRE</font></td>
                        </tr>
                        <tr class="modo1">
                            <td>P.</td>';
        for ($i=1; $i <=12 ; $i++){
            $tabla .='<td>'.number_format($total[2][$i], 2, ',', '.').'</td>';
        }
        $tabla .='
                        </tr>
                        <tr class="modo1">
                            <td>P.A.</td>';
        for ($i=1; $i <=12 ; $i++){
            $tabla .='<td>'.number_format($total[3][$i], 2, ',', '.').'</td>';
        }
        $tabla .='
                        </tr>
                        <tr class="modo1" bgcolor="#c5bcf3">
                            <td>%P.A.</td>';
        for ($i=1; $i <=12 ; $i++){
            $tabla .='<td>'.$total[4][$i].'%</td>';
        }
        $tabla .='
                        </tr>
                        <tr class="modo1">
                            <td>E</td>';
        for ($i=1; $i <=12 ; $i++)
        {
            $tabla .='<td>'.number_format($total[5][$i], 2, ',', '.').'</td>';
        }
        $tabla .='
                        </tr>
                        <tr class="modo1">
                            <td>E.A.</td>';
        for ($i=1; $i <=12 ; $i++){
            $tabla .='<td>'.number_format($total[6][$i], 2, ',', '.').'</td>';
        }
        $tabla .='
                        </tr>
                        <tr class="modo1" bgcolor="#c5bcf3">
                            <td>%E.A.</td>';
        for ($i=1; $i <=12 ; $i++){
            $tabla .='<td>'.$total[7][$i].'%</td>';
        }
        $tabla .='
                        </tr>
                        <tr class="modo1" bgcolor="#bbf3cf">
                            <td>EFICACIA</td>';
        for ($i=1; $i <=12 ; $i++){
            $tabla .='<td>'.$total[8][$i].'%</td>';
        }
        $tabla .='
                        </tr>';
        $tabla .='
                    </table>';


        return $tabla;
    }
    function generar_prog_anual($par_id,$proy_id,$insp_id,$total_programado){
        $tabla='';
        $prod_gest=$this->minsumos_partida->insumos_partida_mensual($insp_id,$this->gestion);

        $nro=0;
        $pa=0;
        foreach($prod_gest as $row)
        {
            $nro++;
            $matriz [1][$nro]=$row['m_id'];
            $matriz [2][$nro]=$row['inspg_monto_prog'];
        }

        for($j = 1 ;$j<=12 ;$j++)
        {
            $matriz_r[1][$j]=$j;
            $matriz_r[2][$j]='0';
        }

        for($i = 1 ;$i<=$nro ;$i++)
        {
            for($j = 1 ;$j<=12 ;$j++)
            {
                if($matriz[1][$i]==$matriz_r[1][$j])
                {
                    $matriz_r[2][$j]=round($matriz[2][$i],3);
                }
            }
        }

        for($j = 1 ;$j<=12 ;$j++){
            $pa=$pa+$matriz_r[2][$j];
            $matriz_r[3][$j]=$pa;
            if($total_programado == 0){
                $matriz_r[4][$j]=0;
            }else{
                $matriz_r[4][$j]=round((($pa/$total_programado)*100),2);
            }
        }
        $tabla='<table class="change_order_items" border="1" style="width: 100%">
                <thead>                        
                <tr>
                    <td style="width:1%;" bgcolor="#504c49" colspan=13><font color="#ffffff">GESTI&Oacute;N '.$this->gestion.'</font></td>
                </tr>
                <tr bgcolor="#504c49">
                    <td style="width:1%;"><font color="#ffffff">P/E</font></td>
                    <td style="width:1%;"><font color="#ffffff"><center>Ene.</center></font></td>
                    <td style="width:1%;"><font color="#ffffff"><center>Feb.</center></font></td>
                    <td style="width:1%;"><font color="#ffffff"><center>Mar.</center></font></td>
                    <td style="width:1%;"><font color="#ffffff"><center>Abr.</center></font></td>
                    <td style="width:1%;"><font color="#ffffff"><center>May.</center></font></td>
                    <td style="width:1%;"><font color="#ffffff"><center>Jun.</center></font></td>
                    <td style="width:1%;"><font color="#ffffff"><center>Jul.</center></font></td>
                    <td style="width:1%;"><font color="#ffffff"><center>Ago.</center></font></td>
                    <td style="width:1%;"><font color="#ffffff"><center>Sep.</center></font></td>
                    <td style="width:1%;"><font color="#ffffff"><center>Oct.</center></font></td>
                    <td style="width:1%;"><font color="#ffffff"><center>Nov.</center></font></td>
                    <td style="width:1%;"><font color="#ffffff"><center>Dic.</center></font></td>
                </tr>';
        $tabla.='<tr bgcolor="#F5F5DC" title="PROGRAMADO">
                    <td style="width:1%;">Prog</td>';
        for($i = 1 ;$i<=12 ;$i++){
            $tabla.= '<td>'.$matriz_r[2][$i].'</td>';
        }
        $epartida=$this->mfinanciera->lista_partidas_proy_gestion_programado($proy_id, $par_id,$this->gestion);
        if(count($epartida)!=0){
            $pe= $this->mfinanciera->get_partida_proy_ejecutado($epartida[0]['pr_id'],$this->gestion);
            if(count($pe)!=0){
            $tabla .='<tr class="modo1">';
            $tabla .='<td>Ejec</td>';
            $tabla .='<td>'.number_format($pe[0]['enero'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pe[0]['febrero'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pe[0]['marzo'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pe[0]['abril'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pe[0]['mayo'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pe[0]['junio'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pe[0]['julio'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pe[0]['agosto'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pe[0]['septiembre'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pe[0]['octubre'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pe[0]['noviembre'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pe[0]['diciembre'], 2, ',', '.').'</td>';
            $tabla .='</tr>';
        }else{
            $tabla .='<tr class="modo1">';
            $tabla .='<td>Ejec</td>';
            $tabla .='<td>'.number_format(0, 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format(0, 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format(0, 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format(0, 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format(0,2, ',', '.').'</td>';
            $tabla .='<td>'.number_format(0, 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format(0, 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format(0, 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format(0, 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format(0, 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format(0, 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format(0, 2, ',', '.').'</td>';
            $tabla .='</tr>';
        }
        }
        $tabla.='</tr>
                 <tr bgcolor="#F5F5DC" title="ACUMULADO (En %)">
                    <td style="width:1%;">%PA</td>';
        for($i = 1 ;$i<=12 ;$i++){
            if($matriz_r[4][$i] == 0){
                $tabla.= '<td class="bg-danger">'.$matriz_r[4][$i].'%</td>';
            }else{
                $tabla.= '<td>'.$matriz_r[4][$i].'%</td>';
            }
        }
        $tabla.='</tr>
        </thead></table>';
        return $tabla;
    }
    function generar_prog_multi_anual($par_id,$proy_id,$insp_id,$total_programado){
        $tabla='';
        $fase = $this->model_faseetapa->get_id_fase($proy_id);
        $a�os=$fase[0]['pfec_fecha_fin']-$fase[0]['pfec_fecha_inicio']+1;
        $gestion=$fase[0]['pfec_fecha_inicio'];
        $gestion_tbl_aux=$gestion;
        $matriz_gest[1][$gestion-1]='Prog';
        $matriz_gest[2][$gestion-1]='Ejec';
        $matriz_gest[3][$gestion-1]='%PA';
        //$matriz_gest[4][$gestion_cab-1]='%E.A.';

        $suma_prog=0;
        $tabla='<table class="change_order_items" border="1" style="width: 100%">
                <thead>                        
                <tr bgcolor="#504c49">
                    <td style="width:1%;"><font color="#ffffff">P/E</font></td>';
        for($k=1;$k<=$a�os;$k++) {
            $tabla.='<td style = "width:1%;" ><font color = "#ffffff" ><center >'.$gestion.'</center ></font ></td >';
            $gestion++;
        }
        $tabla.='</tr>';

        for($k=1;$k<=$a�os;$k++)
        {
            $prod_gest=$this->minsumos_partida->insumo_prog_mensual($insp_id,$fase[0]['pfec_fecha_inicio']);
            $nro=0;
            $total_gestion=0;
            foreach($prod_gest as $row)
            {
                $nro++;
                $matriz [1][$nro]=$row['m_id'];
                $matriz [2][$nro]=$row['inspg_monto_prog'];
                $suma_prog=$suma_prog+$row['inspg_monto_prog'];
            }
            for($j = 1 ;$j<=12 ;$j++){
                $matriz_r[1][$j]=$j;
                $matriz_r[2][$j]='0';
            }

            for($i = 1 ;$i<=$nro ;$i++){
                for($j = 1 ;$j<=12 ;$j++)
                {
                    if($matriz[1][$i]==$matriz_r[1][$j])
                    {
                        $matriz_r[2][$j]=round($matriz[2][$i],3);
                        $total_gestion+=$matriz_r[2][$j];
                    }
                }
            }
            //$fase[0]['pfec_fecha_inicio']
            $matriz_gest[1][$fase[0]['pfec_fecha_inicio']]=$total_gestion;
            if($total_programado!=0)
            {$matriz_gest[3][$fase[0]['pfec_fecha_inicio']]=round($total_gestion/$total_programado*100,2);}
            else{$matriz_gest[3][$fase[0]['pfec_fecha_inicio']]=0;}

            //$prod_gest=$this->minsumos_partida->insumos_partida_mensual($insp_id,$fase[0]['pfec_fecha_inicio']);
            $nro=0;
            $pa=0;
            $epartida=$this->mfinanciera->lista_partidas_proy_gestion_programado($proy_id, $par_id,$fase[0]['pfec_fecha_inicio']);
            if(count($epartida)!=0){
                $pe= $this->mfinanciera->get_partida_proy_ejecutado($epartida[0]['pr_id'],$fase[0]['pfec_fecha_inicio']);
                if(count($pe)!=0){
                    $matriz_gest[2][$fase[0]['pfec_fecha_inicio']]=$pe[0]['enero']+$pe[0]['febrero']+$pe[0]['marzo']+$pe[0]['abril']+$pe[0]['mayo']+$pe[0]['junio']+$pe[0]['julio']+$pe[0]['agosto']+$pe[0]['septiembre']+$pe[0]['octubre']+$pe[0]['noviembre']+$pe[0]['diciembre'];
                }else{
                    $matriz_gest[2][$fase[0]['pfec_fecha_inicio']]=0;
                }
            }else{
                $matriz_gest[2][$fase[0]['pfec_fecha_inicio']]=0;
            }
            $fase[0]['pfec_fecha_inicio']++;
        }
        $gestion_tbl=$gestion_tbl_aux;
        $columna=$gestion_tbl-1;
        $h=1;
        for($j=1;$j<=3;$j++){
            $tabla .= '<tr class="modo1" >
                        <td>'.$matriz_gest[$j][$columna].'</td>';
            for($k=1;$k<=$a�os;$k++){
                $tabla .= '<td>'.$matriz_gest[$h][$gestion_tbl].'</td>';//
                $gestion_tbl++;
            }
            $h++;
            $tabla .= '</tr>';
            $gestion_tbl=$gestion_tbl_aux;
        }

        /*$tabla.='</tr>
                 <tr bgcolor="#F5F5DC" title="ACUMULADO (En %)">
                    <td style="width:1%;">%PA</td>';
        for($i = 1 ;$i<=12 ;$i++){
            if($matriz_r[4][$i] == 0){
                $tabla.= '<td class="bg-danger">'.$matriz_r[4][$i].'%</td>';
            }else{
                $tabla.= '<td>'.$matriz_r[4][$i].'%</td>';
            }
        }*/
        $tabla.='</tr>
        </thead></table>';
        return $tabla;
    }

    /*------------------------- Total Partidas --------------------------*/
    function total_partidas($proy_id,$gestion,$tp){
    $total=$this->suma_total_partidas($proy_id,$gestion);

    $tabla =''; 
        $tabla .='<div class="contenedor_datos">
                    <table class="change_order_items" style="width:100%;" align="center">
                        <tr class="modo1">
                            <td colspan=13 bgcolor="#504c49" style="width:2%;"><font color=#fff>GESTI&Oacute;N '.$this->gestion.'</font></td>
                        </tr>
                        <tr class="modo1">
                            <td bgcolor="#504c49" style="width:2%;"><font color=#fff>P/E</font></td>
                            <td bgcolor="#504c49" style="width:5%;"><font color=#fff>ENERO</font></td>
                            <td bgcolor="#504c49" style="width:5%;"><font color=#fff>FEBRERO</font></td>
                            <td bgcolor="#504c49" style="width:5%;"><font color=#fff>MARZO</font></td>
                            <td bgcolor="#504c49" style="width:5%;"><font color=#fff>ABRIL</font></td>
                            <td bgcolor="#504c49" style="width:5%;"><font color=#fff>MAYO</font></td>
                            <td bgcolor="#504c49" style="width:5%;"><font color=#fff>JUNIO</font></td>
                            <td bgcolor="#504c49" style="width:5%;"><font color=#fff>JULIO</font></td>
                            <td bgcolor="#504c49" style="width:5%;"><font color=#fff>AGOSTO</font></td>
                            <td bgcolor="#504c49" style="width:5%;"><font color=#fff>SEPTIEMBRE</font></td>
                            <td bgcolor="#504c49" style="width:5%;"><font color=#fff>OCTUBRE</font></td>
                            <td bgcolor="#504c49" style="width:5%;"><font color=#fff>NOVIEMBRE</font></td>
                            <td bgcolor="#504c49" style="width:5%;"><font color=#fff>DICIEMBRE</font></td>
                        </tr>
                        <tr class="modo1">
                            <td>P.</td>';
                            for ($i=1; $i <=12 ; $i++){ 
                               $tabla .='<td>'.number_format($total[2][$i], 2, ',', '.').'</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="modo1">
                            <td>P.A.</td>';
                            for ($i=1; $i <=12 ; $i++){ 
                               $tabla .='<td>'.number_format($total[3][$i], 2, ',', '.').'</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="modo1" bgcolor="#c5bcf3">
                            <td>%P.A.</td>';
                            for ($i=1; $i <=12 ; $i++){ 
                               $tabla .='<td>'.$total[4][$i].'%</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="modo1">
                            <td>E</td>';
                            for ($i=1; $i <=12 ; $i++) 
                            { 
                               $tabla .='<td>'.number_format($total[5][$i], 2, ',', '.').'</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="modo1">
                            <td>E.A.</td>';
                            for ($i=1; $i <=12 ; $i++){ 
                               $tabla .='<td>'.number_format($total[6][$i], 2, ',', '.').'</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="modo1" bgcolor="#c5bcf3">
                            <td>%E.A.</td>';
                            for ($i=1; $i <=12 ; $i++){ 
                               $tabla .='<td>'.$total[7][$i].'%</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="modo1" bgcolor="#bbf3cf">
                            <td>EFICACIA</td>';
                            for ($i=1; $i <=12 ; $i++){ 
                               $tabla .='<td>'.$total[8][$i].'%</td>'; 
                            }
                        $tabla .='
                        </tr>';
                    $tabla .='
                    </table>
                </div>';
     
        return $tabla;
  }

    public function reporte_programacion_financiera2($mod,$id_f,$id_p){
        $proy_id = $id_p;
        $gestion = $this->session->userdata('gestion');
        $proyecto = $this->model_proyecto->get_id_proyecto($id_p); 
        $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/logo.jpg" class="siipp"/>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata('entidad').'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$gestion.'<br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                            <b>FORMULARIO : </b> PROGRAMACI&Oacute;N Y EJECUCI&Oacute;N PRESUPUESTARIA DE LA OPERACI&Oacute;N<br>
                        </td>
                        <td width=30%;>
                            <img src="'.base_url().'assets/img/escudo.jpg" alt="" width="80px">
                        </td>
                    </tr>
                </table>

                <footer>
                    <table class="table table-bordered" width="100%">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata('sistema_pie').'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>
                <table class="datos_principales" style="table-layout:fixed;" border="1">
                    <tr style="text-align:center;">
                        <td colspan="4">
                            <b>'.$proyecto[0]['proy_nombre'].'</b><br>
                            '.$proyecto[0]['tipo'].'
                        </td>
                    </tr>
                    <tr>
                        <td style="width:50%; text-align:center;"">
                        <b>'.$proyecto[0]['proy_sisin'].'</b><br>
                            C&oacute;digo SISIN
                        </td>
                        <td style="width:50%; text-align:center;" >
                            <b>'.$proyecto[0]['aper_programa'].''.$proyecto[0]['aper_proyecto'].''.$proyecto[0]['aper_actividad'].'</b><br>
                            Apertura Programatica
                        </td>
                    </tr>
                    <tr>
                        <td style="width:50%;text-align:center;">
                            <b>'.$proyecto[0]['ue'].'</b><br>
                            Unidad Ejecutora
                        </td>
                        <td style="width:50%; text-align:center;">
                            <b>'.$proyecto[0]['ur'].'</b><br>
                            Unidad Responable
                        </td>
                    </tr>
                </table><br>
                <div id="content">
                    <p>'.$this->mis_partidas($mod,$id_p,$id_f,$gestion).'<br>
                        '.$this->total_partidas($id_p,$gestion,1).'</p></div>';

                $html .= '
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'landscape');
        ini_set('memory_limit','-1');
        ini_set('max_execution_time', 0);
        $dompdf->render();
        $dompdf->stream("programacion_ejecucion_financiera.pdf", array("Attachment" => false));
    }


    public function suma_total_partidas($proy_id,$gestion){
        $fase = $this->model_faseetapa->get_id_fase($proy_id);
        $pprog = $this->mfinanciera->vlista_partidas_total_proy_programado($proy_id,$gestion,$fase[0]['pfec_ejecucion']);
        $eprog = $this->mfinanciera->vlista_partidas_total_sigep_proy_programado($proy_id,$gestion);
        
        $m[0]='total';
        $m[1]='enero';
        $m[2]='febrero';
        $m[3]='marzo';
        $m[4]='abril';
        $m[5]='mayo';
        $m[6]='junio';
        $m[7]='julio';
        $m[8]='agosto';
        $m[9]='septiembre';
        $m[10]='octubre';
        $m[11]='noviembre';
        $m[12]='diciembre';

        for($i=1;$i<=12;$i++){
          $totalp[1][$i]=0; //// i
          $totalp[2][$i]=0; //// p
          $totalp[3][$i]=0; //// pa
          $totalp[4][$i]=0; //// %pa
          $totalp[5][$i]=0; //// e
          $totalp[6][$i]=0; //// ea
          $totalp[7][$i]=0; //// %ea
          $totalp[8][$i]=0; //// eficacia
          $totalp[9][$i]=0; //// Menor
          $totalp[10][$i]=0; //// Entre
          $totalp[11][$i]=0; //// Mayor
        }

        if(count($pprog)!=0){
            $suma_pfin=0; $suma_efin=0;
            for($i=1;$i<=12;$i++){
                $totalp[1][$i]=$i; //// i
                $totalp[2][$i]=$pprog[0][$m[$i]]; /// programado
                $suma_pfin=$suma_pfin+$pprog[0][$m[$i]];
                $totalp[3][$i]=$suma_pfin; ///// Programado acumulado

                if($pprog[0][$m[0]]!=0){
                    $totalp[4][$i]=round((($totalp[3][$i]/$pprog[0][$m[0]])*100),2); ////// Programado Acumulado %
                }

                if(count($eprog)!=0){
                  $totalp[5][$i]=$eprog[0][$m[$i]]; /// Ejecutado
                  $suma_efin=$suma_efin+$eprog[0][$m[$i]];
                  $totalp[6][$i]=$suma_efin; ///// Ejecutado acumulado
                  
                  if($pprog[0][$m[0]]!=0){
                    $totalp[7][$i]=round((($totalp[6][$i]/$pprog[0][$m[0]])*100),2); ////// Ejecutado Acumulado %
                  }

                  if($totalp[4][$i]!=0){
                    $totalp[8][$i]=round((($totalp[7][$i]/$totalp[4][$i])*100),2); ////// Eficacia
                  }

                  /*------------------------- eficacia -------------------------*/
                  if($totalp[8][$i]<=75){$totalp[9][$i] = $totalp[8][$i];}
                  if ($totalp[8][$i] >= 76 && $totalp[8][$i] <= 90.9) {$totalp[10][$i] = $totalp[8][$i];}
                  if($totalp[8][$i] >= 91){$totalp[11][$i] = $totalp[8][$i];}
              }
            }
        }

    return $totalp;
    }
    public function suma_total_partidas_multi($proy_id,$gestion,$gran_total){
        $fase = $this->model_faseetapa->get_id_fase($proy_id);
        $pprog = $this->mfinanciera->vlista_partidas_total_proy_programado_global($proy_id,$gestion,$fase[0]['pfec_ejecucion']);
       $eprog = $this->mfinanciera->vlista_partidas_total_sigep_proy_programado_global($proy_id,$gestion);

        $i=1;
        $totalp[1][$i]=0; //// i
        $totalp[2][$i]=0; //// p
        $totalp[3][$i]=0; //// pa
        $totalp[4][$i]=0; //// %pa
        $totalp[5][$i]=0; //// e
        $totalp[6][$i]=0; //// ea
        $totalp[7][$i]=0; //// %ea
        $totalp[8][$i]=0; //// eficacia
        $totalp[9][$i]=0; //// Menor
        $totalp[10][$i]=0; //// Entre
        $totalp[11][$i]=0; //// Mayor


        if(count($pprog)!=0){
            $suma_pfin=0; $suma_efin=0;
            $totalp[1][$i]=$i; //// i
            $totalp[2][$i]=$pprog[0]['total']; /// programado
            $suma_pfin=$suma_pfin+$pprog[0]['total'];
            $totalp[3][$i]=$suma_pfin; ///// Programado acumulado

            if($gran_total!=0){
                $totalp[4][$i]=round((($totalp[3][$i]/$gran_total)*100),2); ////// Programado Acumulado %
            }

            if(count($eprog)!=0){
                $totalp[5][$i]=$eprog[0]['total']; /// Ejecutado
                $suma_efin=$suma_efin+$eprog[0]['total'];
                $totalp[6][$i]=$suma_efin; ///// Ejecutado acumulado

                if($gran_total!=0){
                    $totalp[7][$i]=round((($totalp[6][$i]/$gran_total)*100),2); ////// Ejecutado Acumulado %
                }

                if($totalp[4][$i]!=0){
                    $totalp[8][$i]=round((($totalp[7][$i]/$totalp[4][$i])*100),2); ////// Eficacia
                }

                /*------------------------- eficacia -------------------------*/
                if($totalp[8][$i]<=75){$totalp[9][$i] = $totalp[8][$i];}
                if ($totalp[8][$i] >= 76 && $totalp[8][$i] <= 90.9) {$totalp[10][$i] = $totalp[8][$i];}
                if($totalp[8][$i] >= 91){$totalp[11][$i] = $totalp[8][$i];}
            }
        }
        return $totalp;
    }
    /*==============================================================================================================*/

  /*===================================== REPORTES CONTRATOS =============================================*/
 public function reporte_contrato($mod,$id_f,$id_p)
  { 
    if($mod!='' & $id_f!='' & $id_p!=''){
        $enlaces=$this->menu_modelo->get_Modulos_programacion(1);
        $data['enlaces'] = $enlaces;
        $data['nro_fase']= $this->model_faseetapa->nro_fase($id_p); /// nro de fases y etapas registrados
        $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p); 
        $data['id_f'] = $this->model_faseetapa->get_id_fase($id_p); //// recupera datos de la tabla fase activa
        $data['mod']=$mod;
        $titulo_proy=strtoupper($data['proyecto'][0]['tipo']);
        $data['titulo_proy'] = $titulo_proy;
        $data['componente'] = $this->model_componente->get_componente($data['id_f'][0]['id']);
        $this->load->view('admin/programacion/reportes/contrato/contrato', $data);
    }
    else{
        redirect('admin/dashboard');
    }
  }
 /*==============================================================================================================*/
/*===================================== REPORTES CONTRATOS =============================================*/
  public function grafico_fisico($mod,$id_f,$id_p){ 
    if($mod!='' & $id_f!='' & $id_p!=''){
        $enlaces=$this->menu_modelo->get_Modulos_programacion(1);
        $data['enlaces'] = $enlaces;
        $data['nro_fase']= $this->model_faseetapa->nro_fase($id_p); /// nro de fases y etapas registrados
        $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p); 
        $data['id_f'] = $this->model_faseetapa->get_id_fase($id_p); //// recupera datos de la tabla fase activa
        $data['mod']=$mod;
        $data['componente'] = $this->model_componente->get_componente($data['id_f'][0]['id']);

        $titulo_proy=strtoupper($data['proyecto'][0]['tipo']);
        $data['titulo_proy'] = $titulo_proy;

        if($mod==1){
            $this->load->view('admin/programacion/reportes/grafico_fisico/curva_s', $data);
        }
        elseif ($mod==4){
            $this->load->view('admin/programacion/reportes/grafico_fisico/curva_s_m', $data);
        }
    }
    else{
      redirect('admin/dashboard');
    }
  }

  public function grafico_financiero($mod,$id_f,$id_p){ 
    if($mod!='' & $id_f!='' & $id_p!=''){
        $enlaces=$this->menu_modelo->get_Modulos_programacion(1);
        $data['enlaces'] = $enlaces;
        $data['nro_fase']= $this->model_faseetapa->nro_fase($id_p); /// nro de fases y etapas registrados
        $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p); 
        $data['id_f'] = $this->model_faseetapa->get_id_fase($id_p); //// recupera datos de la tabla fase activa
        $data['mod']=$mod;
        $data['componente'] = $this->model_componente->get_componente($data['id_f'][0]['id']);

        $titulo_proy=strtoupper($data['proyecto'][0]['tipo']);
        $data['titulo_proy'] = $titulo_proy;

        if($mod==1){
            $this->load->view('admin/programacion/reportes/grafico_financiero/curva_s', $data);
        }
        elseif ($mod==4){
            $this->load->view('admin/programacion/reportes/grafico_financiero/curva_s_m', $data);
        }
    }
    else{
      redirect('admin/dashboard');
    }
    
  }

    public function grafico_programacion_financiero($mod, $id_f, $proy_id){
        if($mod!='' & $id_f!='' & $proy_id!=''){
        $data['nro_fase'] = $this->model_faseetapa->nro_fase($proy_id); /// nro de fases y etapas registrados
        $data['proyecto'] = $this->model_proyecto->get_id_proyecto($proy_id);
        $data['unidad_responsable']=$this->model_proyecto->responsable_proy($proy_id,2); //// unidad responsable
        $data['unidad_ejecutora']=$this->model_proyecto->responsable_proy($proy_id,1);  //// unidad ejecutora
        $data['dato_proy'] = $this->model_proyecto->get_id_proyecto($proy_id)[0]; 
        $fase = $this->model_faseetapa->get_id_fase($proy_id); //// recupera datos de la tabla fase activa
        $data['mod'] = $mod;
        $data['componente'] = $this->model_componente->get_componente($fase[0]['id']);
        $data['a�os']=$fase[0]['pfec_fecha_fin']-$fase[0]['pfec_fecha_inicio']+1;
        $data['g_inicio']=$fase[0]['pfec_fecha_inicio'];
        //
        $data['dato_proyecto'] = $this->dato_proyecto($proy_id); ///// Dato Proyecto
        $data['tabla_insumo'] = $this->total_partidas($proy_id,$this->gestion,2); ///// Matriz Tabla Insumo
        $data['total'] = $this->suma_total_partidas($proy_id,$this->gestion); //// Matriz Totalsuma_total_partidas($proy_id,$gestion)
            
        $this->load->view('admin/programacion/reportes/grafico_financiero/rep_curva_s', $data);
        }
        else{
        redirect('admin/dashboard');
        }
    }


 /*==============================================================================================================*/
  /*===================================== SUPERVISION Y SEGUIMIENTO EVALUACION =============================================*/
  public function reporte_supervision($mod,$id_f,$id_p)
  { 
    if($mod!='' & $id_f!='' & $id_p!=''){
    $enlaces=$this->menu_modelo->get_Modulos_programacion(1);
    $data['enlaces'] = $enlaces;
    $data['nro_fase']= $this->model_faseetapa->nro_fase($id_p); /// nro de fases y etapas registrados
    $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p); 
    $data['id_f'] = $this->model_faseetapa->get_id_fase($id_p); //// recupera datos de la tabla fase activa
    $data['mod']=$mod;
    $titulo_proy=strtoupper($data['proyecto'][0]['tipo']);
    $data['titulo_proy'] = $titulo_proy;

    $data['componente'] = $this->model_componente->get_componente($data['id_f'][0]['id']);
    $this->load->view('admin/programacion/reportes/supervision/supervision', $data);
    }
    else{
        redirect('admin/dashboard');
    }
  }
 /*==============================================================================================================*/
/*==================================== IDENTIFICACION DEL PROYECTO ===================================================*/
public function identificacion_proyecto($id)
    {
        $proy_id = $id;
        $gestion = $this->session->userdata('gestion');
        $dictamen = $this->model_objetivo->get_dictamen($proy_id);
        $unidad_responsable=$this->model_proyecto->responsable_proy($proy_id,1);
        $dictamen = $dictamen->row();
        $aper_programatica = $this->model_objetivo->get_aper_programatica_dictamen($proy_id,$gestion);
        $aper_programatica = $aper_programatica->row();
        $data_fase_activa = array(
            'pfec_id' => $dictamen->pfec_id, 
            'fas_fase' => $dictamen->fas_fase, 
            'eta_etapa' => $dictamen->eta_etapa,
            'pfec_descripcion' => $dictamen->pfec_descripcion,
            'fecha_inicial' => $dictamen->fecha_inicial,
            'fecha_final' => $dictamen->fecha_final,
            'ejecucion' => $dictamen->ejecucion,
            'anual_plurianual' => $dictamen->anual_plurianual,
            'tp_id' => $dictamen->tp_id
            );
        $num_principal = ($dictamen->tp_id == 1) ? 1 : 0 ;
        $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/logo.jpg" class="siipp"/>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata('entidad').'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$gestion.'<br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                            <b>FORMULARIO : </b> IDENTIFICACI&Oacute;N DEL PROYECTO <br>
                        </td>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/escudo.jpg" alt="" width="80px">
                        </td>
                    </tr>
                </table>
                <footer>
                    <table class="table table-bordered" width="100%">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata('sistema_pie').'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>
                <table class="datos_principales" style="table-layout:fixed;" border="1">
                    <tr>
                        <td colspan="2">
                            <b>'.$dictamen->proy_nombre.'</b><br>';
                            if($dictamen->tp_id==1)
                            {$html .= 'Proyecto/Actividad';}
                            elseif ($dictamen->tp_id==2 || $dictamen->tp_id==3) 
                            {$html .= 'Programa';}
                            elseif ($dictamen->tp_id==4) 
                            {$html .= 'Acci&oacute;n de Funcionamiento';}
                            $html .= '
                        </td>
                    </tr>
                    <tr>
                        <td style="width:50%;">';
                            if($dictamen->tp_id==1)
                            {$html .= '<b>'.$dictamen->proy_sisin.'</b><br>';}
                            elseif ($dictamen->tp_id==2 || $dictamen->tp_id==3 || $dictamen->tp_id==4) 
                            {$html .= '<b>N/A</b><br>';}
                            $html .= 'SISIN
                        </td>
                        <td style="width:50%; text-align:center;">
                            <b>'.$aper_programatica->programatica.'</b><br>
                            Apertura Programatica
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align:center;">
                            <b>'.$aper_programatica->uni_unidad.'</b><br>
                            Unidad Responsable
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align:center;">
                            <b>'.$unidad_responsable[0]['uresp'].'</b><br>
                            Unidad Ejecutora
                        </td>
                    </tr>
                </table>
                <br>
                <div class="contenedor_principal">
                    '.$this->fase_etapa_activa_proyecto($data_fase_activa, $num_principal).'
                    <div class="titulo_dictamen"> '.($num_principal+=1).'. DATOS GENERALES </div>
                    <div class="contenedor_datos">
                        <table class="table_contenedor">
                            <tr class="collapse_t">
                                <td style="width:18%"  class="fila_unitaria">Tipo</td>
                                <td class="fila_unitaria">'.$dictamen->tp_tipo.'</td>
                            </tr>
                        </table>
                    </div>
                    <div class="titulo_dictamen"> '.($num_principal+=1).'. INDICADORES METAS </div>
                    <div class="contenedor_datos">
                        <table class="table_contenedor">'.$this->indicadores_metas($proy_id).'</table>
                    </div>
                    <div class="titulo_dictamen"> '.($num_principal+=1).'. RESPONSABLES </div>
                    <div class="contenedor_datos">
                        <table class="table_contenedor">
                            <tr class="collapse_t">
                                <td style="width:18%" class="fila_unitaria">Responsable T&eacute;cnico</td>
                                <td class="fila_unitaria">'.$dictamen->resp_tecnico.'</td>
                            </tr>
                            <tr class="collapse_t">
                                <td class="fila_unitaria">Validador POA</td>
                                <td class="fila_unitaria">'.$dictamen->val_poa.'</td>
                            </tr>
                            <tr class="collapse_t">
                                <td class="fila_unitaria">Validador Financiero</td>
                                <td class="fila_unitaria">'.$dictamen->val_fin.'</td>
                            </tr>
                        </table>
                    </div>
                    <div class="titulo_dictamen"> '.($num_principal+=1).'. LOCALIZACI&Oacute;N </div>
                    <div class="contenedor_datos">'.$this->localizacion($proy_id).'</div>
                    
                    <div class="titulo_dictamen"> '.($num_principal+=1).'. CLASIFICACI&Oacute;N SECTORIAL</div>
                    <div class="contenedor_datos">
                        <table class="table_contenedor">
                            <tr class="collapse_t">
                                <td style="width:18%;text-align:center;" class="fila_unitaria">Clasificaci&oacute;n Sectorial</td>
                                <td class="fila_unitaria">
                                    <ul class="lista">
                                        <li><b>SECTOR: </b>'.$dictamen->c_sect_sector.'</li>
                                        <li><b>SUBSECTOR: </b>'.$dictamen->c_sect_subsector.'</li>
                                        <li><b>ACTIVIDAD: </b>'.$dictamen->c_sect_actividad.'</li>
                                    </ul>
                                </td>
                            </tr>
                        </table>
                    </div>
                   
                    <div class="titulo_dictamen"> '.($num_principal+=1).'. RESUMEN T&Eacute;CNICO DEL PROYECTO</div>
                    <div class="contenedor_datos">
                        <table class="table_contenedor">
                            <tr class="collapse_t">
                                <td style="width:18%" class="fila_unitaria">Descripci&oacute;n del Proyecto</td>
                                <td class="fila_unitaria">'.$dictamen->proy_descripcion_proyecto.'</td>
                            </tr>
                        </table>
                    </div>';
                    $fase=$this->model_faseetapa->fase_etapa($dictamen->pfec_id,$proy_id);
                    $html .= '<div class="titulo_dictamen"> '.($num_principal+=1).'. PRESUPUESTO</div>
                    <div class="contenedor_datos">
                        <table class="table_contenedor">
                            <tr class="collapse_t" style="background-color:#ac9fae;font-weight: bold;">
                                <td style="width:21%" class="fila_unitaria">TOTAL FASE PRESUPUESTO</td>
                                <td class="fila_unitaria">'.number_format($fase[0]['pfec_ptto_fase'], 2, ',', '.').' Bs.</td>
                                <td style="width:21%" class="fila_unitaria">PRESUPUESTO EJECUTADO</td>
                                <td class="fila_unitaria">'.number_format($fase[0]['pfec_ptto_fase_e'], 2, ',', '.').' Bs.</td>
                            </tr>';
                            $ptto_fase=$this->model_faseetapa->ptto_fase($dictamen->pfec_id);
                            foreach($ptto_fase as $row)
                            {
                                $html .= '
                                <tr class="collapse_t">
                                    <td style="width:21%" class="fila_unitaria">PRESUPUESTO '.$row['g_id'].'</td>
                                    <td class="fila_unitaria">'.number_format($row['pfecg_ppto_total'], 2, ',', '.').' Bs.</td>
                                    <td style="width:21%" class="fila_unitaria">EJECUCI&Oacute;N '.$row['g_id'].'</td>
                                    <td class="fila_unitaria">'.number_format($row['pfecg_ppto_ejecutado'], 2, ',', '.').' Bs.</td>
                                </tr>';
                            }
                        $html .= '    
                        </table>
                    </div>
                    '.$this->componente($proy_id, $dictamen->pfec_id,$gestion,$num_principal).'
                    <br>
                
                    <table class="table_contenedor" style="margin-top: 10px;>
                        <tr>
                            <td class="fila_unitaria">FIRMAS:</td>
                        </tr>
                    </table>
                    <table style="width: 80%;margin: 0 auto;margin-top: 100px;margin-bottom: 50px;">
                        <tr>
                            <td style="width:30%">
                                <hr>
                            </td>
                            <td style="width:3%"></td>
                            <td style="width:30%">
                                <hr>
                            </td>
                            <td style="width:3%"></td>
                            <td style="width:30%">
                                <hr>
                            </td>
                        </tr>
                    </table>

                </div>
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'portrait');
        $dompdf->render();
        $dompdf->stream("dictamen_proyecto.pdf", array("Attachment" => false));
    }

    public function fase_etapa_activa_proyecto($data, $num_principal)
    {
        $fase_etapa_activa = '';
        $fase_etapa_activa .= '';
        if($data['pfec_id'] != -1){
            if($data['tp_id']==1){
                $fase_etapa_activa .= '
                    <div class="titulo_dictamen"> 1. FASE ETAPA ACTIVA </div>
                    <div class="contenedor_datos">
                        <table class="table_contenedor">
                            <tr class="collapse_t" style="background-color:#ac9fae;">
                                <td>FASE</td>
                                <td>ETAPA</td>
                                <td>DESCRIPCI&Oacute;N</td>
                                <td>F. INICIAL</td>
                                <td>F. FINAL</td>
                                <td>EJECUCI&Oacute;N</td>
                                <td>ANUAL/PLURIANUAL</td>
                            </tr>
                            <tr class="collapse_t">
                                <td>'.$data['fas_fase'].'</td>
                                <td>'.$data['eta_etapa'].'</td>
                                <td>'.$data['pfec_descripcion'].'</td>
                                <td>'.$data['fecha_inicial'].'</td>
                                <td>'.$data['fecha_final'].'</td>
                                <td>'.$data['ejecucion'].'</td>
                                <td>'.$data['anual_plurianual'].'</td>
                            </tr>
                        </table>    
                    </div>
                ';    
            }
            else{
                $fase_etapa_activa .= '';    
            }
        }else{
            $fase_etapa_activa .= '
                <div class="titulo_dictamen"> 1. FASE ETAPA ACTIVA </div>
                <div class="contenedor_datos">
                    <table class="table_contenedor">
                        <tr>
                            <td>Sin Fase Etapa Activa</td>
                        </tr>
                    </table>    
                </div>
            ';
        };
        return $fase_etapa_activa;
    }

    public function indicadores_metas($id)
    {
        $proy_id = $id;
        $query = $this->model_objetivo->get_indicadores_metas($proy_id);
        $count = $query->num_rows();
        $query = $query->result_array();
        $html_metas = '';
        if( $count >= 1 ){
            $html_metas ='
                <tr class="collapse_t" style="background-color:#ac9fae;">
                    <td>#</td>
                    <td>DESCRIPCI&Oacute;N META</td>
                    <td>META</td>
                    <td>EJECUCI&Oacute;N</td>
                    <td>EFICACIA</td>
                </tr>';
            $n=0;
            foreach($query as $fila){
                $n++;
                $html_metas .='
                <tr class="collapse_t">
                    <td>'.$n.'</td>
                    <td>'.$fila['meta_descripcion'].'</td>
                    <td>'.$fila['meta_meta'].'</td>
                    <td>'.$fila['meta_ejec'].'</td>
                    <td>'.$fila['meta_efic'].'</td>
                </tr>'; 
            };
        }else{
            $html_metas = '
                <tr class="collapse_t">
                    <td>Sin Metas Registradas</td>
                </tr>
            ';
        }
        return $html_metas;
    }

    public function localizacion($id)
    {
        $proy_id = $id;
        $query = $this->model_objetivo->get_localizacion_dictamen($proy_id);
        $n = $query->num_rows();
        $html_localizacion = '';
        if($n>0){
            $query = $query->result_array();
            $html_localizacion .= '';
            foreach($query as $fila){
                $html_localizacion .='
                <table class="table_contenedor">
                    <tr class="collapse_t">
                        <td style="width:18%">
                            '.$fila['dep_departamento'].'
                        </td>
                        <td style="width:18%">
                            Provincia: '.$fila['prov_provincia'].'
                        </td>
                        <td>
                            <table class="table_contenedor">
                                <tr style="font-size:7px;background-color:#ac9fae;">
                                    <td style="height:10px;">REGI�N</td>
                                    <td>MUNICIPIO</td>
                                    <td>%P</td>
                                    <td>POB. HOMBRES</td>
                                    <td>POB. MUJERES</td>
                                    <td style="width:15%;">Cantones</td>
                                </tr>
                                <tr>
                                    <td>'.$fila['reg_region'].'</td>
                                    <td>'.$fila['muni_municipio'].'</td>
                                    <td>'.$fila['pm_pondera'].'%</td>
                                    <td>'.$fila['muni_poblacion_hombres'].'</td>
                                    <td>'.$fila['muni_polacion_mujeres'].'</td>
                                    <td>'.$fila['cantones'].'</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>'; 
            };
        }else{
            $html_localizacion .='
                <table class="table_contenedor">
                    <tr class="collapse_t" style="font-weight: bold;">
                        <td> Sin Localizaci�n</td>
                    </tr>
                </table>';
        }
        return $html_localizacion;
    }

    public function componente($id, $fase_activa, $gestion,$num_principal)
    {
        $proy_id = $id;
        if($fase_activa != -1){
            $componentes = $this->model_objetivo->get_componentes_dictamen($proy_id);
            $n = $componentes->num_rows();
            $div_componente = '<div class="titulo_dictamen"> '.($num_principal+=2).'. COMPONENTES</div>';
            $tbl_componente = '';
            if($n>0){
                $componentes = $componentes->result_array();
                $n_componente = 0;
                foreach($componentes as $fila){
                    $n_componente++;
                    $tbl_componente .='
                    <div class="contenedor_datos">
                        <table class="table_contenedor">
                            <tr class="collapse_t">
                                <td style="width:75%;" class="fila_unitaria titulo_dictamen">
                                    <b> '.$n_componente.'. COMPONENTE</b>
                                </td>
                                <td style="width:25%;"></td>
                            </tr>
                        </table>
                    </div>
                    <div class="contenedor_datos">
                        <table class="table_contenedor">
                            <tr class="collapse_t">
                                <td style="width:18%;background-color:#ac9fae;font-weight: bold;">
                                    <b>COMPONENTE</b>
                                </td>
                                <td class="fila_unitaria">'.$fila['com_componente'].'</td>
                                <td style="width:18%;background-color:#ac9fae;font-weight: bold;"> 
                                    <b>PONDERACI&Oacute;N %</b>
                                </td>
                                <td class="fila_unitaria">'.$fila['com_ponderacion'].'</td>
                            </tr>
                        </table>
                    </div>
                    '; 
                };
                $div_componente .= $tbl_componente;
            }else{
                $tbl_componente .='
                    <div class="contenedor_datos">
                        <table class="table_contenedor">
                            <tr class="collapse_t" style="font-weight: bold;">
                                <td>Sin Componentes</td>
                            </tr>
                        </table>
                    </div>';
                $div_componente .= $tbl_componente;
            }
        }else{
            $div_componente = '';
        }
        return $div_componente;
    }

    public function producto($id, $gestion,$n_componente)
    {
        $com_id = $id;
        $productos = $this->model_objetivo->get_productos_dictamen($com_id);
        if ($productos->num_rows()>0) {
            $productos = $productos->result_array();
            $div_producto = '<div class="titulo_dictamen"> 12. PRODUCTOS</div>';
            $div_producto = '';
            $tbl_producto = '';
            $n_producto = 0;
            $div_producto .= '';
            foreach ($productos as $fila) {
                $n_producto++;
                // $titulo_producto .='';
                $tbl_producto .='
                    <div class="contenedor_datos">
                        <table class="table_contenedor">
                            <tr class="collapse_t">
                                <td style="width:50%;" class="fila_unitaria titulo_dictamen">
                                    <b> '.$n_componente.'.'.$n_producto.'. PRODUCTO</b>
                                </td>
                                <td style="width:50%;"></td>
                            </tr>
                        </table>
                    </div>
                    <div class="contenedor_datos">
                        <table class="table_contenedor">
                            <tr class="collapse_t" style="background-color:#ac9fae;font-weight: bold;">
                                <td style="width:4%;">Nro.</td>
                                <td>OBJETIVO DEL PRODUCTO</td>
                                <td style="width:4%;">T.I.</td>
                                <td>INDICADOR</td>
                                <td>L/B</td>
                                <td>META</td>
                                <td>%P</td>
                                <td style="width:50%;">TEMPORALIZACI�N DE LA META '.$fila['prod_id'].'</td>
                            </tr>
                            <tr class="collapse_t">
                                <td>'.$n_producto.'</td>
                                <td>'.$fila['pro_producto'].'</td>
                                <td>'.$fila['indi_abreviacion'].'</td>
                                <td>'.$fila['prod_indicador'].'</td>
                                <td>'.$fila['prod_linea_base'].'</td>
                                <td>'.$fila['prod_meta'].'</td>
                                <td>'.$fila['prod_ponderacion'].'</td>
                                <td> '.$this->temporalizacion_metas_producto($fila['prod_id'],$gestion).'</td>
                            </tr>
                        </table>
                    </div>
                    '.$this->actividad($fila['prod_id'],$gestion,$n_componente,$n_producto).'';
            };
            $div_producto .= $tbl_producto;
        }else{
            $div_producto = '';
        };     
        
        return $div_producto;
    }

/*====================================================================================================================*/

/*========================== REPORTE FINANCIERO ANUAL- PULRIANUAL=============*/
    public function programacion_financiera($modulo, $fase_activa, $proy_id)
    {
        $fase = $this->model_faseetapa->get_id_fase($proy_id)[0];//DATOS DE LA FASE
        if ($fase['pfec_ejecucion'] == 1)  //// DIRECTO
        {
            if($modulo==1)
            {
                redirect('admin/financiero_directo/'.$proy_id.''); ///// Reporte Anual
            }
            elseif($modulo==4)
            {
                redirect('admin/reporte_proy_ejecfin_m/'.$proy_id.''); ///// Reporte Pluri Anual
            }
        } 
        elseif ($fase['pfec_ejecucion'] == 2) //// DELEGADO
        {
            if($modulo==1)
            {
                redirect('admin/financiero_delegado/'.$proy_id.''); ///// Reporte Anual
            }
            elseif($modulo==4)
            {
                redirect('admin/reporte_proy_ejecfin_m/'.$proy_id.''); ///// Reporte Pluri Anual
            }
        }
    }

/*==================================== REPORTE PROGRAMACION FISICA - ANUAL - PLURIANUAL ===============================*/
    public function programacion_fisica($mod,$id_f,$id_p){
        if($mod==1){
            redirect('admin/prog/ejecucion_pfisico/'.$mod.'/'.$id_f.'/'.$id_p.''); ///// Reporte Anual
        }
        elseif ($mod==4){
            redirect('admin/prog/ejecucion_pfisico_m/'.$mod.'/'.$id_f.'/'.$id_p.''); ///// Reporte Pluri Anual
        }
    }


/*==================================== REPORTE PROGRAMACION Y EJECUCION DEL PROYECTO ANUAL=================================*/
    public function reporte_programacion_fisica($mod,$id_f,$id_p){
        $data['pfisica']=$this->programacion_fisica_anual($mod,$id_f,$id_p);
        $this->load->view('admin/programacion/reportes/efisica/prog_fis', $data);
    }

    public function programacion_fisica_anual($mod,$id_f,$proy_id){
        $proyecto = $this->model_proyecto->get_id_proyecto($proy_id);
        $fase = $this->model_faseetapa->get_id_fase($proy_id);
        $a�os=$fase[0]['pfec_fecha_fin']-$fase[0]['pfec_fecha_inicio']+1;
        $meses=$a�os*12;
        for($p=1;$p<=$meses;$p++){$app[$p]=0; $aee[$p]=0;} //// vector vacio para sumar a nivel de actividades programacion , ejecutado

        $tabla ='';
        $tabla .='
       <style>
        .table{font-size: 10px;
              width: 100%;
              max-width:1550px;;
        overflow-x: scroll;
        }
        th{
          padding: 1.4px;
          text-align: center;
          font-size: 10px;
        }
      </style>';
      $tabla .='<div class="rojo"></div>
                <div class="verde"></div>
                <table class="change_order_items" style="width:100%;" align="center">
                    <tr>
                        <td width=20%; align=center>
                            <img src="'.base_url().'assets/img/logo.jpg" width="80%"/>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata('entidad').'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$this->gestion.'<br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                            <b>FORMULARIO : </b> PROGRAMACI&Oacute;N Y EJECUCI&Oacute;N F&Iacute;SICA - ANUAL<br>
                            <b>NOMBRE DE OPERACI&Oacute;N : </b> '.$proyecto[0]['proy_nombre'].'<br>
                            <b>APERTURA PROGRAMATICA : </b> '.$proyecto[0]['aper_programa'].' '.$proyecto[0]['aper_proyecto'].' '.$proyecto[0]['aper_actividad'].'<br>
                        </td>
                        <td width=30%; align=center>
                            <img src="'.base_url().'assets/img/escudo.jpg" alt="" width="40%">
                        </td>
                    </tr>
                </table><hr>';
        for($p=1;$p<=$meses;$p++){$cpp[$p]=0; $cpe[$p]=0;} //// vector vacio para sumar a nivel de productos programacion , ejecutado
        for($p=1;$p<=$meses;$p++){$ppp[$p]=0; $ppe[$p]=0;} //// vector vacio para sumar a nivel de productos programacion , ejecutado
        for($p=1;$p<=$meses;$p++){$app[$p]=0; $aee[$p]=0;} //// vector vacio para sumar a nivel de actividades programacion , ejecutado
        $nro_c = $this->model_componente->componentes_nro($id_f); //// nro de componente
        if ($nro_c>0){
            for($p=1;$p<=$meses;$p++){$cpp[$p]=0; $cpe[$p]=0;} //// vector vacio para sumar a nivel de productos programacion , ejecutado
                $componentes = $this->model_componente->componentes_id($id_f);
                $nro_c=1;
                $gran_total_programado=0;
                $gran_total_ejecutado =0;
                $gran_total_acumulado_ejec=0;
                $gran_total_acumulado_prog=0;
                foreach ($componentes as $rowc){
                    $tabla .='
                        <table class="change_order_items" border=1 style="width:100%;" align="center">
                        <thead>
                            <tr class="modo1" >
                              <th bgcolor="#504c49" colspan=11 style="text-align: left;" align="left"><font color=#fff>COMPONENTE : '.$rowc['com_componente'].'</font></th>
                            </tr>';
                        $nrop=0;
                        $productos = $this->model_producto->list_prod($rowc['com_id']);
                        for($p=1;$p<=$meses;$p++){$ppp[$p]=0; $ppe[$p]=0;} //// vector vacio para sumar a nivel de productos programacion , ejecutado
                        /*--------------------------- Producto --------------------------*/
                        foreach ($productos as $rowp){
                            $nrop++;
                            $tabla.='<tr><th bgcolor="#504c49" colspan=11 style="text-align: left;" align="left"><font color=#fff>PRODUCTO : '.$rowp['prod_producto'].'</font></th></tr>
                            <tr><th bgcolor="#504c49" style="width:1%;" rowspan="2"><font color=#fff>NRO.</font></th>
                              <th bgcolor="#504c49" style="width:5%;" rowspan="2"><font color=#fff>ACTIVIDAD</font></th>
                              <th bgcolor="#504c49" style="width:5%;" rowspan="2"><font color=#fff>TIP. INDICADOR</font></th>
                              <th bgcolor="#504c49" style="width:5%;" rowspan="2"><font color=#fff>INDICADOR</font></th>
                              <th bgcolor="#504c49" style="width:5%;" rowspan="2"><font color=#fff>LINEA BASE</font></th>
                              <th bgcolor="#504c49" style="width:5%;" rowspan="2"><font color=#fff>META TOTAL</font></th>
                              <th bgcolor="#504c49" style="width:5%;" colspan="3"><font color=#fff>META '.$this->session->userdata('gestion').'</font></th>
                              <th bgcolor="#504c49" style="width:5%;" rowspan="2"><font color=#fff>PONDERACI&Oacute;N</font></th>
                              <th bgcolor="#504c49" style="width:54%;" rowspan="2"><font color=#fff>TEMPORALIDAD</font></th>                             
                            </tr>
                            <tr class="modo1" align=center>
                              <th bgcolor="#504c49" style="width:5%;"><font color=#fff>INICIAL</font></th>
                              <th bgcolor="#504c49" style="width:5%;"><font color=#fff>MODIF.</font></th>
                              <th bgcolor="#504c49" style="width:5%;"><font color=#fff>VIGENTE</font></th>
                            </tr>
                                    </thead>
                                    <tbody>';
                            /*--------------------------- Actividad --------------------------*/
                            for($p=1;$p<=$meses;$p++){$app[$p]=0; $aee[$p]=0;} //// vector vacio para sumar a nivel de actividades programacion , ejecutado
                                $nroa=0;
                                $actividad = $this->model_actividad->list_act_anual($rowp['prod_id']);
                                foreach ($actividad as $rowa){
                                    $nroa++;
                                    $ti='';if($rowa['indi_id']==2){ $ti='%';}
                                    $meta_gestion=$this->model_actividad->meta_act_gest($rowa['act_id']);
                                    $meta_gest=$meta_gestion[0]['meta_gest']+$rowa['act_linea_base'];

                                    $tabla .='<tr class="modo1">
                                        <td align=center>'.$nrop.'.'.$nroa.'</td>
                                        <td align=center>'.$rowa['act_actividad'].'</td>
                                        <td align=center>'.$rowa['indi_abreviacion'].'</td>
                                        <td align=center>'.$rowa['act_indicador'].'</td>
                                        <td align=center>'.$rowa['act_linea_base'].'</td>
                                        <td align=center>'.$rowa['act_meta'].'</td>
                                        <td align=center>'.$meta_gest.'</td>
                                        <td></td>
                                        <td >'.$meta_gest.'</td>
                                        <td align=center>'.$rowa['act_ponderacion'].'%</td>
                                        <td align=center>';
                                            $pa=0; $pe=0; $gestion=$fase[0]['pfec_fecha_inicio']; $a=1;$e=1;
                                      for($k=1;$k<=$a�os;$k++){ 
                                        $programado=$this->model_actividad->act_prog_mensual($rowa['act_id'],$gestion); /// programado
                                        $ejecutado=$this->model_actividad->act_ejec_mensual($rowa['act_id'],$gestion); /// ejecutado
                                        $nro=0;
                                        foreach($programado as $row){
                                          $nro++;
                                          $matriz [1][$nro]=$row['m_id'];
                                          $matriz [2][$nro]=$row['pg_fis'];
                                        }
                                        /*---------------- llenando la matriz vacia --------------*/
                                        for($j = 1; $j<=12; $j++){
                                          $matriz_r[1][$j]=$j;
                                          $matriz_r[2][$j]='0';  //// P
                                          $matriz_r[3][$j]='0';  //// PA
                                          $matriz_r[4][$j]='0';  //// %PA
                                          $matriz_r[5][$j]='0';  //// A
                                          $matriz_r[6][$j]='0';  //// B
                                          $matriz_r[7][$j]='0';  //// E
                                          $matriz_r[8][$j]='0';  //// EA
                                          $matriz_r[9][$j]='0';  //// %EA
                                          $matriz_r[10][$j]='0'; //// EFICACIA
                                        }
                                        /*--------------------------------------------------------*/
                                       /*--------------------ejecutado gestion ------------------*/
                                        $nro_e=0;
                                        foreach($ejecutado as $row){
                                          $nro_e++;
                                          $matriz_e [1][$nro_e]=$row['m_id'];
                                          $matriz_e [2][$nro_e]=$row['ejec_fis'];
                                          $matriz_e [3][$nro_e]=$row['ejec_fis_a'];
                                          $matriz_e [4][$nro_e]=$row['ejec_fis_b'];
                                        }
                                        /*--------------------------------------------------------*/
                                        /*------- asignando en la matriz P, PA, %PA ----------*/
                                        for($i = 1 ;$i<=$nro ;$i++){
                                          for($j = 1 ;$j<=12 ;$j++){
                                            if($matriz[1][$i]==$matriz_r[1][$j]){
                                                $matriz_r[2][$j]=round($matriz[2][$i],2);
                                            }
                                          }
                                        }

                                        for($j = 1 ;$j<=12 ;$j++){
                                          $pa=$pa+$matriz_r[2][$j];
                                          $matriz_r[3][$j]=$pa+$rowa['act_linea_base'];
                                          if($rowa['act_meta']!=0){
                                            $matriz_r[4][$j]=round(((($pa+$rowa['act_linea_base'])/$rowa['act_meta'])*100),2);
                                          }
                                          
                                        }
                                        /*-------------------------------------------------*/
                                        /*--------------- EJECUCION ----------------------------------*/
                                        if($rowa['indi_id']==1){
                                          for($i = 1 ;$i<=$nro_e ;$i++){
                                            for($j = 1 ;$j<=12 ;$j++){
                                              if($matriz_e[1][$i]==$matriz_r[1][$j]){
                                                  $matriz_r[7][$j]=round($matriz_e[2][$i],2);
                                              }
                                            }
                                          }
                                        }
                                        elseif ($rowa['indi_id']==2){
                                          for($i = 1 ;$i<=$nro_e ;$i++){
                                              for($j = 1 ;$j<=12 ;$j++){
                                                if($matriz_e[1][$i]==$matriz_r[1][$j]){
                                                  $matriz_r[5][$j]=round($matriz_e[3][$i],2);
                                                  $matriz_r[6][$j]=round($matriz_e[4][$i],2);
                                                  $matriz_r[7][$j]=round($matriz_e[2][$i],2);
                                                }
                                              }
                                            }
                                          /*--------------------------------------------------------*/
                                          }
                                          /*--------------------matriz E,AE,%AE gestion ------------------*/
                                            for($j = 1 ;$j<=12 ;$j++){
                                              $pe=$pe+$matriz_r[7][$j];
                                              $matriz_r[8][$j]=$pe+$rowa['act_linea_base'];
                                              if($rowa['act_meta']!=0){
                                                $matriz_r[9][$j]=round(((($pe+$rowa['act_linea_base'])/$rowa['act_meta'])*100),2);
                                              }
                                              
                                              if($matriz_r[4][$j]==0)
                                                {$matriz_r[10][$j]=round((($matriz_r[9][$j])*100),2);}
                                                else{
                                                $matriz_r[10][$j]=round((($matriz_r[9][$j]/$matriz_r[4][$j])*100),2);
                                                }
                                            }
                                          
                                           // $ap[$n]=$matriz_r[2][1]*$rowa['act_ponderacion']; $n++;
                                            $ap[$a]=(($matriz_r[4][1]*$rowa['act_ponderacion'])/100); $a++;
                                            $ae[$e]=(($matriz_r[9][1]*$rowa['act_ponderacion'])/100); $e++;

                                            $ap[$a]=(($matriz_r[4][2]*$rowa['act_ponderacion'])/100); $a++;
                                            $ae[$e]=(($matriz_r[9][2]*$rowa['act_ponderacion'])/100); $e++;

                                            $ap[$a]=(($matriz_r[4][3]*$rowa['act_ponderacion'])/100); $a++;
                                            $ae[$e]=(($matriz_r[9][3]*$rowa['act_ponderacion'])/100); $e++;

                                            $ap[$a]=(($matriz_r[4][4]*$rowa['act_ponderacion'])/100); $a++;
                                            $ae[$e]=(($matriz_r[9][4]*$rowa['act_ponderacion'])/100); $e++;

                                            $ap[$a]=(($matriz_r[4][5]*$rowa['act_ponderacion'])/100); $a++;
                                            $ae[$e]=(($matriz_r[9][5]*$rowa['act_ponderacion'])/100); $e++;

                                            $ap[$a]=(($matriz_r[4][6]*$rowa['act_ponderacion'])/100); $a++;
                                            $ae[$e]=(($matriz_r[9][6]*$rowa['act_ponderacion'])/100); $e++;

                                            $ap[$a]=(($matriz_r[4][7]*$rowa['act_ponderacion'])/100); $a++;
                                            $ae[$e]=(($matriz_r[9][7]*$rowa['act_ponderacion'])/100); $e++;

                                            $ap[$a]=(($matriz_r[4][8]*$rowa['act_ponderacion'])/100); $a++;
                                            $ae[$e]=(($matriz_r[9][8]*$rowa['act_ponderacion'])/100); $e++;

                                            $ap[$a]=(($matriz_r[4][9]*$rowa['act_ponderacion'])/100); $a++;
                                            $ae[$e]=(($matriz_r[9][9]*$rowa['act_ponderacion'])/100); $e++;

                                            $ap[$a]=(($matriz_r[4][10]*$rowa['act_ponderacion'])/100); $a++;
                                            $ae[$e]=(($matriz_r[9][10]*$rowa['act_ponderacion'])/100); $e++;

                                            $ap[$a]=(($matriz_r[4][11]*$rowa['act_ponderacion'])/100); $a++;
                                            $ae[$e]=(($matriz_r[9][11]*$rowa['act_ponderacion'])/100); $e++;

                                            $ap[$a]=(($matriz_r[4][12]*$rowa['act_ponderacion'])/100); $a++;
                                            $ae[$e]=(($matriz_r[9][12]*$rowa['act_ponderacion'])/100); $e++;
                                            
                                            if($gestion==$this->session->userdata('gestion')){
                                                $gestion_ant=$gestion-1;
                                                $act_total=$this->model_actividad->act_prog_total($rowa['act_id']);
                                                $gran_total_programado+=$act_total->total;

                                                $act_total_acum=$this->model_actividad->act_prog_acumulada($rowa['act_id'],$gestion);
                                                $gran_total_acumulado_prog+=$act_total_acum->total_acum;

                                                //Ejecutado
                                                $act_total_ejec=$this->model_actividad->act_ejec_total($rowa['act_id']);
                                                $gran_total_ejecutado+=$act_total_ejec->total;

                                                $act_total_ejec_acum=$this->model_actividad->act_ejec_acumulada($rowa['act_id'],$gestion);
                                                $gran_total_acumulado_ejec+=$act_total_ejec_acum->total_acum;
                                                $total_activid_ejec=0;
                                                if($act_total->total!=0){$total_activid_ejec=$act_total_ejec_acum->total_acum/$act_total->total*100;}

                                                $tabla .= ' 
                                             <table class="change_order_items" border=1 style="width:100%;" align="center">
                                                <tr class="modo1" bgcolor=#504c49>
                                                    <th colspan="14"><font color="#fff">Gesti&oacute;n '.$gestion.'</font></th>
                                                </tr>
                                                <tr class="modo1" bgcolor=#504c49>
                                                    <th><font color="#fff">P/E</font></th>
                                                    <th><font color="#fff">Dic. '.$gestion_ant.'</font></th>
                                                    <th><font color="#fff">Ene.</font></th>
                                                    <th><font color="#fff">Feb.</font></th>
                                                    <th><font color="#fff">Mar.</font></th>
                                                    <th><font color="#fff">Abr.</font></th>
                                                    <th><font color="#fff">May.</font></th>
                                                    <th><font color="#fff">Jun.</font></th>
                                                    <th><font color="#fff">Jul.</font></th>
                                                    <th><font color="#fff">Ago.</font></th>
                                                    <th><font color="#fff">Sep.</font></th>
                                                    <th><font color="#fff">Oct.</font></th>
                                                    <th><font color="#fff">Nov.</font></th>
                                                    <th><font color="#fff">Dic.</font></th>
                                                </tr>
                                                <tr bgcolor="#F5F5DC" class="modo1">
                                                    <td style="width:1%;">PROG.</td>
                                                    <td ><center>'.round($act_total_acum->total_acum,2).' '.$ti.'</center></td>
                                                    <td ><center>'.$matriz_r[2][1].' '.$ti.'</center></td>
                                                    <td ><center>'.$matriz_r[2][2].' '.$ti.'</center></td>
                                                    <td ><center>'.$matriz_r[2][3].' '.$ti.'</center></td>
                                                    <td ><center>'.$matriz_r[2][4].' '.$ti.'</center></td>
                                                    <td ><center>'.$matriz_r[2][5].' '.$ti.'</center></td>
                                                    <td ><center>'.$matriz_r[2][6].' '.$ti.'</center></td>
                                                    <td ><center>'.$matriz_r[2][7].' '.$ti.'</center></td>
                                                    <td ><center>'.$matriz_r[2][8].' '.$ti.'</center></td>
                                                    <td ><center>'.$matriz_r[2][9].' '.$ti.'</center></td>
                                                    <td ><center>'.$matriz_r[2][10].' '.$ti.'</center></td>
                                                    <td ><center>'.$matriz_r[2][11].' '.$ti.'</center></td>
                                                    <td ><center>'.$matriz_r[2][12].' '.$ti.'</center></td>
                                                </tr>
                                                <!--
                                                <tr bgcolor="#F5F5DC" class="modo1">
                                                    <td style="width:1%;">P.A.</td>
                                                    <td ><center>'.$matriz_r[3][1].'</center></td>
                                                    <td ><center>'.$matriz_r[3][2].'</center></td>
                                                    <td ><center>'.$matriz_r[3][3].'</center></td>
                                                    <td ><center>'.$matriz_r[3][4].'</center></td>
                                                    <td ><center>'.$matriz_r[3][5].'</center></td>
                                                    <td ><center>'.$matriz_r[3][6].'</center></td>
                                                    <td ><center>'.$matriz_r[3][7].'</center></td>
                                                    <td ><center>'.$matriz_r[3][8].'</center></td>
                                                    <td ><center>'.$matriz_r[3][9].'</center></td>
                                                    <td ><center>'.$matriz_r[3][10].'</center></td>
                                                    <td ><center>'.$matriz_r[3][11].'</center></td>
                                                    <td ><center>'.$matriz_r[3][12].'</center></td>
                                                </tr>
                                                <tr bgcolor="#F5F5DC" class="modo1">
                                                    <td style="width:1%;">%P.A.</td>
                                                    <td ><center>'.$matriz_r[4][1].'%</center></td>
                                                    <td ><center>'.$matriz_r[4][2].'%</center></td>
                                                    <td ><center>'.$matriz_r[4][3].'%</center></td>
                                                    <td ><center>'.$matriz_r[4][4].'%</center></td>
                                                    <td ><center>'.$matriz_r[4][5].'%</center></td>
                                                    <td ><center>'.$matriz_r[4][6].'%</center></td>
                                                    <td ><center>'.$matriz_r[4][7].'%</center></td>
                                                    <td ><center>'.$matriz_r[4][8].'%</center></td>
                                                    <td ><center>'.$matriz_r[4][9].'%</center></td>
                                                    <td ><center>'.$matriz_r[4][10].'%</center></td>
                                                    <td ><center>'.$matriz_r[4][11].'%</center></td>
                                                    <td ><center>'.$matriz_r[4][12].'%</center></td>
                                                </tr>
-->
                                                <tr bgcolor="#F5F5DC" class="modo1">
                                                    <td style="width:1%;">EJEC.</td>
                                                    <td ><center>'.round($act_total_ejec_acum->total_acum,2).'</center></td>
                                                    <td ><center>'.$matriz_r[7][1].' '.$ti.'</center></td>
                                                    <td ><center>'.$matriz_r[7][2].' '.$ti.'</center></td>
                                                    <td ><center>'.$matriz_r[7][3].' '.$ti.'</center></td>
                                                    <td ><center>'.$matriz_r[7][4].' '.$ti.'</center></td>
                                                    <td ><center>'.$matriz_r[7][5].' '.$ti.'</center></td>
                                                    <td ><center>'.$matriz_r[7][6].' '.$ti.'</center></td>
                                                    <td ><center>'.$matriz_r[7][7].' '.$ti.'</center></td>
                                                    <td ><center>'.$matriz_r[7][8].' '.$ti.'</center></td>
                                                    <td ><center>'.$matriz_r[7][9].' '.$ti.'</center></td>
                                                    <td ><center>'.$matriz_r[7][10].' '.$ti.'</center></td>
                                                    <td ><center>'.$matriz_r[7][11].' '.$ti.'</center></td>
                                                    <td ><center>'.$matriz_r[7][12].' '.$ti.'</center></td>
                                                </tr>
                                                <tr bgcolor="#F5F5DC" class="modo1">
                                                    <td style="width:1%;">E.A.</td>
                                                    <td ><center>'.round($act_total_ejec_acum->total_acum,2).'</center></td>
                                                    <td ><center>'.$matriz_r[8][1].' '.$ti.'</center></td>
                                                    <td ><center>'.$matriz_r[8][2].' '.$ti.'</center></td>
                                                    <td ><center>'.$matriz_r[8][3].' '.$ti.'</center></td>
                                                    <td ><center>'.$matriz_r[8][4].' '.$ti.'</center></td>
                                                    <td ><center>'.$matriz_r[8][5].' '.$ti.'</center></td>
                                                    <td ><center>'.$matriz_r[8][6].' '.$ti.'</center></td>
                                                    <td ><center>'.$matriz_r[8][7].' '.$ti.'</center></td>
                                                    <td ><center>'.$matriz_r[8][8].' '.$ti.'</center></td>
                                                    <td ><center>'.$matriz_r[8][9].' '.$ti.'</center></td>
                                                    <td ><center>'.$matriz_r[8][10].' '.$ti.'</center></td>
                                                    <td ><center>'.$matriz_r[8][11].' '.$ti.'</center></td>
                                                    <td ><center>'.$matriz_r[8][12].' '.$ti.'</center></td>
                                                </tr>
                                                <tr bgcolor="#F5F5DC" class="modo1">
                                                    <td style="width:1%;">%E.A.</td>
                                                    <td ><center>'.round($total_activid_ejec,2).' %</center></td>
                                                    <td ><center>'.$matriz_r[9][1].'%</center></td>
                                                    <td ><center>'.$matriz_r[9][2].'%</center></td>
                                                    <td ><center>'.$matriz_r[9][3].'%</center></td>
                                                    <td ><center>'.$matriz_r[9][4].'%</center></td>
                                                    <td ><center>'.$matriz_r[9][5].'%</center></td>
                                                    <td ><center>'.$matriz_r[9][6].'%</center></td>
                                                    <td ><center>'.$matriz_r[9][7].'%</center></td>
                                                    <td ><center>'.$matriz_r[9][8].'%</center></td>
                                                    <td ><center>'.$matriz_r[9][9].'%</center></td>
                                                    <td ><center>'.$matriz_r[9][10].'%</center></td>
                                                    <td ><center>'.$matriz_r[9][11].'%</center></td>
                                                    <td ><center>'.$matriz_r[9][12].'%</center></td>
                                                </tr>
                                            </table>';
                                            }
                                            
                                            $gestion++;
                                        }

                                        for($k=1;$k<=$meses;$k++){
                                           $app[$k]=round(($app[$k]+$ap[$k]),1); //// sumando a nivel de las actividades
                                           $aee[$k]=round(($aee[$k]+$ae[$k]),1); //// sumando a nivel de las actividades
                                        }
                                        $tabla .= '<br>';
                                        $tabla .='</td>';
                                    $tabla .'</tr>';
                                }
                                for($kp=1;$kp<=$meses;$kp++){
                                    $ppp[$kp]=round(($ppp[$kp]+($app[$kp]*$rowp['prod_ponderacion'])/100),1); //// sumando a nivel de los productos
                                    $ppe[$kp]=round(($ppe[$kp]+($aee[$kp]*$rowp['prod_ponderacion'])/100),1); //// sumando a nivel de los productos
                                }
                            /*--------------------------- End Actividad --------------------------*/
                        }
                        for($k=1;$k<=$meses;$k++){
                            $cpp[$k]=round(($cpp[$k]+($ppp[$k]*$rowc['com_ponderacion'])/100),0); //// sumando a nivel de los Componentes
                            $cpe[$k]=round(($cpe[$k]+($ppe[$k]*$rowc['com_ponderacion'])/100),0); //// sumando a nivel de los Componentes
                        }
                        /*--------------------------- End Producto --------------------------*/

                    $tabla .='</tbody>
                    </table>';
                }
                $i=1;$meses=12;$gestion=$fase[0]['pfec_fecha_inicio'];
                $tabla .='<hr><b>TABLA DE EFICACIA ANUAL</b>';
                for($kp=1;$kp<=$a�os;$kp++){
                    if($gestion==$this->session->userdata('gestion')){
                        $gestion_ant=$gestion-1;
                        $tabla .='
                        <table class="change_order_items" border=1>
                          <thead>
                            <tr class="modo1" align=center bgcolor="#504c49">
                                <th colspan="14"><font color="#fff">GESTI&Oacute;N '.$gestion.'</font></th>
                            </tr>
                            <tr class="modo1" align=center bgcolor="#504c49">
                                <th><font color="#fff">P/E</font></th>
                                <th><font color="#fff">Dic. (Gesti&oacute;n '.$gestion_ant.')</font></th>
                                <th><font color="#fff">Ene.</font></th>
                                <th><font color="#fff">Feb.</font></th>
                                <th><font color="#fff">Mar.</font></th>
                                <th><font color="#fff">Abr.</font></th>
                                <th><font color="#fff">May.</font></th>
                                <th><font color="#fff">Jun.</font></th>
                                <th><font color="#fff">Jul.</font></th>
                                <th><font color="#fff">Ago.</font></th>
                                <th><font color="#fff">Sep.</font></th>
                                <th><font color="#fff">Oct.</font></th>
                                <th><font color="#fff">Nov.</font></th>
                                <th><font color="#fff">Dic.</font></th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr class="modo1">
                                <td><b>PROGRAMACI&Oacute;N ACUMULADA EN </b></td>';
                        $total_activid=0;
                        if($gran_total_programado!=0){$total_activid=round($gran_total_acumulado_prog/$gran_total_programado*100,2);}
                        $tabla .='<td>'.$total_activid.'%</td>';
                                for($pos=$i;$pos<=$meses;$pos++){
                                    $tabla .= '<td>'.$cpp[$pos].'%</td>';
                                }
                                $tabla .= '
                            </tr>
                            <tr class="modo1">
                                <td><b>EJECUCI&Oacute;N ACUMULADA EN %</b></td>';
                        $total_activid_ejec=0;
                        if($gran_total_programado!=0){$total_activid_ejec=round($gran_total_acumulado_ejec/$gran_total_programado*100,2);}

                        $tabla .='<td>'.$total_activid_ejec.'%</td>';
                                for($pos=$i;$pos<=$meses;$pos++){
                                    $tabla .= '<td bgcolor="#98FB98">'.$cpe[$pos].'%</td>';
                                }
                                $tabla .= '
                            </tr>
                            <tr class="modo1">
                                <td><b>EFICACIA</b></td>';
                        $eficacia=0;
                        if ($total_activid!=0){$eficacia=round($total_activid_ejec/$total_activid*100,2);}
                        $tabla .='<td>'.$eficacia.'%</td>';
                                for($pos=$i;$pos<=$meses;$pos++){
                                    if($cpp[$pos]==0){$cpp[$pos]=1;}
                                    $tabla .= '<td bgcolor="#B0E0E6">'.round((($cpe[$pos]/$cpp[$pos])*100),1).'%</td>';
                                }
                                $tabla .= '
                            </tr>
                          </tbody>
                          </table>';
                    }
                    $i=$meses+1;
                    $meses=$meses+12;
                    $gestion++; 
                }

        }

        return $tabla;
    }


    

/*==================================== REPORTE PROGRAMACION Y EJECUCION DEL PROYECTO MULTI ANUAL =================================*/
    public function reporte_programacion_fisica_m($mod,$id_f,$id_p){
        $data['pfisica']=$this->programacion_fisica_pluri($mod,$id_f,$id_p);
        $this->load->view('admin/programacion/reportes/efisica/prog_fis', $data);
    }

    public function programacion_fisica_pluri($mod,$id_f,$proy_id){
        $proyecto = $this->model_proyecto->get_id_proyecto($proy_id);
        $fase = $this->model_faseetapa->get_id_fase($proy_id);
        $a�os=$fase[0]['pfec_fecha_fin']-$fase[0]['pfec_fecha_inicio']+1;
        $meses=$a�os*12;
        for($p=1;$p<=$meses;$p++){$app[$p]=0; $aee[$p]=0;} //// vector vacio para sumar a nivel de actividades programacion , ejecutado

        $tabla ='';
        $tabla .='
       <style>
        .table{font-size: 10px;
              width: 100%;
              max-width:1550px;;
        overflow-x: scroll;
        }
        th{
          padding: 1.4px;
          text-align: center;
          font-size: 10px;
        }
      </style>';
      $tabla .='<div class="rojo"></div>
                <div class="verde"></div>
                <table class="change_order_items" style="width:100%;" align="center">
                    <tr>
                        <td width=20%; align=center>
                            <img src="'.base_url().'assets/img/logo.jpg" width="80%"/>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata('entidad').'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$this->gestion.'<br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                            <b>FORMULARIO : </b> PROGRAMACI&Oacute;N Y EJECUCI&Oacute;N F&Iacute;SICA - PLURIANUAL<br>
                            <b>NOMBRE DE LA OPERACI&Oacute;N : </b> '.$proyecto[0]['proy_nombre'].'<br>
                            <b>APERTURA PROGRAMATICA : </b> '.$proyecto[0]['aper_programa'].' '.$proyecto[0]['aper_proyecto'].' '.$proyecto[0]['aper_actividad'].'<br>
                        </td>
                        <td width=30%; align=center>
                            <img src="'.base_url().'assets/img/escudo.jpg" alt="" width="40%">
                        </td>
                    </tr>
                </table><hr>';
        for($p=1;$p<=$meses;$p++){$cpp[$p]=0; $cpe[$p]=0;} //// vector vacio para sumar a nivel de productos programacion , ejecutado
        for($p=1;$p<=$meses;$p++){$ppp[$p]=0; $ppe[$p]=0;} //// vector vacio para sumar a nivel de productos programacion , ejecutado
        for($p=1;$p<=$meses;$p++){$app[$p]=0; $aee[$p]=0;} //// vector vacio para sumar a nivel de actividades programacion , ejecutado
        $nro_c = $this->model_componente->componentes_nro($id_f); //// nro de componente
        if ($nro_c>0){
            for($p=1;$p<=$meses;$p++){$cpp[$p]=0; $cpe[$p]=0;} //// vector vacio para sumar a nivel de productos programacion , ejecutado
                $componentes = $this->model_componente->componentes_id($id_f);
                $nro_c=1;
                
                foreach ($componentes as $rowc){
                    $tabla .='<table class="change_order_items" border=1 style="width:100%;" align="center">
                        <thead>
                            <tr class="modo1" >
                              <th bgcolor="#504c49" colspan=11 style="text-align: left;" align="left"><font color=#fff>COMPONENTE : '.$rowc['com_componente'].'</font></th>
                            </tr>';
                        $nrop=0;
                        $productos = $this->model_producto->list_prod($rowc['com_id']);
                        for($p=1;$p<=$meses;$p++){$ppp[$p]=0; $ppe[$p]=0;} //// vector vacio para sumar a nivel de productos programacion , ejecutado
                        /*--------------------------- Producto --------------------------*/
                        foreach ($productos as $rowp){
                            $nrop++;
                            $tabla.='<tr><th bgcolor="#504c49" colspan=11 style="text-align: left;" align="left"><font color=#fff>PRODUCTO : '.$rowp['prod_producto'].'</font></th></tr>
                            <tr><th bgcolor="#504c49" style="width:1%;" ><font color=#fff>NRO.</font></th>
                              <th bgcolor="#504c49" style="width:5%;" ><font color=#fff>ACTIVIDAD</font></th>
                              <th bgcolor="#504c49" style="width:5%;" ><font color=#fff>TIP. INDICADOR</font></th>
                              <th bgcolor="#504c49" style="width:5%;"><font color=#fff>INDICADOR</font></th>
                              <th bgcolor="#504c49" style="width:5%;"><font color=#fff>LINEA BASE</font></th>
                              <th bgcolor="#504c49" style="width:5%;"><font color=#fff>META INICIAL</font></th>
                              <th bgcolor="#504c49" style="width:5%;"><font color=#fff>MODIF.</font></th>
                              <th bgcolor="#504c49" style="width:5%;"><font color=#fff>VIGENTE</font></th>
                              <th bgcolor="#504c49" style="width:5%;"><font color=#fff>PONDERACI&Oacute;N</font></th>
                              <th bgcolor="#504c49" style="width:54%;"><font color=#fff>TEMPORALIDAD</font></th>                             
                            </tr>
                             </thead>
                             <tbody>';

                            /*--------------------------- Actividad --------------------------*/
                            for($p=1;$p<=$meses;$p++){$app[$p]=0; $aee[$p]=0;} //// vector vacio para sumar a nivel de actividades programacion , ejecutado
                                $nroa=0;
                                $actividad = $this->model_actividad->list_act_anual($rowp['prod_id']);
                                foreach ($actividad as $rowa){
                                    $nroa++;
                                    $ti='';if($rowa['indi_id']==2){ $ti='%';}
                                    $meta_gestion=$this->model_actividad->meta_act_gest($rowa['act_id']);
                                    $meta_gest=$meta_gestion[0]['meta_gest']+$rowa['act_linea_base'];

                                    $tabla .='<tr class="modo1">
                                        <td align=center>'.$nrop.'.'.$nroa.'</td>
                                        <td align=center>'.$rowa['act_actividad'].'</td>
                                        <td align=center>'.$rowa['indi_abreviacion'].'</td>
                                        <td align=center>'.$rowa['act_indicador'].'</td>
                                        <td align=center>'.$rowa['act_linea_base'].'</td>
                                        <td align=center>'.$rowa['act_meta'].'</td>
                                        <td align=center></td>
                                        <td align=center></td>
                                        <td align=center>'.$rowa['act_ponderacion'].'%</td>
                                        <td align=center>';
                                    $pa=0; $pe=0; $gestion=$fase[0]['pfec_fecha_inicio']; $a=1;$e=1;
                                    $gestion_cab=$gestion;
                                    $gestion_tbl=$gestion;
                                    $gestion_tbl_aux=$gestion;
                                    $tabla .= ' 
                                             <table class="change_order_items" border=1 style="width:100%;" align="center">
                                                <tr class="modo1" bgcolor=#504c49>
                                                    <th><font color="#fff">P/E</font></th>';
                                    $matriz_gest[1][$gestion_cab-1]='PROG.';
                                    $matriz_gest[2][$gestion_cab-1]='%P.A.';
                                    $matriz_gest[3][$gestion_cab-1]='EJEC.';
                                    $matriz_gest[4][$gestion_cab-1]='%E.A.';
                                    for($k=1;$k<=$a�os;$k++){
                                        $tabla .= '<th><font color="#fff">'.$gestion_cab.'</font></th>';
                                        $matriz_gest[5][$gestion_cab]='0';
                                        $matriz_gest[6][$gestion_cab]='0';
                                        $matriz_gest[7][$gestion_cab]='0';
                                        $matriz_gest[8][$gestion_cab]='0';
                                        $gestion_cab++;
                                    }
                                    $tabla.='</tr>';
                                      for($k=1;$k<=$a�os;$k++){
                                        $programado=$this->model_actividad->act_prog_mensual($rowa['act_id'],$gestion); /// programado
                                        $ejecutado=$this->model_actividad->act_ejec_mensual($rowa['act_id'],$gestion); /// ejecutado
                                        $nro=0;
                                        foreach($programado as $row){
                                          $nro++;
                                          $matriz [1][$nro]=$row['m_id'];
                                          $matriz [2][$nro]=$row['pg_fis'];
                                        }
                                        /*---------------- llenando la matriz vacia --------------*/
                                        for($j = 1; $j<=12; $j++){
                                          $matriz_r[1][$j]=$j;
                                          $matriz_r[2][$j]='0';  //// P
                                          $matriz_r[3][$j]='0';  //// PA
                                          $matriz_r[4][$j]='0';  //// %PA
                                          $matriz_r[5][$j]='0';  //// A
                                          $matriz_r[6][$j]='0';  //// B
                                          $matriz_r[7][$j]='0';  //// E
                                          $matriz_r[8][$j]='0';  //// EA
                                          $matriz_r[9][$j]='0';  //// %EA
                                          $matriz_r[10][$j]='0'; //// EFICACIA
                                        }
                                        /*--------------------------------------------------------*/
                                       /*--------------------ejecutado gestion ------------------*/
                                        $nro_e=0;
                                        foreach($ejecutado as $row){
                                          $nro_e++;
                                          $matriz_e [1][$nro_e]=$row['m_id'];
                                          $matriz_e [2][$nro_e]=$row['ejec_fis'];
                                          $matriz_e [3][$nro_e]=$row['ejec_fis_a'];
                                          $matriz_e [4][$nro_e]=$row['ejec_fis_b'];
                                        }
                                        /*--------------------------------------------------------*/
                                        /*------- asignando en la matriz P, PA, %PA ----------*/
                                        for($i = 1 ;$i<=$nro ;$i++){
                                          for($j = 1 ;$j<=12 ;$j++){
                                            if($matriz[1][$i]==$matriz_r[1][$j]){
                                                $matriz_r[2][$j]=round($matriz[2][$i],2);
                                            }
                                          }
                                        }

                                        for($j = 1 ;$j<=12 ;$j++){
                                          $pa=$pa+$matriz_r[2][$j];
                                          $matriz_r[3][$j]=$pa+$rowa['act_linea_base'];
                                          if($rowa['act_meta']!=0){
                                            $matriz_r[4][$j]=round(((($pa+$rowa['act_linea_base'])/$rowa['act_meta'])*100),2);
                                          }
                                          
                                        }
                                        /*-------------------------------------------------*/
                                        /*--------------- EJECUCION ----------------------------------*/
                                        if($rowa['indi_id']==1){
                                          for($i = 1 ;$i<=$nro_e ;$i++){
                                            for($j = 1 ;$j<=12 ;$j++){
                                              if($matriz_e[1][$i]==$matriz_r[1][$j]){
                                                  $matriz_r[7][$j]=round($matriz_e[2][$i],2);
                                              }
                                            }
                                          }
                                        }
                                        elseif ($rowa['indi_id']==2){
                                          for($i = 1 ;$i<=$nro_e ;$i++){
                                              for($j = 1 ;$j<=12 ;$j++){
                                                if($matriz_e[1][$i]==$matriz_r[1][$j]){
                                                  $matriz_r[5][$j]=round($matriz_e[3][$i],2);
                                                  $matriz_r[6][$j]=round($matriz_e[4][$i],2);
                                                  $matriz_r[7][$j]=round($matriz_e[2][$i],2);
                                                }
                                              }
                                            }
                                          /*--------------------------------------------------------*/
                                          }
                                          /*--------------------matriz E,AE,%AE gestion ------------------*/
                                            for($j = 1 ;$j<=12 ;$j++){
                                              $pe=$pe+$matriz_r[7][$j];
                                              $matriz_r[8][$j]=$pe+$rowa['act_linea_base'];
                                              if($rowa['act_meta']!=0){
                                                $matriz_r[9][$j]=round(((($pe+$rowa['act_linea_base'])/$rowa['act_meta'])*100),2);
                                              }
                                              
                                              if($matriz_r[4][$j]==0)
                                                {$matriz_r[10][$j]=round((($matriz_r[9][$j])*100),2);}
                                                else{
                                                $matriz_r[10][$j]=round((($matriz_r[9][$j]/$matriz_r[4][$j])*100),2);
                                                }
                                            }
                                          
                                           // $ap[$n]=$matriz_r[2][1]*$rowa['act_ponderacion']; $n++;
                                            $ap[$a]=(($matriz_r[4][1]*$rowa['act_ponderacion'])/100); $a++;
                                            $ae[$e]=(($matriz_r[9][1]*$rowa['act_ponderacion'])/100); $e++;

                                            $ap[$a]=(($matriz_r[4][2]*$rowa['act_ponderacion'])/100); $a++;
                                            $ae[$e]=(($matriz_r[9][2]*$rowa['act_ponderacion'])/100); $e++;

                                            $ap[$a]=(($matriz_r[4][3]*$rowa['act_ponderacion'])/100); $a++;
                                            $ae[$e]=(($matriz_r[9][3]*$rowa['act_ponderacion'])/100); $e++;

                                            $ap[$a]=(($matriz_r[4][4]*$rowa['act_ponderacion'])/100); $a++;
                                            $ae[$e]=(($matriz_r[9][4]*$rowa['act_ponderacion'])/100); $e++;

                                            $ap[$a]=(($matriz_r[4][5]*$rowa['act_ponderacion'])/100); $a++;
                                            $ae[$e]=(($matriz_r[9][5]*$rowa['act_ponderacion'])/100); $e++;

                                            $ap[$a]=(($matriz_r[4][6]*$rowa['act_ponderacion'])/100); $a++;
                                            $ae[$e]=(($matriz_r[9][6]*$rowa['act_ponderacion'])/100); $e++;

                                            $ap[$a]=(($matriz_r[4][7]*$rowa['act_ponderacion'])/100); $a++;
                                            $ae[$e]=(($matriz_r[9][7]*$rowa['act_ponderacion'])/100); $e++;

                                            $ap[$a]=(($matriz_r[4][8]*$rowa['act_ponderacion'])/100); $a++;
                                            $ae[$e]=(($matriz_r[9][8]*$rowa['act_ponderacion'])/100); $e++;

                                            $ap[$a]=(($matriz_r[4][9]*$rowa['act_ponderacion'])/100); $a++;
                                            $ae[$e]=(($matriz_r[9][9]*$rowa['act_ponderacion'])/100); $e++;

                                            $ap[$a]=(($matriz_r[4][10]*$rowa['act_ponderacion'])/100); $a++;
                                            $ae[$e]=(($matriz_r[9][10]*$rowa['act_ponderacion'])/100); $e++;

                                            $ap[$a]=(($matriz_r[4][11]*$rowa['act_ponderacion'])/100); $a++;
                                            $ae[$e]=(($matriz_r[9][11]*$rowa['act_ponderacion'])/100); $e++;

                                            $ap[$a]=(($matriz_r[4][12]*$rowa['act_ponderacion'])/100); $a++;
                                            $ae[$e]=(($matriz_r[9][12]*$rowa['act_ponderacion'])/100); $e++;

                                          $matriz_gest[5][$gestion]=$matriz_r[2][1]+$matriz_r[2][2]+$matriz_r[2][3]+$matriz_r[2][4]+$matriz_r[2][5]+$matriz_r[2][6]+$matriz_r[2][7]+$matriz_r[2][8]+$matriz_r[2][9]+$matriz_r[2][10]+$matriz_r[2][11]+$matriz_r[2][12];
                                          $matriz_gest[6][$gestion]=$matriz_r[4][12];
                                          $matriz_gest[7][$gestion]=$matriz_r[7][1]+$matriz_r[7][2]+$matriz_r[7][3]+$matriz_r[7][4]+$matriz_r[7][5]+$matriz_r[7][6]+$matriz_r[7][7]+$matriz_r[7][8]+$matriz_r[7][9]+$matriz_r[7][10]+$matriz_r[7][11]+$matriz_r[7][12];
                                          $matriz_gest[8][$gestion]=$matriz_r[9][12];
                                            $gestion++;
                                        }
                                    $gestion_tbl=$gestion_tbl_aux;
                                    $columna=$gestion_tbl-1;
                                    $h=5;
                                        for($j=1;$j<=4;$j++){
                                            $tabla .= '<tr class="modo1" >
                                                       <td>'.$matriz_gest[$j][$columna].'</td>';
                                            for($k=1;$k<=$a�os;$k++){
                                                $tabla .= '<td>'.$matriz_gest[$h][$gestion_tbl].' '.$ti.'</td>';
                                                $gestion_tbl++;
                                            }
                                            $h++;
                                            $tabla .= '</tr>';
                                            $gestion_tbl=$gestion_tbl_aux;
                                        }

                                    $tabla.='</table>';
                                        for($k=1;$k<=$meses;$k++){
                                           $app[$k]=round(($app[$k]+$ap[$k]),1); //// sumando a nivel de las actividades
                                           $aee[$k]=round(($aee[$k]+$ae[$k]),1); //// sumando a nivel de las actividades
                                        }
                                        //$tabla .= '<br>';
                                        $tabla .='</td>';
                                    $tabla .'</tr>';
                                }
                                for($kp=1;$kp<=$meses;$kp++){
                                    $ppp[$kp]=round(($ppp[$kp]+($app[$kp]*$rowp['prod_ponderacion'])/100),1); //// sumando a nivel de los productos
                                    $ppe[$kp]=round(($ppe[$kp]+($aee[$kp]*$rowp['prod_ponderacion'])/100),1); //// sumando a nivel de los productos
                                }
                            /*--------------------------- End Actividad --------------------------*/
                        }
                        for($k=1;$k<=$meses;$k++){
                            $cpp[$k]=round(($cpp[$k]+($ppp[$k]*$rowc['com_ponderacion'])/100),0); //// sumando a nivel de los Componentes
                            $cpe[$k]=round(($cpe[$k]+($ppe[$k]*$rowc['com_ponderacion'])/100),0); //// sumando a nivel de los Componentes
                        }
                        /*--------------------------- End Producto --------------------------*/

                    $tabla .='</tbody>
                    </table>';
                }
                $i=1;$meses=12;$gestion=$fase[0]['pfec_fecha_inicio'];
                $gestion_cab=$gestion;
                $gestion_tbl_aux=$gestion;
                $tabla .='<hr><b>TABLA DE EFICACIA ANUAL</b>';
                $tabla .='<table class="change_order_items" border=1>
                          <thead>
                            <tr class="modo1" align=center bgcolor="#504c49">
                                <th><font color="#fff">P/E</font></th>';
                for ($kp=1;$kp<=$a�os;$kp++){
                    $tabla.='<th><font color="#fff">'.$gestion.'</font></th>';
                    $gestion++;
                }

                $tabla.='</tr>
                         </thead>
                         <tbody>';
            $matriz_gest_ef[1][$gestion_cab-1]='<b>PROGRAMACI&Oacute;N ACUMULADA EN %</b>';
            $matriz_gest_ef[2][$gestion_cab-1]='<b>EJECUCI&Oacute;N ACUMULADA EN %</b>';
            $matriz_gest_ef[3][$gestion_cab-1]='<b>EFICACIA</b>';

            for($kp=1;$kp<=$a�os;$kp++){
                $matriz_gest_ef[1][$gestion_cab]=$cpp[12];
                $matriz_gest_ef[2][$gestion_cab]=$cpe[12];
                if($cpp[12]==0){$cpp[12]=1;}
                $matriz_gest_ef[3][$gestion_cab]=round((($cpe[12]/$cpp[12])*100),1);
                $gestion_cab++;
            }
            $gestion_tbl=$gestion_tbl_aux;
            $columna=$gestion_tbl-1;
            $h=1;
            $color='';
            for($j=1;$j<=3;$j++){
                $tabla .= '<tr class="modo1" >
                           <td>'.$matriz_gest_ef[$h][$columna].'</td>';
                for($k=1;$k<=$a�os;$k++){
                    $tabla .= '<td '.$color.' >'.$matriz_gest_ef[$h][$gestion_tbl].'%</td>';
                    $gestion_tbl++;
                }
                $h++;
                $tabla .= '</tr>';
                if ($h==2){$color='bgcolor="#98FB98"';}elseif ($h==3){$color='bgcolor="#B0E0E6"';}
                $gestion_tbl=$gestion_tbl_aux;
            }
            $tabla.='</tbody>
            </table>';
        }

        return $tabla;
    }


    public function reporte_programacion_fisica_m2($mod,$id_f,$id_p)
    {
        $proy_id = $id_p;
        $gestion = $this->session->userdata('gestion');
        $dictamen = $this->model_objetivo->get_dictamen($proy_id);
        $unidad_responsable=$this->model_proyecto->responsable_proy($proy_id,1);
        $dictamen = $dictamen->row();
        $aper_programatica = $this->model_objetivo->get_aper_programatica_dictamen($proy_id,$gestion);
        $aper_programatica = $aper_programatica->row();
        $fase = $this->model_faseetapa->get_id_fase($proy_id);
        $a�os=$fase[0]['pfec_fecha_fin']-$fase[0]['pfec_fecha_inicio']+1;
        $meses=$a�os*12;
        for($p=1;$p<=$meses;$p++){$app[$p]=0; $aee[$p]=0;} //// vector vacio para sumar a nivel de actividades programacion , ejecutado
        $num_principal = ($dictamen->tp_id == 1) ? 1 : 0 ;
        $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                     <tr>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/logo.jpg" class="siipp"/>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata('entidad').'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$gestion.'<br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                            <b>FORMULARIO : </b> PROGRAMACI&Oacute;N Y EJECUCI&Oacute;N F&Iacute;SICA - PLURIANUAL<br>
                        </td>
                        <td width=30%;>
                            <img src="'.base_url().'assets/img/escudo.jpg" alt="" width="80px">
                        </td>
                    </tr>
                </table>

                <footer>
                    <table class="table table-bordered" width="100%">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata('sistema_pie').'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>
                <table class="datos_principales" style="table-layout:fixed;" border="1">
                    <tr>
                        <td colspan="2">
                            <b>'.$dictamen->proy_nombre.'</b><br>';
                            if($dictamen->tp_id==1)
                            {$html .= 'Proyecto/Programa/Operaci&oacute;n'; $titulo_componente='COMPONENTE';}
                            elseif ($dictamen->tp_id==2 || $dictamen->tp_id==3) 
                            {$html .= 'Programa'; $titulo_componente='PROCESO';}
                            elseif ($dictamen->tp_id==4) 
                            {$html .= 'Operaci&oacute;n de Funcionamiento';$titulo_componente='PROCESO';}
                            $html .= '
                        </td>
                    </tr>
                    <tr>
                        <td style="width:50%;">';
                            if($dictamen->tp_id==1)
                            {$html .= '<b>'.$dictamen->proy_sisin.'</b><br>';}
                            elseif ($dictamen->tp_id==2 || $dictamen->tp_id==3 || $dictamen->tp_id==4) 
                            {$html .= 'N/A<br>';}
                            $html .= 'SISIN
                        </td>
                        <td style="width:50%; text-align:center;">
                            <b>'.$aper_programatica->programatica.'</b><br>
                            Apertura Programatica
                        </td>
                    </tr>
                    <tr>
                        <td style="width:50%; style="text-align:center;">
                            <b>'.$aper_programatica->uni_unidad.'</b><br>
                            Unidad Responsable
                        </td>
                        <td  style="text-align:center;">
                            <b>'.$unidad_responsable[0]['uresp'].'</b><br>
                            Unidad Ejecutora
                        </td>
                    </tr>
                </table>
                
                <p>';
                for($p=1;$p<=$meses;$p++){$cpp[$p]=0; $cpe[$p]=0;} //// vector vacio para sumar a nivel de productos programacion , ejecutado
                for($p=1;$p<=$meses;$p++){$ppp[$p]=0; $ppe[$p]=0;} //// vector vacio para sumar a nivel de productos programacion , ejecutado
                for($p=1;$p<=$meses;$p++){$app[$p]=0; $aee[$p]=0;} //// vector vacio para sumar a nivel de actividades programacion , ejecutado
                $nro_c = $this->model_componente->componentes_nro($id_f); //// nro de componente
                if ($nro_c>0)
                {
                    $html .= '<div>';
                    for($p=1;$p<=$meses;$p++){$cpp[$p]=0; $cpe[$p]=0;} //// vector vacio para sumar a nivel de productos programacion , ejecutado
                    $componentes = $this->model_componente->componentes_id($id_f);
                    $nro_c=1;
                    foreach ($componentes as $rowc)
                    {   if($dictamen->tp_id!=4)
                        {
                            $html .= '
                            <div class="contenedor_datos">
                                <table class="table_contenedor">
                                    <tr class="collapse_t" >
                                        <td style="width:40%;" class="fila_unitaria titulo_dictamen">
                                            '.$nro_c.'.- '.$titulo_componente.' : '.$rowc['com_componente'].'.
                                        </td>
                                        <td style="width:60%;">'.$rowc['com_ponderacion'].' %</td>
                                    </tr>
                                </table>
                            </div>';
                        }
                       
                        $nro_p = $this->model_producto->productos_nro($rowc['com_id']); //// nro de componente
                        if($nro_p>0)
                        {   
                            $nrop=1;
                            $productos = $this->model_producto->list_prod($rowc['com_id']);
                            for($p=1;$p<=$meses;$p++){$ppp[$p]=0; $ppe[$p]=0;} //// vector vacio para sumar a nivel de productos programacion , ejecutado
                            foreach ($productos as $rowp)
                            {
                                $html .= '
                                <div class="contenedor_datos">
                                    <table class="table_contenedor">
                                        <tr class="collapse_t">
                                            <td style="width:80%;" class="fila_unitaria titulo_dictamen">
                                                 '.$nrop.'.- PRODUCTO : '.$rowp['prod_producto'].'
                                            </td>
                                            <td style="width:20%;">'.$rowp['prod_ponderacion'].' %</td>
                                        </tr>
                                    </table>
                                </div>
                                ';
                                $nro_a = $this->model_actividad->nro_actividades_gest($rowp['prod_id']);
                                if($nro_a>0)
                                {
                                    for($p=1;$p<=$meses;$p++){$app[$p]=0; $aee[$p]=0;} //// vector vacio para sumar a nivel de actividades programacion , ejecutado
                                    $actividad = $this->model_actividad->list_act_anual($rowp['prod_id']);
                                    foreach ($actividad as $rowa)
                                    {
                                    $ti='';if($rowa['indi_id']==2){ $ti='%';}

                                    $html .= '
                                                <div class="contenedor_datos">
                                                <table class="table_contenedor">
                                                    <tr class="collapse_t">
                                                        <td style="width:10%;">ACTIVIDAD</td>
                                                        <td style="width:3%;">T.I.</td>
                                                        <td style="width:7%;">COSTO</td>
                                                        <td style="width:7%;">%P</td>
                                                        <td style="width:5%;">L/B</td>
                                                        <td style="width:5%;">META </td>
                                                        <td style="width:65%;">CRONOGRAMA DE PROGRAMACI&Oacute;N Y EJECUCI&Oacute;N </td>
                                                    </tr>
                                                </table>
                                                </div>

                                                <div class="contenedor_principal"> 
                                                <table class="table_contenedor">
                                                    <tr class="collapse_t">
                                                        <td style="width:10%;">'.$rowa['act_actividad'].'</td>
                                                        <td style="width:3%;">'.$rowa['indi_abreviacion'].'</td>
                                                        <td style="width:7%;">'.number_format($rowa['act_presupuesto'], 2, ',', ' ').' Bs.</td>
                                                        <td style="width:7%;">'.$rowa['act_ponderacion'].' %</td>
                                                        <td style="width:5%;">'.$rowa['act_linea_base'].' '.$ti.'</td>
                                                        <td style="width:5%;">'.round($rowa['act_meta'],1).' '.$ti.'</td>
                                                        <td style="width:65%;">';
                                                        $pa=0; $pe=0; $gestion=$fase[0]['pfec_fecha_inicio']; $a=1;$e=1;
                                                          for($k=1;$k<=$a�os;$k++)
                                                          { 
                                                            /*---------------- llenando la matriz vacia --------------*/
                                                            for($j = 1; $j<=12; $j++)
                                                            {
                                                              $matriz_r[1][$j]=$j;
                                                              $matriz_r[2][$j]='0';
                                                              $matriz_r[3][$j]='0';
                                                              $matriz_r[4][$j]='0';
                                                              $matriz_r[5][$j]='0';
                                                              $matriz_r[6][$j]='0';
                                                              $matriz_r[7][$j]='0';
                                                              $matriz_r[8][$j]='0';
                                                              $matriz_r[9][$j]='0';
                                                              $matriz_r[10][$j]='0';
                                                            }
                                                            /*--------------------------------------------------------*/
                                                            $programado=$this->model_actividad->act_prog_mensual($rowa['act_id'],$gestion); /// programado
                                                            $valorp=count($programado);
                                                            $ejecutado=$this->model_actividad->act_ejec_mensual($rowa['act_id'],$gestion); /// ejecutado
                                                            $valore=count($ejecutado);
                                                            $nro=0;
                                                            foreach($programado as $row)
                                                            {
                                                              $nro++;
                                                              $matriz [1][$nro]=$row['m_id'];
                                                              $matriz [2][$nro]=$row['pg_fis'];
                                                            }
                                                           /*--------------------ejecutado gestion ------------------*/
                                                            $nro_e=0;
                                                            foreach($ejecutado as $row)
                                                            {
                                                              $nro_e++;
                                                              $matriz_e [1][$nro_e]=$row['m_id'];
                                                              $matriz_e [2][$nro_e]=$row['ejec_fis'];
                                                              $matriz_e [3][$nro_e]=$row['ejec_fis_a'];
                                                              $matriz_e [4][$nro_e]=$row['ejec_fis_b'];
                                                            }
                                                            /*--------------------------------------------------------*/
                                                            /*------- asignando en la matriz P, PA, %PA ----------*/
                                                            for($i = 1 ;$i<=$nro ;$i++)
                                                            {
                                                              for($j = 1 ;$j<=12 ;$j++)
                                                              {
                                                                if($matriz[1][$i]==$matriz_r[1][$j])
                                                                {
                                                                    $matriz_r[2][$j]=round($matriz[2][$i],2);
                                                                }
                                                              }
                                                            }

                                                            for($j = 1 ;$j<=12 ;$j++){
                                                              $pa=$pa+$matriz_r[2][$j];
                                                              $matriz_r[3][$j]=$pa+$rowa['act_linea_base'];
                                                              if($rowa['act_meta']!=0)
                                                              {
                                                                $matriz_r[4][$j]=round(((($pa+$rowa['act_linea_base'])/$rowa['act_meta'])*100),2);
                                                              }
                                                              
                                                            }

                                                            if($rowa['indi_id']==1)
                                                            {
                                                              for($i = 1 ;$i<=$nro_e ;$i++){
                                                                for($j = 1 ;$j<=12 ;$j++)
                                                                {
                                                                  if($matriz_e[1][$i]==$matriz_r[1][$j])
                                                                  {
                                                                    $matriz_r[7][$j]=round($matriz_e[2][$i],2);
                                                                  }
                                                                }
                                                              }
                                                            }
                                                            elseif ($rowa['indi_id']==2) 
                                                            {
                                                              for($i = 1 ;$i<=$nro_e ;$i++){
                                                                  for($j = 1 ;$j<=12 ;$j++)
                                                                  {
                                                                    if($matriz_e[1][$i]==$matriz_r[1][$j])
                                                                    {
                                                                      $matriz_r[5][$j]=round($matriz_e[3][$i],2);
                                                                      $matriz_r[6][$j]=round($matriz_e[4][$i],2);
                                                                      $matriz_r[7][$j]=round($matriz_e[2][$i],2);
                                                                    }
                                                                  }
                                                                }
                                                              /*--------------------------------------------------------*/
                                                            }
                                                              /*--------------------matriz E,AE,%AE gestion ------------------*/
                                                                for($j = 1 ;$j<=12 ;$j++){
                                                                  $pe=$pe+$matriz_r[7][$j];
                                                                  $matriz_r[8][$j]=$pe+$rowa['act_linea_base'];
                                                                  if($rowa['act_meta']!=0)
                                                                  {
                                                                    $matriz_r[9][$j]=round(((($pe+$rowa['act_linea_base'])/$rowa['act_meta'])*100),2);
                                                                  }
                                                                  
                                                                  if($matriz_r[4][$j]==0)
                                                                    {$matriz_r[10][$j]=round((($matriz_r[9][$j])*100),2);}
                                                                    else
                                                                    {
                                                                    $matriz_r[10][$j]=round((($matriz_r[9][$j]/$matriz_r[4][$j])*100),2);
                                                                    }
                                                                }
                                                              
                                                               // $ap[$n]=$matriz_r[2][1]*$rowa['act_ponderacion']; $n++;
                                                                $ap[$a]=(($matriz_r[4][1]*$rowa['act_ponderacion'])/100); $a++;
                                                                $ae[$e]=(($matriz_r[9][1]*$rowa['act_ponderacion'])/100); $e++;

                                                                $ap[$a]=(($matriz_r[4][2]*$rowa['act_ponderacion'])/100); $a++;
                                                                $ae[$e]=(($matriz_r[9][2]*$rowa['act_ponderacion'])/100); $e++;

                                                                $ap[$a]=(($matriz_r[4][3]*$rowa['act_ponderacion'])/100); $a++;
                                                                $ae[$e]=(($matriz_r[9][3]*$rowa['act_ponderacion'])/100); $e++;

                                                                $ap[$a]=(($matriz_r[4][4]*$rowa['act_ponderacion'])/100); $a++;
                                                                $ae[$e]=(($matriz_r[9][4]*$rowa['act_ponderacion'])/100); $e++;

                                                                $ap[$a]=(($matriz_r[4][5]*$rowa['act_ponderacion'])/100); $a++;
                                                                $ae[$e]=(($matriz_r[9][5]*$rowa['act_ponderacion'])/100); $e++;

                                                                $ap[$a]=(($matriz_r[4][6]*$rowa['act_ponderacion'])/100); $a++;
                                                                $ae[$e]=(($matriz_r[9][6]*$rowa['act_ponderacion'])/100); $e++;

                                                                $ap[$a]=(($matriz_r[4][7]*$rowa['act_ponderacion'])/100); $a++;
                                                                $ae[$e]=(($matriz_r[9][7]*$rowa['act_ponderacion'])/100); $e++;

                                                                $ap[$a]=(($matriz_r[4][8]*$rowa['act_ponderacion'])/100); $a++;
                                                                $ae[$e]=(($matriz_r[9][8]*$rowa['act_ponderacion'])/100); $e++;

                                                                $ap[$a]=(($matriz_r[4][9]*$rowa['act_ponderacion'])/100); $a++;
                                                                $ae[$e]=(($matriz_r[9][9]*$rowa['act_ponderacion'])/100); $e++;

                                                                $ap[$a]=(($matriz_r[4][10]*$rowa['act_ponderacion'])/100); $a++;
                                                                $ae[$e]=(($matriz_r[9][10]*$rowa['act_ponderacion'])/100); $e++;

                                                                $ap[$a]=(($matriz_r[4][11]*$rowa['act_ponderacion'])/100); $a++;
                                                                $ae[$e]=(($matriz_r[9][11]*$rowa['act_ponderacion'])/100); $e++;

                                                                $ap[$a]=(($matriz_r[4][12]*$rowa['act_ponderacion'])/100); $a++;
                                                                $ae[$e]=(($matriz_r[9][12]*$rowa['act_ponderacion'])/100); $e++;
                                                                
                                                                if($valorp!=0)
                                                                {
                                                                    $html .= '
                                                                    <div > 
                                                                     <table class="table table-bordered">
                                                                        <tr class="collapse_t" style="background-color:#ac9fae;font-weight: bold;">
                                                                            <td>Gesti&oacute;n '.$gestion.'</td>
                                                                            <td>Ene.</td>
                                                                            <td>Feb.</td>
                                                                            <td>Mar.</td>
                                                                            <td>Abr.</td>
                                                                            <td>May.</td>
                                                                            <td>Jun.</td>
                                                                            <td>Jul.</td>
                                                                            <td>Ago.</td>
                                                                            <td>Sep.</td>
                                                                            <td>Oct.</td>
                                                                            <td>Nov.</td>
                                                                            <td>Dic.</td>
                                                                        </tr>
                                                                        <tr bgcolor="#F5F5DC">
                                                                            <td style="width:1%;">PROGRAMADO</td>
                                                                            <td ><center>'.$matriz_r[2][1].' '.$ti.'</center></td>
                                                                            <td ><center>'.$matriz_r[2][2].' '.$ti.'</center></td>
                                                                            <td ><center>'.$matriz_r[2][3].' '.$ti.'</center></td>
                                                                            <td ><center>'.$matriz_r[2][4].' '.$ti.'</center></td>
                                                                            <td ><center>'.$matriz_r[2][5].' '.$ti.'</center></td>
                                                                            <td ><center>'.$matriz_r[2][6].' '.$ti.'</center></td>
                                                                            <td ><center>'.$matriz_r[2][7].' '.$ti.'</center></td>
                                                                            <td ><center>'.$matriz_r[2][8].' '.$ti.'</center></td>
                                                                            <td ><center>'.$matriz_r[2][9].' '.$ti.'</center></td>
                                                                            <td ><center>'.$matriz_r[2][10].' '.$ti.'</center></td>
                                                                            <td ><center>'.$matriz_r[2][11].' '.$ti.'</center></td>
                                                                            <td ><center>'.$matriz_r[2][12].' '.$ti.'</center></td>
                                                                        </tr>';
                                                                        /*<tr bgcolor="#F5F5DC">
                                                                            <td style="width:1%;">PROGRAMADO<BR>ACUMULADO</td>
                                                                            <td ><center>'.$matriz_r[3][1].'</center></td>
                                                                            <td ><center>'.$matriz_r[3][2].'</center></td>
                                                                            <td ><center>'.$matriz_r[3][3].'</center></td>
                                                                            <td ><center>'.$matriz_r[3][4].'</center></td>
                                                                            <td ><center>'.$matriz_r[3][5].'</center></td>
                                                                            <td ><center>'.$matriz_r[3][6].'</center></td>
                                                                            <td ><center>'.$matriz_r[3][7].'</center></td>
                                                                            <td ><center>'.$matriz_r[3][8].'</center></td>
                                                                            <td ><center>'.$matriz_r[3][9].'</center></td>
                                                                            <td ><center>'.$matriz_r[3][10].'</center></td>
                                                                            <td ><center>'.$matriz_r[3][11].'</center></td>
                                                                            <td ><center>'.$matriz_r[3][12].'</center></td>
                                                                        </tr>*/

                                                                        $html .= '

                                                                        <tr bgcolor="#F5F5DC">
                                                                            <td style="width:1%;">%PROGRAMADO<BR>ACUMULADO</td>
                                                                            <td ><center>'.$matriz_r[4][1].'%</center></td>
                                                                            <td ><center>'.$matriz_r[4][2].'%</center></td>
                                                                            <td ><center>'.$matriz_r[4][3].'%</center></td>
                                                                            <td ><center>'.$matriz_r[4][4].'%</center></td>
                                                                            <td ><center>'.$matriz_r[4][5].'%</center></td>
                                                                            <td ><center>'.$matriz_r[4][6].'%</center></td>
                                                                            <td ><center>'.$matriz_r[4][7].'%</center></td>
                                                                            <td ><center>'.$matriz_r[4][8].'%</center></td>
                                                                            <td ><center>'.$matriz_r[4][9].'%</center></td>
                                                                            <td ><center>'.$matriz_r[4][10].'%</center></td>
                                                                            <td ><center>'.$matriz_r[4][11].'%</center></td>
                                                                            <td ><center>'.$matriz_r[4][12].'%</center></td>
                                                                        </tr>

                                                                        <tr bgcolor="#F5F5DC">
                                                                            <td style="width:1%;">EJECUTADO</td>
                                                                            <td ><center>'.$matriz_r[7][1].' '.$ti.'</center></td>
                                                                            <td ><center>'.$matriz_r[7][2].' '.$ti.'</center></td>
                                                                            <td ><center>'.$matriz_r[7][3].' '.$ti.'</center></td>
                                                                            <td ><center>'.$matriz_r[7][4].' '.$ti.'</center></td>
                                                                            <td ><center>'.$matriz_r[7][5].' '.$ti.'</center></td>
                                                                            <td ><center>'.$matriz_r[7][6].' '.$ti.'</center></td>
                                                                            <td ><center>'.$matriz_r[7][7].' '.$ti.'</center></td>
                                                                            <td ><center>'.$matriz_r[7][8].' '.$ti.'</center></td>
                                                                            <td ><center>'.$matriz_r[7][9].' '.$ti.'</center></td>
                                                                            <td ><center>'.$matriz_r[7][10].' '.$ti.'</center></td>
                                                                            <td ><center>'.$matriz_r[7][11].' '.$ti.'</center></td>
                                                                            <td ><center>'.$matriz_r[7][12].' '.$ti.'</center></td>
                                                                        </tr>';
                                                                        /*<tr bgcolor="#F5F5DC">
                                                                            <td style="width:1%;">EJECUTADO<BR>ACUMULADO</td>
                                                                            <td ><center>'.$matriz_r[8][1].' '.$ti.'</center></td>
                                                                            <td ><center>'.$matriz_r[8][2].' '.$ti.'</center></td>
                                                                            <td ><center>'.$matriz_r[8][3].' '.$ti.'</center></td>
                                                                            <td ><center>'.$matriz_r[8][4].' '.$ti.'</center></td>
                                                                            <td ><center>'.$matriz_r[8][5].' '.$ti.'</center></td>
                                                                            <td ><center>'.$matriz_r[8][6].' '.$ti.'</center></td>
                                                                            <td ><center>'.$matriz_r[8][7].' '.$ti.'</center></td>
                                                                            <td ><center>'.$matriz_r[8][8].' '.$ti.'</center></td>
                                                                            <td ><center>'.$matriz_r[8][9].' '.$ti.'</center></td>
                                                                            <td ><center>'.$matriz_r[8][10].' '.$ti.'</center></td>
                                                                            <td ><center>'.$matriz_r[8][11].' '.$ti.'</center></td>
                                                                            <td ><center>'.$matriz_r[8][12].' '.$ti.'</center></td>
                                                                        </tr>*/
                                                                        $html .= '
                                                                        <tr bgcolor="#F5F5DC">
                                                                            <td style="width:1%;">%EJECUTADO<BR>ACUMULADO</td>
                                                                            <td ><center>'.$matriz_r[9][1].'%</center></td>
                                                                            <td ><center>'.$matriz_r[9][2].'%</center></td>
                                                                            <td ><center>'.$matriz_r[9][3].'%</center></td>
                                                                            <td ><center>'.$matriz_r[9][4].'%</center></td>
                                                                            <td ><center>'.$matriz_r[9][5].'%</center></td>
                                                                            <td ><center>'.$matriz_r[9][6].'%</center></td>
                                                                            <td ><center>'.$matriz_r[9][7].'%</center></td>
                                                                            <td ><center>'.$matriz_r[9][8].'%</center></td>
                                                                            <td ><center>'.$matriz_r[9][9].'%</center></td>
                                                                            <td ><center>'.$matriz_r[9][10].'%</center></td>
                                                                            <td ><center>'.$matriz_r[9][11].'%</center></td>
                                                                            <td ><center>'.$matriz_r[9][12].'%</center></td>
                                                                        </tr>
                                                                        </table>
                                                                    </div>';
                                                                }
                                                               /* else
                                                                {
                                                                    $html .= ' 
                                                                    <table class="table_contenedor">
                                                                        <tr>
                                                                            <td class="collapse_t" style="background-color:#ac9fae;font-weight: bold;width:70%;">
                                                                                 Gesti&oacute;n : '.$gestion.'
                                                                            </td>
                                                                            <td style="width:30%;">No Programado</td>
                                                                        </tr>
                                                                    </table>';
                                                                }*/
                                                               
                                                                $gestion++;
                                                            }

                                                            for($k=1;$k<=$meses;$k++)
                                                            {
                                                               // $html .= '['.$ap[$k].']'; 
                                                               $app[$k]=round(($app[$k]+$ap[$k]),1); //// sumando a nivel de las actividades
                                                               $aee[$k]=round(($aee[$k]+$ae[$k]),1); //// sumando a nivel de las actividades
                                                            }
                                                            
                                                       $html .= '</td>
                                                    </tr>
                                                </table>
                                            </div>';
                                    }
                                }
                                else
                                {
                                    $html .= '
                                        <div class="contenedor_datos">
                                            <table class="table_contenedor">
                                                <tr>
                                                    <td>No tiene Actividades registrados</td>
                                                </tr>
                                            </table>    
                                        </div>';
                                }
                                for($kp=1;$kp<=$meses;$kp++)
                                    {
                                        // $html .= '[['.$app[$kp].']'; 
                                        $ppp[$kp]=round(($ppp[$kp]+($app[$kp]*$rowp['prod_ponderacion'])/100),1); //// sumando a nivel de los productos
                                        $ppe[$kp]=round(($ppe[$kp]+($aee[$kp]*$rowp['prod_ponderacion'])/100),1); //// sumando a nivel de los productos
                                    } 
                                 
                                $nrop++;
                            }
                        
                        }
                        else
                        {
                            $html .= '
                            <div class="contenedor_datos">
                                <table class="table_contenedor">
                                    <tr>
                                        <td>No tiene productos registrados</td>
                                    </tr>
                                </table>    
                            </div>';
                        }

                        for($k=1;$k<=$meses;$k++)
                        {
                            // $html .= '['.$ppp[$k].']'; 
                            $cpp[$k]=round(($cpp[$k]+($ppp[$k]*$rowc['com_ponderacion'])/100),0); //// sumando a nivel de los Componentes
                            $cpe[$k]=round(($cpe[$k]+($ppe[$k]*$rowc['com_ponderacion'])/100),0); //// sumando a nivel de los Componentes
                        }

                    $nro_c++;
                    }
                    $html .= '</div><br><div class="contenedor_principal">
                    
                    <div class="titulo_dictamen"> EFICACIA F&Iacute;SICA DE LA ETAPA </div>
                    <div class="contenedor_datos">';
                        $i=1;$meses=12;$gestion=$fase[0]['pfec_fecha_inicio'];
                        for($kp=1;$kp<=$a�os;$kp++)
                        {
                             $html .= '
                            <table class="table table-bordered">
                                <tr class="collapse_t" style="background-color:#ac9fae;font-weight: bold;">
                                    <td style="width:18%" rowspan=2> ESTADO </td>
                                    <td colspan=12> GESTI&Oacute;N : '.$gestion.'</td>
                                </tr>
                                <tr class="collapse_t" style="background-color:#ac9fae;font-weight: bold;">
                                    <td>Ene.</td>
                                    <td>Feb.</td>
                                    <td>Mar.</td>
                                    <td>Abr.</td>
                                    <td>May.</td>
                                    <td>Jun.</td>
                                    <td>Jul.</td>
                                    <td>Ago.</td>
                                    <td>Sep.</td>
                                    <td>Oct.</td>
                                    <td>Nov.</td>
                                    <td>Dic.</td>
                                </tr>
                                <tr class="collapse_t">
                                    <td><b>PROGRAMACI&Oacute;N ACUMULADA EN %</b></td>';
                                    for($pos=$i;$pos<=$meses;$pos++)
                                    {
                                        $html .= '<td>'.$cpp[$pos].'%</td>';
                                    }
                                    $html .= '
                                </tr>
                                <tr class="collapse_t">
                                    <td><b>EJECUCI&Oacute;N ACUMULADA EN %</b></td>';
                                    for($pos=$i;$pos<=$meses;$pos++)
                                    {
                                        $html .= '<td bgcolor="#98FB98">'.$cpe[$pos].'%</td>';
                                    }
                                    $html .= '
                                </tr>
                                <tr class="collapse_t">
                                    <td><b>EFICACIA</b></td>';
                                    for($pos=$i;$pos<=$meses;$pos++)
                                    {
                                        if($cpp[$pos]==0){$cpp[$pos]=1;}
                                        $html .= '<td bgcolor="#B0E0E6">'.round((($cpe[$pos]/$cpp[$pos])*100),1).'%</td>';
                                    }
                                    $i=$meses+1;
                                    $meses=$meses+12;
                                    $html .= '
                                </tr>
                            </table>';
                            $gestion++;           
                        } 
                        
                    $html .= '  
                    </div>

                    <table class="table_contenedor" style="margin-top: 10px;>
                        <tr>
                            <td class="fila_unitaria">FIRMAS:</td>
                        </tr>
                    </table>
                    <table style="width: 80%;margin: 0 auto;margin-top: 100px;margin-bottom: 20px;">
                        <tr>
                            <td style="width:30%">
                                <hr>
                            </td>
                            <td style="width:3%"></td>
                            <td style="width:30%">
                                <hr>
                            </td>
                            <td style="width:3%"></td>
                            <td style="width:30%">
                                <hr>
                            </td>
                        </tr>
                    </table>

                </div>';
                  
                    /*for($kp=1;$kp<=$meses;$kp++)
                    {
                         $html .= '['.$cpp[$kp].']';                
                    }*/
                }
                else
                {
                    $html .= '
                    <div class="contenedor_datos">
                        <table class="table_contenedor">
                            <tr>
                                <td>Sin Fase Etapa Activa</td>
                            </tr>
                        </table>    
                    </div>';
                }
                
                $html .= '
                </p>
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'portrait');
        ini_set('memory_limit','-1');
        ini_set('max_execution_time', 0);
        $dompdf->render();
        $dompdf->stream("programacion_ejecucion_fisica.pdf", array("Attachment" => false));
    }


    /*========================== GRAFICO PROGRAMACION FISICA ANUAL- PULRIANUAL=============*/
    public function grafico_programacion_fisica($mod,$id_f,$id_p){
        $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p);
        $data['unidad_responsable']=$this->model_proyecto->responsable_proy($id_p,2);
        $data['unidad_ejecutora']=$this->model_proyecto->responsable_proy($id_p,1); 

        if($mod==1){
            $this->load->view('admin/programacion/reportes/grafico_fisico/imprimir_curva_s', $data);
        }
        elseif ($mod==4){
            $this->load->view('admin/programacion/reportes/grafico_fisico/imprimir_curva_s_m', $data);
        }
    }

    /*------------------------------- IMPRIMIR GRAFICO --------------------------------*/
    public function imprimir_programacion_fisica($mod,$id_f,$id_p)
    {
        $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p);
        $data['unidad_responsable']=$this->model_proyecto->responsable_proy($id_p,2);
        $data['unidad_ejecutora']=$this->model_proyecto->responsable_proy($id_p,1); 

        if($mod==1)
        {
            $this->load->view('admin/programacion/reportes/grafico_fisico/imprimir_curva_s', $data);
        }
        elseif ($mod==4) 
        {
            $this->load->view('admin/programacion/reportes/grafico_fisico/imprimir_curva_s_m', $data);
        }
    }
    /*=========================================================================================*/

    public function grafico_multianual_programado($mod,$id_f,$id_p)
    {
            $p=100;
            $fase = $this->model_faseetapa->get_id_fase($id_p);
            $a�os=$fase[0]['pfec_fecha_fin']-$fase[0]['pfec_fecha_inicio']+1;
            $meses=$a�os*12;
            for($p=1;$p<=$meses;$p++){$cpp[$p]=0; $cpe[$p]=0;} //// vector vacio para sumar a nivel de productos programacion , ejecutado
            for($p=1;$p<=$meses;$p++){$cef[$p]=0;} //// vector eficacia
            $componentes = $this->model_componente->componentes_id($id_f);

            foreach ($componentes as $rowc)
            {
              for($p=1;$p<=$meses;$p++){$ppp[$p]=0; $ppe[$p]=0;} //// vector vacio para sumar a nivel de productos programacion , ejecutado
              $productos = $this->model_producto->list_prod($rowc['com_id']);
              
              foreach ($productos as $rowp)
              {
                $actividad = $this->model_actividad->list_act_anual($rowp['prod_id']);
                for($p=1;$p<=$meses;$p++){$app[$p]=0; $aee[$p]=0;} //// vector vacio para sumar a nivel de actividades programacion , ejecutado
                
                foreach ($actividad as $rowa)
                {   
                  $pa=0; $pe=0; $gestion=$fase[0]['pfec_fecha_inicio']; $a=1;$e=1;
                  for($k=1;$k<=$a�os;$k++)
                  { 
                    $programado=$this->model_actividad->act_prog_mensual($rowa['act_id'],$gestion); /// programado
                    $ejecutado=$this->model_actividad->act_ejec_mensual($rowa['act_id'],$gestion); /// ejecutado
                    $nro=0;
                    foreach($programado as $row)
                    {
                      $nro++;
                      $matriz [1][$nro]=$row['m_id'];
                      $matriz [2][$nro]=$row['pg_fis'];
                    }
                    /*---------------- llenando la matriz vacia --------------*/
                    for($j = 1; $j<=12; $j++)
                    {
                      $matriz_r[1][$j]=$j;
                      $matriz_r[2][$j]='0';
                      $matriz_r[3][$j]='0';
                      $matriz_r[4][$j]='0';
                      $matriz_r[5][$j]='0';
                      $matriz_r[6][$j]='0';
                      $matriz_r[7][$j]='0';
                      $matriz_r[8][$j]='0';
                      $matriz_r[9][$j]='0';
                      $matriz_r[10][$j]='0';
                    }
                    /*--------------------------------------------------------*/
                    /*--------------------ejecutado gestion ------------------*/
                      $nro_e=0;
                      foreach($ejecutado as $row)
                      {
                        $nro_e++;
                        $matriz_e [1][$nro_e]=$row['m_id'];
                        $matriz_e [2][$nro_e]=$row['ejec_fis'];
                        $matriz_e [3][$nro_e]=$row['ejec_fis_a'];
                        $matriz_e [4][$nro_e]=$row['ejec_fis_b'];
                      }
                      /*--------------------------------------------------------*/
                      /*------- asignando en la matriz P, PA, %PA ----------*/
                      for($i = 1 ;$i<=$nro ;$i++)
                      {
                        for($j = 1 ;$j<=12 ;$j++)
                        {
                          if($matriz[1][$i]==$matriz_r[1][$j])
                          {
                            $matriz_r[2][$j]=round($matriz[2][$i],2);
                          }
                        }
                      }

                      for($j = 1 ;$j<=12 ;$j++){
                        $pa=$pa+$matriz_r[2][$j];
                        $matriz_r[3][$j]=$pa+$rowa['act_linea_base'];
                        $matriz_r[4][$j]=round(((($pa+$rowa['act_linea_base'])/$rowa['act_meta'])*100),2);
                      }


                      $ap[$a]=(($matriz_r[4][1]*$rowa['act_ponderacion'])/100); $a++;
                      $ap[$a]=(($matriz_r[4][2]*$rowa['act_ponderacion'])/100); $a++;
                      $ap[$a]=(($matriz_r[4][3]*$rowa['act_ponderacion'])/100); $a++;
                      $ap[$a]=(($matriz_r[4][4]*$rowa['act_ponderacion'])/100); $a++;
                      $ap[$a]=(($matriz_r[4][5]*$rowa['act_ponderacion'])/100); $a++;
                      $ap[$a]=(($matriz_r[4][6]*$rowa['act_ponderacion'])/100); $a++;
                      $ap[$a]=(($matriz_r[4][7]*$rowa['act_ponderacion'])/100); $a++;
                      $ap[$a]=(($matriz_r[4][8]*$rowa['act_ponderacion'])/100); $a++;
                      $ap[$a]=(($matriz_r[4][9]*$rowa['act_ponderacion'])/100); $a++;
                      $ap[$a]=(($matriz_r[4][10]*$rowa['act_ponderacion'])/100); $a++;
                      $ap[$a]=(($matriz_r[4][11]*$rowa['act_ponderacion'])/100); $a++;
                      $ap[$a]=(($matriz_r[4][12]*$rowa['act_ponderacion'])/100); $a++;

                    $gestion++;

                  }

                  for($k=1;$k<=$meses;$k++)
                  {
                    //echo '['.$ap[$k].']'; 
                    $app[$k]=round(($app[$k]+$ap[$k]),2); //// sumando a nivel de las actividades
                  }

                }

                for($kp=1;$kp<=$meses;$kp++)
                {
                  //echo'[['.$app[$kp].']'; 
                  $ppp[$kp]=round(($ppp[$kp]+($app[$kp]*$rowp['prod_ponderacion'])/100),2); //// sumando a nivel de los productos
                } 
              }
              
              for($k=1;$k<=$meses;$k++)
                {
                  //echo '['.$ppp[$k].']'; 
                  $cpp[$k]=round(($cpp[$k]+($ppp[$k]*$rowc['com_ponderacion'])/100),1); //// sumando a nivel de los productos
                }
            }

            return $cpp;
    }

    public function grafico_multianual_ejecutado($mod,$id_f,$id_p)
    {
            $p=100;
            $fase = $this->model_faseetapa->get_id_fase($id_p);
            $a�os=$fase[0]['pfec_fecha_fin']-$fase[0]['pfec_fecha_inicio']+1;
            $meses=$a�os*12;
            for($p=1;$p<=$meses;$p++){$cpp[$p]=0; $cpe[$p]=0;} //// vector vacio para sumar a nivel de productos programacion , ejecutado
            for($p=1;$p<=$meses;$p++){$cef[$p]=0;} //// vector eficacia
            $componentes = $this->model_componente->componentes_id($id_f);

            foreach ($componentes as $rowc)
            {
              for($p=1;$p<=$meses;$p++){$ppp[$p]=0; $ppe[$p]=0;} //// vector vacio para sumar a nivel de productos programacion , ejecutado
              $productos = $this->model_producto->list_prod($rowc['com_id']);
              
              foreach ($productos as $rowp)
              {
                $actividad = $this->model_actividad->list_act_anual($rowp['prod_id']);
                for($p=1;$p<=$meses;$p++){$app[$p]=0; $aee[$p]=0;} //// vector vacio para sumar a nivel de actividades programacion , ejecutado
                
                foreach ($actividad as $rowa)
                {   
                  $pa=0; $pe=0; $gestion=$fase[0]['pfec_fecha_inicio']; $a=1;$e=1;
                  for($k=1;$k<=$a�os;$k++)
                  { 
                    $programado=$this->model_actividad->act_prog_mensual($rowa['act_id'],$gestion); /// programado
                    $ejecutado=$this->model_actividad->act_ejec_mensual($rowa['act_id'],$gestion); /// ejecutado
                    $nro=0;
                    foreach($programado as $row)
                    {
                      $nro++;
                      $matriz [1][$nro]=$row['m_id'];
                      $matriz [2][$nro]=$row['pg_fis'];
                    }
                    /*---------------- llenando la matriz vacia --------------*/
                    for($j = 1; $j<=12; $j++)
                    {
                      $matriz_r[1][$j]=$j;
                      $matriz_r[2][$j]='0';
                      $matriz_r[3][$j]='0';
                      $matriz_r[4][$j]='0';
                      $matriz_r[5][$j]='0';
                      $matriz_r[6][$j]='0';
                      $matriz_r[7][$j]='0';
                      $matriz_r[8][$j]='0';
                      $matriz_r[9][$j]='0';
                      $matriz_r[10][$j]='0';
                    }
                    /*--------------------------------------------------------*/
                    /*--------------------ejecutado gestion ------------------*/
                      $nro_e=0;
                      foreach($ejecutado as $row)
                      {
                        $nro_e++;
                        $matriz_e [1][$nro_e]=$row['m_id'];
                        $matriz_e [2][$nro_e]=$row['ejec_fis'];
                        $matriz_e [3][$nro_e]=$row['ejec_fis_a'];
                        $matriz_e [4][$nro_e]=$row['ejec_fis_b'];
                      }
                      /*--------------------------------------------------------*/
                      /*------- asignando en la matriz P, PA, %PA ----------*/
                      for($i = 1 ;$i<=$nro ;$i++)
                      {
                        for($j = 1 ;$j<=12 ;$j++)
                        {
                          if($matriz[1][$i]==$matriz_r[1][$j])
                          {
                            $matriz_r[2][$j]=round($matriz[2][$i],2);
                          }
                        }
                      }

                      for($j = 1 ;$j<=12 ;$j++){
                        $pa=$pa+$matriz_r[2][$j];
                        $matriz_r[3][$j]=$pa+$rowa['act_linea_base'];
                        $matriz_r[4][$j]=round(((($pa+$rowa['act_linea_base'])/$rowa['act_meta'])*100),2);
                      }

                      if($rowa['indi_id']==1)
                      {
                        for($i = 1 ;$i<=$nro_e ;$i++){
                          for($j = 1 ;$j<=12 ;$j++)
                          {
                            if($matriz_e[1][$i]==$matriz_r[1][$j])
                            {
                              $matriz_r[7][$j]=round($matriz_e[2][$i],1);
                            }
                          }
                        }
                      }
                      elseif ($rowa['indi_id']==2) 
                      {
                        if($rowa['act_denominador']==0)
                        {
                          for($i = 1 ;$i<=$nro_e ;$i++){
                            for($j = 1 ;$j<=12 ;$j++)
                            {
                              if($matriz_e[1][$i]==$matriz_r[1][$j])
                              {
                                $matriz_r[5][$j]=round($matriz_e[3][$i],1);
                                $matriz_r[6][$j]=round($matriz_e[4][$i],1);
                                $matriz_r[7][$j]=round($matriz_e[2][$i],1);
                              }
                            }
                          }
                        }
                        if ($rowa['act_denominador']==1)
                        {
                          for($i = 1 ;$i<=$nro_e ;$i++){
                            for($j = 1 ;$j<=12 ;$j++)
                            {
                              if($matriz_e[1][$i]==$matriz_r[1][$j])
                              {
                                $matriz_r[5][$j]=round($matriz_e[3][$i],1);
                                $matriz_r[6][$j]=round($matriz_e[4][$i],1);
                                $matriz_r[7][$j]=round($matriz_e[2][$i],1);
                              }
                            }
                          }
                        }
                       /*--------------------------------------------------------*/
                      }
                      /*--------------------matriz E,AE,%AE gestion ------------------*/
                      for($j = 1 ;$j<=12 ;$j++){
                        $pe=$pe+$matriz_r[7][$j];
                        $matriz_r[8][$j]=$pe+$rowa['act_linea_base'];
                        $matriz_r[9][$j]=round(((($pe+$rowa['act_linea_base'])/$rowa['act_meta'])*100),2);
                        if($matriz_r[4][$j]==0)
                        {$matriz_r[10][$j]=round((($matriz_r[9][$j])*100),1);}
                        else
                        {
                          $matriz_r[10][$j]=round((($matriz_r[9][$j]/$matriz_r[4][$j])*100),1);
                        }
                    }

                      $ap[$a]=(($matriz_r[4][1]*$rowa['act_ponderacion'])/100); $a++;
                      $ae[$e]=(($matriz_r[9][1]*$rowa['act_ponderacion'])/100); $e++;

                      $ap[$a]=(($matriz_r[4][2]*$rowa['act_ponderacion'])/100); $a++;
                      $ae[$e]=(($matriz_r[9][2]*$rowa['act_ponderacion'])/100); $e++;

                      $ap[$a]=(($matriz_r[4][3]*$rowa['act_ponderacion'])/100); $a++;
                      $ae[$e]=(($matriz_r[9][3]*$rowa['act_ponderacion'])/100); $e++;

                      $ap[$a]=(($matriz_r[4][4]*$rowa['act_ponderacion'])/100); $a++;
                      $ae[$e]=(($matriz_r[9][4]*$rowa['act_ponderacion'])/100); $e++;

                      $ap[$a]=(($matriz_r[4][5]*$rowa['act_ponderacion'])/100); $a++;
                      $ae[$e]=(($matriz_r[9][5]*$rowa['act_ponderacion'])/100); $e++;

                      $ap[$a]=(($matriz_r[4][6]*$rowa['act_ponderacion'])/100); $a++;
                      $ae[$e]=(($matriz_r[9][6]*$rowa['act_ponderacion'])/100); $e++;

                      $ap[$a]=(($matriz_r[4][7]*$rowa['act_ponderacion'])/100); $a++;
                      $ae[$e]=(($matriz_r[9][7]*$rowa['act_ponderacion'])/100); $e++;

                      $ap[$a]=(($matriz_r[4][8]*$rowa['act_ponderacion'])/100); $a++;
                      $ae[$e]=(($matriz_r[9][8]*$rowa['act_ponderacion'])/100); $e++;

                      $ap[$a]=(($matriz_r[4][9]*$rowa['act_ponderacion'])/100); $a++;
                      $ae[$e]=(($matriz_r[9][9]*$rowa['act_ponderacion'])/100); $e++;

                      $ap[$a]=(($matriz_r[4][10]*$rowa['act_ponderacion'])/100); $a++;
                      $ae[$e]=(($matriz_r[9][10]*$rowa['act_ponderacion'])/100); $e++;

                      $ap[$a]=(($matriz_r[4][11]*$rowa['act_ponderacion'])/100); $a++;
                      $ae[$e]=(($matriz_r[9][11]*$rowa['act_ponderacion'])/100); $e++;

                      $ap[$a]=(($matriz_r[4][12]*$rowa['act_ponderacion'])/100); $a++;
                      $ae[$e]=(($matriz_r[9][12]*$rowa['act_ponderacion'])/100); $e++;

                    $gestion++;

                  }

                  for($k=1;$k<=$meses;$k++)
                  {
                    //echo '['.$ap[$k].']'; 
                    $app[$k]=round(($app[$k]+$ap[$k]),2); //// sumando a nivel de las actividades
                    $aee[$k]=round(($aee[$k]+$ae[$k]),2); //// sumando a nivel de las actividades
                  }

                }

                for($kp=1;$kp<=$meses;$kp++)
                {
                  //echo'[['.$app[$kp].']'; 
                  $ppp[$kp]=round(($ppp[$kp]+($app[$kp]*$rowp['prod_ponderacion'])/100),2); //// sumando a nivel de los productos
                  $ppe[$kp]=round(($ppe[$kp]+($aee[$kp]*$rowp['prod_ponderacion'])/100),2); //// sumando a nivel de los productos
                } 
              }
              
              for($k=1;$k<=$meses;$k++)
                {
                  //echo '['.$ppp[$k].']'; 
                  $cpp[$k]=round(($cpp[$k]+($ppp[$k]*$rowc['com_ponderacion'])/100),1); //// sumando a nivel de los productos
                  $cpe[$k]=round(($cpe[$k]+($ppe[$k]*$rowc['com_ponderacion'])/100),1); //// sumando a nivel de los productos
                  if($cpp[$k]==0)
                  {$cef[$k]=$cpe[$k];}
                  else{$cef[$k]=round((($cpe[$k]/$cpp[$k])*100),2);}

                  if($cef[$k]<=75){$ef_menor[$k] = $cef[$k];}else{$ef_menor[$k] = 0;}
                  if ($cef[$k] >= 76 && $cef[$k] <= 90.9) {$ef_entre[$k] = $cef[$k];}else {$ef_entre[$k] = 0;}
                  if($cef[$k] >= 91){$ef_mayor[$k] = $cef[$k];}else{$ef_mayor[$k] = 0;}

                }
            }

            return $cpp;
    }

    /*==================================== CONTRATOS ===================================================*/
    public function contrato_proyecto($id,$id_f)
    {
        $proy_id = $id;
        $gestion = $this->session->userdata('gestion');
        $dictamen = $this->model_objetivo->get_dictamen($proy_id);
        $unidad_responsable=$this->model_proyecto->responsable_proy($proy_id,1);
        $dictamen = $dictamen->row();
        $aper_programatica = $this->model_objetivo->get_aper_programatica_dictamen($proy_id,$gestion);
        $aper_programatica = $aper_programatica->row();
        $componente = $this->model_componente->componentes_id($id_f);
        $nro_ctto = $this->model_ejecucion->nro_contratos($id_f);
        $num_principal = ($dictamen->tp_id == 1) ? 1 : 0 ;
        $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/logo.jpg" class="siipp"/>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata('entidad').'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$gestion.'<br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                            <b>FORMULARIO : </b> CONTRATOS <br>
                        </td>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/escudo.jpg" alt="" width="80px">
                        </td>
                    </tr>
                </table>
                <footer>
                    <table class="table table-bordered" width="100%">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata('sistema_pie').'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>
                <table class="datos_principales" style="table-layout:fixed;" border="1">
                    <tr>
                        <td colspan="2">
                            <b>'.$dictamen->proy_nombre.'</b><br>';
                            if($dictamen->tp_id==1)
                            {$html .= 'Proyecto/Actividad';}
                            elseif ($dictamen->tp_id==2 || $dictamen->tp_id==3) 
                            {$html .= 'Programa';}
                            elseif ($dictamen->tp_id==4) 
                            {$html .= 'Acci&oacute;n de Funcionamiento';}
                            $html .= '
                        </td>
                    </tr>
                    <tr>
                        <td style="width:50%;">';
                            if($dictamen->tp_id==1)
                            {$html .= '<b>'.$dictamen->proy_sisin.'</b><br>';}
                            elseif ($dictamen->tp_id==2 || $dictamen->tp_id==3 || $dictamen->tp_id==4) 
                            {$html .= 'N/A<br>';}
                            $html .= 'SISIN
                        </td>
                        <td style="width:50%; text-align:center;">
                            <b>'.$aper_programatica->programatica.'</b><br>
                            Apertura Programatica
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                            <b>'.$aper_programatica->uni_unidad.'</b><br>
                            Unidad Responsable
                        </td>
                        <td style="text-align:center;">
                            <b>'.$unidad_responsable[0]['uresp'].'</b><br>
                            Unidad Ejecutora
                        </td>
                    </tr>
                </table>
                <br>
                <div class="contenedor_principal">';
                if($nro_ctto!=0)
                {
                        foreach ($componente as $rowc)
                        {
                            if($dictamen->tp_id!=4)
                            {
                                $html .= '<div class="titulo_dictamen">'.$rowc['com_componente'].'</div>';
                            }
                            $contrato = $this->model_ejecucion->list_contratos($id_f,$rowc['com_id']);
                            foreach ($contrato as $row)
                            {
                                $html .= '                
                                <div class="contenedor_datos">
                                    <table class="table_contenedor">
                                        <tr class="collapse_t" style="background-color:#ac9fae;font-weight: bold;">
                                            <td colspan="4" class="fila_unitaria">IDENTIFICACI&Oacute;N DEL CONTRATO</td>
                                        </tr>
                                        <tr class="collapse_t">
                                            <td style="width:15%"  class="fila_unitaria">NOMBRE : </td>
                                            <td class="fila_unitaria">'.$row['ctto_objeto'].'</td>
                                            <td style="width:15%"  class="fila_unitaria">NRO. CTTO.</td>
                                            <td class="fila_unitaria">'.$row['ctto_numero'].'</td>
                                        </tr>
                                        <tr class="collapse_t">
                                            <td style="width:15%"  class="fila_unitaria">MODO DE ADJUDICACI&Oacute;N : </td>
                                            <td class="fila_unitaria">'.$row['mod_modalidad'].'</td>
                                            <td style="width:15%"  class="fila_unitaria">FECHA DE ADJUDICACI&Oacute;N.</td>
                                            <td class="fila_unitaria">'.date('d/m/Y',strtotime($row['ctto_fecha_adjudicacion'])).'</td>
                                        </tr>
                                        <tr class="collapse_t">
                                            <td style="width:15%"  class="fila_unitaria">OBSERVACI&Oacute;N : </td>
                                            <td colspan=3 class="fila_unitaria">'.$row['ctto_observaciones'].'</td>
                                        </tr>
                                        <tr class="collapse_t">
                                           <td style="width:15%"  class="fila_unitaria">FECHA DE SUSCRIPCI&Oacute;N CTTO.</td>
                                            <td class="fila_unitaria">'.date('d/m/Y',strtotime($row['ctto_fecha_suscripcion'])).'</td>
                                            <td style="width:15%"  class="fila_unitaria">PLAZO</td>
                                            <td class="fila_unitaria">'.$row['ctto_plazo_dd'].'</td>
                                        </tr>
                                    </table>

                                    <table class="table_contenedor">
                                        <tr class="collapse_t" style="background-color:#ac9fae;font-weight: bold;">
                                            <td colspan="8" class="fila_unitaria">IDENTIFICACI&Oacute;N DEL CONTRATISTA</td>
                                        </tr>
                                        <tr class="collapse_t">
                                            <td style="width:15%"  class="fila_unitaria">NOMBRE : </td>
                                            <td colspan=4 class="fila_unitaria">'.$row['ctta_nombre'].'</td>
                                          
                                            <td style="width:15%"  class="fila_unitaria">SIGLA</td>
                                            <td  colspan=2 class="fila_unitaria">'.$row['ctta_sigla'].'</td>
                                        </tr>
                                        <tr class="collapse_t">
                                           <td style="width:15%"  class="fila_unitaria">DOMICILIO : </td>
                                            <td class="fila_unitaria">'.$row['ctta_direccion'].'</td>
                                            <td class="fila_unitaria">NRO.</td>
                                            <td class="fila_unitaria"></td>
                                            <td class="fila_unitaria">CIUDAD</td>
                                            <td class="fila_unitaria">'.$row['ctta_ciudad'].'</td>
                                            <td class="fila_unitaria">PAIS</td>
                                            <td class="fila_unitaria">'.$row['ctta_pais'].'</td>
                                        </tr>
                                    </table>

                                    <table class="table_contenedor">
                                        <tr class="collapse_t" style="background-color:#ac9fae;font-weight: bold;">
                                            <td colspan="5" class="fila_unitaria">CONDICIONES ORIGINALES DEL CONTRATO</td>
                                        </tr>
                                        <tr class="collapse_t">
                                            <td rowspan=2 style="width:15%"  class="fila_unitaria">MONTO TOTAL DEL CONTRATO : </td>
                                            <td class="fila_unitaria"> $us. '.$row['ctto_dolares'].'</td>
                                          
                                            <td style="width:15%"  class="fila_unitaria">MONEDA DE PAGO</td>
                                            <td  colspan=2 class="fila_unitaria">'.$row['ctto_moneda'].'</td>
                                        </tr>
                                        <tr class="collapse_t">
                                           
                                            <td class="fila_unitaria"> Bs. '.$row['ctto_bolivianos'].'</td>
                                            <td class="fila_unitaria">ANTICIPO : '.$row['ctto_anticipo'].' %</td>
                                            <td class="fila_unitaria"> $us.</td>
                                            <td class="fila_unitaria"> Bs.</td>
                                        </tr>
                                    </table>
                                    <table class="table_contenedor">
                                        <tr class="collapse_t" style="background-color:#ac9fae;font-weight: bold;">
                                            <td colspan="6" class="fila_unitaria">GARANTIAS</td>
                                        </tr>
                                        <tr class="collapse_t">
                                            <td style="width:1%"  class="fila_unitaria">NRO</td>
                                            <td style="width:15%"  class="fila_unitaria">DETALLE </td>
                                            <td style="width:5%"  class="fila_unitaria">PLAZO</td>
                                            <td style="width:10%"  class="fila_unitaria">FECHA DE VENCICIMIENTO</td>
                                            <td style="width:10%"  class="fila_unitaria">FECHA DE ENTREGA </td>
                                            <td style="width:15%"  class="fila_unitaria">OBSERVACIONES </td>
                                        </tr>';
                                        $nro=1;
                                        $garantias = $this->model_ejecucion->lista_garantias($row['ctto_id']);
                                        foreach ($garantias as $rowg)
                                        {
                                            $html .= '
                                            <tr class="collapse_t">
                                                <td class="fila_unitaria">'.$nro.'</td>
                                                <td  class="fila_unitaria">'.$rowg['gtia_detalle'].'</td>
                                                <td  class="fila_unitaria">'.$rowg['gtia_plzo'].'</td>
                                                <td  class="fila_unitaria">'.date('d/m/Y',strtotime($rowg['gtia_fecha_vencimiento'])).'</td>
                                                <td  class="fila_unitaria">'.date('d/m/Y',strtotime($rowg['gtia_fecha_emision'])).'</td>
                                                <td  class="fila_unitaria">'.$rowg['gtia_obs'].'</td>
                                            </tr>';
                                            $nro++;
                                        }
                                        $html .= '
                                    </table>';
                                    
                                
                                    $html .= '
                                  
                                </div>';
                            }
                    
                        }
                    }
                    else
                    {
                         $html .= '
                                <div class="contenedor_datos">
                                    <table class="table_contenedor">
                                        <tr>
                                            <td>No tiene Contratos Registrados</td>
                                        </tr>
                                    </table>    
                                </div>';
                    }

                    
                    $html .= '                   
                    <br>
                
                    <table class="table_contenedor" style="margin-top: 10px;>
                        <tr>
                            <td class="fila_unitaria">FIRMAS:</td>
                        </tr>
                    </table>
                    <table style="width: 80%;margin: 0 auto;margin-top: 100px;margin-bottom: 50px;">
                        <tr>
                            <td style="width:30%">
                                <hr>
                            </td>
                            <td style="width:3%"></td>
                            <td style="width:30%">
                                <hr>
                            </td>
                            <td style="width:3%"></td>
                            <td style="width:30%">
                                <hr>
                            </td>
                        </tr>
                    </table>

                </div>
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'portrait');
        $dompdf->render();
        $dompdf->stream("dictamen_proyecto.pdf", array("Attachment" => false));
    }
        /*==================================== SUPERVISION Y EVALUACION ===================================================*/
    public function supervision_evaluacion($id,$id_f)
    {
        $proy_id = $id;
        $gestion = $this->session->userdata('gestion');
        $dictamen = $this->model_objetivo->get_dictamen($proy_id);
        $unidad_responsable=$this->model_proyecto->responsable_proy($proy_id,1);
        $dictamen = $dictamen->row();
        $aper_programatica = $this->model_objetivo->get_aper_programatica_dictamen($proy_id,$gestion);
        $aper_programatica = $aper_programatica->row();
        $componente = $this->model_componente->componentes_id($id_f);
        $nro_fase = $this->model_ejecucion->nro_ejecucion_fase_mes($id_f);
        $num_principal = ($dictamen->tp_id == 1) ? 1 : 0 ;
        $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/logo.jpg" class="siipp"/>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata('entidad').'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$gestion.'<br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                            <b>FORMULARIO : </b> SUPERVISI&Oacute;N Y EVALUACI&Oacute;N <br>
                        </td>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/escudo.jpg" alt="" width="70px">
                        </td>
                    </tr>
                </table>
                <footer>
                    <table class="table table-bordered" width="100%">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata('sistema_pie').'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>
                <table class="datos_principales" style="table-layout:fixed;" border="1">
                    <tr>
                        <td colspan="2">
                            <b>'.$dictamen->proy_nombre.'</b><br>';
                            if($dictamen->tp_id==1)
                            {$html .= 'Proyecto/Programa/Operaci&oacute;n';}
                            elseif ($dictamen->tp_id==2 || $dictamen->tp_id==3) 
                            {$html .= 'Programa';}
                            elseif ($dictamen->tp_id==4) 
                            {$html .= 'Acci&oacute;n de Funcionamiento';}
                            $html .= '
                        </td>
                    </tr>
                    <tr>
                        <td style="width:50%;">';
                            if($dictamen->tp_id==1)
                            {$html .= '<b>'.$dictamen->proy_sisin.'</b><br>';}
                            elseif ($dictamen->tp_id==2 || $dictamen->tp_id==3 || $dictamen->tp_id==4) 
                            {$html .= 'N/A<br>';}
                            $html .= 'SISIN
                        </td>
                        <td style="width:50%; text-align:center;">
                            <b>'.$aper_programatica->programatica.'</b><br>
                            Apertura Programatica
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                            <b>'.$aper_programatica->uni_unidad.'</b><br>
                            Unidad Responsable
                        </td>
                        <td colspan="2" style="text-align:center;">
                            <b>'.$unidad_responsable[0]['uresp'].'</b><br>
                            Unidad Ejecutora
                        </td>
                    </tr>
                </table>
                <br>
                <div class="contenedor_principal">';

                if($nro_fase!=0)
                {
                        foreach ($componente as $rowc)
                        {
                            if($dictamen->tp_id!=4)
                            {
                                $html .= '<div class="titulo_dictamen">'.$rowc['com_componente'].'</div>';
                            }
                            $nro = $this->model_ejecucion->nro_ejecucion_componente($rowc['com_id']);
                            if($nro!=0)
                            {
                                $supervision = $this->model_ejecucion->ejecucion_componente($rowc['com_id']);
                                foreach ($supervision as $row)
                                {
                                    if($row['m_id']==1){$mes='ENERO';}
                                    elseif($row['m_id']==2){$mes='FEBRERO';}
                                    elseif($row['m_id']==3){$mes='MARZO';}
                                    elseif($row['m_id']==4){$mes='ABRIL';}
                                    elseif($row['m_id']==5){$mes='MAYO';}
                                    elseif($row['m_id']==6){$mes='JUNIO';}
                                    elseif($row['m_id']==7){$mes='JULIO';}
                                    elseif($row['m_id']==8){$mes='AGOSTO';}
                                    elseif($row['m_id']==9){$mes='SEPTIEMBRE';}
                                    elseif($row['m_id']==10){$mes='OCTUBRE';}
                                    elseif($row['m_id']==11){$mes='NOVIEMBRE';}
                                    elseif($row['m_id']==12){$mes='DICIEMBRE';}

                                    if($row['estado']==1){$estado='PROCESO ADMINISTRATIVO';}
                                    if($row['estado']==2){$estado='EN EJECUCI&Oacute;N';}
                                    if($row['estado']==3){$estado='CERRADO';}
                                    if($row['estado']==4){$estado='PARALIZADO';}
                                    if($row['estado']==4){$estado='CONCLUIDO';}
                                    $html .= '                
                                    <div class="contenedor_datos">
                                        <table class="table table-bordered">
                                            <tr class="collapse_t" style="background-color:#ac9fae;font-weight: bold;">
                                                <td colspan="4" class="titulo_dictamen"><font bgcolor="#ffffff">'.$mes.'</font></td>
                                            </tr>
                                            <tr class="collapse_t" style="background-color:#ac9fae;font-weight: bold;">
                                                <td style="width:15%"  class="fila_unitaria">PROBLEMAS</td>
                                                <td style="width:15%"  class="fila_unitaria">CAUSAS</td>
                                                <td style="width:15%"  class="fila_unitaria">SOLUCI&Oacute;N</td>
                                                <td style="width:15%"  class="fila_unitaria">ESTADO</td>
                                            </tr>
                                            <tr class="collapse_t">
                                                <td class="fila_unitaria">'.$row['ejec_prob'].'</td>
                                                <td class="fila_unitaria">'.$row['ejec_causas'].'</td>
                                                <td class="fila_unitaria">'.$row['ejec_sol'].'</td>
                                                <td class="fila_unitaria">'.$estado.'</td>
                                            </tr>
                                        </table>
                                    </div>';
                                }
                            }
                            else
                            {
                                $html .= '
                                <div class="contenedor_datos"> 
                                    <table class="table_contenedor">
                                        <tr>
                                            <td>El componente no tiene ejecuciones registradas</td>
                                        </tr>
                                    </table>    
                                </div>';
                            }
                        }
                    }
                    else
                    {
                         $html .= '
                                <div class="contenedor_datos">
                                    <table class="table_contenedor">
                                        <tr>
                                            <td>No tiene Ejecuciones Registradas</td>
                                        </tr>
                                    </table>    
                                </div>';
                    }

                    
                    $html .= '                   
                    <br>
                
                    <table class="table_contenedor" style="margin-top: 10px;>
                        <tr>
                            <td class="fila_unitaria">FIRMAS:</td>
                        </tr>
                    </table>
                    <table style="width: 80%;margin: 0 auto;margin-top: 100px;margin-bottom: 50px;">
                        <tr>
                            <td style="width:30%">
                                <hr>
                            </td>
                            <td style="width:3%"></td>
                            <td style="width:30%">
                                <hr>
                            </td>
                            <td style="width:3%"></td>
                            <td style="width:30%">
                                <hr>
                            </td>
                        </tr>
                    </table>

                </div>
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'portrait');
        $dompdf->render();
        $dompdf->stream("dictamen_proyecto.pdf", array("Attachment" => false));
    }

  /*---------------------------- REPORTES -------------------------------------*/

 /*============================================== REPORTE FINANCIERO DIRECTO ===================================*/
     public function programado_pluri($proy_id)
    {
       $fase= $this->model_faseetapa->get_id_fase($proy_id);
       $mp[1]='enero';
       $mp[2]='febrero';
       $mp[3]='marzo';
       $mp[4]='abril';
       $mp[5]='mayo';
       $mp[6]='junio';
       $mp[7]='julio';
       $mp[8]='agosto';
       $mp[9]='septiembre';
       $mp[10]='octubre';
       $mp[11]='noviembre';
       $mp[12]='diciembre';

        $a�os=$fase[0]['pfec_fecha_fin']-$fase[0]['pfec_fecha_inicio']+1;
        $g_inicio=$fase[0]['pfec_fecha_inicio'];
        $meses=$a�os*12;
       
       for ($i=1; $i <=$meses; $i++){$prog[$i]=0;}

       $variable_p=1; 
       for ($i=1; $i <=$a�os; $i++)
       {
          if($fase[0]['pfec_ejecucion']==1)
          {
            $programado_fin= $this->model_reporte->programado_financiero_directo($g_inicio,$proy_id);
          }
          elseif($fase[0]['pfec_ejecucion']==2)
          {
            $programado_fin= $this->model_reporte->programado_financiero_delegado($g_inicio,$proy_id);
          }

          if(count($programado_fin)!=0) ///// tiene programado 
          {
            for ($j=1; $j <=12; $j++)
            {
              $prog[$variable_p]=$programado_fin[0][$mp[$j]];
              $variable_p++;
            }
          }
          else
          {
            for ($j=1; $j <=12; $j++) ///// no tiene programado
            {
              $prog[$variable_p]=0;
              $variable_p++;
            }
          }

        $g_inicio++;  
       }

       return $prog;
    }

    public function ejecutado_pluri($proy_id)
    {
       $fase= $this->model_faseetapa->get_id_fase($proy_id);
       $me[1]='ejec1';
       $me[2]='ejec2';
       $me[3]='ejec3';
       $me[4]='ejec4';
       $me[5]='ejec5';
       $me[6]='ejec6';
       $me[7]='ejec7';
       $me[8]='ejec8';
       $me[9]='ejec9';
       $me[10]='ejec10';
       $me[11]='ejec11';
       $me[12]='ejec12';

        $a�os=$fase[0]['pfec_fecha_fin']-$fase[0]['pfec_fecha_inicio']+1;
        $g_inicio=$fase[0]['pfec_fecha_inicio'];
        $meses=$a�os*12;
       
       for ($i=1; $i <=$meses; $i++){$ejec[$i]=0;}

       $variable_e=1; 
       for ($i=1; $i <=$a�os; $i++)
       {
          $ejecutado_fin= $this->model_reporte->ejecutado_financiero($proy_id,$g_inicio);
          if(count($ejecutado_fin)!=0) ///// tiene Ejecutado 
          {
            for ($j=1; $j <=12; $j++)
            {
              $ejec[$variable_e]=$ejecutado_fin[0][$me[$j]];
              $variable_e++;
            }
          }
          else
          {
            for ($j=1; $j <=12; $j++) ///// no tiene Ejecutado
            {
              $ejec[$variable_e]=0;
              $variable_e++;
            }
          }

        $g_inicio++;  
       }

       return $ejec;
    }
 public function reporte_proy_ejecfin_m($proy_id)
    {
        $gestion = $this->session->userdata('gestion');
        $dictamen = $this->model_objetivo->get_dictamen($proy_id);
        $unidad_responsable=$this->model_proyecto->responsable_proy($proy_id,1);
        $dictamen = $dictamen->row();
        $aper_programatica = $this->model_objetivo->get_aper_programatica_dictamen($proy_id,$gestion);
        $aper_programatica = $aper_programatica->row();
        $fase = $this->model_faseetapa->get_id_fase($proy_id);
        $a�os=$fase[0]['pfec_fecha_fin']-$fase[0]['pfec_fecha_inicio']+1;
        $meses=$a�os*12;
        /*---------------------------------------------------------------------------*/
        $programado = $this->programado_pluri($proy_id); ///// PROGRAMADO
        $ejecutado = $this->ejecutado_pluri($proy_id);   //// EJECUTADO 

        $meses=$a�os*12;

        for ($i=1; $i <=$meses; $i++)
        {
          $prog_a[$i]=0;$prog_a2[$i]=0; $ejec_a[$i]=0;$ejec_a2[$i]=0; $efi[$i]=0;
          $ef_menor[$i]=0;$ef_entre[$i]=0;$ef_mayor[$i]=0;
          $menor[$i]=0;$entre[$i]=0;$mayor[$i]=0;
        }

        $suma_prog=0;
        for($i=1;$i<=$meses;$i++)
        {
          $suma_prog=$suma_prog+$programado[$i];
          $prog_a[$i]=$suma_prog; ///// Programado acumulado
          $prog_a2[$i]=round((($prog_a[$i]/$fase[0]['pfec_ptto_fase'])*100),2); ////// Programado Acumulado %
        }

        $suma_ejec=0;
        for($i=1;$i<=$meses;$i++)
        {
          $suma_ejec=$suma_ejec+$ejecutado[$i];
          $ejec_a[$i]=$suma_ejec; ///// Programado acumulado
          $ejec_a2[$i]=round((($ejec_a[$i]/$fase[0]['pfec_ptto_fase'])*100),2); ////// Programado Acumulado %
        }

        for($i=1;$i<=$meses;$i++)
        {
          if($prog_a2[$i]!=0)
          {
            $efi[$i]=round((($ejec_a2[$i]/$prog_a2[$i])*100),2);
          }
        }
        /*---------------------------------------------------------------------------*/
        $num_principal = ($dictamen->tp_id == 1) ? 1 : 0 ;
        $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/logo.jpg" class="siipp"/>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata('entidad').'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$gestion.'<br>
                            <b>'.$this->session->userdata('sistema').' </b><br>
                            <b>FORMULARIO : </b> PROGRAMACI&Oacute;N Y EJECUCI&Oacute;N FINANCIERA - PLURI-ANUAL<br>
                        </td>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/escudo.jpg" alt="" width="100px">
                        </td>
                    </tr>
                </table>
                <footer>
                    <table class="table table-bordered" width="100%">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata("sistema_pie").'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>
                <table class="datos_principales" style="table-layout:fixed;" border="1">
                    <tr>
                        <td colspan="2">
                            <b>'.$dictamen->proy_nombre.'</b><br>';
                            if($dictamen->tp_id==1)
                            {$html .= 'Proyecto/Actividad';}
                            elseif ($dictamen->tp_id==2 || $dictamen->tp_id==3) 
                            {$html .= 'Programa';}
                            elseif ($dictamen->tp_id==4) 
                            {$html .= 'Acci&oacute;n de Funcionamiento';}
                            $html .= '
                        </td>
                    </tr>
                    <tr>
                        <td style="width:50%;">';
                            if($dictamen->tp_id==1)
                            {$html .= '<b>'.$dictamen->proy_sisin.'</b><br>';}
                            elseif ($dictamen->tp_id==2 || $dictamen->tp_id==3 || $dictamen->tp_id==4) 
                            {$html .= 'N/A<br>';}
                            $html .= 'SISIN
                        </td>
                        <td style="width:50%; text-align:center;">
                            <b>'.$aper_programatica->programatica.'</b><br>
                            Apertura Programatica
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align:center;">
                            <b>'.$aper_programatica->uni_unidad.'</b><br>
                            Unidad Responsable
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align:center;">
                            <b>'.$unidad_responsable[0]['uresp'].'</b><br>
                            Unidad Ejecutora
                        </td>
                    </tr>
                </table>

                </div><br><div class="contenedor_principal">
                    <div class="contenedor_datos">
                        <table class="table_contenedor">
                            <tr class="collapse_t">
                                <td style="width:30%;" class="fila_unitaria titulo_dictamen">
                                    <b>PRESUPUESTO TOTAL FASE </b>
                                </td>
                                <td style="width:70%;">'.number_format($fase[0]['pfec_ptto_fase'], 2, ',', '.').' Bs.</td>
                            </tr>
                        </table>
                    </div>
                    <div class="titulo_dictamen"> EFICACIA FINANCIERA DE LA ETAPA </div>
                    <div class="contenedor_datos">';
                        $i=1;$meses=12;$gestion=$fase[0]['pfec_fecha_inicio'];
                        for($kp=1;$kp<=$a�os;$kp++)
                        {
                            $html .= '
                            <table class="table table-bordered">
                                <tr class="collapse_t" style="background-color:#ac9fae;font-weight: bold;">
                                    <td style="width:18%" rowspan=2> ESTADO </td>
                                    <td colspan=12> GESTI&Oacute;N : '.$gestion.'</td>
                                </tr>
                                <tr class="collapse_t" style="background-color:#ac9fae;font-weight: bold;">
                                    <td>Ene.</td>
                                    <td>Feb.</td>
                                    <td>Mar.</td>
                                    <td>Abr.</td>
                                    <td>May.</td>
                                    <td>Jun.</td>
                                    <td>Jul.</td>
                                    <td>Ago.</td>
                                    <td>Sep.</td>
                                    <td>Oct.</td>
                                    <td>Nov.</td>
                                    <td>Dic.</td>
                                </tr>
                                <tr class="collapse_t">
                                    <td><b>PROGRAMADO</b></td>';
                                    for($pos=$i;$pos<=$meses;$pos++)
                                    {
                                        $html .= '<td>'.number_format($programado[$pos], 2, ',', '.').'</td>';
                                    }
                                    $html .= '
                                </tr>
                                <tr class="collapse_t">
                                    <td><b>PROGRAMADO ACUMULADO</b></td>';
                                    for($pos=$i;$pos<=$meses;$pos++)
                                    {
                                        $html .= '<td>'.number_format($prog_a[$pos], 2, ',', '.').'</td>';
                                    }
                                    $html .= '
                                </tr>
                                <tr class="collapse_t">
                                    <td><b>PROGRAMADO ACUMULADO EN %</b></td>';
                                    for($pos=$i;$pos<=$meses;$pos++)
                                    {
                                        $html .= '<td>'.number_format($prog_a2[$pos], 2, ',', '.').'</td>';
                                    }
                                    $html .= '
                                </tr>

                                <tr class="collapse_t">
                                    <td><b>EJECUCI&Oacute;N </b></td>';
                                    for($pos=$i;$pos<=$meses;$pos++)
                                    {
                                        $html .= '<td bgcolor="#98FB98">'.$ejecutado[$pos].'%</td>';
                                    }
                                    $html .= '
                                </tr>
                                <tr class="collapse_t">
                                    <td><b>EJECUCI&Oacute;N ACUMULDO</b></td>';
                                    for($pos=$i;$pos<=$meses;$pos++)
                                    {
                                        $html .= '<td bgcolor="#98FB98">'.$ejec_a[$pos].'%</td>';
                                    }
                                    $html .= '
                                </tr>
                                <tr class="collapse_t">
                                    <td><b>EJECUCI&Oacute;N ACUMULADO EN %</b></td>';
                                    for($pos=$i;$pos<=$meses;$pos++)
                                    {
                                        $html .= '<td bgcolor="#98FB98">'.$ejec_a2[$pos].'%</td>';
                                    }
                                    $html .= '
                                </tr>
                                <tr class="collapse_t">
                                    <td><b>EFICACIA</b></td>';
                                    for($pos=$i;$pos<=$meses;$pos++)
                                    {
                                        $html .= '<td bgcolor="#B0E0E6">'.$efi[$pos].'%</td>';
                                    }
                                    $i=$meses+1;
                                    $meses=$meses+12;
                                    $html .= '
                                </tr>
                            </table><br>';
                            $gestion++;           
                        } 
                        
                    $html .= '  
                    <br>
                
                    <table class="table_contenedor" style="margin-top: 10px;>
                        <tr>
                            <td class="fila_unitaria">FIRMAS:</td>
                        </tr>
                    </table>
                    <table style="width: 80%;margin: 0 auto;margin-top: 100px;margin-bottom: 50px;">
                        <tr>
                            <td style="width:30%">
                                <hr>
                            </td>
                            <td style="width:3%"></td>
                            <td style="width:30%">
                                <hr>
                            </td>
                            <td style="width:3%"></td>
                            <td style="width:30%">
                                <hr>
                            </td>
                        </tr>
                    </table>
                    </div>
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'landscape');
        ini_set('memory_limit','-1');
        ini_set('max_execution_time', 0);
        $dompdf->render();
        $dompdf->stream("programacion_ejecucion_fisica.pdf", array("Attachment" => false));
    }

    public function dato_proyecto($proy_id){
        $gestion = $this->session->userData('gestion');
        $proyecto = $this->model_proyecto->get_id_proyecto($proy_id); //// Datos del Proyecto
        $tabla ='';
        $tabla .='
       <style>
        .table{font-size: 10px;
              width: 100%;
              max-width:1550px;;
        overflow-x: scroll;
        }
        th{
          padding: 1.4px;
          text-align: center;
          font-size: 10px;
        }
      </style>';
      $tabla .='<div class="rojo"></div>
                <div class="verde"></div>
                <table class="change_order_items" style="width:100%;" align="center">
                    <tr>
                        <td width=20%; align=center>
                            <img src="'.base_url().'assets/img/logo.jpg" width="80%"/>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata('entidad').'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$this->gestion.'<br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                            <b>GRAFICO CURVA S : </b> PROGRAMACI&Oacute;N Y EJECUCI&Oacute;N PRESUPUESTARIA<br>
                            <b>ACCI&Oacute;N OPERATIVA : </b> '.$proyecto[0]['proy_nombre'].'<br>
                            <b>APERTURA PROGRAMATICA : </b> '.$proyecto[0]['aper_programa'].' '.$proyecto[0]['aper_proyecto'].' '.$proyecto[0]['aper_actividad'].'<br>
                        </td>
                        <td width=30%; align=center>
                            <img src="'.base_url().'assets/img/escudo.jpg" alt="" width="40%">
                        </td>
                    </tr>
                </table><hr>';
        return $tabla;
    }

       /*============================================= FICHA TECNICA DE PRODUCTOS =========================================*/
    function estilo_vertical2(){
        $estilo_vertical = '<style>
        body{
            font-family: sans-serif;
            }
        table{
            font-size: 8px;
            width: 100%;
            background-color:#fff;
        }
        .mv{font-size:10px;}
        .siipp{width:120px;}

        .titulo_pdf {
            text-align: left;
            font-size: 8px;
        }
        .tabla {
        font-family: Verdana, Arial, Helvetica, sans-serif;
        font-size: 8px;
        width: 100%;

        }
        .tabla th {
        padding: 2px;
        font-size: 6px;
        background-color: #1c7368;
        background-repeat: repeat-x;
        color: #FFFFFF;
        border-right-width: 1px;
        border-bottom-width: 1px;
        border-right-style: solid;
        border-bottom-style: solid;
        border-right-color: #558FA6;
        border-bottom-color: #558FA6;
        text-transform: uppercase;
        }
        .tabla .modo1 {
        font-size: 6px;
        font-weight:bold;
       
        background-image: url(fondo_tr01.png);
        background-repeat: repeat-x;
        color: #34484E;
       
        }
        .tabla .modo1 td {
        padding: 2px;
        border-right-width: 2px;
        border-bottom-width: 1px;
        border-right-style: solid;
        border-bottom-style: solid;
        border-right-color: #A4C4D0;
        border-bottom-color: #A4C4D0;
        }
    </style>';
        return $estilo_vertical;
    }

    private $estilo_vertical = '<style>
        body{
            font-family: sans-serif;
            }
        @page {
        margin: 40px 40px;
        }
        header {
            position: fixed;
            left: 0px;
            top: -20px;
            right: 0px;
            height: 20px;
            background-color: #fff;
            }
        header h1{
            margin: 5px 0;
        }
        header h2{
            margin: 0 0 8px 0;
        }
        th{
                padding: 1px;
                text-align: center;
                font-size: 7px;
                color: #ffffff;
            }
        footer {
            position: fixed;
            left: 0px;
            bottom: -82px;
            right: 0px;
            height: 100px;
            border-bottom: 1px solid #ddd;
        }
        footer .page:after {
            content: counter(page) ;
        }
        footer table {
            width: 100%;
        }
        footer p {
            text-align: right;
        }
        footer .izq {
            text-align: left;
        }
        table{
            font-size: 7px;
            width: 100%;
            background-color:#fff;
        }
        th, td {
                padding: 1px;
                text-align: center;
                font-size: 7px;
            }
        .mv{font-size:8px;}
        .siipp{width:140px;}
        .header_table {
            color: #ffffff;
            text-align: center;
            align-items: center;
            align-self: center;
            font-weight: bold;
            background-color: #696969;
            align-content: center;
        }
        .header_subtable {
            color: #ffffff;
            text-align: center;
            align-items: center;
            align-self: center;
            font-weight: bold;
            background-color: #a59393;
            align-content: center;
        }
        .titulo_pdf {
            text-align: left;
            font-size: 7px;
        }
        .datos_principales {
            text-align: center;
            font-size: 7px;
        }
        .pdes_titulo{
            text-decoration: underline;
            font-weight: bold;
            text-transform: uppercase;
        }
        .td_pdes{
            text-align: left;  
        }

        .indi_desemp{
            font-size: 8px;
            border: 1px solid #CCC;
            background-color: #E0E0E0;
            padding: .5em;
        }
        table.fixed { table-layout:fixed; }
        table.fixed td { overflow: hidden; }
        .sub_table{
            border-bottom:1px solid black;
        }
        .contenedor_principal{
            border-style: solid;
            border-width:1px;
        }
        .titulo_dictamen{
            padding:4px;
            background-color: #454545;
            border-style: solid;margin: 2px;
            border-width: 1px;
            color: white;
            font-size: 7px;
            border-color: black;
        }
        .contenedor_datos{
            padding:0px;
            margin:1px;
        }
        .table_contenedor{
            border-style: solid;
            margin: 0px;
            padding: 1px;
            border-width: 1px;
            border-collapse: collapse;
        }
        .collapse_t td{
            border-style: solid;
            margin: 0px;
            padding: 3px;
            border-width: 1px;
            border-collapse: collapse;
        }
        .fila_unitaria{
            text-align:left;
        }
        .lista{
            text-align:left;
            padding-left: 7;
            list-style-type:square;
            margin:2px;
        }
    </style>';

    function rolfun($rol)
    {
      $valor=false;
      for ($i=1; $i <=count($rol) ; $i++) { 
        $data = $this->Users_model->get_datos_usuario_roles($this->session->userdata('fun_id'),$rol[$i]);
        if(count($data)!=0){
          $valor=true;
          break;
        }
      }
      return $valor;
    } 
}