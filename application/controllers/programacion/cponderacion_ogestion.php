<?php

class Cponderacion_ogestion extends CI_Controller
{
    var $gestion;
    var $rol;
    var $fun_id;

    function __construct()
    {
        parent::__construct();
        $this->load->model('mantenimiento/mpoa');
        $this->load->model('programacion/ponderacion/mponderacion');
        $this->load->model('programacion/prog_poa/mobjetivo_gestion');
        //llamar a mi menu
        $this->load->library('menu');
        $this->menu->const_menu(1);
        $this->gestion = $this->session->userData('gestion');
        $this->rol = $this->session->userData('rol');
        $this->fun_id = $this->session->userData('fun_id');
    }

    //lista de programas para el objetivo de gestion
    function lista_red_objetivos_o()
    {
        $ruta = '/prog/pond_ogestion/';
        $data['lista_poa'] = $this->tabla_red_objetivos_ogestion($ruta);
        $ruta = 'programacion/ponderacion/obje_gestion/vred_programa';
        $this->construir_vista($ruta, $data);
    }

    //LISTA DE OBJETIVOS DE GESTION
    function lista_ogestion($poa_id)
    {
        $dato = $this->tabla_ogestion_pond($poa_id);
        $data['dato_poa'] = $this->mponderacion->get_poa($poa_id);
        $data['lista_ogestion'] = $dato['tabla'];
        $data['ponderacion'] = $dato['ponderacion'];
        $data['ponderacion_js'] = '<SCRIPT src="' . base_url() . 'mis_js/programacion/ponderacion/ponderacion_ogestion.js" type="text/javascript"></SCRIPT>';
        $ruta = 'programacion/ponderacion/obje_gestion/vlista_ogestion_pond';
        $this->construir_vista($ruta, $data);
    }

    //GENERAR TABLA DE OBJETIVO DE GESTION CON PONDERACION FILTRADO POR POA ID
    function tabla_ogestion_pond($poa_id)
    {
        $lista_objgestion = $this->mponderacion->poa_obje_estrategico($poa_id);
        $cont = 1;
        $tabla = '';
        $ponderacion = 0;
        foreach ($lista_objgestion as $row) {
            $ponderacion += $row['o_ponderacion'];
            $tabla .= '<tr>';
            $tabla .= '<td >' . $cont . '</td>';
            $tabla .= '<td >
                            <a href="" class="form_pond" data-toggle="modal" data-target="#modal_ponderacion" name="' . $row['o_id'] . '">
                                <img src="' . base_url() . 'assets/ifinal/form2.png" width="50" height="50" class="img-responsive"
                                title="ASIGNAR PONDERACION">
							</a>
					   </td>';
            $tabla .= '<td >' . $row['o_codigo'] . '</td>';
            $tabla .= '<td >' . $row['o_objetivo'] . '</td>';
            $tabla .= '<td >' . $row['fun_nombre'] . ' ' . $row['fun_paterno'] . ' ' . $row['fun_materno'] . '</td>';
            $tabla .= '<td >' . $row['uni_unidad'] . '</td>';
            $tabla .= '<td style="text-align: right">' . intval($row['o_ponderacion']) . '%</td>';
            $tabla .= '</tr >';
            $cont++;
        }
        $data['tabla'] = $tabla;
        $data['ponderacion'] = $ponderacion;
        return $data;
    }

    //GENERAR TABLA DE RED DE OBJETIVOS PARA LA PONDERACION DE OBJETIVO DE GESTION
    function tabla_red_objetivos_ogestion($ruta)
    {
        $lista_poa = $this->mpoa->lista_poa();
        $cont = 1;
        $tabla = '';
        foreach ($lista_poa as $row) {
            $lista_objgestion = $this->mponderacion->poa_obje_estrategico($row['poa_id']);
            $di ="";
            $r = "";
            if(count($lista_objgestion) != 0){
                $di = "ASIGNAR PONDERACION";
                $r = site_url("") . $ruta . $row['poa_id'];
            }else{
                $di = "SIN ACCIONES DE CORTO PLAZO";
            }

            $tabla .= '<tr>';
            $tabla .= '<td >
                            <a href="' . $r . '" class="enviar_poa" name="enviar_poa" id="enviar_poa">
                                <img src="' . base_url() . 'assets/img/folder.png" width="30" height="30" class="img-responsive "title="'.$di.'">
							</a>
					   </td>';
            $tabla .= '<td>' . $row['poa_codigo'] . '</td>';
            $tabla .= '<td>' . $row['aper_programa'] . $row['aper_proyecto'] . $row['aper_actividad'] . " - " . $row['aper_descripcion'] . '</font></td>';
            $tabla .= '<td>' . $row['uni_unidad'] . '</td>';
            $tabla .= '<td>' . $row['poa_fecha_creacion'] . '</td>';
            $tabla .= '</tr>';
            $cont++;
        }
        return $tabla;
    }

    //OBTENER DATOS PARA EL LLENAFO DEL FORMULARIO DE ADICIONAR PONDERACION
    function get_form_ogestion_pond()
    {
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $post = $this->input->post();
            $o_id = $post['o_id'];
            $get_ogestion = $this->mobjetivo_gestion->get_ogestion($o_id);
            $result = array(
                'id' => $get_ogestion[0]['o_id'],
                'codigo' => $get_ogestion[0]['o_codigo'],
                'objetivo' => $get_ogestion[0]['o_objetivo'],
                'ponderacion' => $get_ogestion[0]['o_ponderacion'],
            );
            echo json_encode($result);
        } else {
            show_404();
        }
    }

    //GUARDAR PONDERACION DE OBJETIVO DE GESTION
    function add_pond_ogestion()
    {
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $post = $this->input->post();
            $o_id = $post['o_id'];
            $ponderacion = $post['pond'];
            if ($this->mponderacion->add_pond_ogestion($o_id, $ponderacion)) {
                $result['peticion'] = 'true';
            } else {
                $result['peticion'] = 'true';
            }
            echo json_encode($result);
        } else {
            show_404();
        }
    }

    //FUNCION PARA CONSTRUIR MI VISTA
    function construir_vista($ruta, $data)
    {
        //----------------------------------- MENU-------------------------------
        $menu['enlaces'] = $this->menu->get_enlaces();
        $menu['subenlaces'] = $this->menu->get_sub_enlaces();
        $menu['titulo'] = 'PROGRAMACIÓN';
        //-----------------------------------------------------------------------
        $this->load->view('includes/header');
        $this->load->view('includes/menu_lateral', $menu);
        $this->load->view($ruta, $data);//contenido
        $this->load->view('includes/footer');
    }
}