<?php

class Cprog_insumos extends CI_Controller
{
    var $gestion;
    var $fun_id;
    public $rol = array('1' => '2','2' => '3','3' => '4');
    function __construct()
    {
        parent::__construct();
        $this->load->model('Users_model','',true);
        if($this->rolfun($this->rol)){
            if($this->session->userdata('fun_id')!=null){
                $this->load->model('menu_modelo');
                $this->load->model('programacion/insumos/minsumos');
                $this->load->model('programacion/insumos/minsumos_partida');
                $this->load->model('programacion/insumos/minsumos_delegado');
                $this->load->model('mantenimiento/model_partidas');
                $this->load->model('mantenimiento/model_entidad_tras');
                $this->load->model('programacion/model_proyecto');
                $this->load->model('programacion/model_faseetapa');
                $this->load->model('programacion/model_componente');
                $this->gestion = $this->session->userData('gestion');
                //$this->rol = $this->session->userData('rol');
                $this->fun_id = $this->session->userData('fun_id');
            }else{
                redirect('/','refresh');
            }
        }
        else{
            redirect('admin/dashboard');
        }
    }

    function insumos($proy_id, $tipo) {
        if($proy_id!='' & $tipo!=''){
            if ($tipo == 1) {
                //PROGRAMACION DIRECTA, DIRECTO = 1
                $this->lista_productos($proy_id);
            } else {
                //PROGRAMACION DELEGADA, DELEGADA = 2
                $this->delegado($proy_id);
            }
        }
        else{
            redirect('admin/dashboard');
        }
    }

    /////PROGRAMACION INSUMOS DELEGADA
    function delegado($proy_id){
        if($proy_id!=''){
            $enlaces=$this->menu_modelo->get_Modulos_programacion(1);
            $data['enlaces'] = $enlaces;
            $data['nro_fase']= $this->model_faseetapa->nro_fase($proy_id); /// nro de fases y etapas registrados
            $data['id_f'] = $this->model_faseetapa->get_id_fase($proy_id); //// DATOS DE LA FASE ACTIVA
            $data['mod']=4;

            $data['proy_id'] = $proy_id;
            $data['dato_proy'] = $this->minsumos->dato_proyecto($proy_id); //// PROY ID
            $data['proyecto'] = $this->model_proyecto->get_id_proyecto($proy_id); //// DATOS DEL PROYECTO
            $data['fase'] = $this->model_faseetapa->get_id_fase($proy_id); //// DATOS DE LA FASE ACTIVA
            $componentes=$this->model_componente->componentes_id($data['fase'][0]['id']);  //// COMPONENTES DE LA FASE ACTIVA

            $titulo_proy=strtoupper($data['proyecto'][0]['tipo']);
            $data['titulo_proy'] = $titulo_proy;

            $data['tabla_fuentes'] = $this->fuentes_financiamientos($proy_id, $this->gestion,2); //// Tabla lista de Activos del Componente
            /*---------------------------------------------------------------------------------------------------------*/
            $tabla = '';
            $cont = 1;
            foreach ($componentes AS $row)
            {
                $tabla .= '<tr>';
                $tabla .= '<td>' . $cont . '</td>';
                $tabla .= '<td>
                                <a href="' . site_url("") . '/prog/ins_part_com/' . $proy_id . '/' . $row['com_id'] . '">
                                    <center>
                                        <img src="' . base_url() . 'assets/ifinal/insumo.png" width="30" height="30" class="img-responsive "title="ASIGNAR INSUMOS">
                                    </center>
                                </a>
                            </td>';
                $tabla .= '<td>' . $row['com_componente'] . '</td>';
                $tabla .= '<td>' . $row['com_ponderacion'] . '</td>';
                $tabla .= '</tr>';
                $cont++;
            }
            /*---------------------------------------------------------------------------------------------------------*/

            $data['tabla']=$tabla;
            $this->load->view('admin/programacion/insumos/insumo_componente/list_componentes', $data);
        }
        else{
            redirect('admin/dashboard');
        }
    }

    //PROGRAMACION DIRECTA (Wilmer)
    function lista_productos($proy_id){
        if($proy_id!=''){
            $enlaces=$this->menu_modelo->get_Modulos_programacion(1);
            $data['enlaces'] = $enlaces;
            for($i=0;$i<count($enlaces);$i++)
            {
                $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Modulos_sub($enlaces[$i]['o_child']);
            }
            $data['subenlaces'] = $subenlaces;
            $data['proy_id'] = $proy_id;
            $data['dato_proy'] = $this->minsumos->dato_proyecto($proy_id);
            $data['proyecto'] = $this->model_proyecto->get_id_proyecto($proy_id);
     
            $data['titulo_proy'] = strtoupper($data['proyecto'][0]['tipo']);
            $data['fase'] = $this->model_faseetapa->get_id_fase($proy_id); //// DATOS DE LA FASE ACTIVA

            $data['lista_productos'] = $this->genera_tabla_prod_act($proy_id); /// Lista de Productos
            $data['mod']=4;

            $this->load->view('admin/programacion/insumos/insumo_actividades/list_productos', $data);
        }
        else{
            redirect('admin/dashboard');
        }
    }



    /*----------------GENERA TABLA FUENTES DE FINANCIAMIENTO PROGRAMADAS PARA LA OPERACION -----------*/
    public function fuentes_financiamientos($proy_id,$gestion,$tp_ejec) /////// GENERA TABLA DE FUENTES , ORGANISMOS
    {
        $list_recursos = $this->model_faseetapa->presupuesto_asignados($proy_id,$gestion); //// lista de recursos asignados
        $tabla = '';
        $nro=1; $suma_costo_total=0; $monto_asignado=0; $monto_programado=0;
        foreach ($list_recursos as $row) {
            $suma_prog = $this->minsumos->suma_monto_prog_insumo($proy_id,$gestion,$tp_ejec); //// Suma Programado Insumo
            $tabla .= '<tr>';
            $tabla .= '<td>' . $nro . '</td>';
            $tabla .= '<td>' . $row['ff_codigo'] . '</td>';
            $tabla .= '<td>' . $row['ff_descripcion'] . '</td>';
            $tabla .= '<td>' . $row['of_codigo'] . '</td>';
            $tabla .= '<td>' . $row['of_descripcion'] . '</td>';
            $tabla .= '<td style="text-align: right;">' . number_format($row['presupuesto_asignado'], 2, ',', '.') . '</td>';
            $tabla .= '<td style="text-align: right;">' . number_format($suma_prog[0]['programado'], 2, ',', '.') . '</td>';
            $tabla .= '<td style="text-align: right;">' . number_format(($row['presupuesto_asignado']-$suma_prog[0]['programado']), 2, ',', '.') . '</td>';
            $tabla .= '</tr>';
            $nro++;
            $monto_asignado=$monto_asignado+$row['presupuesto_asignado'];
            $monto_programado=$monto_programado+$suma_prog[0]['programado'];
        }
        $tabla .= '<tr>';
        $tabla .= '<td colspan="5" style="background:#646464;">
                        <b style="color:#fff;"><center>T O T A L</center></b>
                   </td>';
        $tabla .= '<td style="text-align: right; background:#646464;"><b style="color:#fff;">' . number_format($monto_asignado, 2, ',', '.') . '</b></td>';
        $tabla .= '<td style="text-align: right; background:#646464;"><b style="color:#fff;">' . number_format($monto_programado, 2, ',', '.') . '</b></td>';
        $tabla .= '<td style="text-align: right; background:#646464;"><b style="color:#fff;">' . number_format(($monto_asignado-$monto_programado), 2, ',', '.') . '</b></td>';
        $tabla .= '</tr>';
        return $tabla;
    }
    public function fuentes_financiamientos_programados($proy_id,$gestion,$tp_ejec) /////// GENERA TABLA DE FUENTES , ORGANISMOS
    {
        $list_recursos = $this->minsumos_partida->presupuesto_total_gestion($proy_id,$gestion,$tp_ejec); //// lista de recursos asignados
        $tabla = '';
        $nro=1; $suma_costo_total=0; $monto_asignado=0; $monto_programado=0;
        foreach ($list_recursos as $row) {
            $presup_asignado = $this->minsumos_partida->suma_monto_asignado_insumo($proy_id,$row['of_id'],$row['ff_id'],$gestion,$tp_ejec);
            $tabla .= '<tr>';
            $tabla .= '<td>' . $nro . '</td>';
            $tabla .= '<td>' . $row['ff_codigo'] . '</td>';
            $tabla .= '<td>' . $row['ff_descripcion'] . '</td>';
            $tabla .= '<td>' . $row['of_codigo'] . '</td>';
            $tabla .= '<td>' . $row['of_descripcion'] . '</td>';
            $tabla .= '<td style="text-align: right;">' . number_format($presup_asignado[0]['asignado'], 2, ',', '.') . '</td>';
            $tabla .= '<td style="text-align: right;">' . number_format($row['programado'], 2, ',', '.') . '</td>';
            $tabla .= '<td style="text-align: right;">' . number_format(($presup_asignado[0]['asignado']-$row['programado']), 2, ',', '.') . '</td>';
            $tabla .= '</tr>';
            $nro++;
            $monto_asignado=$monto_asignado+$presup_asignado[0]['asignado'];
            $monto_programado=$monto_programado+$row['programado'];
        }
        $tabla .= '<tr>';
        $tabla .= '<td colspan="5" style="background:#646464;">
                        <b style="color:#fff;"><center>T O T A L</center></b>
                   </td>';
        $tabla .= '<td style="text-align: right; background:#646464;"><b style="color:#fff;">' . number_format($monto_asignado, 2, ',', '.') . '</b></td>';
        $tabla .= '<td style="text-align: right; background:#646464;"><b style="color:#fff;">' . number_format($monto_programado, 2, ',', '.') . '</b></td>';
        $tabla .= '<td style="text-align: right; background:#646464;"><b style="color:#fff;">' . number_format(($monto_asignado-$monto_programado), 2, ',', '.') . '</b></td>';
        $tabla .= '</tr>';
        return $tabla;
    }


    //GENERAR LA TABLA DE PRESUPUESTO DE MI PROYECTO
    function genera_tabla_presupuesto($proy_id, $gestion)
    {
        $lista_presupuesto = $this->minsumos->tabla_presupuesto($proy_id, $gestion);
        $saldo_total = $this->minsumos->saldo_total_fin($proy_id, $gestion);
        $tabla = '';
        $cont = 1;
        foreach ($lista_presupuesto as $row) {
            $tabla .= '<tr>';
            $tabla .= '<td>' . $cont . '</td>';
            $tabla .= '<td>' . $row['ff_codigo'] . '</td>';
            $tabla .= '<td>' . $row['ff_descripcion'] . '</td>';
            $tabla .= '<td>' . $row['of_codigo'] . '</td>';
            $tabla .= '<td>' . $row['of_descripcion'] . '</td>';
            $tabla .= '<td style="text-align: right;">' . number_format($row['presupuesto_asignado'], 2, ',', '.') . '</td>';
            $tabla .= '<td style="text-align: right;">' . number_format($row['presupuesto_programado'], 2, ',', '.') . '</td>';
            $tabla .= '<td style="text-align: right;">' . number_format($row['saldo'], 2, ',', '.') . '</td>';
            $tabla .= '</tr>';
            $cont++;
        }
        $tabla .= '<tr>';
        $tabla .= '<td colspan="5" style="background:#646464;">
                        <b style="color:#fff;"><center>T O T A L</center></b>
                   </td>';
        $tabla .= '<td style="text-align: right; background:#646464;"><b style="color:#fff;">' . number_format($saldo_total->asignado, 2, ',', '.') . '</b></td>';
        $tabla .= '<td style="text-align: right; background:#646464;"><b style="color:#fff;">' . number_format($saldo_total->programado, 2, ',', '.') . '</b></td>';
        $tabla .= '<td style="text-align: right; background:#646464;"><b style="color:#fff;">' . number_format($saldo_total->saldo_total, 2, ',', '.') . '</b></td>';
        $tabla .= '</tr>';
        return $tabla;

    }

    //tabla de productos con sus respectivas actividades
    function genera_tabla_prod_act($proy_id){
        $lista_productos = $this->minsumos->lista_productos($proy_id, $this->gestion);
        $tabla = '';
        $cont_acordion = 0;
        foreach ($lista_productos as $row) {
            $cont_acordion++;
            $tabla .= '<div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse' . $cont_acordion . '">
                                        <i class="fa fa-lg fa-angle-down pull-right"></i> <i class="fa fa-lg fa-angle-up pull-right"></i>' .
                $cont_acordion . ' - ' . $row['prod_producto'] . '
                                    </a>
                                </h4>
                            </div>';
            $tabla .= '<div id="collapse' . $cont_acordion . '" class="panel-collapse collapse">
                            <div class="panel-body no-padding table-responsive">
                                <table class="table table-bordered table-condensed">';
            $tabla .= '            <tbody>
                                          <tr>
                                              <td>NRO.</td>
                                              <td>ASIGNAR INSUMOS</td>
                                              <td>ACTIVIDAD</td>
                                              <td>TIPO INDICADOR</td>
                                              <td>INDICADOR</td>
                                              <td>PONDERACI&Oacute;N</td>
                                          </tr>';
            $lista_actividad = $this->minsumos->lista_actividades($row['prod_id'], $this->gestion);
            $cont = 1;
            foreach ($lista_actividad as $row_a) {
                $tabla .= '<tr>';
                $tabla .= '<td>' . $cont_acordion . '-' . $cont . '</td>';
                $tabla .= '<td>
                               <a href="' . site_url("") . '/prog/ins_part/' . $proy_id . '/' . $row['prod_id'] . '/' . $row_a['act_id'] . '">
                                    <center>
                                        <img src="' . base_url() . 'assets/ifinal/money.png" width="30" height="30"
                                        class="img-responsive "title="ASIGNAR INSUMOS">
                                    </center>
                               </a>
                          </td>';
                $tabla .= '<td>' . $row_a['act_actividad'] . '</td>';
                $tabla .= '<td>' . $row_a['indicador'] . '</td>';
                $tabla .= '<td>' . $row_a['act_indicador'] . '</td>';
                $tabla .= '<td>' . $row_a['act_ponderacion'] . '</td>';
                $tabla .= '</tr>';
                $cont++;
            }
            $tabla .= '             </tbody>
                                </table>
                           </div>
                      </div>
                 </div>';
        }
        return $tabla;
    }

    //OBTENER MI TITULO
    function get_ins_titulo($num)
    {
        switch ($num) {
            case 1:
                return (' - RECURSO HUMANO PERMANETE');
                break;
            case 2:
                return (' - DETERMINACI&Oacute;N DE SERVICIOS');
                break;
            case 3:
                return (' - PASAJES');
                break;
            case 4:
                return (' - VIÁTICOS');
                break;
            case 5:
                return (' - CONSULTORÍA POR PRODUCTO');
                break;
            case 6:
                return (' - CONSULTORÍA EN LÍNEA');
                break;
            case 7:
                return (' - MATERIALES Y SUMINISTROS');
                break;
            case 8:
                return (' - ACTIVOS FIJOS');
                break;
            case 9:
                return (' - OTROS INSUMOS');
                break;
        }
    }

    //fUNCION OBTENER PARTIDAS
    function get_partidas_hijos()
    {
        if ($this->input->post()) {
            $par_id = $this->input->post('par_id');
            $get_partida = $this->minsumos->get_partida($par_id);
            $partidas_dep = $this->minsumos->lista_par_dependientes($get_partida->par_codigo);//partidas dependientes
            $combo = '<option value="">Seleccione una opcion</option>';
            foreach ($partidas_dep as $row) {
                $combo .= '<option value="' . $row['par_id'] . '">' . $row['par_codigo'] . ' - ' . $row['par_nombre'] . '</option>';
            }
            echo $combo;
        } else {
            show_404();
        }
    }
    //-------------------------------- PROGRAMACION DELEGADA -----------------------
    //tabla de COMPONENTES PARA INSUMOS
    function genera_tabla_componentes($proy_id)
    {
        $lista_componentes = $this->minsumos_delegado->lista_componentes($proy_id);
        $tabla = '';
        $cont = 1;
        foreach ($lista_componentes AS $row) {
            $tabla .= '<tr>';
            $tabla .= '<td>' . $cont . '</td>';
            $tabla .= '<td>
                           <a href="' . site_url("") . '/prog/ins_com/' . $proy_id . '/' . $row['com_id'] . '">
                                <center>
                                    <img src="' . base_url() . 'assets/ifinal/insumo.png" width="30" height="30" class="img-responsive "title="ASIGNAR INSUMOS">
                                </center>
                           </a>
                       </td>';
            $tabla .= '<td>' . $row['com_componente'] . '</td>';
            $tabla .= '</tr>';
            $cont++;
        }
        return $tabla;
    }

    function rolfun($rol)
    {
        $valor=false;
        for ($i=1; $i <=count($rol) ; $i++) {
            $data = $this->Users_model->get_datos_usuario_roles($this->session->userdata('fun_id'),$rol[$i]);
            if(count($data)!=0){
                $valor=true;
                break;
            }
        }
        return $valor;
    }

    /*=========================================================================================================================*/
    public function get_mes($mes_id)
    {
        $mes[1]='Enero';
        $mes[2]='Febrero';
        $mes[3]='Marzo';
        $mes[4]='Abril';
        $mes[5]='Mayo';
        $mes[6]='Junio';
        $mes[7]='Julio';
        $mes[8]='Agosto';
        $mes[9]='Septiembre';
        $mes[10]='Octubre';
        $mes[11]='Noviembre';
        $mes[12]='Diciembre';

        $dias[1]='31';
        $dias[2]='28';
        $dias[3]='31';
        $dias[4]='30';
        $dias[5]='31';
        $dias[6]='30';
        $dias[7]='31';
        $dias[8]='31';
        $dias[9]='30';
        $dias[10]='31';
        $dias[11]='30';
        $dias[12]='31';

        $valor[1]=$mes[$mes_id];
        $valor[2]=$dias[$mes_id];

        return $valor;
    }
}