<?php

/**
 * Created by PhpStorm.
 * User: user
 * Date: 17/01/2017
 * Time: 10:49
 */
class Cprog_red_objetivos extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->model('mantenimiento/mpoa');
        $this->load->model('programacion/marco_estrategico/mobjetivos');
        //llamar a mi menu
        $this->load->library('menu');
        $this->menu->const_menu(1);
    }
    //FUNCION PARA CONSTRUIR MI VISTA
    function construir_vista($ruta, $data)
    {
        //----------------------------------- MENU-------------------------------
        $menu['enlaces'] = $this->menu->get_enlaces();
        $menu['subenlaces'] = $this->menu->get_sub_enlaces();
        $menu['titulo'] = 'PROGRAMACIÓN';
        //-----------------------------------------------------------------------
        //armar vista
        $this->load->view('includes/header');
        $this->load->view('includes/menu_lateral', $menu);
        $this->load->view($ruta, $data);//contenido
        $this->load->view('includes/footer');
    }
    //PARA VISTA DE PROGRAMACION DEL POA - RED DE OBJETIVOS
    function index(){
        //-----------------------------------------------------------------------
        $data['lista_poa'] = $this->mpoa->lista_poa();
        $ruta = 'programacion/prog_poa/red_objetivos/vred_objetivos';
        $this->construir_vista($ruta, $data);
    }
    //LISTA DE OBJETIVOS ESTRATEGICOS DEL POA
    function lista_objetivos($poa_id){
        //-----------------------------------------------------------------------
        $data['dato_poa'] = $this->mpoa->dato_poa_id($poa_id);
        $lista_objetivos = $this->mobjetivos->lista_obje_poa($poa_id);
        $temporalizacion = array();
        foreach ($lista_objetivos as $row1) {
            $obje_id = $row1['obje_id'];// id de mi objetivo estrategico
            $gestion_inicial = $row1['obje_gestion_curso'];// id de mi objetivo estrategico
            $linea_base = $row1['obje_linea_base'];
            $meta = $row1['obje_meta']; //variable meta
            $temporalizacion[$obje_id] = $this->progracion_gestion($obje_id, $gestion_inicial, $linea_base, $meta);
        }
        $data['lista_objetivos'] = $lista_objetivos;
        $data['temporalizacion'] = $temporalizacion;
        $gestion_inicio = $this->mobjetivos->get_fec_inicio();
        $data['gestion_inicio'] = $gestion_inicio->conf_gestion_desde;

        $ruta = 'programacion/prog_poa/red_objetivos/vlista_objetivos_poa';
        $this->construir_vista($ruta, $data);
    }
    //OBTENER TEMPORALIZACION DE PROGRMACION
    function progracion_gestion($obje_id, $gestion_inicial, $linea_base, $meta)
    {
        $cont = 1;
        $prog_acumulado = $linea_base; //variable de programacion acumulada
        for ($i = $gestion_inicial; $i <= ($gestion_inicial + 4); $i++) {
            $puntero_prog = 'prog' . $cont;//PUNTERO PARA LISTAR LA PROGRAMACION
            $dat_prog = $this->mobjetivos->get_prog_obj($obje_id, $i);
            $dato_programado = $dat_prog[0]['opm_programado'];
            $temporalizacion[$puntero_prog] = $dato_programado;//matriz
            //PROGRAMADO ACUMULADO
            $puntero_prog_acumulado = 'p_acumulado' . $cont;//PUNTERO PARA LA PROGRAMACION ACUMULADA
            $prog_acumulado += $dato_programado;
            $temporalizacion[$puntero_prog_acumulado] = $prog_acumulado;//matriz
            //PROGRAMA ACUMULADO PORECENTUAL
            $puntero_pa_porcentual = 'pa_porc' . $cont;//PUNTERO PARA LA PROGRAMACION ACUMULADA PORCENTUAL
	      if($meta == 0){
                $pa_porcentual = 0;
            }else{
                $pa_porcentual = (($prog_acumulado / $meta) * 100);
            }	           
            $temporalizacion[$puntero_pa_porcentual] = $pa_porcentual;//matriz
            $cont++;
        }
        return $temporalizacion;
    }



}