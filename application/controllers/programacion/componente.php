<?php
class Componente extends CI_Controller { 
    public $rol = array('1' => '3','2' => '4');
    public function __construct ()
    {
        parent::__construct();
        if($this->session->userdata('fun_id')!=null){
            $this->load->model('Users_model','',true);
            if($this->rolfun($this->rol)){ 
            $this->load->model('programacion/model_proyecto');
            $this->load->model('programacion/model_faseetapa');
            $this->load->model('programacion/model_componente');
            $this->load->model('programacion/model_producto');
            $this->load->model('programacion/insumos/minsumos');
            $this->load->model('programacion/model_actividad');
            $this->load->model('menu_modelo');
            }else{
                redirect('admin/dashboard');

            }
        }
        else{
        redirect('/','refresh');
      }
    }

    /*----Actualiza la ponderación de componentes----*/
    public function actualiza_ponderacion($pfec_id,$com_id,$prod_id)
    {
        /*================================ ACTUALIZANDO PONDERACIONES ACTIVIDADES ===============================*/
        $suma = $this->model_actividad->suma_monto_ponderado_total($prod_id);
        $act = $this->model_actividad->list_act_anual($prod_id);
        $ponderacion=0;
        foreach ($act as $row)
        {
            $ponderacion=round((($row['act_pres_p']/$suma[0]['monto_total'])*100),2);
            $update_act = array(
                'act_ponderacion' => $ponderacion
            );

            $this->db->where('act_id', $row['act_id']);
            $this->db->update('_actividades', $update_act);

        }
        /*===========================================================================================*/
        /*========================================= ACTUALIZANDO PONDERACIONES PRODUCTOS ========================*/
        $productos= $this->model_producto->list_prod($com_id);
        $sumatoria_total=0;
        foreach ($productos as $rowp)
        {
            $suma_pa = $this->model_actividad->suma_monto_ponderado_total($rowp['prod_id']);
            $sumatoria_total=$sumatoria_total+$suma_pa[0]['monto_total'];
        }

        $ponderacion=0;
        foreach ($productos as $rowp)
        {
            $suma_prod = $this->model_actividad->suma_monto_ponderado_total($rowp['prod_id']);
            $ponderacion=round((($suma_prod[0]['monto_total']/$sumatoria_total)*100),2);

            $update_prod = array(
                'prod_ponderacion' => $ponderacion
            );
            $this->db->where('prod_id', $rowp['prod_id']);
            $this->db->update('_productos', $update_prod);
        }
        /*============================================================================================================*/

        /*========================================= ACTUALIZANDO PONDERACIONES COMPONENTES =============================*/
        $componente= $this->model_componente->componentes_id($pfec_id);
        $sumatoria_comp=0;
        foreach ($componente as $rowc)
        {
            $productos= $this->model_producto->list_prod($rowc['com_id']);
            $sumatoria_p=0;
            foreach ($productos as $rowp)
            {
                $suma_pa = $this->model_actividad->suma_monto_ponderado_total($rowp['prod_id']);
                $sumatoria_p=$sumatoria_p+$suma_pa[0]['monto_total'];
            }
            $sumatoria_comp=$sumatoria_comp+$sumatoria_p;
        }

        $ponderacion=0;
        foreach ($componente as $rowc)
        {
            $productos= $this->model_producto->list_prod($rowc['com_id']);
            $sumatoria_p=0;
            foreach ($productos as $rowp)
            {
                $suma_pa = $this->model_actividad->suma_monto_ponderado_total($rowp['prod_id']);
                $sumatoria_p=$sumatoria_p+$suma_pa[0]['monto_total'];
            }
            $ponderacion=round((($sumatoria_p/$sumatoria_comp)*100),2);
            $update_comp = array(
                'com_ponderacion' => $ponderacion
            );
            $this->db->where('com_id', $rowc['com_id']);
            $this->db->update('_componentes', $update_comp);

        }
    }
 /*====================================== LISTA DE COMPONENTES ================================*/
  public function lista_componentes($mod,$id_f,$id_p) 
  { 
    if($mod!='' & $id_f!='' & $id_p!=''){ 
        $enlaces=$this->menu_modelo->get_Modulos_programacion(1);
        $data['enlaces'] = $enlaces;
        $data['nro_fase']= $this->model_faseetapa->nro_fase($id_p); /// nro de fases y etapas registrados
        $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p); 
        $data['id_f'] = $this->model_faseetapa->get_id_fase($id_p); //// recupera datos de la tabla fase activa
        $datos = $this->model_faseetapa->datos_fase_etapa($id_f,$id_p);  // desvuelve las fechas del proyecto
        $a�os=$datos[0]['final']-$datos[0]['actual'];
        $data['gest']=$a�os; ///// gestiones de la fase activa
        $data['fecha']=$datos[0]['actual'];
        $data['mod']=$mod;
        
        if($data['proyecto'][0]['tp_id']==1)
            $data['titulo_com']='COMPONENTES';
        else
            $data['titulo_com']='PROCESOS';

        $titulo_proy=strtoupper($data['proyecto'][0]['tipo']);
        $data['titulo_proy'] = $titulo_proy;
        $data['responsable']=$this->model_proyecto->responsable_proy($id_p,'1'); /// REsponsable de la operacion
        $data['fun1']=$this->model_proyecto->asig_responsables(3); ////// tecnico OPERATIVO
        $data['unidades']=$this->model_proyecto->list_unidad_org(); ////// Unidad Organizacional

        if($this->session->userdata("rol_id")==1 || $data['proyecto'][0]['fun_id']==$this->session->userdata("fun_id")){
            $comp=$this->model_componente->componentes_id($id_f);    
        }
        else{
           $comp=$this->model_componente->componentes_fun_id($id_f,$this->session->userdata("fun_id"));     
        }
  
        $tabla ='';
        $num=0; $ponderacion=0;
        foreach($comp as $row)
        {
            $num++;
            $tabla .='<tr>';
                $tabla .='<td>'.$num.'</td>';
                $tabla .='<td align="center"><a href="#" data-toggle="modal" data-target="#modal_mod_ff" class="btn btn-xs mod_ff" title="MODIFICAR DE REGISTRO COMPONENTES" name="'.$row['com_id'].'" id="'.$row['com_ponderacion'].'"><img src="' .base_url(). 'assets/ifinal/modificar.png" WIDTH="35" HEIGHT="35"/><br><font size="1">MODIFICAR</font></a></td>';
                $tabla .='<td align="center"><a href="'.site_url("admin").'/prog/delete_comp/'.$mod.'/'.$data['id_f'][0]['id']."/".$data['id_f'][0]['proy_id'].'/'.$row['com_id'].'" title="ELIMINAR '.$data['titulo_com'].'" onclick="return confirmar()"><img src="' .base_url(). 'assets/ifinal/eliminar.png" WIDTH="35" HEIGHT="35"/><br>ELIMINAR</a><br></td>';
                $tabla .='<td>'.$row['com_componente'].'</td>';
                $tabla .='<td>'.$row['fun_nombre'].' '.$row['fun_paterno'].' '.$row['fun_materno'].'</td>';
                $tabla .='<td>'.$row['uni_unidad'].'</td>';
                $tabla .='<td>'.$row['com_ponderacion'].' %</td>';
                $tabla .='<td align="center"><a href="'.site_url("admin").'/prog/list_prod/'.$mod.'/'.$data['id_f'][0]['id'].'/'.$data['id_f'][0]['proy_id'].'/'.$row['com_id'].'"  title="INGRESAR A PRODUCTOS"><img src="'.base_url().'assets/ifinal/archivo.png" WIDTH="45" HEIGHT="45"/><br>REGISTRO DE<br>PRODUCTOS</a></td>';
            $tabla .='</tr>';
            $ponderacion=$ponderacion+$row['com_ponderacion'];
        }
        $tabla .='<tr>';
            $tabla .='  <th style="width:3%;"></th>
                        <th style="width:2%;"></th>
                        <th style="width:2%;"></th>
                        <th style="width:20%;"></th>
                        <th style="width:20%;"></th>
                        <th style="width:20%;"></th>
                        <th style="width:5%;" align="center">'.($ponderacion).' %</th>
                        <th style="width:5%;"></th>';
        $tabla .='</tr>';

        $data['componentes']=$tabla;
        $this->load->view('admin/programacion/componente/list_componentes', $data);  
    }
    else{
      redirect('admin/dashboard');
    }
  }

 /*====================================== VALIDA COMPONENTE ================================*/
function valida_componente(){
        if($this->input->is_ajax_request() && $this->input->post()){
            $this->form_validation->set_rules('id_f', 'id fase', 'required|trim');
            $this->form_validation->set_rules('comp', 'Componente', 'required|trim');
            $this->form_validation->set_rules('modalidad', 'Modalidad de ejecucion', 'required|trim');
            //=========================== mensajes =========================================
            $this->form_validation->set_message('required', 'El campo es es obligatorio');
            $this->form_validation->set_message('integer', 'El campo  debe poseer solo numeros enteros');
            
            if ($this->form_validation->run() ) {
                $id_f=  $this->input->post('id_f');
                $gest=  $this->input->post('gestiones');
                $fecha=  $this->input->post('fecha');
                $comp=  $this->input->post('comp');
                $mod =  $this->input->post('modalidad');
                $resp=  $this->input->post('resp_id');
                $uni=  $this->input->post('uni_id');
                //=================enviar  evitar codigo malicioso ==========
                $id_f = $this->security->xss_clean($id_f);
                $gest = $this->security->xss_clean($gest);
                $fecha = $this->security->xss_clean($fecha);
                $comp = $this->security->xss_clean(trim($comp));
                $mod = $this->security->xss_clean(trim($mod));
                $resp = $this->security->xss_clean($resp);
                $uni = $this->security->xss_clean($uni);
                
                //======================= VALIDA COMPONENTES========================
                $data = array(
                        'pfec_id' => $id_f,
                        'com_componente' => strtoupper($comp),
                        'fun_id' => $this->session->userdata("fun_id"),
                        'com_modalidad' => $mod,
                        'resp_id' => $resp,
                        'uni_id' => $uni,
                    );
                    $this->db->insert('_componentes',$data);
                    $id_c=$this->db->insert_id();
                //    $this->model_audi->store_audi('_componentes',1,$id_c);
				/*
					no existe el MODELO model_audi ni el metodo store audi,
					Investigar cual era la funcionalidad
					Se ha encontrado el modelo model_auditoria que hace referencia a la tabla tbl_audi_proyecto en la base de datos
					(Hipotesis) Esta tabla almacena las operaciones que se efectuan sobre las tablas del sistema
					los parametros de store_audi son:
						NOMBRE_TABLA: haciendo referencia al nombre de la tabla donde se efectu� la operacion
						OPERACION: 1 en caso de nuevo registro, 2 en caso de actualizacion
						ID_C: identificador del componente, (hipotesis) el metodo insert_id() retorna el id del ultimo registro en la base de datos
				*/
                //==================================================================
                echo 'true';
            } else {
                echo'DATOS ERRONEOS';
            }
        }else{
            show_404();
        }
    }
 /*=================================================================================================================*/

  /*===================================== OBTIENE LOS DATOS DEL COMPONENTE =======================================*/
    public function get_componente()
    {
        if($this->input->is_ajax_request() && $this->input->post())
        {
            $post = $this->input->post();
            $id_c = $post['id_c'];
            $id_c = $this->security->xss_clean($id_c);
            $dato_comp = $this->model_componente->get_componente($id_c);
            //caso para modificar el codigo de proyecto y actividades
            foreach($dato_comp as $row){
                $result = array(
                    'com_id' => $row['com_id'],
                    "pfec_id" =>$row['pfec_id'],
                    "com_componente" =>$row['com_componente'],
                    "modalidad" =>$row['com_modalidad'],
                    "uni_id" =>$row['uni_id'],
                    "resp_id" =>$row['resp_id'],
                    "com_ponderacion" =>$row['com_ponderacion'],
                    "estado" =>$row['estado']
                );
            }
            echo json_encode($result);
        }else{
            show_404();
        }
    }
  /*============================================ UPDATE COMPONENTE =============================================*/
      function update_componente()
      {
        if($this->input->is_ajax_request() && $this->input->post()){
            $this->form_validation->set_rules('com_id', 'id componente', 'required|trim');
            $this->form_validation->set_rules('componente', 'Descripcion del componente', 'required|trim');
            $this->form_validation->set_rules('modalidad', 'Modalidad de ejecucion', 'required|trim');
            //=========================== mensajes =========================================
            $this->form_validation->set_message('required', 'El campo es es obligatorio');
            /*if ($this->form_validation->run() ) {*/
                $post = $this->input->post();
                $id_c = $post['com_id'];
                $ponderacion = $post['ponderacion'];
                $comp = $post['componente'];
                $mod = $post['modalidad'];
                $resp_id = $post['resp_id'];
                $uni_id = $post['uni_id'];
                //================ evitar enviar codigo malicioso ==========
                $id_c= $this->security->xss_clean($id_c);
                $ponderacion= $this->security->xss_clean($ponderacion);
                $comp= $this->security->xss_clean($comp);
                $mod= $this->security->xss_clean($mod);
                $resp_id= $this->security->xss_clean($resp_id);
                $uni_id= $this->security->xss_clean($uni_id);
                
                $update_comp = array(
                        'com_componente' => strtoupper($comp),
                        'com_ponderacion' => $ponderacion,
                        'com_modalidad' => $mod,
                        'resp_id' => $resp_id,
                        'uni_id' => $uni_id,
                        'estado' => '2',
                        'fun_id' => $this->session->userdata("fun_id")
                    );
                $this->db->where('com_id', $id_c);
                $this->db->update('_componentes', $update_comp);
//                $this->model_audi->store_audi('_componentes',2,$id_c);
				/*
					no existe el MODELO model_audi ni el metodo store audi,
					Investigar cual era la funcionalidad
				*/
                echo 'true';
        }else{
            show_404();
        }
    }

    /*=================================== ELIMINA COMPONENTE ==================================================*/
     public function delete_componente($mod,$pfec_id,$proy_id,$com_id){
        $fase = $this->model_faseetapa->get_id_fase($proy_id); //// DATOS DE LA FASE ACTIVA
        
        $productos = $this->model_producto->list_prod($com_id); //// productos
        foreach ($productos as $rowp) {
            $actividades = $this->model_actividad->list_act_anual($rowp['prod_id']); //// Productos
            foreach ($actividades as $rowa)
            {
               if($fase[0]['pfec_ejecucion']==1) //// directo
                {
                    $insumos = $this->model_actividad->imsumo_actividad($rowa['act_id']); //// Insumo actividad
                }
                elseif ($fase[0]['pfec_ejecucion']==2) 
                {
                    $insumos = $this->model_componente->imsumo_componente($com_id); //// Insumo Componente
                }
               foreach ($insumos as $rowi) {
                    $ins_gestion = $this->minsumos->list_insumos_gestion($rowi['ins_id']);
                    foreach ($ins_gestion as $row)
                    {
                       // echo $row['insg_id'].'--'.$row['ins_id'].'-'.$row['g_id'].'--'.$row['insg_monto_prog']."<br>";
                        $ins_fin = $this->minsumos->list_insumo_financiamiento($row['insg_id']);
                        if(count($ins_fin)!=0)
                        {
                            /*----------------- ELIMINA IFIN PROG MES---------------*/
                            $this->db->where('ifin_id', $ins_fin[0]['ifin_id']);
                            $this->db->delete('ifin_prog_mes');
                            /*------------------------------------------------------*/

                            /*----------------- ELIMINA IFIN EJEC MES---------------*/
                            $this->db->where('ifin_id', $ins_fin[0]['ifin_id']);
                            $this->db->delete('ifin_ejec_mes');
                            /*------------------------------------------------------*/

                            /*----------------- ELIMINA IFIN PROG MES---------------*/
                            $this->db->where('ifin_id', $ins_fin[0]['ifin_id']);
                            $this->db->delete('insumo_financiamiento');
                            /*------------------------------------------------------*/

                          //  echo "-----insumo fin : ".$ins_fin[0]['ifin_id'].'-'.$ins_fin[0]['insg_id'].'-'.$ins_fin[0]['ifin_monto'];
                        }

                        /*----------------- ELIMINA INS GESTION---------------*/
                            $this->db->where('insg_id', $row['insg_id']);
                            $this->db->delete('insumo_gestion');
                        /*------------------------------------------------------*/
                       // 
                    }

                        if($fase[0]['pfec_ejecucion']==1) //// directo
                        {
                            /*----------------- ELIMINA INSUMO ACTIVIDAD ---------------*/
                            $this->db->where('ins_id', $rowi['ins_id']);
                            $this->db->delete('_insumoactividad');
                            /*----------------------------------------------------------*/
                        }
                        elseif ($fase[0]['pfec_ejecucion']==2) /// Delegado
                        {
                           /*----------------- ELIMINA INSUMO COMPONENTE ---------------*/
                            $this->db->where('ins_id', $rowi['ins_id']);
                            $this->db->delete('insumocomponente');
                            /*-----------------------------------------------------------*/
                        }

                        /*----------------- ELIMINA INS GESTION---------------*/
                            $this->db->where('ins_id', $rowi['ins_id']);
                            $this->db->delete('insumos');
                        /*------------------------------------------------------*/

                }

                /*------------ ELIMINA ACTIVIDAD PROGRAMADO -----------*/
                $this->db->where('act_id', $rowa['act_id']);
                $this->db->delete('act_programado_mensual');

                $this->db->where('act_id', $rowa['act_id']);
                $this->db->delete('act_ejecutado_mensual');
                /*---------------------------------------------------*/
                /*----------------- ELIMINA ACTIVIDAD ---------------*/
                $this->db->where('act_id', $rowa['act_id']);
                $this->db->delete('_actividades');
                /*---------------------------------------------------*/
            }

            /*------------ ELIMINA PRODUCTO PROGRAMADO -----------*/
            $this->db->where('prod_id', $rowp['prod_id']);
            $this->db->delete('prod_programado_mensual');

            $this->db->where('prod_id', $rowp['prod_id']);
            $this->db->delete('prod_ejecutado_mensual');
            /*---------------------------------------------------*/
            /*----------------- ELIMINA PRODUCTO ---------------*/
            $this->db->where('prod_id', $rowp['prod_id']);
            $this->db->delete('_productos');
            /*---------------------------------------------------*/
        }

        if ($fase[0]['pfec_ejecucion']==2) 
        {
            $insumos = $this->model_componente->imsumo_componente($com_id); //// Insumo Componente
            echo count($insumos);
            if(count($insumos)!=0)
            {
                foreach ($insumos as $rowi) {
                    $ins_gestion = $this->minsumos->list_insumos_gestion($rowi['ins_id']);
                    foreach ($ins_gestion as $row)
                    {
                       // echo $row['insg_id'].'--'.$row['ins_id'].'-'.$row['g_id'].'--'.$row['insg_monto_prog']."<br>";
                        $ins_fin = $this->minsumos->list_insumo_financiamiento($row['insg_id']);
                        if(count($ins_fin)!=0)
                        {
                            /*----------------- ELIMINA IFIN PROG MES---------------*/
                            $this->db->where('ifin_id', $ins_fin[0]['ifin_id']);
                            $this->db->delete('ifin_prog_mes');
                            /*------------------------------------------------------*/
                            /*----------------- ELIMINA IFIN EJEC MES---------------*/
                            $this->db->where('ifin_id', $ins_fin[0]['ifin_id']);
                            $this->db->delete('ifin_ejec_mes');
                            /*------------------------------------------------------*/
                            /*----------------- ELIMINA IFIN PROG MES---------------*/
                            $this->db->where('ifin_id', $ins_fin[0]['ifin_id']);
                            $this->db->delete('insumo_financiamiento');
                            /*------------------------------------------------------*/
                          }

                        /*----------------- ELIMINA INS GESTION---------------*/
                            $this->db->where('insg_id', $row['insg_id']);
                            $this->db->delete('insumo_gestion');
                        /*------------------------------------------------------*/
                    }

                        /*----------------- ELIMINA INSUMO COMPONENTE ---------------*/
                            $this->db->where('ins_id', $rowi['ins_id']);
                            $this->db->delete('insumocomponente');
                        /*-----------------------------------------------------------*/

                        /*----------------- ELIMINA INS GESTION---------------*/
                            $this->db->where('ins_id', $rowi['ins_id']);
                            $this->db->delete('insumos');
                        /*------------------------------------------------------*/
                }
            }
        }

        /*------------ ELIMINA PRODUCTO PROGRAMADO -----------*/
            $this->db->where('com_id', $com_id);
            $this->db->delete('com_prog_mes');

            $this->db->where('com_id', $com_id);
            $this->db->delete('com_ejec_mes');
            /*---------------------------------------------------*/
            /*----------------- ELIMINA PRODUCTO ---------------*/
            $this->db->where('com_id', $com_id);
            $this->db->delete('_componentes');
            /*---------------------------------------------------*/

         /*----Actualiza la ponderación de actividades, productos y componentes-----*/
         $this->actualiza_ponderacion($pfec_id,$com_id,0);

          if(count($this->model_componente->get_componente($com_id))==0)
            {
                $this->session->set_flashdata('success','EL COMPONENTE/PROCESO SE ELIMINO CORRECTAMENTE');
                redirect('admin/prog/list_comp/'.$mod.'/'.$pfec_id.'/'.$proy_id.'/true');
            }
            else
            {
                $this->session->set_flashdata('danger','ERROR AL ELIMINAR, INTENTELO DE NUEVO');
                redirect('admin/prog/list_comp/'.$mod.'/'.$pfec_id.'/'.$proy_id.'/false');
            }
    }

/*    function delete_componente()
    {
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $post = $this->input->post();
            $proy_id = $post['proy_id'];
            $com_id = $post['com_id'];
            //$fase = $this->model_faseetapa->get_id_fase($proy_id);

            $peticion=$this->elimina_componentes($proy_id,$com_id);
            
            if($peticion){
                $result = array(
                    'respuesta' => 'correcto'
                );
            }
            else{
                $result = array(
                    'respuesta' => 'error'
                );
            }
            $result = array(
                    'respuesta' => $proy_id.'-'.$com_id
                );
            echo json_encode($result);

        } else {
            echo 'DATOS ERRONEOS';
        }

    }*/

    /*=========================================================================================================*/
    function rolfun($rol)
    {
      $valor=false;
      for ($i=1; $i <=count($rol) ; $i++) { 
        $data = $this->Users_model->get_datos_usuario_roles($this->session->userdata('fun_id'),$rol[$i]);
        if(count($data)!=0){
          $valor=true;
          break;
        }
      }
      return $valor;
    }

}