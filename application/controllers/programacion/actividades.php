<?php
class Actividades extends CI_Controller { 
    public $rol = array('1' => '3','2' => '4');
    public function __construct ()
    { 
        parent::__construct();
        if($this->session->userdata('fun_id')!=null){
            $this->load->model('Users_model','',true);
            if($this->rolfun($this->rol)){ 
            $this->load->library('pdf2');
            $this->load->model('registro_ejec/mejec_sigep'); ///// a borrar
            $this->load->model('programacion/model_proyecto');
            $this->load->model('programacion/model_faseetapa');
            $this->load->model('programacion/model_actividad');
            $this->load->model('programacion/model_producto');
            $this->load->model('programacion/model_componente');
            $this->load->model('programacion/insumos/minsumos');
            $this->load->model('programacion/insumos/minsumos_delegado');
            $this->load->model('mantenimiento/model_entidad_tras');
            $this->load->model('menu_modelo');
            $this->load->model('Users_model','',true);
            }else{
                redirect('admin/dashboard');
            }
        }
        else{
            redirect('/','refresh');
        }
    }

    public function actualiza_ponderacion($pfec_id,$com_id,$prod_id) /////// GENERA TABLA DE FUENTES , ORGANISMOS
    {
        /*================================ ACTUALIZANDO PONDERACIONES ACTIVIDADES ===============================*/
        $suma = $this->model_actividad->suma_monto_ponderado_total($prod_id);
        $act = $this->model_actividad->list_act_anual($prod_id);
        $ponderacion=0;
        foreach ($act as $row)
        {
            if($suma[0]['monto_total']!=0){$ponderacion=round((($row['act_pres_p']/$suma[0]['monto_total'])*100),2);}
            else{$ponderacion=0;}
            $update_act = array(
                'act_ponderacion' => $ponderacion
            );

            $this->db->where('act_id', $row['act_id']);
            $this->db->update('_actividades', $update_act);

        }
        /*===========================================================================================*/
        /*========================================= ACTUALIZANDO PONDERACIONES PRODUCTOS ========================*/
        $productos= $this->model_producto->list_prod($com_id);
        $sumatoria_total=0;
        foreach ($productos as $rowp)
        {
            $suma_pa = $this->model_actividad->suma_monto_ponderado_total($rowp['prod_id']);
            $sumatoria_total=$sumatoria_total+$suma_pa[0]['monto_total'];
        }

        $ponderacion=0;
        foreach ($productos as $rowp)
        {
            $suma_prod = $this->model_actividad->suma_monto_ponderado_total($rowp['prod_id']);
            $ponderacion=round((($suma_prod[0]['monto_total']/$sumatoria_total)*100),2);

            $update_prod = array(
                'prod_ponderacion' => $ponderacion
            );
            $this->db->where('prod_id', $rowp['prod_id']);
            $this->db->update('_productos', $update_prod);
        }
        /*============================================================================================================*/

        /*========================================= ACTUALIZANDO PONDERACIONES COMPONENTES =============================*/
        $componente= $this->model_componente->componentes_id($pfec_id);
        $sumatoria_comp=0;
        foreach ($componente as $rowc)
        {
            $productos= $this->model_producto->list_prod($rowc['com_id']);
            $sumatoria_p=0;
            foreach ($productos as $rowp)
            {
                $suma_pa = $this->model_actividad->suma_monto_ponderado_total($rowp['prod_id']);
                $sumatoria_p=$sumatoria_p+$suma_pa[0]['monto_total'];
            }
            $sumatoria_comp=$sumatoria_comp+$sumatoria_p;
        }

        $ponderacion=0;
        foreach ($componente as $rowc)
        {
            $productos= $this->model_producto->list_prod($rowc['com_id']);
            $sumatoria_p=0;
            foreach ($productos as $rowp)
            {
                $suma_pa = $this->model_actividad->suma_monto_ponderado_total($rowp['prod_id']);
                $sumatoria_p=$sumatoria_p+$suma_pa[0]['monto_total'];
            }
            $ponderacion=round((($sumatoria_p/$sumatoria_comp)*100),2);
            $update_comp = array(
                'com_ponderacion' => $ponderacion
            );
            $this->db->where('com_id', $rowc['com_id']);
            $this->db->update('_componentes', $update_comp);

        }
        /*============================================================================================================*/

    }
  /*=========================== LISTA DE ACTIVIDADES ANTERIOR ==========================================*/
    function archivo_actividad($mod,$id_f,$id_p,$id_c,$id_pr)
    {
        $enlaces=$this->menu_modelo->get_Modulos_programacion(1);
        $data['enlaces'] = $enlaces;
        $data['nro_fase']= $this->model_faseetapa->nro_fase($id_p); /// nro de fases y etapas registrados
        $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p); ////// DATOS DEL PROYECTO
        $data['id_f'] = $this->model_faseetapa->get_id_fase($id_p); //// DATOS DE LA FASE ACTIVA
        $data['componente'] = $this->model_componente->get_componente($id_c); ///// DATOS DEL COMPONENTE
        $data['producto'] = $this->model_producto->get_producto_id($id_pr); ///// DATOS DEL PRODUCTO
        $data['mod']=$mod;
        $data['id_c']=$id_c;
        $data['id_pr']=$id_pr;

        $data['gestion'] = $this->session->userdata("gestion");
        $data['reg_ejec_programa_js'] = '<SCRIPT src="' . base_url() . 'mis_js/registro_ejecucion/reg_ejec_sigep.js" type="text/javascript"></SCRIPT>';
        $data['lista_archivos'] = $this->mejec_sigep->lista_arc_segip();

        $this->load->view('admin/programacion/actividad/list_actividades_update', $data);
    }

    
    function subir_actividad()
    {
        if ($this->input->post()) {
            $post = $this->input->post();
            $mod = $post['mod'];
            $proy_id = $post['proy_id'];
            $pfec_id = $post['pfec_id'];
            $com_id = $post['com_id'];
            $prod_id = $post['prod_id'];
          
            $tipo = $_FILES['archivo']['type'];
            $tamanio = $_FILES['archivo']['size'];
            $archivotmp = $_FILES['archivo']['tmp_name'];

            $filename = $_FILES["archivo"]["name"];
            $file_basename = substr($filename, 0, strripos($filename, '.'));
            $file_ext = substr($filename, strripos($filename, '.'));
            $allowed_file_types = array('.csv');
            if (in_array($file_ext, $allowed_file_types) && ($tamanio < 90000000)) {
                
                
                $lineas=$this->actividad_archivo($proy_id,$pfec_id,$com_id,$prod_id,$archivotmp);
               

                $this->session->set_flashdata('success','SE IMPORTARON Y SE REGISTRARON '.$lineas.' ACTIVIDADES');
                redirect(site_url("admin") . '/prog/list_act/'.$mod.'/'.$pfec_id.'/'.$proy_id.'/'.$com_id.'/'.$prod_id.'/true');
            } 
            elseif (empty($file_basename)) {
                echo "<script>alert('SELECCIONE ARCHIVO .CSV')</script>";
            } 
            elseif ($filesize > 100000000) {
                //redirect('');
            } 
            else {
                $mensaje = "S�lo estos tipos de archivo se permiten para la carga: " . implode(', ', $allowed_file_types);
                echo '<script>alert("' . $mensaje . '")</script>';
            }

        } else {
            show_404();
        }
    }


    //---------------- subir archivo Actividad -----------------------
    function actividad_archivo($proy_id,$pfec_id,$com_id,$prod_id,$archivotmp)
    {

            $tipo = $_FILES['archivo']['type'];
            $tamanio = $_FILES['archivo']['size'];
            $archivotmp = $_FILES['archivo']['tmp_name'];

            $fase = $this->model_faseetapa->get_id_fase($proy_id); //// DATOS DE LA FASE ACTIVA
            $fase_gestion = $this->model_faseetapa->list_fases_gestiones($pfec_id); //// DATOS DE LA FASE GESTION
                
            $nro_act = count($this->model_actividad->list_act_anual($prod_id)); //// Nro de Actividades
            $act_nro= $nro_act;
            $conf=$this->model_proyecto->configuracion(); /// Confirmacion
             
            //cargamos el archivo
            $lineas = file($archivotmp);
            
            //inicializamos variable a 0, esto nos ayudar� a indicarle que no lea la primera l�nea
            $i=0;
            $nro=0;
            //Recorremos el bucle para leer l�nea por l�nea
            foreach ($lineas as $linea_num => $linea)
            { 
               if($i != 0) 
               { 
                   $datos = explode(";",$linea);
                   

                    $Fecha_inicio = $datos[10]; //// Fecha Inicio
                    $Fecha_final = $datos[11]; //// Fecha Final

                    $veriff1 = $this->verif_fecha($Fecha_inicio); //// verifica fecha de inicio
                    $veriff2 = $this->verif_fecha($Fecha_final); //// verifica fecha final
                    if($veriff1=='true' & $veriff2=='true' & count($datos)==26) //// verif fecha de inicio, final
                    {
                       $nro = $nro+1; //// Nro de Actividad
                       $act_nro++; ///// Nro de actividad
                       $actividad = utf8_encode($datos[1]); //// Actividad
                       $tp_id = (int)$datos[2]; //// tipo de Indicador
                       if(!is_numeric($tp_id)){
                        $tp_id=1;
                       }

                       $indicador = utf8_encode($datos[3]); //// Descripcion Indicador
                       $tp_den = (int)$datos[4]; //// tipo Denominador
                       if(!is_numeric($tp_den)){
                        $tp_den=0;
                       }
                       
                       $formula = utf8_encode($datos[5]); //// Descripcion formula

                       $linea_base = (float)$datos[6]; //// Linea base
                       if(!is_numeric($linea_base)){
                        $linea_base=0;
                        }

                       $meta = (float)$datos[7]; //// Meta
                       if(!is_numeric($meta)){
                        $meta=0;
                        }

                        $costo = (float)$datos[8]; //// Costo
                        if(!is_numeric($costo)){
                        $costo=0;
                        }

                        $costo_unitario=0;
                        if($meta!=0){
                        $costo_unitario=($costo/$meta);
                        }

                        $fuente = utf8_encode($datos[9]); ///// Fuente de Verificacion
                        $denominador = utf8_encode($datos[12]); ///// Total de casos Denominador
                        $numerador = utf8_encode($datos[13]); ///// Total de casos Numerador

                        /*========================= TABLA ACTIVIDADES ==========================*/  
                          $query=$this->db->query('set datestyle to DMY');
                          $data_to_store = array(
                            'prod_id' => $prod_id,
                            'act_actividad' => strtoupper($actividad),
                            'indi_id' => $tp_id,
                            'act_indicador' => strtoupper($indicador),
                            'act_formula' => strtoupper($formula),
                            'act_linea_base' => $linea_base,
                            'act_meta' => $meta,
                            'act_fuente_verificacion' => strtoupper($fuente),
                            'act_presupuesto' => $costo,
                            'act_fecha_inicio' => $Fecha_inicio,
                            'act_fecha_final' => $Fecha_final,
                            'act_denominador' => $tp_den,
                            'nro_act' => $act_nro,
                            'act_costo_uni' => $costo_unitario,
                            'act_total_casos' => strtoupper($denominador), 
                            'act_casos_favorables' => strtoupper($numerador),
                            'fun_id' => $this->session->userdata("fun_id"),
                        );
                            $this->db->insert('_actividades', $data_to_store); 
                            $act_id=$this->db->insert_id(); ////// id del Actividad
                        /*====================================================================*/  

                            $dias = $this->model_actividad->calcula_dias($act_id); ///// calculando al fecha

                            $update_act = array('act_duracion' => $dias[0]['dias'],
                                                'act_pres_p' => ($dias[0]['dias']*$costo));
                            $this->db->where('act_id', $act_id);
                            $this->db->update('_actividades', $update_act);

                            if(count($fase_gestion)==1)
                            {
                                $m[1]=(float)$datos[14]; //// Mes 1
                                $m[2]=(float)$datos[15]; //// Mes 2
                                $m[3]=(float)$datos[16]; //// Mes 3
                                $m[4]=(float)$datos[17]; //// Mes 4
                                $m[5]=(float)$datos[18]; //// Mes 5
                                $m[6]=(float)$datos[19]; //// Mes 6
                                $m[7]=(float)$datos[20]; //// Mes 7
                                $m[8]=(float)$datos[21]; //// Mes 8
                                $m[9]=(float)$datos[22]; //// Mes 9
                                $m[10]=(float)$datos[23]; //// Mes 10
                                $m[11]=(float)$datos[24]; //// Mes 11
                                $m[12]=(float)$datos[25]; //// Mes 12

                                for ($p=1; $p <=12 ; $p++) { 
                                    if($m[$p]!=0)
                                    {
                                        $data_to_store4 = array( 
                                        'act_id' => $act_id, /// Act id
                                        'm_id' => $p, /// Mes
                                        'pg_fis' => $m[$p], /// Programado mes
                                        'pg_fin' => $m[$p]*$costo_unitario, /// programado financiado
                                        'g_id' => $fase_gestion[0]['g_id'], /// Valor mes
                                        );
                                        $this->db->insert('act_programado_mensual', $data_to_store4); ///// Guardar en Tabla Insumo Financiamiento Programado Mes
                                    }
                                }
                            }

                            $nro_a=$conf[0]['conf_proceso']+1;

                            $update_conf = array('conf_proceso' => $nro_a);
                            $this->db->where('ide', $this->session->userdata("gestion"));
                            $this->db->update('configuracion', $update_conf);
                    }

               }

               $i++;
            }

            $this->actualiza_ponderacion($pfec_id,$com_id,$prod_id); //// actualiza ponderaciones

        return $nro;
    }

     /*------------  Funcion para verificar fechas ---------------------     */
    public function verif_fecha($fecha_act) /////// GENERA TABLA DE FUENTES , ORGANISMOS
    {
        $fecha = $fecha_act;
        $valores = explode('/', $fecha);

        if(count($valores)==3)
        {
            if(checkdate($valores[1],$valores[0],$valores[2])){
               return 'true';
            }
            else
            {
                return 'false';
            }
        }
        else
        {
            return 'false';
        }
              //  echo count($valores);
    }


/*============================================================================================================*/
  public function lista_actividades($mod,$id_f,$id_p,$id_c,$id_pr)
  {
    if($mod!='' & $id_f!='' & $id_p!='' & $id_c!='' & $id_pr!=''){    
        $enlaces=$this->menu_modelo->get_Modulos_programacion(1);
        $data['enlaces'] = $enlaces;
        $data['nro_fase']= $this->model_faseetapa->nro_fase($id_p); /// nro de fases y etapas registrados
        $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p); ////// DATOS DEL PROYECTO
        $data['id_f'] = $this->model_faseetapa->get_id_fase($id_p); //// DATOS DE LA FASE ACTIVA
        $data['componente'] = $this->model_componente->get_componente($id_c); ///// DATOS DEL COMPONENTE
        $data['producto'] = $this->model_producto->get_producto_id($id_pr); ///// DATOS DEL PRODUCTO
        $data['mod']=$mod;
        $data['id_c']=$id_c;
        $data['id_pr']=$id_pr;

        $titulo_proy=strtoupper($data['proyecto'][0]['tipo']);
        $data['titulo_proy'] = $titulo_proy;

        if($mod==1) ///// ACTIVIDADES ANUAL
        {
        $data['act'] = $this->model_actividad->list_act_anual($id_pr); /// actividades de la gestion actual
        $this->load->view('admin/programacion/actividad/list_actividades', $data); 
        }
        elseif ($mod==4) //// ACTIVIDADES MULTIANUAL
        {
        $data['act'] = $this->model_actividad->list_act_anual($id_pr);
        $this->load->view('admin/programacion/actividad/list_actividades_m', $data); 
        }
    }
    else{
        redirect('admin/dashboard');
    }
  }
/*============================================================================================================*/
  /*---------------------------------------NUEVA ACTIVIDAD---------------------------------------*/
  public function new_actividad($mod,$id_f,$id_p,$id_c,$id_pr)
  {
    if($mod!='' & $id_f!='' & $id_p!='' & $id_c!='' & $id_pr!=''){    
        $enlaces=$this->menu_modelo->get_Modulos_programacion(1);
        $data['enlaces'] = $enlaces;
        $data['nro_fase']= $this->model_faseetapa->nro_fase($id_p); /// nro de fases y etapas registrados
        $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p); ////// DATOS DEL PROYECTO
        $data['id_f'] = $this->model_faseetapa->get_id_fase($id_p); //// DATOS DE LA FASE ACTIVA
        $data['componente'] = $this->model_componente->get_componente($id_c); ///// DATOS DEL COMPONENTE
        $data['producto'] = $this->model_producto->get_producto_id($id_pr); ///// DATOS DEL PRODUCTO
        $data['mod']=$mod;
        $data['id_c']=$id_c;
        $data['act_reg'] = $this->model_actividad->nro_actividades_gest($id_pr); /// nro de actividades registrados del producto x  
        $data['indi']= $this->model_proyecto->indicador(); /// indicador
        
        $titulo_proy=strtoupper($data['proyecto'][0]['tipo']);
        $data['titulo_proy'] = $titulo_proy;
        //load the view
        $this->load->view('admin/programacion/actividad/form_act', $data);
    }
    else{
        redirect('admin/dashboard');
    }  
  }
 
/*---------------------------------------VALIDA ACTIVIDAD---------------------------------------*/
  public function valida_actividad()
  {
        if ($this->input->server('REQUEST_METHOD') === 'POST') ///// DATOS GENERALES
        {
            $this->form_validation->set_rules('act', 'Actividad', 'required|trim');
            $this->form_validation->set_rules('tipo_i', 'Tipo de Indicador', 'required|trim');
            $this->form_validation->set_rules('indicador', 'Indicador', 'required|trim');
            $this->form_validation->set_rules('met', 'Meta', 'required|trim');
            $this->form_validation->set_rules('f_ini', 'Fecha Inicio', 'required|trim');
            $this->form_validation->set_rules('f_final', 'Fecha Final', 'required|trim');

            if ($this->form_validation->run())
            {
                $verif1 = $this->verif_fecha($this->input->post('f_ini')); //// verifica Fecha 1
                $verif2 = $this->verif_fecha($this->input->post('f_final')); //// verifica Fecha 2
               
               if($verif1=='true' & $verif2=='true')
               {
                 /*---------------------------- FECHAS VALIDAS --------------------------------*/
                 /*========================= TABLA ACTIVIDADES ==========================*/  
                  $query=$this->db->query('set datestyle to DMY');
                  $data_to_store = array(
                    'prod_id' => $this->input->post('id_pr'),
                    'act_actividad' => strtoupper($this->input->post('act')),
                    'indi_id' => $this->input->post('tipo_i'),
                    'act_indicador' => strtoupper($this->input->post('indicador')),
                    'act_formula' => strtoupper($this->input->post('formula')),
                    'act_linea_base' => $this->input->post('lb'),
                    'act_meta' => $this->input->post('met'),
                    'act_fuente_verificacion' => strtoupper($this->input->post('verificacion')),
                    'act_supuestos' => strtoupper($this->input->post('supuestos')),
                    'act_presupuesto' => $this->input->post('costo'),
                    'act_fecha_inicio' => $this->input->post('f_ini'),
                    'act_fecha_final' => $this->input->post('f_final'),
                    'act_denominador' => $this->input->post('den'),
                    'nro_act' => $this->input->post('nro_act'),
                    'act_costo_uni' => $this->input->post('cost_uni'),
                    'act_total_casos' => strtoupper($this->input->post('c_a')), 
                    'act_casos_favorables' => strtoupper($this->input->post('c_b')),
                    'act_casos_desfavorables' => strtoupper($this->input->post('c_c')),
                    'fun_id' => $this->session->userdata("fun_id"),
                );
                    $this->db->insert('_actividades', $data_to_store); 
                    $id_act=$this->db->insert_id(); ////// id del Actividad
                /*========================= END TABLA ACTIVIDADES ==========================*/  

                    $nro_a = $this->model_actividad->nro_actividades_gest($this->input->post('id_pr')); /// nro de actividades
                
                /*========================= DEPENDENCIAS Y DIAS DE LA ACTIVIDAD==========================*/  
                    if($nro_a=='1'){$dep='0';}
                    if($nro_a>'1'){$dep=$this->input->post('act_dep');}
                    
                    $dias = $this->model_actividad->calcula_dias($id_act); ///// calculando al fecha

                    $update_act = array('act_duracion' => $dias[0]['dias'],
                                        'act_precedente' => $dep,
                                        'act_pres_p' => ($dias[0]['dias']*$this->input->post('costo')));
                    $this->db->where('act_id', $id_act);
                    $this->db->update('_actividades', $update_act);
                /*========================= END DEPENDENCIAS Y DIAS DE LA ACTIVIDAD ==========================*/ 

                $conf=$this->model_proyecto->configuracion(); //// configuracion gestion
                $nro_a=$conf[0]['conf_proceso']+1;

                $update_conf = array('conf_proceso' => $nro_a);
                        $this->db->where('ide', $this->session->userdata("gestion"));
                        $this->db->update('configuracion', $update_conf);

                /*============================= UPDATE PGESTION RODUCTOS ===============================*/
                        $gestion=$this->input->post('gest');

                    if ( !empty($_POST["m1"]) && is_array($_POST["m1"]) ) 
                        {
                            foreach ( array_keys($_POST["m1"]) as $como  )
                            {
                                if($_POST["m1"][$como]!=0)
                                {
                                    $this->model_actividad->add_act_gest($id_act,$gestion,1,$_POST["m1"][$como],($_POST["m1"][$como]*$this->input->post('cost_uni')));
                                }
                                if($_POST["m2"][$como]!=0)
                                {
                                    $this->model_actividad->add_act_gest($id_act,$gestion,2,$_POST["m2"][$como],($_POST["m2"][$como]*$this->input->post('cost_uni')));
                                }
                                if($_POST["m3"][$como]!=0)
                                {
                                    $this->model_actividad->add_act_gest($id_act,$gestion,3,$_POST["m3"][$como],($_POST["m3"][$como]*$this->input->post('cost_uni')));
                                }
                                if($_POST["m4"][$como]!=0)
                                {
                                    $this->model_actividad->add_act_gest($id_act,$gestion,4,$_POST["m4"][$como],($_POST["m4"][$como]*$this->input->post('cost_uni')));
                                }
                                if($_POST["m5"][$como]!=0)
                                {
                                    $this->model_actividad->add_act_gest($id_act,$gestion,5,$_POST["m5"][$como],($_POST["m5"][$como]*$this->input->post('cost_uni')));
                                }
                                if($_POST["m6"][$como]!=0)
                                {
                                    $this->model_actividad->add_act_gest($id_act,$gestion,6,$_POST["m6"][$como],($_POST["m6"][$como]*$this->input->post('cost_uni')));
                                }
                                if($_POST["m7"][$como]!=0)
                                {
                                    $this->model_actividad->add_act_gest($id_act,$gestion,7,$_POST["m7"][$como],($_POST["m7"][$como]*$this->input->post('cost_uni')));
                                }
                                if($_POST["m8"][$como]!=0)
                                {
                                    $this->model_actividad->add_act_gest($id_act,$gestion,8,$_POST["m8"][$como],($_POST["m8"][$como]*$this->input->post('cost_uni')));
                                }
                                if($_POST["m9"][$como]!=0)
                                {
                                    $this->model_actividad->add_act_gest($id_act,$gestion,9,$_POST["m9"][$como],($_POST["m9"][$como]*$this->input->post('cost_uni')));
                                }
                                if($_POST["m10"][$como]!=0)
                                {
                                    $this->model_actividad->add_act_gest($id_act,$gestion,10,$_POST["m10"][$como],($_POST["m10"][$como]*$this->input->post('cost_uni')));
                                }
                                if($_POST["m11"][$como]!=0)
                                {
                                    $this->model_actividad->add_act_gest($id_act,$gestion,11,$_POST["m11"][$como],($_POST["m11"][$como]*$this->input->post('cost_uni')));
                                }
                                if($_POST["m12"][$como]!=0)
                                {
                                    $this->model_actividad->add_act_gest($id_act,$gestion,12,$_POST["m12"][$como],($_POST["m12"][$como]*$this->input->post('cost_uni')));
                                }
                                
                                $gestion++;
                            }
                        }

                        $this->actualiza_ponderacion($this->input->post('id_f'),$this->input->post('id_c'),$this->input->post('id_pr')); //// actualiza ponderaciones
                       
                  redirect('admin/prog/list_act/'.$this->input->post('mod').'/'.$this->input->post('id_f')."/".$this->input->post('id_p').'/'.$this->input->post('id_c').'/'.$this->input->post('id_pr').'/true');
                 /*----------------------------------------------------------------------------*/
               }
               else{
                redirect('admin/prog/new_act/'.$this->input->post('mod').'/'.$this->input->post('id_f')."/".$this->input->post('id_p').'/'.$this->input->post('id_c').'/'.$this->input->post('id_pr').'/false');
               }
                        
            }
            else
            {
                  //echo "Error, Contactese con el super administrador";admin/act/new/47/40/29/68
                  redirect('admin/prog/new_act/'.$this->input->post('mod').'/'.$this->input->post('id_f')."/".$this->input->post('id_p').'/'.$this->input->post('id_c').'/'.$this->input->post('id_pr').'/false');
            }
        }
   }

 /*---------------------------------- MODIFICADO ACTIVIDAD -------------------------------------*/
   public function update($mod,$id_f,$id_p,$id_c,$id_pr,$id_ac)
  {
    if($mod!='' & $id_f!='' & $id_p!='' & $id_c!='' & $id_pr!='' & $id_ac!=''){        
        $enlaces=$this->menu_modelo->get_Modulos_programacion(1);
        $data['enlaces'] = $enlaces;
        $data['nro_fase']= $this->model_faseetapa->nro_fase($id_p); /// nro de fases y etapas registrados
        $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p); ////// DATOS DEL PROYECTO
        $data['id_f'] = $this->model_faseetapa->get_id_fase($id_p); //// DATOS DE LA FASE ACTIVA
        $data['componente'] = $this->model_componente->get_componente($id_c); ///// DATOS DEL COMPONENTE
        $data['producto'] = $this->model_producto->get_producto_id($id_pr); ///// DATOS DEL PRODUCTO
        $data['mod']=$mod;
        $data['id_c']=$id_c;
        $data['act_reg'] = $this->model_actividad->nro_actividades_gest($id_pr); /// nro de actividades registrados del producto x  
        $data['indi']= $this->model_proyecto->indicador(); /// indicador
        $data['actividad'] = $this->model_actividad->get_actividad_id($id_ac); //// DATOSA ACTIVIDAD
        $data['programado'] = $this->model_actividad->suma_programado($id_ac); //// SUMA PROGRAMADO

        $titulo_proy=strtoupper($data['proyecto'][0]['tipo']);
        $data['titulo_proy'] = $titulo_proy;

        $this->load->view('admin/programacion/actividad/edit_act', $data); 
    }
    else{
      redirect('admin/dashboard');
    } 
  }

 /*---------------------------------- VALIDAR MODIFICADO ACTIVIDAD -------------------------------------*/
    public function modificar_actividad()
  {
        if ($this->input->server('REQUEST_METHOD') === 'POST') ///// DATOS GENERALES
        {
            $this->form_validation->set_rules('act', 'Actividad', 'required|trim');
            $this->form_validation->set_rules('indicador', 'Indicador', 'required|trim');
            $this->form_validation->set_rules('lb', 'linea Base', 'required|trim');
            $this->form_validation->set_rules('met', 'Meta', 'required|trim');

            if ($this->form_validation->run())
            {
                /*============================= UPDATE ACTIVIDADES ===============================*/
                $verif1 = $this->verif_fecha($this->input->post('f_ini')); //// verifica Fecha 1
                $verif2 = $this->verif_fecha($this->input->post('f_final')); //// verifica Fecha 2
               
                if($verif1=='true' & $verif2=='true'){

                    $query=$this->db->query('set datestyle to DMY');
                    $update_act = array(
                        'act_actividad' => strtoupper($this->input->post('act')),
                        'indi_id' => $this->input->post('tipo_i'),
                        'act_indicador' => strtoupper($this->input->post('indicador')),
                        'act_formula' => strtoupper($this->input->post('formula')),
                        'act_linea_base' => $this->input->post('lb'),
                        'act_meta' => $this->input->post('met'),
                        'act_fuente_verificacion' => $this->input->post('verificacion'),
                        'act_supuestos' => $this->input->post('supuestos'),
                        //'act_ponderacion' => $this->input->post('pn_cion'),
                        'act_presupuesto' => $this->input->post('costo'),
                        'act_fecha_inicio' => $this->input->post('f_ini'),
                        'act_fecha_final' => $this->input->post('f_final'),
                        'act_denominador' => $this->input->post('den'),
                        'act_precedente' => $dep=$this->input->post('act_dep'),
                        'act_costo_uni' => $this->input->post('cost_uni'),
                        'act_total_casos' => strtoupper($this->input->post('c_a')), 
                        'act_casos_favorables' => strtoupper($this->input->post('c_b')),
                        'act_casos_desfavorables' => strtoupper($this->input->post('c_c')),
                        'estado' => '2',
                        'fun_id' => $this->session->userdata("fun_id")
                    );

                        $this->db->where('act_id', $this->input->post('id_ac'));
                        $this->db->update('_actividades', $update_act);
                
                    $dias = $this->model_actividad->calcula_dias($this->input->post('id_ac')); ///// calculando al fecha
                /*============================= END ACTIVIDADES ===============================*/    

                    $update_act = array('act_duracion' => $dias[0]['dias'],
                                        'act_pres_p' => ($dias[0]['dias']*$this->input->post('costo')));
                    $this->db->where('act_id', $this->input->post('id_ac'));
                    $this->db->update('_actividades', $update_act);

                    $gestion=$this->input->post('gest');
                    $this->model_actividad->delete_act_gest($this->input->post('id_ac'));

                 /*============================= UPDATE PGESTION ACTIVIDADES ===============================*/
                    if ( !empty($_POST["m1"]) && is_array($_POST["m1"]) ) 
                        {
                            foreach ( array_keys($_POST["m1"]) as $como  )
                            {
                                if($_POST["m1"][$como]!=0)
                                {
                                    $this->model_actividad->add_act_gest($this->input->post('id_ac'),$gestion,1,$_POST["m1"][$como],($_POST["m1"][$como]*$this->input->post('cost_uni')));
                                }
                                if($_POST["m2"][$como]!=0)
                                {
                                    $this->model_actividad->add_act_gest($this->input->post('id_ac'),$gestion,2,$_POST["m2"][$como],($_POST["m2"][$como]*$this->input->post('cost_uni')));
                                }
                                if($_POST["m3"][$como]!=0)
                                {
                                    $this->model_actividad->add_act_gest($this->input->post('id_ac'),$gestion,3,$_POST["m3"][$como],($_POST["m3"][$como]*$this->input->post('cost_uni')));
                                }
                                if($_POST["m4"][$como]!=0)
                                {
                                    $this->model_actividad->add_act_gest($this->input->post('id_ac'),$gestion,4,$_POST["m4"][$como],($_POST["m4"][$como]*$this->input->post('cost_uni')));
                                }
                                if($_POST["m5"][$como]!=0)
                                {
                                    $this->model_actividad->add_act_gest($this->input->post('id_ac'),$gestion,5,$_POST["m5"][$como],($_POST["m5"][$como]*$this->input->post('cost_uni')));
                                }
                                if($_POST["m6"][$como]!=0)
                                {
                                    $this->model_actividad->add_act_gest($this->input->post('id_ac'),$gestion,6,$_POST["m6"][$como],($_POST["m6"][$como]*$this->input->post('cost_uni')));
                                }
                                if($_POST["m7"][$como]!=0)
                                {
                                    $this->model_actividad->add_act_gest($this->input->post('id_ac'),$gestion,7,$_POST["m7"][$como],($_POST["m7"][$como]*$this->input->post('cost_uni')));
                                }
                                if($_POST["m8"][$como]!=0)
                                {
                                    $this->model_actividad->add_act_gest($this->input->post('id_ac'),$gestion,8,$_POST["m8"][$como],($_POST["m8"][$como]*$this->input->post('cost_uni')));
                                }
                                if($_POST["m9"][$como]!=0)
                                {
                                    $this->model_actividad->add_act_gest($this->input->post('id_ac'),$gestion,9,$_POST["m9"][$como],($_POST["m9"][$como]*$this->input->post('cost_uni')));
                                }
                                if($_POST["m10"][$como]!=0)
                                {
                                    $this->model_actividad->add_act_gest($this->input->post('id_ac'),$gestion,10,$_POST["m10"][$como],($_POST["m10"][$como]*$this->input->post('cost_uni')));
                                }
                                if($_POST["m11"][$como]!=0)
                                {
                                    $this->model_actividad->add_act_gest($this->input->post('id_ac'),$gestion,11,$_POST["m11"][$como],($_POST["m11"][$como]*$this->input->post('cost_uni')));
                                }
                                if($_POST["m12"][$como]!=0)
                                {
                                    $this->model_actividad->add_act_gest($this->input->post('id_ac'),$gestion,12,$_POST["m12"][$como],($_POST["m12"][$como]*$this->input->post('cost_uni')));
                                }
                                
                                $gestion++;
                            }
                        }
                    /*============================= END UPDATE PGESTION RODUCTOS ===============================*/
                    $this->actualiza_ponderacion($this->input->post('id_f'),$this->input->post('id_c'),$this->input->post('id_pr')); //// actualiza ponderaciones
                    
                  redirect('admin/prog/list_act/'.$this->input->post('mod').'/'.$this->input->post('id_f')."/".$this->input->post('id_p').'/'.$this->input->post('id_c').'/'.$this->input->post('id_pr').'/true');
                }
               else{
                redirect('admin/prog/mod_act/'.$this->input->post('mod').'/'.$this->input->post('id_f')."/".$this->input->post('id_p').'/'.$this->input->post('id_c').'/'.$this->input->post('id_pr').'/'.$this->input->post('id_ac').'/false');
               }
            }
            else
            {
                  
                redirect('admin/prog/mod_act/'.$this->input->post('mod').'/'.$this->input->post('id_f')."/".$this->input->post('id_p').'/'.$this->input->post('id_c').'/'.$this->input->post('id_pr').'/'.$this->input->post('id_ac').'/false');
            }
        }
   }



    public function lista_act_efisica($mod,$id_f,$id_p,$id_c,$id_pr)
    {
        if($mod!='' & $id_f!='' & $id_p!='' & $id_c!='' & $id_pr!=''){     
            $enlaces=$this->menu_modelo->get_Modulos_programacion(1);
            $data['enlaces'] = $enlaces;
            $data['nro_fase']= $this->model_faseetapa->nro_fase($id_p); /// nro de fases y etapas registrados
            $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p); 
            $data['producto'] = $this->model_producto->get_producto_id($id_pr); 
            $data['id_f'] = $this->model_faseetapa->get_id_fase($id_p); //// recupera datos de la tabla fase activa
            $data['mod']=$mod;
            $data['id_c']=$id_c;
            $data['id_pr']=$id_pr;
            
            $titulo_proy=strtoupper($data['proyecto'][0]['tipo']);
            $data['titulo_proy'] = $titulo_proy;

            if($mod==1){
                $this->load->view('admin/programacion/ejecucion/fisica/actividades/list_actividades', $data);     
            }
            elseif($mod==4){
                $this->load->view('admin/programacion/ejecucion/fisica/actividades/list_actividades_m', $data);    
            }
        }
        else{
          redirect('admin/dashboard');
        }
    }

   /*---------------------------------- ACTUALIZAR ACTIVIDAD POR GESTION-------------------------------------*/

    public function ejecucion_actividad($mod,$id_f,$id_p,$id_c,$id_pr,$id_ac,$gest)
    {
        if($mod!='' & $id_f!='' & $id_p!='' & $id_c!='' & $id_pr!='' & $id_ac!='' & $gest!=''){     
            $enlaces=$this->menu_modelo->get_Modulos_programacion(1);
            $data['enlaces'] = $enlaces;
            $data['nro_fase']= $this->model_faseetapa->nro_fase($id_p); /// nro de fases y etapas registrados
            $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p); 
            $data['id_f'] = $this->model_faseetapa->get_id_fase($id_p); //// recupera datos de la tabla fase activa
            $data['actividad'] = $this->model_actividad->get_actividad_id($id_ac);
            $data['mod']=$mod;
            $data['id_c']=$id_c;
            $data['id_pr']=$id_pr;
            $data['gestion']=$gest;
            $data['programado']=$this->model_actividad->act_prog_mensual($id_ac,$gest);
            $data['ejecutado']=$this->model_actividad->act_ejec_mensual($id_ac,$gest);
            $data['nro_ejec']=$this->model_actividad->nro_act_ejec_mensual($id_ac,$gest);

            $titulo_proy=strtoupper($data['proyecto'][0]['tipo']);
            $data['titulo_proy'] = $titulo_proy;
            
            $this->load->view('admin/programacion/ejecucion/fisica/actividades/ejec_act', $data);
        }
        else{
        redirect('admin/dashboard');
        }  
    }

  /*---------------------------------- VALIDA PRODUCTOS POR GESTION-------------------------------------*/
  public function valida_ejecucion_actividad()
  {
       //echo $this->input->post('c_a');
        if ($this->input->server('REQUEST_METHOD') === 'POST') ///// DATOS GENERALES
        {
            $this->form_validation->set_rules('id_f', 'Id Fase', 'required|trim');
            $this->form_validation->set_rules('id_p', 'Id proyecto', 'required|trim');
            $this->form_validation->set_rules('id_c', 'Id componente', 'required|trim');
            $this->form_validation->set_rules('id_pr', 'Id producto', 'required|trim');
            $this->form_validation->set_rules('id_act', 'Id actividad', 'required|trim');

            if ($this->form_validation->run())
            {       /*============================= UPDATE PRODUCTOS ===============================*/
                $this->model_actividad->delete_act_ejec_gest($this->input->post('id_act'),$this->input->post('gest'));
                $actividad = $this->model_actividad->get_actividad_id($this->input->post('id_act')); 
                $id_act=$this->input->post('id_act');
                $gestion=$this->input->post('gest');      
                $fase = $this->model_faseetapa->fase_etapa($this->input->post('id_f'),$this->input->post('id_p'));
                $proyecto = $this->model_proyecto->get_id_proyecto($this->input->post('id_p'));

                if($proyecto[0]['tp_id']==1 && $fase[0]['pfec_ejecucion']==2) //// PROYECTOS INVERSION y delegada
                {
                    if($actividad[0]['indi_id']==1)
                    {
                        if($this->input->post('e1')!=0)
                        {   $efin1=$this->input->post('e1')*$this->input->post('costo');
                            $this->model_actividad->add_act_ejec_gest($id_act,$gestion,1,$this->input->post('e1'),0,0,$efin1);
                        }
                        if($this->input->post('e2')!=0)
                        {
                            $efin2=$this->input->post('e2')*$this->input->post('costo');
                            $this->model_actividad->add_act_ejec_gest($id_act,$gestion,2,$this->input->post('e2'),0,0,$efin2);
                        }
                        if($this->input->post('e3')!=0)
                        {
                            $efin3=$this->input->post('e3')*$this->input->post('costo');
                            $this->model_actividad->add_act_ejec_gest($id_act,$gestion,3,$this->input->post('e3'),0,0,$efin3);
                        }
                        if($this->input->post('e4')!=0)
                        {
                            $efin4=$this->input->post('e4')*$this->input->post('costo');
                            $this->model_actividad->add_act_ejec_gest($id_act,$gestion,4,$this->input->post('e4'),0,0,$efin4);
                        }
                        if($this->input->post('e5')!=0)
                        {
                            $efin5=$this->input->post('e5')*$this->input->post('costo');
                            $this->model_actividad->add_act_ejec_gest($id_act,$gestion,5,$this->input->post('e5'),0,0,$efin5);
                        }
                        if($this->input->post('e6')!=0)
                        {
                            $efin6=$this->input->post('e6')*$this->input->post('costo');
                            $this->model_actividad->add_act_ejec_gest($id_act,$gestion,6,$this->input->post('e6'),0,0,$efin6);
                        }
                        if($this->input->post('e7')!=0)
                        {   $efin7=$this->input->post('e7')*$this->input->post('costo');
                            $this->model_actividad->add_act_ejec_gest($id_act,$gestion,7,$this->input->post('e7'),0,0,$efin7);
                        }
                        if($this->input->post('e8')!=0)
                        {
                            $efin8=$this->input->post('e8')*$this->input->post('costo');
                            $this->model_actividad->add_act_ejec_gest($id_act,$gestion,8,$this->input->post('e8'),0,0,$efin8);
                        }
                        if($this->input->post('e9')!=0)
                        {
                            $efin9=$this->input->post('e9')*$this->input->post('costo');
                            $this->model_actividad->add_act_ejec_gest($id_act,$gestion,9,$this->input->post('e9'),0,0,$efin9);
                        }
                        if($this->input->post('e10')!=0)
                        {
                            $efin10=$this->input->post('e10')*$this->input->post('costo');
                            $this->model_actividad->add_act_ejec_gest($id_act,$gestion,10,$this->input->post('e10'),0,0,$efin10);
                        }
                        if($this->input->post('e11')!=0)
                        {
                            $efin11=$this->input->post('e11')*$this->input->post('costo');
                            $this->model_actividad->add_act_ejec_gest($id_act,$gestion,11,$this->input->post('e11'),0,0,$efin11);
                        }
                        if($this->input->post('e12')!=0)
                        {
                            $efin12=$this->input->post('e12')*$this->input->post('costo');
                            $this->model_actividad->add_act_ejec_gest($id_act,$gestion,12,$this->input->post('e12'),0,0,$efin12);
                        }
                    }
                    elseif ($actividad[0]['indi_id']==2) 
                    {
                        if($actividad[0]['act_denominador']==0) /// variable   (b/a)*p
                        {
                            $e1=(($this->input->post('b1')/$this->input->post('a1'))*$this->input->post('p1'));
                            $e2=(($this->input->post('b2')/$this->input->post('a2'))*$this->input->post('p2'));
                            $e3=(($this->input->post('b3')/$this->input->post('a3'))*$this->input->post('p3'));
                            $e4=(($this->input->post('b4')/$this->input->post('a4'))*$this->input->post('p4'));
                            $e5=(($this->input->post('b5')/$this->input->post('a5'))*$this->input->post('p5'));
                            $e6=(($this->input->post('b6')/$this->input->post('a6'))*$this->input->post('p6'));
                            $e7=(($this->input->post('b7')/$this->input->post('a7'))*$this->input->post('p7'));
                            $e8=(($this->input->post('b8')/$this->input->post('a8'))*$this->input->post('p8'));
                            $e9=(($this->input->post('b9')/$this->input->post('a9'))*$this->input->post('p9'));
                            $e10=(($this->input->post('b10')/$this->input->post('a10'))*$this->input->post('p10'));
                            $e11=(($this->input->post('b11')/$this->input->post('a11'))*$this->input->post('p11'));
                            $e12=(($this->input->post('b12')/$this->input->post('a12'))*$this->input->post('p12'));
                        }
                        elseif ($actividad[0]['act_denominador']==1) //// Fijo  (b/a)*100
                        {
                            $e1=(($this->input->post('b1')/$this->input->post('a1'))*100);
                            $e2=(($this->input->post('b2')/$this->input->post('a2'))*100);
                            $e3=(($this->input->post('b3')/$this->input->post('a3'))*100);
                            $e4=(($this->input->post('b4')/$this->input->post('a4'))*100);
                            $e5=(($this->input->post('b5')/$this->input->post('a5'))*100);
                            $e6=(($this->input->post('b6')/$this->input->post('a6'))*100);
                            $e7=(($this->input->post('b7')/$this->input->post('a7'))*100);
                            $e8=(($this->input->post('b8')/$this->input->post('a8'))*100);
                            $e9=(($this->input->post('b9')/$this->input->post('a9'))*100);
                            $e10=(($this->input->post('b10')/$this->input->post('a10'))*100);
                            $e11=(($this->input->post('b11')/$this->input->post('a11'))*100);
                            $e12=(($this->input->post('b12')/$this->input->post('a12'))*100);
                        }
                        /*-------------------------------- EJECUCION RELATIVO ---------------------------*/
                        if($this->input->post('a1')!=0)
                        {
                            $efin1=$e1*$this->input->post('costo');
                            $this->model_actividad->add_act_ejec_gest($id_act,$gestion,1,$e1,$this->input->post('a1'),$this->input->post('b1'),$efin1);
                        }
                        if($this->input->post('a2')!=0)
                        {
                            $efin2=$e2*$this->input->post('costo');
                            $this->model_actividad->add_act_ejec_gest($id_act,$gestion,2,$e2,$this->input->post('a2'),$this->input->post('b2'),$efin2);
                        }
                        if($this->input->post('a3')!=0)
                        {
                            $efin3=$e3*$this->input->post('costo');
                            $this->model_actividad->add_act_ejec_gest($id_act,$gestion,3,$e3,$this->input->post('a3'),$this->input->post('b3'),$efin3);
                        }
                        if($this->input->post('a4')!=0)
                        {
                            $efin4=$e4*$this->input->post('costo');
                            $this->model_actividad->add_act_ejec_gest($id_act,$gestion,4,$e4,$this->input->post('a4'),$this->input->post('b4'),$efin4);
                        }
                        if($this->input->post('a5')!=0)
                        {
                            $efin5=$e5*$this->input->post('costo');
                            $this->model_actividad->add_act_ejec_gest($id_act,$gestion,5,$e5,$this->input->post('a5'),$this->input->post('b5'),$efin5);
                        }
                        if($this->input->post('a6')!=0)
                        {
                            $efin6=$e6*$this->input->post('costo');
                            $this->model_actividad->add_act_ejec_gest($id_act,$gestion,6,$e6,$this->input->post('a6'),$this->input->post('b6'),$efin6);
                        }
                        if($this->input->post('a7')!=0)
                        {
                            $efin7=$e7*$this->input->post('costo');
                            $this->model_actividad->add_act_ejec_gest($id_act,$gestion,7,$e7,$this->input->post('a7'),$this->input->post('b7'),$efin7);
                        }
                        if($this->input->post('a8')!=0)
                        {
                            $efin8=$e8*$this->input->post('costo');
                            $this->model_actividad->add_act_ejec_gest($id_act,$gestion,8,$e8,$this->input->post('a8'),$this->input->post('b8'),$efin8);
                        }
                        if($this->input->post('a9')!=0)
                        {
                            $efin9=$e9*$this->input->post('costo');
                            $this->model_actividad->add_act_ejec_gest($id_act,$gestion,9,$e9,$this->input->post('a9'),$this->input->post('b9'),$efin9);
                        }
                        if($this->input->post('a10')!=0)
                        {
                            $efin10=$e10*$this->input->post('costo');
                            $this->model_actividad->add_act_ejec_gest($id_act,$gestion,10,$e10,$this->input->post('a10'),$this->input->post('b10'),$efin10);
                        }
                        if($this->input->post('a11')!=0)
                        {
                            $efin11=$e11*$this->input->post('costo');
                            $this->model_actividad->add_act_ejec_gest($id_act,$gestion,11,$e11,$this->input->post('a11'),$this->input->post('b11'),$efin11);
                        }
                        if($this->input->post('a12')!=0)
                        {
                            $efin12=$e12*$this->input->post('costo');
                            $this->model_actividad->add_act_ejec_gest($id_act,$gestion,12,$e12,$this->input->post('a12'),$this->input->post('b12'),$efin12);
                        }
                         /*----------------------------------------------------------------------------------------------*/
                        
                    }
                }
                elseif(($proyecto[0]['tp_id']==2 || $proyecto[0]['tp_id']==3 || $proyecto[0]['tp_id']==4) || ($proyecto[0]['tp_id']==1 && $fase[0]['pfec_ejecucion']==1)) //// PROGRAMAS RECURRENTES, OPERACION DE FUNCIONAMIENTO
                {
                    if($actividad[0]['indi_id']==1)
                    {
                        if($this->input->post('e1')!=0)
                        {
                            $this->model_actividad->add_act_ejec_gest($id_act,$gestion,1,$this->input->post('e1'),0,0,0);
                        }
                        if($this->input->post('e2')!=0)
                        {
                            $this->model_actividad->add_act_ejec_gest($id_act,$gestion,2,$this->input->post('e2'),0,0,0);
                        }
                        if($this->input->post('e3')!=0)
                        {
                            $this->model_actividad->add_act_ejec_gest($id_act,$gestion,3,$this->input->post('e3'),0,0,0);
                        }
                        if($this->input->post('e4')!=0)
                        {
                            $this->model_actividad->add_act_ejec_gest($id_act,$gestion,4,$this->input->post('e4'),0,0,0);
                        }
                        if($this->input->post('e5')!=0)
                        {
                            $this->model_actividad->add_act_ejec_gest($id_act,$gestion,5,$this->input->post('e5'),0,0,0);
                        }
                        if($this->input->post('e6')!=0)
                        {
                            $this->model_actividad->add_act_ejec_gest($id_act,$gestion,6,$this->input->post('e6'),0,0,0);
                        }
                        if($this->input->post('e7')!=0)
                        {
                            $this->model_actividad->add_act_ejec_gest($id_act,$gestion,7,$this->input->post('e7'),0,0,0);
                        }
                        if($this->input->post('e8')!=0)
                        {
                            $this->model_actividad->add_act_ejec_gest($id_act,$gestion,8,$this->input->post('e8'),0,0,0);
                        }
                        if($this->input->post('e9')!=0)
                        {
                            $this->model_actividad->add_act_ejec_gest($id_act,$gestion,9,$this->input->post('e9'),0,0,0);
                        }
                        if($this->input->post('e10')!=0)
                        {
                            $this->model_actividad->add_act_ejec_gest($id_act,$gestion,10,$this->input->post('e10'),0,0,0);
                        }
                        if($this->input->post('e11')!=0)
                        {
                            $this->model_actividad->add_act_ejec_gest($id_act,$gestion,11,$this->input->post('e11'),0,0,0);
                        }
                        if($this->input->post('e12')!=0)
                        {
                            $this->model_actividad->add_act_ejec_gest($id_act,$gestion,12,$this->input->post('e12'),0,0,0);
                        }
                    }
                    elseif ($actividad[0]['indi_id']==2) 
                    {
                        if($actividad[0]['act_denominador']==0) /// variable
                        {
                            $e1=(($this->input->post('b1')/$this->input->post('a1'))*$this->input->post('p1'));
                            $e2=(($this->input->post('b2')/$this->input->post('a2'))*$this->input->post('p2'));
                            $e3=(($this->input->post('b3')/$this->input->post('a3'))*$this->input->post('p3'));
                            $e4=(($this->input->post('b4')/$this->input->post('a4'))*$this->input->post('p4'));
                            $e5=(($this->input->post('b5')/$this->input->post('a5'))*$this->input->post('p5'));
                            $e6=(($this->input->post('b6')/$this->input->post('a6'))*$this->input->post('p6'));
                            $e7=(($this->input->post('b7')/$this->input->post('a7'))*$this->input->post('p7'));
                            $e8=(($this->input->post('b8')/$this->input->post('a8'))*$this->input->post('p8'));
                            $e9=(($this->input->post('b9')/$this->input->post('a9'))*$this->input->post('p9'));
                            $e10=(($this->input->post('b10')/$this->input->post('a10'))*$this->input->post('p10'));
                            $e11=(($this->input->post('b11')/$this->input->post('a11'))*$this->input->post('p11'));
                            $e12=(($this->input->post('b12')/$this->input->post('a12'))*$this->input->post('p12'));
                        }
                        elseif ($actividad[0]['act_denominador']==1) //// Fijo
                        {
                            $e1=(($this->input->post('b1')/$this->input->post('a1'))*100);
                            $e2=(($this->input->post('b2')/$this->input->post('a2'))*100);
                            $e3=(($this->input->post('b3')/$this->input->post('a3'))*100);
                            $e4=(($this->input->post('b4')/$this->input->post('a4'))*100);
                            $e5=(($this->input->post('b5')/$this->input->post('a5'))*100);
                            $e6=(($this->input->post('b6')/$this->input->post('a6'))*100);
                            $e7=(($this->input->post('b7')/$this->input->post('a7'))*100);
                            $e8=(($this->input->post('b8')/$this->input->post('a8'))*100);
                            $e9=(($this->input->post('b9')/$this->input->post('a9'))*100);
                            $e10=(($this->input->post('b10')/$this->input->post('a10'))*100);
                            $e11=(($this->input->post('b11')/$this->input->post('a11'))*100);
                            $e12=(($this->input->post('b12')/$this->input->post('a12'))*100);
                        }
                        /*-------------------------------- EJECUCION RELATIVO ---------------------------*/
                        if($this->input->post('a1')!=0)
                        {
                            $this->model_actividad->add_act_ejec_gest($id_act,$gestion,1,$e1,$this->input->post('a1'),$this->input->post('b1'),0);
                        }
                        if($this->input->post('a2')!=0)
                        {
                            $this->model_actividad->add_act_ejec_gest($id_act,$gestion,2,$e2,$this->input->post('a2'),$this->input->post('b2'),0);
                        }
                        if($this->input->post('a3')!=0)
                        {
                            $this->model_actividad->add_act_ejec_gest($id_act,$gestion,3,$e3,$this->input->post('a3'),$this->input->post('b3'),0);
                        }
                        if($this->input->post('a4')!=0)
                        {
                            $this->model_actividad->add_act_ejec_gest($id_act,$gestion,4,$e4,$this->input->post('a4'),$this->input->post('b4'),0);
                        }
                        if($this->input->post('a5')!=0)
                        {
                            $this->model_actividad->add_act_ejec_gest($id_act,$gestion,5,$e5,$this->input->post('a5'),$this->input->post('b5'),0);
                        }
                        if($this->input->post('a6')!=0)
                        {
                            $this->model_actividad->add_act_ejec_gest($id_act,$gestion,6,$e6,$this->input->post('a6'),$this->input->post('b6'),0);
                        }
                        if($this->input->post('a7')!=0)
                        {
                            $this->model_actividad->add_act_ejec_gest($id_act,$gestion,7,$e7,$this->input->post('a7'),$this->input->post('b7'),0);
                        }
                        if($this->input->post('a8')!=0)
                        {
                            $this->model_actividad->add_act_ejec_gest($id_act,$gestion,8,$e8,$this->input->post('a8'),$this->input->post('b8'),0);
                        }
                        if($this->input->post('a9')!=0)
                        {
                            $this->model_actividad->add_act_ejec_gest($id_act,$gestion,9,$e9,$this->input->post('a9'),$this->input->post('b9'),0);
                        }
                        if($this->input->post('a10')!=0)
                        {
                            $this->model_actividad->add_act_ejec_gest($id_act,$gestion,10,$e10,$this->input->post('a10'),$this->input->post('b10'),0);
                        }
                        if($this->input->post('a11')!=0)
                        {
                            $this->model_actividad->add_act_ejec_gest($id_act,$gestion,11,$e11,$this->input->post('a11'),$this->input->post('b11'),0);
                        }
                        if($this->input->post('a12')!=0)
                        {
                            $this->model_actividad->add_act_ejec_gest($id_act,$gestion,12,$e12,$this->input->post('a12'),$this->input->post('b12'),0);
                        }
                         /*----------------------------------------------------------------------------------------------*/
                        
                    }

                }
                    /*============================= END UPDATE PGESTION RODUCTOS ===============================*/

                  redirect('admin/prog/efisica_a/'.$this->input->post('mod').'/'.$this->input->post('id_f').'/'.$this->input->post('id_p').'/'.$this->input->post('id_c').'/'.$this->input->post('id_pr').'/true');
   
            }
            else
            {
                  redirect('admin/prog/ejec_act/'.$this->input->post('mod').'/'.$this->input->post('id_f').'/'.$this->input->post('id_p').'/'.$this->input->post('id_c').'/'.$this->input->post('id_pr').'/'.$this->input->post('id_ac').'/'.$gestion.'/false');
            }
        }
   }
 /*=================================== AVANCE FINANCIAMIENTO DE LA FASE ETAPA ================================*/
    /*=================================== ELIMINA ACTIVIDAD ==================================================*/
    public function delete_actividades($mod,$pfec_id,$proy_id,$com_id,$prod_id,$act_id){

        $fase = $this->model_faseetapa->get_id_fase($proy_id); //// DATOS DE LA FASE ACTIVA
        if($fase[0]['pfec_ejecucion']==1)
        {
            $insumos = $this->model_actividad->imsumo_actividad($act_id); //// Insumo actividad
        }
        elseif($fase[0]['pfec_ejecucion']==2)
        {
            $insumos = $this->model_componente->imsumo_componente($com_id); //// Insumo Componente
        }
        
        foreach ($insumos as $rowi) {
            $ins_gestion = $this->minsumos->list_insumos_gestion($rowi['ins_id']);
            foreach ($ins_gestion as $row)
            {
               // echo $row['insg_id'].'--'.$row['ins_id'].'-'.$row['g_id'].'--'.$row['insg_monto_prog']."<br>";
                $ins_fin = $this->minsumos->list_insumo_financiamiento($row['insg_id']);
                if(count($ins_fin)!=0)
                {
                    /*----------------- ELIMINA IFIN PROG MES---------------*/
                    $this->db->where('ifin_id', $ins_fin[0]['ifin_id']);
                    $this->db->delete('ifin_prog_mes');
                    /*------------------------------------------------------*/

                    /*----------------- ELIMINA IFIN EJEC MES---------------*/
                    $this->db->where('ifin_id', $ins_fin[0]['ifin_id']);
                    $this->db->delete('ifin_ejec_mes');
                    /*------------------------------------------------------*/

                    /*----------------- ELIMINA IFIN PROG MES---------------*/
                    $this->db->where('ifin_id', $ins_fin[0]['ifin_id']);
                    $this->db->delete('insumo_financiamiento');
                    /*------------------------------------------------------*/

                  //  echo "-----insumo fin : ".$ins_fin[0]['ifin_id'].'-'.$ins_fin[0]['insg_id'].'-'.$ins_fin[0]['ifin_monto'];
                }
                /*----------------- ELIMINA INS GESTION---------------*/
                    $this->db->where('insg_id', $row['insg_id']);
                    $this->db->delete('insumo_gestion');
                /*------------------------------------------------------*/
               // 
            }
                if($fase[0]['pfec_ejecucion']==1) //// directo
                {
                    /*----------------- ELIMINA INSUMO ACTIVIDAD ---------------*/
                    $this->db->where('ins_id', $rowi['ins_id']);
                    $this->db->delete('_insumoactividad');
                    /*----------------------------------------------------------*/
                }
                elseif ($fase[0]['pfec_ejecucion']==2) /// Delegado
                {
                   /*----------------- ELIMINA INSUMO COMPONENTE ---------------*/
                    $this->db->where('ins_id', $rowi['ins_id']);
                    $this->db->delete('insumocomponente');
                    /*-----------------------------------------------------------*/
                }

                /*----------------- ELIMINA INS GESTION---------------*/
                    $this->db->where('ins_id', $rowi['ins_id']);
                    $this->db->delete('insumos');
                /*------------------------------------------------------*/
        }

        /*------------ ELIMINA ACTIVIDAD PROGRAMADO -----------*/
            $this->db->where('act_id', $act_id);
            $this->db->delete('act_programado_mensual');

            $this->db->where('act_id', $act_id);
            $this->db->delete('act_ejecutado_mensual');
        /*---------------------------------------------------*/
        /*----------------- ELIMINA ACTIVIDAD ---------------*/
            $this->db->where('act_id', $act_id);
            $this->db->delete('_actividades');
        /*---------------------------------------------------*/

        $this->actualiza_ponderacion($pfec_id,$com_id,$prod_id); //// actualiza ponderaciones
        if(count($this->model_actividad->get_actividad_id($act_id))==0)
        {
            $this->session->set_flashdata('success','LA ACTIVIDAD SE ELIMINO CORRECTAMENTE');
            redirect('admin/prog/list_act/'.$mod.'/'.$pfec_id.'/'.$proy_id.'/'.$com_id.'/'.$prod_id.'/'.$act_id.'/true');
        }
        else
        {
            $this->session->set_flashdata('danger','ERROR AL ELIMINAR, INTENTELO DE NUEVO');
            redirect('admin/prog/list_act/'.$mod.'/'.$pfec_id.'/'.$proy_id.'/'.$com_id.'/'.$prod_id.'/'.$act_id.'/false');
        }

    }

    /*=========================================================================================================*/
    function rolfun($rol)
    {
      $valor=false;
      for ($i=1; $i <=count($rol) ; $i++) { 
        $data = $this->Users_model->get_datos_usuario_roles($this->session->userdata('fun_id'),$rol[$i]);
        if(count($data)!=0){
          $valor=true;
          break;
        }
      }
      return $valor;
    }
}