<?php

class Cponderacion_programas extends CI_Controller
{
    var $gestion;
    var $rol;
    var $fun_id;

    function __construct()
    {
        parent::__construct();
        $this->load->model('mantenimiento/mpoa');
        $this->load->model('mantenimiento/mapertura_programatica');
        $this->load->model('programacion/ponderacion/mponderacion');
        $this->load->model('programacion/prog_poa/mobjetivo_gestion');
        $this->load->model('programacion/prog_poa/mp_terminal');
        //llamar a mi menu
        $this->load->library('menu');
        $this->menu->const_menu(1);
        $this->gestion = $this->session->userData('gestion');
        $this->rol = $this->session->userData('rol');
        $this->fun_id = $this->session->userData('fun_id');
    }

    //LISTADE PROGRAMAS PARA PRODUTO TERMINAL
    function ponderacion_programas()
    {
        $dato = $this->tabla_red_objetivos();
        $data['lista_poa'] = $dato['tabla'];
        $data['ponderacion'] = $dato['ponderacion'];
        $data['ponderacion_js'] = '<SCRIPT src="' . base_url() . 'mis_js/programacion/ponderacion/ponderacion_programa.js" type="text/javascript"></SCRIPT>';
        $ruta = 'programacion/ponderacion/programa/vpond_programa';
        $this->construir_vista($ruta, $data);
    }

    //GENERAR TABLA DE RED DE OBJETIVOS
    function tabla_red_objetivos()
    {
        $lista_poa = $this->mpoa->lista_poa();
        $tabla = '';
        $cont = 1;
        $ponderacion = 0;
        foreach ($lista_poa as $row) {
            $ponderacion += $row['aper_ponderacion'];
            $tabla .= '<tr>';
            $tabla .= '<td>' . $cont . '</td>';
            $tabla .= '<td >
                            <a href="" class="form_pond_prog" data-toggle="modal" data-target="#modal_ponderacion" name="' . $row['aper_id'] . '">
                                <img src="' . base_url() . 'assets/ifinal/form2.png" width="40" height="40" class="img-responsive"
                                title="ASIGNAR PONDERACION">
							</a>
					   </td>';
            $tabla .= '<td>' . $row['poa_codigo'] . '</td>';
            $tabla .= '<td>' . $row['aper_programa'] . $row['aper_proyecto'] . $row['aper_actividad'] . " - " . $row['aper_descripcion'] . '</font></td>';
            $tabla .= '<td>' . $row['uni_unidad'] . '</td>';
            $tabla .= '<td>' . $row['poa_fecha_creacion'] . '</td>';
            $tabla .= '<td>' . $row['aper_ponderacion'] . '</td>';
            $tabla .= '</tr>';
            $cont++;
        }
        $data['tabla'] = $tabla;
        $data['ponderacion'] = $ponderacion;
        return $data;
    }

    //LLENAR MI FORMULARIO
    function get_form_programa_pond()
    {
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $post = $this->input->post();
            $aper_id = $post['aper_id'];
            $get_poa = $this->mponderacion->dato_poa($aper_id);
            $result = array(
                'id' => $get_poa->aper_id,
                'codigo' => $get_poa->poa_codigo,
                'programa' => $get_poa->aper_programa,
                'proyecto' => $get_poa->aper_proyecto,
                'actividad' => $get_poa->aper_actividad,
                'descripcion' => $get_poa->aper_descripcion,
                'ponderacion' => $get_poa->aper_ponderacion
            );
            echo json_encode($result);
        } else {
            show_404();
        }
    }

    //ADICIONAR PONDERACION DEL PROGRAMA
    function add_pond_programa()
    {
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $post = $this->input->post();
            $aper_id = $post['aper_id'];
            $ponderacion = $post['pond'];
            if ($this->mponderacion->add_pond_programa($aper_id, $ponderacion)) {
                $result['peticion'] = 'true';
            } else {
                $result['peticion'] = 'true';
            }
            echo json_encode($result);
        } else {
            show_404();
        }
    }

    //PONDERAR PROGRAMAS
    function ponderar_programas()
    {
        if ($this->input->is_ajax_request() && $this->input->post()) {
            //lista de mi carpeta poa con programas
            $lista_poa = $this->mpoa->lista_poa();
            $suma_total_asignado = $this->mponderacion->sum_total_prog_proy();
            $suma_total_asignado = round($suma_total_asignado->suma_asignado);//suma total del presupuesto por programa y proyecto
            //caso para asignar ponderacion
            foreach ($lista_poa as $row) {
                //OBTENER MI CODIGO DE PROGRAMA DE LA APERTURA
                $aper_programa = $this->mapertura_programatica->dato_apertura($row['aper_id']);
                $aper_programa = $aper_programa[0]['aper_programa'].'';
                $suma_proy =  $this->mponderacion->suma_aper_proy($aper_programa);
                if(count($suma_proy) == 0){
                    $suma_proy = 0;
                }else{
                    $suma_proy = round($suma_proy->suma);
                }

                //CALCULAR LA PONDERACION
                if($suma_total_asignado == 0){
                    $pond_porcentual = 0;
                }else{
                    $pond_porcentual = (($suma_proy * 100) / $suma_total_asignado);
                }
                $this->mponderacion->add_pond_programa($row['aper_id'], $pond_porcentual);
            }
            $data['peticion'] = 'true';
            echo json_encode($data);
        } else {
            show_404();
        }
    }

    //FUNCION PARA CONSTRUIR MI VISTA
    function construir_vista($ruta, $data)
    {
        //----------------------------------- MENU-------------------------------
        $menu['enlaces'] = $this->menu->get_enlaces();
        $menu['subenlaces'] = $this->menu->get_sub_enlaces();
        $menu['titulo'] = 'PROGRAMACIÓN';
        //-----------------------------------------------------------------------
        $this->load->view('includes/header');
        $this->load->view('includes/menu_lateral', $menu);
        $this->load->view($ruta, $data);//contenido
        $this->load->view('includes/footer');
    }
}