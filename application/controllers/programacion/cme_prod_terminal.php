<?php

class Cme_prod_terminal extends CI_Controller
{
    public $rol = array('1' => '2','2' => '6');
    public function __construct()
    {
        parent:: __construct();
        $this->load->model('Users_model','',true);
        $this->load->model('mantenimiento/mindicador');
        $this->load->model('programacion/marco_estrategico/mobjetivos');
        $this->load->model('mantenimiento/model_funcionario');
        $this->load->model('mantenimiento/munidad_organizacional');
        $this->load->model('mantenimiento/mpdes');
        $this->load->model('mantenimiento/mptdi');
        //llamar a mi menu
        $this->load->library('menu');
        $this->menu->const_menu(11);
    }

    function lista_objetivos($obje_id)
    {
        if($this->rolfun($this->rol)){ 
            $gestion = $this->session->userData('gestion');
            //lista de objetivo estrategicos
            $lista_pt = $this->mobjetivos->lista_prod_terminal($obje_id);
            $dato_obje = $this->mobjetivos->dato_objetivo($obje_id);
            $temporalizacion = array();
            foreach ($lista_pt as $row1) {
                $pt_id = $row1['pt_id'];// id de producto terminal
                $gestion_inicial = $this->mobjetivos->get_fec_inicio()->conf_gestion_desde;// TODO questy gesionini
                $linea_base = $row1['obje_linea_base'];
                $meta = $row1['obje_meta']; //variable meta
                $temporalizacion[$pt_id] = $this->progracion_gestiont($pt_id, $gestion_inicial, $linea_base, $meta);
            }
            $gestion_inicio = $this->mobjetivos->get_fec_inicio();
            $data['gestion_inicio'] = $gestion_inicio->conf_gestion_desde;
            $data['lista_objetivos'] = $lista_pt;
            $data['obje_id'] = $obje_id;
            $data['dato_obje'] = $dato_obje[0];
            $data['temporalizacion'] = $temporalizacion;
            //=============  armar vista ============================
            $ruta = 'programacion/marco_estrategico/obj/lista_prog_terminal';
            $this->construir_vista($ruta, $data);
        }
        else{
            redirect('admin/dashboard');
        }
    }

    //OBTENER TEMPORALIZACION DE PROGRMACION
    function progracion_gestiont($obje_id, $gestion_inicial, $linea_base, $meta)
    {
        $cont = 1;
        $prog_acumulado = $linea_base; //variable de programacion acumulada
        for ($i = $gestion_inicial; $i <= ($gestion_inicial + 4); $i++) {
            $puntero_prog = 'prog' . $cont;//PUNTERO PARA LISTAR LA PROGRAMACION
            $dat_prog = $this->mobjetivos->get_prog_pt_obj($obje_id, $i);
            $dato_programado = $dat_prog[0]['opm_programado'];
            $temporalizacion[$puntero_prog] = $dato_programado;//matriz
            //PROGRAMADO ACUMULADO
            $puntero_prog_acumulado = 'p_acumulado' . $cont;//PUNTERO PARA LA PROGRAMACION ACUMULADA
            $prog_acumulado += $dato_programado;
            $temporalizacion[$puntero_prog_acumulado] = $prog_acumulado;//matriz
            //PROGRAMA ACUMULADO PORECENTUAL
            $puntero_pa_porcentual = 'pa_porc' . $cont;//PUNTERO PARA LA PROGRAMACION ACUMULADA PORCENTUAL
            //PREGUNTAR DIVISION ENTRE 0
            if ($meta == 0) {
                $pa_porcentual = 0;
            } else {
                $pa_porcentual = (($prog_acumulado / $meta) * 100);
            }
            $temporalizacion[$puntero_pa_porcentual] = $pa_porcentual;//matriz
            $cont++;
        }
        return $temporalizacion;
    }

    //VISTA DE NUEVO PRODUCTO TERMINAL
    function nuevo_pterminal($obje_id)
    {
        if($this->rolfun($this->rol)){ 
            $gestion = $this->session->userData('gestion');
            $combo_tindicador = "";// combo tipo de indicador
            $combo_resp = "";// combo responsable
            $combo_unidad = "";// combo unidad organizacional
            $combo_pilar_pdes = "";// combo pilar de pdes
            $combo_pilar_ptdi = "";// combo pilar de ptdi
            $dato_obje = $this->mobjetivos->dato_objetivo($obje_id);
            // var_dump($dato_obje);  die;
            $dato_obje = $dato_obje[0];
            // construimos el combo de tipo de indicador
            $lista_indicador = $this->mindicador->get_indicador();
            foreach ($lista_indicador as $row) {
                $combo_tindicador .= "<option value='" . $row['indi_id'] . "'>" . $row['indi_descripcion'] . "</option>";
            }
            // construimos el combo del funcionario
            $lista_funcionario = $this->model_funcionario->get_funcionarios();
            foreach ($lista_funcionario as $row) {
                if ($row['car_id'] == 7) {//cargo responsable
                    $nombre = $row['fun_nombre'] . ' ' . $row['fun_paterno'] . ' ' . $row['fun_materno'];
                    $combo_resp .= "<option value='" . $row['fun_id'] . "'>" . $nombre . " " . $row['car_cargo'] . "</option>";
                }
            }
            //construir el combo pilar PDES
            $lista_pilar = $this->mpdes->lista_pilar($gestion);
            foreach ($lista_pilar as $row) {
                if ($row['pdes_codigo'] == $dato_obje['pdes_pcod'] && $row['pdes_gestion'] == $dato_obje['obje_gestion_curso']) {
                    $combo_pilar_pdes .= '<option value="' . $row['pdes_codigo'] . '" selected>' .
                        $row['pdes_codigo'] . ' - ' . $row['pdes_nivel'] . ' - ' . $row['pdes_descripcion'] . '</option>';
                } else {
                    $combo_pilar_pdes .= '<option value="' . $row['pdes_codigo'] . '">' .
                        $row['pdes_codigo'] . ' - ' . $row['pdes_nivel'] . ' - ' . $row['pdes_descripcion'] . '</option>';
                }
            }
            //construir el combo pilar PTDI
            $lista_pilar = $this->mptdi->lista_pilar($gestion);
            foreach ($lista_pilar as $row) {
                if ($row['ptdi_codigo'] == $dato_obje['ptdi_ecod']) {
                    $combo_pilar_ptdi .= '<option value="' . $row['ptdi_codigo'] . '" selected>' .
                        $row['ptdi_codigo'] . ' - ' . $row['ptdi_nivel'] . ' - ' . $row['ptdi_descripcion'] . '</option>';
                } else {
                    $combo_pilar_ptdi .= '<option value="' . $row['ptdi_codigo'] . '">' .
                        $row['ptdi_codigo'] . ' - ' . $row['ptdi_nivel'] . ' - ' . $row['ptdi_descripcion'] . '</option>';
                }

            }
            $data['combo_indicador'] = $combo_tindicador;
            $data['obje_id'] = $obje_id;
            $data['dato_obje'] = $dato_obje;
            $data['combo_resp'] = $combo_resp;
            $data['combo_unidad'] = $combo_unidad;
            $data['combo_pilar_pdes'] = $combo_pilar_pdes;
            $data['combo_pilar_ptdi'] = $combo_pilar_ptdi;
            $data['lista_pdes'] = $this->mpdes->lista_pdes($gestion);
            $data['lista_ptdi'] = $this->mptdi->lista_ptdi($gestion);
            //gestion inicial
            $gestion_inicio = $this->mobjetivos->get_fec_inicio();
            $data['gestion_inicio'] = $gestion_inicio->conf_gestion_desde;
            //======================  armar vista ========================
            // antigua  == >  vista objetivo
            //$this->session->set_flashdata('guardar', 'EL REGISTRO SE GUARDO CORRECTAMENTE !!!');
            $ruta = 'programacion/marco_estrategico/obj/nuevo_prog_terminal';
            $this->construir_vista($ruta, $data);
        }
        else{
            redirect('admin/dashboard');
        }
    }

    //OBTENER DATO DE UNIDAD ORGANIZACIONAL
    function get_unidad_org()
    {
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $post = $this->input->post();
            $fun_id = $post['fun_id'];//obtiene el id de la apertura
            $uni_id = $this->model_funcionario->get_funcionario($fun_id);
            //unidad organizacional
            $unidad = $this->munidad_organizacional->get_unidad_org($uni_id[0]['uni_id']);
            $result = array(
                'unidad' => $unidad[0]['uni_unidad']
            );
            echo json_encode($result);
        } else {
            show_404();
        }
    }

    //GENERAR MIS DATOS PARA MI MODAL DE GRAFICO
    function get_grafico_obje()
    {
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $post = $this->input->post();
            $obje_id = $post['obje_id'];
            $objetivo = $this->mobjetivos->dato_objetivo($obje_id);
            $linea_base = $objetivo[0]['obje_linea_base'];
            $meta = $objetivo[0]['obje_meta']; //variable meta
            $gestion_inicial = $this->mobjetivos->get_fec_inicio()->conf_gestion_desde;// id de mi objetivo estrategico
            //OBTENER MI TABLA TEMPORALIZACION
            $temporalizacion = $this->progracion_gestion($obje_id, $gestion_inicial, $linea_base, $meta);
            $indi[1] = '';
            $indi[2] = '%';
            $porc = $indi[$objetivo[0]['indi_id']];
            $vec_pa_grafico = array();
            $tabla = '';
            $tabla .= '<tr>
                            <td colspan="6" bgcolor="#2F4F4F"><center><b><font color="#ffffff" size="1">
                         PROGRAMACI&Oacute;N ' . $gestion_inicial . ' - ' . ($gestion_inicial + 4) . '</font></b></center></td>
                       </tr>';
            //---------------------- CABECERA DE GESTIONES
            $tabla .= '<tr>';
            $tabla .= '<td style="width:1%;" bgcolor="#2F4F4F"></td>';
            for ($i = 1; $i <= 5; $i++) {
                $tabla .= '<td style="width:1%;" bgcolor="#2F4F4F"><center><b><font color="#ffffff" size="1">' . ($gestion_inicial++) . '</font></b></center></td>';
            }
            $tabla .= '</tr>';
            //---------------------- FIN DE CABECERA
            //---------------------- PROGRAMACION
            $tabla .= '<tr>';
            $tabla .= '<td style="width:5%;" bgcolor="#E6E6FA"><center><b><font color="#000000" size="1">P</font></b></center></td>';
            for ($i = 1; $i <= 5; $i++) {
                $puntero = 'prog' . $i;
                $prog_gestion = $temporalizacion[$puntero];
                $tabla .= ' <td style="width:1%;" bgcolor="#E6E6FA"><center><font color="#000000" size="1">' . round($prog_gestion, 1) . $porc . '</font></center></td>';
            }
            $tabla .= '</tr>';
            //--------------------- PROGRAMACION ACUMULADA
            $tabla .= '<tr>';
            $tabla .= '<td style="width:5%;" bgcolor="#E6E6FA"><center><b><font color="#000000" size="1">P.A</font></b></center></td>';
            for ($i = 1; $i <= 5; $i++) {
                $puntero_acumulado = 'p_acumulado' . $i;
                $prog_acumulado = $temporalizacion[$puntero_acumulado];
                $tabla .= ' <td style="width:1%;" bgcolor="#E6E6FA"><center><font color="#000000" size="1">' . round($prog_acumulado, 1) . $porc . '</font></center></td>';
            }
            $tabla .= '</tr>';
            //--------------------- PROGRAMACION ACUMULADA PORCENTUAL
            $tabla .= '<tr>';
            $tabla .= '<td style="width:5%;" bgcolor="#E6E6FA"><center><b><font color="#000000" size="1">%P.A</font></b></center></td>';
            for ($i = 1; $i <= 5; $i++) {
                $puntero_pa_porcentual = 'pa_porc' . $i;
                $pa_porcentual = $temporalizacion[$puntero_pa_porcentual];
                $vec_pa_grafico[($i - 1)] = $pa_porcentual;
                $tabla .= ' <td style="width:1%;" bgcolor="#E6E6FA"><center><font color="#000000" size="1">' . round($pa_porcentual, 1) . $porc . '%</font></center></td>';
            }
            $tabla .= '</tr>';
            $result = array(
                'tabla' => $tabla,
                'linea_base' => $linea_base,
                'meta' => $meta,
                'prog_acumulada_p' => $vec_pa_grafico,
                'gestion' => $this->mobjetivos->get_fec_inicio()->conf_gestion_desde// id de mi objetivo estrategico
            );
            echo json_encode($result);
        } else {
            show_404();
        }
    }

    function guardar_pterminal()
    {

        if ($this->input->post()) {
            $this->form_validation->set_rules('fun_id', 'funcionario', 'required');
            $this->form_validation->set_rules('pedes1', 'pedes1', 'required');
            $this->form_validation->set_rules('pedes2', 'pedes2', 'required');
            $this->form_validation->set_rules('pedes3', 'pedes3', 'required');
            $this->form_validation->set_rules('pedes4', 'pedes4', 'required');
            // $this->form_validation->set_rules('ptdi1', 'ptdi1', 'required');
            // $this->form_validation->set_rules('ptdi2', 'ptdi2', 'required');
            // $this->form_validation->set_rules('ptdi3', 'ptdi3', 'required');
            // $this->form_validation->set_rules('ptdi4', 'ptdi4', 'required');
            $this->form_validation->set_rules('obj', 'objetivo', 'required');
            $this->form_validation->set_rules('tipo_i', 'tipo_indicador', 'required');
            $this->form_validation->set_rules('meta', 'meta', 'required');
            //=========================== mensajes =========================================
            $this->form_validation->set_message('required', 'El campo es es obligatorio');
            $post = $this->input->post();
            //gestion inicial
            $gestion_inicio = $this->mobjetivos->get_fec_inicio();
            $gestion_inicial = $gestion_inicio->conf_gestion_desde;
            //$gestion_inicial = $this->session->userData('gestion');
            if ($this->form_validation->run()) {
                $post = $this->input->post();
                $data = array(
                    //'gestion' => $this->session->userData('gestion'),
                    'fun_id' => $post["fun_id"],
                    'pdes1' => $post["pedes1"],
                    'pdes2' => $post["pedes2"],
                    'pdes3' => $post["pedes3"],
                    'pdes4' => $post["pedes4"],
                    'ptdi1' => $post["ptdi1"],
                    'ptdi2' => $post["ptdi2"],
                    'ptdi3' => $post["ptdi3"],
                    'obj' => $this->security->xss_clean($post['obj']),
                    'tipo_i' => $post['tipo_i'],
                    'obje_id' => $post['obje_id'],
                    'indicador' => $this->security->xss_clean($post['indicador']),
                    'formula' => $this->security->xss_clean($post['formula']),
                    'lb' => $this->security->xss_clean($post['lb']),
                    'meta' => $this->security->xss_clean($post['meta']),
                    'pn_cion' => $this->security->xss_clean($post['pn_cion']),
                    'verificacion' => $this->security->xss_clean($post['verificacion']),
                    'supuestos' => $this->security->xss_clean($post['supuestos']),
                    'c_a' => $this->security->xss_clean($post['c_a']),
                    'c_b' => $this->security->xss_clean($post['c_b']),
                    'c_c' => $this->security->xss_clean($post['c_c']),
                    $gestion_inicial => $this->security->xss_clean($post['g1']),//programado por gestion
                    ($gestion_inicial + 1) => $this->security->xss_clean($post['g2']),
                    ($gestion_inicial + 2) => $this->security->xss_clean($post['g3']),
                    ($gestion_inicial + 3) => $this->security->xss_clean($post['g4']),
                    ($gestion_inicial + 4) => $this->security->xss_clean($post['g5']),
                    'denominador' => $post["denominador"],
                );
                //guardar objetivo
                $peticion = $this->mobjetivos->guardar_productot($data);
                $url = site_url("") . '/prog/pterminalg/' . $post['obje_id'];
                if ($peticion) {
                    $this->session->set_flashdata('guardar', 'EL REGISTRO SE GUARDO CORRECTAMENTE');
                    redirect($url, 'refresh');
                    //redirect($url);
                   // echo $this->session->flashdata('guardar');
                } else {
                    echo 'fallo al guardar';
                }
            } else {
                echo 'DATOS ERRONEOS';
            }
        } else {
            show_404();
        }
    }

    //FUNCION OBTENER COMBOS DEL PDES
    function lista_combo_pdes()
    {
        if ($this->input->post()) {
            $gestion = $this->session->userData('gestion');
            $post = $this->input->post();
            $id = $post["elegido"];
            $lista_metas = $this->mpdes->lista_combo($gestion, $id);
            $combo = '<option value="">Seleccione una opci&oacute;n</option>';
            if ($this->input->post('ultimo')) {
                //EN CASO DE SER EL ULTIMO COMBO COLOCAR EN EL VALUE EL ID PARA GUARDAR
                foreach ($lista_metas as $row) {
                    $combo .= '<option value="' . $row['pdes_id'] . '">' .
                        $row['pdes_codigo'] . ' - ' . $row['pdes_nivel'] . ' - ' . $row['pdes_descripcion'] . '</option>';
                }
            } else {
                foreach ($lista_metas as $row) {
                    $combo .= '<option value="' . $row['pdes_codigo'] . '">' .
                        $row['pdes_codigo'] . ' - ' . $row['pdes_nivel'] . ' - ' . $row['pdes_descripcion'] . '</option>';
                }
            }


            echo $combo;
        } else {
            show_404();
        }
    }

    //FUNCION OBTENER COMBOS DEL PTDI
    function lista_combo_ptdi()
    {
        if ($this->input->post()) {
            $gestion = $this->session->userData('gestion');
            $post = $this->input->post();
            $id = $post["elegido"];
            $lista_metas = $this->mptdi->lista_combo($gestion, $id);
            $combo = '<option value="">Seleccione una opci&oacute;n</option>';
            if ($this->input->post('ultimo')) {
                //EN CASO DE SER EL ULTIMO COMBO COLOCAR EN EL VALUE EL ID PARA GUARDAR
                foreach ($lista_metas as $row) {
                    $combo .= '<option value="' . $row['ptdi_id'] . '">' .
                        $row['ptdi_codigo'] . ' - ' . $row['ptdi_nivel'] . ' - ' . $row['ptdi_descripcion'] . '</option>';
                }
            } else {
                foreach ($lista_metas as $row) {
                    $combo .= '<option value="' . $row['ptdi_codigo'] . '">' .
                        $row['ptdi_codigo'] . ' - ' . $row['ptdi_nivel'] . ' - ' . $row['ptdi_descripcion'] . '</option>';
                }
            }
            echo $combo;
        } else {
            show_404();
        }
    }

    //INDICADOR DE DESEMPEÑO
    function indicador_desempenio($obje_id)
    {
        $data = '';
        $data['objetivo'] = $this->mobjetivos->dato_objetivo($obje_id);
        //=============  armar vista ============================
        $ruta = 'programacion/marco_estrategico/obj/indicador_desem';
        $this->construir_vista($ruta, $data);
    }
    //valida indicador de desempeño
    ///////////////////VALIDA INDICADOR DE DESEMPENIO /////////////////////////////
    public function valida_indicador()
    {
        //echo $this->input->post('c_a');
        if ($this->input->server('REQUEST_METHOD') === 'POST') ///// DATOS GENERALES
        {
            $this->form_validation->set_rules('id', 'Objetivo ID', 'required|trim');

            if ($this->form_validation->run()) {
                $data_to_store = array( ///// Tabla Proyectos
                    'obje_eficacia' => $this->input->post('ef1'),
                    'obje_eficiencia' => $this->input->post('ef2'),
                    'obje_eficiencia_pe' => $this->input->post('ef3'),
                    'obje_eficiencia_fi' => $this->input->post('ef4'),
                );
                $this->db->where('obje_id', $this->input->post('id'));
                $this->db->update('objetivosestrategicos', $data_to_store);

                redirect('prog/me/objetivo');
            } else {
                redirect('prog/me/objetivo');
            }
        }
    }

    //FUNCION PARA CONSTRUIR MI VISTA
    function construir_vista($ruta, $data)
    {
        //----------------------------------- MENU-------------------------------
        $menu['enlaces'] = $this->menu->get_enlaces();
        $menu['subenlaces'] = $this->menu->get_sub_enlaces();
        $menu['titulo'] = 'PROGRAMACI&Oacute;N';
        //-----------------------------------------------------------------------
        //armar vista
        $this->load->view('includes/header');
        $this->load->view('includes/menu_lateral', $menu);
        $this->load->view($ruta, $data);//contenido
        //$this->load->view('admin/mantenimiento/vprueba');//contenido
        $this->load->view('includes/footer');
    }

    //LLENAR MI FORMULARIO DE DE CARGAR ARCHIVO
    function get_form_archivo()
    {
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $post = $this->input->post();
            $obje_id = $post['obje_id'];
            $data = $this->mobjetivos->dato_objetivo($obje_id);
            $data = $data[0];
            if (strlen($data['obje_archivo_adjunto']) == 0) {
                $result = array(
                    'respuesta' => 'true',
                    'codigo' => $data['obje_codigo'],
                    'ruta' => ''
                );
            } else {
                $result = array(
                    'respuesta' => 'false',
                    'codigo' => $data['obje_codigo'],
                    'ruta' => $data['obje_archivo_adjunto']
                );
            }

            echo json_encode($result);
        } else {
            show_404();
        }
    }

    //GUARDAR ARCHIVO DEL OBJETIVOS
    function guardar_archivo()
    {
        if ($this->input->post()) {
            $post = $this->input->post();
            $obje_id = $post['id_obje_pdf'];
            $accion = $post['mod_eli'];//accion modificar o eliminar
            $gestion = $this->session->userData('gestion');
            //-------------------------------------------------RECIBIR MI DOCUMENTO
            $filename = $_FILES["userfile"]["name"];
            $file_basename = substr($filename, 0, strripos($filename, '.')); // get file extention
            $file_ext = substr($filename, strripos($filename, '.')); // get file name
            $filesize = $_FILES["userfile"]["size"];
            $allowed_file_types = array('.pdf', '.doc', '.jpg', '.JPG', '.png', '.JPEG');
            if ($accion != '0') {
                $file_eliminar = "archivos/oestrategicos/" . $accion;
                unlink($file_eliminar);
            }
            if (/*in_array($file_ext, $allowed_file_types) && */($filesize < 800000000000)) { // Rename file
                $newfilename = 'OBJ-' . $gestion . '-' . substr(md5(uniqid(rand())), 0, 5) . $file_ext;
                //--------------------------------$data_to_store['pa_ruta_archivo'] = "" . $newfilename;
                if (file_exists("archivos/oestrategicos/" . $newfilename)) {
                    echo "<script>alert('Ya existe este archivo')</script>";
                } else {
                    move_uploaded_file($_FILES["userfile"]["tmp_name"], "archivos/oestrategicos/" . $newfilename);
                    //guardar mi area urbana despues de las validaciones
                    $data['obje_archivo_adjunto'] = $newfilename;
                    $this->db->WHERE('obje_id', $obje_id);
                    $this->db->UPDATE('objetivosestrategicos', $data);
                    $this->session->set_flashdata('guardar_archivo', 'EL ARCHIVO SE GUARDO CORRECTAMENTE');
                    redirect('prog/me/objetivo');
                }
            } elseif (empty($file_basename)) {
                echo "Selecciona un archivo para cargarlo.";
            } elseif ($filesize > 100000000000) {
                $this->session->set_flashdata('guardar_archivo', 'EL ARCHIVO SE GUARDO CORRECTAMENTE');
                redirect('prog/me/objetivo');
            } else {
                $mensaje = "Sólo estos tipos de archivo se permiten para la carga: " . implode(', ', $allowed_file_types);
                echo '<script>alert("' . $mensaje . '")</script>';
                unlink($_FILES["userfile"]["tmp_name"]);
            }

        } else {
            show_404();
        }
    }

    //Eliminar Archivo
    function del_archivo()
    {
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $post = $this->input->post();
            $obje_id = $post['id_objetivo'];
            $data = $this->mobjetivos->dato_objetivo($obje_id);
            $data = $data[0];
            if (strlen($data['obje_archivo_adjunto']) != 0) {
                //guardar objetivo
                $this->mobjetivos->del_archivo($obje_id);
                $file_eliminar = "archivos/oestrategicos/" . $data['obje_archivo_adjunto'];
                unlink($file_eliminar);
            }
            $result = array(
                'respuesta' => 'true',
            );
            echo json_encode($result);
        } else {
            show_404();
        }
    }

    //MODIFICAR OBJETIVOS TERMINALES
    function vista_modificar_objetivot($obje_id, $obje_id_d)
    {
        if($this->rolfun($this->rol)){     
            $gestion = $this->session->userData('gestion');
            $combo_tindicador = "";// combo tipo de indicador
            $combo_resp = "";// combo responsable
            $combo_unidad = "";// combo unidad organizacional
            $combo_pilar_pdes = "";// combo pilar de pdes
            $combo_pilar_ptdi = "";// combo pilar de ptdi
            //datos de mi objetivo estrategico
            $dato_obje = $this->mobjetivos->dato_objetivot($obje_id);
            $dato_obje = $dato_obje[0];
            //lista de mi programacion de gestion
            //$gestion_inicial = $dato_obje['obje_gestion_curso'];// id de mi objetivo estrategico
            //gestion inicial
            $gestion_inicio = $this->mobjetivos->get_fec_inicio();
            $gestion_inicial = $gestion_inicio->conf_gestion_desde;
            $linea_base = $dato_obje['obje_linea_base'];
            $meta = $dato_obje['obje_meta']; //variable meta
            $temporalizacion = $this->progracion_gestiont($obje_id, $gestion_inicial, $linea_base, $meta);
            $data['prog'] = $temporalizacion;
            $data['dato_obje'] = $dato_obje;
            // construimos el combo de tipo de indicador
            $lista_indicador = $this->mindicador->get_indicador();
            foreach ($lista_indicador as $row) {
                if ($row['indi_id'] == $dato_obje['indi_id']) {
                    $combo_tindicador .= "<option value='" . $row['indi_id'] . "' selected>" . $row['indi_descripcion'] . "</option>";
                } else {
                    $combo_tindicador .= "<option value='" . $row['indi_id'] . "'>" . $row['indi_descripcion'] . "</option>";
                }
            }
            // construimos el combo del funcionario
            $lista_funcionario = $this->model_funcionario->get_funcionarios();
            foreach ($lista_funcionario as $row) {
                if ($row['fun_id'] == $dato_obje['fun_id']) {
                    $nombre = $row['fun_nombre'] . ' ' . $row['fun_paterno'] . ' ' . $row['fun_materno'];
                    $combo_resp .= "<option value='" . $row['fun_id'] . "' selected>" . $nombre . " " . $row['car_cargo'] . "</option>";
                } else if($row['car_id'] ==7){
                    $nombre = $row['fun_nombre'] . ' ' . $row['fun_paterno'] . ' ' . $row['fun_materno'];
                    $combo_resp .= "<option value='" . $row['fun_id'] . "'>" . $nombre . " " . $row['car_cargo'] . "</option>";
                }


            }
            //construir el combo pilar PDES
            $lista_pilar = $this->mpdes->lista_pilar($gestion);
            foreach ($lista_pilar as $row) {
                if ($row['pdes_codigo'] == $dato_obje['pdes_pcod'] && $row['pdes_gestion'] == $dato_obje['obje_gestion_curso']) {
                    $combo_pilar_pdes .= '<option value="' . $row['pdes_codigo'] . '" selected>' .
                        $row['pdes_codigo'] . ' - ' . $row['pdes_nivel'] . ' - ' . $row['pdes_descripcion'] . '</option>';
                } else {
                    $combo_pilar_pdes .= '<option value="' . $row['pdes_codigo'] . '">' .
                        $row['pdes_codigo'] . ' - ' . $row['pdes_nivel'] . ' - ' . $row['pdes_descripcion'] . '</option>';
                }
            }
            //construir el combo pilar PTDI
            $lista_pilar = $this->mptdi->lista_pilar($gestion);
            foreach ($lista_pilar as $row) {
                if ($row['ptdi_codigo'] == $dato_obje['ptdi_ecod']) {
                    $combo_pilar_ptdi .= '<option value="' . $row['ptdi_codigo'] . '" selected>' .
                        $row['ptdi_codigo'] . ' - ' . $row['ptdi_nivel'] . ' - ' . $row['ptdi_descripcion'] . '</option>';
                } else {
                    $combo_pilar_ptdi .= '<option value="' . $row['ptdi_codigo'] . '">' .
                        $row['ptdi_codigo'] . ' - ' . $row['ptdi_nivel'] . ' - ' . $row['ptdi_descripcion'] . '</option>';
                }

            }
            $data['combo_indicador'] = $combo_tindicador;
            $data['combo_resp'] = $combo_resp;
            $data['combo_unidad'] = $combo_unidad;
            $data['combo_pilar_pdes'] = $combo_pilar_pdes;
            $data['combo_pilar_ptdi'] = $combo_pilar_ptdi;
            $data['lista_pdes'] = $this->mpdes->lista_pdes($gestion);
            $data['lista_ptdi'] = $this->mptdi->lista_ptdi($gestion);
            $data['obje_id'] = $obje_id;
            $data['obje_id_d'] = $obje_id_d;
            //gestion inicial
            $gestion_inicio = $this->mobjetivos->get_fec_inicio();
            $data['gestion_inicio'] = $gestion_inicio->conf_gestion_desde;
            //======================  armar vista ========================
            $ruta = 'programacion/marco_estrategico/obj/vmod_objetivo_terminal';
            $this->construir_vista($ruta, $data);
        }
        else{
                redirect('admin/dashboard');

            }
    }

    //MODIFICAR PRODUCTO TERMINAL DE OBJETIVO ESTRATEGICO
    function modificar_objetivot()
    {
        if ($this->input->post()) {
            $this->form_validation->set_rules('fun_id', 'funcionario', 'required');
            $this->form_validation->set_rules('pedes1', 'pedes1', 'required');
            $this->form_validation->set_rules('pedes2', 'pedes2', 'required');
            $this->form_validation->set_rules('pedes3', 'pedes3', 'required');
            $this->form_validation->set_rules('pedes4', 'pedes4', 'required');
            $this->form_validation->set_rules('ptdi1', 'ptdi1', 'required');
            $this->form_validation->set_rules('ptdi2', 'ptdi2', 'required');
            $this->form_validation->set_rules('ptdi3', 'ptdi3', 'required');
            $this->form_validation->set_rules('obj', 'objetivo', 'required');
            $this->form_validation->set_rules('tipo_i', 'tipo_indicador', 'required');
            $this->form_validation->set_rules('meta', 'meta', 'required');
            //=========================== mensajes =========================================
            $this->form_validation->set_message('required', 'El campo es es obligatorio');
            $post = $this->input->post();
            $obje_id = $post['obje_id'];
            //gestion inicial
            $gestion_inicio = $this->mobjetivos->get_fec_inicio();
            $gestion_inicial = $gestion_inicio->conf_gestion_desde;
            //$gestion_inicial = $this->session->userData('gestion');
            if ($this->form_validation->run()) {
                $post = $this->input->post();
                $data = array(
                    'gestion' => $this->session->userData('gestion'),
                    'fun_id' => $post["fun_id"],
                    'pdes1' => $post["pedes1"],
                    'pdes2' => $post["pedes2"],
                    'pdes3' => $post["pedes3"],
                    'pdes4' => $post["pedes4"],
                    'ptdi1' => $post["ptdi1"],
                    'ptdi2' => $post["ptdi2"],
                    'ptdi3' => $post["ptdi3"],
                    'obj' => $this->security->xss_clean($post['obj']),
                    'tipo_i' => $post['tipo_i'],
                    'obje_id_d' => $post['obje_id_d'],   //REVIEW REVIEW CDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
                    'indicador' => $this->security->xss_clean($post['indicador']),
                    'formula' => $this->security->xss_clean($post['formula']),
                    'lb' => $this->security->xss_clean($post['lb']),
                    'meta' => $this->security->xss_clean($post['meta']),
                    'pn_cion' => $this->security->xss_clean($post['pn_cion']),
                    'verificacion' => $this->security->xss_clean($post['verificacion']),
                    'supuestos' => $this->security->xss_clean($post['supuestos']),
                    'c_a' => $this->security->xss_clean($post['c_a']),
                    'c_b' => $this->security->xss_clean($post['c_b']),
                    'c_c' => $this->security->xss_clean($post['c_c']),
                    $gestion_inicial => $this->security->xss_clean($post['g1']),//programado por gestion
                    ($gestion_inicial + 1) => $this->security->xss_clean($post['g2']),
                    ($gestion_inicial + 2) => $this->security->xss_clean($post['g3']),
                    ($gestion_inicial + 3) => $this->security->xss_clean($post['g4']),
                    ($gestion_inicial + 4) => $this->security->xss_clean($post['g5']),
                    'denominador' => $post["denominador"],
                );
                //guardar objetivo terminal
                $peticion = $this->mobjetivos->modificar_objt($data, $obje_id);
                $url = site_url("") . '/prog/pterminalg/' . $post['obje_id_d'];
                if ($peticion) {
                    $this->session->set_flashdata('modificar', 'EL REGISTRO SE MODIFICO CORRECTAMENTE !!!');
                    redirect($url, 'refresh');
                    //redirect($url);
                    echo $this->session->flashdata('guardar');
                } else {
                    echo 'fallo al guardar';
                }
            } else {
                echo 'DATOS ERRONEOS';
            }
        } else {
            show_404();
        }
    }

    //ELIMINAR OBJEIVO TERMINAL
    function del_objet()
    {
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $post = $this->input->post();
            $obje_id = $post['obje_id'];
            $peticion = $this->mobjetivos->eliminar_objet($obje_id);
            $data = array(
                'respuesta' => $peticion
            );
            echo json_encode($data);
        } else {
            show_404();
        }
    }


    function rolfun($rol)
    {
      $valor=false;
      for ($i=1; $i <=count($rol) ; $i++) { 
        $data = $this->Users_model->get_datos_usuario_roles($this->session->userdata('fun_id'),$rol[$i]);
        if(count($data)!=0){
          $valor=true;
          break;
        }
      }
      return $valor;
    }


}