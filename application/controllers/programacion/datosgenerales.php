<?php

class Datosgenerales extends CI_Controller { 
  public $rol = array('1' => '3','2' => '4'); 
  public function __construct ()
  {
      parent::__construct();
      $this->load->model('Users_model','',true);
      if($this->rolfun($this->rol)){
        if($this->session->userdata('fun_id')!=null){
        $this->load->model('menu_modelo');
        $this->load->model('Users_model','',true);
        $this->load->model('programacion/model_proyecto');
        $this->load->model('programacion/model_faseetapa');
        $this->load->model('programacion/model_componente');
        $this->load->model('programacion/model_producto');
        $this->load->model('programacion/model_actividad');
        $this->load->model('programacion/insumos/mfinanciera');
        }else{
            redirect('/','refresh');
        }
      }
      else{
          redirect('admin/dashboard');
      }
  }

 /*====================================== DASBOARD PROGRAMACION DEL PROYECTO ================================*/
  public function dashboard_programacion($mod,$id_proy)
  {
    if($mod!='' & $id_proy!=''){  
      $enlaces=$this->menu_modelo->get_Modulos_programacion(1);
      $data['enlaces'] = $enlaces;
      $data['nro_fase']= $this->model_faseetapa->nro_fase($id_proy); /// nro de fases y etapas registrados
      $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_proy); ////// datos generales del proyecto con id_p x
      $data['id_f'] = $this->model_faseetapa->get_id_fase($id_proy); //// recupera datos de la tabla fase activa
      $data['mod']=$mod;
      $data['img']=$this->model_faseetapa->get_img_ejec($data['id_f'][0]['id']);

      $titulo_proy=strtoupper($data['proyecto'][0]['tipo']);
      $data['titulo_proy'] = $titulo_proy;
      
      $this->load->view('admin/programacion/datos_generales/dashboard', $data);
    }
    else{
      redirect('admin/dashboard');
    } 
  }
  /*==================================================================================================*/ 
  /*============================= LISTA DE PROYECTOS ANUAL - PLURIANUAL ===============================*/  
    public function mis_proyectos($mod)
    {
      $enlaces=$this->menu_modelo->get_Modulos($mod);
      $data['enlaces'] = $enlaces;
        for($i=0;$i<count($enlaces);$i++)
        {
          $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
      $data['subenlaces'] = $subenlaces;
      $data['lista_aper_padres'] = $this->model_proyecto->list_prog();//lista de aperturas padres 
       //load the view
      $data['mod']=$mod;
      if($mod==1){
        redirect('admin/proy/list_proy');
      }
      elseif($mod==4){
        redirect('admin/sgp/list_proy');
      }

    }
  /*==================================================================================================*/ 

  /*============================= DATOS GENERALES EDIT FORM1 ===============================*/ 
  public function datos_generales($mod,$id_f,$id_proy)
  {
	  redirect('programacion/proyecto/ruta_edit_proy/'.$id_proy.'/1');
/*    if($mod!='' & $id_f!='' & $id_proy!=''){  
      $enlaces=$this->menu_modelo->get_Modulos_programacion(1);
      $data['enlaces'] = $enlaces;
      $data['nro_fase']= $this->model_faseetapa->nro_fase($id_proy); /// nro de fases y etapas registrados
      $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_proy); ////// datos generales del proyecto con id_p x
      $data['id_f'] = $this->model_faseetapa->get_id_fase($id_proy); //// recupera datos de la tabla fase activa
      $data['mod']=$mod;
      $data['aper'] = $this->model_proyecto->apertura_p($this->session->userdata("gestion"),$data['proyecto'][0]['aper_programa'],'0000','000');
      $data['mod']=$mod;
      //load the view
      $this->load->view('admin/programacion/datos_generales/generales/cgform1', $data);
    } 
    else{
      redirect('admin/dashboard');
    } */
  } 
  /*==================================================================================================*/ 


  /*================= LISTA DE PARTIDAS PROGRAMADAS ============================*/ 
  public function lista_partidas($mod,$fase_id,$proy_id)
  {
    $enlaces=$this->menu_modelo->get_Modulos_programacion(1);
    $data['enlaces'] = $enlaces;
    $data['nro_fase']= $this->model_faseetapa->nro_fase($proy_id); /// nro de fases y etapas registrados
    $data['proyecto'] = $this->model_proyecto->get_id_proyecto($proy_id); ////// datos generales del proyecto con id_p x
    $data['id_f'] = $this->model_faseetapa->get_id_fase($proy_id); //// recupera datos de la tabla fase activa
    $data['mod']=$mod;
    $titulo_proy=strtoupper($data['proyecto'][0]['tipo']);
    $data['titulo_proy'] = $titulo_proy;

    // echo  $this->delete_partidas($proy_id,$this->session->userdata("gestion")); //// Limpia Tablas Partidas
      if($mod==1){
          if($this->delete_partidas($proy_id,$this->session->userdata("gestion"))){
              $this->actualiza_partidas($proy_id,$this->session->userdata("gestion"),$data['id_f'][0]['pfec_ejecucion']); //// Actualiza Tablas Partidas
              $data['partidas']=$this->mis_partidas($mod,$proy_id,$data['id_f'][0]['id'],$this->session->userdata("gestion")); //// Mis Partidas
              $this->load->view('admin/programacion/ejecucion/financiera/list_partidas', $data);
          }
          else{
              echo "ERROR AL LIMIPIAR LAS TABLAS";
          }
      }elseif ($mod==4){
          $part_gest='';
          $fase = $this->model_faseetapa->get_id_fase($proy_id);
          $anios=$fase[0]['pfec_fecha_fin']-$fase[0]['pfec_fecha_inicio']+1;
          for($k=1;$k<=$anios;$k++)
          {
              if($this->delete_partidas($proy_id,$fase[0]['pfec_fecha_inicio'])){
                  $this->actualiza_partidas($proy_id,$fase[0]['pfec_fecha_inicio'],$data['id_f'][0]['pfec_ejecucion']); //// Actualiza Tablas Partidas
              }
              $fase[0]['pfec_fecha_inicio']++;
          }$part_gest.=$this->mis_partidas_m($mod,$proy_id,$data['id_f'][0]['id']);
          $data['partidas']=$part_gest;
          $this->load->view('admin/programacion/ejecucion/financiera/list_partidas_m', $data);
      }

  }

  /*-------------------- EJECUTAR PARTIDAS ------------------------*/
  public function ejecutar_partida($mod,$proy_id,$fase_id,$pr_id)
  {
    $enlaces=$this->menu_modelo->get_Modulos_programacion(1);
    $data['enlaces'] = $enlaces;
    $data['nro_fase']= $this->model_faseetapa->nro_fase($proy_id); /// nro de fases y etapas registrados
    $data['proyecto'] = $this->model_proyecto->get_id_proyecto($proy_id); ////// datos generales del proyecto con id_p x
    $data['id_f'] = $this->model_faseetapa->get_id_fase($proy_id); //// recupera datos de la tabla fase activa
    $data['mod']=$mod;
    $data['gestion']=$this->session->userdata("gestion");

      $titulo_proy=strtoupper($data['proyecto'][0]['tipo']);
    $data['titulo_proy'] = $titulo_proy;

    $data['ppartida'] = $this->mfinanciera->get_partida_programado($pr_id,$this->session->userdata("gestion")); //// Datos de la partida programada
    $m[1]='enero';
    $m[2]='febrero';
    $m[3]='marzo';
    $m[4]='abril';
    $m[5]='mayo';
    $m[6]='junio';
    $m[7]='julio';
    $m[8]='agosto';
    $m[9]='septiembre';
    $m[10]='octubre';
    $m[11]='noviembre';
    $m[12]='diciembre';

    $epartida= $this->mfinanciera->get_partida_proy_ejecutado($pr_id,$this->session->userdata("gestion"));
    if(count($epartida)!=0){
      for ($i=1; $i <=12 ; $i++) { 
        $pe[$i]=$epartida[0][$m[$i]];
      }
    }
    else{
      for ($i=1; $i <=12 ; $i++) { 
        $pe[$i]=0;
      }
    }

    $data['ejec']=$pe;

    $this->load->view('admin/programacion/ejecucion/financiera/ejec_partida', $data);
   
  }
  public function ejecutar_partida_m($mod,$proy_id,$fase_id,$pr_id,$gestion)
    {
        $enlaces=$this->menu_modelo->get_Modulos_programacion(1);
        $data['enlaces'] = $enlaces;
        $data['nro_fase']= $this->model_faseetapa->nro_fase($proy_id); /// nro de fases y etapas registrados
        $data['proyecto'] = $this->model_proyecto->get_id_proyecto($proy_id); ////// datos generales del proyecto con id_p x
        $data['id_f'] = $this->model_faseetapa->get_id_fase($proy_id); //// recupera datos de la tabla fase activa
        $data['mod']=$mod;
        $data['gestion']=$gestion;
        $titulo_proy=strtoupper($data['proyecto'][0]['tipo']);
        $data['titulo_proy'] = $titulo_proy;

        $data['ppartida'] = $this->mfinanciera->get_partida_programado($pr_id,$gestion); //// Datos de la partida programada
        $m[1]='enero';
        $m[2]='febrero';
        $m[3]='marzo';
        $m[4]='abril';
        $m[5]='mayo';
        $m[6]='junio';
        $m[7]='julio';
        $m[8]='agosto';
        $m[9]='septiembre';
        $m[10]='octubre';
        $m[11]='noviembre';
        $m[12]='diciembre';

        $epartida= $this->mfinanciera->get_partida_proy_ejecutado($pr_id,$gestion);
        if(count($epartida)!=0){
            for ($i=1; $i <=12 ; $i++) {
                $pe[$i]=$epartida[0][$m[$i]];
            }
        }
        else{
            for ($i=1; $i <=12 ; $i++) {
                $pe[$i]=0;
            }
        }

        $data['ejec']=$pe;

        $this->load->view('admin/programacion/ejecucion/financiera/ejec_partida', $data);

    }



  /*------------------------------- LISTA DE PARTIDAS ------------------------------*/
  function mis_partidas($mod,$proy_id,$pfec_id,$gestion){
    $proyecto_programado= $this->mfinanciera->lista_partidas_proy_programado($proy_id,$gestion);
    $tabla ='';
    $tabla .='<tabla>';
    $nro=0;
    foreach ($proyecto_programado as $pp) {
      $nro++;
      $tabla .='<tr>';
      $tabla .='<td>'.$nro.'</td>';
      $tabla .= '<td>
                    <a href="' . site_url("admin") . '/prog/ejec_partida/' . $mod . '/' . $proy_id . '/' . $pfec_id . '/'. $pp['pr_id'] .'" class="btn btn-lg btn-primary" title="EJECUTAR PROGRAMACION PRESUPUESTARIA">
                        <i class="glyphicon glyphicon-usd"></i>
                    </a>
                </td>';
      $tabla .='<td>'.$pp['par_codigo'].'</td>';
      $tabla .='<td>'.$pp['ff_id'].'</td>';
      $tabla .='<td>'.$pp['of_id'].'</td>';
      $tabla .='<td>';
        $tabla .='<table class="table table-bordered">';
        $tabla .='<tr>';
          $tabla .='<thead>';
          $tabla .='<th>P/E</th>';
          $tabla .='<th>ENE.</th>';
          $tabla .='<th>FEB.</th>';
          $tabla .='<th>MAR.</th>';
          $tabla .='<th>ABR.</th>';
          $tabla .='<th>MAY.</th>';
          $tabla .='<th>JUN.</th>';
          $tabla .='<th>JUL.</th>';
          $tabla .='<th>AGOST.</th>';
          $tabla .='<th>SEPT.</th>';
          $tabla .='<th>OCT.</th>';
          $tabla .='<th>NOV.</th>';
          $tabla .='<th>DIC.</th>';
          $tabla .='</thead>';
        $tabla .='</tr>';
        $tabla .='<tr>';
          $tabla .='<td>PROGRAMADO</td>';
          $tabla .='<td>'.number_format($pp['enero'], 2, ',', '.').'</td>';
          $tabla .='<td>'.number_format($pp['febrero'], 2, ',', '.').'</td>';
          $tabla .='<td>'.number_format($pp['marzo'], 2, ',', '.').'</td>';
          $tabla .='<td>'.number_format($pp['abril'], 2, ',', '.').'</td>';
          $tabla .='<td>'.number_format($pp['mayo'], 2, ',', '.').'</td>';
          $tabla .='<td>'.number_format($pp['junio'], 2, ',', '.').'</td>';
          $tabla .='<td>'.number_format($pp['julio'], 2, ',', '.').'</td>';
          $tabla .='<td>'.number_format($pp['agosto'], 2, ',', '.').'</td>';
          $tabla .='<td>'.number_format($pp['septiembre'], 2, ',', '.').'</td>';
          $tabla .='<td>'.number_format($pp['octubre'], 2, ',', '.').'</td>';
          $tabla .='<td>'.number_format($pp['noviembre'], 2, ',', '.').'</td>';
          $tabla .='<td>'.number_format($pp['diciembre'], 2, ',', '.').'</td>';
        $tabla .='</tr>';
        $pe= $this->mfinanciera->get_partida_proy_ejecutado($pp['pr_id'],$gestion);
        if(count($pe)!=0){
          $tabla .='<tr style="background-color: #09B043; color: #ffffff;">';
            $tabla .='<td>EJECUTADO</td>';
            $tabla .='<td>'.number_format($pe[0]['enero'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pe[0]['febrero'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pe[0]['marzo'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pe[0]['abril'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pe[0]['mayo'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pe[0]['junio'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pe[0]['julio'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pe[0]['agosto'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pe[0]['septiembre'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pe[0]['octubre'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pe[0]['noviembre'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pe[0]['diciembre'], 2, ',', '.').'</td>';
          $tabla .='</tr>';
        }
        $tabla .='</table>';
      $tabla .='</td>';
      $tabla .='</tr>';
      $tabla .='</tabla>';
    }

    return $tabla;
  }
  function mis_partidas_m($mod,$proy_id,$pfec_id){
        $proyecto_programado= $this->mfinanciera->lista_partidas_x_proy_programado($proy_id);
        $tabla ='';
        $tabla .='<tabla>';
        $nro=0;
        foreach ($proyecto_programado as $pp) {
            $nro++;
            $tabla .='<tr>';
            $tabla .='<td>'.$nro.'</td>';
            $tabla .= '<td>
                    <a href="' . site_url("admin") . '/prog/ejec_partida_m/' . $mod . '/' . $proy_id . '/' . $pfec_id . '/'. $pp['pr_id'] .'/'.$pp['gestion'].'" class="btn btn-lg btn-primary" title="EJECUTAR PROGRAMACION PRESUPUESTARIA - '. $pp['gestion'].'">
                    <i class="glyphicon glyphicon-usd"></i>
                    </a>
                </td>';
            $tabla .='<td>'.$pp['par_codigo'].'</td>';
            $tabla .='<td>'.$pp['ff_id'].'</td>';
            $tabla .='<td>'.$pp['of_id'].'</td>';
            $tabla .='<td>';
            $tabla .='<table class="table table-bordered">';
            $tabla .='<tr>';
            $tabla .='<thead>';
            $tabla .='<th colspan="13">PROGRAMACI&Oacute;N GESTI&Oacute;N - '.$pp['gestion'].' </th>';
            $tabla .='</thead>';

            $tabla .='<thead>';
            $tabla .='<th>P/E</th>';
            $tabla .='<th>ENE.</th>';
            $tabla .='<th>FEB.</th>';
            $tabla .='<th>MAR.</th>';
            $tabla .='<th>ABR.</th>';
            $tabla .='<th>MAY.</th>';
            $tabla .='<th>JUN.</th>';
            $tabla .='<th>JUL.</th>';
            $tabla .='<th>AGOST.</th>';
            $tabla .='<th>SEPT.</th>';
            $tabla .='<th>OCT.</th>';
            $tabla .='<th>NOV.</th>';
            $tabla .='<th>DIC.</th>';
            $tabla .='</thead>';
            $tabla .='</tr>';
            $tabla .='<tr>';
            $tabla .='<td>PROGRAMADO</td>';
            $tabla .='<td>'.number_format($pp['enero'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pp['febrero'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pp['marzo'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pp['abril'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pp['mayo'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pp['junio'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pp['julio'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pp['agosto'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pp['septiembre'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pp['octubre'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pp['noviembre'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pp['diciembre'], 2, ',', '.').'</td>';
            $tabla .='</tr>';
            $pe= $this->mfinanciera->get_partida_proy_ejecutado($pp['pr_id'],$pp['gestion']);
            if(count($pe)!=0){
                $tabla .='<tr style="background-color: #09B043; color: #ffffff;">';
                $tabla .='<td>EJECUTADO</td>';
                $tabla .='<td>'.number_format($pe[0]['enero'], 2, ',', '.').'</td>';
                $tabla .='<td>'.number_format($pe[0]['febrero'], 2, ',', '.').'</td>';
                $tabla .='<td>'.number_format($pe[0]['marzo'], 2, ',', '.').'</td>';
                $tabla .='<td>'.number_format($pe[0]['abril'], 2, ',', '.').'</td>';
                $tabla .='<td>'.number_format($pe[0]['mayo'], 2, ',', '.').'</td>';
                $tabla .='<td>'.number_format($pe[0]['junio'], 2, ',', '.').'</td>';
                $tabla .='<td>'.number_format($pe[0]['julio'], 2, ',', '.').'</td>';
                $tabla .='<td>'.number_format($pe[0]['agosto'], 2, ',', '.').'</td>';
                $tabla .='<td>'.number_format($pe[0]['septiembre'], 2, ',', '.').'</td>';
                $tabla .='<td>'.number_format($pe[0]['octubre'], 2, ',', '.').'</td>';
                $tabla .='<td>'.number_format($pe[0]['noviembre'], 2, ',', '.').'</td>';
                $tabla .='<td>'.number_format($pe[0]['diciembre'], 2, ',', '.').'</td>';
                $tabla .='</tr>';
            }
            $tabla .='</tabla>';
            $tabla .='</td>';
            $tabla .='</tr>';
            $tabla .='</table>';
        }

        return $tabla;
    }

   public function add_ejecucion()
    { //echo $this->input->post('file1');
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {
          $this->form_validation->set_rules('proy_id', 'id de proyecto', 'required|trim');
          $this->form_validation->set_rules('fase_id', 'id de Fase', 'required|trim');
          $this->form_validation->set_rules('pr_id', 'id programado', 'required|trim');
          $this->form_validation->set_rules('gestion', 'Gestión', 'required|trim');
          //$this->form_validation->set_rules('file1', 'Seleccione Archivo', 'required|trim');
              $m[1]='m1';
              $m[2]='m2';
              $m[3]='m3';
              $m[4]='m4';
              $m[5]='m5';
              $m[6]='m6';
              $m[7]='m7';
              $m[8]='m8';
              $m[9]='m9';
              $m[10]='m10';
              $m[11]='m11';
              $m[12]='m12';

              $mes[1]='enero';
              $mes[2]='febrero';
              $mes[3]='marzo';
              $mes[4]='abril';
              $mes[5]='mayo';
              $mes[6]='junio';
              $mes[7]='julio';
              $mes[8]='agosto';
              $mes[9]='septiembre';
              $mes[10]='octubre';
              $mes[11]='noviembre';
              $mes[12]='diciembre';
          if($this->form_validation->run())
          {
            $epartida= $this->mfinanciera->get_partida_proy_ejecutado($this->input->post('pr_id'),$this->input->post('gestion'));
            for ($i=1; $i <=12 ; $i++) { 
              if($this->input->post($m[$i])!=0 & $this->input->post($m[$i])!=''){
                
               // echo "Mes ".$i." - ".$this->input->post($m[$i])." - ".$epartida[0][$mes[$i]]."<br>";
                $monto=$this->input->post($m[$i])-$epartida[0][$mes[$i]];
                if($this->input->post($m[$i])!=$epartida[0][$mes[$i]]){
                  $data_to_store = array( 
                  'proy_id' => $this->input->post('proy_id'),
                  'par_id' => $this->input->post('par_id'),
                  'ff_id' => $this->input->post('ff_id'),
                  'of_id' => $this->input->post('of_id'),
                  'pem_devengado' => $monto,
                  'mes_id' => $i,
                  'gestion' => $this->input->post('gestion'),
                  'fun_id' => $this->session->userdata("fun_id"),
                  'pem_ppto_inicial' =>0,
                  'pem_ppto_vigente' =>0,
                  'pem_modif_aprobadas' =>0,
                  );
                  $this->db->insert('proy_ejec_mes', $data_to_store); 
                }
              }
            }
            $this->session->set_flashdata('success','SE GUARDO CORRECTAMENTE EL REGISTRO DE LA EJECUCIÓN');
            redirect('admin/prog/efinanciero/'.$this->input->post('mod').'/'.$this->input->post('fase_id').'/'.$this->input->post('proy_id').'/true');
          }
          else{
            $this->session->set_flashdata('danger','Error al Guardar la Ejecucion');
            redirect('admin/prog/ejec_partida/'.$this->input->post('mod').'/'.$this->input->post('proy_id').'/'.$this->input->post('fase_id').'/'.$this->input->post('pr_id').'/false');
          }
          }
        else{
          $this->session->set_flashdata('danger','Error al Guardar');
          redirect('admin/prog/ejec_partida/'.$this->input->post('mod').'/'.$this->input->post('proy_id').'/'.$this->input->post('fase_id').'/'.$this->input->post('pr_id').'/false');
        }
    }

  function delete_partidas($proy_id,$gestion){
    $proyecto_programado= $this->mfinanciera->lista_partidas_proy_programado($proy_id,$gestion);
    foreach ($proyecto_programado as $pp) {
      
    //  echo "PROGRAMADO : PAR ID : ".$pp['par_id']."<br>";
      $proyecto_ejecutado= $this->mfinanciera->get_partida_proy_ejecutado($pp['pr_id'],$gestion);
      foreach ($proyecto_ejecutado as $pe) {
          $this->db->where('pr_id', $pp['pr_id']);
          $this->db->delete('proyecto_ejecutado_partidas');/*
          echo "EJECUTADO : PAR ID : ".$pe['par_id']."<br>";*/
      }
        $this->db->where('pr_id', $pp['pr_id']);
        $this->db->delete('proyecto_programado_partidas');
    }

    $proyecto_programado= $this->mfinanciera->lista_partidas_proy_programado($proy_id,$gestion);
    if(count($proyecto_programado)==0){
      return true;
    }
    else{
      return false;
    }
  }

  function actualiza_partidas($proy_id,$gestion,$tp_ejec){
    $partidas_prog= $this->mfinanciera->vlista_partidas_proy_programado($proy_id,$gestion,$tp_ejec);
    foreach ($partidas_prog as $pp) {
      
      $pr_id=$this->mfinanciera->storage_partidas($proy_id,$gestion,$pp['partida_id'],$pp['partida_codigo'],$pp['ffid'],$pp['ofid'],$pp['etid'],$pp['total']
                                          ,$pp['enero'],$pp['febrero'],$pp['marzo'],$pp['abril'],$pp['mayo'],$pp['junio'],$pp['julio'],$pp['agosto'],$pp['septiembre'],$pp['octubre'],$pp['noviembre'],$pp['diciembre']);

      if($pr_id!=0 || $pr_id!='')
      {
          $partidas_ejec= $this->mfinanciera->vlista_partidas_proy_ejecutado($proy_id,$gestion);
          if(count($partidas_ejec)!=0){

            foreach ($partidas_ejec as $pe) {
              if(($pp['partida_id']==$pe['par_id'])&($pp['ffid']==$pe['ff_id'])&($pp['ofid']==$pe['of_id'])){
              //  echo "ENCONTRADO : ".$pp['partida_codigo']."-".$pp['partida_id']."-".$pp['ffid']."-".$pp['ofid']."<br>";
                
                $pe_id=$this->mfinanciera->storage_partidas_ejec($pr_id,$gestion,$pe['par_id'],$pe['ff_id'],$pe['of_id']
                                              ,$pe['ejec1'],$pe['ejec2'],$pe['ejec3'],$pe['ejec4'],$pe['ejec5'],$pe['ejec6'],$pe['ejec7'],$pe['ejec8'],$pe['ejec9'],$pe['ejec10'],$pe['ejec11'],$pe['ejec12']);
                
                if($pe_id!=0 || $pe_id!=''){
                  $this->session->set_flashdata('success','LA PROGRAMACI&Oacute;N Y EJECUCI&Oacute;N DE PARTIDAS SE ACTUALIZARON CORRECTAMENTE');
                  //echo "se registro correctamente programado y ejecutado";
                }
                else{
                  $this->session->set_flashdata('danger','NOSE PUEDO ACTULIZAR CORRECTAMENTE LA EJECUCI&Oacute;N POR PARTIDAS');
                  //echo "Error en el registro Ejecutado";
                }
              }
              
            }

          }
      }
      else{
        $this->session->set_flashdata('danger','NOSE PUEDO ACTUALIZAR CORRECTAMENTE LA PROGRAMACI&Oacute;N POR PARTIDAS');
        //echo "error al guardar programado";
      }
      
    }
  }

  /*================================================================================================*/ 
  /*=================================== RUTA DE EDITADO PROYECTO ==============================================*/
  public  function ruta_edit_proy($mod,$id_proy,$form)
  {
    if($mod!='' & $id_proy!='' & $form!=''){    
        $enlaces=$this->menu_modelo->get_Modulos_programacion(1);
        $data['enlaces'] = $enlaces;

        $data['nro_fase']= $this->model_faseetapa->nro_fase($id_proy); /// nro de fases y etapas registrados
        $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_proy); ////// datos generales del proyecto con id_p x
        $data['id_f'] = $this->model_faseetapa->get_id_fase($id_proy); //// recupera datos de la tabla fase activa
        $data['mod']=$mod;

        if($form==1) /// formulario de editado Datos Generales
        { 
          $data['aper'] = $this->model_proyecto->apertura_p($this->session->userdata("gestion"),$data['proyecto'][0]['aper_programa'],'0000','000');
          $this->load->view('admin/programacion/datos_generales/generales/cgform1', $data);  
        }
        elseif ($form==2) {
          $data['fun1']=$this->model_proyecto->asig_responsables(3); ////// tecnico OPERATIVO
          $data['fun2']=$this->model_proyecto->asig_responsables(4); ////// validador POA
          $data['fun3']=$this->model_proyecto->asig_responsables(5); ////// validador FINANCIERO

          $data['resp1']=$this->model_proyecto->responsable_proy($id_proy,'1');
          $data['resp2']=$this->model_proyecto->responsable_proy($id_proy,'2');
          $data['resp3']=$this->model_proyecto->responsable_proy($id_proy,'3');
          $data['unidad']=$this->model_proyecto->list_unidad_org(); ////// Unidad Ejecutora

          $this->load->view('admin/programacion/datos_generales/generales/cgform2', $data);  
        }
        elseif ($form==3) {
          $data['pdes'] = $this->model_proyecto->datos_pedes($data['proyecto'][0]['pdes_id']);
          $data['ptdi'] = $this->model_proyecto->datos_ptdi($data['proyecto'][0]['ptdi_id']);
          $data['lista_ptdi']=$this->model_proyecto->lista_ptdi();

          if(count($this->model_proyecto->codigo_sectorial($data['proyecto'][0]['codsectorial'],3))!=0){
            $nivel=3;
          }
          elseif(count($this->model_proyecto->codigo_sectorial($data['proyecto'][0]['codsectorial'],2))!=0){ 
            $nivel=2;
          }
          elseif(count($this->model_proyecto->codigo_sectorial($data['proyecto'][0]['codsectorial'],2))==0){ 
            $nivel=2;
          }
          

          $data['codsec'] = $this->model_proyecto->codigo_sectorial($data['proyecto'][0]['codsectorial'],$nivel);
          $data['fifu'] = $this->model_proyecto->datos_finalidad($data['proyecto'][0]['fifu_id']);
          $fifu=$this->model_proyecto->get_fifu($data['proyecto'][0]['fifu_id']);
          if($fifu[0]['fifu_nivel']=='Clase'){
            $data['tfifu']=1;
          }
          elseif($fifu[0]['fifu_nivel']=='Funcion'){
            $data['tfifu']=0; 
          }
          $this->load->view('admin/programacion/datos_generales/generales/cgform4', $data);
        }
        elseif ($form==4) {
          $this->load->view('admin/programacion/datos_generales/generales/cgform3', $data);   
        }
        elseif ($form==5) {
          $this->load->view('admin/programacion/datos_generales/generales/cgform5', $data);   
        }
        elseif ($form==7) {
          $this->load->view('admin/programacion/datos_generales/generales/cgform7', $data);   
        }
        elseif ($form==8) {
          $data['arch']=$this->model_proyecto->get_archivos($id_proy);
          $this->load->view('admin/programacion/datos_generales/generales/cgform8', $data);   
        }
        elseif ($form==9) {
          $data['fases'] = $this->model_faseetapa->fase_etapa_proy($id_proy);
          
          if($data['proyecto'][0]['tp_id']==1)
          {
            $this->load->view('admin/programacion/datos_generales/generales/cgform9', $data);
          }
          elseif($data['proyecto'][0]['tp_id']==2 || $data['proyecto'][0]['tp_id']==3 || $data['proyecto'][0]['tp_id']==4)
          {
          $data['fase_proyecto'] = $this->model_faseetapa->get_id_fase($id_proy); 
          $datos = $this->model_faseetapa->datos_fase_etapa($data['id_f'][0]['id'],$id_proy);  // desvuelve las fechas del proyecto
          $años=$datos[0]['final']-$datos[0]['actual']+1;
          $data['gestiones']=$años;
          $fecha=$datos[0]['actual'];
          $data['fecha']=$fecha;

          if($años>=1)
          {
            $data['fase_gestion1']=$this->model_faseetapa->fase_etapa_gestion($data['id_f'][0]['id'],$fecha);
            if($años>=2)
            {
              $data['fase_gestion2']=$this->model_faseetapa->fase_etapa_gestion($data['id_f'][0]['id'],$fecha+1);
              if($años>=3)
              {
                $data['fase_gestion3']=$this->model_faseetapa->fase_etapa_gestion($data['id_f'][0]['id'],$fecha+2);
                if($años>=4)
                {
                  $data['fase_gestion4']=$this->model_faseetapa->fase_etapa_gestion($data['id_f'][0]['id'],$fecha+3);
                  if($años>=5)
                  {
                    $data['fase_gestion5']=$this->model_faseetapa->fase_etapa_gestion($data['id_f'][0]['id'],$fecha+4);
                    if($años>=6)
                    {
                      $data['fase_gestion6']=$this->model_faseetapa->fase_etapa_gestion($data['id_f'][0]['id'],$fecha+5);
                      if($años>=7)
                      {
                        $data['fase_gestion7']=$this->model_faseetapa->fase_etapa_gestion($data['id_f'][0]['id'],$fecha+6);
                        if($años>=8)
                        {
                          $data['fase_gestion8']=$this->model_faseetapa->fase_etapa_gestion($data['id_f'][0]['id'],$fecha+7);
                          if($años>=9)
                          {
                            $data['fase_gestion9']=$this->model_faseetapa->fase_etapa_gestion($data['id_f'][0]['id'],$fecha+8);
                            if($años>=10)
                            {
                              $data['fase_gestion10']=$this->model_faseetapa->fase_etapa_gestion($data['id_f'][0]['id'],$fecha+9);
                              if($años>=11)
                              {
                                $data['fase_gestion11']=$this->model_faseetapa->fase_etapa_gestion($data['id_f'][0]['id'],$fecha+10);
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
            $this->load->view('admin/programacion/datos_generales/generales/cgform9_up2', $data);
          }
            
        }
        elseif ($form==10) {
          $data['metas'] = $this->model_proyecto->metas_p($id_proy); ///// metas del proyecto
          $this->load->view('admin/programacion/datos_generales/generales/metas_edit', $data); 
        }
    } 
    else{
        redirect('admin/dashboard');
    } 
  }


  /*====================================== VALIDAR MODIFICADO ===============================================*/
  public function actualizar_datos()
  {
        if ($this->input->post('form')=='1' && $this->input->server('REQUEST_METHOD') === 'POST') ///// DATOS GENERALES
        {
            $this->form_validation->set_rules('id', 'Id Proyecto', 'required|trim');
            $this->form_validation->set_rules('nom_proy', 'Proyecto , Actividad', 'required|trim');

            if ($this->form_validation->run()) ///// Al no existir error guarda la informacion en la BD 
            {
                $update_proy = array('proy_nombre' => strtoupper($this->input->post('nom_proy')),
                                    'proy_sisin' => $this->input->post('cod_sisin'),
                                    'fun_id' => $this->session->userdata("fun_id"),
                                    'proy_fecha_registro' => date('d/m/Y h:i:s'),
                                    'estado' => '2');

                        $this->db->where('proy_id', $this->input->post('id'));
                        $this->db->update('_proyectos', $update_proy);

                        $id_aper = $this->model_proyecto->aper_id($this->input->post('id'),$this->session->userdata("gestion"));  // devuelve aper_id

                        $update_aper = array(
                                    'aper_descripcion' => strtoupper($this->input->post('nom_proy')));

                        $this->db->where('aper_id', $id_aper[0]['aper_id']);
                        $this->db->update('aperturaprogramatica', $update_aper);

                if($this->input->post('tp')==2 || $this->input->post('tp')==3 || $this->input->post('tp')==4)
                  {
                    $update_fase = array('pfec_ptto_fase' => $this->input->post('mtotal'));

                    $this->db->where('proy_id', $this->input->post('id'));
                    $this->db->update('_proyectofaseetapacomponente', $update_fase);
                  }

                  redirect('admin/proy/edit_prog/'.$this->input->post('mod').'/'.$this->input->post('id').'/10');       
            }
            else
            {
              redirect('admin/proy/edit_prog/'.$this->input->post('mod').'/'.$this->input->post('id').'/1');   
            }

        }
        elseif($this->input->post('form')=='2' && $this->input->server('REQUEST_METHOD') === 'POST') ///// RESPONSABLES
        { 
            $this->form_validation->set_rules('fun_id_1', 'Tecnico Operativo', 'required|trim');
            $this->form_validation->set_rules('fun_id_2', 'Validador POA', 'required|trim');
            $this->form_validation->set_rules('fun_id_3', 'Validador Financiero', 'required|trim');
            $this->form_validation->set_rules('uni_ejec', 'unidad ejecutora', 'required|trim');
            $this->form_validation->set_rules('uni_ejec2', 'unidad responsable', 'required|trim');

            if ($this->form_validation->run()) ///// Al no existir error guarda la informacion en la BD 
            {
                $this->model_proyecto->update_resp_proy($this->input->post('id'),$this->input->post('fun_id_1'),$this->input->post('fun_id_2'),$this->input->post('fun_id_3'),$this->input->post('uni_ejec'),$this->input->post('uni_ejec2'));
                  $proy = $this->model_proyecto->get_id_proyecto($this->input->post('id'));
                  /*====================================  ÀPERTURA PROGRAMATICA  ========================*/
                  $update_aper = array('uni_id' => $this->input->post('uni_ejec'));
                  $this->db->where('aper_id', $proy[0]['aper_id']);
                  $this->db->update('aperturaprogramatica', $update_aper);
                  
                  redirect('admin/proy/edit_prog/'.$this->input->post('mod').'/'.$this->input->post('id').'/3'); 
            }
            else
            {
              redirect('admin/proy/edit_prog/'.$this->input->post('mod').'/'.$this->input->post('id').'/2/false');  
            }
        }
        elseif($this->input->post('form')=='3' && $this->input->server('REQUEST_METHOD') === 'POST') ///// CLASIFICACION
        {
            $this->form_validation->set_rules('clasificador3', 'Seleccione Sector', 'required|trim');
            $this->form_validation->set_rules('finalidad1', 'Seleccione Finalidad', 'required|trim');
            $this->form_validation->set_rules('pedes4', 'Seleccione Resultado', 'required|trim');

            if ($this->form_validation->run())
            {
                 if($this->input->post('clase')!=''){$clase= $this->input->post('clase');}
                  elseif ($this->input->post('clase')=='')  {$clase=$this->input->post('fun');}

                  if($this->input->post('pedes4')==""){$id_pdes = $this->model_proyecto->get_id_pdes($this->input->post('pedes3'));}   // devuelve id pedes  
                  elseif ($this->input->post('pedes4')!="") {$id_pdes = $this->model_proyecto->get_id_pdes($this->input->post('pedes4')); }  // devuelve id pedes  

                  $id_ptdi = $this->model_proyecto->get_id_ptdi($this->input->post('ptdi3'));  // devuelve id pedes 

                  $update_proy = array('proy_fecha_registro' => date('d/m/Y h:i:s'),
                                       'codsectorial' => $this->input->post('clasificador3'),
                                       'pdes_id' => $id_pdes[0]['pdes_id'],
                                       'ptdi_id' => $id_ptdi[0]['ptdi_id'],
                                       'fifu_id' => $clase);

                        $this->db->where('proy_id', $this->input->post('id'));
                        $this->db->update('_proyectos', $update_proy);

                  redirect('admin/proy/edit_prog/'.$this->input->post('mod').'/'.$this->input->post('id').'/4?qRegistro='.$this->input->post('id')); 
            }
            else
            {
                  redirect('admin/proy/edit_prog/'.$this->input->post('mod').'/'.$this->input->post('id').'/3');
            }
        }
        elseif($this->input->post('form')=='4' && $this->input->server('REQUEST_METHOD') === 'POST') ///// LOCALIZACION
        {

            $this->form_validation->set_rules('area_id', 'Area de Influencia', 'required|trim');
            $this->form_validation->set_rules('pob_c', 'Poblacion censo', 'required|trim');
            $this->form_validation->set_rules('num_pob', 'numero de poblacion beneficiada', 'required|trim');
            $this->form_validation->set_rules('porc_pob', 'porcentaje de poblacion beneficiada', 'required|trim');

            if ($this->form_validation->run()) ///// Al no existir error guarda la informacion en la BD 
            {
                      $update_proy = array('proy_fecha_registro' => date('d/m/Y h:i:s'),
                                      'proy_poblac_beneficiaria' => $this->input->post('pob_bene'),
                                      'area_id' => $this->input->post('area_id'),
                                      'proy_poblac_beneficiada' => $this->input->post('num_pob'),
                                      'proy_porcent_poblac_beneficiada' => $this->input->post('porc_pob'),
                                      'proy_nro_hombres' => $this->input->post('nro_h'),
                                      'proy_nro_mujeres' => $this->input->post('nro_m'));

                        $this->db->where('proy_id', $this->input->post('id'));
                        $this->db->update('_proyectos', $update_proy);

                   redirect('admin/proy/edit_prog/'.$this->input->post('mod').'/'.$this->input->post('id').'/5');      
            }
            else
            {
                  redirect('admin/proy/edit_prog/'.$this->input->post('mod').'/'.$this->input->post('id').'/4?qRegistro='.$this->input->post('id')); 
            }
        }


        elseif($this->input->post('form')=='5' && $this->input->server('REQUEST_METHOD') === 'POST') ///// MARCO LOGICO
        {
            $this->form_validation->set_rules('desc_prob', 'Descripcion del Problema', 'required|trim');
            $this->form_validation->set_rules('desc_sol', 'Descripcion de la Solucion', 'required|trim');
            if ($this->form_validation->run())
            {
                  $update_proy = array('proy_fecha_registro' => date('d/m/Y h:i:s'),
                                      'proy_desc_problema' => strtoupper($this->input->post('desc_prob')), 
                                       'proy_desc_solucion' => strtoupper($this->input->post('desc_sol')),
                                       'proy_obj_general' => strtoupper($this->input->post('obj_gral')),
                                       'proy_obj_especifico' => strtoupper($this->input->post('obj_esp')));

                        $this->db->where('proy_id', $this->input->post('id'));
                        $this->db->update('_proyectos', $update_proy);
                  
                  redirect('admin/proy/edit_prog/'.$this->input->post('mod').'/'.$this->input->post('id').'/7');
            }
            else
            {
                  redirect('admin/proy/edit_prog/'.$this->input->post('mod').'/'.$this->input->post('id').'/5');
            }
        }

        elseif($this->input->post('form')=='7' && $this->input->server('REQUEST_METHOD') === 'POST') ///// RESUMEN TECNICO
        {
            $this->form_validation->set_rules('resumen', 'Resumen Tecnico del proyecto', 'required|trim');

            if ($this->form_validation->run())
            {
                  $update_proy = array('proy_fecha_registro' => date('d/m/Y h:i:s'),
                                        'proy_descripcion_proyecto' => strtoupper($this->input->post('resumen')));

                        $this->db->where('proy_id', $this->input->post('id'));
                        $this->db->update('_proyectos', $update_proy);
                  
                  redirect('admin/proy/edit_prog/'.$this->input->post('mod').'/'.$this->input->post('id').'/8');
            }
            else
            {
                  redirect('admin/proy/edit_prog/'.$this->input->post('mod').'/'.$this->input->post('id').'/7');
            }
        }
  }

    /*====================================== ACTUALIZAR FASE ETAPA - DATOS GENERALES ===============================================*/
    function modificar_fase($mod,$id_f,$id_p,$form)
    {
      if($mod!='' & $id_f!='' & $id_p!='' & $form!=''){  
          $enlaces=$this->menu_modelo->get_Modulos_programacion(1);
          $data['enlaces'] = $enlaces;
          $data['id_f'] = $this->model_faseetapa->get_id_fase($id_p); //// recupera datos de la tabla fase activa
          $data['nro_fase']= $this->model_faseetapa->nro_fase($id_p); /// nro de fases y etapas registrados
          $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p); 
          $data['fase_proyecto'] = $this->model_faseetapa->fase_etapa($id_f,$id_p); 
          $datos = $this->model_faseetapa->datos_fase_etapa($id_f,$id_p);  // desvuelve las fechas del proyecto
          $data['nro_fg'] = $this->model_faseetapa->nro_fasegestion($id_f);
          $data['nro_fg_act'] = $this->model_faseetapa->nro_fasegestion_actual($id_f,$this->session->userdata('gestion')); 
          $data['fi']=$datos[0]['actual'];
          $data['ff']=$datos[0]['final'];
          $data['mod']=$mod;
          //load the view
          if($form==1)
          {
            $data['fases'] = $this->model_faseetapa->fases(); //// lista de Fases
            $data['unidad_org'] = $this->model_proyecto->list_unidad_org(); //// unidad organizacional
            $data['f_top'] = $this->model_proyecto->responsable_proy($id_p,'1'); //// unidad ejecutora
            $data['main_content'] = 'admin/programacion/datos_generales/generales/cgform9_up';
            $this->load->view('admin/programacion/datos_generales/generales/cgform9_up', $data);
          }
          elseif($form==2)
          {

            $años=$datos[0]['final']-$datos[0]['actual']+1;
            $data['gestiones']=$años;
            $fecha=$datos[0]['actual'];
            $data['fecha']=$fecha;

            if($años>=1)
            {
              $data['fase_gestion1']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha);
              if($años>=2)
              {
                $data['fase_gestion2']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+1);
                if($años>=3)
                {
                  $data['fase_gestion3']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+2);
                  if($años>=4)
                  {
                    $data['fase_gestion4']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+3);
                    if($años>=5)
                    {
                      $data['fase_gestion5']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+4);
                      if($años>=6)
                      {
                        $data['fase_gestion6']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+5);
                        if($años>=7)
                        {
                          $data['fase_gestion7']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+6);
                           if($años>=8)
                          {
                            $data['fase_gestion8']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+7);
                          }
                        }
                      }
                    }
                  }
                }
              }
            }

            $this->load->view('admin/programacion/datos_generales/generales/cgform9_up2', $data);
          }
      }
      else{
        redirect('admin/dashboard');
      }
    }

  /*================================================= PROGRAMACION FISICA ============================================================*/
  public function programacion_fisica($mod,$id_f,$id_p)
  {
    if($mod!='' & $id_f!='' & $id_p!=''){    
        $enlaces=$this->menu_modelo->get_Modulos_programacion(1);
        $data['enlaces'] = $enlaces;
        $data['nro_fase']= $this->model_faseetapa->nro_fase($id_p); /// nro de fases y etapas registrados
        $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p); 
        $data['id_f'] = $this->model_faseetapa->get_id_fase($id_p); //// recupera datos de la tabla fase activa
        $data['f_top'] = $this->model_proyecto->responsable_proy($id_p,'1'); //// unidad ejecutora
        $data['mod']=$mod;

        $titulo_proy=strtoupper($data['proyecto'][0]['tipo']);
        $data['titulo_proy'] = $titulo_proy;

        if($data['proyecto'][0]['tp_id']==1)  ///////// para proyectos de inversion
        {
          $this->load->view('admin/programacion/programacion/fisica/p_fisica', $data);   
        }
        else
        {
          /*redireccionando a componente*/
          redirect('admin/prog/list_comp/'.$mod.'/'.$id_f.'/'.$id_p.'');
        }

      }
      else{
        redirect('admin/dashboard');
      }
  }

  /*----------------------------- PROGRAMACION FINANCIERA ------------------------------------*/
  public function programacion_financiera($mod,$id_f,$id_p)
  {
    if($mod!='' & $id_f!='' & $id_p!=''){  
      $enlaces=$this->menu_modelo->get_Modulos_componentes(1);
      $data['enlaces'] = $enlaces;
      for($i=0;$i<count($enlaces);$i++)
      {
        $subenlaces[$enlaces[$i]['idchild']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['idchild'], $this->session->userdata('user_name'));
      }
      $data['subenlaces'] = $subenlaces;
      $data['nro']= $this->model_componente->nro_fase($id_p); /// nro de fases y etapas registrados
      $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p);
      $data['fase_proyecto'] = $this->model_proyecto->fase_etapa($id_f,$id_p);
      $data['id_f'] = $this->model_componente->get_id_fase($id_p); //// recupera datos de la tabla fase encendido
      $data['verif_onn']= $this->model_componente->verif_fase($id_p); /// verificando que la fase este encendida
      $data['mod']=$mod;

      $this->load->view('admin/proyecto/componente/p_financiera', $data);
    }
    else{
        redirect('admin/dashboard');
    }
  }
    /*-------------------- PROGRAMACION FINANCIERA (Datos Generales)-------------------*/
    public function programacion_fin($mod,$id_f,$id_p){
        $fase = $this->model_faseetapa->get_id_fase($id_p);
        if($this->model_faseetapa->verif_fase_etapa_gestion($id_f,$this->session->userdata("gestion"))!=0){
            $fase_gest = $this->model_faseetapa->fase_etapa_gestion($id_f,$this->session->userdata("gestion")); ////// datos fase activa presupuesto gestion actual
            //if($this->model_faseetapa->nro_ffofet($fase_gest[0]['ptofecg_id'])!=0){
                redirect('prog/ins/'.$id_p.'/'.$fase[0]['pfec_ejecucion'].'');
            //}
        }
    }

    function rolfun($rol)
    {
      $valor=false;
      for ($i=1; $i <=count($rol) ; $i++) { 
        $data = $this->Users_model->get_datos_usuario_roles($this->session->userdata('fun_id'),$rol[$i]);
        if(count($data)!=0){
          $valor=true;
          break;
        }
      }
      return $valor;
    }
}