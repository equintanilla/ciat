<?php

class Cprog_insumos_delegado extends CI_Controller
{
    var $gestion;
    var $rol;
    var $fun_id;

    function __construct()
    {
        parent::__construct();
        $this->load->model('programacion/insumos/minsumos');
        $this->load->model('mantenimiento/model_partidas');
        $this->load->model('mantenimiento/model_entidad_tras');
        $this->load->model('programacion/insumos/minsumos_delegado');
        //llamar a mi menu
        $this->load->library('menu');
        $this->menu->const_menu(1);
        $this->gestion = $this->session->userData('gestion');
        $this->rol = $this->session->userData('rol');
        $this->fun_id = $this->session->userData('fun_id');
    }

    //PROGRAMACION DE INSUMOS A NIVEL DE COMPONENTES
    function prog_isumos_com($proy_id, $com_id)
    {

        $data['proy_id'] = $proy_id;
        $data['com_id'] = $com_id;
        $data['dato_proy'] = $this->minsumos->dato_proyecto($proy_id);
        $data['dato_com'] = $this->minsumos_delegado->get_componente($com_id);
        $data['tabla_presupuesto'] = $this->genera_tabla_presupuesto($proy_id, $this->gestion);
        $data['suma_costo_total'] = $this->minsumos_delegado->get_suma_costo_total($com_id)->fnsuma_costo_total_ins_delegado;
        $data['tabla_activos'] = $this->genera_tabla_af($proy_id, $com_id);
        $data['insumos_js_delegado'] = '<SCRIPT src="' . base_url() . 'mis_js/programacion/insumos/abm_insumos_com.js" type="text/javascript"></SCRIPT>';
        $ruta = 'programacion/insumos/vlista_insumos_comp';
        $this->construir_vista($ruta, $data);
    }

    //NUEVO INSUMO - TIPO ACTIVOS FIJOS
    function nuevo_insumo($proy_id, $com_id)
    {
        $data['proy_id'] = $proy_id;
        $data['com_id'] = $com_id;
        $data['dato_proy'] = $this->minsumos->dato_proyecto($proy_id);
        $data['dato_com'] = $this->minsumos_delegado->get_componente($com_id);
        //----------------------------------------------------------------------------------------------------------------------------
        $data['techo'] = $this->minsumos_delegado->tabla_presupuesto($proy_id, $this->gestion);//techo presupuestario
        $data['saldo_total'] = $this->minsumos_delegado->saldo_total_fin($proy_id, $this->gestion);//SALDO TOTAL DEL TECHO PRESUPUESTARIO
        $data['techo_mes'] = $this->minsumos_delegado->sum_prog_mensual_actividades($proy_id, $this->gestion, $com_id);//TECHO POR MES DE SUMA DE ACTIVIDADES
        $data['lista_entidad'] = $this->model_entidad_tras->lista_entidad_tras();//entidad transferencia
        $data['lista_partidas'] = $this->model_partidas->lista_padres();//partidas
        $data['insumos_js_delegado'] = '<SCRIPT src="' . base_url() . 'mis_js/programacion/insumos/abm_insumos_com.js" type="text/javascript"></SCRIPT>';
        $ruta = 'programacion/insumos/vform_ins_comp';
        $this->construir_vista($ruta, $data);

    }

    //GUARDAR INSUMO A NIVEL COMPONENTE ,TIPO ACTIVO FIJO
    function guardar_insumo()
    {
        if ($this->input->post()) {
            $post = $this->input->post();
            $proy_id = $post['proy_id'];
            $com_id = $post['com_id'];
            $cant_fin = $post['cant_fin'];//cantidad de financiamiento
            $data_insumo = $this->get_formulario($post); //datos del insumo
            if ($this->input->post('ins_id')) {
                //MODIFICAR INSUMO
                $ins_id = $this->input->post('ins_id');
                $peticion = $this->minsumos->modificar_insumo($data_insumo, $post, $cant_fin, $ins_id);
                if ($peticion) {
                    echo '<script> alert("EL REGISTRO SE MODIFICÓ CORRECTAMENTE")</script>';
                    redirect(site_url("") . '/prog/ins_com/' . $proy_id . '/' . $com_id);
                } else {
                    echo '<script> alert("ERROR AL MODIFICAR")</script>';
                }
            } else {
                $peticion = $this->minsumos_delegado->guardar_insumo($data_insumo, $post, $cant_fin, $com_id);
                if ($peticion) {
                    echo '<script> alert("EL REGISTRO SE GUARDO CORRECTAMENTE")</script>';
                    redirect(site_url("") . '/prog/ins_com/' . $proy_id . '/' . $com_id);
                } else {
                    echo '<script> alert("ERROR AL GUARDAR")</script>';
                }
            }
        } else {
            show_404();
        }
    }

    //GENERAR LA TABLA DE PRESUPUESTO DE MI PROYECTO
    function genera_tabla_presupuesto($proy_id, $gestion)
    {
        $lista_presupuesto = $this->minsumos_delegado->tabla_presupuesto($proy_id, $gestion);
        $saldo_total = $this->minsumos_delegado->saldo_total_fin($proy_id, $gestion);
        $tabla = '';
        $cont = 1;
        foreach ($lista_presupuesto as $row) {
            $tabla .= '<tr>';
            $tabla .= '<td>' . $cont . '</td>';
            $tabla .= '<td>' . $row['ff_codigo'] . '</td>';
            $tabla .= '<td>' . $row['ff_descripcion'] . '</td>';
            $tabla .= '<td>' . $row['of_codigo'] . '</td>';
            $tabla .= '<td>' . $row['of_descripcion'] . '</td>';
            $tabla .= '<td style="text-align: right;">' . number_format($row['presupuesto_asignado'], 2, ',', '.') . '</td>';
            $tabla .= '<td style="text-align: right;">' . number_format($row['presupuesto_programado'], 2, ',', '.') . '</td>';
            $tabla .= '<td style="text-align: right;">' . number_format($row['saldo'], 2, ',', '.') . '</td>';
            $tabla .= '</tr>';
            $cont++;
        }
        $tabla .= '<tr>';
        $tabla .= '<td colspan="5" style="background:#646464;">
                        <b style="color:#fff;"><center>T O T A L</center></b>
                   </td>';
        $tabla .= '<td style="text-align: right; background:#646464;"><b style="color:#fff;">' . number_format($saldo_total->asignado, 2, ',', '.') . '</b></td>';
        $tabla .= '<td style="text-align: right; background:#646464;"><b style="color:#fff;">' . number_format($saldo_total->programado, 2, ',', '.') . '</b></td>';
        $tabla .= '<td style="text-align: right; background:#646464;"><b style="color:#fff;">' . number_format($saldo_total->saldo_total, 2, ',', '.') . '</b></td>';
        $tabla .= '</tr>';
        return $tabla;

    }

    //OBTENER MIS DATOS DEL FORMULARIO INSUMO
    function get_formulario($post)
    {
        $data = array(
            'ins_fecha_requerimiento' => $this->get_fecha_postgres($post['ins_fecha']),
            'ins_detalle' => strtoupper($this->security->xss_clean($post['ins_detalle'])),
            'ins_unidad_medida' => strtoupper($this->security->xss_clean($post['ins_unidad_medida'])),
            'ins_cant_requerida' => $this->security->xss_clean($post['ins_cantidad']),
            'ins_costo_unitario' => $this->security->xss_clean($post['ins_costo_unitario']),
            'ins_costo_total' => $this->security->xss_clean($post['ins_costo_total']),
            'par_id' => $post['ins_partidas_dependientes'],
            'ins_tipo' => 8
        );
        return $data;
    }

    //GENERAR TABLA ACTIVOS FIJOS = 8
    function genera_tabla_af($proy_id, $com_id)
    {
        $lista_af = $this->minsumos_delegado->lista_activos_fijos($com_id);
        $mod_ins = site_url() . '/prog/mod_ins_com/' . $proy_id . '/' . $com_id;
        $tabla = '';
        foreach ($lista_af as $row) {
            $tabla .= '<tr>';
            $tabla .= '<td>';
            $tabla .= '
                            <a href="' . $mod_ins . '/' . $row['ins_id'] . '" title="MODIFICAR" class="mod_insumo" name="mod_indumo" id="mod_insumo">
                                <img src="' . base_url() . 'assets/ifinal/modificar.png" WIDTH="40" HEIGHT="40"/>
                            </a>
                            <a  title="ELIMINAR" class="del_insumo_comp" name="' . $row['ins_id'] . '" id="del_insumo">
                                 <img src="' . base_url() . 'assets/ifinal/eliminar.png" WIDTH="40" HEIGHT="40"/>
                            </a>';
            $tabla .= '</td>';
            $tabla .= '<td style="font-size: 10px">' . $row['ins_codigo'] . '</td>';
            $tabla .= '<td style="font-size: 9px">' . $row['ins_fecha_requerimiento'] . '</td>';
            $tabla .= '<td style="font-size: 10px">' . $row['ins_detalle'] . '</td>';
            $tabla .= '<td style="font-size: 10px">' . $row['ins_cant_requerida'] . '</td>';
            $tabla .= '<td style="font-size: 10px">' . $row['ins_costo_unitario'] . '</td>';
            $tabla .= '<td style="font-size: 10px">' . $row['ins_costo_total'] . '</td>';
            $tabla .= '<td style="font-size: 10px">' . $row['par_codigo'] . '</td>';
            $tabla .= '<td style="font-size: 10px">' . $this->get_tabla_ins_progmensual_delegado($row['ins_id']) . '</td>';
            $tabla .= '</tr>';
        }
        return $tabla;
    }

    //GENERAR LA TABLA DE FINANCIAMIENTO PROGRAMADO  DEL INSUMO
    function get_tabla_ins_progmensual_delegado($ins_id)
    {
        $prog_mensual = $this->minsumos->lista_progmensual_ins($ins_id);
        $tabla = ' <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th style="background-color: #0AA699;color: #FFFFFF">MONTO</th>
                                    <th style="background-color: #0AA699;color: #FFFFFF">FF</th>
                                    <th style="background-color: #0AA699;color: #FFFFFF">OF</th>
                                    <th style="background-color: #0AA699;color: #FFFFFF">ET</th>
                                    <th  style="background-color: #0AA699;color: #FFFFFF">Enero</th>
                                    <th  style="background-color: #0AA699;color: #FFFFFF">Febrero</th>
                                    <th  style="background-color: #0AA699;color: #FFFFFF">Marzo</th>
                                    <th  style="background-color: #0AA699;color: #FFFFFF">Abril</th>
                                    <th  style="background-color: #0AA699;color: #FFFFFF">Mayo</th>
                                    <th  style="background-color: #0AA699;color: #FFFFFF">Junio</th>
                                    <th  style="background-color: #0AA699;color: #FFFFFF">Julio</th>
                                    <th  style="background-color: #0AA699;color: #FFFFFF">Agosto</th>
                                    <th  style="background-color: #0AA699;color: #FFFFFF">Septiembre</th>
                                    <th  style="background-color: #0AA699;color: #FFFFFF">Octubre</th>
                                    <th  style="background-color: #0AA699;color: #FFFFFF">Noviembre</th>
                                    <th  style="background-color: #0AA699;color: #FFFFFF">Diciembre</th>
                                </tr>
                            </thead>
                        <tbody>';
        //FINANCIAMIENTO PROGRAMADO
        foreach ($prog_mensual as $row) {
            $tabla .= '<tr>';
            $tabla .= '<td>' . $row['ifin_monto'] . '</td>';
            $tabla .= '<td>' . $row['ff_codigo'] . '</td>';
            $tabla .= '<td>' . $row['of_codigo'] . '</td>';
            $tabla .= '<td>' . $row['et_codigo'] . '</td>';
            $tabla .= '<td>' . $row['enero'] . '</td>';
            $tabla .= '<td>' . $row['febrero'] . '</td>';
            $tabla .= '<td>' . $row['marzo'] . '</td>';
            $tabla .= '<td>' . $row['abril'] . '</td>';
            $tabla .= '<td>' . $row['mayo'] . '</td>';
            $tabla .= '<td>' . $row['junio'] . '</td>';
            $tabla .= '<td>' . $row['julio'] . '</td>';
            $tabla .= '<td>' . $row['agosto'] . '</td>';
            $tabla .= '<td>' . $row['septiembre'] . '</td>';
            $tabla .= '<td>' . $row['octubre'] . '</td>';
            $tabla .= '<td>' . $row['noviembre'] . '</td>';
            $tabla .= '<td>' . $row['diciembre'] . '</td>';
            $tabla .= '</tr>';
        }
        $tabla .= '</tbody>
                 </table>
            </div>';
        return $tabla;
    }

    //MODIFICAR INSUMO
    function modificar_insumo($proy_id, $com_id, $ins_id)
    {
        $data['proy_id'] = $proy_id;
        $data['com_id'] = $com_id;
        $data['ins_id'] = $ins_id;
        $data['dato_proy'] = $this->minsumos->dato_proyecto($proy_id);
        $data['dato_com'] = $this->minsumos_delegado->get_componente($com_id);
        $data['get_ins'] = $this->minsumos->get_insumo($ins_id);//DATOS DEL INSUMO
        //----------------------------------------------------------------------------------------------------------------------------
        $data['costo_total'] = $data['get_ins']->ins_costo_total;
        $data['techo'] = $this->minsumos_delegado->tabla_presupuesto($proy_id, $this->gestion);//techo presupuestario
        $data['prog_mensual_ins'] = $this->prog_mensual_fin($proy_id, $ins_id);
        $data['saldo_total'] = $this->minsumos_delegado->saldo_total_fin($proy_id, $this->gestion);//SALDO TOTAL DEL TECHO PRESUPUESTARIO
        $data['techo_mes'] = $this->minsumos_delegado->sum_prog_mensual_actividades($proy_id, $this->gestion, $com_id);//TECHO POR MES DE SUMA DE ACTIVIDADES
        //CASO DE PARTIDAS
        $data['lista_partidas'] = $this->model_partidas->lista_padres();//PARTIDAS PADRES
        $dato_par_id = $this->model_partidas->dato_par($data['get_ins']->par_id);
        $data['par_codigo_padre'] = $dato_par_id[0]['par_depende'];
        $data['lista_part_hijos'] = $this->model_partidas->lista_par_hijos($dato_par_id[0]['par_depende']);//PARTIDAS HIJOS
        //---------------------
        $data['insumos_js_delegado'] = '<SCRIPT src="' . base_url() . 'mis_js/programacion/insumos/abm_insumos_com.js" type="text/javascript"></SCRIPT>';
        $ruta = 'programacion/insumos/vform_mod_ins_com';
        $this->construir_vista($ruta, $data);
    }

    //GENERAR PROGRAMACION DE INSUMOS XON FINANCIAMIENTOS
    function prog_mensual_fin($proy_id, $ins_id)
    {
        $techo = $this->minsumos_delegado->tabla_presupuesto($proy_id, $this->gestion);//techo presupuestario
        $lista_entidad = $this->model_entidad_tras->lista_entidad_tras();//entidad transferencia
        $cont = 1;
        $tabla = '';
        foreach ($techo as $row) {
            $prog_fin = $this->minsumos->ins_prog_mensual($ins_id, $row['ff_id'], $row['of_id']);
            $m_asignado = 0;
            if ($prog_fin->num_rows() != 0) {
                $m_asignado = $prog_fin->row()->ifin_monto;
            }
            $tabla .= '<div class="jarviswidget jarviswidget-color-teal" id="wid-id-1" data-widget-editbutton="false">
                <header style="height: 60px">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr style=" background:#568a89;">
                                <th style="text-align:center;"><b style="color:#fff;">FINANCIAMIENTO</th>
                                <th style="text-align:center;"><b style="color:#fff;">FUENTE FINANCIAMIENTO
                                </th>
                                <th style="text-align:center;"><b style="color:#fff;">ORGANISMO FINANCIADOR
                                </th>
                                <th style="text-align:center;"><b style="color:#fff;">SALDO POR PROGRAMAR</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr style=" background:#568a89;">
                                <td style="text-align: center; "><b style="color:#fff;">
                                        Nro. ' . $cont . '</b>
                                </td>
                                <td style="text-align: center; "><b style="color:#fff;">
                                        ' . $row['ff_codigo'] . '  ' . $row['ff_descripcion'] . '</b>
                                </td>
                                <td style="text-align: center;"><b style="color:#fff;">
                                        ' . $row['of_codigo'] . '  ' . $row['of_descripcion'] . '</b>
                                </td>
                                <td style="text-align: center;"><b style="color:#fff;">
                                        ' . number_format((($row['saldo']) + $m_asignado), 2, ',', '.') . '</b>
                                </td>
                            </tr>
                            <input type="hidden" name="saldo_prog' . $cont . '"
                                   id="saldo_prog' . $cont . '" value="' . (($row['saldo']) + $m_asignado) . '">
                            </tbody>
                        </table>
                    </div>
                </header>
                <!----------- datos del financiamineto --------------->
                <input type="hidden" name="ff' . $cont . '" id="ff' . $cont . '"
                       value="' . $row['ff_id'] . '">
                <input type="hidden" name="of' . $cont . '" id="of' . $cont . '"
                       value="' . $row['of_id'] . '">';
            if ($prog_fin->num_rows() == 0) {
                $tabla .= '
                <div class="widget-body">
                    <div class="row">
                        <div class="col-md-9">
                            <label><b>ENTIDAD DE TRANSFERENCIA</b></label>
                            <select name="ins_et' . $cont . '" id="ins_et' . $cont . '"
                                    class="select2-container select2">
                                <option value="0"> 0 - Sin Entidad de Transferencia</option>';
                foreach ($lista_entidad as $row) {
                    $tabla .= '<option
                                        value="' . $row['et_id'] . '">' . $row['et_codigo'] . " - " . $row['et_descripcion'] . '
                                    </option>';

                }
                $tabla .= '   </select>
                        </div>
                        <div class="col-md-3">
                            <label> <b>MONTO ASIGNADO:</b></label>
                            <input type="text" name="ins_monto' . $cont . '" id="ins_monto' . $cont . '"
                                   onkeypress="if (this.value.length < 12) { return numerosDecimales(event);}else{return false; }"
                                   class="form-control" value="0">
                        </div>
                    </div>
                    <br>
                    <div class="row">';
                $mes[1] = 'ENERO';
                $mes[2] = 'FEBRERO';
                $mes[3] = 'MARZO';
                $mes[4] = 'ABRIL';
                $mes[5] = 'MAYO';
                $mes[6] = 'JUNIO';
                $mes[7] = 'JULIO';
                $mes[8] = 'AGOSTO';
                $mes[9] = 'SEPTIEMBRE';
                $mes[10] = 'OCTUBRE';
                $mes[11] = 'NOVIEMBRE';
                $mes[12] = 'DICIEMBRE';
                for ($i = 1; $i <= 12; $i++) {
                    $tabla .= '    <div class="col-md-2 col-ms-2">
                                        <div class="form-group">
                                         <LABEL>' . $mes[$i] . '</LABEL>
                                        <input type="text" name="mes' . $cont . $i . '" id="mes' . $cont . $i . '" title="VERIFICAR EL MONTO"
                                        onkeypress="if (this.value.length < 15) { return numerosDecimales(event);}else{return false; }"
                                        class="form-control input-xs" value="0" required>
                                        </div>

                            </div>';
                }
                $tabla .= ' </div>
                </div>
            </div>';
            } else {
                $prog_fin = $prog_fin->row();
                $tabla .= '
                <div class="widget-body">
                    <div class="row">
                        <div class="col-md-9">
                            <label><b>ENTIDAD DE TRANSFERENCIA</b></label>
                            <select name="ins_et' . $cont . '" id="ins_et' . $cont . '"
                                    class="select2-container select2">
                                <option value="0"> 0 - Sin Entidad de Transferencia</option>';
                foreach ($lista_entidad as $row) {
                    if ($prog_fin->et_id == $row['et_id']) {
                        $tabla .= '<option value="' . $row['et_id'] . '" selected>' .
                            $row['et_codigo'] . " - " . $row['et_descripcion'] . '
                                      </option>';
                    } else {
                        $tabla .= '<option
                                        value="' . $row['et_id'] . '">' . $row['et_codigo'] . " - " . $row['et_descripcion'] . '
                                    </option>';
                    }
                }
                $tabla .= '   </select>
                        </div>
                        <div class="col-md-3">
                            <label> <b>MONTO ASIGNADO:</b></label>
                            <input type="text" name="ins_monto' . $cont . '" id="ins_monto' . $cont . '"
                                   onkeypress="if (this.value.length < 12) { return numerosDecimales(event);}else{return false; }"
                                   class="form-control" value="' . $prog_fin->ifin_monto . '">
                        </div>
                    </div>
                    <br>
                    <div class="row">';
                $mes[1] = 'enero';
                $mes[2] = 'FEBRERO';
                $mes[3] = 'MARZO';
                $mes[4] = 'ABRIL';
                $mes[5] = 'MAYO';
                $mes[6] = 'JUNIO';
                $mes[7] = 'JULIO';
                $mes[8] = 'AGOSTO';
                $mes[9] = 'SEPTIEMBRE';
                $mes[10] = 'OCTUBRE';
                $mes[11] = 'NOVIEMBRE';
                $mes[12] = 'DICIEMBRE';
                for ($i = 1; $i <= 12; $i++) {
                    $m = strtolower($mes[$i]);
                    $tabla .= '    <div class="col-md-2 col-ms-2">
                                        <div class="form-group">
                                         <LABEL>' . $mes[$i] . '</LABEL>
                                        <input type="text" name="mes' . $cont . $i . '" id="mes' . $cont . $i . '" title="VERIFICAR EL MONTO"
                                        onkeypress="if (this.value.length < 15) { return numerosDecimales(event);}else{return false; }"
                                        class="form-control input-xs" value="' . $prog_fin->$m . '" required>
                                        </div>

                            </div>';
                }
                $tabla .= ' </div>
                </div>
            </div>';
            }
            $cont++;
        }
        return $tabla;
    }

    //
    function eliminar_insumo(){
        if($this->input->is_ajax_request() && $this->input->post()){
            $ins_id = $this->input->post('ins_id');
            $peticion = $this->minsumos_delegado->eliminar_insumo($ins_id);
            if ($peticion) {
                $data['peticion'] = 'verdadero';
            } else {
                $data['peticion'] = 'falso';
            }
            echo json_encode($data);
        }else{

        }
    }

    //FECHA_POSTGRES
    function get_fecha_postgres($fecha)
    {
        $date = new DateTime($fecha);
        return $date->format('Y-m-d');
        //$vec_fecha = explode("/", $fecha);
        //return $vec_fecha[2] . '-' . $vec_fecha[1] . '-' . $vec_fecha[0];
    }

    //FUNCION PARA CONSTRUIR MI VISTA
    function construir_vista($ruta, $data)
    {
        //----------------------------------- MENU-------------------------------
        $menu['enlaces'] = $this->menu->get_enlaces();
        $menu['subenlaces'] = $this->menu->get_sub_enlaces();
        $menu['titulo'] = 'PROGRAMACI&Oacute;N';
        //-----------------------------------------------------------------------
        //armar vista
        $this->load->view('includes/header');
        $this->load->view('includes/menu_lateral', $menu);
        $this->load->view($ruta, $data);//contenido
        //$this->load->view('admin/mantenimiento/vprueba');//contenido
        $this->load->view('includes/footer');
    }
}