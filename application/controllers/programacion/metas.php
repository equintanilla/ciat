<?php

class metas extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('fun_id')!=null){
        $this->load->model('programacion/model_proyecto');
        $this->load->model('Users_model','',true);
        $this->load->model('menu_modelo');
        }else{
            redirect('/','refresh');
        }
        
    }
    //=========================== VALIDA METAS DE PROYECTOS =============================
    function add_metas(){
        if($this->input->is_ajax_request() && $this->input->post()){
            $this->form_validation->set_rules('ind', 'Indicador', 'required|trim');
            $this->form_validation->set_rules('meta', 'meta', 'required|trim');
            $this->form_validation->set_rules('ejec', 'ejec', 'required|trim|integer');
            //=========================== mensajes =========================================
            $this->form_validation->set_message('required', 'El campo es es obligatorio');
            $this->form_validation->set_message('integer', 'El campo  debe poseer solo numeros enteros');
            
            if ($this->form_validation->run() ) {
                $id=  $this->input->post('id_pr');
                $ind=  $this->input->post('ind');
                $meta =  $this->input->post('meta');
                $ejec =  $this->input->post('ejec');
                $efi=  (($this->input->post('ejec')/$this->input->post('meta'))*100);
                $rp =  $this->input->post('rp');
                //=================enviar  evitar codigo malicioso ==========
                $id = $this->security->xss_clean($id);
                $ind = $this->security->xss_clean(trim($ind));
                $meta = $this->security->xss_clean(trim($meta));
                $ejec = $this->security->xss_clean($ejec);
                $efi = $this->security->xss_clean($efi);
                $rp = $this->security->xss_clean($rp);
                //======================= MODIFICAR=

                $data = array(
                        'proy_id' => $id,
                        'meta_descripcion' => strtoupper($ind),
                        'meta_meta' =>$meta,
                        'meta_ejec' =>$ejec,
                        'meta_efic' =>$efi,
                        'meta_rp' =>$rp,
                        'fun_id' => $this->session->userdata("fun_id"),
                    );
                    $this->db->insert('_metas',$data);
                echo 'true';
            } else {
                echo'DATOS ERRONEOS';
            }
        }else{
            show_404();
        }
    }

    function update_metas()
      {
        if($this->input->is_ajax_request() && $this->input->post()){
            $this->form_validation->set_rules('meta_id', 'id de la meta', 'required|trim');
            //=========================== mensajes =========================================
            $this->form_validation->set_message('required', 'El campo es es obligatorio');
            /*if ($this->form_validation->run() ) {*/
                $post = $this->input->post();
                $id = $post['meta_id'];
                $descripcion = $post['indis'];
                $meta = $post['metas'];
                $ejecucion = $post['ejecs'];
                $eficacia=(($post['ejecs']/$post['metas'])*100);
                $rp = $post['rp'];
                //================ evitar enviar codigo malicioso ==========
                $id= $this->security->xss_clean($id);
                $descripcion= $this->security->xss_clean($descripcion);
                $meta= $this->security->xss_clean($meta);
                $ejecucion= $this->security->xss_clean($ejecucion);
                $eficacia= $this->security->xss_clean($eficacia);
                $rp= $this->security->xss_clean($rp);
                
                $update_proy = array(
                        'meta_descripcion' => strtoupper($descripcion),
                        'meta_meta' => $meta,
                        'meta_ejec' => $ejecucion,
                        'meta_efic' => $eficacia,
                        'estado' => '2',
                        'meta_rp' => $rp,
                        'fun_id' => $this->session->userdata("fun_id"),
                    );
                $this->db->where('meta_id', $id);
                $this->db->update('_metas', $update_proy);

                echo 'true';
        }else{
            show_404();
        }
    }

    function delete_meta(){
        if($this->input->is_ajax_request() && $this->input->post()){
            $post = $this->input->post();
            $meta_id = $post['meta_id'];

            $update_proy = array(
                        'estado' => '3',
                        'fun_id' => $this->session->userdata("fun_id"),
                    );
                $this->db->where('meta_id', $meta_id);
                $this->db->update('_metas', $update_proy);
            $sql = $this->db->get();
           
            if($this->db->query($sql)){
                echo $meta_id;
            }else{
                echo false;
            }
        }else{
            show_404();
        }
    }

    function programacion()
    {
        //----------------------- MENU-------------------------------------------------
        $enlaces=$this->menu_modelo->get_Modulos(9);
        $data['enlaces'] = $enlaces;
        for($i=0;$i<count($enlaces);$i++)
        {
            $subenlaces[$enlaces[$i]['idchild']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['idchild'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        $data['titulo'] = 'MANTENIMIENTO';
        //-----------------------------------------------------------------------
        $data['lista_aper'] = $this->model_mantenimiento->lista_aper();
        $data['lista_uni'] = $this->model_mantenimiento->lista_uni();
        $data['main_content'] = 'admin/mantenimiento/vlist_programacion';
        $this->load->view('admin/mantenimiento/vlist_programacion', $data);
    }
}