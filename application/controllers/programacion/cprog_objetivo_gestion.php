<?php

class Cprog_objetivo_gestion extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->model('mantenimiento/mpoa');
        $this->load->model('mantenimiento/munidad_organizacional');
        $this->load->model('mantenimiento/model_funcionario');
        $this->load->model('mantenimiento/mindicador');
        $this->load->model('programacion/marco_estrategico/mobjetivos');
        $this->load->model('programacion/prog_poa/mobjetivo_gestion');
        //llamar a mi menu
        $this->load->library('menu');
        $this->menu->const_menu(1);
    }

    //LISTA DE OBJETIVO DE GESTION
    function lista_ogestion($poa_id, $obje_id)
    {
        $gestion = $this->session->userData('gestion');
        $dato_poa = $this->mpoa->dato_poa_id($poa_id);//datos del poa
        $data['dato_poa'] = $dato_poa[0];
        $dato_objest = $this->mobjetivos->dato_objetivo($obje_id);//dato de objetivo estrategico
        $data['dato_objest'] = $dato_objest[0];
        //-----------------------------------------------------------------------
        $data['lista_objgestion'] = $this->mobjetivo_gestion->lista_ogestion($obje_id, ($dato_poa[0]['aper_id']));
        $lista_objgestion = $this->mobjetivo_gestion->lista_ogestion($obje_id, ($dato_poa[0]['aper_id']));
        $programacion = array();
        foreach ($lista_objgestion as $row) {
            $o_id = $row['o_id'];// id de mi objetivo estrategico
            $linea_base = $row['o_linea_base'];
            $meta = $row['o_meta']; //variable meta
            $programacion[$o_id] = $this->programacion_mensual($o_id, $linea_base, $meta);
        }
        $data['poa_id'] = $poa_id;
        $data['obje_id'] = $obje_id;
        $data['programacion'] = $programacion;
        $ruta = 'programacion/prog_poa/red_objetivos/vlista_objgestion';
        $this->construir_vista($ruta, $data);
    }

    //CALCULO DE PROGRAMACION MENSUAL
    function programacion_mensual($obje_id, $linea_base, $meta)
    {
        $prog_acumulado = $linea_base; //variable de programacion acumulada
        for ($i = 1; $i <= 12; $i++) {
            $puntero_prog = 'p' . $i;//PUNTERO PARA LISTAR LA PROGRAMACION
            $dato_programado = $this->mobjetivo_gestion->get_prog_ogestion($obje_id, $i);//lista de programado
            $dato_programado = $dato_programado[0]['opm_fis'];
            $programado[$puntero_prog] = $dato_programado;//matriz
            //PROGRAMADO ACUMULADO--------------------------------------------------------------------------------
            $puntero_prog_acumulado = 'p_a' . $i;//PUNTERO PARA LA PROGRAMACION ACUMULADA
            $prog_acumulado += $dato_programado;
            $programado[$puntero_prog_acumulado] = $prog_acumulado;//matriz
            //PROGRAMA ACUMULADO PORECENTUAL----------------------------------------------------------------------
            $puntero_pa_porcentual = 'pa_porc' . $i;//PUNTERO PARA LA PROGRAMACION ACUMULADA PORCENTUAL
            if ($meta == 0) {
                $pa_porcentual = 0;
            } else {
                $pa_porcentual = (($prog_acumulado / $meta) * 100);
            }
            $programado[$puntero_pa_porcentual] = $pa_porcentual;//matriz
        }
        return $programado;
    }

    //NUEVO OBJETIVO DE GESTION
    function nuevo_ogestion($poa_id, $obje_id)
    {
        $dato_poa = $this->mpoa->dato_poa_id($poa_id);//datos del poa
        $dato_obje = $this->mobjetivos->dato_objetivo($obje_id);//dato de objetivo estrategico
        $data['dato_poa'] = $dato_poa[0];//datos del poa
        $data['dato_obje'] = $dato_obje[0];//dato de objetivo estrategico
        //------------------------------------------------------------------------------
        $datos = $dato_obje[0];        
        $funcionario= $this->model_funcionario->get_funcionario($datos['fun_id']);
        $data['list_indicador'] = $this->mindicador->get_indicador();
        $data['list_funcionario'] = $this->model_funcionario->get_funcionarios();

        $unidado = $this->munidad_organizacional->get_unidad($funcionario[0]['uni_id']);
        $data['uninda_organizacional'] = $unidado->uni_unidad;
        $ruta = 'programacion/prog_poa/red_objetivos/vnuevo_objgestion';
        $this->construir_vista($ruta, $data);
    }

    //GUARDAR MI OBJETIVO DE GESTION
    function add_objetivo_gestion()
    {
        if ($this->input->post()) {
            $post = $this->input->post();
            $aper_id = $post['aper_id'];
            $poa_id = $post['poa_id'];
            $obje_id = $post['obje_id'];
            $dato_obje = $this->mobjetivos->dato_objetivo($obje_id);//dato de objetivo estrategico
            $datos = $dato_obje[0];
            // $funcionario= $this->model_funcionario->get_funcionario($datos['fun_id']);
            $data = array(
                'aper_id' => $aper_id,
                'obje_id' => $obje_id,
                'fun_id' => $datos['fun_id'],
                // 'objetivo' => $datos['obje_objetivo'],
                'objetivo' => $this->security->xss_clean($post['oobjetivo']),
                'olineabase' => $this->security->xss_clean($post['olineabase']),
                'ometa' => $this->security->xss_clean($post['ometa']),
                'ofuenteverificacion' => $this->security->xss_clean($post['ofuenteverificacion']),
                'osupuesto' => $this->security->xss_clean($post['osupuesto']),
                'oindicador' => $datos['obje_indicador'],
                'o_denominador' => $post['o_denominador'],
                'mes1' => $this->security->xss_clean($post['mes1']),
                'mes2' => $this->security->xss_clean($post['mes2']),
                'mes3' => $this->security->xss_clean($post['mes3']),
                'mes4' => $this->security->xss_clean($post['mes4']),
                'mes5' => $this->security->xss_clean($post['mes5']),
                'mes6' => $this->security->xss_clean($post['mes6']),
                'mes7' => $this->security->xss_clean($post['mes7']),
                'mes8' => $this->security->xss_clean($post['mes8']),
                'mes9' => $this->security->xss_clean($post['mes9']),
                'mes10' => $this->security->xss_clean($post['mes10']),
                'mes11' => $this->security->xss_clean($post['mes11']),
                'mes12' => $this->security->xss_clean($post['mes12']),
            );
            $relativoa = '';
            $relativob = '';
            $relativoc = '';
            $oponderacion = $this->security->xss_clean($post['oponderacion']);
            $tipo_indicador = $post['tipo_indicador'];
            //------------caso absoluto
            if ($tipo_indicador == 1) {
                $data['oformula'] = '-';
            } else {
                $data['oformula'] = $this->security->xss_clean($post['oformula']);
                $relativoa = $this->security->xss_clean($post['relativoa']);
                $relativob = $this->security->xss_clean($post['relativob']);
                $relativoc = $this->security->xss_clean($post['relativoc']);
            }

            if (strlen(trim($oponderacion)) == 0) {
                $oponderacion = 0;
            }
            $data['relativoa'] = $relativoa;
            $data['relativob'] = $relativob;
            $data['relativoc'] = $relativoc;
            $data['relativoc'] = $relativoc;
            $data['oponderacion'] = $oponderacion;
            $data['tipo_indicador'] = $tipo_indicador;
            //======================= MODIFICAR====================
            if (isset($_REQUEST['modificar'])) {
                $o_id = $this->input->post('modificar');
                $gestion = $this->session->userData('gestion');
                $modificar = $this->mobjetivo_gestion->mod_ogestion($data, $o_id, $gestion);
                if ($modificar) {
                    $url = site_url("") . '/prog/obje_gestion/' . $poa_id . '/' . $obje_id;
                    $this->session->set_flashdata('modificar', 'EL REGISTRO SE MODIFICO CORRECTAMENTE !!!');
                    redirect($url, 'refresh');

                } else {
                    $url = site_url("") . '/prog/obje_gestion/' . $poa_id . '/' . $obje_id;
                    $this->session->set_flashdata('error', 'ERROR AL MODIFICAR!!!');
                    redirect($url, 'refresh');
                }
            } else {
                if ($this->mobjetivo_gestion->guardar_objgestion($data)) {
                    $url = site_url("") . '/prog/obje_gestion/' . $poa_id . '/' . $obje_id;
                    $this->session->set_flashdata('guardar', 'EL REGISTRO SE GUARDO CORRECTAMENTE !!!');
                    redirect($url, 'refresh');
                } else {
                    $url = site_url("") . '/prog/obje_gestion/' . $poa_id . '/' . $obje_id;
                    $this->session->set_flashdata('error', 'ERROR AL MODIFICAR!!!');
                    redirect($url, 'refresh');
                }
            }
        } else {
            show_404();
        }

    }

    //FUNCION PARA CONSTRUIR MI VISTA
    function construir_vista($ruta, $data)
    {
        //----------------------------------- MENU-------------------------------
        $menu['enlaces'] = $this->menu->get_enlaces();
        $menu['subenlaces'] = $this->menu->get_sub_enlaces();
        $menu['titulo'] = 'PROGRAMACI&Oacute;N';
        //-----------------------------------------------------------------------
        //armar vista
        $this->load->view('includes/header');
        $this->load->view('includes/menu_lateral', $menu);
        $this->load->view($ruta, $data);//contenido
        //$this->load->view('admin/mantenimiento/vprueba');//contenido
        $this->load->view('includes/footer');
    }

    //grafico para la lista objetivo de gestion
    function get_grafico_ogestion()
    {
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $post = $this->input->post();
            $o_id = $post['o_id'];
            $ogestion = $this->mobjetivo_gestion->get_ogestion($o_id);
            $ogestion = $ogestion[0];
            $codigo = $ogestion['o_codigo'];
            $linea_base = $ogestion['o_linea_base'];
            $meta = $ogestion['o_meta']; //variable meta
            $gestion = $ogestion['o_gestion'];// gestion
            //OBTENER MI TABLA TEMPORALIZACION
            $programacion = $this->programacion_mensual($o_id, $linea_base, $meta);
            $indi[1] = '';
            $indi[2] = '%';
            $porc = $indi[$ogestion['indi_id']];
            $vec_pa_grafico = array();
            $tabla = '';
            $tabla .= '<tr>
                            <td colspan="13" bgcolor="#2F4F4F"><center><b><font color="#ffffff" size="1">
                         PROGRAMACI&Oacute;N MENSUAL - ' . $gestion . '</font></b></center></td>
                       </tr>';
            //---------------------- CABECERA DE GESTIONES
            $mes[1] = 'ENERO';
            $mes[2] = 'FEBRERO';
            $mes[3] = 'MARZO';
            $mes[4] = 'ABRIL';
            $mes[5] = 'MAYO';
            $mes[6] = 'JUNIO';
            $mes[7] = 'JULIO';
            $mes[8] = 'AGOSTO';
            $mes[9] = 'SEPTIEMBRE';
            $mes[10] = 'OCTUBRE';
            $mes[11] = 'NOVIEMBRE';
            $mes[12] = 'DICIEMBRE';
            $tabla .= '<tr>';
            $tabla .= '<td style="width:1%;" bgcolor="#2F4F4F"></td>';
            for ($i = 1; $i <= 12; $i++) {
                $tabla .= '<td style="width:1%;" bgcolor="#2F4F4F"><center><b><font color="#ffffff" size="1">' .
                    $mes[$i] . '</font></b></center></td>';
            }
            $tabla .= '</tr>';
            //---------------------- FIN DE CABECERA
            //---------------------- PROGRAMACION
            $tabla .= '<tr>';
            $tabla .= '<td style="width:5%;" bgcolor="#E6E6FA"><center><b><font color="#000000" size="1">P</font></b></center></td>';
            for ($i = 1; $i <= 12; $i++) {
                $puntero = 'p' . $i;
                $prog_gestion = $programacion[$puntero];
                $tabla .= ' <td style="width:1%;" bgcolor="#E6E6FA"><center><font color="#000000" size="1">' . round($prog_gestion, 1) . $porc . '</font></center></td>';
            }
            $tabla .= '</tr>';
            //--------------------- PROGRAMACION ACUMULADA
            $tabla .= '<tr>';
            $tabla .= '<td style="width:5%;" bgcolor="#E6E6FA"><center><b><font color="#000000" size="1">P.A</font></b></center></td>';
            for ($i = 1; $i <= 12; $i++) {
                $puntero_acumulado = 'p_a' . $i;
                $prog_acumulado = $programacion[$puntero_acumulado];
                $tabla .= ' <td style="width:1%;" bgcolor="#E6E6FA"><center><font color="#000000" size="1">' . round($prog_acumulado, 1) . $porc . '</font></center></td>';
            }
            $tabla .= '</tr>';
            //--------------------- PROGRAMACION ACUMULADA PORCENTUAL
            $tabla .= '<tr>';
            $tabla .= '<td style="width:5%;" bgcolor="#E6E6FA"><center><b><font color="#000000" size="1">%P.A</font></b></center></td>';
            for ($i = 1; $i <= 12; $i++) {
                $puntero_pa_porcentual = 'pa_porc' . $i;
                $pa_porcentual = $programacion[$puntero_pa_porcentual];
                $vec_pa_grafico[($i - 1)] = $pa_porcentual;
                $tabla .= ' <td style="width:1%;" bgcolor="#E6E6FA"><center><font color="#000000" size="1">' . round($pa_porcentual, 1) . $porc . '%</font></center></td>';
            }
            $tabla .= '</tr>';
            $result = array(
                'tabla' => $tabla,
                'linea_base' => $linea_base,
                'meta' => $meta,
                'prog_acumulada_p' => $vec_pa_grafico,
                'gestion' => $gestion,
                'codigo' => $codigo
            );
            echo json_encode($result);
        } else {
            show_404();
        }
    }

    //CASO INDICADOR DE DESEMPEÑO OBJETIVO DE GESTION
    function get_indicador_ogestion()
    {
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $post = $this->input->post();
            $o_id = $post['o_id'];
            $dato = $this->mobjetivo_gestion->get_ogestion($o_id);
            $result = array(
                'codigo' => $dato[0]['o_codigo'],
                'objetivo' => $dato[0]['o_objetivo'],
                'eficacia' => $dato[0]['o_eficacia'],
                'financiera' => $dato[0]['o_efinanciera'],
                'ejecucion' => $dato[0]['o_epejecucion'],
                'fisica' => $dato[0]['o_efisica']
            );
            echo json_encode($result);
        } else {
            show_404();
        }
    }

    //GUARDAR INDICADOR DE DESEMPEÑO
    function add_indicador_ogestion()
    {
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $post = $this->input->post();
            $o_id = $post['o_id'];
            $data = array(
                'o_eficacia' => $this->security->xss_clean($post['eficacia']),
                'o_efinanciera' => $this->security->xss_clean($post['financiera']),
                'o_epejecucion' => $this->security->xss_clean($post['ejecucion']),
                'o_efisica' => $this->security->xss_clean($post['fisica'])
            );
            if ($this->mobjetivo_gestion->add_indicador_ogestion($o_id, $data)) {
                $result = array(
                    'respuesta' => 'true',
                );
            } else {
                $result = array(
                    'respuesta' => 'false',
                );
            }
            echo json_encode($result);
        } else {
            show_404();
        }
    }

    //LLENAR MI FORMULARIO DE DE CARGAR ARCHIVO
    function get_form_archivo()
    {
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $post = $this->input->post();
            $o_id = $post['o_id'];
            $data = $this->mobjetivo_gestion->get_ogestion($o_id);
            //$data = $data->row();
            $data = $data[0];
            if (strlen($data['o_archivo_adjunto']) == 0) {
                $result = array(
                    'respuesta' => 'true',
                    'codigo' => $data['o_codigo'],
                    'ruta' => ''
                );
            } else {
                $result = array(
                    'respuesta' => 'false',
                    'codigo' => $data['o_codigo'],
                    'ruta' => $data['o_archivo_adjunto']
                );
            }

            echo json_encode($result);
        } else {
            show_404();
        }
    }

    //GUARDAR ARCHIVO DEL OBJETIVOS
    function guardar_archivo()
    {
        if ($this->input->post()) {
            $post = $this->input->post();
            $poa_id = $post['poa_id'];
            $obje_id = $post['obje_id'];
            $o_id = $post['id_o_pdf'];
            $accion = $post['mod_eli'];//accion modificar o eliminar
            $gestion = $this->session->userData('gestion');
            //-------------------------------------------------RECIBIR MI DOCUMENTO
            $filename = $_FILES["userfile"]["name"];
            $file_basename = substr($filename, 0, strripos($filename, '.')); // get file extention
            $file_ext = substr($filename, strripos($filename, '.')); // get file name
            $filesize = $_FILES["userfile"]["size"];
            $allowed_file_types = array('.pdf', '.doc', '.jpg', '.JPG', '.png', '.JPEG');
            if ($accion != '0') {
                $file_eliminar = "archivos/obje_gestion/" . $accion;
                unlink($file_eliminar);
            }


            if (/*in_array($file_ext, $allowed_file_types) &&*/ ($filesize < 80000000)) { // Rename file
                $newfilename = '0G-' . $gestion . '-' . substr(md5(uniqid(rand())), 0, 5) . $file_ext;
                //--------------------------------$data_to_store['pa_ruta_archivo'] = "" . $newfilename;
                if (file_exists("archivos/obje_gestion/" . $newfilename)) {
                    echo "<script>alert('Ya existe este archivo')</script>";
                } else {
                    move_uploaded_file($_FILES["userfile"]["tmp_name"], "archivos/ogestion/" . $newfilename);
                    //guardar mi area urbana despues de las validaciones
                    $data['o_archivo_adjunto'] = $newfilename;
                    $this->db->WHERE('o_id', $o_id);
                    $this->db->UPDATE('objetivosgestion', $data);
                    $this->session->set_flashdata('guardar_archivo', 'EL ARCHIVO SE GUARDO CORRECTAMENTE');
                    redirect('prog/obje_gestion/' . $poa_id . '/' . $obje_id);
                }
            } elseif (empty($file_basename)) {
                echo "Selecciona un archivo para cargarlo.";
            } elseif ($filesize > 100000000) {
                $this->session->set_flashdata('guardar_archivo', 'EL ARCHIVO SE GUARDO CORRECTAMENTE');
                redirect('prog/pterminal/' . $poa_id . '/' . $obje_id . '/' . $o_id);
            } else {
                $mensaje = "Sólo estos tipos de archivo se permiten para la carga: " . implode(', ', $allowed_file_types);
                echo '<script>alert("' . $mensaje . '")</script>';
                unlink($_FILES["userfile"]["tmp_name"]);
            }

        } else {
            show_404();
        }
    }

    //FORMULARIO DE MODIFICACION DE OBJETIVO DE GESTION
    function form_mod_ogestion($poa_id, $obje_id, $o_id)
    {
        $data['poa_id'] = $poa_id;
        $data['obje_id'] = $obje_id;
        $data['dato_poa'] = $this->mpoa->dato_poa_id($poa_id);
        $data['dato_objest'] = $this->mobjetivos->dato_objetivo($obje_id);
        $data['dato_o'] = $this->mobjetivo_gestion->get_ogestion($o_id);
        $dato_o = $this->mobjetivo_gestion->get_ogestion($o_id);
        //-------------------------------------------
        $data['list_indicador'] = $this->mindicador->get_indicador();
        $data['list_funcionario'] = $this->model_funcionario->get_funcionarios();
        $linea_base = $dato_o[0]['o_linea_base'];
        $meta = $dato_o[0]['o_meta'];
        $data['programacion'] = $this->programacion_mensual($o_id, $linea_base, $meta);
        $ruta = 'programacion/prog_poa/red_objetivos/vmod_objgestion';
        $this->construir_vista($ruta, $data);

    }

    //ELIMINAR OBJETIVO DE GESTION
    function del_ogestion()
    {
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $post = $this->input->post();
            $o_id = $post['o_id'];
            $peticion = $this->mobjetivo_gestion->eliminar_ogestion($o_id);
            $data = array(
                'respuesta' => $peticion
            );
            echo json_encode($data);
        } else {
            show_404();
        }
    }

    //Eliminar Archivo
    function del_archivo()
    {
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $post = $this->input->post();
            $o_id = $post['id_ogestion'];
            $data = $this->mobjetivo_gestion->get_ogestion($o_id);
            $data = $data[0];
            if (strlen($data['o_archivo_adjunto']) != 0) {
                //guardar objetivo
                $this->mobjetivo_gestion->del_archivo($o_id);
                $file_eliminar = "archivos/ogestion/" . $data['o_archivo_adjunto'];
                unlink($file_eliminar);
            }
            $result = array(
                'respuesta' => 'true',
            );
            echo json_encode($result);
        } else {
            show_404();
        }
    }
}