<?php

class mision extends CI_Controller {
    public $rol = array('1' => '2','2' => '6','3' => '6');
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Users_model','',true);
        $this->load->model('menu_modelo');
        $this->load->model('programacion/model_mision');
         //llamar a mi menu
        $this->load->library('menu');
        $this->menu->const_menu(11);
    }
    
    public function vista_mision()
    {   
        if($this->rolfun($this->rol)){ 
            $data['mision']=$this->model_mision->pei_mision_get();
            $ruta = 'programacion/marco_estrategico/mision';
            $this->construir_vista($ruta,$data);
        }
        else{
            redirect('admin/dashboard');
        }
    }   
   function construir_vista($ruta,$data)
   {
       //----------------------------------- MENU-------------------------------
        $menu['enlaces'] = $this->menu->get_enlaces();
        $menu['subenlaces'] = $this->menu->get_sub_enlaces();
        $menu['titulo'] = 'PROGRAMACIÒN ';
        //-----------------------------------------------------------------------
        //armar vista
        $this->load->view('includes/header');
        $this->load->view('includes/menu_lateral',$menu);
        $this->load->view($ruta,$data);//contenido
        //$this->load->view('admin/mantenimiento/vprueba');//contenido
        $this->load->view('includes/footer');
    }

    public function editar_mision()
    {
        $post = $this->input->post();
        $nueva_mision = $post['vmision'];
        $query=$this->db->query('set datestyle to DMY');

        $data_to_store = array( 
                    'mision' => strtoupper($nueva_mision),
                    'ide' => $this->session->userdata('ide'),
                    'fun_id' => $this->session->userdata('fun_id'),
                    'g_id' => $this->session->userdata('gestion'),
                );
            $this->db->insert('historial_mision', $this->security->xss_clean($data_to_store));

        $this->model_mision->edita_mision($nueva_mision);

        $this->vista_mision();
    }

    function rolfun($rol)
    {
      $valor=false;
      for ($i=1; $i <=count($rol) ; $i++) { 
        $data = $this->Users_model->get_datos_usuario_roles($this->session->userdata('fun_id'),$rol[$i]);
        if(count($data)!=0){
          $valor=true;
          break;
        }
      }
      return $valor;
    }
}
    


