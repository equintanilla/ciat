<?php
//Reporte avance financiero del proyecto
class Creporte_avance_fin extends CI_Controller
{
    var $gestion;
    public function __construct()
    {
        parent:: __construct();
        if($this->session->userdata('fun_id')!=null){
        $this->load->model('programacion/model_proyecto');
        $this->load->model('programacion/model_reporte');
        $this->load->model('programacion/model_faseetapa');
        $this->load->model('programacion/model_componente');
        $this->load->model('programacion/mreporte_proy');
        $this->load->model('programacion/insumos/minsumos');
        $this->load->model('programacion/insumos/minsumos_delegado');
        $this->load->model('menu_modelo');
        $this->gestion = $this->session->userData('gestion');
        }else{
            redirect('/','refresh');
        }
    }

    function main($mod, $fun_id, $proy_id)
    {
      $enlaces = $this->menu_modelo->get_Modulos_programacion(1);
      $data['enlaces'] = $enlaces;
      $data['nro_fase'] = $this->model_faseetapa->nro_fase($proy_id); /// nro de fases y etapas registrados
      $data['proyecto'] = $this->model_proyecto->get_id_proyecto($proy_id);
      $data['dato_proy'] = $this->model_proyecto->get_id_proyecto($proy_id)[0];
      $data['id_f'] = $this->model_faseetapa->get_id_fase($proy_id); //// recupera datos de la tabla fase activa
      $data['mod'] = $mod;
      $data['componente'] = $this->model_componente->get_componente($data['id_f'][0]['id']);
      
      $titulo_proy=strtoupper($data['proyecto'][0]['tipo']);
      $data['titulo_proy'] = $titulo_proy;
      
      if ($mod == 1) {
          $this->load->view('admin/programacion/reportes/grafico_financiero/curva_s', $data); ///// Iframe
      } 
      elseif ($mod == 4) {
          $this->load->view('admin/programacion/reportes/grafico_financiero/curva_s', $data); ///// Iframe
      }
      else{
          redirect('admin/dashboard');
      }
    }

    /*============================= PARA EL REPORTE DE CURVA S FINANCIERO (Wilmer) ================================*/
    public function grafico_programacion_financiero($mod, $id_f, $proy_id)
    {
        $data['nro_fase'] = $this->model_faseetapa->nro_fase($proy_id); /// nro de fases y etapas registrados
        $data['proyecto'] = $this->model_proyecto->get_id_proyecto($proy_id);
        $data['unidad_responsable']=$this->model_proyecto->responsable_proy($proy_id,2); //// unidad responsable
        $data['unidad_ejecutora']=$this->model_proyecto->responsable_proy($proy_id,1);  //// unidad ejecutora
        $data['dato_proy'] = $this->model_proyecto->get_id_proyecto($proy_id)[0]; 
        $fase = $this->model_faseetapa->get_id_fase($proy_id); //// recupera datos de la tabla fase activa
        $data['mod'] = $mod;
        $data['componente'] = $this->model_componente->get_componente($fase[0]['id']);
        $data['años']=$fase[0]['pfec_fecha_fin']-$fase[0]['pfec_fecha_inicio']+1;
        $data['g_inicio']=$fase[0]['pfec_fecha_inicio'];
        //
        $data['dato_proyecto'] = $this->dato_proyecto($proy_id); ///// Dato Proyecto
        $data['tabla_insumo'] = $this->insumo_total($proy_id,$this->gestion); ///// Matriz Tabla Insumo
        $data['total'] = $this->tabla_total($proy_id,$this->gestion); //// Matriz Total
        if ($mod == 1) ///// GRAFICO ANUAL
        {
            $this->load->view('admin/programacion/reportes/grafico_financiero/rep_curva_s', $data);
        } 
        elseif($mod == 4) ///// GRAFICO PLURIANUAL
        {
            $this->load->view('admin/programacion/reportes/grafico_financiero/rep_curva_s', $data);
        }
    }

    /*---------------- PARA IMPRIMIR GRAFICO FINANCIEOR ------------------------------*/
    public function imprimir_programacion_financiero($mod, $id_f, $proy_id)
    {
       
        $data['proyecto'] = $this->model_proyecto->get_id_proyecto($proy_id);
        $fase = $this->model_faseetapa->get_id_fase($proy_id); //// recupera datos de la tabla fase activa
        
        $data['dato_proyecto'] = $this->dato_proyecto($proy_id); ///// Dato Proyecto
        $data['tabla_insumo'] = $this->insumo_total($proy_id,$this->gestion); ///// Matriz Tabla Insumo
        $data['total'] = $this->tabla_total($proy_id,$this->gestion); //// Matriz Total
        
        if ($mod == 1) ///// GRAFICO ANUAL
        {
            $this->load->view('admin/programacion/reportes/grafico_financiero/imprimir_curva_s', $data);
        } 
        elseif($mod == 4) ///// GRAFICO PLURIANUAL
        {
          $this->load->view('admin/programacion/reportes/grafico_financiero/imprimir_curva_s', $data);
        }
    }


    /*-------------------- TABLA TOTAL ANUAL ------------------------*/
    function tabla_total($proy_id,$gestion)
    {
        $fase = $this->model_faseetapa->get_id_fase($proy_id);
        $ms[0]='total';$ms[1]='mes1';$ms[2]='mes2';$ms[3]='mes3';$ms[4]='mes4';$ms[5]='mes5';$ms[6]='mes6';
        $ms[7]='mes7';$ms[8]='mes8';$ms[9]='mes9';$ms[10]='mes10';$ms[11]='mes11';$ms[12]='mes12';
        
        for($i=1;$i<=12;$i++)
        {
          $totalp[1][$i]=0; //// i
          $totalp[2][$i]=0; //// p
          $totalp[3][$i]=0; //// pa
          $totalp[4][$i]=0; //// %pa
          $totalp[5][$i]=0; //// e
          $totalp[6][$i]=0; //// ea
          $totalp[7][$i]=0; //// %ea
          $totalp[8][$i]=0; //// eficacia
          $totalp[9][$i]=0; //// Menor
          $totalp[10][$i]=0; //// Entre
          $totalp[11][$i]=0; //// Mayor
        }

        if($fase[0]['pfec_ejecucion']==1){
            $programado = $this->minsumos->sum_total_insumo_prog($proy_id,$gestion);
            $ejecutado = $this->minsumos->sum_total_insumo_ejec($proy_id,$gestion);
        }
        elseif ($fase[0]['pfec_ejecucion']==2) {
           $programado = $this->minsumos_delegado->sum_total_insumo_prog($proy_id,$gestion);
           $ejecutado = $this->minsumos_delegado->sum_total_insumo_ejec($proy_id,$gestion);
        }
        
        if(count($programado)!=0)
        {
          $suma_pfin=0; $suma_efin=0;
          for($i=1;$i<=12;$i++)
          {
              $totalp[1][$i]=$i; //// i
              $totalp[2][$i]=$programado[0][$ms[$i]]; /// programado
              $suma_pfin=$suma_pfin+$programado[0][$ms[$i]];
              $totalp[3][$i]=$suma_pfin; ///// Programado acumulado

            if($programado[0][$ms[0]]!=0)
            {
              $totalp[4][$i]=round((($totalp[3][$i]/$programado[0][$ms[0]])*100),2); ////// Programado Acumulado %
            }
            
            if(count($ejecutado)!=0){
                $totalp[5][$i]=$ejecutado[0][$ms[$i]]; /// Ejecutado
                $suma_efin=$suma_efin+$ejecutado[0][$ms[$i]];
                $totalp[6][$i]=$suma_efin; ///// Ejecutado acumulado
                
                if($programado[0][$ms[0]]!=0)
                {
                  $totalp[7][$i]=round((($totalp[6][$i]/$programado[0][$ms[0]])*100),2); ////// Ejecutado Acumulado %
                }

                if($totalp[4][$i]!=0)
                {
                  $totalp[8][$i]=round((($totalp[7][$i]/$totalp[4][$i])*100),2); ////// Eficacia
                }

                /*------------------------- eficacia -------------------------*/
                if($totalp[8][$i]<=75){$totalp[9][$i] = $totalp[8][$i];}
                if ($totalp[8][$i] >= 76 && $totalp[8][$i] <= 90.9) {$totalp[10][$i] = $totalp[8][$i];}
                if($totalp[8][$i] >= 91){$totalp[11][$i] = $totalp[8][$i];}
            }
          }
        }
        return $totalp;
    }
    
    public function dato_proyecto($proy_id)
    {
        $gestion = $this->session->userData('gestion');
        $proyecto = $this->model_proyecto->get_id_proyecto($proy_id); //// Datos del Proyecto
        $dato_proy = '
             <table class="datos_principales" style="table-layout:fixed;" border="1">
                <tr>
                    <td colspan="2">
                        <b>'.$proyecto[0]['proy_nombre'].'</b><br>
                        '.$proyecto[0]['tipo'].'
                    </td>
                </tr>
                <tr>
                    <td style="width:50%; text-align:center;"">
                    '.$proyecto[0]['proy_sisin'].'<br>
                        C&oacute;digo SISIN
                    </td>
                    <td style="width:50%; text-align:center;">
                        <b>'.$proyecto[0]['aper_programa'].''.$proyecto[0]['aper_proyecto'].''.$proyecto[0]['aper_actividad'].'</b><br>
                        Apertura Programatica
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;text-align:center;">
                        <b>'.$proyecto[0]['ue'].'</b><br>
                        Unidad Ejecutora
                    </td>
                    <td style="width:50%; text-align:center;">
                        <b>'.$proyecto[0]['ur'].'</b><br>
                        Unidad Responable
                    </td>
                </tr>
            </table><br>';
        return $dato_proy;
    }


    function insumo_total($proy_id,$gestion)
    {
        $total= $this->tabla_total($proy_id,$gestion);
        $tabla = '';   
        $tabla .='<div class="contenedor_datos">
                    <table class="change_order_items" style="width: 90%; font-size: 7pt;">
                        <tr class="even_row">
                            <td colspan=13>GESTI&Oacute;N '.$gestion.'</td>
                        </tr>
                        <tr class="even_row">
                            <td style="width:2%;">P/E</td>
                            <td style="width:5%;">ENERO</td>
                            <td style="width:5%;">FEBRERO</td>
                            <td style="width:5%;">MARZO</td>
                            <td style="width:5%;">ABRIL</td>
                            <td style="width:5%;">MAYO</td>
                            <td style="width:5%;">JUNIO</td>
                            <td style="width:5%;">JULIO</td>
                            <td style="width:5%;">AGOSTO</td>
                            <td style="width:5%;">SEPTIEMBRE</td>
                            <td style="width:5%;">OCTUBRE</td>
                            <td style="width:5%;">NOVIEMBRE</td>
                            <td style="width:5%;">DICIEMBRE</td>
                        </tr>
                        <tr class="odd_row">
                            <td>P.</td>';
                            for ($i=1; $i <=12 ; $i++) 
                            { 
                               $tabla .='<td>'.number_format($total[2][$i], 2, ',', '.').'</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="odd_row">
                            <td>P.A.</td>';
                            for ($i=1; $i <=12 ; $i++) 
                            { 
                               $tabla .='<td>'.number_format($total[3][$i], 2, ',', '.').'</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="odd_row">
                            <td>%P.A.</td>';
                            for ($i=1; $i <=12 ; $i++) 
                            { 
                               $tabla .='<td>'.$total[4][$i].'%</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="odd_row">
                            <td>E</td>';
                            for ($i=1; $i <=12 ; $i++) 
                            { 
                               $tabla .='<td>'.number_format($total[5][$i], 2, ',', '.').'</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="odd_row">
                            <td>E.A.</td>';
                            for ($i=1; $i <=12 ; $i++) 
                            { 
                               $tabla .='<td>'.number_format($total[6][$i], 2, ',', '.').'</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="odd_row">
                            <td>%E.A.</td>';
                            for ($i=1; $i <=12 ; $i++) 
                            { 
                               $tabla .='<td>'.$total[7][$i].'%</td>'; 
                            }
                        $tabla .='
                        </tr>
                        <tr class="odd_row">
                            <td>EFICACIA</td>';
                            for ($i=1; $i <=12 ; $i++) 
                            { 
                               $tabla .='<td>'.$total[8][$i].'%</td>'; 
                            }
                        $tabla .='
                        </tr>';
                    $tabla .='
                    </table>
                </div>';
     
        return $tabla;
    }
    /*=======================================================================================================================*/

    function get_mes($mes_id)
    {
        $mes[1] = 'enero';
        $mes[2] = 'febrero';
        $mes[3] = 'marzo';
        $mes[4] = 'abril';
        $mes[5] = 'mayo';
        $mes[6] = 'junio';
        $mes[7] = 'julio';
        $mes[8] = 'agosto';
        $mes[9] = 'septiembre';
        $mes[10] = 'octubre';
        $mes[11] = 'noviembre';
        $mes[12] = 'diciembre';
        return $mes[$mes_id];
    }

    function get_eficacia($efi)
    {
        if ($efi <= 75) {
            $d['menor'] = "{y: " . $efi . ", color: 'red'},";
            $d['entre'] = "{y: 0, color: 'yellow'},";
            $d['mayor'] = "{y: 0, color: 'green'},";
        }
        if ($efi >= 76 && $efi <= 90) {
            $d['entre'] = "{y: " . $efi . ", color: 'yellow'},";
            $d['menor'] = "{y: 0, color: 'red'},";
            $d['mayor'] = "{y: 0, color: 'green'},";
        }
        if ($efi >= 91) {
            $d['mayor'] = "{y: " . $efi . ", color: 'green'},";
            $d['entre'] = "{y: 0, color: 'yellow'},";
            $d['menor'] = "{y: 0, color: 'red'},";
        }
        return $d;
    }

}