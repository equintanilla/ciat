<?php
class Producto extends CI_Controller { 
    public $rol = array('1' => '3','2' => '4');
    public function __construct ()
    { 
        parent::__construct();
        if($this->session->userdata('fun_id')!=null){
            $this->load->model('Users_model','',true);
            if($this->rolfun($this->rol)){ 
            $this->load->library('pdf2');
            $this->load->model('programacion/model_proyecto');
            $this->load->model('programacion/model_producto');
            $this->load->model('programacion/model_faseetapa');
            $this->load->model('programacion/model_componente');
            $this->load->model('programacion/model_actividad');
            $this->load->model('programacion/insumos/minsumos');
            $this->load->model('menu_modelo');
            }
            else{
            redirect('admin/dashboard');
            }
        }else{
                redirect('/','refresh');
        }
        
    }

    /*----Actualiza la ponderación de componentes----*/
    public function actualiza_ponderacion($pfec_id,$com_id,$prod_id)
    {
        /*================================ ACTUALIZANDO PONDERACIONES ACTIVIDADES ===============================*/
        $suma = $this->model_actividad->suma_monto_ponderado_total($prod_id);
        $act = $this->model_actividad->list_act_anual($prod_id);
        $ponderacion=0;
        foreach ($act as $row)
        {
            $ponderacion=round((($row['act_pres_p']/$suma[0]['monto_total'])*100),2);
            $update_act = array(
                'act_ponderacion' => $ponderacion
            );

            $this->db->where('act_id', $row['act_id']);
            $this->db->update('_actividades', $update_act);

        }
        /*===========================================================================================*/
        /*========================================= ACTUALIZANDO PONDERACIONES PRODUCTOS ========================*/
        $productos= $this->model_producto->list_prod($com_id);
        $sumatoria_total=0;
        foreach ($productos as $rowp)
        {
            $suma_pa = $this->model_actividad->suma_monto_ponderado_total($rowp['prod_id']);
            $sumatoria_total=$sumatoria_total+$suma_pa[0]['monto_total'];
        }

        $ponderacion=0;
        foreach ($productos as $rowp)
        {
            $suma_prod = $this->model_actividad->suma_monto_ponderado_total($rowp['prod_id']);
            $ponderacion=round((($suma_prod[0]['monto_total']/$sumatoria_total)*100),2);

            $update_prod = array(
                'prod_ponderacion' => $ponderacion
            );
            $this->db->where('prod_id', $rowp['prod_id']);
            $this->db->update('_productos', $update_prod);
        }
        /*============================================================================================================*/

        /*========================================= ACTUALIZANDO PONDERACIONES COMPONENTES =============================*/
        $componente= $this->model_componente->componentes_id($pfec_id);
        $sumatoria_comp=0;
        foreach ($componente as $rowc)
        {
            $productos= $this->model_producto->list_prod($rowc['com_id']);
            $sumatoria_p=0;
            foreach ($productos as $rowp)
            {
                $suma_pa = $this->model_actividad->suma_monto_ponderado_total($rowp['prod_id']);
                $sumatoria_p=$sumatoria_p+$suma_pa[0]['monto_total'];
            }
            $sumatoria_comp=$sumatoria_comp+$sumatoria_p;
        }

        $ponderacion=0;
        foreach ($componente as $rowc)
        {
            $productos= $this->model_producto->list_prod($rowc['com_id']);
            $sumatoria_p=0;
            foreach ($productos as $rowp)
            {
                $suma_pa = $this->model_actividad->suma_monto_ponderado_total($rowp['prod_id']);
                $sumatoria_p=$sumatoria_p+$suma_pa[0]['monto_total'];
            }
            $ponderacion=round((($sumatoria_p/$sumatoria_comp)*100),2);
            $update_comp = array(
                'com_ponderacion' => $ponderacion
            );
            $this->db->where('com_id', $rowc['com_id']);
            $this->db->update('_componentes', $update_comp);

        }
    }
 /*===================================== LISTA DE PRODUCTOS=============================================*/
  public function lista_productos($mod,$id_f,$id_p,$id_c)
  { 
    if($mod!='' & $id_f!='' & $id_p!='' & $id_c!=''){     
        $enlaces=$this->menu_modelo->get_Modulos_programacion(1);
        $data['enlaces'] = $enlaces;
        $data['nro_fase']= $this->model_faseetapa->nro_fase($id_p); /// nro de fases y etapas registrados
        $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p); 
        $data['id_f'] = $this->model_faseetapa->get_id_fase($id_p); //// recupera datos de la tabla fase activa
        $data['mod']=$mod;

        $data['id_c']=$id_c;
        $data['componente'] = $this->model_componente->get_componente($id_c);

        $titulo_proy=strtoupper($data['proyecto'][0]['tipo']);
        $data['titulo_proy'] = $titulo_proy;
        $data['productos'] = $this->model_producto->list_prod($id_c);

        if($mod==1) ///// Productos Anual
        {
            $this->load->view('admin/programacion/producto/list_productos', $data);
        }
        elseif ($mod==4) ///// Productos Multi Anual
        {
            $this->load->view('admin/programacion/producto/list_productos_m', $data);
        }
    }
    else{
      redirect('admin/dashboard');
    }
  }


 /*---------------------------------- NUEVO PRODUCTO -------------------------------------*/
  public function new_productos($mod,$id_f,$id_p,$id_c) /////// Formulario de registro Productos
  {
    if($mod!='' & $id_f!='' & $id_p!='' & $id_c!=''){         
        $enlaces=$this->menu_modelo->get_Modulos_programacion(1);
        $data['enlaces'] = $enlaces;
        $data['nro_fase']= $this->model_faseetapa->nro_fase($id_p); /// nro de fases y etapas registrados
        $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p); 
        $data['id_f'] = $this->model_faseetapa->get_id_fase($id_p); //// recupera datos de la tabla fase activa
        $data['mod']=$mod;
        $data['id_c']=$id_c;
        $data['componente'] = $this->model_componente->get_componente($id_c);
        $data['indi']= $this->model_proyecto->indicador(); /// indicador

        $data['p_terminal']= $this->model_producto->prod_terminal($data['proyecto'][0]['aper_programa']); /// lista de productos terminales
        $data['componente'] = $this->model_componente->get_componente($id_c);
        $data['mod']=$mod; 

        $titulo_proy=strtoupper($data['proyecto'][0]['tipo']);
        $data['titulo_proy'] = $titulo_proy;
        //load the view
        $this->load->view('admin/programacion/producto/form_prod', $data);
    }
    else{
      redirect('admin/dashboard');
    }    
  }
 
  /*---------------------------------- VALIDA PRODUCTO -------------------------------------*/
  public function valida_producto()
  {
        if ($this->input->server('REQUEST_METHOD') === 'POST') ///// DATOS GENERALES
        {
            $this->form_validation->set_rules('prod', 'Producto', 'required|trim');
            $this->form_validation->set_rules('tipo_i', 'Tipo de Indicador', 'required|trim');
            $this->form_validation->set_rules('met', 'Meta', 'required|trim');

            if ($this->form_validation->run())
            {
                /*==================================== FORMULARIO PRODUCTOS =====================================*/
                  $data_to_store = array(
                    'com_id' => $this->input->post('id_c'),
                    'prod_producto' => strtoupper($this->input->post('prod')),
                    'indi_id' => $this->input->post('tipo_i'),
                    'prod_indicador' => strtoupper($this->input->post('indicador')),
                    'prod_formula' => strtoupper($this->input->post('formula')),
                    'prod_linea_base' => $this->input->post('lb'),
                    'prod_meta' => $this->input->post('met'),
                    'prod_fuente_verificacion' => strtoupper($this->input->post('verificacion')), 
                    'prod_supuestos' => strtoupper($this->input->post('supuestos')),
                    'pt_id' => $this->input->post('p_t'),
                    'prod_total_casos' => strtoupper($this->input->post('c_a')),
                    'prod_casos_favorables' => strtoupper($this->input->post('c_b')),
                    'prod_casos_desfavorables' => strtoupper($this->input->post('c_c')),
                    'prod_denominador' => $this->input->post('den'),
                    'fun_id' => $this->session->userdata("fun_id"),
                );
                  $this->db->insert('_productos', $data_to_store); 
                /*==================================== END FORMULARIO PRODUCTOS =====================================*/
                  $id_pr=$this->db->insert_id(); ////// id del producto

                $conf=$this->model_proyecto->configuracion(); //// configuracion gestion
                $nro_p=$conf[0]['conf_producto']+1;

                $update_conf = array('conf_producto' => $nro_p);
                        $this->db->where('ide', $this->session->userdata("gestion"));
                        $this->db->update('configuracion', $update_conf);
                        
                $gestion=$this->input->post('gest');

                if ( !empty($_POST["m1"]) && is_array($_POST["m1"]) ) 
                {
                    foreach ( array_keys($_POST["m1"]) as $como  )
                    {
                        if($_POST["m1"][$como]!=0)
                        {
                            $this->model_producto->add_prod_gest($id_pr,$gestion,1,$_POST["m1"][$como]);
                        }
                        if($_POST["m2"][$como]!=0)
                        {
                            $this->model_producto->add_prod_gest($id_pr,$gestion,2,$_POST["m2"][$como]);
                        }
                        if($_POST["m3"][$como]!=0)
                        {
                            $this->model_producto->add_prod_gest($id_pr,$gestion,3,$_POST["m3"][$como]);
                        }
                        if($_POST["m4"][$como]!=0)
                        {
                            $this->model_producto->add_prod_gest($id_pr,$gestion,4,$_POST["m4"][$como]);
                        }
                        if($_POST["m5"][$como]!=0)
                        {
                            $this->model_producto->add_prod_gest($id_pr,$gestion,5,$_POST["m5"][$como]);
                        }
                        if($_POST["m6"][$como]!=0)
                        {
                            $this->model_producto->add_prod_gest($id_pr,$gestion,6,$_POST["m6"][$como]);
                        }
                        if($_POST["m7"][$como]!=0)
                        {
                            $this->model_producto->add_prod_gest($id_pr,$gestion,7,$_POST["m7"][$como]);
                        }
                        if($_POST["m8"][$como]!=0)
                        {
                            $this->model_producto->add_prod_gest($id_pr,$gestion,8,$_POST["m8"][$como]);
                        }
                        if($_POST["m9"][$como]!=0)
                        {
                            $this->model_producto->add_prod_gest($id_pr,$gestion,9,$_POST["m9"][$como]);
                        }
                        if($_POST["m10"][$como]!=0)
                        {
                            $this->model_producto->add_prod_gest($id_pr,$gestion,10,$_POST["m10"][$como]);
                        }
                        if($_POST["m11"][$como]!=0)
                        {
                            $this->model_producto->add_prod_gest($id_pr,$gestion,11,$_POST["m11"][$como]);
                        }
                        if($_POST["m12"][$como]!=0)
                        {
                            $this->model_producto->add_prod_gest($id_pr,$gestion,12,$_POST["m12"][$como]);
                        }
                            $gestion++;
                    }
                }

                /*========================================= ACTUALIZANDO PONDERACIONES ========================*/
                $productos= $this->model_producto->list_prod($this->input->post('id_c'));
                $sumatoria_total=0;
                foreach ($productos as $rowp)
                {
                    $suma_pa = $this->model_actividad->suma_monto_ponderado_total($rowp['prod_id']);
                    $sumatoria_total=$sumatoria_total+$suma_pa[0]['monto_total'];
                }

                $ponderacion=0;
                foreach ($productos as $rowp)
                {
                    $suma_prod = $this->model_actividad->suma_monto_ponderado_total($rowp['prod_id']);
                    $ponderacion=round((($suma_prod[0]['monto_total']/$sumatoria_total)*100),2);
                            
                            $update_prod = array(
                                'prod_ponderacion' => $ponderacion
                            );
                            $this->db->where('prod_id', $rowp['prod_id']);
                            $this->db->update('_productos', $update_prod);
                }
                /*==============================================================================================*/

                  redirect('admin/prog/list_prod/'.$this->input->post('mod').'/'.$this->input->post('id_f')."/".$this->input->post('id_p').'/'.$this->input->post('id_c').'/true');
            }
            else
            {
                 
                  redirect('admin/prog/new_prod/'.$this->input->post('mod').'/'.$this->input->post('id_f')."/".$this->input->post('id_p').'/'.$this->input->post('id_c').'/false');
            }
        }
   }

 /*---------------------------------- MODIFICAR PRODUCTO -------------------------------------*/
public function update($mod,$id_f,$id_p,$id_c,$id_pr,$p)
{
    if($mod!='' & $id_f!='' & $id_p!='' & $id_c!='' & $id_pr!='' & $p!=''){         
        $enlaces=$this->menu_modelo->get_Modulos_programacion(1);
        $data['enlaces'] = $enlaces;
        $data['nro_fase']= $this->model_faseetapa->nro_fase($id_p); /// nro de fases y etapas registrados
        $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p); 
        $data['id_f'] = $this->model_faseetapa->get_id_fase($id_p); //// recupera datos de la tabla fase activa
        $data['id_c']=$id_c;
        $data['componente'] = $this->model_componente->get_componente($id_c);
        $data['producto'] = $this->model_producto->get_producto_id($id_pr);
        $data['mod']=$mod;
        $data['indi']= $this->model_proyecto->indicador(); /// indicador
        $data['p_terminal']= $this->model_producto->prod_terminal($data['proyecto'][0]['aper_programa']); /// lista de productos terminales
        
        $titulo_proy=strtoupper($data['proyecto'][0]['tipo']);
        $data['titulo_proy'] = $titulo_proy;

        $this->load->view('admin/programacion/producto/edit_prod', $data);
    }
    else{
      redirect('admin/dashboard');
    }  
}
 /*---------------------------------- VALIDAR MODIFICACION PRODUCTO -------------------------------------*/
public function modificar_producto()
{
        if ($this->input->server('REQUEST_METHOD') === 'POST') ///// DATOS GENERALES
        {
            $this->form_validation->set_rules('id_pr', 'Id Producto', 'required|trim');

            if ($this->form_validation->run())
            {       /*============================= UPDATE PRODUCTOS ===============================*/
                    $update_prod = array(
                                'com_id' => $this->input->post('id_c'),
                                'prod_producto' => strtoupper($this->input->post('prod')),
                                'indi_id' => $this->input->post('tipo_i'),
                                'prod_indicador' => strtoupper($this->input->post('indicador')),
                                'prod_formula' => strtoupper($this->input->post('formula')),
                                'prod_linea_base' => $this->input->post('lb'),
                                'prod_meta' => $this->input->post('met'),
                                'prod_fuente_verificacion' => strtoupper($this->input->post('verificacion')),
                                'prod_supuestos' => strtoupper($this->input->post('supuestos')),
                                //'prod_ponderacion' => $this->input->post('pn_cion'),
                                'prod_denominador' => $this->input->post('den'),
                                'pt_id' => $this->input->post('p_t'),
                                'prod_total_casos' => strtoupper($this->input->post('c_a')), 
                                'prod_casos_favorables' => strtoupper($this->input->post('c_b')),
                                'prod_casos_desfavorables' => strtoupper($this->input->post('c_c')),
                                'estado' => '2',
                                'fun_id' => $this->session->userdata("fun_id"),
                                );
                        $this->db->where('prod_id', $this->input->post('id_pr'));
                        $this->db->update('_productos', $update_prod);
                    /*============================= END UPDATE PRODUCTOS ===============================*/

                    /*============================= UPDATE PGESTION RODUCTOS ===============================*/
                       $gestion=$this->input->post('gest');
                       $this->model_producto->delete_prod_gest($this->input->post('id_pr'));

                    if ( !empty($_POST["m1"]) && is_array($_POST["m1"]) ) 
                        {
                            foreach ( array_keys($_POST["m1"]) as $como  )
                            {
                                if($_POST["m1"][$como]!=0)
                                {
                                    $this->model_producto->add_prod_gest($this->input->post('id_pr'),$gestion,1,$_POST["m1"][$como]);
                                }
                                if($_POST["m2"][$como]!=0)
                                {
                                    $this->model_producto->add_prod_gest($this->input->post('id_pr'),$gestion,2,$_POST["m2"][$como]);
                                }
                                if($_POST["m3"][$como]!=0)
                                {
                                    $this->model_producto->add_prod_gest($this->input->post('id_pr'),$gestion,3,$_POST["m3"][$como]);
                                }
                                if($_POST["m4"][$como]!=0)
                                {
                                    $this->model_producto->add_prod_gest($this->input->post('id_pr'),$gestion,4,$_POST["m4"][$como]);
                                }
                                if($_POST["m5"][$como]!=0)
                                {
                                    $this->model_producto->add_prod_gest($this->input->post('id_pr'),$gestion,5,$_POST["m5"][$como]);
                                }
                                if($_POST["m6"][$como]!=0)
                                {
                                    $this->model_producto->add_prod_gest($this->input->post('id_pr'),$gestion,6,$_POST["m6"][$como]);
                                }
                                if($_POST["m7"][$como]!=0)
                                {
                                    $this->model_producto->add_prod_gest($this->input->post('id_pr'),$gestion,7,$_POST["m7"][$como]);
                                }
                                if($_POST["m8"][$como]!=0)
                                {
                                    $this->model_producto->add_prod_gest($this->input->post('id_pr'),$gestion,8,$_POST["m8"][$como]);
                                }
                                if($_POST["m9"][$como]!=0)
                                {
                                    $this->model_producto->add_prod_gest($this->input->post('id_pr'),$gestion,9,$_POST["m9"][$como]);
                                }
                                if($_POST["m10"][$como]!=0)
                                {
                                    $this->model_producto->add_prod_gest($this->input->post('id_pr'),$gestion,10,$_POST["m10"][$como]);
                                }
                                if($_POST["m11"][$como]!=0)
                                {
                                    $this->model_producto->add_prod_gest($this->input->post('id_pr'),$gestion,11,$_POST["m11"][$como]);
                                }
                                if($_POST["m12"][$como]!=0)
                                {
                                    $this->model_producto->add_prod_gest($this->input->post('id_pr'),$gestion,12,$_POST["m12"][$como]);
                                }
                                    $gestion++;
                            }
                        }
                    /*============================= END UPDATE PGESTION RODUCTOS ===============================*/
                    redirect('admin/prog/list_prod/'.$this->input->post('mod').'/'.$this->input->post('id_f')."/".$this->input->post('id_p').'/'.$this->input->post('id_c').'/true');
            }
            else
            {
                  redirect('admin/prog/mod_prod/'.$this->input->post('mod').'/'.$this->input->post('id_f')."/".$this->input->post('id_p').'/'.$this->input->post('id_c').'/'.$this->input->post('id_pr').'/false');
            }
        }
   }

    /*============================================== LISTA DE PRODUCTOS - EJECUCION FISICA =======================================================*/
    public function lista_prod_efisica($mod,$id_f,$id_p)
    {
        if($mod!='' & $id_f!='' & $id_p!=''){     
            $enlaces=$this->menu_modelo->get_Modulos_programacion(1);
            $data['enlaces'] = $enlaces;
            $data['nro_fase']= $this->model_faseetapa->nro_fase($id_p); /// nro de fases y etapas registrados
            $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p); 
            $data['id_f'] = $this->model_faseetapa->get_id_fase($id_p); //// recupera datos de la tabla fase activa
            $data['mod']=$mod;

            $titulo_proy=strtoupper($data['proyecto'][0]['tipo']);
            $data['titulo_proy'] = $titulo_proy;

            if($mod==1)
            {
                $this->load->view('admin/programacion/ejecucion/fisica/productos/list_productos', $data); 
            }
            elseif ($mod==4) 
            {
                $this->load->view('admin/programacion/ejecucion/fisica/productos/list_productos_m', $data); 
            }
        }
        else{
        redirect('admin/dashboard');
        }
    }
    /*===========================================================================================================================================*/

   /*=============================================== EJECUCION DEL PRODUCTO ==================================================================*/

    public function ejecucion_producto($mod,$id_f,$id_p,$id_pr,$gest)
    {
        if($mod!='' & $id_f!='' & $id_p!='' & $id_pr!='' & $gest!=''){         
            $enlaces=$this->menu_modelo->get_Modulos_programacion(1);
            $data['enlaces'] = $enlaces;
            $data['nro_fase']= $this->model_faseetapa->nro_fase($id_p); /// nro de fases y etapas registrados
            $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p); 
            $data['id_f'] = $this->model_faseetapa->get_id_fase($id_p); //// recupera datos de la tabla fase activa
            $data['producto'] = $this->model_producto->get_producto_id($id_pr);
            $data['mod']=$mod;
            $data['gestion']=$gest;
            $data['programado']=$this->model_producto->prod_prog_mensual($id_pr,$gest);
            $data['ejecutado']=$this->model_producto->prod_ejec_mensual($id_pr,$gest);
            $data['nro_ejec']=$this->model_producto->nro_prod_ejec_mensual($id_pr,$gest);

            $titulo_proy=strtoupper($data['proyecto'][0]['tipo']);
            $data['titulo_proy'] = $titulo_proy;

            $this->load->view('admin/programacion/ejecucion/fisica/productos/ejec_prod', $data);
        }
        else{
        redirect('admin/dashboard');
        }  
    }
    /*===========================================================================================================================================*/
/*==================================================== VALIDA EJECUCION DEL PRODUCTO =================================================================*/
public function valida_ejecucion_producto()
{
        if ($this->input->server('REQUEST_METHOD') === 'POST') ///// DATOS GENERALES
        {
            $this->form_validation->set_rules('id_f', 'Id Fase', 'required|trim');
            $this->form_validation->set_rules('id_p', 'Id proyecto', 'required|trim');
            $this->form_validation->set_rules('id_pr', 'Id producto', 'required|trim');

            if ($this->form_validation->run())
            {       /*============================= UPDATE PRODUCTOS ===============================*/
                $this->model_producto->delete_prod_ejec_gest($this->input->post('id_pr'),$this->input->post('gest'));
                $producto = $this->model_producto->get_producto_id($this->input->post('id_pr')); 
                $id_pr=$this->input->post('id_pr');
                $gestion=$this->input->post('gest');      
                    if($producto[0]['indi_id']==1)
                    {
                        if($this->input->post('e1')!=0)
                        {
                            $this->model_producto->add_prod_ejec_gest($id_pr,$gestion,1,$this->input->post('e1'),0,0);
                        }
                        if($this->input->post('e2')!=0)
                        {
                            $this->model_producto->add_prod_ejec_gest($id_pr,$gestion,2,$this->input->post('e2'),0,0);
                        }
                        if($this->input->post('e3')!=0)
                        {
                            $this->model_producto->add_prod_ejec_gest($id_pr,$gestion,3,$this->input->post('e3'),0,0);
                        }
                        if($this->input->post('e4')!=0)
                        {
                            $this->model_producto->add_prod_ejec_gest($id_pr,$gestion,4,$this->input->post('e4'),0,0);
                        }
                        if($this->input->post('e5')!=0)
                        {
                            $this->model_producto->add_prod_ejec_gest($id_pr,$gestion,5,$this->input->post('e5'),0,0);
                        }
                        if($this->input->post('e6')!=0)
                        {
                            $this->model_producto->add_prod_ejec_gest($id_pr,$gestion,6,$this->input->post('e6'),0,0);
                        }
                        if($this->input->post('e7')!=0)
                        {
                            $this->model_producto->add_prod_ejec_gest($id_pr,$gestion,7,$this->input->post('e7'),0,0);
                        }
                        if($this->input->post('e8')!=0)
                        {
                            $this->model_producto->add_prod_ejec_gest($id_pr,$gestion,8,$this->input->post('e8'),0,0);
                        }
                        if($this->input->post('e9')!=0)
                        {
                            $this->model_producto->add_prod_ejec_gest($id_pr,$gestion,9,$this->input->post('e9'),0,0);
                        }
                        if($this->input->post('e10')!=0)
                        {
                            $this->model_producto->add_prod_ejec_gest($id_pr,$gestion,10,$this->input->post('e10'),0,0);
                        }
                        if($this->input->post('e11')!=0)
                        {
                            $this->model_producto->add_prod_ejec_gest($id_pr,$gestion,11,$this->input->post('e11'),0,0);
                        }
                        if($this->input->post('e12')!=0)
                        {
                            $this->model_producto->add_prod_ejec_gest($id_pr,$gestion,12,$this->input->post('e12'),0,0);
                        }
                    }
                    elseif ($producto[0]['indi_id']==2) 
                    {
                        if($producto[0]['prod_denominador']==0) /// variable
                        {
                            $e1=(($this->input->post('b1')/$this->input->post('a1'))*$this->input->post('p1'));
                            $e2=(($this->input->post('b2')/$this->input->post('a2'))*$this->input->post('p2'));
                            $e3=(($this->input->post('b3')/$this->input->post('a3'))*$this->input->post('p3'));
                            $e4=(($this->input->post('b4')/$this->input->post('a4'))*$this->input->post('p4'));
                            $e5=(($this->input->post('b5')/$this->input->post('a5'))*$this->input->post('p5'));
                            $e6=(($this->input->post('b6')/$this->input->post('a6'))*$this->input->post('p6'));
                            $e7=(($this->input->post('b7')/$this->input->post('a7'))*$this->input->post('p7'));
                            $e8=(($this->input->post('b8')/$this->input->post('a8'))*$this->input->post('p8'));
                            $e9=(($this->input->post('b9')/$this->input->post('a9'))*$this->input->post('p9'));
                            $e10=(($this->input->post('b10')/$this->input->post('a10'))*$this->input->post('p10'));
                            $e11=(($this->input->post('b11')/$this->input->post('a11'))*$this->input->post('p11'));
                            $e12=(($this->input->post('b12')/$this->input->post('a12'))*$this->input->post('p12'));
                        }
                        elseif ($producto[0]['prod_denominador']==1) //// Fijo
                        {
                            $e1=(($this->input->post('b1')/$this->input->post('a1'))*100);
                            $e2=(($this->input->post('b2')/$this->input->post('a2'))*100);
                            $e3=(($this->input->post('b3')/$this->input->post('a3'))*100);
                            $e4=(($this->input->post('b4')/$this->input->post('a4'))*100);
                            $e5=(($this->input->post('b5')/$this->input->post('a5'))*100);
                            $e6=(($this->input->post('b6')/$this->input->post('a6'))*100);
                            $e7=(($this->input->post('b7')/$this->input->post('a7'))*100);
                            $e8=(($this->input->post('b8')/$this->input->post('a8'))*100);
                            $e9=(($this->input->post('b9')/$this->input->post('a9'))*100);
                            $e10=(($this->input->post('b10')/$this->input->post('a10'))*100);
                            $e11=(($this->input->post('b11')/$this->input->post('a11'))*100);
                            $e12=(($this->input->post('b12')/$this->input->post('a12'))*100);
                        }
                        /*-------------------------------- EJECUCION RELATIVO ---------------------------*/
                        if($this->input->post('a1')!=0)
                        {
                            $this->model_producto->add_prod_ejec_gest($id_pr,$gestion,1,$e1,$this->input->post('a1'),$this->input->post('b1'));
                        }
                        if($this->input->post('a2')!=0)
                        {
                            $this->model_producto->add_prod_ejec_gest($id_pr,$gestion,2,$e2,$this->input->post('a2'),$this->input->post('b2'));
                        }
                        if($this->input->post('a3')!=0)
                        {
                            $this->model_producto->add_prod_ejec_gest($id_pr,$gestion,3,$e3,$this->input->post('a3'),$this->input->post('b3'));
                        }
                        if($this->input->post('a4')!=0)
                        {
                            $this->model_producto->add_prod_ejec_gest($id_pr,$gestion,4,$e4,$this->input->post('a4'),$this->input->post('b4'));
                        }
                        if($this->input->post('a5')!=0)
                        {
                            $this->model_producto->add_prod_ejec_gest($id_pr,$gestion,5,$e5,$this->input->post('a5'),$this->input->post('b5'));
                        }
                        if($this->input->post('a6')!=0)
                        {
                            $this->model_producto->add_prod_ejec_gest($id_pr,$gestion,6,$e6,$this->input->post('a6'),$this->input->post('b6'));
                        }
                        if($this->input->post('a7')!=0)
                        {
                            $this->model_producto->add_prod_ejec_gest($id_pr,$gestion,7,$e7,$this->input->post('a7'),$this->input->post('b7'));
                        }
                        if($this->input->post('a8')!=0)
                        {
                            $this->model_producto->add_prod_ejec_gest($id_pr,$gestion,8,$e8,$this->input->post('a8'),$this->input->post('b8'));
                        }
                        if($this->input->post('a9')!=0)
                        {
                            $this->model_producto->add_prod_ejec_gest($id_pr,$gestion,9,$e9,$this->input->post('a9'),$this->input->post('b9'));
                        }
                        if($this->input->post('a10')!=0)
                        {
                            $this->model_producto->add_prod_ejec_gest($id_pr,$gestion,10,$e10,$this->input->post('a10'),$this->input->post('b10'));
                        }
                        if($this->input->post('a11')!=0)
                        {
                            $this->model_producto->add_prod_ejec_gest($id_pr,$gestion,11,$e11,$this->input->post('a11'),$this->input->post('b11'));
                        }
                        if($this->input->post('a12')!=0)
                        {
                            $this->model_producto->add_prod_ejec_gest($id_pr,$gestion,12,$e12,$this->input->post('a12'),$this->input->post('b12'));
                        }
                         /*----------------------------------------------------------------------------------------------*/
                        
                    }
                    /*============================= END UPDATE PGESTION RODUCTOS ===============================*/

                    redirect('admin/prog/efisica/'.$this->input->post('mod').'/'.$this->input->post('id_f')."/".$this->input->post('id_p').'/true');
            }
            else
            {
                  redirect('admin/prog/mod/'.$this->input->post('mod').'/'.$this->input->post('id_f')."/".$this->input->post('id_p').'/'.$this->input->post('id_c').'/false');
            }
        }
}/*===========================================================================================================================================*/

  /*=================================== ELIMINA PRODUCTO ==================================================*/
    public function delete_producto($mod,$pfec_id,$proy_id,$com_id,$prod_id){
        $fase = $this->model_faseetapa->get_id_fase($proy_id); //// DATOS DE LA FASE ACTIVA

        $actividades = $this->model_actividad->list_act_anual($prod_id); //// Actividades 
        foreach ($actividades as $rowa)
        {
            if($fase[0]['pfec_ejecucion']==1) //// directo
            {
                $insumos = $this->model_actividad->imsumo_actividad($rowa['act_id']); //// Insumo actividad
            }
            elseif($fase[0]['pfec_ejecucion']==2)
            {
                $insumos = $this->model_componente->imsumo_componente($com_id); //// Insumo Componente
            }

            foreach ($insumos as $rowi) {
                $ins_gestion = $this->minsumos->list_insumos_gestion($rowi['ins_id']);
                foreach ($ins_gestion as $row)
                {
                    // echo $row['insg_id'].'--'.$row['ins_id'].'-'.$row['g_id'].'--'.$row['insg_monto_prog']."<br>";
                    $ins_fin = $this->minsumos->list_insumo_financiamiento($row['insg_id']);
                    if(count($ins_fin)!=0)
                    {
                        /*----------------- ELIMINA IFIN PROG MES---------------*/
                        $this->db->where('ifin_id', $ins_fin[0]['ifin_id']);
                        $this->db->delete('ifin_prog_mes');
                        /*------------------------------------------------------*/

                        /*----------------- ELIMINA IFIN EJEC MES---------------*/
                        $this->db->where('ifin_id', $ins_fin[0]['ifin_id']);
                        $this->db->delete('ifin_ejec_mes');
                        /*------------------------------------------------------*/

                        /*----------------- ELIMINA IFIN PROG MES---------------*/
                        $this->db->where('ifin_id', $ins_fin[0]['ifin_id']);
                        $this->db->delete('insumo_financiamiento');
                        /*------------------------------------------------------*/
                      //  echo "-----insumo fin : ".$ins_fin[0]['ifin_id'].'-'.$ins_fin[0]['insg_id'].'-'.$ins_fin[0]['ifin_monto'];
                    }

                    /*----------------- ELIMINA INS GESTION---------------*/
                        $this->db->where('insg_id', $row['insg_id']);
                        $this->db->delete('insumo_gestion');
                    /*------------------------------------------------------*/
                   // 
                }
                    if($fase[0]['pfec_ejecucion']==1) //// directo
                    {
                        /*----------------- ELIMINA INSUMO ACTIVIDAD ---------------*/
                        $this->db->where('ins_id', $rowi['ins_id']);
                        $this->db->delete('_insumoactividad');
                        /*----------------------------------------------------------*/
                    }
                    elseif ($fase[0]['pfec_ejecucion']==2) /// Delegado
                    {
                       /*----------------- ELIMINA INSUMO COMPONENTE ---------------*/
                        $this->db->where('ins_id', $rowi['ins_id']);
                        $this->db->delete('insumocomponente');
                        /*-----------------------------------------------------------*/
                    }

                    /*----------------- ELIMINA INS GESTION---------------*/
                        $this->db->where('ins_id', $rowi['ins_id']);
                        $this->db->delete('insumos');
                    /*------------------------------------------------------*/
            }

            /*------------ ELIMINA ACTIVIDAD PROGRAMADO -----------*/
            $this->db->where('act_id', $rowa['act_id']);
            $this->db->delete('act_programado_mensual');

            $this->db->where('act_id', $rowa['act_id']);
            $this->db->delete('act_ejecutado_mensual');
            /*---------------------------------------------------*/
            /*----------------- ELIMINA ACTIVIDAD ---------------*/
            $this->db->where('act_id', $rowa['act_id']);
            $this->db->delete('_actividades');
            /*---------------------------------------------------*/

        }

        if(count($this->model_actividad->list_act_anual($prod_id))==0)
        {
            /*------------ ELIMINA PRODUCTO PROGRAMADO -----------*/
            $this->db->where('prod_id', $prod_id);
            $this->db->delete('prod_programado_mensual');

            $this->db->where('prod_id', $prod_id);
            $this->db->delete('prod_ejecutado_mensual');
            /*---------------------------------------------------*/
            /*----------------- ELIMINA PRODUCTO ---------------*/
            $this->db->where('prod_id', $prod_id);
            $this->db->delete('_productos');
            /*---------------------------------------------------*/
            $this->actualiza_ponderacion($pfec_id,$com_id,$prod_id);

            if(count($this->model_producto->get_producto_id($prod_id))==0)
            {
                $this->session->set_flashdata('success','EL PRODUCTO SE ELIMINO CORRECTAMENTE');
                redirect('admin/prog/list_prod/'.$mod.'/'.$pfec_id.'/'.$proy_id.'/'.$com_id.'/true');
            }
            else
            {
                $this->session->set_flashdata('danger','ERROR AL ELIMINAR, INTENTELO DE NUEVO');
                redirect('admin/prog/list_prod/'.$mod.'/'.$pfec_id.'/'.$proy_id.'/'.$com_id.'/false');
            } 
        }
        else{
            $this->actualiza_ponderacion($pfec_id,$com_id,$prod_id);
            $this->session->set_flashdata('danger','ERROR AL ELIMINAR, INTENTELO DE NUEVO');
            redirect('admin/prog/list_prod/'.$mod.'/'.$pfec_id.'/'.$proy_id.'/'.$com_id.'/false');
        } 
    }


    function subir_producto()
    {
        if ($this->input->post()) {
            $post = $this->input->post();
            $mod = $post['mod'];
            $proy_id = $post['proy_id'];
            $pfec_id = $post['pfec_id'];
            $com_id = $post['com_id'];
          
            $tipo = $_FILES['archivo']['type'];
            $tamanio = $_FILES['archivo']['size'];
            $archivotmp = $_FILES['archivo']['tmp_name'];

            $filename = $_FILES["archivo"]["name"];
            $file_basename = substr($filename, 0, strripos($filename, '.'));
            $file_ext = substr($filename, strripos($filename, '.'));
            $allowed_file_types = array('.csv');
            if (in_array($file_ext, $allowed_file_types) && ($tamanio < 90000000)) {
                
                
                $lineas=$this->archivo_producto($proy_id,$pfec_id,$com_id,$archivotmp);
               

                $this->session->set_flashdata('success','SE IMPORTARON Y SE REGISTRARON '.$lineas.' PRODUCTOS');
                redirect(site_url("admin") . '/prog/list_prod/'.$mod.'/'.$pfec_id.'/'.$proy_id.'/'.$com_id.'/true');
            } 
            elseif (empty($file_basename)) {
                echo "<script>alert('SELECCIONE ARCHIVO .CSV')</script>";
            } 
            elseif ($filesize > 100000000) {
                //redirect('');
            } 
            else {
                $mensaje = "S�lo estos tipos de archivo se permiten para la carga: " . implode(', ', $allowed_file_types);
                echo '<script>alert("' . $mensaje . '")</script>';
            }

        } else {
            show_404();
        }
    }


   //---------------- SUBIR ARCHIVO PRODUCTO  -----------------------
    function archivo_producto($proy_id,$pfec_id,$com_id,$archivotmp)
    {

            $tipo = $_FILES['archivo']['type'];
            $tamanio = $_FILES['archivo']['size'];
            $archivotmp = $_FILES['archivo']['tmp_name'];

            $fase = $this->model_faseetapa->get_id_fase($proy_id); //// DATOS DE LA FASE ACTIVA
            $fase_gestion = $this->model_faseetapa->list_fases_gestiones($pfec_id); //// DATOS DE LA FASE GESTION
                
            $prod_nro = count($this->model_producto->list_prod($com_id)); //// Nro de Productos
            $conf=$this->model_proyecto->configuracion(); /// Confirmacion
             
            //cargamos el archivo
            $lineas = file($archivotmp);
          
            //inicializamos variable a 0, esto nos ayudar� a indicarle que no lea la primera l�nea
            $i=0;
            $nro=0;
            //Recorremos el bucle para leer l�nea por l�nea
            foreach ($lineas as $linea_num => $linea)
            { 
               if($i != 0) 
               { 
                   $datos = explode(";",$linea);
                   if(count($datos)==23 & $datos[0]!='' & $datos[0]!=0 & $datos[2]!=''){
                        $nro = $nro+1; //// Nro de productos insertados
                        $prod_nro++; ///// Nro de productos

                        $producto = utf8_encode(trim($datos[1])); //// Productos

                        $tp_id = (int)$datos[2]; //// tipo de Indicador
                        if(!is_numeric($tp_id)){
                            $tp_id=1;
                        }

                       $indicador = utf8_encode($datos[3]); //// Descripcion Indicador
                       $tp_den = (int)$datos[4]; //// tipo Denominador
                       if(!is_numeric($tp_den)){
                        $tp_den=0;
                       }
                       
                       $formula = utf8_encode($datos[5]); //// Descripcion formula

                       $linea_base = (float)$datos[6]; //// Linea base
                       if(!is_numeric($linea_base)){
                        $linea_base=0;
                        }

                       $meta = (float)$datos[7]; //// Meta
                       if(!is_numeric($meta)){
                        $meta=0;
                        }

                        $fuente = utf8_encode($datos[8]); ///// Fuente de Verificacion
                        $denominador = utf8_encode($datos[9]); ///// Total de casos Denominador
                        $numerador = utf8_encode($datos[10]); ///// Total de casos Numerador

                        /*========================= TABLA ACTIVIDADES ==========================*/  
                      $query=$this->db->query('set datestyle to DMY');
                      $data_to_store = array(
                        'com_id' => $com_id,
                        'prod_producto' => strtoupper($producto),
                        'indi_id' => $tp_id,
                        'prod_indicador' => strtoupper($indicador),
                        'prod_formula' => strtoupper($formula),
                        'prod_linea_base' => $linea_base,
                        'prod_meta' => $meta,
                        'prod_fuente_verificacion' => strtoupper($fuente), 
                        'pt_id' => 0,
                        'prod_total_casos' => strtoupper($denominador),
                        'prod_casos_favorables' => strtoupper($numerador),
                        'prod_denominador' => $tp_den,
                        'fun_id' => $this->session->userdata("fun_id"),
                    );
                        $this->db->insert('_productos', $data_to_store); 
                        $prod_id=$this->db->insert_id(); ////// id del Actividad
                    /*====================================================================*/  
                        if(count($fase_gestion)==1)
                        {
                            $m[1]=(float)$datos[11]; //// Mes 1
                            $m[2]=(float)$datos[12]; //// Mes 2
                            $m[3]=(float)$datos[13]; //// Mes 3
                            $m[4]=(float)$datos[14]; //// Mes 4
                            $m[5]=(float)$datos[15]; //// Mes 5
                            $m[6]=(float)$datos[16]; //// Mes 6
                            $m[7]=(float)$datos[17]; //// Mes 7
                            $m[8]=(float)$datos[18]; //// Mes 8
                            $m[9]=(float)$datos[19]; //// Mes 9
                            $m[10]=(float)$datos[20]; //// Mes 10
                            $m[11]=(float)$datos[21]; //// Mes 11
                            $m[12]=(float)$datos[22]; //// Mes 12

                            for ($p=1; $p <=12 ; $p++) { 
                                if($m[$p]!=0 || $m[$p]!='')
                                {
                                    $data_to_store4 = array( 
                                    'prod_id' => $prod_id, /// Prod id
                                    'm_id' => $p, /// Mes
                                    'pg_fis' => $m[$p], /// Programado mes
                                    'g_id' => $fase_gestion[0]['g_id'], /// Valor mes
                                    );
                                    $this->db->insert('prod_programado_mensual', $data_to_store4); ///// Guardar en Tabla Insumo Financiamiento Programado Mes
                                }
                            }
                        }

                        $nro_p=$conf[0]['conf_producto']+1;

                        $update_conf = array('conf_producto' => $nro_p);
                        $this->db->where('ide', $this->session->userdata("gestion"));
                        $this->db->update('configuracion', $update_conf);

                   }
                   
               }

               $i++;
            }

        return $nro;
    }

    function rolfun($rol)
    {
      $valor=false;
      for ($i=1; $i <=count($rol) ; $i++) { 
        $data = $this->Users_model->get_datos_usuario_roles($this->session->userdata('fun_id'),$rol[$i]);
        if(count($data)!=0){
          $valor=true;
          break;
        }
      }
      return $valor;
    }
}