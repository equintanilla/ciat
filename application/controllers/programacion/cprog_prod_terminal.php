<?php

class Cprog_prod_terminal extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->model('mantenimiento/mpoa');
        $this->load->model('programacion/marco_estrategico/mobjetivos');
        $this->load->model('mantenimiento/munidad_organizacional');
        $this->load->model('programacion/prog_poa/mobjetivo_gestion');
        $this->load->model('programacion/prog_poa/mp_terminal');
        $this->load->model('mantenimiento/model_funcionario');
        $this->load->model('mantenimiento/mindicador');
        //llamar a mi menu
        $this->load->library('menu');
        $this->menu->const_menu(1);
    }

    //LISTA DE PRODUCTO TERMINAL
    function lista_pterminal($poa_id, $obje_id, $o_id)
    {
        $gestion = $this->session->userData('gestion');
        $dato_poa = $this->mpoa->dato_poa_id($poa_id);//datos del poa
        $data['dato_poa'] = $dato_poa[0];
        $dato_objest = $this->mobjetivos->dato_objetivo($obje_id);//dato de objetivo estrategico
        $data['dato_objest'] = $dato_objest[0];
        $dato_ogestion = $this->mobjetivo_gestion->get_ogestion($o_id);//dato de objetivo de gestion
        $data['dato_ogestion'] = $dato_ogestion[0];
        //--------------------------------------------------------------------
        $data['poa_id'] = $poa_id;
        $data['obje_id'] = $obje_id;
        $data['o_id'] = $o_id;
        //--------LISTA DE PRODUCTOS TERMINALES
        $data['lista_pterminal'] = $this->mp_terminal->lista_pterminal($o_id);
        $lista_pterminal = $this->mp_terminal->lista_pterminal($o_id);
        $programacion = array();
        foreach ($lista_pterminal as $row) {
            $pt_id = $row['pt_id'];// id de mi objetivo estrategico
            $linea_base = $row['pt_linea_base'];
            $meta = $row['pt_meta']; //variable meta
            $programacion[$pt_id] = $this->programacion_mensual($pt_id,$linea_base,$meta);
        }
        $data['programacion'] = $programacion;
        $ruta = 'programacion/prog_poa/red_objetivos/vlista_producto_terminal';
        $this->construir_vista($ruta, $data);
    }
    //OBTERNER GRAFICO DE PRODUCTO TERMINAL
    function get_grafico_pterminal(){
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $post = $this->input->post();
            $pt_id = $post['pt_id'];
            $pterminal = $this->mp_terminal->get_pterminal($pt_id);
            $pterminal = $pterminal[0];
            $linea_base = $pterminal['pt_linea_base'];
            $meta = $pterminal['pt_meta']; //variable meta
            $gestion = $pterminal['pt_gestion'];// gestion
            $codigo = $pterminal['pt_codigo'];//codigo de producto terminal
            //OBTENER MI TABLA TEMPORALIZACION
            $programacion = $this->programacion_mensual($pt_id,$linea_base,$meta);
            $indi[1] = '';
            $indi[2] = '%';
            $porc = $indi[$pterminal['indi_id']];
            $vec_pa_grafico = array();
            $tabla = '';
            $tabla .= '<tr>
                            <td colspan="13" bgcolor="#2F4F4F"><center><b><font color="#ffffff" size="1">
                         PROGRAMACI&Oacute;N MENSUAL - ' .$gestion.'</font></b></center></td>
                       </tr>';
            //---------------------- CABECERA DE GESTIONES
            $mes[1] = 'ENERO';$mes[2] = 'FEBRERO';$mes[3] = 'MARZO';$mes[4] = 'ABRIL';
            $mes[5] = 'MAYO';$mes[6] = 'JUNIO';$mes[7] = 'JULIO';$mes[8] = 'AGOSTO';
            $mes[9] = 'SEPTIEMBRE';$mes[10] = 'OCTUBRE';$mes[11] = 'NOVIEMBRE';$mes[12] = 'DICIEMBRE';
            $tabla .= '<tr>';
            $tabla .= '<td style="width:1%;" bgcolor="#2F4F4F"></td>';
            for ($i = 1; $i <= 12; $i++) {
                $tabla .= '<td style="width:1%;" bgcolor="#2F4F4F"><center><b><font color="#ffffff" size="1">' .
                    $mes[$i]. '</font></b></center></td>';
            }
            $tabla .= '</tr>';
            //---------------------- FIN DE CABECERA
            //---------------------- PROGRAMACION
            $tabla .= '<tr>';
            $tabla .= '<td style="width:5%;" bgcolor="#E6E6FA"><center><b><font color="#000000" size="1">P</font></b></center></td>';
            for ($i = 1; $i <= 12; $i++) {
                $puntero = 'p' . $i;
                $prog_gestion = $programacion[$puntero];
                $tabla .= ' <td style="width:1%;" bgcolor="#E6E6FA"><center><font color="#000000" size="1">' . round($prog_gestion, 1) . $porc . '</font></center></td>';
            }
            $tabla .= '</tr>';
            //--------------------- PROGRAMACION ACUMULADA
            $tabla .= '<tr>';
            $tabla .= '<td style="width:5%;" bgcolor="#E6E6FA"><center><b><font color="#000000" size="1">P.A</font></b></center></td>';
            for ($i = 1; $i <= 12; $i++) {
                $puntero_acumulado = 'p_a' . $i;
                $prog_acumulado = $programacion[$puntero_acumulado];
                $tabla .= ' <td style="width:1%;" bgcolor="#E6E6FA"><center><font color="#000000" size="1">' . round($prog_acumulado, 1) . $porc . '</font></center></td>';
            }
            $tabla .= '</tr>';
            //--------------------- PROGRAMACION ACUMULADA PORCENTUAL
            $tabla .= '<tr>';
            $tabla .= '<td style="width:5%;" bgcolor="#E6E6FA"><center><b><font color="#000000" size="1">%P.A</font></b></center></td>';
            for ($i = 1; $i <= 12; $i++) {
                $puntero_pa_porcentual = 'pa_porc' . $i;
                $pa_porcentual = $programacion[$puntero_pa_porcentual];
                $vec_pa_grafico[($i - 1)] = $pa_porcentual;
                $tabla .= ' <td style="width:1%;" bgcolor="#E6E6FA"><center><font color="#000000" size="1">' . round($pa_porcentual, 1) . $porc . '%</font></center></td>';
            }
            $tabla .= '</tr>';
            $result = array(
                'tabla' => $tabla,
                'linea_base' => $linea_base,
                'meta' => $meta,
                'prog_acumulada_p' => $vec_pa_grafico,
                'codigo' => $codigo
            );
            echo json_encode($result);
        } else {
            show_404();
        }
    }
    //NUEVO PRODUCTO TERMINAL
    function nuevo_pterminal($poa_id, $obje_id, $o_id)
    {
        $dato_poa = $this->mpoa->dato_poa_id($poa_id);//datos del poa
        $data['dato_poa'] = $dato_poa[0];
        $dato_objest = $this->mobjetivos->dato_objetivo($obje_id);//dato de objetivo estrategico
        $data['dato_objest'] = $dato_objest[0];
        $dato_ogestion = $this->mobjetivo_gestion->get_ogestion($o_id);//dato de objetivo de gestion
        $data['dato_ogestion'] = $dato_ogestion[0];

        $dato_ogestion = $this->mobjetivo_gestion->get_ogestion($o_id);//dato de objetivo de gestion
        // var_dump($dato_ogestion);  die;
        $data['dato_ogestion'] = $dato_ogestion[0];
        $dato_obj_terminal = $this->mobjetivos->dato_objetivo_terminal($obje_id);
        $funcionario= $this->model_funcionario->get_funcionario($dato_obj_terminal[0]['fun_id']);
        $unidado = $this->munidad_organizacional->get_unidad($funcionario[0]['uni_id']);
        $data['uninda_organizacional'] = $unidado->uni_unidad;
        $data['dato_terminal'] = $dato_obj_terminal[0];
        

        //--------------------------------------------------------------------
        $data['list_indicador'] = $this->mindicador->get_indicador();
        $data['list_funcionario'] = $this->model_funcionario->get_funcionarios();
        //
        $data['poa_id'] = $poa_id;
        $data['obje_id'] = $obje_id;
        $data['o_id'] = $o_id;
        $ruta = 'programacion/prog_poa/red_objetivos/vnuevo_pterminal';
        $this->construir_vista($ruta, $data);
    }
    //PROGRAMACION MENSUAL DEL PRODUCTO TERMINAL
    function programacion_mensual($pt_id,$linea_base,$meta){
        $prog_acumulado = $linea_base; //variable de programacion acumulada
        for ($i = 1; $i <= 12 ; $i++) {
            $puntero_prog = 'p'.$i;//PUNTERO PARA LISTAR LA PROGRAMACION
            $dato_programado = $this->mp_terminal->get_prog_pterminal($pt_id, $i);//lista de programado
            $dato_programado = $dato_programado[0]['ppm_fis'];
            $programado[$puntero_prog] = $dato_programado;//matriz
            //PROGRAMADO ACUMULADO--------------------------------------------------------------------------------
            $puntero_prog_acumulado = 'p_a' . $i;//PUNTERO PARA LA PROGRAMACION ACUMULADA
            $prog_acumulado += $dato_programado;
            $programado[$puntero_prog_acumulado] = $prog_acumulado;//matriz
            //PROGRAMA ACUMULADO PORECENTUAL----------------------------------------------------------------------
            $puntero_pa_porcentual = 'pa_porc' . $i;//PUNTERO PARA LA PROGRAMACION ACUMULADA PORCENTUAL
            if($meta == 0){
                $pa_porcentual = 0;
            }else{
                $pa_porcentual = (($prog_acumulado / $meta) * 100);
            }
            $programado[$puntero_pa_porcentual] = $pa_porcentual;//matriz
        }
        return $programado;
    }
    //GUARDAR PRODUCTO TERMINAL
    function guardar_pterminal(){
        if($this->input->post()) {
            $post = $this->input->post();
            $poa_id = $post['poa_id'];
            $obje_id = $post['obje_id'];
            $dato_obj_terminal = $this->mobjetivos->dato_objetivo_terminal($obje_id);
            $datos = $dato_obj_terminal[0];
            $o_id = $post['o_id'];
            $dato_pt = array(
                'o_id' => $o_id,
                // 'pt_objetivo' => $datos['obje_objetivo'],
                'pt_objetivo' => strtoupper($this->security->xss_clean($post['ptobjetivo'])),
                'fun_id' => $datos['fun_id'],
                'indi_id' => $this->security->xss_clean($post['pttipo_indicador']),
                'pt_indicador' => $datos['indi_id'],
                'pt_linea_base' => $this->security->xss_clean($post['ptlineabase']),
                'pt_meta' => $this->security->xss_clean($post['ptmeta']),
                'pt_fuente_verificacion' => strtoupper($this->security->xss_clean($post['ptfuente'])),
                'pt_supuestos' => strtoupper($this->security->xss_clean($post['ptsupuesto'])),
                'pt_gestion' => $this->session->userdata('gestion')
            );
            $dato_prog = array(
                'mes1'=>$this->security->xss_clean($post['mes1']),
                'mes2'=>$this->security->xss_clean($post['mes2']),
                'mes3'=>$this->security->xss_clean($post['mes3']),
                'mes4'=>$this->security->xss_clean($post['mes4']),
                'mes5'=>$this->security->xss_clean($post['mes5']),
                'mes6'=>$this->security->xss_clean($post['mes6']),
                'mes7'=>$this->security->xss_clean($post['mes7']),
                'mes8'=>$this->security->xss_clean($post['mes8']),
                'mes9'=>$this->security->xss_clean($post['mes9']),
                'mes10'=>$this->security->xss_clean($post['mes10']),
                'mes11'=>$this->security->xss_clean($post['mes11']),
                'mes12'=>$this->security->xss_clean($post['mes12']),
            );
            $tipo_indicador = $post['pttipo_indicador'];

            if ($tipo_indicador == 2) {
                $dato_pt['pt_total_casos'] = strtoupper($this->security->xss_clean($post['relativoa']));
                $dato_pt['pt_casos_favorables'] = strtoupper($this->security->xss_clean($post['relativob']));
                $dato_pt['pt_casos_desfavorables'] = strtoupper($this->security->xss_clean($post['relativoc']));
                $dato_pt['pt_denominador'] = $this->security->xss_clean($post['pt_denominador']);
                $dato_pt['pt_formula'] = strtoupper($this->security->xss_clean($post['ptformula']));
            }
            if (isset($_REQUEST['modificar'])) {
                //======================= MODIFICAR====================
                 $pt_id = $this->input->post('modificar');
                $modificar = $this->mp_terminal->mod_pterminal($pt_id,$dato_pt,$dato_prog);
                if ($modificar) {
                    $url = site_url("") . '/prog/pterminal/' . $poa_id . '/' . $obje_id.'/'.$o_id;
                    $this->session->set_flashdata('modificar', 'EL REGISTRO SE MODIFICO CORRECTAMENTE !!!');
                    redirect($url, 'refresh');
                }else{
                    $url = site_url("") . '/prog/pterminal/' . $poa_id . '/' . $obje_id.'/'.$o_id;
                    $this->session->set_flashdata('error', 'ERROR AL MODIFICAR!!!');
                    redirect($url, 'refresh');
                }
            } else {
                if ($this->mp_terminal->guardar_pterminal($dato_pt,$dato_prog)) {
                    $url = site_url("") . '/prog/pterminal/' . $poa_id . '/' . $obje_id.'/'.$o_id;
                    $this->session->set_flashdata('guardar', 'EL REGISTRO SE GUARDO CORRECTAMENTE !!!');
                    redirect($url, 'refresh');
                }else{
                    $url = site_url("") . '/prog/pterminal/' . $poa_id . '/' . $obje_id.'/'.$o_id;
                    $this->session->set_flashdata('error', 'ERROR AL MODIFICAR!!!');
                    redirect($url, 'refresh');
                }

            }
        }else{
            show_404();
        }
    }
    //FUNCION PARA CONSTRUIR MI VISTA
    function construir_vista($ruta, $data)
    {
        //----------------------------------- MENU-------------------------------
        $menu['enlaces'] = $this->menu->get_enlaces();
        $menu['subenlaces'] = $this->menu->get_sub_enlaces();
        $menu['titulo'] = 'PROGRAMACI&Oacute;N';
        //-----------------------------------------------------------------------
        //armar vista
        $this->load->view('includes/header');
        $this->load->view('includes/menu_lateral', $menu);
        $this->load->view($ruta, $data);//contenido
        //$this->load->view('admin/mantenimiento/vprueba');//contenido
        $this->load->view('includes/footer');
    }
    //caso indicador de desempenio producto terminal
    function get_indicador_pt(){
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $post = $this->input->post();
            $pt_id = $post['pt_id'];
            $dato = $this->mp_terminal->get_pterminal($pt_id);
            $result = array(
                'codigo' => $dato[0]['pt_codigo'],
                'objetivo' => $dato[0]['pt_objetivo'],
                'eficacia' => $dato[0]['pt_eficacia'],
                'financiera' => $dato[0]['pt_efinanciera'],
                'ejecucion' => $dato[0]['pt_epejecucion'],
                'fisica' => $dato[0]['pt_efisica'],
            );
            echo json_encode($result);
        } else {
            show_404();
        }
    }
    //GUARDAR INDICADOR DE DESEMPEÑO
    function add_indicador_pt(){
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $post = $this->input->post();
            $pt_id = $post['pt_id'];
            $data = array(
                'pt_eficacia' => $this->security->xss_clean($post['eficacia']),
                'pt_efinanciera' => $this->security->xss_clean($post['financiera']),
                'pt_epejecucion' => $this->security->xss_clean($post['ejecucion']),
                'pt_efisica' => $this->security->xss_clean($post['fisica'])
            );
            if($this->mp_terminal->add_indicador_pt($pt_id,$data)){
                $result = array(
                    'respuesta' => 'true',
                );
            }else{
                $result = array(
                    'respuesta' => 'false',
                );
            }
            echo json_encode($result);
        } else {
            show_404();
        }
    }
    //LLENAR MI FORMULARIO DE DE CARGAR ARCHIVO
    function get_form_archivo(){
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $post = $this->input->post();
            $pt_id = $post['pt_id'];
            $data = $this->mp_terminal->get_pterminal($pt_id);
            //$data = $data->row();
            $data = $data[0];
            if(strlen($data['pt_archivo_adjunto']) == 0){
                $result = array(
                    'respuesta' => 'true',
                    'codigo' => $data['pt_codigo'],
                    'ruta' => ''
                );
            }else{
                $result = array(
                    'respuesta' => 'false',
                    'codigo' => $data['pt_codigo'],
                    'ruta' => $data['pt_archivo_adjunto']
                );
            }

            echo json_encode($result);
        } else {
            show_404();
        }
    }
    //GUARDAR ARCHIVO DEL OBJETIVOS
    function guardar_archivo(){
        if($this->input->post()){
            $post = $this->input->post();
            $poa_id = $post['poa_id'];
            $obje_id = $post['obje_id'];
            $o_id = $post['o_id'];
            $pt_id = $post['id_pt_pdf'];
            $accion = $post['mod_eli'];//accion modificar o eliminar
            $gestion = $this->session->userData('gestion');
            //-------------------------------------------------RECIBIR MI DOCUMENTO
            $filename = $_FILES["userfile"]["name"];
            $file_basename = substr($filename, 0, strripos($filename, '.')); // get file extention
            $file_ext = substr($filename, strripos($filename, '.')); // get file name
            $filesize = $_FILES["userfile"]["size"];
            $allowed_file_types = array('.pdf', '.doc', '.jpg', '.JPG', '.png', '.JPEG');
            if($accion != '0' ){
                $file_eliminar = "archivos/productoTerminal/".$accion;
                unlink($file_eliminar);
            }
            if (/*in_array($file_ext, $allowed_file_types) && */($filesize < 80000000)) { // Rename file
                $newfilename =  'PT-'.$gestion.'-'.substr(md5(uniqid(rand())), 0, 5) . $file_ext;
                //--------------------------------$data_to_store['pa_ruta_archivo'] = "" . $newfilename;
                if (file_exists("archivos/productoTerminal/" . $newfilename)) {
                    echo "<script>alert('Ya existe este archivo')</script>";
                } else {
                    move_uploaded_file($_FILES["userfile"]["tmp_name"], "archivos/productoTerminal/" . $newfilename);
                    //guardar mi area urbana despues de las validaciones
                    $data['pt_archivo_adjunto'] = $newfilename;
                    $this->db->WHERE('pt_id', $pt_id);
                    $this->db->UPDATE('_productoterminal', $data);
                    $this->session->set_flashdata('guardar_archivo', 'EL ARCHIVO SE GUARDO CORRECTAMENTE');
                    redirect('prog/pterminal/'.$poa_id.'/'.$obje_id.'/'.$o_id);
                }
            } elseif (empty($file_basename)) {
                echo "Selecciona un archivo para cargarlo.";
            } elseif ($filesize > 100000000) {
                $this->session->set_flashdata('guardar_archivo', 'EL ARCHIVO SE GUARDO CORRECTAMENTE');
                redirect('prog/pterminal/'.$poa_id.'/'.$obje_id.'/'.$o_id);
            } else {
                $mensaje = "Sólo estos tipos de archivo se permiten para la carga: " . implode(', ', $allowed_file_types);
                echo '<script>alert("'.$mensaje.'")</script>';
                unlink($_FILES["userfile"]["tmp_name"]);
            }

        }else{
            show_404();
        }
    }
    //FORMULARIO DE MODIFICACION DE PRODUCTO TERMINAL
    function form_mod_pterminal($poa_id, $obje_id, $o_id,$pt_id)
    {
        $data['poa_id'] = $poa_id;
        $data['obje_id'] = $obje_id;
        $data['o_id'] = $o_id;
        $data['pt_id'] = $pt_id;
        $dato_poa = $this->mpoa->dato_poa_id($poa_id);//datos del poa
        $data['dato_poa'] = $dato_poa[0];
        $dato_objest = $this->mobjetivos->dato_objetivo($obje_id);//dato de objetivo estrategico
        $data['dato_objest'] = $dato_objest[0];
        $dato_ogestion = $this->mobjetivo_gestion->get_ogestion($o_id);//dato de objetivo de gestion
        $data['dato_ogestion'] = $dato_ogestion[0];
        //--------------------------------------------------------------------
        $data['list_indicador'] = $this->mindicador->get_indicador();
        $data['list_funcionario'] = $this->model_funcionario->get_funcionarios();
        $dato_pt = $this->mp_terminal->get_pterminal($pt_id);
        $data['dato_pt'] = $dato_pt[0];
        //---------------------------------------------------------------------
        $linea_base = $dato_pt[0]['pt_linea_base'];
        $meta = $dato_pt[0]['pt_meta'];
        $data['programacion'] = $this->programacion_mensual($pt_id, $linea_base, $meta);
        $ruta = 'programacion/prog_poa/red_objetivos/vmod_pterminal';
        $this->construir_vista($ruta, $data);

    }
    //ELIMINAR PRODUCTO TERMINAL
    function del_pterminal(){
        if($this->input->is_ajax_request() && $this->input->post()){
            $post = $this->input->post();
            $pt_id = $post['pt_id'];
            $peticion = $this->mp_terminal->eliminar_pterminal($pt_id);
            $data = array(
                'respuesta' => $peticion
            );
            echo json_encode($data);
        }else{
            show_404();
        }
    }

    //Eliminar Archivo
    function del_archivo()
    {
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $post = $this->input->post();
            $pt_id = $post['id_pt'];
            $data = $this->mp_terminal->get_pterminal($pt_id);
            $data = $data[0];
            if (strlen($data['pt_archivo_adjunto']) != 0) {
                //guardar objetivo
                $this->mp_terminal->del_archivo($pt_id);
                $file_eliminar = "archivos/productoTerminal/" . $data['pt_archivo_adjunto'];
                unlink($file_eliminar);
            }
            $result = array(
                'respuesta' => 'true',
            );
            echo json_encode($result);
        } else {
            show_404();
        }
    }

}