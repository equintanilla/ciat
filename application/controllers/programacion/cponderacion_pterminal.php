<?php

class Cponderacion_pterminal extends CI_Controller
{
    var $gestion;
    var $rol;
    var $fun_id;

    function __construct()
    {
        parent::__construct();
        $this->load->model('mantenimiento/mpoa');
        $this->load->model('programacion/ponderacion/mponderacion');
        $this->load->model('programacion/prog_poa/mobjetivo_gestion');
        $this->load->model('programacion/prog_poa/mp_terminal');
        //llamar a mi menu
        $this->load->library('menu');
        $this->menu->const_menu(1);
        $this->gestion = $this->session->userData('gestion');
        $this->rol = $this->session->userData('rol');
        $this->fun_id = $this->session->userData('fun_id');
    }

    //LISTADE PROGRAMAS PARA PRODUTO TERMINAL
    function lista_red_objetivos_pt()
    {
        $ruta = '/prog/list_ogestion/';
        $data['lista_poa'] = $this->tabla_red_objetivos($ruta);
        $ruta = 'programacion/ponderacion/prod_terminal/vred_programa';
        $this->construir_vista($ruta, $data);
    }

    //GENERAR TABLA DE RED DE OBJETIVOS
    function tabla_red_objetivos($ruta)
    {

        $lista_poa = $this->mpoa->lista_poa();
        $cont = 1;
        $tabla = '';
        foreach ($lista_poa as $row) {
            $lista_objgestion = $this->mponderacion->poa_obje_estrategico($row['poa_id']);
            $di ="";
            $r = "";
            if(count($lista_objgestion) != 0){
                $di = "ASIGNAR PONDERACION";
                $r = site_url("") . $ruta . $row['poa_id'];
            }else{
                $di = "SIN ACCIONES DE CORTO PLAZO";
            }
            $tabla .= '<tr>';
            $tabla .= '<td >
                            <a href="' . $r . '" class="enviar_poa" name="enviar_poa" id="enviar_poa">
                                <img src="' . base_url() . 'assets/img/folder.png" width="30" height="30" class="img-responsive "title="'.$di.'">
							</a>
					   </td>';
            $tabla .= '<td>' . $row['poa_codigo'] . '</td>';
            $tabla .= '<td>' . $row['aper_programa'] . $row['aper_proyecto'] . $row['aper_actividad'] . " - " . $row['aper_descripcion'] . '</font></td>';
            $tabla .= '<td>' . $row['uni_unidad'] . '</td>';
            $tabla .= '<td>' . $row['poa_fecha_creacion'] . '</td>';
            $tabla .= '</tr>';
            $cont++;
        }
        return $tabla;
    }

    //LISTA DE OBJETIVOS DE GETION PARA PRODUCTO TERMINAL
    function lista_ogestion($poa_id)
    {
        $data['dato_poa'] = $this->mponderacion->get_poa($poa_id);
        $data['lista_ogestion'] = $this->tabla_ogestion($poa_id);
        $ruta = 'programacion/ponderacion/prod_terminal/vlista_ogestion';
        $this->construir_vista($ruta, $data);
    }

    //GENERAR TABLA DE OBJETIVO DE GESTION CON PONDERACION FILTRADO POR POA ID
    function tabla_ogestion($poa_id)
    {
        $lista_objgestion = $this->mponderacion->poa_obje_estrategico($poa_id);
        $cont = 1;
        $tabla = '';
        $ruta =  site_url("") .'/prog/pond_pterminal/'.$poa_id.'/';
        foreach ($lista_objgestion as $row) {
            $lista_pterminal = $this->mp_terminal->lista_pterminal($row['o_id']);
            $di ="";
            $r = "";
            if(count($lista_pterminal) != 0){
                $di = "ASIGNAR PONDERACION";
                $r = $ruta.$row['o_id'];
            }else{
                $di = "SIN PRODUCTO TERMINAL";
            }
            $tabla .= '<tr>';
            $tabla .= '<td >' . $cont . '</td>';
            $tabla .= '<td >
                            <a href="'.$r.'" class="enviar_poa" name="enviar_poa" id="enviar_poa">
                                <img src="' . base_url() . 'assets/ifinal/2.png" width="50" height="50" class="img-responsive "
                                title="'.$di.'">
							</a>
					   </td>';
            $tabla .= '<td >' . $row['o_codigo'] . '</td>';
            $tabla .= '<td >' . $row['o_objetivo'] . '</td>';
            $tabla .= '<td >' . $row['fun_nombre'] . ' ' . $row['fun_paterno'] . ' ' . $row['fun_materno'] . '</td>';
            $tabla .= '<td >' . $row['uni_unidad'] . '</td>';
            $tabla .= '<td style="text-align: right">' . intval($row['o_ponderacion']) . '%</td>';
            $tabla .= '</tr >';
            $cont++;
        }
        return $tabla;
    }

    //LISTA DE PRODUCTO TERMINALES
    function lista_pterminal($poa_id,$o_id){
        $data['poa_id'] = $poa_id;
        $data['o_id'] = $o_id;
        $data['dato_poa'] = $this->mponderacion->get_poa($poa_id);
        $data['dato_ogestion'] = $this->mobjetivo_gestion->get_ogestion($o_id)[0];
        $dato = $this->tabla_pond_pterminal($o_id);
        $data['lista_pterminal'] = $dato['tabla'];
        $data['ponderacion'] = $dato['ponderacion'];
        $data['ponderacion_js'] = '<SCRIPT src="' . base_url() . 'mis_js/programacion/ponderacion/ponderacion_pterminal.js" type="text/javascript"></SCRIPT>';
        $ruta = 'programacion/ponderacion/prod_terminal/vlista_pterminal';
        $this->construir_vista($ruta, $data);
    }

    //LISTA DE PONDERACION DE PRODUCTOS TERMINALES
    function tabla_pond_pterminal($o_id){
        $lista_pterminal = $this->mp_terminal->lista_pterminal($o_id);
        $cont = 1;
        $tabla = '';
        $ponderacion = 0;
        foreach ($lista_pterminal as $row) {
            $ponderacion += $row['pt_ponderacion'];
            $tabla .= '<tr>';
            $tabla .= '<td >' . $cont . '</td>';
            $tabla .= '<td >
                            <a href="" class="form_pond_pt" data-toggle="modal" data-target="#modal_ponderacion" name="' . $row['pt_id'] . '">
                                <img src="' . base_url() . 'assets/ifinal/form2.png" width="40" height="40" class="img-responsive"
                                title="ASIGNAR PONDERACION">
							</a>
					   </td>';
            $tabla .= '<td >' . $row['pt_codigo'] . '</td>';
            $tabla .= '<td >' . $row['pt_objetivo'] . '</td>';
            $tabla .= '<td >' . $row['fun_nombre'] . ' ' . $row['fun_paterno'] . ' ' . $row['fun_materno'] . '</td>';
            $tabla .= '<td >' . $row['unidad'] . '</td>';
            $tabla .= '<td style="text-align: right">' . intval($row['pt_ponderacion']) . '%</td>';
            $tabla .= '</tr >';
            $cont++;
        }
        $data['tabla'] = $tabla;
        $data['ponderacion'] = $ponderacion;
        return $data;
    }

    //LLENAR MI FORMULARIO
    function get_form_pterminal_pond(){
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $post = $this->input->post();
            $pt_id = $post['pt_id'];
            $get_pterminal = $this->mp_terminal->get_pterminal($pt_id)[0];
            $result = array(
                'id' => $get_pterminal['pt_id'],
                'codigo' => $get_pterminal['pt_codigo'],
                'objetivo' => $get_pterminal['pt_objetivo'],
                'ponderacion' => $get_pterminal['pt_ponderacion'],
            );
            echo json_encode($result);
        } else {
            show_404();
        }
    }

    //AÑADIR LA PONDERACION
    function add_pond_pterminal(){
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $post = $this->input->post();
            $pt_id = $post['pt_id'];
            $ponderacion = $post['pond'];
            if ($this->mponderacion->add_pond_pterminal($pt_id, $ponderacion)) {
                $result['peticion'] = 'true';
            } else {
                $result['peticion'] = 'true';
            }
            echo json_encode($result);
        } else {
            show_404();
        }
    }

    //FUNCION PARA CONSTRUIR MI VISTA
    function construir_vista($ruta, $data)
    {
        //----------------------------------- MENU-------------------------------
        $menu['enlaces'] = $this->menu->get_enlaces();
        $menu['subenlaces'] = $this->menu->get_sub_enlaces();
        $menu['titulo'] = 'PROGRAMACIÓN';
        //-----------------------------------------------------------------------
        $this->load->view('includes/header');
        $this->load->view('includes/menu_lateral', $menu);
        $this->load->view($ruta, $data);//contenido
        $this->load->view('includes/footer');
    }
}