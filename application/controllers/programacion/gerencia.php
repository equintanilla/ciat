<?php

class Gerencia extends CI_Controller {  
 
  public function __construct ()
    {
        parent::__construct();
        if($this->session->userdata('fun_id')!=null){
        $this->load->library('pdf2');
        $this->load->model('menu_modelo');
        $this->load->model('Users_model','',true);
        $this->load->model('programacion/model_faseetapa');
        $this->load->model('programacion/model_proyecto');
        $this->load->model('programacion/model_componente');
        $this->load->model('programacion/model_producto');
        $this->load->model('programacion/model_actividad');
        }else{
            redirect('/','refresh');
        }
    }

  /*===================================== LISTA DE PROYECTOS  EN EJECUCION =======================================*/  
    public function list_proyectos()
    {
      $this->load->model('menu_modelo');
      $enlaces=$this->menu_modelo->get_Modulos(4);
      $data['enlaces'] = $enlaces;
      $data['lista_aper_padres'] = $this->model_proyecto->list_prog();//lista de aperturas padres   
      
      for($i=0;$i<count($enlaces);$i++)
      {
        $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
      }
      $data['subenlaces'] = $subenlaces;
      
       //load the view
      $data['mod']=4;
        //$data['programas']=$this->list_programas($data['mod']);
        $data['proyectos']=$this->list_programas($data['mod'],1);
        $data['precurrentes']=$this->list_programas($data['mod'],2);
        $data['pnrecurrentes']=$this->list_programas($data['mod'],3);
        $data['operaciones']=$this->list_programas($data['mod'],4);
      $this->load->view('admin/programacion/proy_multi/top/list_proy', $data);
    }

    public function list_programas($mod,$tp)
    {
        $lista_aper_padres = $this->model_proyecto->list_prog();//lista de aperturas padres
        $tabla ='';
        foreach($lista_aper_padres  as $rowa){
            $proyectos=$this->model_proyecto->operaciones($rowa['aper_programa'],1,1,$tp);
            if(count($proyectos)!=0){
                foreach($proyectos  as $row){
                    /*---------- administrador ---------*/
                    if($this->session->userData('rol_id')==1){
                        $tabla.= $this->operacion($row['proy_id'],$mod);
                    }
                    /*----------- responsable ---------*/
                    elseif(count($this->model_proyecto->proyecto_funcionario($row['proy_id']))==1){
                        $tabla.= $this->operacion($row['proy_id'],$mod);
                    }
                    /*------- responsable proceso -----*/
                    elseif (count($this->model_proyecto->proyecto_funcionario_proceso($row['proy_id']))==1) {
                        $tabla.= $this->operacion($row['proy_id'],$mod);
                    }
                }
            }
        }
        return $tabla;





/*
        $lista_aper_padres = $this->model_proyecto->list_prog();//lista de aperturas padres
      $tabla ='';
      foreach($lista_aper_padres  as $rowa)
      {
        $proyectos=$this->model_proyecto->proy_actividades_g($rowa['aper_programa'],$this->session->userdata('gestion'));
        if(count($proyectos)!=0)
        {
          $tabla .='<tr bgcolor="#99DDF0">';
            $tabla .='<td></td>';
            $tabla .='<td></td>';
            $tabla .='<td><center>' .$rowa['aper_programa'].''.$rowa['aper_proyecto'].''.$rowa['aper_actividad'].'</center></td>';
            $tabla .='<td>'.$rowa['aper_descripcion'].'</td>';
            $tabla .='<td>'.$rowa['aper_sisin'].'</td>';
            $tabla .='<td></td>';
            $tabla .='<td></td>';
            $tabla .='<td></td>';
            $tabla .='<td></td>';
            $tabla .='<td></td>';
            $tabla .='<td></td>';
            $tabla .='<td></td>';
            $tabla .='<td></td>';
            $tabla .='<td></td>';
            $tabla .='<td></td>';
            $tabla .='<td></td>';
            $tabla .='<td></td>';
            $tabla .='<td></td>';
          $tabla .='</tr>';
          foreach($proyectos  as $row)
          {
            if($row['tp_id']!=1){$color='#D7FCF9';}else{$color='#ffffff';}
            $fase = $this->model_faseetapa->get_id_fase($row['proy_id']);
            $tabla .= '<tr bgcolor='.$color.'>';
              $tabla .= '<td>';
              if(count($fase)!=0)
              {
                $tabla .= '<center><a href="javascript:abreVentana(\''.site_url("admin").'/dictamen_proyecto/'.$row['proy_id'].'/'.$fase[0]['pfec_ejecucion'].'\');" title="REGISTRO DE LA OPERACION"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="35" HEIGHT="35"/><br>Registro</a></center>';
              }
              else
              {
                $tabla .= '<center><a href="javascript:abreVentana(\''.site_url("admin").'/dictamen_proyecto/'.$row['proy_id'].'\');" title="REGISTRO DE LA OPERACION"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="35" HEIGHT="35"/><br>Registro</a></center>';
              }
              $tabla .= '</td>';
              $tabla .= '<td>';
                if(count($fase)!=0) //// tiene fase
                {
                  $tabla .= '<center><a href="'.site_url("admin").'/proy/prog/'.$mod.'/'.$row['proy_id'].'" title="PROGRAMACION FISICA"><img src="' . base_url() . 'assets/ifinal/bien.png" WIDTH="30" HEIGHT="30"/><br>Programaci&oacute;n F&iacute;sica</a></center>';
                }

                if($this->session->userdata('rol_id')==1)
                {
                  $tabla .='<center><a href="#" data-toggle="modal" data-target="#modal_del_ff" class="btn btn-xs del_ff" title="ELIMINAR PROYECTO"  name="'.$row['proy_id'].'"><img src="'.base_url().'assets/ifinal/eliminar.png" WIDTH="35" HEIGHT="35"/><br>Eliminar</a></center>';
                }
                                                  
              $tabla .= '</td>';
              $tabla .= '<td>';
              if($row['aper_proyecto']!='')
              {
                $tabla .= '<center>'.$row['aper_programa'].''.$row['aper_proyecto'].''.$row['aper_actividad'].'</center>';
              }
              else
              {
                $tabla .=  '<center><font color="red"><b>Re asignar apertura Programatica</b></font></center>';
              }
              $tabla .= '</td>';
              $tabla .= '<td>'.$row['proy_nombre'].'</td>';
              $tabla .= '<td>'.$row['tp_tipo'].'</td>';
              $tabla .= '<td>'.$row['proy_sisin'].'</td>';
              $tabla .= '<td>'.$row['fun_nombre'].' '.$row['fun_paterno'].' '.$row['fun_materno'].'</td>';
              $tabla .= '<td>'.$row['ue'].'</td>';
              $tabla .= '<td>'.$row['ur'].'</td>';
              if(count($fase)!=0){
                $nc=$this->model_faseetapa->calcula_nc($fase[0]['pfec_fecha_inicio']); //// calcula nuevo/continuo
                $ap=$this->model_faseetapa->calcula_ap($fase[0]['pfec_fecha_inicio'],$fase[0]['pfec_fecha_fin']);
                $nro_fg_act = $this->model_faseetapa->nro_fasegestion_actual($fase[0]['id'],$this->session->userdata('gestion'));
                $fase_gest = $this->model_faseetapa->fase_etapa_gestion($fase[0]['id'],$this->session->userdata("gestion"));
                $tabla .='<td>'.$fase[0]['descripcion'].'</td>';
                $tabla .='<td>* '.$fase[0]['fase'].'<br>* '.$fase[0]['etapa'].'</td>';
                $tabla .='<td>'.$nc.'</td>';
                $tabla .='<td>'.$ap.'</td>';
                $tabla .='<td>'.number_format($fase[0]['pfec_ptto_fase'], 2, ',', '.').' Bs.</td>';
                $tabla .='<td>';
                if($nro_fg_act!=0 && ($fase_gest[0]['estado']==1 || $fase_gest[0]['estado']==2))
                {
                  $tabla .=''.number_format($fase_gest[0]['pfecg_ppto_total'], 2, ',', '.').' Bs.';
                }
                elseif ($nro_fg_act==0) {
                  $tabla .= '<font color="red">la gestion no esta en curso</font>';
                }
                $tabla .='</td>';
                $tabla .='<td>';
                if($this->model_faseetapa->verif_fase_etapa_gestion($fase[0]['id'],$this->session->userdata("gestion"))!=0)
                { 
                  if($this->model_faseetapa->nro_ffofet($fase_gest[0]['ptofecg_id'])!=0)
                  {
                    $techo=$this->model_faseetapa->techo_presupuestario($fase_gest[0]['ptofecg_id']);
                    $tabla .= ''.number_format($techo[0]['suma_techo'], 2, ',', '.').' Bs.';
                  }
                  else{$tabla .= "<font color=red>S/T</font>";}
                }
                $tabla .='</td>';
              }
              else{
                $tabla .='<td bgcolor=#efb0b0><font color=red>Sin Fase</font></td>';
                $tabla .='<td bgcolor=#efb0b0><font color=red>Sin Fase</font></td>';
                $tabla .='<td bgcolor=#efb0b0><font color=red>Sin Fase</font></td>';
                $tabla .='<td bgcolor=#efb0b0><font color=red>Sin Fase</font></td>';
                $tabla .='<td bgcolor=#efb0b0><font color=red>Sin Fase</font></td>';
                $tabla .='<td bgcolor=#efb0b0><font color=red>Sin Fase</font></td>';
                $tabla .='<td bgcolor=#efb0b0><font color=red>Sin Fase</font></td>';
              }
              $pr=$this->model_proyecto->prioridad();
              $tabla .='<td>';
              $tabla .='<select class="form-control" onchange="doSelectAlert(event,this.value,'.$row['proy_id'].');">';
                foreach($pr as $pr)
                {
                  if($pr['pr_id']==$row['proy_pr'])
                  {
                    $tabla .="<option value=".$pr['pr_id']." selected>".$pr['pr_desc']."</option>";
                  }
                  else{
                    $tabla .="<option value=".$pr['pr_id'].">".$pr['pr_desc']."</option>"; 
                  }  
              }
              $tabla .='</select>';    
              $tabla .='</td>';
              $tabla .='<td>';
                $loc=$this->model_proyecto->localizacion($row['proy_id']);
                if(count($loc)!=0){
                  $tabla .='<table class="table table-bordered">';
                    $tabla .='<tr bgcolor="#f5f5f5">';
                    $tabla .='<th>PROVINCIA</th><th>MUNICIPIO</th>';
                    $tabla .='</tr>';
                    foreach($loc as $locali)
                    {
                      $tabla .='<tr>';
                      $tabla .='<td>'.$locali['prov_provincia'].'</td>';
                      $tabla .='<td>'.$locali['muni_municipio'].'</td>';
                      $tabla .='</tr>';
                    }
                  
                  $tabla .='</table>';
                }
                
              $tabla .='</td>';
            $tabla .= '</tr>';
          } 
        }
      }
      return $tabla;*/
    }
    public function operacion($proy_id,$mod){
        $proyecto=$this->model_proyecto->get_id_proyecto($proy_id);
        if($proyecto[0]['t_obs']==2){$obs='POA';}
        elseif($proyecto[0]['t_obs']==3){$obs='FINANCIERO';}
        elseif($proyecto[0]['t_obs']==4){$obs='T&Eacute;CNICO OPERATIVO';}
        if($proyecto[0]['tp_id']!=1){$color='#D7FCF9';}else{$color='#ffffff';}
        $fase = $this->model_faseetapa->get_id_fase($proy_id);

        $tr ='';
        $tr .= '<tr bgcolor='.$color.'>';
        $tr .= '<td>
                <div class="btn-group">
                 <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-list"></i> ACCIONES <span class="caret"></span></button>
                 <ul class="dropdown-menu" role="menu">';
        if(count($fase)!=0){
            $tr .= '<li><a href="javascript:abreVentana(\''.site_url("admin").'/dictamen_proyecto/'.$proyecto[0]['proy_id'].'/'.$fase[0]['pfec_ejecucion'].'\');" title="REGISTRO DE LA OPERACION"><i class="glyphicon glyphicon-file"></i> Resumen PDF</a></li>';
        }else{
            $tr .= '<li><a href="javascript:abreVentana(\''.site_url("admin").'/dictamen_proyecto/'.$proyecto[0]['proy_id'].'\');" title="REGISTRO DE LA OPERACION"><i class="glyphicon glyphicon-file"></i> Resumen PDF</a></li>';
        }
        if(count($fase)!=0) //// tiene fase
        {
            $tr .= '<li><a href="'.site_url("admin").'/proy/prog/'.$mod.'/'.$proyecto[0]['proy_id'].'" class="mod_aper" id="enviar_mod" title="MODIFICAR DATOS"><i class="glyphicon glyphicon-pencil"></i> Modificar</a></li>';
        }
        if($this->session->userdata('rol_id')==1)
        {
            $tr .= '<li><a href="#" data-toggle="modal" data-target="#modal_del_ff" class="del_ff" title="ELIMINAR PROYECTO" name="'.$proyecto[0]['proy_id'].'"><i class="glyphicon glyphicon-trash"></i> Eliminar</a></li>';
        }
        $tr .= '</ul>
                         </div>
                         </td>';
        /*$tr .= '<td>';
        if(count($fase)!=0){
            $tr .= '<center><a href="javascript:abreVentana(\''.site_url("admin").'/dictamen_proyecto/'.$proyecto[0]['proy_id'].'/'.$fase[0]['pfec_ejecucion'].'\');" title="REGISTRO DE LA OPERACION"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="30" HEIGHT="30"/><br>Registro</a></center>';
        }
        else{
            $tr .= '<center><a href="javascript:abreVentana(\''.site_url("admin").'/dictamen_proyecto/'.$proyecto[0]['proy_id'].'\');" title="REGISTRO DE LA OPERACION"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="30" HEIGHT="30"/><br>Registro</a></center>';
        }

        $tr .= '<center><a href="'.site_url("admin").'/proy/edit/'.$proyecto[0]['proy_id'].'/1" title="MODIFICAR OPERACION"><img src="' . base_url() . 'assets/ifinal/modificar.png" WIDTH="35" HEIGHT="35"/><br>Modificar</a></center>';
        if(count($fase)!=0){
            $tr .= '<center><a href="'.site_url("admin").'/prog/prog_fisica/'.$mod.'/'.$fase[0]['id'].'/'.$proyecto[0]['proy_id'].'" title="PROGRAMACION FISICA"><img src="' . base_url() . 'assets/ifinal/bien.png" WIDTH="30" HEIGHT="30"/><br>Programaci&oacute;n F&iacute;sica</a></center>';
        }

        if($this->session->userdata('rol_id')==1){
            $tr .= '<center><a href="'.site_url("admin").'/proy/delete/1/'.$proyecto[0]['proy_id'].'" title="ELIMINAR OPERACION" onclick="return confirmar()"><img src="' . base_url() . 'assets/ifinal/eliminar.png" WIDTH="30" HEIGHT="30"/><br>Eliminar</a></center>';
        }
        $tr .= '</td>';*/
        $tr .= '<td>';
        if($proyecto[0]['aper_proyecto']!=''){
            $tr .= '<center>'.$proyecto[0]['aper_programa'].''.$proyecto[0]['aper_proyecto'].''.$proyecto[0]['aper_actividad'].'</center>';
        }
        else{
            $tr .=  '<center><font color="red"><button class="btn btn-danger" title="RE ASIGNAR CATEGORIA PROGRAMATICA PARA LA GESTION '.$this->session->userdata('gestion').'">ASIGNAR<br>APERTURA<br>PROGRAM&Aacute;TICA<br>'.$this->session->userdata('gestion').'</button></font></center>';
        }

        $tr .= '</td>';
        $tr .= '<td>'.$proyecto[0]['proy_nombre'].'</td>';
        $tr .= '<td>'.$proyecto[0]['tipo'].'</td>';
        $tr .= '<td>'.$proyecto[0]['proy_sisin'].'</td>';
        $tr .= '<td>'.$proyecto[0]['fun_nombre'].' '.$proyecto[0]['fun_paterno'].' '.$proyecto[0]['fun_materno'].'</td>';
        $tr .= '<td>'.$proyecto[0]['ue'].'</td>';
        $tr .= '<td>'.$proyecto[0]['ur'].'</td>';
        if(count($fase)!=0){
            $nc=$this->model_faseetapa->calcula_nc($fase[0]['pfec_fecha_inicio']); //// calcula nuevo/continuo
            $ap=$this->model_faseetapa->calcula_ap($fase[0]['pfec_fecha_inicio'],$fase[0]['pfec_fecha_fin']);
            $nro_fg_act = $this->model_faseetapa->nro_fasegestion_actual($fase[0]['id'],$this->session->userdata('gestion'));
            $fase_gest = $this->model_faseetapa->fase_etapa_gestion($fase[0]['id'],$this->session->userdata("gestion"));
            $tr .='<td>'.$fase[0]['descripcion'].'</td>';
            $tr .='<td>* '.$fase[0]['fase'].'<br>* '.$fase[0]['etapa'].'</td>';
            $tr .='<td>'.$nc.'</td>';
            $tr .='<td>'.$ap.'</td>';
            $tr .='<td>'.number_format($fase[0]['pfec_ptto_fase'], 2, ',', '.').' Bs.</td>';
            $tr .='<td>';
            if($nro_fg_act!=0 && ($fase_gest[0]['estado']==1 || $fase_gest[0]['estado']==2)){
                $tr .=''.number_format($fase_gest[0]['pfecg_ppto_total'], 2, ',', '.').' Bs.';
            }
            elseif ($nro_fg_act==0) {
                $tr .= '<font color="red">la gestion no esta en curso</font>';
            }
            $tr .='</td>';
            $tr .='<td>';
            if($this->model_faseetapa->verif_fase_etapa_gestion($fase[0]['id'],$this->session->userdata("gestion"))!=0){
                if($this->model_faseetapa->nro_ffofet($fase_gest[0]['ptofecg_id'])!=0)
                {
                    $techo=$this->model_faseetapa->techo_presupuestario($fase_gest[0]['ptofecg_id']);
                    $tr .= ''.number_format($techo[0]['suma_techo'], 2, ',', '.').' Bs.';
                }
                else{$tr .= "<font color=red>S/T</font>";}
            }
            $tr .='</td>';
        }
        else{
            $tr .='<td bgcolor=#efb0b0><font color=red>Sin Fase</font></td>';
            $tr .='<td bgcolor=#efb0b0><font color=red>Sin Fase</font></td>';
            $tr .='<td bgcolor=#efb0b0><font color=red>Sin Fase</font></td>';
            $tr .='<td bgcolor=#efb0b0><font color=red>Sin Fase</font></td>';
            $tr .='<td bgcolor=#efb0b0><font color=red>Sin Fase</font></td>';
            $tr .='<td bgcolor=#efb0b0><font color=red>Sin Fase</font></td>';
            $tr .='<td bgcolor=#efb0b0><font color=red>Sin Fase</font></td>';
        }
        $pr=$this->model_proyecto->prioridad();
        $tr .='<td>';
        $tr .='<select class="form-control" onchange="doSelectAlert(event,this.value,'.$proyecto[0]['proy_id'].');">';
        foreach($pr as $pr){
            if($pr['pr_id']==$proyecto[0]['proy_pr']){
                $tr .="<option value=".$pr['pr_id']." selected>".$pr['pr_desc']."</option>";
            }
            else{
                $tr .="<option value=".$pr['pr_id'].">".$pr['pr_desc']."</option>";
            }
        }
        $tr .='</select>';
        $tr .='</td>';
        $tr .='<td>';
        $loc=$this->model_proyecto->localizacion($proyecto[0]['proy_id']);
        if(count($loc)!=0){
            $tr .='<table class="table table-bordered">';
            $tr .='<tr bgcolor="#474544">';
            $tr .='<th>PROVINCIA</th><th>MUNICIPIO</th>';
            $tr .='</tr>';
            foreach($loc as $locali){
                $tr .='<tr>';
                $tr .='<td>'.$locali['prov_provincia'].'</td>';
                $tr .='<td>'.$locali['muni_municipio'].'</td>';
                $tr .='</tr>';
            }

            $tr .='</table>';
        }

        $tr .='</td>';
        $tr .='</tr>';


        return $tr;
    }
  /*=============================================================================================================*/
  /*===================================== LISTA DE PROYECTOS CERRADOS =======================================*/  
    public function list_proyectos_cerrados()
    {
      $this->load->model('menu_modelo');
      $enlaces=$this->menu_modelo->get_Modulos(4);
      $data['enlaces'] = $enlaces;
      $data['lista_aper_padres'] = $this->model_proyecto->list_prog();//lista de aperturas padres   
      
      for($i=0;$i<count($enlaces);$i++)
      {
        $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
      }
      $data['subenlaces'] = $subenlaces;
      
       //load the view
      $data['mod']=4;
      $this->load->view('admin/programacion/proy_multi/cerrado/list_proy', $data);
    }
  /*=============================================================================================================*/  

  /*=============================== LISTA DE ARCHIVOS DOCUMENTOS DEL PROYECTO ============================*/  
    public function list_archivos($id)
    {   $enlaces=$this->menu_modelo->get_Modulos(4);
        $data['enlaces'] = $enlaces;
        for($i=0;$i<count($enlaces);$i++)
        {
          $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        $data['id'] =$id;

        $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id);
        $data['arch']=$this->model_proyecto->get_archivos($id);
        $data['if']='false';

        $data['main_content'] = 'admin/programacion/proy_multi/top/list_doc';
        $this->load->view('admin/programacion/proy_multi/top/list_doc', $data);
    }

    public function list_arch($id_p,$id_adj)
    {   $enlaces=$this->menu_modelo->get_Modulos(4);
        $data['enlaces'] = $enlaces;
        for($i=0;$i<count($enlaces);$i++)
        {
          $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        $data['id'] =$id_p;

        $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p);
        $data['arch']=$this->model_proyecto->get_archivos($id_p);
        $data['doc']=$this->model_proyecto->get_doc($id_adj);
        $data['adj']=$data['doc'][0]['adj_adjunto'];
        $data['if']='true';

        $data['main_content'] = 'admin/programacion/proy_multi/top/list_doc';
        $this->load->view('admin/programacion/proy_multi/top/list_doc', $data);
    }
  /*======================================================================================================*/  

  /*=================================================  SUBIR ARCHIVOS =================================================*/
    function subir_archivos()
    { //echo $this->input->post('file1');
        if ($this->input->server('REQUEST_METHOD') === 'POST')
          {
            $this->form_validation->set_rules('id', 'id de proyecto', 'required|trim');

            if ($this->form_validation->run())
            {
              $filename = $_FILES["file1"]["name"]; ////// datos del archivo 
              $file_basename = substr($filename, 0, strripos($filename, '.')); ///// nombre del archivo
              $file_ext = substr($filename, strripos($filename, '.')); ///// Extension del archivo
              $filesize = $_FILES["file1"]["size"]; //// Tama�o del archivo
              $allowed_file_types = array('.pdf','.docx','.doc','.xlsx','.xls','.jpg','.JPG','.png','.PNG','.JPEG','.JPG'); 

              if($filename!='')
              {
                $data_to_store = array( 
                    'proy_id' => $this->input->post('id'),
                    'documento' => strtoupper($this->input->post('doc')),
                    'tp_doc' => $this->input->post('tp_doc'),
                    'fun_id' => $this->session->userdata("fun_id"),
                    'adj_estado' =>'1',
                );

                if($filesize!=0)
                {
                    if (in_array($file_ext,$allowed_file_types))
                    { // Rename file
                      $newfilename = ''.$this->input->post('id').'_'.substr(md5(uniqid(rand())),0,5).$file_ext;
                      $data_to_store['adj_adjunto'] = "".$newfilename;
                      
                      if (file_exists("archivos/documentos/" . $newfilename))
                      {echo "El archivo ya existe.";}
                      else
                      {
                        move_uploaded_file($_FILES["file1"]["tmp_name"],"archivos/documentos/" . $newfilename);
                      }
                        $this->db->insert('_proyecto_adjuntos', $data_to_store);
                        redirect('admin/proy/proyecto/8/'.$this->input->post('id').'/true');
                    }
                   /* elseif (empty($file_basename)) {  echo "Please select a file to upload.";} 
                    elseif ($filesize > 800000000){ redirect('admin/proy/proyecto/8/'.$this->input->post('id').'/false');}
                    else{echo "Only these file typs are allowed for upload: " . implode(', ',$allowed_file_types);  unlink($_FILES["file1"]["tmp_name"]);       }  */
                }
                elseif($filesize==0)
                {
                  redirect('admin/sgp/proyecto/8/'.$this->input->post('id').'/1/1'); //// El archivo no cuenta con un tama�o
                }
              }
              else
              {
                redirect('admin/sgp/proyecto/8/'.$this->input->post('id').'/1/0');  ///// nose selecciono archivo
              }
            }
            else
              redirect('admin/sgp/proyecto/8/'.$this->input->post('id').'/false');  
          }
    }
    /*----------------------- Eliminar Archivo-------------------------*/
    function eliminar_archivo($id_p,$id_adj)
    {   
      $data['arch'] = $this->model_proyecto->get_archivo_proy($id_adj);
      
      if(file_exists("archivos/documentos/".$data['arch'][0]['adj_adjunto']))
      { 
        $file = "archivos/documentos/".$data['arch'][0]['adj_adjunto']; 
      
      unlink($file);

          $this->db->where('proy_id',$id_p);
          $this->db->where('adj_id',$id_adj);
          $this->db->delete('_proyecto_adjuntos');

       redirect('admin/sgp/proyecto/8/'.$id_p.'/true');
      }
      else
      { 
          $this->db->where('proy_id',$id_p);
          $this->db->where('adj_id',$id_adj);
          $this->db->delete('_proyecto_adjuntos');
        redirect('admin/sgp/proyecto/8/'.$id_p.'/false'); 
      }

    }
  /*================================================= END  SUBIR ARCHIVOS =================================================*/

 /*======================================================== FORMULARIOS DE REGISTRO PROYECTOS ====================================================*/
  function tecnico_operativo()
  {
    $enlaces=$this->menu_modelo->get_Modulos(4);
    $data['enlaces'] = $enlaces;
    for($i=0;$i<count($enlaces);$i++)
    {
      $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
    }
    $data['subenlaces'] = $subenlaces;
    $data['cod']=$this->model_proyecto->cod_proy();
    $data['codigo']=$data['cod'][0]['proy_codigo']+1;
    $data['tp_proy']=$this->model_proyecto->tip_proy();
    $data['tp_gasto']=$this->model_proyecto->tip_gasto();
    $data['programas'] = $this->model_proyecto->list_prog($this->session->userdata("gestion")); ///// lista aperturas padres
    $this->load->view('admin/programacion/proy_multi/formularios/form1', $data); 
  }

  function tecnico_operativo_n($form,$id)
  {
    $enlaces=$this->menu_modelo->get_Modulos(4);
    $data['enlaces'] = $enlaces;
    for($i=0;$i<count($enlaces);$i++)
    {
      $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
    }
    $data['subenlaces'] = $subenlaces;
    $data['id'] = $id;
    $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id);

      if($form=='2') ///// RESPONSABLES
      {
      $data['fun1']=$this->model_proyecto->asig_responsables(3); ////// tecnico OPERATIVO
      $data['fun2']=$this->model_proyecto->asig_responsables(4); ////// validador POA
      $data['fun3']=$this->model_proyecto->asig_responsables(5); ////// validador FINANCIERO
      $data['unidad']=$this->model_proyecto->list_unidad_org(); ////// Unidad Ejecutora

      $this->load->view('admin/programacion/proy_multi/formularios/form2', $data); //////// RESPONSABLES
      }
      elseif ($form=='3') ///// LOCALIZACION
      {
        $this->load->view('admin/programacion/proy_multi/formularios/form4', $data); /////// CLASIFICACION SECTORIAL
      }
      elseif ($form=='4') ///// CLASIFICACION 
      {
        $this->load->view('admin/programacion/proy_multi/formularios/form3', $data); /////// LOCALIZACION
      }
      elseif ($form=='5') ////// MARCO LOGICO
      {
        $this->load->view('admin/programacion/proy_multi/formularios/form5', $data); /////// MARCO LOGICO
      }
      elseif ($form=='7') ///// RESUMEN TECNICO
      {
        $this->load->view('admin/programacion/proy_multi/formularios/form7', $data); //////// 
      }
      elseif ($form=='8') ///// ARCHIVO DOCUMENTOS ADJUNTOS
      {
        $data['arch'] = $this->model_proyecto->get_archivos($id);
        $this->load->view('admin/programacion/proy_multi/formularios/form8', $data); 
      }
      elseif ($form=='9') ///// FASE Y ETAPAS DEL PROGRAMAS RECURRENTES, NO RECURRENTES Y FUNCIONES DE OPERACION
      {
        $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id); 
        $data['fase'] = $this->model_faseetapa->get_id_fase($id); ///// datos fase encendida
        $datos = $this->model_faseetapa->datos_fase_etapa($data['fase'][0]['id'],$id);  // desvuelve las fechas del proyecto

        $data['a�os']=$datos[0]['final']-$datos[0]['actual']+1;
        $data['fecha']=$datos[0]['actual'];

        $data['fase_proyecto'] = $this->model_faseetapa->fase_etapa($data['fase'][0]['id'],$id); 
        $data['nro_fg'] = $this->model_faseetapa->nro_fasegestion($data['fase'][0]['id']); 
        $a�os=$datos[0]['final']-$datos[0]['actual']+1;
        $data['gestiones']=$a�os;
        $fecha=$datos[0]['actual'];
        $data['fecha']=$fecha;

          if($a�os>=1)
          {
            $data['fase_gestion1']=$this->model_faseetapa->fase_etapa_gestion($data['fase'][0]['id'],$fecha);
            if($a�os>=2)
            {
              $data['fase_gestion2']=$this->model_faseetapa->fase_etapa_gestion($data['fase'][0]['id'],$fecha+1);
              if($a�os>=3)
              {
                $data['fase_gestion3']=$this->model_faseetapa->fase_etapa_gestion($data['fase'][0]['id'],$fecha+2);
                if($a�os>=4)
                {
                  $data['fase_gestion4']=$this->model_faseetapa->fase_etapa_gestion($data['fase'][0]['id'],$fecha+3);
                  if($a�os>=5)
                  {
                    $data['fase_gestion5']=$this->model_faseetapa->fase_etapa_gestion($data['fase'][0]['id'],$fecha+4);
                    if($a�os>=6)
                    {
                      $data['fase_gestion6']=$this->model_faseetapa->fase_etapa_gestion($data['fase'][0]['id'],$fecha+5);
                      if($a�os>=7)
                      {
                        $data['fase_gestion7']=$this->model_faseetapa->fase_etapa_gestion($data['fase'][0]['id'],$fecha+6);
                        if($a�os>=8)
                        {
                          $data['fase_gestion8']=$this->model_faseetapa->fase_etapa_gestion($data['fase'][0]['id'],$fecha+7);
                          if($a�os>=9)
                          {
                            $data['fase_gestion9']=$this->model_faseetapa->fase_etapa_gestion($data['fase'][0]['id'],$fecha+8);
                            if($a�os>=10)
                            {
                              $data['fase_gestion10']=$this->model_faseetapa->fase_etapa_gestion($data['fase'][0]['id'],$fecha+9);
                              if($a�os>=11)
                              {
                                $data['fase_gestion11']=$this->model_faseetapa->fase_etapa_gestion($data['fase'][0]['id'],$fecha+10);
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }

          $this->load->view('admin/programacion/proy_multi/fase/form_fase_update2', $data);
    }
    elseif ($form=='10') {
          $data['metas'] = $this->model_proyecto->metas_p($id); ///// metas del proyecto
          $this->load->view('admin/programacion/proy_multi/formularios/metas', $data);
    }
    
  }
  /*================================ END FORMULARIOS DE REGISTRO PROYECTOS =====================================*/

  /*==================================== VALIDA PROYECTOS =====================================================*/
  public function valida()
   { 
      if ($this->input->post('form')=='1'&& $this->input->server('REQUEST_METHOD') === 'POST') ///// DATOS GENERALES
        {
            $this->form_validation->set_rules('prog', 'Nro de Programa', 'required|trim');
            $this->form_validation->set_rules('nom_proy', 'Proyecto o Actividad', 'required|trim');
            $this->form_validation->set_rules('ini', 'Fecha de Inicio', 'required|trim');
            $this->form_validation->set_rules('fin', 'Fecha Final', 'required|trim');
            $this->form_validation->set_rules('cod_proy', 'Codigo de Proyecto', 'required|trim|callback_very_cod');
           
            if ($this->form_validation->run() ) ///// Al no existir error guarda la informacion en la BD 
            {
                /*=================================== PROYECTOS==================================*/
                $data_to_store = array( 
                    'proy_codigo' => $this->input->post('cod_proy'),
                    'proy_nombre' => strtoupper($this->input->post('nom_proy')),
                    'tp_id' => $this->input->post('tp_id'),
                    'proy_gestion_inicio_ddmmaaaa' => $this->input->post('ini'),
                    'proy_gestion_fin_ddmmaaaa' => $this->input->post('fin'),
                    'tg_id' => $this->input->post('tg'),
                    'proy_fecha_registro' => date('d/m/Y'),
                    'fun_id' => $this->session->userdata("fun_id"),
                );
                $this->db->insert('_proyectos', $data_to_store);
                $id_p=$this->db->insert_id();
                /*============================= RESPONSABLES ========================================*/
                $this->model_proyecto->add_resp_proy($id_p,$this->session->userdata("fun_id"),0,0,0,0); 
                /*==================================================================================*/

                $fechas = $this->model_proyecto->fechas_proyecto($id_p);  // devuelve las fechas del proyecto inicio-conclusion
                /*=================================== UPDATE PROYECTOS==================================*/
                $update_proyect = array('proy_gestion_inicio' => $fechas[0]['inicio'],
                                        'proy_gestion_fin' => $fechas[0]['final'],
                                        'proy_gestion_impacto' => ($fechas[0]['final']-$fechas[0]['inicio'])+1);
                $this->db->where('proy_id', $id_p);
                $this->db->update('_proyectos', $update_proyect);

                  if($this->input->post('tp_id')==1)
                  {
                    $proy=$this->input->post('proy1'); $act='000';
                  }
                  elseif ($this->input->post('tp_id')==2 || $this->input->post('tp_id')==3 || $this->input->post('tp_id')==4) 
                  {
                   $proy='0000'; $act=$this->input->post('act1');
                  }

                  $gestiones=$fechas[0]['final']-$fechas[0]['inicio'];
                  $gestion=$fechas[0]['inicio'];

                  /*=================================== APERTURA PROGRAMATICA =========================================*/
                  for($i=0;$i<=$gestiones;$i++)
                  {
                    if($this->session->userdata("gestion")==$gestion)
                    {
                      $this->model_proyecto->add_apertura($id_p,$gestion,$this->input->post('prog'),$proy,$act,$this->input->post('nom_proy'),$this->session->userdata("fun_id"));
                    }
                    else
                    {
                      $this->model_proyecto->add_apertura($id_p,$gestion,$this->input->post('prog'),'','',$this->input->post('nom_proy'),$this->session->userdata("fun_id"));
                    }
                    $gestion++;
                  }
                  /*===================================================================================================*/

                      /*=================================== TIPO DE PROYECTOS=======================================*/
                      /*=================================== PROYECTOS DE INVERSION ==================================*/
                      if ($this->input->post('tp_id')==1) {
                          
                        $update_proy = array('proy_sisin' => $this->input->post('cod_sisin'));
                        $this->db->where('proy_id', $id_p);
                        $this->db->update('_proyectos', $update_proy);
                      }
                      /*======== PROGRAMAS RECURRENTES Y NO RECURRENTES - OPERACION DE FUNCIONAMIENTO ========*/
                      elseif ($this->input->post('tp_id')==2 || $this->input->post('tp_id')==3 || $this->input->post('tp_id')==4) 
                      {
                         $proyecto = $this->model_proyecto->get_id_proyecto($id_p);
                         $data_to_store3 = array( ///// no tiene fase -> Tomando valores del proyecto
                              'proy_id' => $id_p,
                              'pfec_fecha_inicio_ddmmaaa' => $this->input->post('ini'),
                              'pfec_fecha_fin_ddmmaaa' => $this->input->post('fin'),
                              'pfec_fecha_registro' => date('d/m/Y h:i:s'),
                              'pfec_fecha_inicio' => $fechas[0]['inicio'],
                              'pfec_fecha_fin' => $fechas[0]['final'],
                              'pfec_ptto_fase' => $this->input->post('mtotal'),
                              'pfec_ejecucion' => 1,
                              'fun_id' => $this->session->userdata("fun_id"),
                              'aper_id' => $proyecto[0]['aper_id'],
                          );
                          $this->db->insert('_proyectofaseetapacomponente', $data_to_store3);
                          $id_f=$this->db->insert_id();

                          $datos = $this->model_faseetapa->datos_fase_etapa($id_f,$id_p);  // desvuelve las fechas del proyecto

                          $a�os=$datos[0]['final']-$datos[0]['actual'];
                          $fecha=$datos[0]['actual'];

                          /*=======================  fase etapa gestion ======================*/
                                for($i=0;$i<=$a�os;$i++)
                                {
                                   $data_to_store2 = array( ///// Tabla ptto_fase_gestion
                                    'pfec_id' => $id_f,
                                    'g_id' => $fecha,
                                    'fun_id' => $this->session->userdata("fun_id"),
                                  );
                                  $this->db->insert('ptto_fase_gestion', $data_to_store2); ///// inserta a ptto_fase_gestion
                                  $fecha=$fecha+1;
                                }
                          /*========================== end fase etapa gestion ======================*/

                          if($this->input->post('tp_id')==4)
                          {
                              $data_to_store4 = array('pfec_id' => $id_f,'fun_id' => $this->session->userdata("fun_id"),);
                              $this->db->insert('_componentes', $data_to_store4);  ///// insertando datos a la tabla componentes
                          }
                          
                      }

                     /*---------------------- redireccionar--------------------*/
                     if($this->input->post('tp_id')==1 || $this->input->post('tp_id')==2 || $this->input->post('tp_id')==3)
                      {
                        redirect('admin/sgp/proyecto/10/'.$id_p.''); 
                      }
                      elseif($this->input->post('tp_id')==4)
                      {
                        redirect('admin/sgp/proyecto/2/'.$id_p.'');
                      }
                                           
            }   else
            { //echo "error";
                  /*====================== SI EXISTE UN MISMO CODIGO SE VUELVE A LA MISMA VISTA============================*/
                  $enlaces=$this->menu_modelo->get_Modulos(4); 
                  $data['enlaces'] = $enlaces;
                  for($i=0;$i<count($enlaces);$i++)
                  {
                      $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
                  }
                  $data['subenlaces'] = $subenlaces;
                  $data['cod']=$this->model_proyecto->cod_proy();
                  $data['codigo']=$data['cod'][0]['proy_codigo']+1;

                  $this->load->view('admin/programacion/proy_multi/formularios/form1', $data); 
            }

        }
        elseif($this->input->post('form')=='2' && $this->input->server('REQUEST_METHOD') === 'POST') ///// RESPONSABLES DEL PROYECTO
        {  
            $this->form_validation->set_rules('fun_id_1', 'Tecnico Operativo', 'required|trim');
            $this->form_validation->set_rules('fun_id_2', 'Validador POA', 'required|trim');
            $this->form_validation->set_rules('fun_id_3', 'Validador Financiero', 'required|trim');
            $this->form_validation->set_rules('uni_ejec', 'unidad ejecutora', 'required|trim');


          if ($this->form_validation->run()) ///// Al no existir error guarda la informacion en la BD 
            {
                  /*================================  INSERTANDO RESPONSABLES AL PROYECTO  ===================================*/
                  $this->model_proyecto->update_resp_proy($this->input->post('id'),$this->input->post('fun_id_1'),$this->input->post('fun_id_2'),$this->input->post('fun_id_3'),$this->input->post('uni_ejec'),$this->input->post('uni_ejec2'));
                  /*====================================  APERTURA PROGRAMATICA  ========================*/
                  $proy = $this->model_proyecto->get_id_proyecto($this->input->post('id'));

                  $update_aper = array('uni_id' => $this->input->post('uni_ejec'));
                        $this->db->where('aper_id', $proy[0]['aper_id']);
                        $this->db->update('aperturaprogramatica', $update_aper);
                  /*==========================================================================================================*/

                redirect('admin/sgp/proyecto/3/'.$this->input->post('id').'');
            }
            else
            {
                  redirect('admin/sgp/proyecto/2/'.$this->input->post('id').'/false');
            }
        }
        elseif($this->input->post('form')=='3' && $this->input->server('REQUEST_METHOD') === 'POST') ///// CLASIFICACION
        {
          
            $this->form_validation->set_rules('clasificador3', 'Seleccione Sector', 'required|trim');
            $this->form_validation->set_rules('finalidad1', 'Seleccione Finalidad', 'required|trim');
            $this->form_validation->set_rules('pedes4', 'Seleccione Resultado', 'required|trim');
            $this->form_validation->set_rules('ptdi3', 'Seleccione Resultado', 'required|trim');

            if ($this->form_validation->run())
            {
                  if($this->input->post('clase')!='')
                  {$clase= $this->input->post('clase');}
                  elseif ($this->input->post('clase')=='') 
                    {$clase=$this->input->post('fun');}

                  if($this->input->post('pedes4')=="")
                  {
                    $id_pdes = $this->model_proyecto->get_id_pdes($this->input->post('pedes3'));  // devuelve id pedes    
                  }
                  elseif ($this->input->post('pedes4')!="") {
                    $id_pdes = $this->model_proyecto->get_id_pdes($this->input->post('pedes4'));  // devuelve id pedes  
                  }

                  $id_ptdi = $this->model_proyecto->get_id_ptdi($this->input->post('ptdi3'));  // devuelve id pedes  

                  $update_proy = array('proy_fecha_registro' => date('d/m/Y h:i:s'),
                                      'pdes_id' => $id_pdes[0]['pdes_id'],
                                      'ptdi_id' => $id_ptdi[0]['ptdi_id'],
                                      'codsectorial' => $this->input->post('clasificador3'),
                                      'fifu_id' => $clase);

                        $this->db->where('proy_id', $this->input->post('id'));
                        $this->db->update('_proyectos', $update_proy);
                  
                  redirect('admin/sgp/proyecto/4/'.$this->input->post('id').'?qRegistro='.$this->input->post('id'));  
            }
          
            else
            {
                  redirect('admin/sgp/proyecto/3/'.$this->input->post('id').'/false');
            }
        }
        elseif($this->input->post('form')=='4' && $this->input->server('REQUEST_METHOD') === 'POST') ///// LOCALIZACION
        {
            $this->form_validation->set_rules('pob_bene', 'Poblacion Beneficiaria', 'required|trim');
            $this->form_validation->set_rules('area_id', 'Area de Influencia', 'required|trim');
            $this->form_validation->set_rules('pob_c', 'Poblacion censo', 'required|trim');
            $this->form_validation->set_rules('num_pob', 'numero de poblacion beneficiada', 'required|trim');
            $this->form_validation->set_rules('porc_pob', 'porcentaje de poblacion beneficiada', 'required|trim');

            if ($this->form_validation->run()) ///// Al no existir error guarda la informacion en la BD 
            {
                      $update_proy = array('proy_fecha_registro' => date('d/m/Y h:i:s'),
                                      'proy_poblac_beneficiaria' => strtoupper($this->input->post('pob_bene')),
                                      'area_id' => $this->input->post('area_id'),
                                      'proy_poblac_beneficiada' => $this->input->post('num_pob'),
                                      'proy_porcent_poblac_beneficiada' => $this->input->post('porc_pob'),
                                      'proy_nro_hombres' => $this->input->post('nro_h'),
                                      'proy_nro_mujeres' => $this->input->post('nro_m'));

                        $this->db->where('proy_id', $this->input->post('id'));
                        $this->db->update('_proyectos', $update_proy);

                      /*---------------------- redireccionar--------------------*/
                     if($this->input->post('tp_id')==1 || $this->input->post('tp_id')==2 || $this->input->post('tp_id')==3)
                      {
                        redirect('admin/sgp/proyecto/5/'.$this->input->post('id').'');
                      }
                      elseif($this->input->post('tp_id')==4)
                      {
                        redirect('admin/sgp/proyecto/7/'.$this->input->post('id').'');
                      }
                       
            }
            else
            {
                  redirect('admin/sgp/proyecto/4/'.$this->input->post('id').'?qRegistro='.$this->input->post('id').'');
            }
        }
        elseif($this->input->post('form')=='5' && $this->input->server('REQUEST_METHOD') === 'POST') ///// MARCO LOGICO
        {
            $this->form_validation->set_rules('desc_prob', 'Descripcion del Problema', 'required|trim');
            $this->form_validation->set_rules('desc_sol', 'Descripcion de la Solucion', 'required|trim');
            $this->form_validation->set_rules('obj_gral', 'Objetivo General', 'required|trim');
            $this->form_validation->set_rules('obj_esp', 'Objetivo Especifico', 'required|trim');


            if ($this->form_validation->run())
            {
                  $update_proy = array('proy_fecha_registro' => date('d/m/Y h:i:s'),
                                        'proy_desc_problema' => strtoupper($this->input->post('desc_prob')),
                                       'proy_desc_solucion' => strtoupper($this->input->post('desc_sol')),
                                       'proy_obj_general' => strtoupper($this->input->post('obj_gral')),
                                       'proy_obj_especifico' => strtoupper($this->input->post('obj_esp')));

                        $this->db->where('proy_id', $this->input->post('id'));
                        $this->db->update('_proyectos', $update_proy);

                  redirect('admin/sgp/proyecto/7/'.$this->input->post('id').'');
            }
            else
            {
                 redirect('admin/sgp/proyecto/5/'.$this->input->post('id').'');
            }
        }

        elseif($this->input->post('form')=='7' && $this->input->server('REQUEST_METHOD') === 'POST') ///// RESUMEN TECNICO
        {
            $this->form_validation->set_rules('resumen', 'Resumen Tecnico del proyecto', 'required|trim');

            if ($this->form_validation->run())
            {
                  $update_proy = array('proy_fecha_registro' => date('d/m/Y h:i:s'),
                                      'proy_descripcion_proyecto' => strtoupper($this->input->post('resumen')));

                        $this->db->where('proy_id', $this->input->post('id'));
                        $this->db->update('_proyectos', $update_proy);

                        redirect('admin/sgp/proyecto/8/'.$this->input->post('id').'');
            }
            else
            {
                redirect('admin/sgp/proyecto/7/'.$this->input->post('id').'');
            }
        }
  }

  /*=================================== RUTA DE EDITADO ==============================================*/
  public  function ruta_edit_proy($id,$form)
  {
        $enlaces=$this->menu_modelo->get_Modulos(4);
        $data['enlaces'] = $enlaces;
        for($i=0;$i<count($enlaces);$i++) 
        {
          $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id);

        $data['nro_f'] = $this->model_faseetapa->nro_fase($id);

        if($form==1) /// DATOS GENERALES
        { 
          $data['tp_proy']=$this->model_proyecto->tip_proy();
          $data['fase'] = $this->model_faseetapa->get_id_fase($id); ///// datos fase encendida
          $data['tp_gasto']=$this->model_proyecto->tip_gasto();
          $data['programas'] = $this->model_proyecto->list_prog($this->session->userdata("gestion")); ///// lista aperturas padres

          if($data['proyecto'][0]['aper_proyecto']=='') ///// Re asignar apertura
          {
            $this->load->view('admin/programacion/proy_multi/formularios/form1_edit_aper', $data); 
          }
          elseif($data['proyecto'][0]['aper_proyecto']!='') ///// asignado
          {
            $this->load->view('admin/programacion/proy_multi/formularios/form1_edit', $data); 
          }
        }
        elseif ($form==2) ////// RESPONSABLES
        {
          $data['fun1']=$this->model_proyecto->asig_responsables(3); ////// tecnico OPERATIVO
          $data['fun2']=$this->model_proyecto->asig_responsables(4); ////// validador POA
          $data['fun3']=$this->model_proyecto->asig_responsables(5); ////// validador FINANCIERO

          $data['resp1']=$this->model_proyecto->responsable_proy($id,'1');
          $data['resp2']=$this->model_proyecto->responsable_proy($id,'2');
          $data['resp3']=$this->model_proyecto->responsable_proy($id,'3');
          $data['unidad']=$this->model_proyecto->list_unidad_org(); ////// Unidad Ejecutora
          $data['history'] = $this->model_proyecto->historial_usuario($id);

          $data['main_content'] = 'admin/programacion/proy_multi/formularios/form2_edit';
          $this->load->view('admin/programacion/proy_multi/formularios/form2_edit', $data); 
        }
        elseif ($form==3) ///// LOCALIZACION
        {
          $data['pdes'] = $this->model_proyecto->datos_pedes($data['proyecto'][0]['pdes_id']);
          $data['ptdi'] = $this->model_proyecto->datos_ptdi($data['proyecto'][0]['ptdi_id']);
          $data['lista_ptdi']=$this->model_proyecto->lista_ptdi();
          $data['codsec'] = $this->model_proyecto->codigo_sectorial($data['proyecto'][0]['codsectorial']);
          $data['fifu'] = $this->model_proyecto->datos_finalidad($data['proyecto'][0]['fifu_id']);

          $data['main_content'] = 'admin/programacion/proy_multi/formularios/form4_edit';
          $this->load->view('admin/programacion/proy_multi/formularios/form4_edit', $data); 
        }
        elseif ($form==4) ///// CLASIFICADOR
        {
          $data['main_content'] = 'admin/programacion/proy_multi/formularios/form3_edit';
          $this->load->view('admin/programacion/proy_multi/formularios/form3_edit', $data);
        }
        elseif ($form==5) ///// MARCO LOGICO
        {
          $data['main_content'] = 'admin/programacion/proy_multi/formularios/form5_edit';
          $this->load->view('admin/programacion/proy_multi/formularios/form5_edit', $data); 
        }
        elseif ($form==6) 
        {
          $data['main_content'] = 'admin/programacion/proy_multi/formularios/form6_edit';
          $this->load->view('admin/programacion/proy_multi/formularios/form6_edit', $data); 
        }
        elseif ($form==7) 
        {
          $data['main_content'] = 'admin/programacion/proy_multi/formularios/form7_edit';
          $this->load->view('admin/programacion/proy_multi/formularios/form7_edit', $data); 
        }
        elseif ($form==10) ///// METAS
        {
          $data['metas'] = $this->model_proyecto->metas_p($id); ///// metas del proyecto
          $data['main_content'] = 'admin/programacion/proy_multi/formularios/metas_edit';
          $this->load->view('admin/programacion/proy_multi/formularios/metas_edit', $data); 
        }
  }

  /*====================================== VALIDAR MODIFICADO ===============================================*/
  public function actualizar_datos()
  {
      //echo "idfase".$this->input->post('fase').' / '.$this->input->post('etapas');
        if ($this->input->post('form')=='1' && $this->input->server('REQUEST_METHOD') === 'POST') ///// DATOS GENERALES
        {
            $this->form_validation->set_rules('id', 'Id Proyecto', 'required|trim');
            $this->form_validation->set_rules('nom_proy', 'Proyecto , Actividad', 'required|trim');

            if ($this->form_validation->run()) ///// Al no existir error guarda la informacion en la BD 
            {   $tp=$this->input->post('tp_id2');
                /*----------------------------- ACTULIZANDO DATOS DEL PROYECTO -------------------------------*/
                
                $update_proy = array(
                      'proy_nombre' => strtoupper($this->input->post('nom_proy')),
                      'proy_sisin' => $this->input->post('cod_sisin'),
                      'tp_id' => $this->input->post('tp_id2'),
                      'proy_gestion_inicio_ddmmaaaa' => $this->input->post('f_ini'),
                      'proy_gestion_fin_ddmmaaaa' => $this->input->post('f_final'),
                      'tg_id' => $this->input->post('tg'),
                      'proy_fecha_registro' => date('d/m/Y h:i:s'),
                      'fun_id' => $this->session->userdata("fun_id"),
                      'estado' => '2');

                  $this->db->where('proy_id', $this->input->post('id'));
                  $this->db->update('_proyectos', $update_proy);

                  $fechas = $this->model_proyecto->fechas_proyecto($this->input->post('id'));  // devuelve las fechas del proyecto inicio-conclusion
                  $update_proyect = array(
                      'proy_gestion_inicio' => $fechas[0]['inicio'],
                      'proy_gestion_fin' => $fechas[0]['final'],
                      'proy_gestion_impacto' => ($fechas[0]['final']-$fechas[0]['inicio'])+1);
                  $this->db->where('proy_id', $this->input->post('id'));
                  $this->db->update('_proyectos', $update_proyect);

                  //////////////////////////////////////// en casi de que la fecha inicial se adelante /////////////////////////////////////
                  if($fechas[0]['inicio']<$this->input->post('gi'))
                  {
                    $fecha=$fechas[0]['inicio'];
                    $nro=$this->input->post('gi')-$fechas[0]['inicio'];
                    for($i=1;$i<=$nro;$i++)
                    {
                      $this->model_proyecto->add_apertura($this->input->post('id'),$fecha,$this->input->post('prog'),'','','',$this->session->userdata("fun_id"));
                      $fecha++;
                    }
                    if($tp==2 || $tp==3 || $tp==4)
                    {
                      $fase = $this->model_faseetapa->get_id_fase($this->input->post('id')); //// datos fase activa

                      $update_fase = array('pfec_fecha_inicio_ddmmaaa' => $this->input->post('f_ini'),'pfec_fecha_fin_ddmmaaa' => $this->input->post('f_final'),'pfec_fecha_inicio' => $fechas[0]['inicio'],'pfec_fecha_fin' => $fechas[0]['final']);
                      $this->db->where('pfec_id', $fase[0]['id']);
                      $this->db->update('_proyectofaseetapacomponente', $update_fase);

                      $fecha=$fechas[0]['inicio'];
                      for($i=1;$i<=$nro;$i++)
                      {
                          $data_to_store2 = array( ///// Tabla ptto_fase_gestion
                          'pfec_id' => $fase[0]['id'],
                          'g_id' => $fecha,
                          'fun_id' => $this->session->userdata("fun_id"),
                        );
                        $this->db->insert('ptto_fase_gestion', $data_to_store2);
                        $fecha++;
                      }
                    }
                  }
                  ////////////// en caso en que la fecha inicial se reduzca /////////////////////
                  if($fechas[0]['inicio']>$this->input->post('gi'))
                  {
                    $fecha=$this->input->post('gi');
                    $nro=$fechas[0]['inicio']-$this->input->post('gi');
                    for($i=1;$i<=$nro;$i++)
                    {
                      $aper = $this->model_proyecto->aper_id($this->input->post('id'),$fecha); //// aper_id buscado
                      $this->model_proyecto->delete_aper_id($aper[0]['aper_id']); //// elimando apertura programatica
                      $fecha++;
                    }
                    if($tp==2 || $tp==3 || $tp==4)
                    {
                      $fase = $this->model_faseetapa->get_id_fase($this->input->post('id')); //// datos fase activa

                      $update_fase = array('pfec_fecha_inicio_ddmmaaa' => $this->input->post('f_ini'),'pfec_fecha_fin_ddmmaaa' => $this->input->post('f_final'),'pfec_fecha_inicio' => $fechas[0]['inicio'],'pfec_fecha_fin' => $fechas[0]['final']);
                      $this->db->where('pfec_id', $fase[0]['id']);
                      $this->db->update('_proyectofaseetapacomponente', $update_fase);

                      $fecha=$this->input->post('gi');
                      for($i=1;$i<=$nro;$i++)
                      {
                          $this->db->where('g_id', $fecha);
                          $this->db->where('pfec_id', $fase[0]['id']);
                          $this->db->delete('ptto_fase_gestion');
                        $fecha++;
                      }
                    }

                  }
                  else{
                    $this->model_proyecto->update_apertura($this->input->post('aper_id'),$this->input->post('prog'),$this->input->post('proy'),$this->input->post('act'),$this->input->post('nom_proy'),$this->session->userdata("fun_id"));
                  }               
                  /*------------------------------------------------------------------------------------------*/
                  
                  //////////////////////////////////////// en casi de que la fecha final se amplie /////////////////////////////////////
                  if($fechas[0]['final']>$this->input->post('gf'))
                  {
                    $fecha=$this->input->post('gf')+1;
                    $nro=$fechas[0]['final']-$this->input->post('gf');
                    for($i=1;$i<=$nro;$i++)
                    {
                      $this->model_proyecto->add_apertura($this->input->post('id'),$fecha,$this->input->post('prog'),'','',$this->input->post('nom_proy'),$this->session->userdata("fun_id"));
                      $fecha++;
                    }

                    if($tp==2 || $tp==3 || $tp==4)
                    {
                      $fase = $this->model_faseetapa->get_id_fase($this->input->post('id')); //// datos fase activa

                      $update_fase = array('pfec_fecha_inicio_ddmmaaa' => $this->input->post('f_ini'),'pfec_fecha_fin_ddmmaaa' => $this->input->post('f_final'),'pfec_fecha_inicio' => $fechas[0]['inicio'],'pfec_fecha_fin' => $fechas[0]['final']);
                      $this->db->where('pfec_id', $fase[0]['id']);
                      $this->db->update('_proyectofaseetapacomponente', $update_fase);

                      $fecha=$this->input->post('gf')+1;
                      for($i=1;$i<=$nro;$i++)
                      {
                          $data_to_store2 = array( ///// Tabla ptto_fase_gestion
                          'pfec_id' => $fase[0]['id'],
                          'g_id' => $fecha,
                          'fun_id' => $this->session->userdata("fun_id"),
                        );
                        $this->db->insert('ptto_fase_gestion', $data_to_store2);
                        $fecha++;
                      }
                    }
                  }
                  //////////////////////////////////////// en casi de que la fecha final se redusca /////////////////////////////////////
                  elseif($fechas[0]['final']<$this->input->post('gf'))
                  {
                    $fecha=$this->input->post('gf');
                    $nro=$this->input->post('gf')-$fechas[0]['final'];
                    for($i=1;$i<=$nro;$i++)
                    {
                      $aper = $this->model_proyecto->aper_id($this->input->post('id'),$fecha); //// aper_id buscado
                      $this->model_proyecto->delete_aper_id($aper[0]['aper_id']); //// elimando apertura programatica
                      $fecha--;
                    }
                    if($tp==2 || $tp==3 || $tp==4)
                    {
                      $fase = $this->model_faseetapa->get_id_fase($this->input->post('id')); //// datos fase activa

                      $update_fase = array('pfec_fecha_inicio_ddmmaaa' => $this->input->post('f_ini'),'pfec_fecha_fin_ddmmaaa' => $this->input->post('f_final'),'pfec_fecha_inicio' => $fechas[0]['inicio'],'pfec_fecha_fin' => $fechas[0]['final']);
                      $this->db->where('pfec_id', $fase[0]['id']);
                      $this->db->update('_proyectofaseetapacomponente', $update_fase);

                      $fecha=$this->input->post('gf');
                      for($i=1;$i<=$nro;$i++)
                      {
                          $this->db->where('g_id', $fecha);
                          $this->db->where('pfec_id', $fase[0]['id']);
                          $this->db->delete('ptto_fase_gestion');
                        $fecha++;
                      }
                    }

                  }

                  /*=================================================== CAMBIAR EL TIPO DEL PROYECTO ==========================================================*/
                  if($this->input->post('tp_ant')!=$this->input->post('tp_id2'))
                  {
                    /*----------------------------------------------- Inversion a otro programa --------------------------------------------*/
                    if($this->input->post('tp_ant')==1 && $this->model_faseetapa->nro_fase($this->input->post('id'))!=0) /// inversion y ya tiene fase
                    {
                      $fase = $this->model_faseetapa->get_id_fase($this->input->post('id')); //// datos fase activa

                      $update_fase = array('pfec_fecha_inicio_ddmmaaa' => $this->input->post('f_ini'),'pfec_fecha_fin_ddmmaaa' => $this->input->post('f_final'),'pfec_fecha_inicio' => $fechas[0]['inicio'],'pfec_fecha_fin' => $fechas[0]['final'],'pfec_ejecucion' => 1,'pfec_ptto_fase' => $this->input->post('mtotal'));
                        $this->db->where('pfec_id', $fase[0]['id']);
                        $this->db->update('_proyectofaseetapacomponente', $update_fase);

                    }
                    elseif($this->input->post('tp_ant')==1 && $this->model_faseetapa->nro_fase($this->input->post('id'))==0) //// inversion y no tiene fase resgitrado
                    {
                         $proyecto = $this->model_proyecto->get_id_proyecto($this->input->post('id'));
                         $data_to_store3 = array( ///// no tiene fase -> Tomando valores del proyecto
                              'proy_id' => $this->input->post('id'),
                              'pfec_descripcion' => strtoupper($this->input->post('nom_proy')),
                              'pfec_fecha_inicio_ddmmaaa' => $this->input->post('f_ini'),
                              'pfec_fecha_fin_ddmmaaa' => $this->input->post('f_final'),
                              'pfec_fecha_registro' => date('d/m/Y h:i:s'),
                              'pfec_fecha_inicio' => $fechas[0]['inicio'],
                              'pfec_fecha_fin' => $fechas[0]['final'],
                              'pfec_ptto_fase' => $this->input->post('mtotal'),
                              'pfec_ejecucion' => 1,
                              'fun_id' => $this->session->userdata("fun_id"),
                              'aper_id' => $proyecto[0]['aper_id'],
                          );
                          $this->db->insert('_proyectofaseetapacomponente', $data_to_store3);
                          $id_f=$this->db->insert_id();

                          $datos = $this->model_faseetapa->datos_fase_etapa($id_f,$this->input->post('id'));  // desvuelve las fechas del proyecto

                          $a�os=$datos[0]['final']-$datos[0]['actual'];
                          $fecha=$datos[0]['actual'];

                          /*=======================  fase etapa gestion ======================*/
                                for($i=0;$i<=$a�os;$i++)
                                {
                                   $data_to_store2 = array( ///// Tabla ptto_fase_gestion
                                    'pfec_id' => $id_f,
                                    'g_id' => $fecha,
                                    'fun_id' => $this->session->userdata("fun_id"),
                                  );
                                  $this->db->insert('ptto_fase_gestion', $data_to_store2); ///// inserta a ptto_fase_gestion
                                  $fecha=$fecha+1;
                                }
                          /*========================== end fase etapa gestion ======================*/

                          if($this->input->post('tp_id2')==4)
                          {
                              $data_to_store4 = array('pfec_id' => $id_f,'fun_id' => $this->session->userdata("fun_id"),);
                              $this->db->insert('_componentes', $data_to_store4);  ///// insertando datos a la tabla componentes
                          }
                    }
                    /*------------------------------------------------------------------------------------------------------------------------*/
                    /*----------------------------------------------- programas recurrentes, no recurrentes a operacion de funcionamiento--------------------------------------*/
                    if($this->input->post('tp_ant')==2 || $this->input->post('tp_ant')==3)
                    {
                      if($this->input->post('tp_id2')==4)
                      {
                        $fase = $this->model_faseetapa->get_id_fase($this->input->post('id')); //// datos fase activa
                        if($this->model_componente->componentes_nro($fase[0]['id'])==0) //// no tiene componente
                        {
                            $data_to_store4 = array('pfec_id' => $fase[0]['id'],'fun_id' => $this->session->userdata("fun_id"),);
                            $this->db->insert('_componentes', $data_to_store4);  ///// insertando datos a la tabla componentes
                        }
                      }
                    }
                  }
                  /*==========================================================================================================================*/
                  
                    if($tp==2 || $tp==3 || $tp==4)
                      {
                        $update_fase = array('pfec_ptto_fase' => $this->input->post('mtotal'));

                        $this->db->where('proy_id', $this->input->post('id'));
                        $this->db->update('_proyectofaseetapacomponente', $update_fase);
                      }

                        /*---------------------- redireccionar--------------------*/
                    if($tp==1 || $tp==2 || $tp==3)
                      {
                          redirect('admin/sgp/edit/'.$this->input->post('id').'/10');
                      }
                    elseif($tp==4)
                      {
                        redirect('admin/sgp/edit/'.$this->input->post('id').'/2');
                      }
            }
            else
            {
                 redirect('admin/sgp/edit/'.$this->input->post('id').'/1');
            }

        }
        elseif($this->input->post('form')=='2' && $this->input->server('REQUEST_METHOD') === 'POST') ///// RESPONSABLES
        {
            $this->form_validation->set_rules('fun_id_1', 'Tecnico Operativo', 'required|trim');
            $this->form_validation->set_rules('fun_id_2', 'Validador POA', 'required|trim');
            $this->form_validation->set_rules('fun_id_3', 'Validador Financiero', 'required|trim');
            $this->form_validation->set_rules('uni_ejec', 'unidad ejecutora', 'required|trim');


            if ($this->form_validation->run()) ///// Al no existir error guarda la informacion en la BD 
            {
                  $this->model_proyecto->update_resp_proy($this->input->post('id'),$this->input->post('fun_id_1'),$this->input->post('fun_id_2'),$this->input->post('fun_id_3'),$this->input->post('uni_ejec'),$this->input->post('uni_ejec2'));
                 
                  $proy = $this->model_proyecto->get_id_proyecto($this->input->post('id'));
                  /*====================================  �PERTURA PROGRAMATICA  ========================*/
                  $update_aper = array('uni_id' => $this->input->post('uni_ejec'));
                        $this->db->where('aper_id', $proy[0]['aper_id']);
                        $this->db->update('aperturaprogramatica', $update_aper);

                  redirect('admin/sgp/edit/'.$this->input->post('id').'/3'); 
            }
            else
            {
                redirect('admin/sgp/edit'.$this->input->post('id').'/2/false');
               
            }
        }
        elseif($this->input->post('form')=='3' && $this->input->server('REQUEST_METHOD') === 'POST') ///// CLASIFICACION
        {
            $this->form_validation->set_rules('clasificador3', 'Seleccione Sector', 'required|trim');
            $this->form_validation->set_rules('finalidad1', 'Seleccione Finalidad', 'required|trim');
            $this->form_validation->set_rules('pedes4', 'Seleccione Resultado', 'required|trim');
            $this->form_validation->set_rules('ptdi3', 'Seleccione Resultado', 'required|trim');

            if ($this->form_validation->run())
            {
                  if($this->input->post('clase')!=''){$clase= $this->input->post('clase');}
                  elseif ($this->input->post('clase')=='')  {$clase=$this->input->post('fun');}

                  if($this->input->post('pedes4')==""){$id_pdes = $this->model_proyecto->get_id_pdes($this->input->post('pedes3'));}   // devuelve id pedes  
                  elseif ($this->input->post('pedes4')!="") {$id_pdes = $this->model_proyecto->get_id_pdes($this->input->post('pedes4')); }  // devuelve id pedes  

                  $id_ptdi = $this->model_proyecto->get_id_ptdi($this->input->post('ptdi3'));  // devuelve id pedes 

                  $update_proy = array('proy_fecha_registro' => date('d/m/Y h:i:s'),
                                       'codsectorial' => $this->input->post('clasificador3'),
                                       'pdes_id' => $id_pdes[0]['pdes_id'],
                                       'ptdi_id' => $id_ptdi[0]['ptdi_id'],
                                       'fifu_id' => $clase);

                        $this->db->where('proy_id', $this->input->post('id'));
                        $this->db->update('_proyectos', $update_proy);

                  redirect('admin/sgp/edit/'.$this->input->post('id').'/4?qRegistro='.$this->input->post('id').'');      
            }
            else
            {
                  redirect('admin/sgp/edit/'.$this->input->post('id').'/3/false');
            }
        }
        elseif($this->input->post('form')=='4' && $this->input->server('REQUEST_METHOD') === 'POST') ///// LOCALIZACION
        {
            $this->form_validation->set_rules('pob_bene', 'Poblacion Beneficiaria', 'required|trim');
            $this->form_validation->set_rules('area_id', 'Area de Influencia', 'required|trim');
            $this->form_validation->set_rules('pob_c', 'Poblacion censo', 'required|trim');
            $this->form_validation->set_rules('num_pob', 'numero de poblacion beneficiada', 'required|trim');
            $this->form_validation->set_rules('porc_pob', 'porcentaje de poblacion beneficiada', 'required|trim');

            if ($this->form_validation->run()) ///// Al no existir error guarda la informacion en la BD 
            {
                      $update_proy = array('proy_fecha_registro' => date('d/m/Y h:i:s'),
                                      'proy_poblac_beneficiaria' => strtoupper($this->input->post('pob_bene')),
                                      'area_id' => $this->input->post('area_id'),
                                      'proy_poblac_beneficiada' => $this->input->post('num_pob'),
                                      'proy_porcent_poblac_beneficiada' => $this->input->post('porc_pob'),
                                      'proy_nro_hombres' => $this->input->post('nro_h'),
                                      'proy_nro_mujeres' => $this->input->post('nro_m'));

                        $this->db->where('proy_id', $this->input->post('id'));
                        $this->db->update('_proyectos', $update_proy);

                      /*---------------------- redireccionar--------------------*/
                       if($this->input->post('tp')==1 || $this->input->post('tp')==2 || $this->input->post('tp')==3)
                        {
                          redirect('admin/sgp/edit/'.$this->input->post('id').'/5');
                        }
                        elseif($this->input->post('tp')==4)
                        {
                          redirect('admin/sgp/edit/'.$this->input->post('id').'/7');
                        }
            }
            else
            {
                  redirect('admin/sgp/edit/'.$this->input->post('id').'/4?qRegistro='.$this->input->post('id'));
            }
        }
        elseif($this->input->post('form')=='5' && $this->input->server('REQUEST_METHOD') === 'POST') ///// MARCO LOGICO
        {
            $this->form_validation->set_rules('desc_prob', 'Descripcion del Problema', 'required|trim');
            $this->form_validation->set_rules('desc_sol', 'Descripcion de la Solucion', 'required|trim');
            if ($this->form_validation->run())
            {
                  $update_proy = array('proy_fecha_registro' => date('d/m/Y h:i:s'),
                                       'proy_desc_problema' => strtoupper($this->input->post('desc_prob')), 
                                       'proy_desc_solucion' => strtoupper($this->input->post('desc_sol')),
                                       'proy_obj_general' => strtoupper($this->input->post('obj_gral')),
                                       'proy_obj_especifico' => strtoupper($this->input->post('obj_esp')));

                        $this->db->where('proy_id', $this->input->post('id'));
                        $this->db->update('_proyectos', $update_proy);

                  redirect('admin/sgp/edit/'.$this->input->post('id').'/7');
            }
            else
            {
                 redirect('admin/sgp/edit/'.$this->input->post('id').'/5/false');
            }
        }

        elseif($this->input->post('form')=='7' && $this->input->server('REQUEST_METHOD') === 'POST') ///// RESUMEN TECNICO
        {
            $this->form_validation->set_rules('resumen', 'Resumen Tecnico del proyecto', 'required|trim');

            if ($this->form_validation->run())
            {
                  $update_proy = array('proy_fecha_registro' => date('d/m/Y h:i:s'),
                                       'proy_descripcion_proyecto' => strtoupper($this->input->post('resumen')));

                        $this->db->where('proy_id', $this->input->post('id'));
                        $this->db->update('_proyectos', $update_proy);

                    
                        redirect('admin/sgp/proyecto/8/'.$this->input->post('id').'');
            }
            else
            {
                  redirect('admin/sgp/edit/'.$this->input->post('id').'/7');
            }
        }
  }

  /*===================================== OBTIENE LOS DATOS DE LOS RESPONSABLES  =======================================*/
    public function get_responsables()
    {
        if($this->input->is_ajax_request() && $this->input->post())
        {
            $post = $this->input->post();
            $id = $post['id_p']; /// id proyecto
            $tp = $post['tp']; /// tipo de responsable
            $id = $this->security->xss_clean($id);
            $tp = $this->security->xss_clean($tp);
            $dato_resp = $this->model_proyecto->responsable_proy($id,$tp);
            //caso para modificar el codigo de proyecto y actividades
            foreach($dato_resp as $row){
                $result = array(
                    'proy_id' => $row['proy_id'],
                    "fun_id" =>$row['fun_id'],
                    "responsable" =>$row['fun_nombre'].' '.$row['fun_paterno'].' '.$row['fun_materno']
                );
            }
            echo json_encode($result);
        }else{
            show_404();
        }
    }
  /*==================================================================================================================*/

   /*=========================================== ASIGNAR PROYECTO ====================================================*/
      function asignar_proyecto()
      {
        if($this->input->is_ajax_request() && $this->input->post()){
            $this->form_validation->set_rules('id_p', 'id del proyecto', 'required|trim');
            //=========================== mensajes =========================================
            $this->form_validation->set_message('required', 'El campo es es obligatorio');
            /*if ($this->form_validation->run() ) {*/
                $post = $this->input->post();
                $id = $post['id_p'];
                $tp = $post['tp'];
                //================ evitar enviar codigo malicioso ==========
                $id= $this->security->xss_clean($id);
                $est_proy= $this->security->xss_clean($tp);
                
                $update_proy = array('proy_estado' => $est_proy,
                                      'fun_id' => $this->session->userdata("fun_id"));
                $this->db->where('proy_id', $id);
                $this->db->update('_proyectos', $update_proy);

        }else{
            show_404();
        }
    }
    /*==================================================================================================================*/

  /*===================== eliminar proyecto ========================*/
  public  function delete_proyecto(){
        if($this->input->is_ajax_request()) 
        {
            $post = $this->input->post();
            $id = $post['id_p'];
            $tp = $post['tipo'];

            if($tp==1){ //// proyectos de inversion
              $nro=$this->model_componente->nro_fase($id);
            }
            elseif ($tp==2 || $tp==3) { //// programas recurrentes, no recurrentes
              $fase=$this->model_componente->get_id_fase($id);
              $nro=$this->model_componente->nro_componentes_id($fase[0]['id']);
            }
            elseif ($tp==4) { //// programas e funcionamiento
              $fase=$this->model_componente->get_id_fase($id);
              $comp=$this->model_componente->componentes_id($fase[0]['id']);
              $nro=$this->model_producto->nro_productos_gest($comp[0]['com_id']);
            }

             if($nro==0)
             {
                  $proy=$this->model_proyecto->get_id_proyecto($id);
                   if($tp==1){
                      $this->model_proyecto->delete_proyecto($id,$proy[0]['aper_id'],0,0,$tp);
                    }
                    elseif ($tp==2 || $tp==3) { 
                      $fase=$this->model_componente->get_id_fase($id);
                      $this->model_proyecto->delete_proyecto($id,$proy[0]['aper_id'],$fase[0]['id'],0,$tp);
                    }
                    elseif ($tp==4) { 
                      $fase=$this->model_componente->get_id_fase($id);
                      $comp=$this->model_componente->componentes_id($fase[0]['id']);
                      $this->model_proyecto->delete_proyecto($id,$proy[0]['aper_id'],$fase[0]['id'],$comp[0]['com_id'],$tp);
                    }

                 echo "true";
             }
             else
             {
              echo "false"; //// tiene dependencias
             } 
              }else{
                  show_404();
              }
    }

  /*===================== eliminar programas ========================*/
  public  function delete_proyecto2(){
        if($this->input->is_ajax_request()) 
        {
            $post = $this->input->post();
            $id = $post['id_p'];
            $tp = $post['tipo'];

            if($tp==1){ //// proyectos de inversion
              $nro=$this->model_componente->nro_fase($id);
            }
            elseif ($tp==2 || $tp==3) { //// programas recurrentes, no recurrentes
              $fase=$this->model_componente->get_id_fase($id);
              $nro=$this->model_componente->nro_componentes_id($fase[0]['id']);
            }
            elseif ($tp==4) { //// programas e funcionamiento
              $fase=$this->model_componente->get_id_fase($id);
              $comp=$this->model_componente->componentes_id($fase[0]['id']);
              $nro=$this->model_producto->nro_productos_gest($comp[0]['com_id']);
            }

             if($nro==0)
             {
                  $proy=$this->model_proyecto->get_id_proyecto($id);
                   if($tp==1){
                      $this->model_proyecto->delete_proyecto($id,$proy[0]['aper_id'],0,0,$tp);
                    }
                    elseif ($tp==2 || $tp==3) { 
                      $fase=$this->model_componente->get_id_fase($id);
                      $this->model_proyecto->delete_proyecto($id,$proy[0]['aper_id'],$fase[0]['id'],0,$tp);
                    }
                    elseif ($tp==4) { 
                      $fase=$this->model_componente->get_id_fase($id);
                      $comp=$this->model_componente->componentes_id($fase[0]['id']);
                      $this->model_proyecto->delete_proyecto($id,$proy[0]['aper_id'],$fase[0]['id'],$comp[0]['com_id'],$tp);
                    }

                 echo "true";
             }
            
             else
             {
              echo "false"; //// tiene dependencias
             } 
              }else{
                  show_404();
              }
    }


    /*===================================== OBTIENE LOS DATOS DEL PROYECTO =======================================*/
    public function get_proyecto()
    {
        if($this->input->is_ajax_request() && $this->input->post())
        {
            $post = $this->input->post();
            $cod = $post['id_proy'];
            $id = $this->security->xss_clean($cod);
            $dato_proy = $this->model_proyecto->get_id_proyecto($id);
            //caso para modificar el codigo de proyecto y actividades
            foreach($dato_proy as $row){
                $result = array(
                    'proy_id' => $row['proy_id'],
                    "proy_nombre" =>$row['proy_nombre']
                );
            }
            echo json_encode($result);
        }else{
            show_404();
        }
    }
    /*===================================== OBTIENE LOS DATOS DEL PROYECTO =======================================*/
        /*===================================== OBTIENE LOS DATOS DE LA META =======================================*/
    public function get_meta()
    {
        if($this->input->is_ajax_request() && $this->input->post())
        {
            $post = $this->input->post();
            $id = $post['id_m'];
            $id = $this->security->xss_clean($id);
            $dato_meta = $this->model_proyecto->metas_id($id);
            //caso para modificar el codigo de proyecto y actividades
            foreach($dato_meta as $row){
                $result = array(
                    'meta_id' => $row['meta_id'],
                    "proy_id" =>$row['proy_id'],
                    "meta_descripcion" =>$row['meta_descripcion'],
                    "meta_meta" =>$row['meta_meta'],
                    "meta_ejec" =>$row['meta_ejec'],
                    "meta_efic" =>$row['meta_efic']
                );
            }
            echo json_encode($result);
        }else{
            show_404();
        }
    }
    /*====================================================================================================================*/

    /*============================================= PROYECTO OBSERVADO =============================================*/
      function add_obs()
      {
        if($this->input->is_ajax_request() && $this->input->post()){
            $this->form_validation->set_rules('id', 'id del proyecto', 'required|trim');
            $this->form_validation->set_rules('observacion', 'Observacion', 'required');
            //=========================== mensajes =========================================
            $this->form_validation->set_message('required', 'El campo es es obligatorio');
            /*if ($this->form_validation->run() ) {*/
                $post = $this->input->post();
                $id = $post['id'];
                $tp = $post['tp'];
                $obs = $post['observacion'];
                //================ evitar enviar codigo malicioso ==========
                $id= $this->security->xss_clean($id);
                $tp= $this->security->xss_clean($tp);
                $obs = $this->security->xss_clean(trim($obs));
                
                $update_proy = array('t_obs' => $tp,
                                'proy_observacion' => $obs,
                                'proy_estado' => 1,
                                'fun_id' => $this->session->userdata("fun_id"));
                $this->db->where('proy_id', $id);
                $this->db->update('_proyectos', $update_proy);

        }else{
            show_404();
        }
    }
/*====================================================================================================================*/
function verif(){
        if($this->input->is_ajax_request()) 
        {
            $post = $this->input->post();
            $cod1 = $post['prog'];
            $cod2= $post['proy'];
            $cod3 = $post['act'];
            $variable= $this->model_proyecto->nro_aperturas_hijos($cod1,$cod2,$cod3,$this->session->userdata("gestion"));
             if($variable == true)
             {
             echo "true";; ///// no existe un CI registrado
             }
             else
             {
              echo "false";; //// existe el CI ya registrado
             } 
              }else{
                  show_404();
              }
    }
/*================================ PROYECTOS APROBADOS ==================================*/
        function list_proyectos_aprobados()
    {
        $enlaces=$this->menu_modelo->get_Modulos(1);
        $data['enlaces'] = $enlaces;
        for($i=0;$i<count($enlaces);$i++) 
        {
          $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;

        $data['proy1'] = $this->model_proyecto->proyecto_n(1); 
        $data['proy2'] = $this->model_proyecto->proyecto_n(2);
        $data['proy3'] = $this->model_proyecto->proyecto_n(3);
        $data['proy4'] = $this->model_proyecto->proyecto_n(4);

        $data['mod']=4;
        //load the view
        $data['main_content'] = 'admin/programacion/proy_anual/vfin/list_proy_ok';
        $this->load->view('admin/programacion/proy_anual/vfin/list_proy_ok', $data);
    }
  /*====================================================================================================================*/
/*---------------------------------- PDF PROYECTOS -------------------------------------*/   
             public function ficha_tecnica($id_p){
        $data= $this->model_proyecto->get_id_proyecto($id_p);
        $pdes = $this->model_proyecto->datos_pedes($data[0]['pdes_id']);
        $fifu = $this->model_proyecto->datos_finalidad($data[0]['fifu_id']);
        // Creaci�n del objeto de la clase heredada
        $html2='';
$activ=1;
$pro=1;
$insu=1;
$numero=1;
        $html2 .= '
        <html>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <head>
          <style>
            body{
              font-family: sans-serif;
            }
            @page {
              margin: 30px 50px;
            }
            header {
             
            }
            header h1{
              margin: 5px 0;
            }
            header h2{
              margin: 0 0 10px 0;
            }
            footer {
              position: fixed;
              left: 0px;
              bottom: -70px;
              right: 0px;
              height: 40px;
              border-bottom: 2px solid #F5F5F5;
            }

            footer .page:after {
              content: counter(page);
            }
            footer table {
              width: 100%;
            }
            footer p {
              text-align: right;
            }
            footer .izq {
              text-align: left;
            }
            table{font-size: 9px;
            width: 100%;
            }
            .siipp{ width:150px;}
          </style>'.$this->session->userdata('franja_decorativa').'
          </head>
        <body>
          <header>
          
          </header>
          <footer>
            <table class="table table-bordered">
              <tr>
                <td>
                    <p class="izq">
                      '.$this->session->userdata("sistema_pie").'
                    </p>
                </td>
                <td>
                  <p class="page">
                    P&aacute;gina
                  </p>
                </td>
              </tr>
            </table>
          </footer>
          <div id="content">
          <div class="rojo"></div>
          <div class="verde"></div>
          
              <table style="width:100%;"> 
              <tr>
              <td style="20%;">
              <img src="/var/www/html/SISTEMA/assets/img/logo.jpg" class="siipp"/>
              </td>
              <td style="width:60%;">
                <p class="izq">
                <font size=1>
                <b>ENTIDAD : </b> '.$data[0]['proy_codigo'].' - '.$this->session->userdata('entidad').' <br>
                <b>POA - PLAN OPERATIVO ANUAL : </b>'.$this->session->userdata("gestion").'<br>
                <b>'.$this->session->userdata('sistema').'</b><br>
                <b>FORMULARIO : </b> DICTAMEN 
                </font>
                </p>
              </td>
              <td style="width:20%;">
               <img src="/var/www/html/SISTEMA/assets/img/escudo.jpg" width="100px"/>
              </td>
              </tr>
              </table>
            <br>
            <p>
            <table style="width:100%;" cellspacing="0" cellpadding="0" border="1" style="width:100%;border-collapse:#F5F5F5;">
                <thead>                         
                    <tr>

                        <td colspan="2" style="width:80%;">
                            
                            <center><font size=2><b>'.$data[0]['proy_nombre'].'</b></font></center>
                            <center><font size=1>Proyecto/Actividad</font></center>
                            
                        </td>
                    </tr>
                    
                    <tr>
                        <td>';
                                        if($data[0]['tp_id']=='1')
                                        {
                                            $html2.='<center><font size=2><b>'.$data[0]['proy_sisin'].'</font></center>';   
                                        }
                                        elseif($data[0]['tp_id']=='2' || $data[0]['tp_id']=='3' || $data[0]['tp_id']=='4')
                                        {
                                            $html2.='<font size=2><b>N/A</b></font>'; 
                                        }
                                        $html2.='
                            <center><font size=1>SISIN</font></center>
                        </td>
                        <td>
                            <center><font size=2><b>'.$data[0]['aper_programa'].''.$data[0]['aper_proyecto'].''.$data[0]['aper_actividad'].'</b></font></center>
                            <center><font size=1>Apertura Programatica</font></center>
                        </td>
                       
                    </tr>

                     <tr>
                        <td colspan="2">
                            <center>';
                                        if($data[0]['uni1']=='0')
                                        {
                                            $html2.='<font size=1>No Seleccionado</font>';   
                                        }
                                        elseif($data[0]['uni1']!='0')
                                        {
                                            $html2.='<font size=2><b>'.$data[0]['unidad'].'</b></font>'; 
                                        }
                                        $html2.='</center>
                            <center><font size=1>Unidad Ejecutora</font></center>
                        </td>
                    </tr>
                </thead>                      
            </table><br><br>
                    <table  border="1" style="width:100%;border-collapse:collapse;">
                    <tr bgcolor="#696969">
                        <td style="width:100%;">
                            <font size=1 color="#ffffff"><b> 1. FASE ETAPA ACTIVA</b></font>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:100%;">
                        ';
            if($this->model_componente->verif_fase($data[0]['proy_id'])!=0) ///// verificando si tiene fase encendida
                  {
                    $fase = $this->model_componente->get_id_fase($data[0]['proy_id']); //// datos de la fase activa
                    $html2.='
                            <table  border="1" style="width:100%;border-collapse:collapse;">
                                <thead>                         
                                    <tr bgcolor="#DFF0D8">
                                      <td style="width:10%;" >FASE</td>
                                      <td style="width:5%;" >ETAPA</td>
                                      <td style="width:15%;" >DESCRIPCI&Oacute;N</td>
                                      <td style="width:7%;" >F. INICIAL</td>
                                      <td style="width:7%;" >F. FINAL</td> 
                                      <td style="width:5%;" >EJECUCI&Oacute;N</td> 
                                      <td style="width:10%;" >ANUAL/PLURIANUAL</td>
                                      <td style="width:10%;" >NUEVO/CONTINUIDAD</td> 
                                    </tr>
                                </thead>
                                <tbody>
                                 <tr>
                                      <td>'.utf8_decode($fase[0]['fase']).'</td>
                                      <td>'.utf8_decode($fase[0]['etapa']).'</td>
                                      <td>'.utf8_decode($fase[0]['descripcion']).'</td>
                                      <td>'.date('d-m-Y',strtotime($fase[0]['inicio'])).'</td>
                                      <td>'.date('d-m-Y',strtotime($fase[0]['final'])).'</td>
                                      <td>'.utf8_decode($fase[0]['ejec']).'</td>';

                                      if($fase[0]['pfec_fecha_fin']-$fase[0]['pfec_fecha_inicio']==0)
                                      {
                                        $html2.='<td>ANUAL</td>';
                                      }
                                      elseif($fase[0]['pfec_fecha_fin']-$fase[0]['pfec_fecha_inicio']!=0) 
                                      {
                                        $html2.='<td>PLURIANUAL</td>';
                                      }

                                      if($fase[0]['pfec_fecha_inicio']==$this->session->userdata("gestion")){
                                                    $html2.='<td>NUEVO</td>';
                                      }
                                      elseif ($fase[0]['pfec_fecha_inicio']!=$this->session->userdata("gestion")) {
                                                    $html2.= '<td>CONTINUIDAD</td>';
                                      }

                                   
                                      $html2.='
                                     
                                    </tr>
                                </tbody>
                            </table>';
                            }else{
                              $html2.='
                            <table>
                              <tr>
                                <td> NO SE TIENE FACE ACTIVA 
                                </td>
                              </tr>
                            </table>
                              ';
                            }
                        $html2.='</td>
                    </tr>
                    </table>


          <table  border="1" style="width:100%;border-collapse:collapse;">
                <thead>
                    <tr bgcolor="#696969">
                        <td style="width:100%;">
                            <font size=1 color="#ffffff"><b> 2. DATOS GENERALES</b></font>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:100%;">
                            <table  border="1" style="width:100%;border-collapse:collapse;">
                                <thead>                         
                                    <tr>
                                        <td style="width:25%;">
                                            <font size=1>Tipo</font>
                                        </td>
                                        <td style="width:85%;">
                                            '.utf8_decode($data[0]['tipo']).'
                                        </td>
                                    </tr>
                                  
                                    <tr>
                                        <td style="width:25%;">
                                            <font size=1>Duraci&oacute;n</font>
                                        </td>
                                        <td style="width:85%;">
                                        ';
                                        if($data[0]['duracion']=='1')
                                        {
                                            $html2.='Anual';   
                                        }
                                        elseif($data[0]['duracion']>'1')
                                        {
                                            $html2.='PluriAnual'; 
                                        }
                                        
                                        $html2.='</td>
                                    </tr>
                                </thead>                      
                            </table>
                        </td>
                    </tr>
                    <tr bgcolor="#696969">
                        <td style="width:100%;">
                            <font size=1 color="#ffffff"><b> 3. RESPONSABLES</b></font>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:100%;">
                            <table  border="1" style="width:100%;border-collapse:collapse;">
                                <thead>                         
                                    <tr>
                                        <td style="width:25%;">
                                            <font size=1>Responsable T&eacute;cnico</font>
                                        </td>
                                        <td style="width:85%;">';
                                        if($data[0]['fun_id1']=='0')
                                        {
                                            $html2.='No Seleccionado';   
                                        }
                                        elseif($data[0]['fun_id1']!='0')
                                        {
                                            $html2.=''.utf8_decode($data[0]['funcionario1']).''; 
                                        }
                                        $html2.='</td>
                                    </tr>
                                    <tr>
                                        <td style="width:25%;">
                                            <font size=1>Validador POA</font>
                                        </td>
                                        <td style="width:85%;">';
                                        if($data[0]['fun_id2']=='0')
                                        {
                                            $html2.='No Seleccionado';   
                                        }
                                        elseif($data[0]['fun_id2']!='0')
                                        {
                                            $html2.=''.utf8_decode($data[0]['funcionario2']).''; 
                                        }
                                        $html2.='</td>
                                    </tr>
                                    <tr>
                                        <td style="width:25%;">
                                            <font size=1>Validador Financiero</font>
                                        </td>
                                        <td style="width:85%;">';
                                        if($data[0]['fun_id3']=='0')
                                        {
                                            $html2.='No Seleccionado';   
                                        }
                                        elseif($data[0]['fun_id3']!='0')
                                        {
                                            $html2.=''.utf8_decode($data[0]['funcionario3']).''; 
                                        }
                                        $html2.='</td>
                                    </tr>
                                    <tr>
                                        <td style="width:25%;">
                                            <font size=1>Unidad Ejecutora</font>
                                        </td>
                                        <td style="width:85%;">';
                                        if($data[0]['uni1']=='0')
                                        {
                                            $html2.='No Seleccionado';   
                                        }
                                        elseif($data[0]['uni1']!='0')
                                        {
                                            $html2.=''.utf8_decode($data[0]['unidad']).''; 
                                        }
                                        $html2.='</td>
                                    </tr>
                                </thead>                      
                            </table>
                        </td>
                    </tr>
                    <tr bgcolor="#696969">
                        <td style="width:100%;">
                            <font size=1 color="#ffffff"><b> 4. LOCALIZACI&Oacute;N</b></font>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:100%;">
                            
                             ';
                              $nro_dep= $this->model_proyecto->nro_proy_dep($data[0]['proy_id']);
                                               if($nro_dep!=0)
                                               { ////uno
                                                $dep= $this->model_proyecto->proy_dep($data[0]['proy_id']);
                                                     foreach ($dep as $rowd)
                                                     {/////dos
                                                    $html2.= '
                                                    <table border="1">
                                                      <tr>
                                                          <td style="width:25%;">
                                                              <font size=1>'.$rowd['dep_departamento'].'</font>
                                                          </td>
                                                          <td style="width:85%;">';
                                                            $nro_prov= $this->model_proyecto->nro_proy_prov($data[0]['proy_id']);
                                                              if($nro_prov!=0)
                                                                { ////tres
                                                                  $prov= $this->model_proyecto->proy_prov($data[0]['proy_id']);
                                                                   foreach ($prov as $rowp)
                                                                   {/////cuatro
                                                                    if($rowp['dep_id']==$rowd['dep_id'])
                                                                    {///////cuerpo

                                                                    $html2.= '<table  border="1" style="width:100%;border-collapse:collapse;">
                                                                          <thead>
                                                                            <tr>
                                                                              <td style="width:25%; background:;">PROVINCIA: '.$rowd['dep_departamento'].' - '.$rowp['prov_provincia'].'</td>
                                                                              <td style="width:85%;">';
                                                                                $nro_mun= $this->model_proyecto->nro_proy_mun($data[0]['proy_id']);
                                                                                 if($nro_mun!=0)
                                                                                  {
                                                                                    $muni= $this->model_proyecto->proy_mun($data[0]['proy_id']);
                                                                                  $html2.= '
                                                                                        <table  border="1" style="width:100%;border-collapse:collapse;">
                                                                                            <thead>
                                                                                              <tr bgcolor="#C2C9D1">
                                                                                                <td style="width:15%; ">REGI&Oacute;N</td>
                                                                                                <td style="width:15%; ">MUNICIPIO</td>
                                                                                                <td style="width:5%;  ">%P</td>
                                                                                                <td style="width:20%; ">POB. HOMBRES</td>
                                                                                                <td style="width:20%; ">POB. MUJERES</td>
                                                                                                <td style="width:20%; "></td>
                                                                                              </tr>';
                                                                                              foreach ($muni as $rowm)
                                                                                           {
                                                                                            if($rowm['prov_id']==$rowp['prov_id'])
                                                                                            {
                                                                                            $html2.='<tr>
                                                                                                    <td>'.$rowm['reg_region'].'</td>
                                                                                                    <td>'.$rowm['muni_municipio'].'</td>
                                                                                                    <td>'.round($rowm['pm_pondera']).'%</td>
                                                                                                    <td>'.number_format($rowm['muni_poblacion_hombres']*10000).'</td>
                                                                                                    <td>'.number_format($rowm['muni_polacion_mujeres']*10000).'</td>
                                                                                                    <td>';
                                                                                                     $nro_can= $this->model_proyecto->nro_proy_cant($data[0]['proy_id']);
                                                                                                      if($nro_can!=0)
                                                                                                      {
                                                                                                        $can= $this->model_proyecto->proy_cant($data[0]['proy_id']);
                                                                                                        foreach ($can as $rowc)
                                                                                                         {
                                                                                                            if($rowc['muni_id']==$rowm['muni_id'])
                                                                                                            {
                                                                                                            $html2.='<table  border="1" style="width:100%;border-collapse:collapse;">
                                                                                                                <thead>
                                                                                                                  <tr>
                                                                                                                        <td>'.$rowc['can_canton'].'</td>
                                                                                                                  </tr>
                                                                                                                </thead>
                                                                                                              </table>';
                                                                                                            }
                                                                                                          
                                                                                                          }

                                                                                                      }
                                                                                                      elseif ($nro_can==0)
                                                                                                      {
                                                                                                      $html2.= 'N/A';
                                                                                                      }
                                                                                        $html2.='</td>
                                                                                                </tr>';
                                                                                            }
                                                                                            
                                                                                           }
                                                                                    $html2.= '</thead>
                                                                                      </table>';
                                                                                  }
                                                                                  elseif ($nro_mun==0) 
                                                                                  {
                                                                                  $html2.= "no tiene municipios registrados";
                                                                                  }

                                                                            $html2.= '
                                                                              </tr>
                                                                            </thead>
                                                                            </table>';




                                                                    }//////cuerpo
                                                                    
                                                                   }/////cuatro
                                                                }/////tres
                                                                elseif ($nro_prov==0) {
                                                                $html2.= "no tiene provincias";
                                                                }

                                                        $html2.='</td>';
                                                      $html2.='</tr></table>';
                                                     }////dos
                                               }//////uno
                                               elseif ($nro_dep==0) {
                                              $html2.= "No tiene Ubicacion";
                                               }
                                          $html2.= '
                        </td>
                    </tr>
                     <tr bgcolor="#696969">
                        <td style="width:100%;">
                            <font size=1 color="#ffffff"><b> 5. POBLACI&Oacute;N BENEFICIARIA</b></font>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:100%;">
                            <table  border="1" style="width:100%;border-collapse:collapse;">
                                <thead>                         
                                    <tr>
                                        <td style="width:25%;">
                                            <font size=1>Poblaci&oacute;n Beneficiaria</font>
                                        </td>
                                        <td style="width:85%;">';
                                        if($data[0]['proy_poblac_beneficiaria']=='')
                                        {
                                            $html2.='por registrar';   
                                        }
                                        elseif($data[0]['proy_poblac_beneficiaria']!='')
                                        {
                                            $html2.=''.$data[0]['proy_poblac_beneficiaria'].''; 
                                        }
                                        $html2.='</td>
                                    </tr>
                                    <tr>
                                        <td style="width:25%;">
                                            <font size=1>Area de Influencia</font>
                                        </td>
                                        <td style="width:85%;">';
                                        if($data[0]['area_id']=='1')
                                        {
                                            $html2.='Urbano';   
                                        }
                                        elseif($data[0]['area_id']!='2')
                                        {
                                            $html2.='Rural'; 
                                        }
                                        elseif($data[0]['area_id']=='0')
                                        {
                                            $html2.='No seleccionado'; 
                                        }
                                        $html2.='</td>
                                    </tr>
                                    <tr>
                                        <td style="width:25%;">
                                            <font size=1>Latitud</font>
                                        </td>
                                        <td style="width:85%;">por registrar</td>
                                    </tr>
                                    <tr>
                                        <td style="width:25%;">
                                            <font size=1>Longitud</font>
                                        </td>
                                        <td style="width:85%;">por registrar</td>
                                    </tr>
                                    <tr>
                                        <td style="width:25%;">
                                            <font size=1>Poblaci&oacute;n</font>
                                        </td>
                                        <td style="width:85%;">2.585.484</td>
                                    </tr>
                                    <tr>
                                        <td style="width:25%;">
                                            <font size=1>Poblaci&oacute;n Beneficiada</font>
                                        </td>
                                        <td style="width:85%;">'.$data[0]['proy_poblac_beneficiada'].'</td>
                                    </tr>
                                    <tr>
                                        <td style="width:25%;">
                                            <font size=1>% Poblaci&oacute;n Beneficiada</font>
                                        </td>
                                        <td style="width:85%;">'.$data[0]['proy_porcent_poblac_beneficiada'].'</td>
                                    </tr>
                                    
                                </thead>                      
                            </table>
                        </td>
                    </tr>
                    <tr bgcolor="#696969">
                        <td style="width:100%;">
                            <font size=1 color="#ffffff"><b> 6. CLASIFICACI&Oacute;N</b></font>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:100%;">
                            <table  border="1" style="width:100%;border-collapse:collapse;">
                                <thead>                         
                                    <tr>
                                        <td style="width:25%;">
                                            <font size=1>PDES</font>
                                        </td>
                                        
                                        <td style="width:85%;">';
                                            if($data[0]['pdes_id']!='0')
                                            {
                                                $html2.='
                                                   ['.$pdes[0]['id1'].'] <u>PILAR</u> : '.utf8_decode($pdes[0]['pilar']).'<br> 
                                                   ['.$pdes[0]['id2'].'] <u>META</u> : '.utf8_decode($pdes[0]['meta']).'<br> 
                                                   ['.$pdes[0]['id3'].'] <u>RESULTADO</u> : '.utf8_decode($pdes[0]['resultado']).'<br>
                                                   ['.$pdes[0]['id4'].'] <u>ACCI&Oacute;N</u> : '.utf8_decode($pdes[0]['accion']).'
                                                        ';   
                                            }
                                            elseif($data[0]['pdes_id']=='0')
                                            {
                                                $html2.='
                                                        <u>PILAR</u> : - <br> 
                                                        <u>META</u> : - <br> 
                                                        <u>RESULTADO</u> : - <br>
                                                        <u>ACCI&Oacute;N</u> : - 
                                                        '; 
                                            }
                                            $html2.='
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:25%;">
                                            <font size=1>PTDI</font>
                                        </td>
                                       
                                        <td style="width:85%;">';
                                            if($data[0]['pdes_id']!='0')
                                            {
                                                $html2.='
                                                        <u>PILAR</u> : sin datos<br> 
                                                        <u>META</u> : sin datos<br> 
                                                        <u>RESULTADO</u> : sin datos<br>
                                                        <u>ACCI&Oacute;N</u> : sin datos
                                                        ';   
                                            }
                                            elseif($data[0]['pdes_id']=='0')
                                            {
                                                $html2.='
                                                        <u>PILAR</u> : - <br> 
                                                        <u>META</u> : - <br> 
                                                        <u>RESULTADO</u> : - <br>
                                                        <u>ACCI&Oacute;N</u> : - 
                                                        '; 
                                            }
                                            $html2.='
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:25%;">
                                            <font size=1>Clasificador Sectorial </font>
                                        </td>
                                        <td style="width:85%;">
                                            
                                            <u>SECTOR</u> : '.utf8_decode($data[0]['sector']).'<br> 
                                            <u>SUBSECTOR</u> : '.utf8_decode($data[0]['subsector']).'<br> 
                                            <u>ACTIVIDAD</u> : '.utf8_decode($data[0]['descclasificadorsectorial']).'
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:25%;">
                                            <font size=1>Finalidad Funci&oacute;n </font>
                                        </td>
                                        <td style="width:85%;">';
                                            if($data[0]['fifu_id']!='0')
                                            {
                                                $html2.='
                                                        <u>FINALIDAD</u> : '.utf8_decode($fifu[0]['finalidad']).'<br> 
                                                        <u>FUNCI&Oacute;N</u> : '.utf8_decode($fifu[0]['funcion']).'<br> 
                                                        <u>CLASE</u> : '.utf8_decode($fifu[0]['accion']).'
                                                        ';   
                                            }
                                            elseif($data[0]['fifu_id']=='0')
                                            {
                                                $html2.='
                                                        <u>FINALIDAD</u> : - <br> 
                                                        <u>FUNCI&Oacute;N</u> : - <br> 
                                                        <u>CLASE</u> : -
                                                        '; 
                                            }
                                            $html2.='
                                            
                                        </td>
                                    </tr>
                                </thead>                      
                            </table>
                        </td>
                    </tr>
                    <tr bgcolor="#696969">
                        <td style="width:100%;">
                            <font size=1 color="#ffffff"><b> 7. MARCO L&Oacute;GICO</b></font>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:100%;">
                            <table  border="1" style="width:100%;border-collapse:collapse;">
                                <thead>                         
                                    <tr>
                                        <td style="width:25%;">
                                            <font size=1>Descripci&oacute;n del Problema</font>
                                        </td>
                                        <td style="width:85%;">'.$data[0]['desc_prob'].'</td>
                                    </tr>
                                    <tr>
                                        <td style="width:25%;">
                                            <font size=1>Descripcion de la Soluci&oacute;n</font>
                                        </td>
                                        <td style="width:85%;">'.$data[0]['desc_sol'].'</td>
                                    </tr>
                                    <tr>
                                        <td style="width:25%;">
                                            <font size=1>Objetivo General</font>
                                        </td>
                                        <td style="width:85%;">'.$data[0]['obj_gral'].'</td>
                                    </tr>
                                    <tr>
                                        <td style="width:25%;">
                                            <font size=1>Objetivo Especifico</font>
                                        </td>
                                        <td style="width:85%;">'.$data[0]['obj_esp'].'</td>
                                    </tr>
                                </thead>                      
                            </table>
                        </td>
                    </tr>
                    <tr bgcolor="#696969">
                        <td style="width:100%;">
                            <font size=1 color="#ffffff"><b> 8. RESUMEN T&Eacute;CNICO DEL PROYECTO</b></font>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:100%;">
                            <table  border="1" style="width:100%;border-collapse:collapse;">
                                <thead>                         
                                    <tr>
                                        <td style="width:25%;">
                                            <font size=1>Descripci&oacute;n del Problema</font>
                                        </td>
                                        <td style="width:85%;">'.$data[0]['proy_descripcion_proyecto'].'</td>
                                    </tr>
                                </thead>                      
                            </table>
                        </td>
                    </tr>
                   ';
                  
                  if($this->model_componente->verif_fase($data[0]['proy_id'])!=0) ///// verificando si tiene fase encendida
                  {
                    $html2.='
                    <tr bgcolor="#696969">
                        <td style="width:100%;">
                            <font size=1 color="#ffffff"><b> 9. PRESUPUESTO</b></font>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:100%;">
                            <table  border="1" style="width:100%;border-collapse:collapse;">
                                <thead>                         
                                    <tr bgcolor="">
                                      <td style="width:20%;"><b>PRESUPUESTO ASIGNADO</b></td>
                                      <td style="width:30%;">'.number_format($fase[0]['pfec_gestion']).' Bs.</td>
                                      <td style="width:20%;"><b>PRESUPUESTO EJECUTADO</b></td>
                                      <td style="width:30%;">'.number_format($fase[0]['pfec_referencial']).' Bs.</td>
                                    </tr>
                                </thead>
                                <tbody>';
                                $faseg= $this->model_proyecto->fase_etapa_gestiones($fase[0]['id']);
                                $sumg=0; $sume=0;
                                foreach ($faseg as $row)
                                {
                                  $html2.='
                                  <tr>
                                      <td style="width:25%;">PRESUPUESTO GESTI&Oacute;N '.$row['pfecg_gestion'].'</td>
                                      <td style="width:25%;">'.number_format($row['pfecg_ppto_total']).' Bs.</td>
                                      <td style="width:25%;">PRESUPUESTO EJECUTADO '.$row['pfecg_gestion'].'</td>
                                      <td style="width:25%;">'.number_format($row['pfecg_ppto_ejecutado']).' Bs.</td>
                                  </tr>';
                                  $sumg=$sumg+$row['pfecg_ppto_total'];
                                  $sume=$sume+$row['pfecg_ppto_ejecutado'];
                                }
                                $html2.='
                                  <tr>
                                      <td style="width:25%;">TOTAL :</td>
                                      <td style="width:25%;">'.number_format($sumg).' Bs.</td>
                                      <td style="width:25%;">TOTAL EJECUTADO</td>
                                      <td style="width:25%;">'.number_format($sume).' Bs.</td>
                                  </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>'; $nro_c= $this->model_componente->nro_componentes_id($fase[0]['id']);
                    if($nro_c!=0)
                    {//componente
                    $comp= $this->model_componente->componentes_id($fase[0]['id']); $sum_c=1;$productos=1;
                        foreach ($comp as $row)
                          { ///////////////// foreach componentes////////////////
                            $html2.='
                    <tr bgcolor="#696969">
                                <td style="width:100%;">
                                    <font size=1 color="#ffffff"><b> 10. COMPONENTES</b></font>
                                </td>
                    </tr>
                    <tr> <td>
                          <table  border="1" style="width:100%;border-collapse:collapse;">
                                <thead>                         
                                    <tr bgcolor="">
                                      <td style="width:20%;"><b>'.$sum_c.' - COMPONENTE</b></td>
                                      <td style="width:30%;">'.$row['com_componente'].'</td>
                                      <td style="width:20%;"><b>PONDERAC&Iacute;ON %</b></td>
                                      <td style="width:30%;">'.$row['com_ponderacion'].' %</td>
                                    </tr>
                                </thead>  
                            </table>
                    </tr></td>';
                     $nro_p= $this->model_producto->nro_productos_gest($row['com_id']);
                            if($nro_p!=0)
                            { /////////////productos/////////////
                              $prod= $this->model_producto->producto_id_gestion($row['com_id'],$this->session->userdata("gestion"));
                              $html2.='
                    <tr bgcolor="#696969">
                       <td style="width:100%;">
                                        <font size=1 color="#ffffff"><b> 11. PRODUCTOS '.$pro.'</b></font>
                       </td>
                    </tr>
              
                    <tr bgcolor="#568A89">
                      <td>
                        <table>
                          <tr>
                                        <td style="width:5%;"><center><b><font color="#ffffff" >Nro</font></b></center></td>
                                        <td style="width:20%;"><center><b><font color="#ffffff">OBJETIVO DEL PRODUCTO</font></b></center></td>
                                        <td style="width:5%;"><center><b><font color="#ffffff">TI</font></b></center></td>
                                        <td style="width:5%;"><center><b><font color="#ffffff">INDICADOR</font></b></center></td>
                                        <td style="width:5%;"><center><b><font color="#ffffff">L/B</font></b></center></td>
                                        <td style="width:5%;"><center><b><font color="#ffffff">META</font></b></center></td>
                                        <td style="width:5%;"><center><b><font color="#ffffff">%P</font></b></center></td>
                                        <td style="width:50%;"><center><b><font color="#ffffff">TEMPORALIZACI&Oacute;N DE LA META</font></b></center></td>
                          </tr>
                        </table>
                      </td>
                    </tr>';
                    foreach ($prod as $row)
                                    { 
                    $html2.='
                    <tr>
                    <td>
                        <table>
                          <tr>
                                                <td style="width:5%;">'.$numero.'</td>'; $numero++; $html2.='
                                                <td style="width:20%;">'.$row['prod_producto'].'</td>
                                                <td style="width:5%;">'.$row['indi_abreviacion'].'</td>
                                                <td style="width:5%;">'.$row['prod_indicador'].'</td>';
                                                if($row['indi_id']==1){
                                                  $html2.= '<td>'.round($row['prod_linea_base'],1).'</td>';  
                                                  }
                                                  elseif ($row['indi_id']==2){
                                                  $html2.= '<td style="width:5%;">'.round($row['prod_linea_base'],1).' %</td>';
                                                  }
                                                if($row['indi_id']==1){
                                                  $html2.= '<td style="width:5%;">'.round($row['prod_meta'],1).'</td>';  
                                                  }
                                                  elseif ($row['indi_id']==2){
                                                  $html2.= '<td style="width:5%;">'.round($row['prod_meta'],1).' %</td>';
                                                  }
                                                  $html2.='
                                                  <td style="width:5%px;">'.round($row['prod_ponderacion'],1).' %</font></td>
                                                  <td style="width:50%;">
                                          <table style="width:100%;">
                                          <thead>
                                              <tr bgcolor="#DCDCDC">
                                              <td colspan=13=13>GESTI&Oacute;N : '.$this->session->userdata("gestion").'</td>
                                              </tr>                        
                                              <tr bgcolor="#DCDCDC">
                                                  <td style="width:1%;">Edo</td>
                                                  <td ><center>Ene.</center></td>
                                                  <td style="width:1%;"><center>Feb.</center></td>
                                                  <td style="width:1%;"><center>Mar.</center></td>
                                                  <td style="width:1%;"><center>Abr.</center></td>
                                                  <td style="width:1%;"><center>May.</center></td>
                                                  <td style="width:1%;"><center>Jun.</center></td>
                                                  <td style="width:1%;"><center>Jul.</center></td>
                                                  <td style="width:1%;"><center>Ago.</center></td>
                                                  <td style="width:1%;"><center>Sep.</center></td>
                                                  <td style="width:1%;"><center>Oct.</center></td>
                                                  <td style="width:1%;"><center>Nov.</center></td>
                                                  <td style="width:1%;"><center>Dic.</center></td>
                                              </tr>';
                                              

                                              if($row['indi_id']==1)
                                              {
                                                  $html2.='<tr bgcolor="#F5F5DC"><td style="width:1%;" bgcolor="#DCDCDC"><b>P</b></td>
                                                  <td style="width:1%;">'.round($row['pg_fis_1p'],1).'</td>
                                                  <td style="width:1%;">'.round($row['pg_fis_2p'],1).'</td>
                                                  <td style="width:1%;">'.round($row['pg_fis_3p'],1).'</td>
                                                  <td style="width:1%;">'.round($row['pg_fis_4p'],1).'</td>
                                                  <td style="width:1%;">'.round($row['pg_fis_5p'],1).'</td>
                                                  <td style="width:1%;">'.round($row['pg_fis_6p'],1).'</td>
                                                  <td style="width:1%;">'.round($row['pg_fis_7p'],1).'</td>
                                                  <td style="width:1%;">'.round($row['pg_fis_8p'],1).'</td>
                                                  <td style="width:1%;">'.round($row['pg_fis_9p'],1).'</td>
                                                  <td style="width:1%;">'.round($row['pg_fis_10p'],1).'</td>
                                                  <td style="width:1%;">'.round($row['pg_fis_11p'],1).'</td>
                                                  <td style="width:1%;">'.round($row['pg_fis_12p'],1).'</td>
                                              </tr>
                                              <tr bgcolor="#F5F5DC"><td style="width:1%;" bgcolor="#DCDCDC"><b>E</b></td>
                                                  <td style="width:1%;">'.round($row['pg_fis_1e'],1).'</td>
                                                  <td style="width:1%;">'.round($row['pg_fis_2e'],1).'</td>
                                                  <td style="width:1%;">'.round($row['pg_fis_3e'],1).'</td>
                                                  <td style="width:1%;">'.round($row['pg_fis_4e'],1).'</td>
                                                  <td style="width:1%;">'.round($row['pg_fis_5e'],1).'</td>
                                                  <td style="width:1%;">'.round($row['pg_fis_6e'],1).'</td>
                                                  <td style="width:1%;">'.round($row['pg_fis_7e'],1).'</td>
                                                  <td style="width:1%;">'.round($row['pg_fis_8e'],1).'</td>
                                                  <td style="width:1%;">'.round($row['pg_fis_9e'],1).'</td>
                                                  <td style="width:1%;">'.round($row['pg_fis_10e'],1).'</td>
                                                  <td style="width:1%;">'.round($row['pg_fis_11e'],1).'</td>
                                                  <td style="width:1%;">'.round($row['pg_fis_12e'],1).'</td>
                                              </tr>';
                                              }
                                              elseif ($row['indi_id']==2) {
                                                  $html2.='<tr bgcolor="#F5F5DC"><td style="width:1%;" bgcolor="#DCDCDC"><b>P</b></td>
                                                  <td style="width:1%;">'.round($row['pg_fis_1p'],1).'%</td>
                                                  <td style="width:1%;">'.round($row['pg_fis_2p'],1).'%</td>
                                                  <td style="width:1%;">'.round($row['pg_fis_3p'],1).'%</td>
                                                  <td style="width:1%;">'.round($row['pg_fis_4p'],1).'%</td>
                                                  <td style="width:1%;">'.round($row['pg_fis_5p'],1).'%</td>
                                                  <td style="width:1%;">'.round($row['pg_fis_6p'],1).'%</td>
                                                  <td style="width:1%;">'.round($row['pg_fis_7p'],1).'%</td>
                                                  <td style="width:1%;">'.round($row['pg_fis_8p'],1).'%</td>
                                                  <td style="width:1%;">'.round($row['pg_fis_9p'],1).'%</td>
                                                  <td style="width:1%;">'.round($row['pg_fis_10p'],1).'%</td>
                                                  <td style="width:1%;">'.round($row['pg_fis_11p'],1).'%</td>
                                                  <td style="width:1%;">'.round($row['pg_fis_12p'],1).'%</td>
                                              </tr>
                                              <tr bgcolor="#F5F5DC"><td style="width:1%;" bgcolor="#DCDCDC"><b>E</b></td>
                                                  <td style="width:1%;">'.round($row['pg_fis_1e'],1).'%</td>
                                                  <td style="width:1%;">'.round($row['pg_fis_2e'],1).'%</td>
                                                  <td style="width:1%;">'.round($row['pg_fis_3e'],1).'%</td>
                                                  <td style="width:1%;">'.round($row['pg_fis_4e'],1).'%</td>
                                                  <td style="width:1%;">'.round($row['pg_fis_5e'],1).'%</td>
                                                  <td style="width:1%;">'.round($row['pg_fis_6e'],1).'%</td>
                                                  <td style="width:1%;">'.round($row['pg_fis_7e'],1).'%</td>
                                                  <td style="width:1%;">'.round($row['pg_fis_8e'],1).'%</td>
                                                  <td style="width:1%;">'.round($row['pg_fis_9e'],1).'%</td>
                                                  <td style="width:1%;">'.round($row['pg_fis_10e'],1).'%</td>
                                                  <td style="width:1%;">'.round($row['pg_fis_11e'],1).'%</td>
                                                  <td style="width:1%;">'.round($row['pg_fis_12e'],1).'%</td>
                                              </tr>';
                                              }
                                              $html2.='
                                          </thead>
                                          </table>
                                      </td>
                          </tr>
                        </table>                        
                    </td>
                    </tr>';
                    $nro_a= $this->model_actividad->nro_actividades_gest($row['prod_id']); $actividades=1;
                              if($nro_a!=0)
                              { ///////////////////actividades/////////////
                                $html2.='
                    <tr bgcolor="#696969">
                        <td style="width:100%;">
                                        <font size=1 color="#ffffff"><b> 12. ACTIVIDADES '.$pro.'.'.$activ.' </b></font>
                       </td> 
                    </tr>
                    <tr>
                      <td>
                          <table>
                                                <tr bgcolor="#5C6C86">
                                                    <td style="width:10%;"><center><b><font color="#ffffff">OBJETIVO DE LA ACTIVIDAD</b></center></td>
                                                    <td style="width:5%;"><center><b><font color="#ffffff">TI</b></center></td>
                                                    <td style="width:5%;"><center><b><font color="#ffffff">F. INICIO</b></center></td>
                                                    <td style="width:5%;"><center><b><font color="#ffffff">F. FINAL</b></center></td>
                                                    <td style="width:8%;"><center><b><font color="#ffffff">DURACI&Oacute;N</b></center></td>
                                                    <td style="width:8%;"><center><b><font color="#ffffff">COSTO</b></center></td>
                                                    <td style="width:5%;"><center><b><font color="#ffffff">%p</b></center></td>
                                                    <td style="width:2%;"><center><b><font color="#ffffff">L/B</b></center></td>
                                                    <td style="width:5%;"><center><b><font color="#ffffff">META</b></center></td>
                                                    <td style="width:40%;"><center><b><font color="#ffffff">TEMPORALIZACI&Oacute;N DE LA META</b></center></td>
                                                </tr>
                          </table>
                      </td>
                    </tr>';
                    $act= $this->model_actividad->actividad_id_gestion($row['prod_id'],$this->session->userdata("gestion"));
                           foreach ($act as $rowa)
                            {////////////foreach actividad///////
                    $html2.='
                    <tr>
                      <td>
                          <table>
                                          <tr bgcolor="#f5f5f5">
                                              <td style="width:10%;">'.$rowa['act_actividad'].'</td>
                                              <td style="width:5%;">'.$rowa['indi_abreviacion'].'</td>
                                              
                                              <td style="width:5%;">'.date('d-m-Y',strtotime($rowa['act_fecha_inicio'])).'</td>
                                              <td style="width:5%;">'.date('d-m-Y',strtotime($rowa['act_fecha_final'])).'</td>
                                              <td style="width:8%;">'.$rowa['act_duracion'].'</td>
                                              <td style="width:8%;">'.number_format($rowa['act_presupuesto']).'Bs.</td>
                                              <td style="width:5%;">'.$rowa['act_ponderacion'].'%</td>';
                                              if($rowa['indi_id']==1){
                                                $html2.= '<td style="width:2%;">'.round($rowa['act_linea_base'],1).'</td>';  
                                                }
                                                elseif ($row['indi_id']==2){
                                                $html2.= '<td style="width:2%;">'.round($rowa['act_linea_base'],1).' %</td>';
                                                }
                                              if($rowa['indi_id']==1){
                                                $html2.= '<td style="width:5%;">'.round($rowa['act_meta'],1).'</td>';  
                                                }
                                                elseif ($rowa['indi_id']==2){
                                                $html2.= '<td style="width:5%;">'.round($rowa['act_meta'],1).' %</td>';
                                                }
                                              $html2.='
                                              <td style="width:40%;">
                                                 <!--temporalizacion de la meta-->
                                                 <table style="width:100%;">
                                                <thead>
                                                    <tr bgcolor="#DCDCDC">
                                                    <td colspan=7>GESTI&Oacute;N : '.$this->session->userdata("gestion").'</td>
                                                    </tr>                         
                                                    ';
                                                    if($rowa['indi_id']==1)
                                                    {
                                                      $html2.='
                                                      <tr bgcolor="#DCDCDC">
                                                        <td style="width:1%;">P/E</td>
                                                        <td style="width:1%;"><center>Ene.</center></td>
                                                        <td style="width:1%;"><center>Feb.</center></td>
                                                        <td style="width:1%;"><center>Mar.</center></td>
                                                        <td style="width:1%;"><center>Abr.</center></td>
                                                        <td style="width:1%;"><center>May.</center></td>
                                                        <td style="width:1%;"><center>Jun.</center></td>
                                                    </tr>
                                                    <tr>
                                                      <td style="width:1%;" bgcolor="#DCDCDC"><b>P</b></td>
                                                        <td style="width:1%;">'.round($rowa['ag_fis_1p'],1).'</td>
                                                        <td style="width:1%;">'.round($rowa['ag_fis_2p'],1).'</td>
                                                        <td style="width:1%;">'.round($rowa['ag_fis_3p'],1).'</td>
                                                        <td style="width:1%;">'.round($rowa['ag_fis_4p'],1).'</td>
                                                        <td style="width:1%;">'.round($rowa['ag_fis_5p'],1).'</td>
                                                        <td style="width:1%;">'.round($rowa['ag_fis_6p'],1).'</td>
                                                    </tr>
                                                    <tr>
                                                      <td style="width:1%;" bgcolor="#DCDCDC"><b>E</b></td>
                                                        <td style="width:1%;">'.round($rowa['ag_fis_1e'],1).'</td>
                                                        <td style="width:1%;">'.round($rowa['ag_fis_2e'],1).'</td>
                                                        <td style="width:1%;">'.round($rowa['ag_fis_3e'],1).'</td>
                                                        <td style="width:1%;">'.round($rowa['ag_fis_4e'],1).'</td>
                                                        <td style="width:1%;">'.round($rowa['ag_fis_5e'],1).'</td>
                                                        <td style="width:1%;">'.round($rowa['ag_fis_6e'],1).'</td>
                                                    </tr>
                                                    <tr bgcolor="#DCDCDC">
                                                        <td style="width:1%;">P/E</td>
                                                        <td style="width:1%;"><center>Jul.</center></td>
                                                        <td style="width:1%;"><center>Ago.</center></td>
                                                        <td style="width:1%;"><center>Sep.</center></td>
                                                        <td style="width:1%;"><center>Oct.</center></td>
                                                        <td style="width:1%;"><center>Nov.</center></td>
                                                        <td style="width:1%;"><center>Dic.</center></td>
                                                    </tr>
                                                    <tr>
                                                      <td style="width:1%;" bgcolor="#DCDCDC"><b>P</b></td>
                                                        <td style="width:1%;">'.round($rowa['ag_fis_7p'],1).'</td>
                                                        <td style="width:1%;">'.round($rowa['ag_fis_8p'],1).'</td>
                                                        <td style="width:1%;">'.round($rowa['ag_fis_9p'],1).'</td>
                                                        <td style="width:1%;">'.round($rowa['ag_fis_10p'],1).'</td>
                                                        <td style="width:1%;">'.round($rowa['ag_fis_11p'],1).'</td>
                                                        <td style="width:1%;">'.round($rowa['ag_fis_12p'],1).'</td>
                                                    </tr>
                                                    <tr>
                                                      <td style="width:1%;" bgcolor="#DCDCDC"><b>E</b></td>
                                                        <td style="width:1%;">'.round($rowa['ag_fis_7e'],1).'</td>
                                                        <td style="width:1%;">'.round($rowa['ag_fis_8e'],1).'</td>
                                                        <td style="width:1%;">'.round($rowa['ag_fis_9e'],1).'</td>
                                                        <td style="width:1%;">'.round($rowa['ag_fis_10e'],1).'</td>
                                                        <td style="width:1%;">'.round($rowa['ag_fis_11e'],1).'</td>
                                                        <td style="width:1%;">'.round($rowa['ag_fis_12e'],1).'</td>
                                                    </tr>';
                                                    }
                                                    elseif ($rowa['indi_id']==2) {
                                                        $html2.='
                                                        <tr bgcolor="#DCDCDC">
                                                        <td style="width:1%;">P/E</td>
                                                        <td style="width:1%;"><center>Ene.</center></td>
                                                        <td style="width:1%;"><center>Feb.</center></td>
                                                        <td style="width:1%;"><center>Mar.</center></td>
                                                        <td style="width:1%;"><center>Abr.</center></td>
                                                        <td style="width:1%;"><center>May.</center></td>
                                                        <td style="width:1%;"><center>Jun.</center></td>
                                                        </tr>
                                                        <tr>
                                                          <td style="width:1%;" bgcolor="#DCDCDC"><b>P</b></td>
                                                            <td style="width:1%;">'.round($rowa['ag_fis_1p'],1).'%</td>
                                                            <td style="width:1%;">'.round($rowa['ag_fis_2p'],1).'%</td>
                                                            <td style="width:1%;">'.round($rowa['ag_fis_3p'],1).'%</td>
                                                            <td style="width:1%;">'.round($rowa['ag_fis_4p'],1).'%</td>
                                                            <td style="width:1%;">'.round($rowa['ag_fis_5p'],1).'%</td>
                                                            <td style="width:1%;">'.round($rowa['ag_fis_6p'],1).'%</td>
                                                        </tr>
                                                        <tr>
                                                          <td style="width:1%;" bgcolor="#DCDCDC"><b>E</b></td>
                                                            <td style="width:1%;">'.round($rowa['ag_fis_1e'],1).'%</td>
                                                            <td style="width:1%;">'.round($rowa['ag_fis_2e'],1).'%</td>
                                                            <td style="width:1%;">'.round($rowa['ag_fis_3e'],1).'%</td>
                                                            <td style="width:1%;">'.round($rowa['ag_fis_4e'],1).'%</td>
                                                            <td style="width:1%;">'.round($rowa['ag_fis_5e'],1).'%</td>
                                                            <td style="width:1%;">'.round($rowa['ag_fis_6e'],1).'%</td>
                                                        </tr>
                                                        <tr bgcolor="#DCDCDC">
                                                            <td style="width:1%;">P/E</td>
                                                            <td style="width:1%;"><center>Jul.</center></td>
                                                            <td style="width:1%;"><center>Ago.</center></td>
                                                            <td style="width:1%;"><center>Sep.</center></td>
                                                            <td style="width:1%;"><center>Oct.</center></td>
                                                            <td style="width:1%;"><center>Nov.</center></td>
                                                            <td style="width:1%;"><center>Dic.</center></td>
                                                        </tr>
                                                        <tr>
                                                          <td style="width:1%;" bgcolor="#DCDCDC"><b>P</b></td>
                                                            <td style="width:1%;">'.round($rowa['ag_fis_7p'],1).'%</td>
                                                            <td style="width:1%;">'.round($rowa['ag_fis_8p'],1).'%</td>
                                                            <td style="width:1%;">'.round($rowa['ag_fis_9p'],1).'%</td>
                                                            <td style="width:1%;">'.round($rowa['ag_fis_10p'],1).'%</td>
                                                            <td style="width:1%;">'.round($rowa['ag_fis_11p'],1).'%</td>
                                                            <td style="width:1%;">'.round($rowa['ag_fis_12p'],1).'%</td>
                                                        </tr>
                                                        <tr>
                                                          <td style="width:1%;" bgcolor="#DCDCDC"><b>E</b></td>
                                                            <td style="width:1%;">'.round($rowa['ag_fis_7e'],1).'%</td>
                                                            <td style="width:1%;">'.round($rowa['ag_fis_8e'],1).'%</td>
                                                            <td style="width:1%;">'.round($rowa['ag_fis_9e'],1).'%</td>
                                                            <td style="width:1%;">'.round($rowa['ag_fis_10e'],1).'%</td>
                                                            <td style="width:1%;">'.round($rowa['ag_fis_11e'],1).'%</td>
                                                            <td style="width:1%;">'.round($rowa['ag_fis_12e'],1).'%</td>
                                                        </tr>';
                                                    }
                                                    $html2.='
                                                    </thead>
                                                </table>
                                                 <!--fin de la temporalizacion de la meta-->
                                             </td>
                                           </tr>
                            
                          </table>
                      </td>
                    </tr> ';
                              }//////end foreach actividades//////
////============================================================insumos============
                         $html2.='     
                    <tr bgcolor="#696969">
                        <td style="width:100%;">
                           <font size=1 color="#ffffff"><b> 11. INSUMOS </b></font>
                        </td>
                    </tr>
                    <tr>
                      <td>
                                      <table border="1" style="width:100%;border-collapse:collapse;">
                                        <tr bgcolor="#556B2F">
                                          <td style="width:10%; color:#fff;">C�digo</td>
                                          <td style="width:15%; color:#fff;">Tipo de Insumo</td>
                                          <td style="width:10%; color:#fff;">Fecha de Requerimiento</td>
                                          <td style="width:40%; color:#fff;">Detalle/Caracterist&iacute;ca</td>
                                          <td style="width:5%; color:#fff;">CR</td>
                                          <td style="width:10%; color:#fff;">Costo Unitario</td>
                                          <td style="width:10%; color:#fff;">Costo Total</td>
                                        </tr>
                                      </table>
                      </td>
                    </tr>
                    ';
                                      $tipo_insumos[1] = "Recurso Humano Permanente";
                                      $tipo_insumos[2] = "Servicio";
                                      $tipo_insumos[3] = "Pasaje";
                                      $tipo_insumos[4] = "Vi&aacute;ticos";
                                      $tipo_insumos[5] = "Consultor&iacute;a por Producto";
                                      $tipo_insumos[6] = "CConsultor&iacute;a en L&iacute;nea";
                                      $tipo_insumos[7] = "Materiales y suministros";
                                      $tipo_insumos[8] = "Activos Fijos";
                                      $tipo_insumos[9] = "Otros Insumos";
                                      $a = 0;

                                  $activa= $this->model_actividad->actividad_id_gestion($row['prod_id'],$this->session->userdata("gestion"));
                                     foreach ($activa as $rowa)
                                      { ///insumos//

                                    $lista_insumos = $this->model_insumos->lista_insumos($rowa['act_id']); 
                                    foreach ($lista_insumos as $rowin)
                                          { $date = date_create($rowin['ins_fecha_requerimiento']);
                                      $html2.='

                    <tr>
                      <td>
                                              <table border="1" style="width:100%;border-collapse:collapse;">
                                                <tr>
                                                  <td style="width:10%;">'.$rowin['ins_codigo'].'</td>
                                                  <td style="width:15%;">'.$tipo_insumos[trim($rowin['ins_tipo'])].'</td>
                                                  <td style="width:10%;">'.date_format($date, 'd-m-Y') .'</td>
                                                  <td style="width:40%">'.$rowin['ins_detalle'].'</td>
                                                  <td style="width:5%;">'.$rowin['ins_cant_existente'].'</td>
                                                  <td style="width:10%;">'.$rowin['ins_costo_unitario'].'</td>
                                                  <td style="width:10%;">'.$rowin['ins_costo_total'].'</td>
                                                </tr>
                                             </table> 
                      </td>
                    </tr>';
                                              
                                          }/////end foreach insumos///

                                      }/////fin de insumos//////

////=============================================================end insumos============= 

                    ///////==================end clasificacion  por objeto de gasto=================///////// 
                                $activ++;                             
                            }////end actividades///
                              $pro++; 
                          }////end foreach producto///
                        }////end producto///
                      } ////end foreach componente//////
                    }///end componente
                
   ////////================Clasificacion por objeto de gasto======================//////////
                   $html2.='
                    <tr bgcolor="#696969">
                        <td style="width:100%;">
                           <font size=1 color="#ffffff"><b> 11. CLASIFICACI&Oacute;N POR OBJETO DE GASTO </b></font>
                        </td>
                    </tr>
                    <tr>
                      <td>
                          <table border="1" style="width:100%;border-collapse:collapse;">
                            <tr bgcolor="#5C6C86">
                              <td style="width:10%; color:#fff;"> Partida</td>
                              <td style="width:20%; color:#fff;"> Descripci&oacute;n</td> 
                              <td style="width:15%; color:#fff;"> Fuente Financiamiento</td> 
                              <td style="width:15%; color:#fff;"> Organismo Financiador </td>
                              <td style="width:15%; color:#fff;"> Entidad de Transferencia</td>
                              <td style="width:10%; color:#fff;"> Importe</td>   
                            </tr>
                          </table>
                      </td>
                    </tr>';
          $comps= $this->model_componente->componentes_id($fase[0]['id']); 
                        foreach ($comps as $row)
                      {////////comp////
            $prods= $this->model_producto->producto_id_gestion($row['com_id'],$this->session->userdata("gestion"));
                          foreach ($prods as $rowp) 
                          {//////produ/////                                                
                 
                  $clasgasto=$this->model_insumos->clasficcacion_gasto($rowp['prod_id']);
                    foreach ($clasgasto as $rowg) 
                                  { ///foreach gasto                                                
                        $html2.='<tr>
                                  <td>
                                      <table border="1" style="width:100%;border-collapse:collapse;">
                                        <tr>
                                          <td style="width:10%;"> '.$rowg['par_codigo'].'</td>
                                          <td style="width:20%;"> '.$rowg['par_nombre'].'</td> 
                                          <td style="width:15%;"> '.$rowg['ff_codigo'].'</td> 
                                          <td style="width:15%;"> '.$rowg['of_codigo'].'</td>
                                          <td style="width:15%;"> '.$rowg['et_codigo'].'</td>
                                          <td style="width:10%;"> '.$rowg['total_monto'].'</td>   
                                        </tr>
                                      </table>
                                  </td>
                                </tr>
                    ';         
                              }////end gastos/////
                             }/////foreach prod///
                            }////////comp///////
////////================Clasificacion por objeto de gasto======================//////////
 } $num=1;
$html2.='
                  <tr bgcolor="#696969">
                        <td style="width:100%;">
                           <font size=1 color="#ffffff"><b> 12. METAS </b></font>
                        </td>
                  </tr>
                  <tr>
                   <td>
                    <table>
                     <tr>
                      <td style="width:20%;">Nro</td>
                      <td style="width:20%;">INDICADOR</td>
                      <td style="width:20%;">META</td>
                      <td style="width:20%;">EJECUCI�N</td>
                      <td style="width:20%;">EFICACIA</td>
                     </tr>
                    </table>
                    </td>
                  </tr>  
                  <tr>
                    <td>
                      <table>
                      ';
                        $metas=$this->model_proyecto->metas_p($id_p);
                        foreach ($metas as $rowm) 
                        {
                $html2.='
                        <tr>
                          <td style="width:20%;"><font size="1"> META : '.$num.'</font></td>'; $num++; $html2.='
                          <td style="width:20%;"><font size="1">'.$rowm['meta_descripcion'].'</font></td>
                          <td style="width:20%;"><font size="1">'.$rowm['meta_meta'].'</font></td>
                          <td style="width:20%;"><font size="1">'.$rowm['meta_ejec'].'</font></td>
                          <td style="width:20%;"><font size="1">'.$rowm['meta_efic'].'</font></td>
                        </tr>
                        ';
                        }
               $html2.='
                      </table>
                    <td>
                  </tr>
                <tr>
                  <td>
                    <table border="1" style="width:100%;border-collapse:collapse;">
                      <tr>
                        <td style="height:30px;"></td>
                        <td style="height:30px;"></td>
                        <td style="height:30px;"></td>
                        <td style="height:30px;"></td>
                       </tr>
                       <tr>
                        <td>T&eacute;cnico Operativo de Planificaci&oacute;n</td>
                        <td> Jefe Inmediato</td>
                        <td>Validador POA</td>
                        <td> Validador Financiero</td>
                       </tr>
                    </table>        
                  </td>
                 </tr>
                   
                </thead>                      
            </table>
            </p>
          </div>
        </body>
        </html>';



        $dompdf = new DOMPDF();
        $dompdf->load_html($html2);
        $dompdf->set_paper('letter','');
        $dompdf->render();
        $dompdf->stream("reporte_matriz_resultado_final.pdf", array("Attachment" => false));
     
    }

       /*---------------------------------- PDF HISTORIAL DE USUARIOS -------------------------------------*/   
       public function historial_usuarios($id_p){
        $data= $this->model_proyecto->get_id_proyecto($id_p);
        $history = $this->model_proyecto->historial_usuario($id_p);
        $resp = $this->model_proyecto->historial_usuario($id_p,1);
        // Creaci�n del objeto de la clase heredada
        $html2='';

        $html2 .= '
        <html>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <head>
          <style>
            body{
              font-family: sans-serif;
            }
            @page {
              margin: 100px 50px;
            }
            header { position: fixed;
              left: 0px;
              top: -80px;
              right: 0px;
              height: 90px;
              background-color: #ddd;
            }
            header h1{
              margin: 5px 0;
            }
            header h2{
              margin: 0 0 10px 0;
            }
            footer {
              position: fixed;
              left: 0px;
              bottom: -70px;
              right: 0px;
              height: 40px;
              border-bottom: 2px solid #ddd;
            }

            footer .page:after {
              content: counter(page);
            }
            footer table {
              width: 100%;
            }
            footer p {
              text-align: right;
            }
            footer .izq {
              text-align: left;
            }
            table{font-size: 9px;
            width: 100%;

            }
          </style>
          
        <body>
          <header>

          <table style="width:100%;"> 
          <tr>
          <td style="width:85%;">
            <p class="izq">
            <font size=1>
            <b>ENTIDAD : </b> '.$this->session->userdata('entidad').'<br>
            <b>POA - PLAN OPERATIVO ANUAL : </b> 2016 - 2020<br>
            <b>'.$this->session->userdata('sistema').' </b><br>
            <b>FORMULARIO : </b> HISTORIAL DE RESPONSABLES
            </font>
            </p>
          </td>
          <td style="width:15%;">
            imagen
          </td>
          </tr>
          </table>

          </header>
          <footer>
            <table class="table table-bordered">
              <tr>
                <td>
                    <p class="izq">
                      '.$this->session->userdata("sistema_pie").'
                    </p>
                </td>
                <td>
                  <p class="page">
                    P&aacute;gina
                  </p>
                </td>
              </tr>
            </table>
          </footer>
          <div id="content">
            <br>
            <p>
            <table style="width:100%;" cellspacing="0" cellpadding="0" border="1" style="width:100%;border-collapse:collapse;">
                <thead>                         
                    <tr>
                        <td rowspan="2" style="width:20%;">
                            <center><font size=5><b>0</b></font></center>
                            <center><font size=1>Numero BD</font></center>
                        </td>
                        <td colspan="2" style="width:80%;">
                            
                            <center><font size=2><b>'.utf8_decode($data[0]['proy_nombre']).'</b></font></center>
                            <center><font size=1>Proyecto/Actividad</font></center>
                            
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <center><font size=1><b>NOMBRE DEL PROYECTO</b></font></center>
                            <center><font size=1>Localizacion</font></center>
                        </td>
                        <td>
                            <center><font size=1><b>NOMBRE DEL PROYECTO</b></font></center>
                            <center><font size=1>Distrito</font></center>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <center><font size=2><b>N/A</b></font></center>
                            <center><font size=1>SISIN</font></center>
                        </td>
                        <td colspan="2" rowspan="2">
                            <center><b></b></center>
                            <center><font size=1>Unidad Ejecutora</font></center>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <center><font size=2><b>'.$data[0]['aper_programa'].''.$data[0]['aper_proyecto'].''.$data[0]['aper_actividad'].'</b></font></center>
                            <center><font size=1>Apertura Programatica</font></center>
                        </td>
                    </tr>
                </thead>                      
            </table><br>

            <table  border="1" style="width:100%;border-collapse:collapse;">
                <thead>                         
                    <tr bgcolor="#2F4F4F">
                        <td style="width:100%;">
                            <font size=1 color="#ffffff"><b>HISTORIAL DE USUARIOS-RESPONSABLES</b></font>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:100%;">
                            <table  border="1" style="width:100%;border-collapse:collapse;">
                                <thead>                         
                                    <tr>
                                        <td style="width:1%;">Nro.</td>
                                        <td style="width:5%;">NOMBRES</td>
                                        <td style="width:5%;">APELLIDOS</td>
                                        <td style="width:5%;">USUARIO</td>
                                        <td style="width:5%;">FECHA DE REGISTRO</td>
                                        <td style="width:5%;">TELEFONO / CEL</td>
                                        <td style="width:5%;">DOMICILIO</td>
                                    </tr>'; 
                                    $num=1;
                       foreach ($history as $row)
                        {
                          $html2.='<tr>
                                        <td style="width:1%;">'.$num.'</td>
                                        <td style="width:5%;">'.$row['fun_nombre'].'</td>
                                        <td style="width:5%;">'.$row['fun_paterno'].''.$row['fun_materno'].'</td>
                                        <td style="width:5%;">'.$row['fun_usuario'].'</td>
                                        <td style="width:5%;">'.$row['pfun_fecha'].'</td>
                                        <td style="width:5%;">'.$row['fun_telefono'].'</td>
                                        <td style="width:5%;">'.$row['fun_domicilio'].'</td>
                                  </tr>';
                  $num++;} 
                  
                  $html2.='</thead>                      
                            </table>
                        </td>
                    </tr>
                   
                </thead>                      
            </table>
            </p>
          </div>
        </body>
        </html>';



        $dompdf = new DOMPDF();
        $dompdf->load_html($html2);
        $dompdf->set_paper('letter','');
        $dompdf->render();
        $dompdf->stream("reporte_matriz_resultado_final.pdf", array("Attachment" => false));
    }
}