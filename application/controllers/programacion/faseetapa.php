<?php

class Faseetapa extends CI_Controller {
  public $rol = array('1' => '3','2' => '4','3' => '5');
  public function __construct ()
    {
        parent::__construct();
        if($this->session->userdata('fun_id')!=null){
        $this->load->library('pdf2');
        $this->load->model('programacion/model_proyecto');
        $this->load->model('programacion/model_faseetapa');
        $this->load->model('programacion/model_componente');
        $this->load->model('menu_modelo');
        $this->load->model('Users_model','',true);
        }else{
            redirect('/','refresh');
        }

    }
 /*================================================ LISTA DE FASES ETAPAS PROGRAMACION ==================================================*/
    function list_fase_etapa($id_p)
    {
      if($this->rolfun($this->rol) & $id_p!=''){
          /*$enlaces=$this->menu_modelo->get_Modulos(1);
          $data['enlaces'] = $enlaces;
          for($i=0;$i<count($enlaces);$i++){
            $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
          }
          $data['subenlaces'] = $subenlaces;*/

          $enlaces=$this->menu_modelo->get_Modulos_programacion(1);
          $data['enlaces'] = $enlaces;
          $data['nro_fase']= $this->model_faseetapa->nro_fase($id_p); /// nro de fases y etapas registrados
          $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p); ////// DATOS DEL PROYECTO
          $data['id_f'] = $this->model_faseetapa->get_id_fase($id_p); //// DATOS DE LA FASE ACTIVA
          $data['mod']=4;

          $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p);
          $titulo_proy=strtoupper($data['proyecto'][0]['tipo']);
          $data['titulo_proy'] = $titulo_proy;
          $data['fases'] = $this->model_faseetapa->fase_etapa_proy($id_p);
          if($data['proyecto'][0]['tp_id']==4){$titulo='Resultados';}else{$titulo='Metas';}
          $data['titulo'] = $titulo;
          //load the view
          $this->load->view('admin/programacion/proy_anual/fase/list_fase_etapa', $data);
      }
      else{
        redirect('admin/dashboard');
      }
    }
  /*=========================================================================================================================*/

   /*================================================ LISTA DE FASES ETAPAS SGP ==================================================*/
    function list_fase_etapa_sgp($id_p)
    {
      if($this->rolfun($this->rol) & $id_p!=''){
        $enlaces=$this->menu_modelo->get_Modulos(4);
        $data['enlaces'] = $enlaces;
        for($i=0;$i<count($enlaces);$i++)
        {
          $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;

        $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p);
        $data['fases'] = $this->model_faseetapa->fase_etapa_proy($id_p);
        //load the view
        $this->load->view('admin/programacion/proy_multi/fase/list_fase_etapa', $data);
      }
      else{
        redirect('admin/dashboard');
      }
    }
  /*=========================================================================================================================*/

    /*============================ BORRA DATOS F/E=================================*/
    public function delete_faseetapa($id_p){

        $this->db->where('proy_id', $id_p);
        $this->db->delete('_proyectofaseetapacomponente');
    }

  /*================================================ AGREGAR NUEVA FASE ETAPA ==================================================*/
  function nueva_fase($id_p,$id_f,$form)
  {
    if($this->rolfun($this->rol) & $id_p!='' & $id_f!='' & $form!=''){
        /*$enlaces=$this->menu_modelo->get_Modulos(1);
        $data['enlaces'] = $enlaces;
        for($i=0;$i<count($enlaces);$i++){
          $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;*/

        $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p);
        if($data['proyecto'][0]['tp_id']==4){$titulo='Resultados';}else{$titulo='Metas';}
        $data['titulo'] = $titulo;

        $enlaces=$this->menu_modelo->get_Modulos_programacion(1);
        $data['enlaces'] = $enlaces;
        $data['nro_fase']= $this->model_faseetapa->nro_fase($id_p); /// nro de fases y etapas registrados
        $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p); ////// DATOS DEL PROYECTO
        $data['id_f'] = $this->model_faseetapa->get_id_fase($id_p); //// DATOS DE LA FASE ACTIVA
        $data['mod']=1;

      if($form==1) ///// PRIMER FORMULARIO DE REGISTRO F/E
      {
          $data['unidad_org'] = $this->model_proyecto->list_unidad_org(); //// unidad organizacional
          $data['f_top'] = $this->model_proyecto->responsable_proy($id_p,'1'); //// unidad ejecutora
          $data['main_content'] = 'admin/programacion/proy_anual/fase/form_fase_add';
          $this->load->view('admin/programacion/proy_anual/fase/form_fase_add', $data);
      }
      elseif($form==2) ///// SEGUNDO FORMULARIO DE REGISTRO F/E
      {

          $datos = $this->model_faseetapa->datos_fase_etapa($id_f,$id_p);  // desvuelve las fechas del proyecto
          $data['a�os']=$datos[0]['final']-$datos[0]['actual']+1;
          $data['fecha']=$datos[0]['actual'];
          $data['fase_proyecto'] = $this->model_faseetapa->fase_etapa($id_f,$id_p);
          $data['nro_fg'] = $this->model_faseetapa->nro_fasegestion($id_f);
          $a�os=$datos[0]['final']-$datos[0]['actual']+1;
          $data['gestiones']=$a�os;
          $fecha=$datos[0]['actual'];
          $data['fecha']=$fecha;

            if($a�os>=1)
            {
              $data['fase_gestion1']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha);
              if($a�os>=2)
              {
                $data['fase_gestion2']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+1);
                if($a�os>=3)
                {
                  $data['fase_gestion3']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+2);
                  if($a�os>=4)
                  {
                    $data['fase_gestion4']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+3);
                    if($a�os>=5)
                    {
                      $data['fase_gestion5']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+4);
                      if($a�os>=6)
                      {
                        $data['fase_gestion6']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+5);
                        if($a�os>=7)
                        {
                          $data['fase_gestion7']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+6);
                          if($a�os>=8)
                          {
                            $data['fase_gestion8']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+7);
                            if($a�os>=9)
                            {
                              $data['fase_gestion9']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+8);
                              if($a�os>=10)
                              {
                                $data['fase_gestion10']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+9);
                                if($a�os>=11)
                                {
                                  $data['fase_gestion11']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+10);
                                  if($a�os>=12)
                                  {
                                    $data['fase_gestion12']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+11);
                                    if($a�os>=13)
                                    {
                                      $data['fase_gestion13']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+12);
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }

           $this->load->view('admin/programacion/proy_anual/fase/form_fase_update2', $data);
      }
    }
    else{
        redirect('admin/dashboard');
    }
  }
/*=========================================================================================================================*/

 /*================================================ AGREGAR NUEVA FASE ETAPA SGP ==================================================*/
  function nueva_fase_sgp($id_p,$id_f,$form)
  {
    if($this->rolfun($this->rol) & $id_p!='' & $id_f!='' & $form!=''){
      $enlaces=$this->menu_modelo->get_Modulos(4);
      $data['enlaces'] = $enlaces;
      for($i=0;$i<count($enlaces);$i++)
      {
        $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
      }
      $data['subenlaces'] = $subenlaces;
      $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p);

      if($form==1) ///// PRIMER FORMULARIO DE REGISTRO F/E
      {
          $data['unidad_org'] = $this->model_proyecto->list_unidad_org(); //// unidad organizacional
          $data['f_top'] = $this->model_proyecto->responsable_proy($id_p,'1'); //// unidad ejecutora
          $data['main_content'] = 'admin/programacion/proy_multi/fase/form_fase_add';
          $this->load->view('admin/programacion/proy_multi/fase/form_fase_add', $data);
      }
      elseif($form==2) ///// SEGUNDO FORMULARIO DE REGISTRO F/E
      {

          $datos = $this->model_faseetapa->datos_fase_etapa($id_f,$id_p);  // desvuelve las fechas del proyecto
          $data['a�os']=$datos[0]['final']-$datos[0]['actual']+1;
          $data['fecha']=$datos[0]['actual'];
          $data['fase_proyecto'] = $this->model_faseetapa->fase_etapa($id_f,$id_p);
          $data['nro_fg'] = $this->model_faseetapa->nro_fasegestion($id_f);
          $a�os=$datos[0]['final']-$datos[0]['actual']+1;
          $data['gestiones']=$a�os;
          $fecha=$datos[0]['actual'];
          $data['fecha']=$fecha;

            if($a�os>=1)
            {
              $data['fase_gestion1']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha);
              if($a�os>=2)
              {
                $data['fase_gestion2']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+1);
                if($a�os>=3)
                {
                  $data['fase_gestion3']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+2);
                  if($a�os>=4)
                  {
                    $data['fase_gestion4']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+3);
                    if($a�os>=5)
                    {
                      $data['fase_gestion5']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+4);
                      if($a�os>=6)
                      {
                        $data['fase_gestion6']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+5);
                        if($a�os>=7)
                        {
                          $data['fase_gestion7']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+6);
                          if($a�os>=8)
                          {
                            $data['fase_gestion8']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+7);
                            if($a�os>=9)
                            {
                              $data['fase_gestion9']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+8);
                              if($a�os>=10)
                              {
                                $data['fase_gestion10']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+9);
                                if($a�os>=11)
                                {
                                  $data['fase_gestion11']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+10);
                                  if($a�os>=12)
                                  {
                                    $data['fase_gestion12']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+11);
                                    if($a�os>=13)
                                    {
                                      $data['fase_gestion13']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+12);
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }

            $this->load->view('admin/programacion/proy_multi/fase/form_fase_update2', $data);
      }
    }
    else{
        redirect('admin/dashboard');
    }
  }
/*=========================================================================================================================*/

/*=========================================== ENCENDER FASE ETAPA ======================================================*/
   public function encender_fase(){
        if($this->input->is_ajax_request())
        {
            $post = $this->input->post();
            $id_f = $post['id_f'];
            $id_proy = $post['id_p'];

            $pfec=$this->model_faseetapa->verif_off($id_f,$id_proy);

             if($pfec[0]['pfec_estado'] == 1)
             {
                echo "true"; /////fase encendido
             }
             else{
                $this->model_faseetapa->encender_fase_etapa($id_f,$id_proy);
                echo "false"; ///// fase apagado, actulizado
             }

        }else{
                  show_404();
              }
    }
/*=========================================================================================================================*/
/*========================================= VALIDAR NUEVA FASE ETAPA 1 ================================================================================*/
  function add_fase()
  {
         if($this->input->server('REQUEST_METHOD') === 'POST')
          {
            $this->form_validation->set_rules('id', 'Id del proyecto ', 'required|trim');
            $this->form_validation->set_rules('fase', 'Fase', 'required|trim');
            $this->form_validation->set_rules('etapas', 'Etapas', 'required|trim');
            $this->form_validation->set_rules('f_inicio', 'Fecha Inicio', 'required|trim');
            $this->form_validation->set_rules('f_final', 'Fecha Conclusion', 'required|trim');
            $this->form_validation->set_rules('f_ejec', 'forma de ejecucion', 'required|trim');

            if ($this->form_validation->run())
            {
                  $query=$this->db->query('set datestyle to DMY');
                  $data_to_store = array( ///// Tabla _proyectofaseetapacomponente
                    'proy_id' => $this->input->post('id'),
                    'fas_id' => $this->input->post('fase'),
                    'eta_id' => $this->input->post('etapas'),
                    'pfec_descripcion' => strtoupper($this->input->post('desc')),
                    'pfec_fecha_inicio_ddmmaaa' => $this->input->post('f_inicio'),
                    'pfec_fecha_fin_ddmmaaa' => $this->input->post('f_final'),
                    'pfec_fecha_registro' => date('d/m/Y h:i:s'),
                    'pfec_ejecucion' => $this->input->post('f_ejec'),
                    'pfec_ptto_fase' => $this->input->post('monto_total'),
                    'aper_id' => $this->input->post('aper'),
                    'fun_id' => $this->session->userdata("fun_id"),
                    'unidad_ejec' => $this->input->post('uni_ejec'),
                );
                $this->db->insert('_proyectofaseetapacomponente', $data_to_store); ///// inserta a _proyectofaseetapacomponente 
                $id_pfec = $this->db->insert_id();

                $a�os=$this->input->post('g2')-$this->input->post('g1');
                $fecha=$this->input->post('g1');
                $nro_fase_act=$this->model_faseetapa->nro_fase($this->input->post('id'));
                if($nro_fase_act==1){$activo=1;}else{$activo=0;}

                $update_fase = array(  'pfec_fecha_inicio' => $this->input->post('g1'),
                                       'pfec_fecha_fin' => $this->input->post('g2'),
                                       'pfec_estado' => $activo);

                        $this->db->where('proy_id', $this->input->post('id'));
                        $this->db->where('pfec_id', $id_pfec);
                        $this->db->update('_proyectofaseetapacomponente', $update_fase);

                /*=======================  fase etapa gestion ======================*/
                      for($i=0;$i<=$a�os;$i++)
                      {
                         $data_to_store2 = array( ///// Tabla ptto_fase_gestion
                            'pfec_id' => $id_pfec,
                            'g_id' => $fecha,
                            'fun_id' => $this->session->userdata("fun_id"),
                          );
                          $this->db->insert('ptto_fase_gestion', $data_to_store2); ///// inserta a ptto_fase_gestion
                        $fecha=$fecha+1;
                      }
                /*========================== end fase etapa gestion ======================*/

                if($this->input->post('tp')==0) /////// formulario de registro TOP
                {
                  redirect('admin/proy/newfase/'.$this->input->post('id').'/'.$id_pfec.'/2');
                }
                elseif ($this->input->post('tp')==1) /////// Datos Generales
                {
                  redirect('admin/comp/newfase/'.$this->input->post('mod').'/'.$this->input->post('id').'/'.$id_pfec.'/2');
                }

            }
            else
            {   if($this->input->post('tp')==0) /////// formulario de registro TOP
                {
                  redirect('admin/proy/newfase/'.$this->input->post('id').'/0/1');
                }
                elseif ($this->input->post('tp')==1) /////// Datos Generales
                {
                  redirect('admin/comp/newfase/'.$this->input->post('mod').'/'.$this->input->post('id').'/0/1');
                }

            }
        }
  }
/*=========================================================================================================================*/

/*========================================= VALIDAR NUEVA FASE ETAPA 1 ================================================================================*/
  function add_fase_sgp()
  {
         if($this->input->server('REQUEST_METHOD') === 'POST')
          {
            $this->form_validation->set_rules('id', 'Id del proyecto ', 'required|trim');
            $this->form_validation->set_rules('fase', 'Fase', 'required|trim');
            $this->form_validation->set_rules('etapas', 'Etapas', 'required|trim');
            $this->form_validation->set_rules('f_inicio', 'Fecha Inicio', 'required|trim');
            $this->form_validation->set_rules('f_final', 'Fecha Conclusion', 'required|trim');
            $this->form_validation->set_rules('f_ejec', 'forma de ejecucion', 'required|trim');

            if ($this->form_validation->run())
            {
                  $query=$this->db->query('set datestyle to DMY');
                  $data_to_store = array( ///// Tabla _proyectofaseetapacomponente
                    'proy_id' => $this->input->post('id'),
                    'fas_id' => $this->input->post('fase'),
                    'eta_id' => $this->input->post('etapas'),
                    'pfec_descripcion' => strtoupper($this->input->post('desc')),
                    'pfec_fecha_inicio_ddmmaaa' => $this->input->post('f_inicio'),
                    'pfec_fecha_fin_ddmmaaa' => $this->input->post('f_final'),
                    'pfec_fecha_registro' => date('d/m/Y h:i:s'),
                    'pfec_ejecucion' => $this->input->post('f_ejec'),
                    'pfec_ptto_fase' => $this->input->post('monto_total'),
                    'aper_id' => $this->input->post('aper'),
                    'fun_id' => $this->session->userdata("fun_id"),
                    'unidad_ejec' => $this->input->post('uni_ejec'),
                );
                $this->db->insert('_proyectofaseetapacomponente', $data_to_store); ///// inserta a _proyectofaseetapacomponente 
                $id_pfec = $this->db->insert_id();

                $datos = $this->model_faseetapa->datos_fase_etapa($id_pfec,$this->input->post('id'));  // desvuelve las fechas del proyecto

                $a�os=$datos[0]['final']-$datos[0]['actual'];
                $fecha=$datos[0]['actual'];

                $nro_fase_act=$this->model_faseetapa->nro_fase($this->input->post('id'));
                if($nro_fase_act==1){$activo=1;}else{$activo=0;}

                $update_fase = array(  'pfec_fecha_inicio' => $datos[0]['actual'],
                                       'pfec_fecha_fin' => $datos[0]['final'],
                                       'pfec_estado' => $activo);

                        $this->db->where('proy_id', $this->input->post('id'));
                        $this->db->where('pfec_id', $id_pfec);
                        $this->db->update('_proyectofaseetapacomponente', $update_fase);

                /*=======================  fase etapa gestion ======================*/
                      for($i=0;$i<=$a�os;$i++)
                      {
                         $data_to_store2 = array( ///// Tabla ptto_fase_gestion
                            'pfec_id' => $id_pfec,
                            'g_id' => $fecha,
                            'fun_id' => $this->session->userdata("fun_id"),
                          );
                          $this->db->insert('ptto_fase_gestion', $data_to_store2); ///// inserta a ptto_fase_gestion
                        $fecha=$fecha+1;
                      }
                /*========================== end fase etapa gestion ======================*/

                if($this->input->post('tp')==0) /////// formulario de registro TOP
                {
                  redirect('admin/sgp/newfase/'.$this->input->post('id').'/'.$id_pfec.'/2');
                }
                elseif ($this->input->post('tp')==1) /////// Datos Generales SGP
                {
                  redirect('admin/comp/newfase/'.$this->input->post('mod').'/'.$this->input->post('id').'/'.$id_pfec.'/2');
                }
            }
            else
            {   if($this->input->post('tp')==0) /////// formulario de registro TOP
                {
                  redirect('admin/sgp/newfase/'.$this->input->post('id').'/0/1');
                }
                elseif ($this->input->post('tp')==1) /////// Datos Generales
                {
                  redirect('admin/comp/newfase/'.$this->input->post('mod').'/'.$this->input->post('id').'/0/1');
                }

            }
        }
  }
/*=========================================================================================================================*/
/*======================================== VALIDAR NUEVA FASE ETAPA 2=================================================================================*/
  function add_fase2()
  {
         if($this->input->server('REQUEST_METHOD') === 'POST')
          {
            $this->form_validation->set_rules('id_f', 'Id de la fase', 'required|trim');
            $this->form_validation->set_rules('id_p', 'Id del Proyecto', 'required|trim');

            $update_fasee = array('pfec_ptto_fase_e' => $this->input->post('pe'));

                        $this->db->where('pfec_id', $this->input->post('id_f'));
                        $this->db->where('proy_id', $this->input->post('id_p'));
                        $this->db->update('_proyectofaseetapacomponente', $update_fasee);

            if ($this->form_validation->run())
            {
                  if($this->input->post('gest')>=1)
                  {
                    $update_fase = array('pfecg_ppto_total'     => str_replace(',','.',str_replace('.','',$this->input->post('pt1'))),
                                         'pfecg_ppto_ejecutado' => str_replace(',','.',str_replace('.','',$this->input->post('pte1'))),
                                         'estado' => $this->input->post('estado'),
                                         'fun_id' => $this->session->userdata("fun_id"));

                        $this->db->where('g_id', $this->input->post('gestion'));
                        $this->db->where('pfec_id', $this->input->post('id_f'));
                        $this->db->update('ptto_fase_gestion', $update_fase);

                      if($this->input->post('gest')>=2)
                      {
                        $update_fase = array('pfecg_ppto_total'     => str_replace(',','.',str_replace('.','',$this->input->post('pt2'))),
                                             'pfecg_ppto_ejecutado' => str_replace(',','.',str_replace('.','',$this->input->post('pte2'))),
                                             'estado' => $this->input->post('estado'),
                                             'fun_id' => $this->session->userdata("fun_id"));

                            $this->db->where('g_id', $this->input->post('gestion')+1);
                            $this->db->where('pfec_id', $this->input->post('id_f'));
                            $this->db->update('ptto_fase_gestion', $update_fase);

                          if($this->input->post('gest')>=3)
                          {
                            $update_fase = array('pfecg_ppto_total'     => str_replace(',','.',str_replace('.','',$this->input->post('pt3'))),
                                                 'pfecg_ppto_ejecutado' => str_replace(',','.',str_replace('.','',$this->input->post('pte3'))),
                                                 'estado' => $this->input->post('estado'),
                                                 'fun_id' => $this->session->userdata("fun_id"));

                                $this->db->where('g_id', $this->input->post('gestion')+2);
                                $this->db->where('pfec_id', $this->input->post('id_f'));
                                $this->db->update('ptto_fase_gestion', $update_fase);


                              if($this->input->post('gest')>=4)
                              {
                                $update_fase = array('pfecg_ppto_total'     => str_replace(',','.',str_replace('.','',$this->input->post('pt4'))),
                                                     'pfecg_ppto_ejecutado' => str_replace(',','.',str_replace('.','',$this->input->post('pte4'))),
                                                     'estado' => $this->input->post('estado'),
                                                     'fun_id' => $this->session->userdata("fun_id"));

                                    $this->db->where('g_id', $this->input->post('gestion')+3);
                                    $this->db->where('pfec_id', $this->input->post('id_f'));
                                    $this->db->update('ptto_fase_gestion', $update_fase);

                                  if($this->input->post('gest')>=5)
                                  {
                                    $update_fase = array('pfecg_ppto_total'     => str_replace(',','.',str_replace('.','',$this->input->post('pt5'))),
                                                         'pfecg_ppto_ejecutado' => str_replace(',','.',str_replace('.','',$this->input->post('pte5'))),
                                                         'estado' => $this->input->post('estado'),
                                                         'fun_id' => $this->session->userdata("fun_id"));

                                        $this->db->where('g_id', $this->input->post('gestion')+4);
                                        $this->db->where('pfec_id', $this->input->post('id_f'));
                                        $this->db->update('ptto_fase_gestion', $update_fase);

                                      if($this->input->post('gest')>=6)
                                      {
                                        $update_fase = array('pfecg_ppto_total'     => str_replace(',','.',str_replace('.','',$this->input->post('pt6'))),
                                                             'pfecg_ppto_ejecutado' => str_replace(',','.',str_replace('.','',$this->input->post('pte6'))),
                                                             'estado' => $this->input->post('estado'),
                                                             'fun_id' => $this->session->userdata("fun_id"));

                                            $this->db->where('g_id', $this->input->post('gestion')+5);
                                            $this->db->where('pfec_id', $this->input->post('id_f'));
                                            $this->db->update('ptto_fase_gestion', $update_fase);

                                            if($this->input->post('gest')>=7)
                                            {
                                              $update_fase = array('pfecg_ppto_total'     => str_replace(',','.',str_replace('.','',$this->input->post('pt7'))),
                                                                   'pfecg_ppto_ejecutado' => str_replace(',','.',str_replace('.','',$this->input->post('pte7'))),
                                                                   'estado' => $this->input->post('estado'),
                                                                   'fun_id' => $this->session->userdata("fun_id"));

                                                  $this->db->where('g_id', $this->input->post('gestion')+6);
                                                  $this->db->where('pfec_id', $this->input->post('id_f'));
                                                  $this->db->update('ptto_fase_gestion', $update_fase);

                                                  if($this->input->post('gest')>=8)
                                                  {
                                                    $update_fase = array('pfecg_ppto_total'     => str_replace(',','.',str_replace('.','',$this->input->post('pt8'))),
                                                                         'pfecg_ppto_ejecutado' => str_replace(',','.',str_replace('.','',$this->input->post('pte8'))),
                                                                         'estado' => $this->input->post('estado'),
                                                                         'fun_id' => $this->session->userdata("fun_id"));

                                                        $this->db->where('g_id', $this->input->post('gestion')+7);
                                                        $this->db->where('pfec_id', $this->input->post('id_f'));
                                                        $this->db->update('ptto_fase_gestion', $update_fase);

                                                        if($this->input->post('gest')>=9)
                                                        {
                                                          $update_fase = array('pfecg_ppto_total'     => str_replace(',','.',str_replace('.','',$this->input->post('pt9'))),
                                                                               'pfecg_ppto_ejecutado' => str_replace(',','.',str_replace('.','',$this->input->post('pte9'))),
                                                                               'estado' => $this->input->post('estado'),
                                                                               'fun_id' => $this->session->userdata("fun_id"));

                                                              $this->db->where('g_id', $this->input->post('gestion')+8);
                                                              $this->db->where('pfec_id', $this->input->post('id_f'));
                                                              $this->db->update('ptto_fase_gestion', $update_fase);

                                                              if($this->input->post('gest')>=10)
                                                              {
                                                                $update_fase = array('pfecg_ppto_total'     => str_replace(',','.',str_replace('.','',$this->input->post('pt10'))),
                                                                                     'pfecg_ppto_ejecutado' => str_replace(',','.',str_replace('.','',$this->input->post('pte10'))),
                                                                                     'estado' => $this->input->post('estado'),
                                                                                     'fun_id' => $this->session->userdata("fun_id"));

                                                                    $this->db->where('g_id', $this->input->post('gestion')+9);
                                                                    $this->db->where('pfec_id', $this->input->post('id_f'));
                                                                    $this->db->update('ptto_fase_gestion', $update_fase);

                                                                    if($this->input->post('gest')>=11)
                                                                    {
                                                                      $update_fase = array('pfecg_ppto_total'     => str_replace(',','.',str_replace('.','',$this->input->post('pt11'))),
                                                                                           'pfecg_ppto_ejecutado' => str_replace(',','.',str_replace('.','',$this->input->post('pte11'))),
                                                                                           'estado' => $this->input->post('estado'),
                                                                                           'fun_id' => $this->session->userdata("fun_id"));

                                                                          $this->db->where('g_id', $this->input->post('gestion')+10);
                                                                          $this->db->where('pfec_id', $this->input->post('id_f'));
                                                                          $this->db->update('ptto_fase_gestion', $update_fase);

                                                                          if($this->input->post('gest')>=12)
                                                                          {
                                                                            $update_fase = array('pfecg_ppto_total'     => str_replace(',','.',str_replace('.','',$this->input->post('pt12'))),
                                                                                                 'pfecg_ppto_ejecutado' => str_replace(',','.',str_replace('.','',$this->input->post('pte12'))),
                                                                                                 'estado' => $this->input->post('estado'),
                                                                                                 'fun_id' => $this->session->userdata("fun_id"));

                                                                                $this->db->where('g_id', $this->input->post('gestion')+11);
                                                                                $this->db->where('pfec_id', $this->input->post('id_f'));
                                                                                $this->db->update('ptto_fase_gestion', $update_fase);
                                                                          }
                                                                    }
                                                              }
                                                        }
                                                  }
                                            }
                                      }
                                  }
                              }
                          }
                      }
                  }

                if($this->input->post('tp')==0) /////// datos generales-mi programacion
                {
                  if($this->input->post('ep')==1)//////// tecnico operativo
                  {
                    if($this->input->post('tp_id')==2 || $this->input->post('tp_id')==3 || $this->input->post('tp_id')==4)///// recurrente, no recurrente
                    {
                        redirect('admin/proy/prog/4/'.$this->input->post('id_p'));
                    }
                    elseif ($this->input->post('tp_id')==1) ///// inversion
                    {
                       redirect('admin/proy/fase_etapa/'.$this->input->post('id_p').'');
                     }
                  }
                  elseif($this->input->post('ep')==2)/////// validador POA
                  {
                    if($this->input->post('tp_id')==2 || $this->input->post('tp_id')==3 || $this->input->post('tp_id')==4) //// recurrente, no recurrente
                    {
                        redirect('admin/proy/list_proy_poa');
                    }
                    elseif ($this->input->post('tp_id')==1) ////// inversion
                    {
                       redirect('admin/proy/fase_etapa/'.$this->input->post('id_p').'');
                     }
                  }
                  elseif($this->input->post('ep')==3)/////// validador FINANCIERO
                  {
                    redirect('admin/proy/list_proy_fin');
                  }
                  else{
                      redirect('admin/proy/prog/4/'.$this->input->post('id_p'));
                  }

                }
                elseif ($this->input->post('tp')==1) ////// Datos generales-Programacion del proyecto
                {
                 redirect('admin/proy/edit_prog/'.$this->input->post('mod').'/'.$this->input->post('id_p').'/9');
                }

            }
            else
            {
                if($this->input->post('tp')==0)
                {
                  redirect('admin/proy/newfase/'.$this->input->post('mod').'/'.$this->input->post('id_p').'/0/2');
                }
                elseif ($this->input->post('tp')==1)
                {
                  redirect('admin/comp/update_f/'.$this->input->post('id_f').'/'.$this->input->post('id_p').'/2');
                }

            }
        }
  }
/*=========================================================================================================================*/

/*======================================== VALIDAR NUEVA FASE ETAPA 2 SGP=================================================================================*/
  function add_fase2_sgp()
  {
         if($this->input->server('REQUEST_METHOD') === 'POST')
          {
            $this->form_validation->set_rules('id_f', 'Id de la fase', 'required|trim');
            $this->form_validation->set_rules('id_p', 'Id del Proyecto', 'required|trim');

            $update_fasee = array('pfec_ptto_fase_e' => $this->input->post('pe'));

                        $this->db->where('pfec_id', $this->input->post('id_f'));
                        $this->db->where('proy_id', $this->input->post('id_p'));
                        $this->db->update('_proyectofaseetapacomponente', $update_fasee);

            if ($this->form_validation->run())
            {
                  if($this->input->post('gest')>=1)
                  {
                    $update_fase = array('pfecg_ppto_total' => $this->input->post('pt1'),
                                         'pfecg_ppto_ejecutado' => $this->input->post('pte1'),
                                         'estado' => $this->input->post('estado'),
                                         'fun_id' => $this->session->userdata("fun_id"));

                        $this->db->where('g_id', $this->input->post('gestion'));
                        $this->db->where('pfec_id', $this->input->post('id_f'));
                        $this->db->update('ptto_fase_gestion', $update_fase);

                      if($this->input->post('gest')>=2)
                      {
                        $update_fase = array('pfecg_ppto_total' => $this->input->post('pt2'),
                                             'pfecg_ppto_ejecutado' => $this->input->post('pte2'),
                                             'estado' => $this->input->post('estado'),
                                             'fun_id' => $this->session->userdata("fun_id"));

                            $this->db->where('g_id', $this->input->post('gestion')+1);
                            $this->db->where('pfec_id', $this->input->post('id_f'));
                            $this->db->update('ptto_fase_gestion', $update_fase);

                          if($this->input->post('gest')>=3)
                          {
                            $update_fase = array('pfecg_ppto_total' => $this->input->post('pt3'),
                                                 'pfecg_ppto_ejecutado' => $this->input->post('pte3'),
                                                 'estado' => $this->input->post('estado'),
                                                 'fun_id' => $this->session->userdata("fun_id"));

                                $this->db->where('g_id', $this->input->post('gestion')+2);
                                $this->db->where('pfec_id', $this->input->post('id_f'));
                                $this->db->update('ptto_fase_gestion', $update_fase);


                              if($this->input->post('gest')>=4)
                              {
                                $update_fase = array('pfecg_ppto_total' => $this->input->post('pt4'),
                                                     'pfecg_ppto_ejecutado' => $this->input->post('pte4'),
                                                     'estado' => $this->input->post('estado'),
                                                     'fun_id' => $this->session->userdata("fun_id"));

                                    $this->db->where('g_id', $this->input->post('gestion')+3);
                                    $this->db->where('pfec_id', $this->input->post('id_f'));
                                    $this->db->update('ptto_fase_gestion', $update_fase);

                                  if($this->input->post('gest')>=5)
                                  {
                                    $update_fase = array('pfecg_ppto_total' => $this->input->post('pt5'),
                                                         'pfecg_ppto_ejecutado' => $this->input->post('pte5'),
                                                         'estado' => $this->input->post('estado'),
                                                         'fun_id' => $this->session->userdata("fun_id"));

                                        $this->db->where('g_id', $this->input->post('gestion')+4);
                                        $this->db->where('pfec_id', $this->input->post('id_f'));
                                        $this->db->update('ptto_fase_gestion', $update_fase);

                                      if($this->input->post('gest')>=6)
                                      {
                                        $update_fase = array('pfecg_ppto_total' => $this->input->post('pt6'),
                                                             'pfecg_ppto_ejecutado' => $this->input->post('pte6'),
                                                             'estado' => $this->input->post('estado'),
                                                             'fun_id' => $this->session->userdata("fun_id"));

                                            $this->db->where('g_id', $this->input->post('gestion')+5);
                                            $this->db->where('pfec_id', $this->input->post('id_f'));
                                            $this->db->update('ptto_fase_gestion', $update_fase);

                                            if($this->input->post('gest')>=7)
                                            {
                                              $update_fase = array('pfecg_ppto_total' => $this->input->post('pt7'),
                                                                   'pfecg_ppto_ejecutado' => $this->input->post('pte7'),
                                                                   'estado' => $this->input->post('estado'),
                                                                   'fun_id' => $this->session->userdata("fun_id"));

                                                  $this->db->where('g_id', $this->input->post('gestion')+6);
                                                  $this->db->where('pfec_id', $this->input->post('id_f'));
                                                  $this->db->update('ptto_fase_gestion', $update_fase);

                                                  if($this->input->post('gest')>=8)
                                                  {
                                                    $update_fase = array('pfecg_ppto_total' => $this->input->post('pt8'),
                                                                         'pfecg_ppto_ejecutado' => $this->input->post('pte8'),
                                                                         'estado' => $this->input->post('estado'),
                                                                         'fun_id' => $this->session->userdata("fun_id"));

                                                        $this->db->where('g_id', $this->input->post('gestion')+7);
                                                        $this->db->where('pfec_id', $this->input->post('id_f'));
                                                        $this->db->update('ptto_fase_gestion', $update_fase);

                                                        if($this->input->post('gest')>=9)
                                                        {
                                                          $update_fase = array('pfecg_ppto_total' => $this->input->post('pt9'),
                                                                               'pfecg_ppto_ejecutado' => $this->input->post('pte9'),
                                                                               'estado' => $this->input->post('estado'),
                                                                               'fun_id' => $this->session->userdata("fun_id"));

                                                              $this->db->where('g_id', $this->input->post('gestion')+8);
                                                              $this->db->where('pfec_id', $this->input->post('id_f'));
                                                              $this->db->update('ptto_fase_gestion', $update_fase);

                                                              if($this->input->post('gest')>=10)
                                                              {
                                                                $update_fase = array('pfecg_ppto_total' => $this->input->post('pt10'),
                                                                                     'pfecg_ppto_ejecutado' => $this->input->post('pte10'),
                                                                                     'estado' => $this->input->post('estado'),
                                                                                     'fun_id' => $this->session->userdata("fun_id"));

                                                                    $this->db->where('g_id', $this->input->post('gestion')+9);
                                                                    $this->db->where('pfec_id', $this->input->post('id_f'));
                                                                    $this->db->update('ptto_fase_gestion', $update_fase);

                                                                    if($this->input->post('gest')>=11)
                                                                    {
                                                                      $update_fase = array('pfecg_ppto_total' => $this->input->post('pt11'),
                                                                                           'pfecg_ppto_ejecutado' => $this->input->post('pte11'),
                                                                                           'estado' => $this->input->post('estado'),
                                                                                           'fun_id' => $this->session->userdata("fun_id"));

                                                                          $this->db->where('g_id', $this->input->post('gestion')+10);
                                                                          $this->db->where('pfec_id', $this->input->post('id_f'));
                                                                          $this->db->update('ptto_fase_gestion', $update_fase);

                                                                          if($this->input->post('gest')>=12)
                                                                          {
                                                                            $update_fase = array('pfecg_ppto_total' => $this->input->post('pt12'),
                                                                                                 'pfecg_ppto_ejecutado' => $this->input->post('pte12'),
                                                                                                 'estado' => $this->input->post('estado'),
                                                                                                 'fun_id' => $this->session->userdata("fun_id"));

                                                                                $this->db->where('g_id', $this->input->post('gestion')+11);
                                                                                $this->db->where('pfec_id', $this->input->post('id_f'));
                                                                                $this->db->update('ptto_fase_gestion', $update_fase);
                                                                          }
                                                                    }
                                                              }
                                                        }
                                                  }
                                            }
                                      }
                                  }
                              }
                          }
                      }
                  }

                if($this->input->post('tp')==0) /////// datos generales- Gerencia de Proyectos
                {
                    if($this->input->post('tp_id')==2 || $this->input->post('tp_id')==3 || $this->input->post('tp_id')==4)///// recurrente, no recurrente
                    {
                        redirect('admin/sgp/list_proy');
                    }
                    elseif ($this->input->post('tp_id')==1) ///// inversion
                    {
                       redirect('admin/sgp/fase_etapa/'.$this->input->post('id_p').'');
                    }
                }
                elseif ($this->input->post('tp')==1) ////// Datos generales-Programacion del proyecto SGP
                {
                 redirect('admin/sgp/edit_prog/'.$this->input->post('mod').'/'.$this->input->post('id_p').'/9');
                }

            }
            else
            {
                if($this->input->post('tp')==0)
                {
                  redirect('admin/sgp/newfase/'.$this->input->post('mod').'/'.$this->input->post('id_p').'/0/2');
                }
                elseif ($this->input->post('tp')==1)
                {
                  redirect('admin/comp/update_f/'.$this->input->post('id_f').'/'.$this->input->post('id_p').'/2');
                }

            }
        }
  }
/*=========================================================================================================================*/

/*============================================= MODIFICAR FASE ETAPA ============================================================================*/
    function modificar_fase($id_f,$id_p,$form)
    {
      if($this->rolfun($this->rol) & $id_f!='' & $id_p!='' & $form!=''){
          /* $enlaces=$this->menu_modelo->get_Modulos(1);
   $data['enlaces'] = $enlaces;
   for($i=0;$i<count($enlaces);$i++){
     $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
   }
   $data['subenlaces'] = $subenlaces;*/

          $enlaces=$this->menu_modelo->get_Modulos_programacion(1);
          $data['enlaces'] = $enlaces;
          $data['nro_fase']= $this->model_faseetapa->nro_fase($id_p); /// nro de fases y etapas registrados
          $data['id_f'] = $this->model_faseetapa->get_id_fase($id_p); //// DATOS DE LA FASE ACTIVA
          $data['mod']=4;

          $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p); //// Datos del proyecto
          $data['fase_proyecto'] = $this->model_faseetapa->fase_etapa($id_f,$id_p); //// Datos de la fase activa
          if($data['proyecto'][0]['tp_id']==4){$titulo='Resultados';}else{$titulo='Metas';}
          $data['titulo'] = $titulo;
          $datos = $this->model_faseetapa->datos_fase_etapa($id_f,$id_p);  // desvuelve las fechas del proyecto
          $data['nro_fg'] = $this->model_faseetapa->nro_fasegestion($id_f);
          $data['nro_fg_act'] = $this->model_faseetapa->nro_fasegestion_actual($id_f,$this->session->userdata('gestion'));
          $data['fi']=$datos[0]['actual'];
          $data['ff']=$datos[0]['final'];

          //load the view
        if($form==1) /////// PRIMER FORMULARIO DE REGISTRO F/E
        {
          $data['f_gest'] = $this->model_faseetapa->fase_gestion($id_f,$this->session->userdata('gestion'));
          $data['verif'] = $this->model_faseetapa->nro_ffofet($id_f); ///// para controlar el modificado de las fechas
          $data['nro_c'] = $this->model_componente->componentes_nro($id_f); /// nro de componentes ya registrados
          $data['fases'] = $this->model_faseetapa->fases(); //// lista de Fases
          $data['unidad_org'] = $this->model_proyecto->list_unidad_org(); //// unidad organizacional
          $data['f_top'] = $this->model_proyecto->responsable_proy($id_p,'1'); //// unidad ejecutora

          $data['main_content'] = 'admin/programacion/proy_anual/fase/form_fase_update';
          $this->load->view('admin/programacion/proy_anual/fase/form_fase_update', $data);
        }
        elseif($form==2) /////// SEGUNDO FORMULARIO DE REGISTRO F/E
        {

          $a�os=$datos[0]['final']-$datos[0]['actual']+1;
          $data['gestiones']=$a�os;
          $fecha=$datos[0]['actual'];
          $data['fecha']=$fecha;

          if($a�os>=1)
          {
            $data['fase_gestion1']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha);
            if($a�os>=2)
            {
              $data['fase_gestion2']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+1);
              if($a�os>=3)
              {
                $data['fase_gestion3']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+2);
                if($a�os>=4)
                {
                  $data['fase_gestion4']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+3);
                  if($a�os>=5)
                  {
                    $data['fase_gestion5']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+4);
                    if($a�os>=6)
                    {
                      $data['fase_gestion6']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+5);
                      if($a�os>=7)
                      {
                        $data['fase_gestion7']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+6);
                        if($a�os>=8)
                        {
                          $data['fase_gestion8']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+7);
                          if($a�os>=9)
                          {
                            $data['fase_gestion9']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+8);
                            if($a�os>=10)
                            {
                              $data['fase_gestion10']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+9);
                              if($a�os>=11)
                              {
                                $data['fase_gestion11']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+10);
                                if($a�os>=12)
                                {
                                  $data['fase_gestion12']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+11);
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }

          $this->load->view('admin/programacion/proy_anual/fase/form_fase_update2', $data);
        }
      }
      else{
        redirect('admin/dashboard');
      }
    }
/*=========================================================================================================================*/

/*============================================= MODIFICAR FASE ETAPA SGP ============================================================================*/
    function modificar_fase_sgp($id_f,$id_p,$form)
    {
      if($this->rolfun($this->rol) & $id_f!='' & $id_p!='' & $form!=''){
        $enlaces=$this->menu_modelo->get_Modulos(4);
        $data['enlaces'] = $enlaces;
        for($i=0;$i<count($enlaces);$i++)
        {
          $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;

        $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p); //// Datos del proyecto
        $data['fase_proyecto'] = $this->model_faseetapa->fase_etapa($id_f,$id_p); //// Datos de la fase activa
        $datos = $this->model_faseetapa->datos_fase_etapa($id_f,$id_p);  // desvuelve las fechas del proyecto
        $data['nro_fg'] = $this->model_faseetapa->nro_fasegestion($id_f);
        $data['nro_fg_act'] = $this->model_faseetapa->nro_fasegestion_actual($id_f,$this->session->userdata('gestion'));
        $data['fi']=$datos[0]['actual'];
        $data['ff']=$datos[0]['final'];
        //load the view
        if($form==1) /////// PRIMER FORMULARIO DE REGISTRO F/E
        {
          $data['f_gest'] = $this->model_faseetapa->fase_gestion($id_f,$this->session->userdata('gestion'));
          $data['verif'] = $this->model_faseetapa->nro_ffofet($id_f); ///// para controlar el modificado de las fechas
          $data['nro_c'] = $this->model_componente->componentes_nro($id_f); /// nro de componentes ya registrados
          $data['fases'] = $this->model_faseetapa->fases(); //// lista de Fases
          $data['unidad_org'] = $this->model_proyecto->list_unidad_org(); //// unidad organizacional
          $data['f_top'] = $this->model_proyecto->responsable_proy($id_p,'1'); //// unidad ejecutora

          $data['main_content'] = 'admin/programacion/proy_multi/fase/form_fase_update';
          $this->load->view('admin/programacion/proy_multi/fase/form_fase_update', $data);
        }
        elseif($form==2) /////// SEGUNDO FORMULARIO DE REGISTRO F/E
        {

          $a�os=$datos[0]['final']-$datos[0]['actual']+1;
          $data['gestiones']=$a�os;
          $fecha=$datos[0]['actual'];
          $data['fecha']=$fecha;


          if($a�os>=1)
          {
            $data['fase_gestion1']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha);
            if($a�os>=2)
            {
              $data['fase_gestion2']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+1);
              if($a�os>=3)
              {
                $data['fase_gestion3']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+2);
                if($a�os>=4)
                {
                  $data['fase_gestion4']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+3);
                  if($a�os>=5)
                  {
                    $data['fase_gestion5']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+4);
                    if($a�os>=6)
                    {
                      $data['fase_gestion6']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+5);
                      if($a�os>=7)
                      {
                        $data['fase_gestion7']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+6);
                        if($a�os>=8)
                        {
                          $data['fase_gestion8']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+7);
                          if($a�os>=9)
                          {
                            $data['fase_gestion9']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+8);
                            if($a�os>=10)
                            {
                              $data['fase_gestion10']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+9);
                              if($a�os>=11)
                              {
                                $data['fase_gestion11']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+10);
                                if($a�os>=12)
                                {
                                  $data['fase_gestion12']=$this->model_faseetapa->fase_etapa_gestion($id_f,$fecha+11);
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
          $this->load->view('admin/programacion/proy_multi/fase/form_fase_update2', $data);
        }
      }
      else{
        redirect('admin/dashboard');
      }
    }
/*=========================================================================================================================*/
/*======================================= ACTUALIZAR/MODIFICAR  FASE ETAPA ==================================================================================*/
  function update_fase_etapa()
  {
         if($this->input->server('REQUEST_METHOD') === 'POST')
          {
            $this->form_validation->set_rules('id', 'Codigo fase ', 'required|trim');
            $this->form_validation->set_rules('id_p', 'Id del proyecto ', 'required|trim');
            $this->form_validation->set_rules('fase', 'Fase', 'required|trim');
            $this->form_validation->set_rules('etapas', 'Etapas', 'required|trim');
            $this->form_validation->set_rules('f_ejec', 'forma de ejecucion', 'required|trim');

            if ($this->form_validation->run())
            {
                /*=========================== ACTUALIZANDO FASE ETAPA ==================================*/
              $query=$this->db->query('set datestyle to DMY');
              $update_fase = array(
                              'fas_id' => $this->input->post('fase'),
                              'eta_id' => $this->input->post('etapas'),
                              'pfec_descripcion' => strtoupper($this->input->post('desc')),
                              'pfec_fecha_inicio_ddmmaaa' => $this->input->post('f_inicio'),
                              'pfec_fecha_fin_ddmmaaa' => $this->input->post('f_final'),
                              'pfec_fecha_registro' => date('d/m/Y h:i:s'),
                              'pfec_ejecucion' => $this->input->post('f_ejec'),
                              'pfec_ptto_fase' => $this->input->post('monto_total'),
                              'fun_id' => $this->session->userdata("fun_id"),
                              'unidad_ejec' => $this->input->post('uni_ejec'),
                              'estado' => 2);

                $this->db->where('pfec_id', $this->input->post('id'));
                $this->db->where('proy_id', $this->input->post('id_p'));
                $this->db->update('_proyectofaseetapacomponente', $update_fase);
                /*=======================================================================================*/

                $fechas = $this->model_faseetapa->datos_fase_etapa($this->input->post('id'),$this->input->post('id_p'));  // devuelve aper_id

                      $update_fase2 = array(  'pfec_fecha_inicio' => $fechas[0]['actual'],
                                       'pfec_fecha_fin' => $fechas[0]['final']);

                      $this->db->where('pfec_id', $this->input->post('id'));
                      $this->db->update('_proyectofaseetapacomponente', $update_fase2);

                /*=======================  fase etapa gestion ======================*/

                /*-------------------- cuando la fecha inicial se modifica antes fi2<fi1-----------------*/
                if($fechas[0]['actual']<$this->input->post('fi'))
                {
                      $gestion_inicio_nuevo=$fechas[0]['actual'];
                      for($i=$gestion_inicio_nuevo;$i<$this->input->post('fi');$i++)
                      {
                         $data_to_store2 = array( ///// Tabla ptto_fase_gestion
                            'pfec_id' => $this->input->post('id'),
                            'g_id' => $i,
                            'fun_id' => $this->session->userdata("fun_id"),
                          );
                          $this->db->insert('ptto_fase_gestion', $data_to_store2); ///// inserta a ptto_fase_gestion
                      }
                }
                /*-------------------- cuando la fecha final se modifica despues ff2>ff1-----------------*/
                if($fechas[0]['final']>$this->input->post('ff'))
                {
                      $gestion_final_anterior=$this->input->post('ff')+1;
                      for($i=$gestion_final_anterior;$i<=$fechas[0]['final'];$i++)
                      {
                         $data_to_store2 = array( ///// Tabla ptto_fase_gestion
                            'pfec_id' => $this->input->post('id'),
                            'g_id' => $i,
                            'fun_id' => $this->session->userdata("fun_id"),
                          );
                          $this->db->insert('ptto_fase_gestion', $data_to_store2); ///// inserta a ptto_fase_gestion
                      }
                }

                /*----------- cuando la fecha inicial se modifica despues fi2>fi1 --------------*/
                if($fechas[0]['actual']>$this->input->post('fi'))
                {
                      $gestion_inicio_anterior=$this->input->post('fi');
                      for($i=$gestion_inicio_anterior;$i<$fechas[0]['actual'];$i++)
                      {
                          /*-------- Eliminando gestiones de ptto fase ----------*/
                          $this->db->where('pfec_id', $this->input->post('id'));
                          $this->db->where('g_id', $i);
                          $this->db->delete('ptto_fase_gestion');
                          /*------------------------------------------------------*/
                      }
                }

                /*----------- cuando la fecha final se modifica antes ff2>ff1 --------------*/
                if($this->input->post('ff')>$fechas[0]['final'])
                {
                      $gestion_final_nuevo=$fechas[0]['final']+1;
                      for($i=$gestion_final_nuevo;$i<=$this->input->post('ff');$i++)
                      {
                          /*-------- Eliminando gestiones de ptto fase ----------*/
                          $this->db->where('pfec_id', $this->input->post('id'));
                          $this->db->where('g_id', $i);
                          $this->db->delete('ptto_fase_gestion');
                          /*------------------------------------------------------*/
                      }
                }

                if($this->input->post('tp')==0)
                {
                  redirect('admin/proy/update_f/'.$this->input->post('id').'/'.$this->input->post('id_p').'/2'); ///// Registro De proyectos Fase Etapa
                }
                elseif ($this->input->post('tp')==1) {
                  redirect('admin/prog/update_fase/'.$this->input->post('mod').'/'.$this->input->post('id').'/'.$this->input->post('id_p').'/2'); ///// Programacion del proyecto Datos Generales
                }


            }
            else
            {
              if($this->input->post('tp')==0)
                {
                  redirect('admin/proy/update_f/'.$this->input->post('id').'/'.$this->input->post('id_p').'/1');
                }
                elseif ($this->input->post('tp')==1) {
                  redirect('admin/proy/update_fase/'.$this->input->post('mod').'/'.$this->input->post('id').'/'.$this->input->post('id_p').'/1');
                }

            }
        }
  }

/*================================== ACTUALIZAR/MODIFICAR  FASE ETAPA ==========================================*/
  function update_fase_etapa_sgp()
  {
         if($this->input->server('REQUEST_METHOD') === 'POST')
          {
            $this->form_validation->set_rules('id', 'Codigo fase ', 'required|trim');
            $this->form_validation->set_rules('id_p', 'Id del proyecto ', 'required|trim');
            $this->form_validation->set_rules('fase', 'Fase', 'required|trim');
            $this->form_validation->set_rules('etapas', 'Etapas', 'required|trim');
            $this->form_validation->set_rules('f_ejec', 'forma de ejecucion', 'required|trim');

            if ($this->form_validation->run())
            {
                /*=========================== ACTUALIZANDO FASE ETAPA ==================================*/
                $query=$this->db->query('set datestyle to DMY');
                if($this->input->post('nro')==0 & $this->input->post('nro_c')==0)
                {
                      $update_fase = array(
                                      'fas_id' => $this->input->post('fase'),
                                      'eta_id' => $this->input->post('etapas'),
                                      'pfec_descripcion' => strtoupper($this->input->post('desc')),
                                      'pfec_fecha_inicio_ddmmaaa' => $this->input->post('f_inicio'),
                                      'pfec_fecha_fin_ddmmaaa' => $this->input->post('f_final'),
                                      'pfec_fecha_registro' => date('d/m/Y h:i:s'),
                                      'pfec_ejecucion' => $this->input->post('f_ejec'),
                                      'pfec_ptto_fase' => $this->input->post('monto_total'),
                                      'fun_id' => $this->session->userdata("fun_id"),
                                      'unidad_ejec' => $this->input->post('uni_ejec'),
                                      'estado' => 2);

                        $this->db->where('pfec_id', $this->input->post('id'));
                        $this->db->where('proy_id', $this->input->post('id_p'));
                        $this->db->update('_proyectofaseetapacomponente', $update_fase);
                }
                elseif($this->input->post('nro')!=0)
                {
                      $update_fase = array(
                                      'fas_id' => $this->input->post('fase'),
                                      'eta_id' => $this->input->post('etapas'),
                                      'pfec_descripcion' => $this->input->post('desc'),
                                      'pfec_fecha_registro' => date('d/m/Y h:i:s'),
                                      'pfec_ejecucion' => $this->input->post('f_ejec'),
                                      'pfec_ptto_fase' => $this->input->post('monto_total'),
                                      'fun_id' => $this->session->userdata("fun_id"),
                                      'unidad_ejec' => $this->session->userdata("uni_ejec"),
                                      'estado' => 2);

                        $this->db->where('pfec_id', $this->input->post('id'));
                        $this->db->where('proy_id', $this->input->post('id_p'));
                        $this->db->update('_proyectofaseetapacomponente', $update_fase);
                }

                /*=========================== END ACTUALIZANDO FASE ETAPA ==================================*/

                /*=========================== SACANDO LAS GESTIONES DE INICIO Y FINAL DE LA FASE ==================================*/
                $datos = $this->model_faseetapa->datos_fase_etapa($this->input->post('id'),$this->input->post('id_p'));  // devuelve aper_id
                $nro_comp = $this->model_componente->componentes_nro($this->input->post('id')); /// nro de componentes ya registrados
                /*=========================== END SACANDO LAS GESTIONES DE INICIO Y FINAL DE LA FASE ==================================*/

                if(($datos[0]['actual']!=$this->input->post('fi') || $datos[0]['final']!=$this->input->post('ff') && $nro_comp==0) && $this->input->post('tp')==0)
                {
                          /*=========================== BORRANDO DATOS DE FASEETAPA GESTION ==================================*/
                          $this->model_faseetapa->delete_fechas_faseetapa($this->input->post('id'));
                          /*=========================== END BORRANDO DATOS DE FASEETAPA GESTION ==================================*/

                          /*=========================== INSERTANDO DATOS DE LAS GESTIONES A LA TABLA F/E ==================================*/
                          $a�os=$datos[0]['final']-$datos[0]['actual'];
                          $fecha=$datos[0]['actual'];

                           $update_fase2 = array('pfec_fecha_inicio' => $datos[0]['actual'],
                                                 'pfec_fecha_fin' => $datos[0]['final']);

                                  $this->db->where('proy_id', $this->input->post('id_p'));
                                  $this->db->where('pfec_id', $this->input->post('id'));
                                  $this->db->update('_proyectofaseetapacomponente', $update_fase2);

                          /*=======================  fase etapa gestion ======================*/
                            for($i=0;$i<=$a�os;$i++)
                            {
                               $data_to_store2 = array( ///// Tabla ptto_fase_gestion
                                  'pfec_id' => $this->input->post('id'),
                                  'g_id' => $fecha,
                                  'fun_id' => $this->session->userdata("fun_id"),
                                  'estado' => 2,
                                );
                                $this->db->insert('ptto_fase_gestion', $data_to_store2); ///// inserta a ptto_fase_gestion
                              $fecha=$fecha+1;
                            }
                          /*========================== end fase etapa gestion ======================*/

                }
                if($this->input->post('tp')==0)
                {
                  redirect('admin/sgp/update_f/'.$this->input->post('id').'/'.$this->input->post('id_p').'/2'); ///// Registro De proyectos Fase Etapa
                }
                elseif ($this->input->post('tp')==1) {
                  redirect('admin/sgp/update_fase/'.$this->input->post('mod').'/'.$this->input->post('id').'/'.$this->input->post('id_p').'/2'); ///// Programacion del proyecto Datos Generales
                }


            }
            else
            {
              if($this->input->post('tp')==0)
                {
                  redirect('admin/sgp/update_f/'.$this->input->post('id').'/'.$this->input->post('id_p').'/1');
                }
                elseif ($this->input->post('tp')==1) {
                  redirect('admin/sgp/update_fase/'.$this->input->post('mod').'/'.$this->input->post('id').'/'.$this->input->post('id_p').'/1');
                }


            }
        }
  }
/*=========================================================================================================================*/
  /*======================================= OBTIENE DATOS DE LA FASE ACTIVA ============================*/
  public function get_fase_activa(){
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $post = $this->input->post();
            $id_fase = $post['id_fase'];
            $id_proy = $post['id_proy'];
            $dato = $this->model_faseetapa->fase_etapa($id_fase,$id_proy);
                $result = array(
                    'descripcion' => $dato[0]['descripcion'],
                    'eficacia' => $dato[0]['pfec_eficacia'],
                    'financiera' => $dato[0]['pfec_eficiencia'],
                    'ejecucion' => $dato[0]['pfec_eficiencia_pe'],
                    'fisica' => $dato[0]['pfec_eficiencia_fi'],
                );
            echo json_encode($result);
        } else {
            show_404();
        }
    }
  /*=====================================================================================================*/

  /*============================================= ADICIONA INDICADOR====================================*/
      function add_indicador(){
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $post = $this->input->post();
            $id_fase = $post['id_fase'];
            $eficacia = $post['eficacia'];
            $financiera = $post['financiera'];
            $ejecucion = $post['ejecucion'];
            $fisica = $post['fisica'];
            if($this->model_faseetapa->add_indicador_fase($id_fase,$eficacia,$financiera,$ejecucion,$fisica)){
                $result = array(
                    'respuesta' => 'true',
                );
            }else{
                $result = array(
                    'respuesta' => 'false',
                );
            }
            echo json_encode($result);
        } else {
            show_404();
        }
    }
    /*=====================================================================================================*/

   /*================================== VERIFICA LAS DEPENDENCIA DE LA FASE ============================-*/
    public function verif_fase()
      {
        if($this->input->is_ajax_request())
            {
                $post = $this->input->post();
                $id_f = $post['id_f'];

                $nro=$this->model_componente->componentes_nro($id_f);

                 if($nro == 0)
                 {
                 echo "true"; ///// La fase no tiene dependencias
                 }
                 else
                 {
                  echo "false";; //// La fase tiene dependencias
                 }
              }else{
                      show_404();
                  }
      }

    /*=================================== ELIMINA FASE ETAPA ==================================================*/
    public function delete_fase(){
        if($this->input->is_ajax_request() && $this->input->post()){
            $post = $this->input->post();
            $pfec_id = $post['pfec_id'];

            $update_ptto = array(
                    'estado' => '3',
                    'fun_id' => $this->session->userdata("fun_id"),
                    );
            $this->db->where('pfec_id', $pfec_id);
            $this->db->update('ptto_fase_gestion', $update_ptto);

            $update_fase = array(
                    'pfec_estado' => '0',
                    'estado' => '3',
                    'fun_id' => $this->session->userdata("fun_id"),
                    );
            $this->db->where('pfec_id', $pfec_id);
            $this->db->update('_proyectofaseetapacomponente', $update_fase);

            $sql = $this->db->get();

            if($this->db->query($sql)){
                echo $pfec_id;
            }else{
                echo false;
            }
        }else{
            show_404();
        }
    }
/*================================== ASIGNAR PREUPUESTO A LA FASE ETAPA =======================================================================================*/
    function asignar_presupuesto($id_p)
    {
      if($this->rolfunn(5) & $id_p!=''){
        $enlaces=$this->menu_modelo->get_Modulos(1);
        $data['enlaces'] = $enlaces;
        for($i=0;$i<count($enlaces);$i++)
        {
          $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;

        $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p); ///// datos del proyecto
        $titulo_proy=strtoupper($data['proyecto'][0]['tipo']);
        $data['titulo'] = $titulo_proy;
        $data['fase'] = $this->model_faseetapa->get_id_fase($id_p); ///// datos fase encendida
        $data['fase_gest'] = $this->model_faseetapa->fase_gestion($data['fase'][0]['id'],$this->session->userdata("gestion")); //// fase de la gestion actual
        $data['ffi'] = $this->model_faseetapa->fuentefinanciamiento(); ///// fuente financiamiento
        $data['fof'] = $this->model_faseetapa->organismofinanciador(); ///// organismo financiador
        //load the view
        $this->load->view('admin/programacion/proy_anual/fase/fase_asig_ptto', $data);
      }
      else{
        redirect('admin/dashboard');
      }
    }

/*====================================================== VALIDA TECHO PRESUPUESTARIO ====================================================*/
    function add_techo_presupuesto()
    {
       if($this->input->server('REQUEST_METHOD') === 'POST')
        {
          $this->form_validation->set_rules('id_p', 'Codigo Id del proyecto ', 'required|trim');
          $this->form_validation->set_rules('id_f', 'Id de la fase ', 'required|trim');
          $this->form_validation->set_rules('id_fg', 'Id de la fase gestion veigente', 'required|trim');

          if ($this->form_validation->run())
          {
               if($this->input->post('numero')>=1)
               {
                  $this->model_faseetapa->store_asig($this->input->post('id_fg'),$this->input->post('ff1'),$this->input->post('fo1'),$this->input->post('p1'),1);
                  if($this->input->post('numero')>=2)
                   {
                      $this->model_faseetapa->store_asig($this->input->post('id_fg'),$this->input->post('ff2'),$this->input->post('fo2'),$this->input->post('p2'),2);
                      if($this->input->post('numero')>=3)
                       {
                          $this->model_faseetapa->store_asig($this->input->post('id_fg'),$this->input->post('ff3'),$this->input->post('fo3'),$this->input->post('p3'),3);
                          if($this->input->post('numero')>=4)
                           {
                              $this->model_faseetapa->store_asig($this->input->post('id_fg'),$this->input->post('ff4'),$this->input->post('fo4'),$this->input->post('p4'),4);
                              if($this->input->post('numero')>=5)
                               {
                                  $this->model_faseetapa->store_asig($this->input->post('id_fg'),$this->input->post('ff5'),$this->input->post('fo5'),$this->input->post('p5'),5);
                                    if($this->input->post('numero')>=6)
                                     {
                                        $this->model_faseetapa->store_asig($this->input->post('id_fg'),$this->input->post('ff6'),$this->input->post('fo6'),$this->input->post('p6'),6);
                                        if($this->input->post('numero')>=7)
                                         {
                                            $this->model_faseetapa->store_asig($this->input->post('id_fg'),$this->input->post('ff7'),$this->input->post('fo7'),$this->input->post('p7'),7);
                                            if($this->input->post('numero')>=8)
                                             {
                                                $this->model_faseetapa->store_asig($this->input->post('id_fg'),$this->input->post('ff8'),$this->input->post('fo8'),$this->input->post('p8'),8);
                                                if($this->input->post('numero')>=9)
                                                 {
                                                    $this->model_faseetapa->store_asig($this->input->post('id_fg'),$this->input->post('ff9'),$this->input->post('fo9'),$this->input->post('p9'),9);
                                                    if($this->input->post('numero')>=10)
                                                     {
                                                        $this->model_faseetapa->store_asig($this->input->post('id_fg'),$this->input->post('ff10'),$this->input->post('fo10'),$this->input->post('p10'),10);
                                                     }
                                                     else{
                                                      $this->session->set_flashdata('danger','NRO DE RECURSOS SUPERA AL NUMERO DE RECURSOS ESTABLECIDOS, CONTACTESE CON EL ADMINISTRADOR ');
                                                      redirect('admin/proy/asig_ptto/'.$this->input->post('id_p').'');
                                                     }
                                                 }
                                             }
                                         }
                                     }
                               }
                           }
                       }
                   }
               }

              $this->session->set_flashdata('success','SE GUARDO CORRECTAMENTE LOS RECURSOS ASIGNADOS');
              redirect(site_url("admin") . '/proy/ver_techo_ptto/'.$this->input->post('id_p').'/'.$this->input->post('id_fg').'/true');
          }
          else
          {
            $this->session->set_flashdata('danger','ERROR AL REGISTRAR RECURSOS');
           redirect('admin/proy/asig_ptto/'.$this->input->post('id_p').'');
          }
      }
    }
/*===============================================================================================================================================*/

/*========================================= TECHO PRESUPUESTARIO DE LA FASE ==============================================================*/
  function ver_techo_ptto($id_p,$id_fg)
  {
    if($this->rolfunn(5) & $id_p!='' & $id_fg!=''){
      $enlaces=$this->menu_modelo->get_Modulos(1);
      $data['enlaces'] = $enlaces;
      for($i=0;$i<count($enlaces);$i++)
      {
        $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
      }
      $data['subenlaces'] = $subenlaces;

      $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p); ///// datos del proyecto
      $titulo_proy=strtoupper($data['proyecto'][0]['tipo']);
      $data['titulo'] = $titulo_proy;
      $data['fase'] = $this->model_faseetapa->get_id_fase($id_p); ///// datos fase encendida
      $data['fase_gest'] = $this->model_faseetapa->fase_gestion($data['fase'][0]['id'],$this->session->userdata("gestion")); //// fase de la gestion actual
      $data['nro'] = $this->model_faseetapa->verif_presupuesto_activo($data['fase_gest'][0]['ptofecg_id']);
      $data['techo']=$this->model_faseetapa->techo_presupuestario($data['fase_gest'][0]['ptofecg_id']);

      $data['fase_asig'] = $this->model_faseetapa->fase_presupuesto_id($id_fg); //// lista del presupuesto asignado
      $data['ffi'] = $this->model_faseetapa->fuentefinanciamiento(); ///// fuente financiamiento
      $data['fof'] = $this->model_faseetapa->organismofinanciador(); ///// organismo financiador

      $this->load->view('admin/programacion/proy_anual/fase/list_fase_techo', $data);
    }
    else{
        redirect('admin/dashboard');
    }
  }
/*===============================================================================================================================================*/
  /*===================================== OBTIENE LOS DATOS DEL TECHO PRESUPUESTO =======================================*/
    public function get_techo_ptto()
    {
        if($this->input->is_ajax_request() && $this->input->post())
        {
            $post = $this->input->post();
            $id_ffofet = $post['id_ffofet']; /// id id_ffofet
            $suma_techo = $post['suma_techo']; /// ptto
            $id_ffofet = $this->security->xss_clean($id_ffofet);
            $suma_techo = $this->security->xss_clean($suma_techo);
            $dato_ffofet = $this->model_faseetapa->get_techo_id($id_ffofet);
            //caso para modificar el codigo de proyecto y actividades
            foreach($dato_ffofet as $row){
                $result = array(
                    'ffofet_id' => $row['ffofet_id'],
                    "ptofecg_id" =>$row['ptofecg_id'],
                    "ff_id" =>$row['ff_id'],
                    "of_id" =>$row['of_id'],
                    "et_id" =>$row['et_id'],
                    "monto" =>$suma_techo-$row['ffofet_monto'],
                    "ffofet_monto" =>$row['ffofet_monto']
                );
            }
            echo json_encode($result);
        }else{
            show_404();
        }
    }
  /*==================================================================================================================*/

   /*--------------------- ACTUALIZA LAS FUENTES DE FINANCIAMENTO --------------------*/
    public function valida_techo_ptto()
    {
      $fase = $this->model_faseetapa->fase_etapa($this->input->post('id_f'),$this->input->post('id_p'));
      $proyecto = $this->model_proyecto->get_id_proyecto($this->input->post('id_p'));

      $lista_fases_gestion=$this->model_faseetapa->list_fases_gestiones($this->input->post('id_f'));

      foreach ($lista_fases_gestion as $row)
      {
        //  echo "ptofecg_id : " .$row['ptofecg_id'].'-- Nro. '.$this->model_faseetapa->nro_fuentes($row['ptofecg_id'])."<br>";

          if($row['g_id']==$this->session->userdata("gestion")){$ffofet_id=$row['ptofecg_id'];}
          if($this->model_faseetapa->nro_fuentes($row['ptofecg_id'])==0)
          {
              if ( !empty($_POST["ffofet_id"]) && is_array($_POST["ffofet_id"]) ) {
                foreach ( array_keys($_POST["ffofet_id"]) as $como)
                {
                    $monto_ff=0;
                    if($row['g_id']==$this->session->userdata("gestion")){$monto_ff=$_POST["f_monto"];}
                    $data = array(
                        'ptofecg_id' => $row['ptofecg_id'],
                        'ff_id' => $_POST["ff"][$como],
                        'of_id' =>$_POST["of"][$como],
                        'ffofet_monto' =>$monto_ff,
                        'nro' =>$_POST["nro"][$como],
                    );
                    $this->db->insert('_ffofet',$data);

                //  echo $_POST["ffofet_id"][$como].'--'.$monto_ff.'<br>';
                }

             }
          }
      }

      $this->session->set_flashdata('success','SE ACTUALIZARON LAS FUENTES PARA LAS GESTIONES '.$fase[0]['pfec_fecha_inicio'].' a '.$fase[0]['pfec_fecha_fin'].'');
      redirect(site_url("admin") . '/proy/ver_techo_ptto/'.$this->input->post('id_p').'/'.$ffofet_id.'/true');

    }
   /*----------------------------------------------------------------------------------*/
  //=========================== VALIDA UN NUEVO TECHO PRESUPUESTARIO =============================
    function validar_techo_ptto(){

        if($this->input->is_ajax_request() && $this->input->post()){
            $this->form_validation->set_rules('id_fg', 'id fase gestion', 'required|trim');
            $this->form_validation->set_rules('ff_id', 'ff_id', 'required|trim|integer');
            $this->form_validation->set_rules('of_id', 'of_id', 'required|trim|integer');
            //=========================== mensajes =========================================
            $this->form_validation->set_message('required', 'El campo es es obligatorio');
            $this->form_validation->set_message('integer', 'El campo  debe poseer solo numeros enteros');

            if ($this->form_validation->run() ) {
                $proy_id=  $this->input->post('p_id');
                $fase_id=  $this->input->post('f_id');
                $id_fg=  $this->input->post('id_fg');
                $ff_id=  $this->input->post('ff_id');
                $of_id =  $this->input->post('of_id');
                $ffofet_monto =  $this->input->post('ffofet_monto');
                //=================enviar  evitar codigo malicioso ==========
                $proy_id = $this->security->xss_clean($proy_id);
                $fase_id = $this->security->xss_clean($fase_id);
                $id_fg = $this->security->xss_clean($id_fg);
                $ff_id = $this->security->xss_clean($ff_id);
                $of_id = $this->security->xss_clean($of_id);
                $ffofet_monto = $this->security->xss_clean($ffofet_monto);

                $lista_fases_gestion=$this->model_faseetapa->list_fases_gestiones($fase_id);
                $nro = count($this->model_faseetapa->fase_presupuesto_id($this->input->post('id_fg')))+1; //// lista del presupuesto asignado
                foreach ($lista_fases_gestion as $row)
                {
                    $monto=0;
                    if($row['g_id']==$this->session->userdata("gestion")){$monto=$ffofet_monto;}
                    $data = array(
                        'ptofecg_id' => $row['ptofecg_id'],
                        'ff_id' => $ff_id,
                        'of_id' =>$of_id,
                        'ffofet_monto' =>$monto,
                        'nro' =>$nro,
                    );
                    $this->db->insert('_ffofet',$data);
                }

                echo 'true';
            } else {
                echo'DATOS ERRONEOS';
            }
        }else{
            show_404();
        }
    }
  /*===================================== UPDATE LOS DATOS DEL TECHO PRESUPUESTO =======================================*/
      function update_techo_ptto()
      {
        if($this->input->is_ajax_request() && $this->input->post()){
            $this->form_validation->set_rules('ffofet_id', 'id de la meta', 'required|trim');
            //=========================== mensajes =========================================
            $this->form_validation->set_message('required', 'El campo es es obligatorio');
            /*if ($this->form_validation->run() ) {*/
                $post = $this->input->post();
                $proy_id = $post['proy_id'];
                $fase_id = $post['fase_id'];
                $ffofet_id = $post['ffofet_id'];
                $ff_id = $post['ff_id'];
                $of_id = $post['of_id'];
                $ffofet_monto = $post['ffofet_monto'];
                //================ evitar enviar codigo malicioso ==========
                $proy_id= $this->security->xss_clean($proy_id);
                $fase_id= $this->security->xss_clean($fase_id);
                $ffofet_id= $this->security->xss_clean($ffofet_id);
                $ff_id= $this->security->xss_clean($ff_id);
                $of_id= $this->security->xss_clean($of_id);
                $ffofet_monto= $this->security->xss_clean($ffofet_monto);

                $update_techo = array(
                        'ff_id' => $ff_id,
                        'of_id' => $of_id,
                        'ffofet_monto' => $ffofet_monto,
                        );
                $this->db->where('ffofet_id', $ffofet_id);
                $this->db->update('_ffofet', $update_techo);

                echo 'true';

        }else{
            show_404();
        }
    }

/*================================ ELIMINAR TECHO PREUPUESTARIO X ===============================*/
  function delete_recurso($proy_id,$ptofecg_id,$ffofet_id,$nro)
  {
      $fase = $this->model_faseetapa->get_id_fase($proy_id);
      $lista_fases_gestion=$this->model_faseetapa->list_fases_gestiones($fase[0]['id']);
      foreach ($lista_fases_gestion as $row)
      {
         //  echo "pfec id ".$row['pfec_id']." - ".$row['g_id']."<br>";
          $this->db->where('ptofecg_id', $row['ptofecg_id']);
          $this->db->where('nro', $nro);
          $this->db->delete('_ffofet');
      }

      $this->session->set_flashdata('success','SE ELIMINO CORRECTAMENTE EL RECURSO');
      redirect(site_url("admin") . '/proy/ver_techo_ptto/'.$proy_id.'/'.$ptofecg_id.'/true');
  }
    /*===================================== OBTIENE LOS DATOS DEL PROYECTO =======================================*/
    public function get_proyecto()
    {
        if($this->input->is_ajax_request() && $this->input->post())
        {
            $post = $this->input->post();
            $cod = $post['id_proy'];
            $id = $this->security->xss_clean($cod);
            $dato_proy = $this->model_proyecto->get_id_proyecto($id);
            //caso para modificar el codigo de proyecto y actividades
            foreach($dato_proy as $row){
                $result = array(
                    'proy_id' => $row['proy_id'],
                    "proy_nombre" =>$row['proy_nombre']
                );
            }
            echo json_encode($result);
        }else{
            show_404();
        }
    }
    /*===================================== OBTIENE LOS DATOS DEL PROYECTO =======================================*/
    function rolfun($rol)
    {
      $valor=false;
      for ($i=1; $i <=count($rol) ; $i++) {
        $data = $this->Users_model->get_datos_usuario_roles($this->session->userdata('fun_id'),$rol[$i]);
        if(count($data)!=0){
          $valor=true;
          break;
        }
      }
      return $valor;
    }

    function rolfunn($tp_rol)
    {
      $valor=false;
      $data = $this->Users_model->get_datos_usuario_roles($this->session->userdata('fun_id'),$tp_rol);
      if(count($data)!=0){
        $valor=true;
      }
      return $valor;
    }

    function verif_existente(){
        if($this->input->is_ajax_request())
        {
            $post = $this->input->post();
            $proy_id  = $post['proy_id'];
            $fas_id   = $post['fas_id'];
            $eta_id   = $post['eta_id'];
            $pfec_id  = $post['pfec_id'];
            $variable= $this->model_faseetapa->fase_etapa_existe($proy_id,$fas_id,$eta_id,$pfec_id);
            if($variable == true)
            {
                echo "true"; ///// no existe un CI registrado
            }
            else
            {
                echo "false"; //// existe el CI ya registrado
            }
        }else{
            show_404();
        }
    }
}