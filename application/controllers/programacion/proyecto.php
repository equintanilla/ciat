<?php
define("DOMPDF_ENABLE_REMOTE", true);
define("DOMPDF_TEMP_DIR", "/tmp");
class Proyecto extends CI_Controller {
    public $rol = array('1' => '3','2' => '4','3' => '5');
    public function __construct ()
    {
        parent::__construct();
        if($this->session->userdata('fun_id')!=null){
            $this->load->library('pdf2');
            $this->load->model('menu_modelo');
            $this->load->model('Users_model','',true);
            $this->load->model('programacion/model_faseetapa');
            $this->load->model('programacion/model_proyecto');
            $this->load->model('programacion/model_componente');
            $this->load->model('programacion/model_producto');
            $this->load->model('programacion/model_actividad');
            $this->load->model('mantenimiento/mapertura_programatica');
            $this->load->model('mantenimiento/munidad_organizacional');
            $this->load->model('reportes/model_objetivo');
            $this->gestion = $this->session->userData('gestion'); /// Gestion
            $this->fun_id = $this->session->userData('fun_id'); /// Fun id
            $this->rol_id = $this->session->userData('rol_id'); /// Rol Id
        }else{
            redirect('/','refresh');
        }
    }

    /*===================================== COMBO FUNCIONARIO UNIDAD =======================================*/
    public function combo_funcionario_unidad($accion='')
    {
        $salida="";
        $accion=$_POST["accion"];
        switch ($accion) {
            case 'unidad2':
                $salida="";
                $id_pais=$_POST["elegido"];

                $combog = pg_query('SELECT u.*
          from funcionario f
          Inner Join unidadorganizacional as u On u."uni_id"=f."uni_id"
          where  f."fun_id"='.$id_pais.'');
                $salida.= "<option value=''>".mb_convert_encoding('Seleccione Unidad Ejecutora', 'cp1252', 'UTF-8')."</option>";
                while($sql_p = pg_fetch_row($combog))
                {$salida.= "<option value='".$sql_p[0]."'>".$sql_p[2]."</option>";}

                echo $salida;
                //return $salida;
                break;
        }
    }
    /*===================================== END COMBO FUNCIONARIO UNIDAD =======================================*/


    /*------------------------------------ TECNICO DE UNIDAD EJECUTORA ---------------------------------*/
    public function list_proyectos()
    {
        if($this->rolfunn(3)){
            $enlaces=$this->menu_modelo->get_Modulos(1);
            $data['enlaces'] = $enlaces;
            $data['lista_aper_padres'] = $this->model_proyecto->list_prog();//lista de aperturas padres
            for($i=0;$i<count($enlaces);$i++)
            {
                $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
            }
            $data['subenlaces'] = $subenlaces;
            //load the view
            $data['mod']=1;

            //$data['programas']=$this->list_programas($data['mod']);
            $data['proyectos']=$this->list_programas($data['mod'],1);
            $data['precurrentes']=$this->list_programas($data['mod'],2);
            $data['pnrecurrentes']=$this->list_programas($data['mod'],3);
            $data['operaciones']=$this->list_programas($data['mod'],4);
            $this->load->view('admin/programacion/proy_anual/top/list_proy', $data);
        }
        else{
            redirect('admin/dashboard');
        }
    }

    public function list_programas($mod,$tp)
    {
        $lista_aper_padres = $this->model_proyecto->list_prog();//lista de aperturas padres
        $tabla ='';
        foreach($lista_aper_padres  as $rowa){
            $proyectos=$this->model_proyecto->operaciones($rowa['aper_programa'],1,1,$tp);
            if(count($proyectos)!=0){
                foreach($proyectos  as $row){
                    /*---------- administrador ---------*/
                    if($this->rol_id==1){
                        $tabla.= $this->operacion($row['proy_id'],$mod);
                    }
                    /*----------- responsable ---------*/
                    elseif(count($this->model_proyecto->proyecto_funcionario($row['proy_id']))==1){
                        $tabla.= $this->operacion($row['proy_id'],$mod);
                    }
                    /*------- responsable proceso -----*/
                    elseif (count($this->model_proyecto->proyecto_funcionario_proceso($row['proy_id']))==1) {
                        $tabla.= $this->operacion($row['proy_id'],$mod);
                    }
                }
            }
        }
        return $tabla;
    }
    public function operacion($proy_id,$mod){
        $tabla ='';
        $proyecto=$this->model_proyecto->get_id_proyecto($proy_id);

        $obs='';
        if(count($proyecto)!=0) {
            if ($proyecto[0]['t_obs'] == 2) {
                $obs = 'POA';
            } elseif ($proyecto[0]['t_obs'] == 3) {
                $obs = 'FINANCIERO';
            } elseif ($proyecto[0]['t_obs'] == 4) {
                $obs = 'TECNICO OPERATIVO';
            }
            if ($proyecto[0]['tp_id'] != 1) {
                $color = '#D7FCF9';
            } else {
                $color = '#ffffff';
            }
            $fase = $this->model_faseetapa->get_id_fase($proyecto[0]['proy_id']);
            $tabla .= '<tr bgcolor=' . $color . '>';
            $tabla .= '<td>';
            $tabla .= '<div class="btn-group"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-list"></i> ACCIONES <span class="caret"></span></button><ul class="dropdown-menu" role="menu">';
            if (count($fase) != 0) {
                $tabla .= '<li><a href="javascript:abreVentana(\'' . site_url("admin") . '/dictamen_proyecto/' . $proyecto[0]['proy_id'] . '/' . $fase[0]['pfec_ejecucion'] . '\');" title="REGISTRO DE LA OPERACION"><i class="glyphicon glyphicon-file"></i> Resumen de la operaci&oacute;n en PDF</a></li>';
            } else {
                $tabla .= '<li><a href="javascript:abreVentana(\'' . site_url("admin") . '/dictamen_proyecto/' . $proyecto[0]['proy_id'] . '\');" title="REGISTRO DE LA OPERACION"><i class="glyphicon glyphicon-file"></i> Resumen de la operaci&oacute;n en PDF</a></li>';
            }
            $tabla .= '<li><a href="' . site_url("admin") . '/proy/edit/' . $proyecto[0]['proy_id'] . '/1" title="Editar datos generales"><i class="glyphicon glyphicon-pencil"></i> Editar datos generales</a></li>';
            if (count($fase) != 0) //// tiene fase
            {
                $tabla .= '<li><a href="' . site_url("admin") . '/prog/prog_fisica/' . $mod . '/' . $fase[0]['id'] . '/' . $proyecto[0]['proy_id'] . '" title="PROGRAMACION FISICA"><i class="glyphicon glyphicon-align-left"></i> Programaci&oacute;n F&iacute;sica</a></li>';

                if ($this->model_faseetapa->verif_fase_etapa_gestion($fase[0]['id'], $this->session->userdata("gestion")) != 0) {
                    $fase_gest = $this->model_faseetapa->fase_etapa_gestion($fase[0]['id'], $this->session->userdata("gestion")); ////// datos fase activa presupuesto gestion actual
                    //if($this->model_faseetapa->nro_ffofet($fase_gest[0]['ptofecg_id'])!=0)
                    //{
                    $tabla .= '<li><a href="' . site_url("") . '/prog/ins/' . $proyecto[0]['proy_id'] . '/' . $fase[0]['pfec_ejecucion'] . '" title="PROGRAMACION PRESUPUESTARIA"><i class="glyphicon glyphicon-usd"></i> Programaci&oacute;n Insumos</a></li>';
                    //}
                }

                if ($this->model_proyecto->verif_proy($proyecto[0]['proy_id']) != 0) {
                    $tabla .= '<li><a href="#" data-toggle="modal" data-target="#modal_mod_aper" class="mod_aper" name="' . $proyecto[0]['proy_id'] . '" id="2" title="ASIGNAR LA OPERACION A ANALISTA POA"><i class="glyphicon glyphicon-envelope"></i> Enviar al analista POA</li>';
                }
            }

            if ($this->session->userdata('rol_id') == 1) {
                $tabla .= '<li><a href="#" data-toggle="modal" data-target="#modal_del_ff" class="del_ff" title="ELIMINAR PROYECTO"  name="' . $proyecto[0]['proy_id'] . '"><i class="glyphicon glyphicon-trash"></i> Eliminar</a></li>';
            }
            $tabla .= '</ul></div>';
            $tabla .= '</td>';
            $tabla .= '<td class="text-center">';
            if ($proyecto[0]['aper_proyecto'] != '') {
                $tabla .= '' . $proyecto[0]['aper_programa'] . '' . $proyecto[0]['aper_proyecto'] . '' . $proyecto[0]['aper_actividad'] . '<br><br>';
            } else {
                $tabla .= '<button class="btn btn-danger" title="RE ASIGNAR CATEGORIA PROGRAMATICA PARA LA GESTION ' . $this->session->userdata('gestion') . '"><i class="glyphicon glyphicon-fire"></i>&nbsp;ASIGNAR APERTURA<br>PROGRAM&Aacute;TICA<br>' . $this->session->userdata('gestion') . '</button><br><br>';
            }
            if ($proyecto[0]['t_obs'] != 0 & $proyecto[0]['t_obs'] != 1) {
                $tabla .= '<a data-toggle="modal" data-target="#modal_mod_aper2" class="btn btn-warning mod_aper2" name="' . $proyecto[0]['proy_observacion'] . '" id="' . $proyecto[0]['t_obs'] . '"><i class="glyphicon glyphicon-info-sign"></i> OBSERVADO<br>' . $obs . '</a><br><br>';
            }

            $tabla .= '</td>';
            $tabla .= '<td>' . $proyecto[0]['proy_nombre'] . '</td>';
            $tabla .= '<td>' . $proyecto[0]['tp_tipo'] . '</td>';
            $tabla .= '<td>' . $proyecto[0]['proy_sisin'] . '</td>';
            $tabla .= '<td>' . $proyecto[0]['fun_nombre'] . ' ' . $proyecto[0]['fun_paterno'] . ' ' . $proyecto[0]['fun_materno'] . '</td>';
            $tabla .= '<td>' . $proyecto[0]['ue'] . '</td>';
            $tabla .= '<td>' . $proyecto[0]['ur'] . '</td>';
            if (count($fase) != 0) {
                $nc = $this->model_faseetapa->calcula_nc($fase[0]['pfec_fecha_inicio']); //// calcula nuevo/continuo
                $ap = $this->model_faseetapa->calcula_ap($fase[0]['pfec_fecha_inicio'], $fase[0]['pfec_fecha_fin']);
                $nro_fg_act = $this->model_faseetapa->nro_fasegestion_actual($fase[0]['id'], $this->session->userdata('gestion'));
                $fase_gest = $this->model_faseetapa->fase_etapa_gestion($fase[0]['id'], $this->session->userdata("gestion"));
                $tabla .= '<td>' . $fase[0]['descripcion'] . '</td>';
                $tabla .= '<td>* ' . $fase[0]['fase'] . '<br>* ' . $fase[0]['etapa'] . '</td>';
                $tabla .= '<td>' . $nc . '</td>';
                $tabla .= '<td>' . $ap . '</td>';
                $tabla .= '<td>' . number_format($fase[0]['pfec_ptto_fase'], 2, ',', '.') . ' Bs.</td>';
                $tabla .= '<td>';
                if ($nro_fg_act != 0 && ($fase_gest[0]['estado'] == 1 || $fase_gest[0]['estado'] == 2)) {
                    $tabla .= '' . number_format($fase_gest[0]['pfecg_ppto_total'], 2, ',', '.') . ' Bs.';
                } elseif ($nro_fg_act == 0) {
                    $tabla .= '<font color="red">la gestion no esta en curso</font>';
                }
                $tabla .= '</td>';
                $tabla .= '<td>';
                if ($this->model_faseetapa->verif_fase_etapa_gestion($fase[0]['id'], $this->session->userdata("gestion")) != 0) {
                    if ($this->model_faseetapa->nro_ffofet($fase_gest[0]['ptofecg_id']) != 0) {
                        $techo = $this->model_faseetapa->techo_presupuestario($fase_gest[0]['ptofecg_id']);
                        $tabla .= '' . number_format($techo[0]['suma_techo'], 2, ',', '.') . ' Bs.';
                    } else {
                        $tabla .= "<font color=red>S/T</font>";
                    }
                }
                $tabla .= '</td>';
            } else {
                $tabla .= '<td bgcolor=#efb0b0><font color=red>Sin Fase</font></td>';
                $tabla .= '<td bgcolor=#efb0b0><font color=red>Sin Fase</font></td>';
                $tabla .= '<td bgcolor=#efb0b0><font color=red>Sin Fase</font></td>';
                $tabla .= '<td bgcolor=#efb0b0><font color=red>Sin Fase</font></td>';
                $tabla .= '<td bgcolor=#efb0b0><font color=red>Sin Fase</font></td>';
                $tabla .= '<td bgcolor=#efb0b0><font color=red>Sin Fase</font></td>';
                $tabla .= '<td bgcolor=#efb0b0><font color=red>Sin Fase</font></td>';
            }
            $pr = $this->model_proyecto->prioridad();
            $tabla .= '<td>';
            $tabla .= '<select class="form-control" onchange="doSelectAlert(event,this.value,' . $proyecto[0]['proy_id'] . ');">';
            foreach ($pr as $pr) {
                if ($pr['pr_id'] == $proyecto[0]['proy_pr']) {
                    $tabla .= "<option value=" . $pr['pr_id'] . " selected>" . $pr['pr_desc'] . "</option>";
                } else {
                    $tabla .= "<option value=" . $pr['pr_id'] . ">" . $pr['pr_desc'] . "</option>";
                }
            }
            $tabla .= '</select>';
            $tabla .= '</td>';
            $tabla .= '<td>';
            $prov = $this->model_proyecto->proy_prov($proyecto[0]['proy_id']);
            if (count($prov) != 0) {
                foreach ($prov as $locali) {
                    $tabla .= '<li><b>' . strtoupper($locali['prov_provincia']) . ' </b>';
                    $muni = $this->model_proyecto->provincia_municipio($proyecto[0]['proy_id'], $locali['prov_id']);
                    $tabla .= '(';
                    foreach ($muni as $muni) {
                        $tabla .= '' . $muni['muni_municipio'] . ',';
                    }
                    $tabla .= ')';
                    $tabla .= '</li>';
                }
            }

            $tabla .= '</td>';
            $tabla .= '</tr>';
        }
        return $tabla;
    }
    /*--------------------------------------------------------------------------------------------*/
    /*============== PROYECTOS VALIDADOR POA =================*/
    public function list_proyectos_poa(){
        if($this->rolfunn(4)){
            $enlaces=$this->menu_modelo->get_Modulos(1);
            $data['enlaces'] = $enlaces;
            for($i=0;$i<count($enlaces);$i++){
                $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
            }
            $data['subenlaces'] = $subenlaces;
            $data['lista_aper_padres'] = $this->model_proyecto->list_prog();//lista de aperturas padres
            $data['mod']=1;
            //load the view
            //$data['programas']=$this->list_programas_poa($data['mod']);
            $data['proyectos']=$this->list_programas_poa($data['mod'],1);
            $data['precurrentes']=$this->list_programas_poa($data['mod'],2);
            $data['pnrecurrentes']=$this->list_programas_poa($data['mod'],3);
            $data['operaciones']=$this->list_programas_poa($data['mod'],4);
            $this->load->view('admin/programacion/proy_anual/vpoa/list_proy', $data);
        }
        else{
            redirect('admin/dashboard');
        }
    }

    public function list_programas_poa($mod,$tp){
        $lista_aper_padres = $this->model_proyecto->list_prog();//lista de aperturas padres
        $tabla ='';
        foreach($lista_aper_padres  as $rowa){
            $proyectos=$this->model_proyecto->list_proyectos($rowa['aper_programa'],2,2,$tp);
            if(count($proyectos)!=0){
                $tabla .='<tr bgcolor="#99DDF0" title="CATEGORIA PROGRAMATICA">';
                $tabla .='<td></td>';
                $tabla .='<td><center>' .$rowa['aper_programa'].''.$rowa['aper_proyecto'].''.$rowa['aper_actividad'].'</center></td>';
                $tabla .='<td>'.$rowa['aper_descripcion'].'</td>';
                $tabla .='<td>'.$rowa['aper_sisin'].'</td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='</tr>';
                foreach($proyectos  as $row){
                    if($row['tp_id']!=1){$color='#D7FCF9';}else{$color='#ffffff';}
                    $fase = $this->model_faseetapa->get_id_fase($row['proy_id']);
                    $fase_gest = $this->model_faseetapa->fase_etapa_gestion($fase[0]['id'],$this->session->userdata("gestion"));
                    $tabla .= '<tr bgcolor='.$color.' title="'.$row['tp_tipo'].'">';
                    $tabla .= '<td class="text-left fila">';
                    $tabla .= '<div class="btn-group"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-list"></i> ACCIONES <span class="caret"></span></button><ul class="dropdown-menu" role="menu">';
                    $tabla .= '<li><a href="javascript:abreVentana(\''.site_url("admin").'/dictamen_proyecto/'.$row['proy_id'].'/'.$fase[0]['pfec_ejecucion'].'\');" title="REGISTRO DE LA OPERACION"><i class="glyphicon glyphicon-file"></i> Resumen de la operación en PDF</a></li>';
                    $tabla .= '<li><a href="'.site_url("admin").'/proy/edit/'.$row['proy_id'].'/1" title="Editar datos generales"><i class="glyphicon glyphicon-pencil"></i> Editar datos generales</a></li>';
                    $tabla .= '<li><a href="'.site_url("admin").'/prog/prog_fisica/'.$mod.'/'.$fase[0]['id'].'/'.$row['proy_id'].'" title="PROGRAMACION FISICA"><i class="glyphicon glyphicon-align-left"></i> Programaci&oacute;n F&iacute;sica</a></li>';
                   
                    $tabla .= '<li><a href="#" data-toggle="modal" data-target="#modal_mod_aper" class="mod_aper" name="'.$row['proy_id'].'" id="4" title="APROBAR OPERACI&Oacute;N"><i class="glyphicon glyphicon-ok"></i> Aprobar Operaci&oacute;n</a></li>';
                    $tabla .= '<li><a data-toggle="modal" data-target="#modal_mod_aper2" class="mod_aper2" name="'.$row['proy_id'].'" id="2" id="OBSERVAR OPERACI&Oacute;N"><i class="glyphicon glyphicon-eye-open"></i> Observar Operaci&oacute;n</a></li>';
                    if($this->session->userdata('rol_id')==1)
                    {
                        $tabla .='<li><a href="#" data-toggle="modal" data-target="#modal_del_ff" class="del_ff" title="ELIMINAR PROYECTO"  name="'.$row['proy_id'].'"><i class="glyphicon glyphicon-trash"></i> Eliminar</a></li>';
                    }
                    $tabla .= '</ul></div>';
                    $tabla .= '</td>';
//              $tabla .= '<td>';
                    //            $tabla .= 'XXX';
                    //          $tabla .= '</td>';
                    $tabla .= '<td>';
                    if($row['aper_proyecto']!=''){
                        $tabla .= '<center>'.$row['aper_programa'].''.$row['aper_proyecto'].''.$row['aper_actividad'].'</center>';
                    }
                    else{
                        $tabla .=  '<center><font color="red"><button class="btn btn-danger" title="RE ASIGNAR CATEGORIA PROGRAMATICA PARA LA GESTION '.$this->session->userdata('gestion').'">ASIGNAR<br>APERTURA<br>PROGRAM&Aacute;TICA<br>'.$this->session->userdata('gestion').'</button></font></center>';
                    }
                    $tabla .= '</td>';
                    $tabla .= '<td>'.$row['proy_nombre'].'</td>';
                    $tabla .= '<td>'.$row['tp_tipo'].'</td>';
                    $tabla .= '<td>'.$row['proy_sisin'].'</td>';
                    $tabla .= '<td class="analista">'.$row['fun_nombre'].' '.$row['fun_paterno'].' '.$row['fun_materno'].'</td>';
                    $tabla .= '<td>'.$row['ue'].'</td>';
                    $tabla .= '<td>'.$row['ur'].'</td>';

                    $nc=$this->model_faseetapa->calcula_nc($fase[0]['pfec_fecha_inicio']); //// calcula nuevo/continuo
                    $ap=$this->model_faseetapa->calcula_ap($fase[0]['pfec_fecha_inicio'],$fase[0]['pfec_fecha_fin']);
                    $nro_fg_act = $this->model_faseetapa->nro_fasegestion_actual($fase[0]['id'],$this->session->userdata('gestion'));
                    $fase_gest = $this->model_faseetapa->fase_etapa_gestion($fase[0]['id'],$this->session->userdata("gestion"));
                    $tabla .='<td>'.$fase[0]['descripcion'].'</td>';
                    $tabla .='<td>* '.$fase[0]['fase'].'<br>* '.$fase[0]['etapa'].'</td>';
                    $tabla .='<td>'.$nc.'</td>';
                    $tabla .='<td>'.$ap.'</td>';
                    $tabla .='<td>'.number_format($fase[0]['pfec_ptto_fase'], 2, ',', '.').' Bs.</td>';
                    $tabla .='<td>';
                    if($nro_fg_act!=0 && ($fase_gest[0]['estado']==1 || $fase_gest[0]['estado']==2))
                    {
                        $tabla .=''.number_format($fase_gest[0]['pfecg_ppto_total'], 2, ',', '.').' Bs.';
                    }
                    elseif ($nro_fg_act==0) {
                        $tabla .= '<font color="red">la gestion no esta en curso</font>';
                    }
                    $tabla .='</td>';
                    $tabla .='<td>';
                    if($this->model_faseetapa->verif_fase_etapa_gestion($fase[0]['id'],$this->session->userdata("gestion"))!=0)
                    {
                        if($this->model_faseetapa->nro_ffofet($fase_gest[0]['ptofecg_id'])!=0)
                        {
                            $techo=$this->model_faseetapa->techo_presupuestario($fase_gest[0]['ptofecg_id']);
                            $tabla .= ''.number_format($techo[0]['suma_techo'], 2, ',', '.').' Bs.';
                        }
                        else{$tabla .= "<font color=red>S/T</font>";}
                    }
                    $tabla .='</td>';

                    $tabla .='<td>';
                    $prov=$this->model_proyecto->proy_prov($row['proy_id']);
                    if(count($prov)!=0){
                        foreach($prov as $locali)
                        {
                            $tabla .='<li><b>'.strtoupper($locali['prov_provincia']).' </b>';
                            $muni=$this->model_proyecto->provincia_municipio($row['proy_id'],$locali['prov_id']);
                            $tabla .='(';
                            foreach($muni as $muni)
                            {
                                $tabla .=''.$muni['muni_municipio'].',';
                            }
                            $tabla .=')';
                            $tabla .='</li>';
                        }
                    }

                    $tabla .='</td>';
                    $tabla .= '</tr>';
                }
            }
        }

        /*return $tabla;
        $lista_aper_padres = $this->model_proyecto->list_prog();//lista de aperturas padres
        $tabla ='';
        foreach($lista_aper_padres  as $rowa){
            $proyectos=$this->model_proyecto->list_proyectos($rowa['aper_programa'],2,2,$tp);
            if(count($proyectos)!=0){
                $tabla .='<tr bgcolor="#99DDF0">';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td><center>' .$rowa['aper_programa'].''.$rowa['aper_proyecto'].''.$rowa['aper_actividad'].'</center></td>';
                $tabla .='<td>'.$rowa['aper_descripcion'].'</td>';
                $tabla .='<td>'.$rowa['aper_sisin'].'</td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='</tr>';
                foreach($proyectos  as $row)
                {
                    if($row['tp_id']!=1){$color='#D7FCF9';}else{$color='#ffffff';}
                    $fase = $this->model_faseetapa->get_id_fase($row['proy_id']);
                    $fase_gest = $this->model_faseetapa->fase_etapa_gestion($fase[0]['id'],$this->session->userdata("gestion"));
                    $tabla .= '<tr bgcolor='.$color.'>';
                    $tabla .= '<td>';
                    $tabla .= '<center><a href="javascript:abreVentana(\''.site_url("admin").'/dictamen_proyecto/'.$row['proy_id'].'/'.$fase[0]['pfec_ejecucion'].'\');" title="REGISTRO DE LA OPERACION"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="35" HEIGHT="35"/><br>Registro</a></center>';
                    if(count($this->model_faseetapa->fase_etapa_gestion($fase[0]['id'],$this->session->userdata("gestion")))!=0){
                        $tabla .= '<center><a href="#" data-toggle="modal" data-target="#modal_mod_aper" class="btn btn-xs mod_aper" name="'.$row['proy_id'].'" id="4" title="APROBAR OPERACI&Oacute;N"> <img src="' . base_url() . 'assets/ifinal/ok1.jpg" WIDTH="35" HEIGHT="35"/><br>Aprobar Operaci&oacute;n</a></center>';
                    }
                    $tabla .= '<center><a data-toggle="modal" data-target="#modal_mod_aper2" class="btn btn-xs mod_aper2" name="'.$row['proy_id'].'" id="2" id="OBSERVAR OPERACI&Oacute;N"><img src="' . base_url() . 'assets/ifinal/archivo1.png" /><br>Observar<br>Operaci&oacute;n</a></center>';
                    $tabla .= '</td>';
                    $tabla .= '<td>';
                    $tabla .= '<center><a href="'.site_url("admin").'/proy/edit/'.$row['proy_id'].'/1" title="MODIFICAR OPERACION"><img src="' . base_url() . 'assets/ifinal/modificar.png" WIDTH="35" HEIGHT="35"/><br>Modificar</a></center>';
                    $tabla .= '<center><a href="'.site_url("admin").'/prog/prog_fisica/'.$mod.'/'.$fase[0]['id'].'/'.$row['proy_id'].'" title="PROGRAMACION FISICA"><img src="' . base_url() . 'assets/ifinal/bien.png" WIDTH="30" HEIGHT="30"/><br>Programaci&oacute;n F&iacute;sica</a></center>';
                    if(count($this->model_faseetapa->fase_etapa_gestion($fase[0]['id'],$this->session->userdata("gestion")))!=0)
                    {
                        $tabla .= '<center><a href="'.site_url("").'/prog/ins/'.$row['proy_id'].'/'.$fase[0]['pfec_ejecucion'].'" title="PROGRAMACION PRESUPUESTARIA"><img src="' . base_url() . 'assets/ifinal/insumo.png" WIDTH="35" HEIGHT="35"/><br>Programaci&oacute;n Insumos</a></center>';
                    }
                    if($this->session->userdata('rol_id')==1)
                    {
                        $tabla .= '<center><a href="'.site_url("admin").'/proy/delete/1/'.$row['proy_id'].'" title="ELIMINAR OPERACION" onclick="return confirmar()"><img src="' . base_url() . 'assets/ifinal/eliminar.png" WIDTH="35" HEIGHT="35"/><br>Eliminar</a></center>';
                    }
                    $tabla .= '</td>';
                    $tabla .= '<td>';
                    if($row['aper_proyecto']!=''){
                        $tabla .= '<center>'.$row['aper_programa'].''.$row['aper_proyecto'].''.$row['aper_actividad'].'</center>';
                    }
                    else{
                        $tabla .=  '<center><font color="red"><button class="btn btn-danger" title="RE ASIGNAR CATEGORIA PROGRAMATICA PARA LA GESTION '.$this->session->userdata('gestion').'">ASIGNAR<br>APERTURA<br>PROGRAM&Aacute;TICA<br>'.$this->session->userdata('gestion').'</button></font></center>';
                    }
                    $tabla .= '</td>';
                    $tabla .= '<td>'.$row['proy_nombre'].'</td>';
                    $tabla .= '<td>'.$row['tp_tipo'].'</td>';
                    $tabla .= '<td>'.$row['proy_sisin'].'</td>';
                    $tabla .= '<td>'.$row['fun_nombre'].' '.$row['fun_paterno'].' '.$row['fun_materno'].'</td>';
                    $tabla .= '<td>'.$row['ue'].'</td>';
                    $tabla .= '<td>'.$row['ur'].'</td>';

                    $nc=$this->model_faseetapa->calcula_nc($fase[0]['pfec_fecha_inicio']); //// calcula nuevo/continuo
                    $ap=$this->model_faseetapa->calcula_ap($fase[0]['pfec_fecha_inicio'],$fase[0]['pfec_fecha_fin']);
                    $nro_fg_act = $this->model_faseetapa->nro_fasegestion_actual($fase[0]['id'],$this->session->userdata('gestion'));
                    $fase_gest = $this->model_faseetapa->fase_etapa_gestion($fase[0]['id'],$this->session->userdata("gestion"));
                    $tabla .='<td>'.$fase[0]['descripcion'].'</td>';
                    $tabla .='<td>* '.$fase[0]['fase'].'<br>* '.$fase[0]['etapa'].'</td>';
                    $tabla .='<td>'.$nc.'</td>';
                    $tabla .='<td>'.$ap.'</td>';
                    $tabla .='<td>'.number_format($fase[0]['pfec_ptto_fase'], 2, ',', '.').' Bs.</td>';
                    $tabla .='<td>';
                    if($nro_fg_act!=0 && ($fase_gest[0]['estado']==1 || $fase_gest[0]['estado']==2))
                    {
                        $tabla .=''.number_format($fase_gest[0]['pfecg_ppto_total'], 2, ',', '.').' Bs.';
                    }
                    elseif ($nro_fg_act==0) {
                        $tabla .= '<font color="red">la gestion no esta en curso</font>';
                    }
                    $tabla .='</td>';
                    $tabla .='<td>';
                    if($this->model_faseetapa->verif_fase_etapa_gestion($fase[0]['id'],$this->session->userdata("gestion"))!=0)
                    {
                        if($this->model_faseetapa->nro_ffofet($fase_gest[0]['ptofecg_id'])!=0)
                        {
                            $techo=$this->model_faseetapa->techo_presupuestario($fase_gest[0]['ptofecg_id']);
                            $tabla .= ''.number_format($techo[0]['suma_techo'], 2, ',', '.').' Bs.';
                        }
                        else{$tabla .= "<font color=red>S/T</font>";}
                    }
                    $tabla .='</td>';

                    $tabla .='<td>';
                    $loc=$this->model_proyecto->localizacion($row['proy_id']);
                    if(count($loc)!=0){
                        $tabla .='<table class="table table-bordered">';
                        $tabla .='<tr bgcolor="#474544">';
                        $tabla .='<th>PROVINCIA</th><th>MUNICIPIO</th>';
                        $tabla .='</tr>';
                        foreach($loc as $locali)
                        {
                            $tabla .='<tr>';
                            $tabla .='<td>'.$locali['prov_provincia'].'</td>';
                            $tabla .='<td>'.$locali['muni_municipio'].'</td>';
                            $tabla .='</tr>';
                        }

                        $tabla .='</table>';
                    }

                    $tabla .='</td>';
                    $tabla .= '</tr>';
                }
            }
        }*/

        return $tabla;
    }
    /*========== PROYECTOS VALIDADOR FINANCIERO ===========*/
    public function list_proyectos_financiero(){
        if($this->rolfunn(5)){
            $enlaces=$this->menu_modelo->get_Modulos(1);
            $data['enlaces'] = $enlaces;
            for($i=0;$i<count($enlaces);$i++)
            {
                $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
            }
            $data['subenlaces'] = $subenlaces;
            $data['lista_aper_padres'] = $this->model_proyecto->list_prog();//lista de aperturas padres
            $data['mod']=1;
            //load the view
            //$data['programas']=$this->list_programas_fin($data['mod']);
            $data['proyectos']=$this->list_programas_fin($data['mod'],1);
            $data['precurrentes']=$this->list_programas_fin($data['mod'],2);
            $data['pnrecurrentes']=$this->list_programas_fin($data['mod'],3);
            $data['operaciones']=$this->list_programas_fin($data['mod'],4);
            $this->load->view('admin/programacion/proy_anual/vfin/list_proy', $data);
        }
        else{
            redirect('admin/dashboard');
        }
    }

    public function list_programas_fin($mod,$tp){
        $lista_aper_padres = $this->model_proyecto->list_prog();//lista de aperturas padres
        $tabla ='';
        foreach($lista_aper_padres  as $rowa){
            $proyectos=$this->model_proyecto->list_proyectos($rowa['aper_programa'],1,3,$tp);
            if(count($proyectos)!=0){
                $tabla .='<tr bgcolor="#99DDF0" title="CATEGORIA PROGRAMATICA PADRE">';
                $tabla .='<td></td>';
                $tabla .='<td><center>' .$rowa['aper_programa'].''.$rowa['aper_proyecto'].''.$rowa['aper_actividad'].'</center></td>';
                $tabla .='<td>'.$rowa['aper_descripcion'].'</td>';
                $tabla .='<td>'.$rowa['aper_sisin'].'</td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='</tr>';
                foreach($proyectos  as $row){
                    if($row['tp_id']!=1){$color='#D7FCF9';}else{$color='#ffffff';}
                    $fase = $this->model_faseetapa->get_id_fase($row['proy_id']);
                    $nro_f = $this->model_faseetapa->nro_fase($row['proy_id']); //// nro de fases
                    $tabla .= '<tr title="'.$row['tp_tipo'].'">';
                    $tabla .= '<td>';
                    $tabla .= '</td>';
                    $tabla .= '<td>';
                    if($row['aper_proyecto']!=''){
                        $tabla .= '<center>'.$row['aper_programa'].''.$row['aper_proyecto'].''.$row['aper_actividad'].'</center>';
                    }
                    else{
                        $tabla .=  '<center><font color="red"><button class="btn btn-danger" title="RE ASIGNAR CATEGORIA PROGRAMATICA PARA LA GESTION '.$this->session->userdata('gestion').'">ASIGNAR<br>APERTURA<br>PROGRAM&Aacute;TICA<br>'.$this->session->userdata('gestion').'</button></font></center>';
                    }
                    $tabla .= '</td>';
                    $tabla .= '<td>'.$row['proy_nombre'].'</td>';
                    $tabla .= '<td>'.$row['tp_tipo'].'</td>';
                    $tabla .= '<td>'.$row['proy_sisin'].'</td>';
                    $tabla .= '<td>'.$row['fun_nombre'].' '.$row['fun_paterno'].' '.$row['fun_materno'].'</td>';
                    $tabla .= '<td>'.$row['ue'].'</td>';
                    $tabla .= '<td>'.$row['ur'].'</td>';

                    if(count($fase)!=0){
                        $nc=$this->model_faseetapa->calcula_nc($fase[0]['pfec_fecha_inicio']); //// calcula nuevo/continuo
                        $ap=$this->model_faseetapa->calcula_ap($fase[0]['pfec_fecha_inicio'],$fase[0]['pfec_fecha_fin']);
                        $nro_fg_act = $this->model_faseetapa->nro_fasegestion_actual($fase[0]['id'],$this->session->userdata('gestion'));
                        $fase_gest = $this->model_faseetapa->fase_etapa_gestion($fase[0]['id'],$this->session->userdata("gestion"));
                        $tabla .='<td>* '.$fase[0]['fase'].'<br>* '.$fase[0]['etapa'].'</td>';
                        $tabla .='<td>'.$nc.'</td>';
                        $tabla .='<td>'.$ap.'</td>';
                        $tabla .='<td>'.number_format($fase[0]['pfec_ptto_fase'], 2, ',', '.').' Bs.</td>';
                        $tabla .='<td>';
                        if($nro_fg_act!=0 && ($fase_gest[0]['estado']==1 || $fase_gest[0]['estado']==2))
                        {
                            $tabla .=''.number_format($fase_gest[0]['pfecg_ppto_total'], 2, ',', '.').' Bs.';
                        }
                        elseif ($nro_fg_act==0) {
                            $tabla .= '<font color="red">la gestion no esta en curso</font>';
                        }
                        $tabla .='</td>';
                        $tabla .='<td>';
                        if($this->model_faseetapa->verif_fase_etapa_gestion($fase[0]['id'],$this->session->userdata("gestion"))!=0)
                        {
                            if($this->model_faseetapa->nro_ffofet($fase_gest[0]['ptofecg_id'])!=0)
                            {
                                $techo=$this->model_faseetapa->techo_presupuestario($fase_gest[0]['ptofecg_id']);
                                $tabla .= ''.number_format($techo[0]['suma_techo'], 2, ',', '.').' Bs.';
                            }
                            else{$tabla .= "<font color=red>S/T</font>";}
                        }
                        $tabla .='</td>';
                    }
                    else{
                        $tabla .='<td bgcolor=#efb0b0><font color=red>Sin Fase</font></td>';
                        $tabla .='<td bgcolor=#efb0b0><font color=red>Sin Fase</font></td>';
                        $tabla .='<td bgcolor=#efb0b0><font color=red>Sin Fase</font></td>';
                        $tabla .='<td bgcolor=#efb0b0><font color=red>Sin Fase</font></td>';
                        $tabla .='<td bgcolor=#efb0b0><font color=red>Sin Fase</font></td>';
                        $tabla .='<td bgcolor=#efb0b0><font color=red>Sin Fase</font></td>';
                    }

                    $tabla .='<td>';
                    $prov=$this->model_proyecto->proy_prov($row['proy_id']);
                    if(count($prov)!=0){
                        foreach($prov as $locali)
                        {
                            $tabla .='<li><b>'.strtoupper($locali['prov_provincia']).' </b>';
                            $muni=$this->model_proyecto->provincia_municipio($row['proy_id'],$locali['prov_id']);
                            $tabla .='(';
                            foreach($muni as $muni)
                            {
                                $tabla .=''.$muni['muni_municipio'].',';
                            }
                            $tabla .=')';
                            $tabla .='</li>';
                        }
                    }

                    $tabla .='</td>';
                    $tabla .= '</tr>';
                }
            }
        }

        return $tabla;
    }
    /*===================================== LISTA DE MIS ACCIONES POR UNIDAD ==================================*/
    public function mis_acciones()
    {
        if($this->rolfun($this->rol)){
            $enlaces=$this->menu_modelo->get_Modulos(1);
            $data['enlaces'] = $enlaces;
            for($i=0;$i<count($enlaces);$i++)
            {
                $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
            }
            $data['subenlaces'] = $subenlaces;
            $data['mod']=1;
            $data['lista_aper_padres'] = $this->model_proyecto->list_prog();//lista de aperturas padres
            //load the view
            $data['programas']=$this->list_programas_acciones();
            $this->load->view('admin/programacion/proy_anual/mis_acciones/list_proy', $data);
        }
        else{
            redirect('admin/dashboard');
        }
    }

    public function list_programas_acciones()
    {
        $lista_aper_padres = $this->model_proyecto->list_prog();//lista de aperturas padres
        $tabla ='';
        foreach($lista_aper_padres  as $rowa)
        {
            $proyectos=$this->model_proyecto->proy_actividades($rowa['aper_programa'],$this->session->userdata('gestion'));
            if(count($proyectos)!=0)
            {
                $tabla .='<tr bgcolor="#99DDF0">';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td><center>' .$rowa['aper_programa'].''.$rowa['aper_proyecto'].''.$rowa['aper_actividad'].'</center></td>';
                $tabla .='<td>'.$rowa['aper_descripcion'].'</td>';
                $tabla .='<td>'.$rowa['aper_sisin'].'</td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='</tr>';
                foreach($proyectos  as $row)
                {
                    if($row['t_obs']==2){$obs='POA';}
                    elseif($row['t_obs']==3){$obs='FINANCIERO';}
                    elseif($row['t_obs']==4){$obs='TECNICO OPERATIVO';}
                    if($row['tp_id']!=1){$color='#D7FCF9';}else{$color='#ffffff';}
                    $fase = $this->model_faseetapa->get_id_fase($row['proy_id']);
                    $tabla .= '<tr bgcolor='.$color.'>';
                    $tabla .= '<td>';
                    if(count($fase)!=0)
                    {
                        $tabla .= '<center><a href="javascript:abreVentana(\''.site_url("admin").'/dictamen_proyecto/'.$row['proy_id'].'/'.$fase[0]['pfec_ejecucion'].'\');" title="REGISTRO DE LA OPERACION"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="35" HEIGHT="35"/><br>Registro</a></center>';
                    }
                    else
                    {
                        $tabla .= '<center><a href="javascript:abreVentana(\''.site_url("admin").'/dictamen_proyecto/'.$row['proy_id'].'\');" title="REGISTRO DE LA OPERACION"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="35" HEIGHT="35"/><br>Registro</a></center>';
                    }
                    $tabla .= '</td>';
                    $tabla .= '<td>';
                    $tabla .= '<center><a href="'.site_url("admin").'/proy/edit/'.$row['proy_id'].'/1" title="MODIFICAR OPERACION"><img src="' . base_url() . 'assets/ifinal/modificar.png" WIDTH="35" HEIGHT="35"/><br>Modificar</a></center>';
                    if(count($fase)!=0) //// tiene fase
                    {


                        if($this->model_faseetapa->verif_fase_etapa_gestion($fase[0]['id'],$this->session->userdata("gestion"))!=0)
                        { $fase_gest = $this->model_faseetapa->fase_etapa_gestion($fase[0]['id'],$this->session->userdata("gestion")); ////// datos fase activa presupuesto gestion actual
                            if($this->model_faseetapa->nro_ffofet($fase_gest[0]['ptofecg_id'])!=0)
                            {
                                $tabla .= '<center><a href="'.site_url("").'/prog/ins/'.$row['proy_id'].'/'.$fase[0]['pfec_ejecucion'].'" title="PROGRAMACION PRESUPUESTARIA"><img src="' . base_url() . 'assets/ifinal/insumo.png" WIDTH="35" HEIGHT="35"/><br>Programaci&oacute;n Insumos</a></center>';
                            }
                        }

                        if($this->model_proyecto->verif_proy($row['proy_id'])!=0)
                        {
                            $tabla .= '<center><a href="#" data-toggle="modal" data-target="#modal_mod_aper" class="btn btn-xs mod_aper" name="'.$row['proy_id'].'" id="2" title="ASIGNAR LA OPERACION A ANALISTA POA"><img src="' . base_url() . 'assets/ifinal/ok1.jpg" WIDTH="35" HEIGHT="30"/><br><font size=1>Asignar Operaci&oacute;n</font></center>';
                        }
                    }

                    if($this->session->userdata('rol_id')==1)
                    {
                        $tabla .='<center><a href="#" data-toggle="modal" data-target="#modal_del_ff" class="btn btn-xs del_ff" title="ELIMINAR PROYECTO"  name="'.$row['proy_id'].'"><img src="'.base_url().'assets/ifinal/eliminar.png" WIDTH="35" HEIGHT="35"/><br>Eliminar</a></center>';
                    }

                    $tabla .= '</td>';
                    $tabla .= '<td>';
                    if($row['aper_proyecto']!='')
                    {
                        $tabla .= '<center>'.$row['aper_programa'].''.$row['aper_proyecto'].''.$row['aper_actividad'].'</center>';
                    }
                    else
                    {
                        $tabla .=  '<center><font color="red"><b>Re asignar apertura Programatica</b></font></center>';
                    }
                    if($row['t_obs']!=0 & $row['t_obs']!=1){
                        $tabla .=  '<br><center><a data-toggle="modal" data-target="#modal_mod_aper2" class="btn btn-xs mod_aper2" name="'.$row['proy_observacion'].'" id="'.$row['t_obs'].'"><img src="'.base_url().'assets/ifinal/1.png" WIDTH="35" HEIGHT="35"/></a><br><font color="red" size=1>'.$obs.'</font></center>';
                    }

                    $tabla .= '</td>';
                    $tabla .= '<td>'.$row['proy_nombre'].'</td>';
                    $tabla .= '<td>'.$row['tp_tipo'].'</td>';
                    $tabla .= '<td>'.$row['proy_sisin'].'</td>';
                    $tabla .= '<td>'.$row['fun_nombre'].' '.$row['fun_paterno'].' '.$row['fun_materno'].'</td>';
                    $tabla .= '<td>'.$row['ue'].'</td>';
                    $tabla .= '<td>'.$row['ur'].'</td>';
                    if(count($fase)!=0){
                        $nc=$this->model_faseetapa->calcula_nc($fase[0]['pfec_fecha_inicio']); //// calcula nuevo/continuo
                        $ap=$this->model_faseetapa->calcula_ap($fase[0]['pfec_fecha_inicio'],$fase[0]['pfec_fecha_fin']);
                        $nro_fg_act = $this->model_faseetapa->nro_fasegestion_actual($fase[0]['id'],$this->session->userdata('gestion'));
                        $fase_gest = $this->model_faseetapa->fase_etapa_gestion($fase[0]['id'],$this->session->userdata("gestion"));
                        $tabla .='<td>'.$fase[0]['descripcion'].'</td>';
                        $tabla .='<td>* '.$fase[0]['fase'].'<br>* '.$fase[0]['etapa'].'</td>';
                        $tabla .='<td>'.$nc.'</td>';
                        $tabla .='<td>'.$ap.'</td>';
                        $tabla .='<td>'.number_format($fase[0]['pfec_ptto_fase'], 2, ',', '.').' Bs.</td>';
                        $tabla .='<td>';
                        if($nro_fg_act!=0 && ($fase_gest[0]['estado']==1 || $fase_gest[0]['estado']==2))
                        {
                            $tabla .=''.number_format($fase_gest[0]['pfecg_ppto_total'], 2, ',', '.').' Bs.';
                        }
                        elseif ($nro_fg_act==0) {
                            $tabla .= '<font color="red">la gestion no esta en curso</font>';
                        }
                        $tabla .='</td>';
                        $tabla .='<td>';
                        if($this->model_faseetapa->verif_fase_etapa_gestion($fase[0]['id'],$this->session->userdata("gestion"))!=0)
                        {
                            if($this->model_faseetapa->nro_ffofet($fase_gest[0]['ptofecg_id'])!=0)
                            {
                                $techo=$this->model_faseetapa->techo_presupuestario($fase_gest[0]['ptofecg_id']);
                                $tabla .= ''.number_format($techo[0]['suma_techo'], 2, ',', '.').' Bs.';
                            }
                            else{$tabla .= "<font color=red>S/T</font>";}
                        }
                        $tabla .='</td>';
                    }
                    else{
                        $tabla .='<td bgcolor=#efb0b0><font color=red>Sin Fase</font></td>';
                        $tabla .='<td bgcolor=#efb0b0><font color=red>Sin Fase</font></td>';
                        $tabla .='<td bgcolor=#efb0b0><font color=red>Sin Fase</font></td>';
                        $tabla .='<td bgcolor=#efb0b0><font color=red>Sin Fase</font></td>';
                        $tabla .='<td bgcolor=#efb0b0><font color=red>Sin Fase</font></td>';
                        $tabla .='<td bgcolor=#efb0b0><font color=red>Sin Fase</font></td>';
                        $tabla .='<td bgcolor=#efb0b0><font color=red>Sin Fase</font></td>';
                    }
                    $pr=$this->model_proyecto->prioridad();
                    $tabla .='<td>';
                    $tabla .='<select class="form-control" onchange="doSelectAlert(event,this.value,'.$row['proy_id'].');">';
                    foreach($pr as $pr)
                    {
                        if($pr['pr_id']==$row['proy_pr'])
                        {
                            $tabla .="<option value=".$pr['pr_id']." selected>".$pr['pr_desc']."</option>";
                        }
                        else{
                            $tabla .="<option value=".$pr['pr_id'].">".$pr['pr_desc']."</option>";
                        }
                    }
                    $tabla .='</select>';
                    $tabla .='</td>';
                    $tabla .='<td>';
                    $prov=$this->model_proyecto->proy_prov($row['proy_id']);
                    if(count($prov)!=0){
                        foreach($prov as $locali)
                        {
                            $tabla .='<li><b>'.strtoupper($locali['prov_provincia']).' </b>';
                            $muni=$this->model_proyecto->provincia_municipio($row['proy_id'],$locali['prov_id']);
                            $tabla .='(';
                            foreach($muni as $muni)
                            {
                                $tabla .=''.$muni['muni_municipio'].',';
                            }
                            $tabla .=')';
                            $tabla .='</li>';
                        }
                    }

                    $tabla .='</td>';
                    $tabla .= '</tr>';
                }
            }
        }
        return $tabla;
    }
    /*===================================== PROYECTOS APROBADOS ===========================================*/
    public function list_proyectos_aprobados()
    {
        if($this->rolfun($this->rol)){
            $enlaces=$this->menu_modelo->get_Modulos(1);
            $data['enlaces'] = $enlaces;
            for($i=0;$i<count($enlaces);$i++)
            {
                $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
            }
            $data['subenlaces'] = $subenlaces;
            $data['lista_aper_padres'] = $this->model_proyecto->list_prog();//lista de aperturas padres
            $data['mod']=1;
            //load the view
            $data['proyectos']=$this->mis_operaciones_aprobadas($data['mod'],1);
            $data['precurrentes']=$this->mis_operaciones_aprobadas($data['mod'],2);
            $data['pnrecurrentes']=$this->mis_operaciones_aprobadas($data['mod'],3);
            $data['operaciones']=$this->mis_operaciones_aprobadas($data['mod'],4);
            $this->load->view('admin/programacion/proy_anual/vfin/list_proy_ok', $data);
        }
        else{
            redirect('admin/dashboard');
        }
    }
    public function mis_operaciones_aprobadas($mod,$tp)
    {
        $lista_aper_padres = $this->model_proyecto->list_prog();//lista de aperturas padres
        $tabla ='';
        $nro=1;
        foreach($lista_aper_padres  as $rowa)
        {
            $proyectos=$this->model_proyecto->list_proyectos($rowa['aper_programa'],4,1,$tp);
            if(count($proyectos)!=0)
            {
                $tabla .='<tr bgcolor="#99DDF0">';
                $tabla .='<td></td>';
                $tabla .='<td><center>' .$rowa['aper_programa'].''.$rowa['aper_proyecto'].''.$rowa['aper_actividad'].'</center></td>';
                $tabla .='<td>'.$rowa['aper_descripcion'].'</td>';
                $tabla .='<td>'.$rowa['aper_sisin'].'</td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='<td></td>';
                $tabla .='</tr>';
                foreach($proyectos  as $row)
                {
                    if($row['tp_id']!=1){$color='#D7FCF9';}else{$color='#ffffff';}
                    $fase = $this->model_faseetapa->get_id_fase($row['proy_id']);
                    $tabla .= '<tr bgcolor='.$color.'>';
                    $tabla .= '<td>'.$nro.'';
                    $tabla .= '<center><a href="javascript:abreVentana(\''.site_url("admin").'/dictamen_proyecto/'.$row['proy_id'].'/'.$fase[0]['pfec_ejecucion'].'\');" title="REGISTRO DE LA OPERACION"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="35" HEIGHT="35"/><br>Registro</a></center>';
                    if($this->session->userdata('rol_id')==1){
                        $tabla .= '<center><a data-toggle="modal" data-target="#modal_mod_aper2" class="btn btn-xs mod_aper2" name="'.$row['proy_id'].'" id="3" id="OBSERVAR OPERACI&Oacute;N"><img src="' . base_url() . 'assets/ifinal/archivo1.png"/><font size=1><br>Observar<br>Operaci&oacute;n</a></font></center>';
                    }

                    if(count($fase)!=0){
                        $tabla .='<center>
                          <a href="" class="form_pond" data-toggle="modal" data-target="#' . $nro . '">
                            <img src="' . base_url() . 'assets/ifinal/doc.jpg" width="35" height="35" class="img-responsive" title="REPORTES"><br>Mis Reportes
                          </a>
                          </center>
                            <div class="modal fade bs-example-modal-lg" tabindex="-1" id="'.$nro.'"  role="dialog" aria-labelledby="myLargeModalLabel">
                              <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close text-danger" data-dismiss="modal" aria-hidden="true">
                                      &times;
                                    </button>
                                      <h4 class="modal-title">
                                        <img src="' . base_url() . 'assets/img/logo.png" width="150" alt="SmartAdmin" class="img-responsive">
                                              </h4>
                                        </div>
                                        <div class="modal-body no-padding">
                                          <div class="well">
                                            <table class="table table-hover">
                                              <thead>
                                              <tr>
                                                <th colspan="3"><center><font color="#000000">'.$row['proy_nombre'].'</font></center></th>
                                              </tr>
                                              </thead>
                                              <tr>
                                                <td><b>IDENTIFICACI&Oacute;N DE LA ETAPA</b></td>
                                                <td><center><a href="javascript:abreVentana(\''.site_url("admin").'/identificacion/'.$row['proy_id'].'\');" title="IDENTIFICACION DE LA ETAPA"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
                                                <td><center><a href="javascript:abreVentana(\''.site_url("admin").'/identificacion/'.$row['proy_id'].'\');" title="IDENTIFICACION DE LA ETAPA"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
                                              </tr>
                                              <tr>
                                                <td><b>PROGRAMACI&Oacute;N Y EJECUCI&Oacute;N F&Iacute;SICA DE LA ETAPA</b></td>
                                                <td><center><a href="javascript:abreVentana(\''.site_url("admin").'/rep_programacion/1/'.$fase[0]['id'].'/'.$row['proy_id'].'\');" title="PROGRAMACION Y EJECUCION FISICA ANUAL DE LA ETAPA"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
                                                <td><center><a href="javascript:abreVentana(\''.site_url("admin").'/rep_programacion/4/'.$fase[0]['id'].'/'.$row['proy_id'].'\');" title="PROGRAMACION Y EJECUCION FISICA PLURI-ANUAL DE LA ETAPA"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
                                              </tr>
                                              <tr>
                                                <td><b>PROGRAMACI&Oacute;N Y EJECUCI&Oacute;N FINANCIERA DE LA ETAPA</b></td>
                                                <td><center><a href="javascript:abreVentana(\''.site_url("admin").'/rep_financiero/1/'.$fase[0]['id'].'/'.$row['proy_id'].'\');" title="PROGRAMACION Y EJECUCION FINANCIERA ANUAL DE LA ETAPA"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
                                                <td><center><a href="javascript:abreVentana(\''.site_url("admin").'/rep_financiero/4/'.$fase[0]['id'].'/'.$row['proy_id'].'\');" title="PROGRAMACION Y EJECUCION FINANCIERA PLURI-ANUAL DE LA ETAPA"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
                                              </tr>
                                              <tr>
                                                <td><b>REGISTRO DE CONTRATOS</b></td>
                                                <td><center><a href="javascript:abreVentana(\''.site_url("admin").'/contratos/'.$row['proy_id'].'/'.$fase[0]['id'].'\');" title="REPORTE DE CONTRATOS"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
                                                <td><center><a href="javascript:abreVentana(\''.site_url("admin").'/contratos/'.$row['proy_id'].'/'.$fase[0]['id'].'\');" title="REPORTE DE CONTRATOS"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
                                              </tr>
                                              <tr>
                                                <td><b>INFORMES DE SUPERVISI&Oacute;N Y EVALUACI&Oacute;N</b></td>
                                                <td><center><a href="javascript:abreVentana(\''.site_url("admin").'/supervision_evaluacion/'.$row['proy_id'].'/'.$fase[0]['id'].'\');" title="REPORTE DE CONTRATOS"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
                                                <td><center><a href="javascript:abreVentana(\''.site_url("admin").'/supervision_evaluacion/'.$row['proy_id'].'/'.$fase[0]['id'].'\');" title="REPORTE DE CONTRATOS"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
                                              </tr>
                                              <tr>
                                                <td><b>CURVA "S" AVANCE F&Iacute;SICO </b></td>
                                                <td><center><a href="javascript:abreVentanaFis(\''.site_url('admin').'/imprimir_curva_fis/1/'.$fase[0]['id'].'/'.$row['proy_id'].'\');" title="GRAFICO AVANCE FISICO ANUAL"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
                                                <td><center><a href="javascript:abreVentanaFis(\''.site_url('admin').'/imprimir_curva_fis/4/'.$fase[0]['id'].'/'.$row['proy_id'].'\');" title="GRAFICO AVANCE FISICO PLURI-ANUAL"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
                                              </tr>
                                              <tr>
                                                <td><b>CURVA "S" AVANCE FINANCIERO</b></td>
                                                <td><center><a href="javascript:abreVentanaFin(\''.site_url("admin").'/imprimir_curva_fin/1/'.$fase[0]['id'].'/'.$row['proy_id'].'\');" title="GRAFICO AVANCE FINANCIERO ANUAL"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
                                                <td><center><a href="javascript:abreVentanaFin(\''.site_url("admin").'/imprimir_curva_fin/4/'.$fase[0]['id'].'/'.$row['proy_id'].'\');" title="GRAFICO AVANCE FINANCIERO PLURI-ANUAL"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
                                              </tr>
                                            </table>
                                          </div>
                                        </div>
                                  </div>
                                </div>
                              </div>
                            </div>';
                    }

                    $tabla .= '</td>';

                    $tabla .= '<td>';
                    if($row['aper_proyecto']!=''){
                        $tabla .= '<center>'.$row['aper_programa'].''.$row['aper_proyecto'].''.$row['aper_actividad'].'</center>';
                    }
                    else{
                        $tabla .=  '<center><font color="red"><button class="btn btn-danger" title="RE ASIGNAR CATEGORIA PROGRAMATICA PARA LA GESTION '.$this->session->userdata('gestion').'">ASIGNAR<br>APERTURA<br>PROGRAM&Aacute;TICA<br>'.$this->session->userdata('gestion').'</button></font></center>';
                    }
                    $tabla .= '</td>';
                    $tabla .= '<td>'.$row['proy_nombre'].'</td>';
                    $tabla .= '<td>'.$row['tp_tipo'].'</td>';
                    $tabla .= '<td>'.$row['proy_sisin'].'</td>';
                    $tabla .= '<td>'.$row['fun_nombre'].' '.$row['fun_paterno'].' '.$row['fun_materno'].'</td>';
                    $tabla .= '<td>'.$row['ue'].'</td>';
                    $tabla .= '<td>'.$row['ur'].'</td>';

                    $nc=$this->model_faseetapa->calcula_nc($fase[0]['pfec_fecha_inicio']); //// calcula nuevo/continuo
                    $ap=$this->model_faseetapa->calcula_ap($fase[0]['pfec_fecha_inicio'],$fase[0]['pfec_fecha_fin']);
                    $nro_fg_act = $this->model_faseetapa->nro_fasegestion_actual($fase[0]['id'],$this->session->userdata('gestion'));
                    $fase_gest = $this->model_faseetapa->fase_etapa_gestion($fase[0]['id'],$this->session->userdata("gestion"));
                    $tabla .='<td>* '.$fase[0]['fase'].'<br>* '.$fase[0]['etapa'].'</td>';
                    $tabla .='<td>'.$nc.'</td>';
                    $tabla .='<td>'.$ap.'</td>';
                    $tabla .='<td>'.number_format($fase[0]['pfec_ptto_fase'], 2, ',', '.').' Bs.</td>';
                    $tabla .='<td>';
                    if($nro_fg_act!=0 && ($fase_gest[0]['estado']==1 || $fase_gest[0]['estado']==2))
                    {
                        $tabla .=''.number_format($fase_gest[0]['pfecg_ppto_total'], 2, ',', '.').' Bs.';
                    }
                    elseif ($nro_fg_act==0) {
                        $tabla .= '<font color="red">la gestion no esta en curso</font>';
                    }
                    $tabla .='</td>';
                    $tabla .='<td>';
                    if($this->model_faseetapa->verif_fase_etapa_gestion($fase[0]['id'],$this->session->userdata("gestion"))!=0)
                    {
                        if($this->model_faseetapa->nro_ffofet($fase_gest[0]['ptofecg_id'])!=0)
                        {
                            $techo=$this->model_faseetapa->techo_presupuestario($fase_gest[0]['ptofecg_id']);
                            $tabla .= ''.number_format($techo[0]['suma_techo'], 2, ',', '.').' Bs.';
                        }
                        else{$tabla .= "<font color=red>S/T</font>";}
                    }
                    $tabla .='</td>';
                    $tabla .='<td>';
                    $loc=$this->model_proyecto->localizacion($row['proy_id']);
                    if(count($loc)!=0){
                        $tabla .='<table class="table table-bordered">';
                        $tabla .='<tr bgcolor="#474544">';
                        $tabla .='<th>PROVINCIA</th><th>MUNICIPIO</th>';
                        $tabla .='</tr>';
                        foreach($loc as $locali)
                        {
                            $tabla .='<tr>';
                            $tabla .='<td>'.$locali['prov_provincia'].'</td>';
                            $tabla .='<td>'.$locali['muni_municipio'].'</td>';
                            $tabla .='</tr>';
                        }

                        $tabla .='</table>';
                    }
                    $tabla .='</td>';
                    $tabla .= '</tr>';
                    $nro++;
                }
            }
        }

        return $tabla;
    }
    /*=============================== LISTA DE ARCHIVOS DOCUMENTOS DEL PROYECTO ============================*/
    public function list_archivos($id)
    {
        if($this->rolfunn(3) & $id!=''){
            $enlaces=$this->menu_modelo->get_Modulos(1);
            $data['enlaces'] = $enlaces;
            for($i=0;$i<count($enlaces);$i++)
            {
                $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
            }
            $data['subenlaces'] = $subenlaces;
            $data['id'] =$id;

            $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id);
            $data['arch']=$this->model_proyecto->get_archivos($id);
            $data['if']='false';
            $this->load->view('admin/programacion/proy_anual/formularios/list_doc', $data);
        }
        else{
            redirect('admin/dashboard');
        }
    }
    /*=======================================================================================================*/

    public function list_arch($id_p,$id_adj)
    {
        if($this->rolfunn(3) & $id_p!='' & $id_adj!=''){
            $enlaces=$this->menu_modelo->get_Modulos(1);
            $data['enlaces'] = $enlaces;
            for($i=0;$i<count($enlaces);$i++)
            {
                $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
            }
            $data['subenlaces'] = $subenlaces;
            $data['id'] =$id_p;

            $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p);
            $data['arch']=$this->model_proyecto->get_archivos($id_p);
            $data['doc']=$this->model_proyecto->get_doc($id_adj);
            $data['adj']=$data['doc'][0]['adj_adjunto'];
            $data['if']='true';

            $this->load->view('admin/programacion/proy_anual/formularios/list_doc', $data);
        }
        else{
            redirect('admin/dashboard');
        }
    }
    /*=======================================================================================================*/

    /*=================================================  SUBIR ARCHIVOS =================================================*/
    function subir_archivos()
    { //echo $this->input->post('file1');

        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {
            $this->form_validation->set_rules('id', 'id de proyecto', 'required|trim');

            if ($this->form_validation->run())
            {
                $filename = $_FILES["file1"]["name"]; ////// datos del archivo
                $file_basename = substr($filename, 0, strripos($filename, '.')); ///// nombre del archivo
                $file_ext = substr($filename, strripos($filename, '.')); ///// Extension del archivo
                $filesize = $_FILES["file1"]["size"]; //// Tama�o del archivo
                $allowed_file_types = array('.pdf','.docx','.doc','.xlsx','.xls','.jpg','.JPG','.png','.PNG','.JPEG','.JPG');

                if($filename!='')
                {
                    $data_to_store = array(
                        'proy_id' => $this->input->post('id'),
                        'documento' => strtoupper($this->input->post('doc')),
                        'tp_doc' => $this->input->post('tp_doc'),
                        'fun_id' => $this->session->userdata("fun_id"),
                        'adj_estado' =>'1',
                    );

                    if($filesize!=0)
                    {
                        if (in_array($file_ext,$allowed_file_types))
                        { // Rename file
                            $newfilename = ''.$this->input->post('id').'_'.substr(md5(uniqid(rand())),0,5).$file_ext;
                            $data_to_store['adj_adjunto'] = "".$newfilename;

                            if (file_exists("archivos/documentos/" . $newfilename))
                            {echo "El archivo ya existe.";}
                            else
                            {
                                move_uploaded_file($_FILES["file1"]["tmp_name"],"archivos/documentos/" . $newfilename);
                            }
                            $this->db->insert('_proyecto_adjuntos', $data_to_store);
                            $this->session->set_flashdata('success','SE GUARDO CORRECTAMENTE EL ARCHIVO');
                            redirect('admin/proy/proyecto/'.$this->input->post('id').'/8/true');
                        }
                        /* elseif (empty($file_basename)) {  echo "Please select a file to upload.";}
                         elseif ($filesize > 800000000){ redirect('admin/proy/proyecto/8/'.$this->input->post('id').'/false');}
                         else{echo "Only these file typs are allowed for upload: " . implode(', ',$allowed_file_types);  unlink($_FILES["file1"]["tmp_name"]);       }  */
                    }
                    elseif($filesize==0)
                    {
                        $this->session->set_flashdata('danger','Error!!!, no tiene un tama�o definido el archivo');
                        redirect('admin/proy/proyecto/'.$this->input->post('id').'/8/1/1'); //// El archivo no cuenta con un tama�o
                    }
                }
                else
                {
                    $this->session->set_flashdata('danger','Error!!!, Seleccione archivo');
                    redirect('admin/proy/proyecto/'.$this->input->post('id').'/8/1/0');  ///// nose selecciono archivo
                }
            }
            else
                $this->session->set_flashdata('danger','Error!!!');
            redirect('admin/proy/proyecto/'.$this->input->post('id').'/8/false');
        }
    }
    /*----------------------- Eliminar Archivo-------------------------*/
    function eliminar_archivo($id_p,$id_adj)
    {
        $data['arch'] = $this->model_proyecto->get_archivo_proy($id_adj);

        if(file_exists("archivos/documentos/".$data['arch'][0]['adj_adjunto']))
        {
            $file = "archivos/documentos/".$data['arch'][0]['adj_adjunto'];
            unlink($file);

            $this->db->where('proy_id',$id_p);
            $this->db->where('adj_id',$id_adj);
            $this->db->delete('_proyecto_adjuntos');

            return true;
            // redirect('admin/proy/proyecto/'.$id_p.'/8/true');
        }
        else
        {
            $this->db->where('proy_id',$id_p);
            $this->db->where('adj_id',$id_adj);
            $this->db->delete('_proyecto_adjuntos');

            return false;
            // redirect('admin/proy/proyecto/'.$id_p.'/8/false');
        }

    }

    function delete_archivo(){
        if($this->input->is_ajax_request() && $this->input->post()){
            $post = $this->input->post();
            $proy_id = $post['proy_id'];
            $adj_id = $post['adj_id'];

            $peticion=$this->eliminar_archivo($proy_id,$adj_id);
            if ($peticion) {
                $result = array(
                    'respuesta' => 'correcto'
                );
            } else {
                $result = array(
                    'respuesta' => 'error'
                );
            }
            echo json_encode($result);
        }else{
            show_404();
        }
    }
    /*================================================= END  SUBIR ARCHIVOS =================================================*/

    /*======================================================== FORMULARIOS DE REGISTRO PROYECTOS ====================================================*/
    function tecnico_operativo()
    {
        if($this->rolfun($this->rol)){
            $enlaces=$this->menu_modelo->get_Modulos(1);
            $data['enlaces'] = $enlaces;
            for($i=0;$i<count($enlaces);$i++)
            {
                $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
            }
            $data['subenlaces'] = $subenlaces;
            $data['cod']=$this->model_proyecto->cod_proy();
            if(isset($data['cod'][0]['proy_codigo']))
                $data['codigo']=$data['cod'][0]['proy_codigo']+1;
            else
                $data['codigo']=1;
            $data['tp_proy']=$this->model_proyecto->tip_proy();
            $data['tp_gasto']=$this->model_proyecto->tip_gasto();
            $data['list_cap']=$this->model_proyecto->list_cap();
            $data['programas'] = $this->model_proyecto->list_prog($this->session->userdata("gestion")); ///// lista aperturas padres
            $this->load->view('admin/programacion/proy_anual/formularios/form1', $data);
        }
        else{
            redirect('admin/dashboard');
        }
    }

    function tecnico_operativo_n($id,$form)
    {
        if($this->rolfun($this->rol) & $id!='' & $form!=''){

            $enlaces=$this->menu_modelo->get_Modulos_programacion(1);
            $data['enlaces'] = $enlaces;
            $data['nro_fase']= $this->model_faseetapa->nro_fase($id); /// nro de fases y etapas registrados
            $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id); ////// DATOS DEL PROYECTO
            $data['id_f'] = $this->model_faseetapa->get_id_fase($id); //// DATOS DE LA FASE ACTIVA
            $data['mod']=1;

            if($data['proyecto'][0]['tp_id']==4){$titulo='Resultados';}else{$titulo='Metas';}
            $data['titulo'] = $titulo;
            if($form=='2') ///// RESPONSABLES
            {

                $data['fun1']=$this->model_proyecto->asig_responsables(3); ////// tecnico OPERATIVO
                $data['fun2']=$this->model_proyecto->asig_responsables(4); ////// validador POA
                $data['fun3']=$this->model_proyecto->asig_responsables(5); ////// validador FINANCIERO

                $data['direcciones']=$this->model_proyecto->direcciones_administrativas(); ////// Direcciones Administrativas

                $data['resp1']=$this->model_proyecto->responsable_proy($id,'1');
                $data['resp2']=$this->model_proyecto->responsable_proy($id,'2');
                $data['resp3']=$this->model_proyecto->responsable_proy($id,'3');
                $data['unidad']=$this->model_proyecto->unidades_ejecu(); ////// Unidad Ejecutora
                $data['unidad2']=$this->model_proyecto->list_unidad_org(); ////// Unidad responsables
                $data['history'] = $this->model_proyecto->historial_usuario($id);

                $this->load->view('admin/programacion/proy_anual/formularios/form2', $data); //// RESPONSABLES DE LA OPERACION
            }
            elseif ($form=='3') ///// CLASIFICACION
            {
                $data['pdes'] = $this->model_proyecto->datos_pedes($data['proyecto'][0]['pdes_id']);
                $data['ptdi'] = $this->model_proyecto->datos_ptdi($data['proyecto'][0]['ptdi_id']);
                $data['lista_ptdi']=$this->model_proyecto->lista_ptdi();

                if(count($this->model_proyecto->codigo_sectorial($data['proyecto'][0]['codsectorial'],3))!=0){
                    $nivel=3;
                }
                elseif(count($this->model_proyecto->codigo_sectorial($data['proyecto'][0]['codsectorial'],2))!=0){
                    $nivel=2;
                }
                elseif(count($this->model_proyecto->codigo_sectorial($data['proyecto'][0]['codsectorial'],2))==0){
                    $nivel=2;
                }


                $data['codsec'] = $this->model_proyecto->codigo_sectorial($data['proyecto'][0]['codsectorial'],$nivel);
                $data['fifu'] = $this->model_proyecto->datos_finalidad($data['proyecto'][0]['fifu_id']);
                $fifu=$this->model_proyecto->get_fifu($data['proyecto'][0]['fifu_id']);
                if($fifu[0]['fifu_nivel']=='Clase'){
                    $data['tfifu']=1;
                }
                elseif($fifu[0]['fifu_nivel']=='Funcion'){
                    $data['tfifu']=0;
                }
                $this->load->view('admin/programacion/proy_anual/formularios/form4', $data); /////// CLASIFICACION
            }
            elseif ($form=='4') ///// LOCALIZACION
            {
                $this->load->view('admin/programacion/proy_anual/formularios/form3', $data); /////// LOCALIZACION
            }
            elseif ($form=='5') ////// MARCO LOGICO
            {
                $this->load->view('admin/programacion/proy_anual/formularios/form5', $data); /////// MARCO LOGICO
            }
            elseif ($form=='7') ///// RESUMEN TECNICO
            {
                $this->load->view('admin/programacion/proy_anual/formularios/form7', $data); //////// RESUMEN TECNICO
            }
            elseif ($form=='8') ///// ARCHIVO DOCUMENTOS ADJUNTOS
            {
                $data['arch'] = $this->model_proyecto->get_archivos($id);
                $this->load->view('admin/programacion/proy_anual/formularios/form8', $data);
            }
            elseif ($form=='9') ///// FASE Y ETAPAS DEL PROGRAMAS RECURRENTES, NO RECURRENTES Y FUNCIONES DE OPERACION
            {
                $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id);
                $data['fase'] = $this->model_faseetapa->get_id_fase($id); ///// datos fase encendida

                if(count($data['fase'])!=0)
                {
                    $datos = $this->model_faseetapa->datos_fase_etapa($data['fase'][0]['id'],$id);  // desvuelve las fechas del proyecto

                    $data['a�os']=$datos[0]['final']-$datos[0]['actual']+1;
                    $data['fecha']=$datos[0]['actual'];

                    $data['fase_proyecto'] = $this->model_faseetapa->fase_etapa($data['fase'][0]['id'],$id);
                    $data['nro_fg'] = $this->model_faseetapa->nro_fasegestion($data['fase'][0]['id']);
                    $a�os=$datos[0]['final']-$datos[0]['actual']+1;
                    $data['gestiones']=$a�os;
                    $fecha=$datos[0]['actual'];
                    $data['fecha']=$fecha;

                    if($a�os>=1)
                    {
                        $data['fase_gestion1']=$this->model_faseetapa->fase_etapa_gestion($data['fase'][0]['id'],$fecha);
                        if($a�os>=2)
                        {
                            $data['fase_gestion2']=$this->model_faseetapa->fase_etapa_gestion($data['fase'][0]['id'],$fecha+1);
                            if($a�os>=3)
                            {
                                $data['fase_gestion3']=$this->model_faseetapa->fase_etapa_gestion($data['fase'][0]['id'],$fecha+2);
                                if($a�os>=4)
                                {
                                    $data['fase_gestion4']=$this->model_faseetapa->fase_etapa_gestion($data['fase'][0]['id'],$fecha+3);
                                    if($a�os>=5)
                                    {
                                        $data['fase_gestion5']=$this->model_faseetapa->fase_etapa_gestion($data['fase'][0]['id'],$fecha+4);
                                        if($a�os>=6)
                                        {
                                            $data['fase_gestion6']=$this->model_faseetapa->fase_etapa_gestion($data['fase'][0]['id'],$fecha+5);
                                            if($a�os>=7)
                                            {
                                                $data['fase_gestion7']=$this->model_faseetapa->fase_etapa_gestion($data['fase'][0]['id'],$fecha+6);
                                                if($a�os>=8)
                                                {
                                                    $data['fase_gestion8']=$this->model_faseetapa->fase_etapa_gestion($data['fase'][0]['id'],$fecha+7);
                                                    if($a�os>=9)
                                                    {
                                                        $data['fase_gestion9']=$this->model_faseetapa->fase_etapa_gestion($data['fase'][0]['id'],$fecha+8);
                                                        if($a�os>=10)
                                                        {
                                                            $data['fase_gestion10']=$this->model_faseetapa->fase_etapa_gestion($data['fase'][0]['id'],$fecha+9);
                                                            if($a�os>=11)
                                                            {
                                                                $data['fase_gestion11']=$this->model_faseetapa->fase_etapa_gestion($data['fase'][0]['id'],$fecha+10);
                                                                if($a�os>=12)
                                                                {
                                                                    $data['fase_gestion12']=$this->model_faseetapa->fase_etapa_gestion($data['fase'][0]['id'],$fecha+12);
                                                                    if($a�os>=13)
                                                                    {
                                                                        $data['fase_gestion13']=$this->model_faseetapa->fase_etapa_gestion($data['fase'][0]['id'],$fecha+13);

                                                                    }

                                                                }
                                                            }
                                                        }

                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    $this->load->view('admin/programacion/proy_anual/fase/form_fase_update2', $data);
                }
                else
                {
                    echo "<center><font color=red size=4>ERROR AL REGISTRAR LAS GESTIONES DEL PROGRAMA !!!!</red></font><br><font color=blue><br>Vuelva a Registrar las fechas del Programa, o comuniquese con el administrador</font></center>";
                }

            }
            elseif ($form=='10') {
                $data['metas'] = $this->model_proyecto->metas_p($id); ///// metas del proyecto
                $this->load->view('admin/programacion/proy_anual/formularios/metas', $data);
            }

        }
        else{
            redirect('admin/dashboard');
        }

    }

    /*==================================== VALIDA PROYECTOS =====================================================*/
    public function valida()
    {
        if ($this->input->post('form')=='1'&& $this->input->server('REQUEST_METHOD') === 'POST') ///// DATOS GENERALES
        {
            $this->form_validation->set_rules('prog', 'Nro de Programa', 'required|trim');
            $this->form_validation->set_rules('nom_proy', 'Proyecto o Actividad', 'required|trim');
            $this->form_validation->set_rules('ini', 'Fecha de Inicio', 'required|trim');
            $this->form_validation->set_rules('fin', 'Fecha Final', 'required|trim');
            // $this->form_validation->set_rules('cod_proy', 'Codigo de Proyecto', 'required|trim|callback_very_cod');

            if ($this->form_validation->run() ) ///// Al no existir error guarda la informacion en la BD
            {
                /*=================================== PROYECTOS==================================*/
                $query=$this->db->query('set datestyle to DMY');
                $data_to_store = array(
                    'proy_codigo' => $this->input->post('cod_proy'),
                    'proy_nombre' => strtoupper($this->input->post('nom_proy')),
                    'tp_id' => $this->input->post('tp_id'),
                    'proy_gestion_inicio_ddmmaaaa' => $this->input->post('ini'),
                    'proy_gestion_fin_ddmmaaaa' => $this->input->post('fin'),
                    'tg_id' => $this->input->post('tg'),
                    'cp_id' => $this->input->post('tp_cap'),
                    'comp' => $this->input->post('comp'),
                    'proy_fecha_registro' => date('d/m/Y'),
                    'fun_id' => $this->session->userdata("fun_id"),
                );
                $this->db->insert('_proyectos', $data_to_store);
                $proy_id=$this->db->insert_id();
                $id_p=$this->db->insert_id();
                /*============================= RESPONSABLES ========================================*/
                $this->model_proyecto->add_resp_proy($id_p,$this->session->userdata("fun_id"),0,0,0,0,0);
                /*==================================================================================*/

                $fechas = $this->model_proyecto->fechas_proyecto($id_p);  // devuelve las fechas del proyecto inicio-conclusion
                /*=================================== UPDATE PROYECTOS==================================*/
                $update_proyect = array('proy_gestion_inicio' => $fechas[0]['inicio'],
                    'proy_gestion_fin' => $fechas[0]['final'],
                    'proy_gestion_impacto' => ($fechas[0]['final']-$fechas[0]['inicio'])+1);
                $this->db->where('proy_id', $id_p);
                $this->db->update('_proyectos', $update_proyect);

                if($this->input->post('tp_id')==1)
                {
                    $proy=$this->input->post('proy1'); $act='000';
                }
                elseif ($this->input->post('tp_id')==2 || $this->input->post('tp_id')==3 || $this->input->post('tp_id')==4)
                {
                    $proy='0000'; $act=$this->input->post('act1');
                }

                $gestiones=$fechas[0]['final']-$fechas[0]['inicio'];
                $gestion=$fechas[0]['inicio'];

                /*=================================== APERTURA PROGRAMATICA =========================================*/
                for($i=0;$i<=$gestiones;$i++)
                {
                    if($this->session->userdata("gestion")==$gestion){
                        $this->model_proyecto->add_apertura($id_p,$gestion,$this->input->post('prog'),$proy,$act,$this->input->post('nom_proy'),$this->session->userdata("fun_id"));
                    }
                    else{
                        $this->model_proyecto->add_apertura($id_p,$gestion,$this->input->post('prog'),'','',$this->input->post('nom_proy'),$this->session->userdata("fun_id"));
                    }

                    $gestion++;
                }
                /*===================================================================================================*/

                /*=================================== TIPO DE PROYECTOS=======================================*/
                /*=================================== PROYECTOS DE INVERSION ==================================*/
                if ($this->input->post('tp_id')==1) {

                    $update_proy = array('proy_sisin' => $this->input->post('cod_sisin'));
                    $this->db->where('proy_id', $id_p);
                    $this->db->update('_proyectos', $update_proy);
                }
                /*======== PROGRAMAS RECURRENTES Y NO RECURRENTES - OPERACION DE FUNCIONAMIENTO ========*/
                else
                {
                    $proyecto = $this->model_proyecto->get_id_proyecto($id_p);
                    $query=$this->db->query('set datestyle to DMY');
                    $data_to_store3 = array( ///// no tiene fase -> Tomando valores del proyecto
                        'proy_id' => $id_p,
                        'pfec_fecha_inicio_ddmmaaa' => $this->input->post('ini'),
                        'pfec_fecha_fin_ddmmaaa' => $this->input->post('fin'),
                        'pfec_fecha_registro' => date('d/m/Y h:i:s'),
                        'pfec_fecha_inicio' => $fechas[0]['inicio'],
                        'pfec_fecha_fin' => $fechas[0]['final'],
                        'pfec_ptto_fase' => $this->input->post('mtotal'),
                        'pfec_ejecucion' => 1,
                        'fun_id' => $this->session->userdata("fun_id"),
                        'aper_id' => $proyecto[0]['aper_id'],
                    );
                    $this->db->insert('_proyectofaseetapacomponente', $data_to_store3);
                    $id_f=$this->db->insert_id();

                    $datos = $this->model_faseetapa->datos_fase_etapa($id_f,$id_p);  // desvuelve las fechas del proyecto

                    $a�os=$datos[0]['final']-$datos[0]['actual'];
                    $fecha=$datos[0]['actual'];

                    /*=======================  fase etapa gestion ======================*/
                    for($i=0;$i<=$a�os;$i++)
                    {
                        $data_to_store2 = array( ///// Tabla ptto_fase_gestion
                            'pfec_id' => $id_f,
                            'g_id' => $fecha,
                            'fun_id' => $this->session->userdata("fun_id"),
                        );
                        $this->db->insert('ptto_fase_gestion', $data_to_store2); ///// inserta a ptto_fase_gestion
                        $fecha=$fecha+1;
                    }
                    /*========================== end fase etapa gestion ======================*/
                }

                /*---------------------- redireccionar--------------------*/
                redirect('admin/proy/proyecto/'.$id_p.'/10'); ///// Metas/Resultados

            }
            else
            { //echo "error";
                /*====================== SI EXISTE UN MISMO CODIGO SE VUELVE A LA MISMA VISTA============================*/
                $enlaces=$this->menu_modelo->get_Modulos(1);
                $data['enlaces'] = $enlaces;
                for($i=0;$i<count($enlaces);$i++)
                {
                    $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
                }
                $data['subenlaces'] = $subenlaces;
                $data['cod']=$this->model_proyecto->cod_proy();
                $data['codigo']=$data['cod'][0]['proy_codigo']+1;

                $this->load->view('admin/programacion/proy_anual/formularios/form1', $data);
            }

        }
        elseif($this->input->post('form')=='2' && $this->input->server('REQUEST_METHOD') === 'POST') ///// RESPONSABLES DEL PROYECTO
        {
            $this->form_validation->set_rules('fun_id_1', 'Tecnico Operativo', 'required|trim');
            $this->form_validation->set_rules('fun_id_2', 'Validador POA', 'required|trim');
            $this->form_validation->set_rules('fun_id_3', 'Validador Financiero', 'required|trim');
            $this->form_validation->set_rules('uni_ejec', 'unidad ejecutora', 'required|trim');
            $this->form_validation->set_rules('uni_ejec2', 'unidad responsable', 'required|trim');

            if ($this->form_validation->run()) ///// Al no existir error guarda la informacion en la BD
            {
                $this->model_proyecto->update_resp_proy($this->input->post('id'),$this->input->post('fun_id_1'),$this->input->post('fun_id_2'),$this->input->post('fun_id_3'),$this->input->post('uni_ejec'),$this->input->post('uni_ejec2'),$this->input->post('uni_adm'));
                $proy = $this->model_proyecto->get_id_proyecto($this->input->post('id'));
                /*====================================  �PERTURA PROGRAMATICA  ========================*/
                $query=$this->db->query('set datestyle to DMY');
                $update_aper = array('uni_id' => $this->input->post('uni_ejec'));
                $this->db->where('aper_id', $proy[0]['aper_id']);
                $this->db->update('aperturaprogramatica', $update_aper);

                redirect('admin/proy/proyecto/'.$this->input->post('id').'/3');
            }
            else
            {
                redirect('admin/proy/proyecto/'.$this->input->post('id').'/2');

            }
        }

        elseif($this->input->post('form')=='3' && $this->input->server('REQUEST_METHOD') === 'POST') ///// CLASIFICACION
        {
            $this->form_validation->set_rules('finalidad1', 'Seleccione Finalidad', 'required|trim');
            $this->form_validation->set_rules('pedes4', 'Seleccione Resultado', 'required|trim');

            if ($this->form_validation->run())
            {
                if($this->input->post('clasificador3')!=''){
                    $cl= $this->input->post('clasificador3');
                    $cod_sec=$this->input->post('clasificador3');
                }
                elseif ($this->input->post('clasificador3')==''){
                    $cl=$this->input->post('clasificador2');
                    $codsec = $this->model_proyecto->get_id_cl($cl);
                    $cod_sec=$codsec[0]['codsectorial'];
                }

                if($this->input->post('clase')!=''){$clase= $this->input->post('clase');}
                elseif ($this->input->post('clase')=='')  {$clase=$this->input->post('fun');}

                if($this->input->post('pedes4')==""){$id_pdes = $this->model_proyecto->get_id_pdes($this->input->post('pedes3'));}   // devuelve id pedes
                elseif ($this->input->post('pedes4')!="") {$id_pdes = $this->model_proyecto->get_id_pdes($this->input->post('pedes4')); }  // devuelve id pedes

                $id_ptdi = $this->model_proyecto->get_id_ptdi($this->input->post('ptdi3'));  // devuelve id pedes


                $query=$this->db->query('set datestyle to DMY');
                $update_proy = array('proy_fecha_registro' => date('d/m/Y h:i:s'),
                    'codsectorial' => $cod_sec,
                    'pdes_id' => $id_pdes[0]['pdes_id'],
                    'ptdi_id' => $id_ptdi[0]['ptdi_id'],
                    'fifu_id' => $clase);

                $this->db->where('proy_id', $this->input->post('id'));
                $this->db->update('_proyectos', $this->security->xss_clean($update_proy));

                redirect('admin/proy/proyecto/'.$this->input->post('id').'/4?qRegistro='.$this->input->post('id').'');
            }
            else
            {
                redirect('admin/proy/proyecto/'.$this->input->post('id').'/3/false');
            }
        }
        elseif($this->input->post('form')=='4' && $this->input->server('REQUEST_METHOD') === 'POST') ///// LOCALIZACION
        {
            $this->form_validation->set_rules('pob_bene', 'Poblacion Beneficiaria', 'required|trim');
            $this->form_validation->set_rules('area_id', 'Area de Influencia', 'required|trim');
            $this->form_validation->set_rules('pob_c', 'Poblacion censo', 'required|trim');
            $this->form_validation->set_rules('num_pob', 'numero de poblacion beneficiada', 'required|trim');
            $this->form_validation->set_rules('porc_pob', 'porcentaje de poblacion beneficiada', 'required|trim');

            if ($this->form_validation->run()) ///// Al no existir error guarda la informacion en la BD
            {     $query=$this->db->query('set datestyle to DMY');
                $update_proy = array('proy_fecha_registro' => date('d/m/Y h:i:s'),
                    'proy_poblac_beneficiaria' => strtoupper($this->input->post('pob_bene')),
                    'area_id' => $this->input->post('area_id'),
                    'proy_poblac_beneficiada' => $this->input->post('num_pob'),
                    'proy_porcent_poblac_beneficiada' => $this->input->post('porc_pob'),
                    'proy_nro_hombres' => $this->input->post('nro_h'),
                    'proy_nro_mujeres' => $this->input->post('nro_m'));

                $this->db->where('proy_id', $this->input->post('id'));
                $this->db->update('_proyectos', $update_proy);

                /*---------------------- redireccionar--------------------*/
                if($this->input->post('tp_id')==1 || $this->input->post('tp_id')==2 || $this->input->post('tp_id')==3)
                {
                    redirect('admin/proy/proyecto/'.$this->input->post('id').'/5');
                }
                elseif($this->input->post('tp_id')==4)
                {
                    redirect('admin/proy/proyecto/'.$this->input->post('id').'/7');
                }

            }
            else
            {
                redirect('admin/proy/proyecto/'.$this->input->post('id').'/4?qRegistro='.$this->input->post('id').'/false');
            }

        }
        elseif($this->input->post('form')=='5' && $this->input->server('REQUEST_METHOD') === 'POST') ///// MARCO LOGICO
        {
            $this->form_validation->set_rules('desc_prob', 'Descripcion del Problema', 'required|trim');

            if ($this->form_validation->run())
            {     $query=$this->db->query('set datestyle to DMY');
                $update_proy = array('proy_fecha_registro' => date('d/m/Y h:i:s'),
                    'proy_desc_problema' => strtoupper($this->input->post('desc_prob')),
                    'proy_desc_solucion' => strtoupper($this->input->post('desc_sol')),
                    'proy_obj_general' => strtoupper($this->input->post('obj_gral')),
                    'proy_obj_especifico' => $this->input->post('obj_esp'));

                $this->db->where('proy_id', $this->input->post('id'));
                $this->db->update('_proyectos', $update_proy);

                redirect('admin/proy/proyecto/'.$this->input->post('id').'/7');
            }
            else
            {
                redirect('admin/proy/proyecto/'.$this->input->post('id').'/5/false');
            }
        }

        elseif($this->input->post('form')=='7' && $this->input->server('REQUEST_METHOD') === 'POST') ///// RESUMEN TECNICO
        {
            $this->form_validation->set_rules('resumen', 'Resumen Tecnico del proyecto', 'required|trim');

            if ($this->form_validation->run())
            {
                $query=$this->db->query('set datestyle to DMY');
                $update_proy = array('proy_fecha_registro' => date('d/m/Y h:i:s'),
                    'proy_descripcion_proyecto' => strtoupper($this->input->post('resumen')));

                $this->db->where('proy_id', $this->input->post('id'));
                $this->db->update('_proyectos', $update_proy);

                redirect('admin/proy/proyecto/'.$this->input->post('id').'/8');
            }
            else
            {
                redirect('admin/proy/proyecto/'.$this->input->post('id').'/7/false');
            }
        }
    }

    /*=================================== RUTA DE EDITADO ==============================================*/
    public  function ruta_edit_proy($id,$form)
    {
        if($this->rolfun($this->rol) & $id!='' & $form!=''){
            $enlaces=$this->menu_modelo->get_Modulos_programacion(1);
            $data['enlaces'] = $enlaces;
            for($i=0;$i<count($enlaces);$i++)
            {
                $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
            }
            $data['subenlaces'] = $subenlaces;
            $data['nro_fase']= $this->model_faseetapa->nro_fase($id); /// nro de fases y etapas registrados
            $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id);
            $data['id_f'] = $this->model_faseetapa->get_id_fase($id); //// DATOS DE LA FASE ACTIVA
            $data['mod']=1;
            $data['nro_f'] = $this->model_faseetapa->nro_fase($id);
            $data['list_cap']=$this->model_proyecto->list_cap();
            if($data['proyecto'][0]['tp_id']==4){$titulo='Resultados';$comp='PROCESOS';}else{$titulo='Metas';$comp='COMPONENTES';}
            $data['titulo'] = $titulo;
            $data['comp'] = $comp;
            if($form==1) /// DATOS GENERALES
            {
                $data['tp_proy']=$this->model_proyecto->tip_proy();
                $data['tp_gasto']=$this->model_proyecto->tip_gasto();
                $data['fase'] = $this->model_faseetapa->get_id_fase($id); ///// datos fase encendida
                $data['rol_modifica'] =  $this->rol_modifica();
                $data['programas'] = $this->model_proyecto->list_prog($this->session->userdata("gestion")); ///// lista aperturas padres

                if($data['proyecto'][0]['aper_proyecto']=='') ///// Re asignar apertura
                {
                    $this->load->view('admin/programacion/proy_anual/formularios/form1_edit_aper', $data);
                }
                elseif($data['proyecto'][0]['aper_proyecto']!='') ///// asignado
                {
                    $this->load->view('admin/programacion/proy_anual/formularios/form1_edit', $data);
                }

            }
        }
        else{
            redirect('admin/dashboard');
        }
    }
    /*================================ VALIDAR MODIFICADO APERTURA PROGRAMATICA GESTION ==================================*/
    public function actualizar_apertura()
    {
        //echo "idfase".$this->input->post('fase').' / '.$this->input->post('etapas');
        if ($this->input->server('REQUEST_METHOD') === 'POST') ///// DATOS GENERALES
        {
            $this->form_validation->set_rules('id', 'Id Proyecto', 'required|trim');
            $this->form_validation->set_rules('aper_id', 'Apertura Programatica', 'required|trim');

            if ($this->form_validation->run())
            {
                if($this->input->post('tp_id')==1)
                {
                    $proy=$this->input->post('proy1'); $act='000';
                }
                elseif ($this->input->post('tp_id')==2 || $this->input->post('tp_id')==3 || $this->input->post('tp_id')==4)
                {
                    $proy='0000'; $act=$this->input->post('act1');
                }

                $proyecto = $this->model_proyecto->get_id_proyecto($this->input->post('id'));
                $this->model_proyecto->update_apertura($this->input->post('aper_id'),$this->input->post('prog'),$proy,$act,$proyecto[0]['proy_nombre'],$this->session->userdata("fun_id"));

                if($this->input->post('mod')==1)
                {
                    redirect('admin/proy/list_proy');
                }
                elseif($this->input->post('mod')==4)
                {
                    redirect('admin/sgp/list_proy');
                }
            }
            else
            {
                echo "Error";
            }
        }
    }
    /*=====================================================================================================================*/

    /*====================================== VALIDAR MODIFICADO ===============================================*/
    public function actualizar_datos()
    {
        //echo "idfase".$this->input->post('fase').' / '.$this->input->post('etapas');
        if ($this->input->post('form')=='1' && $this->input->server('REQUEST_METHOD') === 'POST') ///// DATOS GENERALES
        {
            $this->form_validation->set_rules('id', 'Id Proyecto', 'required|trim');
            $this->form_validation->set_rules('nom_proy', 'Proyecto , Actividad', 'required|trim');

            if ($this->form_validation->run()) ///// Al no existir error guarda la informacion en la BD
            {   $tp=$this->input->post('tp_id2');
                /*----------------------------- ACTULIZANDO DATOS DEL PROYECTO -------------------------------*/
                $query=$this->db->query('set datestyle to DMY');
                $update_proy = array(
                    'proy_nombre' => strtoupper($this->input->post('nom_proy')),
                    'proy_sisin' => $this->input->post('cod_sisin'),
                    'tp_id' => $this->input->post('tp_id2'),
                    'proy_gestion_inicio_ddmmaaaa' => $this->input->post('f_ini'),
                    'proy_gestion_fin_ddmmaaaa' => $this->input->post('f_final'),
                    'tg_id' => $this->input->post('tg'),
                    'cp_id' => $this->input->post('tp_cap'),
                    'comp' => $this->input->post('comp'),
                    'proy_fecha_registro' => date('d/m/Y h:i:s'),
                    'fun_id' => $this->session->userdata("fun_id"),
                    'estado' => '2');

                $this->db->where('proy_id', $this->input->post('id'));
                $this->db->update('_proyectos', $update_proy);

                $fechas = $this->model_proyecto->fechas_proyecto($this->input->post('id'));  // devuelve las fechas del proyecto inicio-conclusion
                $query=$this->db->query('set datestyle to DMY');
                $update_proyect = array(
                    'proy_gestion_inicio' => $fechas[0]['inicio'],
                    'proy_gestion_fin' => $fechas[0]['final'],
                    'proy_gestion_impacto' => ($fechas[0]['final']-$fechas[0]['inicio'])+1);
                $this->db->where('proy_id', $this->input->post('id'));
                $this->db->update('_proyectos', $update_proyect);

                //////////////////////////////////////// en caso de que la fecha inicial se adelante /////////////////////////////////////
                if($fechas[0]['inicio']<$this->input->post('gi'))
                {
                    $fecha=$fechas[0]['inicio'];
                    $nro=$this->input->post('gi')-$fechas[0]['inicio'];
                    for($i=1;$i<=$nro;$i++)
                    {
                        $this->model_proyecto->add_apertura($this->input->post('id'),$fecha,$this->input->post('prog'),'','',$this->input->post('nom_proy'),$this->session->userdata("fun_id"));
                        $fecha++;
                    }
                    if($tp==2 || $tp==3 || $tp==4)
                    {
                        $fase = $this->model_faseetapa->get_id_fase($this->input->post('id')); //// datos fase activa

                        $update_fase = array( 'pfec_fecha_inicio_ddmmaaa' => $this->input->post('f_ini'),
                            'pfec_fecha_fin_ddmmaaa' => $this->input->post('f_final'),
                            'pfec_fecha_inicio' => $fechas[0]['inicio'],
                            'pfec_ptto_fase' => $this->input->post('mtotal'),
                            'pfec_fecha_fin' => $fechas[0]['final']);
                        $this->db->where('pfec_id', $fase[0]['id']);
                        $this->db->update('_proyectofaseetapacomponente', $update_fase);

                        $fecha=$fechas[0]['inicio'];
                        for($i=1;$i<=$nro;$i++)
                        {
                            $query=$this->db->query('set datestyle to DMY');
                            $data_to_store2 = array( ///// Tabla ptto_fase_gestion
                                'pfec_id' => $fase[0]['id'],
                                'g_id' => $fecha,
                                'fun_id' => $this->session->userdata("fun_id"),
                            );
                            $this->db->insert('ptto_fase_gestion', $data_to_store2);
                            $fecha++;
                        }
                    }
                }
                ////////////// en caso en que la fecha inicial se reduzca /////////////////////
                if($fechas[0]['inicio']>$this->input->post('gi'))
                {
                    $fecha=$this->input->post('gi');
                    $nro=$fechas[0]['inicio']-$this->input->post('gi');
                    for($i=1;$i<=$nro;$i++)
                    {
                        $aper = $this->model_proyecto->aper_id($this->input->post('id'),$fecha); //// aper_id buscado
                        $this->model_proyecto->delete_aper_id($aper[0]['aper_id']); //// elimando apertura programatica
                        $fecha++;
                    }
                    if($tp==2 || $tp==3 || $tp==4)
                    {
                        $fase = $this->model_faseetapa->get_id_fase($this->input->post('id')); //// datos fase activa

                        $update_fase = array( 'pfec_fecha_inicio_ddmmaaa' => $this->input->post('f_ini'),
                            'pfec_fecha_fin_ddmmaaa' => $this->input->post('f_final'),
                            'pfec_fecha_inicio' => $fechas[0]['inicio'],
                            'pfec_ptto_fase' => $this->input->post('mtotal'),
                            'pfec_fecha_fin' => $fechas[0]['final']);
                        $this->db->where('pfec_id', $fase[0]['id']);
                        $this->db->update('_proyectofaseetapacomponente', $update_fase);

                        $fecha=$this->input->post('gi');
                        for($i=1;$i<=$nro;$i++)
                        {
                            $this->db->where('g_id', $fecha);
                            $this->db->where('pfec_id', $fase[0]['id']);
                            $this->db->delete('ptto_fase_gestion');
                            $fecha++;
                        }
                    }

                }
                else{
                    $this->model_proyecto->update_apertura($this->input->post('aper_id'),$this->input->post('prog'),$this->input->post('proy'),$this->input->post('act'),$this->input->post('nom_proy'),$this->session->userdata("fun_id"));
                }
                /*------------------------------------------------------------------------------------------*/

                //////////////////////////////////////// en caso de que la fecha final se amplie /////////////////////////////////////
                if($fechas[0]['final']>$this->input->post('gf'))
                {

                    $fecha=$this->input->post('gf')+1;
                    $nro=$fechas[0]['final']-$this->input->post('gf');
                    for($i=1;$i<=$nro;$i++)
                    {
                        $this->model_proyecto->add_apertura($this->input->post('id'),$fecha,$this->input->post('prog'),'','',$this->input->post('nom_proy'),$this->session->userdata("fun_id"));
                        $fecha++;
                    }

                    if($tp==2 || $tp==3 || $tp==4)
                    {
                        $fase = $this->model_faseetapa->get_id_fase($this->input->post('id')); //// datos fase activa

                        $update_fase = array( 'pfec_fecha_inicio_ddmmaaa' => $this->input->post('f_ini'),
                            'pfec_fecha_fin_ddmmaaa' => $this->input->post('f_final'),
                            'pfec_fecha_inicio' => $fechas[0]['inicio'],
                            'pfec_ptto_fase' => $this->input->post('mtotal'),
                            'pfec_fecha_fin' => $fechas[0]['final']);
                        $this->db->where('pfec_id', $fase[0]['id']);
                        $this->db->update('_proyectofaseetapacomponente', $update_fase);

                        $fecha=$this->input->post('gf')+1;
                        for($i=1;$i<=$nro;$i++)
                        {
                            $data_to_store2 = array( ///// Tabla ptto_fase_gestion
                                'pfec_id' => $fase[0]['id'],
                                'g_id' => $fecha,
                                'fun_id' => $this->session->userdata("fun_id"),
                            );
                            $this->db->insert('ptto_fase_gestion', $data_to_store2);
                            $fecha++;
                        }
                    }
                }
                //////////////////////////////////////// en caso de que la fecha final se reduzca /////////////////////////////////////
                elseif($fechas[0]['final']<$this->input->post('gf'))
                {
                    $fecha=$this->input->post('gf');
                    $nro=$this->input->post('gf')-$fechas[0]['final'];

                    for($i=1;$i<=$nro;$i++)
                    {
                        $aper = $this->model_proyecto->aper_id($this->input->post('id'),$fecha); //// aper_id buscado
                        $this->model_proyecto->delete_aper_id($aper[0]['aper_id']); //// elimando apertura programatica*/
                        // echo "Gestion a eliminar ".$fecha.' aper '.$aper[0]['aper_id'].'<br>';
                        $fecha--;
                    }

                    if($tp==2 || $tp==3 || $tp==4)
                    {
                        $fase = $this->model_faseetapa->get_id_fase($this->input->post('id')); //// datos fase activa

                        $update_fase = array( 'pfec_fecha_inicio_ddmmaaa' => $this->input->post('f_ini'),
                            'pfec_fecha_fin_ddmmaaa' => $this->input->post('f_final'),
                            'pfec_fecha_inicio' => $fechas[0]['inicio'],
                            'pfec_ptto_fase' => $this->input->post('mtotal'),
                            'pfec_fecha_fin' => $fechas[0]['final']);
                        $this->db->where('pfec_id', $fase[0]['id']);
                        $this->db->update('_proyectofaseetapacomponente', $update_fase);

                        $fecha=$this->input->post('gf');
                        for($i=1;$i<=$nro;$i++)
                        {
                            $this->db->where('g_id', $fecha);
                            $this->db->where('pfec_id', $fase[0]['id']);
                            $this->db->delete('ptto_fase_gestion');
                            $fecha++;
                        }
                    }

                }

                /*=================================================== CAMBIAR EL TIPO DEL PROYECTO ==========================================================*/
                if($this->input->post('tp_ant')!=$this->input->post('tp_id2'))
                {
                    /*----------------------------------------------- Inversion a otro programa --------------------------------------------*/
                    if($this->input->post('tp_ant')==1 && $this->model_faseetapa->nro_fase($this->input->post('id'))!=0) /// inversion y ya tiene fase
                    {
                        $fase = $this->model_faseetapa->get_id_fase($this->input->post('id')); //// datos fase activa

                        $update_fase = array('pfec_fecha_inicio_ddmmaaa' => $this->input->post('f_ini'),'pfec_fecha_fin_ddmmaaa' => $this->input->post('f_final'),'pfec_fecha_inicio' => $fechas[0]['inicio'],'pfec_fecha_fin' => $fechas[0]['final'],'pfec_ejecucion' => 1,'pfec_ptto_fase' => $this->input->post('mtotal'));
                        $this->db->where('pfec_id', $fase[0]['id']);
                        $this->db->update('_proyectofaseetapacomponente', $update_fase);

                    }
                    elseif($this->input->post('tp_ant')==1 && $this->model_faseetapa->nro_fase($this->input->post('id'))==0) //// inversion y no tiene fase resgitrado
                    {
                        $proyecto = $this->model_proyecto->get_id_proyecto($this->input->post('id'));
                        $query=$this->db->query('set datestyle to DMY');
                        $data_to_store3 = array( ///// no tiene fase -> Tomando valores del proyecto
                            'proy_id' => $this->input->post('id'),
                            'pfec_descripcion' => strtoupper($this->input->post('nom_proy')),
                            'pfec_fecha_inicio_ddmmaaa' => $this->input->post('f_ini'),
                            'pfec_fecha_fin_ddmmaaa' => $this->input->post('f_final'),
                            'pfec_fecha_registro' => date('d/m/Y h:i:s'),
                            'pfec_fecha_inicio' => $fechas[0]['inicio'],
                            'pfec_fecha_fin' => $fechas[0]['final'],
                            'pfec_ptto_fase' => $this->input->post('mtotal'),
                            'pfec_ejecucion' => 1,
                            'fun_id' => $this->session->userdata("fun_id"),
                            'aper_id' => $proyecto[0]['aper_id'],
                        );
                        $this->db->insert('_proyectofaseetapacomponente', $data_to_store3);
                        $id_f=$this->db->insert_id();

                        $datos = $this->model_faseetapa->datos_fase_etapa($id_f,$this->input->post('id'));  // desvuelve las fechas del proyecto

                        $a�os=$datos[0]['final']-$datos[0]['actual'];
                        $fecha=$datos[0]['actual'];

                        /*=======================  fase etapa gestion ======================*/
                        for($i=0;$i<=$a�os;$i++)
                        {
                            $data_to_store2 = array( ///// Tabla ptto_fase_gestion
                                'pfec_id' => $id_f,
                                'g_id' => $fecha,
                                'fun_id' => $this->session->userdata("fun_id"),
                            );
                            $this->db->insert('ptto_fase_gestion', $data_to_store2); ///// inserta a ptto_fase_gestion
                            $fecha=$fecha+1;
                        }
                        /*========================== end fase etapa gestion ======================*/

                        if($this->input->post('tp_id2')==4)
                        {
                            $data_to_store4 = array('pfec_id' => $id_f,'fun_id' => $this->session->userdata("fun_id"),);
                            $this->db->insert('_componentes', $data_to_store4);  ///// insertando datos a la tabla componentes
                        }
                    }
                    /*------------------------------------------------------------------------------------------------------------------------*/
                    /*----------------------------------------------- programas recurrentes, no recurrentes a operacion de funcionamiento--------------------------------------*/
                    if($this->input->post('tp_ant')==2 || $this->input->post('tp_ant')==3)
                    {
                        if($this->input->post('tp_id2')==4)
                        {
                            $fase = $this->model_faseetapa->get_id_fase($this->input->post('id')); //// datos fase activa
                            if($this->model_componente->componentes_nro($fase[0]['id'])==0) //// no tiene componente
                            {
                                $data_to_store4 = array('pfec_id' => $fase[0]['id'],'fun_id' => $this->session->userdata("fun_id"),);
                                $this->db->insert('_componentes', $data_to_store4);  ///// insertando datos a la tabla componentes
                            }
                        }
                    }
                }
                /*==========================================================================================================================*/

                if($tp==2 || $tp==3 || $tp==4)
                {
                    $update_fase = array('pfec_ptto_fase' => $this->input->post('mtotal'));

                    $this->db->where('proy_id', $this->input->post('id'));
                    $this->db->update('_proyectofaseetapacomponente', $update_fase);
                }

                /*---------------------- redireccionar--------------------*/
                redirect('admin/proy/proyecto/'.$this->input->post('id').'/10');
            }
            else
            {
                redirect('admin/proy/edit/'.$this->input->post('id').'/1');
            }

        }

    }

    /*===================================== OBTIENE LOS DATOS DE LOS RESPONSABLES  =======================================*/
    public function get_responsables()
    {
        if($this->input->is_ajax_request() && $this->input->post())
        {
            $post = $this->input->post();
            $id = $post['id_p']; /// id proyecto
            $tp = $post['tp']; /// tipo de responsable
            $id = $this->security->xss_clean($id);
            $tp = $this->security->xss_clean($tp);
            $dato_resp = $this->model_proyecto->responsable_proy($id,$tp);
            //caso para modificar el codigo de proyecto y actividades
            $result = array();
            foreach($dato_resp as $row){
                $result = array(
                    'proy_id' => $row['proy_id'],
                    "fun_id" =>$row['fun_id'],
                    "responsable" =>$row['fun_nombre'].' '.$row['fun_paterno'].' '.$row['fun_materno']
                );
            }
            echo json_encode($result);
        }else{
            show_404();
        }
    }
    /*==================================================================================================================*/

    /*=========================================== ASIGNAR PROYECTO ====================================================*/
    function asignar_proyecto()
    {
        if($this->input->is_ajax_request() && $this->input->post()){
            $this->form_validation->set_rules('id_p', 'id del proyecto', 'required|trim');
            //=========================== mensajes =========================================
            $this->form_validation->set_message('required', 'El campo es es obligatorio');
            /*if ($this->form_validation->run() ) {*/
            $post = $this->input->post();
            $id = $post['id_p'];
            $tp = $post['tp'];
            //================ evitar enviar codigo malicioso ==========
            $id= $this->security->xss_clean($id);
            $est_proy= $this->security->xss_clean($tp);

            $update_proy = array('proy_estado' => $est_proy,
                'fun_id' => $this->session->userdata("fun_id"));
            $this->db->where('proy_id', $id);
            $this->db->update('_proyectos', $update_proy);

        }else{
            show_404();
        }
    }
    /*==================================================================================================================*/

    /*====================================== ELIMINAR PROYECTO =============================================================*/
    public function delete_proyecto(){
        if($this->input->is_ajax_request() && $this->input->post()){
            $post = $this->input->post();
            $proy_id = $post['proy_id'];

            $proyecto = $this->model_proyecto->get_id_proyecto($proy_id);
            $aperturas = $this->model_proyecto->mis_programas($proy_id);

            foreach($aperturas as $row)
            {
                $update_prog = array(
                    'aper_estado' => '3',
                    'fun_id' => $this->session->userdata("fun_id"));
                $this->db->where('aper_id', $row['aper_id']);
                $this->db->update('aperturaprogramatica', $update_prog);
            }

            /*----------------- ACTUALIZANDO ESTADO DEL PROYECTO ---------*/
            $update_proy = array(
                'estado' => '3',
                'fun_id' => $this->session->userdata("fun_id"));
            $this->db->where('proy_id', $proy_id);
            $this->db->update('_proyectos', $update_proy);
            /*------------------------------------------------------------*/
            /*----------------- ELIMINANDO PROY AREA -------------------*/
            $this->db->where('PROY_ID', $proy_id);
            $this->db->delete('proyectos_t_area');

            /*----------------- ELIMINANDO PROY LINEA -------------------*/
            $this->db->where('PROY_ID', $proy_id);
            $this->db->delete('proyectos_tp_linea');

            /*----------------- ELIMINANDO PROY PUNTO -------------------*/
            $this->db->where('PROY_ID', $proy_id);
            $this->db->delete('proyectos_tp_puntos');

            $sql = $this->db->get();

            if($this->db->query($sql)){
                echo $proy_id;
            }else{
                echo false;
            }
        }else{
            show_404();
        }
    }

    /*===================================== OBTIENE LOS DATOS DEL PROYECTO =======================================*/
    public function get_proyecto()
    {
        if($this->input->is_ajax_request() && $this->input->post())
        {
            $post = $this->input->post();
            $cod = $post['id_proy'];
            $id = $this->security->xss_clean($cod);
            $dato_proy = $this->model_proyecto->get_id_proyecto($id);
            //caso para modificar el codigo de proyecto y actividades
            foreach($dato_proy as $row){
                $result = array(
                    'proy_id' => $row['proy_id'],
                    "proy_nombre" =>$row['proy_nombre']
                );
            }
            echo json_encode($result);
        }else{
            show_404();
        }
    }
    /*===================================== OBTIENE LOS DATOS DEL PROYECTO =======================================*/
    /*===================================== OBTIENE LOS DATOS DE LA META =======================================*/
    public function get_meta()
    {
        if($this->input->is_ajax_request() && $this->input->post())
        {
            $post = $this->input->post();
            $id = $post['id_m'];
            $id = $this->security->xss_clean($id);
            $dato_meta = $this->model_proyecto->metas_id($id);
            //caso para modificar el codigo de proyecto y actividades
            foreach($dato_meta as $row){
                $result = array(
                    'meta_id' => $row['meta_id'],
                    "proy_id" =>$row['proy_id'],
                    "meta_descripcion" =>$row['meta_descripcion'],
                    "meta_meta" =>$row['meta_meta'],
                    "meta_ejec" =>$row['meta_ejec'],
                    "meta_efic" =>$row['meta_efic'],
                    "meta_rp"   =>$row['meta_rp']
                );
            }
            echo json_encode($result);
        }else{
            show_404();
        }
    }
    /*====================================================================================================================*/

    /*============================================= PROYECTO OBSERVADO =============================================*/
    function add_obs()
    {
        if($this->input->is_ajax_request() && $this->input->post()){
            $this->form_validation->set_rules('id', 'id del proyecto', 'required|trim');
            $this->form_validation->set_rules('observacion', 'Observacion', 'required');
            //=========================== mensajes =========================================
            $this->form_validation->set_message('required', 'El campo es es obligatorio');
            /*if ($this->form_validation->run() ) {*/
            $post = $this->input->post();
            $id = $post['id'];
            $tp = $post['tp'];
            $obs = $post['observacion'];
            //================ evitar enviar codigo malicioso ==========
            $id= $this->security->xss_clean($id);
            $tp= $this->security->xss_clean($tp);
            $obs = $this->security->xss_clean(trim($obs));

            $update_proy = array('t_obs' => $tp,
                'proy_observacion' => strtoupper($obs),
                'proy_estado' => 1,
                'fun_id' => $this->session->userdata("fun_id"));
            $this->db->where('proy_id', $id);
            $this->db->update('_proyectos', $update_proy);

        }else{
            show_404();
        }
    }
    /*====================================================================================================================*/
    function verif(){
        if($this->input->is_ajax_request())
        {
            $post = $this->input->post();
            $cod1 = $post['prog'];
            $cod2= $post['proy'];
            $cod3 = $post['act'];
            $variable= $this->model_proyecto->nro_aperturas_hijos($cod1,$cod2,$cod3,$this->session->userdata("gestion"));
            if($variable == true)
            {
                echo "true";; ///// no existe un CI registrado
            }
            else
            {
                echo "false";; //// existe el CI ya registrado
            }
        }else{
            show_404();
        }
    }

    /*=================================== PRIORIDAD DE LOS PROYECTOS ===============================================*/
    function prioridad_proyecto()
    {
        if($this->input->is_ajax_request() && $this->input->post()){
            $this->form_validation->set_rules('id', 'id proyecto', 'required|trim');
            //=========================== mensajes =========================================
            $this->form_validation->set_message('required', 'El campo es es obligatorio');
            /*if ($this->form_validation->run() ) {*/
            $post = $this->input->post();
            $id = $post['id'];
            $pr = $post['pr'];
            //================ evitar enviar codigo malicioso ==========
            $id= $this->security->xss_clean($id);
            $pr= $this->security->xss_clean($pr);

            $update_proy = array(
                'proy_pr' => $pr,
            );
            $this->db->where('proy_id', $id);
            $this->db->update('_proyectos', $update_proy);


        }else{
            show_404();
        }
    }
    /*======================================================================================================*/
    /*============================================= FICHA TECNICA DE PRODUCTOS =========================================*/
    private $estilo_vertical = '<style>
        body{
            font-family: sans-serif;
            }
        @page {
        margin: 40px 40px;
        }
        header {
            position: fixed;
            left: 0px;
            top: -20px;
            right: 0px;
            height: 20px;
            background-color: #fff;
            }
        header h1{
            margin: 5px 0;
        }
        header h2{
            margin: 0 0 10px 0;
        }
        th{
                padding: 1px;
                text-align: center;
                font-size: 8px;
                color: #ffffff;
            }
        footer {
            position: fixed;
            left: 0px;
            bottom: -82px;
            right: 0px;
            height: 100px;
            border-bottom: 1px solid #ddd;
        }
        footer .page:after {
            content: counter(page) ;
        }
        footer table {
            width: 100%;
        }
        footer p {
            text-align: right;
        }
        footer .izq {
            text-align: left;
        }
        table{
            font-size: 7px;
            width: 100%;
            background-color:#fff;
        }
        th, td {
                padding: 1px;
                text-align: center;
                font-size: 7px;
            }
        .mv{font-size:10px;}
        .siipp{width:180px;}
        .header_table {
            color: #ffffff;
            text-align: center;
            align-items: center;
            align-self: center;
            font-weight: bold;
            background-color: #696969;
            align-content: center;
        }
        .header_subtable {
            color: #ffffff;
            text-align: center;
            align-items: center;
            align-self: center;
            font-weight: bold;
            background-color: #a59393;
            align-content: center;
        }
        .titulo_pdf {
            text-align: left;
            font-size: 10px;
        }
        .datos_principales {
            text-align: center;
            font-size: 9px;
        }
        .pdes_titulo{
            text-decoration: underline;
            font-weight: bold;
            text-transform: uppercase;
        }
        .td_pdes{
            text-align: left;  
        }

        .indi_desemp{
            font-size: 14px;
            border: 1px solid #CCC;
            background-color: #E0E0E0;
            padding: .5em;
        }
        table.fixed { table-layout:fixed; }
        table.fixed td { overflow: hidden; }
        .sub_table{
            border-bottom:1px solid black;
        }
        .contenedor_principal{
            border-style: solid;
            border-width:1px;
        }
        .titulo_dictamen{
            padding:4px;
            background-color: #454545;
            border-style: solid;margin: 2px;
            border-width: 1px;
            color: white;
            font-size: 10px;
            border-color: black;
        }
        .contenedor_datos{
            padding:0px;
            margin:1px;
        }
        .table_contenedor{
            border-style: solid;
            margin: 0px;
            padding: 1px;
            border-width: 1px;
            border-collapse: collapse;
        }
        .collapse_t td{
            border-style: solid;
            margin: 0px;
            padding: 3px;
            border-width: 1px;
            border-collapse: collapse;
        }
        .fila_unitaria{
            text-align:left;
        }
        .lista{
            text-align:left;
            padding-left: 8;
            list-style-type:square;
            margin:2px;
        }
    </style>';
    /*---------------------------------- PDF HISTORIAL DE USUARIOS -------------------------------------*/
    public function historial_usuarios($proy_id){
        $gestion = $this->session->userdata('gestion');
        $dictamen = $this->model_objetivo->get_dictamen($proy_id);
        $history = $this->model_proyecto->historial_usuario($proy_id);
        $unidad_responsable=$this->model_proyecto->responsable_proy($proy_id,1);
        $dictamen = $dictamen->row();
        $aper_programatica = $this->model_objetivo->get_aper_programatica_dictamen($proy_id,$gestion);
        $aper_programatica = $aper_programatica->row();

        $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=10%;>
                            <img src="'.base_url().'assets/img/logo.jpg" class="siipp"/>
                        </td>
                        <td width=50%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata('entidad').'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$gestion.'<br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                            <b>FORMULARIO : </b> RESPONSABLES DE LA OPERACI&Oacute;N <br>
                        </td>
                        <td width=30%;>
                            <img src="'.base_url().'assets/img/escudo.jpg" alt="" width="100px">
                        </td>
                    </tr>
                </table>

                
                <footer>
                    <table class="table table-bordered" width="100%">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata('sistema_pie').'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>
                <table class="datos_principales" style="table-layout:fixed;" border="1">
                    <tr>
                        <td colspan="2">
                            <b>'.$dictamen->proy_nombre.'</b><br>';
        if($dictamen->tp_id==1)
        {$html .= 'Proyecto/Actividad';}
        elseif ($dictamen->tp_id==2 || $dictamen->tp_id==3)
        {$html .= 'Programa';}
        elseif ($dictamen->tp_id==4)
        {$html .= 'Acci&oacute;n de Funcionamiento';}
        $html .= '
                        </td>
                    </tr>
                    <tr>
                        <td style="width:50%;">';
        if($dictamen->tp_id==1)
        {$html .= '<b>'.$dictamen->proy_sisin.'</b><br>';}
        elseif ($dictamen->tp_id==2 || $dictamen->tp_id==3 || $dictamen->tp_id==4)
        {$html .= '<b>N/A</b><br>';}
        $html .= 'SISIN
                        </td>
                        <td style="width:50%; text-align:center;">
                            <b>'.$aper_programatica->programatica.'</b><br>
                            Apertura Programatica
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align:center;">
                            <b>'.$aper_programatica->uni_unidad.'</b><br>
                            Unidad Responsable
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align:center;">
                            <b>'.$unidad_responsable[0]['uresp'].'</b><br>
                            Unidad Ejecutora
                        </td>
                    </tr>
                </table>
                
                <br>
                <div class="contenedor_principal">
                    <div class="titulo_dictamen">RESPONSABLES T&Eacute;CNICOS DE LA OPERACI&Oacute;N</div>
                    <div class="contenedor_datos">
                      <table class="table_contenedor">
                      <thead>                         
                          <tr class="collapse_t" style="background-color:#ac9fae;font-weight: bold;">
                              <td style="width:1%;">Nro.</td>
                              <td style="width:5%;">NOMBRES</td>
                              <td style="width:5%;">APELLIDOS</td>
                              <td style="width:5%;">USUARIO</td>
                              <td style="width:5%;">FECHA DE REGISTRO</td>
                              <td style="width:5%;">TELEFONO / CEL</td>
                              <td style="width:5%;">DOMICILIO</td>
                          </tr>';
        $num=1;
        foreach ($history as $row)
        {
            $html.='
                                <tr>
                                    <td style="width:1%;">'.$num.'</td>
                                    <td style="width:5%;">'.$row['fun_nombre'].'</td>
                                    <td style="width:5%;">'.$row['fun_paterno'].''.$row['fun_materno'].'</td>
                                    <td style="width:5%;">'.$row['fun_usuario'].'</td>
                                    <td style="width:5%;">'.$row['pfun_fecha'].'</td>
                                    <td style="width:5%;">'.$row['fun_telefono'].'</td>
                                    <td style="width:5%;">'.$row['fun_domicilio'].'</td>
                              </tr>';
            $num++;
        }

        $html.='
                        </thead>                      
                    </table>
                    </div>
                    <table class="table_contenedor" style="margin-top: 10px;>
                        <tr>
                            <td class="fila_unitaria">FIRMAS:</td>
                        </tr>
                    </table>
                    <table style="width: 80%;margin: 0 auto;margin-top: 100px;margin-bottom: 50px;">
                        <tr>
                            <td style="width:30%">
                                <hr>
                            </td>
                            <td style="width:3%"></td>
                            <td style="width:30%">
                                <hr>
                            </td>
                            <td style="width:3%"></td>
                            <td style="width:30%">
                                <hr>
                            </td>
                        </tr>
                    </table>

                </div>
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'portrait');
        $dompdf->render();
        $dompdf->stream("dictamen_proyecto.pdf", array("Attachment" => false));
    }

    function rolfun($rol)
    {
        $valor=false;
        for ($i=1; $i <=count($rol) ; $i++) {
            $data = $this->Users_model->get_datos_usuario_roles($this->session->userdata('fun_id'),$rol[$i]);
            if(count($data)!=0){
                $valor=true;
                break;
            }
        }
        return $valor;
    }

    function rolfunn($tp_rol)
    {
        $valor=false;
        $data = $this->Users_model->get_datos_usuario_roles($this->session->userdata('fun_id'),$tp_rol);
        if(count($data)!=0){
            $valor=true;
        }
        return $valor;
    }

    function rol_modifica(){
        $valor=true;
        $roles=$this->Users_model->get_datos_usuario_rol_modif_datos($this->session->userdata('fun_id'));
        foreach($roles as $row){
            if($row['r_id']==3){
                $valor=false;
                break;
            }
        }
        return $valor;
    }
}