<?php

class nueva_session extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Users_model', '', true);
        $this->load->model('model_pei');
        $this->load->model('model_gestion');
        $this->load->library('session');
    }
    public function cambiar_gestion_h($value='')
    {
        $gestion_usuario = strtoupper($this->input->post('gestion_usu'));
        $fun_id = strtoupper($this->input->post('fun_id'));
        $fun=$this->model_gestion->f_id($fun_id);
        echo $user_name=strtoupper($fun[0]['fun_usuario']);
        echo $password=strtoupper($fun[0]['fun_password']);
        $this->load->model('Users_model');
        $is_valid = $this->Users_model->validate($user_name, $password);
        $data = $this->Users_model->datos_usuario($user_name, $password);
        $gestion = $this->Users_model->obtener_gestion();
        $mensaje="usted cambio la gestion actual"; 
        if ($is_valid == true) {
            $data = array(
                'user_name' => $user_name,
                'funcionario' => $data[0]['fun_nombre'] . " " . $data[0]['fun_paterno'],
                'usuario' => $data[0]['fun_usuario'],
                'fun_id' => $data[0]['fun_id'],
                'uni_id' => $data[0]['uni_id'],
                'unidad' => $data[0]['uni_unidad'],
                'boton_id' => $data[0]['b_id'],
                'rol_id' => $data[0]['r_id'],
                'gestion' => $gestion_usuario,
                'mes' => $gestion[0]['conf_mes'],
                'mensaje' => $mensaje,        
            );
            $this->session->set_userdata($data);
            echo "entro a la sesion";
            echo $this->session->userdata("gestion"); 
            redirect('admin/dashboard');
        }else{
            echo "No Entro a la Sesión";
            $data['message_error'] = 'USuario / Contraseña Incorrecto';
            $this->load->view('admin/login', $data);
        }
    }
    public function cambiar_gestion()
    {
        $nueva_gestion = strtoupper($this->input->post('gestion_usu'));
        $this->session->set_userdata('gestion', $nueva_gestion);
        echo "
            <script>
                alert('La Operacion se realizo con exito');
            </script>
        ";
        redirect('cambiar_gestion','refresh');
    }
    
}