<?php
define("DOMPDF_ENABLE_REMOTE", true);
define("DOMPDF_TEMP_DIR", "/tmp");
class C_social extends CI_Controller {  
  public $rol = array('1' => '1','2' => '8');
  public function __construct ()
    {
        parent::__construct();
        $this->load->library('pdf2');
        $this->load->model('menu_modelo');
        $this->load->model('Users_model','',true);
        $this->load->model('programacion/model_faseetapa');
        $this->load->model('reportes/model_objetivo');
        $this->load->model('programacion/model_proyecto');
        $this->load->model('programacion/model_componente');
        $this->load->model('programacion/model_producto');
        $this->load->model('programacion/model_actividad');
        $this->load->model('mantenimiento/mapertura_programatica');
        $this->load->model('mantenimiento/munidad_organizacional');
        $this->load->library("security");
        $this->load->library('session');
//        if($this->session->userdata('fun_id')!=null){ //VERSION 1.0 ACCESO RESTRINGIDO
//        if($this->session->userdata('gestion')!='' && $this->session->userdata('mes')!= ''){ //VERSION 1.1 ACCESO PUBLICO
    }

    /*================================= MENU - CONTROL SOCIAL ==================================*/
    public function menu()
    {
     if($this->session->userdata('gestion')!='0' && $this->session->userdata('mes')!= '0'){ //VERSION 1.1 ACCESO PUBLICO
        $data['mod']=1;
        $data['programas']=$this->list_programas(0,1); ///// Todos
        $data['proyectos_inversion']=$this->list_programas(1,1); ///// Proyectos de Inversion
        $data['programas_recurrentes']=$this->list_programas(2,1); ///// Programas Recurrentes
        $data['programas_nrecurrentes']=$this->list_programas(3,1); ///// Programas No Recurrentes
        $data['operacion_funcionamiento']=$this->list_programas(4,1); ///// Operacion de Funcionamiento
        
        $this->load->view('admin/control_social/menu_controlsocial', $data);
      }
      else{
		$this->session->set_flashdata('error','ERROR: Mes y gestión no válidos para acceder a <b>Control Social</b>');
        redirect('/','refresh');
      }
    }


    /*================================= MIS OPERACIONES INSTITUCIONALES ==================================*/
    public function mis_operaciones()
    {
      if($this->rolfun($this->rol)){  
        $data['mod']=1;
        $data['programas']=$this->list_programas(0,1); ///// Todos
        $data['proyectos_inversion']=$this->list_programas(1,1); ///// Proyectos de Inversion
        $data['programas_recurrentes']=$this->list_programas(2,1); ///// Programas Recurrentes
        $data['programas_nrecurrentes']=$this->list_programas(3,1); ///// Programas No Recurrentes
        $data['operacion_funcionamiento']=$this->list_programas(4,1); ///// Operacion de Funcionamiento
        
        $this->load->view('admin/control_social/mis_operaciones/operaciones', $data);
      }
      else{
        redirect('/','refresh');
      }
    }

    /*================================= SELECT RESUMEN EJECUCION FISICA ==================================*/
    public function ejecucion_fisica()
    {
      if($this->rolfun($this->rol)){   
        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];
        $this->load->view('admin/control_social/fisica/select_operaciones', $data);
      }
      else{
        redirect('/','refresh');
      }
    }


    /*================================= SELECT RESUMEN EJECUCION FINANCIERA ==================================*/
    public function ejecucion_financiera()
    {
      if($this->rolfun($this->rol)){  
        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];
        $this->load->view('admin/control_social/financiera/select_operaciones', $data);
      }
      else{
        redirect('/','refresh');
      }
    }

    /*======================= LISTA DE OPERACIONES TOP ==========================*/
    public function list_programas($tp,$tp_rep)
    {
       if($tp==0){
        $id ='id="dt_basic"';
        $titulo='TODOS LAS OPERACIONES INSTITUCIONALES';
      }
      elseif ($tp==1) {
        $id ='id="dt_basic1"';
        $titulo='PROYECTOS DE INVERSI&Oacute;N';
      }
      elseif ($tp==2) {
        $id ='id="dt_basic2"';
        $titulo='PROGRAMAS RECURRENTES';
      }
      elseif ($tp==3) {
        $id ='id="dt_basic3"';
        $titulo='PROGRAMAS NO RECURRENTES';
      }
      elseif ($tp==4) {
        $id ='id="dt_basic4"';
        $titulo='OPERACI&Oacute;N DE FUNCIONAMIENTO';
        }

      $lista_aper_padres = $this->model_proyecto->list_prog();//lista de aperturas padres 
      $tabla ='';
      if($tp_rep==1){
        $tb=''.$id.' class="table1 table table-bordered" width="100%"';
        }
        elseif ($tp_rep==2) {
          $tb='border="0" cellpadding="0" cellspacing="0" class="tabla"';
        }
      $tabla .='
        <table '.$tb.'>
          <thead>
		  <tr class="modo1">
                <th>';
                if($tp_rep==1){
                  $tabla .='
                  <div class="dropdown">
                    <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenu1" style="width:100%;" data-toggle="dropdown" aria-expanded="true">
                      OPCIONES
                      <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                      <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:abreVentana(\''.site_url("admin").'/imprimir_operaciones/'.$tp.'\');" title="IMPRIMIR PDF - '.$titulo.'">IMPRIMIR '.$titulo.'</a></li>
                     
                    </ul>
                  </div>';
                }
                $tabla .='
                </th>
                <th>CATEGORIA PROGRAM&Aacute;TICA '.$this->session->userdata("gestion").'</th>
                <th>PROYECTO_PROGRAMA_OPERACI&Oacute;N INSTITUCIONAL</th>
                <th>TIPO DE OPERACI&Oacute;N</th>
                <th>C&Oacute;DIGO_SISIN</th>
                <th>RESPONSABLE UNIDAD EJECUTORA</th>
                <th>UNIDAD EJECUTORA</th>
                <th>UNIDAD RESPONSABLE</th>
                <th>NUEVO-CONTINUO</th>
                <th>ANUAL-PLURIANUAL</th>
                <th>COSTO TOTAL DEL PROYECTO</th>
                <th>PRESUPUESTO REQUERIDO '.$this->session->userdata("gestion").'</th>
                <th>PRESUPUESTO TOTAL ASIGNADO '.$this->session->userdata("gestion").'</th>
                <th>LOCALIZACI&Oacute;N</th>
            </tr>
                </thead>
                <tbody>';
                foreach($lista_aper_padres  as $rowa)
                {
                $proyectos=$this->model_proyecto->list_proy_prog_ope($rowa['aper_programa'],$tp);
                if(count($proyectos)!=0)
                {
                  $tabla .='<tr bgcolor="#99DDF0" class="modo1" title="CATEGORIA PROGRAM&Aacute;TICA PADRE">';
                    $tabla .='<td></td>';
                    $tabla .='<td><center>' .$rowa['aper_programa'].''.$rowa['aper_proyecto'].''.$rowa['aper_actividad'].'</center></td>';
                    $tabla .='<td>'.$rowa['aper_descripcion'].'</td>';
                    $tabla .='<td>'.$rowa['aper_sisin'].'</td>';
                    $tabla .='<td></td>';
                    $tabla .='<td></td>';
                    $tabla .='<td></td>';
                    $tabla .='<td></td>';
                    $tabla .='<td></td>';
                    $tabla .='<td></td>';
                    $tabla .='<td></td>';
                    $tabla .='<td></td>';
                    $tabla .='<td></td>';
                    $tabla .='<td></td>';
                  $tabla .='</tr>';
                  foreach($proyectos  as $row)
                  {
                    if($row['tp_id']!=1){$color='#D7FCF9';}else{$color='#ffffff';}
                    $fase = $this->model_faseetapa->get_id_fase($row['proy_id']);
                    $tabla .= '<tr bgcolor='.$color.' class="modo1" title="'.$row['tp_tipo'].'">';
                      $tabla .= '<td>';
                      if($tp_rep==1){
                        if(count($fase)!=0){
                        $tabla .= '<center><a href="javascript:abreVentana(\''.site_url("admin").'/dictamen_proyecto/'.$row['proy_id'].'/'.$fase[0]['pfec_ejecucion'].'\');" title="REGISTRO DE LA OPERACION"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="35" HEIGHT="35"/><br>Registro</a></center>';
                        }
                        else{
                          $tabla .= '<center><a href="javascript:abreVentana(\''.site_url("admin").'/dictamen_proyecto/'.$row['proy_id'].'\');" title="REGISTRO DE LA OPERACION"><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="35" HEIGHT="35"/><br>Registro</a></center>';
                        }
                      }
                      $tabla .= '</td>';
                      $tabla .= '<td>';
                      if($row['aper_proyecto']!=''){
                        $tabla .= '<center>'.$row['aper_programa'].''.$row['aper_proyecto'].''.$row['aper_actividad'].'</center>';
                      }
                      else{
                        if($tp_rep==1){
                          $tabla .=  '<center><font color="red"><button class="btn btn-danger" title="RE ASIGNAR CATEGORIA PROGRAMATICA PARA LA GESTION '.$this->session->userdata('gestion').'">ASIGNAR<br>APERTURA<br>PROGRAM&Aacute;TICA<br>'.$this->session->userdata('gestion').'</button></font></center>';
                        }
                        else{
                          $tabla .=  '<center><font color="red">ASIGNAR<br>APERTURA<br>PROGRAM&Aacute;TICA<br>'.$this->session->userdata('gestion').'</font></center>';
                        }
                      }
                      
                      $tabla .= '</td>';
                      $tabla .= '<td>'.$row['proy_nombre'].'</td>';
                      $tabla .= '<td>'.$row['tp_tipo'].'</td>';
                      $tabla .= '<td>'.$row['proy_sisin'].'</td>';
                      $tabla .= '<td>'.$row['fun_nombre'].' '.$row['fun_paterno'].' '.$row['fun_materno'].'</td>';
                      $tabla .= '<td>'.$row['ue'].'</td>';
                      $tabla .= '<td>'.$row['ur'].'</td>';
                      if(count($fase)!=0){
                        $nc=$this->model_faseetapa->calcula_nc($fase[0]['pfec_fecha_inicio']); //// calcula nuevo/continuo
                        $ap=$this->model_faseetapa->calcula_ap($fase[0]['pfec_fecha_inicio'],$fase[0]['pfec_fecha_fin']);
                        $nro_fg_act = $this->model_faseetapa->nro_fasegestion_actual($fase[0]['id'],$this->session->userdata('gestion'));
                        $fase_gest = $this->model_faseetapa->fase_etapa_gestion($fase[0]['id'],$this->session->userdata("gestion"));
                        $tabla .='<td>'.$nc.'</td>';
                        $tabla .='<td>'.$ap.'</td>';
                        $tabla .='<td>'.number_format($fase[0]['pfec_ptto_fase'], 2, ',', '.').' Bs.</td>';
                        $tabla .='<td>';
                        if($nro_fg_act!=0 && ($fase_gest[0]['estado']==1 || $fase_gest[0]['estado']==2))
                        {
                          $tabla .=''.number_format($fase_gest[0]['pfecg_ppto_total'], 2, ',', '.').' Bs.';
                        }
                        elseif ($nro_fg_act==0) {
                          $tabla .= '<font color="red">la gestion no esta en curso</font>';
                        }
                        $tabla .='</td>';
                        $tabla .='<td>';
                        if($this->model_faseetapa->verif_fase_etapa_gestion($fase[0]['id'],$this->session->userdata("gestion"))!=0)
                        { 
                          if($this->model_faseetapa->nro_ffofet($fase_gest[0]['ptofecg_id'])!=0)
                          {
                            $techo=$this->model_faseetapa->techo_presupuestario($fase_gest[0]['ptofecg_id']);
                            $tabla .= ''.number_format($techo[0]['suma_techo'], 2, ',', '.').' Bs.';
                          }
                          else{$tabla .= "<font color=red>S/T</font>";}
                        }
                        $tabla .='</td>';
                      }
                      else{
                        $tabla .='<td bgcolor=#efb0b0><font color=red>Sin Fase</font></td>';
                        $tabla .='<td bgcolor=#efb0b0><font color=red>Sin Fase</font></td>';
                        $tabla .='<td bgcolor=#efb0b0><font color=red>Sin Fase</font></td>';
                        $tabla .='<td bgcolor=#efb0b0><font color=red>Sin Fase</font></td>';
                        $tabla .='<td bgcolor=#efb0b0><font color=red>Sin Fase</font></td>';
                      }

                      $tabla .='<td>';
                        $prov=$this->model_proyecto->proy_prov($row['proy_id']);
                        if(count($prov)!=0){
                            foreach($prov as $locali)
                            {
                              $tabla .='<li><b>'.strtoupper($locali['prov_provincia']).' </b>';
                                $muni=$this->model_proyecto->provincia_municipio($row['proy_id'],$locali['prov_id']);
                                $tabla .='(';
                                foreach($muni as $muni)
                                {
                                  $tabla .=''.$muni['muni_municipio'].',';
                                }
                                $tabla .=')';
                              $tabla .='</li>';
                            }
                        }
                        
                      $tabla .='</td>';
                    $tabla .= '</tr>';
                  } 
                }
              }

              $tabla .=
              '</tbody>
            </table>';
        return $tabla;
    }



        /*======================= LISTA DE OPERACIONES TOP ==========================*/
    public function list_programas_excel($tp)
    {
       if($tp==0){
        $titulo='TODOS LAS OPERACIONES INSTITUCIONALES';
      }
      elseif ($tp==1) {
        $titulo='PROYECTOS DE INVERSI&Oacute;N';
      }
      elseif ($tp==2) {
        $titulo='PROGRAMAS RECURRENTES';
      }
      elseif ($tp==3) {
        $titulo='PROGRAMAS NO RECURRENTES';
      }
      elseif ($tp==4) {
        $titulo='OPERACI&Oacute;N DE FUNCIONAMIENTO';
        }

      $lista_aper_padres = $this->model_proyecto->list_prog();//lista de aperturas padres 

      $tabla .='
        <table border="1" cellpadding="0" cellspacing="0" class="tabla">
          <thead>
            <tr >
                <th></th>
                <th>CATEGORIA PROGRAM&Aacute;TICA '.$this->session->userdata("gestion").'</th>
                <th>PROYECTO_PROGRAMA_OPERACI&Oacute;N INSTITUCIONAL</th>
                <th>TIPO DE OPERACI&Oacute;N</th>
                <th>C&Oacute;DIGO_SISIN</th>
                <th>RESPONSABLE UNIDAD EJECUTORA</th>
                <th>UNIDAD EJECUTORA</th>
                <th>UNIDAD RESPONSABLE</th>
                <th>NUEVO-CONTINUO</th>
                <th>ANUAL-PLURIANUAL</th>
                <th>COSTO TOTAL DEL PROYECTO</th>
                <th>PRESUPUESTO REQUERIDO '.$this->session->userdata("gestion").'</th>
                <th>PRESUPUESTO TOTAL ASIGNADO '.$this->session->userdata("gestion").'</th>
                <th>LOCALIZACI&Oacute;N</th>
            </tr>
              </thead>
                <tbody>';
                foreach($lista_aper_padres  as $rowa)
                {
                $proyectos=$this->model_proyecto->list_proy_prog_ope($rowa['aper_programa'],$tp);
                if(count($proyectos)!=0)
                {
                  $tabla .='<tr bgcolor="#99DDF0" class="modo1" title="CATEGORIA PROGRAM&Aacute;TICA PADRE">';
                    $tabla .='<td></td>';
                    $tabla .='<td><center>' .$rowa['aper_programa'].''.$rowa['aper_proyecto'].''.$rowa['aper_actividad'].'</center></td>';
                    $tabla .='<td>'.$rowa['aper_descripcion'].'</td>';
                    $tabla .='<td>'.$rowa['aper_sisin'].'</td>';
                    $tabla .='<td></td>';
                    $tabla .='<td></td>';
                    $tabla .='<td></td>';
                    $tabla .='<td></td>';
                    $tabla .='<td></td>';
                    $tabla .='<td></td>';
                    $tabla .='<td></td>';
                    $tabla .='<td></td>';
                    $tabla .='<td></td>';
                    $tabla .='<td></td>';
                  $tabla .='</tr>';
                  foreach($proyectos  as $row)
                  {
                    if($row['tp_id']!=1){$color='#D7FCF9';}else{$color='#ffffff';}
                    $fase = $this->model_faseetapa->get_id_fase($row['proy_id']);
                    $tabla .= '<tr bgcolor='.$color.' class="modo1" title="'.$row['tp_tipo'].'">';
                      $tabla .= '<td></td>';
                      $tabla .= '<td>';
                      if($row['aper_proyecto']!=''){
                        $tabla .= '<center>'.$row['aper_programa'].''.$row['aper_proyecto'].''.$row['aper_actividad'].'</center>';
                      }
                      else{
                        if($tp_rep==1){
                          $tabla .=  '<center><font color="red"><button class="btn btn-danger" title="RE ASIGNAR CATEGORIA PROGRAMATICA PARA LA GESTION '.$this->session->userdata('gestion').'">ASIGNAR<br>APERTURA<br>PROGRAM&Aacute;TICA<br>'.$this->session->userdata('gestion').'</button></font></center>';
                        }
                        else{
                          $tabla .=  '<center><font color="red">ASIGNAR<br>APERTURA<br>PROGRAM&Aacute;TICA<br>'.$this->session->userdata('gestion').'</font></center>';
                        }
                      }
                      
                      $tabla .= '</td>';
                      $tabla .= '<td>'.$row['proy_nombre'].'</td>';
                      $tabla .= '<td>'.$row['tp_tipo'].'</td>';
                      $tabla .= '<td>'.$row['proy_sisin'].'</td>';
                      $tabla .= '<td>'.$row['fun_nombre'].' '.$row['fun_paterno'].' '.$row['fun_materno'].'</td>';
                      $tabla .= '<td>'.$row['ue'].'</td>';
                      $tabla .= '<td>'.$row['ur'].'</td>';
                      if(count($fase)!=0){
                        $nc=$this->model_faseetapa->calcula_nc($fase[0]['pfec_fecha_inicio']); //// calcula nuevo/continuo
                        $ap=$this->model_faseetapa->calcula_ap($fase[0]['pfec_fecha_inicio'],$fase[0]['pfec_fecha_fin']);
                        $nro_fg_act = $this->model_faseetapa->nro_fasegestion_actual($fase[0]['id'],$this->session->userdata('gestion'));
                        $fase_gest = $this->model_faseetapa->fase_etapa_gestion($fase[0]['id'],$this->session->userdata("gestion"));
                        $tabla .='<td>'.$nc.'</td>';
                        $tabla .='<td>'.$ap.'</td>';
                        $tabla .='<td>'.number_format($fase[0]['pfec_ptto_fase'], 2, ',', '.').' Bs.</td>';
                        $tabla .='<td>';
                        if($nro_fg_act!=0 && ($fase_gest[0]['estado']==1 || $fase_gest[0]['estado']==2))
                        {
                          $tabla .=''.number_format($fase_gest[0]['pfecg_ppto_total'], 2, ',', '.').' Bs.';
                        }
                        elseif ($nro_fg_act==0) {
                          $tabla .= '<font color="red">la gestion no esta en curso</font>';
                        }
                        $tabla .='</td>';
                        $tabla .='<td>';
                        if($this->model_faseetapa->verif_fase_etapa_gestion($fase[0]['id'],$this->session->userdata("gestion"))!=0)
                        { 
                          if($this->model_faseetapa->nro_ffofet($fase_gest[0]['ptofecg_id'])!=0)
                          {
                            $techo=$this->model_faseetapa->techo_presupuestario($fase_gest[0]['ptofecg_id']);
                            $tabla .= ''.number_format($techo[0]['suma_techo'], 2, ',', '.').' Bs.';
                          }
                          else{$tabla .= "<font color=red>S/T</font>";}
                        }
                        $tabla .='</td>';
                      }
                      else{
                        $tabla .='<td bgcolor=#efb0b0><font color=red>Sin Fase</font></td>';
                        $tabla .='<td bgcolor=#efb0b0><font color=red>Sin Fase</font></td>';
                        $tabla .='<td bgcolor=#efb0b0><font color=red>Sin Fase</font></td>';
                        $tabla .='<td bgcolor=#efb0b0><font color=red>Sin Fase</font></td>';
                        $tabla .='<td bgcolor=#efb0b0><font color=red>Sin Fase</font></td>';
                      }

                      $tabla .='<td>';
                        $prov=$this->model_proyecto->proy_prov($row['proy_id']);
                        if(count($prov)!=0){
                            foreach($prov as $locali)
                            {
                              $tabla .='<li><b>'.strtoupper($locali['prov_provincia']).' </b>';
                                $muni=$this->model_proyecto->provincia_municipio($row['proy_id'],$locali['prov_id']);
                                $tabla .='(';
                                foreach($muni as $muni)
                                {
                                  $tabla .=''.$muni['muni_municipio'].',';
                                }
                                $tabla .=')';
                              $tabla .='</li>';
                            }
                        }
                        
                      $tabla .='</td>';
                    $tabla .= '</tr>';
                  } 
                }
              }

              $tabla .=
              '</tbody>
            </table>';
        return $tabla;
    }

    public function imprimir_mis_operaciones($tp)
    {
      if($tp==0){
        $programas=$this->list_programas(0,2); ///// Todos
        $titulo='PROYECTOS,PROGRAMAS,OPERACIONES INSTITUCIONALES';
      }
      elseif ($tp==1) {
        $programas=$this->list_programas(1,2); ///// Proyectos de Inversion
        $titulo='PROYECTOS DE INVERSI&Oacute;N PUBLICA';
      }
      elseif ($tp==2) {
        $programas=$this->list_programas(2,2); ///// Programas Recurrentes
        $titulo='PROGRAMAS RECURRENTES';
      }
      elseif ($tp==3) {
        $programas=$this->list_programas(3,2); ///// Programas No Recurrentes
        $titulo='PROGRAMAS NO RECURRENTES';
      }
      elseif($tp==4){
        $programas=$this->list_programas(4,2); ///// Operacion de Funcionamiento
        $titulo='OPERACI&Oacute;N DE FUNCIONAMIENTO';
      }
        
    $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr >
                        <td width=20%;>
                            <center><img src="'.base_url().'assets/img/logo.jpg" class="siipp"/></center>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata("entidad").'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> <br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                            <b>'.$titulo.'</b>
                        </td>
                        <td width=20%;>
                            <center><img src="'.base_url().'assets/img/escudo.jpg" alt="" width="90px"></center>
                        </td>
                    </tr>
                </table>
                <footer>
                    <table class="table table-bordered" width="100%">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata('sistema_pie').'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>
                <div>
                  '.$programas.'
                </div>
            </body>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'landscape');
        ini_set('memory_limit','-1');
        ini_set('max_execution_time', 0);
        $dompdf->render();
        $dompdf->stream("reporte_mis_operaciones.pdf", array("Attachment" => false));
    }

    public function exportar_mis_operaciones($tp)
    {
      //establecemos el timezone para obtener la hora local
      date_default_timezone_set('America/Lima');

      //la fecha de exportaci�n sera parte del nombre del archivo Excel
      $fecha = date("d-m-Y H:i:s");
      if($tp==0){
        $programas=$this->list_programas_excel(0); ///// Todos
        $titulo='PROYECTOS,PROGRAMAS,OPERACIONES INSTITUCIONALES';
      }
      elseif ($tp==1) {
        $programas=$this->list_programas(1); ///// Proyectos de Inversion
        $titulo='PROYECTOS DE INVERSI&Oacute;N PUBLICA';
      }
      elseif ($tp==2) {
        $programas=$this->list_programas(2); ///// Programas Recurrentes
        $titulo='PROGRAMAS RECURRENTES';
      }
      elseif ($tp==3) {
        $programas=$this->list_programas(3); ///// Programas No Recurrentes
        $titulo='PROGRAMAS NO RECURRENTES';
      }
      elseif($tp==4){
        $programas=$this->list_programas(4); ///// Operacion de Funcionamiento
        $titulo='OPERACI&Oacute;N DE FUNCIONAMIENTO';
      }
      //Inicio de exportaci�n en Excel
      header('Content-type: application/vnd.ms-excel');
      header("Content-Disposition: attachment; filename=Reporte_PAC_$fecha.xls"); //Indica el nombre del archivo resultante
      header("Pragma: no-cache");
      header("Expires: 0");
      echo "";
      echo "".$programas."";
    }

    function rolfun($rol)
    {
/*      $valor=false;
      for ($i=1; $i <=count($rol) ; $i++) { 
        $data = $this->Users_model->get_datos_usuario_roles($this->session->userdata('fun_id'),$rol[$i]);
        if(count($data)!=0){
          $valor=true;
          break;
        }
      }
      return $valor;*/
	  return true; // VERSION 1.1 SE RETORNA SIEMPRE TRUE PARA GARANTIZAR EL ACCESO PUBLICO
    }

    function rolfunn($tp_rol)
    {
      $valor=false;
      $data = $this->Users_model->get_datos_usuario_roles($this->session->userdata('fun_id'),$tp_rol);
      if(count($data)!=0){
        $valor=true;
      }
      return $valor;
    }

    /*============================================= FICHA TECNICA DE PRODUCTOS =========================================*/
    private $estilo_vertical = 
    '<style>
        body{
            font-family: sans-serif;
            }
        @page {
        margin: 40px 40px;
        }
        header {
            position: fixed;
            left: 0px;
            top: -20px;
            right: 0px;
            height: 20px;
            background-color: #fff;
            }
        header h1{
            margin: 5px 0;
        }
        header h2{
            margin: 0 0 10px 0;
        }

        footer {
            position: fixed;
            left: 0px;
            bottom: -82px;
            right: 0px;
            height: 100px;
            border-bottom: 1px solid #ddd;
        }
        footer .page:after {
            content: counter(page) ;
        }
        footer table {
            width: 100%;
        }
        footer p {
            text-align: right;
        }
        footer .izq {
            text-align: left;
        }
    table{
            font-size: 7px;
            width: 100%;
            background-color:#fff;
        }
        .mv{font-size:9px;}
        .siipp{width:120px;}

    .titulo_pdf {
                text-align: left;
                font-size: 10px;
            }
            .tabla {
    font-family: Verdana, Arial, Helvetica, sans-serif;
    font-size: 7px;
                width: 100%;

    }
    .tabla th {
    padding: 5px;
    font-size: 7px;
    background-color: #83aec0;
    background-image: url(fondo_th.png);
    background-repeat: repeat-x;
    color: #FFFFFF;
    border-right-width: 1px;
    border-bottom-width: 1px;
    border-right-style: solid;
    border-bottom-style: solid;
    border-right-color: #558FA6;
    border-bottom-color: #558FA6;
    font-family: "Trebuchet MS", Arial;
    text-transform: uppercase;
    }
    .tabla .modo1 {
    font-size: 7px;
    font-weight:bold;

    background-image: url(fondo_tr01.png);
    background-repeat: repeat-x;
    color: #34484E;
    font-family: "Trebuchet MS", Arial;
    }
    .tabla .modo1 td {
    padding: 5px;
    border-right-width: 1px;
    border-bottom-width: 1px;
    border-right-style: solid;
    border-bottom-style: solid;
    border-right-color: #A4C4D0;
    border-bottom-color: #A4C4D0;

    }
    </style>';

    public function get_mes($mes_id)
    {
      $mes[1]='ENERO';
      $mes[2]='FEBRERO';
      $mes[3]='MARZO';
      $mes[4]='ABRIL';
      $mes[5]='MAYO';
      $mes[6]='JUNIO';
      $mes[7]='JULIO';
      $mes[8]='AGOSTO';
      $mes[9]='SEPTIEMBRE';
      $mes[10]='OCTUBRE';
      $mes[11]='NOVIEMBRE';
      $mes[12]='DICIEMBRE';

      $dias[1]='31';
      $dias[2]='28';
      $dias[3]='31';
      $dias[4]='30';
      $dias[5]='31';
      $dias[6]='30';
      $dias[7]='31';
      $dias[8]='31';
      $dias[9]='30';
      $dias[10]='31';
      $dias[11]='30';
      $dias[12]='31';

      $valor[1]=$mes[$mes_id];
      $valor[2]=$dias[$mes_id];

      return $valor;
    }
}