<?php

class Cprog_insumos_directo extends CI_Controller
{
    var $gestion;
    var $fun_id;
    public $rol = array('1' => '3','2' => '4','3' => '1');
    public function __construct ()
    {
        parent::__construct();
        if($this->session->userdata('fun_id')!=null){
            $this->load->model('Users_model','',true);
            if($this->rolfun($this->rol)){
                $this->load->model('menu_modelo');
                $this->load->model('programacion/insumos/minsumos_partida');
                $this->load->model('programacion/insumos/minsumos');
                $this->load->model('mantenimiento/model_partidas');
                $this->load->model('mantenimiento/model_entidad_tras');
                $this->load->model('programacion/insumos/minsumos_delegado');
                $this->load->model('programacion/model_proyecto');
                $this->load->model('programacion/model_faseetapa');

                $this->load->model('programacion/model_componente');
                $this->gestion = $this->session->userData('gestion');
                $this->fun_id = $this->session->userData('fun_id');
            }else{
                redirect('admin/dashboard');
            }
        }
        else{
            redirect('/','refresh');
        }
    }


    //PROGRAMACION DE INSUMOS A NIVEL DE ACTIVIDADES
    function prog_isumos_act($proy_id, $prod_id, $act_id,$insp_id)
    {
        if($proy_id!='' & $prod_id!='' & $act_id!=''){
            $enlaces=$this->menu_modelo->get_Modulos_programacion(1);
            $data['enlaces'] = $enlaces;
            $data['nro_fase']= $this->model_faseetapa->nro_fase($proy_id); /// nro de fases y etapas registrados
            $data['id_f'] = $this->model_faseetapa->get_id_fase($proy_id); //// DATOS DE LA FASE ACTIVA
            $data['mod']=4;

            $data['proyecto'] = $this->model_proyecto->get_id_proyecto($proy_id);
            $data['fase'] = $this->model_faseetapa->get_id_fase($proy_id); //// DATOS DE LA FASE ACTIVA
            $titulo_proy=strtoupper($data['proyecto'][0]['tipo']);
            $data['titulo_proy'] = $titulo_proy;

            $data['proy_id'] = $proy_id;
            $data['prod_id'] = $prod_id;
            $data['act_id'] = $act_id;
            $data['insp_id'] = $insp_id;
            $data['dato_proy'] = $this->minsumos->dato_proyecto($proy_id);
            $data['dato_prod'] = $this->minsumos->dato_producto($prod_id);
            $data['dato_act'] = $this->minsumos->dato_actividad($act_id);

            if(count($data['dato_act'])!=0 & count($data['dato_prod'])!=0)
            {
                $data['tabla_fuentes'] = $this->fuentes_financiamientos($proy_id, $this->gestion,1); //// Tabla lista de Activos del Componente
                $data['sumatorias'] = $this->suma_total_fuentes($data['fase'][0]['id'], $this->gestion); //// Suma Total, Asignado, Programado, Saldo
                $lista_insumos = $this->minsumos->lista_insumos($act_id,$insp_id);

                $tabla = '';
                $cont = 0;
                $total_insumos=0;
                foreach ($lista_insumos as $row) {
                    $cont++;
                    $tabla .= '<tr>';
                    $tabla .= '<td>'.$cont.'</td>';
                    $tabla .= '<td>'.$row['ins_codigo'].'</td>';
                    $tabla .= '<td>'.$row['ti_nombre'].'</td>';
                    $tabla .= '<td>'.date('d-m-Y',strtotime($row['ins_fecha_requerimiento'])).'</td>';
                    $tabla .= '<td>'.$row['ins_detalle'] .'</td>';
                    $tabla .= '<td>'.$row['par_codigo'] .'</td>';
                    $tabla .= '<td>'.strtoupper($row['par_nombre']) .'</td>';
                    $tabla .= '<td>'.$row['ins_cant_requerida'] .'</td>';
                    $tabla .= '<td>'.number_format($row['ins_costo_unitario'], 2, ',', '.') .'</td>';
                    $tabla .= '<td>'.number_format($row['ins_costo_total'], 2, ',', '.') .'</td>';
                    $tabla .= '<td><div class="btn-group">
                                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-list"></i> ACCIONES <span class="caret"></span></button>
                                    <ul class="dropdown-menu" role="menu">
                     <li><a href="'.site_url("prog").'/mod_ins_a/'.$row['ins_tipo'].'/'.$proy_id.'/'.$prod_id.'/'.$act_id.'/'.$row['ins_id'].'/'.$this->gestion.'/'.$row['insp_id'].'" class="mod_aper" title="MODIFICAR INSUMO"><i class="glyphicon glyphicon-pencil"></i> Modificar</a></li>
                     <li><a href="#" data-toggle="modal" data-target="#modal_del_ff" class="del_ff" title="ELIMINAR INSUMO" id="'.$row['ins_tipo'].'" name="'.$row['ins_id'].'"><i class="glyphicon glyphicon-trash"></i> Eliminar</a></li>
                 </ul>
                 </div>
                 </td>';
                    $tabla .= '</tr>';
                    $total_insumos+=$row['ins_costo_total'];
                }
                $data['lista_insumos'] = $tabla;
                $data['total_insumos'] = $total_insumos;
                $data['lista_tipo_insumo'] = $this->minsumos->lista_tipo_insumo();
                $data['atras']='prog/ins/'.$proy_id.'/'.$data['fase'][0]['pfec_ejecucion'];
                $this->load->view('admin/programacion/insumos/insumo_actividades/ins_actividad', $data);
            }
            else{
                redirect('admin/dashboard');
                //redirect('admin/prog/list_act/1/'.$data['fase'][0]['id'].'/'.$proy_id.'/'.$data['dato_prod']->com_id.'/'.$prod_id.'/'.$act_id.'');
            }
        }
        else{
            redirect('admin/dashboard');
        }
    }

    /*------------------- IMPORTAR ARCHIVO REQUERIMIENTO -----------------------*/
    function importar_archivo_requerimiento()
    {
        if ($this->input->post()) {
            $post = $this->input->post();
            $proy_id = $post['proy_id'];
            $pfec_id = $post['pfec_id'];
            $prod_id = $post['prod_id'];
            $act_id = $post['act_id'];

            $tipo = $_FILES['archivo']['type'];
            $tamanio = $_FILES['archivo']['size'];
            $archivotmp = $_FILES['archivo']['tmp_name'];

            $filename = $_FILES["archivo"]["name"];
            $file_basename = substr($filename, 0, strripos($filename, '.'));
            $file_ext = substr($filename, strripos($filename, '.'));
            $allowed_file_types = array('.csv');
            if (in_array($file_ext, $allowed_file_types) && ($tamanio < 90000000)) {

                $lineas=$this->subir_requerimientos($proy_id,$pfec_id,$prod_id,$act_id,$archivotmp);

                $this->session->set_flashdata('success','SE IMPORTARON Y SE REGISTRARON '.$lineas.' REQUERIMIENTOS');
                redirect(site_url("") . '/prog/ins_act/'.$proy_id.'/'.$prod_id.'/'.$act_id.'/true');
            }
            elseif (empty($file_basename)) {
                echo "<script>alert('SELECCIONE ARCHIVO .CSV')</script>";
            }
            elseif ($filesize > 100000000) {
                //redirect('');
            }
            else {
                $mensaje = "Sólo estos tipos de archivo se permiten para la carga: " . implode(', ', $allowed_file_types);
                echo '<script>alert("' . $mensaje . '")</script>';
            }

        } else {
            show_404();
        }
    }


    public function subir_requerimientos($proy_id,$pfec_id,$prod_id,$act_id,$archivotmp)
    {
        $fase = $this->model_faseetapa->get_id_fase($proy_id); //// DATOS DE LA FASE ACTIVA
        $conf=$this->model_proyecto->configuracion(); /// Confirmacion
        $lineas = file($archivotmp);

        $i=0;
        $nro=0;
        //Recorremos el bucle para leer línea por línea
        foreach ($lineas as $linea_num => $linea)
        {
            if($i != 0)
            {
                $datos = explode(";",$linea);
                if(count($datos)==27){
                    $tp = (int)$datos[0]; //// Tipo de Insumo
                    $veriff = $this->verif_fecha($datos[5]);
                    if($tp>=is_numeric(1) & $tp<=is_numeric(9) & $datos[5]!='' & $veriff=='true')
                    {
                        $cod_partida = (int)$datos[1]; //// Codigo Partida
                        $desc_partida = utf8_encode($datos[2]); //// Descripcion Partida
                        $par_id = $this->minsumos->get_partida_codigo($cod_partida); //// DATOS DE LA FASE ACTIVA
                        $detalle = utf8_encode($datos[3]); //$datos[0]// detalle
                        $unidad = utf8_encode($datos[4]); ///// unidad de medida
                        $f_requerida = $datos[5]; //// Fecha requerida


                        $obj_perfil = utf8_encode($datos[6]); //// Objetivo , Perfil / consultorias
                        $duracion = (int)$datos[7]; //// Duracion / consultorias
                        if($datos[7]==''){
                            $duracion = 0; //// Duracion / consultorias
                        }

                        $fecha_inicio = $datos[8]; //// Fecha Inicio / consultorias
                        $veriff = $this->verif_fecha($fecha_inicio); //// verifica de Inicio
                        if($datos[8]=='' || $veriff=='false'){
                            $fecha_inicio =date('d/m/Y');
                        }

                        $fecha_final = $datos[9]; //// Fecha Inicio / consultorias
                        $veriff = $this->verif_fecha($fecha_final); //// verifica de Inicio
                        if($datos[9]=='' || $veriff=='false'){
                            $fecha_final =date('d/m/Y');
                        }

                        $evaluador = utf8_encode($datos[10]); //// Evaluador / consultorias
                        $cantidad = (int)$datos[11]; //// Cantidad
                        $unitario = (float)$datos[12]; //// Costo Unitario
                        $total = (float)$datos[13]; //// Costo Total
                        $observacion = utf8_encode($datos[26]); //// Observacion insumo
                        if(!is_numeric($unitario)){
                            if($cantidad!=0){
                                $unitario=round(($total/$unitario),2);
                            }
                        }
                        $var=14;
                        for ($i=1; $i <=12 ; $i++) {

                            $m[$i]=(float)$datos[$var]; //// Mes i
                            if($m[$i]==''){
                                $m[$i]=0;
                            }

                            $var++;
                        }

                        $valores_f_requerida = explode ("/", $f_requerida);
                        $gestion_requerimiento   = $valores_f_requerida[2]; //// Gestion del Requerimiento

                        $veriff = $this->verif_fecha($f_requerida); //// verifica de requerimiento
                        if($veriff=='true' & count($par_id)!=0) //// verif fecha de requerimiento
                        {
                            $nro++;
                            if($tp==1){
                                $nro_ins=$conf[0]['conf_rrhhp']+1;

                                $update_conf = array('conf_rrhhp' => $nro_ins);
                                $this->db->where('ide', $this->gestion);
                                $this->db->update('configuracion', $update_conf);

                                $query=$this->db->query('set datestyle to DMY');
                                $data_to_store = array(
                                    'ins_codigo' => $this->session->userdata("name").'/INS/RHP/'.$this->gestion.'/'.$nro_ins, /// Codigo Insumo
                                    'ins_fecha_requerimiento' => $f_requerida, /// Fecha de Requerimiento
                                    'ins_detalle' => strtoupper($detalle), /// Insumo Detalle
                                    'ins_cant_requerida' => $cantidad, /// Cantidad Requerida
                                    'ins_costo_unitario' => $unitario, /// Costo Unitario
                                    'ins_costo_total' => $total, /// Costo Total
                                    'ins_tipo' => $tp, /// Ins Tipo
                                    'ins_unidad_medida' => strtoupper($unidad), /// Insumo Unidad de Medida
                                    'par_id' => $par_id[0]['par_id'], /// Partidas

                                    'fun_id' => $this->session->userdata("fun_id"), /// Funcionario
                                );

                            }
                            elseif($tp==2){
                                $nro_ins=$conf[0]['conf_servicios']+1;

                                $update_conf = array('conf_servicios' => $nro_ins);
                                $this->db->where('ide', $this->gestion);
                                $this->db->update('configuracion', $update_conf);

                                $query=$this->db->query('set datestyle to DMY');
                                $data_to_store = array(
                                    'ins_codigo' => $this->session->userdata("name").'/INS/SERV/'.$this->gestion.'/'.$nro_ins, /// Codigo Insumo
                                    'ins_fecha_requerimiento' => $f_requerida, /// Fecha de Requerimiento
                                    'ins_detalle' => strtoupper($detalle), /// Insumo Detalle
                                    'ins_cant_requerida' => $cantidad, /// Cantidad Requerida
                                    'ins_costo_unitario' => $unitario, /// Costo Unitario
                                    'ins_costo_total' => $total, /// Costo Total
                                    'ins_tipo' => $tp, /// Ins Tipo
                                    'ins_unidad_medida' => strtoupper($unidad), /// Insumo Unidad de Medida
                                    'par_id' => $par_id[0]['par_id'], /// Partidas

                                    'fun_id' => $this->session->userdata("fun_id"), /// Funcionario
                                );

                            }
                            elseif($tp==3){
                                $nro_ins=$conf[0]['conf_pasajes']+1;

                                $update_conf = array('conf_pasajes' => $nro_ins);
                                $this->db->where('ide', $this->gestion);
                                $this->db->update('configuracion', $update_conf);

                                $query=$this->db->query('set datestyle to DMY');
                                $data_to_store = array(
                                    'ins_codigo' => $this->session->userdata("name").'/INS/PAS/'.$this->gestion.'/'.$nro_ins, /// Codigo Insumo
                                    'ins_fecha_requerimiento' => $f_requerida, /// Fecha de Requerimiento
                                    'ins_detalle' => strtoupper($detalle), /// Insumo Detalle
                                    'ins_cant_requerida' => $cantidad, /// Cantidad Requerida
                                    'ins_costo_unitario' => $unitario, /// Costo Unitario
                                    'ins_costo_total' => $total, /// Costo Total
                                    'ins_tipo' => $tp, /// Ins Tipo
                                    'ins_unidad_medida' => strtoupper($unidad), /// Insumo Unidad de Medida
                                    'par_id' => $par_id[0]['par_id'], /// Partidas

                                    'fun_id' => $this->session->userdata("fun_id"), /// Funcionario
                                );

                            }
                            elseif($tp==4){
                                $nro_ins=$conf[0]['conf_viaticos']+1;

                                $update_conf = array('conf_viaticos' => $nro_ins);
                                $this->db->where('ide', $this->gestion);
                                $this->db->update('configuracion', $update_conf);

                                $query=$this->db->query('set datestyle to DMY');
                                $data_to_store = array(
                                    'ins_codigo' => $this->session->userdata("name").'/INS/VIA/'.$this->gestion.'/'.$nro_ins, /// Codigo Insumo
                                    'ins_fecha_requerimiento' => $f_requerida, /// Fecha de Requerimiento
                                    'ins_detalle' => strtoupper($detalle), /// Insumo Detalle
                                    'ins_cant_requerida' => $cantidad, /// Cantidad Requerida
                                    'ins_costo_unitario' => $unitario, /// Costo Unitario
                                    'ins_costo_total' => $total, /// Costo Total
                                    'ins_tipo' => $tp, /// Ins Tipo
                                    'ins_unidad_medida' => strtoupper($unidad), /// Insumo Unidad de Medida
                                    'par_id' => $par_id[0]['par_id'], /// Partidas

                                    'fun_id' => $this->session->userdata("fun_id"), /// Funcionario
                                );

                            }
                            elseif($tp==5){
                                $nro_ins=$conf[0]['conf_cons_producto']+1;

                                $update_conf = array('conf_cons_producto' => $nro_ins);
                                $this->db->where('ide', $this->gestion);
                                $this->db->update('configuracion', $update_conf);

                                $query=$this->db->query('set datestyle to DMY');
                                $data_to_store = array(
                                    'ins_codigo' => $this->session->userdata("name").'/INS/CP/'.$this->gestion.'/'.$nro_ins, /// Codigo Insumo
                                    'ins_fecha_requerimiento' => $f_requerida, /// Fecha de Requerimiento
                                    'ins_detalle' => strtoupper($detalle), /// Insumo Detalle
                                    'ins_objetivo' => strtoupper($obj_perfil), /// Insumo Objetivo
                                    'ins_duracion' => $duracion, /// Insumo Duracion
                                    'ins_fecha_inicio' => $fecha_inicio, /// Fecha de Inicio
                                    'ins_fecha_conclusion' => $Fecha_final, /// Fecha Final

                                    'ins_evaluador' => strtoupper($evaluador), /// Insumo Evaluacion
                                    'ins_cant_requerida' => $cantidad, /// Cantidad Requerida
                                    'ins_costo_unitario' => $unitario, /// Costo Unitario
                                    'ins_costo_total' => $total, /// Costo Total
                                    'ins_tipo' => $tp, /// Ins Tipo
                                    'par_id' => $par_id[0]['par_id'], /// Partidas

                                    'fun_id' => $this->session->userdata("fun_id"), /// Funcionario
                                );

                            }
                            elseif($tp==6){
                                $nro_ins=$conf[0]['conf_cons_linea']+1;

                                $update_conf = array('conf_cons_linea' => $nro_ins);
                                $this->db->where('ide', $this->gestion);
                                $this->db->update('configuracion', $update_conf);

                                $query=$this->db->query('set datestyle to DMY');
                                $data_to_store = array(
                                    'ins_codigo' => $this->session->userdata("name").'/INS/CL/'.$this->gestion.'/'.$nro_ins, /// Codigo Insumo
                                    'ins_fecha_requerimiento' => $f_requerida, /// Fecha de Requerimiento
                                    'ins_duracion' => $duracion, /// Insumo Duracion
                                    'ins_fecha_inicio' => $fecha_inicio, /// Fecha de Inicio
                                    'ins_fecha_conclusion' => $Fecha_final, /// Fecha Final
                                    'ins_detalle' => strtoupper($detalle), /// Insumo actividades del consultor
                                    'ins_perfil' => strtoupper($obj_perfil), /// Insumo perfil del consultor
                                    'ins_evaluador' => strtoupper($evaluador), /// Insumo evaluador del consultor
                                    'car_id' => 0, /// cargo id
                                    'ins_cant_requerida' => $cantidad, /// Cantidad Requerida
                                    'ins_costo_unitario' => $unitario, /// Costo Unitario
                                    'ins_costo_total' => $total, /// Costo Total
                                    'ins_tipo' => $tp, /// Ins Tipo
                                    'par_id' => $par_id[0]['par_id'], /// Partidas

                                    'fun_id' => $this->session->userdata("fun_id"), /// Funcionario
                                );

                            }
                            elseif($tp==7){
                                $nro_ins=$conf[0]['conf_materiales']+1;

                                $update_conf = array('conf_materiales' => $nro_ins);
                                $this->db->where('ide', $this->gestion);
                                $this->db->update('configuracion', $update_conf);

                                $query=$this->db->query('set datestyle to DMY');
                                $data_to_store = array(
                                    'ins_codigo' => $this->session->userdata("name").'/INS/MAT/'.$this->gestion.'/'.$nro_ins, /// Codigo Insumo
                                    'ins_fecha_requerimiento' => $f_requerida, /// Fecha de Requerimiento
                                    'ins_detalle' => strtoupper($detalle), /// Insumo Detalle
                                    'ins_cant_requerida' => $cantidad, /// Cantidad Requerida
                                    'ins_costo_unitario' => $unitario, /// Costo Unitario
                                    'ins_costo_total' => $total, /// Costo Total
                                    'ins_tipo' => $tp, /// Ins Tipo
                                    'ins_unidad_medida' => strtoupper($unidad), /// Insumo Unidad de Medida
                                    'par_id' => $par_id[0]['par_id'], /// Partidas

                                    'fun_id' => $this->session->userdata("fun_id"), /// Funcionario
                                );

                            }
                            elseif($tp==8){
                                $nro_ins=$conf[0]['conf_activos']+1;

                                $update_conf = array('conf_activos' => $nro_ins);
                                $this->db->where('ide', $this->gestion);
                                $this->db->update('configuracion', $update_conf);

                                $query=$this->db->query('set datestyle to DMY');
                                $data_to_store = array(
                                    'ins_codigo' => $this->session->userdata("name").'/INS/AF/'.$this->gestion.'/'.$nro_ins, /// Codigo Insumo
                                    'ins_fecha_requerimiento' => $f_requerida, /// Fecha de Requerimiento
                                    'ins_detalle' => strtoupper($detalle), /// Insumo Detalle
                                    'ins_cant_requerida' => $cantidad, /// Cantidad Requerida
                                    'ins_costo_unitario' => $unitario, /// Costo Unitario
                                    'ins_costo_total' => $total, /// Costo Total
                                    'ins_tipo' => $tp, /// Ins Tipo
                                    'ins_unidad_medida' => strtoupper($unidad), /// Insumo Unidad de Medida
                                    'par_id' => $par_id[0]['par_id'], /// Partidas

                                    'fun_id' => $this->session->userdata("fun_id"), /// Funcionario
                                );

                            }
                            elseif($tp==9){
                                $nro_ins=$conf[0]['conf_otros_insumos']+1;

                                $update_conf = array('conf_otros_insumos' => $nro_ins);
                                $this->db->where('ide', $this->gestion);
                                $this->db->update('configuracion', $update_conf);

                                $query=$this->db->query('set datestyle to DMY');
                                $data_to_store = array(
                                    'ins_codigo' => $this->session->userdata("name").'/INS/OI/'.$this->gestion.'/'.$nro_ins, /// Codigo Insumo
                                    'ins_fecha_requerimiento' => $f_requerida, /// Fecha de Requerimiento
                                    'ins_detalle' => strtoupper($detalle), /// Insumo Detalle
                                    'ins_cant_requerida' => $cantidad, /// Cantidad Requerida
                                    'ins_costo_unitario' => $unitario, /// Costo Unitario
                                    'ins_costo_total' => $total, /// Costo Total
                                    'ins_tipo' => $tp, /// Ins Tipo
                                    'ins_unidad_medida' => strtoupper($unidad), /// Insumo Unidad de Medida
                                    'par_id' => $par_id[0]['par_id'], /// Partidas

                                    'fun_id' => $this->session->userdata("fun_id"), /// Funcionario
                                );

                            }

                            $this->db->insert('insumos', $data_to_store); ///// Guardar en Tabla Insumos
                            $ins_id=$this->db->insert_id();
                            /*---------------------------------------------------------*/
                            /*----------------------------------------------------------*/
                            $data_to_store2 = array( ///// Tabla InsumoActividad
                                'act_id' => $act_id, /// act_id
                                'ins_id' => $ins_id, /// ins_id
                            );
                            $this->db->insert('_insumoactividad', $data_to_store2);
                            /*----------------------------------------------------------*/
                            $gestion_fase=$fase[0]['pfec_fecha_inicio'];

                            /*---------------- Recorriendo Gestiones de la Fase -----------------------*/
                            for ($g=$fase[0]['pfec_fecha_inicio']; $g <=$fase[0]['pfec_fecha_fin'] ; $g++)
                            {
                                $gest=$g;
                                $gest=($gest%100);
                                if($g==$gestion_requerimiento || $gest==$gestion_requerimiento){

                                    $data_to_store = array(
                                        'ins_id' => $ins_id, /// Id Insumo
                                        'g_id' => $g, /// Gestion
                                        'insg_monto_prog' => $total, /// Monto programado
                                    );
                                    $this->db->insert('insumo_gestion', $data_to_store); ///// Guardar en Tabla Insumo Gestion
                                    $insg_id=$this->db->insert_id();

                                    $ptto_fase_gestion = $this->model_faseetapa->fase_gestion($fase[0]['id'],$g); //// DATOS DE LA FASE GESTION
                                    $fuentes=$this->model_faseetapa->fase_presupuesto_id($ptto_fase_gestion[0]['ptofecg_id']);

                                    if(count($fuentes)==1)
                                    {
                                        /*------------------- Guardando Fuente Financiamiento ------*/
                                        $query=$this->db->query('set datestyle to DMY');
                                        $data_to_store3 = array(
                                            'insg_id' => $insg_id, /// Id Insumo gestion
                                            'ifin_monto' => $total, /// Monto programado
                                            'ifin_gestion' => $g, /// Gestion
                                            'ffofet_id' => $fuentes[0]['ffofet_id'], /// ffotet id
                                            'ff_id' => $fuentes[0]['ff_id'], /// ff id
                                            'of_id' => $fuentes[0]['of_id'], /// ff id
                                            'nro_if' => 1, /// Nro if
                                        );
                                        $this->db->insert('insumo_financiamiento', $data_to_store3); ///// Guardar en Tabla Insumo Financiamiento
                                        $ifin_id=$this->db->insert_id();

                                        for ($p=1; $p <=12 ; $p++) {
                                            if($m[$p]!=0 & is_numeric($unitario))
                                            {
                                                $data_to_store4 = array(
                                                    'ifin_id' => $ifin_id, /// Id Insumo Financiamiento
                                                    'mes_id' => $p, /// Mes
                                                    'ipm_fis' => $m[$p], /// Valor mes
                                                );
                                                $this->db->insert('ifin_prog_mes', $data_to_store4); ///// Guardar en Tabla Insumo Financiamiento Programado Mes
                                            }
                                        }
                                        $valor=1; //// se insertaron por completo
                                        /*-----------------------------------------------------------*/
                                    }
                                    else{
                                        $valor=0; //// no se insertaron insumos fin y programados
                                    }
                                }
                                else{
                                    $data_to_store = array(
                                        'ins_id' => $ins_id, /// Id Insumo
                                        'g_id' => $g, /// Gestion
                                        'insg_monto_prog' => 0, /// Monto programado
                                    );
                                    $this->db->insert('insumo_gestion', $data_to_store); ///// Guardar en Tabla Insumo Gestion
                                }
                            }
                        }
                    }

                }
            }
            $i++;
        }
        return $nro;
    }

    /*------------Lista de Fuentes de Financiamiento de la operacion ---------------------     */
    public function fuentes_financiamientos($proy_id,$gestion,$tp_ejec) /////// GENERA TABLA DE FUENTES , ORGANISMOS
    {
        $list_recursos = $this->model_faseetapa->presupuesto_asignados($proy_id,$gestion); //// lista de recursos asignados
        $tabla = '';
        $nro=1; $suma_costo_total=0; $monto_asignado=0; $monto_programado=0;
        foreach ($list_recursos as $row) {
            $suma_prog = $this->minsumos->suma_monto_prog_insumo($row['ffofet_id'],$gestion,$tp_ejec); //// Suma Programado Insumo
            $tabla .= '<tr>';
            $tabla .= '<td>' . $nro . '</td>';
            $tabla .= '<td>' . $row['ff_codigo'] . '</td>';
            $tabla .= '<td>' . $row['ff_descripcion'] . '</td>';
            $tabla .= '<td>' . $row['of_codigo'] . '</td>';
            $tabla .= '<td>' . $row['of_descripcion'] . '</td>';
            $tabla .= '<td style="text-align: right;">' . number_format($row['presupuesto_asignado'], 2, ',', '.') . '</td>';
            $tabla .= '<td style="text-align: right;">' . number_format($suma_prog[0]['programado'], 2, ',', '.') . '</td>';
            $tabla .= '<td style="text-align: right;">' . number_format(($row['presupuesto_asignado']-$suma_prog[0]['programado']), 2, ',', '.') . '</td>';
            $tabla .= '</tr>';
            $nro++;
            $monto_asignado=$monto_asignado+$row['presupuesto_asignado'];
            $monto_programado=$monto_programado+$suma_prog[0]['programado'];
        }
        $tabla .= '<tr>';
        $tabla .= '<td colspan="5" style="background:#646464;">
                        <b style="color:#fff;"><center>T O T A L</center></b>
                   </td>';
        $tabla .= '<td style="text-align: right; background:#646464;"><b style="color:#fff;">' . number_format($monto_asignado, 2, ',', '.') . '</b></td>';
        $tabla .= '<td style="text-align: right; background:#646464;"><b style="color:#fff;">' . number_format($monto_programado, 2, ',', '.') . '</b></td>';
        $tabla .= '<td style="text-align: right; background:#646464;"><b style="color:#fff;">' . number_format(($monto_asignado-$monto_programado), 2, ',', '.') . '</b></td>';
        $tabla .= '</tr>';
        return $tabla;
    }

    /*------------ Sumatoria de las Fuentes Reuqeridas de la Operacion --------------------------*/
    public function suma_total_fuentes($fase_id,$gestion) /////// GENERA TABLA DE FUENTES , ORGANISMOS
    {
        $fase_gest = $this->model_faseetapa->fase_etapa_gestion($fase_id,$gestion); //// Lista de las gestiones de la Fase
        $list_fuentes = $this->model_faseetapa->fase_presupuesto_id($fase_gest[0]['ptofecg_id']); //// lista del presupuesto asignado
        $nro=1; $suma_costo_total=0; $monto_asignado=0; $monto_programado=0;
        foreach ($list_fuentes as $row) {
            $suma_prog = $this->minsumos->suma_monto_prog_insumo($row['ffofet_id'],$gestion,1); //// Suma Programado Insumo
            $monto_asignado=$monto_asignado+$row['ffofet_monto'];
            $monto_programado=$monto_programado+$suma_prog[0]['programado'];
        }
        $suma[1]=$monto_asignado; //// Monto Asigando
        $suma[2]=$monto_programado; /// Monto Programado
        $suma[3]=$monto_asignado-$monto_programado; //// Saldo Por Programar

        return $suma;
    }

    /*------------ Tabla de Recursos Permanentes ---------------------     */
    public function tabla_recursos_permanentes($proy_id,$prod_id,$act_id)
    {
        $mod_ins = site_url() . '/prog/mod_ins_a/1/' . $proy_id . '/' . $prod_id .'/'.$act_id;
        $del_ins = site_url() . '/prog/del_ins_a/1/' . $proy_id . '/' . $prod_id .'/'.$act_id;

        $tabla = '';
        $tabla .= '<thead>';
        $tabla .= ' <tr>
                        <th>
                            <a href="' . site_url("") . '/prog/nuevo_ins_a/1/'.$proy_id.'/'.$prod_id.'/'.$act_id.'">
                                <center>
                                    <img src="' . base_url() . 'assets/ifinal/2.png" width="35" height="35" class="img-responsive "title="ASIGNAR INSUMOS">NUEVO
                                </center>
                           </a>
                        </th>
                        <th>C&Oacute;DIGO</th>
                        <th>FECHA REQUERIMIENTO</th>
                        <th>DETALLE - CARACTERISTICA</th>
                        <th>CANT. REQUERIDA (Meses)</th>
                        <th>COSTO UNITARIO</th>
                        <th>COSTO TOTAL</th>
                        <th>PARTIDA</th>
                        <th>PROGRAMACI&Oacute;N PRESUPUESTARIA MENSUAL </th>
                    </tr>';
        $tabla .= '<thead>';
        $tabla .= '<tbody>';
        $lista_insumos = $this->minsumos->lista_insumos_tipo($act_id,1);
        $cont = 0;
        foreach ($lista_insumos as $row) {
            $insg = $this->minsumos->get_dato_insumo_gestion_actual($row['ins_id']);
            $cont++;
            $tabla .= '<tr>';
            $tabla .= '<td>';
            $tabla .= ' <center>
                            <a href="' . $mod_ins . '/'.$row['ins_id'].'/'.$this->gestion.'" title="MODIFICAR INSUMO" class="mod_insumo" name="mod_indumo" id="mod_insumo">
                                <img src="' . base_url() . 'assets/ifinal/modificar.png" WIDTH="40" HEIGHT="40"/><br>Modificar
                            </a>
                            <a href="#" data-toggle="modal" data-target="#modal_del_ff" class="btn btn-xs del_ff" title="ELIMINAR REQUERIMIENTO"  name="'.$row['ins_id'].'" id="1">
                                <img src="'.base_url().'assets/ifinal/eliminar.png" WIDTH="35" HEIGHT="35"/><br>Eliminar
                            </a>
                            </center>';
            $tabla .= '</td>';
            $tabla.='</td>';
            $tabla .= '<td>'.$row['ins_codigo'].'</td>';
            $tabla .= '<td>'.date('d-m-Y',strtotime($row['ins_fecha_requerimiento'])).'</td>';
            $tabla .= '<td>'.$row['ins_detalle'] .'</td>';
            $tabla .= '<td>'.$row['ins_cant_requerida'] .'</td>';
            $tabla .= '<td>'.number_format($row['ins_costo_unitario'], 2, ',', '.') .'</td>';
            $tabla .= '<td>'.number_format($row['ins_costo_total'], 2, ',', '.') .'</td>';
            $tabla .= '<td>'.$row['par_codigo'].'-'.$row['par_nombre'] .'</td>';
            if(count($insg)!=0){
                $tabla .= '<td>' . $this->get_tabla_ins_progmensual_directo($insg[0]['insg_id']) . '</td>';
            }
            else{
                $tabla .= '<td></td>';
            }

            $tabla .= '</tr>';
        }
        $tabla .= '<tbody>';

        return $tabla;
    }

    /*------------ Determinacion de Servicios ---------------------     */
    public function tabla_determinacion_servicios($proy_id,$prod_id,$act_id)
    {
        $mod_ins = site_url() . '/prog/mod_ins_a/2/' . $proy_id . '/' . $prod_id .'/'.$act_id;
        $del_ins = site_url() . '/prog/del_ins_a/2/' . $proy_id . '/' . $prod_id .'/'.$act_id;

        $tabla = '';
        $tabla .= '<thead>';
        $tabla .= ' <tr>
                        <th>
                             <a href="' . site_url("") . '/prog/nuevo_ins_a/2/'.$proy_id.'/'.$prod_id.'/'.$act_id.'">
                                <center>
                                    <img src="' . base_url() . 'assets/ifinal/2.png" width="35" height="35" class="img-responsive "title="ASIGNAR INSUMOS">NUEVO
                                </center>
                           </a>
                        </th>
                        <th>C&Oacute;DIGO</th>
                        <th>FECHA REQUERIMIENTO</th>
                        <th>DETALLE-SERVICIOS</th>
                        <th>CANTIDAD REQUERIDA</th>
                        <th>COSTO UNITARIO</th>
                        <th>COSTO TOTAL</th>
                        <th>PARTIDA</th>
                        <th>PROGRAMACI&Oacute;N PRESUPUESTARIA MENSUAL </th>
                    </tr>';
        $tabla .= '<thead>';
        $tabla .= '<tbody>';
        $lista_insumos = $this->minsumos->lista_insumos_tipo($act_id,2);
        $cont = 0;
        foreach ($lista_insumos as $row) {
            $insg = $this->minsumos->get_dato_insumo_gestion_actual($row['ins_id']);
            $cont++;
            $tabla .= '<tr>';
            $tabla .= '<td>';
            $tabla .= ' <center>
                            <a href="' . $mod_ins . '/'.$row['ins_id'].'/'.$this->gestion.'" title="MODIFICAR INSUMO" class="mod_insumo" name="mod_indumo" id="mod_insumo">
                                <img src="' . base_url() . 'assets/ifinal/modificar.png" WIDTH="40" HEIGHT="40"/><br>Modificar
                            </a>
                            <a href="#" data-toggle="modal" data-target="#modal_del_ff" class="btn btn-xs del_ff" title="ELIMINAR REQUERIMIENTO"  name="'.$row['ins_id'].'" id="2">
                                <img src="'.base_url().'assets/ifinal/eliminar.png" WIDTH="35" HEIGHT="35"/><br>Eliminar
                            </a>
                            </center>';
            $tabla .= '</td>';
            $tabla.='</td>';
            $tabla .= '<td>'.$row['ins_codigo'].'</td>';
            $tabla .= '<td>'.date('d-m-Y',strtotime($row['ins_fecha_requerimiento'])).'</td>';
            $tabla .= '<td>'.$row['ins_detalle'] .'</td>';
            $tabla .= '<td>'.$row['ins_cant_requerida'] .'</td>';
            $tabla .= '<td>'.number_format($row['ins_costo_unitario'], 2, ',', '.') .'</td>';
            $tabla .= '<td>'.number_format($row['ins_costo_total'], 2, ',', '.') .'</td>';
            $tabla .= '<td>'.$row['par_codigo'].'-'.$row['par_nombre'] .'</td>';
            if(count($insg)!=0){
                $tabla .= '<td>' . $this->get_tabla_ins_progmensual_directo($insg[0]['insg_id']) . '</td>';
            }
            else{
                $tabla .= '<td></td>';
            }
            $tabla .= '</tr>';
        }
        $tabla .= '<tbody>';

        return $tabla;
    }

    /*------------ Determinacion de Pasajes ---------------------     */
    public function tabla_determinacion_pasajes($proy_id,$prod_id,$act_id)
    {
        $mod_ins = site_url() . '/prog/mod_ins_a/3/' . $proy_id . '/' . $prod_id .'/'.$act_id;
        $del_ins = site_url() . '/prog/del_ins_a/3/' . $proy_id . '/' . $prod_id .'/'.$act_id;

        $tabla = '';
        $tabla .= '<thead>';
        $tabla .= ' <tr>
                        <th>
                             <a href="' . site_url("") . '/prog/nuevo_ins_a/3/'.$proy_id.'/'.$prod_id.'/'.$act_id.'">
                                <center>
                                    <img src="' . base_url() . 'assets/ifinal/2.png" width="35" height="35" class="img-responsive "title="ASIGNAR INSUMOS">NUEVO
                                </center>
                           </a>
                        </th>
                        <th>C&Oacute;DIGO</th>
                        <th>FECHA REQUERIMIENTO</th>
                        <th>RUTA</th>
                        <th>CANTIDAD</th>
                        <th>COSTO UNITARIO</th>
                        <th>COSTO TOTAL</th>
                        <th>PARTIDA</th>
                        <th>PROGRAMACI&Oacute;N PRESUPUESTARIA MENSUAL </th>
                    </tr>';
        $tabla .= '<thead>';
        $tabla .= '<tbody>';
        $lista_insumos = $this->minsumos->lista_insumos_tipo($act_id,3);
        $cont = 0;
        foreach ($lista_insumos as $row) {
            $insg = $this->minsumos->get_dato_insumo_gestion_actual($row['ins_id']);
            $cont++;
            $tabla .= '<tr>';
            $tabla .= '<td>';
            $tabla .= ' <center>
                            <a href="' . $mod_ins . '/'.$row['ins_id'].'/'.$this->gestion.'" title="MODIFICAR INSUMO" class="mod_insumo" name="mod_indumo" id="mod_insumo">
                                <img src="' . base_url() . 'assets/ifinal/modificar.png" WIDTH="40" HEIGHT="40"/><br>Modificar
                            </a>
                            <a href="#" data-toggle="modal" data-target="#modal_del_ff" class="btn btn-xs del_ff" title="ELIMINAR REQUERIMIENTO"  name="'.$row['ins_id'].'" id="3">
                                <img src="'.base_url().'assets/ifinal/eliminar.png" WIDTH="35" HEIGHT="35"/><br>Eliminar
                            </a>
                            </center>';
            $tabla .= '</td>';
            $tabla.='</td>';
            $tabla .= '<td>'.$row['ins_codigo'].'</td>';
            $tabla .= '<td>'.date('d-m-Y',strtotime($row['ins_fecha_requerimiento'])).'</td>';
            $tabla .= '<td>'.$row['ins_detalle'] .'</td>';
            $tabla .= '<td>'.$row['ins_cant_requerida'] .'</td>';
            $tabla .= '<td>'.number_format($row['ins_costo_unitario'], 2, ',', '.') .'</td>';
            $tabla .= '<td>'.number_format($row['ins_costo_total'], 2, ',', '.') .'</td>';
            $tabla .= '<td>'.$row['par_codigo'].'-'.$row['par_nombre'] .'</td>';
            if(count($insg)!=0){
                $tabla .= '<td>' . $this->get_tabla_ins_progmensual_directo($insg[0]['insg_id']) . '</td>';
            }
            else{
                $tabla .= '<td></td>';
            }
            $tabla .= '</tr>';
        }
        $tabla .= '<tbody>';

        return $tabla;
    }

    /*------------ Determinacion de Viaticos ---------------------     */
    public function tabla_determinacion_viaticos($proy_id,$prod_id,$act_id)
    {
        $mod_ins = site_url() . '/prog/mod_ins_a/4/' . $proy_id . '/' . $prod_id .'/'.$act_id;
        $del_ins = site_url() . '/prog/del_ins_a/4/' . $proy_id . '/' . $prod_id .'/'.$act_id;

        $tabla = '';
        $tabla .= '<thead>';
        $tabla .= ' <tr>
                        <th>
                             <a href="' . site_url("") . '/prog/nuevo_ins_a/4/'.$proy_id.'/'.$prod_id.'/'.$act_id.'">
                                <center>
                                    <img src="' . base_url() . 'assets/ifinal/2.png" width="35" height="35" class="img-responsive "title="ASIGNAR INSUMOS">NUEVO
                                </center>
                           </a>
                        </th>
                        <th>C&Oacute;DIGO</th>
                        <th>FECHA REQUERIMIENTO</th>
                        <th>DESTINO</th>
                        <th>DIAS VIATICO</th>
                        <th>VIATICO DIARIO</th>
                        <th>COSTO TOTAL</th>
                        <th>PARTIDA</th>
                        <th>PROGRAMACI&Oacute;N PRESUPUESTARIA MENSUAL </th>
                    </tr>';
        $tabla .= '<thead>';
        $tabla .= '<tbody>';
        $lista_insumos = $this->minsumos->lista_insumos_tipo($act_id,4);
        $cont = 0;
        foreach ($lista_insumos as $row) {
            $insg = $this->minsumos->get_dato_insumo_gestion_actual($row['ins_id']);
            $cont++;
            $tabla .= '<tr>';
            $tabla .= '<td>';
            $tabla .= ' <center>
                            <a href="' . $mod_ins . '/'.$row['ins_id'].'/'.$this->gestion.'" title="MODIFICAR INSUMO" class="mod_insumo" name="mod_indumo" id="mod_insumo">
                                <img src="' . base_url() . 'assets/ifinal/modificar.png" WIDTH="40" HEIGHT="40"/><br>Modificar
                            </a>
                            <a href="#" data-toggle="modal" data-target="#modal_del_ff" class="btn btn-xs del_ff" title="ELIMINAR REQUERIMIENTO"  name="'.$row['ins_id'].'" id="4">
                                <img src="'.base_url().'assets/ifinal/eliminar.png" WIDTH="35" HEIGHT="35"/><br>Eliminar
                            </a>
                            </center>';
            $tabla .= '</td>';
            $tabla.='</td>';
            $tabla .= '<td>'.$row['ins_codigo'].'</td>';
            $tabla .= '<td>'.date('d-m-Y',strtotime($row['ins_fecha_requerimiento'])).'</td>';
            $tabla .= '<td>'.$row['ins_detalle'] .'</td>';
            $tabla .= '<td>'.$row['ins_cant_requerida'] .'</td>';
            $tabla .= '<td>'.number_format($row['ins_costo_unitario'], 2, ',', '.') .'</td>';
            $tabla .= '<td>'.number_format($row['ins_costo_total'], 2, ',', '.') .'</td>';
            $tabla .= '<td>'.$row['par_codigo'].'-'.$row['par_nombre'] .'</td>';
            if(count($insg)!=0){
                $tabla .= '<td>' . $this->get_tabla_ins_progmensual_directo($insg[0]['insg_id']) . '</td>';
            }
            else{
                $tabla .= '<td></td>';
            }
            $tabla .= '</tr>';
        }
        $tabla .= '<tbody>';

        return $tabla;
    }

    /*------------ Consultoria por producto ---------------------     */
    public function tabla_consultoria_producto($proy_id,$prod_id,$act_id)
    {
        $mod_ins = site_url() . '/prog/mod_ins_a/5/' . $proy_id . '/' . $prod_id .'/'.$act_id;
        $del_ins = site_url() . '/prog/del_ins_a/5/' . $proy_id . '/' . $prod_id .'/'.$act_id;

        $tabla = '';
        $tabla .= '<thead>';
        $tabla .= ' <tr>
                        <th>
                             <a href="' . site_url("") . '/prog/nuevo_ins_a/5/'.$proy_id.'/'.$prod_id.'/'.$act_id.'">
                                <center>
                                    <img src="' . base_url() . 'assets/ifinal/2.png" width="35" height="35" class="img-responsive "title="ASIGNAR INSUMOS">NUEVO
                                </center>
                           </a>
                        </th>
                        <th>C&Oacute;DIGO</th>
                        <th>FECHA REQUERIMIENTO</th>
                        <th>DESCRIPCI&Oacute;N</th>
                        <th>CANTIDAD</th>
                        <th>COSTO UNITARIO</th>
                        <th>COSTO TOTAL</th>
                        <th>PARTIDA</th>
                        <th>PROGRAMACI&Oacute;N PRESUPUESTARIA MENSUAL </th>
                    </tr>';
        $tabla .= '<thead>';
        $tabla .= '<tbody>';
        $lista_insumos = $this->minsumos->lista_insumos_tipo($act_id,5);
        $cont = 0;
        foreach ($lista_insumos as $row) {
            $insg = $this->minsumos->get_dato_insumo_gestion_actual($row['ins_id']);
            $cont++;
            $tabla .= '<tr>';
            $tabla .= '<td>';
            $tabla .= ' <center>
                            <a href="' . $mod_ins . '/'.$row['ins_id'].'/'.$this->gestion.'" title="MODIFICAR INSUMO" class="mod_insumo" name="mod_indumo" id="mod_insumo">
                                <img src="' . base_url() . 'assets/ifinal/modificar.png" WIDTH="40" HEIGHT="40"/><br>Modificar
                            </a>
                            <a href="#" data-toggle="modal" data-target="#modal_del_ff" class="btn btn-xs del_ff" title="ELIMINAR REQUERIMIENTO"  name="'.$row['ins_id'].'" id="5">
                                <img src="'.base_url().'assets/ifinal/eliminar.png" WIDTH="35" HEIGHT="35"/><br>Eliminar
                            </a>
                            </center>';
            $tabla .= '</td>';
            $tabla.='</td>';
            $tabla .= '<td>'.$row['ins_codigo'].'</td>';
            $tabla .= '<td>'.date('d-m-Y',strtotime($row['ins_fecha_requerimiento'])).'</td>';
            $tabla .= '<td>'.$row['ins_detalle'] .'</td>';
            $tabla .= '<td>'.$row['ins_cant_requerida'] .'</td>';
            $tabla .= '<td>'.number_format($row['ins_costo_unitario'], 2, ',', '.') .'</td>';
            $tabla .= '<td>'.number_format($row['ins_costo_total'], 2, ',', '.') .'</td>';
            $tabla .= '<td>'.$row['par_codigo'].'-'.$row['par_nombre'] .'</td>';
            if(count($insg)!=0){
                $tabla .= '<td>' . $this->get_tabla_ins_progmensual_directo($insg[0]['insg_id']) . '</td>';
            }
            else{
                $tabla .= '<td></td>';
            }
            $tabla .= '</tr>';
        }
        $tabla .= '<tbody>';

        return $tabla;
    }

    /*------------ Consultoria de Linea ---------------------     */
    public function tabla_consultoria_linea($proy_id,$prod_id,$act_id)
    {
        $mod_ins = site_url() . '/prog/mod_ins_a/6/' . $proy_id . '/' . $prod_id .'/'.$act_id;
        $del_ins = site_url() . '/prog/del_ins_a/6/' . $proy_id . '/' . $prod_id .'/'.$act_id;

        $tabla = '';
        $tabla .= '<thead>';
        $tabla .= ' <tr>
                        <th>
                             <a href="' . site_url("") . '/prog/nuevo_ins_a/6/'.$proy_id.'/'.$prod_id.'/'.$act_id.'">
                                <center>
                                    <img src="' . base_url() . 'assets/ifinal/2.png" width="35" height="35" class="img-responsive "title="ASIGNAR INSUMOS">NUEVO
                                </center>
                           </a>
                        </th>
                        <th>C&Oacute;DIGO</th>
                        <th>FECHA REQUERIMIENTO</th>
                        <th>ACTIVIDAD DE LA CONSULTORIA</th>
                        <th>CANTIDAD REQUERIDA</th>
                        <th>COSTO UNITARIO</th>
                        <th>COSTO TOTAL</th>
                        <th>PARTIDA</th>
                        <th>PROGRAMACI&Oacute;N PRESUPUESTARIA MENSUAL </th>
                    </tr>';
        $tabla .= '<thead>';
        $tabla .= '<tbody>';
        $lista_insumos = $this->minsumos->lista_insumos_tipo($act_id,6);
        $cont = 0;
        foreach ($lista_insumos as $row) {
            $insg = $this->minsumos->get_dato_insumo_gestion_actual($row['ins_id']);
            $cont++;
            $tabla .= '<tr>';
            $tabla .= '<td>';
            $tabla .= ' <center>
                            <a href="' . $mod_ins . '/'.$row['ins_id'].'/'.$this->gestion.'" title="MODIFICAR INSUMO" class="mod_insumo" name="mod_indumo" id="mod_insumo">
                                <img src="' . base_url() . 'assets/ifinal/modificar.png" WIDTH="40" HEIGHT="40"/><br>Modificar
                            </a>
                            <a href="#" data-toggle="modal" data-target="#modal_del_ff" class="btn btn-xs del_ff" title="ELIMINAR REQUERIMIENTO"  name="'.$row['ins_id'].'" id="6">
                                <img src="'.base_url().'assets/ifinal/eliminar.png" WIDTH="35" HEIGHT="35"/><br>Eliminar
                            </a>
                            </center>';
            $tabla .= '</td>';
            $tabla.='</td>';
            $tabla .= '<td>'.$row['ins_codigo'].'</td>';
            $tabla .= '<td>'.date('d-m-Y',strtotime($row['ins_fecha_requerimiento'])).'</td>';
            $tabla .= '<td>'.$row['ins_detalle'] .'</td>';
            $tabla .= '<td>'.$row['ins_cant_requerida'] .'</td>';
            $tabla .= '<td>'.number_format($row['ins_costo_unitario'], 2, ',', '.') .'</td>';
            $tabla .= '<td>'.number_format($row['ins_costo_total'], 2, ',', '.') .'</td>';
            $tabla .= '<td>'.$row['par_codigo'].'-'.$row['par_nombre'] .'</td>';
            if(count($insg)!=0){
                $tabla .= '<td>' . $this->get_tabla_ins_progmensual_directo($insg[0]['insg_id']) . '</td>';
            }
            else{
                $tabla .= '<td></td>';
            }
            $tabla .= '</tr>';
        }
        $tabla .= '<tbody>';

        return $tabla;
    }

    /*------------ Determinacion de Materiales y Suministro ---------------------     */
    public function tabla_determinacion_suministros($proy_id,$prod_id,$act_id)
    {
        $mod_ins = site_url() . '/prog/mod_ins_a/7/' . $proy_id . '/' . $prod_id .'/'.$act_id;
        $del_ins = site_url() . '/prog/del_ins_a/7/' . $proy_id . '/' . $prod_id .'/'.$act_id;

        $tabla = '';
        $tabla .= '<thead>';
        $tabla .= ' <tr>
                        <th>
                             <a href="' . site_url("") . '/prog/nuevo_ins_a/7/'.$proy_id.'/'.$prod_id.'/'.$act_id.'">
                                <center>
                                    <img src="' . base_url() . 'assets/ifinal/2.png" width="35" height="35" class="img-responsive "title="ASIGNAR INSUMOS">NUEVO
                                </center>
                           </a>
                        </th>
                        <th>C&Oacute;DIGO</th>
                        <th>FECHA REQUERIMIENTO</th>
                        <th>DESCRIPCI&Oacute;N</th>
                        <th>CANTIDAD</th>
                        <th>COSTO UNITARIO</th>
                        <th>COSTO TOTAL</th>
                        <th>PARTIDA</th>
                        <th>PROGRAMACI&Oacute;N PRESUPUESTARIA MENSUAL </th>
                    </tr>';
        $tabla .= '<thead>';
        $tabla .= '<tbody>';
        $lista_insumos = $this->minsumos->lista_insumos_tipo($act_id,7);
        $cont = 0;
        foreach ($lista_insumos as $row) {
            $insg = $this->minsumos->get_dato_insumo_gestion_actual($row['ins_id']);
            $cont++;
            $tabla .= '<tr>';
            $tabla .= '<td>';
            $tabla .= ' <center>
                            <a href="' . $mod_ins . '/'.$row['ins_id'].'/'.$this->gestion.'" title="MODIFICAR INSUMO" class="mod_insumo" name="mod_indumo" id="mod_insumo">
                                <img src="' . base_url() . 'assets/ifinal/modificar.png" WIDTH="40" HEIGHT="40"/><br>Modificar
                            </a>
                            <a href="#" data-toggle="modal" data-target="#modal_del_ff" class="btn btn-xs del_ff" title="ELIMINAR REQUERIMIENTO"  name="'.$row['ins_id'].'" id="7">
                                <img src="'.base_url().'assets/ifinal/eliminar.png" WIDTH="35" HEIGHT="35"/><br>Eliminar
                            </a>
                            </center>';
            $tabla .= '</td>';
            $tabla.='</td>';
            $tabla .= '<td>'.$row['ins_codigo'].'</td>';
            $tabla .= '<td>'.date('d-m-Y',strtotime($row['ins_fecha_requerimiento'])).'</td>';
            $tabla .= '<td>'.$row['ins_detalle'] .'</td>';
            $tabla .= '<td>'.$row['ins_cant_requerida'] .'</td>';
            $tabla .= '<td>'.number_format($row['ins_costo_unitario'], 2, ',', '.') .'</td>';
            $tabla .= '<td>'.number_format($row['ins_costo_total'], 2, ',', '.') .'</td>';
            $tabla .= '<td>'.$row['par_codigo'].'-'.$row['par_nombre'] .'</td>';
            if(count($insg)!=0){
                $tabla .= '<td>' . $this->get_tabla_ins_progmensual_directo($insg[0]['insg_id']) . '</td>';
            }
            else{
                $tabla .= '<td></td>';
            }
            $tabla .= '</tr>';
        }
        $tabla .= '<tbody>';

        return $tabla;
    }

    /*------------ Determinacion de Activos Fijos ---------------------     */
    public function tabla_determinacion_activos($proy_id,$prod_id,$act_id)
    {
        $mod_ins = site_url() . '/prog/mod_ins_a/8/' . $proy_id . '/' . $prod_id .'/'.$act_id;
        $del_ins = site_url() . '/prog/del_ins_a/8/' . $proy_id . '/' . $prod_id .'/'.$act_id;

        $tabla = '';
        $tabla .= '<thead>';
        $tabla .= ' <tr>
                        <th>
                             <a href="' . site_url("") . '/prog/nuevo_ins_a/8/'.$proy_id.'/'.$prod_id.'/'.$act_id.'">
                                <center>
                                    <img src="' . base_url() . 'assets/ifinal/2.png" width="35" height="35" class="img-responsive "title="ASIGNAR INSUMOS">NUEVO
                                </center>
                           </a>
                        </th>
                        <th>C&Oacute;DIGO</th>
                        <th>FECHA REQUERIMIENTO</th>
                        <th>DESCRIPCI&Oacute;N</th>
                        <th>CANTIDAD</th>
                        <th>COSTO UNITARIO</th>
                        <th>COSTO TOTAL</th>
                        <th>PARTIDA</th>
                        <th>PROGRAMACI&Oacute;N PRESUPUESTARIA MENSUAL </th>
                    </tr>';
        $tabla .= '<thead>';
        $tabla .= '<tbody>';
        $lista_insumos = $this->minsumos->lista_insumos_tipo($act_id,8);
        $cont = 0;
        foreach ($lista_insumos as $row) {
            $insg = $this->minsumos->get_dato_insumo_gestion_actual($row['ins_id']);
            $cont++;
            $tabla .= '<tr>';
            $tabla .= '<td>';
            $tabla .= ' <center>
                            <a href="' . $mod_ins . '/'.$row['ins_id'].'/'.$this->gestion.'" title="MODIFICAR INSUMO" class="mod_insumo" name="mod_indumo" id="mod_insumo">
                                <img src="' . base_url() . 'assets/ifinal/modificar.png" WIDTH="40" HEIGHT="40"/><br>Modificar
                            </a>
                            <a href="#" data-toggle="modal" data-target="#modal_del_ff" class="btn btn-xs del_ff" title="ELIMINAR REQUERIMIENTO"  name="'.$row['ins_id'].'" id="8">
                                <img src="'.base_url().'assets/ifinal/eliminar.png" WIDTH="35" HEIGHT="35"/><br>Eliminar
                            </a>
                            </center>';
            $tabla .= '</td>';
            $tabla.='</td>';
            $tabla .= '<td>'.$row['ins_codigo'].'</td>';
            $tabla .= '<td>'.date('d-m-Y',strtotime($row['ins_fecha_requerimiento'])).'</td>';
            $tabla .= '<td>'.$row['ins_detalle'] .'</td>';
            $tabla .= '<td>'.$row['ins_cant_requerida'] .'</td>';
            $tabla .= '<td>'.number_format($row['ins_costo_unitario'], 2, ',', '.') .'</td>';
            $tabla .= '<td>'.number_format($row['ins_costo_total'], 2, ',', '.') .'</td>';
            $tabla .= '<td>'.$row['par_codigo'].'-'.$row['par_nombre'] .'</td>';
            if(count($insg)!=0){
                $tabla .= '<td>' . $this->get_tabla_ins_progmensual_directo($insg[0]['insg_id']) . '</td>';
            }
            else{
                $tabla .= '<td></td>';
            }
            $tabla .= '</tr>';
        }
        $tabla .= '<tbody>';

        return $tabla;
    }

    /*------------ Determinacion de Otros Insumos ---------------------     */
    public function tabla_determinacion_otros($proy_id,$prod_id,$act_id)
    {
        $mod_ins = site_url() . '/prog/mod_ins_a/9/' . $proy_id . '/' . $prod_id .'/'.$act_id;
        $del_ins = site_url() . '/prog/del_ins_a/9/' . $proy_id . '/' . $prod_id .'/'.$act_id;

        $tabla = '';
        $tabla .= '<thead>';
        $tabla .= ' <tr>
                        <th>
                             <a href="' . site_url("") . '/prog/nuevo_ins_a/9/'.$proy_id.'/'.$prod_id.'/'.$act_id.'">
                                <center>
                                    <img src="' . base_url() . 'assets/ifinal/2.png" width="35" height="35" class="img-responsive "title="ASIGNAR INSUMOS">NUEVO
                                </center>
                           </a>
                        </th>
                        <th>C&Oacute;DIGO</th>
                        <th>FECHA REQUERIMIENTO</th>
                        <th>DESCRIPCI&Oacute;N</th>
                        <th>CANTIDAD</th>
                        <th>COSTO UNITARIO</th>
                        <th>COSTO TOTAL</th>
                        <th>PARTIDA</th>
                        <th>PROGRAMACI&Oacute;N PRESUPUESTARIA MENSUAL </th>
                    </tr>';
        $tabla .= '<thead>';
        $tabla .= '<tbody>';
        $lista_insumos = $this->minsumos->lista_insumos_tipo($act_id,9);
        $cont = 0;
        foreach ($lista_insumos as $row) {
            $insg = $this->minsumos->get_dato_insumo_gestion_actual($row['ins_id']);
            $cont++;
            $tabla .= '<tr>';
            $tabla .= '<td>';
            $tabla .= ' <center>
                            <a href="' . $mod_ins . '/'.$row['ins_id'].'/'.$this->gestion.'" title="MODIFICAR INSUMO" class="mod_insumo" name="mod_indumo" id="mod_insumo">
                                <img src="' . base_url() . 'assets/ifinal/modificar.png" WIDTH="40" HEIGHT="40"/><br>Modificar
                            </a>
                            <a href="#" data-toggle="modal" data-target="#modal_del_ff" class="btn btn-xs del_ff" title="ELIMINAR REQUERIMIENTO"  name="'.$row['ins_id'].'" id="9">
                                <img src="'.base_url().'assets/ifinal/eliminar.png" WIDTH="35" HEIGHT="35"/><br>Eliminar
                            </a>
                            </center>';
            $tabla .= '</td>';
            $tabla.='</td>';
            $tabla .= '<td>'.$row['ins_codigo'].'</td>';
            $tabla .= '<td>'.date('d-m-Y',strtotime($row['ins_fecha_requerimiento'])).'</td>';
            $tabla .= '<td>'.$row['ins_detalle'] .'</td>';
            $tabla .= '<td>'.$row['ins_cant_requerida'] .'</td>';
            $tabla .= '<td>'.number_format($row['ins_costo_unitario'], 2, ',', '.') .'</td>';
            $tabla .= '<td>'.number_format($row['ins_costo_total'], 2, ',', '.') .'</td>';
            $tabla .= '<td>'.$row['par_codigo'].'-'.$row['par_nombre'] .'</td>';
            if(count($insg)!=0){
                $tabla .= '<td>' . $this->get_tabla_ins_progmensual_directo($insg[0]['insg_id']) . '</td>';
            }
            else{
                $tabla .= '<td></td>';
            }
            $tabla .= '</tr>';
        }
        $tabla .= '<tbody>';

        return $tabla;
    }

    /*------------------------ NUEVO INSUMO A NIVEL DE ACTIVIDADES ----------------------*/
    function nuevo_insumo($tipo_insumo, $proy_id, $prod_id, $act_id,$insp_id)
    {
        if($tipo_insumo!='' & $proy_id!='' & $prod_id!='' & $act_id!='' & $insp_id!=''){
            $enlaces=$this->menu_modelo->get_Modulos_programacion(1);
            $data['enlaces'] = $enlaces;
            $data['nro_fase']= $this->model_faseetapa->nro_fase($proy_id); /// nro de fases y etapas registrados
            $data['id_f'] = $this->model_faseetapa->get_id_fase($proy_id); //// DATOS DE LA FASE ACTIVA
            $data['mod']=4;
            $data['proyecto'] = $this->model_proyecto->get_id_proyecto($proy_id); //// Datos del Proyecto
            $data['fase'] = $this->model_faseetapa->get_id_fase($proy_id); //// DATOS DE LA FASE ACTIVA
            $titulo_proy=strtoupper($data['proyecto'][0]['tipo']);
            $data['titulo_proy'] = $titulo_proy;
            $data['gestiones'] = $data['fase'][0]['pfec_fecha_fin']-$data['fase'][0]['pfec_fecha_inicio']+1; //// Nro de Gestione en funcion a su fase activa
            $data['gestion'] = $this->gestion;
            $data['proy_id'] = $proy_id;
            $data['prod_id'] = $prod_id;
            $data['act_id'] = $act_id;
            $data['ins_tipo'] = $tipo_insumo;
            $data['dato_proy'] = $this->minsumos->dato_proyecto($proy_id);
            $data['dato_prod'] = $this->minsumos->dato_producto($prod_id);
            $data['dato_act'] = $this->minsumos->dato_actividad($act_id);
            $data['insp_id'] = $insp_id;
            $insumo_partida= $this->minsumos_partida->get_dato_insumo($insp_id);
            $data['part_id'] = $insumo_partida[0]['par_id'];
            //----------------------------------------------------------------
            $data['ins_titulo'] = 'NUEVO INSUMO' . $this->get_ins_titulo($tipo_insumo);
            $data['techo'] = $this->minsumos->tabla_presupuesto($proy_id, $this->gestion);//techo presupuestario
            $data['saldo_total'] = $this->minsumos->saldo_total_fin($proy_id, $this->gestion);//SALDO TOTAL DEL TECHO PRESUPUESTARIO
            $data['lista_entidad'] = $this->model_entidad_tras->lista_entidad_tras();//entidad transferencia
            $data['lista_cargo'] = $this->minsumos->lista_cargo();
            $data['sumatorias'] = $this->suma_total_fuentes($data['fase'][0]['id'], $this->gestion); //// Suma Total, Asignado, Programado, Saldo
            $data['lista_partidas'] = $this->model_partidas->lista_padres();//partidas

            if($tipo_insumo==1 || $tipo_insumo==2 || $tipo_insumo==3 || $tipo_insumo==4 || $tipo_insumo==5 || $tipo_insumo==6 || $tipo_insumo==7 || $tipo_insumo==8 || $tipo_insumo==9){
                $this->load->view('admin/programacion/insumos/insumo_actividades/requerimientos/form_insumo_act', $data);
            }
            else{
                redirect(site_url("") . '/prog/ins_act/'.$proy_id.'/'.$prod_id.'/'.$act_id.'/false');
            }
        }
        else{
            redirect('admin/dashboard');
        }
    }

    /*-------------------- FORMULARIO DE MODIFICAR INSUMOS ---------------------*/
    function mod_insumo($tipo_insumo,$proy_id,$prod_id,$act_id,$ins_id,$gestion,$insp_id)
    {
        if($tipo_insumo!='' & $proy_id!='' & $prod_id!='' & $act_id!='' & $ins_id!='' & $gestion!=''& $insp_id!=''){
            $enlaces=$this->menu_modelo->get_Modulos_programacion(1);
            $data['enlaces'] = $enlaces;
            $data['nro_fase']= $this->model_faseetapa->nro_fase($proy_id); /// nro de fases y etapas registrados
            $data['id_f'] = $this->model_faseetapa->get_id_fase($proy_id); //// DATOS DE LA FASE ACTIVA
            $data['mod']=4;

            $data['proyecto'] = $this->model_proyecto->get_id_proyecto($proy_id); ///// DATOS DEL PROYECTO
            $data['fase'] = $this->model_faseetapa->get_id_fase($proy_id); //// DATOS DE LA FASE ACTIVA

            $data['gestiones'] = $data['fase'][0]['pfec_fecha_fin']-$data['fase'][0]['pfec_fecha_inicio']+1; //// Nro de Gestione en funcion a su fase activa

            $titulo_proy=strtoupper($data['proyecto'][0]['tipo']);
            $data['titulo_proy'] = $titulo_proy;

            $data['gestion'] = $gestion; //// GESTION VIGENTE

            $data['proy_id'] = $proy_id; //// PROY ID
            $data['prod_id'] = $prod_id;
            $data['act_id'] = $act_id;
            $data['insp_id']=$insp_id;
            $data['ins_tipo'] = $tipo_insumo;
            $data['dato_proy'] = $this->minsumos->dato_proyecto($proy_id);
            $data['dato_prod'] = $this->minsumos->dato_producto($prod_id);
            $data['dato_act'] = $this->minsumos->dato_actividad($act_id);
            $data['lista_cargo'] = $this->minsumos->lista_cargo();
            $data['ins_titulo'] = 'INSUMO' . $this->get_ins_titulo($tipo_insumo);
            $data['ins_id'] = $ins_id;  //// INS ID
            $data['insumo'] = $this->minsumos->get_dato_insumo($ins_id); //// DATOS DEL INSUMO
            if(count($data['insumo'])!=0)
            {
                $data['insumo_gestion'] = $this->minsumos->list_insumos_gestion($ins_id); //// DATOS DEL INSUMO
                $insumo_gestion_actual = $this->minsumos->get_insumo_gestion($ins_id,$this->gestion); //// GET INSUMO GESTION
                if(count($insumo_gestion_actual)!=0){
                    $monto_programado=$insumo_gestion_actual[0]['insg_monto_prog'];
                }
                else{
                    $monto_programado=0;
                }
                $data['monto_programado']=$monto_programado;

                $data['lista_partidas'] = $this->model_partidas->lista_padres();//partidas
                $sumatorias = $this->suma_total_fuentes($data['fase'][0]['id'], $gestion); //// Suma Total, Asignado, Programado, Saldo
                $insumo_gestion_programado = $this->minsumos->suma_dato_insumo_programado($ins_id,$gestion); //// DATOS DEL INSUMO PROGRAMADO

                $suma_programado=0;
                if(count($insumo_gestion_programado)!=0){$suma_programado=$insumo_gestion_programado[0]['suma'];}
                $data['saldo_por_programar']=$sumatorias[1]-$sumatorias[2]+$suma_programado;
                //$data['saldo_por_programar']=$sumatorias[3]-$data['insumo'][0]['ins_costo_total']; //// Original
                $this->load->view('admin/programacion/insumos/insumo_actividades/requerimientos/update_insumo_act', $data);
            }
            else{
                redirect(site_url("") . '/prog/ins_act/'.$proy_id.'/'.$prod_id.'/'.$act_id.'/false');
            }
        }
        else{
            redirect('admin/dashboard');
        }
    }


    /*---------------------- GUARDAR INSUMO A NIVEL DE COMPONENTES -----------------------*/
    function guardar_insumo()
    {
        if ($this->input->post()) {
            $post = $this->input->post();
            $proy_id = $post['proy_id'];//proy id
            $prod_id = $post['prod_id'];//prod id
            $act_id = $post['act_id'];//act id
            $ins_tipo = $post['ins_tipo'];//tipo de insumo
            $insp_id=$post['insp_id'];
            $fase = $this->model_faseetapa->get_id_fase($proy_id); //// DATOS DE LA FASE ACTIVA
            $conf=$this->model_proyecto->configuracion();

            $veriff = $this->verif_fecha($post['ins_fecha']); //// verifica de requerimiento

            if($ins_tipo>=1 && $ins_tipo<=9)
            {
                if($veriff=='true') //// verif fecha de requerimiento
                {
                    /*------------------------------------------------------------------------------------*/
                    if($ins_tipo==1){
                        $nro_ins=$conf[0]['conf_rrhhp']+1;

                        $update_conf = array('conf_rrhhp' => $nro_ins);
                        $this->db->where('ide', $this->gestion);
                        $this->db->update('configuracion', $update_conf);

                        $query=$this->db->query('set datestyle to DMY');
                        $data_to_store = array(
                            'ins_codigo' => $this->session->userdata("name").'/INS/RHP/'.$this->gestion.'/'.$nro_ins, /// Codigo Insumo
                            'ins_fecha_requerimiento' => $post['ins_fecha'], /// Fecha de Requerimiento
                            'ins_detalle' => strtoupper($post['ins_detalle']), /// Insumo Detalle
                            'ins_cant_requerida' => $post['ins_cantidad'], /// Cantidad Requerida
                            'ins_costo_unitario' => $post['ins_costo_unitario'], /// Costo Unitario
                            'ins_costo_total' => $post['ins_costo_total'], /// Costo Total
                            'ins_tipo' => $post['ins_tipo'], /// Ins Tipo
                            'par_id' => $post['ins_partidas_dependientes'], /// Partidas
                            'fun_id' => $this->session->userdata("fun_id"), /// Funcionario
                            'ins_tipo' => $ins_tipo, /// tipo insumo
                            'insp_id'=>$post['insp_id'],
                            'ins_gestion'=>$this->gestion,///
                        );

                    }
                    elseif($ins_tipo==2){
                        $nro_ins=$conf[0]['conf_servicios']+1;

                        $update_conf = array('conf_servicios' => $nro_ins);
                        $this->db->where('ide', $this->gestion);
                        $this->db->update('configuracion', $update_conf);

                        $query=$this->db->query('set datestyle to DMY');
                        $data_to_store = array(
                            'ins_codigo' => $this->session->userdata("name").'/INS/SERV/'.$this->gestion.'/'.$nro_ins, /// Codigo Insumo
                            'ins_fecha_requerimiento' => $post['ins_fecha'], /// Fecha de Requerimiento
                            'ins_detalle' => strtoupper($post['ins_detalle']), /// Insumo Detalle
                            'ins_cant_requerida' => $post['ins_cantidad'], /// Cantidad Requerida
                            'ins_costo_unitario' => $post['ins_costo_unitario'], /// Costo Unitario
                            'ins_costo_total' => $post['ins_costo_total'], /// Costo Total
                            'ins_tipo' => $post['ins_tipo'], /// Ins Tipo
                            'par_id' => $post['ins_partidas_dependientes'], /// Partidas
                            'fun_id' => $this->session->userdata("fun_id"), /// Funcionario
                            'ins_tipo' => $ins_tipo, /// tipo insumo
                            'insp_id'=>$post['insp_id'], ///
                            'ins_gestion'=>$this->gestion,///
                        );

                    }
                    elseif($ins_tipo==3){
                        $nro_ins=$conf[0]['conf_pasajes']+1;

                        $update_conf = array('conf_pasajes' => $nro_ins);
                        $this->db->where('ide', $this->gestion);
                        $this->db->update('configuracion', $update_conf);

                        $query=$this->db->query('set datestyle to DMY');
                        $data_to_store = array(
                            'ins_codigo' => $this->session->userdata("name").'/INS/PAS/'.$this->gestion.'/'.$nro_ins, /// Codigo Insumo
                            'ins_fecha_requerimiento' => $post['ins_fecha'], /// Fecha de Requerimiento
                            'ins_detalle' => strtoupper($post['ins_detalle']), /// Insumo Detalle
                            'ins_cant_requerida' => $post['ins_cantidad'], /// Cantidad Requerida
                            'ins_costo_unitario' => $post['ins_costo_unitario'], /// Costo Unitario
                            'ins_costo_total' => $post['ins_costo_total'], /// Costo Total
                            'ins_tipo' => $post['ins_tipo'], /// Ins Tipo
                            'par_id' => $post['ins_partidas_dependientes'], /// Partidas
                            'fun_id' => $this->session->userdata("fun_id"), /// Funcionario
                            'ins_tipo' => $ins_tipo, /// tipo insumo
                            'insp_id'=>$post['insp_id'],
                            'ins_gestion'=>$this->gestion,///
                        );

                    }
                    elseif($ins_tipo==4){
                        $nro_ins=$conf[0]['conf_viaticos']+1;

                        $update_conf = array('conf_viaticos' => $nro_ins);
                        $this->db->where('ide', $this->gestion);
                        $this->db->update('configuracion', $update_conf);

                        $query=$this->db->query('set datestyle to DMY');
                        $data_to_store = array(
                            'ins_codigo' => $this->session->userdata("name").'/INS/VIA/'.$this->gestion.'/'.$nro_ins, /// Codigo Insumo
                            'ins_fecha_requerimiento' => $post['ins_fecha'], /// Fecha de Requerimiento
                            'ins_detalle' => strtoupper($post['ins_detalle']), /// Insumo Detalle
                            'ins_cant_requerida' => $post['ins_cantidad'], /// Cantidad Requerida
                            'ins_costo_unitario' => $post['ins_costo_unitario'], /// Costo Unitario
                            'ins_costo_total' => $post['ins_costo_total'], /// Costo Total
                            'ins_tipo' => $post['ins_tipo'], /// Ins Tipo
                            'par_id' => $post['ins_partidas_dependientes'], /// Partidas
                            'fun_id' => $this->session->userdata("fun_id"), /// Funcionario
                            'ins_tipo' => $ins_tipo, /// tipo insumo
                            'insp_id'=>$post['insp_id'],
                            'ins_gestion'=>$this->gestion,///
                        );

                    }
                    elseif($ins_tipo==5){
                        $nro_ins=$conf[0]['conf_cons_producto']+1;

                        $update_conf = array('conf_cons_producto' => $nro_ins);
                        $this->db->where('ide', $this->gestion);
                        $this->db->update('configuracion', $update_conf);

                        $query=$this->db->query('set datestyle to DMY');
                        $data_to_store = array(
                            'ins_codigo' => $this->session->userdata("name").'/INS/CP/'.$this->gestion.'/'.$nro_ins, /// Codigo Insumo
                            'ins_fecha_requerimiento' => $post['ins_fecha'], /// Fecha de Requerimiento
                            'ins_detalle' => strtoupper($post['ins_detalle']), /// Insumo Detalle
                            'ins_objetivo' => strtoupper($post['objetivo']), /// Insumo Objetivo
                            'ins_duracion' => $post['ins_duracion'], /// Insumo Duracion
                            'ins_fecha_inicio' => $post['ins_i'], /// Fecha de Inicio
                            'ins_fecha_conclusion' => $post['ins_f'], /// Fecha Final
                            'ins_productos' => strtoupper($post['ins_prod']), /// Insumo Producto
                            'ins_evaluador' => strtoupper($post['ins_eva']), /// Insumo Evaluacion
                            'ins_cant_requerida' => $post['ins_cantidad'], /// Cantidad Requerida
                            'ins_costo_unitario' => $post['ins_costo_unitario'], /// Costo Unitario
                            'ins_costo_total' => $post['ins_costo_total'], /// Costo Total
                            'ins_tipo' => $post['ins_tipo'], /// Ins Tipo
                            'par_id' => $post['ins_partidas_dependientes'], /// Partidas
                            'fun_id' => $this->session->userdata("fun_id"), /// Funcionario
                            'ins_tipo' => $ins_tipo, /// tipo insumo
                            'insp_id'=>$post['insp_id'],
                            'ins_gestion'=>$this->gestion,///
                        );

                    }
                    elseif($ins_tipo==6){
                        $nro_ins=$conf[0]['conf_cons_linea']+1;

                        $update_conf = array('conf_cons_linea' => $nro_ins);
                        $this->db->where('ide', $this->gestion);
                        $this->db->update('configuracion', $update_conf);

                        $query=$this->db->query('set datestyle to DMY');
                        $data_to_store = array(
                            'ins_codigo' => $this->session->userdata("name").'/INS/CL/'.$this->gestion.'/'.$nro_ins, /// Codigo Insumo
                            'ins_fecha_requerimiento' => $post['ins_fecha'], /// Fecha de Requerimiento
                            'ins_duracion' => $post['ins_duracion'], /// Insumo Duracion
                            'ins_fecha_inicio' => $post['ins_i'], /// Fecha de Inicio
                            'ins_fecha_conclusion' => $post['ins_f'], /// Fecha Final
                            'ins_actividades' => strtoupper($post['ins_act']), /// Insumo actividades del consultor
                            'ins_perfil' => strtoupper($post['ins_perfil']), /// Insumo perfil del consultor
                            'ins_cargo' => strtoupper($post['ins_cargo']), /// Insumo cargo del consultor
                            'ins_evaluador' => strtoupper($post['ins_perfil']), /// Insumo evaluador del consultor
                            'car_id' => $post['ins_car_id'], /// cargo id
                            'ins_cant_requerida' => $post['ins_cantidad'], /// Cantidad Requerida
                            'ins_costo_unitario' => $post['ins_costo_unitario'], /// Costo Unitario
                            'ins_costo_total' => $post['ins_costo_total'], /// Costo Total
                            'ins_tipo' => $post['ins_tipo'], /// Ins Tipo
                            'par_id' => $post['ins_partidas_dependientes'], /// Partidas
                            'fun_id' => $this->session->userdata("fun_id"), /// Funcionario
                            'ins_tipo' => $ins_tipo, /// tipo insumo
                            'insp_id'=>$post['insp_id'],
                            'ins_gestion'=>$this->gestion,///
                        );

                    }
                    elseif($ins_tipo==7){
                        $nro_ins=$conf[0]['conf_materiales']+1;

                        $update_conf = array('conf_materiales' => $nro_ins);
                        $this->db->where('ide', $this->gestion);
                        $this->db->update('configuracion', $update_conf);

                        $query=$this->db->query('set datestyle to DMY');
                        $data_to_store = array(
                            'ins_codigo' => $this->session->userdata("name").'/INS/MAT/'.$this->gestion.'/'.$nro_ins, /// Codigo Insumo
                            'ins_fecha_requerimiento' => $post['ins_fecha'], /// Fecha de Requerimiento
                            'ins_detalle' => strtoupper($post['ins_detalle']), /// Insumo Detalle
                            'ins_unidad_medida' => strtoupper($post['ins_unidad']), /// Insumo Unidad de Medida
                            'ins_cant_requerida' => $post['ins_cantidad'], /// Cantidad Requerida
                            'ins_costo_unitario' => $post['ins_costo_unitario'], /// Costo Unitario
                            'ins_costo_total' => $post['ins_costo_total'], /// Costo Total
                            'ins_tipo' => $post['ins_tipo'], /// Ins Tipo
                            'par_id' => $post['ins_partidas_dependientes'], /// Partidas
                            'fun_id' => $this->session->userdata("fun_id"), /// Funcionario
                            'ins_tipo' => $ins_tipo, /// tipo insumo
                            'insp_id'=>$post['insp_id'],
                            'ins_gestion'=>$this->gestion,///
                        );

                    }
                    elseif($ins_tipo==8){
                        $nro_ins=$conf[0]['conf_activos']+1;

                        $update_conf = array('conf_activos' => $nro_ins);
                        $this->db->where('ide', $this->gestion);
                        $this->db->update('configuracion', $update_conf);

                        $query=$this->db->query('set datestyle to DMY');
                        $data_to_store = array(
                            'ins_codigo' => $this->session->userdata("name").'/INS/AF/'.$this->gestion.'/'.$nro_ins, /// Codigo Insumo
                            'ins_fecha_requerimiento' => $post['ins_fecha'], /// Fecha de Requerimiento
                            'ins_detalle' => strtoupper($post['ins_detalle']), /// Insumo Detalle
                            'ins_unidad_medida' => strtoupper($post['ins_unidad']), /// Insumo Unidad de Medida
                            'ins_cant_requerida' => $post['ins_cantidad'], /// Cantidad Requerida
                            'ins_costo_unitario' => $post['ins_costo_unitario'], /// Costo Unitario
                            'ins_costo_total' => $post['ins_costo_total'], /// Costo Total
                            'ins_tipo' => $post['ins_tipo'], /// Ins Tipo
                            'par_id' => $post['ins_partidas_dependientes'], /// Partidas
                            'fun_id' => $this->session->userdata("fun_id"), /// Funcionario
                            'ins_tipo' => $ins_tipo, /// tipo insumo
                            'insp_id'=>$post['insp_id'],
                        );

                    }
                    elseif($ins_tipo==9){
                        $nro_ins=$conf[0]['conf_otros_insumos']+1;

                        $update_conf = array('conf_otros_insumos' => $nro_ins);
                        $this->db->where('ide', $this->gestion);
                        $this->db->update('configuracion', $update_conf);

                        $query=$this->db->query('set datestyle to DMY');
                        $data_to_store = array(
                            'ins_codigo' => $this->session->userdata("name").'/INS/OI/'.$this->gestion.'/'.$nro_ins, /// Codigo Insumo
                            'ins_fecha_requerimiento' => $post['ins_fecha'], /// Fecha de Requerimiento
                            'ins_detalle' => strtoupper($post['ins_detalle']), /// Insumo Detalle
                            'ins_unidad_medida' => strtoupper($post['ins_unidad']), /// Insumo Unidad de Medida
                            'ins_cant_requerida' => $post['ins_cantidad'], /// Cantidad Requerida
                            'ins_costo_unitario' => $post['ins_costo_unitario'], /// Costo Unitario
                            'ins_costo_total' => $post['ins_costo_total'], /// Costo Total
                            'ins_tipo' => $post['ins_tipo'], /// Ins Tipo
                            'par_id' => $post['ins_partidas_dependientes'], /// Partidas
                            'fun_id' => $this->session->userdata("fun_id"), /// Funcionario
                            'ins_tipo' => $ins_tipo, /// tipo insumo
                            'insp_id'=>$post['insp_id'],
                            'ins_gestion'=>$this->gestion,///
                        );

                    }

                    $this->db->insert('insumos', $data_to_store); ///// Guardar en Tabla Insumos
                    $ins_id=$this->db->insert_id();
                    /*---------------------------------------------------------*/
                    /*----------------------------------------------------------*/
                    $data_to_store2 = array( ///// Tabla InsumoActividad
                        'act_id' => $act_id, /// act_id
                        'ins_id' => $ins_id, /// ins_id
                    );
                    $this->db->insert('_insumoactividad', $data_to_store2);
                    /*----------------------------------------------------------*/
                    /*$gestion=$fase[0]['pfec_fecha_inicio'];
                    $insg=0;

                    if (!empty($_POST["gest"]) && is_array($_POST["gest"]) )
                    {
                        foreach ( array_keys($_POST["gest"]) as $como  )
                        {
                            $data_to_store = array(
                                'ins_id' => $ins_id, /// Id Insumo
                                'g_id' => $gestion, /// Gestion
                                'insg_monto_prog' => $_POST["gest"][$como], /// Monto programado
                            );
                            $this->db->insert('insumo_gestion', $data_to_store); ///// Guardar en Tabla Insumo Gestion
                            $insg_id=$this->db->insert_id();


                            if($gestion==$this->gestion){$ins_g=$insg_id;}

                            $gestion++;
                        }
                    }*/

                    echo '<script> alert("EL REGISTRO SE REGISTRO CORRECTAMENTE")</script>';
                    redirect(site_url("") . '/prog/ins_act/'.$proy_id.'/'.$prod_id.'/'.$act_id.'/'.$insp_id);
                    /*------------------------------------------------------------------------------------*/
                }
                else
                {
                    redirect(site_url("") . '/prog/nuevo_ins_a/'.$ins_tipo.'/'.$proy_id.'/'.$prod_id.'/'.$act_id.'/error_fecha');
                }
            }
            else
            {
                redirect(site_url("") . '/prog/nuevo_ins_a/'.$ins_tipo.'/'.$proy_id.'/'.$prod_id.'/'.$act_id.'/false');
            }

        } else {
            show_404();
        }
    }


    /*-------------------- VALIDAR UPDATE INSUMO A NIVEL COMPONENTE --------------------*/
    function guardar_update_insumo()
    {
        if ($this->input->post()) {
            $post = $this->input->post();
            $proy_id = $post['proy_id']; // proy id
            $prod_id = $post['prod_id'];//prod id
            $act_id = $post['act_id'];//act id
            $ins_id = $post['ins_id']; // ins id
            $ins_tipo = $post['ins_tipo'];//tipo de insumo
            $gest = $post['gestion'];//gestion
            $costo_anterior = $post['ins_costo_total_ant'];
            $insp_id= $post['insp_id'];
            //  $cant_fin = $post['cant_fin'];//cantidad de financiamiento

            $fase = $this->model_faseetapa->get_id_fase($proy_id); //// DATOS DE LA FASE ACTIVA

            $veriff = $this->verif_fecha($post['ins_fecha']); //// verifica de requerimiento

            if($ins_tipo>=1 && $ins_tipo<=9)
            {
                if($veriff=='true') //// verif fecha de requerimiento
                {
                    /*------------------------------------------------------------------------------------*/
                    if($ins_tipo==1 || $ins_tipo==2 || $ins_tipo==3 || $ins_tipo==4 || $ins_tipo==7 || $ins_tipo==8 || $ins_tipo==9){
                        /*------------------------- Update Insumos ------------------------*/
                        $query=$this->db->query('set datestyle to DMY');
                        $update_ins = array(
                            'ins_fecha_requerimiento' => $post['ins_fecha'], /// Fecha de Requerimiento
                            'ins_detalle' => strtoupper($post['ins_detalle']), /// Insumo Detalle
                            'ins_unidad_medida' => strtoupper($post['ins_unidad_medida']), /// Unidad de Medida
                            'ins_cant_requerida' => $post['ins_cantidad'], /// Cantidad Requerida
                            'ins_costo_unitario' => $post['ins_costo_unitario'], /// Costo Unitario
                            'ins_costo_total' => $post['ins_costo_total'], /// Costo Total
                            'fun_id' => $this->session->userdata("fun_id") /// Funcionario
                        );
                        /*--------------------------------------------------------------------*/
                    }
                    elseif($ins_tipo==5){
                        /*------------------------- Update Insumos ------------------------*/
                        $query=$this->db->query('set datestyle to DMY');
                        $update_ins = array(
                            'ins_fecha_requerimiento' => $post['ins_fecha'], /// Fecha de Requerimiento
                            'ins_detalle' => strtoupper($post['ins_detalle']), /// Insumo Detalle
                            'ins_objetivo' => strtoupper($post['objetivo']), /// Insumo Objetivo
                            'ins_duracion' => $post['ins_duracion'], /// Insumo Duracion
                            'ins_fecha_inicio' => $post['ins_i'], /// Fecha de Inicio
                            'ins_fecha_conclusion' => $post['ins_f'], /// Fecha Final
                            'ins_productos' => strtoupper($post['ins_prod']), /// Insumo Producto
                            'ins_evaluador' => strtoupper($post['ins_eva']), /// Insumo Evaluacion
                            'ins_cant_requerida' => $post['ins_cantidad'], /// Cantidad Requerida
                            'ins_costo_unitario' => $post['ins_costo_unitario'], /// Costo Unitario
                            'ins_costo_total' => $post['ins_costo_total'], /// Costo Total
                            'fun_id' => $this->session->userdata("fun_id") /// Funcionario
                        );
                        /*--------------------------------------------------------------------*/
                    }
                    elseif($ins_tipo==6){
                        /*------------------------- Update Insumos ------------------------*/
                        $query=$this->db->query('set datestyle to DMY');
                        $update_ins = array(
                            'ins_fecha_requerimiento' => $post['ins_fecha'], /// Fecha de Requerimiento
                            'ins_duracion' => $post['ins_duracion'], /// Insumo Duracion
                            'ins_objetivo' => strtoupper($post['objetivo']), /// Insumo Objetivo
                            'ins_duracion' => $post['ins_duracion'], /// Insumo Duracion
                            'ins_fecha_inicio' => $post['ins_i'], /// Fecha de Inicio
                            'ins_fecha_conclusion' => $post['ins_f'], /// Fecha Final
                            'ins_actividades' => strtoupper($post['ins_act']), /// Insumo actividades del consultor
                            'ins_perfil' => strtoupper($post['ins_perfil']), /// Insumo perfil del consultor
                            'ins_cargo' => strtoupper($post['ins_cargo']), /// Insumo cargo del consultor
                            'ins_evaluador' => strtoupper($post['ins_perfil']), /// Insumo evaluador del consultor
                            'car_id' => $post['ins_car_id'], /// cargo id
                            'ins_cant_requerida' => $post['ins_cantidad'], /// Cantidad Requerida
                            'ins_costo_unitario' => $post['ins_costo_unitario'], /// Costo Unitario
                            'ins_costo_total' => $post['ins_costo_total'], /// Costo Total
                            'fun_id' => $this->session->userdata("fun_id") /// Funcionario
                        );
                        /*--------------------------------------------------------------------*/
                    }
                    $this->db->where('ins_id', $ins_id);
                    $this->db->update('insumos', $update_ins);

                    /*$insumo_gestion = $this->minsumos->list_insumos_gestion($ins_id); //// DATOS DEL INSUMO GESTION
                    $gestion=$fase[0]['pfec_fecha_inicio'];
                    $ins_g=0;
                    if(count($insumo_gestion)!=0){
                        //exit(count($insumo_gestion));
                        //------------------ Update Insumo Gestion ---------------
                        if (!empty($_POST["gest"]) && is_array($_POST["gest"]) )
                        {
                            foreach ( array_keys($_POST["gest"]) as $como  )
                            {
                                $update_insg = array(
                                    'ins_id' => $ins_id, /// Id Insumo
                                    'g_id' => $gestion, /// Gestion
                                    'insg_monto_prog' => $_POST["gest"][$como], /// Monto programado
                                    'insg_estado' => 2 /// Insumo GEstion Modificado
                                );
                                $this->db->where('insg_id', $_POST["insg"][$como]);
                                $this->db->update('insumo_gestion', $update_insg);

                                //*--------------------- MODIFICANDO EL MONTO FINANCIERO -----------------
                                if($costo_anterior!=$post['ins_costo_total']) //// Se Modifico El Presupuesto
                                {
                                    $insumo_fin = $this->minsumos->list_insumo_financiamiento($_POST["insg"][$como]); //// DATOS DEL INSUMO FINANCIAMIENTO
                                    foreach ($insumo_fin as $row)
                                    {
                                        $query=$this->db->query('set datestyle to DMY');
                                        $update_ifin = array(
                                            'ifin_monto' => 0, /// Modificando el monto
                                            'ifin_estado' => 2 /// Estado Modificado
                                        );
                                        $this->db->where('insg_id', $_POST["insg"][$como]);
                                        $this->db->update('insumo_financiamiento', $update_ifin);
                                    }
                                }
                                //*----------------------------------------------------------------------
                                if($gestion==$this->gestion){$ins_g=$_POST["insg"][$como];}
                                $gestion++;

                            }
                        }
                        //*----------------------------------------------------
                    }
                    else{
                        if (!empty($_POST["gest"]) && is_array($_POST["gest"]) )
                        {
                            //*------------------ Inserta Insumo Gestion ---------------
                            foreach ( array_keys($_POST["gest"]) as $como  )
                            {
                                $data_to_store = array(
                                    'ins_id' => $ins_id, /// Id Insumo
                                    'g_id' => $gestion, /// Gestion
                                    'insg_monto_prog' => $_POST["gest"][$como], /// Monto programado
                                );
                                $this->db->insert('insumo_gestion', $data_to_store); ///// Guardar en Tabla Insumo Gestion
                                $insg_id=$this->db->insert_id();

                                if($gestion==$this->gestion){$ins_g=$insg_id;}
                                $gestion++;
                            }
                            //*----------------------------------------------------
                        }
                    }*/

                    redirect(site_url("") . '/prog/ins_act/'.$proy_id.'/'.$prod_id.'/'.$act_id.'/'.$insp_id);
                }
                else{
                    redirect(site_url("") . '/prog/mod_ins_a/'.$ins_tipo.'/'.$proy_id.'/'.$prod_id.'/'.$act_id.'/'.$ins_id.'/'.$gest.'/error_fecha');
                }
            }
            else{
                redirect(site_url("") . '/prog/mod_ins_a/'.$ins_tipo.'/'.$proy_id.'/'.$prod_id.'/'.$act_id.'/'.$ins_id.'/'.$gest.'/false');
            }


        } else {
            show_404();
        }
    }

    ///FORM INSUMO PROGRAMADO POR TIPO DE INSUMO
    function insumo_programado($tipo_insumo,$proy_id,$prod_id,$act_id,$ins_id,$insg_id,$insp_id)
    {
        if($tipo_insumo!='' & $proy_id!='' & $prod_id!='' & $act_id!='' & $ins_id!='' & $insg_id!='' & $insp_id!=''){
            $enlaces=$this->menu_modelo->get_Modulos(1);
            $data['enlaces'] = $enlaces;
            for($i=0;$i<count($enlaces);$i++)
            {
                $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
            }
            $data['subenlaces'] = $subenlaces;
            $data['proyecto'] = $this->model_proyecto->get_id_proyecto($proy_id);
            $data['fase'] = $this->model_faseetapa->get_id_fase($proy_id); //// DATOS DE LA FASE ACTIVA
            $data['insumo'] = $this->minsumos->get_dato_insumo($ins_id); //// DATOS DEL INSUMO
            $data['insumo_gest'] = $this->minsumos->get_dato_insumo_gestion($insg_id,$ins_id); //// DATOS DEL INSUMO GESTION
            $data['list_ig'] = $this->minsumos->list_insumos_gestion($ins_id); //// LISTA DE INSUMOS GESTIONES

            $titulo_proy=strtoupper($data['proyecto'][0]['tipo']);
            $data['titulo_proy'] = $titulo_proy;   /// Titulo Tipo de la Operacion

            $data['gestion'] = $data['insumo_gest'][0]['g_id'];
            $data['proy_id'] = $proy_id;
            $data['prod_id'] = $prod_id;
            $data['act_id'] = $act_id;
            $data['insp_id'] = $insp_id;
            $data['ins_tipo'] = $tipo_insumo;
            $data['dato_proy'] = $this->minsumos->dato_proyecto($proy_id);
            $data['dato_prod'] = $this->minsumos->dato_producto($prod_id);
            $data['dato_act'] = $this->minsumos->dato_actividad($act_id);
            $data['ins_titulo'] = $this->get_ins_titulo($tipo_insumo);
            $data['ins_tipo'] = $tipo_insumo;
            //----------------------------------------------------------------------------------------------------------------------------
            $data['lista_fuentes_techo'] = $this->minsumos_delegado->tabla_presupuesto($proy_id, $data['insumo_gest'][0]['g_id']);//lista de fuentes y su presupuesto asignado
            $data['saldo_total'] = $this->minsumos_delegado->saldo_total_fin($proy_id, $data['insumo_gest'][0]['g_id']);//SALDO TOTAL DEL TECHO PRESUPUESTARIO
            $data['lista_entidad'] = $this->model_entidad_tras->lista_entidad_tras();//entidad transferencia
            //$data['techo_mes'] = $this->minsumos_delegado->sum_prog_mensual_actividades($proy_id, $this->gestion, $com_id);//TECHO POR MES DE SUMA DE ACTIVIDADES

            // echo "Fase id : ".$data['fase'][0]['id'].'-'.$data['gestion'];
            $fase_gest = $this->model_faseetapa->fase_etapa_gestion($data['fase'][0]['id'],$data['insumo_gest'][0]['g_id']); //// Lista de las gestiones de la Fase
            $data['list_fuentes'] = $this->model_faseetapa->fase_presupuesto_partida($fase_gest[0]['ptofecg_id']); //// lista del presupuesto asignado

            $vmes[1]='mes1';
            $vmes[2]='mes2';
            $vmes[3]='mes3';
            $vmes[4]='mes4';
            $vmes[5]='mes5';
            $vmes[6]='mes6';
            $vmes[7]='mes7';
            $vmes[8]='mes8';
            $vmes[9]='mes9';
            $vmes[10]='mes10';
            $vmes[11]='mes11';
            $vmes[12]='mes12';

            $data['vmes']=$vmes;
            //  $data['l_ins_fin'] = $this->minsumos->list_insumo_financiamiento($insg_id); //// lista de insumos de financiamiento por insumo gestion

            $data['atras']='prog/mod_ins_a/'.$tipo_insumo.'/'.$proy_id.'/'.$prod_id.'/'.$act_id.'/'.$ins_id.'/'.$this->gestion;
            $this->load->view('admin/programacion/insumos/insumo_actividades/requerimientos/form_insumo_act_prog', $data);
        }
        else{
            redirect('admin/dashboard');
        }
    }

    /*---------------------- Guardar Insumo - Programado -----------------------*/
    function guardar_insumo_programado()
    {
        if ($this->input->post()) {
            $post = $this->input->post();
            $proy_id = $post['proy_id']; /// proy_id
            $prod_id = $post['prod_id']; /// prod_id
            $act_id = $post['act_id']; /// Act_id
            $ins_id = $post['ins_id']; /// ins_id
            $insg_id = $post['insg_id']; /// insg_id
            $gestion = $post['gestion']; /// gestion

            $insumo = $this->minsumos->get_insumo($ins_id); //// DATOS DEL INSUMO
            $mes[1]="m1";$mes[2]="m2";$mes[3]="m3";$mes[4]="m4";$mes[5]="m5";$mes[6]="m6";$mes[7]="m7";$mes[8]="m8";$mes[9]="m9";$mes[10]="m10";$mes[11]="m11";$mes[12]="m12";

            $nro_if=1;
            if ( !empty($_POST["ff"]) && is_array($_POST["ff"]) )
            {
                foreach ( array_keys($_POST["ff"]) as $como  )
                {
                    // echo "Ins_id :".$ins_id." ff :".$_POST["ff"][$como]." of :".$_POST["of"][$como]." et :".$_POST["et"][$como]." monto asig :".$_POST["monto_asig"][$como]." Gestion :".$insumo->ins_gestion."<br>";

                    if($_POST["monto_asig"][$como]!=0 & $_POST["ifin_id"][$como]==0) /// No tiene monto y no tiene ifin_id (Inserta Datos)
                    {
                        /*--------------------- GUARDA INSUMO FINANCIAMIENTO --------------------*/
                        $query=$this->db->query('set datestyle to DMY');
                        $data_to_store = array( ///// Tabla insumos
                            'insg_id' => $insg_id, /// Id Insumo Gestion
                            'ffofet_id' => $_POST["ffofet_id"][$como], /// Ffofet Id
                            'ff_id' => $_POST["ff"][$como], /// Fuente de Financiamiento
                            'of_id' => $_POST["of"][$como], /// Organismo Financiador
                            'et_id' => $_POST["et"][$como], /// Entidad de Transferencia
                            'ifin_monto' => $_POST["monto_asig"][$como], /// Monto Asignado
                            'ifin_gestion' => $gestion, /// Gestion
                            'nro_if' => $nro_if, /// nro if
                        );
                        $this->db->insert('insumo_financiamiento', $data_to_store); ///// Guardar en Tabla Insumo Financiamiento
                        $ifin_id=$this->db->insert_id();

                        for ($i=1; $i <=12 ; $i++) {
                            if($_POST[$mes[$i]][$como]!=0)
                            {
                                /*--------------------- GUARDA INSUMO FINANCIAMIENTO --------------------*/
                                $data_to_store2 = array( ///// Tabla ifin_prog_mes
                                    'ifin_id' => $ifin_id, /// Id Insumo Financiamiento
                                    'mes_id' => $i, /// Mes Id
                                    'ipm_fis' => $_POST[$mes[$i]][$como], /// Programado
                                );
                                $this->db->insert('ifin_prog_mes', $data_to_store2); ///// Guardar en Tabla Insumos
                                /*-----------------------------------------------------------------------*/
                            }
                        }

                    }
                    elseif($_POST["monto_asig"][$como]==0 & $_POST["ifin_id"][$como]!=0) /// No tiene monto , pero tiene ifin_id (Elimina las dos tablas)
                    {
                        /*----------------- ELIMINA IFIN PROG MES---------------*/
                        $this->db->where('ifin_id', $_POST["ifin_id"][$como]);
                        $this->db->delete('ifin_prog_mes');
                        /*------------------------------------------------------*/

                        /*----------------- ELIMINA IFIN PROG MES---------------*/
                        $this->db->where('ifin_id', $_POST["ifin_id"][$como]);
                        $this->db->delete('insumo_financiamiento');
                        /*------------------------------------------------------*/


                    }
                    elseif($_POST["monto_asig"][$como]!=0 & $_POST["ifin_id"][$como]!=0) /// tiene monto Asignado y tiene ifin_id (Actualizar las dos tablas)
                    {
                        /*----------------- ACTUALIZA DATOS INSUMOS FIN --------------------*/
                        $query=$this->db->query('set datestyle to DMY');
                        $update_if = array(
                            'ifin_monto' => $_POST["monto_asig"][$como], /// Monto Asignado
                            'et_id' => $_POST["et"][$como], /// Entidad de Transferencia
                            'ifin_gestion' => $gestion, /// Gestion;
                            'nro_if' => $nro_if
                        );
                        $this->db->where('ifin_id', $_POST["ifin_id"][$como]);
                        $this->db->update('insumo_financiamiento', $update_if);
                        /*------------------------------------------------------------------*/
                        /*----------------- ELIMINA IFIN PROG MES---------------*/
                        $this->db->where('ifin_id', $_POST["ifin_id"][$como]);
                        $this->db->delete('ifin_prog_mes');
                        /*------------------------------------------------------*/

                        /*----------------- INSERTA INSUMO PROGRAMADO MES --------------*/
                        for ($i=1; $i <=12 ; $i++) {
                            if($_POST[$mes[$i]][$como]!=0)
                            {
                                /*--------------------- GUARDA INSUMO FINANCIAMIENTO --------------------*/
                                $data_to_store2 = array( ///// Tabla ifin_prog_mes
                                    'ifin_id' => $_POST["ifin_id"][$como], /// Id Insumo Financiamiento
                                    'mes_id' => $i, /// Mes Id
                                    'ipm_fis' => $_POST[$mes[$i]][$como], /// Programado
                                );
                                $this->db->insert('ifin_prog_mes', $data_to_store2); ///// Guardar en Tabla Insumos
                                /*-----------------------------------------------------------------------*/
                            }
                        }

                    }

                    $nro_if++;

                }
            }

            echo '<script> alert("EL REGISTRO SE REGISTRO CORRECTAMENTE")</script>';
            //redirect(site_url("") . '/prog/ins_c_prog/'.$proy_id.'/'.$com_id.'/'.$ins_id.'/'.$insg_id.'/true'); /// directamente al formulario programado
            redirect(site_url("") . '/prog/ins_act/'.$proy_id.'/'.$prod_id.'/'.$act_id.'/true'); /// directamente a la lista de requerimientos
        } else {
            show_404();
        }
    }

    /*---------------------- Guardar Insumo - Ejecutado -----------------------*/
    function guardar_insumo_ejecutado()
    {
        if ($this->input->post()) {
            $post = $this->input->post();
            $mod = $post['mod']; /// mod
            $proy_id = $post['proy_id']; /// proy_id
            $prod_id = $post['prod_id']; /// prod_id
            $act_id = $post['act_id']; /// act_id
            $ins_id = $post['ins_id']; /// ins_id
            $insg_id = $post['insg_id']; /// insg_id
            $gestion = $post['gestion']; /// gestion

            $fase = $this->model_faseetapa->get_id_fase($proy_id); //// DATOS DE LA FASE ACTIVA

            $insumo = $this->minsumos->get_insumo($ins_id); //// DATOS DEL INSUMO
            $mes[1]="m1";$mes[2]="m2";$mes[3]="m3";$mes[4]="m4";$mes[5]="m5";$mes[6]="m6";$mes[7]="m7";$mes[8]="m8";$mes[9]="m9";$mes[10]="m10";$mes[11]="m11";$mes[12]="m12";

            $nro_if=1;
            if ( !empty($_POST["ff"]) && is_array($_POST["ff"]) )
            {
                foreach ( array_keys($_POST["ff"]) as $como  )
                {
                    // echo "Ins_id :".$ins_id." ff :".$_POST["ff"][$como]." of :".$_POST["of"][$como]." et :".$_POST["et"][$como]." monto asig :".$_POST["monto_asig"][$como]." Gestion :".$insumo->ins_gestion."<br>";

                    if($_POST["monto_ejec"][$como]!=0 & $_POST["ifin_id"][$como]==0) /// No tiene monto y no tiene ifin_id (Inserta Datos)
                    {
                        /*--------------------- GUARDA INSUMO FINANCIAMIENTO --------------------*/
                        $query=$this->db->query('set datestyle to DMY');
                        $data_to_store = array( ///// Tabla insumos
                            'insg_id' => $insg_id, /// Id Insumo Gestion
                            'ffofet_id' => $_POST["ffofet_id"][$como], /// Ffofet Id
                            'ff_id' => $_POST["ff"][$como], /// Fuente de Financiamiento
                            'of_id' => $_POST["of"][$como], /// Organismo Financiador
                            'et_id' => $_POST["et"][$como], /// Entidad de Transferencia
                            'ifin_gestion' => $gestion, /// Gestion
                            'nro_if' => $nro_if, /// nro if
                        );
                        $this->db->insert('insumo_financiamiento', $data_to_store); ///// Guardar en Tabla Insumo Financiamiento
                        $ifin_id=$this->db->insert_id();

                        for ($i=1; $i <=12 ; $i++) {
                            if($_POST[$mes[$i]][$como]!=0)
                            {
                                /*--------------------- GUARDA INSUMO FINANCIAMIENTO --------------------*/
                                $data_to_store2 = array( ///// Tabla ifin_prog_mes
                                    'ifin_id' => $ifin_id, /// Id Insumo Financiamiento
                                    'mes_id' => $i, /// Mes Id
                                    'ipm_fis' => 0, /// Programado
                                );
                                $this->db->insert('ifin_prog_mes', $data_to_store2); ///// Guardar en Tabla Insumos
                                /*-----------------------------------------------------------------------*/
                                /*--------------------- GUARDA INSUMO FINANCIAMIENTO --------------------*/
                                $data_to_store2 = array( ///// Tabla ifin_prog_mes
                                    'ifin_id' => $ifin_id, /// Id Insumo Financiamiento
                                    'mes_id' => $i, /// Mes Id
                                    'iem_fis' => $_POST[$mes[$i]][$como], /// Ejecutado
                                );
                                $this->db->insert('ifin_ejec_mes', $data_to_store2); ///// Guardar en Tabla Ejecucion Insumos
                                /*-----------------------------------------------------------------------*/
                            }
                        }
                    }
                    elseif($_POST["monto_ejec"][$como]==0 & $_POST["ifin_id"][$como]!=0) /// No tiene monto , pero tiene ifin_id (Elimina las dos tablas)
                    {
                        /*----------------- ELIMINA IFIN EJEC MES---------------*/
                        $this->db->where('ifin_id', $_POST["ifin_id"][$como]);
                        $this->db->delete('ifin_ejec_mes');
                        /*------------------------------------------------------*/
                    }
                    elseif($_POST["monto_ejec"][$como]!=0 & $_POST["ifin_id"][$como]!=0) /// tiene monto Asignado y tiene ifin_id (Actualizar las dos tablas)
                    {
                        /*----------------- ELIMINA IFIN EJEC MES---------------*/
                        $this->db->where('ifin_id', $_POST["ifin_id"][$como]);
                        $this->db->delete('ifin_ejec_mes');
                        /*------------------------------------------------------*/
                        /*----------------- INSERTA INSUMO EJECUTADO MES --------------*/
                        for ($i=1; $i <=12 ; $i++) {
                            if($_POST[$mes[$i]][$como]!=0)
                            {
                                /*--------------------- GUARDA INSUMO FINANCIAMIENTO --------------------*/
                                $data_to_store2 = array( ///// Tabla ifin_prog_mes
                                    'ifin_id' => $_POST["ifin_id"][$como], /// Id Insumo Financiamiento
                                    'mes_id' => $i, /// Mes Id
                                    'iem_fis' => $_POST[$mes[$i]][$como], /// Programado
                                );
                                $this->db->insert('ifin_ejec_mes', $data_to_store2); ///// Guardar en Tabla Ejecucion Insumos
                                /*-----------------------------------------------------------------------*/
                            }
                        }

                    }
                    $nro_if++;
                }
            }

            echo '<script> alert("LA OPERACION SE EJECUTO CORRECTAMENTE")</script>';
            redirect(site_url("admin") . '/prog/insumos_act/'.$mod.'/'.$proy_id.'/'.$fase[0]['id'].'/'.$prod_id.'/'.$act_id.'/true'); /// directamente a la lista de requerimientos
        } else {
            show_404();
        }
    }

    //GENERAR LA TABLA DE PRESUPUESTO DE MI PROYECTO
    function genera_tabla_presupuesto($proy_id, $gestion)
    {
        $lista_presupuesto = $this->minsumos_delegado->tabla_presupuesto($proy_id, $gestion);
        $saldo_total = $this->minsumos_delegado->saldo_total_fin($proy_id, $gestion);
        $tabla = '';
        $cont = 1;
        foreach ($lista_presupuesto as $row) {
            $tabla .= '<tr>';
            $tabla .= '<td>' . $cont . '</td>';
            $tabla .= '<td>' . $row['ff_codigo'] . '</td>';
            $tabla .= '<td>' . $row['ff_descripcion'] . '</td>';
            $tabla .= '<td>' . $row['of_codigo'] . '</td>';
            $tabla .= '<td>' . $row['of_descripcion'] . '</td>';
            $tabla .= '<td style="text-align: right;">' . number_format($row['presupuesto_asignado'], 2, ',', '.') . '</td>';
            $tabla .= '<td style="text-align: right;">' . number_format($row['presupuesto_programado'], 2, ',', '.') . '</td>';
            $tabla .= '<td style="text-align: right;">' . number_format($row['saldo'], 2, ',', '.') . '</td>';
            $tabla .= '</tr>';
            $cont++;
        }
        $tabla .= '<tr>';
        $tabla .= '<td colspan="5" style="background:#646464;">
                        <b style="color:#fff;"><center>T O T A L</center></b>
                   </td>';
        $tabla .= '<td style="text-align: right; background:#646464;"><b style="color:#fff;">' . number_format($saldo_total->asignado, 2, ',', '.') . '</b></td>';
        $tabla .= '<td style="text-align: right; background:#646464;"><b style="color:#fff;">' . number_format($saldo_total->programado, 2, ',', '.') . '</b></td>';
        $tabla .= '<td style="text-align: right; background:#646464;"><b style="color:#fff;">' . number_format($saldo_total->saldo_total, 2, ',', '.') . '</b></td>';
        $tabla .= '</tr>';
        return $tabla;

    }

    //OBTENER MIS DATOS DEL FORMULARIO INSUMO
    function get_formulario($post)
    {
        $data = array(
            'ins_fecha_requerimiento' => $this->get_fecha_postgres($post['ins_fecha']),
            'ins_detalle' => strtoupper($this->security->xss_clean($post['ins_detalle'])),
            'ins_unidad_medida' => strtoupper($this->security->xss_clean($post['ins_unidad_medida'])),
            'ins_cant_requerida' => $this->security->xss_clean($post['ins_cantidad']),
            'ins_costo_unitario' => $this->security->xss_clean($post['ins_costo_unitario']),
            'ins_costo_total' => $this->security->xss_clean($post['ins_costo_total']),
            'par_id' => $post['ins_partidas_dependientes'],
            'ins_tipo' => 8
        );
        return $data;
    }


    //GENERAR LA TABLA DE FINANCIAMIENTO PROGRAMADO  DEL INSUMO
    function get_tabla_ins_progmensual_directo($insg_id)
    {
        $list_prog_mensual = $this->minsumos->get_list_insumo_financiamiento($insg_id);

        // $prog_mensual = $this->minsumos->lista_progmensual_ins($ins_id);
        $tabla = ' <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th style="background-color: #0AA699;color: #FFFFFF">MONTO</th>
                                    <th style="background-color: #0AA699;color: #FFFFFF">FF</th>
                                    <th style="background-color: #0AA699;color: #FFFFFF">OF</th>
                                    <th style="background-color: #0AA699;color: #FFFFFF">ET</th>
                                    <th  style="background-color: #0AA699;color: #FFFFFF">Enero</th>
                                    <th  style="background-color: #0AA699;color: #FFFFFF">Febrero</th>
                                    <th  style="background-color: #0AA699;color: #FFFFFF">Marzo</th>
                                    <th  style="background-color: #0AA699;color: #FFFFFF">Abril</th>
                                    <th  style="background-color: #0AA699;color: #FFFFFF">Mayo</th>
                                    <th  style="background-color: #0AA699;color: #FFFFFF">Junio</th>
                                    <th  style="background-color: #0AA699;color: #FFFFFF">Julio</th>
                                    <th  style="background-color: #0AA699;color: #FFFFFF">Agosto</th>
                                    <th  style="background-color: #0AA699;color: #FFFFFF">Septiembre</th>
                                    <th  style="background-color: #0AA699;color: #FFFFFF">Octubre</th>
                                    <th  style="background-color: #0AA699;color: #FFFFFF">Noviembre</th>
                                    <th  style="background-color: #0AA699;color: #FFFFFF">Diciembre</th>
                                </tr>
                            </thead>
                        <tbody>';
        //FINANCIAMIENTO PROGRAMADO
        if(count($list_prog_mensual)!=0)
        {
            foreach ($list_prog_mensual as $row) {
                $tabla .= '<tr>';
                $tabla .= '<td>' . $row['ifin_monto'] . '</td>';
                $tabla .= '<td>' . $row['ff_codigo'] . '</td>';
                $tabla .= '<td>' . $row['of_codigo'] . '</td>';
                $tabla .= '<td>' . $row['et_codigo'] . '</td>';
                $tabla .= '<td>' . number_format($row['mes1'], 2, ',', '.') . '</td>';
                $tabla .= '<td>' . number_format($row['mes2'], 2, ',', '.') . '</td>';
                $tabla .= '<td>' . number_format($row['mes3'], 2, ',', '.') . '</td>';
                $tabla .= '<td>' . number_format($row['mes4'], 2, ',', '.') . '</td>';
                $tabla .= '<td>' . number_format($row['mes5'], 2, ',', '.') . '</td>';
                $tabla .= '<td>' . number_format($row['mes6'], 2, ',', '.') . '</td>';
                $tabla .= '<td>' . number_format($row['mes7'], 2, ',', '.') . '</td>';
                $tabla .= '<td>' . number_format($row['mes8'], 2, ',', '.') . '</td>';
                $tabla .= '<td>' . number_format($row['mes9'], 2, ',', '.') . '</td>';
                $tabla .= '<td>' . number_format($row['mes10'], 2, ',', '.') . '</td>';
                $tabla .= '<td>' . number_format($row['mes11'], 2, ',', '.') . '</td>';
                $tabla .= '<td>' . number_format($row['mes12'], 2, ',', '.') . '</td>';
                $tabla .= '</tr>';
            }
        }

        $tabla .= '</tbody>
                 </table>
            </div>';
        return $tabla;
    }


    function del_insumo($ins_id,$ins_tipo)
    {
        $ins_gestion = $this->minsumos->list_insumos_gestion($ins_id);
        foreach ($ins_gestion as $row)
        {
            // echo $row['insg_id'].'--'.$row['ins_id'].'-'.$row['g_id'].'--'.$row['insg_monto_prog']."<br>";

            $ins_fin = $this->minsumos->list_insumo_financiamiento($row['insg_id']);
            if(count($ins_fin)!=0)
            {
                /*----------------- ELIMINA IFIN PROG MES---------------*/
                $this->db->where('ifin_id', $ins_fin[0]['ifin_id']);
                $this->db->delete('ifin_prog_mes');
                /*------------------------------------------------------*/

                /*----------------- ELIMINA IFIN EJEC MES---------------*/
                $this->db->where('ifin_id', $ins_fin[0]['ifin_id']);
                $this->db->delete('ifin_ejec_mes');
                /*------------------------------------------------------*/

                /*----------------- ELIMINA IFIN PROG MES---------------*/
                $this->db->where('ifin_id', $ins_fin[0]['ifin_id']);
                $this->db->delete('insumo_financiamiento');
                /*------------------------------------------------------*/

                //  echo "-----insumo fin : ".$ins_fin[0]['ifin_id'].'-'.$ins_fin[0]['insg_id'].'-'.$ins_fin[0]['ifin_monto'];
            }

            /*----------------- ELIMINA INS GESTION---------------*/
            $this->db->where('insg_id', $row['insg_id']);
            $this->db->delete('insumo_gestion');
            /*------------------------------------------------------*/
            //
        }
        /*----------------- ELIMINA INS GESTION---------------*/
        $this->db->where('ins_id', $ins_id);
        $this->db->delete('_insumoactividad');
        /*------------------------------------------------------*/

        /*----------------- ELIMINA INS GESTION---------------*/
        $this->db->where('ins_id', $ins_id);
        $this->db->delete('insumos');
        /*------------------------------------------------------*/

        $conf=$this->model_proyecto->configuracion();
        if($ins_tipo==1){
            $nro_ins=$conf[0]['conf_rrhhp']-1;
            $update_conf = array('conf_rrhhp' => $nro_ins);
        }
        elseif($ins_tipo==2){
            $nro_ins=$conf[0]['conf_servicios']-1;
            $update_conf = array('conf_servicios' => $nro_ins);
        }
        elseif($ins_tipo==3){
            $nro_ins=$conf[0]['conf_pasajes']-1;
            $update_conf = array('conf_pasajes' => $nro_ins);
        }
        elseif($ins_tipo==4){
            $nro_ins=$conf[0]['conf_viaticos']-1;
            $update_conf = array('conf_viaticos' => $nro_ins);
        }
        elseif($ins_tipo==5){
            $nro_ins=$conf[0]['conf_cons_producto']-1;
            $update_conf = array('conf_cons_producto' => $nro_ins);
        }
        elseif($ins_tipo==6){
            $nro_ins=$conf[0]['conf_cons_linea']-1;
            $update_conf = array('conf_cons_linea' => $nro_ins);
        }
        elseif($ins_tipo==7){
            $nro_ins=$conf[0]['conf_materiales']-1;
            $update_conf = array('conf_materiales' => $nro_ins);
        }
        elseif($ins_tipo==8){
            $nro_ins=$conf[0]['conf_activos']-1;
            $update_conf = array('conf_activos' => $nro_ins);
        }
        elseif($ins_tipo==9){
            $nro_ins=$conf[0]['conf_otros_insumos']-1;
            $update_conf = array('conf_otros_insumos' => $nro_ins);
        }

        $this->db->where('ide', $this->gestion);
        $this->db->update('configuracion', $update_conf);

        if(count($this->minsumos->get_dato_insumo($ins_id))==0){
            return true;
            /*$this->session->set_flashdata('success','SE ELIMINO CORRECTAMENTE EL REQUERIMIENTO');
            redirect(site_url("") . '/prog/ins_act/'.$proy_id.'/'.$prod_id.'/'.$act_id.'/true'); /// directamente a la lista de requerimientos*/
        }
        else{
            return false;
            /*$this->session->set_flashdata('danger','ERROR AL ELIMINAR EL REQUERIMIENTO');
            redirect(site_url("") . '/prog/ins_act/'.$proy_id.'/'.$prod_id.'/'.$act_id.'/false');*/
        }
    }

    function delete_insumo()
    {
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $post = $this->input->post();
            $ins_id = $post['ins_id'];
            $ins_tipo = $post['ins_tipo'];

            $peticion=$this->del_insumo($ins_id,$ins_tipo);
            if($peticion){
                $result = array(
                    'respuesta' => 'correcto'
                );
            }
            else{
                $result = array(
                    'respuesta' => 'error'
                );
            }

            echo json_encode($result);

        } else {
            echo 'DATOS ERRONEOS';
        }
    }
    //FECHA_POSTGRES
    function get_fecha_postgres($fecha)
    {
        $date = new DateTime($fecha);
        return $date->format('Y-m-d');
        //$vec_fecha = explode("/", $fecha);
        //return $vec_fecha[2] . '-' . $vec_fecha[1] . '-' . $vec_fecha[0];
    }

    //FUNCION PARA CONSTRUIR MI VISTA
    function construir_vista($ruta, $data)
    {
        //----------------------------------- MENU-------------------------------
        $menu['enlaces'] = $this->menu->get_enlaces();
        $menu['subenlaces'] = $this->menu->get_sub_enlaces();
        $menu['titulo'] = 'PROGRAMACI&Oacute;N';
        //-----------------------------------------------------------------------
        //armar vista
        $this->load->view('includes/header');
        $this->load->view('includes/menu_lateral', $menu);
        $this->load->view($ruta, $data);//contenido
        //$this->load->view('admin/mantenimiento/vprueba');//contenido
        $this->load->view('includes/footer');
    }

    /*=========================================================================================================================*/
    public function get_mes($mes_id)
    {
        $mes[1]='Enero';
        $mes[2]='Febrero';
        $mes[3]='Marzo';
        $mes[4]='Abril';
        $mes[5]='Mayo';
        $mes[6]='Junio';
        $mes[7]='Julio';
        $mes[8]='Agosto';
        $mes[9]='Septiembre';
        $mes[10]='Octubre';
        $mes[11]='Noviembre';
        $mes[12]='Diciembre';

        $dias[1]='31';
        $dias[2]='28';
        $dias[3]='31';
        $dias[4]='30';
        $dias[5]='31';
        $dias[6]='30';
        $dias[7]='31';
        $dias[8]='31';
        $dias[9]='30';
        $dias[10]='31';
        $dias[11]='30';
        $dias[12]='31';

        $valor[1]=$mes[$mes_id];
        $valor[2]=$dias[$mes_id];

        return $valor;
    }

    function rolfun($rol)
    {
        $valor=false;
        for ($i=1; $i <=count($rol) ; $i++) {
            $data = $this->Users_model->get_datos_usuario_roles($this->session->userdata('fun_id'),$rol[$i]);
            if(count($data)!=0){
                $valor=true;
                break;
            }
        }
        return $valor;
    }
    public function get_ins_titulo($num)
    {
        switch ($num) {
            case 1:
                return (' - RECURSO HUMANO PERMANENTE');
                break;
            case 2:
                return (' - DETERMINACI&Oacute;N DE SERVICIOS');
                break;
            case 3:
                return (' - PASAJES');
                break;
            case 4:
                return (' - VIÁTICOS');
                break;
            case 5:
                return (' - CONSULTORÍA POR PRODUCTO');
                break;
            case 6:
                return (' - CONSULTORÍA EN LÍNEA');
                break;
            case 7:
                return (' - MATERIALES Y SUMINISTROS');
                break;
            case 8:
                return (' - ACTIVOS FIJOS');
                break;
            case 9:
                return (' - OTROS INSUMOS');
                break;
        }
    }
    /*------------  Funcion para verificar fechas ---------------------     */
    public function verif_fecha($fecha_act) /////// GENERA TABLA DE FUENTES , ORGANISMOS
    {
        $fecha = $fecha_act;
        $valores = explode('/', $fecha);

        if(count($valores)==3)
        {
            if(checkdate($valores[1],$valores[0],$valores[2])){
                return 'true';
            }
            else
            {
                return 'false';
            }
        }
        else
        {
            return 'false';
        }
        //  echo count($valores);
    }
}