<?php
define("DOMPDF_ENABLE_REMOTE", true);
define("DOMPDF_TEMP_DIR", "/tmp");
class Ejecucion extends CI_Controller {  
    public $rol = array('1' => '2','2' => '6','3' => '4','4' => '5','5' => '3');
	private $estilo_vertical = '<style>
        body{
            font-family: sans-serif;
            }
        @page {
        margin: 40px 40px;
        }
        header {
            position: fixed;
            left: 0px;
            top: -20px;
            right: 0px;
            height: 20px;
            background-color: #fff;
            }
        header h1{
            margin: 5px 0;
        }
        header h2{
            margin: 0 0 10px 0;
        }
        th{
                padding: 1px;
                text-align: center;
                font-size: 6px;
                color: #ffffff;
            }
        footer {
            position: fixed;
            left: 0px;
            bottom: -82px;
            right: 0px;
            height: 100px;
            border-bottom: 1px solid #ddd;
        }
        footer .page:after {
            content: counter(page) ;
        }
        footer table {
            width: 100%;
        }
        footer p {
            text-align: right;
        }
        footer .izq {
            text-align: left;
        }
        table{
            font-size: 6px;
            border: 1px;
            width: 100%;
            background-color:#fff;
        }
        th, td {
                padding: 1px;
                text-align: center;
                font-size: 6px;
            }
        .mv{font-size:10px;}
        .siipp{width:190px;}
        .header_table {color: #ffffff;text-align: center;align-items: center;align-self: center;
            font-weight: bold;background-color: #696969;align-content: center;
        }
        .head_tabla_dos{color: #000000;text-align: left;align-self: center;
            font-weight: bold;background-color: #B2B2B2;
        }
        .titulo_pdf {
            text-align: left;
            font-size: 10px;
        }

        table.fixed { table-layout:fixed; }
        table.fixed td { overflow: hidden; }
        .sub_table{
            border-bottom:1px solid black;
        }
        .contenedor_principal{
            border-style: solid;
            border-width:1px;
        }
        .table_contenedor{
            border-style: solid;
            margin: 0px;
            padding: 1px;
            border-width: 1px;
            border-collapse: collapse;
        }
        .collapse_t td{
            border-style: solid;
            margin: 0px;
            padding: 3px;
            border-width: 1px;
            border-collapse: collapse;
        }
        .fila_unitaria{
            text-align:left;
        }
        .titulo_dictamen{
            padding:4px;
            background-color: #454545;
            border-style: solid;margin: 2px;
            border-width: 1px;
            color: white;
            font-size: 8px;
            border-color: black;
        }
    </style>';

	private $estilo_horizontal = '<style>
        body{
            font-family: sans-serif;
            }
        @page {
        margin: 40px 40px;
        }
        header {
            position: fixed;
            left: 0px;
            top: -20px;
            right: 0px;
            height: 20px;
            background-color: #fff;
            }
        header h1{
            margin: 5px 0;
        }
        header h2{
            margin: 0 0 10px 0;
        }
        th{
                padding: 1px;
                text-align: center;
                font-size: 6px;
                color: #ffffff;
            }
        footer {
            position: fixed;
            left: 0px;
            bottom: -82px;
            right: 0px;
            height: 100px;
            border-bottom: 1px solid #ddd;
        }
        footer .page:after {
            content: counter(page) ;
        }
        footer table {
            width: 100%;
        }
        footer p {
            text-align: right;
        }
        footer .izq {
            text-align: left;
        }
        table{
            font-size: 6px;
            border: 1px;
            width: 100%;
            background-color:#fff;
        }
        th, td {
                padding: 1px;
                text-align: center;
                font-size: 6px;
            }
        .mv{font-size:10px;}
        .siipp{width:190px;}
        .header_table {
            color: #000000;
            text-align: left;
            align-items: center;
            align-self: center;
            font-weight: bold;
            background-color: #ffffff;
            align-content: center;
        }

        .titulo_pdf {
            text-align: left;
            font-size: 10px;
        }

        table.fixed { table-layout:fixed; }
        table.fixed td { overflow: hidden; }
        .sub_table{
            border-bottom:1px solid black;
        }
        .contenedor_principal{
            border-style: solid;
            border-width:1px;
        }
        .table_contenedor{
            border-style: solid;
            margin: 0px;
            padding: 1px;
            border-width: 1px;
            border-collapse: collapse;
        }
        .collapse_t td{
            border-style: solid;
            margin: 0px;
            padding: 3px;
            border-width: 1px;
            border-collapse: collapse;
        }
        .fila_unitaria{
            text-align:left;
        }
        .titulo_dictamen{
            padding:4px;
            background-color: #454545;
            border-style: solid;margin: 2px;
            border-width: 1px;
            color: white;
            font-size: 8px;
            border-color: black;
        }
        .jarviswidget > div {
            font-size:1.3em;
		}
		.titulo, .titulo_s{
            margin:15px 0 15px 0;
			padding:0 0 5px 0;
			border-bottom:2px solid #ccc;
			text-align:center;
		}
		.titulo_s{
            border-bottom:0;
		}

		.border1{
            border:2px solid #bbb;
			background-color:rgb(245,245,255);
			padding-top:5px;
			padding-bottom:5px;
		}
		.bg-green{
            background-color:#b9e5a9;
		}
		.bg-warning{
            background-color:#ffe88c;
		}
		.bg-info{
            background-color:#9bc1ff;
		}
		.bg-gray{
            background-color:#ccc;
		}
		.bg-gray2{
            background-color:#ddd;
		}
		.bg-primary{
            background-color:#4786ad;
		}
		        .table{
		  border-color:#777 !important;
		  border: 1px solid #777;
		  border-spacing: 0;
		  border-collapse: collapse;
          padding: 0.3em;
        }
table.tabla_avance.table-condensed.table > tbody > tr > td, .table-condensed.table > tbody > tr > th, .table-condensed.table > tfoot > tr > td, .table-condensed.table > tfoot > tr > th, .table-condensed.table > thead > tr > td, .table-condensed.table > thead > tr > th {
    padding: 0.3em;
    
}		
        th{
		  font-weight:bold;
		  font-size:10px;
		  border-color:#777 !important;
        }
		td{
		  font-size:9px;
		  border-color:#777 !important;
		}
		tr.head_componente th{
			background-color:#ccc;
			color:#333;
		  border-color:#777 !important;
		}
		tr.head_producto td{
			background-color:#eee;
			color:#333;
			font-weight:bold;
		  border-color:#777 !important;
		}
		tr.row_actividad > td{
			background-color:#fff;
			color:#333;
		  border-color:#777 !important;
		}
		tr.head_tabla > th{
			background-color:#aaa;
			color:#fff;
		  border-color:#777 !important;
		}
		.bg-green{
			background-color:#b9e5a9;
		}
		.bg-warning{
			background-color:#ffe88c;
		}
		.bg-info{
			background-color:#9bc1ff;
		}
		.bg-gray{
			background-color:#ccc;
		}
		.bg-gray2{
			background-color:#ddd;
		}
		.bg-primary{
			background-color:#4786ad;
		}
		.text-right{
		    text-align:right;
		}
    </style>';
    public function __construct ()
    {
        parent::__construct();
        if($this->session->userdata('fun_id')!=null){
            $this->load->model('Users_model','',true);
            if($this->rolfun($this->rol)){ 
            $this->load->library('pdf2');
            $this->load->model('programacion/model_proyecto');
            $this->load->model('programacion/model_faseetapa');
            $this->load->model('programacion/model_actividad');
            $this->load->model('programacion/model_producto');
            $this->load->model('programacion/model_componente');
            $this->load->model('programacion/model_mantenimiento');
            $this->load->model('ejecucion/model_ejecucion');
            $this->load->model('programacion/marco_estrategico/mobjetivos');
            $this->load->model('mantenimiento/mapertura_programatica');
            $this->load->model('mantenimiento/munidad_organizacional');
            $this->load->model('reportes/model_objetivo');
            $this->load->model('menu_modelo');
            $this->load->model('programacion/insumos/mfinanciera');
			
        $this->load->model('reportes/taqpacha/model_taqpacha','model_taqpacha');
            
            $this->mes = $this->session->userData('mes');
            $this->gestion = $this->session->userData('gestion');

            }else{
                redirect('admin/dashboard');
            }
        }
        else{
            redirect('/','refresh');
        }
    }

    public function combo_estados_proy() /////// COMBO DE LOS ESTADOS DEL PROYECTOS
    {
        //echo "urbanizaciones";
        $salida = "";
        $id_pais = $_POST["elegido"];
        // construimos el combo de ciudades deacuerdo al pais seleccionado
        $combog = pg_query("SELECT * FROM dp_estados WHERE st_clase=$id_pais");
        $salida .= "<option value=''>" . mb_convert_encoding('SELECCIONE ESTADO DE LA ACCION', 'cp1252', 'UTF-8') . "</option>";
        while ($sql_p = pg_fetch_row($combog)) {
            $salida .= "<option value='" . $sql_p[0] . "'>" . $sql_p[1] . "</option>";
        }
        echo $salida;
    }

    //======Funcion encargada de generar ventana flotante en regsitro de ejecución=====//
    public function fn_genera_prog_partidas()
    {
        $tabla='';
        $id_proy=$_POST['id'];
        $nro=0;
        $fase = $this->model_faseetapa->get_id_fase($id_proy);
        $fase_gest = $this->model_faseetapa->fase_etapa_gestion($fase[0]['id'],$this->session->userdata("gestion"));
        if($this->delete_partidas($id_proy,$this->gestion)){
            $this->actualiza_partidas($id_proy,$this->gestion,$fase[0]['pfec_ejecucion']); //// Actualiza Tablas Partidas
            $proyecto_programado= $this->mfinanciera->lista_partidas_proy_programado($id_proy,$this->gestion);
            if(count($proyecto_programado)!=0){
                $nropr=0;
                $tabla .='<div class="table-responsive"><table class="table table-hover table-bordered">';
                $tabla .='<tr>';
                $tabla .='<th>Nro.</th>';
                $tabla .='<th>Ejec.</th>';
                $tabla .='<th>Partida</th>';
                $tabla .='<th>Fuente</th>';
                $tabla .='<th>Org. Financiador</th>';
                $tabla .='<th></th>';
                $tabla .='</tr>';
                foreach ($proyecto_programado as $pp) {
                    $nropr++;
                    $nro++;
                    $tabla .='<tr>';
                    $tabla .='<td>'.$nro.'</td>';
                    $tabla .= '<td>
                                 <a class="btn btn-lg btn-primary" href="'.site_url("").'/ejec/ejec_fin/'.$id_proy.'/'.$pp['pr_id'].'" title="EJECUTAR PROGRAMACION PRESUPUESTARIA">
					            <i class="glyphicon glyphicon-usd"></i>
                                </a>
                                </td>';
                    $tabla .='<td>'.$pp['par_codigo'].'</td>';
                    $tabla .='<td>'.$pp['ff_id'].'</td>';
                    $tabla .='<td>'.$pp['of_id'].'</td>';
                    $tabla .='<td>';
                    $tabla .='<table class="table table-bordered">';
                    $tabla .='<tr>';
                    $tabla .='<thead>';
                    $tabla .='<th>P/E</th>';
                    $tabla .='<th>ENE.</th>';
                    $tabla .='<th>FEB.</th>';
                    $tabla .='<th>MAR.</th>';
                    $tabla .='<th>ABR.</th>';
                    $tabla .='<th>MAY.</th>';
                    $tabla .='<th>JUN.</th>';
                    $tabla .='<th>JUL.</th>';
                    $tabla .='<th>AGOST.</th>';
                    $tabla .='<th>SEPT.</th>';
                    $tabla .='<th>OCT.</th>';
                    $tabla .='<th>NOV.</th>';
                    $tabla .='<th>DIC.</th>';
                    $tabla .='</thead>';
                    $tabla .='</tr>';
                    $tabla .='<tr>';
                    $tabla .='<td>PROGRAMADO</td>';
                    $tabla .='<td>'.number_format($pp['enero'], 2, ',', '.').'</td>';
                    $tabla .='<td>'.number_format($pp['febrero'], 2, ',', '.').'</td>';
                    $tabla .='<td>'.number_format($pp['marzo'], 2, ',', '.').'</td>';
                    $tabla .='<td>'.number_format($pp['abril'], 2, ',', '.').'</td>';
                    $tabla .='<td>'.number_format($pp['mayo'], 2, ',', '.').'</td>';
                    $tabla .='<td>'.number_format($pp['junio'], 2, ',', '.').'</td>';
                    $tabla .='<td>'.number_format($pp['julio'], 2, ',', '.').'</td>';
                    $tabla .='<td>'.number_format($pp['agosto'], 2, ',', '.').'</td>';
                    $tabla .='<td>'.number_format($pp['septiembre'], 2, ',', '.').'</td>';
                    $tabla .='<td>'.number_format($pp['octubre'], 2, ',', '.').'</td>';
                    $tabla .='<td>'.number_format($pp['noviembre'], 2, ',', '.').'</td>';
                    $tabla .='<td>'.number_format($pp['diciembre'], 2, ',', '.').'</td>';
                    $tabla .='</tr>';
                    $pe= $this->mfinanciera->get_partida_proy_ejecutado($pp['pr_id'],$this->gestion);
                    if(count($pe)!=0){
                        $tabla .='<tr>';
                        $tabla .='<td>EJECUTADO</td>';
                        $tabla .='<td>'.number_format($pe[0]['enero'], 2, ',', '.').'</td>';
                        $tabla .='<td>'.number_format($pe[0]['febrero'], 2, ',', '.').'</td>';
                        $tabla .='<td>'.number_format($pe[0]['marzo'], 2, ',', '.').'</td>';
                        $tabla .='<td>'.number_format($pe[0]['abril'], 2, ',', '.').'</td>';
                        $tabla .='<td>'.number_format($pe[0]['mayo'], 2, ',', '.').'</td>';
                        $tabla .='<td>'.number_format($pe[0]['junio'], 2, ',', '.').'</td>';
                        $tabla .='<td>'.number_format($pe[0]['julio'], 2, ',', '.').'</td>';
                        $tabla .='<td>'.number_format($pe[0]['agosto'], 2, ',', '.').'</td>';
                        $tabla .='<td>'.number_format($pe[0]['septiembre'], 2, ',', '.').'</td>';
                        $tabla .='<td>'.number_format($pe[0]['octubre'], 2, ',', '.').'</td>';
                        $tabla .='<td>'.number_format($pe[0]['noviembre'], 2, ',', '.').'</td>';
                        $tabla .='<td>'.number_format($pe[0]['diciembre'], 2, ',', '.').'</td>';
                        $tabla .='</tr>';
                    }
                    $tabla .='</table>';
                    $tabla .='</td>';
                    $tabla .='</tr>';
                }
                $tabla .='</table></div>';
            }
        }
        echo $tabla;
    }

    //===== Verifica si el regsitro de ejecución del mey ya existe =====//
    function verifica_registro(){
        if($this->input->is_ajax_request())
        {
            $post = $this->input->post();
            $pfec_id = $post['pfec_id'];
            $com_id  = $post['com_id'];
            $mes_id  = $post['mes_id'];
            $variable= $this->model_ejecucion->verificar_registro($pfec_id,$com_id,$mes_id,$this->session->userdata("gestion"));
            if($variable == true)
            {
                echo "true";; ///// no existe un CI registrado
            }
            else
            {
                echo "false";; //// existe el CI ya registrado
            }
        }else{
            show_404();
        }
    }
    /*========================================== PROYECTOS DE INVERSION ==========================================*/
    public function mis_operaciones(){
      $this->load->model('menu_modelo');
      $enlaces=$this->menu_modelo->get_Modulos(3);
      $data['menu']=$this->menu(3); //// genera menu

        $data['proyectos']= $this->mis_operaciones_aprobadas(1,1);
        $data['precurrentes']=$this->mis_operaciones_aprobadas(1,2);
        $data['pnrecurrentes']=$this->mis_operaciones_aprobadas(1,3);
        $data['operaciones']=$this->mis_operaciones_aprobadas(1,4);
       $this->load->view('admin/ejecucion/lista_operaciones/mis_operaciones', $data);

    }
    /*============================================================================================================*/

    /*---------------------------- LISTA DE OPERACIONES  ------------------------*/
    public function mis_operaciones_aprobadas($mod,$tp_id){
      $lista_aper_padres = $this->model_proyecto->list_prog();//lista de aperturas padres 
      $tabla ='';
      $nro=1;
        $proyectos=$this->model_proyecto->lista_proyectos($mod,$tp_id);
        if(count($proyectos)!=0){
          $nro=0;
          foreach($proyectos  as $row){
            if($row['tp_id']!=1){$color='#D7FCF9';}else{$color='#ffffff';}
            $nro++;
            $fase = $this->model_faseetapa->get_id_fase($row['proy_id']);
            //$fase_gest = $this->model_faseetapa->fase_etapa_gestion($fase[0]['id'],$this->session->userdata("gestion"));
            $tabla .= '<tr bgcolor='.$color.'>';
            if(count($fase)!=0){
              $tabla .='<td>'.$nro.'</td>';
              $tabla .= '<td style="padding:5px;">';
                $tabla .= '<div class="btn-group-vertical">';
              $tabla.='<a class="btn btn-info" data-toggle="modal" data-target="#'.$row['proy_id'].'" title="EJECUCIÓN FÍSICA" ><i class="glyphicon glyphicon-align-left"></i> EJECUCIÓN FÍSICA</a>
                            <div class="modal fade bs-example-modal-lg" tabindex="-1" id="'.$row['proy_id'].'"  role="dialog" aria-labelledby="myLargeModalLabel">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close text-danger lead" data-dismiss="modal" aria-hidden="true">
                                                &times;
                                            </button>
                                            <h4 class="modal-title">
                                                <img src="'.base_url().'assets/img/logo.png" style="width:300px;" alt="'.$this->session->userdata('name').'">
                                            </h4>
                                        </div>
                                        <div class="modal-body no-padding">
                                        <br>
                                        <h2 class="text-center modal-title" id="myModalLabel">'.$row['proy_nombre'].'</h2>
                                        <br>
                                            <div class="well">';
                                            $nro_comp=1;
                                            $comp = $this->model_componente->componentes_id($fase[0]['id']);
                                            if($row['tp_id']==4){$titulo_comp='PROCESOS DE LA OPERACI&Oacute;N';}else{$titulo_comp='COMPONENTES DE LA OPERACI&Oacute;N';}
                                            if(count($comp)!=0){
                                                $tabla .='<table class="table table-hover table-bordered">
                                                            <thead>
                                                            <tr>
                                                                <th style="width:2%;">#</th>
                                                                <th style="width:40%;">'.$titulo_comp.'</th>
                                                                <th style="width:20%;">PONDERACI&Oacute;N</th>
                                                                <th style="width:20%;"></th>
                                                            </tr>
                                                            </thead>';
                                                            foreach ($comp as $rowc){
                                                                $tabla .='
                                                                    <tr>
                                                                    <td>'.$nro_comp.'</td>
                                                                    <td>'.$rowc['com_componente'].'</td>
                                                                    <td>'.$rowc['com_ponderacion'].'%</td>
                                                                    <td><a href="'.site_url("admin").'/ejec/meses_operacion/2/'.$row['proy_id'].'/'.$fase[0]['id'].'/'.$rowc['com_id'].'" title="EJECUTAR POR COMPONENTE" class="btn btn-primary" style="width:90%;">EJECUTAR OPERACI&Oacute;N</a>
                                                                    <a href="'.site_url("admin").'/ejec/list_contratos/2/'.$row['proy_id'].'/'.$fase[0]['id'].'/'.$rowc['com_id'].'/'.$row['aper_programa'].'" title="REGISTRO DE CONTRATO" class="btn btn-primary" style="width:90%;">REGISTRO DE CONTRATO</a>
                                                                    </td>
                                                                    </tr>';
                                                                $nro_comp++;
                                                            }
                                                $tabla .='</table>';                
                                            }
                                            $tabla.='   
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                        <button type="button" data-dismiss="modal" class="btn btn-default">
                                          <i class="glyphicon glyphicon-remove"></i> 
                                          CERRAR VENTANA
                                        </button>
                                        
                                        </div>
                                    </div>
                                </div>
                            </div>';
              $tabla .= '
                        <style>
                          #mdialTamanio{
                            width: 80% !important;
                          }
                        </style>';
                $tabla.='<a class="btn btn-primary" data-toggle="modal" data-target="#mod'.$row['proy_id'].'" title="EJECUCION PRESUPUESTARIA" onClick="valida_vatos('.$row['proy_id'].');" ><i class="glyphicon glyphicon-usd"></i> EJECUCIÓN PTTO.</a>
                            <div class="modal fade bs-example-modal-lg" tabindex="-1" id="mod'.$row['proy_id'].'"  role="dialog" aria-labelledby="myLargeModalLabel">
                                <div class="modal-dialog modal-lg" role="document" id="mdialTamanio">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title">
                                                <img src="'.base_url().'assets/img/logo.png" style="width:300px;" alt="'.$this->session->userdata('name') .'">
                                            </h4>
                                      </div>
                                      <div class="modal-body">
                                        <br>
                                        <h2 class="text-center modal-title" id="myModalLabel">'.$row['proy_nombre'].'</h2>
                                        <br>
                                     <div class="text-center alert alert-info"><strong>EJECUCI&Oacute;N PRESUPUESTARIA '.$this->gestion.'</strong></div>
                                     <div id=_'.$row['proy_id'].' name="'.$row['proy_id'].'"></div>';

                                      $tabla .='
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">
                                        <i class="glyphicon glyphicon-remove"></i> 
                                        CERRAR VENTANA</button>
                                      </div>
                                    </div>
                                </div>
                            </div>';
                $tabla .='<a class="btn btn-default" href="" class="form_pond" data-toggle="modal" data-target="#'.$row['proy_id'].'a">
                            <i class="glyphicon glyphicon-stats"></i> REPORTES
                          </a>
                            <div class="modal fade bs-example-modal-lg" tabindex="-1" id="'.$row['proy_id'].'a"  role="dialog" aria-labelledby="myLargeModalLabel">
                              <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close text-danger" data-dismiss="modal" aria-hidden="true">
                                      &times;
                                    </button>
                                            <h4 class="modal-title">
                                                <img src="'.base_url().'assets/img/logo.png" style="width:300px;" alt="'.$this->session->userdata('name').'">
                                            </h4>
                                        </div>
                                        <br>
                                        <h2 class="text-center modal-title" id="myModalLabel">'.$row['proy_nombre'].'</h2>
                                        <br>
                                        <div class="modal-body no-padding">
                                          <div class="well">
                                            <table class="table table-hover table-bordered">
                                              <tr>
                                                <td><b>IDENTIFICACI&Oacute;N DE LA ETAPA</b></td>
                                                <td><center><a href="javascript:abreVentana(\''.site_url("admin").'/prog/identificacion_proy/'.$row['proy_id'].'\');" title="IDENTIFICACION DE LA ETAPA"><i style="font-size:30px;" class="glyphicon glyphicon-print"></i><br>Identificación</a></center></td>
                                                <td></td>
                                              </tr>
                                              <tr>
                                                <td><b>PROGRAMACI&Oacute;N Y EJECUCI&Oacute;N F&Iacute;SICA DE LA ETAPA</b></td>
                                                <td><center><a href="javascript:abreVentana(\''.site_url("admin").'/prog/ejecucion_pfisico/1/'.$fase[0]['id'].'/'.$row['proy_id'].'\');" title="PROGRAMACION Y EJECUCION FISICA ANUAL DE LA ETAPA"><i style="font-size:30px;" class="glyphicon glyphicon-print"></i><br>Anual</a></center></td>
                                                <td><center><a href="javascript:abreVentana(\''.site_url("admin").'/prog/ejecucion_pfisico_m/4/'.$fase[0]['id'].'/'.$row['proy_id'].'\');" title="PROGRAMACION Y EJECUCION FISICA PLURI-ANUAL DE LA ETAPA"><i style="font-size:30px;" class="glyphicon glyphicon-print"></i><br>Pluri Anual</a></center></td>
                                              </tr>
                                              <tr>
                                                <td><b>PROGRAMACI&Oacute;N Y EJECUCI&Oacute;N FINANCIERA DE LA ETAPA</b></td>
                                                <td><center><a href="javascript:abreVentana(\''.site_url("admin").'/prog/rep_financiero/1/'.$fase[0]['id'].'/'.$row['proy_id'].'\');" title="PROGRAMACION Y EJECUCION FINANCIERA ANUAL DE LA ETAPA"><i style="font-size:30px;" class="glyphicon glyphicon-print"></i><br>Anual</a></center></td>
                                                <td><center><a href="javascript:abreVentana(\''.site_url("admin").'/prog/rep_financiero/4/'.$fase[0]['id'].'/'.$row['proy_id'].'\');" title="PROGRAMACION Y EJECUCION FINANCIERA PLURI-ANUAL DE LA ETAPA"><i style="font-size:30px;" class="glyphicon glyphicon-print"></i><br>Pluri Anual</a></center></td>
                                              </tr>
                                              <tr>
                                                <td><b>REGISTRO DE CONTRATOS</b></td>
                                                <td><center><a href="javascript:abreVentana(\''.site_url("admin").'/prog/reporte_contrato/'.$row['proy_id'].'/'.$fase[0]['id'].'\');" title="REPORTE DE CONTRATOS"><i style="font-size:30px;" class="glyphicon glyphicon-print"></i><br>Contratos</a></center></td>
                                                <td></td>
                                              </tr>
                                              <tr>
                                                <td><b>INFORMES DE SUPERVISI&Oacute;N Y EVALUACI&Oacute;N</b></td>
                                                <td><center><a href="javascript:abreVentana(\''.site_url("admin").'/prog/reporte_supervision/'.$row['proy_id'].'/'.$fase[0]['id'].'\');" title="REPORTE DE SUPERVISIÓN"><i style="font-size:30px;" class="glyphicon glyphicon-print"></i><br>Supervisión</a></center></td>
                                                <td></td>
                                              </tr>
                                              <tr>
                                                <td><b>CURVA "S" AVANCE F&Iacute;SICO </b></td>
                                                <td><center><a href="javascript:abreVentanaFis(\''.site_url('admin').'/imprimir_curva_fis/1/'.$fase[0]['id'].'/'.$row['proy_id'].'\');" title="GRAFICO AVANCE FISICO ANUAL"><i style="font-size:30px;" class="glyphicon glyphicon-print"></i><br>Anual</a></center></td>
                                                <td><center><a href="javascript:abreVentanaFis(\''.site_url('admin').'/imprimir_curva_fis/4/'.$fase[0]['id'].'/'.$row['proy_id'].'\');" title="GRAFICO AVANCE FISICO PLURI-ANUAL"><i style="font-size:30px;" class="glyphicon glyphicon-print"></i><br>Pluri Anual</a></center></td>
                                              </tr>
                                              <tr>
                                                <td><b>CURVA "S" AVANCE FINANCIERO</b></td>
                                                <td><center><a href="javascript:abreVentanaFin(\''.site_url("").'/prog/rep_curva_fin/1/'.$fase[0]['id'].'/'.$row['proy_id'].'\');" title="GRAFICO AVANCE FINANCIERO ANUAL"><i style="font-size:30px;" class="glyphicon glyphicon-print"></i><br>Anual</a></center></td>
                                                <td><center><a href="javascript:abreVentanaFin(\''.site_url("").'/prog/rep_curva_fin/4/'.$fase[0]['id'].'/'.$row['proy_id'].'\');" title="GRAFICO AVANCE FINANCIERO PLURI-ANUAL"><i style="font-size:30px;" class="glyphicon glyphicon-print"></i><br>Pluri Anual</a></center></td>
                                              </tr>
                                            </table>
                                          </div>
                                        </div>
                                  </div>
                                </div>
                              </div>
                            </div>';
              $tabla .= '</div>';
              $tabla .='</td>';
              $tabla .= '<td>'.$row['proy_sisin'].'</td>';
              $tabla .= '<td>'.$row['aper_programa'].''.$row['aper_proyecto'].''.$row['aper_actividad'].'</td>';
              $tabla .= '<td>'.$row['proy_nombre'].'</td>';
              $tabla .= '<td>'.$row['fun_nombre'].' '.$row['fun_paterno'].' '.$row['fun_materno'].'</td>';
              $tabla .= '<td>'.$row['uni_unidad'].'</td>';
              $nc=$this->model_faseetapa->calcula_nc($fase[0]['pfec_fecha_inicio']); //// calcula nuevo/continuo
              $ap=$this->model_faseetapa->calcula_ap($fase[0]['pfec_fecha_inicio'],$fase[0]['pfec_fecha_fin']); //// calcula Anual/Plurianual
              $tabla .= '<td>'.$nc.'</td>';
              $tabla .= '<td>'.$ap.'</td>';
              $tabla .= '<td>';
              $nombre='';
              if(isset($fase_gest[0]['pfecg_ppto_total']))
                  $nombre=number_format($fase_gest[0]['pfecg_ppto_total'], 2, ',', ' ');
              else
                   $nombre='<a data-toggle="modal" data-target="#modal_mod_aper2" class="btn btn-warning mod_aper2" name="." id="2"><i class="glyphicon glyphicon-info-sign"></i> ERROR EN FECHA <br>DE FASE</a>';
              $tabla .= $nombre.'</td>';
            }
            else{
              $tabla .='<td>'.$nro.'</td>';
              $tabla .= '<td style="padding:5px;">';

              $tabla .='</td>';
              $tabla .= '<td>'.$row['proy_sisin'].'</td>';
              $tabla .= '<td>'.$row['aper_programa'].''.$row['aper_proyecto'].''.$row['aper_actividad'].'</td>';
              $tabla .= '<td>'.$row['proy_nombre'].'</td>';
              $tabla .= '<td>'.$row['fun_nombre'].' '.$row['fun_paterno'].' '.$row['fun_materno'].'</td>';
              $tabla .= '<td>'.$row['uni_unidad'].'</td>';
              $tabla .= '<td></td>';
              $tabla .= '<td></td>';
              $tabla .= '<td></td>';
            }
            $tabla .= '</tr>';
            $nro++;
          }
        }

      return $tabla;
    }

      /*------------------------------- LISTA DE PARTIDAS ------------------------------*/
  function mis_partidas($mod,$proy_id,$pfec_id,$gestion){
    $proyecto_programado= $this->mfinanciera->lista_partidas_proy_programado($proy_id,$gestion);
    $tabla ='';
    $tabla .='<tabla>';
    $nro=0;
    foreach ($proyecto_programado as $pp) {
      $nro++;
      $tabla .='<tr>';
      $tabla .='<td>'.$nro.'</td>';
      $tabla .= '<td>
                    <a href="' . site_url("admin") . '/prog/ejec_partida/' . $mod . '/' . $proy_id . '/' . $pfec_id . '/'. $pp['pr_id'] .'">
                        <center>
                            <img src="' . base_url() . 'assets/ifinal/insumo.png" width="30" height="30" class="img-responsive "title="EJECUTAR PROGRAMACION PRESUPUESTARIA">
                        </center>
                    </a>
                </td>';
      $tabla .='<td>'.$pp['par_codigo'].'</td>';
      $tabla .='<td>'.$pp['ff_id'].'</td>';
      $tabla .='<td>'.$pp['of_id'].'</td>';
      $tabla .='<td>';
        $tabla .='<table class="table table-bordered">';
        $tabla .='<tr>';
          $tabla .='<thead>';
          $tabla .='<th>P/E</th>';
          $tabla .='<th>ENE.</th>';
          $tabla .='<th>FEB.</th>';
          $tabla .='<th>MAR.</th>';
          $tabla .='<th>ABR.</th>';
          $tabla .='<th>MAY.</th>';
          $tabla .='<th>JUN.</th>';
          $tabla .='<th>JUL.</th>';
          $tabla .='<th>AGOST.</th>';
          $tabla .='<th>SEPT.</th>';
          $tabla .='<th>OCT.</th>';
          $tabla .='<th>NOV.</th>';
          $tabla .='<th>DIC.</th>';
          $tabla .='</thead>';
        $tabla .='</tr>';
        $tabla .='<tr>';
          $tabla .='<td>PROGRAMADO</td>';
          $tabla .='<td>'.number_format($pp['enero'], 2, ',', '.').'</td>';
          $tabla .='<td>'.number_format($pp['febrero'], 2, ',', '.').'</td>';
          $tabla .='<td>'.number_format($pp['marzo'], 2, ',', '.').'</td>';
          $tabla .='<td>'.number_format($pp['abril'], 2, ',', '.').'</td>';
          $tabla .='<td>'.number_format($pp['mayo'], 2, ',', '.').'</td>';
          $tabla .='<td>'.number_format($pp['junio'], 2, ',', '.').'</td>';
          $tabla .='<td>'.number_format($pp['julio'], 2, ',', '.').'</td>';
          $tabla .='<td>'.number_format($pp['agosto'], 2, ',', '.').'</td>';
          $tabla .='<td>'.number_format($pp['septiembre'], 2, ',', '.').'</td>';
          $tabla .='<td>'.number_format($pp['octubre'], 2, ',', '.').'</td>';
          $tabla .='<td>'.number_format($pp['noviembre'], 2, ',', '.').'</td>';
          $tabla .='<td>'.number_format($pp['diciembre'], 2, ',', '.').'</td>';
        $tabla .='</tr>';
        $pe= $this->mfinanciera->get_partida_proy_ejecutado($pp['pr_id'],$gestion);
        if(count($pe)!=0){
          $tabla .='<tr>';
            $tabla .='<td>EJECUTADO</td>';
            $tabla .='<td>'.number_format($pe[0]['enero'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pe[0]['febrero'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pe[0]['marzo'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pe[0]['abril'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pe[0]['mayo'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pe[0]['junio'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pe[0]['julio'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pe[0]['agosto'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pe[0]['septiembre'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pe[0]['octubre'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pe[0]['noviembre'], 2, ',', '.').'</td>';
            $tabla .='<td>'.number_format($pe[0]['diciembre'], 2, ',', '.').'</td>';
          $tabla .='</tr>';
        }
        $tabla .='</table>';
      $tabla .='</td>';
      $tabla .='</tr>';
      $tabla .='</tabla>';
    }

    return $tabla;
  }

    function delete_partidas($proy_id,$gestion){
    $proyecto_programado= $this->mfinanciera->lista_partidas_proy_programado($proy_id,$gestion);
    foreach ($proyecto_programado as $pp) {
      
    //  echo "PROGRAMADO : PAR ID : ".$pp['par_id']."<br>";
      $proyecto_ejecutado= $this->mfinanciera->get_partida_proy_ejecutado($pp['pr_id'],$gestion);
      foreach ($proyecto_ejecutado as $pe) {
          $this->db->where('pr_id', $pp['pr_id']);
          $this->db->delete('proyecto_ejecutado_partidas');/*
          echo "EJECUTADO : PAR ID : ".$pe['par_id']."<br>";*/
      }
        $this->db->where('pr_id', $pp['pr_id']);
        $this->db->delete('proyecto_programado_partidas');
    }

    $proyecto_programado= $this->mfinanciera->lista_partidas_proy_programado($proy_id,$gestion);
    if(count($proyecto_programado)==0){
      return true;
    }
    else{
      return false;
    }
  }

  function actualiza_partidas($proy_id,$gestion,$tp_ejec){
    $partidas_prog= $this->mfinanciera->vlista_partidas_proy_programado($proy_id,$gestion,$tp_ejec);
    foreach ($partidas_prog as $pp) {
      
      $pr_id=$this->mfinanciera->storage_partidas($proy_id,$gestion,$pp['partida_id'],$pp['partida_codigo'],$pp['ffid'],$pp['ofid'],$pp['etid'],$pp['total']
                                          ,$pp['enero'],$pp['febrero'],$pp['marzo'],$pp['abril'],$pp['mayo'],$pp['junio'],$pp['julio'],$pp['agosto'],$pp['septiembre'],$pp['octubre'],$pp['noviembre'],$pp['diciembre']);

      if($pr_id!=0 || $pr_id!='')
      {
          $partidas_ejec= $this->mfinanciera->vlista_partidas_proy_ejecutado($proy_id,$gestion);
          if(count($partidas_ejec)!=0){

            foreach ($partidas_ejec as $pe) {
              if(($pp['partida_id']==$pe['par_id'])&($pp['ffid']==$pe['ff_id'])&($pp['ofid']==$pe['of_id'])){
              //  echo "ENCONTRADO : ".$pp['partida_codigo']."-".$pp['partida_id']."-".$pp['ffid']."-".$pp['ofid']."<br>";
                
                $pe_id=$this->mfinanciera->storage_partidas_ejec($pr_id,$gestion,$pe['par_id'],$pe['ff_id'],$pe['of_id']
                                              ,$pe['ejec1'],$pe['ejec2'],$pe['ejec3'],$pe['ejec4'],$pe['ejec5'],$pe['ejec6'],$pe['ejec7'],$pe['ejec8'],$pe['ejec9'],$pe['ejec10'],$pe['ejec11'],$pe['ejec12']);
                
                if($pe_id!=0 || $pe_id!=''){
                  $this->session->set_flashdata('success','LA PROGRAMACI&Oacute;N Y EJECUCI&Oacute;N DE PARTIDAS SE ACTUALIZARON CORRECTAMENTE');
                  //echo "se registro correctamente programado y ejecutado";
                }
                else{
                  $this->session->set_flashdata('danger','NOSE PUEDO ACTULIZAR CORRECTAMENTE LA EJECUCI&Oacute;N POR PARTIDAS');
                  //echo "Error en el registro Ejecutado";
                }
              }
              
            }

          }
      }
      else{
        $this->session->set_flashdata('danger','NOSE PUEDO ACTUALIZAR CORRECTAMENTE LA PROGRAMACI&Oacute;N POR PARTIDAS');
        //echo "error al guardar programado";
      }
      
    }
  }
    /*================================================ MESES DE EJECUCION DEL PROYECTO X ============================================*/

	function list_meses($tp,$id_p,$id_f,$id_c){
		$this->load->model('menu_modelo');
		$enlaces=$this->menu_modelo->get_Modulos(3);
		$data['enlaces'] = $enlaces;
		for($i=0;$i<count($enlaces);$i++)
		{
			$subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
		}
		$data['subenlaces'] = $subenlaces;

		$data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p); ///// Datos del Proyecto
		$lista_meses = $this->model_ejecucion->list_meses_ejec($id_f,$id_c); ///7 Lista de Ejecucion por meses
		$data['meses']=$lista_meses;
		$data['fase'] = $this->model_faseetapa->fase_etapa($id_f,$id_p); ///// Datos de la Fase Activa
		$data['componente'] = $this->model_componente->get_componente($id_c); //// Datos del Componente
		$data['id_p']=$id_p;
		$data['id_f']=$id_f;
		$data['tipo']=$tp;

		$titulo_proy=strtoupper($data['proyecto'][0]['tipo']);
		$data['titulo_proy'] = $titulo_proy;

		if($this->model_ejecucion->verif_ejecucion_fase_mes($id_f,$id_c,$this->session->userdata("mes"),$this->session->userdata('gestion'))==0)
		{
			$data['verif'] = 'true';$data['dato_form'] = 'FORMULARIO';
		}
		else
		{
			$fase_ejec = $this->model_ejecucion->datos_ejecucion_fase_mes($id_f,$id_c,$this->session->userdata("mes"),$this->session->userdata('gestion'));
			if($fase_ejec[0]['ejec_est']==0) //// Borrador
			{
				$data['verif'] = 'true'; $data['dato_form'] = 'REGISTRO';
			}
			elseif ($fase_ejec[0]['ejec_est']==1) ///// validado y cerrado
			{
				$data['verif'] = 'false'; $data['dato_form'] = 'FORMULARIO (CERRADO)';
			}

		}

		$dato_mes=$this->get_mes($this->session->userdata("mes"));
		$mes = $dato_mes[1];
		$dias = $dato_mes[2];
		$data['mes_id']=$this->session->userdata("mes");
		$data['mes']=$mes;

		/*----------------------------- Tabla de Meses de ejecucion --------------------------------*/
		$tabla = '';
		$nro_mes=1;
		$url_link='';
		$url_link_rev='';
		$estado_eje='';
		foreach($lista_meses as $row)
		{
			if($this->session->userdata("mes")==$row['m_id']){$color='#b3d7f5';}else{$color='';}
			$mes_ejecucion=$this->get_mes($row['m_id']);
			$tabla .= '<tr bgcolor="'.$color.'">';
			$tabla .= '<td>'.$nro_mes.'</td>';
			if($row['ejec_est']==0) //// guardado y modificable http://localhost/sigre/index.php/admin/ejec/proy/2/802/652/465/4
			{
				$estado_eje = '<p class="btn btn-success"><i class="glyphicon glyphicon-edit"></i> (BORRADOR)</p>';
				$url_link = '<li><a href="' . site_url("admin") . '/ejec/proy/'.$tp.'/'.$data['proyecto'][0]['proy_id'].'/'.$row['pfec_id'].'/'.$row['com_id'].'/'.$row['m_id'].'" title="REPORTE DE EJECUCION DE LA OPERACION (GUARDADO)" ><i class="glyphicon glyphicon-pencil"></i> Registro de datos</a></li>';
				$url_link_rev ='';
			}
			elseif($row['ejec_est']==1) //// validado y cerrado
			{
				$estado_eje = '<p class="btn btn-danger"><i class="glyphicon glyphicon-ban-circle"></i> (CERRADO)</p>';
				$url_link = '';
				$url_link_rev = '<li><a href="#" data-toggle="modal" data-target="#modal_del_ff" class="btn btn-xs del_ff" title="REVERTIR EJECUCIÓN"  name="'.$row['fe_id'].'" id="'.$row['com_id'].'" style="text-align:left;"><i class="glyphicon glyphicon-refresh"></i> Revertir</a></li>';
			}
			$tabla .= '<td><div class="btn-group"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-list"></i> ACCIONES <span class="caret"></span></button><ul class="dropdown-menu" role="menu">';
			$tabla .= $url_link.$url_link_rev.'
                  <li><a data-toggle="modal" data-target="#'.$nro_mes.'" title="ANEXOS DE LA EJECUCIÓN"><i class="glyphicon glyphicon-paperclip"></i> Anexos</a></li>';
			$tabla .='<li><a href="javascript:abreVentana(\''.site_url("admin").'/ejec/reporte_ejecucion/'.$tp.'/'.$data['proyecto'][0]['proy_id'].'/'.$row['pfec_id'].'/'.$row['com_id'].'/'.$row['m_id'].'\');" title="REPORTE DE EJECUCIÓN"><i class="glyphicon glyphicon-file"></i> Reporte de ejecución</a></li>
                  <li><a href="javascript:abreVentana(\''.base_url().'assets/sgp/'.$row['f_archivo_sgp'].'\');" title="REPORTE DE SGP"><i class="glyphicon glyphicon-list"></i> SGP</a></li>
                  </div>';
			$archivos = $this->model_ejecucion->list_archivos_mes($row['pfec_id'],$id_c,$row['m_id']);
			$tabla .= '<div class="modal fade bs-example-modal-lg" tabindex="-1" id="'.$nro_mes.'"  role="dialog" aria-labelledby="myLargeModalLabel">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close text-danger " data-dismiss="modal" aria-hidden="true">
                                            &times;
                                        </button>
                                        <h4 class="modal-title text-center text-info">
                                            <b>'.$titulo_proy.' : </b>'.$data['proyecto'][0]['proy_nombre'].'<br>
                                            <b>GESTI&Oacute;N : </b></font>'.$this->session->userdata("gestion").' - <b>MES DE EJECUCI&Oacute;N : </b></font>'.$mes_ejecucion[1].'
                                        </h4>
                                    </div>
                                    <div class="modal-body no-padding">
                                        <div class="well">
                                            <table class="table table-bordered">
                                                <thead>                         
                                                    <tr>
                                                        <th style="width:1%;">Nro</th>
                                                        <th style="width:15%;">ANEXO DE LA EJECUCI&Oacute;N</th>
                                                        <th style="width:5%;">VER ANEXO</th>
                                                    </tr>
                                                </thead>
                                                <tbody>';
			$num=1;
			if(count($archivos)!=0)
			{
				foreach($archivos as $rowa)
				{
					$tabla .= '<tr>';
					$tabla .= '<td>'.$num.'</td>';
					$tabla .= '<td>'.$rowa['documento'].'</td>';
					$tabla .= '<td><center><a href="' . base_url() . '/archivos/emesproy/'.$rowa['archivo'].'" title="VER ARCHIVO " download><img src="' . base_url() . 'assets/ifinal/carp.png" WIDTH="35" HEIGHT="35"/></a></center></td>';
					$tabla .= '</tr>';
					$num=$num+1;
				}
			}
			else
			{
				$tabla .= '<td colspan=3>SIN ANEXOS</td>';
			}
			$tabla .= '
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </td>';
			$tabla .= '<td>'.$mes_ejecucion[1].'</td>';
			$tabla .= '<td style="text-align:left;">'.$row['ejec_prob'].'</td>';
			/*$tabla .= '<td>'.$row['ejec_efect'].'</td>';
			$tabla .= '<td>'.$row['ejec_causas'].'</td>';*/
			$tabla .= '<td style="text-align:left;">'.$row['ejec_sol'].'</td>';
			$tabla .= '<td>'.$estado_eje.'</td>';
			$tabla .= '</tr>';

			$nro_mes++;
		}

		/*------------------------------------------------------------------------------------------*/
		$data['meses_ejecucion']=$tabla;

		$this->load->view('admin/ejecucion/lista_operaciones/historial_ejecucion/list_meses_ejecucion', $data);

	}
    /*=================================================================================================================================*/
   
    /*====================================== FORMULARIO DE REGISTRO DEL PROYECTO SELECCIONADO X ==============================================*/
    public function proyecto_n($tp,$id_p,$id_f,$id_c,$mes_id)
    {
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(3);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++)
        {
             $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;

        $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p);
        $data['fase'] = $this->model_faseetapa->fase_etapa($id_f,$id_p);
        $data['tipo'] = $tp;
        $titulo_proy=strtoupper($data['proyecto'][0]['tipo']);
        $data['titulo_proy'] = $titulo_proy;
       
        $meses=$this->get_mes($mes_id);
        $mes = $meses[1];
        $dias = $meses[2];

        $data['mes']=$mes;
        $data['id_f']=$id_f;
        $data['mes_id']=$mes_id;
        $data['proy_estado'] = $this->model_ejecucion->proy_estado();
        $data['dependecias'] = $this->model_ejecucion->dependencias();
        $data['observaciones'] = $this->model_ejecucion->observaciones_fase($id_f,$mes_id,$id_c);
        $data['componente'] = $this->model_componente->get_componente($id_c);

        $data['atras']='ejec/meses_operacion/'.$tp.'/'.$id_p.'/'.$id_f.'/'.$id_c.'/'.$this->session->userdata('mes');

        if($this->model_ejecucion->verif_ejecucion_fase_mes($id_f,$id_c,$mes_id,$this->session->userdata('gestion'))==0)
        {
            $this->load->view('admin/ejecucion/lista_operaciones/formularios/form_registro_ejecucion', $data);
        }
        else
        {  
            $data['fase_ejec'] = $this->model_ejecucion->datos_ejecucion_fase_mes($id_f,$id_c,$mes_id,$this->session->userdata('gestion'));
            if($data['fase_ejec'][0]['ejec_est']==0) //// Borrador
            {
                $this->load->view('admin/ejecucion/lista_operaciones/formularios/form_update_ejecucion', $data);
            }
            elseif ($data['fase_ejec'][0]['ejec_est']==1) ///// validado y cerrado
            {
               $this->load->view('admin/ejecucion/proyecto_ver', $data);
            }
            
        }
/*       */
    }
    /*===========================================================================================================*/

    /*====================================== VALIDA EJECUCION DEL PROYECTO X=======================================*/
    public function valida()
    {   $fase = $this->model_faseetapa->fase_etapa($this->input->post('id_f'),$this->input->post('id_proy'));
        $proyecto = $this->model_proyecto->get_id_proyecto($this->input->post('id_proy'));
        if($this->input->post('est')==2)
        {
            /*===================================================== EJECUCION PRODUCTO ACTIVIDAD ==================================================*/
            if ( !empty($_POST["producto"]) && is_array($_POST["producto"]) ) {
                $id=1;
                foreach ( array_keys($_POST["producto"]) as $como  ) {

                /*================================================= PRODUCTOS EJECUCION MES ======================================================*/
                    if($_POST["indi"][$como]==1) //// Absoluto
                    {

                       if ($_POST["pe"][$como]!=0) //// Se registra ejecucion 
                       {
                          $this->model_ejecucion->delete_prod_ejec_mes_id($_POST["producto"][$como],$this->input->post('mes_id'),$this->session->userdata('gestion'));
                          $this->model_producto->add_prod_ejec_gest($_POST["producto"][$como],$this->session->userdata('gestion'),$this->input->post('mes_id'),$_POST["pe"][$como],0,0); 
                       }
                       else
                       {
                        $this->model_ejecucion->delete_prod_ejec_mes_id($_POST["producto"][$como],$this->input->post('mes_id'),$this->session->userdata('gestion'));
                       }
                          
                    }
                    if($_POST["indi"][$como]==2) //// Relativo
                    {
                        if($_POST["pa"][$como]!=0) //// se registra ejecucion
                        {
                            if($_POST["den"][$como]==0) /// variable
                                $ejecucion=(($_POST["pb"][$como]/$_POST["pa"][$como])*$_POST["pp"][$como]);
                            elseif ($_POST["den"][$como]==1) /// fijo
                                $ejecucion=(($_POST["pb"][$como]/$_POST["pa"][$como])*100);

                            $this->model_ejecucion->delete_prod_ejec_mes_id($_POST["producto"][$como],$this->input->post('mes_id'),$this->session->userdata('gestion'));
                            $this->model_producto->add_prod_ejec_gest($_POST["producto"][$como],$this->session->userdata('gestion'),$this->input->post('mes_id'),$ejecucion,$_POST["pa"][$como],$_POST["pb"][$como]); 
                        }
                        else
                       {
                        $this->model_ejecucion->delete_prod_ejec_mes_id($_POST["producto"][$como],$this->input->post('mes_id'),$this->session->userdata('gestion'));
                       }
                    }
                /*=================================================================================================================================*/   
                /*================================================= ACTIVIDADES EJECUCION MES ======================================================*/
                    foreach ( array_keys($_POST["actividad"]) as $coma  ) {
                        
                        if($_POST["producto"][$como]==$_POST["prod"][$coma])
                        {
                            
                            if($proyecto[0]['tp_id']==1 && $fase[0]['pfec_ejecucion']==2) //// PROYECTOS INVERSION y delegada
                            {
                                if($_POST["india"][$coma]==1) //// INDICADOR ABSOLUTO
                                {
                                    if ($_POST["ae"][$coma]!=0) 
                                    {
                                       $efin=$_POST["ae"][$coma]*$_POST["cost_u"][$coma];
                                       $this->model_ejecucion->delete_act_ejec_mes_id($_POST["actividad"][$coma],$this->input->post('mes_id'),$this->session->userdata('gestion')); 
                                       $this->model_actividad->add_act_ejec_gest($_POST["actividad"][$coma],$this->session->userdata('gestion'),$this->input->post('mes_id'),$_POST["ae"][$coma],0,0,$efin); 
                                    }
                                    else
                                    {
                                       $this->model_ejecucion->delete_act_ejec_mes_id($_POST["actividad"][$coma],$this->input->post('mes_id'),$this->session->userdata('gestion'));  
                                    }
                                }
                                elseif($_POST["india"][$coma]==2) //// INDICADOR RELATIVO
                                {
                                    if ($_POST["aa"][$coma]!=0) 
                                    {
                                        if($_POST["dena"][$coma]==0) /// variable (b/a)*p
                                            $ejecucion=(($_POST["ab"][$coma]/$_POST["aa"][$coma])*$_POST["ap"][$coma]);
                                        elseif ($_POST["dena"][$coma]==1) /// fijo (b/a)*100
                                            $ejecucion=((($_POST["ab"][$coma])/($_POST["aa"][$coma]))*100);

                                       $efin=$ejecucion*$_POST["cost_u"][$coma];
                                       $this->model_ejecucion->delete_act_ejec_mes_id($_POST["actividad"][$coma],$this->input->post('mes_id'),$this->session->userdata('gestion'));  
                                       $this->model_actividad->add_act_ejec_gest($_POST["actividad"][$coma],$this->session->userdata('gestion'),$this->input->post('mes_id'),$ejecucion,$_POST["aa"][$coma],$_POST["ab"][$coma],$efin); 
                                    }
                                    else
                                    {
                                        $this->model_ejecucion->delete_act_ejec_mes_id($_POST["actividad"][$coma],$this->input->post('mes_id'),$this->session->userdata('gestion')); 
                                    }
                                    
                                }
                            }
                            elseif(($proyecto[0]['tp_id']==2 || $proyecto[0]['tp_id']==3 || $proyecto[0]['tp_id']==4) || ($proyecto[0]['tp_id']==1 && $fase[0]['pfec_ejecucion']==1)) //// PROGRAMAS RECURRENTES, OPERACION DE FUNCIONAMIENTO
                            {
                                if($_POST["india"][$coma]==1) //// INDICADOR ABSOLUTO
                                {
                                    if ($_POST["ae"][$coma]!=0)
                                    {
                                        $this->model_ejecucion->delete_act_ejec_mes_id($_POST["actividad"][$coma],$this->input->post('mes_id'),$this->session->userdata('gestion')); 
                                        $this->model_actividad->add_act_ejec_gest($_POST["actividad"][$coma],$this->session->userdata('gestion'),$this->input->post('mes_id'),$_POST["ae"][$coma],0,0,0); 
                                       //echo "Abs. Registro normal";
                                    }
                                    else
                                    {
                                        $this->model_ejecucion->delete_act_ejec_mes_id($_POST["actividad"][$coma],$this->input->post('mes_id'),$this->session->userdata('gestion')); 
                                    }
                                }
                                elseif($_POST["india"][$coma]==2) //// INDICADOR RELATIVO
                                {
                                    if ($_POST["aa"][$coma]!=0)
                                    {
                                        if($_POST["dena"][$coma]==0) /// variable (b/a)*p 
                                            $ejecucion=(($_POST["ab"][$coma]/$_POST["aa"][$coma])*$_POST["ap"][$coma]);
                                        elseif ($_POST["dena"][$coma]==1) /// fijo (b/a)*100
                                            $ejecucion=(($_POST["ab"][$coma]/$_POST["aa"][$coma])*100);

                                        $this->model_ejecucion->delete_act_ejec_mes_id($_POST["actividad"][$coma],$this->input->post('mes_id'),$this->session->userdata('gestion')); 
                                        $this->model_actividad->add_act_ejec_gest($_POST["actividad"][$coma],$this->session->userdata('gestion'),$this->input->post('mes_id'),$ejecucion,$_POST["aa"][$coma],$_POST["ab"][$coma],0); 
                                       // echo "Rel. Registro Normal";
                                    }
                                    else
                                    {
                                        $this->model_ejecucion->delete_act_ejec_mes_id($_POST["actividad"][$coma],$this->input->post('mes_id'),$this->session->userdata('gestion')); 
                                    }
                                    
                                } 
                            }
                           
                        }
                        
                    }
                }
            }
            /*=========================================================================================================================================================*/
        }

		//AGREGAMOS EL $proy_id para identificar el proyecto al registra el SGP digital
        $this->model_ejecucion->ejecucion_fase_mes($this->input->post('accion'),$this->input->post('id_f'),$this->input->post('id_c'),$this->input->post('mes_id'),$this->session->userdata("fun_id"),$this->input->post('prob'),$this->input->post('efectos'),$this->input->post('causas'),$this->input->post('sol'),$this->input->post('bton_reg'),$this->session->userdata("gestion"),$this->input->post('est'),$this->input->post('dep'),$this->input->post('fpi'),$this->input->post('fpf'));

	    //$respuesta = $this->crear_pdf($this->input->post('id_proy'), $this->input->post('mes_id'), $this->session->userdata("gestion"));
	    $respuesta="";
	    //Actualiza nombre de archivo en registro
	    $this->model_ejecucion->guardar_pdf_bd($this->input->post('id_f'),$this->input->post('id_c'),$this->input->post('mes_id'),$this->session->userdata("gestion"),$respuesta);

	    redirect('admin/ejec/meses_operacion/'.$this->input->post('tp').'/'.$this->input->post('id_proy').'/'.$this->input->post('id_f').'/'.$this->input->post('id_c').'/true');
   }
   
   /*========================= PARA CREAR LOS ARCHIVOS PDF DE LAS EJECUCIONES (SGPs) ===============================*/
   public function crear_pdf($id_proy, $id_m, $gestion){
		$respuesta = '';
				/*INSERTAR SGP EN PDF AQUI
				Cuando m_estado == 1
				Aumentar parametro: id_proy al metodo.*/
//				Utilizar m_id para identificar el mes-
//				g_id para identificar el año
//				Armar el archivo dentro de la carpeta /assets/sgp/id_proy/
//				El nombre del archivo debe ser: id-proy.mes.año.nro.pdf				
	   $nombre_carpeta = '/var/www/siipp/assets/sgp/'.$id_proy;
				
	   $data = $this->reporte($id_proy);

	   $this->load->helper('file');

	   $dompdf = new DOMPDF();
	   $dompdf->load_html($data['datos_generales'].''.$data['pe_fisica'].''.$data['pe_financiera']);
	   $dompdf->set_paper('letter', 'L');//portrait landscape
	   $dompdf->render();
	   $contador = 0;
	   $nasne = $id_proy.'.'.$this->nro_a_mes($id_m).'.'.$gestion;

	   if (!file_exists($nombre_carpeta)) {
		   mkdir($nombre_carpeta, 0755, true);
	   }else{
		   $archivos = get_filenames($nombre_carpeta);
		   foreach($archivos as $item){
			   $porciones = explode(".",$item);
			   $comparador = $porciones[0].'.'.$porciones[1].'.'.$porciones[2];
			   if($comparador == $nasne){
				   $contador++;
			   }
		   }
	   }
	   //Estructura del nombre de archivo ID-PROY.MES-EJECUCIÓN.AÑO.NRO.pdf
	   $nombre_archivo = $nasne.'.'.$contador.'.pdf';

	   if(!write_file($nombre_carpeta.'/'.$nombre_archivo, $dompdf->output()))
	   {
		   $respuesta = $id_proy.'/'.$nombre_archivo;//$respuesta.'U: El archivo <b>'.$nombre_archivo.'</b> no se pudo crear en la carpeta SGP.';
	   }
	   else
	   {
		   $respuesta = $id_proy.'/'.$nombre_archivo;//$respuesta.'Se actualizo correctamente el archivo archivo! ('.$nombre_archivo.')';
	   }
	   return $respuesta;
   }

   /*========================= CONVIERTE UN MES DE FORMATO NUMERAL A SU EQUIVALENTE EN LITERAL ===============================*/
	public function nro_a_mes($nro){
		switch ($nro) {
			case 1:
				return "Enero"; break;
			case 2:
				return "Febrero";	break;
			case 3:
				return "Marzo"; break;
			case 4:
				return "Abril"; break;
			case 5:
				return "Mayo"; break;
			case 6:
				return "Junio"; break;
			case 7:
				return "Julio"; break;
			case 8:
				return "Agosto"; break;
			case 9:
				return "Septiembre"; break;
			case 10:
				return "Octubre"; break;
			case 11:
				return "Noviembre"; break;
			case 12:
				return "Diciembre"; break;
		}		
	}
   
   /*========================= PARA CERRAR EL PROYECTO ===============================*/
   public function update_proyecto()
   { 
        if ($this->input->server('REQUEST_METHOD') === 'POST') ///// DATOS GENERALES
        {
            $this->form_validation->set_rules('proy_id', 'Id Proyecto', 'required|trim');
            $this->form_validation->set_rules('conclusiones', 'conclusiones , conclusiones', 'required|trim');
            $this->form_validation->set_rules('recomendaciones', 'recomendaciones , recomendaciones', 'required|trim');

            if ($this->form_validation->run()) 
            { 
                /*------------------------ ACTUALIZANDO EL ESTADO DEL PROYECTO --------------------*/
                $update_proy = array(
                        'proy_conclusiones' => strtoupper($this->input->post('conclusiones')),
                        'proy_recomendaciones' => strtoupper($this->input->post('recomendaciones')),
                        'proy_lecciones' => strtoupper($this->input->post('lecciones')),
                        'proy_estado' => '5', //// Conclusion fisica y Financiera de la operacion
                    );
                $this->db->where('proy_id', $this->input->post('proy_id'));
                $this->db->update('_proyectos', $update_proy);
                /*---------------------------------------------------------------------------------*/

                if($this->input->post('accion')==1) ///// nuevo registro de la ejecucion
                {
                    $query=$this->db->query('set datestyle to DMY');
                    $data_to_store = array( 
                        'pfec_id' => $this->input->post('id_f'),
                        'com_id' => $this->input->post('id_c'),
                        'm_id' => $this->session->userdata("mes"),
                        'id_fun' => $this->session->userdata("fun_id"),
                        'ejec_fecha' => date('d/m/Y h:i:s'),
                        'ejec_est' => 1, ///// cerrado
                        'g_id' => $this->session->userdata("gestion"),
                        'estado' => 4,
                        'st_id' => 13,
                    );
                    $this->db->insert('fase_ejecucion', $data_to_store);
                }
                elseif ($this->input->post('accion')==2) ///// actualizar estado de la ejecucion
                {
                    $query=$this->db->query('set datestyle to DMY');
                    $update_ejec = array(

                        'id_fun' => $this->session->userdata("fun_id"),
                        'ejec_fecha' => date('d/m/Y h:i:s'),
                        'ejec_est' => 1, ///// cerrado
                        'g_id' => $this->session->userdata("gestion"),
                        'estado' => 4,
                        'st_id' => 13
                        );

                    $this->db->where('pfec_id', $this->input->post('id_f'));
                    $this->db->where('m_id', $this->session->userdata("mes"));
                    $this->db->where('com_id', $this->input->post('id_c'));
                    $this->db->where('g_id', $this->session->userdata("gestion"));
                    $this->db->update('fase_ejecucion', $update_ejec);
                }

                redirect('admin/ejec/meses_operacion/'.$this->input->post('tp').'/'.$this->input->post('proy_id').'/'.$this->input->post('id_f').'/'.$this->input->post('id_c'));
            }
        }
   }

    //=========================== VALIDA OBSERVACIONES =============================
        function valida_observacion(){
        if($this->input->is_ajax_request() && $this->input->post()){
            $this->form_validation->set_rules('f1', 'Fecha Inicial', 'required|trim');
            $this->form_validation->set_rules('f2', 'Fecha Final', 'required|trim');
            $this->form_validation->set_rules('obs', 'Observacion', 'required|trim');
            //=========================== mensajes =========================================
            $this->form_validation->set_message('required', 'El campo es es obligatorio');

            if ($this->form_validation->run() ) {
                $id_p=  $this->input->post('id_pr');
                $id_f=  $this->input->post('id_f');
                $id_m=  $this->input->post('id_m');
                $id_c=  $this->input->post('id_c');
                $f1 =  $this->input->post('f1');
                $f2 =  $this->input->post('f2');
                $obs =  $this->input->post('obs');
                //=================enviar  evitar codigo malicioso ==========
                $id_p = $this->security->xss_clean($id_p);
                $id_f = $this->security->xss_clean($id_f);
                $id_m = $this->security->xss_clean($id_m);
                $id_c = $this->security->xss_clean($id_c);
                $f1 = $this->security->xss_clean($f1);
                $f2 = $this->security->xss_clean($f2);
                $obs = $this->security->xss_clean($obs);
                //======================= MODIFICAR=

                $data = array(
                        'm_id' => $id_m,
                        'g_id' => $this->session->userdata("gestion"),
                        'pfec_id' =>$id_f,
                        'com_id' =>$id_c,
                        'fecha_inicio' =>$f1,
                        'fecha_final' =>$f2,
                        'observacion' =>strtoupper($obs),
                        'fun_id' => $this->session->userdata("fun_id"),
                    );
                    $this->db->insert('fase_observacion',$data);
                    
                    echo'true';
                
            } else {
                echo'DATOS ERRONEOS';
            }
        }else{
            show_404();
        }
    }

    /*===================================== OBTIENE LOS DATOS DE LA OBSERVACION =======================================*/
    public function get_observacion()
    {
        if($this->input->is_ajax_request() && $this->input->post())
        {
            $post = $this->input->post();
            $id = $post['id_fo'];
            $id = $this->security->xss_clean($id);
            $dato_obs = $this->model_ejecucion->observaciones_id($id);
            //caso para modificar el codigo de proyecto y actividades
            foreach($dato_obs as $row){
                $result = array(
                    'fo_id' => $row['fo_id'],
                    "m_id" =>$row['m_id'],
                    "g_id" =>$row['g_id'],
                    "fecha_inicio" =>$row['fecha_inicio'],
                    "fecha_final" =>$row['fecha_final'],
                    "observacion" =>$row['observacion'],
                    "pfec_id" =>$row['pfec_id']
                );
            }
            echo json_encode($result);
        }else{
            show_404();
        }
    }
    /*====================================================================================================================*/
    function update_observacion()
      {
        if($this->input->is_ajax_request() && $this->input->post()){
            $this->form_validation->set_rules('fo_id', 'id fase observacion', 'required|trim');
            //=========================== mensajes =========================================
            $this->form_validation->set_message('required', 'El campo es es obligatorio');
            /*if ($this->form_validation->run() ) {*/
                $post = $this->input->post();
                $id = $post['fo_id'];
                $fi = $post['fi'];
                $ff = $post['ff'];
                $obs = $post['obser'];
                //================ evitar enviar codigo malicioso ==========
                $id= $this->security->xss_clean($id);
                $fi= $this->security->xss_clean($fi);
                $ff= $this->security->xss_clean($ff);
                $obs= $this->security->xss_clean($obs);
                
                $update_fo = array(
                        'fecha_inicio' => $fi,
                        'fecha_final' => $ff,
                        'observacion' => strtoupper($obs),
                        'estado' => '2',
                        'fun_id' => $this->session->userdata("fun_id"),
                    );
                $this->db->where('fo_id', $id);
                $this->db->update('fase_observacion', $update_fo);

                echo 'true';
        }else{
            show_404();
        }
    }

    /*======================================== DELETE OBSERVACION ======================================*/
    function delete_observacion(){
        if($this->input->is_ajax_request() && $this->input->post()){
            $post = $this->input->post();
            $fo_id = $post['fo_id'];

            $this->db->where('fo_id', $fo_id);
            $this->db->delete('fase_observacion');
            $sql = $this->db->get();
           
            if($this->db->query($sql)){
                echo $fo_id;
            }else{
                echo false;
            }
        }else{
            show_404();
        }
    }
/*----------------------------------- ARCHIVOS MENSUALES ------------------------------------*/
    function list_archivos($tp,$id_p,$id_f,$id_c,$id_mes)
    {
        $enlaces=$this->menu_modelo->get_Modulos(3);
        $data['enlaces'] = $enlaces;
        
        for($i=0;$i<count($enlaces);$i++)
        {
             $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;

      $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p);
      $data['fase'] = $this->model_faseetapa->fase_etapa($id_f,$id_p);
      $data['componente'] = $this->model_componente->get_componente($id_c);
      $archivos = $this->model_ejecucion->lista_archivos_mes($id_f,$id_mes);

        $titulo_proy=strtoupper($data['proyecto'][0]['tipo']);
        $data['titulo_proy'] = $titulo_proy;
       
        $meses=$this->get_mes($id_mes);
        $mes = $meses[1];
        $dias = $meses[2];

        $data['mes']=$mes;
        $data['tipo']=$tp;
        $data['id_mes'] = $id_mes;

        $tabla = ''; $nro=1;
        foreach($archivos as $row)
        {
            $tabla .= '<tr>';
                $tabla .= '<td>'.$nro.'</td>';
                $tabla .= '<td>'.$row['documento'].'</td>';
                if(file_exists("archivos/emesproy/".$row['archivo']))
                { 
                    if($row['tip_doc']==1)
                    {
                        $tabla .= '<td><center><a href="javascript:abreVentana(\''.base_url().'/archivos/emesproy/'.$row['archivo'].'\');" title="VER ARCHIVO (IMAGEN)" ><img src="' . base_url() . 'assets/ifinal/img.jpg" WIDTH="35" HEIGHT="35"/></a></center></td>';
                    }
                    elseif ($row['tip_doc']==2) 
                    {
                        $tabla .= '<td><center><a href="javascript:abreVentana(\''.base_url().'/archivos/emesproy/'.$row['archivo'].'\');" title="VER ARCHIVO (PDF)" ><img src="' . base_url() . 'assets/ifinal/pdf.png" WIDTH="35" HEIGHT="35"/></a></center></td>';
                    }
                    elseif ($row['tip_doc']==3) ///// word
                    {
                        $tabla .= '<td><center><a href="' . base_url() . '/archivos/emesproy/'.$row['archivo'].'" title="VER ARCHIVO (WORD)" download><img src="' . base_url() . 'assets/ifinal/word.png" WIDTH="35" HEIGHT="35"/></a></center></td>';
                    }
                    elseif ($row['tip_doc']==4) ///// excel
                    {
                        $tabla .= '<td><center><a href="' . base_url() . '/archivos/emesproy/'.$row['archivo'].'" title="VER ARCHIVO (EXCEL)" download><img src="' . base_url() . 'assets/ifinal/excel.jpg" WIDTH="35" HEIGHT="35"/></a></center></td>';
                    }
                }
                else
                {
                    $tabla .= '<td><font color=red>Error!!</font></td>';
                }
                if($this->session->userdata("fun_id")==$row['id_fun'] || $this->session->userdata("rol")==0)
                {
                    $tabla .= '<td><a href="' . site_url("admin") . '/ejec/delete_anexo_ejec/'.$tp.'/'.$id_p.'/'.$id_f.'/'.$id_c.'/'.$row['fa_id'].'/'.$id_mes.'" title="ELIMINAR ARCHIVO" onclick="return confirm("EN REALIDAD DESEA ELIMINAR ESTE ARCHIVO ?");"><img src="'.base_url().'assets/ifinal/eliminar.png" WIDTH="35" HEIGHT="35"></a></td>';
                }
                else 
                {
                    $tabla .= '<td></td>';
                }
            $tabla .= '</tr>';
            $nro++;
        }

        $data['anexos']=$tabla;
        $this->load->view('admin/ejecucion/lista_operaciones/formularios/anexos_ejecucion', $data); 
    }

    /*======================================== VALIDA ARCHIVO ADJUNTADO MES ====================================*/
    function valida_archivo()
    { //echo $this->input->post('file1');
        if ($this->input->server('REQUEST_METHOD') === 'POST')
          {
            $this->form_validation->set_rules('id_p', 'id de proyecto', 'required|trim');

            if ($this->form_validation->run())
            {
              $filename = $_FILES["file1"]["name"]; ////// datos del archivo 
              $file_basename = substr($filename, 0, strripos($filename, '.')); ///// nombre del archivo
              $file_ext = substr($filename, strripos($filename, '.')); ///// Extension del archivo
              $filesize = $_FILES["file1"]["size"]; //// Tamaño del archivo
              $allowed_file_types = array('.pdf','.docx','.doc','.DOC','.PDF','.xlsx','.xls','.jpg','.JPG','.png','.PNG','.JPEG','.JPG'); 

              if($filename!='')
              {
                 $data_to_store = array( 
                    'pfec_id' => $this->input->post('id_f'),
                    'com_id' => $this->input->post('id_c'),
                    'documento' => $this->input->post('doc'),
                    'ejec_fecha' => date('d/m/Y h:i:s'),
                    'id_fun' => $this->session->userdata("fun_id"),
                    'm_id' => $this->input->post('id_mes'),
                    'tip_doc' => $this->input->post('tp_doc'),
                    'g_id' => $this->session->userdata("gestion"),
                );

                if($filesize!=0)
                {
                    if (in_array($file_ext,$allowed_file_types))
                    { // Rename file
                      $newfilename = ''.$this->input->post('id_p').'_'.substr(md5(uniqid(rand())),0,5).$file_ext;
                      $data_to_store['archivo'] = "".$newfilename;
                      
                      if (file_exists("archivos/emesproy/" . $newfilename))
                      {echo "El archivo ya existe.";}
                      else
                      {
                        move_uploaded_file($_FILES["file1"]["tmp_name"],"archivos/emesproy/" . $newfilename);
                      }
                        $this->db->insert('fase_ejecucion_adjuntos', $data_to_store);
                        redirect('admin/ejec/archivos/'.$this->input->post('tp').'/'.$this->input->post('id_p').'/'.$this->input->post('id_f').'/'.$this->input->post('id_c').'/'.$this->input->post('id_mes').'/true'); 
                    }
                   /* elseif (empty($file_basename)) {  echo "Please select a file to upload.";} 
                    elseif ($filesize > 800000000){ redirect('admin/proy/proyecto/8/'.$this->input->post('id').'/false');}
                    else{echo "Only these file typs are allowed for upload: " . implode(', ',$allowed_file_types);  unlink($_FILES["file1"]["tmp_name"]);       }  */
                }
                elseif($filesize==0)
                {
                  redirect('admin/ejec/archivos/'.$this->input->post('tp').'/'.$this->input->post('id_p').'/'.$this->input->post('id_f').'/'.$this->input->post('id_c').'/'.$this->input->post('id_mes').'/1/1'); 
                }
              }
              else
              {
                redirect('admin/ejec/archivos/'.$this->input->post('tp').'/'.$this->input->post('id_p').'/'.$this->input->post('id_f').'/'.$this->input->post('id_c').'/'.$this->input->post('id_mes').'/1/0'); 
              }
            }
            else
              redirect('admin/ejec/archivos/'.$this->input->post('tp').'/'.$this->input->post('id_p').'/'.$this->input->post('id_f').'/'.$this->input->post('id_c').'/'.$this->input->post('id_mes').'/false');   
          }
    }
    /*==========================================================================================================================*/

/*--------------------  Eliminar Anexo Ejecucion -------------------------*/
public function borrar_archivo_anexo_ejecucion($tp,$proy_id,$fase_id,$com_id,$id,$mes_id) /////// Eliminar fase
  {   

      $arch = $this->model_proyecto->get_archivo_mes($id);
      if(file_exists("archivos/emesproy/".$arch[0]['archivo']))
      {$file = "archivos/emesproy/".$arch[0]['archivo']; 
      
      unlink($file);

        $this->db->where('fa_id', $id);
        $this->db->delete('fase_ejecucion_adjuntos'); 

        redirect('admin/ejec/archivos/'.$tp.'/'.$proy_id.'/'.$fase_id.'/'.$com_id.'/'.$mes_id.'/true');
      }
      else
      {
        $this->db->where('fa_id', $id);
        $this->db->delete('fase_ejecucion_adjuntos'); 

        redirect('admin/ejec/archivos/'.$tp.'/'.$proy_id.'/'.$fase_id.'/'.$com_id.'/'.$mes_id.'/false');
      }

  }

    /*================================== REVERTIR EJECUCION MENSUAL =================================================*/
    public function revertir_mes(){
        if($this->input->is_ajax_request() && $this->input->post()){
            $post = $this->input->post();
            $fe_id = $post['fe_id'];
            $com_id = $post['com_id'];

            $update_ejec = array(
                    'ejec_est' => 0,
                    'id_fun' => $this->session->userdata("fun_id"),
                );
            $this->db->where('fe_id', $fe_id);
            $this->db->update('fase_ejecucion', $update_ejec);

            $sql = $this->db->get();
           
            if($this->db->query($sql)){
                echo $fe_id;
            }else{
                echo false;
            }
        }else{
            show_404();
        }
    }


  /*------------------ GET ARCHIVO SELECCIONADO -------------------*/



/*================================================ LISTA DE CONTRATO =========================================================*/
    public function lista_contratos($tp,$id_p,$id_f,$id_c,$aper,$mes_id)
    {
      $enlaces=$this->menu_modelo->get_Modulos(3);
      $data['enlaces'] = $enlaces;
        for($i=0;$i<count($enlaces);$i++)
        {
             $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;

      $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p);
      $data['componente'] = $this->model_componente->get_componente($id_c);
      $titulo_proy=strtoupper($data['proyecto'][0]['tipo']);
      $data['titulo_proy'] = $titulo_proy;

      $data['id_p']=$id_p;
      $data['id_f']=$id_f;
      $data['aper']=$aper;
      $data['tipo']=$tp;
      $data['mes_id']=$mes_id;
      $this->load->view('admin/ejecucion/contratos/list_contratos', $data); 
    }

/*============================================= NUEVO CONTRATO ==========================================*/
    public function contrato($tp,$id_p,$id_f,$id_c,$aper)
    {
      $enlaces=$this->menu_modelo->get_Modulos(3);
      $data['enlaces'] = $enlaces;
        for($i=0;$i<count($enlaces);$i++)
        {
             $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;

      $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p);
      $titulo_proy=strtoupper($data['proyecto'][0]['tipo']);
      $data['titulo_proy'] = $titulo_proy;
      $data['fase'] = $this->model_faseetapa->fase_etapa($id_f,$id_p);
      $data['componente'] = $this->model_componente->get_componente($id_c);
      $data['mod'] = $this->model_ejecucion->modalidades();
      $data['tipo_ctto'] = $this->model_ejecucion->tipo_contrato();
      $data['contratista'] = $this->model_ejecucion->contratista();
      $data['aper']=$aper;
      $data['id_p']=$id_p;
      $data['id_f']=$id_f;
      $data['tipo']=$tp;
      $this->load->view('admin/ejecucion/contratos/contrato', $data); 
    }

/*============================================= VALIDA CONTRATO ==========================================*/
    public function valida_contrato()
    {
        if($this->input->server('REQUEST_METHOD') === 'POST') 
          {
            $this->form_validation->set_rules('id_p', 'Id del proyecto ', 'required|trim');
            $this->form_validation->set_rules('id_f', 'Fase', 'required|trim');
            $this->form_validation->set_rules('aper', 'apertura', 'required|trim');
            $this->form_validation->set_rules('f_adj', 'Fecha adjudicacion', 'required|trim');
            $this->form_validation->set_rules('f_sus', 'Fecha suscripcion', 'required|trim');
            $this->form_validation->set_rules('f_orden', 'Fecha orden', 'required|trim');

            if ($this->form_validation->run())
            {
                  $data_to_store = array( 
                    'pfec_id' => $this->input->post('id_f'),
                    'ctto_objeto' => strtoupper($this->input->post('obj')),
                    'mod_id' => $this->input->post('mod_id'),
                    'ctto_fecha_adjudicacion' => $this->input->post('f_adj'),
                    'ctto_numero' => $this->input->post('nro_ctto'),
                    'ctto_fecha_suscripcion' => $this->input->post('f_sus'),
                    'ctto_plazo_aa' => $this->input->post('pl_aa'),
                    'ctto_plazo_mm' => $this->input->post('pl_mm'),
                    'ctto_plazo_dd' => $this->input->post('pl_dd'),
                    'ctto_observaciones' => strtoupper($this->input->post('obs')),
                    'ctta_id' => $this->input->post('ctta_id'),
                    'ctto_fecha_orden_inicio' => $this->input->post('f_orden'),
                    'ctto_dolares' => $this->input->post('dolares'),
                    'ctto_bolivianos' => $this->input->post('bol'),
                    'ctto_anticipo' => $this->input->post('ant'),
                    'ctto_resolucion' => $this->input->post('res'),
                    'ctto_moneda' => strtoupper($this->input->post('moneda')),
                    'com_id' => $this->input->post('id_c'),
	                'ctto_tip_id' => $this->input->post('ctto_tip_id'),
                );
                $this->db->insert('_contratos', $data_to_store);

                redirect('admin/ejec/list_contratos/'.$this->input->post('tp').'/'.$this->input->post('id_p').'/'.$this->input->post('id_f').'/'.$this->input->post('id_c').'/'.$this->input->post('aper').'/true'); 
            }
            else
            { 
                redirect('admin/ejec/new_contrato/'.$this->input->post('tp').'/'.$this->input->post('id_p').'/'.$this->input->post('id_f').'/'.$this->input->post('id_c').'/'.$this->input->post('aper').'');
            }
        }
        else
        {
            echo "error";
        }
    }
    /*============================================= UPDATE CONTRATO ==========================================*/
    public function update_contrato($tp,$ctto_id,$id_p,$id_f,$id_c,$aper)
    {
      $enlaces=$this->menu_modelo->get_Modulos(3);
      $data['enlaces'] = $enlaces;
        for($i=0;$i<count($enlaces);$i++)
        {
             $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;

      $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p);
      $titulo_proy=strtoupper($data['proyecto'][0]['tipo']);
      $data['titulo_proy'] = $titulo_proy;
      $data['componente'] = $this->model_componente->get_componente($id_c);
      $data['fase'] = $this->model_faseetapa->fase_etapa($id_f,$id_p);
      $data['mod'] = $this->model_ejecucion->modalidades();
      $data['tipo_ctto'] = $this->model_ejecucion->tipo_contrato();
      $data['contrato'] = $this->model_ejecucion->contrato_id($ctto_id);
      $data['contratista'] = $this->model_ejecucion->contratista();
      $data['ctto_id']=$ctto_id;
      $data['aper']=$aper;
      $data['id_p']=$id_p;
      $data['id_f']=$id_f;
      $data['aper']=$aper;
      $data['tipo']=$tp;
      $this->load->view('admin/ejecucion/contratos/update_contrato', $data); 
    }
/*============================================= VALIDA CONTRATO ==========================================*/
    public function valida_update_contrato()
    {
        if($this->input->server('REQUEST_METHOD') === 'POST') 
          {
            $this->form_validation->set_rules('id_p', 'Id del proyecto ', 'required|trim');
            $this->form_validation->set_rules('id_f', 'Fase', 'required|trim');
            $this->form_validation->set_rules('aper', 'apertura', 'required|trim');
            $this->form_validation->set_rules('f_adj', 'Fecha adjudicacion', 'required|trim');
            $this->form_validation->set_rules('f_sus', 'Fecha suscripcion', 'required|trim');
            $this->form_validation->set_rules('f_orden', 'Fecha orden', 'required|trim');

            if ($this->form_validation->run())
            {
                $update_ctto = array( 
                    'ctto_objeto' => strtoupper($this->input->post('obj')),
                    'mod_id' => $this->input->post('mod_id'),
                    'ctto_fecha_adjudicacion' => $this->input->post('f_adj'),
                    'ctto_numero' => $this->input->post('nro_ctto'),
                    'ctto_fecha_suscripcion' => $this->input->post('f_sus'),
                    'ctto_plazo_aa' => $this->input->post('pl_aa'),
                    'ctto_plazo_mm' => $this->input->post('pl_mm'),
                    'ctto_plazo_dd' => $this->input->post('pl_dd'),
                    'ctto_observaciones' => strtoupper($this->input->post('obs')),
                    'ctta_id' => $this->input->post('ctta_id'),
                    'ctto_fecha_orden_inicio' => $this->input->post('f_orden'),
                    'ctto_dolares' => $this->input->post('dolares'),
                    'ctto_bolivianos' => $this->input->post('bol'),
                    'ctto_anticipo' => $this->input->post('ant'),
                    'ctto_moneda' => strtoupper($this->input->post('moneda')),
                    'ctto_resolucion' => $this->input->post('res'),
	                'ctto_tip_id' => $this->input->post('ctto_tip_id')
                    );

                $this->db->where('ctto_id', $this->input->post('ctto_id'));
                $this->db->update('_contratos', $update_ctto);

                redirect('admin/ejec/list_contratos/'.$this->input->post('tp').'/'.$this->input->post('id_p').'/'.$this->input->post('id_f').'/'.$this->input->post('id_c').'/'.$this->input->post('aper').'/true'); 
            }
            else
            { 
                redirect('admin/ejec/new_contrato/'.$this->input->post('tp').'/'.$this->input->post('ctto_id').'/'.$this->input->post('id_p').'/'.$this->input->post('id_f').'/'.$this->input->post('id_c').'/'.$this->input->post('aper').'/false');
            }
        }
        else
        {
            echo "error";
        }
    }

    /*==================================== ELIMINAR CONTRATO ======================================*/
    function delete_contrato(){
        if($this->input->is_ajax_request() && $this->input->post()){
            $post = $this->input->post();
            $ctto_id = $post['ctto_id'];

            $this->db->where('ctto_id', $ctto_id);
            $this->db->delete('_contratos');
            $sql = $this->db->get();
           
            if($this->db->query($sql)){
                echo $ctto_id;
            }else{
                echo false;
            }
        }else{
            show_404();
        }
    }

    //=========================== VALIDA CONTRATISTA =============================
    function valida_contratista(){
        if($this->input->is_ajax_request() && $this->input->post()){
            $this->form_validation->set_rules('ctta_nombre', 'Nombre Contratista', 'required|trim');
            $this->form_validation->set_rules('ctta_sigla', 'Sigla', 'required|trim');
            $this->form_validation->set_rules('ctta_direccion', 'Direccion', 'required|trim');
            //=========================== mensajes =========================================
            $this->form_validation->set_message('required', 'El campo es es obligatorio');
            
            if ($this->form_validation->run() ) {
                $ctta_nombre=  $this->input->post('ctta_nombre');
                $ctta_sigla=  $this->input->post('ctta_sigla');
                $ctta_direccion =  $this->input->post('ctta_direccion');
                $ctta_ciudad =  $this->input->post('ctta_ciudad');
                $ctta_pais =  $this->input->post('ctta_pais');
                $ctta_casilla =  $this->input->post('ctta_casilla');
                $ctta_fono =  $this->input->post('ctta_fono');
                $ctta_nit =  $this->input->post('ctta_nit');
                $ctta_fax =  $this->input->post('ctta_fax');
                $ctta_email =  $this->input->post('ctta_email');
                $ctta_rep =  $this->input->post('ctta_rep');
                $ctta_cargo =  $this->input->post('ctta_cargo');
                //=================enviar  evitar codigo malicioso ==========
                $ctta_nombre = $this->security->xss_clean($ctta_nombre);
                $ctta_sigla = $this->security->xss_clean($ctta_sigla);
                $ctta_direccion = $this->security->xss_clean($ctta_direccion);
                $ctta_ciudad = $this->security->xss_clean($ctta_ciudad);
                $ctta_pais = $this->security->xss_clean($ctta_pais);
                $ctta_casilla = $this->security->xss_clean($ctta_casilla);
                $ctta_fono = $this->security->xss_clean($ctta_fono);
                $ctta_fax = $this->security->xss_clean($ctta_fax);
                $ctta_nit = $this->security->xss_clean($ctta_nit);
                $ctta_email = $this->security->xss_clean($ctta_email);
                $ctta_rep = $this->security->xss_clean($ctta_rep);
                $ctta_cargo = $this->security->xss_clean($ctta_cargo);

                $data = array(
                        'ctta_nombre' => strtoupper($ctta_nombre),
                        'ctta_sigla' => strtoupper($ctta_sigla),
                        'ctta_direccion' =>strtoupper($ctta_direccion),
                        'ctta_ciudad' =>strtoupper($ctta_ciudad),
                        'ctta_pais' =>strtoupper($ctta_pais),
                        'ctta_casilla' =>strtoupper($ctta_casilla),
                        'ctta_telefonos' =>$ctta_fono,
                        'ctta_nit' =>$ctta_nit,
                        'ctta_fax' =>$ctta_fax,
                        'ctta_email' =>$ctta_email,
                        'ctta_representante' =>strtoupper($ctta_rep),
                        'ctta_cargo' =>strtoupper($ctta_cargo),
                        'fun_id' =>$this->session->userdata("fun_id"),
                    );
                    $this->db->insert('_contratistas',$data);
            } else {
                echo'DATOS ERRONEOS';
            }
        }else{
            show_404();
        }
    }

/*================================================ LISTA DE GARANTIAS =========================================================*/
    public function lista_garantias($tp,$ctto_id,$id_p,$id_f,$id_c,$aper)
    {
      $enlaces=$this->menu_modelo->get_Modulos(3);
      $data['enlaces'] = $enlaces;
        for($i=0;$i<count($enlaces);$i++)
        {
             $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;

      $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p);
      $titulo_proy=strtoupper($data['proyecto'][0]['tipo']);
      $data['titulo_proy'] = $titulo_proy;
      $data['componente'] = $this->model_componente->get_componente($id_c);
      $data['contrato'] = $this->model_ejecucion->contrato_id($ctto_id);
      $data['garantias'] = $this->model_ejecucion->lista_garantias($ctto_id);
      $data['id_p']=$id_p;
      $data['id_f']=$id_f;
      $data['aper']=$aper;
      $data['tipo']=$tp;
      $this->load->view('admin/ejecucion/contratos/garantias/list_garantias', $data); 
    }

/*============================================= NUEVA GARANTIA ==========================================*/
    public function garantia($tp,$ctto_id,$id_p,$id_f,$id_c,$aper)
    {
      $enlaces=$this->menu_modelo->get_Modulos(3);
      $data['enlaces'] = $enlaces;
        for($i=0;$i<count($enlaces);$i++)
        {
             $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;

      $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p);
      $titulo_proy=strtoupper($data['proyecto'][0]['tipo']);
      $data['titulo_proy'] = $titulo_proy;
      $data['fase'] = $this->model_faseetapa->fase_etapa($id_f,$id_p);
      $data['componente'] = $this->model_componente->get_componente($id_c);
      $data['contrato'] = $this->model_ejecucion->contrato_id($ctto_id);
	  $data['garantia_tipo'] = $this->model_ejecucion->tipo_garantia();
      $data['id_p']=$id_p;
      $data['id_f']=$id_f;
      $data['aper']=$aper;
      $data['tipo']=$tp;
      $this->load->view('admin/ejecucion/contratos/garantias/garantia', $data); 
    }
    /*============================================= VALIDA GARANTIA ==========================================*/
    public function valida_garantia()
    {
        if($this->input->server('REQUEST_METHOD') === 'POST') 
          {
            $this->form_validation->set_rules('id_p', 'Id del proyecto ', 'required|trim');
            $this->form_validation->set_rules('id_f', 'Fase', 'required|trim');
            $this->form_validation->set_rules('aper', 'apertura', 'required|trim');
	        $this->form_validation->set_rules('gtia_tip_id', 'tipo', 'required');
            if ($this->form_validation->run())
            {
                  $data_to_store = array( 
                    'ctto_id' => $this->input->post('id_ctto'),
                    'gtia_detalle' => strtoupper($this->input->post('detalle')),
                    'gtia_plzo' => $this->input->post('plazo'),
                    'gtia_emisor' => strtoupper($this->input->post('emisor')),
                    'gtia_fecha_vencimiento' => $this->input->post('f_venc'),
                    'gtia_fecha_emision' => $this->input->post('f_ent'),
                    'gtia_obs' => strtoupper($this->input->post('obs')),
                    'fun_id' =>$this->session->userdata("fun_id"),
	                'gtia_tip_id' =>$this->input->post("gtia_tip_id"),
                );
                $this->db->insert('boleta_garantia', $data_to_store);

                redirect('admin/ejec/garantias/'.$this->input->post('tp').'/'.$this->input->post('id_ctto').'/'.$this->input->post('id_p').'/'.$this->input->post('id_f').'/'.$this->input->post('id_c').'/'.$this->input->post('aper').'/true'); 
            }
            else 
            { 
                redirect('admin/ejec/new_garantia/'.$this->input->post('tp').'/'.$this->input->post('id_ctto').'/'.$this->input->post('id_p').'/'.$this->input->post('id_f').'/'.$this->input->post('id_c').'/'.$this->input->post('aper').'/false'); 
            }
        }
        else
        {
            echo "error";
        }
    }
    /*============================================= UPDATE CONTRATO ==========================================*/
    public function update_garantia($tp,$gtia_id,$ctto_id,$id_p,$id_f,$id_c,$aper)
    {
      $enlaces=$this->menu_modelo->get_Modulos(3);
      $data['enlaces'] = $enlaces;
        for($i=0;$i<count($enlaces);$i++)
        {
             $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;

      $data['proyecto'] = $this->model_proyecto->get_id_proyecto($id_p);
      $data['componente'] = $this->model_componente->get_componente($id_c);
      $titulo_proy=strtoupper($data['proyecto'][0]['tipo']);
      $data['titulo_proy'] = $titulo_proy;
      $data['fase'] = $this->model_faseetapa->fase_etapa($id_f,$id_p);
      $data['mod'] = $this->model_ejecucion->modalidades();
      $data['contrato'] = $this->model_ejecucion->contrato_id($ctto_id);
      $data['garantia'] = $this->model_ejecucion->garantia_id($gtia_id);
      $data['garantia_tipo'] = $this->model_ejecucion->tipo_garantia();
      $data['id_p']=$id_p;
      $data['id_f']=$id_f;
      $data['ctto_id']=$ctto_id;
      $data['aper']=$aper;
      $data['tipo']=$tp;
      $this->load->view('admin/ejecucion/contratos/garantias/update_garantia', $data); 
    }

    /*============================================= VALIDA UPDATE GARANTIA ==========================================*/
    public function valida_update_garantia()
    {
        if($this->input->server('REQUEST_METHOD') === 'POST') 
          {
            $this->form_validation->set_rules('id_p', 'Id del proyecto ', 'required|trim');
            $this->form_validation->set_rules('id_f', 'Fase', 'required|trim');
            $this->form_validation->set_rules('aper', 'apertura', 'required|trim');
	        $this->form_validation->set_rules('gtia_tip_id', 'Tipo', 'required');
            if ($this->form_validation->run())
            {
                $update_ctto = array( 
                    'gtia_detalle' => strtoupper($this->input->post('detalle')),
                    'gtia_plzo' => $this->input->post('plazo'),
                    'gtia_emisor' => strtoupper($this->input->post('emisor')),
                    'gtia_fecha_vencimiento' => $this->input->post('f_venc'),
                    'gtia_fecha_emision' => $this->input->post('f_ent'),
                    'gtia_obs' => strtoupper($this->input->post('obs')),
	                'gtia_tip_id' => $this->input->post('gtia_tip_id'),
                    'gtia_estado' => '2'
                    );

                $this->db->where('gtia_id', $this->input->post('id_gta'));
                $this->db->update('boleta_garantia', $update_ctto);

                redirect('admin/ejec/garantias/'.$this->input->post('tp').'/'.$this->input->post('id_ctto').'/'.$this->input->post('id_p').'/'.$this->input->post('id_f').'/'.$this->input->post('id_c').'/'.$this->input->post('aper').'/true2'); 
            }
            else
            { 
                redirect('admin/ejec/edit_garantia/'.$this->input->post('tp').'/'.$this->input->post('id_gta').'/'.$this->input->post('id_ctto').'/'.$this->input->post('id_p').'/'.$this->input->post('id_f').'/'.$this->input->post('id_c').'/'.$this->input->post('aper').'/false'); 
            }
        }
        else
        {
            echo "error";
        }
    }

    /*==================================== ELIMINAR GARANTIA ======================================*/
    function delete_garantia(){
        if($this->input->is_ajax_request() && $this->input->post()){
            $post = $this->input->post();
            $gtia_id = $post['gtia_id'];

            $this->db->where('gtia_id', $gtia_id);
            $this->db->delete('boleta_garantia');
            $sql = $this->db->get();
           
            if($this->db->query($sql)){
                echo $gtia_id;
            }else{
                echo false;
            }
        }else{
            show_404();
        }
    }



    /*============================================ OBJETIVOS ESTRATEGICOS ==================================*/
    public function cprog_red_objetivos() 
      {
        $enlaces=$this->menu_modelo->get_Modulos(3);
        $data['enlaces'] = $enlaces;
        for($i=0;$i<count($enlaces);$i++) 
        {
            $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;

        $meses=$this->get_mes($this->session->userdata('mes'));
        $mes = $meses[1];
        $dias = $meses[2];

        $data['mes']=$mes;
        $data['lista_objetivos'] = $this->mobjetivos->lista_objetivos($this->session->userdata('gestion'));

        $this->load->view('admin/ejecucion/objetivo_estrategico/mis_objetivos', $data);
      }

    /*============================================ OBJETIVOS ESTRATEGICOS  EJECUTAR ==================================*/
    public function objetivo_estrategico($obj_id) 
      {
        $enlaces=$this->menu_modelo->get_Modulos(3);
        $data['enlaces'] = $enlaces;
        for($i=0;$i<count($enlaces);$i++) 
        {
            $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;

        $data['objetivo'] = $this->mobjetivos->dato_objetivo($obj_id);
        $data['observaciones'] = $this->model_ejecucion->oe_observacion($obj_id,$this->session->userdata('gestion'));
        $data['oe'] = $this->model_ejecucion->oe($obj_id);
        $data['nro_p'] = $this->model_ejecucion->nro_oe_programado($obj_id,$this->session->userdata('gestion')); // nro programado
        $data['programado'] = $this->model_ejecucion->oe_programado($obj_id,$this->session->userdata('gestion')); // programado
        $data['nro_e'] =$this->model_ejecucion->nro_oe_ejecutado($obj_id,$this->session->userdata('gestion')); /// nro ejecutado
        $data['ejecutado'] =$this->model_ejecucion->oe_ejecutado($obj_id,$this->session->userdata('gestion')); /// ejecutado

        $o_ejec = $this->model_ejecucion->get_ejecucion_oe($obj_id,$this->session->userdata('gestion'));
        if(count($o_ejec)!=0)
        {
            if($o_ejec[0]['oe_estado']==1)
            {
                redirect('admin/ejec/obj_update/'.$obj_id.''); 
            }
            else
            {
                redirect('admin/ejec/obj_cerrado/'.$obj_id.''); 
            }
        }
        else
        {
           $this->load->view('admin/ejecucion/objetivo_estrategico/objetivo', $data); 
        }

      }

    /*======================================== VALIDA EJECUCION OBJETIVO ESTRATEGICO ====================================*/
    public function valida_oe()
    { // echo $this->input->post('p').'-'.$this->input->post('e').'-'.$this->input->post('ea').'-'.$this->input->post('eb');
        if ($this->input->server('REQUEST_METHOD') === 'POST')
          {
            $this->form_validation->set_rules('obje_id', 'obje_id', 'required|trim');
            //$this->form_validation->set_rules('file1', 'Seleccione Archivo', 'required|trim');
            if ($this->form_validation->run())
            { 
                if($this->input->post('estado')==1) //// Nuevo 
                {
                    $data_to_store = array( 
                    'obje_id' => $this->input->post('obje_id'),
                    'ejec_prob' => strtoupper($this->input->post('prob')),
                    'ejec_causas' => strtoupper($this->input->post('causas')),
                    'ejec_sol' => strtoupper($this->input->post('sol')),
                    'g_id' => $this->input->post('id_g'),
                    'fun_id' => $this->session->userdata("fun_id"),
                    'oe_estado' => $this->input->post('bton_reg'),
                    );
                    $this->db->insert('oe_ejecucion', $data_to_store);
                }
                elseif ($this->input->post('estado')==2) ///// Modificado
                {
                    $update_oe = array( 
                    'ejec_prob' => strtoupper($this->input->post('prob')),
                    'ejec_causas' => strtoupper($this->input->post('causas')),
                    'ejec_sol' => strtoupper($this->input->post('sol')),
                    'fun_id' => $this->session->userdata("fun_id"),
                    'oe_estado' => $this->input->post('bton_reg'),
                    );

                $this->db->where('oe_id', $this->input->post('oe_id'));
                $this->db->update('oe_ejecucion', $update_oe);
                }


                if($this->input->post('indi_id')==1)
                {
                        $this->model_ejecucion->delete_oe_id($this->input->post('obje_id'),$this->input->post('id_g'));
                        $this->model_ejecucion->add_oe_ejec($this->input->post('obje_id'),$this->input->post('id_g'),$this->input->post('e'),0,0); 
                }
                elseif ($this->input->post('indi_id')==2)
                {
                    if($this->input->post('den')==0) /// variable
                        $ejecucion=(($this->input->post('eb')/$this->input->post('ea'))*$this->input->post('p'));
                    elseif ($this->input->post('den')==1) /// fijo
                        $ejecucion=(($this->input->post('eb')/$this->input->post('ea'))*100);

                        $this->model_ejecucion->delete_oe_id($this->input->post('obje_id'),$this->input->post('id_g'));
                        $this->model_ejecucion->add_oe_ejec($this->input->post('obje_id'),$this->input->post('id_g'),$ejecucion,$this->input->post('ea'),$this->input->post('eb')); 
                }


                if($this->input->post('bton_reg')==0)
                {
                    redirect('admin/ejec/obj_update/'.$this->input->post('obje_id').'/true'); 
                }
                elseif($this->input->post('bton_reg')==1)
                {
                   redirect('admin/ejec/obj_cerrado/'.$this->input->post('obje_id').'/cerrado');  
                }
                
            }
            else
              redirect('admin/ejec/obj_est/'.$this->input->post('obje_id').'/false');   
          }
    }
    /*==========================================================================================================================*/
    /*============================================ OBJETIVOS ESTRATEGICOS  EJECUTAR ==================================*/
    public function objetivo_estrategico_update($obj_id) 
      {
        $enlaces=$this->menu_modelo->get_Modulos(3);
        $data['enlaces'] = $enlaces;
        for($i=0;$i<count($enlaces);$i++) 
        {
            $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;

        $data['objetivo'] = $this->mobjetivos->dato_objetivo($obj_id);
        $data['observaciones'] = $this->model_ejecucion->oe_observacion($obj_id,$this->session->userdata('gestion'));
        $data['oe'] = $this->model_ejecucion->oe($obj_id);
        $data['o_ejec'] = $this->model_ejecucion->get_ejecucion_oe($obj_id,$this->session->userdata('gestion'));

        $data['nro_p'] = $this->model_ejecucion->nro_oe_programado($obj_id,$this->session->userdata('gestion')); // nro programado
        $data['programado'] = $this->model_ejecucion->oe_programado($obj_id,$this->session->userdata('gestion')); // programado
        $data['nro_e'] =$this->model_ejecucion->nro_oe_ejecutado($obj_id,$this->session->userdata('gestion')); /// nro ejecutado
        $data['ejecutado'] =$this->model_ejecucion->oe_ejecutado($obj_id,$this->session->userdata('gestion')); /// ejecutado

        $this->load->view('admin/ejecucion/objetivo_estrategico/objetivo_update', $data);
      }

          /*============================================ OBJETIVOS ESTRATEGICOS  EJECUTAR ==================================*/
        public function objetivo_estrategico_cerrado($obj_id) 
      {
        $enlaces=$this->menu_modelo->get_Modulos(3);
        $data['enlaces'] = $enlaces;
        for($i=0;$i<count($enlaces);$i++) 
        {
            $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;

        $data['objetivo'] = $this->mobjetivos->dato_objetivo($obj_id);
        $data['observaciones'] = $this->model_ejecucion->oe_observacion($obj_id,$this->session->userdata('gestion'));
        $data['oe'] = $this->model_ejecucion->oe($obj_id);
        $data['o_ejec'] = $this->model_ejecucion->get_ejecucion_oe($obj_id,$this->session->userdata('gestion'));

        $data['nro_p'] = $this->model_ejecucion->nro_oe_programado($obj_id,$this->session->userdata('gestion')); // nro programado
        $data['programado'] = $this->model_ejecucion->oe_programado($obj_id,$this->session->userdata('gestion')); // programado
        $data['nro_e'] =$this->model_ejecucion->nro_oe_ejecutado($obj_id,$this->session->userdata('gestion')); /// nro ejecutado
        $data['ejecutado'] =$this->model_ejecucion->oe_ejecutado($obj_id,$this->session->userdata('gestion')); /// ejecutado

        $this->load->view('admin/ejecucion/objetivo_estrategico/objetivo_ver', $data);
      }

        //=========================== VALIDA OBSERVACIONES OE =============================
        public function valida_observacion_oe(){
        if($this->input->is_ajax_request() && $this->input->post()){
            $this->form_validation->set_rules('f1', 'Fecha Inicial', 'required|trim');
            $this->form_validation->set_rules('f2', 'Fecha Final', 'required|trim');
            $this->form_validation->set_rules('obs', 'Observacion', 'required|trim');
            //=========================== mensajes =========================================
            $this->form_validation->set_message('required', 'El campo es es obligatorio');
            
            if ($this->form_validation->run() ) {
                $obje_id=  $this->input->post('obje_id');
                $id_g=  $this->input->post('id_g');
                $f1 =  $this->input->post('f1');
                $f2 =  $this->input->post('f2');
                $obs =  $this->input->post('obs');
                //=================enviar  evitar codigo malicioso ==========
                $obje_id = $this->security->xss_clean($obje_id);
                $id_g = $this->security->xss_clean($id_g);
                $f1 = $this->security->xss_clean($f1);
                $f2 = $this->security->xss_clean($f2);
                $obs = $this->security->xss_clean($obs);
                //======================= MODIFICAR=

                $data = array(
                        'obje_id' => $obje_id,
                        'g_id' => $id_g,
                        'fecha_inicio' =>$f1,
                        'fecha_final' =>$f2,
                        'observacion' =>strtoupper($obs),
                        'fun_id' => $this->session->userdata("fun_id"),
                    );
                    $this->db->insert('oe_observacion',$data);

                
            } else {
                echo'DATOS ERRONEOS';
            }
        }else{
            show_404();
        }
    }

    /*===================================== OBTIENE LOS DATOS DE LA OBSERVACION OE =======================================*/
    public function get_observacion_oe()
    {
        if($this->input->is_ajax_request() && $this->input->post())
        {
            $post = $this->input->post();
            $id = $post['obs_id'];
            $id = $this->security->xss_clean($id);
            $dato_obs = $this->model_ejecucion->observaciones_id_oe($id);
            //caso para modificar el codigo de proyecto y actividades
            foreach($dato_obs as $row){
                $result = array(
                    'obs_id' => $row['obs_id'],
                    'obje_id' => $row['obje_id'],
                    "g_id" =>$row['g_id'],
                    "fecha_inicio" =>$row['fecha_inicio'],
                    "fecha_final" =>$row['fecha_final'],
                    "observacion" =>$row['observacion']
                );
            }
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    /*====================================================================================================================*/
    public function update_observacion_oe()
      {
        if($this->input->is_ajax_request() && $this->input->post()){
            $this->form_validation->set_rules('obs_id', 'obs_id', 'required|trim');
            //=========================== mensajes =========================================
            $this->form_validation->set_message('required', 'El campo es es obligatorio');
            /*if ($this->form_validation->run() ) {*/
                $post = $this->input->post();
                $id = $post['obs_id'];
                $fi = $post['fi'];
                $ff = $post['ff'];
                $obs = $post['obser'];
                //================ evitar enviar codigo malicioso ==========
                $id= $this->security->xss_clean($id);
                $fi= $this->security->xss_clean($fi);
                $ff= $this->security->xss_clean($ff);
                $obs= $this->security->xss_clean($obs);
                
                $update_oe = array(
                        'fecha_inicio' => $fi,
                        'fecha_final' => $ff,
                        'observacion' => strtoupper($obs),
                        'estado' => '2',
                        'fun_id' => $this->session->userdata("fun_id"),
                    );
                $this->db->where('obs_id', $id);
                $this->db->update('oe_observacion', $update_oe);

                echo 'true';
        }else{
            show_404();
        }
    }

    /*======================================== DELETE OBSERVACION OE ======================================*/
    public function delete_observacion_oe(){
        if($this->input->is_ajax_request() && $this->input->post()){
            $post = $this->input->post();
            $obs_id = $post['obs_id'];

            $this->db->where('obs_id', $obs_id);
            $this->db->delete('oe_observacion');
            $sql = $this->db->get();
           
            if($this->db->query($sql)){
                echo $fo_id;
            }else{
                echo false;
            }
        }else{
            show_404();
        }
    }
    /*============================================ ARCHIVOS ADJUNTOS ==================================*/
    public function archivos_adjuntos_oe($obj_id) 
      {
        $enlaces=$this->menu_modelo->get_Modulos(3);
        $data['enlaces'] = $enlaces;
        for($i=0;$i<count($enlaces);$i++) 
        {
            $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;

        $data['objetivo'] = $this->mobjetivos->dato_objetivo($obj_id);
        $data['archivos'] = $this->model_ejecucion->archivos_oe($obj_id,$this->session->userdata('gestion'));

        $this->load->view('admin/ejecucion/objetivo_estrategico/documentos', $data);
      }

    /*======================================== VALIDA ARCHIVO ADJUNTADO MES ====================================*/
    function valida_archivo_oe()
    { //echo $this->input->post('file1');
        if ($this->input->server('REQUEST_METHOD') === 'POST')
          {
            $this->form_validation->set_rules('obje_id', 'obje_id', 'required|trim');

            if ($this->form_validation->run())
            {
              $filename = $_FILES["file1"]["name"]; ////// datos del archivo 
              $file_basename = substr($filename, 0, strripos($filename, '.')); ///// nombre del archivo
              $file_ext = substr($filename, strripos($filename, '.')); ///// Extension del archivo
              $filesize = $_FILES["file1"]["size"]; //// Tamaño del archivo
              $allowed_file_types = array('.pdf','.docx','.doc','.xlsx','.xls','.jpg','.jpeg','.JPG','.png','.PNG','.JPEG','.JPG'); 

              if($filename!='')
              {
                 $data_to_store = array( 
                    'obje_id' => $this->input->post('obje_id'),
                    'documento' => $this->input->post('doc'),
                    'fun_id' => $this->session->userdata("fun_id"),
                    'tip_doc' => $this->input->post('tp_doc'),
                    'g_id' => $this->session->userdata("gestion"),
                );

                if($filesize!=0)
                {
                    if (in_array($file_ext,$allowed_file_types))
                    { // Rename file
                      $newfilename = ''.$this->input->post('id_p').'_'.substr(md5(uniqid(rand())),0,5).$file_ext;
                      $data_to_store['archivo'] = "".$newfilename;
                      
                      if (file_exists("archivos/emesproy/" . $newfilename))
                      {echo "El archivo ya existe.";}
                      else
                      {
                        move_uploaded_file($_FILES["file1"]["tmp_name"],"archivos/oestrategico_adjuntos/" . $newfilename);
                      }
                        $this->db->insert('oe_adjuntos', $data_to_store);
                        redirect('admin/ejec/archivos_oe/'.$this->input->post('obje_id').'/true'); 
                    }
                   /* elseif (empty($file_basename)) {  echo "Please select a file to upload.";} 
                    elseif ($filesize > 800000000){ redirect('admin/proy/proyecto/8/'.$this->input->post('id').'/false');}
                    else{echo "Only these file typs are allowed for upload: " . implode(', ',$allowed_file_types);  unlink($_FILES["file1"]["tmp_name"]);       }  */
                }
                elseif($filesize==0)
                {
                  redirect('admin/ejec/archivos_oe/'.$this->input->post('obje_id').'/1/1'); 
                }
              }
              else
              {
                redirect('admin/ejec/archivos_oe/'.$this->input->post('obje_id').'/1/0'); 
              }
            }
            else
              redirect('admin/ejec/archivos_oe/'.$this->input->post('obje_id').'/false'); redirect('admin/ejec/archivos/'.$this->input->post('tp').'/'.$this->input->post('id_p').'/'.$this->input->post('id_f').'/'.$this->input->post('id_c').'/'.$this->input->post('id_mes').'/false');   
          }
    }

    /*==========================================================================================================================*/
    public function delete_archivo(){
        if($this->input->is_ajax_request() && $this->input->post()){
            $post = $this->input->post();
            $oe_id = $post['oe_id'];
            $obje_id = $post['obje_id'];

            $archivo = $this->model_ejecucion->archivos_id_oe($oe_id);

            $file = "archivos/oestrategico_adjuntos/".$archivo[0]['archivo']; 
            unlink($file);

            $this->db->where('oe_id', $oe_id);
            $this->db->delete('oe_adjuntos'); 

            $sql = $this->db->get();
           
            if($this->db->query($sql)){
                echo $oe_id;
            }else{
                echo false;
            }
        }else{
            show_404();
        }
    }


    /*------------------------------- LISTA APERTURAS PROGRAMATICAS --------------------- */
    public function list_aperturas()
    {
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(3);
        $data['enlaces'] = $enlaces;
      
        for($i=0;$i<count($enlaces);$i++)
        {
             $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
       //load the view
        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];

        $lista_aper_padres = $this->mapertura_programatica->lista_aperturas_padres();//lista de aperturas padres
        $data['lista_uni'] = $this->munidad_organizacional->lista_unidad_org();
        $cont = 1;
        $tabla = '';
        foreach ($lista_aper_padres as $row) {
            $lista_aper_hijas = $this->mapertura_programatica->lista_aper_hijas($row['aper_programa'],$row['aper_gestion']);
            if(count($lista_aper_hijas)!=0)
            {
                $tabla .=  '<tr style="background-color: #8fbc8f" id="tr' . $row['aper_id'] . '">';
                $tabla .=  '<td style="color: #FFFFFF">' . ($cont++) . '</td>';
                $tabla .=  '<td style="color: #FFFFFF"></td>';
                $tabla .=  '<td style="color: #FFFFFF">' . $row['aper_gestion'] . '</td>';
                $tabla .=  '<td style="color: #FFFFFF">' . $row['aper_programa'] . '</td>';
                $tabla .=  '<td style="color: #FFFFFF">' . $row['aper_proyecto'] . '</td>';
                $tabla .=  '<td style="color: #FFFFFF">' . $row['aper_actividad'] . '</td>';
                $tabla .=  '<td style="color: #FFFFFF">' . $row['aper_descripcion'] . '</td>';
                $tabla .=  '<td style="color: #FFFFFF">' . $row['uni_unidad'] . '</td>';
                $tabla .=  '</tr>';
                
                foreach($lista_aper_hijas as $row2){
                    $ppto = $this->model_ejecucion->get_proyecto_ejec_ptto($row2['aper_id'] );
                    if(count($ppto)!=0){$color='#E6E6FA';}else{$color='';}
                    $tabla .=  '<tr style="background-color: '.$color.'" id="tr' . $row2['aper_id'] . '">';
                    $tabla .=  '<td>'.($cont++).'</td>';
                    $tabla .=  '<td>
                                    <a href="' . site_url("admin") . '/ejec/ejec_sigep/'.$row2['aper_id'].'">
                                        <center>
                                            <img src="' . base_url() . 'assets/ifinal/money.png" width="30" height="30"
                                            class="img-responsive "title="REGISTRAR PROGRAMACION PRESUPUESTARIA SIGEP">REGISTRAR PRESUPUESTO
                                        </center>
                                    </a>
                               </td>';
                    $tabla .=  '<td>' . $row2['aper_gestion'] . '</td>';
                    $tabla .=  '<td>' . (int)$row2['aper_programa'] . '</td>';
                    $tabla .=  '<td>' . (int)$row2['aper_proyecto'] . '</td>';
                    $tabla .=  '<td>' . (int)$row2['aper_actividad'] . '</td>';
                    $tabla .=  '<td>' . $row2['aper_descripcion'] . '</td>';
                    $tabla .=  '<td>' . $row2['uni_unidad'] . '</td>';
                    $tabla .= '</tr>';
                }
            }
        }
        $data['lista_aperturas'] = $tabla;
        $this->load->view('admin/ejecucion/presupuestaria/list_aperturas', $data);
    }

    /*--------------------- REGISTRO PRESUPUESTO SIGEP (MANUAL)----------------------*/
    public function ejecucion_presupuesto($aper_id)
    {
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(3);
        $data['enlaces'] = $enlaces;
      
        for($i=0;$i<count($enlaces);$i++)
        {
             $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
       //load the view
        $mes[1]='MES ENERO';
        $mes[2]='MES FEBRERO';
        $mes[3]='MES MARZO';
        $mes[4]='MES ABRIL';
        $mes[5]='MES MAYO';
        $mes[6]='MES JUNIO';
        $mes[7]='MES JULIO';
        $mes[8]='MES AGOSTO';
        $mes[9]='MES SEPTIEMBRE';
        $mes[10]='MES OCTUBRE';
        $mes[11]='MES NOVIEMBRE';
        $mes[12]='MES DICIEMBRE';
        
        $data['mes']=$mes;
        $data['apertura'] = $this->mapertura_programatica->dato_apertura($aper_id);//get apertura
        $ppto = $this->model_ejecucion->get_proyecto_ejec_ptto($aper_id); // get ppto
        if(count($ppto)==0){
            $this->load->view('admin/ejecucion/presupuestaria/form_ejecucion', $data);
        }
        elseif(count($ppto)!=0)
        {
            $data['ppto']=$ppto;
            $this->load->view('admin/ejecucion/presupuestaria/update_ejecucion', $data);
        }
        else{
            echo "error";
        }
    }

    /*------------------------ Valida registro manual sigep --------------------------*/
    public function valida_ejec_sigep()
    {   
        if ( !empty($_POST["mes"]) && is_array($_POST["mes"]) ) {
                
                foreach ( array_keys($_POST["mes"]) as $como  ) {

                    if($_POST["pi"][$como]!=0 || $_POST["pi"][$como]!='')
                    {
                        if($this->input->post('tp')=='insert')
                        {
                           // echo "insert : ".$_POST["mes"][$como].'----'.$_POST["pi"][$como].'<br>'; 
                            $query=$this->db->query('set datestyle to DMY');
                            $data_to_store = array( 
                                'aper_id' => $this->input->post('aper_id'),
                                'm_id' => $_POST["mes"][$como],
                                'pe_pi' => $_POST["pi"][$como],
                                'pe_pm' => $_POST["pm"][$como],
                                'pe_pv' => $_POST["pv"][$como],
                                'pe_pe' => $_POST["pe"][$como],
                                'fun_id' => $this->session->userdata("fun_id"),
                            );
                            $this->db->insert('_proy_ejec_ppto', $data_to_store);
                        }
                        elseif($this->input->post('tp')=='modif')
                        {
                            if($_POST["pe_id"][$como]!=0)
                            {
                                $query=$this->db->query('set datestyle to DMY');
                                $update_pres = array( 
                                    'pe_pi' => $_POST["pi"][$como],
                                    'pe_pm' => $_POST["pm"][$como],
                                    'pe_pv' => $_POST["pv"][$como],
                                    'pe_pe' => $_POST["pe"][$como],
                                );

                                $this->db->where('pe_id', $_POST["pe_id"][$como]);
                                $this->db->where('aper_id', $this->input->post('aper_id'));
                                $this->db->update('_proy_ejec_ppto', $update_pres);
                            }
                            else
                            {
                                $query=$this->db->query('set datestyle to DMY');
                                $data_to_store = array( 
                                    'aper_id' => $this->input->post('aper_id'),
                                    'm_id' => $_POST["mes"][$como],
                                    'pe_pi' => $_POST["pi"][$como],
                                    'pe_pm' => $_POST["pm"][$como],
                                    'pe_pv' => $_POST["pv"][$como],
                                    'pe_pe' => $_POST["pe"][$como],
                                    'fun_id' => $this->session->userdata("fun_id"),
                                );
                                $this->db->insert('_proy_ejec_ppto', $data_to_store);
                            }
                          
                        }
                       
                    }
                    else
                    {
                        echo "Error";
                    }
                
                }
                redirect('admin/ejec/ejec_sigep/'.$this->input->post('aper_id').'/true'); 
            }
        else{
            echo "error";
        }
    }

    /*------------------------------- CARGAR ARCHIVO SIGEP --------------------- */
    public function cargar_archivo_sigep()
    {
        $this->load->model('menu_modelo');
        $enlaces=$this->menu_modelo->get_Modulos(3);
        $data['enlaces'] = $enlaces;

        for($i=0;$i<count($enlaces);$i++)
        {
            $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
        }
        $data['subenlaces'] = $subenlaces;
        //load the view
        $mes=$this->get_mes($this->session->userdata("mes"));
        $data['mes'] = $mes[1];
        $data['dias'] = $mes[2];
        $data['meses'] = $this->model_ejecucion->list_meses(); /// Lista de Meses
        $list_meses = $this->model_ejecucion->list_meses_sigep($this->session->userdata('gestion')); /// Lista de Meses sigep

        $tabla ='';
        foreach($list_meses as $row){
            $tabla .='<tr>';
            $tabla .='<td>'.$row['m_descripcion'].'</td>';
            if($row['sg_estado']==1){
                $estado='REGISTRADO';
            }
            else{
                $estado='MODIFICADO';
            }
            $tabla .='<td>'.$estado.'</td>';
            $tabla .='<td>'.date('d/m/Y G:i:s',strtotime($row['fecha'])).'</td>';
            $tabla .='<td>
					<a class="btn btn-info" data-toggle="modal" data-target="#'.$row['sg_id'].'" title="VER DETALLE DE PROGRAMAS NUEVOS" ><i class="glyphicon glyphicon-info-sign"></i> VER</a>
                            <div class="modal fade bs-example-modal-lg" tabindex="-1" id="'.$row['sg_id'].'"  role="dialog" aria-labelledby="myLargeModalLabel">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close text-danger lead" data-dismiss="modal" aria-hidden="true">
                                                &times;
                                            </button>
                                            <h4 class="modal-title">
                                                <img src="'.base_url().'assets/img/logo.png" style="width:300px;" alt="SIIPP">
                                            </h4>
                                        </div>
                                        <div class="modal-body no-padding">
										<br>
                                        <h2 class="text-center modal-title" id="myModalLabel"> OPERACIONES NUEVAS EN EL MES DE '.$row['m_descripcion'].'</h2>
										<br>
                                            <div class="well">';
            $nro_comp=1;
            $comp = $this->model_ejecucion->proyectos_nuevos($row['m_id'],$row['g_id']);
            if(count($comp)!=0){
                $tabla .='<table class="table table-hover table-bordered">
                                                            <thead>
                                                            <tr>
                                                                <th style="width:2%;">#</th>
                                                                <th style="width:10%;">CAT. PROGRAMÁTICA</th>
                                                                <th style="width:30%;">NOMBRE DE LA OPERACION</th>
                                                                <th style="width:10%;">TIPO</th>
                                                                <th style="width:12%;">PTTO. INICIAL</th>
                                                                <th style="width:12%;">PTTO. MODIF</th>
                                                                <th style="width:12%;">PTTO. VIGENTE</th>
                                                                <th style="width:12%;">PTTO. EJEC.</th>
                                                            </tr>
                                                            </thead>';
                foreach ($comp as $rowc){
                    $array = explode("  ", $rowc['cat_prog']);

                    $programa = (int)($array[0]); //// programa
                    $proyecto = (int)($array[1]); //// proyecto
                    $actividad = (int)($array[2]); //// actividad
                    $tipo='';
                    $tb='';
                    if($proyecto>0 && $actividad==0){
                        $tipo='Proyecto';
                    }
                    elseif($proyecto==0 && $actividad>0){
                        $tipo='Programa';
                        $tb='style="background-color:#B2CC96;"';
                    }
                    $tabla .='
                                                                    <tr '.$tb.'>
                                                                    <td>'.$nro_comp.'</td>
                                                                    <td>'.$rowc['cat_prog'].'</td>
                                                                    <td>'.$rowc['cat_nombre'].'</td>
                                                                    <td>'.$tipo.'</td>
                                                                    <td style="text-align:right;">'.number_format($rowc['pe_pi'],2,'.',',').'</td>
                                                                    <td style="text-align:right;">'.number_format($rowc['pe_pm'],2,'.',',').'</td>
                                                                    <td style="text-align:right;">'.number_format($rowc['pe_pv'],2,'.',',').'</td>
                                                                    <td style="text-align:right;">'.number_format($rowc['pe_pe'],2,'.',',').'</td>
                                                                    </tr>';
                    $nro_comp++;
                }
                $tabla .='</table>';
            }
            $tabla.='   
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                        <button type="button" data-dismiss="modal" class="btn btn-default">
                                          <i class="glyphicon glyphicon-remove"></i> 
										  CERRAR VENTANA
                                        </button>
                                        
                                        </div>
                                    </div>
                                </div>
                            </div>';
            $tabla .='</td></tr>';
        }
        $data['meses_sigep'] = $tabla;
        $this->load->view('admin/ejecucion/presupuestaria/lista_sigep', $data);
    }


    public function subir_sigep()
    {
        if ($this->input->post()) {
            $post = $this->input->post();
            $mes_id = $post['mes_id']; /// Mes id

            $tipo = $_FILES['archivo']['type'];
            $tamanio = $_FILES['archivo']['size'];
            $archivotmp = $_FILES['archivo']['tmp_name'];

            //cargamos el archivo
            $lineas = file($archivotmp);

            //inicializamos variable a 0, esto nos ayudará a indicarle que no lea la primera línea
            $i=0;
            $nro=0;
            $nro_not=0;
            $nro_nuevos=0;
            //Recorremos el bucle para leer línea por línea
            $dat='false';
            foreach ($lineas as $linea_num => $linea)
            {
                if($i != 0)
                {
                    $datos = explode(";",$linea);
                    /* $programa = (int)($datos[2]); //// programa
                     $proyecto = (int)($datos[3]); //// proyecto
                     $actividad = (int)($datos[4]); //// actividad*/
                    // echo $datos[0]."<br>";
                    $cadena=$datos[0];
                    $array = explode("  ", $cadena);

                    $pi = (float)($datos[2]); //// presupuesto inicial
                    $pm = (float)($datos[3]); //// presupuesto modificado
                    $pv = (float)($datos[4]); //// presupuesto Vigente
                    $pe = (float)($datos[5]); //// presupuesto Ejecutado

                    if(count($array)==3 & is_numeric($pi) & is_numeric($pm) & is_numeric($pv) & is_numeric($pe)){
                        $programa = (int)($array[0]); //// programa
                        $proyecto = (int)($array[1]); //// proyecto
                        $actividad = (int)($array[2]); //// actividad

                        if(is_numeric($programa) & is_numeric($proyecto) & is_numeric($actividad)){
                            $aper=$this->model_ejecucion->aperturasprogramaticas($programa,$proyecto,$actividad,$this->session->userdata('gestion'));
                            if(count($aper)!=0){
                                $ptto=$this->model_ejecucion->operacion_ptto_sigep($aper[0]['aper_id'],$mes_id);
                                //Inserta o actualiza datos
                                if(count($ptto)!=0){
                                    $this->model_ejecucion->storage_ptto($aper[0]['aper_id'],$mes_id,1,$pi,$pm,$pv,$pe);
                                    $dat='true';
                                }
                                else{
                                    $this->model_ejecucion->storage_ptto($aper[0]['aper_id'],$mes_id,2,$pi,$pm,$pv,$pe);
                                    $dat='true';
                                }
                                $nro++;
                            }else{
                                //Generar el log de las aperturas programáticas no encontradas
                                $nuevo=$this->model_ejecucion->proyectos_existente($cadena,$this->session->userdata('gestion'));
                                if(count($nuevo)==0){
                                    $this->model_ejecucion->set_proyectos_nuevos($cadena,utf8_encode($datos[1]),$pi,$pm,$pv,$pe,$mes_id,$this->session->userdata('gestion'));
                                }
                                $nro_nuevos++;
                            }
                        }
                        // echo $programa."-".$proyecto."-".$actividad."<br>";
                    }
                    else{
                        $nro_not++;
                    }
                }
                $i++;
            }

            if($dat=='true'){
                $messigep=$this->model_ejecucion->meses_sigep($mes_id,$this->session->userdata('gestion'));
                if(count($messigep)==0){
                    $query=$this->db->query('set datestyle to DMY');
                    $data_to_store = array(
                        'm_id' => $mes_id,
                        'g_id' => $this->session->userdata('gestion'),
                        'fun_id' => $this->session->userdata("fun_id"),
                    );
                    $this->db->insert('mes_ejecucion_sigep', $data_to_store);
                }
                else{
                    $query=$this->db->query('set datestyle to DMY');
                    $update_pres = array(
                        'fun_id' => $this->session->userdata("fun_id"),
                        'sg_estado' => 2,
                        'fecha'=>date('Y-m-d G:i:s'),
                    );

                    $this->db->where('m_id', $mes_id);
                    $this->db->where('g_id', $this->session->userdata('gestion'));
                    $this->db->update('mes_ejecucion_sigep', $update_pres);
                }
                $this->session->set_flashdata('success','SE IMPORTARON Y SE REGISTRARON '.$nro.' DATOS, NO SE IMPORTARON '.$nro_not.', NUEVOS PROYECTOS DETECTADOS '.$nro_nuevos);
                redirect(site_url("admin") . '/ejec/l_sigep');
            }
            else{
                $this->session->set_flashdata('danger','Error al Importar Archivo, Vuelva a intentar, verifique Archivo');
                redirect(site_url("admin") . '/ejec/l_sigep');
            }

        }
        else{
            $this->session->set_flashdata('danger','Error al Importar Archivo, Vuelva a intentar, verifique Archivo');
            redirect(site_url("admin") . '/ejec/l_sigep');
        }
    }
    /*----------------------------------------------------------------------------------- */

    public function get_mes($mes_id)
    {
      $mes[1]='ENERO';
      $mes[2]='FEBRERO';
      $mes[3]='MARZO';
      $mes[4]='ABRIL';
      $mes[5]='MAYO';
      $mes[6]='JUNIO';
      $mes[7]='JULIO';
      $mes[8]='AGOSTO';
      $mes[9]='SEPTIEMBRE';
      $mes[10]='OCTUBRE';
      $mes[11]='NOVIEMBRE';
      $mes[12]='DICIEMBRE';

      $dias[1]='31';
      $dias[2]='28';
      $dias[3]='31';
      $dias[4]='30';
      $dias[5]='31';
      $dias[6]='30';
      $dias[7]='31';
      $dias[8]='31';
      $dias[9]='30';
      $dias[10]='31';
      $dias[11]='30';
      $dias[12]='31';

      $valor[1]=$mes[$mes_id];
      $valor[2]=$dias[$mes_id];

      return $valor;
    }

     /*---------------------------------- REPORTE PDF OBJETIVO ESTRATEGICO -----------------------------*/
    public function reporte_ejecucion_mensual($tp,$proy_id,$fase_id,$com_id,$mes_id)
    {  
        $gestion=$this->session->userdata('gestion');
        $proyecto = $this->model_proyecto->get_id_proyecto($proy_id);
        $fase = $this->model_faseetapa->fase_etapa($fase_id,$proy_id);
        
        $titulo_proy=strtoupper($proyecto[0]['tipo']);
       
        $meses=$this->get_mes($mes_id);
        $mes = $meses[1];
        $dias = $meses[2];

        $componente = $this->model_componente->get_componente($com_id);
        $fase_ejec = $this->model_ejecucion->datos_ejecucion_fase_mes($fase_id,$com_id,$mes_id,$this->session->userdata('gestion'));

        $html = '';
        $html .= '
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                <table width="100%">
                    <tr>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/logo.jpg" class="siipp"/>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata('entidad').'<br>
                            <b>POA - PLAN OPERATIVO ANUAL : </b> '.$gestion.'<br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                            <b>FORMULARIO POA : </b> EJECUCI&Oacute;N MES : '.$mes.'<br>
                        </td>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/escudo.jpg" alt="" width="90px">
                        </td>
                    </tr>
                </table>
               
                <footer>
                    <table class="table table-bordered">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata('sistema_pie').'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>
                <table class="datos_principales" style="table-layout:fixed;" border="1">
                    <tr>
                        <td colspan="2">
                            '.$proyecto[0]['proy_nombre'].'<br>
                            '.$proyecto[0]['tipo'].'
                        </td>
                    </tr>
                    <tr>
                        <td style="width:50%;">
                            '.$proyecto[0]['proy_sisin'].'<br>
                            SISIN
                        </td>
                        <td style="width:50%; text-align:center;">
                            '.$proyecto[0]['aper_programa'].''.$proyecto[0]['aper_proyecto'].''.$proyecto[0]['aper_actividad'].'<br>
                            Apertura Programatica
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                            '.$proyecto[0]['ue'].'<br>
                            Unidad Ejecutora
                        </td>
                        <td style="text-align:center;">
                            '.$proyecto[0]['ur'].'<br>
                            Unidad Responsable
                        </td>
                    </tr>
                </table><br>
                <table class="datos_principales" style="table-layout:fixed;" border="1">
                    <tr>
                        <td colspan="2">
                            PERIODO DEL INFORME
                        </td>
                    </tr>
                    <tr>
                        <td style="width:50%;">
                            '.date('d/m/Y',strtotime($fase_ejec[0]['f_inicio_pinforme'])).'<br>
                            FECHA DE INICIO
                        </td>
                        <td style="width:50%; text-align:center;">
                            '.date('d/m/Y',strtotime($fase_ejec[0]['f_final_pinforme'])).'<br>
                            FECHA FINAL
                        </td>
                    </tr>
                </table>';
                    $html.='<div class="contenedor_datos">
                            <table class="table_contenedor" border=1>
                                <tr class="collapse_t">
                                    <td style="width:25%;" class="fila_unitaria titulo_dictamen">
                                        <b> COMPONENTE DE LA OPERACI&Oacute;N</b>
                                    </td>
                                    <td style="width:75%;">'.$componente[0]['com_componente'].'</td>
                                </tr>
                            </table>
                        </div>';

                    $html.='
                        <div class="contenedor_datos" >
                            <table class="table_contenedor">
                                <tr class="collapse_t" style="background-color:#ac9fae;" >
                                    <td>Nro.</td>
                                    <td>PRODUCTOS DE LA OPERACI&Oacute;N</td>
                                    <td>ACTIVIDAD DE LA OPERACI&Oacute;N</td>
                                    <td>COSTO ACTIVIDAD</td>
                                    <td>TIPO DE INDICADOR</td>
                                    <td>INDICADOR</td>
                                    <td>LINEA BASE</td>
                                    <td>META</td>
                                    <td>% DE PONDERACI&Oacute;N</td>
                                    <td>VERIFICACI&Oacute;N</td>
                                    <td>EJECUCI&Oacute;N PRODUCTO '.$mes.' '.$gestion.'</td>
                                </tr>';

                    $productos=$this->model_producto->list_prod($componente[0]['com_id']);
                    $nrop=1;
                    foreach($productos as $rowp)
                    {
                        $html .= '<tr class="collapse_t" bgcolor="#e8e8e8">';
                            $html .= '<td>'.$nrop.'</td>';
                            $html .= '<td>'.$rowp['prod_producto'].'</td>';
                            $html .= '<td></td>';
                            $html .= '<td></td>';
                            $html .= '<td>'.$rowp['indi_abreviacion'].'</td>';
                            $html .= '<td>'.$rowp['prod_indicador'].'</td>';
                            $html .= '<td>'.$rowp['prod_linea_base'].'</td>';
                            $html .= '<td>'.$rowp['prod_meta'].'</td>';
                            $html .= '<td>'.$rowp['prod_ponderacion'].' %</td>';
                            $html .= '<td>'.$rowp['prod_fuente_verificacion'].'</td>';
                                /*---------------------- PROGRAMADO EJECUTADO PRODUCTO ----------------------------*/
                                $programado = $this->model_producto->list_prodgest_anual($rowp['prod_id']);
                                $ejecutado=$this->model_producto->prod_ejec_mensual($rowp['prod_id'],$this->session->userdata("gestion")); /// ejecutado
                                    $n=0;   
                                      /*------------------- programado gestion ------------------*/
                                          $nro=0;
                                          foreach($programado as $row)
                                          {
                                            $nro++;
                                            $matriz [1][$nro]=$row['m_id'];
                                            $matriz [2][$nro]=$row['pg_fis'];
                                          }
                                    /*--------------------------------------------------------*/

                                    /*---------------- llenando la matriz vacia --------------*/
                                          for($j = 1; $j<=12; $j++)
                                          {
                                            $matriz_r[1][$j]=$j; ///// ID MES
                                            $matriz_r[2][$j]='0'; ///// PROGRAMADO
                                            $matriz_r[3][$j]='0'; ///// ACUMULADO PROGRAMADO
                                            $matriz_r[4][$j]='0'; ///// % ACUMULADO PROGRAMADO
                                            $matriz_r[5][$j]='0'; ///// A NUMERADOR
                                            $matriz_r[6][$j]='0'; ///// B DENOMINADOR
                                            $matriz_r[7][$j]='0'; ///// EJECUTADO
                                            $matriz_r[8][$j]='0'; ///// ACUMULADO EJECUTADO
                                            $matriz_r[9][$j]='0'; ///// % ACUMULADO EJECUTADO
                                            $matriz_r[10][$j]='0'; //// EFICACIA
                                          }
                                    /*--------------------------------------------------------*/
                                    /*--------------------ejecutado gestion ------------------*/
                                          $nro_e=0;
                                          foreach($ejecutado as $row)
                                          {
                                              $nro_e++;
                                              $matriz_e [1][$nro_e]=$row['m_id'];
                                              $matriz_e [2][$nro_e]=$row['pejec_fis'];
                                              $matriz_e [3][$nro_e]=$row['pejec_fis_a'];
                                              $matriz_e [4][$nro_e]=$row['pejec_fis_b'];
                                          }
                                    /*--------------------------------------------------------*/
                                        /*------- asignando en la matriz P, PA, %PA ----------*/
                                          for($i = 1 ;$i<=$nro ;$i++)
                                          {
                                            for($j = 1 ;$j<=12 ;$j++)
                                            {
                                              if($matriz[1][$i]==$matriz_r[1][$j])
                                              {
                                                $matriz_r[2][$j]=round($matriz[2][$i],1);
                                              }
                                            }
                                          }

                                          $pa=0;        
                                          for($j = 1 ;$j<=12 ;$j++){
                                            $pa=$pa+$matriz_r[2][$j];
                                            $matriz_r[3][$j]=$pa+$rowp['prod_linea_base'];
                                            if($rowp['prod_meta']!=0)
                                            {
                                                $matriz_r[4][$j]=round(((($pa+$rowp['prod_linea_base'])/$rowp['prod_meta'])*100),1);
                                            }
                                            
                                          }

                                          if($rowp['indi_id']==1)
                                          {
                                            for($i = 1 ;$i<=$nro_e ;$i++){
                                                for($j = 1 ;$j<=12 ;$j++)
                                                {
                                                    if($matriz_e[1][$i]==$matriz_r[1][$j])
                                                    {
                                                        $matriz_r[7][$j]=round($matriz_e[2][$i],1);
                                                    }
                                                }
                                            }
                                          }
                                          elseif ($rowp['indi_id']==2) 
                                          {
                                            for($i = 1 ;$i<=$nro_e ;$i++){
                                                  for($j = 1 ;$j<=12 ;$j++)
                                                  {
                                                      if($matriz_e[1][$i]==$matriz_r[1][$j])
                                                      {
                                                          $matriz_r[5][$j]=round($matriz_e[3][$i],2);
                                                          $matriz_r[6][$j]=round($matriz_e[4][$i],2);
                                                          $matriz_r[7][$j]=round($matriz_e[2][$i],2);
                                                      }
                                                  }
                                              }
                                          /*--------------------------------------------------------*/
                                          }
                                          /*--------------------matriz E,AE,%AE gestion ------------------*/
                                          $pe=0; 
                                          for($j = 1 ;$j<=12 ;$j++){
                                            $pe=$pe+$matriz_r[7][$j];
                                            $matriz_r[8][$j]=$pe+$rowp['prod_linea_base'];
                                            if($rowp['prod_meta']!=0)
                                                $matriz_r[9][$j]=round(((($pe+$rowp['prod_linea_base'])/$rowp['prod_meta'])*100),1);
                                            
                                            if($matriz_r[4][$j]!=0)
                                                $matriz_r[10][$j]=round((($matriz_r[9][$j]/$matriz_r[4][$j])*100),1);
                                            }
                                /*----------------------------------------------------------------------------------*/
                            $html .= '<td>';
                                $html .= '<table>';
                                $html .= '<tr>';
                                $html .= '<td style="background-color:#ac9fae;">PROG.</td>';
                                $html .= '<td>'.$matriz_r[2][$mes_id].'</td>';
                                $html .= '</tr>';
                                if($rowp['indi_id']==1)
                                {
                                    $html .= '<tr>';
                                    $html .= '<td style="background-color:#ac9fae;">EJEC.</td>';
                                    $html .= '<td>'.$matriz_r[7][$mes_id].'</td>';
                                    $html .= '</tr>';
                                }
                                if($rowp['indi_id']==2)
                                {
                                    $html .= '<tr>';
                                    $html .= '<td style="background-color:#ac9fae;">EJEC.</td>';
                                    $html .= '<td>'.$matriz_r[5][$mes_id].'</td>';
                                    $html .= '</tr>';

                                    $html .= '<tr>';
                                    $html .= '<td style="background-color:#ac9fae;">EJEC.</td>';
                                    $html .= '<td>'.$matriz_r[6][$mes_id].'</td>';
                                    $html .= '</tr>';  
                                }
                               
                                $html .= '</table>';
                            $html .= '</td>';
                        $html .= '</tr>';

                        $act=$this->model_actividad->list_act_anual($rowp['prod_id']);
                        $nroa=1;
                        foreach($act as $rowa)
                        {
                            $html .= '<tr class="collapse_t" bgcolor="#b0f1ef">';
                            $html .= '<td>'.$nroa.'</td>';
                            $html .= '<td></td>';
                            $html .= '<td>'.$rowa['act_actividad'].'</td>';
                            $html .= '<td>'.number_format($rowa['act_costo_uni'], 2, ',', '.').'Bs.</td>';
                            $html .= '<td>'.$rowa['indi_abreviacion'].'</td>';
                            $html .= '<td>'.$rowa['act_indicador'].'</td>';
                            $html .= '<td>'.$rowa['act_linea_base'].'</td>';
                            $html .= '<td>'.$rowa['act_meta'].'</td>';
                            $html .= '<td>'.$rowa['act_ponderacion'].' %</td>';
                            $html .= '<td>'.$rowa['act_fuente_verificacion'].'</td>';
                            /*------------------------- PROGRAMACION - EJECUCION ACTIVIDAD -------------------------*/
                            $programado=$this->model_actividad->act_prog_mensual($rowa['act_id'],$this->session->userdata("gestion")); /// programado
                            $ejecutado=$this->model_actividad->act_ejec_mensual($rowa['act_id'],$this->session->userdata("gestion")); /// ejecutado
                            $nro=0;
                            foreach($programado as $row)
                            {
                              $nro++;
                              $matriz [1][$nro]=$row['m_id'];
                              $matriz [2][$nro]=$row['pg_fis'];
                            }

                            /*---------------- llenando la matriz vacia --------------*/
                            for($j = 1; $j<=12; $j++)
                            {
                              $matriz_r[1][$j]=$j;
                              $matriz_r[2][$j]='0';
                              $matriz_r[3][$j]='0';
                              $matriz_r[4][$j]='0';
                              $matriz_r[5][$j]='0';
                              $matriz_r[6][$j]='0';
                              $matriz_r[7][$j]='0';
                              $matriz_r[8][$j]='0';
                              $matriz_r[9][$j]='0';
                              $matriz_r[10][$j]='0';
                            }
                            /*--------------------------------------------------------*/

                            /*--------------------ejecutado gestion ------------------*/
                            $nro_e=0;
                            foreach($ejecutado as $row)
                            {
                              $nro_e++;
                              $matriz_e [1][$nro_e]=$row['m_id'];
                              $matriz_e [2][$nro_e]=$row['ejec_fis'];
                              $matriz_e [3][$nro_e]=$row['ejec_fis_a'];
                              $matriz_e [4][$nro_e]=$row['ejec_fis_b'];
                            }
                            /*--------------------------------------------------------*/
                            /*------- asignando en la matriz P, PA, %PA ----------*/
                            for($i = 1 ;$i<=$nro ;$i++)
                            {
                              for($j = 1 ;$j<=12 ;$j++)
                              {
                                if($matriz[1][$i]==$matriz_r[1][$j])
                                {
                                    $matriz_r[2][$j]=round($matriz[2][$i],1);
                                }
                              }
                            }
                            $pa=0;
                            for($j = 1 ;$j<=12 ;$j++){
                              $pa=$pa+$matriz_r[2][$j];
                              $matriz_r[3][$j]=$pa+$rowa['act_linea_base'];
                              if($rowa['act_meta']!=0)
                                $matriz_r[4][$j]=round(((($pa+$rowa['act_linea_base'])/$rowa['act_meta'])*100),1);  
                            }

                            if($rowa['indi_id']==1)
                            {
                              for($i = 1 ;$i<=$nro_e ;$i++){
                                for($j = 1 ;$j<=12 ;$j++)
                                {
                                  if($matriz_e[1][$i]==$matriz_r[1][$j])
                                  {
                                      $matriz_r[7][$j]=round($matriz_e[2][$i],1);
                                  }
                                }
                              }
                            }
                            elseif ($rowa['indi_id']==2) 
                            {
                              for($i = 1 ;$i<=$nro_e ;$i++){
                                  for($j = 1 ;$j<=12 ;$j++)
                                  {
                                    if($matriz_e[1][$i]==$matriz_r[1][$j])
                                      {
                                          $matriz_r[5][$j]=round($matriz_e[3][$i],1);
                                          $matriz_r[6][$j]=round($matriz_e[4][$i],1);
                                          $matriz_r[7][$j]=round($matriz_e[2][$i],1);
                                      }
                                  }
                                }
                              /*--------------------------------------------------------*/
                              }
                              /*--------------------matriz E,AE,%AE gestion ------------------*/
                                $pe=0;
                                for($j = 1 ;$j<=12 ;$j++){
                                  $pe=$pe+$matriz_r[7][$j];
                                  $matriz_r[8][$j]=$pe+$rowa['act_linea_base'];
                                  if($rowa['act_meta']!=0)
                                    $matriz_r[9][$j]=round(((($pe+$rowa['act_linea_base'])/$rowa['act_meta'])*100),1);  
                                  if($matriz_r[4][$j]!=0)
                                        $matriz_r[10][$j]=round((($matriz_r[9][$j]/$matriz_r[4][$j])*100),1);
                                }
                            /*---------------------------------------------------------------------------------------*/
                            $html .= '<td>';
                                $html .= '<table>';
                                $html .= '<tr>';
                                $html .= '<td style="background-color:#ac9fae;">PROG.</td>';
                                $html .= '<td>'.$matriz_r[2][$mes_id].'</td>';
                                $html .= '</tr>';
                                if($rowp['indi_id']==1)
                                {
                                    $html .= '<tr>';
                                    $html .= '<td style="background-color:#ac9fae;">EJEC.</td>';
                                    $html .= '<td>'.$matriz_r[7][$mes_id].'</td>';
                                    $html .= '</tr>';
                                }
                                if($rowp['indi_id']==2)
                                {
                                    $html .= '<tr>';
                                    $html .= '<td style="background-color:#ac9fae;">EJEC.</td>';
                                    $html .= '<td>'.$matriz_r[5][$mes_id].'</td>';
                                    $html .= '</tr>';

                                    $html .= '<tr>';
                                    $html .= '<td style="background-color:#ac9fae;">EJEC.</td>';
                                    $html .= '<td>'.$matriz_r[6][$mes_id].'</td>';
                                    $html .= '</tr>';  
                                }
                               
                                $html .= '</table>';
                            $html .= '</td>';
                            $html .= '</tr>';

                            $nroa++;
                        }
                        $nrop++;
                    }        
                $html .= '</table></div><br>';

                $html.='<div class="contenedor_datos">
                            <table class="table_contenedor" border=1>
                                <tr class="collapse_t">
                                    <td style="width:25%;" class="fila_unitaria titulo_dictamen">
                                        <b> OBSERVACI&Oacute;N</b>
                                    </td>
                                    <td style="width:75%;">';
                                        $observaciones = $this->model_ejecucion->observaciones_fase($fase_id,$mes_id,$com_id);
                                        if(count($observaciones)!=0)
                                        {
                                            $html.='<table class="table_contenedor">
                                            <tr class="collapse_t" style="background-color:#ac9fae;" >
                                                <td>Nro.</td>
                                                <td>FECHA INICIO</td>
                                                <td>FECHA FINAL</td>
                                                <td>DESCRIPCI&Oacute;N OBSERVACI&Oacute;N</td>
                                            </tr>';
                                            $nro_obs=1;
                                            foreach($observaciones  as $row)
                                            {
                                                $html.='<tr>';
                                                    $html.='<td>'.$nro_obs.'</td>';
                                                    $html.='<td>'.date('d/m/Y',strtotime($row['fecha_inicio'])).'</td>';
                                                    $html.='<td>'.date('d/m/Y',strtotime($row['fecha_final'])).'</td>';
                                                    $html.='<td>'.$row['observacion'].'</td>';
                                                $html.='</tr>';
                                                $nro_obs++;
                                            }
                                            $html.='</table>';
                                        }
                                        else
                                        {
                                            $html.='Sin Observaciones Registrados';
                                        }

                                    $html.='</td>

                                </tr>
                            </table>
                        </div><br>
                        <div class="contenedor_datos">
                            <table class="table_contenedor">
                                <tr class="collapse_t">
                                    <td style="width:18%" class="fila_unitaria">PROBLEMAS</td>
                                    <td class="fila_unitaria">'.$fase_ejec[0]['ejec_prob'].'</td>
                                </tr>

                                <tr class="collapse_t">
                                    <td class="fila_unitaria">SOLUCIONES</td>
                                    <td class="fila_unitaria">'.$fase_ejec[0]['ejec_sol'].'</td>
                                </tr>
                            </table>
                        </div>';

                $html .= '
                <table style="width: 95%;margin: 0 auto;margin-top:30px;margin-bottom: 10px;">
                    <tr>
                        <td style="width:3%;margin-bottom: 5px;">FIRMAS:</td>
                        <td style="width:30%">
                            <hr>
                        </td>
                        <td style="width:3%"></td>
                        <td style="width:30%">
                            <hr>
                        </td>
                        <td style="width:3%"></td>
                        <td style="width:30%">
                            <hr>
                        </td>
                    </tr>
                </table>
            </body>
        </html>';
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->set_paper('letter', 'landscape');
        $dompdf->render();
        $dompdf->stream("reporte_ejecucion_proyecto.pdf", array("Attachment" => false));
    }

    /*========================================== PROYECTOS DE INVERSION ==========================================*/
    public function ejec_presupuesto($proy_id,$pr_id){
      $enlaces=$this->menu_modelo->get_Modulos(3);
      $data['menu']=$this->menu(3); //// genera menu
      $data['proyecto'] = $this->model_proyecto->get_id_proyecto($proy_id); ////// datos generales del proyecto con id_p x
      $data['titulo_proy']=strtoupper($data['proyecto'][0]['tipo']);
      $data['id_f'] = $this->model_faseetapa->get_id_fase($proy_id); //// recupera datos de la tabla fase activa
      $data['ppartida'] = $this->mfinanciera->get_partida_programado($pr_id,$this->session->userdata("gestion")); //// Datos de la partida programada
      $m[1]='enero';
      $m[2]='febrero';
      $m[3]='marzo';
      $m[4]='abril';
      $m[5]='mayo';
      $m[6]='junio';
      $m[7]='julio';
      $m[8]='agosto';
      $m[9]='septiembre';
      $m[10]='octubre';
      $m[11]='noviembre';
      $m[12]='diciembre';

      $epartida= $this->mfinanciera->get_partida_proy_ejecutado($pr_id,$this->session->userdata("gestion"));
      if(count($epartida)!=0){
        for ($i=1; $i <=12 ; $i++) { 
          $pe[$i]=$epartida[0][$m[$i]];
        }
      }
      else{
        for ($i=1; $i <=12 ; $i++) { 
          $pe[$i]=0;
        }
      }

      $data['ejec']=$pe;

      $this->load->view('admin/ejecucion/lista_operaciones/formularios/ejec_presupuesto', $data);

    }

    public function add_ejecucion(){
        if ($this->input->server('REQUEST_METHOD') === 'POST'){
            $this->form_validation->set_rules('proy_id', 'id de proyecto', 'required|trim');
            $this->form_validation->set_rules('fase_id', 'id de Fase', 'required|trim');
            $this->form_validation->set_rules('pr_id', 'id programado', 'required|trim');
            //$this->form_validation->set_rules('file1', 'Seleccione Archivo', 'required|trim');
              $m[1]='m1';
              $m[2]='m2';
              $m[3]='m3';
              $m[4]='m4';
              $m[5]='m5';
              $m[6]='m6';
              $m[7]='m7';
              $m[8]='m8';
              $m[9]='m9';
              $m[10]='m10';
              $m[11]='m11';
              $m[12]='m12';

              $mes[1]='enero';
              $mes[2]='febrero';
              $mes[3]='marzo';
              $mes[4]='abril';
              $mes[5]='mayo';
              $mes[6]='junio';
              $mes[7]='julio';
              $mes[8]='agosto';
              $mes[9]='septiembre';
              $mes[10]='octubre';
              $mes[11]='noviembre';
              $mes[12]='diciembre';
          if($this->form_validation->run()){
            $epartida= $this->mfinanciera->get_partida_proy_ejecutado($this->input->post('pr_id'),$this->session->userdata("gestion"));
            for ($i=1; $i <=12 ; $i++) { 
              if($this->input->post($m[$i])!=0 & $this->input->post($m[$i])!=''){ //$this->input->post($m[$i])!=0 &
                
               // echo "Mes ".$i." - ".$this->input->post($m[$i])." - ".$epartida[0][$mes[$i]]."<br>";
	              $monto=$this->input->post($m[$i])-$epartida[0][$mes[$i]];
		            if($this->input->post($m[$i])!=$epartida[0][$mes[$i]]){
			            $data_to_store = array(
				            'proy_id' => $this->input->post('proy_id'),
				            'par_id' => $this->input->post('par_id'),
				            'ff_id' => $this->input->post('ff_id'),
				            'of_id' => $this->input->post('of_id'),
				            'pem_devengado' => $monto,
				            'mes_id' => $i,
				            'gestion' => $this->session->userdata("gestion"),
				            'fun_id' => $this->session->userdata("fun_id"),
				            'pem_ppto_inicial' =>0,
				            'pem_ppto_vigente' =>0,
				            'pem_modif_aprobadas' =>0,
			            );
			            $this->db->insert('proy_ejec_mes', $data_to_store);
		            }
              }
            }
            $this->session->set_flashdata('success','SE GUARDO CORRECTAMENTE EL REGISTRO DE LA EJECUCIÓN');
            redirect('admin/ejec/mis_operaciones');
          }
          else{
            $this->session->set_flashdata('danger','Error al Guardar la Ejecucion');
            redirect('admin/ejec/mis_operaciones');
          }
          }
        else{
          $this->session->set_flashdata('danger','Error al Guardar');
          redirect('admin/ejec/mis_operaciones');
        }
    }
    function rolfun($rol)
    {
      $valor=false;
      for ($i=1; $i <=count($rol) ; $i++) { 
        $data = $this->Users_model->get_datos_usuario_roles($this->session->userdata('fun_id'),$rol[$i]);
        if(count($data)!=0){
          $valor=true;
          break;
        }
      }
      return $valor;
    }

    /*-------------------------------- GENERAR MENU -------------------------------------*/
    function menu($mod){
      $enlaces=$this->menu_modelo->get_Modulos($mod);
      for($i=0;$i<count($enlaces);$i++) {
        $subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
      }

      $tabla ='';
      for($i=0;$i<count($enlaces);$i++){
          if(count($subenlaces[$enlaces[$i]['o_child']])>0){
              $tabla .='<li>';
                  $tabla .='<a href="#">';
                      $tabla .='<i class="'.$enlaces[$i]['o_image'].'"></i> <span class="menu-item-parent">'.$enlaces[$i]['o_titulo'].'</span></a>';    
                      $tabla .='<ul>';    
                          foreach ($subenlaces[$enlaces[$i]['o_child']] as $item) {
                          $tabla .='<li><a href="'.base_url($item['o_url']).'">'.$item['o_titulo'].'</a></li>';
                      }
                      $tabla .='</ul>';
              $tabla .='</li>';
          }
      }
      return $tabla;
    }
    /*
	###############################################################################################################################################
	###############################################################################################################################################
	
	METODOS PARA EL REPORTE SGP EN PDF
	
	###############################################################################################################################################
	###############################################################################################################################################
	*/
	/*------------------------FUNCIONES PARA GENERARR SGP DIGITAL------------------------*/
	public function reporte($proy_id)
	{
		if($this->session->userdata('logged')){

			$data['gestion'] = $this->session->userdata('gestion');
			$data['mes_id'] = $this->session->userdata('mes');

			$resp_dictamen = $this->model_objetivo->get_dictamen2($proy_id, $this->session->userdata('gestion'));
			$data['dictamen'] = $resp_dictamen->row_array();
			$data['unidad_responsable']=$this->model_proyecto->responsable_proy($proy_id,2); //// unidad responsable
			$data['unidad_ejecutora']=$this->model_proyecto->responsable_proy($proy_id,1);  //// unidad ejecutora
			$localizacion = $this->model_objetivo->get_localizacion_dictamen($proy_id);
			$data['localizacion'] = $localizacion->result_array();
			$res_metas = $this->model_objetivo->get_indicadores_metas($proy_id);
			$data['metas'] = $res_metas->result_array();
			$res_componentes = $this->model_objetivo->get_componentes_dictamen($proy_id);
			$data['componentes'] = $res_componentes->result_array();


			$fase=$this->model_faseetapa->fase_etapa($data['dictamen']['pfec_id'],$proy_id);
			$data['presupuesto_total'] = number_format($fase[0]['pfec_ptto_fase'], 2, ',', '.');
			$presupuesto_total = $fase[0]['pfec_ptto_fase'];

			$enlaces=$this->menu_modelo->get_Modulos(7);
			$data['enlaces'] = $enlaces;

			$mod = 4;
			$id_p = $proy_id;
			$id_f = $fase[0]['id'];// ID DE LA FASE ACTIVA DEL PROYECTO / PROGRAMA

			$data['datos_generales'] = $this->datos_generales($resp_dictamen->row_array(), $this->model_proyecto->responsable_proy($proy_id,1), $localizacion->result_array(), $res_metas->result_array(), $res_componentes->result_array());
			$data['pe_fisica']=$this->programacion_fisica_pluri($mod,$id_f,$id_p); //programacion ejecucion fisica
			$data['pe_financiera']=$this->prog_financiera($mod,$id_f,$id_p);//programacion ejecucion financiera
			return $data;
		}
		else{
			return 'ERROR';
		}
	}

	public function datos_generales($dictamen,$unidad_ejecutora, $localizacion, $metas, $componentes){
		$tabla = '';
		$tabla .= '<html>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <head>'.$this->estilo_vertical.$this->session->userdata('franja_decorativa').'</head>
            <body>
                <header>
                </header>
                <div class="rojo"></div>
                <div class="verde"></div>
                   <table width="100%">
                    <tr>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/logo.jpg" class="siipp"/>
                        </td>
                        <td width=60%; class="titulo_pdf">
                            <b>ENTIDAD : </b>'.$this->session->userdata('entidad').'<br>
                            <b>'.$this->session->userdata('sistema').'</b><br>
                        </td>
                        <td width=20%;>
                            <img src="'.base_url().'assets/img/escudo.jpg" alt="" width="90px">
                        </td>
                    </tr>
                </table>
                <footer>
                    <table width="100%">
                        <tr>
                            <td><p class="izq">'.$this->session->userdata('sistema_pie').'</p></td>
                            <td><p class="page">P&aacute;gina </p></td>
                        </tr>
                    </table>
                </footer>
				<h2 class="titulo_s">
				REPORTE SGPP
				</h2>
				<h2 class="titulo"><b>1. IDENTIFICACIÓN DEL PROYECTO - PROGRAMA</b></h2>
				<div class="row">
				<div class="col-md-2 text-primary text-right"><b>Nombre de la operación:</b></div>
				<div class="col-md-5 border1">'.$dictamen['proy_nombre'].'</div>
				<div class="col-md-2 text-primary text-right"><b>Código SISIN:</b></div>
				<div class="col-md-3 border1" style="font-size:1.4em; font-weight:bold;">';

		if($dictamen['proy_sisin'] == ''){
			$tabla.= '-';
		}else{
			$tabla.= ''.$dictamen['proy_sisin'];
		}

		$tabla.= '</div>
				</div>
				<br>
				<div class="row">
				<div class="col-md-2 text-primary text-right"><b>Funcionario responsable:</b></div>
				<div class="col-md-5 border1">'.$unidad_ejecutora[0]['fun_nombre'].' '.$unidad_ejecutora[0]['fun_paterno'].' '.$unidad_ejecutora[0]['fun_materno'].'</div>
				<div class="col-md-2 text-primary text-right"><b>Cargo:</b></div>
				<div class="col-md-3 border1">'.$unidad_ejecutora[0]['fun_cargo'].'</div>
				</div>
				<h4 class="titulo"><b>CLASIFICACIÓN SECTORIAL</b></h4>
											<div class="row">
												<div class="col-md-2 text-primary text-right"><b>Código Sectorial:</b></div>
												<div class="col-md-2 border1" style="font-size:1.4em; font-weight:bold;">'.$dictamen['codsectorial'].'</div>
												<div class="col-md-8">
													<div class="row">
														<div class="col-md-4 text-primary text-right"><b>Sector:</b></div>
														<div class="col-md-8 border1">'.explode("-",$dictamen['codsectorial'])[0].'.'.$dictamen['c_sect_sector'].'</div>
													</div>
													<div class="row">
														<div class="col-md-4 text-primary text-right"><b>Subsector:</b></div>
														<div class="col-md-8 border1">'.explode("-",$dictamen['codsectorial'])[1].'.'.$dictamen['c_sect_subsector'].'</div>
													</div>
													<div class="row">
														<div class="col-md-4 text-primary text-right"><b>Actividad Económica:</b></div>
														<div class="col-md-8 border1">'.explode("-",$dictamen['codsectorial'])[2].'.'.$dictamen['c_sect_actividad'].'</div>
													</div>
												</div>
											</div>
											<h4 class="titulo"><b>LOCALIZACIÓN</b></h4>
											<div class="row">
												<div class="col-md-3 text-center text-primary">
													<b>Departamento:</b>
												</div>
												<div class="col-md-3 text-center text-primary">
													<b>Provincia:</b>
												</div>
												<div class="col-md-3 text-center text-primary">
													<b>Municipio:</b>
												</div>
												<div class="col-md-3 text-center text-primary">
													<b>Comunidad / Distritos / Cantones:</b>
												</div>
											</div>';
		foreach($localizacion as $item){
			$tabla.= '<div class="row">
												<div class="col-md-3 text-center">
														<div class="border1">'.$item['dep_departamento'].'</div>
												</div>
												<div class="col-md-3 text-center">
														<div class="border1">'.$item['prov_provincia'].'</div>
												</div>
												<div class="col-md-3 text-center">
														<div class="border1">'.$item['muni_municipio'].'</div>
												</div>
												<div class="col-md-3 text-center">
														<div class="border1">'.$item['cantones'].'</div>
												</div>
											</div>';
		}

		$tabla.='<h4 class="titulo"><b>DESCRIPCIÓN DEL PROYECTO - PROGRAMA</b></h4>
											<div class="row">
												<div class="col-md-12 border1">
												'.$dictamen['proy_descripcion_proyecto'].'
												</div>
											</div>
											<br>
											<div class="row">
												<div class="col-md-2 text-primary text-right"><b>FASE A EJECUTAR</b></div>
												<div class="col-md-4 text-primary text-center"><b>COSTO DE LA FASE A EJECUTAR</b></div>
												<div class="col-md-6 text-primary text-center" style="text-transform:uppercase;"><b>DURACIÓN DE LA FASE DE ';
		if($dictamen['fas_fase'] == 'Ninguno'){
			$tabla.= 'Inversión';
		}else{
			$tabla.= ''.$dictamen['fas_fase'];
		}
		$tabla.='</b></div>
											</div>
											<br>
											<div class="row">
												<div class="col-md-2 text-primary text-right"><b>';
		if($dictamen['fas_fase'] == 'Ninguno'){
			$tabla.= 'Inversión';
		}else{
			$tabla.= $dictamen['fas_fase'];
		}
		$presupuesto_total='';
		$tabla.=':</b></div>
												<div class="col-md-4 border1">'.$presupuesto_total.'</div>
												<div class="col-md-2 text-primary text-right"><b>Fecha de inicio:</b></div>
												<div class="col-md-4 border1">'.$dictamen['fecha_inicial'].'</div>
											</div>
											<br>
											<div class="row">
												<div class="col-md-2 col-md-offset-6 text-primary text-right"><b>Fecha de término:</b></div>
												<div class="col-md-4 border1">'.$dictamen['fecha_final'].'</div>
											</div>
											<br>
											<div class="row">
												<div class="col-md-6">
													<div class="text-primary text-center"><b>Metas del Proyecto - Programa:</b></div>
													<table class="table table-borderded table-condensed tabla-metas">
														<tr>
															<th class="text-center">Unidad</th>
															<th class="text-center">Cantidad</th>
														</tr>';
		foreach($metas as $item){
			$tabla.='<tr>
															<td>'.$item['meta_descripcion'].'</td>
															<td>'.$item['meta_meta'].'</td>
														</tr>';
		}
		$tabla.='</table>
												</div>
												<div class="col-md-6">
													<div class="text-primary text-center"><b>Indicadores de evaluación:</b></div>
													<table class="table table-borderded table-condensed tabla-indicadores">
														<tr>
															<th class="text-center">Unidad</th>
															<th class="text-center">Valor</th>
														</tr>
														<tr>
															<td>VANP</td>
															<td>-</td>
														</tr>
														<tr>
															<td>VANS</td>
															<td>- </td>
														</tr>
														<tr>
															<td>CAE</td>
															<td>- </td>
														</tr>
													</table>
												</div>
											</div>
											<br>
											<div class="row">
												<div class="col-md-10 col-md-offset-1">
													<div class="text-primary text-center"><b>Determinación de los componentes de la operación y su modalidad de ejecución:</b></div>
													<table class="table table-borderded table-condensed tabla-modalidad">
														<tr>
															<th width="60%" class="text-center">Componentes</th>
															<th class="text-center">Modalidad de Ejecución <br><span style="font-size:0.8em;">(Administración Propia / Contratación de terceros)</span></th>
														</tr>';
		foreach($componentes as $item){
			$tabla.='<tr>
															<td>'.$item['com_componente'].'</td>
															<td>';
			if($item['com_modalidad'] == ''){
				$tabla.= 'No especificado';
			}else{
				$tabla.=''.$item['com_modalidad'];
			}
			$tabla.='</td>
														</tr>';
		}

		$tabla.='</table>
												</div>
											</div>
		
		';
		return $tabla;
	}

	public function fisico($proy_id){
		/*		if($proy_id == NULL){
					redirect('error_404');
				}*///
		//	$proy_id = 617; // ixiamas

//		$proy_id = 860; //
		//	$id_f = 751;

		$mod = 4;
		$id_p = $proy_id;
		if($this->session->userdata('logged')){

			$data['gestion'] = $this->session->userdata('gestion');
			$data['mes_id'] = $this->session->userdata('mes');

			$resp_dictamen = $this->model_objetivo->get_dictamen2($proy_id, $this->session->userdata('gestion'));
			$data['dictamen'] = $resp_dictamen->row_array();
			$data['unidad_responsable']=$this->model_proyecto->responsable_proy($proy_id,2); //// unidad responsable
			$data['unidad_ejecutora']=$this->model_proyecto->responsable_proy($proy_id,1);  //// unidad ejecutora

			$fase=$this->model_faseetapa->fase_etapa($data['dictamen']['pfec_id'],$proy_id);
			$data['presupuesto_total'] = number_format($fase[0]['pfec_ptto_fase'], 2, ',', '.');

			$id_f = $fase[0]['id'];// ID DE LA FASE ACTIVA DEL PROYECTO / PROGRAMA

			$enlaces=$this->menu_modelo->get_Modulos(7);
			$data['enlaces'] = $enlaces;

			$data['pe_fisica']=$this->programacion_fisica_pluri($mod,$id_f,$id_p); //programacion ejecucion fisica
			$data['pe_financiera']=$this->prog_financiera($mod,$id_f,$id_p);//programacion ejecucion financiera

			for($i=0;$i<count($enlaces);$i++)
			{
				$subenlaces[$enlaces[$i]['o_child']]=$this->menu_modelo->get_Enlaces($enlaces[$i]['o_child'], $this->session->userdata('user_name'));
			}
			$data['subenlaces'] = $subenlaces;
			$this->load->view('reportes/sgp/sgp_fisico', $data);
		}
		else{
			redirect('admin/dashboard');
		}
	}

	public function formatea_fecha($fecha){
		$dia = substr($fecha,8,2);
		$nmes = intval(substr($fecha,5,2));
		$anio = substr($fecha,0,4);
		$mes = '';
		switch ($nmes) {
			case 1:	$mes = 'Ene'; break;
			case 2:	$mes = 'Feb'; break;
			case 3:	$mes = 'Mar'; break;
			case 4:	$mes = 'Abr'; break;
			case 5:	$mes = 'May'; break;
			case 6:	$mes = 'Jun'; break;
			case 7:	$mes = 'Jul'; break;
			case 8:	$mes = 'Ago'; break;
			case 9:	$mes = 'Sep'; break;
			case 10:$mes = 'Oct'; break;
			case 11:$mes = 'Nov'; break;
			case 12:$mes = 'Dic'; break;
		}
		return $dia.' '.$mes.' '.$anio;
	}

	public function programacion_fisica_pluri($mod,$id_f,$proy_id){
		$gestion = $this->session->userdata('gestion');

		$proyecto = $this->model_proyecto->get_id_proyecto($proy_id);
		$fase = $this->model_faseetapa->get_id_fase($proy_id);
		$años=$fase[0]['pfec_fecha_fin']-$fase[0]['pfec_fecha_inicio']+1;
		$meses=$años*12;
		for($p=1;$p<=$meses;$p++){$app[$p]=0; $aee[$p]=0;} //// vector vacio para sumar a nivel de actividades programacion , ejecutado

		$tabla ='<h2 class="titulo"><b>2. PROGRAMACIÓN - EJECUCIÓN FÍSICA</b></h2>';
		for($p=1;$p<=$meses;$p++){$cpp[$p]=0; $cpe[$p]=0;} //// vector vacio para sumar a nivel de productos programacion , ejecutado
		for($p=1;$p<=$meses;$p++){$ppp[$p]=0; $ppe[$p]=0;} //// vector vacio para sumar a nivel de productos programacion , ejecutado
		for($p=1;$p<=$meses;$p++){$app[$p]=0; $aee[$p]=0;} //// vector vacio para sumar a nivel de actividades programacion , ejecutado
		$nro_c = $this->model_componente->componentes_nro($id_f); //// nro de componente
		if ($nro_c>0){
			for($p=1;$p<=$meses;$p++){$cpp[$p]=0; $cpe[$p]=0;} //// vector vacio para sumar a nivel de productos programacion , ejecutado
			$componentes = $this->model_componente->componentes_id($id_f);
			$nro_c=1;

			foreach ($componentes as $rowc){
				$pres_comp = 0;
				$tabla .='
                        <table class="table table-condensed change_order_items" border="1" style="width:90%;">
                        <thead>
                            <tr class="header_table" align=left>
                              <th class="header_table" colspan=12>COMPONENTE: <b>'.$rowc['com_componente'].'</b></th>
                            </tr>
                            <tr class="head_tabla">
                              <th><center>Nº</center></th>
                              <th><center>PRODUCTO</center></th>
                              <th><center>ACTIVIDAD</center></th>
                              <th><center>INDICADOR</center></th>
                              <th><center>LINEA BASE</center></th>
                              <th><center>META</center></th>
                              <th><center>PONDERACI&Oacute;N</center></th>
                              <th><center>Fecha Inicio</center></th>
                              <th><center>Fecha Final</center></th>
                              <th><center>DURACIÓN (d.c.)</center></th>
                              <th><center>COSTO (Bs)</center></th>
                              <th><center>PROGRAMACION / EJECUCIÓN</center></th>
                            </tr>
                        </thead>
                      <tbody>';
				$nrop=0;
				$productos = $this->model_producto->list_prod($rowc['com_id']);
				for($p=1;$p<=$meses;$p++){$ppp[$p]=0; $ppe[$p]=0;} //// vector vacio para sumar a nivel de productos programacion , ejecutado
				/*--------------------------- Producto --------------------------*/
				foreach ($productos as $rowp){
					$nrop++;
					$tabla.='
                            <tr class="modo1 head_producto" align=left>
							  <td>'.$nrop.'</td>
                              <td style="text-transform:uppercase;">'.$rowp['prod_producto'].'</td>
							  <td>-</td>
							  <td><center>'.$rowp['prod_indicador'].'</center></td>
							  <td><center>'.$rowp['prod_linea_base'].'</center></td>
							  <td><center>'.$rowp['prod_meta'].'</center></td>
							  <td><center>'.$rowp['prod_ponderacion'].'</center></td>
							  <td>-</td>
							  <td>-</td>
							  <td>-</td>
							  <td>-</td>
							  <td>-</td>
                            </tr>';
					/*--------------------------- Actividad --------------------------*/
					for($p=1;$p<=$meses;$p++){$app[$p]=0; $aee[$p]=0;} //// vector vacio para sumar a nivel de actividades programacion , ejecutado
					$nroa=0;
					$actividad = $this->model_actividad->list_act_anual($rowp['prod_id']);
					foreach ($actividad as $rowa){
						$nroa++;
						$ti='';if($rowa['indi_id']==2){ $ti='%';}
						$meta_gestion=$this->model_actividad->meta_act_gest($rowa['act_id']);
						$meta_gest=$meta_gestion[0]['meta_gest']+$rowa['act_linea_base'];

						$pres_comp = $pres_comp + $rowa['act_presupuesto'];

						$tabla .='<tr class="modo1 row_actividad">
                                        <td>'.$nrop.'.'.$nroa.'</td>
                                        <td>-</td>
                                        <td>'.$rowa['act_actividad'].'</td>
                                        <td><center>'.$rowa['act_indicador'].'</center></td>
                                        <td><center>'.$rowa['act_linea_base'].'</center></td>
                                        <td><center>'.$rowa['act_meta'].'</center></td>
                                        <td><center>'.$rowa['act_ponderacion'].'%</center></td>
                                        <td><center>'.$this->formatea_fecha($rowa['act_fecha_inicio']).'</center></td>
                                        <td><center>'.$this->formatea_fecha($rowa['act_fecha_final']).'</center></td>
                                        <td><center>'.$rowa['act_duracion'].'</center></td>
                                        <td class="text-right">'.$rowa['act_presupuesto'].'</td>
                                        <td style="padding:2px;">';
						$pa=0; $pe=0; $gestion=$fase[0]['pfec_fecha_inicio']; $a=1;$e=1;
						for($k=1;$k<=$años;$k++){
							$programado=$this->model_actividad->act_prog_mensual($rowa['act_id'],$gestion); /// programado
							$ejecutado=$this->model_actividad->act_ejec_mensual($rowa['act_id'],$gestion); /// ejecutado
							$nro=0;
							foreach($programado as $row){
								$nro++;
								$matriz [1][$nro]=$row['m_id'];
								$matriz [2][$nro]=$row['pg_fis'];
							}
							/*---------------- llenando la matriz vacia --------------*/
							for($j = 1; $j<=12; $j++){
								$matriz_r[1][$j]=$j;
								$matriz_r[2][$j]='0';  //// P
								$matriz_r[3][$j]='0';  //// PA
								$matriz_r[4][$j]='0';  //// %PA
								$matriz_r[5][$j]='0';  //// A
								$matriz_r[6][$j]='0';  //// B
								$matriz_r[7][$j]='0';  //// E
								$matriz_r[8][$j]='0';  //// EA
								$matriz_r[9][$j]='0';  //// %EA
								$matriz_r[10][$j]='0'; //// EFICACIA
							}
							/*--------------------------------------------------------*/
							/*--------------------ejecutado gestion ------------------*/
							$nro_e=0;
							foreach($ejecutado as $row){
								$nro_e++;
								$matriz_e [1][$nro_e]=$row['m_id'];
								$matriz_e [2][$nro_e]=$row['ejec_fis'];
								$matriz_e [3][$nro_e]=$row['ejec_fis_a'];
								$matriz_e [4][$nro_e]=$row['ejec_fis_b'];
							}
							/*--------------------------------------------------------*/
							/*------- asignando en la matriz P, PA, %PA ----------*/
							for($i = 1 ;$i<=$nro ;$i++){
								for($j = 1 ;$j<=12 ;$j++){
									if($matriz[1][$i]==$matriz_r[1][$j]){
										$matriz_r[2][$j]=round($matriz[2][$i],2);
									}
								}
							}

							for($j = 1 ;$j<=12 ;$j++){
								$pa=$pa+$matriz_r[2][$j];
								$matriz_r[3][$j]=$pa+$rowa['act_linea_base'];
								if($rowa['act_meta']!=0){
									$matriz_r[4][$j]=round(((($pa+$rowa['act_linea_base'])/$rowa['act_meta'])*100),2);
								}

							}
							/*-------------------------------------------------*/
							/*--------------- EJECUCION ----------------------------------*/
							if($rowa['indi_id']==1){
								for($i = 1 ;$i<=$nro_e ;$i++){
									for($j = 1 ;$j<=12 ;$j++){
										if($matriz_e[1][$i]==$matriz_r[1][$j]){
											$matriz_r[7][$j]=round($matriz_e[2][$i],2);
										}
									}
								}
							}
							elseif ($rowa['indi_id']==2){
								for($i = 1 ;$i<=$nro_e ;$i++){
									for($j = 1 ;$j<=12 ;$j++){
										if($matriz_e[1][$i]==$matriz_r[1][$j]){
											$matriz_r[5][$j]=round($matriz_e[3][$i],2);
											$matriz_r[6][$j]=round($matriz_e[4][$i],2);
											$matriz_r[7][$j]=round($matriz_e[2][$i],2);
										}
									}
								}
								/*--------------------------------------------------------*/
							}
							/*--------------------matriz E,AE,%AE gestion ------------------*/
							for($j = 1 ;$j<=12 ;$j++){
								$pe=$pe+$matriz_r[7][$j];
								$matriz_r[8][$j]=$pe+$rowa['act_linea_base'];
								if($rowa['act_meta']!=0){
									$matriz_r[9][$j]=round(((($pe+$rowa['act_linea_base'])/$rowa['act_meta'])*100),2);
								}

								if($matriz_r[4][$j]==0)
								{$matriz_r[10][$j]=round((($matriz_r[9][$j])*100),2);}
								else{
									$matriz_r[10][$j]=round((($matriz_r[9][$j]/$matriz_r[4][$j])*100),2);
								}
							}

							// $ap[$n]=$matriz_r[2][1]*$rowa['act_ponderacion']; $n++;
							$ap[$a]=(($matriz_r[4][1]*$rowa['act_ponderacion'])/100); $a++;
							$ae[$e]=(($matriz_r[9][1]*$rowa['act_ponderacion'])/100); $e++;

							$ap[$a]=(($matriz_r[4][2]*$rowa['act_ponderacion'])/100); $a++;
							$ae[$e]=(($matriz_r[9][2]*$rowa['act_ponderacion'])/100); $e++;

							$ap[$a]=(($matriz_r[4][3]*$rowa['act_ponderacion'])/100); $a++;
							$ae[$e]=(($matriz_r[9][3]*$rowa['act_ponderacion'])/100); $e++;

							$ap[$a]=(($matriz_r[4][4]*$rowa['act_ponderacion'])/100); $a++;
							$ae[$e]=(($matriz_r[9][4]*$rowa['act_ponderacion'])/100); $e++;

							$ap[$a]=(($matriz_r[4][5]*$rowa['act_ponderacion'])/100); $a++;
							$ae[$e]=(($matriz_r[9][5]*$rowa['act_ponderacion'])/100); $e++;

							$ap[$a]=(($matriz_r[4][6]*$rowa['act_ponderacion'])/100); $a++;
							$ae[$e]=(($matriz_r[9][6]*$rowa['act_ponderacion'])/100); $e++;

							$ap[$a]=(($matriz_r[4][7]*$rowa['act_ponderacion'])/100); $a++;
							$ae[$e]=(($matriz_r[9][7]*$rowa['act_ponderacion'])/100); $e++;

							$ap[$a]=(($matriz_r[4][8]*$rowa['act_ponderacion'])/100); $a++;
							$ae[$e]=(($matriz_r[9][8]*$rowa['act_ponderacion'])/100); $e++;

							$ap[$a]=(($matriz_r[4][9]*$rowa['act_ponderacion'])/100); $a++;
							$ae[$e]=(($matriz_r[9][9]*$rowa['act_ponderacion'])/100); $e++;

							$ap[$a]=(($matriz_r[4][10]*$rowa['act_ponderacion'])/100); $a++;
							$ae[$e]=(($matriz_r[9][10]*$rowa['act_ponderacion'])/100); $e++;

							$ap[$a]=(($matriz_r[4][11]*$rowa['act_ponderacion'])/100); $a++;
							$ae[$e]=(($matriz_r[9][11]*$rowa['act_ponderacion'])/100); $e++;

							$ap[$a]=(($matriz_r[4][12]*$rowa['act_ponderacion'])/100); $a++;
							$ae[$e]=(($matriz_r[9][12]*$rowa['act_ponderacion'])/100); $e++;

							$tabla .= ' 
                                             <table class="table table-bordered table-condensed change_order_items tabla_avance" border="1" style="margin:2px 2px 2px 2px; width:98%;">
                                                <tr class="head_tabla">
                                                    <th colspan="13">Gesti&oacute;n '.$gestion.'</th>
                                                </tr>
                                                <tr class="modo1">
                                                    <th>-</th>
                                                    <th>Ene</th>
                                                    <th>Feb</th>
                                                    <th>Mar</th>
                                                    <th>Abr</th>
                                                    <th>May</th>
                                                    <th>Jun</th>
                                                    <th>Jul</th>
                                                    <th>Ago</th>
                                                    <th>Sep</th>
                                                    <th>Oct</th>
                                                    <th>Nov</th>
                                                    <th>Dic</th>
                                                </tr>
                                                <tr class="modo1">
                                                    <td style="width:1%;">P</td>
                                                    <td >'.$matriz_r[2][1].' '.$ti.'</td>
                                                    <td >'.$matriz_r[2][2].' '.$ti.'</td>
                                                    <td >'.$matriz_r[2][3].' '.$ti.'</td>
                                                    <td >'.$matriz_r[2][4].' '.$ti.'</td>
                                                    <td >'.$matriz_r[2][5].' '.$ti.'</td>
                                                    <td >'.$matriz_r[2][6].' '.$ti.'</td>
                                                    <td >'.$matriz_r[2][7].' '.$ti.'</td>
                                                    <td >'.$matriz_r[2][8].' '.$ti.'</td>
                                                    <td >'.$matriz_r[2][9].' '.$ti.'</td>
                                                    <td >'.$matriz_r[2][10].' '.$ti.'</td>
                                                    <td >'.$matriz_r[2][11].' '.$ti.'</td>
                                                    <td >'.$matriz_r[2][12].' '.$ti.'</td>
                                                </tr>
<!--                                                <tr bgcolor="#F5F5DC" class="modo1">
                                                    <td style="width:1%;">%P.A.</td>
                                                    <td >'.$matriz_r[4][1].'%</td>
                                                    <td >'.$matriz_r[4][2].'%</td>
                                                    <td >'.$matriz_r[4][3].'%</td>
                                                    <td >'.$matriz_r[4][4].'%</td>
                                                    <td >'.$matriz_r[4][5].'%</td>
                                                    <td >'.$matriz_r[4][6].'%</td>
                                                    <td >'.$matriz_r[4][7].'%</td>
                                                    <td >'.$matriz_r[4][8].'%</td>
                                                    <td >'.$matriz_r[4][9].'%</td>
                                                    <td >'.$matriz_r[4][10].'%</td>
                                                    <td >'.$matriz_r[4][11].'%</td>
                                                    <td >'.$matriz_r[4][12].'%</td>
                                                </tr>-->

                                                <tr bgcolor="#F5F5DC" class="modo1">
                                                    <td style="width:1%;">E</td>
                                                    <td >'.$matriz_r[7][1].' '.$ti.'</td>
                                                    <td >'.$matriz_r[7][2].' '.$ti.'</td>
                                                    <td >'.$matriz_r[7][3].' '.$ti.'</td>
                                                    <td >'.$matriz_r[7][4].' '.$ti.'</td>
                                                    <td >'.$matriz_r[7][5].' '.$ti.'</td>
                                                    <td >'.$matriz_r[7][6].' '.$ti.'</td>
                                                    <td >'.$matriz_r[7][7].' '.$ti.'</td>
                                                    <td >'.$matriz_r[7][8].' '.$ti.'</td>
                                                    <td >'.$matriz_r[7][9].' '.$ti.'</td>
                                                    <td >'.$matriz_r[7][10].' '.$ti.'</td>
                                                    <td >'.$matriz_r[7][11].' '.$ti.'</td>
                                                    <td >'.$matriz_r[7][12].' '.$ti.'</td>
                                                </tr>
<!--                                                <tr bgcolor="#F5F5DC" class="modo1">
                                                    <td style="width:1%;">%E.A.</td>
                                                    <td >'.$matriz_r[9][1].'%</td>
                                                    <td >'.$matriz_r[9][2].'%</td>
                                                    <td >'.$matriz_r[9][3].'%</td>
                                                    <td >'.$matriz_r[9][4].'%</td>
                                                    <td >'.$matriz_r[9][5].'%</td>
                                                    <td >'.$matriz_r[9][6].'%</td>
                                                    <td >'.$matriz_r[9][7].'%</td>
                                                    <td >'.$matriz_r[9][8].'%</td>
                                                    <td >'.$matriz_r[9][9].'%</td>
                                                    <td >'.$matriz_r[9][10].'%</td>
                                                    <td >'.$matriz_r[9][11].'%</td>
                                                    <td >'.$matriz_r[9][12].'%</td>
                                                </tr>-->
                                            </table>';
							$gestion++;
						}

						for($k=1;$k<=$meses;$k++){
							$app[$k]=round(($app[$k]+$ap[$k]),1); //// sumando a nivel de las actividades
							$aee[$k]=round(($aee[$k]+$ae[$k]),1); //// sumando a nivel de las actividades
						}
						$tabla .= '';
						$tabla .='</td>';
						$tabla .'</tr>';
					}

					//PRESUPUESTO ANUAL
					$tabla.= '
								<tr>
									<td colspan="10" class="text-right">PRESUPUESTO TOTAL DEL COMPONENTE</td>
									<td style="font-size:14px;"><b>'.$pres_comp.'</b></td>
									<td></td>
								</tr>
								';

					for($kp=1;$kp<=$meses;$kp++){
						$ppp[$kp]=round(($ppp[$kp]+($app[$kp]*$rowp['prod_ponderacion'])/100),1); //// sumando a nivel de los productos
						$ppe[$kp]=round(($ppe[$kp]+($aee[$kp]*$rowp['prod_ponderacion'])/100),1); //// sumando a nivel de los productos
					}
					/*--------------------------- End Actividad --------------------------*/
				}
				for($k=1;$k<=$meses;$k++){
					$cpp[$k]=round(($cpp[$k]+($ppp[$k]*$rowc['com_ponderacion'])/100),0); //// sumando a nivel de los Componentes
					$cpe[$k]=round(($cpe[$k]+($ppe[$k]*$rowc['com_ponderacion'])/100),0); //// sumando a nivel de los Componentes
				}
				/*--------------------------- End Producto --------------------------*/

				$tabla .='</tbody>
                    </table>';
			}
			$i=1;$meses=12;$gestion=$fase[0]['pfec_fecha_inicio'];
			$tabla .='<br><b>EFICACIA EN EJECUCIÓN FÍSICA (ANUAL)</b>';
			for($kp=1;$kp<=$años;$kp++){
				$tabla .='
                        <table class="table table-condensed change_order_items tabla_eficacia" border="0" width="70%">
                          <thead>
                            <tr class="modo1 head_tabla" align=center bgcolor="#504c49">
                                <th colspan="13"><center>GESTI&Oacute;N '.$gestion.'</center></th>
                            </tr>
                            <tr class="modo1 " align=center bgcolor="#efecec">
                                <th width="28%">-</th>
                                <th width="6%"><center>Ene</center></th>
                                <th width="6%"><center>Feb</center></th>
                                <th width="6%"><center>Mar</center></th>
                                <th width="6%"><center>Abr</center></th>
                                <th width="6%"><center>May</center></th>
                                <th width="6%"><center>Jun</center></th>
                                <th width="6%"><center>Jul</center></th>
                                <th width="6%"><center>Ago</center></th>
                                <th width="6%"><center>Sep</center></th>
                                <th width="6%"><center>Oct</center></th>
                                <th width="6%"><center>Nov</center></th>
                                <th width="6%"><center>Dic</center></th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr class="modo1">
                                <td><b>PROGRAMACI&Oacute;N ACUMULADA EN %</b></td>';
				for($pos=$i;$pos<=$meses;$pos++){
					$tabla .= '<td class="pac text-right">'.$cpp[$pos].'</td>';
				}
				$tabla .= '
                            </tr>
                            <tr class="modo1">
                                <td><b>EJECUCI&Oacute;N ACUMULADA EN %</b></td>';
				for($pos=$i;$pos<=$meses;$pos++){
					$tabla .= '<td class="eac text-right" bgcolor="#98FB98">'.$cpe[$pos].'</td>';
				}
				$tabla .= '
                            </tr>
                            <tr class="modo1">
                                <td><b>EFICACIA POR MES EN %</b></td>';
				for($pos=$i;$pos<=$meses;$pos++){
					if($cpp[$pos]==0){$cpp[$pos]=1;}
					$tabla .= '<td style="font-weight:bold;" class="efic text-right" bgcolor="#B0E0E6">'.round((($cpe[$pos]/$cpp[$pos])*100),1).'</td>';
				}
				$tabla .= '
                            </tr>
                          </tbody>
                          </table>';
				$i=$meses+1;
				$meses=$meses+12;
				$gestion++;
			}

		}

		return $tabla;
	}

	public function suma_total_partidas($proy_id,$gestion){
		$fase = $this->model_faseetapa->get_id_fase($proy_id);
		$pprog = $this->mfinanciera->vlista_partidas_total_proy_programado($proy_id,$gestion,$fase[0]['pfec_ejecucion']);
		$eprog = $this->mfinanciera->vlista_partidas_total_sigep_proy_programado($proy_id,$gestion);

		$m[0]='total';
		$m[1]='enero';
		$m[2]='febrero';
		$m[3]='marzo';
		$m[4]='abril';
		$m[5]='mayo';
		$m[6]='junio';
		$m[7]='julio';
		$m[8]='agosto';
		$m[9]='septiembre';
		$m[10]='octubre';
		$m[11]='noviembre';
		$m[12]='diciembre';

		for($i=1;$i<=12;$i++){
			$totalp[1][$i]=0; //// i
			$totalp[2][$i]=0; //// p
			$totalp[3][$i]=0; //// pa
			$totalp[4][$i]=0; //// %pa
			$totalp[5][$i]=0; //// e
			$totalp[6][$i]=0; //// ea
			$totalp[7][$i]=0; //// %ea
			$totalp[8][$i]=0; //// eficacia
			$totalp[9][$i]=0; //// Menor
			$totalp[10][$i]=0; //// Entre
			$totalp[11][$i]=0; //// Mayor
		}

		if(count($pprog)!=0){
			$suma_pfin=0; $suma_efin=0;
			for($i=1;$i<=12;$i++){
				$totalp[1][$i]=$i; //// i
				$totalp[2][$i]=$pprog[0][$m[$i]]; /// programado
				$suma_pfin=$suma_pfin+$pprog[0][$m[$i]];
				$totalp[3][$i]=$suma_pfin; ///// Programado acumulado

				if($pprog[0][$m[0]]!=0){
					$totalp[4][$i]=round((($totalp[3][$i]/$pprog[0][$m[0]])*100),2); ////// Programado Acumulado %
				}

				if(count($eprog)!=0){
					$totalp[5][$i]=$eprog[0][$m[$i]]; /// Ejecutado
					$suma_efin=$suma_efin+$eprog[0][$m[$i]];
					$totalp[6][$i]=$suma_efin; ///// Ejecutado acumulado

					if($pprog[0][$m[0]]!=0){
						$totalp[7][$i]=round((($totalp[6][$i]/$pprog[0][$m[0]])*100),2); ////// Ejecutado Acumulado %
					}

					if($totalp[4][$i]!=0){
						$totalp[8][$i]=round((($totalp[7][$i]/$totalp[4][$i])*100),2); ////// Eficacia
					}

					/*------------------------- eficacia -------------------------*/
					if($totalp[8][$i]<=75){$totalp[9][$i] = $totalp[8][$i];}
					if ($totalp[8][$i] >= 76 && $totalp[8][$i] <= 90.9) {$totalp[10][$i] = $totalp[8][$i];}
					if($totalp[8][$i] >= 91){$totalp[11][$i] = $totalp[8][$i];}
				}
			}
		}

		return $totalp;
	}

	public function prog_financiera($mod,$id_f,$proy_id){
		$gestion = $this->session->userdata('gestion');

		$proyecto = $this->model_proyecto->get_id_proyecto($proy_id);
		$fase = $this->model_faseetapa->get_id_fase($proy_id);
		$tabla ='';


		//    $this->mis_partidas($mod,$proy_id,$id_f,$this->gestion,$fase[0]['pfec_ejecucion']);
		//    $tabla .=''.$proy_id.'--'.$this->gestion.'-'.$fase[0]['pfec_ejecucion'];
		$proyecto_programado= $this->mfinanciera->lista_partidas_proy_programado($proy_id,$gestion,$fase[0]['pfec_ejecucion']);
		$nro=0;
		$tabla .='<br><h2 class="titulo"><b>3 . EJECUCIÓN FINANCIERA (POR PARTIDA)</b></h2>';
		foreach ($proyecto_programado as $pp){
			$nro++;
			$ff= $this->mfinanciera->get_ff($pp['ff_id']);
			$of= $this->mfinanciera->get_of($pp['of_id']);
			$tabla .='<table class="table table-condensed change_order_items" style="width:95%;" align="center">';
			$tabla .='<tr class="head_tabla" style="background-color:#ac9fae;font-weight: bold;">';
			$tabla .='<th style="width:3%;"><center>Nº</center></th>';
			$tabla .='<th style="width:5%;"><center>PARTIDA</center></th>';
			$tabla .='<th style="width:5%;"><center>FUENTE</center></th>';
			$tabla .='<th style="width:5%;"><center>ORGANISMO</center></th>';
			$tabla .='<th style="width:82%;"><center>PROGRAMADO - EJECUTADO</center></th>';
			$tabla .='</tr>';
			$tabla .='<tr>';
			$tabla .='<td><center>'.$nro.'</center></td>';
			$tabla .='<td><center>'.$pp['par_codigo'].'</center></td>';
			$tabla .='<td><center>'.$ff[0]['ff_codigo'].'</center></td>';
			$tabla .='<td><center>'.$of[0]['of_codigo'].'</center></td>';
			$tabla .='<td>';
			$tabla .='<table class="table table-condensed change_order_items tabla_partidas" style="width:98%;" align="center">';
			$tabla .='<tr class="modo1 head_componente">';
			$tabla .='<th bgcolor="#504c49" style="width:8.8%;"><center>P/E</center></th>';
			$tabla .='<th bgcolor="#504c49" style="width:7.6%;"><center>ENE</center></th>';
			$tabla .='<th bgcolor="#504c49" style="width:7.6%;"><center>FEB</center></th>';
			$tabla .='<th bgcolor="#504c49" style="width:7.6%;"><center>MAR</center></th>';
			$tabla .='<th bgcolor="#504c49" style="width:7.6%;"><center>ABR</center></th>';
			$tabla .='<th bgcolor="#504c49" style="width:7.6%;"><center>MAY</center></th>';
			$tabla .='<th bgcolor="#504c49" style="width:7.6%;"><center>JUN</center></th>';
			$tabla .='<th bgcolor="#504c49" style="width:7.6%;"><center>JUL</center></th>';
			$tabla .='<th bgcolor="#504c49" style="width:7.6%;"><center>AGO</center></th>';
			$tabla .='<th bgcolor="#504c49" style="width:7.6%;"><center>SEP</center></th>';
			$tabla .='<th bgcolor="#504c49" style="width:7.6%;"><center>OCT</center></th>';
			$tabla .='<th bgcolor="#504c49" style="width:7.6%;"><center>NOV</center></th>';
			$tabla .='<th bgcolor="#504c49" style="width:7.6%;"><center>DIC</center></th>';
			$tabla .='</tr>';
			$tabla .='<tr class="modo1">';
			$tabla .='<td>PROGRAMADO</td>';
			$tabla .='<td class="text-right">'.number_format($pp['enero'], 2, ',', '.').'</td>';
			$tabla .='<td class="text-right">'.number_format($pp['febrero'], 2, ',', '.').'</td>';
			$tabla .='<td class="text-right">'.number_format($pp['marzo'], 2, ',', '.').'</td>';
			$tabla .='<td class="text-right">'.number_format($pp['abril'], 2, ',', '.').'</td>';
			$tabla .='<td class="text-right">'.number_format($pp['mayo'], 2, ',', '.').'</td>';
			$tabla .='<td class="text-right">'.number_format($pp['junio'], 2, ',', '.').'</td>';
			$tabla .='<td class="text-right">'.number_format($pp['julio'], 2, ',', '.').'</td>';
			$tabla .='<td class="text-right">'.number_format($pp['agosto'], 2, ',', '.').'</td>';
			$tabla .='<td class="text-right">'.number_format($pp['septiembre'], 2, ',', '.').'</td>';
			$tabla .='<td class="text-right">'.number_format($pp['octubre'], 2, ',', '.').'</td>';
			$tabla .='<td class="text-right">'.number_format($pp['noviembre'], 2, ',', '.').'</td>';
			$tabla .='<td class="text-right">'.number_format($pp['diciembre'], 2, ',', '.').'</td>';
			$tabla .='</tr>';
			$pe= $this->mfinanciera->get_partida_proy_ejecutado($pp['pr_id'],$gestion);
			if(count($pe)!=0){
				$tabla .='<tr class="modo1">';
				$tabla .='<td>EJECUTADO</td>';
				$tabla .='<td class="text-right">'.number_format($pe[0]['enero'], 2, ',', '.').'</td>';
				$tabla .='<td class="text-right">'.number_format($pe[0]['febrero'], 2, ',', '.').'</td>';
				$tabla .='<td class="text-right">'.number_format($pe[0]['marzo'], 2, ',', '.').'</td>';
				$tabla .='<td class="text-right">'.number_format($pe[0]['abril'], 2, ',', '.').'</td>';
				$tabla .='<td class="text-right">'.number_format($pe[0]['mayo'], 2, ',', '.').'</td>';
				$tabla .='<td class="text-right">'.number_format($pe[0]['junio'], 2, ',', '.').'</td>';
				$tabla .='<td class="text-right">'.number_format($pe[0]['julio'], 2, ',', '.').'</td>';
				$tabla .='<td class="text-right">'.number_format($pe[0]['agosto'], 2, ',', '.').'</td>';
				$tabla .='<td class="text-right">'.number_format($pe[0]['septiembre'], 2, ',', '.').'</td>';
				$tabla .='<td class="text-right">'.number_format($pe[0]['octubre'], 2, ',', '.').'</td>';
				$tabla .='<td class="text-right">'.number_format($pe[0]['noviembre'], 2, ',', '.').'</td>';
				$tabla .='<td class="text-right">'.number_format($pe[0]['diciembre'], 2, ',', '.').'</td>';
				$tabla .='</tr>';
			}
			$tabla .='</table>';
			$tabla .='</td>';

			$tabla .='</tr>';
			$tabla .='</table>';
		}
		$total=$this->suma_total_partidas($proy_id,$gestion);
		$tabla .='<br><b>EFICACIA EN EJECUCIÓN FINANCIERA (ANUAL)</b>';
		$tabla .='
                    <table class="table tabla_eficacia change_order_items" style="width:95%;" align="center">
                        <tr class="modo1 head_tabla">
                            <th colspan=13><center><b>GESTI&Oacute;N '.$gestion.'</b></th>
                        </tr>
                        <tr class="modo1 head_tabla">
                            <th bgcolor="#504c49" style="width:16%;"><center>P/E</center></th>
                            <th bgcolor="#504c49" style="width:7%;"><center>ENERO</center></th>
                            <th bgcolor="#504c49" style="width:7%;"><center>FEBRERO</center></th>
                            <th bgcolor="#504c49" style="width:7%;"><center>MARZO</center></th>
                            <th bgcolor="#504c49" style="width:7%;"><center>ABRIL</center></th>
                            <th bgcolor="#504c49" style="width:7%;"><center>MAYO</center></th>
                            <th bgcolor="#504c49" style="width:7%;"><center>JUNIO</center></th>
                            <th bgcolor="#504c49" style="width:7%;"><center>JULIO</center></th>
                            <th bgcolor="#504c49" style="width:7%;"><center>AGOSTO</center></th>
                            <th bgcolor="#504c49" style="width:7%;"><center>SEPTIEMBRE</center></th>
                            <th bgcolor="#504c49" style="width:7%;"><center>OCTUBRE</center></th>
                            <th bgcolor="#504c49" style="width:7%;"><center>NOVIEMBRE</center></th>
                            <th bgcolor="#504c49" style="width:7%;"><center>DICIEMBRE</center></th>
                        </tr>
                        <tr class="modo1">
                            <td class=""><b>PROGRAMADO</b></td>';
		for ($i=1; $i <=12 ; $i++){
			$tabla .='<td class="fpa text-right">'.number_format($total[2][$i], 2, ',', '.').'</td>';
		}
		$tabla .='
                        </tr>
                        <tr class="modo1">
                            <td><b>PROGRAMADO ACUMULADO</b></td>';
		for ($i=1; $i <=12 ; $i++){
			$tabla .='<td class="fep text-right">'.number_format($total[3][$i], 2, ',', '.').'</td>';
		}
		$tabla .='
                        </tr>
                        <tr class="modo1" bgcolor="#c5bcf3">
                            <td><b>% PROGRAMADO ACUMULADO</b></td>';
		for ($i=1; $i <=12 ; $i++){
			$tabla .='<td class="fea text-right">'.$total[4][$i].'%</td>';
		}
		$tabla .='
                        </tr>
                        <tr class="modo1">
                            <td class=""><b>EJECUTADO</b></td>';
		for ($i=1; $i <=12 ; $i++)
		{
			$tabla .='<td class="fpa text-right">'.number_format($total[5][$i], 2, ',', '.').'</td>';
		}
		$tabla .='
                        </tr>
                        <tr class="modo1">
                            <td><b>EJECUTADO ACUMULADO</b></td>';
		for ($i=1; $i <=12 ; $i++){
			$tabla .='<td class="fep text-right">'.number_format($total[6][$i], 2, ',', '.').'</td>';
		}
		$tabla .='
                        </tr>
                        <tr class="modo1" bgcolor="#c5bcf3">
                            <td><b>% EJECUTADO ACUMULADO</b></td>';
		for ($i=1; $i <=12 ; $i++){
			$tabla .='<td class="fea text-right">'.$total[7][$i].'%</td>';
		}
		$tabla .='
                        </tr>
                        <tr class="modo1" bgcolor="#bbf3cf">
                            <td><b>EFICACIA POR MES</b></td>';
		for ($i=1; $i <=12 ; $i++){
			$tabla .='<td style="font-weight:bold;" class="efic text-right">'.$total[8][$i].'%</td>';
		}
		$tabla .='
                        </tr>';
		$tabla .='
                    </table>
             </body>
        </html>';
		return $tabla;
	}
	


	
	
	
}