<style>
    .container {
    position: relative;
    max-width: 650px;
    margin: 0px auto;
    margin-top: 50px;
    }

    .comment {
    background-color: blue;
    float: left;
    width: 100%;
    height: auto;
    }

    .commenter {
    float: left;
    }

    .commenter img {
    width: 35px;
    height: 35px;
    }

    .comment-text-area {
    float: left;
    width: calc(100% - 35px);
    height: auto;
    background-color: red;
    }

    .textinput {
    float:left;
    width: 100%;
    min-height: 35px;
    outline: none;
    resize: none;
    border: 1px solid #f0f0f0;
    }
</style>
<script xmlns="http://www.w3.org/1999/html">
    function abreVentana(PDF) {
        var direccion;
        direccion = '' + PDF;
        window.open(direccion, "Reporte Misión Insitucional", "width=800,height=650,scrollbars=SI");
    };
</script>
<?php $site_url = site_url(""); ?>
<div id="main" role="main">
    <div id="ribbon">
        <ol class="breadcrumb">
            <li>Inicio</li><li>Marco Estrategico</li><li>Misión</li>
        </ol>
    </div>
    <div id="content">
        <div class="row">
           <h2 style=""><i class="icon fa fa-check" style="color:#e7f3ff;">&nbsp;</i><b>MISIÓN INSTITUCIONAL</b></h2>
        </div><br>
        <div class="row">
            <div class="col-md-1">
            </div>
            <div class="col-md-12">
                <div class="jarviswidget well jarviswidget-sortable" id="wid-id-1" role="widget" style="border:outset;">
                    <div role="content">
                        <div class="widget-body">
                            <div class="jumbotron">
                                <form class="form-horizontal" action="<?php echo $site_url . '/programacion/mision/editar_mision' ?>" id="form_mision" name="form_mision" method="post">
                                    <h2>
                                       <b> Modificar Misión</b>
                                    </h2>
                                    <div class="row">
                                        <div class="col-md-1"></div>
                                        <div class="col-md-12">
                                            <div class="comment-text-area">
                                                <textarea style="background-color: #f8f8f8;border-radius: 4px; border: 1px solid #ccc;" id="vmision" name="vmision" class="textinput" placeholder="Ingrese Misión" rows="8" disable><?php  echo $mision[0]['conf_mision'];?></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-1"></div>
                                    </div><br><br>

                                    <div class="row">
                                        
                                        <a href="<?php echo $site_url.'/mision'?>">
                                            <button id="edit" class="btn btn-danger" type="button"> CANCELAR </button>
                                        </a>
                                        <input type="button" value="GUARDAR" id="btsubmit" class="btn btn-primary" onclick="valida_mision()" title="GUARDAR MISION">
                                         <!--reporte modificando-->
                                        <a href="javascript:abreVentana('<?php echo $site_url.'/reportes/reporte/mision_vision';?>');" class="btn btn-success" title="REPORTE">REPORTE</a>
                                        <a href="javascript:abreVentana('<?php echo $site_url.'/reportes/reporte/historial_mision_vision/1';?>');" class="btn btn-default" title="REPORTE">HISTORIAL</a>
                                                <!--end reporte modificando-->
                                    </div> 
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function valida_mision()
    { 
        alertify.confirm("GUARDAR MISI\u00D3N?", function (a) {
            if (a) {
                //============= GUARDAR DESPUES DE LA VALIDACION ===============
                document.form_mision.submit();
            } else {
                alertify.error("OPCI\u00D3N CANCELADA");
            }
        });
    }
</script>