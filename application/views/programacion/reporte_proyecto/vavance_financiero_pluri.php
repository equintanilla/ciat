<?php if ($this->session->userdata('usuario')) { ?>
    <!DOCTYPE html>
    <html lang="en-us">
    <head>
        <meta charset="utf-8">
        <title> <?php echo $this->session->userdata('name')?> </title>
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <!-- Basic Styles -->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">

        <!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css">
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">

        <!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">

        <!-- FAVICONS -->
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
        <style type="text/css">
            aside {
                background: #05678B;
            }

        </style>
    </head>
    <body class="">
    <!-- HEADER -->
    <header id="header">
        <div id="logo-group">
            <span id="logo"> <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="SmartAdmin"> </span>
        </div>
    </header>
    <!-- END HEADER -->
    <aside id="left-panel">
        <div class="login-info">
        <span>
          <a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
               <span>
                    <i class="fa fa-user" aria-hidden="true"></i> <?php echo $this->session->userdata("user_name"); ?>
               </span>
          </a>
        </span>
        </div>
        <nav>
            <ul>
                <li>
                    <a href='<?php echo site_url("admin") . '/dashboard'; ?>' title="MENU PRINCIPAL"><i class="fa fa-lg fa-fw fa-home"></i>
                        <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span>
                    </a>
                </li>
                <li class="text-center">
                    <a href="<?php echo base_url() . 'index.php/admin/proy/mis_proyectos/' . $mod ?>" title="PROGRAMACION -> MIS PROYECTOS">
                        <span class="menu-item-parent">MIS PROYECTOS</span>
                    </a>
                </li>
                <?php
                if ($nro_fase == 1) {
                    for ($i = 0; $i < count($enlaces); $i++) {
                        ?>
                        <li>
                            <a href="#">
                                <i class="<?php echo $enlaces[$i]['o_image']; ?>"></i>
                                <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span>
                            </a>
                            <ul>
                                <?php
                                $submenu = $this->menu_modelo->get_Modulos_sub($enlaces[$i]['o_child']);
                                foreach ($submenu as $row) {
                                    ?>
                                    <li>
                                        <a href="<?php echo base_url($row['o_url']) . "/" . $mod . "/" . $id_f[0]['id'] . "/" . $id_f[0]['proy_id']; ?>"><?php echo $row['o_titulo']; ?></a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </li>
                        <?php
                    }
                }
                ?>
            </ul>
        </nav>
        <span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>
    </aside>

    <!-- MAIN PANEL -->
    <div id="main" role="main">
        <div>
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url() . 'index.php/admin/proy/list_proy' ?>" title="MIS PROYECTOS">Mis Operaciones</a></li>
                <li>
                    <a href="<?php echo base_url() . 'index.php/admin/comp/menu/' . $mod . '/' . $proyecto[0]['proy_id'] ?>">T&eacute;cnico de Planificaci&oacute;n</a>
                </li>
                <li>Curva "S" Avance Financiero</li>
            </ol>
        </div>
        <!-- MAIN CONTENT -->
        <div id="content">
            <section id="widget-grid" class="">
                <div class="row">
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="jarviswidget jarviswidget-color-darken">
                            <header>
                                <span class="widget-icon"> <span class="glyphicon glyphicon-folder-close"></span></i> </span>

                                <h2>CURVA "S" AVANCE FINANCIERO</h2>
                            </header>
                            <div class="widget-body">
                                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="row">
                                        <div class="well well-sm col-sm-2">
                                            <label style="color: #1167ac;font-weight: bold">CÓDIGO:</label>
                                            <?php echo $dato_proy['proy_codigo'] ?>
                                        </div>
                                        <div class="well well-sm col-sm-10">
                                            <label style="color: #1167ac;font-weight: bold"> NOMBRE DEL PROYECTO:</label>
                                            <?php echo $dato_proy['proy_nombre'] ?>
                                        </div>
                                    </div>
                                    <?php
                                    for ($i = 0; $i < count($fases); $i++) {
                                        ?>
                                        <div class="row">
                                            <div class="well well-sm col-sm-2">
                                                <label style="color: #1167ac;font-weight: bold">GESTIÓN DEL PROYECTO:</label>
                                                <?php echo $fases[$i]['gestion'] ?>
                                            </div>
                                            <div class="well well-sm col-sm-10">
                                                <label style="color: #1167ac;font-weight: bold"> PRESUPUESTO DE LA FASE:</label>
                                                <?php echo $fases[$i]['ppto_fase'] . ' Bs.' ?>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div id="<?php echo 'graf_prog_ejec' . $i ?>" class="well well-sm col-md-6 col-sm-6 col-xs-6">

                                            </div>
                                            <div id="<?php echo 'graf_eficacia' . $i ?>" class="well well-sm col-md-6 col-sm-6 col-xs-6">

                                            </div>
                                        </div>
                                        <!--graficos-->
                                        <script src="<?php echo base_url(); ?>assets/highcharts/js/highcharts.js"></script>
                                        <script src="<?php echo base_url(); ?>assets/highcharts/js/highcharts-more.js"></script>
                                        <script src="<?php echo base_url(); ?>assets/highcharts/js/modules/exporting.js"></script>
                                        <script type="text/javascript">
                                            var chart1 = new Highcharts.Chart({
                                                chart: {
                                                    renderTo: '<?php echo 'graf_prog_ejec' . $i ?>', // div contenedor
                                                    type: 'line' // tipo de grafico
                                                },
                                                title: {
                                                    text: 'AVANCE FINANCIERO',
                                                    x: -20 //center
                                                },
                                                subtitle: {
                                                    text: ''
                                                },
                                                xAxis: {
                                                    categories: ['ENERO', 'FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO', 'JULIO', 'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE', 'NOVIEMBRE', 'DICIEMBRE']
                                                },
                                                yAxis: {
                                                    title: {
                                                        text: 'PORCENTAJES (%)'
                                                    }
                                                },
                                                plotOptions: {
                                                    line: {
                                                        dataLabels: {
                                                            enabled: true
                                                        },
                                                        enableMouseTracking: false
                                                    }
                                                },
                                                series: [
                                                    {
                                                        name: 'PROGRAMACIÓN ACUMULADA EN %',
                                                        data: [<?php echo $fases[$i]['graf_prog']?>]
                                                    },
                                                    {
                                                        name: 'EJECUCIÓN ACUMULADA EN %',
                                                        data: [<?php echo $fases[$i]['graf_ejec']?>]
                                                    }
                                                ]
                                            });
                                        </script>
                                        <script type="text/javascript">
                                            var chart<?php echo  $i ?> = new Highcharts.chart('<?php echo 'graf_eficacia' . $i ?>', {
                                                chart: {
                                                    type: 'column'
                                                },
                                                title: {
                                                    text: 'SEMÁFORO EFICACIA'
                                                },
                                                xAxis: {
                                                    categories: ['ENERO', 'FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO', 'JULIO', 'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE', 'NOVIEMBRE', 'DICIEMBRE']
                                                },
                                                yAxis: {
                                                    min: 0,
                                                    title: {
                                                        text: 'PORCENTAJES (%)'
                                                    },
                                                    stackLabels: {
                                                        enabled: false,

                                                    }
                                                },
                                                legend: {
                                                    align: 'right',
                                                    x: -30,
                                                    verticalAlign: 'top',
                                                    y: 25,
                                                    floating: true,
                                                    backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                                                    borderColor: '#CCC',
                                                    borderWidth: 1,
                                                    shadow: true
                                                },
                                                tooltip: {
                                                    headerFormat: '<b>{point.x}</b><br/>',
                                                    pointFormat: '{series.name}: <br/> TOTAL:   {point.y}'
                                                },
                                                plotOptions: {
                                                    column: {
                                                        stacking: 'normal',
                                                        dataLabels: {
                                                            enabled: false,
                                                            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: '<b style="color: #FF0000;">MENOR A 75%</b>',
                                                    data: [<?php echo $fases[$i]['ef_menor']?>]

                                                }, {
                                                    name: '<b style="color: #d6d21f;">ENTRE 76% Y 90%</b>',
                                                    data: [<?php echo $fases[$i]['ef_entre']?>]
                                                }, {
                                                    name: '<b style="color: green;">MAYOR A 91%</b>',
                                                    data: [<?php echo $fases[$i]['ef_mayor']?>]
                                                }]
                                            });
                                        </script>

                                        <div class="row">
                                            <div class="well well-sm col-sm-12 table-responsive">
                                                <style type="text/css">
                                                    th {
                                                        background-color: #474544;
                                                        color: white;
                                                    }

                                                    td {
                                                        color: black;
                                                        font-weight: bold;
                                                    }
                                                </style>
                                                <table class="table table-bordered table-responsive " width="100%">
                                                    <thead>
                                                    <tr>
                                                        <th style="text-align: center;vertical-align: middle">PROGRAMACIÓN / EJECUCIÓN</th>
                                                        <th>ENERO</th>
                                                        <th>FEBRERO</th>
                                                        <th>MARZO</th>
                                                        <th>ABRIL</th>
                                                        <th>MAYO</th>
                                                        <th>JUNIO</th>
                                                        <th>JULIO</th>
                                                        <th>AGOSTO</th>
                                                        <th>SEPTIEMBRE</th>
                                                        <th>OCTUBRE</th>
                                                        <th>NOVIEMBRE</th>
                                                        <th>DICIEMBRE</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="">
                                                    <?php echo $fases[$i]['tabla'] ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>


                                </article>
                            </div>
                        </div>
                    </article>
                </div>
            </section>
        </div>
        <!-- END MAIN CONTENT -->
    </div>

    <!-- PAGE FOOTER -->
    <div class="page-footer">
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <span class="txt-color-white"><?php echo $this->session->userdata('name')?>  <?php echo $this->session->userData('gestion') ?></span>
            </div>
        </div>
    </div>
    <!-- END PAGE FOOTER -->

    <!--================================================== -->

    <script>
        if (!window.jQuery) {
            document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-2.0.2.min.js"><\/script>');
        }
    </script>

    <script>
        if (!window.jQuery.ui) {
            document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
        }
    </script>

    <!-- IMPORTANT: APP CONFIG -->
    <script src="<?php echo base_url(); ?>/assets/js/app.config.js"></script>

    <!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
    <script src="<?php echo base_url(); ?>/assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script>

    <!-- BOOTSTRAP JS -->
    <script src="<?php echo base_url(); ?>/assets/js/bootstrap/bootstrap.min.js"></script>

    <!-- CUSTOM NOTIFICATION -->
    <script src="<?php echo base_url(); ?>/assets/js/notification/SmartNotification.min.js"></script>

    <!-- JARVIS WIDGETS -->
    <script src="<?php echo base_url(); ?>/assets/js/smartwidgets/jarvis.widget.min.js"></script>

    <!-- EASY PIE CHARTS -->
    <script src="<?php echo base_url(); ?>/assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

    <!-- SPARKLINES -->
    <script src="<?php echo base_url(); ?>/assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>


    <!-- JQUERY UI + Bootstrap Slider -->
    <script src="<?php echo base_url(); ?>/assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>

    <!-- browser msie issue fix -->
    <script src="<?php echo base_url(); ?>/assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>

    <!-- FastClick: For mobile devices -->
    <script src="<?php echo base_url(); ?>/assets/js/plugin/fastclick/fastclick.min.js"></script>

    <!-- Demo purpose only -->
    <script src="<?php echo base_url(); ?>/assets/js/demo.min.js"></script>

    <!-- MAIN APP JS FILE -->
    <script src="<?php echo base_url(); ?>/assets/js/app.min.js"></script>

    <!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
    <!-- Voice command : plugin -->
    <script src="<?php echo base_url(); ?>/assets/js/speech/voicecommand.min.js"></script>

    <!-- PAGE RELATED PLUGIN(S) -->
    <script src="<?php echo base_url(); ?>/assets/js/plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
    <script src="<?php echo base_url(); ?>/assets/js/plugin/fuelux/wizard/wizard.min.js"></script>


    </body>

    </html>
<?php } else { ?>
    <script>

        function redireccionar() {
            window.location = "<?php echo base_url(); ?>index.php/admin/logins";
        }
        setTimeout("redireccionar()", 2000);
    </script>
    <style>
        .espacio {
            height: 40%;
        }
    </style>
    <div class="container-fluid imagenn">
        <div class="espacio">
        </div>

        <div class="tour-dos col-md-12">
            <center><img src="<?php echo base_url(); ?>assets/img/logo.png" class="salir"/></center>
            <center><h4 class="error-text box animated"><?php echo $this->session->userdata('name')?> -- Cerrar sesión</h4></center>
            <center><img src="<?php echo base_url(); ?>assets/img/salir.gif" class="salir"/></center>

        </div>
    </div>
    <?php

} ?>
