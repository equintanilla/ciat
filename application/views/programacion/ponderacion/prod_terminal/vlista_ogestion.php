<?php
$atras = site_url("") . '/prog/pond_pt';
?>
<div id="main" role="main">
    <ol class="breadcrumb">
        <li>Ponderaciónn</li>
        <li><a href="<?php echo $atras ?>">Red Programas</a></li>
        <li>Acciones de corto plazo</li>
    </ol>
    <div id="content">
        <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                <section id="widget-grid" class="well">
                    <div class="" style="font-size: 15px">
                        <b>LISTA DE ACCIONES A CORTO PLAZO</b> <br>
                        <b>C&oacute;DIGO POA: </b>
                        <small class="txt-color-blueLight"><?php echo $dato_poa->poa_codigo ?></small>
                        <br>
                        <b>CATEGOR&Iacute;A PROGRAM&Aacute;TICA: </b>
                        <small class="txt-color-blueLight"> <?php echo $dato_poa->aper_programa . $dato_poa->aper_proyecto .
                                $dato_poa->aper_actividad . " - " . $dato_poa->aper_descripcion ?></small>
                        <br>
                        <b>GESTI&Oacute;N: </b>
                        <small class="txt-color-blueLight"><?php echo $dato_poa->poa_gestion ?></small>
                    </div>
                </section>
            </article>
            <article class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                <section id="widget-grid" class="well">
                    <center>
                        <div class="dropdown">
                            <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenu1" style="width:100%;" data-toggle="dropdown" aria-expanded="true">
                                OPCIONES
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                <li role="presentation">
                                    <a role="menuitem" tabindex="-1" href="<?php echo $atras ?>">VOLVER ATRAS</a>
                                </li>
                            </ul>
                        </div>
                    </center>
                </section>
            </article>

        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-id-21" data-widget-editbutton="false">
                    <header>
                        <span class="widget-icon"> <span class="fa fa-table"></span></span>
                        <h2>LISTA DE ACCIONES DE CORTO PLAZO</h2>
                    </header>
                    <div>
                        <div class="widget-body no-padding">
                            <div class="table-responsive">
                                <table id="dt_basic" class="table table-bordered "
                                       width="100%">
                                    <thead>
                                    <tr>
                                        <th>Nro</th>
                                        <th TITLE="PRODUCTO TEMRINAL"><font size="1"> ASIGNAR</font></th>
                                        <th><font size="1">CÓDIGO </font></th>
                                        <th><font size="1">OBJETIVO DE GESTIÓN</font></th>
                                        <th><font size="1">RESPONSABLE</font></th>
                                        <th><font size="1">UNIDAD ORGANIZACIONAL </font></th>
                                        <th><font size="1"> PONDERACIÓN </font></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    echo $lista_ogestion;
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
