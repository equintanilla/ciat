<!-- MAIN PANEL -->
<?php
$site_url = site_url("");
$poa_id = $dato_poa['poa_id'];
$obje_id = $dato_obje['obje_id'];
$rol_id = $this->session->userData('rol_id');
?>
<div id="main" role="main">
    <!-- RIBBON -->
    <div id="">
        <!-- breadcrumb -->
        <ol class="breadcrumb">
            <li> Programación del POA</li>
            <li><a href="<?php echo $site_url. '/prog/redobj/'; ?>">Red de Objetivos </a></li>
            <li><a href="<?php echo $site_url.'/prog/obj/' . $poa_id ?>">Acci&oacute;n de Mediano Plazo</a></li>
            <li>
                <a href="<?php echo $site_url . '/prog/obje_gestion/' . $poa_id . '/' . $obje_id ?>">Acci&oacute;n de Corto Plazo</a></li>
            <li> Nuevo Registro</li>
        </ol>
    </div>
    <!-- END RIBBON -->
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                <section id="widget-grid" class="well">
                    <div class="" style="font-size: 15px">
                        <b>ACCI&Oacute;N DE CORTO PLAZO - NUEVO</b> <br>
                        <b>C&oacute;DIGO POA: </b><small class="txt-color-blueLight"><?php echo $dato_poa['poa_codigo'] ?></small><br>
                        <b>CATEGOR&Iacute;A PROGRAM&Aacute;TICA: </b><small class="txt-color-blueLight"><?php echo $dato_poa['aper_programa'] . $dato_poa['aper_proyecto'] .
                                $dato_poa['aper_actividad'] . " - " . $dato_poa['aper_descripcion'] ?></small><br>
                        <b>ACCI&Oacute;N DE MEDIANO PLAZO: </b><small class="txt-color-blueLight"><?php echo $dato_obje['obje_objetivo']  ?></small><br>
                        <b>PDES PILAR: </b><small class="txt-color-blueLight"><?php echo $dato_obje['pdes_pilar']  ?></small><br><b>ACCION: </b><small class="txt-color-blueLight"><?php echo $dato_obje['pdes_accion']  ?></small><br>
                        <b>PTDI EJE: </b><small class="txt-color-blueLight"><?php echo $dato_obje['ptdi_eje']  ?></small><br><b>PROGRAMA: </b><small class="txt-color-blueLight"><?php echo $dato_obje['ptdi_programa']  ?></small><br>
                    </div>
                </section>
            </article>
            <article class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                <section id="widget-grid" class="well">
                    <center>
                        <div class="dropdown">
                            <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenu1" style="width:100%;" data-toggle="dropdown" aria-expanded="true">
                                OPCIONES
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                <li role="presentation">
                                    <a role="menuitem" tabindex="-1" href="<?php echo $site_url . '/prog/obje_gestion/' . $poa_id . '/' . $obje_id ?>">VOLVER ATRAS</a>
                                </li>
                            </ul>
                        </div>
                    </center>
                </section>
            </article>

        </div>
        <!-- ==================  NUEVO OBJETIVO DE GESTIÓN ========================== -->
        <div class="row">
            <article class="col-sm-12 col-md-12 col-lg-12">
                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget jarviswidget-color-darken" id="wid-id-1" data-widget-editbutton="false">
                    <header>
                        <span class="widget-icon"> <i class="glyphicon glyphicon-pencil"></i> </span>
                        <h2>NUEVA ACCI&Oacute;N DE CORTO PLAZO</h2>
                    </header>
                    <!-- widget content -->
                    <div class="widget-body">
                        <form action="<?php echo $site_url.'/prog/o_guardar' ?>"
                              id="form_add_objgestion"
                              name="form_add_objgestion" novalidate="novalidate" method="post">
                            <input type="hidden" name="poa_id" id="poa_id"
                                   value="<?php echo $poa_id ?>">
                            <input type="hidden" name="aper_id" id="aper_id"
                                   value="<?php echo $dato_poa['aper_id'] ?>">
                            <input type="hidden" name="obje_id" id="obje_id"
                                   value="<?php echo $obje_id ?>">
                            <div id="bootstrap-wizard-1" class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <div class="well">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <LABEL><b>Responsable</b></label>
                                                        <?php 
                                                        if($rol_id == 1){
                                                            ?>
                                                            <select name="funcionario" id="funcionario" disabled="disabled" 
                                                                    class="select2">
                                                                <option value=""> Seleccione una opción</option>
                                                                <?php
                                                                foreach ($list_funcionario as $row) {
                                                                    $dato = $row['fun_nombre'] . " " . $row['fun_paterno'] . " " . $row['fun_materno'];

                                                                    if ($row['fun_id'] == $dato_obje['fun_id']) {
                                                                        echo '<option value="' . $row['fun_id'] . '" selected>' . $dato . '</option>';
                                                                    } else {
                                                                        echo '<option value="' . $row['fun_id'] . '">' . $dato . '</option>';
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                            <?php
                                                        }else{
                                                            ?>
                                                            <input type="text" name="funcionario" id="funcionario"
                                                                   class="form-control" disabled="disabled"
                                                                   value="<?php echo $this->session->userData('funcionario') ?>">
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <LABEL><b>Unidad Organizacional</b></label>
                                                        <?php
                                                        if($rol_id == 1){
                                                            ?>
                                                            <input type="text" class="form-control" name="uni_unidad" value="<?php echo $uninda_organizacional ?>" 
                                                                   id="uni_unidad" disabled="disabled">
                                                            <?php
                                                        }else{
                                                            ?>
                                                            <input type="text" name="uni_unidad" id="uni_unidad"
                                                                   class="form-control" disabled="disabled"
                                                                   value="<?php echo $this->session->userData('unidad') ?>">
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br>

                                        <div class="well">
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <LABEL><b>Código</b></label>
                                                        <input disabled="disabled" class="form-control" type="text"
                                                               value="AUTOMÁTICO">
                                                    </div>
                                                </div>
                                                <div class="col-sm-9">
                                                    <div class="form-group">
                                                        <LABEL><b>Acci&oacute;n</b></label>
                                                <textarea name="oobjetivo" id="oobjetivo" style="width:100%;" 
                                                          rows="4" class="form-control custom-scroll"
                                                        onkeypress="if (this.value.length > 500){return false; }"><?php echo $dato_obje['obje_objetivo'] ?></textarea>
                                                        <!--onpaste="return false"-->
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <LABEL><b>Tipo Indicador? </b></label><br>
                                                               <input type="hidden" name="tipo_indicador" id="tipo_indicador" value="<?php echo $dato_obje['indi_id'] ?>">
                                                                <select class="form-control" id="tipo_indicador2" name="tipo_indicador2" disabled="disabled">
                                                                    <option value="">Seleccione</option>
                                                                    <?php
                                                                    foreach ($list_indicador as $row) {
                                                                        if ($row['indi_id'] == $dato_obje['indi_id']) {
                                                                            echo '<option value="' . $row['indi_id'] . '" selected>' . $row['indi_descripcion'] . '</option>';
                                                                        } else {
                                                                            echo '<option value="' . $row['indi_id'] . '">' . $row['indi_descripcion'] . '</option>';
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div id="caja_relativo2" name="caja_relativo2"style="display: none;">
                                                            <div class="col-sm-12">
                                                                <label for=""><b>Denominador</b></label>
                                                                <select class="form-control input-sm" id="o_denominador" name="o_denominador">
                                                                    <option value="0">Variable</option>
                                                                    <option value="1">Fijo</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <br>
                                                    </div>
                                                </div>

                                                <div class="col-sm-9">
                                                    <div class="form-group">
                                                        <LABEL><b>Indicador</b></label>
                                                <textarea name="oindicador" id="oindicador" disabled="disabled" 
                                                          style="width:100%;"
                                                          rows="4" class="form-control custom-scroll"
                                                          onkeypress="if (this.value.length > 500){return false; }"><?php echo $dato_obje['obje_indicador'] ?></textarea>
                                                    </div>
                                                </div>


                                            </div>
                                            <div class="row" style="padding-top: 10px;">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <LABEL><b>Fórmula</b></label>
                                                <textarea name="oformula" id="oformula"
                                                          style="width:100%;"
                                                          rows="3" class="form-control custom-scroll"
                                                          onkeypress="if (this.value.length > 500){return false; }"> </textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="alert alert-block alert-success">
                                                        <b>La suma de la programación mensual mas la línea base debe ser igual a la meta. </b><br>
                                                        <center><b style="align-content: center">( <span style="color: #00A300">PROGRAMACION MENSUAL </span> +
                                                                <span style="color: #0000cc">LINEA BASE</span> ) =
                                                                <span style="color: #f00000"> META </span></b></center>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <LABEL><b><span style="color: #0000cc">LÍNEA BASE</span></b></label>
                                                        <input class="form-control" type="text" name="olineabase"
                                                               id="olineabase" value="0" placeholder="0"
                                                               onkeypress="if (this.value.length < 4) { return numerosDecimales(event);}else{return false; }"
                                                        >
                                                        <!--<input class="form-control" type="text" name="olineabase" id="olineabase" value="0" placeholder="0" onkeypress="if (this.value.length < 4) { return soloNumeros(event);}else{return false; }" onkeyup="format(this)" onchange="format(this)" onpaste="return false">-->
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <LABEL><b><span style="color: #f00000">META</span></b><span id="tipo_meta"> </span></label>
                                                        <input class="form-control" type="text" name="ometa"
                                                               id="ometa"
                                                               placeholder="0"
                                                               onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"
                                                               onpaste="return false">
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <LABEL><b>Ponderación</b></label>
                                                        <input class="form-control" type="text" name="oponderacion"
                                                               id="oponderacion" placeholder="0%"
                                                               onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"
                                                        >
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <LABEL><b>Fuente Verificación</b></label>
                                                <textarea name="ofuenteverificacion" id="ofuenteverificacion"
                                                          rows="3" class="form-control"
                                                          style="width:100%;"
                                                          onkeypress="if (this.value.length > 500){return false; }"></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <LABEL><b>Supuesto</b></label>
                                                <textarea name="osupuesto" id="osupuesto"
                                                          rows="3" class="form-control" style="width:100%;"
                                                          type="text"
                                                          onkeypress="if (this.value.length > 500){return false; }"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> <!-- end well -->
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="well" id="caja_prog_mensual">
                                            <div class="row">
                                                <center><label><b id="titulo_indicador"
                                                                  style="color: #00a5c3"></b></label>
                                                </center>
                                                <center>
                                                    <LABEL><b><span style="color: #00A300"> PROGRAMACION MENSUAL <?php echo $this->session->userData('gestion') ?></span></b></label>
                                                </center>
                                                <?php
                                                $mes[1] = ' Enero';
                                                $mes[2] = ' Febrero';
                                                $mes[3] = ' Marzo';
                                                $mes[4] = ' Abril';
                                                $mes[5] = ' Mayo';
                                                $mes[6] = ' Junio';
                                                $mes[7] = ' Julio';
                                                $mes[8] = ' Agosto';
                                                $mes[9] = ' Septiembre';
                                                $mes[10] = ' Octubre';
                                                $mes[11] = ' Noviembre';
                                                $mes[12] = ' Diciembre';
                                                $cont = 1;
                                                while ($cont <= 12) {
                                                    echo '<div class="col-sm-6">
															<div class="form-group text-center">';

                                                    if ($dato_obje['indi_id']== 1) {
                                                        echo '<LABEL ><b> <label class="relativo" style="display: none;">%</label>' . $mes[$cont] . '</b></label>';
                                                    } else {
                                                        echo '<LABEL ><b><label class="relativo">%</label>' . $mes[$cont] . '</b></label>';
                                                    }


                                                    echo '<input class="form-control" type="text" name="mes' . $cont . '" id="mes' . $cont . '" value="0" placeholder="0" onkeypress="if (this.value.length < 8) { return numerosDecimales(event);}else{return false; }" onpaste="return false" >';
                                                    echo '</div>
														</div>';
                                                    $cont++;
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        <br>
                                        <?php if ($dato_obje['indi_id']== 1) { 
                                        echo '<div class="well" id="caja_relativo" name="caja_relativo" style="display: none;"  >';
                                        } else { 
                                        echo '<div class="well" id="caja_relativo" name="caja_relativo" >';
                                         }  ?> 
                                            <div>
                                                <div class="row text-align-center">
                                                    <center><LABEL><b
                                                                style="color: #00a5c3">CARACTERÍSTICA</b></label>
                                                    </center>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <LABEL><b>Total de Casos</b></label>
                                                                <input class="form-control" type="text"
                                                                       name="relativoa"
                                                                       id="relativoa" style="width:100%;"
                                                                       onkeypress="if (this.value.length > 80){return false; }">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <LABEL><b>Casos Favorables</b></label>
                                                                <input class="form-control" type="text"
                                                                       name="relativob"
                                                                       id="relativob" style="width:100%;"
                                                                       onkeypress="if (this.value.length > 80){return false; }">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <LABEL><b>Casos Desfavorables</b></label>
                                                                <input class="form-control" type="text"
                                                                       name="relativoc"
                                                                       id="relativoc" style="width:100%;"
                                                                       onkeypress="if (this.value.length > 80){return false; }">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-actions">
                                    <div class="col-md-1 pull-right">
                                        <a href="<?php echo $site_url.'/prog/obje_gestion/'.$poa_id.'/'.$obje_id ?>"
                                           class="btn btn-lg btn-danger" data-dismiss="modal">CANCELAR </a>
                                    </div>
                                    <div class="pull-right ">
                                        <button type="button" name="guardar_og" id="guardar_og"
                                                class="btn  btn-lg btn-primary"><i
                                                class="fa fa-save guardar_og"></i>
                                            GUARDAR
                                        </button>
                                    </div>
                                </div>


                            </div>
                        </form>
                    </div>
                    <!-- end widget content -->
                </div>
                <!-- end widget -->
            </article>
        </div>
    </div>
    <!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->

