<!-- MAIN PANEL -->
<?php
$site_url = site_url("");
$rol_id = $this->session->userData('rol_id');
?>
<div id="main" role="main">
    <!-- RIBBON -->
    <div id="">
        <!-- breadcrumb -->
        <ol class="breadcrumb">
            <li> Programación del POA</li>
            <li><a href="<?php echo $site_url . '/prog/redobj/'; ?>">Red de Objetivos </a></li>
            <li><a href="<?php echo $site_url . '/prog/obj/' . $poa_id ?>">Acci&oacute;n de Mediano Plazo </a></li>
            <li>
                <a href="<?php echo $site_url . '/prog/obje_gestion/' . $poa_id . '/' . $obje_id ?>">Acci&oacute;n de Corto Plazo</a></li>
            <li> Modificar Registro</li>
        </ol>
    </div>
    <!-- END RIBBON -->
    <?php
    $combo_tindicador = "";
    foreach ($list_indicador as $row) {
        $combo_tindicador .= "<option value='" . $row['indi_id'] . "'>" . $row['indi_descripcion'] . "</option>";
    }

    ?>
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                <section id="widget-grid" class="well">
                    <div class="" style="font-size: 15px">
                        <b>ACCI&Oacute;N DE CORTO PLAZO - MODIFICAR</b> <br>
                        <b>C&oacute;DIGO POA: </b><small class="txt-color-blueLight"><?php echo $dato_poa[0]['poa_codigo'] ?></small><br>
                        <b>CATEGOR&Iacute;A PROGRAM&Aacute;TICA: </b><small class="txt-color-blueLight"><?php echo $dato_poa[0]['aper_programa'] . $dato_poa[0]['aper_proyecto'] .
                                $dato_poa[0]['aper_actividad'] . " - " . $dato_poa[0]['aper_descripcion'] ?></small><br>
                        <b>ACCI&Oacute;N DE MEDIANO PLAZO: </b><small class="txt-color-blueLight"><?php echo $dato_objest[0]['obje_objetivo']  ?></small><br>
                        <b>PDES PILAR: </b><small class="txt-color-blueLight"><?php echo $dato_objest[0]['pdes_pilar']  ?></small><br><b>ACCION: </b><small class="txt-color-blueLight"><?php echo $dato_objest[0]['pdes_accion']  ?></small><br>
                        <b>PTDI EJE: </b><small class="txt-color-blueLight"><?php echo $dato_objest[0]['ptdi_eje']  ?></small><br><b>PROGRAMA: </b><small class="txt-color-blueLight"><?php echo $dato_objest[0]['ptdi_programa']  ?></small><br>
                    </div>
                </section>
            </article>
            <article class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                <section id="widget-grid" class="well">
                    <center>
                        <div class="dropdown">
                            <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenu1" style="width:100%;" data-toggle="dropdown" aria-expanded="true">
                                OPCIONES
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                <li role="presentation">
                                    <a role="menuitem" tabindex="-1" href="<?php echo $site_url . '/prog/obje_gestion/' . $poa_id . '/' . $obje_id ?>">VOLVER ATRAS</a>
                                </li>
                            </ul>
                        </div>
                    </center>
                </section>
            </article>

        </div>
        <div class="row">
            <article class="col-sm-12 col-md-12 col-lg-12">
                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget jarviswidget-color-darken" id="wid-id-1" data-widget-editbutton="false">
                    <header>
                        <span class="widget-icon"> <i class="glyphicon glyphicon-pencil"></i> </span>

                        <h2>MODIFICAR ACCI&Oacute;N DE CORTO PLAZO</h2>
                    </header>
                    <!-- widget content -->
                    <div class="widget-body">
                        <form action="<?php echo $site_url . '/prog/o_guardar' ?>" id="form_mod_ogestion" name="form_mod_ogestion" novalidate="novalidate" method="post">
                            <input type="hidden" name="modificar" id="modificar"
                                   value="<?php echo $dato_o[0]['o_id'] ?>">
                            <input type="hidden" name="poa_id" id="poa_id"
                                   value="<?php echo $dato_poa[0]['poa_id'] ?>">
                            <input type="hidden" name="aper_id" id="aper_id"
                                   value="<?php echo $dato_poa[0]['aper_id'] ?>">
                            <input type="hidden" name="obje_id" id="obje_id"
                                   value="<?php echo $dato_objest[0]['obje_id'] ?>">

                            <div id="bootstrap-wizard-1" class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <div class="well">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <LABEL><b>Responsable</b></label>
                                                        <select name="funcionario" id="funcionario"
                                                                class="form-control">
                                                            <option value=""> Seleccione una opción</option>
                                                            <?php
                                                            foreach ($list_funcionario as $row) {
                                                                $dato = $row['fun_nombre'] . " " . $row['fun_paterno'] . " " . $row['fun_materno'];
                                                                if ($row['fun_id'] == $dato_o[0]['fun_id']) {
                                                                    echo '<option value="' . $row['fun_id'] . '" selected>' . $dato . '</option>';
                                                                } else {
                                                                    echo '<option value="' . $row['fun_id'] . '">' . $dato . '</option>';
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <LABEL><b>Unidad Organizacional</b></label>
                                                        <?php
                                                        if ($rol_id == 1) {
                                                            ?>
                                                            <input type="text" class="form-control" name="uni_unidad"
                                                                   id="uni_unidad" value="<?php echo $dato_o[0]['unidad'] ?>" disabled="disabled">
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <input type="text" name="uni_unidad" id="uni_unidad"
                                                                   class="form-control" disabled="disabled"
                                                                   value="<?php echo $this->session->userData('unidad') ?>">
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br>

                                        <div class="well">
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <LABEL><b>Código</b></label>
                                                        <input value="<?php echo $dato_o[0]['o_codigo'] ?>"
                                                               disabled="disabled" class="form-control" type="text"
                                                               name="ocodigo" id="ocodigo">
                                                    </div>
                                                </div>
                                                <div class="col-sm-9">
                                                    <div class="form-group">
                                                        <LABEL><b>Acci&oacute;n de Corto Plazo</b></label>
                                                        <textarea name="oobjetivo" id="oobjetivo" style="width:100%;"
                                                                  rows="4" class="form-control custom-scroll"
                                                                  onpaste="return false"
                                                                  onkeypress="if (this.value.length > 500){return false; }"><?php echo $dato_o[0]['o_objetivo'] ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <LABEL><b>Tipo Indicador? </b></label><br>
                                                                    
                                                                <select class="form-control" id="tipo_indicador" name="tipo_indicador" onChange="pagoOnChange(this)">
                                                                    <option value="">Seleccione</option>
                                                                    <?php
                                                                    foreach ($list_indicador as $row) {
                                                                        if ($row['indi_id'] == $dato_o[0]['indi_id']) {
                                                                            echo '<option value="' . $row['indi_id'] . '" selected>' . $row['indi_descripcion'] . '</option>';
                                                                        } else {
                                                                            echo '<option value="' . $row['indi_id'] . '">' . $row['indi_descripcion'] . '</option>';
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <?php
                                                        if ($dato_o[0]['indi_id'] == 1) {
                                                            ?>
                                                            <div id="caja_relativo2" name="caja_relativo2"
                                                                 style="display: none;">
                                                                <div class="col-sm-12">
                                                                    <label for=""><b>Denominador</b></label>
                                                                    <select class="form-control" id="o_denominador"
                                                                            name="o_denominador"
                                                                            onChange="denominador(this)">
                                                                        <option value="0">Variable</option>
                                                                        <option value="1">Fijo</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <div id="caja_relativo2" name="caja_relativo2">
                                                                <div class="col-sm-12">
                                                                    <label for=""><b>Denominador</b></label>
                                                                    <select class="form-control" id="o_denominador"
                                                                            name="o_denominador"
                                                                            onChange="denominador(this)">
                                                                        <?php
                                                                        if ($dato_o[0]['o_denominador'] == 0) {
                                                                            ?>
                                                                            <option value="0" selected>Variable
                                                                            </option>
                                                                            <option value="1">Fijo</option>
                                                                            <?php
                                                                        } else {
                                                                            ?>
                                                                            <option value="0">Variable</option>
                                                                            <option value="1" selected>Fijo</option>
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <?php
                                                        }
                                                        ?>

                                                    </div>


                                                </div>


                                                <div class="col-sm-9">
                                                    <div class="form-group">
                                                        <LABEL><b>Indicador</b></label>
                                                <textarea name="oindicador" id="oindicador" style="width:100%;"
                                                          rows="4" class="form-control custom-scroll"
                                                          onkeypress="if (this.value.length > 500){return false; }"><?php echo $dato_o[0]['o_indicador'] ?></textarea>
                                                    </div>
                                                </div>


                                            </div>
                                            <div class="row" style="padding-top: 10px;">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <LABEL><b>Fórmula</b></label>
                                                        <?php
                                                        if ($dato_o[0]['indi_id'] == 1) {
                                                            echo '<textarea name="oformula" id="oformula" onpaste="return false" disabled="disabled"
                                                          style="width:100%;"
                                                          rows="3" class="form-control custom-scroll"
                                                          onkeypress="if (this.value.length > 500){return false; }">N/A</textarea>';
                                                        } else {
                                                            echo '<textarea name="oformula" id="oformula" onpaste="return false"
                                                                        style="width:100%;"
                                                                        rows="3" class="form-control custom-scroll"
                                                                        onkeypress="if (this.value.length > 500){return false; }">' . $dato_o[0]['o_formula'] . '</textarea>';
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="alert alert-block alert-success">
                                                        <b>La suma de la programación mensual mas la línea base debe ser igual a la meta. </b><br>
                                                        <center><b style="align-content: center">( <span style="color: #00A300">PROGRAMACION MENSUAL </span> +
                                                                <span style="color: #0000cc">LINEA BASE</span> ) =
                                                                <span style="color: #f00000"> META </span></b></center>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <LABEL><b><span style="color: #0000cc">LÍNEA BASE</span></b></label>
                                                        <input class="form-control"
                                                               value="<?php echo round($dato_o[0]['o_linea_base'], 1) ?>"
                                                               type="text" name="olineabase" id="olineabase"
                                                               placeholder="0"
                                                               onkeypress="if (this.value.length < 6) { return numerosDecimales(event);}else{return false; }"
                                                               onpaste="return false">
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <?php
                                                        if ($dato_o[0]['indi_id'] == 1) {
                                                            echo '<LABEL><b><span style="color: #f00000">Meta</span></b><span id="tipo_meta"> </span></label>';
                                                        } else {
                                                            echo '<LABEL><b><span style="color: #f00000">Meta</span></b><span id="tipo_meta"> -% </span></label>';
                                                        }
                                                        ?>
                                                        <input name="ometa" id="ometa" class="form-control"
                                                               value="<?php echo round($dato_o[0]['o_meta'], 1) ?>"
                                                               type="text"
                                                               placeholder="0"
                                                               onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"
                                                               onpaste="return false">
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <LABEL><b>Ponderación</b></label>
                                                        <input class="form-control"
                                                               value="<?php echo round($dato_o[0]['o_ponderacion'], 1) ?>"
                                                               type="text" name="oponderacion" id="oponderacion"
                                                               placeholder="0%"
                                                               onkeypress="if (this.value.length < 6) { return numerosDecimales(event);}else{return false; }"
                                                               onpaste="return false">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <LABEL><b>Fuente Verificación</b></label>
                                                <textarea name="ofuenteverificacion" id="ofuenteverificacion"
                                                          style="width:100%;"
                                                          rows="3" class="form-control custom-scroll"
                                                          onpaste="return false"
                                                          onkeypress="if (this.value.length > 500){return false; }"><?php echo $dato_o[0]['o_fuente_verificacion'] ?></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <LABEL><b>Supuesto</b></label>
                                                <textarea name="osupuesto" id="osupuesto" style="width:100%;"
                                                          rows="3" class="form-control custom-scroll"
                                                          onpaste="return false"
                                                          onkeypress="if (this.value.length > 500){return false; }"><?php echo $dato_o[0]['o_supuestos'] ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="well">
                                            <div class="row">
                                                <center>
                                                    <LABEL><b><span
                                                                style="color: #00A300"> PROGRAMACION MENSUAL <?php echo $this->session->userData('gestion') ?></span></b></label>
                                                </center>
                                                <?php
                                                $mes[1] = ' Enero';
                                                $mes[2] = ' Febrero';
                                                $mes[3] = ' Marzo';
                                                $mes[4] = ' Abril';
                                                $mes[5] = ' Mayo';
                                                $mes[6] = ' Junio';
                                                $mes[7] = ' Julio';
                                                $mes[8] = ' Agosto';
                                                $mes[9] = ' Septiembre';
                                                $mes[10] = ' Octubre';
                                                $mes[11] = ' Noviembre';
                                                $mes[12] = ' Diciembre';
                                                $cont = 1;
                                                while ($cont <= 12) {
                                                    echo '<div class="col-sm-6">
															<div class="form-group text-center">';
                                                    if ($dato_o[0]['indi_id'] == 1) {
                                                        echo '<LABEL ><b> <label class="relativo" style="display: none;">%</label>' . $mes[$cont] . '</b></label>';
                                                    } else {
                                                        echo '<LABEL ><b><label class="relativo">%</label>' . $mes[$cont] . '</b></label>';
                                                    }
                                                    echo '<input class="form-control" value="' . round($programacion['p' . $cont], 2) . '" type="text" name="mes' . $cont . '" id="mes' . $cont . '" value="0" placeholder="0" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }" onpaste="return false">';
                                                    echo '</div>
														</div>';
                                                    $cont++;
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        <br>

                                        <div class="well">
                                            <br>
                                            <?php
                                            if ($dato_o[0]['indi_id'] == 2) {
                                                ?>
                                                <div id="caja_relativo" name="caja_relativo">
                                                    <div class="row ">
                                                        <center><LABEL><b
                                                                    style="color: #00a5c3">CARACTERÍSTICA</b></label>
                                                        </center>
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="form-group">
                                                                    <LABEL><b>Total de Casos</b></label>
                                                                    <input class="form-control"
                                                                           value="<?php echo $dato_o[0]['o_total_casos'] ?>"
                                                                           type="text"
                                                                           name="relativoa" id="relativoa"
                                                                           style="width:100%;"
                                                                           onkeypress="if (this.value.length > 60){return false; }">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="form-group">
                                                                    <LABEL><b>Casos Favorables</b></label>
                                                                    <input class="form-control"
                                                                           value="<?php echo $dato_o[0]['o_casos_favorables'] ?>"
                                                                           type="text"
                                                                           name="relativob" id="relativob"
                                                                           style="width:100%;"
                                                                           onkeypress="if (this.value.length > 60){return false; }">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="form-group">
                                                                    <LABEL><b>Casos Desfavorables</b></label>
                                                                    <input class="form-control"
                                                                           value="<?php echo $dato_o[0]['o_casos_desfavorables'] ?>"
                                                                           type="text"
                                                                           name="relativoc" id="relativoc"
                                                                           style="width:100%;"
                                                                           onkeypress="if (this.value.length > 60){return false; }">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            } else {
                                                ?>
                                                <div id="caja_relativo" name="caja_relativo" style="display: none;">
                                                    <div class="row">
                                                        <center><LABEL><b
                                                                    style="color: #00a5c3">CARACTERÍSTICA</b></label>
                                                        </center>
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="form-group">
                                                                    <LABEL><b>Total de Casos</b></label>
                                                                    <input class="form-control" type="text"
                                                                           name="relativoa"
                                                                           id="relativoa" style="width:100%;"
                                                                           onkeypress="if (this.value.length > 60){return false; }">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="form-group">
                                                                    <LABEL><b>Casos Favorables</b></label>
                                                                    <input class="form-control" type="text"
                                                                           name="relativob"
                                                                           id="relativob" style="width:100%;"
                                                                           onkeypress="if (this.value.length > 60){return false; }">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="form-group">
                                                                    <LABEL><b>Casos Desfavorables</b></label>
                                                                    <input class="form-control" type="text"
                                                                           name="relativoc"
                                                                           id="relativoc" style="width:100%;"
                                                                           onkeypress="if (this.value.length > 60){return false; }">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                            ?>


                                        </div>
                                    </div>
                                </div>
                                <br>

                                <div class="row">
                                    <div class="col-md-1 pull-right">
                                        <a href="<?php echo $site_url . '/prog/obje_gestion/' . $poa_id . '/' . $obje_id ?>"
                                           class="btn btn-lg btn-danger" data-dismiss="modal">CANCELAR </a>
                                    </div>
                                    <div class="pull-right">
                                        <button type="button" name="og_modificar" id="og_modificar"
                                                class="btn  btn-lg btn-primary"><i
                                                class="fa fa-save guardar_og"></i>
                                            MODIFICAR
                                        </button>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </article>
        </div>
    </div>
</div>

