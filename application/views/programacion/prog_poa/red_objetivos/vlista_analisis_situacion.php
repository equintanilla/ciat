<script xmlns="http://www.w3.org/1999/html">
    function abreVentana(PDF) {
        var direccion;
        direccion = '' + PDF;
        window.open(direccion, "Reporte de Proyectos", "width=800,height=650,scrollbars=SI");
    };
</script>
<div id="main" role="main">
    <div id="">
        <ol class="breadcrumb">
            <li>Programación</li><li> Analisis de Situación</li><li>FODA</li>
        </ol>
    </div>
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-12 animated fadeInDown">
                <h1 class="page-title txt-color-blueDark"><i class="fa fa-pencil-square-o fa-fw "></i> Analisis de Situación</h1>
            </div>
        </div>
        <section id="widget-grid" class="">
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-teal" id="wid-id-0" data-widget-editbutton="false">
                        <header>
                            <span class="widget-icon"> <span class="fa fa-table"></span> </span>
                            <h2>PROGRAMAS PARA REGISTRO DE ANALISIS DE SITUACI&Oacute;N</h2>
                        </header>
                        <div>
                            <div class="widget-body no-padding">
                                <div class="table-responsive">
                                    <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                        <thead>
                                        <tr>
                                            <th><center> REGISTRO <BR>DE<BR>ANALISIS DE SITUACI&Oacute;N </center></th>
                                            <!-- <th>CÓDIGO</th> -->
                                            <th>APERTURA PROGRAMÁTICA</th>
                                            <th>UNIDAD ORGANIZACIONAL</th>
                                            <th>FECHA DE CREACIÓN</th>
                                        </tr>
                                        </thead>
                                        <tbody id="bdi">
                                        <?php
                                        foreach($lista_poa as $row)
                                        {
                                            ?>
                                            
                                            <?php
                                            echo '<tr id="tr'.$row['poa_id'].'">';
                                            echo'<td>';
                                            ?><center>
                                                <a href="<?php echo site_url('admin').'/prog/analisis/'.$row['poa_id'] ?>"
                                                    data-toggle="tooltip" title="VER">
                                                    <img src="<?php echo base_url();?>/assets/ifinal/archivo.png" width="40" height="40"
                                                    class="img-responsive "title="VER DETALLE">
                                                </a>
                                                </center>
                                            <?php
                                            '</td>';
                                            /*echo '<td><font size="1">'.$row['poa_codigo'].'</font></td>';*/
                                            echo '<td><font size="1">'.$row['aper_programa'].$row['aper_proyecto'].$row['aper_actividad']." - ".$row['aper_descripcion'].'</font></td>';
                                            echo '<td><font size="1">'.$row['uni_unidad'].'</font></td>';
                                            echo '<td><font size="1">'.$row['poa_fecha_creacion'].'</font></td>';
                                            echo '</tr>';
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </section>
    </div>
</div>
