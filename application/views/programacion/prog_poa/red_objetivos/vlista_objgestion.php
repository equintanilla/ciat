<!-- MAIN PANEL -->
<?php
$site_url = site_url("");
$poa_id = $dato_poa['poa_id'];
?>
<script xmlns="http://www.w3.org/1999/html">
    function abreVentana(PDF) {
        var direccion;
        direccion = '' + PDF;
        window.open(direccion, "Reporte de Proyectos", "width=800,height=650,scrollbars=SI");
    }
</script>
<style>
    table{
        font-size: 9px;
        width: 100%;
        max-width:1550px;;
        overflow-x: scroll;
        }
        th{
          padding: 1.4px;
          text-align: center;
          font-size: 9px;
        }
        td{
          padding: 1.4px;
          text-align: center;
          font-size: 9px;
        }
</style>
<div id="main" role="main">
    <div>
        <ol class="breadcrumb">
            <li> Programación del POA</li>
            <li><a href="<?php echo $site_url . '/prog/redobj/'; ?>">Red de Objetivos </a></li>
            <li><a href="<?php echo $site_url . '/prog/obj/' . $poa_id ?>">Acci&oacute;n de Mediano Plazo</a></li>
            <li> Acci&oacute;n de Corto Plazo</li>
        </ol>
    </div>
    <div id="content">
        <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                <section id="widget-grid" class="well">
                    <div class="" style="font-size: 15px">
                        <b>ACCI&Oacute;N DE CORTO PLAZO</b> <br>
                        <b>C&oacute;DIGO POA: </b><small class="txt-color-blueLight"><?php echo $dato_poa['poa_codigo'] ?></small><br>
                        <b>CATEGOR&Iacute;A PROGRAM&Aacute;TICA: </b><small class="txt-color-blueLight"><?php echo $dato_poa['aper_programa'] . $dato_poa['aper_proyecto'] .
                                $dato_poa['aper_actividad'] . " - " . $dato_poa['aper_descripcion'] ?></small><br>
                        <b>GESTI&Oacute;N: </b><small class="txt-color-blueLight"><?php echo $dato_poa['poa_gestion'] ?></small><br>
                        <b>ACCI&Oacute;N DE MEDIANO PLAZO: </b><small class="txt-color-blueLight"><?php echo $dato_objest['obje_codigo'] . ' -- ' . $dato_objest['obje_objetivo'] ?></small><br>

                    </div>
                </section>
            </article>
            <article class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                <section id="widget-grid" class="well">
                    <center>
                        <div class="dropdown">
                            <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenu1" style="width:100%;" data-toggle="dropdown" aria-expanded="true">
                                OPCIONES
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                <li role="presentation">
                                    <a role="menuitem" tabindex="-1" href="<?php echo $site_url . '/prog/o_nuevo/' . $poa_id . '/' . $obje_id; ?>">NUEVA ACCION DE CORTO PLAZO</a>
                                </li>
                                <li role="presentation">
                                    <a role="menuitem" tabindex="-1" href="javascript:abreVentana('<?php echo site_url("admin") . '/objetivos_gestion/'.$poa_id.'/'.$obje_id; ?>');">
                                        REPORTE</a>
                                </li>

                                <li role="presentation">
                                    <a role="menuitem" tabindex="-1" href="<?php echo $site_url . '/prog/obj/' . $poa_id ?>">VOLVER ATRAS</a>
                                </li>
                            </ul>
                        </div>
                    </center>
                </section>
            </article>

        </div>

        <?php
        $mensaje = $this->session->flashdata('modificar');
        if ($mensaje) {
            ?>
            <div class="alert alert-block alert-success">
                <a class="close" data-dismiss="alert" href="#">×</a>
                <h1 class="alert-heading"><i class="fa fa-check-square-o"></i> <?php echo $mensaje ?></h1>
            </div>
            <?php
        }
        $mensaje = $this->session->flashdata('guardar');
        if ($mensaje) {
            ?>
            <div class="alert alert-block alert-success">
                <a class="close" data-dismiss="alert" href="#">×</a>
                <h1 class="alert-heading"><i class="fa fa-check-square-o"></i> <?php echo $mensaje ?></h1>
            </div>
            <?php
        }
        $mensaje = $this->session->flashdata('guardar_archivo');
        if ($mensaje) {
            ?>
            <div class="alert alert-block alert-success">
                <a class="close" data-dismiss="alert" href="#">×</a>
                <h1 class="alert-heading"><i class="fa fa-check-square-o"></i> <?php echo $mensaje ?></h1>
            </div>
            <?php
        }
        $mensaje = $this->session->flashdata('error');
        if ($mensaje) {
            ?>
            <div class="alert alert-block alert-danger">
                <a class="close" data-dismiss="alert" href="#">×</a>
                <h1 class="alert-heading"><i class="fa fa-check-square-o"></i> <?php echo $mensaje ?></h1>
            </div>
            <?php
        }
        ?>
       <!--
            <div class="row">
                <a href="<?php /*echo $site_url . '/prog/obj/' . $poa_id */?>"
                   class="btn btn-labeled btn-success" title="VOLVER LISTA"> <span
                        class="btn-label"><i
                            class="fa fa-arrow-left" aria-hidden="true"></i></span><font size="1">ATRAS </font></a>

                <a href="<?php /*echo $site_url . '/prog/o_nuevo/' . $poa_id . '/' . $obje_id; */?>"
                   class="btn btn-labeled btn-primary" title="NUEVO"> <span class="btn-label"><i
                            class="fa fa-plus-square" aria-hidden="true"></i></span><font size="1">
                        NUEVO REGISTRO</font></a>

                <a href="javascript:abreVentana('<?php /*echo site_url("admin") . '/objetivos_gestion/'.$poa_id.'/'.$obje_id; */?>');"
                       class="btn btn-labeled btn-danger" title="REPORTE">
                    <span class="btn-label"><i class="fa fa-file-pdf-o"></i></span><b>REPORTE</b></a><br>
            </div>
        </div>
        <br>-->


        <!-- TABLA DE LISTA DE OBJETIVO DE GESTION -->
        <section id="widget-grid" class="">
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <?php
                    if (count($lista_objgestion) == 0) {
                        ?>
                        <div class="row">
                            <div class="alert alert-block alert-warning">
                                <a class="close" data-dismiss="alert" href="#">×</a>

                                <h1 class="alert-heading"> No existen Acciones de corto plazo cargados.</h1>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="true">
                        <header>
                            <span class="widget-icon"> <span class="glyphicon glyphicon-folder-close"></span></span>
                            <h2>LISTA DE ACCIONES DE CORTO PLAZO</h2>
                        </header>
                        <div>
                            <div class="widget-body no-padding">
                                <div class="table-responsive">
                                    <table id="dt_basic" class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th>
                                                <a href="<?php echo $site_url . '/prog/o_nuevo/' . $poa_id . '/' . $obje_id;  ?>" style="text-decoration: none"
                                                   title="NUEVA ACCIÓN DE CORTO PLAZO">
                                                    <img src="<?php echo base_url(); ?>assets/ifinal/2.png" width="30" height="30">
                                                </a>
                                                <br style="font-size: 20px">Nuevo Acc.
                                            </th>
                                            <th style="width:80%;">REGISTRO DE<BR>PRODUCTOS TERMINALES</th>
                                            <th>CÓDIGO</th>
                                            <th>ACCI&Oacute;N DE CORTO PLAZO</th>
                                            <th>RESPONSABLE</th>
                                            <th>UNIDAD ORGANIZACIONAL</th>
                                            <th>TIPO DE INDICADOR</th>
                                            <th>INDICADOR DE RESULTADO</th>
                                            <th>LÍNEA BASE</th>
                                            <th>META</th>
                                            <th>FUENTE VERIFICACION</th>
                                            <th>CRONOGRAMA DE EJECUCI&Oacute;N <?php echo $this->session->userData('gestion'); ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        foreach ($lista_objgestion as $row) {
                                            $this->db->SELECT('p.*,f.fun_nombre,f.fun_paterno,f.fun_materno,(SELECT get_unidad(f.uni_id)) AS unidad,i.indi_abreviacion');
                                            $this->db->FROM('_productoterminal p');
                                            $this->db->JOIN('funcionario f', 'p.fun_id = f.fun_id','LEFT');
                                            $this->db->JOIN('indicador i', 'i.indi_id = p.indi_id','LEFT');
                                            $this->db->WHERE('(p.pt_estado = 1 OR p.pt_estado = 2)');
                                            $this->db->WHERE('p.o_id', $row['o_id'] );
                                            $this->db->WHERE('p.pt_gestion', $dato_poa['poa_gestion']);
                                            $query = $this->db->get();
                                            $di = "";
                                            if ( $query->num_rows() > 0) {
                                                $di = "ASIGNAR PRODUCTO TERMINAL";
                                            } else {
                                                $di = "SIN PRODUCTO TERMINAL";
                                            }
                                            echo '<tr id="tr'.$row['o_id'].'">';
                                            //generar mis botones
                                            ?>
                                            <td>
                                                <a href="<?php echo $site_url.'/prog/o_mod/'.$poa_id.'/'.$obje_id.'/'.$row['o_id'] ?>"
                                                   title="MODIFICAR" class=" modificar" name="<?php echo $row['o_id'] ?>" id="modificar">
                                                    <img src="<?php echo base_url(); ?>assets/ifinal/modificar.png" WIDTH="40" HEIGHT="40"/>
                                                </a>
                                                <p style="font-size: 8px"><b>Modificar</b></p>
                                                <a href="" title="ELIMINAR" class="del_ogestion"
                                                   name="<?php echo $row['o_id'] ?>" id="del_ogestion">
                                                    <img src="<?php echo base_url(); ?>assets/ifinal/eliminar.png"
                                                         WIDTH="40" HEIGHT="40"/>
                                                </a>
                                                <p style="font-size: 8px"><b>Eliminar</b></p>
                                                <a href="" title="INDICADOR DE DESEMPEÑO" class=" o_indicador_desempenio"
                                                   name="<?php echo $row['o_id'] ?>" id="o_indicador_desempenio"
                                                   data-toggle="modal" data-target="#modal_indicador_desem">
                                                    <img src="<?php echo base_url(); ?>assets/ifinal/form1.jpg"
                                                         WIDTH="35" HEIGHT="35"/>
                                                </a>
                                                <p style="font-size: 8px"><b>Indicador</b></p>
                                                <a href="" title="DOCUMENTO" class="o_pdf"
                                                   name="<?php echo $row['o_id'] ?>" id="o_pdf"
                                                   data-toggle="modal" data-target="#modal_cargar_pdf">
                                                    <img src="<?php echo base_url(); ?>assets/ifinal/doc.png"
                                                         WIDTH="40" HEIGHT="40"/>
                                                </a>
                                                <p style="font-size: 8px"><b>Archivo</b></p>
                                            </td>
                                            <?php
                                            //------------------------------------------------------------------------------------------------
                                            echo '<td><center><a href="' . site_url("") . '/prog/pterminal/' .
                                                $poa_id . '/' . $obje_id . '/' . $row['o_id'] . '">
                                            <img src="' . base_url() . 'assets/ifinal/archivo.png" width="40" height="40"
                                            class="img-responsive "title="'.$di.'">
                                            </a></center></td>';
                                            echo '<td>' . $row['o_codigo'] . '</td>';
                                            echo '<td>' . $row['o_objetivo'] . '</td>';
                                            echo '<td>' . $row['fun_nombre'] . ' ' . $row['fun_paterno'] . ' ' . $row['fun_materno'] . '</td>';
                                            echo '<td>' . $row['unidad'] . '</td>';
                                            echo '<td>' . $row['indi_abreviacion'] . '</td>';
                                            echo '<td>' . $row['o_indicador'] . '</td>';
                                            echo '<td>' . number_format($row['o_linea_base'],1,'.','') . '</td>';
                                            echo '<td>' . number_format($row['o_meta'],1,'.','') . '</td>';
                                            echo '<td>' . $row['o_fuente_verificacion'] . '</td>';

                                            //-----------------------------TABLA------------------------------------------------------------------
                                            $indi[1] = '';
                                            $indi[2] = '%';
                                            $porc = $indi[$row['indi_id']];
                                            echo '<td>';
                                            echo '<table class="table table-bordered">
                                                          <thead>';
                                            //---------------------- CABECERA DE GESTIONES
                                            echo '<tr>';
                                            echo '<td  bgcolor="#2F4F4F">
                                                        <center>
                                                        <button type="button" class="btn btn-primary btn-xs graf_ogestion" name="' . $row["o_id"] . '" id="graf_ogestion"
                                                        data-toggle="modal" data-target="#modal_grafico"
                                                        title="PROGRAMACION">
                                                        <span class="glyphicon glyphicon-stats" aria-hidden="true">
                                                        </center>
                                                  </td>';
                                            $mes[1] = 'ENERO';
                                            $mes[2] = 'FEBRERO';
                                            $mes[3] = 'MARZO';
                                            $mes[4] = 'ABRIL';
                                            $mes[5] = 'MAYO';
                                            $mes[6] = 'JUNIO';
                                            $mes[7] = 'JULIO';
                                            $mes[8] = 'AGOSTO';
                                            $mes[9] = 'SEPTIEMBRE';
                                            $mes[10] = 'OCTUBRE';
                                            $mes[11] = 'NOVIEMBRE';
                                            $mes[12] = 'DICIEMBRE';
                                            for ($i = 1; $i <= 12; $i++) {
                                                echo '<td  bgcolor="#2F4F4F"><center><b><font color="#ffffff" size="1">' . $mes[$i] . '</font></b></center></td>';
                                            }
                                            echo '</tr>';
                                            //---------------------- FIN DE CABECERA
                                            $o_id = $row['o_id'];
                                            //---------------------- PROGRAMACION
                                            echo '<tr>';
                                            echo '<td bgcolor="#F5F5DC"><center><b><font color="#000000" size="1">P</font></b></center></td>';
                                            for ($i = 1; $i <= 12; $i++) {
                                                $puntero = 'p' . $i;
                                                $prog = $programacion[$o_id][$puntero];
                                                echo ' <td  bgcolor="#F5F5DC"><center><font color="#000000" size="1">' . round($prog, 1) . $porc . '</font></center></td>';
                                            }
                                            echo '</tr>';
                                            //--------------------- PROGRAMACION ACUMULADA
                                            echo '<tr>';
                                            echo '<td  bgcolor="#98FB98"><center><b><font color="#000000" size="1">P.A</font></b></center></td>';
                                            for ($i = 1; $i <= 12; $i++) {
                                                $puntero_acumulado = 'p_a' . $i;
                                                $prog_acumulado = $programacion[$o_id][$puntero_acumulado];
                                                echo ' <td bgcolor="#98FB98"><center><font color="#000000" size="1">' . round($prog_acumulado, 1) . $porc . '</font></center></td>';
                                            }
                                            echo '</tr>';
                                            //--------------------- PROGRAMACION ACUMULADA PORCENTUAL
                                            echo '<tr>';
                                            echo '<td bgcolor="#B0E0E6"><center><b><font color="#000000" size="1">%P.A</font></b></center></td>';
                                            for ($i = 1; $i <= 12; $i++) {
                                                $puntero_pa_porcentual = 'pa_porc' . $i;
                                                $pa_porcentual = $programacion[$o_id][$puntero_pa_porcentual];
                                                echo ' <td bgcolor="#B0E0E6"><center><font color="#000000" size="1">' . round($pa_porcentual, 1) . $porc . '%</font></center></td>';
                                            }
                                            echo '</tr>';
                                            echo '              </thead>
                                                        </table>';
                                            echo '</td>';
                                            echo '</tr>';
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </section>
    </div>
    <!-- Large modal -->
</div>
<!-- ------------------------------- MODAL DE GRAFICO OBJETIVO DE GESTION-------------- -->
<div class="modal fade bs-example-modal-lg" id="modal_grafico" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="col-md-12 des">
                <center>PROGRAMACI&Oacute;N OBJETIVO DE GESTI&Oacute;N</center>
            </div>
            <table class="table table-bordered" style="width:100%;">
                <tbody id="tabla_grafico">
                </tbody>
            </table>
            <div class="row">
                <div class="col-md-4 des">
                    CÓDIGO: <span class="badge" id="codigo"></span>
                </div>
                <div class="col-md-4 des">
                    L&Iacute;NEA BASE:<span class="badge" id="linea_base"></span>
                </div>
                <div class="col-md-4 des">
                    META: <span class="badge" id="meta"></span>
                </div>
            </div>

            <div class="row">
                <div id="" class="col-md-12">
                    <div id="grafico_objetivo" class="graf">
                    </div>
                </div>

            </div>


        </div>
    </div>
</div>
<!-- =============================      Modal INDICADOR DE DESEMPEÑO     ===========================-->
<div class="modal fade bs-example-modal-lg " id="modal_indicador_desem" role="dialog"
     data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close text-danger" data-dismiss="modal" aria-hidden="true">
                    <i class="fa fa-times-circle" aria-hidden="true"></i></button>
                <h4 class="modal-title text-center text-info">
                    <b><i class="glyphicon glyphicon-pencil"></i>INDICADOR DE DESEMPE&Ntilde;O</b>
                    <input type="hidden" id="o_id_desem" name="o_id_desem">
                </h4>
            </div>
            <div class="modal-body no-padding">
                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false"
                     data-widget-deletebutton="false">
                    <header>
                        <div class="row text-center">
                            <h2>Ingrese los Datos del Indicador de Desempe&ntilde;o</h2>
                        </div>
                    </header>
                    <!-- widget content -->
                    <div class="widget-body">
                        <form id="form_add_indicador" name="form_add_indicador" novalidate="novalidate" method="post">
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <LABEL><b>C&oacute;digo</b></label>
                                        <input class="form-control" type="text"
                                               id="modal_o_codigo"
                                               disabled="disabled">
                                    </div>
                                </div>
                                <div class="col-sm-9">
                                    <div class="form-group">
                                        <LABEL><b style="color: #2B3D53;">OBJETIVO DE GESTI&Oacute;N: </b>
                                            <b><font id="objetivo_texto"></font></b></label>

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-9">
                                    <div class="form-group">
                                        <label><b>EFICACIA</b></label>
                                               <textarea name="modal_o_eficacia" id="modal_o_eficacia" style="width:100%;"
                                                         style="text-transform:uppercase;"
                                                         onkeyup="javascript:this.value=this.value.toUpperCase();"
                                                         rows="2" class="form-control custom-scroll"
                                                         onkeypress="if (this.value.length > 500){return false; }"></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-3 text-align-left">
                                    <div class="form-group"><br>
                                        <label><b style="color: #2B3D53;"> > 100% M&Aacute;S EFICAZ</b></label><br>
                                        <label><b style="color: #2B3D53;"> = 100% EFICAZ</b></label><br>
                                        <label><b style="color: #2B3D53;"> < 100% MENOS EFICAZ</b></label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-9">
                                    <div class="form-group">
                                        <label><b>EFICIENCIA FINANCIERA</b></label>
                                               <textarea name="modal_o_financiera" id="modal_o_financiera" style="width:100%;"
                                                         style="text-transform:uppercase;"
                                                         onkeyup="javascript:this.value=this.value.toUpperCase();"
                                                         rows="2" class="form-control custom-scroll"
                                                         onkeypress="if (this.value.length > 500){return false; }"></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-3 text-align-left">
                                    <div class="form-group"><br>
                                        <label><b style="color: #2B3D53;"> > 100% MENOS EFICIENTE</b></label><br>
                                        <label><b style="color: #2B3D53;"> = 100% EFICIENTE</b></label><br>
                                        <label><b style="color: #2B3D53;"> < 100% M&Aacute;S EFICIENTE</b></label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-9">
                                    <div class="form-group">
                                        <label><b>EFICIENCIA EN EL PLAZO DE EJECUCI&Oacute;N</b></label>
                                               <textarea name="modal_o_ejecucion" id="modal_o_ejecucion" style="width:100%;"
                                                         style="text-transform:uppercase;"
                                                         onkeyup="javascript:this.value=this.value.toUpperCase();"
                                                         rows="2" class="form-control custom-scroll"
                                                         onkeypress="if (this.value.length > 500){return false; }"></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-3 text-align-left">
                                    <div class="form-group"><br>
                                        <label><b style="color: #2B3D53;"> > 100% MENOS EFICIENTE</b></label><br>
                                        <label><b style="color: #2B3D53;"> = 100% EFICIENTE</b></label><br>
                                        <label><b style="color: #2B3D53;"> < 100% M&Aacute;S EFICIENTE</b></label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-9">
                                    <div class="form-group">
                                        <label><b>EFICIENCIA F&Iacute;SICA</b></label>
                                               <textarea name="modal_o_fisica" id="modal_o_fisica" style="width:100%;"
                                                         style="text-transform:uppercase;"
                                                         onkeyup="javascript:this.value=this.value.toUpperCase();"
                                                         rows="2" class="form-control custom-scroll"
                                                         onkeypress="if (this.value.length > 500){return false; }"></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-3 text-align-left">
                                    <div class="form-group"><br>
                                        <label><b style="color: #2B3D53;"> > 100% MENOS EFICIENTE</b></label><br>
                                        <label><b style="color: #2B3D53;"> = 100% EFICIENTE</b></label><br>
                                        <label><b style="color: #2B3D53;"> < 100% M&Aacute;S EFICIENTE</b></label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <ul class="pager wizard no-margin">
                                        <li class="previous disabled" style="float: left">
                                            <button type="button" class="btn  btn-ms btn-danger" data-dismiss="modal">
                                                CANCELAR
                                            </button>
                                        </li>
                                        <li class="next" style="float: right;">
                                            <button type="button"
                                                    class="btn  btn-ms btn-primary" id="guardar_indicador"
                                                    name="guardar_indicador">
                                                GUARDAR
                                            </button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--</form>-->
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- =============================       MODAL SUBIR PDF                 ========================== -->
<div class="modal animated fadeInDown" id="modal_cargar_pdf" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close text-danger " data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h3 class="modal-title text-center">
                    <b><i class="glyphicon glyphicon-circle-arrow-up"></i> <span id="titulo_archivo">SUBIR ARCHIVO </span></b>
                </h3>
            </div>
            <form action="<?php echo $site_url . '/prog/add/o_pdf' ?>" enctype="multipart/form-data"
                  id="oform_subir_pdf" name="oform_subir_pdf" novalidate="novalidate" method="post">
                <input type="hidden" name="poa_id" id="poa_id" value="<?php echo $poa_id ?>">
                <input type="hidden" name="obje_id" id="obje_id" value="<?php echo $obje_id ?>">
                <input type="hidden" name="id_o_pdf" id="id_o_pdf">
                <input type="hidden" name="mod_eli" id="mod_eli">

                <div class="modal-body no-padding">
                    <div class="row">
                        <div id="bootstrap-wizard-1" class="col-sm-12">
                            <div class="well">
                                <div class="row">
                                    <label><b style="font-size: 15px">C&oacute;digo Objetivo </b></label>

                                    <div class="col-sm-10">
                                        <input type="text" name="codigo_mpdf" id="codigo_mpdf" disabled="disabled" class="form-control">
                                    </div>
                                </div>
                                <br>

                                <div class="row">
                                    <LABEL><b style="font-size: 15px">Subir archivo PDF menor a 5 mb</b></label>

                                    <div class="col-sm-11">
                                        <div class="form-group">
                                            <LABEL><b STYLE="font-size: 12px">Seleccionar Archivo</b></label>
                                            <input class="form-control" type="file" name="userfile"
                                                   id="userfile" placeholder="Seleccione el Archivo" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-1">
                                        <label for=""> </label>
                                        <img id="load" style="display: none"
                                             src="<?php echo base_url() ?>/assets/img/loading.gif" width="30"
                                             height="30">
                                    </div>
                                </div>
                            </div> <!-- end well -->
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row text-align-center" style="align-content: center">
                        <div class="col-md-3">
                            <button type="button" class="btn btn-warning btn-sm" data-dismiss="modal">
                                 <span>
                                     <i class="fa fa-times" aria-hidden="true"></i>
                                 </span>
                                <font size="2">CANCELAR </font>
                            </button>
                        </div>
                        <div class="col-md-3">
                            <a class="btn btn-success btn-sm"
                               target="_blank" id="over_pdf" name="over_pdf">
                                 <span>
                                     <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                                 </span>
                                <font size="2">VER PDF </font>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <button type="button" name="oguardar_pdf" id="oguardar_pdf" class="btn btn-primary">
                                 <span>
                                     <i class="fa fa-save" aria-hidden="true"></i>
                                 </span>
                                <font size="2">GUARDAR </font>
                            </button>
                        </div>
                        <div class="col-md-3">
                            <button type="button" name="oreemplazar_pdf" id="oreemplazar_pdf" class="btn btn-danger">
                                 <span>
                                     <i class="fa fa-pencil-square-o " aria-hidden="true"></i>
                                 </span>
                                <font size="2">ELIMINAR </font>
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>


</div>
<!-- END MAIN PANEL -->

