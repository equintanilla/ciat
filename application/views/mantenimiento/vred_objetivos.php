<!-- MAIN PANEL -->
<div id="main" role="main">
    <div id="">
        <ol class="breadcrumb">
            <li>Mantenimiento</li><li> Asignar Acciones de mediano plazo a carpeta POA</li>
        </ol>
    </div>
    <div id="content">
       <!-- <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-12 animated fadeInDown">
                <h3 class="page-title txt-color-blueDark"><i class="fa fa-pencil-square-o fa-fw "></i> Asignar aaciones de mediano plazo a carpeta POA</h3>
            </div>
        </div>-->

        <!-- widget grid -->
        <section id="widget-grid" class="">

            <!-- row -->
            <div class="row">

                <!-- NEW WIDGET START -->
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">

                        <header>
                            <span class="widget-icon"> <span class="fa fa-table"></span> </span>

                            <h2>Asignar aaciones de mediano plazo a carpeta POA</h2>
                        </header>

                        <div>
                            <div class="widget-body no-padding">
                                <div class="table-responsive">
                                    <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                        <thead>
                                        <tr>
                                            <th> Asignar </th>
                                            <th>CÓDIGO</th>
                                            <th>APERTURA PROGRAMÁTICA</th>
                                            <th>UNIDAD ORGANIZACIONAL</th>
                                            <th>FECHA DE CREACIÓN</th>
                                        </tr>
                                        </thead>
                                        <tbody id="bdi">
                                        <?php
                                        foreach($lista_poa as $row)
                                        {
                                            echo '<tr id="tr'.$row['poa_id'].'">';
                                            echo'<td ><a href="'.site_url("").'/mnt/asignar/'.$row['poa_id'].'"><center>
											<img src="'.base_url().'assets/img/folder.png" width="30" height="30" class="img-responsive "title="ASIGNAR">
											</center></a></center></td>';
                                            echo '<td><font size="1">'.$row['poa_codigo'].'</font></td>';
                                            echo '<td><font size="1">'.$row['aper_programa'].$row['aper_proyecto'].$row['aper_actividad']." - ".$row['aper_descripcion'].'</font></td>';
                                            echo '<td><font size="1">'.$row['uni_unidad'].'</font></td>';
                                            echo '<td><font size="1">'.$row['poa_fecha_creacion'].'</font></td>';
                                            echo '</tr>';

                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- end widget content -->
                        </div>
                        <!-- end widget div -->
                    </div>
                    <!-- end widget -->
                </article>
                <!-- WIDGET END -->
            </div>
        </section>
        <!-- end widget grid -->

    </div>
</div>
<!-- END MAIN PANEL -->




