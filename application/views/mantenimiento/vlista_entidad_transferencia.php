<!-- MAIN PANEL -->
<div id="main" role="main">

    <!-- RIBBON -->
    <div id="">


        <!-- breadcrumb -->
        <ol class="breadcrumb">
            <li>Mantenimiento</li>
            <li>Entidad de Transferencia</li>
        </ol>
    </div>
    <!-- END RIBBON -->

    <!-- MAIN CONTENT -->
    <div id="content">
        
        <!--<div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-12 animated fadeInDown">
                <h1 class="page-title txt-color-blueDark"><i class="fa fa-pencil-square-o fa-fw "></i> LISTA DE ENTIDAD
                    DE TRANSFERENCIA</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-12">
                <div class="form-group">
                    <a data-toggle="modal" href="#modal_nuevo_et" class="btn btn-labeled btn-success pull-left ">
                        <span class="btn-label"><i class="glyphicon glyphicon-file"></i></span><b>NUEVO</b></a><br><br>
                </div>
            </div>
        </div>-->

        <!-- widget grid -->
        <section id="widget-grid" class="">

            <!-- row -->
            <div class="row">

                <!-- NEW WIDGET START -->
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">

                        <header>
                            <span class="widget-icon"> <i class="fa fa-table"></i></span>
                            <h2 class="font-md"><strong> ENTIDAD DE TRANSFERENCIA</strong></h2>
                        </header>
                        <h1 id="prueba"></h1>

                        <div>
                            <div class="widget-body no-padding">
                                <div class="table-responsive">
                                    <table id="dt_basic" class="table table-striped table-bordered table-hover"
                                           width="100%">
                                        <thead>
                                        <tr>
                                            <th>
                                                <center>
                                                    <a data-toggle="modal" href="#modal_nuevo_et" style="text-decoration: none" title="NUEVO ESCALA SALARIAL">
                                                        <img src="<?php echo base_url(); ?>assets/ifinal/2.png" width="30" height="30">
                                                    </a>
                                                    <br style="font-size: 20px">Nueva Entidad <br> Transferencia
                                                </center>
                                            </th>
                                            <th>C&Oacute;DIGO</th>
                                            <th>DESCRIPCI&Oacute;N</th>
                                            <th>SIGLA</th>
                                            <th>GESTIÓN</th>
                                            <th>MODIFICAR</th>
                                            <th>ELIMINAR</th>

                                        </tr>
                                        </thead>
                                        <tbody id="tabla_et">
                                        <?php
                                        $cont = 1;
                                        foreach ($lista_et as $row) {
                                            echo '<tr id="tr' . $row['et_id'] . '">';
                                            echo '<td>' . $cont . '</td>';
                                            echo '<td><font size="1">' . $row['et_codigo'] . '</font></td>';
                                            echo '<td><font size="1">' . $row['et_descripcion'] . '</font></td>';
                                            echo '<td><font size="1">' . $row['et_sigla'] . '</font></td>';
                                            echo '<td><font size="1">' . $row['et_gestion'] . '</font></td>';
                                            ?>
                                            <input type="hidden" name="id_mod" id="id_mod"
                                                   value="<?php echo $row['et_id'] ?>">
                                            <td>
                                                
                                                        <BUTTON data-toggle="modal" data-target="#modal_mod_et"
                                                                class="btn btn-xs botones dos mod_et"
                                                                name="<?php echo $row['et_id'] ?>" id="enviar_mod">
                                                            
                                                                <img src="<?php echo  base_url() ?>assets/ifinal/modificar.png" title="modificar" id="modificar">
                                                                <P style="font-size: 8px;"><b>Modificar</b></P>
                                                            
                                                        </BUTTON>
                                            </td>
                                            <td>
                                                        <button class="btn btn-xs botones uno del_et"
                                                                name="<?php echo $row['et_id'] ?>" id="eliminar">
                                                            
                                                                                           <img src="<?php echo base_url() ?>assets/ifinal/eliminar.png" title="eliminar" id="eliminar">
                                                                                           <P style="font-size: 8px;"><b>Eliminar</b></P>
                                                            
                                                        </button>
                                            </td>

                                            <?php
                                            echo '</tr>';
                                            $cont++;
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- end widget content -->
                        </div>
                        <!-- end widget div -->
                    </div>
                    <!-- end widget -->
                </article>
                <!-- WIDGET END -->
            </div>
        </section>
        <!-- end widget grid -->

    </div>
    <!-- END MAIN CONTENT -->
    <!-- ================== Modal NUEVO ENTIDAD TRANSFERENCIA ========================== -->
    <div class="modal animated fadeInDown" id="modal_nuevo_et" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close text-danger " data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title text-center text-info">
                        <b><i class="glyphicon glyphicon-pencil"></i> NUEVO REGISTRO</b>
                    </h4>
                </div>
                <div class="modal-body no-padding">
                    <div class="row">
                        <form id="form_et" novalidate="novalidate" method="post">
                            <div id="bootstrap-wizard-1" class="col-sm-12">
                                <div class="well">
                                    <div class="row">
                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <LABEL><b>Entidad de Transferencia</b></label>
                                                <input class="form-control" type="text" name="etdescripcion"
                                                       id="etdescripcion"
                                                       placeholder="Ingrese Entidad de Transferencia"
                                                       style="text-transform:uppercase;"
                                                       onkeyup="javascript:this.value=this.value.toUpperCase();"
                                                       onkeypress="if (this.value.length < 100) { return soloLetras_carracter_especial(event);}else{return false; }">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <LABEL><b>Sigla</b></label>
                                                <input class="form-control" type="text" name="etsigla" id="etsigla"
                                                       placeholder="Ingrese la Sigla"
                                                       style="text-transform:uppercase;"
                                                       onkeyup="javascript:this.value=this.value.toUpperCase();"
                                                       onkeypress="if (this.value.length < 12) { return event;}else{return false; }">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <LABEL><b>Código</b></label>
                                                <input class="form-control" type="text" name="etcodigo" id="etcodigo"
                                                       placeholder="Ingrese el Código"
                                                       onkeypress="if (this.value.length < 8) { return soloNumeros(event);}else{return false; }">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <LABEL><b>Gestión</b></label>
                                                <input class="form-control" type="text" name="etgestion" id="etgestion"
                                                       value="<?php echo $this->session->userData('gestion') ?>"
                                                       placeholder="Ingrese la gestión"
                                                       data-mask="9999" data-mask-placeholder="X">
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- end well -->
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-3 pull-left">
                            <button class="btn btn-lg btn-danger" data-dismiss="modal">CANCELAR</button>
                        </div>
                        <div class="col-md-3 pull-right ">
                            <button type="submit" name="enviar_et" id="enviar_et" class="btn  btn-lg btn-primary"><i
                                    class="fa fa-save"></i>
                                GUARDAR
                            </button>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- ================== Modal  MODIFICAR  ENTIDAD TRANSFERENCIA ========================== -->
    <div class="modal animated fadeInDown" id="modal_mod_et" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close text-danger " data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title text-center text-info">
                        <b><i class="glyphicon glyphicon-pencil"></i> MODIFICAR REGISTRO</b>
                    </h4>
                </div>
                <div class="modal-body no-padding">
                    <div class="row">
                        <form id="mod_formet" novalidate="novalidate" method="post">
                            <div id="bootstrap-wizard-1" class="col-sm-12">
                                <div class="well">
                                    <div class="row">
                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <LABEL><b>Entidad de Transferencia</b></label>
                                                <input class="form-control" type="text" name="mod_etdescripcion"
                                                       id="mod_etdescripcion"
                                                       placeholder="Ingrese Entidad de Transferencia"
                                                       style="text-transform:uppercase;"
                                                       onkeyup="javascript:this.value=this.value.toUpperCase();"
                                                       onkeypress="if (this.value.length < 100) { return soloLetras_carracter_especial(event);}else{return false; }">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <LABEL><b>Sigla</b></label>
                                                <input class="form-control" type="text" name="mod_etsigla"
                                                       id="mod_etsigla" placeholder="Ingrese la sigla"
                                                       style="text-transform:uppercase;"
                                                       onkeyup="javascript:this.value=this.value.toUpperCase();"
                                                       onkeypress="if (this.value.length < 12) { return event;}else{return false; }">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <LABEL><b>Código</b></label>
                                                <input disabled="disabled" class="form-control" type="text"
                                                       name="mod_etcodigo" id="mod_etcodigo"
                                                       placeholder="Ingrese el código"
                                                       onkeypress="if (this.value.length < 8) { return soloNumeros(event);}else{return false; }">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <LABEL><b>Gestión</b></label>
                                                <input class="form-control" type="text" name="mod_etgestion"
                                                       id="mod_etgestion" placeholder="Ingrese la gestión"
                                                       data-mask="9999" data-mask-placeholder="X">
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- end well -->
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-3 pull-left">
                            <button class="btn btn-lg btn-danger" data-dismiss="modal">CANCELAR</button>
                        </div>
                        <div class="col-md-3 pull-right ">
                            <button type="submit" name="mod_etenviar" id="mod_etenviar" class="btn  btn-lg btn-primary">
                                <i class="fa fa-save"></i>
                                ACEPTAR
                            </button>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

</div>
<!-- END MAIN PANEL -->