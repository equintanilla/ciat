<style type="text/css">
    .btn{  transition-duration: 0.5s; }
    .btn:hover{transform: scale(1.2);}
</style>
<div id="main" role="main">
    <div id="ribbon">
        <ol class="breadcrumb">
            <li>Mantenimiento</li>
            <li>Configuración</li>
        </ol>
    </div><br>
 <h1 style="font-size: 24px;"><i class="icon fa fa-check" style="color:#e7f3ff;">&nbsp;</i><b>CONFIGURACIÓN PARA LA BASE DE DATOS</b></h1>
    <div id="content">
        <div class="row">
            <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 animated fadeInDown">
                        <h1>Gestión Actual <?php echo $gestion_db;?></h1>
                        <div class="panel panel-darken">
                            <div class="panel-heading">
                                <h4>Modificar Gestion</h4>
                            </div>
                            <div class="panel-body">
                                <?php echo $gestion;?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 animated fadeInDown">
                        <h1>Mes Actual <?php echo $mes_db;?></h1>
                        <div class="panel panel-darken">
                            <div class="panel-heading">
                                <h4>Modificar Mes</h4>
                            </div>
                            <div class="panel-body">
                                    <?php echo $mes;?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                <div class="row">
                    <div class="col-md-12 animated color-darken" align="center">
                        <div class="alert alert-block color-darken">
                            <h4 style=""><i class="icon fa fa-check" style="color:#e7f3ff;">&nbsp;</i><b>Agregar Nuevo Registro</b></h4>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="bootstrap-wizard-1">
                        <div class="well">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="widget-body no-padding">
                                        <div class="table-responsive">
                                            <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>Estado</th>
                                                        <th>GESTIÓN</th>
                                                        <th>NOMBRE ENTIDAD</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="bdi" style="font-size:13px;">
                                                    <?php
                                                        foreach ($gestion_lista as $fila) {?>
                                                            <tr>
                                                                <td style="width:8%;">
                                                                    <?php 
                                                                        if($fila['estado'] == 0){?>
                                                                            <a data-toggle="tooltip" rel="tooltip" title="Desbloquear" class="btn btn-xs botones act_gestion" name="<?php echo $fila['ide'];?>" id="activar_gest">
                                                                                <img src="<?php echo base_url();?>/assets/ifinal/cnd2.png" width="20" height="20" class="img-responsive">
                                                                            </a>
                                                                        <?php }else{?>
                                                                            <a data-toggle="tooltip" rel="tooltip" title="Bloquear" class="btn btn-xs botones desact_gestion" name="<?php echo $fila['ide'];?>" id="descativar_gest">
                                                                                <img src="<?php echo base_url();?>/assets/ifinal/cnd1.png" width="20" height="20" class="img-responsive">
                                                                            </a>
                                                                        <?php }
                                                                    ?>
                                                                </td>
                                                                <td style="width:12%;"><?php echo $fila['ide'];?></td>
                                                                <td style="width:80%;"><?php echo $fila['conf_nombre_entidad'];?></td>
                                                            </tr>        
                                                        <?php }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

