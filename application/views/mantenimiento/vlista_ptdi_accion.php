<!-- MAIN PANEL -->
<div id="main" role="main">

    <!-- RIBBON -->
    <div id="ribbon">
        <!-- breadcrumb -->
        <ol class="breadcrumb">
            <li>Mantenimiento</li>
            <li>PTDI ACCIÒN</li>
        </ol>
    </div>
    <!-- END RIBBON -->

    <!-- MAIN CONTENT -->
    <div id="content">

     
        
        <!-- widget grid -->
        <section id="widget-grid" class="">

            <!-- row -->
            <div class="row">

                <!-- NEW WIDGET START -->

                <!-- NEW WIDGET START -->
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                     <!--========================ACCION=================================-->
                    <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
                        <header>
                           <h2 class="font-md"><strong>LISTA ACCIÒN</strong></h2>  
                        </header>
                        <!-- MAIN CONTENT -->
                        <!-- widget div-->
                        <div>
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <!-- end widget edit box -->
                            <!-- widget content -->
                            <div class="widget-body no-padding">

                                <table id="dt_basic3" class="table table-striped table-bordered table-hover"
                                       width="100%">
                                    <thead>
                                    <tr>
                                        <th data-hide="phone">
                                            #
                                        </th>
                                        <th data-hide="phone">CÓDIGO</th>
                                        <th data-hide="phone">NIVEL</th>
                                        <th data-hide="phone">DESCRIPCIÓN</th>
                                        <th data-hide="phone">GESTIÓN</th>
                                     

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($list_ptdi_accion as $row2) {?>
                                        
                                   
                                        <tr>
                                            <td>1</td>
                                            <td><?php echo $row2['ptdi_codigo'];?></td>
                                            <td><?php echo $row2['ptdi_nivel']?></td>
                                            <td><?php echo $row2['ptdi_descripcion']?></td>
                                            <td><?php echo $row2['ptdi_gestion']?></td>
                                          
                                        </tr>

                                   <?php  }?>
                                    </tbody>
                                </table>

                            </div>
                            <!-- end widget content -->

                        </div>
                        <!-- end widget div -->

                    </div>
                
                    <!--======================end acciones===================================-->
                </article>

            </div>

        </section>
        <!-- end widget grid -->

    </div>
    <!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->
<!-- ================== Modal NUEVO pedes ========================== -->
<div class="modal animated fadeInDown" id="modal_nuevo_car" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close text-danger " data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title text-center text-info">
                    <b><i class="glyphicon glyphicon-pencil"></i> NUEVA ESCALA SALARIAL</b>
                </h4>
            </div>
            <div class="modal-body no-padding">
                <div class="row">
                    <form id="form_car" novalidate="novalidate" method="post">
                        <div id="bootstrap-wizard-1" class="col-sm-12">
                            <div class="well">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <LABEL><b>Ingrese el nombre del Cargo</b></label>
                                            <input class="form-control" type="text" name="car_nombre" id="car_nombre"
                                                   placeholder="Nombre de Cargo" style="width:100%;"
                                                   style="text-transform:uppercase;"
                                                   onkeyup="javascript:this.value=this.value.toUpperCase();"
                                                   onkeypress="if (this.value.length < 100) { return soloLetras(event);}else{return false; }">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <LABEL><b>Es dependiente? </b></LABEL><br>
                                            <input type="radio" id="depndiente" value="si" name="dependiente"
                                                   class="car_si"/> SI
                                            <input type="radio" id="dependiente" value="no" name="dependiente"
                                                   class="car_no"/> NO

                                        </div>
                                    </div>
                                </div>
                                <div id="content_parent" class="row" style="display: none;">
                                    <div class="col-md-1">
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <select name="padre" id="padre" class="form-control ">
                                                <option value="">Seleccione un Cargo</option>
                                                <?php
                                                //imprimir todos 
                                                foreach ($cargos as $row) {
                                                    echo '<option value="' . $row['car_id'] . '">' . $row['car_cargo'] . '</option>';
                                                }
                                               /* foreach ($list_car_padre as $row) {
                                                    echo '<option value="' . $row['car_id'] . '">' . $row['car_cargo'] . '</option>';
                                                }*/
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <LABEL><b>Ingrese el Sueldo</b></label>
                                            <input class="form-control" type="text" name="car_sueldo" id="car_sueldo"
                                                   placeholder="Ingrese Sueldo"
                                                   onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <LABEL><b>Ingrese el Código</b></label>
                                            <input class="form-control" type="text" name="car_codigo" id="car_codigo"
                                                   placeholder="Ingrese Código"
                                                   onkeypress="if (this.value.length < 8) { return soloNumeros(event);}else{return false; }">
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- end well -->
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-3 pull-left">
                        <button class="btn btn-default btn-danger" data-dismiss="modal">CANCELAR</button>
                    </div>
                    <div class="col-md-3 pull-right ">
                        <button type="submit" name="enviar_car" id="enviar_car" class="btn  btn-default btn-primary"><i
                                class="fa fa-save"></i>
                            GUARDAR
                        </button>
                    </div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- ================== Modal  MODIFICAR   CARGO ========================== -->
<div class="modal animated fadeInDown" id="modal_mod_car" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close text-danger " data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title text-center text-info">
                    <b><i class="glyphicon glyphicon-pencil"></i> MODIFICAR REGISTRO</b>
                </h4>
            </div>
            <div class="modal-body no-padding">
                <div class="row">
                    <form id="mod_formcar" name="mod_formcar" novalidate="novalidate" method="post">
                        <div id="bootstrap-wizard-1" class="col-sm-12">
                            <div class="well">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <LABEL><b>Ingrese el nombre del Cargo</b></label>
                                            <input class="form-control" type="text" name="modcar_nombre"
                                                   id="modcar_nombre" placeholder="Ingrese el nombre del Cargo"
                                                   style="width:100%;" style="text-transform:uppercase;"
                                                   onkeyup="javascript:this.value=this.value.toUpperCase();"
                                                   onkeypress="if (this.value.length < 100) { return soloLetras(event);}else{return false; }">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <LABEL><b>Es dependiente? </b></LABEL><br>
                                            <input type="radio" id="modcar_si" value="si" name="modcar_si"
                                                   class="modcar_si custom-controls-stacked"/> SI
                                            <input type="radio" id="modcar_no" value="no" name="modcar_no"
                                                   class="modcar_no custom-controls-stacked"/> NO

                                        </div>
                                    </div>
                                </div>
                                <div id="modcontent_parent" class="row" style="display: none;">
                                    <div class="col-md-1">
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <select name="modcar_padre" id="modcar_padre" class="form-control ">
                                                <option value="">Seleccione un Cargo</option>
                                                <?php
                                                foreach ($list_car_padre as $row) {
                                                    echo '<option value="' . $row['car_id'] . '">' . $row['car_cargo'] . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <LABEL><b>Sueldo</b></label>
                                            <input class="form-control" type="text" name="modcar_sueldo"
                                                   id="modcar_sueldo"
                                                   onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <LABEL><b>Ingrese el Código</b></label>
                                            <input class="form-control" type="text" name="modcar_codigo"
                                                   id="modcar_codigo" placeholder="Ingrese Código">
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- end well -->
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-3 pull-left">
                        <button class="btn btn-lg btn-danger" data-dismiss="modal">CANCELAR</button>
                    </div>
                    <div class="col-md-3 pull-right ">
                        <button type="submit" name="mod_parenviar" id="mod_parenviar" class="btn  btn-lg btn-primary"><i
                                class="fa fa-save"></i>
                            ACEPTAR
                        </button>
                    </div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- /.modal -->