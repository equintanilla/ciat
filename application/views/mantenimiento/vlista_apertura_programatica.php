<div id="main" role="main">
    <div id="">
        <ol class="breadcrumb">
            <li>Mantenimiento</li>
            <li>Apertura Programática</li>
        </ol>
    </div>
    <div id="content">
        <section id="widget-grid" class="">
            <div class="row">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-id-1" data-widget-editbutton="false"
                     data-widget-colorbutton="false" data-widget-deletebutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                        <h2 class="font-md"><strong> &nbsp;LISTA DE APERTURA PROGRAMATICA</strong></h2>

                    </header>
                    <div>
                        <div class="widget-body no-padding">
                            <table id="datatable_fixed_column" class="table table-bordered" width="100%">
                                <thead>
                                <tr>
                                    <TH></TH>
                                    <th class="hasinput icon-addon">
                                        <input type="text" class="form-control" placeholder="Gesti&oacute;n"/>
                                        <label for="dateselect_filter" class="glyphicon glyphicon-calendar no-margin padding-top-15"></label>
                                    </th>
                                    <th class="hasinput">
                                        <input type="text" class="form-control" placeholder="Programa"/>
                                    </th>
                                    <th class="hasinput">
                                        <input type="text" class="form-control" placeholder="Proyecto"/>
                                    </th>
                                    <th class="hasinput">
                                        <input type="text" class="form-control" placeholder="Actividad"/>
                                    </th>
                                    <th class="hasinput">
                                        <input type="text" class="form-control" placeholder="Descripci&oacute;n"/>
                                    </th>
                                    <th class="hasinput">
                                        <input type="text" class="form-control" placeholder="U. organizacional"/>
                                    </th>
                                </tr>
                                <tr style="background-color: #66b2e8">
                                    <th> #</th>
                                    <th>GESTIÓN</th>
                                    <th>PROGRAMA</th>
                                    <th>PROYECTO</th>
                                    <th>ACTIVIDAD</th>
                                    <th>DESCRIPCI&Oacute;N</th>
                                    <th>UNIDAD ORGANIZACIONAL</th>
                                </tr>
                                </thead>
                                <tbody id="bdi">
                                <?php
                                echo $lista_aperturas;
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>



