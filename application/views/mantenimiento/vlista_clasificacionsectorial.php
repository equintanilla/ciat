<?php
    $gestion = $this->session->userdata('gestion');
?>
<div id="main" role="main">
    <div id="ribbon">
        <ol class="breadcrumb">
            <li>Mantenimiento</li>
            <li>Clasificación Sectorial</li>
        </ol>
    </div>
    <div id="content">
        <!--<div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-12 animated fadeInDown">
                <h1 class="page-title txt-color-blueDark"><i class="fa fa-pencil-square-o fa-fw "></i> Clasificadores Sectoriales</h1>
            </div>
        </div>-->
        <section id="widget-grid">
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                            <h2 class="font-md"><strong> <?php echo $titulo;?></strong></h2>
                        </header>
                        <div>
                            <div class="jarviswidget-editbox"></div>
                            <div class="widget-body no-padding">
                                <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                    <thead>
                                        <tr>
                                            <th style="width:15%;">
                                                <center>
                                                    <a data-toggle="modal" href="#modal_nuevo_sector" class="">
                                                        <img src="<?php echo base_url() ?>assets/ifinal/2.png" title="Agregar Sector">
                                                    </a><br style="font-size: 20px">Nuevo sector
                                                </center>
                                            </th>
                                            <th data-hide="phone">CÓDIGO</th>
                                            <th data-hide="phone">DESCRIPCIÓN</th>
                                            <th data-hide="phone">ABREVIACIÓN</th>
                                            <th data-hide="phone">TIPO</th>
                                            <th data-hide="phone">GESTIÓN</th>
                                            <th>MODIFICAR</th>
                                            <th>ELIMINAR</th>
                                            <th>VER SUBSECTORES </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $a=0;
                                            foreach ($sectores as $fila) {
                                            ?>
                                                <tr>
                                                    <td>
                                                            <?php echo $a;?>
                                                        
                                                    </td>
                                                    <td>
                                                        <?php echo $fila['codsectorial'];?>
                                                    </td>
                                                    <td>
                                                        <?php echo $fila['descclasificadorsectorial'];?>
                                                    </td>
                                                    <td>
                                                        <?php echo $fila['abrevsector'];?>
                                                    </td>
                                                    <td>
                                                        <?php echo $fila['codsectorialduf'];?>
                                                    </td>
                                                    <td>
                                                        <?php echo $fila['gestion'];?>
                                                    </td>
                                                    <td>
                                                       
                                                            <button data-toggle="modal" data-target="#modal_mod_sector" class="btn btn-xs botones dos mod_sector"  name="<?php echo $fila['codsectorial'];?>" id="enviar_mod">
                                                                <img src="<?php echo base_url() ?>assets/ifinal/modificar.png" title="Modificar">
                                                                <P style="font-size: 8px;"><b>Modificar</b></P>
                                                            </button>
                                                    </td>        
                                                    <td>
                                                            <button class="btn btn-xs botones eliminar_sector" name="<?php echo $fila['codsec'];?>" id="<?php echo $fila['gestion'];?>">
                                                                <img src="<?php echo base_url() ?>assets/ifinal/eliminar.png" title="Eliminar"/>
                                                                <P style="font-size: 8px;"><b>Eliminar</b></P>
                                                            </button>
                                                            
                                                    </td>
                                                    <td>
                                                        <a href="<?php echo site_url() . '/subsector/'.$fila['codsec'].'/'.$fila['sector'];?>" title="Ver Subsectores">
                                                                <button class="btn btn-xs botones" name="ver_sector" id="ver_sector">
                                                                    <div class="btn-hover-postion1">
                                                                        <img src="<?php echo base_url() ?>assets/ifinal/carp.png" id="ver_sector">
                                                                        <P style="font-size: 8px;"><b>Ver Subsectores</b></P>
                                                                    </div>
                                                                </button>
                                                            </a>
                                                    </td>
                                                </tr>
                                            <?php $a++; }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </section>
    </div>
    <!-- ================== Modal Nueva Clasificación Sectorial ========================== -->
    <div class="modal fade bs-example-modal-lg animated fadeInDown" id="modal_nuevo_sector" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close text-danger" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title text-center text-info">
                        <b><i class="glyphicon glyphicon-pencil"></i> NUEVO SECTOR</b>
                    </h4>
                </div>
                <form id="form_nuevo_sector" name="form_nuevo_sector" novalidate="novalidate" method="post" action="<?php echo site_url() ?>/mantenimiento/clasificacion_sectorial/agregar_nuevosector" novalidate="novalidate">
                    <div class="modal-body no-padding">
                        <div class="row">
                            <div id="bootstrap-wizard-1" class="col-sm-12">
                                <div class="well">
                                    <div class="row">
                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <label><b>NOMBRE SECTOR</b></label><br>
                                                <input type="text" name="clas_nombre" id="clas_nombre" placeholder="Nombre del Sector" class="form-control" style="width:100%;" onblur="javascript:this.value=this.value.toUpperCase();">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label><b>ABREVIACIÓN SECTOR</b></label>
                                                <input type="text" name="clas_abre" id="clas_abre" placeholder="Abreviación Sector" class="form-control" style="width:100%;" onblur="javascript:this.value=this.value.toUpperCase();"
                                                onkeypress="if (this.value.length < 3) { return true;}else{return false; }">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label><b>GESTIÓN</b></label>
                                                <input class="form-control" type="text" name="clas_gestion" id="clas_gestion" style="width:100%;"
                                                    style="text-transform:uppercase;" value="<?php echo $gestion;?>" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label><b>CÓDIGO</b></label>
                                                <input type="text" name="clas_cod" id="clas_cod" placeholder="99" class="form-control"
                                                    onkeypress="if (this.value.length < 2) { return soloNumeros(event);}else{return false; }" onpaste="return false">
                                            </div>
                                        </div>
                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <label>
                                                    <b>
                                                        Codigo Generado:
                                                    </b>
                                                </label><br>
                                                <h4>
                                                    <span id="display"></span>
                                                </h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="row">
                            <div class="col-md-3 pull-left">
                                <button class="btn btn-default btn-danger" data-dismiss="modal">CANCELAR</button>
                            </div>
                            <div class="col-md-3 pull-right ">
                                <button type="submit" name="clas_nuevo_sector" id="clas_nuevo_sector" class="btn  btn-default btn-primary">
                                    <i class="fa fa-save"></i>
                                    GUARDAR
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.modal -->
    <!-- ================== Modal Modificar Clasificación Sectorial ========================== -->
    <div class="modal fade bs-example-modal-lg animated fadeInDown" id="modal_mod_sector" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close text-danger" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title text-center text-info">
                        <b><i class="glyphicon glyphicon-pencil"></i> MODIFICAR SECTOR</b>
                    </h4>
                </div>
                <form id="form_mod_sector" name="form_mod_sector" novalidate="novalidate" method="post" action="<?php echo site_url() ?>/mantenimiento/clasificacion_sectorial/modifica_sector" novalidate="novalidate">
                    <div class="modal-body no-padding">
                        <div class="row">
                            <div id="bootstrap-wizard-1" class="col-sm-12">
                                <div class="well">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label><b>CÓDIGO</b></label>
                                                <input type="text" name="clas_mod_cod" id="clas_mod_cod" placeholder="99" class="form-control"
                                                    onkeypress="if (this.value.length < 2) { return soloNumeros(event);}else{return false; }"
                                                    onpaste="return false" disabled>
                                                <input type="text" name="codsectorial_mod" id="codsectorial_mod" hidden>
                                                <input type="text" name="mod_codsec" id="mod_codsec" hidden>
                                                <input type="text" name="mod_gestion_sector" id="mod_gestion_sector" hidden>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label><b>GESTIÓN</b></label>
                                                <input class="form-control" type="text" name="clas_mod_gestion" id="clas_mod_gestion" style="width:100%;"
                                                    style="text-transform:uppercase;" disabled>
                                            </div>
                                        </div>
                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <label><b>NOMBRE SECTOR</b></label><br>
                                                <input type="text" name="clas_mod_nombre" id="clas_mod_nombre" placeholder="Nombre del Sector" class="form-control" style="width:100%;" onblur="javascript:this.value=this.value.toUpperCase();">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label><b>ABREVIACIÓN SECTOR</b></label>
                                                <input type="text" name="clas_mod_abre" id="clas_mod_abre" placeholder="Abreviación Sector" class="form-control" style="width:100%;" onblur="javascript:this.value=this.value.toUpperCase();"
                                                onkeypress="if (this.value.length < 3) { return true;}else{return false; }">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="row">
                            <div class="col-md-3 pull-left">
                                <button class="btn btn-default btn-danger" data-dismiss="modal">CANCELAR</button>
                            </div>
                            <div class="col-md-3 pull-right ">
                                <button type="submit" name="clas_mod_sector" id="clas_mod_sector" class="btn  btn-default btn-primary">
                                    <i class="fa fa-save"></i>
                                    GUARDAR
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
