<?php
    $gestion = $this->session->userdata('gestion');
?>
<div id="main" role="main">
    <div id="ribbon">
        <ol class="breadcrumb">
            <li>Mantenimiento</li>
            <li>Clasificación Sectorial</li>
            <li>Subsectores, Actividades Economicas</li>
        </ol>
    </div>
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-12">
                <h1 class="page-title txt-color-blueDark">
                    <span style="font-size:20px;">
                        <i class="fa fa-table"></i>
                        Sector <?php echo $sector.' '.$pk1;?> / Subsectores y Actividades Economicas
                    </span>
                </h1>
            </div>
        </div>
        <section id="widget-grid">
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-blue" id="wid-id-1">
                        <header role="heading">
                            <span class="widget-icon">
                                <i class="fa fa-table" style="font-size:15px;"></i>
                            </span>
                            <h2>
                                <?php echo $titulo;?>
                            </h2>
                        </header>
                        <div>
                            <div class="widget-body no-padding">
                                <table id="dt_basic" class="table table-bordered table-hover" width="100%">
                                    <thead>
                                        <tr>
                                            <th data-hide="phone">
                                                <span>N°</span>
                                            </th>
                                            <th data-hide="phone" style="width:10%;">
                                                <center>
                                                    <a data-toggle="modal" href="#modal_nuevo_subsector" name="<?php $codsec?>" class="">
                                                        <img src="<?php echo base_url() ?>assets/ifinal/2.png" title="Agregar Sub Sector">
                                                    </a>
                                                </center>
                                            </th>
                                            <th data-hide="phone">CÓDIGO</th>
                                            <th data-hide="phone">DESCRIPCIÓN</th>
                                            <th data-hide="phone" style="width:4%;">ABREVIACIÓN</th>
                                            <th data-hide="phone" style="width:8%;">TIPO</th>
                                            <th data-hide="phone" style="width:4%;">GESTIÓN</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            $n = 1;
                                            foreach ($subsectores as $fila) {
                                                if ($fila['nivel'] == 2) { ?>
                                                    <tr class="info">
                                                        <td>
                                                            <?php echo $n;?>
                                                        </td>
                                                        <td>
                                                            Sub Sector
                                                        </td>
                                                        <td>
                                                            <?php echo $fila['codsectorial'];?>
                                                        </td>
                                                        <td>
                                                            <?php echo $fila['descclasificadorsectorial'];?>
                                                        </td>
                                                        <td>
                                                            <?php echo $fila['abrevsector'];?>
                                                        </td>
                                                        <td>
                                                            <?php echo $fila['codsectorialduf'];?>
                                                        </td>
                                                        <td>
                                                            <?php echo $fila['gestion'];?>
                                                        </td>
                                                    </tr>
                                                <?php } else { ?>
                                                    <tr>
                                                        <td>
                                                            <?php echo $n;?>
                                                        </td>
                                                        <td>
                                                            <center>
                                                                <button class="btn btn-xs botones uno del_car" name="" id="eliminar">
                                                                    <div class="btn-hover-postion1">
                                                                        <img src="<?php echo base_url() ?>assets/ifinal/modificar.png" title="eliminar" id="eliminar">
                                                                    </div>
                                                                </button>
                                                                <button class="btn btn-xs botones uno del_car" name="" id="eliminar">
                                                                        <div class="btn-hover-postion1">
                                                                            <img src="<?php echo base_url() ?>assets/ifinal/eliminar.png" title="eliminar" id="eliminar">
                                                                        </div>
                                                                </button>
                                                            </center>
                                                        </td>
                                                        <td>
                                                            <?php echo $fila['codsectorial'];?>
                                                        </td>
                                                        <td>
                                                            <?php echo $fila['descclasificadorsectorial'];?>
                                                        </td>
                                                        <td>
                                                            <?php echo $fila['abrevsector'];?>
                                                        </td>
                                                        <td>
                                                            <?php echo $fila['codsectorialduf'];?>
                                                        </td>
                                                        <td>
                                                            <?php echo $fila['gestion'];?>
                                                        </td>
                                                    </tr>
                                                <?php }
                                                $n++;
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </section>
    </div>
    <!-- *************************** Modales Para SUBSECTORES ************************* -->
    <!-- ================== Modal Nuevo Subsector ========================== -->
    <div class="modal fade bs-example-modal-lg animated fadeInDown" id="modal_nuevo_subsector" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close text-danger" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title text-center text-info">
                        <b><i class="glyphicon glyphicon-pencil"></i> NUEVO SUBSECTOR</b>
                    </h4>
                </div>
                <form id="form_nuevo_sector" name="form_nuevo_sector" novalidate="novalidate" method="post" action="<?php echo site_url() ?>/mantenimiento/clasificacion_sectorial/agregar_nuevosector" novalidate="novalidate">
                    <div class="modal-body no-padding">
                        <div class="row">
                            <div id="bootstrap-wizard-1" class="col-sm-12">
                                <div class="well">
                                    <div class="row">
                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <label><b>NOMBRE SECTOR</b></label><br>
                                                <input type="text" name="clas_nombre" id="clas_nombre" placeholder="Nombre del Sector" class="form-control" style="width:100%;" onblur="javascript:this.value=this.value.toUpperCase();">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label><b>ABREVIACIÓN SECTOR</b></label>
                                                <input type="text" name="clas_abre" id="clas_abre" placeholder="Abreviación Sector" class="form-control" style="width:100%;" onblur="javascript:this.value=this.value.toUpperCase();"
                                                onkeypress="if (this.value.length < 3) { return true;}else{return false; }">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label><b>GESTIÓN</b></label>
                                                <input class="form-control" type="text" name="clas_gestion" id="clas_gestion" style="width:100%;"
                                                    style="text-transform:uppercase;" value="<?php echo $gestion;?>" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label><b>CÓDIGO</b></label>
                                                <input type="text" name="clas_subcod" id="clas_subcod" placeholder="99" class="form-control"
                                                    onkeypress="if (this.value.length < 2) { return soloNumeros(event);}else{return false; }" onpaste="return false">
                                            </div>
                                        </div>
                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <label>
                                                    <b>
                                                        Codigo Generado:
                                                    </b>
                                                </label><br>
                                                <h4>
                                                    <span id="display"></span>
                                                </h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="row">
                            <div class="col-md-3 pull-left">
                                <button class="btn btn-default btn-danger" data-dismiss="modal">CANCELAR</button>
                            </div>
                            <div class="col-md-3 pull-right ">
                                <button type="submit" name="clas_nuevo_sector" id="clas_nuevo_sector" class="btn  btn-default btn-primary">
                                    <i class="fa fa-save"></i>
                                    GUARDAR
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.modal -->
    <!-- ================== Modal Modificar Clasificación Sectorial ========================== -->
    <div class="modal fade bs-example-modal-lg animated fadeInDown" id="modal_mod_sector" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close text-danger" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title text-center text-info">
                        <b><i class="glyphicon glyphicon-pencil"></i> MODIFICAR SECTOR</b>
                    </h4>
                </div>
                <form id="form_mod_sector" name="form_mod_sector" novalidate="novalidate" method="post" action="<?php echo site_url() ?>/mantenimiento/clasificacion_sectorial/modifica_sector" novalidate="novalidate">
                    <div class="modal-body no-padding">
                        <div class="row">
                            <div id="bootstrap-wizard-1" class="col-sm-12">
                                <div class="well">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label><b>CÓDIGO</b></label>
                                                <input type="text" name="clas_mod_cod" id="clas_mod_cod" placeholder="99" class="form-control"
                                                    onkeypress="if (this.value.length < 2) { return soloNumeros(event);}else{return false; }"
                                                    onpaste="return false" disabled>
                                                <input type="text" name="codsectorial_mod" id="codsectorial_mod" hidden>
                                                <input type="text" name="mod_codsec" id="mod_codsec" hidden>
                                                <input type="text" name="mod_gestion_sector" id="mod_gestion_sector" hidden>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label><b>GESTIÓN</b></label>
                                                <input class="form-control" type="text" name="clas_mod_gestion" id="clas_mod_gestion" style="width:100%;"
                                                    style="text-transform:uppercase;" disabled>
                                            </div>
                                        </div>
                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <label><b>NOMBRE SECTOR</b></label><br>
                                                <input type="text" name="clas_mod_nombre" id="clas_mod_nombre" placeholder="Nombre del Sector" class="form-control" style="width:100%;" onblur="javascript:this.value=this.value.toUpperCase();">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label><b>ABREVIACIÓN SECTOR</b></label>
                                                <input type="text" name="clas_mod_abre" id="clas_mod_abre" placeholder="Abreviación Sector" class="form-control" style="width:100%;" onblur="javascript:this.value=this.value.toUpperCase();"
                                                onkeypress="if (this.value.length < 3) { return true;}else{return false; }">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="row">
                            <div class="col-md-3 pull-left">
                                <button class="btn btn-default btn-danger" data-dismiss="modal">CANCELAR</button>
                            </div>
                            <div class="col-md-3 pull-right ">
                                <button type="submit" name="clas_mod_sector" id="clas_mod_sector" class="btn  btn-default btn-primary">
                                    <i class="fa fa-save"></i>
                                    GUARDAR
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- *************************** FIN Modales Para SUBSECTORES ************************* -->
</div>