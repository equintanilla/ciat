<!-- MAIN PANEL -->
<div id="main" role="main">

    <!-- RIBBON -->
    <div id="ribbon">

                

        <!-- breadcrumb -->
        <ol class="breadcrumb">
            <li>Mantenimiento</li>
            <li>PDES ACCIÓN</li>
        </ol>
    </div>
    <!-- END RIBBON -->

    <!-- MAIN CONTENT -->
    <div id="content">

        
        <!-- widget grid -->
        <section id="widget-grid" class="">

            <!-- row -->
            <div class="row">

                <!-- NEW WIDGET START -->

                <!-- NEW WIDGET START -->
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">


                    <!-- end widget -->

                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
                        <header>
                            <h2 class="font-md"><strong> LISTA ACCIÓN</strong></h2>
                        </header>
                        <!-- MAIN CONTENT -->
                        <!-- widget div-->
                        <div>
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <!-- end widget edit box -->
                            <!-- widget content -->
                            <div class="widget-body no-padding">

                                <table id="dt_basic3" class="table table-striped table-bordered table-hover"
                                       width="100%">
                                    <thead>
                                    <tr>
                                        <th data-hide="phone">
                                             <!-- <center>
                                                    <a data-toggle="modal" href="#modal_nuevo_car" style="text-decoration: none" title="NUEVO ESCALA SALARIAL">
                                                        <img src="<?php echo base_url(); ?>assets/ifinal/2.png" width="30" height="30">
                                                    </a>
                                                    <br style="font-size: 20px">acción

                                                </center> -->

                                        </th>
                                        <th data-hide="phone">CÓDIGO</th>
                                        <th data-hide="phone">NIVEL</th>
                                        <th data-hide="phone">DESCRIPCIÓN</th>
                                        <th data-hide="phone">GESTIÓN</th>
                                        <!-- <th>MODIFICAR</th>
                                        <th>ELIMINAR</th> -->

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $a=0; foreach ($list_pdes_accion as $row2) {?>
                                        
                                   
                                        <tr>
                                            <td><?php echo $a;  ?></td>
                                            <td><?php echo $row2['pdes_codigo'];?></td>
                                            <td><?php echo $row2['pdes_nivel']?></td>
                                            <td><?php echo $row2['pdes_descripcion']?></td>
                                            <td><?php echo $row2['pdes_gestion']?></td>

                                            </BUTTON>
                                            </td>
                                        </tr>    

                                   <?php $a++; }?>
                                    </tbody>
                                </table>

                            </div>
                            <!-- end widget content -->

                        </div>
                        <!-- end widget div -->

                    </div>
                    <!--======================end metas===================================-->
                </article>

            </div>

        </section>
        <!-- end widget grid -->

    </div>
    <!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->
<!-- ================== Modal NUEVO pedes ========================== -->
<div class="modal fade bs-example-modal-lg" id="modal_nuevo_pdes" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <!--====================================contenino============================-->
       <div class="modal-header">
                    <button type="button" class="close text-danger " data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title text-center text-info"><b>ADICIONAR PDES PILAR</b>
                    </h4>
                </div>
                <div class="modal-body no-padding">
                    <div class="row">
                        <form id="add_pdes_form" novalidate="novalidate" method="post">
                            <div id="bootstrap-wizard-1" class="col-sm-12">
                                <div class="well">
                                    <div class="row">
                                        <div class="col-sm-11">
                                            <div class="form-group">
                                                <LABEL><b>CÒDIGO</b></label>
                                                <input class="form-control" type="text" name="pdes_codigo"
                                                       id="pdes_codigo" placeholder="Ingrese el còdigo del PDES"
                                                        >
                                            </div>
                                        </div>
                                    </div>
                                     <div class="row">
                                        <div class="col-sm-11">
                                            <div class="form-group">
                                                <LABEL><b>GESTIÓN</b></label>
                                                <input class="form-control" type="text" name="pdes_gestion"
                                                       id="pdes_gestion" placeholder="Ingrese GESTIÓN del PDES"
                                                     >
                                            </div>
                                        </div>
                                    </div>
                                    
                                   <textarea id="pdes_descripcion" name="pdes_descripcion" class="form-control" rows="3" placeholder="Descripcìòn del PDES"></textarea>
                                </div> <!-- end well -->
                            </div>
                        
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-3 pull-left">
                            <button class="btn btn-ms btn-danger" data-dismiss="modal">CANCELAR</button>
                        </div>
                        <div class="col-md-3 pull-right ">
                            <button type="submit" name="enviar_pdes" id="enviar_pdes"
                                    class="btn  btn-ms btn-primary"><i class="fa fa-save"></i>
                                ACEPTAR
                            </button> 
                            </form>
                        </div>
                    </div>
                </div>
      <!--================================end contenido============================-->
    </div>
  </div>
</div>
<!--===================================  modificar pdes  ===================================-->
<div class="modal fade modificar_pdes" id="modificar_pdes" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <!--====================================contenino============================-->
       <div class="modal-header">
                    <button type="button" class="close text-danger " data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title text-center text-info"><b>MODIFICAR PDES</b>
                    </h4>
                </div>
                <div class="modal-body no-padding">
                    <div class="row">
                        <form id="mod_pdes_form" novalidate="novalidate" method="post" action="pdes_mod_pilar">
                            <div id="bootstrap-wizard-1" class="col-sm-12">
                                <div class="well">
                                    <div class="row">
                                        <div class="col-sm-11">
                                            <div class="form-group">
                                                <LABEL><b>CÒDIGO</b></label>
                                                <input class="form-control" type="text" name="modpdes_codigo"
                                                       id="modpdes_codigo" placeholder="Ingrese el còdigo del PDES"
                                                        >
                                                        <input class="form-control" type="hidden" name="modpdes_id"
                                                       id="modpdes_id" placeholder="Ingrese el còdigo del PDES"
                                                        >
                                            </div>
                                        </div>
                                    </div>
                                     <div class="row">
                                        <div class="col-sm-11">
                                            <div class="form-group">
                                                <LABEL><b>GESTIÓN</b></label>
                                                <input class="form-control" type="text" name="modpdes_gestion"
                                                       id="modpdes_gestion" placeholder="Ingrese GESTIÓN del PDES"
                                                     >
                                            </div>
                                        </div>
                                    </div>
                                    
                                   <textarea id="modpdes_descripcion" name="modpdes_descripcion" class="form-control" rows="3" placeholder="Descripcìòn del PDES"></textarea>
                                </div> <!-- end well -->
                            </div>
                        
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-3 pull-left">
                            <button class="btn btn-ms btn-danger" data-dismiss="modal">CANCELAR</button>
                        </div>
                        <div class="col-md-3 pull-right ">
                            <button type="submit" name="enviar_pdes" id="mod_pdes_pilar"
                                    class="btn  btn-ms btn-primary"><i class="fa fa-save"></i>
                                ACEPTAR
                            </button> 
                            </form>
                        </div>
                    </div>
                </div>
      <!--================================end contenido============================-->
    </div>
  </div>
</div>