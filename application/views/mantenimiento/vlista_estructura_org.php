<style type="text/css">
    .btn{  transition-duration: 0.5s; }
    .btn:hover{transform: scale(1.2);}
</style>

<!-- MAIN PANEL -->
<div id="main" role="main">

    <!-- RIBBON -->
    <div id="ribbon">



        <!-- breadcrumb -->
        <ol class="breadcrumb">
            <li>Mantenimiento</li>
            <li>Estructura Organizacional</li>
        </ol>
    </div>
    <!-- END RIBBON -->

    <!-- MAIN CONTENT -->
    <div id="content">

        <!--<div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-6">
                <h1 class="page-title txt-color-blueDark"><i class="fa fa-pencil-square-o fa-fw "></i> ESTRUCTURA
                    ORGANIZACIONAL <span>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-12">
                <div class="form-group">
                    <a data-toggle="modal" href="#modal_nuevo_uni" class="btn btn-labeled btn-success pull-left ">
                        <span class="btn-label"><i class="glyphicon glyphicon-file"></i></span><b>NUEVO</b></a><br><br>
                </div>
            </div>
        </div>-->
        <!-- widget grid -->
        <section id="widget-grid" class="">

            <!-- row -->
            <div class="row">

                <!-- NEW WIDGET START -->

                <!-- NEW WIDGET START -->
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">


                    <!-- end widget -->

                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-color-darken" id="wid-id-1" data-widget-editbutton="false">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                            <h2 class="font-md"><strong> ESTRUCTURA ORGANIZACIONAL</strong></h2>
                        </header>
                        <!-- MAIN CONTENT -->
                        <!-- widget div-->
                        <div>
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <!-- end widget edit box -->
                            <!-- widget content -->
                            <div class="widget-body no-padding">

                                <table id="dt_basic" class="table table-striped table-bordered table-hover"
                                       width="100%">
                                    <thead>
                                    <tr>
                                        <th data-hide="phone">
                                                <center>
                                                    <a data-toggle="modal" href="#modal_nuevo_uni" style="text-decoration: none" title="NUEVA ESTRUCTURA ORGANIZACIONAL">
                                                        <img src="<?php echo base_url(); ?>assets/ifinal/2.png" width="30" height="30">
                                                    </a>
                                                    <br style="font-size: 20px">Vueva Estructura Org.
                                                </center>
                                        </th>
                                        <th data-class="expand">DEPENDE</th>
                                        <th data-hide="phone"><i
                                                class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i>
                                            NOMBRE DE UNIDAD ORGANIZACIONAL
                                        </th>
                                        <th>MODIFICAR</th>
                                        <th>ELIMINAR</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach ($estructura as $variable) {
                                        ?>
                                        <tr id="tr<?php echo $variable['uni_id'] ?>">
                                            <td><?php echo $variable['uni_id']; ?></td>
                                            <td><?php
                                                if ($variable['uni_depende'] == 0) {
                                                    echo '<font size="1" style="color:red;"> No depende</font>';
                                                } else {
                                                    echo $variable['uni_depende'];
                                                }
                                                ?>
                                            </td>
                                            <td><?php echo $variable['uni_unidad']; ?></td>
                                            <input type="hidden" name="id_mod" id="id_mod"
                                                   value="<?php echo $variable['uni_id'] ?>">
                                            <td>
                                                <BUTTON data-toggle="modal" data-target="#modal_mod_uni"
                                                        class="btn btn-xs botones dos mod_uni"
                                                        name="<?php echo $variable['uni_id'] ?>" id="enviar_mod">
                                                    <div class="btn-hover-postion2">
                                                        <img src="<?php echo base_url() ?>assets/ifinal/modificar.png" title="modificar" id="modificar"> 
                                                        <p style="font-size: 8px;"><b>Modificar</b></p>
                                                    </div>
                                                </BUTTON>
                                            </td>
                                            <?php
                                            if ($variable['uni_depende'] == 0) {
                                                ?>
                                                <td>
                                                    <BUTTON disabled="disabled" class="btn btn-xs botones uno ">
                                                        <div class="btn-hover-postion1">
                                                            <img src="<?php echo base_url() ?>assets/ifinal/eliminar.png" title="eliminar" id="eliminar">
                                                            <p style="font-size: 8px;"><b>Eliminar</b></p>
                                                        </div>
                                                    </BUTTON>
                                                </td>
                                                <?php
                                            } else {
                                                ?>
                                                <td>
                                                    <BUTTON class="btn btn-xs botones uno del_uni"
                                                            name="<?php echo $variable['uni_id'] ?>" id="eliminar">
                                                        <div class="btn-hover-postion1">
                                                            <img src="<?php echo base_url() ?>assets/ifinal/eliminar.png" title="eliminar" id="eliminar">
                                                            <p style="font-size: 8px;"><b>Eliminar</b></p>
                                                        </div>

                                                    </BUTTON>
                                                </td>
                                                <?php
                                            }
                                            ?>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                    </tbody>
                                </table>

                            </div>
                            <!-- end widget content -->

                        </div>
                        <!-- end widget div -->

                    </div>
                    <!-- end widget -->
                </article>

            </div>

        </section>
        <!-- end widget grid -->

    </div>
    <!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->

<!-- ================== Modal NUEVO ESTRUCTURA ORGANIZACIONAL========================== -->
<div class="modal animated fadeInDown" id="modal_nuevo_uni" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close text-danger " data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title text-center text-dark">
                    <b><i class="glyphicon glyphicon-pencil"></i> NUEVA UNIDAD ORGANIZACIONAL</b>
                </h4>
            </div>
            <div class="modal-body no-padding">
                <div class="row">
                    <form id="form_uni" novalidate="novalidate" method="post">
                        <div id="bootstrap-wizard-1" class="col-sm-12">
                            <div class="well">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <LABEL><b>Ingrese el nombre de la Unidad Organizacional</b></label>
                                            <input class="form-control" type="text" name="uni_unidad" id="uni_unidad"
                                                   placeholder="Nombre de la Unidad Organizaional" style="width:100%;"
                                                   style="text-transform:uppercase;"
                                                   onkeyup="javascript:this.value=this.value.toUpperCase();"
                                                   onkeypress="if (this.value.length < 100) { return soloLetras(event);}else{return false; }">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <LABEL><b>Es dependiente? </b></LABEL><br>
                                            <input type="radio" id="depndiente" value="si" name="dependiente"
                                                   class="uni_si"/> SI
                                            <input type="radio" id="dependiente" value="no" name="dependiente"
                                                   class="uni_no"/> NO

                                        </div>
                                    </div>
                                </div>
                                <div id="content_parent" class="row" style="display: none;">
                                    <div class="col-md-1">
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <select name="padre" id="padre" class="form-control ">
                                                <option value="">Seleccione una Unidad</option>
                                                <?php
                                                foreach ($estructura as $row) {
                                                    /*if ($row['uni_depende'] == 0 && $row['uni_estado'] == 1) {
                                                        echo '<option value="' . $row['uni_id'] . '">' . $row['uni_unidad'] . '</option>';
                                                    }*/
                                                    echo '<option value="' . $row['uni_id'] . '">' . $row['uni_unidad'] . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <LABEL><b>Ingrese el Código</b></label>
                                            <input class="form-control" type="text" name="uni_codigo" id="uni_codigo"
                                                   placeholder="Ingrese Código"
                                                   onkeypress="if (this.value.length < 8) { return soloNumeros(event);}else{return false; }">
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- end well -->
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-3 pull-left">
                        <button class="btn btn-ms btn-danger" data-dismiss="modal">CANCELAR</button>
                    </div>
                    <div class="col-md-3 pull-right ">
                        <button type="submit" name="enviar_uni" id="enviar_uni" class="btn  btn-ms btn-primary"><i
                                class="fa fa-save"></i>
                            GUARDAR
                        </button>
                    </div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- ================== Modal  MODIFICAR   ESTRUCTURA ORGANIZACIONAL  ========================== -->
<div class="modal animated fadeInDown" id="modal_mod_uni" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close text-danger " data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title text-center text-darck">
                    <b><i class="glyphicon glyphicon-pencil"></i> MODIFICAR REGISTRO</b>
                </h4>
            </div>
            <div class="modal-body no-padding">
                <div class="row">
                    <form id="mod_formuni" name="mod_formuni" novalidate="novalidate" method="post">
                        <div id="bootstrap-wizard-1" class="col-sm-12">
                            <div class="well">
                                <div class="row">
                                    <div class="col-sm-11">
                                        <div class="form-group">
                                            <LABEL><b>Ingrese el Nombre de la Unidad Organizacional</b></label>
                                            <input class="form-control" type="text" name="moduni_unidad"
                                                   id="moduni_unidad" placeholder="Nombre de la Unidad Organizacional"
                                                   style="width:100%;" onkeyup="javascript:this.value=this.value.toUpperCase();"
                                                   onkeypress="if (this.value.length < 100) { return soloLetras(event);}else{return false; }">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <LABEL><b>Es dependiente? </b></LABEL><br>
                                            <input type="radio" id="moduni_si" value="si" name="moduni_si"
                                                   class="moduni_si custom-controls-stacked"/> SI
                                            <input type="radio" id="moduni_no" value="no" name="moduni_no"
                                                   class="moduni_no custom-controls-stacked"/> NO

                                        </div>
                                    </div>
                                </div>
                                <div id="modcontent_parent" class="row" style="display: none;">
                                    <div class="col-md-1">
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <select name="moduni_padre" id="moduni_padre" class="form-control ">
                                                <option value="">Seleccione una Unidad Organizacional</option>
                                                <?php
                                                foreach ($estructura as $row) {

                                                        echo '<option value="' . $row['uni_id'] . '">' . $row['uni_unidad'] . '</option>';

                                                }
                                                  /*  foreach ($estructura as $row) {
                                                        if ($row['uni_depende'] == 0) {
                                                            echo '<option value="' . $row['uni_id'] . '">' . $row['uni_unidad'] . '</option>';
                                                        }
                                                    }*/
                                                //echo '<option value="' . $row['uni_id'] . '">' . $row['uni_unidad'] . '</option>';
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <LABEL><b>Código</b></label>
                                            <input class="form-control" type="text" name="moduni_codigo"
                                                   id="moduni_codigo" placeholder="Ingrese Código"
                                                   onkeypress="if (this.value.length < 8) { return soloNumeros(event);}else{return false; }">
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- end well -->
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-3 pull-left">
                        <button class="btn btn-ms btn-danger" data-dismiss="modal">CANCELAR</button>
                    </div>
                    <div class="col-md-3 pull-right ">
                        <button type="submit" name="mod_unienviar" id="mod_unienviar" class="btn  btn-ms btn-primary"><i
                                class="fa fa-save"></i>
                            ACEPTAR
                        </button>
                    </div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- /.modal -->