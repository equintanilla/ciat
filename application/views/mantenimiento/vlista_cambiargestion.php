<style type="text/css">
    .btn{  transition-duration: 0.5s; }
    .btn:hover{transform: scale(1.2);}
</style>
<?php $site_url = site_url(""); ?>
<div id="main" role="main">
    <div id="ribbon">
        <ol class="breadcrumb">
            <li>Mantenimiento</li>
            <li>Cambiar Gestión</li>
        </ol>
    </div><br>
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-10 animated fadeInDown" align="center">
            <div class="alert alert-block alert-success" style="background-color: #568A89; color:white; height:50px; margin: 0 auto;">
                <h4><i class="icon fa fa-check" style="color:#e7f3ff;">&nbsp;</i>MODIFICAR GESTIÓN Y/O MES DE LA SESIÓN</h4>
            </div>
        </div>
    </div><br>
    <div id="content">
        <div class="row">
            <div class="col-md-6 animated fadeInLeft">
                <h1>Gestión Actual <?php echo $this->session->userdata('gestion');?></h1>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4>Cambiar Gestion</h4>
                    </div>
                    <div class="panel-body">
                        <?php echo $gestion;?>
                    </div>
                </div>
            </div>
            <div class="col-md-6 animated fadeInDown">
                <div class="table">
                    
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 animated fadeInLeft">
                <h1>Mes Actual <?php echo $mes_texto;?></h1>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4>Cambiar Mes</h4>
                    </div>
                    <div class="panel-body">
                        <form action="<?php echo $site_url . '/mantenimiento/cambiar_gestion/cambia_mes_sesion' ?>" id="form_cambio_mes" name="form_cambio_mes" method="post">
                            <select name="mes_sesion" id="mes_sesion" class="form-control" value="3">
                                <option value="1">seleccionar Mes</option>
                                <option value="1">Enero</option> 
                                <option value="2">Febrero</option> 
                                <option value="3">Marzo</option> 
                                <option value="4">Abril</option> 
                                <option value="5">Mayo</option>
                                <option value="6">Junio</option> 
                                <option value="7">Julio</option>
                                <option value="8">Agosto</option>
                                <option value="9">Septiembre</option> 
                                <option value="10">Octubre</option> 
                                <option value="11">Noviembre</option> 
                                <option value="12">Diciembre</option>  
                            </select>
                            <BUTTON type="submit" class="btn btn-xs btn-primary">
                                <div class="btn-hover-postion1">
                                    Seleccionar
                                </div>
                            </BUTTON>
                        </form>
                        
                    </div>
                </div>
            </div>
            <div class="col-md-6 animated fadeInDown">
                <div class="table">
                </div>
            </div>
        </div>
    </div>
</div>