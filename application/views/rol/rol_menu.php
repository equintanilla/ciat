

    <!-- MAIN PANEL -->
    <div id="main" role="main">
        <!-- RIBBON -->
        <div id="">
            <!-- breadcrumb -->
            <ol class="breadcrumb">
                <li>SESIÒN</li>
                <li>ROL</li>
            </ol>
        </div>
        <!-- END RIBBON -->

        <!-- MAIN CONTENT -->
        <div id="content">
        <div class="row"><!--row-->
            <div class="col-md-12">
            <style type="text/css">
            dl {
                width: 100%;
            }
            dt, dd {
                padding: 15px;
            }
            dt {
                background: #333333;
                color: white;
                border-bottom: 1px solid #141414;
                border-top: 1px solid #4E4E4E;
                font: icon;
                align-content: center;
                cursor: pointer;
            }
            dd {
                background: #F5F5F5;
                line-height: 1.6em;
            }
            dt.activo, dt:hover {
                background:#008B8B;
            }
            dt:before {
                content: "+";
                margin-right: 20px;
                font-size: 20px;
            }
            dt.activo:before {
                content: "-";
                margin-right: 20px;
                font-size: 20px;
            }
            /*iconos */
            .btn{  transition-duration: 0.5s; }
            .btn:hover{transform: scale(1.2);}
        </style>
        <!-- tabla -->
                       <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">


                    <!-- end widget -->

                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                            <h2 class="font-md"><strong> TABLA DE ROLES</strong></h2>
                        </header>
                        <!-- MAIN CONTENT -->
                        <!-- widget div-->
                        <div>

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body no-padding">

                                <table id="dt_basic" class="table table-striped table-bordered table-hover"
                                       width="100%">
                                    <thead>
                                    <tr>
                                        <th>Nº</th>
                                        <th data-hide="phone">ROLOS</th>
                                        <th data-hide="phone">MENU</th>
                                      

                                    </tr>
                                    </thead>
                                    <tbody>
                                    
                                       <?php echo $rol;?>
                                    </tbody>
                                </table>

                            </div>
                            <!-- end widget content -->

                        </div>
                        <!-- end widget div -->

                    </div>
                    <!-- end widget -->
                </article>
        <!-- end tabla -->
            
            </div>
        </div><!--end row-->
        </div>
        <!--END CONTENT-->
    </div>
    <!--END MAIN PANEL-->

                                
