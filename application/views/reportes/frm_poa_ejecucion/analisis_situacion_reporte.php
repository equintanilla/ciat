<script xmlns="http://www.w3.org/1999/html">
    function abreVentana(PDF) {
        var direccion;
        direccion = '' + PDF;
        window.open(direccion, "Reporte de Proyectos", "width=800,height=650,scrollbars=SI");
    };
</script>
<div id="main" role="main">
    <div id="">
        <ol class="breadcrumb">
            <li>Frm. POA Ejecución</li><li> Analisis de Situación</li>
        </ol>
    </div>
    <div id="content">
        <section id="widget-grid" class="">
            <div class="row">
            <div class="alert alert-block alert-success" >
              <a class="close" data-dismiss="alert" href="#">×</a>
              <h4 class="alert-heading"><i class="glyphicon glyphicon-user"></i><?php echo " FUNCIONARIO RESPONSABLE : ".$this->session->userdata("funcionario")."   ||  "; ?><i class="glyphicon glyphicon-calendar"></i><?php echo "FECHA ACTUAL : " . date("d") . " del " . date("m") . " de " . date("Y");?></h4>
            </div>
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <section id="widget-grid" class="well">
                  <div align="center">
                    <h1><b> <?php echo $this->session->userdata('entidad')?>  </b><br><small>FORMULARIOS POA - ACCIONES <br> AL MES DE <?php echo $mes;?>  de <?php echo $this->session->userdata("gestion");?></small></h1>
                  </div>
                </section>
            </article>
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-teal" id="wid-id-0" data-widget-editbutton="false">
                        <header>
                            <span class="widget-icon"> <span class="fa fa-table"></span> </span>
                            <h2>LISTA DE CARPETA POA</h2>
                        </header>
                        <div>
                            <div class="widget-body no-padding">
                                <div class="table-responsive">
                                    <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                        <thead>
                                            <tr>
                                                <th> AO </th>
                                                <th>PROGRAMA</th>
                                                <th>UNIDAD ORGANIZACIONAL</th>
                                                <th>FECHA DE CREACIÓN</th>
                                            </tr>
                                        </thead>
                                        <tbody id="bdi">
                                        <?php
                                        foreach($lista_poa as $row)
                                        {
                                            echo '<tr id="tr'.$row['poa_id'].'">';
                                            echo'<td>';
                                            ?>
                                                <a href="javascript:abreVentana('<?php echo site_url("admin").'/reporte_analisis_situacion/'.$row['poa_id']; ?>');"
                                                    data-toggle="tooltip" rel="tooltip" title="Generar Reporte">
                                                    <img src="<?php echo base_url();?>/assets/ifinal/pdf.png" width="30" height="30"
                                                    class="img-responsive">
                                                </a>
                                            <?php
                                            '</td>';
                                            echo '<td><font size="1">'.$row['aper_programa'].$row['aper_proyecto'].$row['aper_actividad']." - ".$row['aper_descripcion'].'</font></td>';
                                            echo '<td><font size="1">'.$row['uni_unidad'].'</font></td>';
                                            echo '<td><font size="1">'.$row['poa_fecha_creacion'].'</font></td>';
                                            echo '</tr>';
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </section>
    </div>
</div>
