<?php $atras = site_url() . '/rep/global' ?>
<div id="main" role="main">
    <div id="">
        <ol class="breadcrumb">
            <li>Reporte Gerencial</li>
            <li><a href="<?php echo $atras ?>">Reporte Global</a></li>
            <li>Reporte por Unidad Ejecutora</li>
        </ol>
    </div>
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-12 animated fadeInDown">
                <h1 class="page-title txt-color-blueDark"><i class="fa fa-pencil-square-o fa-fw ">&nbsp;</i><b>REPORTE POR UNIDAD EJECUTORA</b>
                </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <a href="<?php echo $atras ?>" class="btn btn-labeled btn-success" title="ATRAS">
                    <span class="btn-label"><i class="fa fa-arrow-left" aria-hidden="true"></i></span><font size="1">ATRAS </font>
                </a>
            </div>
        </div>
        <br>
        <article class="col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-10">
                <header>
                    <span class="widget-icon"> <i class="fa fa-pie-chart"></i> </span>

                    <h2>NIVEL DE REPORTE</h2>
                </header>
                <div>
                    <div class="widget-body no-padding">
                        <br>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel-group smart-accordion-default" id="accordion-2">
                                    <div class="panel panel-teal">
                                        <div class="panel-heading">
                                            <h4 class="panel-title" class="collapsed">
                                                <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseOne-1">
                                                    <i class="fa fa-fw fa-plus-circle  "></i><i class="fa fa-fw fa-minus-circle"></i>
                                                    REPORTE POR PROYECTO
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne-1" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="well well-sm col-sm-6">
                                                        <div id="pie_proy"></div>
                                                    </div>
                                                    <div class="well well-sm col-sm-6">
                                                        <div id="col_proy"></div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="well well-sm col-sm-12 table-responsive">
                                                        <style type="text/css">
                                                            .head_proy {
                                                                background-color: #568A89;
                                                                color: #ffffff;
                                                                align-content: center;
                                                                text-align: center;
                                                                vertical-align: middle%;
                                                            }

                                                            td {
                                                                color: black;
                                                                font-weight: bold;
                                                                text-align: right;
                                                            }

                                                            .descripcion {
                                                                text-align: left;
                                                            }
                                                        </style>
                                                        <table id="dt_basic2" class="table table-bordered table-responsive " width="100%">
                                                            <thead>
                                                            <tr>
                                                                <th rowspan="2" class="head_proy" style="vertical-align: middle">DETALLE</th>
                                                                <th colspan="3" class="head_proy">CANTIDAD</th>
                                                                <th colspan="4" class="head_proy">PRESUPUESTO</th>
                                                                <th colspan="3" class="head_proy">EJECUCIÓN FINANCIERA</th>
                                                            </tr>
                                                            <tr>
                                                                <th class="head_proy">INV</th>
                                                                <th class="head_proy">PINV</th>
                                                                <th class="head_proy">TOTAL</th>
                                                                <th class="head_proy">PPTO. INICIAL</th>
                                                                <th class="head_proy">MODIF. (Bs.)</th>
                                                                <th class="head_proy">PPTO. VIGENTE</th>
                                                                <th class="head_proy">[%]</th>
                                                                <th class="head_proy">PPTO. EJEC.</th>
                                                                <th class="head_proy">SALDO</th>
                                                                <th class="head_proy">EJEC[%]</th>
                                                            </tr>

                                                            </thead>
                                                            <tbody>
                                                            <?php
                                                            echo $tabla;
                                                            ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="panel panel-greenDark">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseTwo-1" class="collapsed">
                                                    <i class="fa fa-fw fa-plus-circle  "></i><i class="fa fa-fw fa-minus-circle  "></i>
                                                    REPORTE DE PRESUPUESTO DE INVERSIÓN PÚBLICA
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseTwo-1" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="well well-sm col-sm-6">
                                                        <div id="pie_proy_inv"></div>
                                                    </div>
                                                    <div class="well well-sm col-sm-6">
                                                        <div id="col_proy_inv"></div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="well well-sm col-sm-12 table-responsive">
                                                        <style type="text/css">
                                                            .head_pi {
                                                                background-color: #496949;
                                                                color: #ffffff;
                                                                align-content: center;
                                                                text-align: center;
                                                                vertical-align: middle%;
                                                            }
                                                        </style>
                                                        <table id="dt_basic2" class="table table-bordered table-responsive " width="100%">
                                                            <thead>
                                                            <tr>
                                                                <th rowspan="2" class="head_pi" style="vertical-align: middle">UNIDAD EJECUTORA</th>
                                                                <th colspan="3" class="head_pi">CANTIDAD</th>
                                                                <th colspan="4" class="head_pi">PRESUPUESTO</th>
                                                                <th colspan="3" class="head_pi">EJECUCIÓN FINANCIERA</th>
                                                            </tr>
                                                            <tr>
                                                                <th class="head_pi">INV</th>
                                                                <th class="head_pi">PINV</th>
                                                                <th class="head_pi">TOTAL</th>
                                                                <th class="head_pi">PPTO. INICIAL</th>
                                                                <th class="head_pi">MODIF. (Bs.)</th>
                                                                <th class="head_pi">PPTO. VIGENTE</th>
                                                                <th class="head_pi">[%]</th>
                                                                <th class="head_pi">PPTO. EJEC.</th>
                                                                <th class="head_pi">SALDO</th>
                                                                <th class="head_pi">EJEC[%]</th>
                                                            </tr>

                                                            </thead>
                                                            <tbody>
                                                            <?php
                                                            echo $tabla_proy_inv;
                                                            ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="panel panel-pinkDark">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-2" href="#collapseThree-1" class="collapsed">
                                                    <i class="fa fa-fw fa-plus-circle  "></i> <i class="fa fa-fw fa-minus-circle  "></i>
                                                    REPORTE DE PRESUPUESTO DE PROGRAMAS NO RECURRENTES
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseThree-1" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="well well-sm col-sm-6">
                                                        <div id="pie_prog_no_rec"></div>
                                                    </div>
                                                    <div class="well well-sm col-sm-6">
                                                        <div id="col_prog_no_rec"></div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="well well-sm col-sm-12 table-responsive">
                                                        <style type="text/css">
                                                            .head_pnr {
                                                                background-color: #A8829F;
                                                                color: #ffffff;
                                                                align-content: center;
                                                                text-align: center;
                                                                vertical-align: middle%;
                                                            }
                                                        </style>
                                                        <table id="dt_basic2" class="table table-bordered table-responsive " width="100%">
                                                            <thead>
                                                            <tr>
                                                                <th rowspan="2" class="head_pnr" style="vertical-align: middle">UNIDAD EJECUTORA</th>
                                                                <th colspan="3" class="head_pnr">CANTIDAD</th>
                                                                <th colspan="4" class="head_pnr">PRESUPUESTO</th>
                                                                <th colspan="3" class="head_pnr">EJECUCIÓN FINANCIERA</th>
                                                            </tr>
                                                            <tr>
                                                                <th class="head_pnr">P-NR</th>
                                                                <th class="head_pnr">P-R</th>
                                                                <th class="head_pnr">TOTAL</th>
                                                                <th class="head_pnr">PPTO. INICIAL</th>
                                                                <th class="head_pnr">MODIF. (Bs.)</th>
                                                                <th class="head_pnr">PPTO. VIGENTE</th>
                                                                <th class="head_pnr">[%]</th>
                                                                <th class="head_pnr">PPTO. EJEC.</th>
                                                                <th class="head_pnr">SALDO</th>
                                                                <th class="head_pnr">EJEC[%]</th>
                                                            </tr>

                                                            </thead>
                                                            <tbody>
                                                            <?php
                                                            echo $tabla_prog_no_rec;
                                                            ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="panel panel-orange">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-2" href="#collapsefor-1" class="collapsed">
                                                    <i class="fa fa-fw fa-plus-circle  "></i> <i class="fa fa-fw fa-minus-circle  "></i>
                                                    REPORTE DE PROGRAMA DE INVERSIÓN PÚBLICA
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapsefor-1" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="well well-sm col-sm-6">
                                                        <div id="pie_pi_pnr"></div>
                                                    </div>
                                                    <div class="well well-sm col-sm-6">
                                                        <div id="col_pi_pnr"></div>
                                                    </div>
                                                </div>
                                                <style type="text/css">
                                                    .head__pi_pnr {
                                                        background-color: #C79121;
                                                        color: #ffffff;
                                                        align-content: center;
                                                        text-align: center;
                                                        vertical-align: middle%;
                                                    }
                                                </style>
                                                <table id="dt_basic2" class="table table-bordered table-responsive " width="100%">
                                                    <thead>
                                                    <tr>
                                                        <th rowspan="2" class="head__pi_pnr" style="vertical-align: middle">UNIDAD EJECUTORA</th>
                                                        <th colspan="3" class="head__pi_pnr">CANTIDAD</th>
                                                        <th colspan="4" class="head__pi_pnr">PRESUPUESTO</th>
                                                        <th colspan="3" class="head__pi_pnr">EJECUCIÓN FINANCIERA</th>
                                                    </tr>
                                                    <tr>
                                                        <th class="head__pi_pnr">INV.</th>
                                                        <th class="head__pi_pnr">P.N.R.</th>
                                                        <th class="head__pi_pnr">TOTAL</th>
                                                        <th class="head__pi_pnr">PPTO. INICIAL</th>
                                                        <th class="head__pi_pnr">MODIF. (Bs.)</th>
                                                        <th class="head__pi_pnr">PPTO. VIGENTE</th>
                                                        <th class="head__pi_pnr">[%]</th>
                                                        <th class="head__pi_pnr">PPTO. EJEC.</th>
                                                        <th class="head__pi_pnr">SALDO</th>
                                                        <th class="head__pi_pnr">EJEC[%]</th>
                                                    </tr>

                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                    echo $tabla_pi_pnr;
                                                    ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </article>


    </div>
</div>
<!-- HIGH CHART  -->
<script src="<?php echo base_url(); ?>assets/highcharts/js/highcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/highcharts/js/highcharts-more.js"></script>
<script src="<?php echo base_url(); ?>assets/highcharts/js/modules/exporting.js"></script>
<script src="<?php echo base_url(); ?>assets/highcharts/js/highcharts-3d.js"></script>
<script type="text/javascript">
    Highcharts.setOptions({
        lang: {
            contextButtonTitle: 'Opciones',
            printChart: 'Imprimir',
            downloadJPEG: 'Descargar en JPEG',
            downloadPDF: 'Descargar en PDF',
            downloadPNG: 'Descargar en PNG',
            downloadSVG: 'Descargar en SVG',
            loading: 'Cargando....'
        },
        credits: {
            enabled: false
        }
    });
    Highcharts.chart('pie_proy', {
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45
            }
        },
        title: {
            text: 'REPORTE POR UNIDAD EJECUTORA'
        },
        subtitle: {
            text: 'POR DE PROYECTOS'
        },
        plotOptions: {
            pie: {
                innerSize: 100,
                depth: 45
            }
        },
        series: [{
            name: 'Total[%] :',
            data: [
                <?php echo $pie_proy?>
            ]
        }]
    });
    var chart = Highcharts.chart('col_proy', {

        title: {
            text: 'EJECUCIÓN FINANCIERA [%]'
        },

        subtitle: {
            text: 'Proyectos'
        },
        yAxis: {
            min: 0,
            title: {
                text: 'PORCENTAJES (%)'
            },
            stackLabels: {
                enabled: false,

            }
        },
        xAxis: {
            categories: [<?php echo $col_proy_titulo?>]
        },

        series: [{
            type: 'column',
            colorByPoint: true,
            data: [<?php echo $col_proy_data?>],
            showInLegend: false
        }]

    });
    //inversion  publica
    Highcharts.chart('pie_proy_inv', {
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45
            }
        },
        title: {
            text: 'PRESUPUESTO DE INVERSIÓN PUBLICA'
        },
        subtitle: {
            text: 'UNIDAD EJECUTORA'
        },
        plotOptions: {
            pie: {
                innerSize: 100,
                depth: 45
            }
        },
        series: [{
            name: 'Total[%] :',
            data: [
                <?php echo $pie_proy_inv?>
            ]
        }]
    });
    var chart2 = Highcharts.chart('col_proy_inv', {

        title: {
            text: 'EJECUCIÓN FINANCIERA [%]'
        },

        subtitle: {
            text: 'UNIDAD EJECUTORA'
        },
        yAxis: {
            min: 0,
            title: {
                text: 'PORCENTAJES (%)'
            },
            stackLabels: {
                enabled: false,

            }
        },
        xAxis: {
            categories: [<?php echo $col_titulo_proy_inv?>]
        },

        series: [{
            type: 'column',
            colorByPoint: true,
            data: [<?php echo $col_data_proy_inv?>],
            showInLegend: false
        }]

    });
    //programa no recurrente
    Highcharts.chart('pie_prog_no_rec', {
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45
            }
        },
        title: {
            text: 'PRESUPUESTO DE PROGRAMAS NO RECURRENTES'
        },
        subtitle: {
            text: 'UNIDAD EJECUTORA'
        },
        plotOptions: {
            pie: {
                innerSize: 100,
                depth: 45
            }
        },
        series: [{
            name: 'Total[%] :',
            data: [
                <?php echo $pie_prog_no_rec?>
            ]
        }]
    });
    var chart3 = Highcharts.chart('col_prog_no_rec', {

        title: {
            text: 'EJECUCIÓN FINANCIERA [%]'
        },

        subtitle: {
            text: 'UNIDAD EJECUTORA'
        },
        yAxis: {
            min: 0,
            title: {
                text: 'PORCENTAJES (%)'
            },
            stackLabels: {
                enabled: false,

            }
        },
        xAxis: {
            categories: [<?php echo $col_titulo_prog_no_rec?>]
        },

        series: [{
            type: 'column',
            colorByPoint: true,
            data: [<?php echo $col_data_prog_no_rec?>],
            showInLegend: false
        }]

    });
    //proyecto de inversion  + programa no recurrente
    Highcharts.chart('pie_pi_pnr', {
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45
            }
        },
        title: {
            text: 'PRESUPUESTO DE PROGRAMAS NO RECURRENTES'
        },
        subtitle: {
            text: 'UNIDAD EJECUTORA'
        },
        plotOptions: {
            pie: {
                innerSize: 100,
                depth: 45
            }
        },
        series: [{
            name: 'Total[%] :',
            data: [
                <?php echo $pie_prog_no_rec?>
            ]
        }]
    });
    var chart3 = Highcharts.chart('col_pi_pnr', {

        title: {
            text: 'EJECUCIÓN FINANCIERA [%]'
        },

        subtitle: {
            text: 'UNIDAD EJECUTORA'
        },
        yAxis: {
            min: 0,
            title: {
                text: 'PORCENTAJES (%)'
            },
            stackLabels: {
                enabled: false,

            }
        },
        xAxis: {
            categories: [<?php echo $col_titulo_pi_pnr?>]
        },

        series: [{
            type: 'column',
            colorByPoint: true,
            data: [<?php echo $col_data_pi_pnr?>],
            showInLegend: false
        }]

    });
</script>


