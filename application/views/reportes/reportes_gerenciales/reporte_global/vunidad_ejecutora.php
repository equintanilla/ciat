<?php $atras = site_url() . '/rep/global' ?>
<div id="main" role="main">
    <div id="">
        <ol class="breadcrumb">
            <li>Reporte Gerencial</li>
            <li><a href="<?php echo $atras ?>">Reporte Global</a></li>
            <li>Reporte por Unidad Ejecutora</li>
        </ol>
    </div>
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-12 animated fadeInDown">
                <h1 class="page-title txt-color-blueDark"><i class="fa fa-pencil-square-o fa-fw ">&nbsp;</i><b>REPORTE POR UNIDAD EJECUTORA</b>
                </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <a href="<?php echo $atras ?>" class="btn btn-labeled btn-success" title="ATRAS">
                    <span class="btn-label"><i class="fa fa-arrow-left" aria-hidden="true"></i></span><font size="1">ATRAS </font>
                </a>
            </div>
        </div>
        <br>
        <article class="col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-10">
                <header>
                    <span class="widget-icon"> <i class="fa fa-pie-chart"></i> </span>

                    <h2>NIVEL DE REPORTE</h2>
                </header>
                <div>
                    <div class="widget-body no-padding">
                        <br>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="well well-sm col-sm-6">
                                        <div id="pie_pi_pnr"></div>
                                    </div>
                                    <div class="well well-sm col-sm-6">
                                        <div id="col_pi_pnr"></div>
                                    </div>
                                </div>
                                <style type="text/css">
                                    .head__pi_pnr {
                                        background-color: #404040;
                                        color: #ffffff;
                                        align-content: center;
                                        text-align: center;
                                        vertical-align: middle%;
                                    }
                                </style>
                                <table id="dt_basic2" class="table table-bordered table-responsive " width="100%">
                                    <thead>
                                    <tr>
                                        <th rowspan="2" class="head__pi_pnr" style="vertical-align: middle">UNIDAD EJECUTORA</th>
                                        <th colspan="8" class="head__pi_pnr">CANTIDAD</th>
                                        <th colspan="4" class="head__pi_pnr">PRESUPUESTO</th>
                                        <th colspan="3" class="head__pi_pnr">EJECUCIÓN FINANCIERA</th>
                                    </tr>
                                    <tr>
                                        <th class="head__pi_pnr">INV.</th>
                                        <th class="head__pi_pnr">P.R.</th>
                                        <th class="head__pi_pnr">P.N.R.</th>
                                        <th class="head__pi_pnr">A.F.</th>
                                        <th class="head__pi_pnr">P.INV.</th>
                                        <th class="head__pi_pnr">E.INV.</th>
                                        <th class="head__pi_pnr">E.OPE.</th>

                                        <th class="head__pi_pnr">TOTAL</th>
                                        <th class="head__pi_pnr">PPTO. INICIAL</th>
                                        <th class="head__pi_pnr">MODIF. (Bs.)</th>
                                        <th class="head__pi_pnr">PPTO. VIGENTE</th>
                                        <th class="head__pi_pnr">[%]</th>
                                        <th class="head__pi_pnr">PPTO. EJEC.</th>
                                        <th class="head__pi_pnr">SALDO</th>
                                        <th class="head__pi_pnr">EJEC[%]</th>
                                    </tr>

                                    </thead>
                                    <tbody>
                                    <?php
                                    //echo $tabla_pi_pnr;
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </article>


    </div>
</div>
<!-- HIGH CHART  -->
<script src="<?php echo base_url(); ?>assets/highcharts/js/highcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/highcharts/js/highcharts-more.js"></script>
<script src="<?php echo base_url(); ?>assets/highcharts/js/modules/exporting.js"></script>
<script src="<?php echo base_url(); ?>assets/highcharts/js/highcharts-3d.js"></script>
<script type="text/javascript">
    Highcharts.setOptions({
        lang: {
            contextButtonTitle: 'Opciones',
            printChart: 'Imprimir',
            downloadJPEG: 'Descargar en JPEG',
            downloadPDF: 'Descargar en PDF',
            downloadPNG: 'Descargar en PNG',
            downloadSVG: 'Descargar en SVG',
            loading: 'Cargando....'
        },
        credits: {
            enabled: false
        }
    });
    Highcharts.chart('pie_proy', {
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45
            }
        },
        title: {
            text: 'REPORTE POR UNIDAD EJECUTORA'
        },
        subtitle: {
            text: 'POR DE PROYECTOS'
        },
        plotOptions: {
            pie: {
                innerSize: 100,
                depth: 45
            }
        },
        series: [{
            name: 'Total[%] :',
            data: [
                <?php echo $pie_proy?>
            ]
        }]
    });
    var chart = Highcharts.chart('col_proy', {

        title: {
            text: 'EJECUCIÓN FINANCIERA [%]'
        },

        subtitle: {
            text: 'Proyectos'
        },
        yAxis: {
            min: 0,
            title: {
                text: 'PORCENTAJES (%)'
            },
            stackLabels: {
                enabled: false,

            }
        },
        xAxis: {
            categories: [<?php echo $col_proy_titulo?>]
        },

        series: [{
            type: 'column',
            colorByPoint: true,
            data: [<?php echo $col_proy_data?>],
            showInLegend: false
        }]

    });
    //inversion  publica
    Highcharts.chart('pie_proy_inv', {
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45
            }
        },
        title: {
            text: 'PRESUPUESTO DE INVERSIÓN PUBLICA'
        },
        subtitle: {
            text: 'UNIDAD EJECUTORA'
        },
        plotOptions: {
            pie: {
                innerSize: 100,
                depth: 45
            }
        },
        series: [{
            name: 'Total[%] :',
            data: [
                <?php echo $pie_proy_inv?>
            ]
        }]
    });
    var chart2 = Highcharts.chart('col_proy_inv', {

        title: {
            text: 'EJECUCIÓN FINANCIERA [%]'
        },

        subtitle: {
            text: 'UNIDAD EJECUTORA'
        },
        yAxis: {
            min: 0,
            title: {
                text: 'PORCENTAJES (%)'
            },
            stackLabels: {
                enabled: false,

            }
        },
        xAxis: {
            categories: [<?php echo $col_titulo_proy_inv?>]
        },

        series: [{
            type: 'column',
            colorByPoint: true,
            data: [<?php echo $col_data_proy_inv?>],
            showInLegend: false
        }]

    });
    //programa no recurrente
    Highcharts.chart('pie_prog_no_rec', {
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45
            }
        },
        title: {
            text: 'PRESUPUESTO DE PROGRAMAS NO RECURRENTES'
        },
        subtitle: {
            text: 'UNIDAD EJECUTORA'
        },
        plotOptions: {
            pie: {
                innerSize: 100,
                depth: 45
            }
        },
        series: [{
            name: 'Total[%] :',
            data: [
                <?php echo $pie_prog_no_rec?>
            ]
        }]
    });
    var chart3 = Highcharts.chart('col_prog_no_rec', {

        title: {
            text: 'EJECUCIÓN FINANCIERA [%]'
        },

        subtitle: {
            text: 'UNIDAD EJECUTORA'
        },
        yAxis: {
            min: 0,
            title: {
                text: 'PORCENTAJES (%)'
            },
            stackLabels: {
                enabled: false,

            }
        },
        xAxis: {
            categories: [<?php echo $col_titulo_prog_no_rec?>]
        },

        series: [{
            type: 'column',
            colorByPoint: true,
            data: [<?php echo $col_data_prog_no_rec?>],
            showInLegend: false
        }]

    });
    //proyecto de inversion  + programa no recurrente
    Highcharts.chart('pie_pi_pnr', {
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45
            }
        },
        title: {
            text: 'PRESUPUESTO DE PROGRAMAS NO RECURRENTES'
        },
        subtitle: {
            text: 'UNIDAD EJECUTORA'
        },
        plotOptions: {
            pie: {
                innerSize: 100,
                depth: 45
            }
        },
        series: [{
            name: 'Total[%] :',
            data: [
                <?php echo $pie_prog_no_rec?>
            ]
        }]
    });
    var chart3 = Highcharts.chart('col_pi_pnr', {

        title: {
            text: 'EJECUCIÓN FINANCIERA [%]'
        },

        subtitle: {
            text: 'UNIDAD EJECUTORA'
        },
        yAxis: {
            min: 0,
            title: {
                text: 'PORCENTAJES (%)'
            },
            stackLabels: {
                enabled: false,

            }
        },
        xAxis: {
            categories: [<?php echo $col_titulo_pi_pnr?>]
        },

        series: [{
            type: 'column',
            colorByPoint: true,
            data: [<?php echo $col_data_pi_pnr?>],
            showInLegend: false
        }]

    });
</script>


