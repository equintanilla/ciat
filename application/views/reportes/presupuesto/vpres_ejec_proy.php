<div id="main" role="main">
    <div id="">
        <ol class="breadcrumb">
            <li>Presupuesto</li>
            <li><a href="<?php echo $atras_main ?>">Lista de Programas</a></li>
            <li><a href="<?php echo $atras ?>">Lista de Proyectos</a></li>
            <li>Presupuesto Programado</li>
        </ol>
    </div>
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-12 animated fadeInDown">
                <h1 class="page-title txt-color-blueDark"><i class="fa fa-pencil-square-o fa-fw ">&nbsp;</i>
                    PRESUPUESTO EJECUTADO
                </h1>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <a href="<?php echo $atras ?>" class="btn btn-labeled btn-success" title="ATRAS">
                    <span class="btn-label"><i class="fa fa-arrow-left" aria-hidden="true"></i></span><font size="1">ATRAS </font>
                </a>
            </div>
        </div>
        <br>

    </div>
    <div class="row">
        <article class="col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget jarviswidget-color-teal" id="wid-id-10">
                <header>
                    <span class="widget-icon"> <i class="fa fa-list-ol" aria-hidden="true"></i></span>

                    <h2></h2>
                </header>
                <div>
                    <div class="widget-body no-padding">
                        <div class="table-responsive">
                            <style type="text/css">
                                th {
                                    color: black;
                                    font-weight: bold;
                                }
                            </style>
                            <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                <tr>
                                    <th>PARTIDA</th>
                                    <th>DESCRIPCIÓN</th>
                                    <th>F.F</th>
                                    <th>O.F</th>
                                    <th>E.T</th>
                                    <th>PPTO. INICIAL</th>
                                    <th>MOD. APROBADAS</th>
                                    <th>PPTO. VIGENTE</th>
                                    <th><center>PRESUPUESTO MENSUAL</center></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                    echo $tabla_prog_ejec;
                                ?>
                                </tbody>
                                <style type="text/css">
                                    th{
                                        background-color: #B0E0E6;
                                        font-weight: bold;
                                        color: black;
                                    }
                                </style>
                                <tfoot>
                                    <tr>
                                        <?php echo $footer?>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </article>
    </div>
</div>
</div>
