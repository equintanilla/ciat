<div id="main" role="main">
    <div id="">
        <ol class="breadcrumb">
            <li>Presupuesto</li>
            <li>Lista de Programas</li>
        </ol>
    </div>
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-12 animated fadeInDown">
                <h1 class="page-title txt-color-blueDark"><i class="fa fa-table fa-fw"></i> LISTA DE PROGRAMAS</h1>
            </div>
        </div>
        <section id="widget-grid" class="">
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-teal" id="wid-id-0" data-widget-editbutton="false">
                        <header>
                            <span class="widget-icon"> <span class="fa fa-table"></span> </span>
                            <h2>PROGRAMAS</h2>
                        </header>
                        <div>
                            <div class="widget-body no-padding">
                                <div class="table-responsive">
                                    <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                        <thead>
                                        <tr>
                                            <th>NRO.</th>
                                            <th title="LISTA DE PROYECTOS"><center>SELECCI&Oacute;N <br>LISTA DE <br> ACCIONES</center></th>
                                            <th>PROGRAMA</th>
                                            <th>DESCRIPCI&Oacute;N</th>
                                            <th>UNIDAD RESPONSABLE</th>
                                            <th>FECHA DE CREACIÓN</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $ruta = site_url() . '/' . $ruta_proy . '/';
                                        $ruta_img = base_url() . 'assets/ifinal/carp.jpg';
                                        $cont = 1;
                                        foreach ($lista_poa as $row) {
                                            echo '<tr>';
                                            echo '<td>' . $cont . '</td>';
                                            echo '<td>
                                                      <a href="' . $ruta . $row['aper_programa'] . '">
											            <img src="' . $ruta_img . '" width="30" height="30" class="img-responsive "title="LISTA DE PROYECTOS">
											          </a>
											      </td>';
                                            echo '<td><font size="1">' . $row['aper_programa'] . '-' . $row['aper_proyecto'] . '-' . $row['aper_actividad'] . '</font></td>';
                                            echo '<td><font size="1">' . $row['aper_descripcion'] . '</font></td>';
                                            echo '<td><font size="1">' . $row['uni_unidad'] . '</font></td>';
                                            echo '<td><font size="1">' . $row['poa_fecha_creacion'] . '</font></td>';
                                            echo '</tr>';
                                            $cont++;
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </section>
    </div>
</div>




