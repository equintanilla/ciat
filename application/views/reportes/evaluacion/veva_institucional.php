<?php
$gestion = $this->session->userdata('gestion');
$mes_id = $this->session->userdata('mes');
?>
<div id="main" role="main">
    <div id="">
        <ol class="breadcrumb">
            <li>Evaluación</li>
            <li>Evaluación Institucional</li>
        </ol>
    </div>
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-12 animated fadeInDown">
                <h1 class="page-title txt-color-blueDark"><i class="fa fa-pencil-square-o fa-fw ">&nbsp;</i>
                    EVALUACIÓN INSTITUCIONAL
                </h1>
            </div>
        </div>

        <div class="row">

            <article class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <div class="jarviswidget jarviswidget-color-teal" id="wid-id-0" data-widget-editbutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-table"></i> </span>

                        <h2>EFICACIA OBJETIVO DE GESTIÓN</h2>
                    </header>
                    <div>
                        <div class="widget-body">
                            <div class="row">
                                <div class="col-sm-12" id="eficacia_ogestion">

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 table-responsive" style="overflow-x: scroll">
                                    <style type="text/css">
                                        .th_o {
                                            background-color: #568A89;
                                            color: white;
                                        }
                                    </style>
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th class="th_o">ENE.</th>
                                            <th class="th_o">FEB.</th>
                                            <th class="th_o">MAR.</th>
                                            <th class="th_o">ABR.</th>
                                            <th class="th_o">MAY.</th>
                                            <th class="th_o">JUN.</th>
                                            <th class="th_o">JUL.</th>
                                            <th class="th_o">AGO.</th>
                                            <th class="th_o">SEP.</th>
                                            <th class="th_o">OCT.</th>
                                            <th class="th_o">NOV.</th>
                                            <th class="th_o">DIC.</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <?php
                                            echo $eficacia_ogestion;
                                            ?>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
            <article class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-table"></i> </span>

                        <h2>EFICACIA PRODUCTO TERMINAL</h2>
                    </header>
                    <div>
                        <div class="widget-body">
                            <div class="row">
                                <div class="col-sm-12" id="eficacia_pterminal">

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 table-responsive" style="overflow-x: scroll">
                                    <style type="text/css">
                                        .th_pt {
                                            background-color: #404040;
                                            color: white;
                                        }

                                    </style>
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th class="th_pt">ENE.</th>
                                            <th class="th_pt">FEB.</th>
                                            <th class="th_pt">MAR.</th>
                                            <th class="th_pt">ABR.</th>
                                            <th class="th_pt">MAY.</th>
                                            <th class="th_pt">JUN.</th>
                                            <th class="th_pt">JUL.</th>
                                            <th class="th_pt">AGO.</th>
                                            <th class="th_pt">SEP.</th>
                                            <th class="th_pt">OCT.</th>
                                            <th class="th_pt">NOV.</th>
                                            <th class="th_pt">DIC.</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <?php
                                            echo $eficacia_pterminal;
                                            ?>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>

        <div class="row">
            <article class="col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-blue" id="wid-id-1" data-widget-editbutton="false">
                    <header>
                        <span class="widget-icon"> <span class="glyphicon glyphicon-folder-close"></span></i> </span>

                        <h2>EFICACIA DE EJECUCIÓN PRESUPUESTARIA</h2>
                    </header>
                    <div class="widget-body">
                        <div class="row">
                            <div class="col-sm-12" id="graf_tacometro">

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <style type="text/css">
                                    th {
                                        background-color: #60747C;
                                        color: white;
                                    }
                                </style>
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>DESCRIPCIÓN</th>
                                        <th>PRESUPUESTO INICIAL</th>
                                        <th>MODIFICACIONES</th>
                                        <th>PRESUPUESTO VIGENTE <?php echo $gestion ?></th>
                                        <th>PROGRAMADO A <?php echo $mes[$mes_id] ?></th>
                                        <th>EJECUTADO A <?php echo $mes[$mes_id] ?></th>
                                        <th>EJECUCIÓN A <?php echo $mes[$mes_id] ?> RESPECTO A LO PROGRAMADO</th>
                                        <th>EJECUCIÓN A <?php echo $mes[$mes_id] ?> RESPECTO AL TOTAL</th>
                                        <th>DESEMPEÑO A <?php echo $mes[$mes_id] ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <?php
                                        $ejec_resp_prog = $lista_ejec_pres->ejec_respecto_prog;
                                        echo '<td>'.$this->session->userdata('entidad').'</td>';
                                        echo '<td>' . number_format($lista_ejec_pres->ppto_inicial, 2, ',', '.') . '</td>';
                                        echo '<td>' . number_format($lista_ejec_pres->modif_aprobadas, 2, ',', '.') . '</td>';
                                        echo '<td>' . number_format($lista_ejec_pres->ppto_vigente, 2, ',', '.') . '</td>';
                                        echo '<td>' . number_format($lista_ejec_pres->programado, 2, ',', '.') . '</td>';
                                        echo '<td>' . number_format($lista_ejec_pres->ejecutado, 2, ',', '.') . '</td>';
                                        echo '<td>' . number_format($lista_ejec_pres->ejec_respecto_prog, 2, ',', '.') . '%</td>';
                                        echo '<td>' . number_format($lista_ejec_pres->ejec_respecto_total, 2, ',', '.') . '%</td>';
                                        if ($ejec_resp_prog <= 30) {
                                            echo '<td><button class="btn btn-sm" style="background-color:red;color: white;font-weight: bold"> PÉSIMO </button></td>';
                                        } elseif ($ejec_resp_prog >= 31 && $ejec_resp_prog <= 50) {
                                            echo '<td><button class="btn btn-sm" style="background-color:orange;color: white;font-weight: bold"> MALO</button></td>';
                                        } elseif ($ejec_resp_prog >= 51 && $ejec_resp_prog <= 65) {
                                            echo '<td><button class="btn btn-sm" style="background-color:#ffdf32;color: white;font-weight: bold"> REGULAR</button></td>';
                                        } elseif ($ejec_resp_prog >= 66 && $ejec_resp_prog <= 79) {
                                            echo '<td><button class="btn btn-sm" style="background-color:green;color: white;font-weight: bold" > BUENO</button></td>';
                                        } elseif ($ejec_resp_prog >= 80 && $ejec_resp_prog <= 90) {
                                            echo '<td><button class="btn btn-sm" style="background-color:cornflowerblue; color:white;font-weight: bold"> MUY BUENO</button></td>';
                                        } elseif ($ejec_resp_prog >= 91) {
                                            echo '<td><button class="btn btn-sm" style="background-color:blue;color:white;font-weight: bold"> EXCELENTE</button></td>';
                                        }
                                        ?>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>

    </div>
</div>
<!--graficos-->
<script src="<?php echo base_url(); ?>assets/highcharts/js/highcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/highcharts/js/highcharts-more.js"></script>
<script src="<?php echo base_url(); ?>assets/highcharts/js/modules/exporting.js"></script>
<script src="<?php echo base_url(); ?>assets/highcharts/js/highcharts-3d.js"></script>
<script type="text/javascript">
    Highcharts.setOptions({
        lang: {
            contextButtonTitle: 'Opciones',
            printChart: 'Imprimir Tacometro',
            downloadJPEG: 'Descargar en JPEG',
            downloadPDF: 'Descargar en PDF',
            downloadPNG: 'Descargar en PNG',
            downloadSVG: 'Descargar en SVG',
            loading: 'loading....'
        }
    });
    Highcharts.chart('eficacia_ogestion', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'SEMÁFORO EFICACIA OBETIVO DE GESTIÓN'
        },
        xAxis: {
            categories: ['ENERO', 'FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO', 'JULIO', 'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE', 'NOVIEMBRE', 'DICIEMBRE']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'PORCENTAJES (%)'
            },
            stackLabels: {
                enabled: false,

            }
        },
        legend: {
            align: 'right',
            x: -30,
            verticalAlign: 'top',
            y: 25,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: true
        },
        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '{series.name}: <br/> TOTAL:   {point.y}'
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: false,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                }
            }
        },
        series: [{
            name: '<b style="color: #FF0000;">MENOR A 75%</b>',
            data: [<?php echo $o_menor?>]

        }, {
            name: '<b style="color: #d6d21f;">ENTRE 76% Y 90%</b>',
            data: [<?php echo $o_entre?>]
        }, {
            name: '<b style="color: green;">MAYOR A 91%</b>',
            data: [<?php echo $o_mayor?>]
        }]
    });
</script>
<script type="text/javascript">
    Highcharts.chart('eficacia_pterminal', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'SEMÁFORO EFICACIA PRODUCTO TERMINAL'
        },
        xAxis: {
            categories: ['ENERO', 'FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO', 'JULIO', 'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE', 'NOVIEMBRE', 'DICIEMBRE']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'PORCENTAJES (%)'
            },
            stackLabels: {
                enabled: false,

            }
        },
        legend: {
            align: 'right',
            x: -30,
            verticalAlign: 'top',
            y: 25,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: true
        },
        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '{series.name}: <br/> TOTAL:   {point.y}'
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: false,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                }
            }
        },
        series: [{
            name: '<b style="color: #FF0000;">MENOR A 75%</b>',
            data: [<?php echo $pt_menor?>]

        }, {
            name: '<b style="color: #d6d21f;">ENTRE 76% Y 90%</b>',
            data: [<?php echo $pt_entre?>]
        }, {
            name: '<b style="color: green;">MAYOR A 91%</b>',
            data: [<?php echo $pt_mayor?>]
        }]
    });
</script>
<script type="text/javascript">
    Highcharts.chart('graf_tacometro', {
            chart: {
                type: 'gauge',
                plotBackgroundColor: null,
                plotBackgroundImage: null,
                plotBorderWidth: 0,
                plotShadow: false
            },

            title: {
                text: '<b>EJECUCIÓN PRESUPUESTARIA A NIVEL INSTITUCIONAL A <?php echo $mes[$mes_id]?> GESTIÓN <?php echo $gestion?></b>'
            },

            pane: {
                startAngle: -120,
                endAngle: 120,
                background: [{
                    backgroundColor: {
                        linearGradient: {x1: 0, y1: 0, x2: 0, y2: 1},
                        stops: [
                            [0, '#FFF'],
                            [1, '#333']
                        ]
                    },
                    borderWidth: 0,
                    outerRadius: '109%'
                }, {
                    backgroundColor: {
                        linearGradient: {x1: 0, y1: 0, x2: 0, y2: 1},
                        stops: [
                            [0, '#333'],
                            [1, '#FFF']
                        ]
                    },
                    borderWidth: 1,
                    outerRadius: '107%'
                }, {
                    // default background
                }, {
                    backgroundColor: '#DDD',
                    borderWidth: 0,
                    outerRadius: '105%',
                    innerRadius: '103%'
                }]
            },

            credits: {
                enabled: true,
                href: "#",
                position: {
                    align: 'center',
                    x: -3,
                    verticalAlign: 'bottom',
                    y: -115
                },
                style: {
                    color: '#ffff0',
                    fontSize: '12px'
                },
                text: '<br><strong>EJECUCIÓN A <?php echo $mes[$mes_id]?><br> <strong>RESPECTO A LO PROGRAMADO</strong> </strong><br><strong><?php echo
                $lista_ejec_pres->ejec_respecto_prog?>%</strong>'
            },
            // the value axis
            yAxis: {
                min: 0,
                max: 100,

                minorTickInterval: 'auto',
                minorTickWidth: 1,
                minorTickLength: 10,
                minorTickPosition: 'inside',
                minorTickColor: '#000000',

                tickPixelInterval: 30,
                tickWidth: 2,
                tickPosition: 'inside',
                tickLength: 10,
                tickColor: '#000000',
                labels: {
                    step: 2,
                    rotation: 'auto'
                },
                title: {
                    text: 'Procentaje [%]'
                },
                plotBands: [{
                    from: 0,
                    to: 30,
                    color: '#FF0000' // rojo
                }, {
                    from: 30,
                    to: 50,
                    color: '#FF9900' // naranja
                }, {
                    from: 50,
                    to: 65,
                    color: '#FFFF00' // amarillo
                }, {
                    from: 65,
                    to: 79,
                    color: '#006400' // verde
                }, {
                    from: 79,
                    to: 90,
                    color: '#00CCFF' // celeste
                }, {
                    from: 90,
                    to: 100,
                    color: '#0000CC' // celeste
                }


                ]
            },

            series: [{
                name: 'Ejecución a <?php echo $mes[$mes_id] ?> respecto a lo programado',
                data: [<?php echo $lista_ejec_pres->ejec_respecto_prog?>],
                tooltip: {
                    valueSuffix: '[%]'
                }
            }]

        }
    );
</script>
