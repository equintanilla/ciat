<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><?php echo $this->session->userdata('name')?></title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.core.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css" id="toggleCSS" />
		<style>
		.jarviswidget > div {
			font-size:1.3em;
		}
		.titulo, .titulo_s{
			margin:15px 0 15px 0;
			padding:0 0 5px 0;
			border-bottom:2px solid #ccc;
			text-align:center;
		}
		.titulo_s{
		border-bottom:0;
		}
		table{
			margin:5px 0;
			border:1px solid #bbb;
			font-size:1.1em !important;
		}
		table td, table th{
			border:2px solid #ccc;
		}
		table td{
			background-color:rgb(245,245,255);
		}
		table th{
			background-color:rgb(235,235,240);
		}
		th{
		}
		@media print { 
			.no-print { display:none; } 
			.titulo, .titulo_s{text-align:center;}
			#logo img{ display:none;}
			#logo::before{ content:'<?php echo $this->session->userdata('entidad')?>'; display:block; clear:both; text-align:center; font-weight:bold; font-size:1.5em;}
			#logo::after{ content:'<?php echo $this->session->userdata('sistema')?>'; text-align:center; display:block; clear:both;}
		}
		.border1{
			border:2px solid #bbb;
			background-color:rgb(245,245,255);
			padding-top:5px;
			padding-bottom:5px;
		}
		.bg-green{
			background-color:#b9e5a9;
		}
		.bg-warning{
			background-color:#ffe88c;
		}
		.bg-info{
			background-color:#9bc1ff;
		}
		.bg-gray{
			background-color:#ccc;
		}
		.bg-gray2{
			background-color:#ddd;
		}
		.bg-primary{
			background-color:#4786ad;
		}
		
		</style>
	</head>
	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->
<!-- HEADER -->
<header id="header">
	<div id="logo-group" class="col-md-2">
		<span id="logo"> <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="<?php echo $this->session->userdata('name')?>"> </span>
	</div>
	<div class="col-md-6 no-print" style="font-size:15px; margin-top:15px; margin-bottom:-10px;">
		<div class="text-center">
			<span>
				<div class="badge bg-color-blue" style="padding:7px;">
					<span style="font-size:1.6em;">Gesti&oacute;n de trabajo: <?php echo $this->session->userdata('desc_mes').'/'.$this->session->userdata('gestion');?></span>
				</div>
			</span>
			<div class="project-context hidden-xs">
				<span class="project-selector" style="font-size:0.7em;">
					<i class="fa fa-lg fa-fw fa-calendar txt-color-blue"></i>
					<a href="<?php echo base_url();?>index.php/cambiar_gestion">Cambiar Gestión</a>
				</span>
	<!--			<ul class="dropdown-menu">
					<li>
						<a href="<?php echo base_url();?>index.php/cambiar_gestion">Cambiar Gestión</a>
					</li>
				</ul>-->
			</div>
		</div>
	</div>
	<!-- pulled right: nav area -->
	<div class="pull-right col-md-2 no-print">
		<!-- logout button -->
		<div id="logout" class="btn-header transparent pull-right">
            <span> 
				<a href="<?php echo base_url(); ?>index.php/admin/logout" title="Salir" data-action="userLogout" data-logout-msg="Seguro de Salir?">
					Salir <i class="fa fa-sign-out"></i>
				</a> 
			</span>
		</div>
		<!-- end logout button -->

		<!-- fullscreen button -->
		<div id="fullscreen" class="btn-header transparent pull-right">
            <span> 
				<a href="javascript:void(0);" data-action="launchFullscreen" title="Pantalla Completa">
					<i class="fa fa-arrows-alt"></i>
				</a> 
			</span>
		</div>
		<!-- end fullscreen button -->
		<!-- collapse menu button -->
		<div id="hide-menu" class="btn-header pull-right">
            <span> 
				<a href="javascript:void(0);" data-action="toggleMenu" title="Menu">
					<i class="fa fa-reorder"></i>
				</a>
			</span>
		</div>
		<!-- end collapse menu -->

	</div>
	<!-- end pulled right: nav area -->

</header>
<!-- END HEADER -->
		<!-- Left panel : Navigation area --> 
		<aside id="left-panel" class="no-print">
			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                              <span>
                                  <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                              </span>
						<i class="fa fa-angle-down"></i>
					</a> 
					
				</span>
			</div>

			<nav class="no-print">
				<ul>
					<li class="">
	                <a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MENÚ PRINCIPAL"><i
	                        class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
	            	</li>
		            <li class="text-center">
		                <a href="<?php echo base_url().'index.php/admin/proy/list_proy' ?>" title="MIS PROYECTOS"><span class="menu-item-parent">PROGRAMACI&Oacute;N DEL POA</span></a>
		            </li>
					<?php
		                for($i=0;$i<count($enlaces);$i++)
		                {
		                    if(count($subenlaces[$enlaces[$i]['o_child']])>0)
		                    {
		            ?>
		            <li>
		              	<a href="#" >
		              		<i class="<?php echo $enlaces[$i]['o_image']?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
		              	<ul >
		              	<?php
		                foreach ($subenlaces[$enlaces[$i]['o_child']] as $item) {
		                ?>
		                <li><a href="<?php echo base_url($item['o_url']); ?>"><?php echo $item['o_titulo']; ?></a></li>
		                <?php } ?>
		                </ul>
		            </li>
		            	<?php 
		                    }
		            } ?>
				</ul>
			</nav>
			<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>

		</aside>

		<!-- MAIN PANEL -->
		<div id="main" role="main">
			<!-- RIBBON -->
			<div id="ribbon" class="no-print">
				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>
				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li><a href="<?php echo site_url('taqpacha')?>" title="Otros reportes">Reportes SGP</a></li>
					<li>REPORTE SGPP - Programación y ejecución</li>
				</ol>
			</div>
			<!-- END RIBBON -->

			<!-- MAIN CONTENT -->
			<div id="content">

				<!-- widget grid -->
<!--				<section id="widget-grid" class=""> // Incluir el ID widget-grid elimina elementos DOM dentro de header-->
				<section id="" class="">
					<!-- row -->
					<div class="row">
							<!-- Widget ID (each widget will need unique ID)-->
							<div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" data-widget-fullscreenbutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false">
								<!-- widget div-->
									<div class="widget-body">
													<a href="<?= site_url('taqpacha')?>" class="btn btn-primary btn-xs pull-right lead no-print">
														<i class="glyphicon glyphicon-backward"></i> Volver atrás
													</a>
													<button class="btn btn-lg btn-primary imprimir_sgp"><i class="glyphicon glyphicon-print"></i> Vista preliminar de impresión</button>
													<br>
										<div id="area_reporte">
<!--												<div class="text-center">
												<img src="<?= base_url('assets/img_v1.1/Escudo.min.png')?>">
												</div>-->
												<h2 class="titulo_s">
													<span class="text-primary"><?php echo $this->session->userdata('sistema')?> </span>
													<br>
													REPORTE SGPP
												</h2>
											
											<h2 class="titulo"><b>2. PROGRAMACION Y EJECUCIÓN DEL PROYECTO - PROGRAMA</b></h2>
											<div class="row">
												<div class="col-md-2 text-primary text-right"><b>Nombre de la operación:</b></div>
												<div class="col-md-5 border1"><?php echo $dictamen['proy_nombre'];?></div>
												<div class="col-md-2 text-primary text-right"><b>Código SISIN:</b></div>
												<div class="col-md-3 border1" style="font-size:1.4em; font-weight:bold;"><?php if($dictamen['proy_sisin'] == '') echo '-'; else echo $dictamen['proy_sisin'];?></div>
											</div>
											<br>
											<div class="row">
												<div class="col-md-2 text-primary text-right"><b>Funcionario responsable:</b></div>
												<div class="col-md-5 border1"><?php echo $unidad_ejecutora[0]['fun_nombre'].' '.$unidad_ejecutora[0]['fun_paterno'].' '.$unidad_ejecutora[0]['fun_materno']?></div>
												<div class="col-md-2 text-primary text-right"><b>Cargo:</b></div>
												<div class="col-md-3 border1"><?= $unidad_ejecutora[0]['fun_cargo']?></div>
											</div>
											
											<h2 class="titulo"><b>PROGRAMACION Y EJECUCIÓN FÍSICA</b></h2>
											<div class="table-responsive">
											<?php echo $pe_fisica;?>
											</div>
											
											<br>
											
											<h2 class="titulo"><b>PROGRAMACION Y EJECUCIÓN FINANCIERA</b></h2>
											<div class="table-responsive">
											<?php echo $pe_financiera;?>
											</div>

											<h4 class="titulo"><b>RESPONSABLE DE LA INFORMACIÓN</b></h4>
											<div class="row">
												<div class="col-md-2 text-primary text-right"><b>Nombre:</b></div>
												<div class="col-md-4 border1"><?php echo $unidad_ejecutora[0]['fun_nombre'].' '.$unidad_ejecutora[0]['fun_paterno'].' '.$unidad_ejecutora[0]['fun_materno']?></div>
												<div class="col-md-2 text-primary text-right"><b>Cargo:</b></div>
												<div class="col-md-4 border1"><?= $unidad_ejecutora[0]['fun_cargo']?></div>
											</div>
											<br>
											<div class="row">
												<div class="col-md-2 text-primary text-right"><b>Institución:</b></div>
												<div class="col-md-4 border1"><?= $unidad_ejecutora[0]['uejec']?></div>
												<div class="col-md-2 text-primary text-right"><b>Telefono(s):</b></div>
												<div class="col-md-4 border1"><?= $unidad_ejecutora[0]['fun_telefono']?></div>
											</div>
											<br><br><br><br><br><br><br><br><br>
											<div class="row">
												<div class="col-md-2 col-md-offset-5 text-center" style="border-top:4px solid #777;"><b>Firma y sello responsable</b></div>
											</div>
											
										</div>
										<button class="btn btn-lg btn-primary imprimir_sgp"><i class="glyphicon glyphicon-print"></i> Vista preliminar de impresión</button>

									</div>
									<!-- end widget content -->
								<!-- end widget div -->
							</div>
						<!-- end widget -->
					</div>
					<!-- end row -->
				</section>
				<!-- end widget grid -->					
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN PANEL -->
		<!-- PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->
		<!--================================================== -->
		<script src="<?php echo base_url();?>/assets/js/libs/jquery-2.0.2.min.js"></script>
		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>
		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url(); ?>assets/js/session_time/jquery-idletimer.js"></script>
		<script src="<?php echo base_url();?>/assets/js/app.config.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/mis_js/validacion_form.js"></script>
		<SCRIPT src="<?php echo base_url(); ?>mis_js/programacion/ejecucion/abm_ejecucion.js" type="text/javascript"></SCRIPT>
		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url();?>/assets/js/bootstrap/bootstrap.min.js"></script>
		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url();?>/assets/js/notification/SmartNotification.min.js"></script>
		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url();?>/assets/js/smartwidgets/jarvis.widget.min.js"></script>
		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url();?>/assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<!-- SPARKLINES -->
		<script src="<?php echo base_url();?>/assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/select2/select2.min.js"></script>
		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
		<!-- browser msie issue fix -->
		<script src="<?php echo base_url();?>/assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url();?>/assets/js/plugin/fastclick/fastclick.min.js"></script>
		<!-- Demo purpose only -->
		<script src="<?php echo base_url();?>/assets/js/demo.min.js"></script>
		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url();?>/assets/js/app.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<!-- Voice command : plugin -->
		<script src="<?php echo base_url();?>/assets/js/speech/voicecommand.min.js"></script>
		<script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
		<script src="<?php echo base_url();?>/assets/js/plugin/fuelux/wizard/wizard.min.js"></script>

		<!-- JS PARA GRAFICAR -->
		<script src="<?php echo base_url('assets/highcharts/js/highcharts.js');?>"></script>
		<script src="<?php echo base_url('assets/highcharts/js/highcharts-3d.js');?>"></script>
		<script src="<?php echo base_url('assets/highcharts/js/modules/exporting.js');?>"></script> 

		<!-- Para conservar el aspecto en la impresión -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>
		
		<script type="text/javascript">
		function clean_number(nro){
			var lon = nro.length;
			var sub = nro;
			if(sub.substr(lon-1,lon) == '%'){
				sub = sub.substr(0, lon-1);
			}
			console.log(sub);
			return sub;
		}
		$(document).ready(function() {
			pageSetUp();

			$('.tabla_avance td').each(function(){
				if(clean_number($(this).html()) > 0){
					$(this).addClass('bg-primary');
				}
			});

			$('.tabla_partidas td').each(function(){
				if(parseInt(clean_number($(this).html())) > 0){
					$(this).addClass('bg-info');
				}
			});
			
			
			$('.fpa').each(function(){
				if(parseInt(clean_number($(this).html())) > 0){
					$(this).removeClass('bg-gray2');
					$(this).addClass('bg-primary');
				}
			});

			$('.fea').each(function(){
				if(parseInt(clean_number($(this).html())) > 0){
					$(this).removeClass('bg-gray2');
					$(this).addClass('bg-info');
				}
			});
			
			$('.fep').each(function(){
				if(parseInt(clean_number($(this).html())) > 0){
					$(this).removeClass('bg-gray2');
					$(this).addClass('bg-gray2');
				}
			});

			
			var fpac = 0;
//			var c = 0;
			$('.pac').each(function(){
				var str = $(this).html();
				if(parseInt(str.substring(0,str.length-1)) > 0 && fpac == 0){
/*					if(c == 0){
						$(this).prev().addClass('bg-gray');
						$(this).prev().html('0%');
						c = c+1;
					}*/
					$(this).addClass('bg-gray');
					if(parseInt(str.substring(0,str.length-1)) == 100){
						fpac = 1;
					}
				}else{
					$(this).html('-');
				}

			});

//			var ce = 0;
			var epac = 0;
			$('.eac').each(function(){
				var str = $(this).html();
				if(parseInt(str.substring(0,str.length-1)) > 0 && epac == 0){
/*					if(ce == 0){
						$(this).prev().addClass('bg-gray');
						$(this).prev().html('0%');
						ce = c+1;
					}*/
					$(this).addClass('bg-gray');
					if(parseInt(str.substring(0,str.length-1)) == 100){
						epac = 1;
					}
				}else{
					$(this).html('-');
				}
			});

//			var cefi = 0;
			var efi = 0;
			$('.tabla_eficacia td.efic').each(function(){
				var str = $(this).html();
				if(parseInt(str.substring(0,str.length-1)) > 0 && efi == 0){
					if(parseInt(str.substring(0,str.length-1)) > 0){
						$(this).addClass('bg-danger');
					}
					if(parseInt(str.substring(0,str.length-1)) > 35){
						$(this).removeClass('bg-danger');
						$(this).addClass('bg-warning');
					}
					if(parseInt(str.substring(0,str.length-1)) > 65){
						$(this).removeClass('bg-warning');
						$(this).addClass('bg-info');
					}
					if(parseInt(str.substring(0,str.length-1)) > 95){
						$(this).removeClass('bg-info');
						$(this).addClass('bg-green');
					}
/*					if(parseInt(str.substring(0,str.length-1)) == 100){
						efi = 1;
					}

/*					if(cefi == 0){
						$(this).prev().addClass('bg-danger');
						$(this).prev().html('0%');
						cefi = c+1;
					}*/
					
					
				}else{
					$(this).html('-');
				}
			});
			
			

			$('.imprimir_sgp').on('click', function(e){
				$ant = $(this).html();
				$('.imprimir_sgp').html('<i class="glyphicon glyphicon-cog"></i> Generando reporte...espere');
				$('.imprimir_sgp').addClass('btn-success');
				e.stopImmediatePropagation()
				e.preventDefault();
				html2canvas($("#area_reporte"), {
					onrendered: function(canvas) {
						$('.imprimir_sgp').html($ant);
						$('.imprimir_sgp').removeClass('btn-success');
						var ctx = canvas.getContext('2d');
						ctx.webkitImageSmoothingEnabled = false;
						ctx.mozImageSmoothingEnabled = false;
						ctx.imageSmoothingEnabled = false;
						var myImage = canvas.toDataURL("image/png");
						var tWindow = window.open("", "_blank","location=no, toolbar=no,scrollbars=yes,resizable=yes,top=50,left=50,width=1000,height=600");
						var x = $(tWindow.document.body).append('<html><head><title><?php echo $this->session->userdata('name')?> </title><style>button{ display:block; padding:5px; font-size:0.9em; margin:5px auto;}@media print {.no-print { display:none; }</style></head><body><button class="no-print" onclick="window.print();">IMPRIMIR DOCUMENTO</button><br><img id="Image" src=' + myImage + ' style="width:100%;"></body></html>');
						x.ready(function() {
/*								tWindow.focus();
								tWindow.print();*/
							});
					}
				});		
			});			
		})
		</script>
	</body>
</html>
