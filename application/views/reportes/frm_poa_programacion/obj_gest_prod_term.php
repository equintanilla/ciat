<!-- MAIN PANEL -->
<script xmlns="http://www.w3.org/1999/html">
    function abreVentana(PDF) {
        var direccion;
        direccion = '' + PDF;
        window.open(direccion, "Reporte de Proyectos", "width=800,height=650,scrollbars=SI");
    }
</script>
<div id="main" role="main">
    <div id="">
        <ol class="breadcrumb">
            <li>Frm. POA Programación</li><li> Objetivos de Gestión y Producto Terminal</li>
        </ol>
    </div>
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-12 animated fadeInDown">
                <h1 class="page-title txt-color-blueDark"><i class="fa fa-pencil-square-o fa-fw "></i> OBJETIVOS DE GESTIÓN Y PRODUCTO TERMINAL</h1>
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- row -->
            <div class="row">
                <!-- NEW WIDGET START -->
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-color-teal" id="wid-id-0" data-widget-editbutton="false">
                        <header>
                            <span class="widget-icon"> <span class="fa fa-table"></span> </span>
                            <h2>LISTA DE CARPETA POA</h2>
                        </header>

                        <div>
                            <div class="widget-body no-padding">
                                <div class="table-responsive">
                                    <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                        <thead>
                                        <tr>
                                            <th> AO </th>
                                            <th>PROGRAMA</th>
                                            <th>UNIDAD ORGANIZACIONAL</th>
                                            <th>FECHA DE CREACIÓN</th>
                                        </tr>
                                        </thead>
                                        <tbody id="bdi">
                                        <?php
                                        foreach($lista_poa as $row)
                                        {
                                            echo '<tr id="tr'.$row['poa_id'].'">';
                                            echo'<td >';
                                            ?>
                                                <center>
                                                    <a href="javascript:abreVentana('<?php echo site_url("admin").'/reporte_red_obj/'.$row['poa_id'].'/'.$n; ?>');" title="REPORTE">
                                                        <img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/>
                                                    </a>
                                                </center>
                                            <?php
                                            '</td>';
                                            echo '<td><font size="1">'.$row['aper_programa'].$row['aper_proyecto'].$row['aper_actividad']." - ".$row['aper_descripcion'].'</font></td>';
                                            echo '<td><font size="1">'.$row['uni_unidad'].'</font></td>';
                                            echo '<td><font size="1">'.$row['poa_fecha_creacion'].'</font></td>';
                                            echo '</tr>';
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- end widget content -->
                        </div>
                        <!-- end widget div -->
                    </div>
                    <!-- end widget -->
                </article>
                <!-- WIDGET END -->
            </div>
        </section>
        <!-- end widget grid -->

    </div>
</div>
<!-- END MAIN PANEL -->




