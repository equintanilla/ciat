<div id="main" role="main">
    <div>
        <ol class="breadcrumb">
            <?php 
                if($n == 1){ ?>
                    <li>Frm. POA Programación</li><li> Objetivos Estrategicos</li>
                <?php } else {
                    if($n == 2){ ?>
                        <li>Frm. POA Ejecución</li><li> Objetivos Estrategicos</li>
                    <?php }
                    }
            ?>
        </ol>
    </div>
    <div class="row">
        <div class="col-sm-2 col-md-2 col-lg-2"></div>
        <div class="col-sm-8 col-md-8 col-lg-8 col-xs-12">
            <div class="row">
                <div class="col-md-12 animated fadeInDown" align="center">
                    <div class="alert alert-block alert-success" style="background-color: #568A89; color:white; height:50px; margin: 0 auto;">
                        <h4>
                            <i class="fa fa-file-pdf-o" style="font-size:30px;">&nbsp;</i>
                            Reporte <?php echo $titulo;?>
                        </h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div id="content">				
                <section id="widget-grid" class="">
                    <div class="row">
                        <?php
                            if($n == 1) { ?>
                                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <iframe 
                                        id = "ipdf"
                                        style = "border:1px solid #666CCC" title = "Reporte Objetivos Estrategicos Programación"
                                        width = "100%"
                                        height = "635px;"
                                        src = "<?php echo base_url().'index.php/admin/reportes/objes' ?>"
                                        allowtransparency="true"
                                        frameborder = "0">
                                        Actualizar Versión Navegador
                                    </iframe>
                                </article>
                            <?php } else {
                                if($n == 2) { ?>
                                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <iframe
                                            id = "ipdf"
                                            style = "border:1px solid #666CCC" title = "Reporte Objetivos Estrategicos Ejecución"
                                            width = "100%"
                                            height = "635px;"
                                            src = "<?php echo base_url().'index.php/admin/rep/pdf/obj_ac' ?>"
                                            allowtransparency="true"
                                            frameborder = "0">
                                            Actualizar Versión Navegador
                                        </iframe>
                                    </article>
                                <?php }
                            }
                        ?>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
