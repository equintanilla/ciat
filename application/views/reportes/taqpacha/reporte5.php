<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><?php echo $this->session->userdata('name')?></title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.core.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css" id="toggleCSS" />
		<style>
		.titulo{
			margin:15px 0 15px 0;
			padding:0 0 5px 0;
			border-bottom:1px solid #ddd;
		}
		table{
			font-size:1.2em !important;
		}
		th{
			text-align:center;
		}
		@media print { 
			.no-print { display:none; } 
			#logo img{ width:400px; display:inline-block; margin:30px auto; float:right;}
			#logo::before{ content:url('<?php echo base_url('assets/img_v1.1/logo_gadlp_small.png');?>') !important; display:inline-block; float:left;}
			#logo::after{ content:''; display:block; clear:both;}
		}
		</style>
	</head>
	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->
<!-- HEADER -->
<header id="header">
	<div id="logo-group" class="col-md-2">
		<span id="logo"> <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="<?php echo $this->session->userdata('name')?>"> </span>
	</div>
	<div class="col-md-6 no-print" style="font-size:15px; margin-top:15px; margin-bottom:-10px;">
		<div class="text-center">
			<span>
				<div class="badge bg-color-blue" style="padding:7px;">
					<span style="font-size:1.6em;">Gesti&oacute;n de trabajo: <?php echo $this->session->userdata('desc_mes').'/'.$this->session->userdata('gestion');?></span>
				</div>
			</span>
			<div class="project-context hidden-xs">
				<span class="project-selector" style="font-size:0.7em;">
					<i class="fa fa-lg fa-fw fa-calendar txt-color-blue"></i>
					<a href="<?php echo base_url();?>index.php/cambiar_gestion">Cambiar Gestión</a>
				</span>
	<!--			<ul class="dropdown-menu">
					<li>
						<a href="<?php echo base_url();?>index.php/cambiar_gestion">Cambiar Gestión</a>
					</li>
				</ul>-->
			</div>
		</div>
	</div>
	<!-- pulled right: nav area -->
	<div class="pull-right col-md-2 no-print">
		<!-- logout button -->
		<div id="logout" class="btn-header transparent pull-right">
            <span> 
				<a href="<?php echo base_url(); ?>index.php/admin/logout" title="Salir" data-action="userLogout" data-logout-msg="Seguro de Salir?">
					Salir <i class="fa fa-sign-out"></i>
				</a> 
			</span>
		</div>
		<!-- end logout button -->

		<!-- fullscreen button -->
		<div id="fullscreen" class="btn-header transparent pull-right">
            <span> 
				<a href="javascript:void(0);" data-action="launchFullscreen" title="Pantalla Completa">
					<i class="fa fa-arrows-alt"></i>
				</a> 
			</span>
		</div>
		<!-- end fullscreen button -->
		<!-- collapse menu button -->
		<div id="hide-menu" class="btn-header pull-right">
            <span> 
				<a href="javascript:void(0);" data-action="toggleMenu" title="Menu">
					<i class="fa fa-reorder"></i>
				</a>
			</span>
		</div>
		<!-- end collapse menu -->

	</div>
	<!-- end pulled right: nav area -->

</header>
<!-- END HEADER -->

		<!-- Left panel : Navigation area --> 
		<aside id="left-panel" class="no-print">
			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                              <span>
                                  <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                              </span>
						<i class="fa fa-angle-down"></i>
					</a> 
					
				</span>
			</div>

			<nav class="no-print">
				<ul>
					<li class="">
	                <a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MENÚ PRINCIPAL"><i
	                        class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
	            	</li>
		            <li class="text-center">
		                <a href="<?php echo base_url().'index.php/admin/proy/list_proy' ?>" title="MIS PROYECTOS"><span class="menu-item-parent">PROGRAMACI&Oacute;N DEL POA</span></a>
		            </li>
					<?php
		                for($i=0;$i<count($enlaces);$i++)
		                {
		                    if(count($subenlaces[$enlaces[$i]['o_child']])>0)
		                    {
		            ?>
		            <li>
		              	<a href="#" >
		              		<i class="<?php echo $enlaces[$i]['o_image']?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
		              	<ul >
		              	<?php
		                foreach ($subenlaces[$enlaces[$i]['o_child']] as $item) {
		                ?>
		                <li><a href="<?php echo base_url($item['o_url']); ?>"><?php echo $item['o_titulo']; ?></a></li>
		                <?php } ?>
		                </ul>
		            </li>
		            	<?php 
		                    }
		            } ?>
				</ul>
			</nav>
			<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>

		</aside>

		<!-- MAIN PANEL -->
		<div id="main" role="main">
			<!-- RIBBON -->
			<div id="ribbon" class="no-print">
				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>
				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li><a href="<?php echo site_url('taqpacha')?>" title="Otros reportes">Reportes Taqpacha</a></li>
					<li>Inversión Pública gestión <?= $gestion?> por sector económico</li>
				</ol>
			</div>
			<!-- END RIBBON -->

			<!-- MAIN CONTENT -->
			<div id="content">

				<!-- widget grid -->
<!--				<section id="widget-grid" class=""> // Incluir el ID widget-grid elimina elementos DOM dentro de header-->
				<section id="" class="">
					<!-- row -->
					<div class="row">
							<!-- Widget ID (each widget will need unique ID)-->
							<div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" data-widget-fullscreenbutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false">
								<!-- widget div-->
									<div class="widget-body">
										<div class="well well-sm" style="margin-bottom:10px;">	
											<h2 style="margin:8px auto;">
												<i class="glyphicon glyphicon-stats"></i> INVERSIÓN PÚBLICA GESTION <?= $gestion?> POR SECTOR ECONÓMICO
												<a href="<?= site_url('taqpacha')?>" class="btn btn-primary btn-xs pull-right lead no-print">
													<i class="glyphicon glyphicon-backward"></i> Volver atrás
												</a>
											</h2>
										</div>
										
										<h4><b>Fecha y hora de impresión: <span class="text-success"><?php echo date("d-m-Y, h:i:sa")?></span></b></h4>
										<h2 class="titulo">TABLA DE RESULTADOS</h2>
										<div class="table-responsive">
											<table border="1" cellspacing="0" class="table table-bordered">
												<tr>
													<th>SECTOR</th>
													<th>CANTIDAD TOTAL</th>
													<!--<th>PPTO. VIGENTE (en Bs)</th>
													<th>PPTO. EJECUTADO (en Bs)</th>-->
												</tr>
												<?php 
												$cant = 0;
												$pptoi = 0;
                                                $pptoejec=0;
												$c = 0;
												function toMoney($val,$symbol='',$r=2)
												{
													$n = $val; 
													$c = is_float($n) ? 1 : number_format($n,$r);
													$d = '.';
													$t = ',';
													$sign = ($n < 0) ? '-' : '';
													$i = $n=number_format(abs($n),$r); 
													$j = (($j = strlen((string)$i)) > 3) ? $j % 3 : 0; 
												   return  $symbol.$sign .($j ? substr($i,0, $j) + $t : '').preg_replace('/(\d{3})(?=\d)/',"$1" + $t,substr($i,$j)) ;
												}												
												
												foreach($reporte1 as $item):?>
													<tr>
														<td><?= $item['_sector']?></td>
														<td class="text-center"><?= $item['cantidad']?></td>
														<!--<td class="text-right"><?php echo toMoney($item['presup']);?></td>
														<td class="text-right"><?= toMoney($item['ejecutado'])?></td>-->
														<?php
														$c++;
														$cant = $cant + $item['cantidad'];
														$pptoi = $pptoi + $item['presup'];
                                                        $pptoejec= $pptoejec + $item['ejecutado'];
														?>
													</tr>
												<?php endforeach;?>
												
												<tr class="bg-primary">
													<td class="text-right"><b>Total General</b></td>
													<td class="text-center"><b><?= $cant;?></b></td>
													<!--<td class="text-right"><b><?= toMoney($pptoi);?></b></td>
													<td class="text-right"><b><?= toMoney($pptoejec);?></b></td>-->
												</tr>
											</table>
										</div>
										<div class="row">
										<?php if($c > 1):?>
											<div class="col-md-12" id="grafica1" style="height: 840px; margin: 0 auto"></div>
										<?php else:?>
											<div class="col-md-12" id="grafica1" style="height: 300px; margin: 0 auto"></div>
										<?php endif;?>
										</div>
										<h2 class="titulo"></h2>
<!--										<div class="row">
											<div class="col-md-12" id="grafica2" style="height: 750px; margin: 0 auto">
											</div>
											-->
										</div>
									</div>
									<!-- end widget content -->
								<!-- end widget div -->
							</div>
						<!-- end widget -->
					</div>
					<!-- end row -->
				</section>
				<!-- end widget grid -->					
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN PANEL -->
		<!-- PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->
		<!--================================================== -->
		<script src="<?php echo base_url();?>/assets/js/libs/jquery-2.0.2.min.js"></script>
		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>
		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url(); ?>assets/js/session_time/jquery-idletimer.js"></script>
		<script src="<?php echo base_url();?>/assets/js/app.config.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/mis_js/validacion_form.js"></script>
		<SCRIPT src="<?php echo base_url(); ?>mis_js/programacion/ejecucion/abm_ejecucion.js" type="text/javascript"></SCRIPT>
		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url();?>/assets/js/bootstrap/bootstrap.min.js"></script>
		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url();?>/assets/js/notification/SmartNotification.min.js"></script>
		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url();?>/assets/js/smartwidgets/jarvis.widget.min.js"></script>
		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url();?>/assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<!-- SPARKLINES -->
		<script src="<?php echo base_url();?>/assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/select2/select2.min.js"></script>
		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
		<!-- browser msie issue fix -->
		<script src="<?php echo base_url();?>/assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url();?>/assets/js/plugin/fastclick/fastclick.min.js"></script>
		<!-- Demo purpose only -->
		<script src="<?php echo base_url();?>/assets/js/demo.min.js"></script>
		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url();?>/assets/js/app.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<!-- Voice command : plugin -->
		<script src="<?php echo base_url();?>/assets/js/speech/voicecommand.min.js"></script>
		<script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
		<script src="<?php echo base_url();?>/assets/js/plugin/fuelux/wizard/wizard.min.js"></script>

		<!-- JS PARA GRAFICAR -->
		<script src="<?php echo base_url('assets/highcharts/js/highcharts.js');?>"></script>
		<script src="<?php echo base_url('assets/highcharts/js/highcharts-3d.js');?>"></script>
		<script src="<?php echo base_url('assets/highcharts/js/modules/exporting.js');?>"></script> 

		<script type="text/javascript">
		$(document).ready(function() {
			pageSetUp();
		})
		</script>
		<script>
		

		$(function () {
/*		    $(".selector").on("change", function (e) {
				alert("Modificar");
		    });*/

			//GRAFICA 1 BARRAS			
			Highcharts.chart('grafica1', {
				chart: {
					type: 'bar'
				},
				title: {
					text: 'PRESUPUESTO VIGENTE GESTION <?= $gestion;?> POR SECTOR ECONOMICO'
				},
				subtitle: {
					text: 'En millones de Bolivianos'
				},
				xAxis: {
					categories: [
					<?php 
						$lista = '';
						foreach($reporte1 as $item){
							$lista = $lista."'".$item['_sector']."',";
						}
						echo $lista;
					?>
					],
					title: {
						text: null
					}
				},
				yAxis: {
					min: 0,
					title: {
						text: '',
						align: 'high'
					},
					labels: {
						overflow: 'visible'
					}
				},
				tooltip: {
					valueSuffix: ''
				},
				plotOptions: {
					series: {
						stacking: 'normal'
					},
					bar: {
						dataLabels: {
							enabled: true
						}
					}

				},
				legend: {
					layout: 'vertical',
					align: 'right',
					verticalAlign: 'top',
					x: -40,
					y: 80,
					floating: true,
					borderWidth: 1,
					backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
					shadow: true
				},
				credits: {
					enabled: false
				},
				series: [
				{
					name: 'Presupuesto vigente (En millones de Bs)',
					data: [
					<?php 
						$lista = '';
						foreach($reporte1 as $item){
							if($item['presup']=='')
								$lista = $lista."'"."0"."',";
							else
								$lista = $lista."".round($item['presup']/1000000,2).",";
						}
						echo $lista;
					?>
					]
				},{
					name: 'Cantidad',
					data: [
					<?php 
						$lista = '';
						foreach($reporte1 as $item){
							if($item['cantidad']=='')
								$lista = $lista."'"."0"."',";
							else
								$lista = $lista."".$item['cantidad'].",";
						}
						echo $lista;
					?>
					]
				},
				]
			});			
			

		});
		</script>
	</body>
</html>
