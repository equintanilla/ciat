<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><?php echo $this->session->userdata('name')?></title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.core.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css" id="toggleCSS" />
		<style>
		.widget-body div:hover{background-color:#eee;}
		.selector{height:45px; padding:12px 7px !important; font-size:15px;}
		</style>
	</head>
	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->
<!-- HEADER -->
<header id="header">
	<div id="logo-group" class="col-md-2">
		<span id="logo"> <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="<?php echo $this->session->userdata('name')?>"> </span>
	</div>
	<div class="col-md-6" style="font-size:15px; margin-top:15px; margin-bottom:-10px;">
		<div class="text-center">
			<span>
				<div class="badge bg-color-blue" style="padding:7px;">
					<span style="font-size:1.6em;">Gesti&oacute;n de trabajo: <?php echo $this->session->userdata('desc_mes').'/'.$this->session->userdata('gestion');?></span>
				</div>
			</span>
			<div class="project-context hidden-xs">
				<span class="project-selector" style="font-size:0.7em;">
					<i class="fa fa-lg fa-fw fa-calendar txt-color-blue"></i>
					<a href="<?php echo base_url();?>index.php/cambiar_gestion">Cambiar Gestión</a>
				</span>
	<!--			<ul class="dropdown-menu">
					<li>
						<a href="<?php echo base_url();?>index.php/cambiar_gestion">Cambiar Gestión</a>
					</li>
				</ul>-->
			</div>
		</div>
	</div>
	<!-- pulled right: nav area -->
	<div class="pull-right col-md-2">
		<!-- logout button -->
		<div id="logout" class="btn-header transparent pull-right">
            <span> 
				<a href="<?php echo base_url(); ?>index.php/admin/logout" title="Salir" data-action="userLogout" data-logout-msg="Seguro de Salir?">
					Salir <i class="fa fa-sign-out"></i>
				</a> 
			</span>
		</div>
		<!-- end logout button -->

		<!-- fullscreen button -->
		<div id="fullscreen" class="btn-header transparent pull-right">
            <span> 
				<a href="javascript:void(0);" data-action="launchFullscreen" title="Pantalla Completa">
					<i class="fa fa-arrows-alt"></i>
				</a> 
			</span>
		</div>
		<!-- end fullscreen button -->
		<!-- collapse menu button -->
		<div id="hide-menu" class="btn-header pull-right">
            <span> 
				<a href="javascript:void(0);" data-action="toggleMenu" title="Menu">
					<i class="fa fa-reorder"></i>
				</a>
			</span>
		</div>
		<!-- end collapse menu -->

	</div>
	<!-- end pulled right: nav area -->

</header>
<!-- END HEADER -->

		<!-- Left panel : Navigation area --> 
		<aside id="left-panel">
			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                              <span>
                                  <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                              </span>
						<i class="fa fa-angle-down"></i>
					</a> 
					
				</span>
			</div>

			<nav>
				<ul>
					<li class="">
	                <a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MENÚ PRINCIPAL"><i
	                        class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
	            	</li>
		            <li class="text-center">
		                <a href="<?php echo base_url().'index.php/admin/proy/list_proy' ?>" title="MIS PROYECTOS"><span class="menu-item-parent">PROGRAMACI&Oacute;N DEL POA</span></a>
		            </li>
					<?php
		                for($i=0;$i<count($enlaces);$i++)
		                {
		                    if(count($subenlaces[$enlaces[$i]['o_child']])>0)
		                    {
		            ?>
		            <li>
		              	<a href="#" >
		              		<i class="<?php echo $enlaces[$i]['o_image']?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
		              	<ul >
		              	<?php
		                foreach ($subenlaces[$enlaces[$i]['o_child']] as $item) {
		                ?>
		                <li><a href="<?php echo base_url($item['o_url']); ?>"><?php echo $item['o_titulo']; ?></a></li>
		                <?php } ?>
		                </ul>
		            </li>
		            	<?php 
		                    }
		            } ?>
				</ul>
			</nav>
			<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>

		</aside>

		<!-- MAIN PANEL -->
		<div id="main" role="main">
			<!-- RIBBON -->
			<div id="ribbon">
				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>
				<!-- breadcrumb -->
				<ol class="breadcrumb">
<!--					<li><a href="<?php echo base_url().'index.php/admin/proy/list_proy' ?>" title="Otros reportes">Otros reportes</a></li>-->
					<li>Otros Reportes</li>
					<li>Reportes Taqpacha</li>
				</ol>
			</div>
			<!-- END RIBBON -->

			<!-- MAIN CONTENT -->
			<div id="content">

				<!-- widget grid -->
<!--				<section id="widget-grid" class=""> // Incluir el ID widget-grid elimina elementos DOM dentro de header-->
				<section id="" class="">
					<!-- row -->
					<div class="row">
							<!-- Widget ID (each widget will need unique ID)-->
							<div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" data-widget-fullscreenbutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false">
								<header>
									<span class="widget-icon"> <i class="fa fa-check"></i> </span>
									<h2 class="font-md"><strong>LISTA DE REPORTES TAQPACHA</strong></h2>
								</header>
								<!-- widget div-->
									<div class="widget-body">
										<div class="row"><div class="col-md-12 well well-sm">
													<div class="row">
														<div class="col-md-8"><h2 style="margin:10px 0;">1. Presupuesto Inversión Pública gestión <?= $gestion;?></h2></div>
														<div class="col-md-2"></div>
														<div class="col-md-2"><a href="<?= site_url('taqpacha/pip');?>" class="btn btn-lg btn-block btn-primary lead">
																	<i class="glyphicon glyphicon-stats"></i> VER REPORTE
														</a></div>
													</div>
										</div></div>
										<div class="row"><div class="col-md-12 well well-sm">
													<div class="row">
														<div class="col-md-8"><h2 style="margin:10px 0;">2. Presupuesto Programas No Recurrentes gestión <?= $gestion;?></h2></div>
														<div class="col-md-2"></div>
														<div class="col-md-2"><a href="<?= site_url('taqpacha/ppnr');?>" class="btn btn-lg btn-block btn-primary lead">
																	<i class="glyphicon glyphicon-stats"></i> VER REPORTE
														</a></div>
													</div>
										</div></div>
										<div class="row"><div class="col-md-12 well well-sm">
													<div class="row">
														<div class="col-md-8"><h2 style="margin:10px 0;">3. Estado de Proyectos de Inversión Pública gestión <?= $gestion;?></h2></div>
														<div class="col-md-2"></div>
														<div class="col-md-2"><a href="<?= site_url('taqpacha/epip');?>" class="btn btn-lg btn-block btn-primary lead">
																	<i class="glyphicon glyphicon-stats"></i> VER REPORTE
														</a></div>
													</div>
										</div></div>
										<div class="row"><div class="col-md-12 well well-sm">
													<div class="row">
														<div class="col-md-8"><h2 style="margin:10px 0;">4. Estado de Programas No Recurrentes gestión <?= $gestion;?></h2></div>
														<div class="col-md-2"></div>
														<div class="col-md-2"><a href="<?= site_url('taqpacha/eppnr');?>" class="btn btn-lg btn-block btn-primary lead">
																	<i class="glyphicon glyphicon-stats"></i> VER REPORTE
														</a></div>
													</div>
										</div></div>
										<div class="row"><div class="col-md-12 well well-sm">
													<div class="row">
														<div class="col-md-8"><h2 style="margin:10px 0;">5. Inversión Pública gestión <?= $gestion;?> por sector económico</h2></div>
														<div class="col-md-2 padre_selector">
															<select id="r5" class="form-control selector">
															  <option value="-1">&laquo; TODOS &raquo;</option>
															  <?php 
																foreach($sectores as $item){
																	echo '<option value="'.$item['codsectorial'].'">'.$item['sector'].'</option>';
																}
															  ?>
															</select>
														</div>
														<div class="col-md-2 vecino"><a href="<?= site_url('taqpacha/ipse');?>" class="btn btn-lg btn-block btn-primary lead">
																	<i class="glyphicon glyphicon-stats"></i> VER REPORTE
														</a></div>
													</div>
										</div></div>
										<div class="row"><div class="col-md-12 well well-sm">
													<div class="row">
														<div class="col-md-8"><h2 style="margin:10px 0;">6. Programas No Recurrentes gestión <?= $gestion;?>  por sector económico</h2></div>
														<div class="col-md-2 padre_selector">
															<select id="r6" class="form-control selector">
															  <option value="-1">&laquo; TODOS &raquo;</option>
															  <?php 
																foreach($sectores as $item){
																	echo '<option value="'.$item['codsectorial'].'">'.$item['sector'].'</option>';
																}
															  ?>
															</select>
														</div>
														<div class="col-md-2 vecino"><a href="<?= site_url('taqpacha/ippnr');?>" class="btn btn-lg btn-block btn-primary lead">
																	<i class="glyphicon glyphicon-stats"></i> VER REPORTE
														</a></div>
													</div>
										</div></div>
										<div class="row"><div class="col-md-12 well well-sm">
													<div class="row">
														<div class="col-md-8"><h2 style="margin:10px 0;">7. Inversion Pública gestión <?= $gestion;?> por región </h2></div>
														<div class="col-md-2 padre_selector">
															<select id="r7" class="form-control selector">
															  <option value="-1">&laquo; TODOS &raquo;</option>
															  <?php 
																foreach($regiones as $item){
																	echo '<option value="'.$item['reg_id'].'">'.$item['reg_region'].'</option>';
																}
															  ?>
															</select>
														</div>
														<div class="col-md-2 vecino"><a href="<?= site_url('taqpacha/ipr');?>" class="btn btn-lg btn-block btn-primary lead">
																	<i class="glyphicon glyphicon-stats"></i> VER REPORTE
														</a></div>
													</div>
										</div></div>
										<div class="row"><div class="col-md-12 well well-sm">
													<div class="row">
														<div class="col-md-8"><h2 style="margin:10px 0;">8. Programas No Recurrentes gestión <?= $gestion;?> por región</h2></div>
														<div class="col-md-2 padre_selector">
															<select id="r8" class="form-control selector">
															  <option value="-1">&laquo; TODOS &raquo;</option>
															  <?php 
																foreach($regiones as $item){
																	echo '<option value="'.$item['reg_id'].'">'.$item['reg_region'].'</option>';
																}
															  ?>
															</select>
														</div>
														<div class="col-md-2 vecino"><a href="<?= site_url('taqpacha/pnrr');?>" class="btn btn-lg btn-block btn-primary lead">
																	<i class="glyphicon glyphicon-stats"></i> VER REPORTE
														</a></div>
													</div>
										</div></div>
										<div class="row"><div class="col-md-12 well well-sm">
													<div class="row">
														<div class="col-md-8"><h2 style="margin:10px 0;">9. Inversión Pública gestión <?= $gestion;?> por provincia</h2></div>
														<div class="col-md-2 padre_selector">
															<select id="r9" class="form-control selector">
															  <option value="-1">&laquo; TODOS &raquo;</option>
															  <?php 
																foreach($provincias as $item){
																	echo '<option value="'.$item['prov_id'].'">'.$item['prov_provincia'].'</option>';
																}
															  ?>
															</select>
														</div>
														<div class="col-md-2 vecino"><a href="<?= site_url('taqpacha/ipp');?>" class="btn btn-lg btn-block btn-primary lead">
																	<i class="glyphicon glyphicon-stats"></i> VER REPORTE
														</a></div>
													</div>
										</div></div>
										<div class="row"><div class="col-md-12 well well-sm">
													<div class="row">
														<div class="col-md-8"><h2 style="margin:10px 0;">10. Programas No Recurrentes gestión <?= $gestion;?> por provincia</h2></div>
														<div class="col-md-2 padre_selector">
															<select id="r10" class="form-control selector">
															  <option value="-1">&laquo; TODOS &raquo;</option>
															  <?php 
																foreach($provincias as $item){
																	echo '<option value="'.$item['prov_id'].'">'.$item['prov_provincia'].'</option>';
																}
															  ?>
															</select>
														</div>
														<div class="col-md-2 vecino"><a href="<?= site_url('taqpacha/pnrp');?>" class="btn btn-lg btn-block btn-primary lead">
																	<i class="glyphicon glyphicon-stats"></i> VER REPORTE
														</a></div>
													</div>
										</div></div>
										<div class="row"><div class="col-md-12 well well-sm">
													<div class="row">
														<div class="col-md-8"><h2 style="margin:10px 0;">11. Inversión Pública gestión <?= $gestion;?> por municipio</h2></div>
														<div class="col-md-2 padre_selector">
															<select id="r11" class="form-control selector">
															  <option value="-1">&laquo; TODOS &raquo;</option>
															  <?php 
																foreach($municipios as $item){
																	echo '<option value="'.$item['muni_id'].'">'.$item['muni_municipio'].'</option>';
																}
															  ?>
															</select>
														</div>
														<div class="col-md-2 vecino"><a href="<?= site_url('taqpacha/ipm');?>" class="btn btn-lg btn-block btn-primary lead">
																	<i class="glyphicon glyphicon-stats"></i> VER REPORTE
														</a></div>
													</div>
										</div></div>
										<div class="row"><div class="col-md-12 well well-sm">
													<div class="row">
														<div class="col-md-8"><h2 style="margin:10px 0;">12. Programas No Recurrentes gestión <?= $gestion;?> por municipio</h2></div>
														<div class="col-md-2 padre_selector">
															<select id="r12" class="form-control selector">
															  <option value="-1">&laquo; TODOS &raquo;</option>
															  <?php 
																foreach($municipios as $item){
																	echo '<option value="'.$item['muni_id'].'">'.$item['muni_municipio'].'</option>';
																}
															  ?>
															</select>
														</div>
														<div class="col-md-2 vecino"><a href="<?= site_url('taqpacha/pnrm');?>" class="btn btn-lg btn-block btn-primary lead">
																	<i class="glyphicon glyphicon-stats"></i> VER REPORTE
														</a></div>
													</div>
										</div></div>

									</div>
									<!-- end widget content -->
								<!-- end widget div -->
							</div>
						<!-- end widget -->
					</div>
					<!-- end row -->
				</section>
				<!-- end widget grid -->					
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN PANEL -->
		<!-- PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->
		<!--================================================== -->
		<script src="<?php echo base_url();?>/assets/js/libs/jquery-2.0.2.min.js"></script>
		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>
		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url(); ?>assets/js/session_time/jquery-idletimer.js"></script>
		<script src="<?php echo base_url();?>/assets/js/app.config.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/mis_js/validacion_form.js"></script>
		<SCRIPT src="<?php echo base_url(); ?>mis_js/programacion/ejecucion/abm_ejecucion.js" type="text/javascript"></SCRIPT>
		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url();?>/assets/js/bootstrap/bootstrap.min.js"></script>
		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url();?>/assets/js/notification/SmartNotification.min.js"></script>
		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url();?>/assets/js/smartwidgets/jarvis.widget.min.js"></script>
		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url();?>/assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<!-- SPARKLINES -->
		<script src="<?php echo base_url();?>/assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/select2/select2.min.js"></script>
		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
		<!-- browser msie issue fix -->
		<script src="<?php echo base_url();?>/assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url();?>/assets/js/plugin/fastclick/fastclick.min.js"></script>
		<!-- Demo purpose only -->
		<script src="<?php echo base_url();?>/assets/js/demo.min.js"></script>
		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url();?>/assets/js/app.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<!-- Voice command : plugin -->
		<script src="<?php echo base_url();?>/assets/js/speech/voicecommand.min.js"></script>
		<script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
		<script src="<?php echo base_url();?>/assets/js/plugin/fuelux/wizard/wizard.min.js"></script>
		<script type="text/javascript">
		$(document).ready(function() {
			pageSetUp();
		})
		</script>
		<script>
		$(function () {
			var r5_url = $('#r5').parents('.padre_selector').siblings('.vecino').children('a').attr('href');
			var r6_url = $('#r6').parents('.padre_selector').siblings('.vecino').children('a').attr('href');
			var r7_url = $('#r7').parents('.padre_selector').siblings('.vecino').children('a').attr('href');
			var r8_url = $('#r8').parents('.padre_selector').siblings('.vecino').children('a').attr('href');
			var r9_url = $('#r9').parents('.padre_selector').siblings('.vecino').children('a').attr('href');
			var r10_url = $('#r10').parents('.padre_selector').siblings('.vecino').children('a').attr('href');
			var r11_url = $('#r11').parents('.padre_selector').siblings('.vecino').children('a').attr('href');
			var r12_url = $('#r12').parents('.padre_selector').siblings('.vecino').children('a').attr('href');
		    $("#r5").on("change", function (e) {$(this).parents('.padre_selector').siblings('.vecino').children('a').attr('href', r5_url+'/'+$(this).val());});
		    $("#r6").on("change", function (e) {$(this).parents('.padre_selector').siblings('.vecino').children('a').attr('href', r6_url+'/'+$(this).val());});
		    $("#r7").on("change", function (e) {$(this).parents('.padre_selector').siblings('.vecino').children('a').attr('href', r7_url+'/'+$(this).val());});
		    $("#r8").on("change", function (e) {$(this).parents('.padre_selector').siblings('.vecino').children('a').attr('href', r8_url+'/'+$(this).val());});
		    $("#r9").on("change", function (e) {$(this).parents('.padre_selector').siblings('.vecino').children('a').attr('href', r9_url+'/'+$(this).val());});
		    $("#r10").on("change", function (e) {$(this).parents('.padre_selector').siblings('.vecino').children('a').attr('href', r10_url+'/'+$(this).val());});
		    $("#r11").on("change", function (e) {$(this).parents('.padre_selector').siblings('.vecino').children('a').attr('href', r11_url+'/'+$(this).val());});
		    $("#r12").on("change", function (e) {$(this).parents('.padre_selector').siblings('.vecino').children('a').attr('href', r12_url+'/'+$(this).val());});
		});
		</script>
	</body>
</html>
