<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
		<title><?php echo $this->session->userdata('name')?></title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css"> 
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<!--estiloh-->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css"> 
		    <!--para las alertas-->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.core.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css" id="toggleCSS" />
	    <meta name="viewport" content="width=device-width">
		<!--fin de stiloh-->
          	<script>
		  	function abreVentana(PDF)
			{
				var direccion;
				direccion = '' + PDF;
				window.open(direccion, "Reporte de Proyectos" , "width=800,height=650,scrollbars=SI") ;
			}                                                  
          	</script>
			<style>
			table{font-size: 12px;
            width: 100%;
            max-width:1550px;;
			overflow-x: scroll;
            }
            th{
              padding: 1.4px;
              text-align: center;
              font-size: 12px;
            }
			@media print { 
				.text-center{text-align:center;}
				.no-print { display:none; } 
				#logo img{ width:400px; display:inline-block; margin:30px auto; float:right;}
				#logo::before{ content:url('<?php echo base_url('assets/img_v1.1/logo_gadlp_small.png');?>') !important; display:inline-block; float:left;}
				#logo::after{ content:''; display:block; clear:both;}
				.dataTables_length, .dataTables_paginate{display:none;}
				.title{text-align:center;}
				#dt_basic_filter::before{ content:'FILTRADO POR ...' !important; display:inline-block; float:left;}
				.btn-danger{ background-color:#a02;}
				.btn-warning{ background-color:#c92;}
				.btn-danger, .btn-warning{ display:block; padding:5px; color:#fff; text-align:center;}
				th, td{ border:1px solid #777;}
				th{background-color:#777; color:#fff; border-color:#ccc;}
				table{border-spacing: 0; border-collapse: separate; margin:15px auto;}
			}
			</style>
	</head>
	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->
<header id="header">
	<div id="logo-group" class="col-md-2">
		<span id="logo"> <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="<?php echo $this->session->userdata('name')?>"> </span>
	</div>
	<div class="col-md-6 no-print" style="font-size:15px; margin-top:15px; margin-bottom:-10px;">
		<div class="text-center">
			<span>
				<div class="badge bg-color-blue" style="padding:7px;">
					<span style="font-size:1.6em;">Gesti&oacute;n de trabajo: <?php echo $this->session->userdata('desc_mes').'/'.$this->session->userdata('gestion');?></span>
				</div>
			</span>
			<div class="project-context hidden-xs">
				<span class="project-selector" style="font-size:0.7em;">
					<i class="fa fa-lg fa-fw fa-calendar txt-color-blue"></i>
					<a href="<?php echo base_url();?>index.php/cambiar_gestion">Cambiar Gestión</a>
				</span>
	<!--			<ul class="dropdown-menu">
					<li>
						<a href="<?php echo base_url();?>index.php/cambiar_gestion">Cambiar Gestión</a>
					</li>
				</ul>-->
			</div>
		</div>
	</div>
	<!-- pulled right: nav area -->
	<div class="pull-right col-md-2 no-print">
		<!-- logout button -->
		<div id="logout" class="btn-header transparent pull-right">
            <span> 
				<a href="<?php echo base_url(); ?>index.php/admin/logout" title="Salir" data-action="userLogout" data-logout-msg="Seguro de Salir?">
					Salir <i class="fa fa-sign-out"></i>
				</a> 
			</span>
		</div>
		<!-- end logout button -->

		<!-- fullscreen button -->
		<div id="fullscreen" class="btn-header transparent pull-right">
            <span> 
				<a href="javascript:void(0);" data-action="launchFullscreen" title="Pantalla Completa">
					<i class="fa fa-arrows-alt"></i>
				</a> 
			</span>
		</div>
		<!-- end fullscreen button -->
		<!-- collapse menu button -->
		<div id="hide-menu" class="btn-header pull-right">
            <span> 
				<a href="javascript:void(0);" data-action="toggleMenu" title="Menu">
					<i class="fa fa-reorder"></i>
				</a>
			</span>
		</div>
		<!-- end collapse menu -->

	</div>
	<!-- end pulled right: nav area -->

</header>
		<!-- END HEADER -->
		<!-- Left panel : Navigation area -->
		<aside id="left-panel" class="no-print">
			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                            <span>
                                <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                            </span>
						<i class="fa fa-angle-down"></i>
					</a>
				</span>
			</div>
			<nav>
				<ul>
					<li class="">
	                	<a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MENÚ PRINCIPAL"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
	            	</li>
		            <li class="text-center">
		                <a href="#" title="PROGRAMACION"> <span class="menu-item-parent">PROGRAMACI&Oacute;N DEL POA</span></a>
		            </li>
				<?php
		            for($i=0;$i<count($enlaces);$i++)
		            {
		                if(count($subenlaces[$enlaces[$i]['o_child']])>0)
		                {
		            ?>
			            <li>
			              	<a href="#" >
			              		<i class="<?php echo $enlaces[$i]['o_image']?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
			              	<ul >
			              	<?php
			                foreach ($subenlaces[$enlaces[$i]['o_child']] as $item) {
			                ?>
			                <li><a href="<?php echo base_url($item['o_url']); ?>"><?php echo $item['o_titulo']; ?></a></li>
			                <?php } ?>
			                </ul>
			            </li>
		            <?php 
		            }
		        } ?>
				</ul>
			</nav>
			<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>
		</aside>

		<!-- MAIN PANEL -->
		<div id="main" role="main">

			<!-- RIBBON -->
			<div id="ribbon" class="no-print">
				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>
				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li>Otros reportes</li>
					<li>Reporte Porcentaje de Datos Cargados en el <?php echo $this->session->userdata('name')?></li>
				</ol>
			</div>
			<!-- MAIN CONTENT -->
			<div id="content">
				<!-- widget grid -->
				<section id="widget-grid" class="">
					<div class="row">
						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="jarviswidget jarviswidget-color-darken" >
                              <header>
                                  <span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
                                  <h2 class="font-md title"><strong> Reporte Porcentaje de Datos Cargados en el <?php echo $this->session->userdata('name')?> Gestión <?php echo $this->session->userdata("gestion")?></strong></h2>
                              </header>
								<div>
									<div class="widget-body no-padding">
									<div class="table-responsive">
										<table id="dt_basic" class="table table-bordered" style="width:100%;" font-size: "7px";>
											<thead>
												<tr>
													<th>CATEGORIA PROGRAMATICA <?php echo $this->session->userdata("gestion");?></th>
													<th>PROYECTO_PROGRAMA_OPERACI&Oacute;N DE FUNCIONAMIENTO</th>
													<th>TIPO DE OPERACI&Oacute;N</th>
													<th>C&Oacute;DIGO_SISIN</th>
													<th>RESPONSABLE (UE)</th>
													<th>UNIDAD EJECUTORA</th>
													<th>UNIDAD RESPONSABLE</th>
													<th>% DATOS GENERALES</th>
													<th>% PROG. FISICA</th>
													<th>% PROG. FINANCIERA</th>
													<th>% EJEC. FISICA (Mes <?php echo $this->session->userdata("mes");?>)</th>
													<th>% EJEC. FINANCIERA (Mes <?php echo $this->session->userdata("mes");?>)</th>
													<th>PROVINCIA_MUNICIPIO</th>
												</tr>
											</thead>
											<tbody>
												<?php echo $programas;?>
											</tbody>
										</table>
										</div>
									</div>
									<!-- end widget content -->
								</div>
								<!-- end widget div -->
							</div>
							<!-- end widget -->
						</article>
						<!-- WIDGET END -->
					</div>
				</section>
			</div>
			<!-- END MAIN CONTENT -->
		<script>
		function doSelectAlert(event,dato,id) {
		    var option = event.srcElement.children[event.srcElement.selectedIndex];
		    if (option.dataset.noAlert !== undefined) {
		        return;
		    }
		    if(dato==1){valor='BAJA'}
		    if(dato==2){valor='MEDIA'}
		    if(dato==3){valor='ALTA'}
		    var OK = confirm("Prioridad "+valor+ "  GUARDAR INFORMACION ?");
				if (OK) {
					var url = "<?php echo site_url("admin")?>/proy/prioridad";
				        $.ajax({
				            type: "post",
				            url: url,
				            data:{id:id,pr:dato},
				                success: function (data) {
				                window.location.reload(true);
				            }
				        });
					}
		}
		</script>
		</div>
		<!-- END MAIN PANEL -->
	<!-- ========================================================================================================= -->
		<!-- PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->
		<!--================================================== -->
		<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
		<script data-pace-options='{ "restartOnRequestAfter": true }' src="<?php echo base_url(); ?>assets/js/plugin/pace/pace.min.js"></script>
		<script>
			if (!window.jQuery) {
				document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-2.0.2.min.js"><\/script>');
			}
		</script>

		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>

		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url(); ?>assets/js/session_time/jquery-idletimer.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/app.config.js"></script>
		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.min.js"></script>
		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url(); ?>assets/js/notification/SmartNotification.min.js"></script>
		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url(); ?>assets/js/smartwidgets/jarvis.widget.min.js"></script>
		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<!-- SPARKLINES -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/select2/select2.min.js"></script>
		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
		<!-- browser msie issue fix -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/fastclick/fastclick.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
		<!-- Demo purpose only -->
		<script src="<?php echo base_url(); ?>assets/js/demo.min.js"></script>
		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url(); ?>assets/js/app.min.js"></script>
		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<!-- Voice command : plugin -->
		<script src="<?php echo base_url(); ?>assets/js/speech/voicecommand.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
		<!-- PAGE RELATED PLUGIN(S) -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.colVis.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.tableTools.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
		<script src="<?php echo base_url(); ?>mis_js/programacion/programacion/tablas.js"></script>
	
		<!--================= ELIMINAR PROYECTO  =========================================-->
		<script type="text/javascript">
		    $(function () {
						

		        function reset() {
		            $("#toggleCSS").attr("href", "<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css");
		            alertify.set({
		                labels: {
		                    ok: "ACEPTAR",
		                    cancel: "CANCELAR"
		                },
		                delay: 5000,
		                buttonReverse: false,
		                buttonFocus: "ok"
		            });
		        }

		        // =====================================================================
		        $(".del_ff").on("click", function (e) {
		            reset();
		            var name = $(this).attr('name'); 
		            var request;
		            // confirm dialog
		            alertify.confirm("REALMENTE DESEA ELIMINAR ESTE PROYECTO ?", function (a) {
		                if (a) { 
		                    url = "<?php echo site_url("admin")?>/proy/delete";
		                    if (request) {
		                        request.abort();
		                    }
		                    request = $.ajax({
		                        url: url,
		                        type: "POST",
		                        data: {
		                            proy_id: name
		                        },

		                    });
		                    window.location.reload(true);
		                    request.done(function (response, textStatus, jqXHR) {
		                        $('#tr' + response).html("");
		                    });
		                    request.fail(function (jqXHR, textStatus, thrown) {
		                        console.log("ERROR: " + textStatus);
		                    });
		                    request.always(function () {
		                        //console.log("termino la ejecuicion de ajax");
		                    });

		                    e.preventDefault();
		                    alertify.success("Se eliminó el Proyecto correctamente");

		                } else {
		                    // user clicked "cancel"
		                    alertify.error("Opcion cancelada");
		                }
		            });
		            return false;
		        });
				
    	<!-- ======================================= ASIGNAR PROYECTO AL VALIDADOR POA ======================================= -->
				//limpiar variable
				var id_aper ='';
				$(".mod_aper2").on("click",function(e){
					obs = $(this).attr('name');
					resp = $(this).attr('id');
					document.getElementById("obs").value=obs;
				});
				
				
		    });
				//limpiar variable
				var id_aper =''; 
				$(".mod_aper").on("click",function(e){
					//============== ASIGNAR PROYECTO AL VALIDADOR POA ================
					id_proy = $(this).attr('name');
					tp = $(this).attr('id');
					document.getElementById("id_p").value=id_proy;
					document.getElementById("resp").value=tp;
					var url = "<?php echo site_url("admin")?>/proy/get_resp";
					var codigo ='';
					var request;
					if(request){
						request.abort();
					}
					request = $.ajax({
						url:url,
						type:"POST",
						dataType:'json',
						data: "id_p="+id_proy+"&tp="+tp
					});

					request.done(function(response,textStatus,jqXHR){
						$('#responsable').html(response.responsable);
						//document.mod_formaper.modunidad_o.selectedIndex=response.uni_id;
					});
					request.fail(function(jqXHR,textStatus,thrown){
						console.log("ERROR: "+ textStatus);
					});
					request.always(function(){
						//console.log("termino la ejecuicion de ajax");
					});
					e.preventDefault();
					// =============================VALIDAR EL FORMULARIO DE MODIFICACION
					$("#mod_aperenviar").on("click",function(e){
						var $valid = $("#mod_formaper").valid();
						if (!$valid) {
							$validator.focusInvalid();
						} else {
							//==========================================================
							//var aper_programa = document.getElementById("modaper_programa").value
							var id_p = document.getElementById("id_p").value 
							var tp = document.getElementById("resp").value 
							var url = "<?php echo site_url("admin")?>/proy/asig_proy";
							$.ajax({
								type:"post",
								url:url,
								data:{id_p:id_p,tp:tp},
								success:function(data){
									window.location.reload(true);
								}
							});
						}
					});
				});

		</script>
	</body>
</html>
