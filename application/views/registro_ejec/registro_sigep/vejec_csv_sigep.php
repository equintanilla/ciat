<?php
$atras = site_url() . '/reg/l_sigep';
?>
<div id="main" role="main">
    <div>
        <ol class="breadcrumb">
            <li>Registro de Ejecuci&oacute;n POA</li>
            <li>Ejecuci&oacute;n Presupuestaria SIGEP</li>
            <li><a href="<?php echo $atras ?>">Subir Archivo Sigep</a></li>
        </ol>
    </div>
    <div id="content">
        <br>
        <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                <section id="widget-grid" class="well">
                    <div class="" style="font-size: 15px">
                        <b>  SUBIR ARCHIVO SIGEP DEL MES DE <B><?php echo strtoupper($this->session->userdata('desc_mes'));?></B></b>
                    </div>
                </section>
            </article>
            <article class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                <section id="widget-grid" class="well">
                    <center>
                        <div class="dropdown">
                            <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenu1" style="width:100%;" data-toggle="dropdown" aria-expanded="true">
                                OPCIONES
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo $atras; ?>">VOLVER ATRAS</a></li>
                            </ul>
                        </div>
                    </center>
                </section>
            </article>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-block alert-success">
                    <a class="close" data-dismiss="alert" href="#">×</a>
                    <b style="font-size: 15px">INFORMACION PARA SUBIR EL ARCHIVO CSV!!! <br>
                        1. EL ARCHIVO DEBE GUARDARSE EN FORMATO CSV Y DEBE ESTAR SEPARADO SOLO POR PUNTO Y COMA ; (PUEDE VERIFICAR VIENDO EL ARCHIVO EN BLOC DE NOTAS) <br>
                        2. SOLO DEBE EXISTIR 9 COLUMNAS Y NO DEBE EXISTIR FILAS ADICIONALES. <br>
                        3. EN LA COLUMNA DE TEXTO NO DEBE EXISTIR EL CARACTER PUNTO Y COMA ; <br>
                        4. LOS NUMEROS NO DEBEN ESTAR SEPARADO POR MILES Y LO DECIMALES DEBEN ESTAR SEPARADOS PO PUNTO.
                    </b>
                    <div class="row">
                        <div class="col-md-6 img-responsive text-center">
                            <img src="<?php echo base_url(); ?>assets/img/informacion1.png" class="img-responsive">
                        </div>
                        <div class="col-md-6 img-responsive text-center">
                            <img src="<?php echo base_url(); ?>assets/img/informacion2.png" class="img-responsive">
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <section id="widget-grid" class="">
                <article class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="jarviswidget jarviswidget-color-darken">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-upload"></i> </span>

                            <h2 class="font-md"><strong>ADJUNTAR ARCHIVO SIGEP</strong></h2>
                        </header>
                        <div class="panel-body">
                            <div>
                                <form class="smart-form" action="<?php echo site_url() . '/reg/add_sigep' ?>" novalidate="novalidate" method="post"
                                      enctype="multipart/form-data" id="form_subir_sigep" name="form_subir_sigep">
                                    <fieldset>
                                        <section class="form-group">
                                            <label class="label"><b>NOMBRE DEL ARCHIVO</b></label>
                                            <label class="input">
                                                <i class="icon-prepend fa fa-pencil"></i>
                                                <input type="text" class="" name="nombre" id="nombre" onblur="javascript:this.value=this.value.toUpperCase();"
                                                       onkeypress="if (this.value.length > 40){return false; }">
                                                <b class="tooltip tooltip-bottom-left"><i class="fa fa-warning txt-color-teal"></i> INGRESE NOMBRE DEL ARCHIVO</b>
                                            </label>
                                        </section>

                                        <section class="form-group">
                                            <label class="label"><b>SUBIR ARCHIVO .CSV </b></label>
                                            <label class="input input-file">
                                              <span class="button">
                                                  <input type="file" id="file" name="file" onchange="this.parentNode.nextSibling.value = this.value">
                                                  ELEGIR
                                              </span><input type="text" placeholder="Seleccionar Archivo" readonly="">
                                                <b class="tooltip tooltip-top-left">
                                                    <i class="fa fa-warning txt-color-teal"></i> EL ARCHIVO A SUBIR, DEBE SER EXTENSION .CSV
                                                </b>
                                            </label>
                                        </section>
                                    </fieldset>
                                    <footer>
                                        <button type="button" name="subir_archivo" id="subir_archivo" class="btn btn-primary">
                                            GUARDAR
                                        </button>
                                        <button type="button" class="btn btn-danger" onclick="window.history.back();">
                                            CANCELAR
                                        </button>
                                        <img id="load" style="display: none" src="<?php echo base_url() ?>/assets/img/loading.gif" width="30" height="30">
                                    </footer>
                                </form>
                            </div>
                        </div>
                    </div>
                </article>

                <article class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                    <div class="jarviswidget jarviswidget-color-darken">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-table"></i> </span>

                            <h2 class="font-md">LISTA DE ARCHIVOS SIGEP </h2>
                        </header>

                        <div>
                            <div class="widget-body no-padding">

                                <div class="table-responsive">
                                    <table id="dt_basic" class="table table-bordered" style="width:100%;">
                                        <thead>
                                        <tr style="width: 100%">
                                            <th style="width:1%;"><font size="1">Nro</font></th>
                                            <th style="width:15%;"><font size="1">DOCUMENTO</font></th>
                                            <th style="width:10%;"><font size="1">MES</font></th>
                                            <th style="width:10%;"><font size="1">FUNCIONARIO</font></th>
                                            <th style="width:10%;"><font size="1">FECHA</font></th>
                                            <th style="width:5%;" title="SINCRONIZAR"><font size="1">SINCRONIZAR</font></th>
                                            <th style="width:10%;" TITLE="GUARDAR EJECUCIÓN DEL MES"><font size="1">G.EJECUCION</font></th>
                                            <th style="width:5%;"><font size="1">ELIMINAR</font></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $cont = 1;
                                        $ruta_img_sin_g = base_url() . 'assets/ifinal/archivo1.png';//sin guardar
                                        $ruta_img_guardar = base_url() . 'assets/ifinal/bien.png';// guardar
                                        $ruta_img_sinc = base_url() . 'assets/ifinal/bdexit.png';//sincronizar
                                        $ruta_img_sinc2 = base_url() . 'assets/ifinal/ok1.png';//sincronizar
                                        $ruta_img_del = base_url() . 'assets/ifinal/eliminar.png';//eliminar
                                        foreach ($lista_archivos AS $row) {
                                            $nombre = $row['fun_nombre'] . ' ' . $row['fun_paterno'] . ' ' . $row['fun_materno'];
                                            echo '<tr>';
                                            echo '<td>' . $cont . '</td>';
                                            echo '<td>' . $row['as_nombre'] . '</td>';
                                            echo '<td>' . $row['m_descripcion'] . '</td>';
                                            echo '<td>' . $nombre . '</td>';
                                            $date = new DateTime($row['fecha_creacion']);
                                            echo '<td>' . $date->format('d-m-Y') . '</td>';
                                            if ($row['as_sincronizar'] == 0) {
                                                echo '<td>
                                                    <a  data-toggle="modal" data-target="#modal_sincronizar" class="sincronizar_sigep"
                                                    name="' . $row['as_ruta'] . '" id="' . $row['as_id'] . '" rel="tooltip"
                                                    data-original-title="SINCRONIZAR ARCHIVO">
						                                <img src="' . $ruta_img_sinc . '" width="30" height="30" class="img-responsive">
					                                </a>
					                              </td>';
                                            } else {
                                                echo '<td>
                                                    <a  rel="tooltip" data-original-title="ARCHIVO SINCRONIZADO">
						                                <img src="' . $ruta_img_sinc2 . '" width="30" height="30" class="img-responsive">
					                                </a>
					                              </td>';
                                            }
                                            if ($row['as_guardar_ejecucion'] == 0) {
                                                echo '<td>
                                                    <a  data-toggle="modal" data-target="#modal_guardar_ejecucion" class="guardar_ejecucion"
                                                    name="' . $row['as_id'] . '" id="' . $row['m_descripcion'] . '" rel="tooltip"
                                                    data-original-title="GUARDAR EJECUCIÓN POR MES">
						                                <img src="' . $ruta_img_sin_g . '" width="30" height="30" class="img-responsive">
					                                </a>
					                              </td>';
                                            } else {
                                                echo '<td>
                                                    <a rel="tooltip" data-original-title="ARCHIVO GUARDADO">
						                                <img src="' . $ruta_img_guardar . '" width="30" height="30" class="img-responsive">
					                                </a>
					                              </td>';
                                            }

                                            if ($row['as_guardar_ejecucion'] == 0 && $row['as_sincronizar'] == 0) {
                                                echo '<td>
                                                    <a class="eliminar_archivo" name="' . $row['as_ruta'] . '" id="' . $row['as_id'] . '" rel="tooltip"
                                                     data-original-title="ELIMINAR ARCHIVO">
						                                <img src="' . $ruta_img_del . '" width="30" height="30" class="img-responsive" >
					                                </a>
					                              </td>';
                                            } else {
                                                echo '<td>
                                                    <a rel="tooltip" data-original-title="NO SE PUEDE ELIMINAR">
						                                <img src="' . $ruta_img_del . '" width="30" height="30" class="img-responsive">
					                                </a>
					                              </td>';
                                            }
                                            echo '</tr>';
                                        }
                                        ?>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </section>
        </div>
        <div class="row">
            <section id="widget-grid" class="">
                <div class="row">
                    <div class="jarviswidget jarviswidget-color-darken" id="wid-id-1" data-widget-editbutton="false"
                         data-widget-colorbutton="false" data-widget-deletebutton="false">
                        <header>
                            <h2><p>&nbsp;TABLA TEMPORAL DEL SIGEP</p></h2>
                        </header>
                        <div>
                            <div class="widget-body no-padding">
                                <table id="datatable_fixed_column" class="table table-bordered" width="100%">
                                    <thead>
                                    <tr>
                                        <TH></TH>
                                        <th class="hasinput icon-addon">
                                            <input type="text" class="form-control" placeholder="AP."/>
                                        </th>
                                        <th class="hasinput">
                                            <input type="text" class="form-control" placeholder="COD."/>
                                        </th>
                                        <th class="hasinput">
                                            <input type="text" class="form-control" placeholder="NOMBRE"/>
                                        </th>
                                        <th class="hasinput">
                                            <input type="text" class="form-control" placeholder="PAR XLS."/>
                                        </th>
                                        <th class="hasinput">
                                            <input type="text" class="form-control" placeholder="PAR"/>
                                        </th>
                                        <th class="hasinput">
                                            <input type="text" class="form-control" placeholder="F.F. XLS."/>
                                        </th>
                                        <th class="hasinput">
                                            <input type="text" class="form-control" placeholder="F.F."/>
                                        </th>
                                        <th class="hasinput">
                                            <input type="text" class="form-control" placeholder="O.F. XLS."/>
                                        </th>
                                        <th class="hasinput">
                                            <input type="text" class="form-control" placeholder="O.F."/>
                                        </th>
                                        <th class="hasinput">
                                            <input type="text" class="form-control" placeholder="PRES. INICIAL"/>
                                        </th>
                                        <th class="hasinput">
                                            <input type="text" class="form-control" placeholder="MOD. APROB"/>
                                        </th>
                                        <th class="hasinput">
                                            <input type="text" class="form-control" placeholder="PRES. VIG"/>
                                        </th>
                                        <th class="hasinput">
                                            <input type="text" class="form-control" placeholder="DEVENGADO"/>
                                        </th>
                                        <th class="hasinput">
                                            <input type="text" class="form-control" placeholder="MES"/>
                                        </th>
                                    </tr>
                                    <tr style="background-color: #66b2e8">
                                        <th><font size="1">Nro.</font></th>
                                        <th TITLE="CÓDIGO DE APERTURA DEL EXCEL"><font size="1"> APERTURA XLS.</font></th>
                                        <th TITLE="CÓDIGO DE PROYECTO"><font size="1">COD. PROY </font></th>
                                        <th><font size="1">NOMBRE PROYECTO</font></th>
                                        <th TITLE="CÓDIGO DE PARTIDA DEL EXCEL"><font size="1"> PAR XLS.</font></th>
                                        <th><font size="1">PARTIDA</font></th>
                                        <th TITLE="CÓDIGO FUENTE FINANCIAMIENTO DEL EXCEL"><font size="1"> F.F. XLS.</font></th>
                                        <th><font size="1">FUENTE FINANCIAMIENTO</font></th>
                                        <th TITLE="CÓDIGO ORGANISMO FINANCIADOR DEL EXCEL"><font size="1">O.F. XLS.</font></th>
                                        <th><font size="1">ORGANISMO FINANCIADOR</font></th>
                                        <th><font size="1">PRESUPUESTO INICIAL</font></th>
                                        <th><font size="1">MODIFICACIONES APROBADAS</font></th>
                                        <th><font size="1">PRESUPUESTO VIGENTE</font></th>
                                        <th><font size="1">DEVENGADO</font></th>
                                        <th><font size="1">MES</font></th>
                                    </tr>
                                    </thead>
                                    <tbody id="bdi">
                                    <?php
                                    echo $tabla_proy_ejec;
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>



    </div>
</div>
<!-- =========================== SINCRONIZAR ================================ -->
<div class="modal animated fadeInDown" id="modal_sincronizar" tabindex="-1" role="dialog" data-keyboard="false"
     data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" id="modal_cerrar" class="close text-danger" data-dismiss="modal" aria-hidden="true"> &times;</button>
                <h4 class="modal-title">
                    <img src="<?php echo base_url(); ?>assets/img/logo.png" width="150" alt="SmartAdmin">
                </h4>
                <center><H6><b>SINCRONIZAR !!!</b></H6></center>
            </div>
            <div class="modal-body no-padding">
                <div class="row">
                    <div id="bootstrap-wizard-1" class="col-sm-12">
                        <div class="">
                            <input type="hidden" name="as_id" id="as_id">

                            <div class="row">
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <img src="<?php echo base_url(); ?>assets/ifinal/bdexit.png" WIDTH="70"
                                             HEIGHT="70"/>
                                    </div>
                                </div>
                                <div class="col-sm-10">
                                    <div class="form-group">
                                        <LABEL><H5><b style="color: #568a89;">ESTA SEGURO DE SINCRONIZAR EL ARCHIVO? </b></H5>
                                            <img id="load_modal" style="display: none" src="<?php echo base_url() ?>/assets/img/loading.gif" width="30" height="30"><br>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-3 pull-left">
                        <button class="btn btn-sm btn-danger" id="btn_cerrar" data-dismiss="modal">CANCELAR</button>
                    </div>
                    <div class="col-md-3 pull-right ">
                            <button type="button" name="btn_sincronizar" id="btn_sincronizar" class="btn  btn-sm btn-primary"><i class="fa fa-save"></i>ACEPTAR</button>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- =========================== GUARDAR EJECUCION POR MES ================================ -->
<div class="modal animated fadeInDown" id="modal_guardar_ejecucion" tabindex="-1" role="dialog" data-keyboard="false"
     data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" id="modal_cerrar_ejecucion" class="close text-danger" data-dismiss="modal" aria-hidden="true">&times; </button>
                <h4 class="modal-title">
                    <img src="<?php echo base_url(); ?>assets/img/logo.png" width="150" alt="SmartAdmin">
                </h4>
                <center><H6><b>EJECUCION FINANCIERA <b id="titulo_mes2"> </b> !!!</b></H6></center>
            </div>
            <div class="modal-body no-padding">
                <div class="row">
                    <div id="bootstrap-wizard-1" class="col-sm-12">
                        <div class="">
                            <input type="hidden" name="as_id_guardar" id="as_id_guardar">

                            <div class="row">
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <img src="<?php echo base_url(); ?>assets/ifinal/guardar.png" WIDTH="70" HEIGHT="70"/>
                                    </div>
                                </div>
                                <div class="col-sm-10">
                                    <div class="form-group">
                                        <LABEL><H5><b style="color: #568a89;">ESTA SEGURO DE GUARDAR LA EJECUCIÓN FINANCIERA DEL MES DE <b id="titulo_mes"></b> ? </b>
                                            </H5>
                                            <img id="load_modal_ejecucion" style="display: none" src="<?php echo base_url() ?>/assets/img/loading.gif" width="30"
                                                 height="30"><br>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-3 pull-left">
                        <button class="btn btn-sm btn-danger" id="btn_cerrar_ejecucion" data-dismiss="modal">CANCELAR
                        </button>
                    </div>
                    <div class="col-md-3 pull-right ">
                        <button type="button" name="btn_ejecucion" id="btn_ejecucion" class="btn  btn-sm btn-primary">
                            <i class="fa fa-save"></i> ACEPTAR
                        </button>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- =========================== GUARDAR EJECUCION POR MES ================================ -->
<div class="modal animated fadeInDown" id="modal_guardar_ejecucion" tabindex="-1" role="dialog" data-keyboard="false"
     data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" id="modal_cerrar_ejecucion" class="close text-danger" data-dismiss="modal" aria-hidden="true">&times; </button>
                <h4 class="modal-title">
                    <img src="<?php echo base_url(); ?>assets/img/logo.png" width="150" alt="SmartAdmin">
                </h4>
                <center><H6><b>EJECUCION FINANCIERA <b id="titulo_mes2"> </b> !!!</b></H6></center>
            </div>
            <div class="modal-body no-padding">
                <div class="row">
                    <div id="bootstrap-wizard-1" class="col-sm-12">
                        <div class="">

                            <div class="row">
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <img src="<?php echo base_url(); ?>assets/ifinal/guardar.png" WIDTH="70" HEIGHT="70"/>
                                    </div>
                                </div>
                                <div class="col-sm-10">
                                    <div class="form-group">
                                        <LABEL><H5><b style="color: #568a89;">ESTA SEGURO DE GUARDAR LA EJECUCIÓN FINANCIERA DEL MES DE <b id="titulo_mes"></b> ? </b>
                                            </H5>
                                            <img id="load_modal_ejecucion" style="display: none" src="<?php echo base_url() ?>/assets/img/loading.gif" width="30"
                                                 height="30"><br>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-3 pull-left">
                        <button class="btn btn-sm btn-danger" id="btn_cerrar_ejecucion" data-dismiss="modal">CANCELAR
                        </button>
                    </div>
                    <div class="col-md-3 pull-right ">
                        <button type="button" name="btn_ejecucion" id="btn_ejecucion" class="btn  btn-sm btn-primary">
                            <i class="fa fa-save"></i> ACEPTAR
                        </button>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
