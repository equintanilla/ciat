<?php
$atras = site_url() . '/reg/l_sigep';
?>
<div id="main" role="main">
    <div>
        <ol class="breadcrumb">
            <li>Registro de Ejecuci&oacute;n POA</li>
            <li>Lista SIGEP</li>
        </ol>
    </div>
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 animated fadeInDown">
                <h5 class="page-title txt-color-blueDark"><i class="fa fa-pencil-square-o fa-fw ">&nbsp;</i>
                    EJECUCION PRESUPUESTARIA DEL MES DE <b><?php echo strtoupper($this->session->userdata('desc_mes'));?></b>
                </h5>
            </div>
        </div>
        <br>
        <div class="row">
            <a href="<?php echo $atras ?>" class="btn btn-labeled btn-success" title="VOLVER">
                <span class="btn-label"><i class="fa fa-arrow-left" aria-hidden="true"></i></span>
                <fontsize="1">ATRAS </font>
            </a>
        </div>
        <br>
        <section id="widget-grid" class="">
            <div class="row">
                <div class="jarviswidget jarviswidget-color-teal" id="wid-id-1" data-widget-editbutton="false"
                     data-widget-colorbutton="false" data-widget-deletebutton="false">
                    <header>
                        <p>&nbsp;LISTA DEL SIGEP</p>
                    </header>
                    <div>
                        <div class="widget-body no-padding">
                            <table id="datatable_fixed_column" class="table table-bordered" width="100%">
                                <thead>
                                <tr>
                                    <TH></TH>
                                  <!--  <th class="hasinput icon-addon">
                                        <input type="text" class="form-control" placeholder="AP."/>
                                    </th>-->
                                    <th class="hasinput">
                                        <input type="text" class="form-control" placeholder="COD."/>
                                    </th>
                                    <th class="hasinput">
                                        <input type="text" class="form-control" placeholder="NOMBRE"/>
                                    </th>
                                    <th class="hasinput">
                                        <input type="text" class="form-control" placeholder="APERTURA"/>
                                    </th>
                                    <th class="hasinput">
                                        <input type="text" class="form-control" placeholder="PAR"/>
                                    </th>
                                    <th class="hasinput">
                                        <input type="text" class="form-control" placeholder="F.F."/>
                                    </th>
                                    <th class="hasinput">
                                        <input type="text" class="form-control" placeholder="O.F."/>
                                    </th>
                                    <th class="hasinput">
                                        <input type="text" class="form-control" placeholder="PRES. INICIAL"/>
                                    </th>
                                    <th class="hasinput">
                                        <input type="text" class="form-control" placeholder="MOD. APROB"/>
                                    </th>
                                    <th class="hasinput">
                                        <input type="text" class="form-control" placeholder="PRES. VIG"/>
                                    </th>
                                    <th class="hasinput">
                                        <input type="text" class="form-control" placeholder="DEVENGADO"/>
                                    </th>
                                    <th class="hasinput">
                                        <input type="text" class="form-control" placeholder="MES"/>
                                    </th>
                                </tr>
                                <tr style="background-color: #66b2e8">
                                    <th><font size="1">Nro.</font></th>
                                   <!-- <th TITLE="CÓDIGO DE APERTURA"><font size="1"> APERTURA </font></th>-->
                                    <th TITLE="CÓDIGO DE PROYECTO"><font size="1">COD. PROY </font></th>
                                    <th><font size="1">NOMBRE PROYECTO</font></th>
                                    <th><font size="1">PARTIDA</font></th>
                                    <th><font size="1">APERTURA</font></th>
                                    <th><font size="1">FUENTE FINANCIAMIENTO</font></th>
                                    <th><font size="1">ORGANISMO FINANCIADOR</font></th>
                                    <th><font size="1">PRESUPUESTO INICIAL</font></th>
                                    <th><font size="1">MODIFICACIONES APROBADAS</font></th>
                                    <th><font size="1">PRESUPUESTO VIGENTE</font></th>
                                    <th><font size="1">DEVENGADO</font></th>
                                    <th><font size="1">MES</font></th>
                                </tr>
                                </thead>
                                <tbody id="bdi">
                                <?php
                                echo $tabla_ejec;
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>
</div>

