<?php
$site_url = site_url("");
?>
<div id="main" role="main">
    <div>
        <ol class="breadcrumb">
            <li>Registro de Ejecuci&oacute;n POA</li>
            <li>Importaci&oacute;n de Ejecuci&oacute;n Presupuestaria SIGEP</li>
        </ol>
    </div>
    <div id="content">
        <div class="row">
           <!-- <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 animated fadeInDown">
                <h5 class="page-title txt-color-blueDark"><i class="fa fa-pencil-square-o fa-fw ">&nbsp;</i>
                    IMPORTACI&Oacute;N DE EJECUCI&Oacute;N PRESUPUESTARIA DEL MES DE <b><?php /*echo strtoupper($this->session->userdata('desc_mes'));*/?></b>
                </h5>
            </div>-->
            <div class="row">
                <div class="col-md-2">
                    <a href="<?php echo $site_url . '/reg/c_sigep' ?>" class="btn btn-labeled btn-primary" title="CARGAR ARCHIVO">
                        <span class="btn-label"><i class="fa fa-upload" aria-hidden="true"></i></span>
                        <font size="1">CARGAR ARCHIVO SIGEP</font>
                    </a>
                </div>
               <!-- <div class="col-md-3">
                    <a href="<?php /*echo $site_url . '/reg/lista_sigep' */?>" class="btn btn-labeled btn-primary" title="CARGAR ARCHIVO">
                        <span class="btn-label"><i class="fa fa-table" aria-hidden="true"></i></span>
                        <font size="1">VER EJECUCION PRESUPUESTARIA</font>
                    </a>
                </div>-->
            </div>
        </div>
        <br>
        <section id="widget-grid" class="">
            <div class="row">
                <div class="jarviswidget jarviswidget-color-darken" id="wid-id-1" data-widget-editbutton="false"
                     data-widget-colorbutton="false" data-widget-deletebutton="false">
                    <header>
                        <h2><p>&nbsp;EJECUCI&Oacute;N PRESUPUESTARIA DEL MES DE <b><?php echo strtoupper($this->session->userdata('desc_mes'));?></b></p></h2>
                    </header>
                    <div>
                        <div class="widget-body no-padding">
                            <table id="datatable_fixed_column" class="table table-bordered" width="100%">
                                <thead>
                                <tr>
                                    <TH></TH>
                                    <!--  <th class="hasinput icon-addon">
                                          <input type="text" class="form-control" placeholder="AP."/>
                                      </th>-->
                                    <th class="hasinput">
                                        <input type="text" class="form-control" placeholder="COD."/>
                                    </th>
                                    <th class="hasinput">
                                        <input type="text" class="form-control" placeholder="NOMBRE"/>
                                    </th>
                                    <th class="hasinput">
                                        <input type="text" class="form-control" placeholder="APERTURA"/>
                                    </th>
                                    <th class="hasinput">
                                        <input type="text" class="form-control" placeholder="PAR"/>
                                    </th>
                                    <th class="hasinput">
                                        <input type="text" class="form-control" placeholder="F.F."/>
                                    </th>
                                    <th class="hasinput">
                                        <input type="text" class="form-control" placeholder="O.F."/>
                                    </th>
                                    <th class="hasinput">
                                        <input type="text" class="form-control" placeholder="PRES. INICIAL"/>
                                    </th>
                                    <th class="hasinput">
                                        <input type="text" class="form-control" placeholder="MOD. APROB"/>
                                    </th>
                                    <th class="hasinput">
                                        <input type="text" class="form-control" placeholder="PRES. VIG"/>
                                    </th>
                                    <th class="hasinput">
                                        <input type="text" class="form-control" placeholder="DEVENGADO"/>
                                    </th>
                                    <th class="hasinput">
                                        <input type="text" class="form-control" placeholder="MES"/>
                                    </th>
                                </tr>
                                <tr style="background-color: #66b2e8">
                                    <th><font size="1">Nro.</font></th>
                                    <!-- <th TITLE="CÓDIGO DE APERTURA"><font size="1"> APERTURA </font></th>-->
                                    <th TITLE="CÓDIGO DE PROYECTO"><font size="1">COD. PROY </font></th>
                                    <th><font size="1">NOMBRE PROYECTO</font></th>
                                    <th><font size="1">PARTIDA</font></th>
                                    <th><font size="1">APERTURA</font></th>
                                    <th><font size="1">FUENTE FINANCIAMIENTO</font></th>
                                    <th><font size="1">ORGANISMO FINANCIADOR</font></th>
                                    <th><font size="1">PRESUPUESTO INICIAL</font></th>
                                    <th><font size="1">MODIFICACIONES APROBADAS</font></th>
                                    <th><font size="1">PRESUPUESTO VIGENTE</font></th>
                                    <th><font size="1">DEVENGADO</font></th>
                                    <th><font size="1">MES</font></th>
                                </tr>
                                </thead>
                                <tbody id="bdi">
                                <?php
                                echo $tabla_proy_ejec;
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>
</div>

