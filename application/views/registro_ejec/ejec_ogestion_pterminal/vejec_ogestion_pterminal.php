<div id="main" role="main">
    <div id="">
        <ol class="breadcrumb">
            <li> Registro de Ejecuci&oacute;n</li>
            <li><a href="<?php echo $atras ?>">Red de Programas </a></li>
            <li> Mis Objetivos de Gestión y Producto Terminal</li>
        </ol>
    </div>
    <div id="content">
        <article class="row">
            <article class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                <div class="alert alert-block alert-success">
                    <h4 class="alert-heading">
                        <img src="<?php echo base_url() . 'assets/ifinal/responsable.png' ?>" alt=""> <u>Responsable</u>:
                        <?php echo $this->session->userdata("funcionario"); ?>
                    </h4>
                </div>
            </article>

            <article class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                <section id="widget-grid" class="well">
                    <div class="" style="font-size: 15px">
                        <b>EJECUCI&Oacute;N DE ACCIONES DE CORTO PLAZO Y PRODUCTO TERMINAL</b> <br>
                        <b>PROGRAMA: </b>
                        <small class="txt-color-blueLight"><?php echo $dato_poa[0]['aper_programa'] .
                                $dato_poa[0]['aper_proyecto'] . $dato_poa[0]['aper_actividad'] . " - " . $dato_poa[0]['aper_descripcion'] ?></small>
                        <br><b>MES: </b>
                        <small class="txt-color-blueLight"><?php echo $mes_nombre[$mes_id]; ?></small>
                        <br><b>GESTI&Oacute;N: </b>
                        <small class="txt-color-blueLight"><?php echo $this->session->userData('gestion') ?></small>
                        <br>

                    </div>
                </section>
            </article>
            <article class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                <section id="widget-grid" class="well">
                    <center>
                        <div class="dropdown">
                            <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenu1" style="width:100%;" data-toggle="dropdown" aria-expanded="true">
                                OPCIONES
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                <li role="presentation">
                                    <a role="menuitem" tabindex="-1" href="<?php echo $atras ?>">VOLVER ATRAS</a>
                                </li>
                                <li role="presentation">
                                    <a role="menuitem" tabindex="-1" href="javascript:abreVentana('<?php echo site_url("admin") . '/lmreporte/'.$poa_id.'/'.$aper_id; ?>');">
                                        REPORTE</a>
                                </li>

                            </ul>
                        </div>
                    </center>
                </section>
            </article>

    </div>
    <section id="widget-grid" class="">
        <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="jarviswidget jarviswidget-color-darken">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>

                        <h2 class="font-md"><strong>EJECUCI&Oacute;N DE CORTO PLAZO Y PRODUCTOS DEL PROGRAMA</strong></h2>
                    </header>
                    <div class="widget-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <form action="<?php echo site_url("") . '/reg/add_ejec_op' ?>" id="form_add_ejec_op" name="form_add_ejec_op" method="post"
                                      enctype="multipart/form-data">
                                    <input type="hidden" name="poa_id" id="poa_id" value="<?php echo $poa_id ?>">
                                    <input type="hidden" name="aper_id" id="aper_id" value="<?php echo $aper_id ?>">
                                    <input type="hidden" name="mes_id" id="mes_id" value="<?php echo $mes_id ?>">
                                    <input type="hidden" name="validar" id="validar" value="1">

                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="jarviswidget jarviswidget-color-teal" id="wid-id-21" data-widget-editbutton="false">
                                                <header>
                                                    <span class="widget-icon"> <span class="fa fa-table"></span> </span>

                                                    <h2>ACCI&Oacute;N DE CORTO PLAZO/ PRODUCTO TERMINAL</h2>
                                                </header>
                                                <div>
                                                    <style type="text/css">
                                                        th#ogestion {
                                                            background-color: #568A89;
                                                            color: white;
                                                            font-weight: bold;
                                                        }
                                                    </style>
                                                    <div class="widget-body no-padding">
                                                        <div class="table-responsive">
                                                            <table id="dt_basicno" class="table table-bordered" width="100%">
                                                                <thead>
                                                                <tr>
                                                                    <th id="ogestion">Nro</th>
                                                                    <th id="ogestion">CÓDIGO</th>
                                                                    <th id="ogestion">ACCIÓN DE CORTO PLAZO</th>
                                                                    <th id="ogestion">RESPONSABLE</th>
                                                                    <th id="ogestion" title="TIPO DE INDICADOR"> T.I</font></th>
                                                                    <th id="ogestion"> EJECUCIÓN <?php echo $this->session->userData('gestion'); ?></th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <?php
                                                                echo $tabla_ejecucion;
                                                                ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    $problemas = '';
                                    $causas = '';
                                    $soluciones = '';
                                    $validar = '';
                                    $reg_id = '';
                                    if (isset($dato_ejec_prog)) {
                                        $problemas = $dato_ejec_prog->reg_problemas;
                                        $causas = $dato_ejec_prog->reg_causas;
                                        $soluciones = $dato_ejec_prog->reg_soluciones;
                                        $validar = $dato_ejec_prog->reg_validar;
                                        ?>
                                        <input type="hidden" name="modificar" id="modificar" value="1">
                                        <?php
                                    }
                                    //VERIFICAR SI ESTA VALIDADO
                                    $disabled_estado = '';
                                    if ($validar == 2) {
                                        $disabled_estado = 'disabled="disabled"';
                                    }
                                    ?>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label><font size="2"><b>PROBLEMAS</b></font></label>
                                                            <textarea id="reg_problemas" name="reg_problemas"
                                                                      style="width:100%;" <?php echo $disabled_estado ?>
                                                                      onblur="javascript:this.value=this.value.toUpperCase();"
                                                                      rows="8" class="form-control custom-scroll"
                                                                      onkeypress="if (this.value.length > 300){return false; }"><?php echo $problemas ?></textarea>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label><font size="2"><b>CAUSAS</b></font></label>
                                                       <textarea id="reg_causas" name="reg_causas"
                                                                 style="width:100%;" <?php echo $disabled_estado ?>
                                                                 onblur="javascript:this.value=this.value.toUpperCase();"
                                                                 rows="8" class="form-control custom-scroll"
                                                                 onkeypress="if (this.value.length > 300){return false; }"><?php echo $causas ?></textarea>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label><font size="2"><b>SOLUCIONES</b></font></label>
                                                       <textarea id="reg_soluciones" name="reg_soluciones"
                                                                 style="width:100%;" <?php echo $disabled_estado ?>
                                                                 onblur="javascript:this.value=this.value.toUpperCase();"
                                                                 rows="8" class="form-control custom-scroll"
                                                                 onkeypress="if (this.value.length > 300){return false; }"><?php echo $soluciones ?></textarea>
                                            </div>
                                        </div>

                                    </div>
                                    <?php
                                    if (!isset($dato_ejec_prog)) {
                                        ?>
                                        <!-- MODAL SUBIR PDF -->
                                        <div class="well well-sm">
                                            <div class="row">
                                                <div class="col-sm-5">
                                                    <div class="form-group">
                                                        <LABEL><b style="font-size: 15px">NOMBRE DEL ARCHIVO</b></label>
                                                        <input class="form-control" type="text" name="nombre_archivo" id="nombre_archivo"
                                                               placeholder="Nombre de Archivo" onblur="javascript:this.value=this.value.toUpperCase();">
                                                    </div>
                                                </div>
                                                <div class="col-sm-5">
                                                    <div class="form-group">
                                                        <LABEL><b style="font-size: 15px">SUBIR ARCHIVO MENOR A 5 MB.</b></label>
                                                        <input class="form-control" type="file" name="userfile" id="userfile" placeholder="Seleccione el Archivo" required>
                                                    </div>
                                                </div>
                                                <div class="col-sm-1">
                                                    <label for=""> </label>
                                                    <img id="load" style="display: none" src="<?php echo base_url() ?>/assets/img/loading.gif" width="30" height="30">
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                    <div class="form-actions chat-footer">
                                        <a href="<?php echo $atras ?>" class="btn btn-lg btn-danger" title="Volver a Mis Programas">CANCELAR</a>
                                        <?php
                                        if (isset($mostrar)) {
                                            ?>
                                            <button type="button" value="<?php echo $ejec_id ?>" class="btn btn-primary btn-lg" name="guardar_registro"
                                                    id="guardar_registro" <?php echo $disabled_estado ?> title="MODIFICAR REGISTRO">MODIFICAR
                                            </button>
                                            <?php
                                        } else {
                                            ?>
                                            <button type="button" value="0" class="btn btn-primary btn-lg" name="guardar_registro" id="guardar_registro"
                                                <?php echo $disabled_estado ?> title="GUARDAR REGISTRO">GUARDAR
                                            </button>
                                            <?php
                                        }
                                        ?>
                                        <button type="button" value="1" class="btn btn-success btn-lg" name="validar_registro" id="validar_registro"
                                            <?php echo $disabled_estado ?> title="VALIDAR Y CERRAR REGISTRO">VALIDAR
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>
</div>
</div>


