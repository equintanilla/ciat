<div id="main" role="main">
    <div id="">
        <ol class="breadcrumb">
            <li> Registro de Ejecuci&oacute;n de acciones de corto plazo y Producto Terminal</li>
            <li> Por Programas</li>
        </ol>
    </div>
    <div id="content">
        <section id="widget-grid" class="">
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
                        <header>
                            <span class="widget-icon"> <span class="fa fa-table"></span> </span>

                            <h2>LISTA DE PROGRAMAS PARA REGISTRO DE EJECUCI&Oacute;N DE ACCIONES DE CORTO PLAZO Y PRODUCTOS TERMINALES</h2>
                        </header>
                        <div>
                            <div class="widget-body no-padding">
                                <div class="table-responsive">
                                    <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                        <thead>
                                        <tr>
                                            <th>Nro.</th>
                                            <th TITLE="REGISTRO DE EJECUCIÓN">REGISTRO DE EJECUCI&Oacute;N<BR>DE ACCIONES Y<BR>PRODUCTO TERMINAL</th>
                                            <th TITLE="ARCHIVOS">ARCHIVOS DE REGISTRO</th>
                                            <th>HISTORIAL DE<BR>REGISTRO</th>
                                            <!-- <th>CÓDIGO</th> -->
                                            <th>ACCION</th>
                                            <th>PROGRAMAS</th>
                                            <th>UNIDAD ORGANIZACIONAL</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $cont = 1;
                                        $ruta_reg = site_url("") . '/reg/lo/';//ruta registar ejecucion
                                        $ruta_m = site_url("") . '/reg/lm/';//ruta mostrar ejecucion
                                        $ruta_img_reg = base_url() . 'assets/ifinal/form2.jpg';//ruta imagen registro
                                        $ruta_img_m = base_url() . 'assets/ifinal/proyecto1.png';//ruta imagen registro mostar
                                        $ruta_img_arc = base_url() . 'assets/ifinal/doc.jpg';//ruta imagen ARCHIVOS
                                        $ruta_arc = site_url("") . '/reg/arc/';//ruta de archivos
                                        $ruta_mes = site_url("") . '/reg/lmes/';//ruta para el mes
                                        $ruta_img_mes = base_url() . 'assets/ifinal/formularios.jpg';//ruta imagen MES
                                        $ruta_img_sin_archivos = base_url() . 'assets/ifinal/1.png';
                                        foreach ($lista_poa as $row) {
                                            echo '<tr>';
                                            echo '<td>' . $cont . '</td>';

                                            if ($row['ejecutado'] == 0) {
                                                echo '<td>';
                                                echo '
                                                    <a href="' . $ruta_reg . $row['poa_id'] . '/' . $row['aper_id'] . '/' . $row['o_id'] . '">
											            <img src="' . $ruta_img_reg . '" width="40" height="40" class="img-responsive" title="REGISTRAR EJECUCIÓN">
											        </a>';
                                                echo '</td>';
                                                echo '<td>
                                                    <a href="" title="ARCHIVOS">
											            <img src="' . $ruta_img_sin_archivos . '" width="40" height="40" class="img-responsive"
											            title="REGISTRE LA EJECUCIÓN DEL PROGRAMA">
											        </a>
                                                  </td>';
                                            } else {
                                                echo '<td>';
                                                echo '
                                                    <a href="' . $ruta_m . $row['poa_id'] . '/' . $row['aper_id'] . '/' . $row['o_id'] . '">
											            <img src="' . $ruta_img_m . '" width="40" height="40" class="img-responsive" title="MOSTRAR EJECUCIÓN">
											        </a>';
                                                echo '</td>';
                                                echo '<td>
                                                    <a href="' . $ruta_arc . $row['poa_id'] . '/' . $row['aper_id'] . '/' . $row['o_id'] . '" title="ARCHIVOS">
											            <img src="' . $ruta_img_arc . '" width="40" height="40" class="img-responsive" title="ARCHIVOS">
											        </a>
                                                  </td>';
                                            }
                                            echo '<td>
                                                    <a href="' . $ruta_mes . $row['poa_id'] . '/' . $row['aper_id'] . '/' . $row['o_id'] . '" title="EJECUCION POR MESES">
											            <img src="' . $ruta_img_mes . '" width="40" height="40" class="img-responsive" title="EJECUCION POR MESES">
											        </a>
                                                  </td>';
                                            echo '<td><font size="1">' . $row['o_objetivo'] . '</font></td>';
                                            echo '<td><font size="1">' . $row['aper_programa'] . $row['aper_proyecto'] . $row['aper_actividad'] . " - " . $row['aper_descripcion'] . '</font></td>';
                                            echo '<td><font size="1">' . $row['uni_unidad'] . '</font></td>';
                                           // echo '<td><font size="1">' . $row['poa_fecha_creacion'] . '</font></td>';
                                            echo '</tr>';
                                            $cont++;
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </section>
    </div>
</div>
