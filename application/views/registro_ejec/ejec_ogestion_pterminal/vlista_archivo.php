<div id="main" role="main">
    <div id="">
        <ol class="breadcrumb">
            <li> Registro de Ejecuci&oacute;n</li>
            <li><a href="<?php echo $atras ?>">Red de Programas </a></li>
            <li> Lista de Archivos</li>
        </ol>
    </div>
    <div id="content">
        <article class="row">
            <article class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                <div class="alert alert-block alert-success">
                    <h4 class="alert-heading">
                        <img src="<?php echo base_url() . 'assets/ifinal/responsable.png' ?>" alt=""> <u>Responsable</u>:
                        <?php echo $this->session->userdata("funcionario"); ?>
                    </h4>
                </div>
            </article>

            <article class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                <section id="widget-grid" class="well">
                    <div class="" style="font-size: 15px">
                        <b>EJECUCI&Oacute;N DE ACCIONES DE CORTO PLAZO Y PRODUCTO TERMINAL</b> <br>
                        <b>PROGRAMA: </b>
                        <small class="txt-color-blueLight"><?php echo $dato_poa[0]['aper_programa'] .
                                $dato_poa[0]['aper_proyecto'] . $dato_poa[0]['aper_actividad'] . " - " . $dato_poa[0]['aper_descripcion'] ?></small>
                        <br><b>MES: </b>
                        <small class="txt-color-blueLight"><?php echo $mes_nombre[$mes_id]; ?></small>
                        <br><b>GESTI&Oacute;N: </b>
                        <small class="txt-color-blueLight"><?php echo $this->session->userData('gestion') ?></small>
                        <br>

                    </div>
                </section>
            </article>
            <article class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                <section id="widget-grid" class="well">
                    <center>
                        <div class="dropdown">
                            <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenu1" style="width:100%;" data-toggle="dropdown" aria-expanded="true">
                                OPCIONES
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                <li role="presentation">
                                    <a role="menuitem" tabindex="-1" href="<?php echo $atras ?>">VOLVER ATRAS</a>
                                </li>
                            </ul>
                        </div>
                    </center>
                </section>
            </article>

    </div>


    <section id="widget-grid" class="">
        <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="jarviswidget jarviswidget-color-darken">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>

                        <h2 class="font-md"><strong>ADJUNTAR ARCHIVO</strong></h2>
                    </header>
                    <form action="<?php echo site_url("") . '/reg/add_arc' ?>" id="form_arc_ejec_prog" name="form_arc_ejec_prog" method="post"
                          enctype="multipart/form-data">
                        <input type="hidden" name="poa_id" id="poa_id" value="<?php echo $poa_id ?>">
                        <input type="hidden" name="aper_id" id="aper_id" value="<?php echo $aper_id ?>">
                        <input type="hidden" name="mes_id" id="mes_id" value="<?php echo $mes_id ?>">
                        <input type="hidden" name="principal" id="principal" value="<?php echo $principal ?>">

                        <div class="well">
                            <label class="control-label">Seleccione un Archivo</label>

                            <div class="form-group">
                                <input type="file" id="file" name="file" class="file">
                            </div>
                            <label class="control-label">Nombre del Archivo</label>

                            <div class="form-group">
                                <input class="form-control" type="text" name="nombre_archivo" id="nombre_archivo" placeholder="Nombre Documento">
                            </div>
                            <input type="hidden" name="reg_id" id="reg_id" value="<?php echo $reg_id ?>">

                            <div class="col-sm-1">
                                <label for=""> </label>
                                <img id="load" style="display: none" src="<?php echo base_url() ?>/assets/img/loading.gif" width="30" height="30">
                            </div>
                        </div>

                            <a href="<?php echo $atras ?>" class="btn btn-lg btn-danger" title="Volver"> CANCELAR </a>
                            <input type="button" name="subir_archivo" id="subir_archivo" value="SUBIR" class="btn btn-primary btn-lg">
                    </form>
                </div>

            </article>
            <article class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="jarviswidget jarviswidget-color-darken">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>

                        <h2 class="font-md">LISTA DE ARCHIVOS / DOCUMENTOS :
                            <?php echo $mes_nombre[$mes_id] ?>
                        </h2>
                    </header>
                    <div>
                        <div class="widget-body no-padding">
                            <div class="table-responsive">
                                <table id="dt_basic1" class="table table-bordered table-responsive">
                                    <thead>
                                    <tr>
                                        <th>Nro</th>
                                        <th>DOCUMENTO</th>
                                        <th>FECHA</th>
                                        <th>&nbsp;VER&nbsp;</th>
                                        <th>&nbsp;ELIMINAR&nbsp;</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $num = 1;
                                    foreach ($lista_archivos as $row) {
                                        echo '<tr>';
                                        echo '<td>' . $num . '</td>';
                                        echo '<td>' . $row['pa_nombre'] . '</td>';
                                        $date = new DateTime($row['fecha_creacion']);
                                        echo '<td>' . $date->format('d-m-Y') . '</td>';
                                        if (file_exists("archivos/ejec_programa/" . $row['pa_ruta_archivo'])) {
                                            ?>
                                            <td>
                                                <a href="<?php echo base_url(); ?>archivos/ejec_programa/<?php echo $row['pa_ruta_archivo'] ?>"
                                                   target="_blank" title="VER ARCHIVO">
                                                    <img src="<?php echo base_url() . 'assets/ifinal/pdf.png' ?>" alt="">
                                                </a>
                                            </td>
                                            <?php
                                        } else {
                                            ?>
                                            <td>
                                                <a href="#" title="NO EXISTE EL ARCHIVO">
                                                    <img src="<?php echo base_url() . 'assets/ifinal/nodoc.png' ?>" title="NO EXISTE EL ARCHIVO">
                                                </a>
                                            </td>
                                            <?php
                                        }
                                        ?>
                                        <td>
                                            <a class="del_archivo" title="ELIMINAR ARCHIVO" id="del_archivo" name="<?php echo $row['pa_id'] ?>">
                                                <img src="<?php echo base_url() . 'assets/ifinal/eliminar.png' ?>">
                                            </a>
                                        </td>
                                        <?php
                                        echo '</tr>';
                                        $num = $num + 1;
                                    }
                                    ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </article>

        </div>
    </section>
</div>
</div>



