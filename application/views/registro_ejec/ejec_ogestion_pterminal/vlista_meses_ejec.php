<?php
$site_url = site_url("");
$atras = $site_url.'/reg/ejec_op';
?>
<div id="main" role="main">
    <div>
        <ol class="breadcrumb">
            <li> Registro de Ejecuci&oacute;n</li>
            <li><a href="<?php echo $atras?>">Red de Programas </a></li>
            <li> Lista de Registro de Ejecuci&oacute;n por Mes</li>
        </ol>
    </div>
    <div id="content">
        <article class="row">
            <article class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                <div class="alert alert-block alert-success">
                    <h4 class="alert-heading">
                        <img src="<?php echo base_url() . 'assets/ifinal/responsable.png' ?>" alt=""> <u>Responsable</u>:
                        <?php echo $this->session->userdata("funcionario"); ?>
                    </h4>
                </div>
            </article>

            <article class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                <section id="widget-grid" class="well">
                    <div class="" style="font-size: 15px">
                        <b>EJECUCI&Oacute;N DE ACCIONES DE CORTO PLAZO Y PRODUCTO TERMINAL</b> <br>
                        <b>PROGRAMA: </b>
                        <small class="txt-color-blueLight"><?php echo $dato_poa['aper_programa'] . $dato_poa['aper_proyecto'] . $dato_poa['aper_actividad'] .
                                " - " . $dato_poa['aper_descripcion'] ?></small>
                        <br><b>GESTI&Oacute;N: </b>
                        <small class="txt-color-blueLight"><?php echo $this->session->userData('gestion') ?></small>
                        <br>

                    </div>
                </section>
            </article>
            <article class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                <section id="widget-grid" class="well">
                    <center>
                        <div class="dropdown">
                            <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenu1" style="width:100%;" data-toggle="dropdown" aria-expanded="true">
                                OPCIONES
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                <li role="presentation">
                                    <a role="menuitem" tabindex="-1" href="<?php echo $atras ?>">VOLVER ATRAS</a>
                                </li>
                            </ul>
                        </div>
                    </center>
                </section>
            </article>

    </div>
        <input type="hidden" name="poa_id" id="poa_id" value="<?php echo $poa_id?>">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget jarviswidget-color-darken">
                <header>
                    <span class="widget-icon"> <span class="glyphicon glyphicon-folder-close"></span></span>
                    <h2>LISTA REGISTRO DE EJECUCIÓN POR MES</h2>
                </header>
                <div>
                    <div class="widget-body no-padding">
                        <div class="table">
                            <table id="dt_basic2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Nro.</th>
                                    <th TITLE="REGISTRO DE EJECUCIÓN">REGISTRO DE<BR>EJECUCI&Oacute;N</th>
                                    <th TITLE="LISTA DE ARCHIVOS">ARCHIVOS</th>
                                    <th TITLE="REVERTIR REGISTRO DE EJECUCIÓN">REVERSIONES</th>
                                    <th>MES</th>
                                    <th>PROBLEMAS</th>
                                    <th>CAUSAS</th>
                                    <th>SOLUCIONES</th>
                                    <th>FUNCIONARIO</th>
                                    <th>FECHA REGISTRO</th>
                                    <th>ESTADO</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                echo $tabla_ejec_mes;
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </div>
</div>



