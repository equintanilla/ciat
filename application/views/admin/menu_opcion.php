<div id="main" role="main">
    <div id="">
        <ol class="breadcrumb">
            <li>P&aacute;gina Inicio</li>
            <li>Vista</li>
        </ol>
    </div>
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-12 animated fadeInDown">
                <h1 class="page-title txt-color-blueDark"><i class="fa fa-pencil-square-o fa-fw ">&nbsp;</i>
                    <!--REPORTE TAQPACHA-->
                </h1>
            </div>
        </div>
        <article class="col-xs-10 col-sm-10 col-md-10 col-lg-10 col-xs-offset-1 col-sm-offset-1 col-lg-offset-1 col-md-offset-1">
            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">
                <header>
                    <span class="widget-icon"> <span class="fa fa-table"></span> </span>
                    <h2> Principal</h2>
                </header>
                <style type="text/css">
                    .img-circle{
                        width: 80px;
                    }
                    .ruta {
                        text-decoration: none;
                        color-link: white;
                    }

                    .icono {
                        transition: 0.3s;
                        color: white;
                    }

                    i:hover {

                        transform: scale(1.2);
                        letter-spacing: 5px;
                        color: whitesmoke;
                    }

                </style>
                <div>
                    <!--<div class="widget-body no-padding">
                        <br>
                        <div class="row">
                            <div class="well well-sm col-sm-6 animated rotateInDownLeft">
                                <div class="well well-sm bg-color-pinkDark txt-color-white text-center">
                                    <a href="<?php echo base_url(); ?>index.php/admin/dm/1/" class="jarvismetro-tile big-cubes bg-color-greenLight">
                                        <span class="iconbox"> 
                                            <img class="img-circle" src="<?php echo base_url(); ?>assets/img/programacion.png"/>
                                            <span>
                                                Programación
                                                <span class="label pull-right bg-color-darken"></span>
                                            </span>
                                        </span>
                                    </a>
                                    <a href="<?php echo base_url(); ?>index.php/admin/dm/1/" class="ruta">
                                        <h5 style="font-weight: bold;font-style: italic;color: white">Programación</h5>
                                        <img class="img-circle" src="<?php echo base_url(); ?>assets/img/programacion.png"/>
                                        <i class="fa fa-pie-chart fa-5x icono" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="well well-sm col-sm-6 animated rotateInDownRight">
                                <div class="well well-sm bg-color-teal txt-color-white text-center">
                                    <a href="" class="ruta">
                                        <h5 style="font-weight: bold;font-style: italic;color: white">POR ESTADO DE PROYECTOS</h5>
                                        <i class="fa fa-line-chart fa-5x icono" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="well well-sm col-sm-6 animated bounceInLeft">
                                <div class="well well-sm bg-color-blue txt-color-white text-center">
                                    <h5>POR SECTOR ECONÓMICO</h5>
                                    <i class="fa fa-area-chart fa-5x" aria-hidden="true"></i>
                                </div>
                            </div>
                            <div class="well well-sm col-sm-6 animated bounceInRight">
                                <div class="well well-sm bg-color-redLight txt-color-white text-center">
                                    <h5>POR REGIÓN</h5>
                                    <i class="fa fa-bar-chart fa-5x" aria-hidden="true"></i>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="well well-sm col-sm-6 animated rotateInUpLeft">
                                <div class="well well-sm bg-color-yellow txt-color-white text-center">
                                    <h5>POR PROVINCIA</h5>
                                    <i class="fa fa-signal fa-5x" aria-hidden="true"></i>
                                </div>
                            </div>
                            <div class="well well-sm col-sm-6 animated rotateInUpRight">
                                <div class="well well-sm bg-color-orangeDark txt-color-white text-center">
                                    <h5>POR MUNICIPIO</h5>
                                    <i class="fa fa-pie-chart fa-5x" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </div>-->
                </div>
            </div>
        </article>
    </div>
</div>
