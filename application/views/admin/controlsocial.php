<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

		<title> Control Social </title>
		<meta name="description" content="">
		<meta name="author" content="">
			
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">

		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">

		<!-- SmartAdmin RTL Support is under construction
			 This RTL CSS will be released in version 1.5
		<link rel="stylesheet" type="text/css" media="screen" href="css/smartadmin-rtl.min.css"> -->

		<!-- We recommend you use "your_style.css" to override SmartAdmin
		     specific styles this will also ensure you retrain your customization with each SmartAdmin update.
		<link rel="stylesheet" type="text/css" media="screen" href="css/your_style.css"> -->

		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">

		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">

		<!-- GOOGLE FONT -->
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

		<!-- Specifying a Webpage Icon for Web Clip 
			 Ref: https://developer.apple.com/library/ios/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html -->
		<link rel="apple-touch-icon" href="img/splash/sptouch-icon-iphone.png">
		<link rel="apple-touch-icon" sizes="76x76" href="img/splash/touch-icon-ipad.png">
		<link rel="apple-touch-icon" sizes="120x120" href="img/splash/touch-icon-iphone-retina.png">
		<link rel="apple-touch-icon" sizes="152x152" href="img/splash/touch-icon-ipad-retina.png">
		<!--estiloh-->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
		<!--fin de stiloh-->
		<!-- iOS web-app metas : hides Safari UI Components and Changes Status Bar Appearance -->
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		
		<!-- Startup image for web apps -->
		<link rel="apple-touch-startup-image" href="<?php echo base_url(); ?>assets/img/splash/ipad-landscape.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
		<link rel="apple-touch-startup-image" href="<?php echo base_url(); ?>assets/img/splash/ipad-portrait.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
		<link rel="apple-touch-startup-image" href="<?php echo base_url(); ?>assets/img/splash/iphone.png" media="screen and (max-device-width: 320px)">
<style>
	#dt_basic{
  display: inline-block;
width:100%;
max-width:1350px;;
    overflow-x: scroll;
    

}
</style>
<script>
                    function abreVentana(PDF)
                    {             
                         var direccion;
                         direccion = '' + PDF;
                         window.open(direccion, "Reporte de Proyectos" , "width=800,height=650,scrollbars=SI") ; 
                                                                                        
                    }
                                                                  
          </script>
	</head>
	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->

		<!-- HEADER -->
		<header id="header">
			<div id="logo-group">

				<!-- PLACE YOUR LOGO HERE -->
				<span id="logo"> <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="SmartAdmin"> </span>
				<!-- END LOGO PLACEHOLDER -->

				<!-- Note: The activity badge color changes when clicked and resets the number to 0
				Suggestion: You may want to set a flag when this happens to tick off all checked messages / notifications -->
				
				<!-- END AJAX-DROPDOWN -->
			</div>

			

			<!-- pulled right: nav area -->
			
			<!-- end pulled right: nav area -->

		</header>
		<!-- END HEADER -->

		<!-- Left panel : Navigation area -->
		<!-- Note: This width of the aside area can be adjusted through LESS variables -->
		<aside id="left-panel">

			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as it --> 
					<style type="text/css">.fa-user{ font-size: 15px; }</style>
					<center><a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
						<i class="fa fa-user" aria-hidden="true"></i> 
						<span>
							Invitado 
						</span>
						
					</a> </center>
					
				</span>
			</div>
			<!-- end user info -->

			<!-- NAVIGATION : This navigation is also responsive

			To make this navigation dynamic please make sure to link the node
			(the reference to the nav > ul) after page load. Or the navigation
			will not initialize.
			-->
			<nav>
				<!-- NOTE: Notice the gaps after each icon usage <i></i>..
				Please note that these links work a bit different than
				traditional href="" links. See documentation for details.
				-->

				<ul>
					<li>
						<a href="http://200.105.169.252/SISTEMA/index.php/admin/logins" title="Dashboard"><i class="fa fa-sign-in" aria-hidden="true"></i> <span class="menu-item-parent">Atras</span></a>
					</li>
					
				</ul>
			</nav>
			<span class="minifyme" data-action="minifyMenu"> 
				<i class="fa fa-arrow-circle-left hit"></i> 
			</span>

		</aside>
		<!-- END NAVIGATION -->

		<!-- MAIN PANEL -->
		<div id="main" role="main">

			<!-- RIBBON -->
			<div id="ribbon">

				

				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li><a href="http://200.105.169.252/SISTEMA/index.php/admin/logins" title="Dashboard"><i class="fa fa-sign-in" aria-hidden="true"></i> <span class="menu-item-parent">Atras</span></a></li><li>Control Social</li>
				</ol>
				<!-- end breadcrumb -->

				<!-- You can also add more buttons to the
				ribbon for further usability

				Example below:

				<span class="ribbon-button-alignment pull-right">
				<span id="search" class="btn btn-ribbon hidden-xs" data-title="search"><i class="fa-grid"></i> Change Grid</span>
				<span id="add" class="btn btn-ribbon hidden-xs" data-title="add"><i class="fa-plus"></i> Add</span>
				<span id="search" class="btn btn-ribbon" data-title="search"><i class="fa-search"></i> <span class="hidden-mobile">Search</span></span>
				</span> -->

			</div>
			<!-- END RIBBON -->

			<!-- MAIN CONTENT -->
			<div id="content">

				<!-- row -->
				<div class="row">
				
					<!-- col -->
					<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
						<h1 class="page-title txt-color-blueDark"><!-- PAGE HEADER --><i class="fa-fw fa fa-home"></i> Control Social </h1>
					</div>
				
				</div>
				<!-- end row -->
				
				<!-- row -->
				<div class="row">
				
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<!--proyectos-->
		<section id="widget-grid" class="">
					<div class="row">
						
						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								
								<div class="jarviswidget jarviswidget-color-teal" >
									<header>
										<span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
										<h2 class="font-md"><strong>PROYECTOS DE INVERSI&Oacute;N</strong></h2>				
									</header>
			
								<div>
									<div class="widget-body no-padding">

									<div class="table-responsive">

										<table id="dt_basic" class="table table-striped table-bordered table-hover" style="width:100%;" font-size: "7px";>
											<thead>	<style type="text/css">#graf{font-size: 30px;}</style> 		                
												
													<th style="width:5%;"><font size="1">CODIGO</font></th>
													<th style="width:10%;"><font size="1">APERTURA</font></th>
													<th style="width:20%;"><font size="1">&nbsp;PROYECTO/ACTIVIDAD&nbsp;</font></th>
													<th style="width:10%;"><font size="1">&nbsp;CODIGO/SISIN&nbsp;</font></th>
													<th style="width:10%;"><font size="1">&nbsp;RESPONSABLE&nbsp;</font></th>
													<th style="width:10%;"><font size="1">&nbsp;UNIDAD/RESPONSABLE&nbsp;</font></th>
													<th style="width:10%;"><font size="1">&nbsp;Fase/Etapa&nbsp;</font></th>
													<th style="width:10%;"><font size="1">&nbsp;Nuevo/Continuidad&nbsp;</font></th>
													<th style="width:10%;"><font size="1">&nbsp;Anual/Plurianual&nbsp;</font></th>
													<th style="width:5%;"><font size="1">&nbsp;PRESUPUESTO&nbsp;</font></th>
													
													<th style="width:2%;"><font size="1">PROVINCIA/MUNICIPIO</font></th>
													<th style="width:1%;"><font size="1">IMPRIMIR</font></th>
																								
												</tr>
											</thead>
											<tbody>
												<?php $nro=1;
			                                      foreach($proy1 as $row)
			                                      { 
			                                      	if($this->session->userdata("fun_id")==0 & $row['proy_estado']==1)
			                                      	{

			                                       echo '<tr>';
			                                       
			                                        echo '<td><font size="1">'.$row['proy_codigo'].'</font></td>';
			                                        echo '<td><font size="1">'.$row['aper_programa'].''.$row['aper_proyecto'].''.$row['aper_actividad'].'</font></td>';
			                                        echo '<td><font size="1">'.$row['proy_nombre'].'</font></td>';
			                                        echo '<td><font size="1">'.$row['proy_sisin'].'</font></td>';
			                                        echo '<td><font size="1">'.$row['funcionario1'].'</font></td>';
			                                        echo '<td><font size="1">'.$row['unidad'].'</font></td>';

			                                        if($this->model_componente->verif_fase($row['proy_id'])!=0)
			                                        	{
			                                        		$data['fase'] = $this->model_componente->get_id_fase($row['proy_id']); 
			                                        		echo '<td><font size="1">- '.$data['fase'][0]['fase'].'<br>- '.$data['fase'][0]['etapa'].'</font></td>';
			                                        		if($data['fase'][0]['pfec_fecha_inicio']==$this->session->userdata("gestion")){
			                                        			echo '<td><font size="1">NUEVO</font></td>';
			                                        		}
			                                        		elseif ($data['fase'][0]['pfec_fecha_inicio']!=$this->session->userdata("gestion")) {
			                                        			echo '<td><font size="1">CONTINUIDAD</font></td>';
			                                        		}
			                                        		
			                                        		if($data['fase'][0]['pfec_fecha_fin']-$data['fase'][0]['pfec_fecha_inicio']==0){
			                                        			echo '<td><font size="1">ANUAL</font></td>';
			                                        		}
			                                        		elseif ($data['fase'][0]['pfec_fecha_fin']-$data['fase'][0]['pfec_fecha_inicio']!=0) {
			                                        			echo '<td><font size="1">PLURI ANUAL</font></td>';
			                                        		}

			                                        		echo '<td><font size="1">'.number_format($data['fase'][0]['pfec_gestion']).' Bs.</font></td>';
			                                        		 	
			                                        		
			                                        	}
			                                        elseif ($this->model_componente->verif_fase($row['proy_id'])==0) {

			                                        	echo '<td><font size="1" color="red">N/R</font></td>';
			                                        	echo '<td><font size="1" color="red"> N/R</font></td>';
			                                        	echo '<td><font size="1" color="red">N/R</font></td>';
			                                        	echo '<td><font size="1" color="red"> N/R</font></td>';
			                                        	

			                                        }
			                                        ?>
			                                        <!-- ================================================================================ UBICACION =========================================================================== --> 
			                                        <?php
			                                         echo '<td><font size="1">';
					                                        echo '<table class="table table-bordered" border="1">
					                                        		<thead>';
																		$nro_prov= $this->model_proyecto->nro_proy_prov($row['proy_id']);
																		if($nro_prov!=0)
																		{
																			$prov= $this->model_proyecto->proy_prov($row['proy_id']);
																			foreach ($prov as $rowp)
										                                    {
										                                    	echo '<tr><td bgcolor="#696969"><center><b><font color="#ffffff">'.strtoupper($rowp['prov_provincia']).'</font></b></center></td></tr>';
										                                    	$nro_mun= $this->model_proyecto->nro_proy_mun($row['proy_id']);
										                                    	echo '<tr><td>';
										                                    	if($nro_mun!=0)
										                                    	{	$muni= $this->model_proyecto->proy_mun($row['proy_id']);
										                                    		echo '<table >';
										                                    		foreach ($muni as $rowm)
													                                {
													                                    if($rowm['prov_id']==$rowp['prov_id'])
													                                    {
													                                    	echo '<tr><td> - '.$rowm['muni_municipio'].'</td></tr>';
													                                    }
													                                }
													                                echo '</table>';
										                                    	}
										                                    	elseif ($nro_mun==0) 
										                                    	{
										                                    		echo "<font color='blue'>No tiene municipios</font>";
										                                    	}
										                                    	echo'</td></tr>';

										                                    }
																		}
							                                 			elseif($nro_prov==0) 
							                                 			{
								                                 			echo '<td>';
								                                 			?>
								                                 			<center><img src="<?php echo base_url(); ?>assets/Iconos/map_delete.png" WIDTH="30" HEIGHT="30"/></center>
								                                 			<?php
								                                 			echo '</td>';
							                                 			}
		                            							echo '</thead>
		                            							</table>';
					                                        echo'</font></td>';
					                                       
			                                        ?>
			                                        <!-- ================================================================================ END UBICACION =========================================================================== -->
			                                        <td>
																	<div class="btnapp">
                                                                        <div class="hover-btn">
                                                                        	
                                                                            <a href="javascript:abreVentana('<?php echo site_url("admin").'/proy/ficha_tecnica/'.$row['proy_id']; ?>');" class="btn btn-xs  botones tres" title="FICHA TECNICA - PDF">
                                                                                <div class="btn-hover-postion3">
                                                                                 <i class="fa fa-file-pdf-o" aria-hidden="true"></i> 
                                                                                </div>
                                                                            </a>
                                                                             

                                                                        </div>
                                                                    </div>
														</td>
			                                          
			                                            
			                                           	<?php
			                                           if($this->model_componente->verif_fase($row['proy_id'])!=0) ////// tiene fase activa
			                                        	{ $data['fase'] = $this->model_componente->get_id_fase($row['proy_id']); 
			                                        		?>
			                                        		
			                                        		<?php
			                                        	}
			                                        	elseif($this->model_componente->verif_fase($row['proy_id'])==0) //// no tiene fase activa
			                                        	{
			                                        		?>
			                                        		
			                                        		<?php
			                                        	}
			                                          
			                                      echo '</tr>';
			                                      }
			                                     /*///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
			                                      elseif($row['fun_id1']==$this->session->userdata("fun_id") & $row['proy_estado']==1) ///////// SOLO FUNCIONARIOS 
			                                      	{

			                                       echo '<tr>';
			                                       	echo '<td>';
			                                       	if($this->model_componente->nro_fase($row['proy_id'])!=0) ///// verificando que tenga fase etapa registrados
			                                        {
			                                        	if($this->model_componente->verif_fase($row['proy_id'])!=0) //// verificando que tenga fase activa
			                                        	{ 
			                                        		$data['fase'] = $this->model_componente->get_id_fase($row['proy_id']);
			                                        		$c=0;$p=0;$a=0;
			                                        		if($this->model_componente->nro_componentes_id($data['fase'][0]['id'])!=0) //// si tiene nro de componentes
			                                        		{ $c=1;
			                                        			$data['comp'] = $this->model_componente->componentes_id($data['fase'][0]['id']); //// sacando componentes
			                                        			
			                                        			foreach($data['comp'] as $rowc)
			                                      				{
			                                      					if($this->model_producto->nro_productos_gest($rowc['com_id'])!=0) //// si tiene productos
			                                        				{  $p=1;
			                                        					$data['prod'] = $this->model_producto->productos_id($rowc['com_id']); //// sacando productos
			                                        					foreach($data['prod'] as $rowp)
			                                      						{
			                                      							if($this->model_actividad->nro_actividades_gest($rowp['prod_id'])!=0) //// si tiene actividades
			                                        						{
			                                        							$a=1;
			                                        						}
			                                      						}
			                                        				}			                                        		

			                                      				} 
			                                        		}
			                                        		if($c==1)
			                                        		{
			                                        			if($p==1)
			                                        			{
			                                        				if($a==1)////// yes
			                                        				{
			                                        					?><style type="text/css">#graf1{font-size: 35px;}</style> 
			                                        					<a href='<?php echo site_url("admin").'/proy/asig_vpoa/'.$row['proy_id']; ?>' class="btn bg-color-blue txt-color-white" onclick="return confirm('ASIGNAR EL PROYECTO A : <?php  echo $row['funcionario2'];?> VALIDADOR POA ?');" title="ASIGNAR A VALIDADOR POA"><i class="glyphicon glyphicon-fast-backward" id="graf1"></i></a>
			                                        					<?php
			                                        				}
			                                        				elseif ($a==0)
			                                        				{
			                                        					?>
							                                        	<a href="#" class="btn bg-color-blue txt-color-white" title="NO TIENE ACTIVIDADE REGISTRADO"><i class="fa fa-gear fa-4x fa-spin" id="graf1"></i></a>
							                                        	<?php
			                                        				}
			                                        			}
			                                        			elseif ($p==0)
			                                        			{
			                                        				?>
						                                        	<a href="#" class="btn bg-color-blue txt-color-white" title="NO TIENE PRODUCTO REGISTRADO"><i class="fa fa-gear fa-4x fa-spin" id="graf1"></i></a>
						                                        	<?php
			                                        			}
			                                        		}
				                                        	elseif ($c==0)
				                                        	{
				                                        		?>
					                                        	<a href="#" class="btn bg-color-blue txt-color-white" title="NO TIENE COMPONENTE REGISTRADO"><i class="fa fa-gear fa-4x fa-spin" id="graf1"></i></a>
					                                        	<?php
				                                        	}

			                                        	}
			                                        	elseif ($this->model_componente->verif_fase($row['proy_id'])==0) //// no tiene fase activa
			                                        	{
			                                        		?>
			                                        		<a href="#" class="btn bg-color-blue txt-color-white" title="NO TIENE ACTIVADO LA FASE"><i class="fa fa-gear fa-4x fa-spin" id="graf1"></i></a>
			                                        		<?php
			                                        	}
			                                        }
			                                        elseif ($this->model_componente->nro_fase($row['proy_id'])==0) ///// no tiene fases registrados
			                                        {
			                                        	?>
			                                        	<a href="#" class="btn bg-color-red txt-color-white" title="NO TIENE FASE ESTAPA REGISTRADO"><i class="fa fa-gear fa-4x fa-spin"></i></a>
			                                        	<?php
			                                        }
			                                       	echo '</td>'; 
			                                        echo '<td><font size="1">'.$row['proy_codigo'].'</font></td>';
			                                        echo '<td><font size="1">'.$row['aper_programa'].''.$row['aper_proyecto'].''.$row['aper_actividad'].'</font></td>';
			                                        
			                                        echo '<td><font size="1">'.$row['proy_nombre'].'</font></td>';
			                                        echo '<td><font size="1">'.$row['proy_sisin'].'</font></td>';
			                                        echo '<td><font size="1">'.$row['funcionario1'].'</font></td>';
			                                        echo '<td><font size="1">'.$row['unidad'].'</font></td>';
			                        
			                                        
			                                        if($this->model_componente->verif_fase($row['proy_id'])!=0) //// fase activa
			                                        	{
			                                        		$data['fase'] = $this->model_componente->get_id_fase($row['proy_id']); 
			                                        		echo '<td><font size="1">- '.$data['fase'][0]['fase'].'<br>- '.$data['fase'][0]['etapa'].'</font></td>';
			                                        		if($data['fase'][0]['pfec_fecha_inicio']==$this->session->userdata("gestion")){
			                                        			echo '<td><font size="1">NUEVO</font></td>';
			                                        		}
			                                        		elseif ($data['fase'][0]['pfec_fecha_inicio']!=$this->session->userdata("gestion")) {
			                                        			echo '<td><font size="1">CONTINUIDAD</font></td>';
			                                        		}
			                                        		
			                                        		if($data['fase'][0]['pfec_fecha_fin']-$data['fase'][0]['pfec_fecha_inicio']==0){
			                                        			echo '<td><font size="1">ANUAL</font></td>';
			                                        		}
			                                        		elseif ($data['fase'][0]['pfec_fecha_fin']-$data['fase'][0]['pfec_fecha_inicio']!=0) {
			                                        			echo '<td><font size="1">PLURI ANUAL</font></td>';
			                                        		}

			                                        		echo '<td><font size="1">'.number_format($data['fase'][0]['pfec_gestion']).' Bs.</font></td>';
			                                        		$data['fase_gest'] = $this->model_componente->fase_gestion($data['fase'][0]['id'],$this->session->userdata("gestion")); //fase gestion vigente
			                                        		echo '<td><font size="1">'.number_format($data['fase_gest'][0]['pfecg_ppto_total']).' Bs.</font></td>';
			                                        		
			                                        	}
			                                        elseif ($this->model_componente->verif_fase($row['proy_id'])==0) {

			                                        	echo '<td><font size="1" color="red">N/R</font></td>';
			                                        	echo '<td><font size="1" color="red"> N/R</font></td>';
			                                        	echo '<td><font size="1" color="red">N/R</font></td>';
			                                        	echo '<td><font size="1" color="red"> N/R</font></td>';
			                                        	echo '<td><font size="1" color="red"> N/R</font></td>';

			                                        }
			                                        ?>
			                                        <!-- ================================================================================ UBICACION =========================================================================== --> 
			                                        <?php
			                                         echo '<td><font size="1">';
					                                        echo '<table class="table table-bordered" border="1">
					                                        		<thead>';
																		$nro_prov= $this->model_proyecto->nro_proy_prov($row['proy_id']);
																		if($nro_prov!=0)
																		{
																			$prov= $this->model_proyecto->proy_prov($row['proy_id']);
																			foreach ($prov as $rowp)
										                                    {
										                                    	echo '<tr><td bgcolor="#696969"><center><b><font color="#ffffff">'.strtoupper($rowp['prov_provincia']).'</font></b></center></td></tr>';
										                                    	$nro_mun= $this->model_proyecto->nro_proy_mun($row['proy_id']);
										                                    	echo '<tr><td>';
										                                    	if($nro_mun!=0)
										                                    	{	$muni= $this->model_proyecto->proy_mun($row['proy_id']);
										                                    		echo '<table >';
										                                    		foreach ($muni as $rowm)
													                                {
													                                    if($rowm['prov_id']==$rowp['prov_id'])
													                                    {
													                                    	echo '<tr><td> - '.$rowm['muni_municipio'].'</td></tr>';
													                                    }
													                                }
													                                echo '</table>';
										                                    	}
										                                    	elseif ($nro_mun==0) 
										                                    	{
										                                    		echo "<font color='blue'>No tiene municipios</font>";
										                                    	}
										                                    	echo'</td></tr>';

										                                    }
																		}
							                                 			elseif($nro_prov==0) 
							                                 			{
								                                 			echo '<td>';
								                                 			?>
								                                 			<center><img src="<?php echo base_url(); ?>assets/Iconos/map_delete.png" WIDTH="30" HEIGHT="30"/></center>
								                                 			<?php
								                                 			echo '</td>';
							                                 			}
		                            							echo '</thead>
		                            							</table>';
					                                        echo'</font></td>';
					                                       
			                                        ?>
			                                        <!-- ================================================================================ END UBICACION =========================================================================== -->
			                                        <td>
																	<div class="btnapp">
                                                                        <div class="hover-btn">
                                                                        	<a href='#' class="btn btn-xs delete botones uno" name="<?php echo $row['proy_id']?>" id="1" title="ELIMINAR PROYECTO">
                                                                                <div class="btn-hover-postion1">
                                                                                 <span class="glyphicon glyphicon-trash"></span>
                                                                                </div>
                                                                            </a>
                                                                            <a href="<?php echo site_url("admin").'/proy/edit_proy/'.$row['proy_id']; ?>" class="btn btn-xs  botones dos" title="MODIFICAR">
                                                                                <div class="btn-hover-postion2">
                                                                                 <span class="glyphicon glyphicon-pencil"></span>
                                                                                </div>
                                                                            </a>
                                                                            <a href="javascript:abreVentana('<?php echo site_url("admin").'/proy/ficha_tecnica/'.$row['proy_id']; ?>');" class="btn btn-xs  botones tres" title="FICHA TECNICA - PDF">
                                                                                <div class="btn-hover-postion3">
                                                                                 <i class="fa fa-file-pdf-o" aria-hidden="true"></i> 
                                                                                </div>
                                                                            </a>
                                                                             

                                                                        </div>
                                                                    </div>
														</td>
														<!-- <td><a href="<?php echo base_url().'index.php/admin/proy/ubicacion/'.$row['proy_id'].'/3?qRegistro='.$row['proy_id'].'' ?>" title="LOCALIZACION"><span class="glyphicon glyphicon-globe" aria-hidden="true"></span></i></a></td> -->

			                                          	
			                                           
			                                           <?php
			                                           if($this->model_componente->verif_fase($row['proy_id'])!=0) ////// tiene fase activa
			                                        	{ $data['fase'] = $this->model_componente->get_id_fase($row['proy_id']); 
			                                        		?>
			                                        		
			                                        		<?php
			                                        	}
			                                        	elseif($this->model_componente->verif_fase($row['proy_id'])==0) //// no tiene fase activa
			                                        	{
			                                        		?>
			                                        		
			                                        		<?php
			                                        	}
			                                      echo '</tr>';
			                                      }
			                                      }
			                                     ?>
											</tbody>
										</table>
										</div>

									</div>
									<!-- end widget content -->
				
								</div>
								<!-- end widget div -->
				
							</div>
							<br>
							<!-- end widget -->
				
						</article>
						<!-- WIDGET END -->
					</div>
				</section>

				
				<!--fin de proyectos-->
						
					</div>
				
				</div>
				
				<!-- end row -->

			</div>
			<!-- END MAIN CONTENT -->

		</div>
		<!-- END MAIN PANEL -->

		<!-- PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white">COCAE © SISTEMA INTEGRAL DE PLANIFICACIÓN DE PROYECTOS</span>
				</div>

				<div class="col-xs-6 col-sm-6 text-right hidden-xs">
					<div class="txt-color-white inline-block">
						<i class="txt-color-blueLight hidden-mobile">Última actividad de la cuenta  <i class="fa fa-clock-o"></i> <strong>52 mins ago &nbsp;</strong> </i>
						<div class="btn-group dropup">
							<button class="btn btn-xs dropdown-toggle bg-color-blue txt-color-white" data-toggle="dropdown">
								<i class="fa fa-link"></i> <span class="caret"></span>
							</button>
							<ul class="dropdown-menu pull-right text-left">
								<li>
									<div class="padding-5">
										<p class="txt-color-darken font-sm no-margin">Download Progress</p>
										<div class="progress progress-micro no-margin">
											<div class="progress-bar progress-bar-success" style="width: 50%;"></div>
										</div>
									</div>
								</li>
								<li class="divider"></li>
								<li>
									<div class="padding-5">
										<p class="txt-color-darken font-sm no-margin">Server Load</p>
										<div class="progress progress-micro no-margin">
											<div class="progress-bar progress-bar-success" style="width: 20%;"></div>
										</div>
									</div>
								</li>
								<li class="divider"></li>
								<li>
									<div class="padding-5">
										<p class="txt-color-darken font-sm no-margin">Memory Load <span class="text-danger">*critical*</span></p>
										<div class="progress progress-micro no-margin">
											<div class="progress-bar progress-bar-danger" style="width: 70%;"></div>
										</div>
									</div>
								</li>
								<li class="divider"></li>
								<li>
									<div class="padding-5">
										<button class="btn btn-block btn-default">refresh</button>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
			<!-- PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userdata('name')?> <?php echo $this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->

		<!--================================================== -->

		<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
		<script data-pace-options='{ "restartOnRequestAfter": true }' src="<?php echo base_url(); ?>assets/js/plugin/pace/pace.min.js"></script>

		<script>
			if (!window.jQuery) {
				document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-2.0.2.min.js"><\/script>');
			}
		</script>

		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>

		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url(); ?>assets/js/app.config.js"></script>
		

		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 

		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.min.js"></script>

		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url(); ?>assets/js/notification/SmartNotification.min.js"></script>

		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url(); ?>assets/js/smartwidgets/jarvis.widget.min.js"></script>

		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

		<!-- SPARKLINES -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>

		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>

		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>

		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/select2/select2.min.js"></script>

		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>

		<!-- browser msie issue fix -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>

		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/fastclick/fastclick.min.js"></script>

		<!--[if IE 8]>

		<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

		<![endif]-->

		<!-- Demo purpose only -->
		<script src="<?php echo base_url(); ?>assets/js/demo.min.js"></script>

		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url(); ?>assets/js/app.min.js"></script>

		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<!-- Voice command : plugin -->
		<script src="<?php echo base_url(); ?>assets/js/speech/voicecommand.min.js"></script>


		<!-- PAGE RELATED PLUGIN(S) -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.colVis.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.tableTools.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>

		<script type="text/javascript">
		function reset() {
		    $("#toggleCSS").attr("href", base_url + "assets/themes_alerta/alertify.default.css");
		    alertify.set({
		        labels: {
		            ok: "ACEPTAR",
		            cancel: "CANCELAR"
		        },
		        delay: 5000,
		        buttonReverse: false,
		        buttonFocus: "ok"
		    });
		}
		$(function() {

		$(".delete").click(function(){
	
			var name = $(this).attr('name');
			var tip = $(this).attr('id');
			var request;
			if(confirm("DESEA ELIMINAR EL PROYECTO SELECCIONADO ?"))
			{
			$.ajax({
			type: "POST",
			url: "<?php echo site_url("admin")?>/proy/delet_proy", 
			data:{id_p:name,tipo:tip},
			dataType: 'json',
			success:function(datos){
				
				if (datos==true) {
	                    window.location.reload(true);
	                } 
	            else { 
	                  alert("EL PROYECTO NOSE PUEDE ELIMINAR, TIENE DEPENDENCIAS")
	                }
			}
			});
			
			}
			return false;
			});
		});
		// DO NOT REMOVE : GLOBAL FUNCTIONS!
		
		$(document).ready(function() {
			
			pageSetUp();
	
			/* BASIC ;*/
				var responsiveHelper_dt_basic = undefined;
				var responsiveHelper_datatable_fixed_column = undefined;
				var responsiveHelper_datatable_col_reorder = undefined;
				var responsiveHelper_datatable_tabletools = undefined;
				
				var breakpointDefinition = {
					tablet : 1024,
					phone : 480
				};
	
				$('#dt_basic').dataTable({
					"ordering": false,
					"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
					"autoWidth" : true,
					"preDrawCallback" : function() {
						// Initialize the responsive datatables helper once.
						if (!responsiveHelper_dt_basic) {
							responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
						}
					},
					"rowCallback" : function(nRow) {
						responsiveHelper_dt_basic.createExpandIcon(nRow);
					},
					"drawCallback" : function(oSettings) {
						responsiveHelper_dt_basic.respond();
					}
				});
				$('#dt_basic1').dataTable({
					"ordering": false,
					"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
					"autoWidth" : true,
					"preDrawCallback" : function() {
						// Initialize the responsive datatables helper once.
						if (!responsiveHelper_dt_basic) {
							responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic1'), breakpointDefinition);
						}
					},
					"rowCallback" : function(nRow) {
						responsiveHelper_dt_basic.createExpandIcon(nRow);
					},
					"drawCallback" : function(oSettings) {
						responsiveHelper_dt_basic.respond();
					}
				});
				$('#dt_basic3').dataTable({
					"ordering": false,
					"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
					"autoWidth" : true,
					"preDrawCallback" : function() {
						// Initialize the responsive datatables helper once.
						if (!responsiveHelper_dt_basic) {
							responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic4'), breakpointDefinition);
						}
					},
					"rowCallback" : function(nRow) {
						responsiveHelper_dt_basic.createExpandIcon(nRow);
					},
					"drawCallback" : function(oSettings) {
						responsiveHelper_dt_basic.respond();
					}
				});
				$('#dt_basic2').dataTable({
					"ordering": false,
					"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
					"autoWidth" : true,
					"preDrawCallback" : function() {
						// Initialize the responsive datatables helper once.
						if (!responsiveHelper_dt_basic) {
							responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic4'), breakpointDefinition);
						}
					},
					"rowCallback" : function(nRow) {
						responsiveHelper_dt_basic.createExpandIcon(nRow);
					},
					"drawCallback" : function(oSettings) {
						responsiveHelper_dt_basic.respond();
					}
				});
		})

		</script>

	</body>

</html>

