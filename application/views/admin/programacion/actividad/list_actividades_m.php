<!DOCTYPE html>
<html lang="en-us">
  <head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    <title><?php echo $this->session->userdata('name')?></title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- Basic Styles -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
    <!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
    <!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
    <!-- FAVICONS -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.core.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css" id="toggleCSS"/>
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
    <script>
    function confirmar()
    {
      if(confirm('¿Estas seguro de Eliminar?'))
        return true;
      else
        return false;
    }
    </script>
    <style type="text/css">
      aside{background: #05678B;}
    </style>
    <style type="text/css">
      table{font-size: 10px;
        width: 100%;
        max-width:1550px;;
        overflow-x: scroll;
        }
      th{
          padding: 1.4px;
          text-align: center;
          font-size: 10px;
        }
    </style>
  </head>
  <body class="">
    <!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->
    <!-- HEADER -->
    <header id="header">
      <!-- pulled right: nav area -->
      <div class="pull-right">
        <!-- collapse menu button -->
        <div id="hide-menu" class="btn-header pull-right">
          <span> <a href="javascript:void(0);" data-action="toggleMenu" title="Menu"><i class="fa fa-reorder"></i></a> </span>
        </div>
        <!-- end collapse menu -->
        <!-- logout button -->
        <div id="logout" class="btn-header transparent pull-right">
          <span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Salir" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
        </div>
        <!-- end logout button -->
        <!-- search mobile button (this is hidden till mobile view port) -->
        <div id="search-mobile" class="btn-header transparent pull-right">
          <span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
        </div>
        <!-- end search mobile button -->
        <!-- fullscreen button -->
        <div id="fullscreen" class="btn-header transparent pull-right">
          <span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Pantalla Completa"><i class="fa fa-arrows-alt"></i></a> </span>
        </div>
        <!-- end fullscreen button -->
      </div>
      <!-- end pulled right: nav area -->
    </header>
    <!-- END HEADER -->
    <!-- Left panel : Navigation area -->
    <aside id="left-panel">
      <!-- User info -->
      <div class="login-info">
        <span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
          <a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
              <span>
                  <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
              </span>
          </a> 
        </span>
      </div>

      <nav>
        <ul>
          <li>
            <a href='<?php echo site_url("admin").'/dashboard'; ?>' title="MENU PRINCIPAL"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
          </li>
          <li class="text-center">
              <a href="<?php echo base_url().'index.php/admin/proy/mis_proyectos/'.$mod ?>" title="PROGRAMACION -> MIS PROYECTOS"> <span class="menu-item-parent">PROGRAMACI&Oacute;N F&Iacute;SICA</span></a>
          </li>
          <?php
          if($nro_fase==1){
              for($i=0;$i<count($enlaces);$i++)
              {
                ?>
                 <li>
                        <a href="#" >
                          <i class="<?php echo $enlaces[$i]['o_image'];?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
                        <ul >
                        <?php
                        $submenu= $this->menu_modelo->get_Modulos_sub($enlaces[$i]['o_child']);
                    foreach($submenu as $row) {
                        ?>
                        <li><a href="<?php echo base_url($row['o_url'])."/".$mod."/".$id_f[0]['id']."/".$id_f[0]['proy_id']; ?>"><?php echo $row['o_titulo']; ?></a></li>
                        <?php } ?>
                        </ul>
                  </li>
                <?php
              }
          }
          ?>
        </ul>
      </nav>
      <span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>
    </aside>

    <!-- MAIN PANEL -->
    <div id="main" role="main">
      <!-- RIBBON -->
      <div id="ribbon">
        <!-- breadcrumb -->
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url().'index.php/admin/sgp/list_proy' ?>" title="VOLVER A MIS PROYECTOS">Gerencia de Proyectos</a></li><li><a href="<?php echo base_url().'index.php/admin/prog/list_prod/'.$mod.'/'.$id_f[0]['id']."/".$id_f[0]['proy_id'].'/'.$componente[0]['com_id']; ?>" title="PRODUCTOS">Mis Productos</a></li><li>Mis Actividades</li>                 
        </ol>
      </div>
      <!-- END RIBBON -->
        <!-- MAIN CONTENT -->
        <div id="content">
          <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <section id="widget-grid" class="well">
                <div class="">
                  <h1> <?php echo $titulo_proy;?> : <small><?php echo $proyecto[0]['proy_nombre']?></small></h1>
                  <h1> CATEGORIA PROGRAM&Aacute;TICA : <small><?php echo $proyecto[0]['aper_programa'].''.$proyecto[0]['aper_proyecto'].''.$proyecto[0]['aper_actividad']?></small>
                  <h1> COMPONENTE : <small><?php echo $componente[0]['com_componente']?></small>
                  <h1> PRODUCTO : <small><?php echo $producto[0]['prod_producto']?></small>
                </div>
            </section>
          </article>

        <section id="widget-grid" class="">
            <!-- row -->
            <div class="row">
              <!-- NEW WIDGET START -->
              <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php 
                  if($this->session->flashdata('success'))
                    { ?>
                    <div class="alert alert-success">
                      <?php echo $this->session->flashdata('success'); ?>
                    </div>
                <?php 
                    }
                  elseif($this->session->flashdata('danger'))
                  {
                    {
                      ?>
                      <div class="alert alert-danger">
                        <?php echo $this->session->flashdata('danger'); ?>
                      </div>
                      <?php
                    }
                  }
                ?>
                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget jarviswidget-color-darken" >
                  <header>
                      <span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
                      <h2 class="font-md"><strong>ACTIVIDADES DE LA OPERACI&Oacute;N <?php echo $id_f[0]['pfec_fecha_inicio'].' - '.$id_f[0]['pfec_fecha_fin'];?></strong></h2>  
                  </header>
                  <div>
                    <div class="widget-body no-padding">
                      <div class="table-responsive">
                        <table id="dt_basic" class="table table table-bordered" width="100%">
                          <thead>                             
                              <tr>
                                <th style="width:1%;"><center><a href='<?php echo site_url("admin").'/prog/new_act/'.$mod.'/'.$id_f[0]['id']."/".$id_f[0]['proy_id'].'/'.$componente[0]['com_id'].'/'.$producto[0]['prod_id']; ?>' title="NUEVO DE REGISTRO ACTIVIDADES "><img src="<?php echo base_url(); ?>assets/ifinal/2.png" WIDTH="40" HEIGHT="40"/><BR>REGISTRO DE ACTIVIDADES</a></center></th>
                                <th style="width:1%;"><b>E/B</b></th>
                                <th style="width:20%;"><b>&nbsp;&nbsp;ACTIVIDAD_HITO&nbsp;&nbsp;</b></th>
                                <th style="width:10%;"><b>DEPENDENCIA</b></th>
                                <th style="width:5%;"><b>DURACI&Oacute;N</b></th>
                                <th style="width:5%;"><b>&nbsp;COSTO_ACTIVIDAD&nbsp;</b></th>
                                <th style="width:5%;"><b>COSTO_UNITARIO</b></th>
                                <th style="width:5%;"><b>COSTO_PONDERADO</b></th>
                                <th style="width:5%;"><b>PONDERACI&Oacute;N</b></th>
                                <th style="width:10%;"><b>FECHA_INICIAL</b></th>
                                <th style="width:10%;"><b>FECHA_CONCLUSI&Oacute;N</b></th>
                                <th style="width:5%;"><b>TIPO DE INDICADOR</b></th>
                                <th style="width:5%;"><b>&nbsp;INDICADOR&nbsp;</b></th>
                                <th style="width:5%;"><b>&nbsp;LINEA/BASE&nbsp;</b></th>
                                <th style="width:5%;"><b>META TOTAL</b></th>
                                <th style="width:15%;"><b>CRONOGRAMA DE PROGRAMACI&Oacute;N MULTI-ANUAL</b></th>
                              </tr>
                            </thead>
                          <tbody id="bdi">
                          <?php 
                              $años=$id_f[0]['pfec_fecha_fin']-$id_f[0]['pfec_fecha_inicio']+1;
                              $total = $this->model_actividad->suma_monto_ponderado_total($id_pr); //// suma tota del presupuesto
                              if($total[0]['monto_total']==0){$total[0]['monto_total']=1;}
                              $suma_presupuesto=0;$suma_costo=0;$suma_costo_unitario=0;
                              foreach($act as $rowa)
                              { 
                                $color="";
                                if(count($this->model_actividad->get_programado_actividad($rowa['act_id']))==0){$color="#fbbfbf";}
                                $ti='';if($rowa['indi_id']==2){ $ti='%';}
                                if($rowa['act_meta']==0){$rowa['act_meta']=1;}
                                echo '<tr bgcolor='.$color.'>';
                                echo '<td><center><b>'.$rowa['nro_act'].'</b></center></td>';
                                ?>
                                <td style="width:5%;">
                                  <center>
                                    <a  href='<?php echo site_url("admin").'/prog/mod_act/'.$mod.'/'.$id_f[0]['id']."/".$id_f[0]['proy_id'].'/'.$componente[0]['com_id'].'/'.$producto[0]['prod_id'].'/'.$rowa['act_id']; ?>' title="MODIFICAR">
                                      <img src="<?php echo base_url(); ?>assets/ifinal/modificar.png" WIDTH="35" HEIGHT="35"/>
                                    </a>MODIFICAR<br>
                                    <a href='<?php echo site_url("admin").'/prog/delete_act/'.$mod.'/'.$id_f[0]['id']."/".$id_f[0]['proy_id'].'/'.$componente[0]['com_id'].'/'.$producto[0]['prod_id'].'/'.$rowa['act_id']; ?>' title="ELIMINAR ACTIVIDAD" onclick="return confirmar()"><img src="<?php echo base_url(); ?>assets/ifinal/eliminar.png" WIDTH="35" HEIGHT="35"/><br>ELIMINAR</a><br>
                                  </center> 
                                </td>
                                <?php
                                  echo '<td>'.$rowa['act_actividad'].'</td>';
                                  echo '<td>'.$rowa['act_precedente'].'</td>';
                                  echo '<td>'.$rowa['act_duracion'].' d/c</td>';
                                  echo '<td>'.number_format($rowa['act_presupuesto'], 2, ',', ' ').' Bs.</td>';
                                  echo '<td>'.number_format($rowa['act_presupuesto']/$rowa['act_meta'], 2, ',', ' ').'Bs.</td>';
                                  echo '<td>'.number_format($rowa['act_pres_p'], 2, ',', ' ').'Bs.</td>';
                                  echo '<td>'.$rowa['act_ponderacion'].' %</td>';
                                  echo '<td>'.date('Y-m-d',strtotime($rowa['act_fecha_inicio'])).'</td>';
                                  echo '<td>'.date('Y-m-d',strtotime($rowa['act_fecha_final'])).'</td>';
                                  echo '<td>'.$rowa['indi_abreviacion'].'</td>';
                                  echo '<td>'.$rowa['act_indicador'].'</td>';
                                  echo '<td>'.$rowa['act_linea_base'].'</td>';
                                  echo '<td>'.$rowa['act_meta'].'</td>';
                                  echo '<td>';
                                                $pa=0; $gestion=$id_f[0]['pfec_fecha_inicio'];
                                                for($k=1;$k<=$años;$k++)
                                                { 
                                                  $act_gest=$this->model_actividad->act_prog_mensual($rowa['act_id'],$gestion);
                                                  $nro=0;
                                                  foreach($act_gest as $row)
                                                  {
                                                    $nro++;
                                                    $matriz [1][$nro]=$row['m_id'];
                                                    $matriz [2][$nro]=$row['pg_fis'];
                                                    $matriz [3][$nro]=$row['pg_fin'];
                                                  }

                                                  for($j = 1 ;$j<=12 ;$j++)
                                                  {
                                                      $matriz_r[1][$j]=$j;
                                                      $matriz_r[2][$j]='0';
                                                      $matriz_r[3][$j]='0';
                                                      $matriz_r[4][$j]='0';
                                                      $matriz_r[5][$j]='0';
                                                  }

                                                  for($i = 1 ;$i<=$nro ;$i++)
                                                  {
                                                    for($j = 1 ;$j<=12 ;$j++)
                                                    {
                                                      if($matriz[1][$i]==$matriz_r[1][$j])
                                                      {
                                                        $matriz_r[2][$j]=round($matriz[2][$i],2);
                                                        $matriz_r[5][$j]=round($matriz[3][$i],2);
                                                        }
                                                    }
                                                  }

                                                  for($j = 1 ;$j<=12 ;$j++){
                                                    $pa=$pa+$matriz_r[2][$j];
                                                    $matriz_r[3][$j]=$pa+$rowa['act_linea_base'];
                                                    $matriz_r[4][$j]=round(((($pa+$rowa['act_linea_base'])/$rowa['act_meta'])*100),2);
                                                  }
                                                echo '<table class="table table-bordered">
                                                            <thead>                        
                                                                <tr>
                                                                    <td style="width:1%;" bgcolor="#008080" colspan=13><font color="#ffffff">GESTI&Oacute;N '.$gestion.'</font></td>
                                                                </tr>
                                                                <tr bgcolor="#F5F5DC">
                                                                    <th style="width:1%;">PROGRAMACI&Oacute;N</th>
                                                                    <th style="width:1%;"><center>ENERO</center></th>
                                                                    <th style="width:1%;"><center>FEBRERO</center></th>
                                                                    <th style="width:1%;"><center>MARZO</center></th>
                                                                    <th style="width:1%;"><center>ABRIL.</center></th>
                                                                    <th style="width:1%;"><center>MAYO</center></th>
                                                                    <th style="width:1%;"><center>JUNIO</center></th>
                                                                    <th style="width:1%;"><center>JULIO</center></th>
                                                                    <th style="width:1%;"><center>AGOSTO</center></th>
                                                                    <th style="width:1%;"><center>SEPTIEMBRE</center></th>
                                                                    <th style="width:1%;"><center>OCTUBRE</center></th>
                                                                    <th style="width:1%;"><center>NOVIEMBRE</center></th>
                                                                    <th style="width:1%;"><center>DICIEMBRE</center></th>
                                                                </tr>
                                                                <tr bgcolor="#F5F5DC">
                                                                    <th style="width:1%;">FISICA</th>
                                                                    <th ><center>'.$matriz_r[2][1].'</center></th>
                                                                    <th ><center>'.$matriz_r[2][2].'</center></th>
                                                                    <th ><center>'.$matriz_r[2][3].'</center></th>
                                                                    <th ><center>'.$matriz_r[2][4].'</center></th>
                                                                    <th ><center>'.$matriz_r[2][5].'</center></th>
                                                                    <th ><center>'.$matriz_r[2][6].'</center></th>
                                                                    <th ><center>'.$matriz_r[2][7].'</center></th>
                                                                    <th ><center>'.$matriz_r[2][8].'</center></th>
                                                                    <th ><center>'.$matriz_r[2][9].'</center></th>
                                                                    <th ><center>'.$matriz_r[2][10].'</center></th>
                                                                    <th ><center>'.$matriz_r[2][11].'</center></th>
                                                                    <th ><center>'.$matriz_r[2][12].'</center></th>
                                                                </tr>
                                                                <tr bgcolor="#F5F5DC">
                                                                  <th style="width:1%;">FINANCIERA</th>
                                                                  <th ><center>'.number_format($matriz_r[5][1], 2, ',', ' ').'</center></th>
                                                                  <th ><center>'.number_format($matriz_r[5][2], 2, ',', ' ').'</center></th>
                                                                  <th ><center>'.number_format($matriz_r[5][3], 2, ',', ' ').'</center></th>
                                                                  <th ><center>'.number_format($matriz_r[5][4], 2, ',', ' ').'</center></th>
                                                                  <th ><center>'.number_format($matriz_r[5][5], 2, ',', ' ').'</center></th>
                                                                  <th ><center>'.number_format($matriz_r[5][6], 2, ',', ' ').'</center></th>
                                                                  <th ><center>'.number_format($matriz_r[5][7], 2, ',', ' ').'</center></th>
                                                                  <th ><center>'.number_format($matriz_r[5][8], 2, ',', ' ').'</center></th>
                                                                  <th ><center>'.number_format($matriz_r[5][9], 2, ',', ' ').'</center></th>
                                                                  <th ><center>'.number_format($matriz_r[5][10], 2, ',', ' ').'</center></th>
                                                                  <th ><center>'.number_format($matriz_r[5][11], 2, ',', ' ').'</center></th>
                                                                  <th ><center>'.number_format($matriz_r[5][12], 2, ',', ' ').'</center></th>
                                                                </tr>
                                                  </thead>';
                                                  echo'</table><br>';
                                                  $gestion++;
                                                }
                                                echo'</th>';
                                echo '</tr>';
                                $suma_presupuesto=$suma_presupuesto+$rowa['act_pres_p'];
                                $suma_costo=$suma_costo+$rowa['act_presupuesto'];
                                $suma_costo_unitario=$suma_costo_unitario+($rowa['act_presupuesto']/$rowa['act_meta']);
                                }
                                ?>
                                <tr>
                                  <td style="width:1%;"></td>
                                  <td style="width:1%;"><td>
                                  <td style="width:20%;"></td>
                                  <td style="width:10%;"></td>
                                  <td style="width:5%;"><font size="1"><b><?php echo number_format($suma_costo, 2, ',', ' '); ?> Bs.</b></font></td>
                                  <td style="width:5%;"><font size="1"><b><?php echo number_format($suma_costo_unitario, 2, ',', ' '); ?> Bs.</b></font></td>
                                  <td style="width:5%;"><font size="1"><b><?php echo number_format($suma_presupuesto, 2, ',', ' '); ?> Bs.</b></font></td>
                                  <td style="width:5%;"><center><?php echo $suma_presupuesto/$total[0]['monto_total']; ?></center></td>
                                  <td style="width:5%;"></td>
                                  <td style="width:10%;"></td>
                                  <td style="width:10%;"></td>
                                  <td style="width:5%;"></td>
                                  <td style="width:5%;"></td>
                                  <td style="width:5%;"></td>
                                  <td style="width:5%;"></td>
                                  <td style="width:5%;"></td>
                                </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <!-- end widget content -->
                  </div>
                  <div class="form-actions">
                   <a href="<?php echo base_url().'index.php/admin/prog/list_prod/'.$mod.'/'.$id_f[0]['id']."/".$id_f[0]['proy_id'].'/'.$componente[0]['com_id']; ?>" title="MIS PRODUCTOS" class="btn btn-lg btn-default" title="Volver atras">ATRAS</a>
                  </div>
                  <!-- end widget div -->
                </div>
                <!-- end widget -->
              </article>
              <!-- WIDGET END -->
            </div>
          </section>
        <!--///////////fin de tabla///////-->
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN PANEL -->
    </div>

<!-- PAGE FOOTER -->
<div class="page-footer">
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
        </div>
    </div>
</div>
<!-- END PAGE FOOTER -->
<!--================================================== -->

<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
<script data-pace-options='{ "restartOnRequestAfter": true }'
        src="<?php echo base_url(); ?>assets/js/plugin/pace/pace.min.js"></script>
<script>
    if (!window.jQuery) {
        document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-2.0.2.min.js"><\/script>');
    }
</script>
<script>
    if (!window.jQuery.ui) {
        document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
    }
</script>
<!-- IMPORTANT: APP CONFIG -->
<script src="<?php echo base_url(); ?>assets/js/session_time/jquery-idletimer.js"></script>
<script src = "<?php echo base_url(); ?>mis_js/control_session.js"></script>
<script src="<?php echo base_url(); ?>assets/js/app.config.js"></script>

<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/tabla/jquery-1.12.3.js"></script>
<!--fin tablas-->
<!-- BOOTSTRAP JS -->
<script src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.min.js"></script>
<!-- CUSTOM NOTIFICATION -->
<script src="<?php echo base_url(); ?>assets/js/notification/SmartNotification.min.js"></script>
<!--  JARVIS WIDGETS -->
<script src="<?php echo base_url(); ?>assets/js/smartwidgets/jarvis.widget.min.js"></script>
<!-- JQUERY VALIDATE -->
<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
<!-- JQUERY MASKED INPUT -->
<script src="<?php echo base_url(); ?>assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
<!-- JQUERY SELECT2 INPUT -->
<script src="<?php echo base_url(); ?>assets/js/plugin/select2/select2.min.js"></script>
<!-- JQUERY UI + Bootstrap Slider -->
<script src="<?php echo base_url(); ?>assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
<!-- browser msie issue fix -->
<script src="<?php echo base_url(); ?>assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
<!-- FastClick: For mobile devices -->
<script src="<?php echo base_url(); ?>assets/js/plugin/fastclick/fastclick.min.js"></script>
<!-- Demo purpose only -->
<script src="<?php echo base_url(); ?>assets/js/demo.min.js"></script>
<!-- MAIN APP JS FILE -->
<script src="<?php echo base_url(); ?>assets/js/app.min.js"></script>
<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
<!-- Voice command : plugin -->
<script src="<?php echo base_url(); ?>assets/js/speech/voicecommand.min.js"></script>
<!-- ///// mis validaciones js ///// -->
<SCRIPT src="<?php echo base_url(); ?>assets/js/mis_js/validacion_form.js" type="text/javascript"></SCRIPT>
<!--alertas -->
<script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
<!-- PAGE RELATED PLUGIN(S) -->
<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
      <script type="text/javascript">
          $(document).ready(function() {
             pageSetUp();
             /* BASIC ;*/
                var responsiveHelper_dt_basic = undefined;
                var responsiveHelper_datatable_fixed_column = undefined;
                var responsiveHelper_datatable_col_reorder = undefined;
                var responsiveHelper_datatable_tabletools = undefined;
                
                var breakpointDefinition = {
                     tablet : 1024,
                     phone : 480
                };
 
                $('#dt_basic').dataTable({
                     "ordering": false,
                     "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
                          "t"+
                          "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                     "autoWidth" : true,
                     "preDrawCallback" : function() {
                          // Initialize the responsive datatables helper once.
                          if (!responsiveHelper_dt_basic) {
                               responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                          }
                     },
                     "rowCallback" : function(nRow) {
                          responsiveHelper_dt_basic.createExpandIcon(nRow);
                     },
                     "drawCallback" : function(oSettings) {
                          responsiveHelper_dt_basic.respond();
                     }
                });
          })
    </script>
</body>
</html>
