<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
		<title><?php echo $this->session->userdata('name')?></title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.core.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css" id="toggleCSS" />
		<script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
        <style type="text/css">
            aside{background: #05678B;}
        </style>
    	<script>
		function confirmar()
		{
			if(confirm('¿Estas seguro de Eliminar el Insumo?'))
				return true;
			else
				return false;
		}
		</script>
    	<style type="text/css">
	    .table1{
          display: inline-block;
          width:100%;
          max-width:1550px;
          overflow-x: scroll;
          }
	    table {
	        font-size: 10px;
	        width: 100%;
	        max-width:1550px;;
	        overflow-x: scroll;
	        }
	    th{
	        padding: 1.4px;
	        text-align: center;
	        font-size: 10px;
	        }
        dl {width: 100%;}

        dt, dd {padding: 15px;}

        dt {
            background: #5C6C86;
            color: white;
            border-bottom: 1px solid #141414;
            border-top: 1px solid #4E4E4E;
            font: icon;
            align-content: center;
            cursor: pointer;
        }

        dd {
            background: #F5F5F5;
            line-height: 1.6em;
        }

        dt.activo, dt:hover {
            background: #008B8B;
        }

        dt:before {
            content: "+";
            margin-right: 20px;
            font-size: 20px;

        }

        dt.activo:before {
            content: "-";
            margin-right: 20px;
            font-size: 20px;
        }
	    </style>
	</head>
	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->
		<!-- HEADER -->
		<header id="header">
			<!-- pulled right: nav area -->
			<div class="pull-right">
				<!-- collapse menu button -->
				<div id="hide-menu" class="btn-header pull-right">
					<span> <a href="javascript:void(0);" data-action="toggleMenu" title="Menu"><i class="fa fa-reorder"></i></a> </span>
				</div>
				<!-- end collapse menu -->
				<!-- logout button -->
				<div id="logout" class="btn-header transparent pull-right">
					<span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Salir" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
				</div>
				<!-- end logout button -->
				<!-- search mobile button (this is hidden till mobile view port) -->
				<div id="search-mobile" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
				</div>
				<!-- end search mobile button -->
				<!-- fullscreen button -->
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Pantalla Completa"><i class="fa fa-arrows-alt"></i></a> </span>
				</div>
				<!-- end fullscreen button -->
			</div>
			<!-- end pulled right: nav area -->
		</header>
		<!-- END HEADER -->

		<!-- Left panel : Navigation area -->
		<aside id="left-panel">

			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                              <span>
                                  <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                              </span>
						<i class="fa fa-angle-down"></i>
					</a> 
				</span>
			</div>

			<nav>
                <ul>
                    <li class="">
                        <a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MENÚ PRINCIPAL"><i
                                    class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
                    </li>
                    <li class="text-center">
                        <?php
                        if($proyecto[0]['proy_estado']==1){ ?>
                            <a href="<?php echo base_url().'index.php/admin/proy/list_proy' ?>" title="MIS PROYECTOS"><span class="menu-item-parent">PROGRAMACI&Oacute;N DEL POA</span></a>
                            <?php
                        }
                        elseif ($proyecto[0]['proy_estado']==2){ ?>
                            <a href="<?php echo base_url().'index.php/admin/proy/list_proy_poa' ?>" title="MIS PROYECTOS"><span class="menu-item-parent">PROGRAMACI&Oacute;N DEL POA</span></a>
                            <?php
                        }
                        ?>
                    </li>
                    <?php
                    if($nro_fase==1){
                        for($i=0;$i<count($enlaces);$i++){ ?>
                            <li>
                                <a href="#" >
                                    <i class="<?php echo $enlaces[$i]['o_image'];?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
                                <ul>
                                    <?php
                                    $submenu= $this->menu_modelo->get_Modulos_sub($enlaces[$i]['o_child']);
                                    foreach($submenu as $row) {
                                        ?>
                                        <li><a href="<?php echo base_url($row['o_url'])."/".$mod."/".$id_f[0]['id']."/".$id_f[0]['proy_id']; ?>"><?php echo $row['o_titulo']; ?></a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                            <?php
                        }
                    }
                    ?>
                </ul>
			</nav>
			<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>

		</aside>

		<!-- MAIN PANEL -->
		<div id="main" role="main">
			<!-- RIBBON -->
			<div id="ribbon">
				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>
				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url().'index.php/admin/proy/list_proy' ?>" title="MIS OPERACIONES">Mis Operaciones</a></li><li>Programaci&oacute;n de Requerimientos - Nivel de Componentes</li><li>Insumo Actividad</li>
				</ol>
			</div>
			<!-- END RIBBON -->
			<!-- MAIN CONTENT -->
			<div id="content">
				<!-- widget grid -->
				<section id="widget-grid" class="">
					<!-- row -->
					<div class="row">
						<article class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
				            <section id="widget-grid" class="well">
				                <div class="">
				                  <h1> PROGRAMACI&Oacute;N DE REQUERIMIENTO A NIVEL DE COMPONENTES (DELEGADO)</h1>
                                    <h1> CATEGORIA PROGRAM&Aacute;TICA : <small><?php echo $proyecto[0]['aper_programa'].''.$proyecto[0]['aper_proyecto'].''.$proyecto[0]['aper_actividad']?></small></h1>
                                    <h1> <?php echo $titulo_proy;?> : <small><?php echo $proyecto[0]['proy_nombre']?></small>
                                    <h1> COMPONENTE : <small><?php echo $dato_com->com_componente?></small></h1>
				                </div>
				            </section>
				        </article>
				        <!--<article class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
				            <section id="widget-grid" class="well">
				              <style type="text/css">#graf{font-size: 80px;}</style> 
				              <center>
				                <div class="dropdown">
				                <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenu1" style="width:70%;" data-toggle="dropdown" aria-expanded="true">
				                  OPCIONES INSUMO
				                  <span class="caret"></span>
				                </button>
				                <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
				                  <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url().'index.php/admin/dashboard' ?>">SALIR A MENU PRINCIPAL</a></li>
				                  <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url().'index.php/admin/proy/operacion/'.$proyecto[0]['proy_id'].'/'.$fase[0]['id']; ?>">MI OPERACI&Oacute;N </a></li>
				                  <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url().'index.php/admin/proy/mis_proyectos/1'; ?>">LISTA DE OPERACIONES</a></li>
				                  <li role="presentation"><a  href="<?php echo base_url(); ?>assets/video/plantilla_requerimiento.xlsx" style="cursor: pointer;" download>DESCARGAR PLANTILLA REQUERIMIENTO</a></li>
				                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#" data-toggle="modal" data-target="#modal_importar"  title="IMPORTAR REQUERIMIENTOS">IMPORTAR REQUERIMIENTOS</a></li>
				                  <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url().'index.php/'.$atras.''; ?>">VOLVER ATRAS</a></li>
				                </ul>
				              </div>
				              </center>
				            </section>
				        </article>
-->
				        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			                <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false">
			                    <header>
			                        <span class="widget-icon"> <i class="fa fa-table"></i> </span>
			                        <h2>RECURSOS PROGRAMADOS DEL COMPONENTE</h2>
			                    </header>
			                    <div>
			                        <div class="widget-body">
			                            <div class="table-responsive">
			                                <table class="table table-bordered" width="100%">
			                                    <thead>
			                                    <tr style=" background:#646464;">
			                                        <th rowspan="2" style="text-align:center;padding: 2%;"><b style="color:#fff;">NRO</b>
			                                        </th>
			                                        <th colspan="2"><b style="color:#fff;"><center>FUENTE FINANCIAMIENTO</center></b></th>
			                                        <th colspan="2"><b style="color:#fff;"><center>ORGANISMO FINANCIADOR</center></b></th>
			                                        <th rowspan="2" style="text-align:center;padding: 2%;"><b style="color:#fff;"><center>PRESUPUESTO ASIGNADO <?php echo $this->session->userdata("gestion");?></center></b></th>
			                                        <th rowspan="2" style="text-align:center;padding: 2%;"><b style="color:#fff;"><center>PRESUPUESTO PROGRAMADO <?php echo $this->session->userdata("gestion");?></center></b></th>
			                                        <th rowspan="2" style="text-align:center;padding: 2%;"><b style="color:#fff;"><center>SALDO POR PROGRAMAR <?php echo $this->session->userdata("gestion");?></center></b></th>
			                                    </tr>
			                                    <tr style=" background:#646464;">
			                                        <th><b style="color:#fff;">C&Oacute;DIGO</b></th>
			                                        <th><b style="color:#fff;">DESCRIPCI&Oacute;N</b></th>
			                                        <th><b style="color:#fff;">C&Oacute;DIGO</b></th>
			                                        <th><b style="color:#fff;">DESCRIPCI&Oacute;N</b></th>
			                                    </tr>
			                                    </thead>
			                                    <tbody id="presupuesto">
			                                    <?php echo $tabla_fuentes;?>
			                                    </tbody>
			                                </table>
			                            </div>

			                            <div class="row text-align-right">
								            <div class="col-md-12"><h1> COSTO TOTAL PROGRAMADO VIGENTE: <kbd class=" primary"> <?php echo number_format($total_vigente, 2, ',', '.')?> Bs.</kbd></h1>
								            </div>
								        </div>
			                        </div>
			                    </div>
			                </div>
			            </article>

			            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			            	<?php 
			                  if($this->session->flashdata('success')){ ?>
			                    <div class="alert alert-success">
			                      	<?php echo $this->session->flashdata('success'); ?>
			                    </div>
			                    <script type="text/javascript">alertify.success("<?php echo '<font size=2>'.$this->session->flashdata('success').'</font>'; ?>")</script>
			                <?php 
			                    }
			                  elseif($this->session->flashdata('danger')){ ?>
			                      <div class="alert alert-danger">
			                        <?php echo $this->session->flashdata('danger'); ?>
			                      </div>
			                      <script type="text/javascript">alertify.error("<?php echo '<font size=2>'.$this->session->flashdata('danger').'</font>'; ?>")</script>
			                    <?php
			                  }
			                ?>
			                <div class="jarviswidget jarviswidget-color-black" id="wid-id-0" data-widget-editbutton="false">
			                    <header>
			                        <span class="widget-icon"> <span class="fa fa-table"></span></span>
			                        <h2>LISTA DE REQUERIMIENTOS</h2><div id="patito" class="col-xs-8 col-sm-1"><a class="btn btn-block btn-success" href="<?php echo site_url("prog/nuevo_ins_part_com").'/'.$proy_id.'/'.$com_id;?>" title="NUEVO REQUERIMIENTO"><i class="glyphicon glyphicon-plus"></i> NUEVO</a></div>
			                    </header>
			                    <div>
			                        <div class="widget-body no-padding">
			                            <table id="dt_basic" class="table table-bordered" width="100%">
		                                    <thead>
		                                    <tr>
		                                        <th> #</th>
                                                <th>ACCIONES</th>
                                                <th>PARTIDA</th>
                                                <th>DESCRIPCIÓN</th>
                                                <th>FF</th>
                                                <th>OF</th>
                                                <th>ET</th>
                                                <th>PRESUP. INICIAL</th>
                                                <th>PRESUP. MODIFICACIONES</th>
                                                <th>PRESUP. VIGENTE</th>
                                                <th>JUSTIFICACION</th>
		                                        <th>PROGRAMACIÓN ANUAL</th>
		                                    </tr>
		                                    </thead>
		                                    <tbody>
		                                    <?php echo $lista_insumos;?>
		                                    </tbody>
		                                </table>
			                        </div>
			                    </div>
			                </div>
			            </div>
			            </div>


			            

					</div>
				</section>
				<!-- end widget grid -->
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN PANEL -->
		<!-- PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->
		<!-- ================== MODAL SUBIR ARCHIVO ========================== -->
    	<div class="modal animated fadeInDown" id="modal_importar" tabindex="-1" role="dialog">
        <link href="<?php echo base_url(); ?>assets/file/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
        <script src="<?php echo base_url(); ?>assets/file/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/file/js/fileinput.min.js" type="text/javascript"></script> 
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body no-padding">
                    <div class="row">
                       <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="row">
                            <h2 class="row-seperator-header"><i class="glyphicon glyphicon-import"></i> IMPORTAR ARCHIVO (.CSV) </h2>
                            <div class="col-sm-12">
                              <!-- well -->
                              <div class="well">
                                <!-- row -->
                                <div class="row">
                                  <!-- col -->
                                  <div class="col-sm-12">
                                    <p class="alert alert-info">
                                      <i class="fa fa-info"></i> Por favor guardar el archivo (Excel.xls) a extension (.csv) delimitado por (; "Punto y comas"). verificar el archivo .csv para su correcta importaci&oacute;n
                                    </p>
                                    <!-- row -->
                                    <div class="row">
                                      <form action="<?php echo site_url() . '/insumos/cprog_insumos_directo/importar_archivo_requerimiento' ?>" method="post" enctype="multipart/form-data" id="form_subir_sigep" name="form_subir_sigep">
                                        <input type="hidden" id="proy_id" name="proy_id" value="<?php echo $proyecto[0]['proy_id'];?>" />
										<input type="hidden" id="pfec_id" name="pfec_id" value="<?php echo $fase[0]['id'];?>" />
										<input type="hidden" id="prod_id" name="prod_id" value="<?php echo $prod_id;?>" /> 
										<input type="hidden" id="act_id" name="act_id" value="<?php echo $act_id;?>" /> 
                                         <fieldset>
		                                    <section class="form-group">
		                                        <label class="label"><b>SUBIR ARCHIVO .CSV </b></label>
		                                        <label class="input input-file">
		                                              <span class="button">
		                                                  <input id="archivo" accept=".csv" name="archivo" type="file" class="file">
		                                                  <input name="MAX_FILE_SIZE" type="hidden" value="20000" />
		                                            <b class="tooltip tooltip-top-left">
		                                                <i class="fa fa-warning txt-color-teal"></i> EL ARCHIVO A SUBIR, DEBE SER EXTENSION .CSV
		                                            </b>
		                                        </label>
		                                    </section>
		                                </fieldset>
			                            <div >
			                                <button type="button" name="subir_archivo" id="subir_archivo" class="btn btn-success" style="width:100%;">SUBIR REQUERIMIENTOS .CSV</button>
					                        <center><img id="load" style="display: none" src="<?php echo base_url() ?>/assets/img/loading.gif" width="50" height="50"></center>
			                            </div>
                                      </form> 
                                    </div>
                                    <!-- end row -->
                                  </div>
                                  <!-- end col -->
                                </div>
                                <!-- end row -->
                              </div>
                              <!-- end well -->
                            </div>
                          </div>
                        </article>
                    </div>   
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
		<!--================================================== -->
		<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
		<script data-pace-options='{ "restartOnRequestAfter": true }' src="<?php echo base_url(); ?>assets/js/plugin/pace/pace.min.js"></script>

		<script>
			if (!window.jQuery) {document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-2.0.2.min.js"><\/script>');}
		</script>
		<script>
			if (!window.jQuery.ui) {document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');}
		</script>
		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url(); ?>assets/js/session_time/jquery-idletimer.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/app.config.js"></script>
		<script src = "<?php echo base_url(); ?>mis_js/programacion/insumos/insumos_tablas.js"></script>
		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.min.js"></script>
		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url(); ?>assets/js/notification/SmartNotification.min.js"></script>
		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url(); ?>assets/js/smartwidgets/jarvis.widget.min.js"></script>
		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<!-- SPARKLINES -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/select2/select2.min.js"></script>
		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
		<!-- browser msie issue fix -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/fastclick/fastclick.min.js"></script>
		<!-- Demo purpose only -->
		<script src="<?php echo base_url(); ?>assets/js/demo.min.js"></script>
		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url(); ?>assets/js/app.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<script src="<?php echo base_url(); ?>assets/js/speech/voicecommand.min.js"></script>
		<script type="text/javascript">
		$(function () {

		    //SUBIR ARCHIVO
		    $("#subir_archivo").on("click", function () {

		        var $valid = $("#form_subir_sigep").valid();
		        if (!$valid) {
		            $validator.focusInvalid();
		        } else {
		        	if(document.getElementById('archivo').value==''){
		        		alertify.alert('POR FAVOR SELECCIONE ARCHIVO .CSV');
		        		return false;
		        	}
		            proy_id = document.getElementById('proy_id').value;
		            pfec_id = document.getElementById('pfec_id').value;
		            prod_id = document.getElementById('prod_id').value;
		            act_id = document.getElementById('act_id').value;
		            archivo = document.getElementById('archivo').value;
		            //si el valor es diferente de 0 no existe fallas
		                alertify.confirm("REALMENTE DESEA SUBIR ESTE ARCHIVO?", function (a) {
		                    if (a) {
		                        document.getElementById("load").style.display = 'block';
		                        document.getElementById('subir_archivo').disabled = true;
		                        document.forms['form_subir_sigep'].submit();
		                    } else {
		                        alertify.error("OPCI\u00D3N CANCELADA");
		                    }
		                });
		           
		        }
		    });
	    });
		</script>
		<script type="text/javascript">
		    $(function () {
		        function reset() {
		            $("#toggleCSS").attr("href", "<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css");
		            alertify.set({
		                labels: {
		                    ok: "ACEPTAR",
		                    cancel: "CANCELAR"
		                },
		                delay: 5000,
		                buttonReverse: false,
		                buttonFocus: "ok"
		            });
		        }

		        // =====================================================================

                $("#dt_basic").on("click", ".del_ff",function (e) {
                    reset();
                    var name = $(this).attr('name');
                    var id = $(this).attr('id');
                    var request;
                    // confirm dialog
                    alertify.confirm("DESEA ELIMINAR EL REQUERIMIENTO?", function (a) {
                        if (a) {
                            url = "<?php echo site_url("")?>/prog/del_ins_part";
                            if (request) {
                                request.abort();
                            }
                            request = $.ajax({
                                url: url,
                                type: "POST",
                                dataType: "json",
                                data: "insp_id="+name

                            });

                            request.done(function (response, textStatus, jqXHR) {
                                reset();
                                if (response.respuesta == 'correcto') {
                                    alertify.alert("EL INSUMO SE ELIMINO CORRECTAMENTE ", function (e) {
                                        if (e) {
                                            window.location.reload(true);
                                        }
                                    });
                                } else {
                                    alertify.alert("ERROR AL ELIMINAR!!!", function (e) {
                                        if (e) {
                                            window.location.reload(true);
                                        }
                                    });
                                }
                            });
                            request.fail(function (jqXHR, textStatus, thrown) {
                                console.log("ERROR: " + textStatus);
                            });
                            request.always(function () {
                                //console.log("termino la ejecuicion de ajax");
                            });

                            e.preventDefault();

                        } else {
                            // user clicked "cancel"
                            alertify.error("Opcion cancelada");
                        }
                    });
                    return false;
                });
		    });
		</script>
	</body>
</html>
