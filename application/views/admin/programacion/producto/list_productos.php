<!DOCTYPE html>
<html lang="en-us">
  <head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    <title><?php echo $this->session->userdata('name')?></title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- Basic Styles -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
    <!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
    <!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
    <!-- FAVICONS -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.core.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css" id="toggleCSS"/>
    <style type="text/css">
      aside{background: #05678B;}
    </style>
    <script>
      function confirmar()
      {
        if(confirm('¿Estas seguro de Eliminar ?'))
          return true;
        else
          return false;
      }
    </script>
      <style>
        table{font-size: 9px;
        width: 100%;
        max-width:1550px;;
        overflow-x: scroll;
        }
        th{
          padding: 1.4px;
          text-align: center;
          font-size: 10px;
        }
      </style>
  </head>
  <body class="">
    <!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->
    <!-- HEADER -->
    <header id="header">
      <!-- pulled right: nav area -->
      <div class="pull-right">
        <!-- collapse menu button -->
        <div id="hide-menu" class="btn-header pull-right">
          <span> <a href="javascript:void(0);" data-action="toggleMenu" title="Menu"><i class="fa fa-reorder"></i></a> </span>
        </div>
        <!-- end collapse menu -->
        <!-- logout button -->
        <div id="logout" class="btn-header transparent pull-right">
          <span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Salir" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
        </div>
        <!-- end logout button -->
        <!-- search mobile button (this is hidden till mobile view port) -->
        <div id="search-mobile" class="btn-header transparent pull-right">
          <span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
        </div>
        <!-- end search mobile button -->
        <!-- fullscreen button -->
        <div id="fullscreen" class="btn-header transparent pull-right">
          <span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Pantalla Completa"><i class="fa fa-arrows-alt"></i></a> </span>
        </div>
        <!-- end fullscreen button -->
      </div>
      <!-- end pulled right: nav area -->
    </header>
    <!-- END HEADER -->
    <!-- Left panel : Navigation area -->
    <aside id="left-panel">
      <!-- User info -->
      <div class="login-info">
        <span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
          <a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
            <span>
                <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
            </span>
          </a> 
        </span>
      </div>

      <nav>
        <ul>
          <li>
            <a href='<?php echo site_url("admin").'/dashboard'; ?>' title="MENU PRINCIPAL"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
          </li>
          <li class="text-center">
            <a href="<?php echo base_url().'index.php/admin/proy/mis_proyectos/'.$mod ?>" title="PROGRAMACION -> MIS PROYECTOS"> <span class="menu-item-parent">PROGRAMACI&Oacute;N F&Iacute;SICA</span></a>
          </li>
          <?php
          if($nro_fase==1){
              for($i=0;$i<count($enlaces);$i++)
              {
                ?>
                <li>
                  <a href="#" >
                      <i class="<?php echo $enlaces[$i]['o_image'];?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
                    <ul>
                      <?php
                        $submenu= $this->menu_modelo->get_Modulos_sub($enlaces[$i]['o_child']);
                        foreach($submenu as $row) {
                        ?>
                        <li><a href="<?php echo base_url($row['o_url'])."/".$mod."/".$id_f[0]['id']."/".$id_f[0]['proy_id']; ?>"><?php echo $row['o_titulo']; ?></a></li>
                        <?php } ?>
                    </ul>
                </li>
                <?php
              }
          }
          ?>
        </ul>
      </nav>
      <span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>
    </aside>

    <!-- MAIN PANEL -->
    <div id="main" role="main">
      <!-- RIBBON -->
      <div id="ribbon">
        <!-- breadcrumb -->
          <ol class="breadcrumb">                         
               <?php 
                if($proyecto[0]['tp_id']==1 || $proyecto[0]['tp_id']==2 || $proyecto[0]['tp_id']==3)
                {
                     ?>
                     <li><a href="<?php echo base_url().'index.php/admin/proy/list_proy' ?>" title="VOLVER A MIS PROYECTOS">Mis Operaciones</a></li><li><a href="<?php echo base_url().'index.php/admin/prog/list_comp/'.$mod.'/'.$id_f[0]['id']."/".$id_f[0]['proy_id'] ?>" title="MIS COMPONENTES">Mis Componentes</a></li><li>Mis Productos</li> 
                     <?php
                }
                elseif ($proyecto[0]['tp_id']==4) 
                {
                     ?>
                     <li><a href="<?php echo base_url().'index.php/admin/proy/list_proy' ?>" title="VOLVER A MIS PROYECTOS">Mis Operaciones</a></li><li>Objetivo de Productos</li> 
                     <?php
                }
               ?>
          </ol>
      </div>
      <!-- END RIBBON -->
        <!-- MAIN CONTENT -->
        <div id="content">
          <article class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
            <section id="widget-grid" class="well">
                <div class="">
                  <h1> <?php echo $titulo_proy;?> : <small><?php echo $proyecto[0]['proy_nombre']?></small></h1>
                  <h1> CATEGORIA PROGRAM&Aacute;TICA : <small><?php echo $proyecto[0]['aper_programa'].''.$proyecto[0]['aper_proyecto'].''.$proyecto[0]['aper_actividad']?></small>
                  <h1> COMPONENTE : <small><?php echo $componente[0]['com_componente'];?></small>
                </div>
            </section>
          </article>

          <article class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
            <section id="widget-grid" class="well">
              <center>
                <div class="dropdown">
                  <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenu1" style="width:100%;" data-toggle="dropdown" aria-expanded="true">
                    OPCIONES DEL PRODUCTOS
                    <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url().'index.php/admin/dashboard' ?>">SALIR A MENU PRINCIPAL</a></li>
                    <!-- <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url().'index.php/admin/proy/operacion/'.$proyecto[0]['proy_id'].'/'.$id_f[0]['id']; ?>">MI OPERACI&Oacute;N </a></li> -->
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url().'index.php/admin/proy/mis_proyectos/'.$mod ?>">LISTA DE OPERACIONES</a></li>
                    <li role="presentation"><a  href="<?php echo base_url(); ?>assets/video/plantilla_producto.xlsx" style="cursor: pointer;" download>DESCARGAR PLANTILLA PRODUCTOS</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#" data-toggle="modal" data-target="#modal_importar"  title="IMPORTAR ARCHIVO DE PRODUCTOS">IMPORTAR PRODUCTOS</a></li>
                  </ul>
                </div>
              </center>
            </section>
          </article>

            <section id="widget-grid" class="">
            <!-- row -->
            <div class="row">
              <!-- NEW WIDGET START -->
              <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php 
                    if($this->session->flashdata('success')){ ?>
                      <div class="alert alert-success">
                          <?php echo $this->session->flashdata('success'); ?>
                      </div>
                      <script type="text/javascript">alertify.success("<?php echo '<font size=2>'.$this->session->flashdata('success').'</font>'; ?>")</script>
                  <?php 
                      }
                    elseif($this->session->flashdata('danger')){ ?>
                        <div class="alert alert-danger">
                          <?php echo $this->session->flashdata('danger'); ?>
                        </div>
                        <script type="text/javascript">alertify.error("<?php echo '<font size=2>'.$this->session->flashdata('danger').'</font>'; ?>")</script>
                      <?php
                    }
                  ?>
                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget jarviswidget-color-darken" >
                  <header>
                      <span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
                      <h2 class="font-md"><strong>PRODUCTOS DE LA OPERACI&Oacute;N <?php echo $this->session->userdata("gestion");?></strong></h2>  
                  </header>

                  <div>
                    <div class="widget-body no-padding">
                      <div class="table-responsive">
                        <table id="dt_basic" class="table table-bordered" width="100%">
                          <thead>
                              <tr>
                                <td style="width:1%;"><center><a href='<?php echo site_url("admin").'/prog/new_prod/'.$mod.'/'.$id_f[0]['id']."/".$id_f[0]['proy_id'].'/'.$componente[0]['com_id']; ?>' title="NUEVO DE REGISTRO PRODUCTOS "><img src="<?php echo base_url(); ?>assets/ifinal/2.png" WIDTH="40" HEIGHT="40"/><br>REGISTRAR PRODUCTOS</a></center></td>
                                <th style="width:5%;"><b>E/B</b></th>
                                <th style="width:15%;"><b>PRODUCTOS</b></th>
                                <th style="width:1%;"><b>TIPO DE INDICADOR</b></th>
                                <th style="width:5%;"><b>INDICADOR</b></th>
                                <th style="width:1%;"><b>LINEA BASE</b></th>
                                <th style="width:1%;"><b>META TOTAL</b></th>
                                <th style="width:1%;"><b>META GESTI&Oacute;N</b></th>
                                <th style="width:5%;"><b>PONDERACI&Oacute;N</b></th>
                                <th style="width:5%;"><b>MEDIOS DE VERIFICACI&Oacute;N</b></th>
                                <th style="width:20%;"><b>CRONOGRAMA DE PROGRAMACI&Oacute;N</b></th>
                              </tr>
                          </thead>
                          <tbody id="bdi">
                          <?php $nro=1; $ponderacion=0;
                            foreach($productos as $rowp)
                            { $ti='';
                              if($rowp['indi_id']==2){ $ti='%';}
                              echo '<tr>';
                              echo '<td><font size=2><center><b></b>';
                              echo '</center></font></td>';
                              ?>
                              <td style="width:5%;">
                                <center>
                                  <a href='<?php echo site_url("admin").'/prog/mod_prod/'.$mod.'/'.$id_f[0]['id']."/".$id_f[0]['proy_id'].'/'.$componente[0]['com_id'].'/'.$rowp['prod_id'].'/0'; ?>' title="MODIFICAR DATOS DEL PRODUCTO">
                                      <img src="<?php echo base_url(); ?>assets/ifinal/modificar.png" WIDTH="40" HEIGHT="40"/>MODIFICAR
                                  </a><br>
                                  <a href='<?php echo site_url("admin").'/prog/delete_prod/'.$mod.'/'.$id_f[0]['id']."/".$id_f[0]['proy_id'].'/'.$componente[0]['com_id'].'/'.$rowp['prod_id']; ?>' title="ELIMINAR PRODUCTO" onclick="return confirmar()"><img src="<?php echo base_url(); ?>assets/ifinal/eliminar.png" WIDTH="35" HEIGHT="35"/><br>ELIMINAR</a><br>
                                  <a href='<?php echo site_url("admin").'/prog/list_act/'.$mod.'/'.$id_f[0]['id'].'/'.$id_f[0]['proy_id'].'/'.$componente[0]['com_id'].'/'.$rowp['prod_id']; ?>' title="INGRESAR A ACTIVIDADES"><img src="<?php echo base_url(); ?>assets/ifinal/archivo.png" WIDTH="40" HEIGHT="40"/>REGISTRO DE ACTIVIDADES</a>
                                </center> 
                              </td>
                              <?php
                              $meta_gest=$this->model_producto->meta_prod_gest($rowp['prod_id']);
                              $años=$id_f[0]['pfec_fecha_fin']-$id_f[0]['pfec_fecha_inicio']+1;
                              echo '<td><font size=1>'.$rowp['prod_producto'].'</font></td>';
                              echo '<td><font size=1>'.$rowp['indi_abreviacion'].'</font></td>';
                              echo '<td><font size=1>'.$rowp['prod_indicador'].'</font></td>';
                              echo '<td><font size=1>'.round($rowp['prod_linea_base'],3).' '.$ti.'</font></td>';
                              echo '<td><font size=1>'.round($rowp['prod_meta'],3).' '.$ti.'</font></td>';
                              echo '<td><font size=1>'.round($meta_gest[0]['meta_gest'],3).' '.$ti.'</font></td>';
                              echo '<td><font size=1>'.$rowp['prod_ponderacion'].' %</font></td>';
                              echo '<td><font size=1>'.$rowp['prod_fuente_verificacion'].'</font></td>';
                              echo '<td>';
                                /*------------------------------------------------------------------------------*/
                                $pa=0; $gestion=$id_f[0]['pfec_fecha_inicio'];
                                for($k=1;$k<=$años;$k++)
                                { 
                                  $prod_gest=$this->model_producto->prod_prog_mensual($rowp['prod_id'],$gestion);
                                  $nro=0;
                                  foreach($prod_gest as $row)
                                  {
                                    $nro++;
                                    $matriz [1][$nro]=$row['m_id'];
                                    $matriz [2][$nro]=$row['pg_fis'];
                                  }

                                  for($j = 1 ;$j<=12 ;$j++)
                                  {
                                    $matriz_r[1][$j]=$j;
                                    $matriz_r[2][$j]='0';
                                  }

                                  for($i = 1 ;$i<=$nro ;$i++)
                                  {
                                    for($j = 1 ;$j<=12 ;$j++)
                                    {
                                      if($matriz[1][$i]==$matriz_r[1][$j])
                                      {
                                        $matriz_r[2][$j]=round($matriz[2][$i],3);
                                      }
                                    }
                                  }

                                  for($j = 1 ;$j<=12 ;$j++){
                                    $pa=$pa+$matriz_r[2][$j];
                                    $matriz_r[3][$j]=$pa+$rowp['prod_linea_base'];
									if($rowp['prod_meta'] == 0){
										$matriz_r[4][$j]=0;
									}else{
										$matriz_r[4][$j]=round(((($pa+$rowp['prod_linea_base'])/$rowp['prod_meta'])*100),2);
									}
                                  }
                                  /*------------------------------------------------------------------------------*/
                                  if($this->session->userData('gestion')==$gestion)
                                  {
                                    echo '<table class="table table-bordered">
                                            <thead>                        
                                              <tr>
                                                  <td style="width:1%;" bgcolor="#008080" colspan=13><font color="#ffffff">GESTI&Oacute;N '.$gestion.'</font></td>
                                              </tr>
                                              <tr bgcolor="#F5F5DC">
                                                  <td style="width:1%;"></td>
                                                  <td style="width:1%;"><center>Ene.</center></td>
                                                  <td style="width:1%;"><center>Feb.</center></td>
                                                  <td style="width:1%;"><center>Mar.</center></td>
                                                  <td style="width:1%;"><center>Abr.</center></td>
                                                  <td style="width:1%;"><center>May.</center></td>
                                                  <td style="width:1%;"><center>Jun.</center></td>
                                                  <td style="width:1%;"><center>Jul.</center></td>
                                                  <td style="width:1%;"><center>Ago.</center></td>
                                                  <td style="width:1%;"><center>Sep.</center></td>
                                                  <td style="width:1%;"><center>Oct.</center></td>
                                                  <td style="width:1%;"><center>Nov.</center></td>
                                                  <td style="width:1%;"><center>Dic.</center></td>
                                              </tr>
                                              <tr bgcolor="#F5F5DC">
                                                  <td style="width:1%;">Pd</td>';
                                                  for($i = 1 ;$i<=12 ;$i++)
                                                  {
                                                    echo '<td>'.$matriz_r[2][$i].'</td>';
                                                  }
                                                  echo'
                                              </tr>
                                              <tr bgcolor="#F5F5DC">
                                                  <td style="width:1%;">PA</td>';
                                                  for($i = 1 ;$i<=12 ;$i++)
                                                  {
                                                    echo '<td>'.$matriz_r[3][$i].'</td>';
                                                  }
                                                  echo'
                                              </tr>
                                              <tr bgcolor="#F5F5DC">
                                                  <td style="width:1%;">%PA</td>';
                                                  for($i = 1 ;$i<=12 ;$i++)
                                                  {
													if($matriz_r[4][$i] == 0){
														echo '<td class="bg-danger">'.$matriz_r[4][$i].'%</td>';
													}else{
														echo '<td>'.$matriz_r[4][$i].'%</td>';
													}
                                                  }
                                                  echo'
                                              </tr>
                                            </thead>';
                                  echo'</table><br>';
                                  }
                                  
                                  $gestion++;
                                }
                                echo'</td>';
                            echo '</tr>';
                              $nro=$nro+1;
                              $ponderacion=$ponderacion+$rowp['prod_ponderacion'];
                            }
                           ?>
                           <tr>
                                <td style="width:1%;"></td>
                                <td style="width:5%;"></td>
                                <td style="width:10%;"></font></td>
                                <td style="width:1%;"></td>
                                <td style="width:5%;"></td>
                                <td style="width:5%;"></td>
                                <td style="width:5%;"></td>
                                <td style="width:1%;"></td>
                                <td style="width:1%;"><center><?php echo $ponderacion; ?> %</center></td>
                                <td style="width:1%;"></td>
                                <td style="width:5%;"></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div class="form-actions">
                    <?php 
                        if($proyecto[0]['tp_id']==1 || $proyecto[0]['tp_id']==2 || $proyecto[0]['tp_id']==3)
                        {
                          ?>
                            <a href="<?php echo base_url().'index.php/admin/prog/list_comp/'.$mod.'/'.$id_f[0]['id']."/".$id_f[0]['proy_id'] ?>" title="MIS COMPONENTES" class="btn btn-lg btn-default" title="Volver atras">ATRAS</a>
                          <?php
                        }
                        elseif ($proyecto[0]['tp_id']==4) 
                        {
                          ?>
                            <a href="<?php echo base_url().'index.php/admin/proy/list_proy' ?>" title="MIS ACCIONES" class="btn btn-lg btn-default" title="Volver atras">ATRAS</a>
                          <?php
                        }
                     ?>
                    </div>
                    <!-- end widget content -->
                  </div>
                  <!-- end widget div -->
                </div>
                <!-- end widget -->
              </article>
              <!-- WIDGET END -->
            </div>
          </section>    
        <!--///////////fin de tabla///////-->
    </div>
    <!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->

<!-- PAGE FOOTER -->
<div class="page-footer">
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
        </div>
    </div>
</div>
<!-- END PAGE FOOTER -->
<!-- ================== MODAL SUBIR ARCHIVO ========================== -->
        <div class="modal animated fadeInDown" id="modal_importar" tabindex="-1" role="dialog">
        <link href="<?php echo base_url(); ?>assets/file/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
        <script src="<?php echo base_url(); ?>assets/file/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/file/js/fileinput.min.js" type="text/javascript"></script> 
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body no-padding">
                    <div class="row">
                       <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="row">
                            <h2 class="row-seperator-header"><i class="glyphicon glyphicon-import"></i> IMPORTAR ARCHIVO (.CSV) </h2>
                            <div class="col-sm-12">
                              <!-- well -->
                              <div class="well">
                                <!-- row -->
                                <div class="row">
                                  <!-- col -->
                                  <div class="col-sm-12">
                                    <p class="alert alert-info">
                                      <i class="fa fa-info"></i> Por favor guardar el archivo (Excel.xls) a extension (.csv) delimitado por (; "Punto y comas"). verificar el archivo .csv para su correcta importaci&oacute;n
                                    </p>
                                    <!-- row -->
                                    <div class="row">
                                      <form  action="<?php echo site_url() . '/programacion/producto/subir_producto' ?>" method="post" enctype="multipart/form-data" id="form_subir_sigep" name="form_subir_sigep">
                                          <input class="form-control" type="hidden" name="mod" id="mod" value="<?php echo $mod ?>">
                                          <input class="form-control" type="hidden" name="pfec_id" id="pfec_id" value="<?php echo $id_f[0]['id'] ?>"> 
                                          <input class="form-control" type="hidden" name="proy_id" id="proy_id" value="<?php echo $proyecto[0]['proy_id'] ?>">
                                          <input class="form-control" type="hidden" name="com_id" id="com_id" value="<?php echo $id_c ?>">
                                         <fieldset>
                                        <section class="form-group">
                                            <label class="label"><b>SUBIR ARCHIVO .CSV </b></label>
                                            <label class="input input-file">
                                                  <span class="button">
                                                      <input id="archivo" accept=".csv" name="archivo" type="file" class="file">
                                                      <input name="MAX_FILE_SIZE" type="hidden" value="20000" />
                                                <b class="tooltip tooltip-top-left">
                                                    <i class="fa fa-warning txt-color-teal"></i> EL ARCHIVO A SUBIR, DEBE SER EXTENSION .CSV
                                                </b>
                                            </label>
                                        </section>
                                    </fieldset>
                                  <div >
                                      <button type="button" name="subir_archivo" id="subir_archivo" class="btn btn-success" style="width:100%;">SUBIR ARCHIVO .CSV</button>
                                  <center><img id="load" style="display: none" src="<?php echo base_url() ?>/assets/img/loading.gif" width="50" height="50"></center>
                                  </div>
                                      </form> 
                                    </div>
                                    <!-- end row -->
                                  </div>
                                  <!-- end col -->
                                </div>
                                <!-- end row -->
                              </div>
                              <!-- end well -->
                            </div>
                          </div>
                        </article>
                    </div>   
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <!--================================================== -->
<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
<script data-pace-options='{ "restartOnRequestAfter": true }'
        src="<?php echo base_url(); ?>assets/js/plugin/pace/pace.min.js"></script>
<script>
    if (!window.jQuery) {
        document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-2.0.2.min.js"><\/script>');
    }
</script>
<script>
    if (!window.jQuery.ui) {
        document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
    }
</script>
<!-- IMPORTANT: APP CONFIG -->
<script src="<?php echo base_url(); ?>assets/js/session_time/jquery-idletimer.js"></script>
<script src = "<?php echo base_url(); ?>mis_js/control_session.js"></script>
<script src="<?php echo base_url(); ?>assets/js/app.config.js"></script>
<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/tabla/jquery.dataTables.min.js"></script>
<!-- FastClick: For mobile devices -->
<script src="<?php echo base_url(); ?>assets/js/tabla/jquery-1.12.3.js"></script>
<!--fin tablas-->
<!-- BOOTSTRAP JS -->
<script src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.min.js"></script>
<!-- CUSTOM NOTIFICATION -->
<script src="<?php echo base_url(); ?>assets/js/notification/SmartNotification.min.js"></script>
<!--  JARVIS WIDGETS -->
<script src="<?php echo base_url(); ?>assets/js/smartwidgets/jarvis.widget.min.js"></script>
<!-- JQUERY VALIDATE -->
<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
<!-- JQUERY MASKED INPUT -->
<script src="<?php echo base_url(); ?>assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
<!-- JQUERY SELECT2 INPUT -->
<script src="<?php echo base_url(); ?>assets/js/plugin/select2/select2.min.js"></script>
<!-- JQUERY UI + Bootstrap Slider -->
<script src="<?php echo base_url(); ?>assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
<!-- browser msie issue fix -->
<script src="<?php echo base_url(); ?>assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
<!-- FastClick: For mobile devices -->
<script src="<?php echo base_url(); ?>assets/js/plugin/fastclick/fastclick.min.js"></script>
<!-- Demo purpose only -->
<script src="<?php echo base_url(); ?>assets/js/demo.min.js"></script>
<!-- MAIN APP JS FILE -->
<script src="<?php echo base_url(); ?>assets/js/app.min.js"></script>
<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
<!-- Voice command : plugin -->
<script src="<?php echo base_url(); ?>assets/js/speech/voicecommand.min.js"></script>
<!-- ///// mis validaciones js ///// -->
<SCRIPT src="<?php echo base_url(); ?>assets/js/mis_js/validacion_form.js" type="text/javascript"></SCRIPT>
<!--alertas -->
<script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
<!-- PAGE RELATED PLUGIN(S) -->
<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
<script type="text/javascript">
    $(function () {
      //SUBIR ARCHIVO
      $("#subir_archivo").on("click", function () {

          var $valid = $("#form_subir_sigep").valid();
          if (!$valid) {
              $validator.focusInvalid();
          } else {
            if(document.getElementById('archivo').value==''){
              alertify.alert('PORFAVOR SELECCIONE ARCHIVO .CSV');
              return false;
            }
              proy_id = document.getElementById('proy_id').value;
              pfec_id = document.getElementById('pfec_id').value;
              com_id = document.getElementById('com_id').value;
              archivo = document.getElementById('archivo').value;
              //si el valor es diferente de 0 no existe fallas
                  alertify.confirm("REALMENTE DESEA SUBIR ESTE ARCHIVO?", function (a) {
                      if (a) {
                          document.getElementById("load").style.display = 'block';
                          document.getElementById('subir_archivo').disabled = true;
                          document.forms['form_subir_sigep'].submit();
                      } else {
                          alertify.error("OPCI\u00D3N CANCELADA");
                      }
                  });
             
          }
      });
      });
    </script>
  <script type="text/javascript">
      // DO NOT REMOVE : GLOBAL FUNCTIONS!
      $(document).ready(function() {
           pageSetUp();
           /* BASIC ;*/
            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;
            
            var breakpointDefinition = {
                 tablet : 1024,
                 phone : 480
            };

            $('#dt_basic').dataTable({
                 "ordering": false,
                 "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
                      "t"+
                      "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                 "autoWidth" : true,
                 "preDrawCallback" : function() {
                      // Initialize the responsive datatables helper once.
                      if (!responsiveHelper_dt_basic) {
                           responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                      }
                 },
                 "rowCallback" : function(nRow) {
                      responsiveHelper_dt_basic.createExpandIcon(nRow);
                 },
                 "drawCallback" : function(oSettings) {
                      responsiveHelper_dt_basic.respond();
                 }
            });
          })
    </script>
</body>
</html>
