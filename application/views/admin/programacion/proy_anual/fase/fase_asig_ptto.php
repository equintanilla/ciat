<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
		<title><?php echo $this->session->userdata('name')?></title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
		<script type='text/javascript' src='<?php echo base_url(); ?>assets/formexp.js'></script>
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/estil.css">
	</head>
	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->
		<!-- HEADER -->
		<header id="header">
			<!-- pulled right: nav area -->
			<div class="pull-right">
				<!-- collapse menu button -->
				<div id="hide-menu" class="btn-header pull-right">
					<span> <a href="javascript:void(0);" data-action="toggleMenu" title="Menu"><i class="fa fa-reorder"></i></a> </span>
				</div>
				<!-- end collapse menu -->
				<!-- logout button -->
				<div id="logout" class="btn-header transparent pull-right">
					<span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Salir" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
				</div>
				<!-- end logout button -->
				<!-- search mobile button (this is hidden till mobile view port) -->
				<div id="search-mobile" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
				</div>
				<!-- end search mobile button -->
				<!-- fullscreen button -->
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Pantalla Completa"><i class="fa fa-arrows-alt"></i></a> </span>
				</div>
				<!-- end fullscreen button -->
			</div>
			<!-- end pulled right: nav area -->
		</header>
		<!-- END HEADER -->
		<!-- Left panel : Navigation area -->
		<aside id="left-panel">
			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                            <span>
                                  <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                            </span>
						<i class="fa fa-angle-down"></i>
					</a> 
				</span>
			</div>

			<nav>
				<ul>
					<li class="">
	                <a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MEN&Uacute; PRINCIPAL">
	                <i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
	            	</li>
		            <li class="text-center">
		                <a href="<?php echo base_url().'index.php/admin/proy/list_proy_fin' ?>" title="MIS PROYECTOS"><span class="menu-item-parent">PROGRAMACI&Oacute;N DEL POA</span></a>
		            </li>
					<?php
		                for($i=0;$i<count($enlaces);$i++)
		                {
		                    if(count($subenlaces[$enlaces[$i]['o_child']])>0)
		                    {
		            ?>
		            <li>
		              	<a href="#" >
		              		<i class="<?php echo $enlaces[$i]['o_image']?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
		              	<ul >
		              	<?php
		                foreach ($subenlaces[$enlaces[$i]['o_child']] as $item) {
		                ?>
		                <li><a href="<?php echo base_url($item['o_url']); ?>"><?php echo $item['o_titulo']; ?></a></li>
		                <?php } ?>
		                </ul>
		            </li>
		            <?php 
		                    }
		            	} ?>
				</ul>
			</nav>
			<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>

		</aside>

		<!-- MAIN PANEL -->
		<div id="main" role="main">

			<!-- RIBBON -->
			<div id="ribbon">

				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>

				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url().'index.php/admin/proy/list_proy_fin' ?>" title="MIS PROYECTOS">Mis Operaciones</a></li><li><a href="<?php echo base_url().'index.php/admin/proy/list_proy_fin' ?>">T&eacute;cnico Analista Financiero</a></li><li>Asignar Presupuesto</li>
				</ol>
			</div>
			<!-- END RIBBON -->
			<?php
					$attributes = array('class' => 'form-horizontal', 'id' => 'formulario','name' =>'formulario','enctype' => 'multipart/form-data');
					echo validation_errors();
					echo form_open('admin/proy/add_ptto', $attributes);
			?>
			<!-- MAIN CONTENT -->
			<div id="content">
				<!-- widget grid -->
				<section id="widget-grid" class="">
					<!-- row -->
					<div class="row">
						<!-- NEW WIDGET START -->
						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="jarviswidget jarviswidget-color-darken" >
									<header>
										<span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
										<h2 class="font-md"><strong>ASIGNACI&Oacute;N DE RECURSOS</strong></h2>				
									</header>
										<!-- widget content -->
									<div class="widget-body">
									<form id="formulario" name="formulario" novalidate="novalidate" method="post">
									<input class="form-control" type="hidden" name="id_p" id="id_p" value="<?php echo $proyecto[0]['proy_id'] ?>">
									<input class="form-control" type="hidden" name="id_f" id="id_f" value="<?php echo $fase[0]['id'] ?>">
									<input class="form-control" type="hidden" name="id_fg" id="id_fg" value="<?php echo $fase_gest[0]['ptofecg_id'] ?>">
									<div class="">
									<h2 class="alert alert-success"><center><?php echo $titulo.' - '.$proyecto[0]['proy_nombre'] ?></center></h2>
											</div>
											<?php 
											if($proyecto[0]['tp_id']==1)
											{ $unidad = $this->model_proyecto->responsable_proy($proyecto[0]['proy_id'],'1');
												?>
												<table class="table table-bordered" style="width:100%;">
												<thead>
													<tr>
														<th style="width:10%;"><font size="1">FASE</font></th>
														<th style="width:5%;"><font size="1">ETAPA</font></th>
														<th style="width:15%;"><font size="1">DESCRIPCI&Oacute;N FASE</font></th>
														<th style="width:10%;"><font size="1">UNIDAD EJECUTORA </font></th>
														<th style="width:10%;"><font size="1">FECHA DE INICIO</font></th>
														<th style="width:10%;"><font size="1">FECHA DE CONCLUSI&Oacute;N</font></th>
														<th style="width:10%;"><font size="1">NUEVO/CONTINUO</font></th>
														<th style="width:10%;"><font size="1">ANUAL/PLURIANUAL</font></th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td><font size="1"><?php echo $fase[0]['fase'] ?></font></td>
														<td><font size="1"><?php echo $fase[0]['etapa'] ?></font></td>
														<td><font size="1"><?php echo $fase[0]['descripcion'] ?></font></td>
														<td><font size="1"><?php echo $unidad[0]['uejec'] ?></td>
														<td><font size="1"><?php echo date('d-m-Y',strtotime($fase[0]['inicio'])) ?></font></td>
														<td><font size="1"><?php echo date('d-m-Y',strtotime($fase[0]['final'])) ?></font></td>
														<td><font size="1"><?php echo $this->model_faseetapa->calcula_nc($fase[0]['pfec_fecha_inicio']) ?></td>
														<td><font size="1"><?php echo $this->model_faseetapa->calcula_ap($fase[0]['pfec_fecha_inicio'],$fase[0]['pfec_fecha_fin']) ?></td>
														
													</tr>
													</tbody>
												</table><br>
												<?php
											}
											?>	
											
									<table class="table table-bordered">
											<thead>			                
												<tr>
													<td style="width:20%;"><font size="1"><b>PRESUPUESTO TOTAL</b></font></td>
													<td style="width:16%;"><input class="form-control" type="text" value="<?php echo number_format($fase[0]['pfec_ptto_fase'], 2, ',', ' ') ?>" disabled="true"><input class="form-control" type="hidden" name="pt" id="pt" value="<?php echo $fase[0]['pfec_ptto_fase'] ?>"></td>
													<td style="width:20%;"><font size="1"><b>PRESPUESTO GESTI&Oacute;N <?php echo $this->session->userdata("gestion");?>:</b></font></td>
													<td style="width:16%;"><input class="form-control" type="text" value="<?php echo number_format($fase_gest[0]['pfecg_ppto_total'], 2, ',', ' ') ?>" disabled="true"><input class="form-control" type="hidden" name="pg" id="pg" value="<?php echo $fase_gest[0]['pfecg_ppto_total'] ?>"></td>
													<td style="width:10%;"><font size="1"><b>TOTAL ASIGNADO :</b></font></td>
													<td style="width:15%;"><input class="form-control" type="text" name="pta" id="pta" value="0" disabled="true"></td>
												</tr>
											</thead>
									</table><br>
									<?php 
										if($fase[0]['pfec_ptto_fase']!=0)
										{
											if($fase_gest[0]['pfecg_ppto_total']!=0)
											{
												?>
												<div class="table-responsive" >
											<center>
												<div id="capainicio">
												<table class="table table-bordered" style="width:80%;" >
													<thead>			                
														<tr>
															<th style="width:10%;">
																 <select size="1" name="numero" onchange="expandir_formulario()" class="form-control">
																  <option value="1">1</option>
																  <option value="2">2</option>
																  <option value="3">3</option>
																  <option value="4">4</option>
																  <option value="5">5</option>
																  <option value="6">6</option>
																  <option value="7">7</option>
																  <option value="8">8</option>
																  <option value="9">9</option>
																  <option value="10">10</option>
																  </select>
															</th>
															<th style="width:30%;"><font size="2"><center>FUENTE FINANCIAMIENTO</center></font></th>
															<th style="width:30%;"><font size="2"><center>ORGANISMO FINANCIADOR</center></font></th>
															<th style="width:30%;"><font size="2"><center>IMPORTE</center></font></th>
														</tr>
													</thead>
												</table>
												</div>

												<div >
													<table class="table table-bordered" style="width:80%;" >
													<tr>
														<td style="width:10%;">
															Asig. 1
								                        </td>
								                        <td style="width:30%;">
															<select class="form-control" id="ff1" name="ff1" title="Seleccione Fuente Financiamiento">
								                                <option value="">Seleccione Fuente financiamiento </option>
								                                <?php 
																	foreach($ffi as $row){ ?>
																		<option value="<?php echo $row['ff_id']; ?>"><?php echo $row['ff_codigo'].' - '.$row['ff_descripcion']; ?></option>
																	<?php 	
																	}
																?>      
								                            </select>
								                        </td>
														<td style="width:30%;">
															<select class="form-control" id="fo1" name="fo1" title="Seleccione Organismo Financiador">
								                                <option value="">Seleccione Fuente Organismo </option>
								                                <?php 
																	foreach($fof as $row){ ?>
																		<option value="<?php echo $row['of_id']; ?>"><?php echo $row['of_codigo'].' - '.$row['of_descripcion']; ?></option>
																	<?php 	
																	}
																?>      
								                            </select>
														</td>
														<td style="width:30%;">
															<input class="form-control" type="text" name="p1" id="p1" value="0" onblur="javascript:suma_presupuesto();" onkeypress="if (this.value.length < 11) { return numerosDecimales(event);}else{return false; }" onpaste="return false">
														</td>
																
														</tr>

													</table>
												</div>

												<div id="capaexpansion2">
													<table class="table table-bordered" style="width:80%;" >
													<tr>
														<td style="width:10%;">
															Asig. 2
								                        </td>
								                        <td style="width:30%;">
															<select class="form-control" id="ff2" name="ff2" title="Seleccione Fuente Financiamiento">
								                                <option value="">Seleccione Fuente financiamiento </option>
								                                <?php 
																	foreach($ffi as $row){ ?>
																		<option value="<?php echo $row['ff_id']; ?>"><?php echo $row['ff_codigo'].' - '.$row['ff_descripcion']; ?></option>
																	<?php 	
																	}
																?>     
								                            </select>
								                        </td>
														<td style="width:30%;">
															<select class="form-control" id="fo2" name="fo2" title="Seleccione Organismo Financiador">
								                                <option value="">Seleccione Fuente Organismo </option>
								                                <?php 
																	foreach($fof as $row){ ?>
																		<option value="<?php echo $row['of_id']; ?>"><?php echo $row['of_codigo'].' - '.$row['of_descripcion']; ?></option>
																	<?php 	
																	}
																?>     
								                            </select>
														</td>
														<td style="width:30%;">
															<input class="form-control" type="text" name="p2" id="p2" value="0" onblur="javascript:suma_presupuesto();" onkeypress="if (this.value.length < 11) { return numerosDecimales(event);}else{return false; }" onpaste="return false">
														</td>
																
														</tr>

													</table>
												</div>

												<div id="capaexpansion3">
													<table class="table table-bordered" style="width:80%;" >
													<tr>
														<td style="width:10%;">
															Asig. 3
								                        </td>
								                        <td style="width:30%;">
															<select class="form-control" id="ff3" name="ff3" title="Seleccione fuente de financiamiento">
								                                <option value="">Seleccione Fuente financiamiento </option>
								                                <?php 
																	foreach($ffi as $row){ ?>
																		<option value="<?php echo $row['ff_id']; ?>"><?php echo $row['ff_codigo'].' - '.$row['ff_descripcion']; ?></option>
																	<?php 	
																	}
																?>       
								                            </select>
								                        </td>
														<td style="width:30%;">
															<select class="form-control" id="fo3" name="fo3" title="Seleccione fuente de organismo">
								                                <option value="">Seleccione Fuente Organismo </option>
								                               <?php 
																	foreach($fof as $row){ ?>
																		<option value="<?php echo $row['of_id']; ?>"><?php echo $row['of_codigo'].' - '.$row['of_descripcion']; ?></option>
																	<?php 	
																	}
																?>      
								                            </select>
														</td>
														<td style="width:30%;">
															<input class="form-control" type="text" name="p3" id="p3" value="0" onblur="javascript:suma_presupuesto();" onkeypress="if (this.value.length < 11) { return numerosDecimales(event);}else{return false; }" onpaste="return false">
														</td>
																
														</tr>

													</table>
												</div>

												<div id="capaexpansion4">
													<table class="table table-bordered" style="width:80%;" >
													<tr>
														<td style="width:10%;">
															Asig. 4
								                        </td>
								                        <td style="width:30%;">
															<select class="form-control" id="ff4" name="ff4" title="Seleccione fuente de financiamiento">
								                                <option value="">Seleccione Fuente financiamiento </option>
								                                <?php 
																	foreach($ffi as $row){ ?>
																		<option value="<?php echo $row['ff_id']; ?>"><?php echo $row['ff_codigo'].' - '.$row['ff_descripcion']; ?></option>
																	<?php 	
																	}
																?>       
								                            </select>
								                        </td>
														<td style="width:30%;">
															<select class="form-control" id="fo4" name="fo4" title="Seleccione fuente de organismo">
								                                <option value="">Seleccione Fuente Organismo </option>
								                                <?php 
																	foreach($fof as $row){ ?>
																		<option value="<?php echo $row['of_id']; ?>"><?php echo $row['of_codigo'].' - '.$row['of_descripcion']; ?></option>
																	<?php 	
																	}
																?>      
								                            </select>
														</td>
														<td style="width:30%;">
															<input class="form-control" type="text" name="p4" id="p4" value="0" onblur="javascript:suma_presupuesto();" onkeypress="if (this.value.length < 11) { return numerosDecimales(event);}else{return false; }" onpaste="return false">
														</td>
																
														</tr>

													</table>
												</div>

												<div id="capaexpansion5">
													<table class="table table-bordered" style="width:80%;" >
													<tr>
														<td style="width:10%;">
															Asig. 5
								                        </td>
								                        <td style="width:30%;">
															<select class="form-control" id="ff5" name="ff5" title="Seleccione fuente de financiamiento">
								                                <option value="">Seleccione Fuente financiamiento </option>
								                                <?php 
																	foreach($ffi as $row){ ?>
																		<option value="<?php echo $row['ff_id']; ?>"><?php echo $row['ff_codigo'].' - '.$row['ff_descripcion']; ?></option>
																	<?php 	
																	}
																?>       
								                            </select>
								                        </td>
														<td style="width:30%;">
															<select class="form-control" id="fo5" name="fo5" title="Seleccione fuente de organismo">
								                                <option value="">Seleccione Fuente Organismo </option>
								                                <?php 
																	foreach($fof as $row){ ?>
																		<option value="<?php echo $row['of_id']; ?>"><?php echo $row['of_codigo'].' - '.$row['of_descripcion']; ?></option>
																	<?php 	
																	}
																?>     
								                            </select>
														</td>
														<td style="width:30%;">
															<input class="form-control" type="text" name="p5" id="p5" value="0" onblur="javascript:suma_presupuesto();" onkeypress="if (this.value.length < 11) { return numerosDecimales(event);}else{return false; }" onpaste="return false">
														</td>
																
														</tr>

													</table>
												</div>

												<div id="capaexpansion6">
													<table class="table table-bordered" style="width:80%;" >
													<tr>
														<td style="width:10%;">
															Asig. 6
								                        </td>
								                        <td style="width:30%;">
															<select class="form-control" id="ff6" name="ff6" title="Seleccione fuente de financiamiento">
								                                <option value="">Seleccione Fuente financiamiento </option>
								                                <?php 
																	foreach($ffi as $row){ ?>
																		<option value="<?php echo $row['ff_id']; ?>"><?php echo $row['ff_codigo'].' - '.$row['ff_descripcion']; ?></option>
																	<?php 	
																	}
																?>       
								                            </select>
								                        </td>
														<td style="width:30%;">
															<select class="form-control" id="fo6" name="fo6" title="Seleccione fuente de organismo">
								                                <option value="">Seleccione Fuente Organismo </option>
								                                <?php 
																	foreach($fof as $row){ ?>
																		<option value="<?php echo $row['of_id']; ?>"><?php echo $row['of_codigo'].' - '.$row['of_descripcion']; ?></option>
																	<?php 	
																	}
																?>      
								                            </select>
														</td>
														<td style="width:30%;">
															<input class="form-control" type="text" name="p6" id="p6" value="0" onblur="javascript:suma_presupuesto();" onkeypress="if (this.value.length < 11) { return numerosDecimales(event);}else{return false; }" onpaste="return false">
														</td>
																
														</tr>

													</table>
												</div>

												<div id="capaexpansion7">
													<table class="table table-bordered" style="width:80%;" >
													<tr>
														<td style="width:10%;">
															Asig. 7
								                        </td>
								                        <td style="width:30%;">
															<select class="form-control" id="ff7" name="ff7" title="Seleccione fuente de financiamiento">
								                               <option value="">Seleccione Fuente financiamiento </option>
								                               <?php 
																	foreach($ffi as $row){ ?>
																		<option value="<?php echo $row['ff_id']; ?>"><?php echo $row['ff_codigo'].' - '.$row['ff_descripcion']; ?></option>
																	<?php 	
																	}
																?>       
								                            </select>
								                        </td>
														<td style="width:30%;">
															<select class="form-control" id="fo7" name="fo7" title="Seleccione fuente de organismo">
								                                <option value="">Seleccione Fuente Organismo </option>
								                                <?php 
																	foreach($fof as $row){ ?>
																		<option value="<?php echo $row['of_id']; ?>"><?php echo $row['of_codigo'].' - '.$row['of_descripcion']; ?></option>
																	<?php 	
																	}
																?>      
								                            </select>
														</td>
														<td style="width:30%;">
															<input class="form-control" type="text" name="p7" id="p7" value="0" onblur="javascript:suma_presupuesto();" onkeypress="if (this.value.length < 11) { return numerosDecimales(event);}else{return false; }" onpaste="return false">
														</td>
																
														</tr>

													</table>
												</div>

												<div id="capaexpansion8">
													<table class="table table-bordered" style="width:80%;" >
													<tr>
														<td style="width:10%;">
															Asig. 8
								                        </td>
								                        <td style="width:30%;">
															<select class="form-control" id="ff8" name="ff8" title="Seleccione fuente de financiamiento">
								                                <option value="">Seleccione Fuente financiamiento </option>
								                                <?php 
																	foreach($ffi as $row){ ?>
																		<option value="<?php echo $row['ff_id']; ?>"><?php echo $row['ff_codigo'].' - '.$row['ff_descripcion']; ?></option>
																	<?php 	
																	}
																?>       
								                            </select>
								                        </td>
														<td style="width:30%;">
															<select class="form-control" id="fo8" name="fo8" title="Seleccione fuente de organismo">
								                                <option value="">Seleccione Fuente Organismo </option>
								                                <?php 
																	foreach($fof as $row) { ?>
																		<option value="<?php echo $row['of_id']; ?>"><?php echo $row['of_codigo'].' - '.$row['of_descripcion']; ?></option>
																	<?php 	
																	}
																?>      
								                            </select>
														</td>
														<td style="width:30%;">
															<input class="form-control" type="text" name="p8" id="p8" value="0" onblur="javascript:suma_presupuesto();" onkeypress="if (this.value.length < 11) { return numerosDecimales(event);}else{return false; }" onpaste="return false">
														</td>
																
														</tr>

													</table>
												</div>

												<div id="capaexpansion9">
													<table class="table table-bordered" style="width:80%;" >
													<tr>
														<td style="width:10%;">
															Asig. 9
								                        </td>
								                        <td style="width:30%;">
															<select class="form-control" id="ff9" name="ff9" title="Seleccione fuente de financiamiento">
								                                <option value="">Seleccione Fuente financiamiento </option>
								                                <?php 
																	foreach($ffi as $row){ ?>
																		<option value="<?php echo $row['ff_id']; ?>"><?php echo $row['ff_codigo'].' - '.$row['ff_descripcion']; ?></option>
																	<?php 	
																	}
																?>       
								                            </select>
								                        </td>
														<td style="width:30%;">
															<select class="form-control" id="fo9" name="fo9" title="Seleccione fuente de organismo">
								                                <option value="">Seleccione Fuente Organismo </option>
								                                <?php 
																	foreach($fof as $row){ ?>
																		<option value="<?php echo $row['of_id']; ?>"><?php echo $row['of_codigo'].' - '.$row['of_descripcion']; ?></option>
																	<?php 	
																	}
																?>      
								                            </select>
														</td>
														<td style="width:30%;">
															<input class="form-control" type="text" name="p9" id="p9" value="0" onblur="javascript:suma_presupuesto();" onkeypress="if (this.value.length < 11) { return numerosDecimales(event);}else{return false; }" onpaste="return false">
														</td>
																
														</tr>

													</table>
												</div>

												<div id="capaexpansion10">
													<table class="table table-bordered" style="width:80%;" >
													<tr>
														<td style="width:10%;">
															Asig. 10
								                        </td>
								                        <td style="width:30%;">
															<select class="form-control" id="ff10" name="ff10" title="Seleccione fuente de financiamiento">
								                                <option value="">Seleccione Fuente financiamiento </option>
								                                <?php 
																	foreach($ffi as $row){ ?>
																		<option value="<?php echo $row['ff_id']; ?>"><?php echo $row['ff_codigo'].' - '.$row['ff_descripcion']; ?></option>
																	<?php 	
																	}
																?>       
								                            </select>
								                        </td>
														<td style="width:30%;">
															<select class="form-control" id="fo10" name="fo10" title="Seleccione fuente de organismo">
								                                <option value="">Seleccione Fuente Organismo </option>
								                               <?php 
																	foreach($fof as $row){ ?>
																		<option value="<?php echo $row['of_id']; ?>"><?php echo $row['of_codigo'].' - '.$row['of_descripcion']; ?></option>
																	<?php 	
																	}
																?>      
								                            </select>
														</td>
														<td style="width:30%;">
															<input class="form-control" type="text" name="p10" id="p10" value="0" onblur="javascript:suma_presupuesto();" onkeypress="if (this.value.length < 11) { return numerosDecimales(event);}else{return false; }" onpaste="return false">
														</td>
																
														</tr>

													</table>
												</div>
												
												<div >
													<table class="table table-bordered" style="width:80%;" >
													<tr>
														<td style="width:50%;">
																<div align="right">
																	<a href="<?php echo base_url().'index.php/admin/proy/list_proy_fin' ?>" class="btn btn-lg btn-default" title="Volver a Mis Proyectos ">CANCELAR</a>
																	<input type="button" value="GUARDAR PRESUPUESTO" class="btn btn-primary btn-lg" onclick="valida_envia()" title="GUARDAR DATOS">
																</div>
								                        </td>
													</tr>
													</table>
												</div>

											</center>

												</div>
												<?php
											}
											else {
												if($this->session->userdata("rol_id")==1){ ?>
													<center><a href="<?php echo base_url().'index.php/admin/proy/proyecto/'.$proyecto[0]['proy_id'].'/9' ?>"><font color="blue">NO SE TIENE REGISTRADO EL MONTO REQUERIDO DE LA GESTI&Oacute;N</font></a></center>
													<?php
		                        				}
		                        				else{ ?>
													<center><font color="red" size="2">NO SE TIENE REGISTRADO EL MONTO REQUERIDO DE LA GESTI&Oacute;N (CONTACTESE CON EL T&Eacute;CNICO DE UNIDAD EJECUTORA : RESP. <?php echo $proyecto[0]['fun_nombre'].' '.$proyecto[0]['fun_paterno'].' '.$proyecto[0]['fun_materno'];?>)</font></center>
													<?php
		                        				}
												
											}
										
										}
										else {
											if($this->session->userdata("rol_id")==1){ ?>
												<center><a href="<?php echo base_url().'index.php/admin/proy/edit/'.$proyecto[0]['proy_id'].'/1' ?>"><font color="blue">NO SE TIENE REGISTRADO EL MONTO PRESUPUESTO DEL PROGRAMA</font></a></center>
												<?php
	                        				}
	                        				else{ ?>
												<center><font color="red" size="2">NOSE TIENE REGISTRADO EL MONTO PRESUPUESTO DEL PROGRAMA (CONTACTESE CON EL T&Eacute;CNICO DE UNIDAD EJECUTORA : RESP. <?php echo $proyecto[0]['fun_nombre'].' '.$proyecto[0]['fun_paterno'].' '.$proyecto[0]['fun_materno'];?>)</font></center>
												<?php
	                        				}
										}
									?>
									

									</div>
									<!-- end widget content -->
				
								</div>
								<!-- end widget div -->
				
							</div>
							<!-- end widget -->
						</article>
						<!-- WIDGET END -->
					</form>
					</div>
				
				</section>

				<!-- end widget grid -->					
			</div>
			<!-- END MAIN CONTENT -->

		</div>
		<!-- END MAIN PANEL -->

		<!-- PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
	<script>
function expandir_formulario(){

if (document.formulario.numero.value == "1"){
	xDisplay('capaexpansion', 'block')
	xDisplay('capaexpansion2', 'none')
	xDisplay('capaexpansion3', 'none')
	xDisplay('capaexpansion4', 'none')
	xDisplay('capaexpansion5', 'none')
	xDisplay('capaexpansion6', 'none')
	xDisplay('capaexpansion7', 'none')
	xDisplay('capaexpansion8', 'none')
	xDisplay('capaexpansion9', 'none')
	xDisplay('capaexpansion10', 'none')
	xDisplay('capafinal', 'block')
 }

 if (document.formulario.numero.value == "2"){
	xDisplay('capaexpansion', 'block')
	xDisplay('capaexpansion2', 'block')
	xDisplay('capaexpansion3', 'none')
	xDisplay('capaexpansion4', 'none')
	xDisplay('capaexpansion5', 'none')
	xDisplay('capaexpansion6', 'none')
	xDisplay('capaexpansion7', 'none')
	xDisplay('capaexpansion8', 'none')
	xDisplay('capaexpansion9', 'none')
	xDisplay('capaexpansion10', 'none')
	xDisplay('capafinal', 'block')
 }
 if (document.formulario.numero.value == "3"){
	xDisplay('capaexpansion', 'block')
	xDisplay('capaexpansion2', 'block')
	xDisplay('capaexpansion3', 'block')
	xDisplay('capaexpansion4', 'none')
	xDisplay('capaexpansion5', 'none')
	xDisplay('capaexpansion6', 'none')
	xDisplay('capaexpansion7', 'none')
	xDisplay('capaexpansion8', 'none')
	xDisplay('capaexpansion9', 'none')
	xDisplay('capaexpansion10', 'none')
	xDisplay('capafinal', 'block')
 }
 if (document.formulario.numero.value == "4"){
	xDisplay('capaexpansion', 'block')
	xDisplay('capaexpansion2', 'block')
	xDisplay('capaexpansion3', 'block')
	xDisplay('capaexpansion4', 'block')
	xDisplay('capaexpansion5', 'none')
	xDisplay('capaexpansion6', 'none')
	xDisplay('capaexpansion7', 'none')
	xDisplay('capaexpansion8', 'none')
	xDisplay('capaexpansion9', 'none')
	xDisplay('capaexpansion10', 'none')
	xDisplay('capafinal', 'block')
 }
 if (document.formulario.numero.value == "5"){
	xDisplay('capaexpansion', 'block')
	xDisplay('capaexpansion2', 'block')
	xDisplay('capaexpansion3', 'block')
	xDisplay('capaexpansion4', 'block')
	xDisplay('capaexpansion5', 'block')
	xDisplay('capaexpansion6', 'none')
	xDisplay('capaexpansion7', 'none')
	xDisplay('capaexpansion8', 'none')
	xDisplay('capaexpansion9', 'none')
	xDisplay('capaexpansion10', 'none')
	xDisplay('capafinal', 'block')
 }
  if (document.formulario.numero.value == "6"){
	xDisplay('capaexpansion', 'block')
	xDisplay('capaexpansion2', 'block')
	xDisplay('capaexpansion3', 'block')
	xDisplay('capaexpansion4', 'block')
	xDisplay('capaexpansion5', 'block')
	xDisplay('capaexpansion6', 'block')
	xDisplay('capaexpansion7', 'none')
	xDisplay('capaexpansion8', 'none')
	xDisplay('capaexpansion9', 'none')
	xDisplay('capaexpansion10', 'none')
	xDisplay('capafinal', 'block')
 }
if (document.formulario.numero.value == "7"){
	xDisplay('capaexpansion', 'block')
	xDisplay('capaexpansion2', 'block')
	xDisplay('capaexpansion3', 'block')
	xDisplay('capaexpansion4', 'block')
	xDisplay('capaexpansion5', 'block')
	xDisplay('capaexpansion6', 'block')
	xDisplay('capaexpansion7', 'block')
	xDisplay('capaexpansion8', 'none')
	xDisplay('capaexpansion9', 'none')
	xDisplay('capaexpansion10', 'none')
	xDisplay('capafinal', 'block')
 }
 if (document.formulario.numero.value == "8"){
	xDisplay('capaexpansion', 'block')
	xDisplay('capaexpansion2', 'block')
	xDisplay('capaexpansion3', 'block')
	xDisplay('capaexpansion4', 'block')
	xDisplay('capaexpansion5', 'block')
	xDisplay('capaexpansion6', 'block')
	xDisplay('capaexpansion7', 'block')
	xDisplay('capaexpansion8', 'block')
	xDisplay('capaexpansion9', 'none')
	xDisplay('capaexpansion10', 'none')
	xDisplay('capafinal', 'block')
 }
 if (document.formulario.numero.value == "9"){
	xDisplay('capaexpansion', 'block')
	xDisplay('capaexpansion2', 'block')
	xDisplay('capaexpansion3', 'block')
	xDisplay('capaexpansion4', 'block')
	xDisplay('capaexpansion5', 'block')
	xDisplay('capaexpansion6', 'block')
	xDisplay('capaexpansion7', 'block')
	xDisplay('capaexpansion8', 'block')
	xDisplay('capaexpansion9', 'block')
	xDisplay('capaexpansion10', 'none')
	xDisplay('capafinal', 'block')
 }
 if (document.formulario.numero.value == "10"){
	xDisplay('capaexpansion', 'block')
	xDisplay('capaexpansion2', 'block')
	xDisplay('capaexpansion3', 'block')
	xDisplay('capaexpansion4', 'block')
	xDisplay('capaexpansion5', 'block')
	xDisplay('capaexpansion6', 'block')
	xDisplay('capaexpansion7', 'block')
	xDisplay('capaexpansion8', 'block')
	xDisplay('capaexpansion9', 'block')
	xDisplay('capaexpansion10', 'block')
	xDisplay('capafinal', 'block')
 }

}
    </script>
		<!-- END PAGE FOOTER -->
		<script type="text/javascript">
		//------------------------------------------
			function suma_presupuesto()
			{ 		
					pgestion = parseFloat($('[name="pg"]').val());
					a = parseFloat($('[name="p1"]').val());
					if(a<=pgestion) //// 1
					{
						$('[name="pta"]').val((a).toFixed(2) );
						b = parseFloat($('[name="p2"]').val());
				
						if(parseFloat(a+b)<=pgestion) //// 2
						{
							$('[name="pta"]').val((a+b).toFixed(2) );
							c = parseFloat($('[name="p3"]').val());
						
							if(parseFloat(a+b+c)<=pgestion) ///// 3
							{
								$('[name="pta"]').val((a+b+c).toFixed(2) );
								d = parseFloat($('[name="p4"]').val());
							

								if(parseFloat(a+b+c+d)<=pgestion) ////// 4
								{
									$('[name="pta"]').val((a+b+c+d).toFixed(2) );
									e = parseFloat($('[name="p5"]').val());
									
									if(parseFloat(a+b+c+d+e)<=pgestion) ////// 5
									{
										$('[name="pta"]').val((a+b+c+d+e).toFixed(2) );
										f = parseFloat($('[name="p6"]').val());
										
										if(parseFloat(a+b+c+d+e+f)<=pgestion) ////// 6
										{
											$('[name="pta"]').val((a+b+c+d+e+f).toFixed(2) );
											g = parseFloat($('[name="p7"]').val());
											
											if(parseFloat(a+b+c+d+e+f+g)<=pgestion) ////// 7
											{
												$('[name="pta"]').val((a+b+c+d+e+f+g).toFixed(2) );
												h = parseFloat($('[name="p8"]').val());

												if(parseFloat(a+b+c+d+e+f+g+h)<=pgestion) ////// 8
												{
													$('[name="pta"]').val((a+b+c+d+e+f+g+h).toFixed(2) );
													i = parseFloat($('[name="p9"]').val());

													if(parseFloat(a+b+c+d+e+f+g+h+i)<=pgestion) ////// 9
													{
														$('[name="pta"]').val((a+b+c+d+e+f+g+h+i).toFixed(2) );
														j = parseFloat($('[name="p10"]').val());

														if(parseFloat(a+b+c+d+e+f+g+h+i+j)<=pgestion) ////// 10
														{
															$('[name="pta"]').val((a+b+c+d+e+f+g+h+i+j).toFixed(2) );
															
														}
												
													}
													else
													{
														$('[name="p9"]').focus();//foco de inicio
													}
											
												}
												else
												{
													$('[name="p8"]').focus();//foco de inicio
												}
										
											}
											else
											{
												$('[name="p7"]').focus();//foco de inicio
											}
										}
										else
										{
											$('[name="p6"]').focus();//foco de inicio
										}
									}
									else
									{
										$('[name="p5"]').focus();//foco de inicio
									}
								}
								else
								{
									$('[name="p4"]').focus();//foco de inicio
								}
							}
							else
							{
								$('[name="p3"]').focus();//foco de inicio
							}
						}
						else
						{
							$('[name="p2"]').focus();//foco de inicio
						}
					}
					else
					{
						$('[name="p1"]').focus();//foco de inicio
					}

			}
		</script>

		<script>
            function valida_envia()
            { 
              
               valor=0;
               v_gestion=document.formulario.pg.value //// presupuesto gestion

               if(document.formulario.numero.value>=1) ///// 1 valor asignado
               {
               		if(document.formulario.ff1.value=="")
               		{
               			alert("Seleccione fuente de financiamiento 1") 
					    document.formulario.ff1.focus() 
					    return 0;
               		}
               		if(document.formulario.fo1.value=="")
               		{
               			alert("Seleccione Organismo Financiador 1") 
					    document.formulario.fo1.focus() 
					    return 0;
               		}
               		if(document.formulario.p1.value=="")
               		{
               			alert("Registre Valor del importe 1") 
					    document.formulario.p1.focus() 
					    return 0;
               		}
               		if(parseFloat(document.formulario.p1.value)>parseFloat(v_gestion))
               		{
               			alert("Valor en exceso en 1ra asignacion suman : "+document.formulario.p1.value+"Bs. y sobrepasa el presupuesto de la gestion") 
			            document.formulario.p1.focus() 
			            return 0; 
               		}
               	
               		 if(document.formulario.numero.value>=2) ///// 2 valor asignado
		               {
		               		if(document.formulario.ff2.value=="")
		               		{
		               			alert("Seleccione fuente de financiamiento 2") 
							    document.formulario.ff2.focus() 
							    return 0;
		               		}
		               		if(document.formulario.fo2.value=="")
		               		{
		               			alert("Seleccione Organismo Financiador 2") 
							    document.formulario.fo2.focus() 
							    return 0;
		               		}
		               		if(document.formulario.p2.value=="")
		               		{
		               			alert("Registre Valor del importe 2") 
							    document.formulario.p2.focus() 
							    return 0;
		               		}
		               		if(parseFloat(document.formulario.p1.value)+parseFloat(document.formulario.p2.value)>parseFloat(v_gestion))
		               		{
		             	 			alert("Valor en exceso en 1ra y 2da asignacion suman : "+(parseFloat(document.formulario.p1.value)+parseFloat(document.formulario.p2.value))+"Bs. y sobrepasa el presupuesto de la gestion") 
					          		document.formulario.p2.focus() 
					          		return 0; 
		               		}
		               		
		               		if(document.formulario.numero.value>=3) ///// 3 valor asignado
				               {
				               		if(document.formulario.ff3.value=="")
				               		{
				               			alert("Seleccione fuente de financiamiento 3") 
									    document.formulario.ff3.focus() 
									    return 0;
				               		}
				               		if(document.formulario.fo3.value=="")
				               		{
				               			alert("Seleccione Organismo Financiador 3") 
									    document.formulario.fo3.focus() 
									    return 0;
				               		}
				               		if(document.formulario.p3.value=="")
				               		{
				               			alert("Registre Valor del importe 3") 
									    document.formulario.p3.focus() 
									    return 0;
				               		}
				               		if(parseFloat(document.formulario.p1.value)+parseFloat(document.formulario.p2.value)+parseFloat(document.formulario.p3.value)>parseFloat(v_gestion))
				               		{
				            			alert("Valor en exceso en 1ra,2da asignacion y 3ra asignacion suman : "+(parseFloat(document.formulario.p1.value)+parseFloat(document.formulario.p2.value)+parseFloat(document.formulario.p3.value))+"Bs. , sobrepasa el presupuesto de la gestion") 
							          	document.formulario.p3.focus() 
							            return 0; 
				               		}

				               			if(document.formulario.numero.value>=4) ///// 4 valor asignado
							               {
							               		if(document.formulario.ff4.value=="")
							               		{
							               			alert("Seleccione fuente de financiamiento 4") 
												    document.formulario.ff4.focus() 
												    return 0;
							               		}
							               		if(document.formulario.fo4.value=="")
							               		{
							               			alert("Seleccione Organismo Financiador 4") 
												    document.formulario.fo4.focus() 
												    return 0;
							               		}
							               		if(document.formulario.p4.value=="")
							               		{
							               			alert("Registre Valor del importe 4") 
												    document.formulario.p4.focus() 
												    return 0;
							               		}
							               		if(parseFloat(document.formulario.p1.value)+parseFloat(document.formulario.p2.value)+parseFloat(document.formulario.p3.value)+parseFloat(document.formulario.p4.value)>parseFloat(v_gestion))
							               		{
							            			alert("Valor en exceso en 1ra,2da,3ra asignacion y 4ta asignacion suman : "+(parseFloat(document.formulario.p1.value)+parseFloat(document.formulario.p2.value)+parseFloat(document.formulario.p3.value)+parseFloat(document.formulario.p4.value))+"Bs. y sobrepasa el presupuesto de la gestion") 
										          	document.formulario.p4.focus() 
										            return 0; 
							               		}

							               			if(document.formulario.numero.value>=5) ///// 5 valor asignado
										               {
										               		if(document.formulario.ff5.value=="")
										               		{
										               			alert("Seleccione fuente de financiamiento 5") 
															    document.formulario.ff5.focus() 
															    return 0;
										               		}
										               		if(document.formulario.fo5.value=="")
										               		{
										               			alert("Seleccione Organismo Financiador 5") 
															    document.formulario.fo5.focus() 
															    return 0;
										               		}
										               		if(document.formulario.p5.value=="")
										               		{
										               			alert("Registre Valor del importe 5") 
															    document.formulario.p5.focus() 
															    return 0;
										               		}
										               		if(parseFloat(document.formulario.p1.value)+parseFloat(document.formulario.p2.value)+parseFloat(document.formulario.p3.value)+parseFloat(document.formulario.p4.value)+parseFloat(document.formulario.p5.value)>parseFloat(v_gestion))
										               		{
										            			alert("Valor en exceso en 1ra,2da,3ra,4ta y 5ta asignacion suman : "+(parseFloat(document.formulario.p1.value)+parseFloat(document.formulario.p2.value)+parseFloat(document.formulario.p3.value)+parseFloat(document.formulario.p4.value)+parseFloat(document.formulario.p5.value))+"Bs. y sobrepasa el presupuesto de la gestion") 
													          	document.formulario.p5.focus() 
													            return 0; 
										               		}

										               			if(document.formulario.numero.value>=6) ///// 6 valor asignado
													               {
													               		if(document.formulario.ff6.value=="")
													               		{
													               			alert("Seleccione fuente de financiamiento 6") 
																		    document.formulario.ff6.focus() 
																		    return 0;
													               		}
													               		if(document.formulario.fo6.value=="")
													               		{
													               			alert("Seleccione Organismo Financiador 6") 
																		    document.formulario.fo6.focus() 
																		    return 0;
													               		}
													               		if(document.formulario.p6.value=="")
													               		{
													               			alert("Registre Valor del importe 6") 
																		    document.formulario.p6.focus() 
																		    return 0;
													               		}
													               		if(parseFloat(document.formulario.p1.value)+parseFloat(document.formulario.p2.value)+parseFloat(document.formulario.p3.value)+parseFloat(document.formulario.p4.value)+parseFloat(document.formulario.p5.value)+parseFloat(document.formulario.p6.value)>parseFloat(v_gestion))
													               		{
													            			alert("Valor en exceso en 1ra,2da,3ra,4ta,5ta y 6ta asignacion suman : "+(parseFloat(document.formulario.p1.value)+parseFloat(document.formulario.p2.value)+parseFloat(document.formulario.p3.value)+parseFloat(document.formulario.p4.value)+parseFloat(document.formulario.p5.value)+parseFloat(document.formulario.p6.value))+"Bs. y sobrepasa el presupuesto de la gestion") 
																          	document.formulario.p6.focus() 
																            return 0; 
													               		}

													               			if(document.formulario.numero.value>=7) ///// 7 valor asignado
																               {
																               		if(document.formulario.ff7.value=="")
																               		{
																               			alert("Seleccione fuente de financiamiento 7") 
																					    document.formulario.ff7.focus() 
																					    return 0;
																               		}
																               		if(document.formulario.fo7.value=="")
																               		{
																               			alert("Seleccione Organismo Financiador 7") 
																					    document.formulario.fo7.focus() 
																					    return 0;
																               		}
																               		if(document.formulario.p7.value=="")
																               		{
																               			alert("Registre Valor del importe 7") 
																					    document.formulario.p7.focus() 
																					    return 0;
																               		}
																               		if(parseFloat(document.formulario.p1.value)+parseFloat(document.formulario.p2.value)+parseFloat(document.formulario.p3.value)+parseFloat(document.formulario.p4.value)+parseFloat(document.formulario.p5.value)+parseFloat(document.formulario.p6.value)+parseFloat(document.formulario.p7.value)>parseFloat(v_gestion))
																               		{
																            			alert("Valor en exceso en 1ra,2da,3ra,4ta,5ta,6to y 7ma asignacion suman : "+(parseFloat(document.formulario.p1.value)+parseFloat(document.formulario.p2.value)+parseFloat(document.formulario.p3.value)+parseFloat(document.formulario.p4.value)+parseFloat(document.formulario.p5.value)+parseFloat(document.formulario.p6.value)+parseFloat(document.formulario.p7.value))+"Bs. y sobrepasa el presupuesto de la gestion") 
																			          	document.formulario.p7.focus() 
																			            return 0; 
																               		}

																               			if(document.formulario.numero.value>=8) ///// 8 valor asignado
																			               {
																			               		if(document.formulario.ff8.value=="")
																			               		{
																			               			alert("Seleccione fuente de financiamiento 8") 
																								    document.formulario.ff8.focus() 
																								    return 0;
																			               		}
																			               		if(document.formulario.fo8.value=="")
																			               		{
																			               			alert("Seleccione Organismo Financiador 8") 
																								    document.formulario.fo8.focus() 
																								    return 0;
																			               		}
																			               		if(document.formulario.p8.value=="")
																			               		{
																			               			alert("Registre Valor del importe 8") 
																								    document.formulario.p8.focus() 
																								    return 0;
																			               		}
																			               		if(parseFloat(document.formulario.p1.value)+parseFloat(document.formulario.p2.value)+parseFloat(document.formulario.p3.value)+parseFloat(document.formulario.p4.value)+parseFloat(document.formulario.p5.value)+parseFloat(document.formulario.p6.value)+parseFloat(document.formulario.p7.value)+parseFloat(document.formulario.p8.value)>parseFloat(v_gestion))
																			               		{
																			            			alert("Valor en exceso en 1ra,2da,3ra,4ta,5ta,6to,7ma y 8va asignacion suman : "+(parseFloat(document.formulario.p1.value)+parseFloat(document.formulario.p2.value)+parseFloat(document.formulario.p3.value)+parseFloat(document.formulario.p4.value)+parseFloat(document.formulario.p5.value)+parseFloat(document.formulario.p6.value)+parseFloat(document.formulario.p7.value)+parseFloat(document.formulario.p8.value))+"Bs. y sobrepasa el presupuesto de la gestion") 
																						          	document.formulario.p8.focus() 
																						            return 0; 
																			               		}

																			               			if(document.formulario.numero.value>=9) ///// 9 valor asignado
																						               {
																						               		if(document.formulario.ff9.value=="")
																						               		{
																						               			alert("Seleccione fuente de financiamiento 9") 
																											    document.formulario.ff9.focus() 
																											    return 0;
																						               		}
																						               		if(document.formulario.fo9.value=="")
																						               		{
																						               			alert("Seleccione Organismo Financiador 9") 
																											    document.formulario.fo8.focus() 
																											    return 0;
																						               		}
																						               		if(document.formulario.p9.value=="")
																						               		{
																						               			alert("Registre Valor del importe 9") 
																											    document.formulario.p9.focus() 
																											    return 0;
																						               		}
																						               		if(parseFloat(document.formulario.p1.value)+parseFloat(document.formulario.p2.value)+parseFloat(document.formulario.p3.value)+parseFloat(document.formulario.p4.value)+parseFloat(document.formulario.p5.value)+parseFloat(document.formulario.p6.value)+parseFloat(document.formulario.p7.value)+parseFloat(document.formulario.p8.value)+parseFloat(document.formulario.p9.value)>parseFloat(v_gestion))
																						               		{
																						            			alert("Valor en exceso en 1ra,2da,3ra,4ta,5ta,6ta,7ma,8va y 9na asignacion suman : "+(parseFloat(document.formulario.p1.value)+parseFloat(document.formulario.p2.value)+parseFloat(document.formulario.p3.value)+parseFloat(document.formulario.p4.value)+parseFloat(document.formulario.p5.value)+parseFloat(document.formulario.p6.value)+parseFloat(document.formulario.p7.value)+parseFloat(document.formulario.p8.value)+parseFloat(document.formulario.p9.value))+"Bs. y sobrepasa el presupuesto de la gestion") 
																									          	document.formulario.p9.focus() 
																									            return 0; 
																						               		}

																						               		  if(document.formulario.numero.value>=10) ///// 10 valor asignado
																								               {
																								               		if(document.formulario.ff10.value=="")
																								               		{
																								               			alert("Seleccione fuente de financiamiento 10") 
																													    document.formulario.ff10.focus() 
																													    return 0;
																								               		}
																								               		if(document.formulario.fo10.value=="")
																								               		{
																								               			alert("Seleccione Organismo Financiador 10") 
																													    document.formulario.fo10.focus() 
																													    return 0;
																								               		}
																								               		if(document.formulario.p10.value=="")
																								               		{
																								               			alert("Registre Valor del importe 10") 
																													    document.formulario.p10.focus() 
																													    return 0;
																								               		}
																								               		if(parseFloat(document.formulario.p1.value)+parseFloat(document.formulario.p2.value)+parseFloat(document.formulario.p3.value)+parseFloat(document.formulario.p4.value)+parseFloat(document.formulario.p5.value)+parseFloat(document.formulario.p6.value)+parseFloat(document.formulario.p7.value)+parseFloat(document.formulario.p8.value)+parseFloat(document.formulario.p9.value)+parseFloat(document.formulario.p10.value)>parseFloat(v_gestion))
																								               		{
																								            			alert("Valor en exceso en 1ra,2da,3ra,4ta,5ta,6ta,7ma,8va,9na y 10ma asignacion suman : "+(parseFloat(document.formulario.p1.value)+parseFloat(document.formulario.p2.value)+parseFloat(document.formulario.p3.value)+parseFloat(document.formulario.p4.value)+parseFloat(document.formulario.p5.value)+parseFloat(document.formulario.p6.value)+parseFloat(document.formulario.p7.value)+parseFloat(document.formulario.p8.value)+parseFloat(document.formulario.p9.value)+parseFloat(document.formulario.p10.value))+"Bs. y sobrepasa el presupuesto de la gestion") 
																											          	document.formulario.p10.focus() 
																											            return 0; 
																								               		}
																								               		
																								               }
																						               		
																						               }
																			               		
																			               }
																               		
																               }
													               		
													               }
										               		
										               }
							               		
							               }
				               		
				               }
		               }
               }
               if(document.formulario.pta.value!=0)
               {
               		alert("Numero de asignacion seleccionados: "+document.formulario.numero.value)
                  	var OK = confirm("DESEA GUARDAR INFORMACION ?");
		                  if (OK) {
		                      document.formulario.submit(); 
		                    }
               }
               else
               {
               	alert('por favor realize el registro del monto asignado ')
               	document.formulario.p1.focus() 
				return 0;
               }   	
            }
          </script>

		<!--================================================== -->

		<script>
			if (!window.jQuery) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-2.0.2.min.js"><\/script>');
			}
		</script>

		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>

		<script src="<?php echo base_url(); ?>assets/js/session_time/jquery-idletimer.js"></script>
		<script src="<?php echo base_url();?>/assets/js/app.config.js"></script>

		<script src="<?php echo base_url(); ?>assets/js/mis_js/validacion_form.js"></script>
		<script src = "<?php echo base_url(); ?>mis_js/control_session.js"></script>
		<SCRIPT src="<?php echo base_url(); ?>mis_js/programacion/ejecucion/abm_ejecucion.js" type="text/javascript"></SCRIPT>
		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url();?>/assets/js/bootstrap/bootstrap.min.js"></script>
		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url();?>/assets/js/notification/SmartNotification.min.js"></script>
		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url();?>/assets/js/smartwidgets/jarvis.widget.min.js"></script>
		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url();?>/assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<!-- SPARKLINES -->
		<script src="<?php echo base_url();?>/assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/select2/select2.min.js"></script>
		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
		<!-- browser msie issue fix -->
		<script src="<?php echo base_url();?>/assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url();?>/assets/js/plugin/fastclick/fastclick.min.js"></script>
		<!-- Demo purpose only -->
		<script src="<?php echo base_url();?>/assets/js/demo.min.js"></script>
		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url();?>/assets/js/app.min.js"></script>
		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<!-- Voice command : plugin -->
		<script src="<?php echo base_url();?>/assets/js/speech/voicecommand.min.js"></script>
		<script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
		<script src="<?php echo base_url();?>/assets/js/plugin/fuelux/wizard/wizard.min.js"></script>

	</body>
</html>
