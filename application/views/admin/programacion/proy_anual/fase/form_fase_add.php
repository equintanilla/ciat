<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">--> 
		<title><?php echo $this->session->userdata('name')?></title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.core.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css" id="toggleCSS" />
        <style type="text/css">
            aside{background: #05678B;}
        </style>
        <style>
            .show-grid [class^="col-"] {
                padding-top: 10px;
                padding-bottom: 10px;
                background-color: rgba(61, 106, 124, 0.15);
                border: 1px solid rgba(61, 106, 124, 0.2);
            }
        
            .show-grid {
                margin-bottom: 15px;
            }
        </style>
	</head>
	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->

		<!-- HEADER -->
		<header id="header">
			<!-- pulled right: nav area -->
			<div class="pull-right">
				<!-- collapse menu button -->
				<div id="hide-menu" class="btn-header pull-right">
					<span> <a href="javascript:void(0);" data-action="toggleMenu" title="Menu"><i class="fa fa-reorder"></i></a> </span>
				</div>
				<!-- end collapse menu -->
				<!-- logout button -->
				<div id="logout" class="btn-header transparent pull-right">
					<span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Salir" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
				</div>
				<!-- end logout button -->
				<!-- search mobile button (this is hidden till mobile view port) -->
				<div id="search-mobile" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
				</div>
				<!-- end search mobile button -->
				<!-- fullscreen button -->
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Pantalla Completa"><i class="fa fa-arrows-alt"></i></a> </span>
				</div>
				<!-- end fullscreen button -->
			</div>
			<!-- end pulled right: nav area -->
		</header>
		<!-- END HEADER -->

		<!-- Left panel : Navigation area -->
		<aside id="left-panel">
			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                            <span>
                                <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                            </span>
						<i class="fa fa-angle-down"></i>
					</a> 
				</span>
			</div>

			<nav>
                <ul>
                    <li class="">
                        <a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MENÚ PRINCIPAL"><i
                                    class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
                    </li>
                    <li class="text-center">
                        <?php
                        if($proyecto[0]['proy_estado']==1){ ?>
                            <a href="<?php echo base_url().'index.php/admin/proy/list_proy' ?>" title="MIS PROYECTOS"><span class="menu-item-parent">PROGRAMACI&Oacute;N DEL POA</span></a>
                            <?php
                        }
                        elseif ($proyecto[0]['proy_estado']==2){ ?>
                            <a href="<?php echo base_url().'index.php/admin/proy/list_proy_poa' ?>" title="MIS PROYECTOS"><span class="menu-item-parent">PROGRAMACI&Oacute;N DEL POA</span></a>
                            <?php
                        }
                        ?>
                    </li>
                    <?php
                    if($nro_fase==1){
                        for($i=0;$i<count($enlaces);$i++){ ?>
                            <li>
                                <a href="#" >
                                    <i class="<?php echo $enlaces[$i]['o_image'];?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
                                <ul>
                                    <?php
                                    $submenu= $this->menu_modelo->get_Modulos_sub($enlaces[$i]['o_child']);
                                    foreach($submenu as $row) {
                                        ?>
                                        <li><a href="<?php echo base_url($row['o_url'])."/".$mod."/".$id_f[0]['id']."/".$id_f[0]['proy_id']; ?>"><?php echo $row['o_titulo']; ?></a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                            <?php
                        }
                    }
                    ?>
                </ul>
			</nav>
			<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>

		</aside>

		<!-- MAIN PANEL -->
		<div id="main" role="main">

			<!-- RIBBON -->
			<div id="ribbon">

				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>
				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<?php
					if($proyecto[0]['proy_estado']==1){ ?>
						<li><a href="<?php echo base_url().'index.php/admin/proy/list_proy' ?>" title="MIS PROYECTOS">Mis Operaciones</a></li><li><a href="<?php echo base_url().'index.php/admin/proy/list_proy' ?>">T&eacute;cnico de Unidad Ejecutora</a></li><li>Fases de la Operaci&oacute;n (Nuevo)</li>
						<?php
					}
					elseif ($proyecto[0]['proy_estado']==2){ ?>
						<li><a href="<?php echo base_url().'index.php/admin/proy/list_proy_poa' ?>" title="MIS PROYECTOS">Mis Operaciones</a></li><li><a href="<?php echo base_url().'index.php/admin/proy/list_proy_poa' ?>">T&eacute;cnico Validador POA</a></li><li>Fases de la Operaci&oacute;n (Nuevo)</li>
						<?php
					}
					?>
				</ol>
			</div>
			<!-- END RIBBON -->
			<?php
                $combo_fase="";
                $sql = pg_query("SELECT * FROM _fases WHERE fas_clase=100");
                    while($sql_p = pg_fetch_row($sql))
                    {
                        $combo_fase.= "<option value='".$sql_p[0]."'>".$sql_p[1]."</option>";
                    }
            ?>
			<!-- MAIN CONTENT -->
			<div id="content">
				<div class="row">
					<nav role="navigation" class="navbar navbar-default navbar-inverse">
				        <div class="navbar-header">
				            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
				                <span class="sr-only">Toggle navigation</span>
				                <span class="icon-bar"></span>
				                <span class="icon-bar"></span>
				                <span class="icon-bar"></span>
				            </button>
				        </div>
				 
				        <div id="navbarCollapse" class="collapse navbar-collapse">
				            <ul class="nav navbar-nav">
				                <li><a href="<?php echo base_url().'index.php/admin/proy/edit/'.$proyecto[0]['proy_id'].'/1' ?>"><font size="2">&nbsp;DATOS GENERALES&nbsp;</font></a></li>
				               	<li><a href="<?php echo base_url().'index.php/admin/proy/proyecto/'.$proyecto[0]['proy_id'].'/10' ?>" title="<?php echo strtoupper($titulo);?> DEL PROYECTO"><font size="2">&nbsp;<?php echo strtoupper($titulo);?>&nbsp;</font></a></li>
		                        <?php
		                        if($this->session->userdata("rol_id")==1){ ?>
		                        	<li><a href="<?php echo base_url().'index.php/admin/proy/proyecto/'.$proyecto[0]['proy_id'].'/2' ?>"><font size="2">&nbsp;RESPONSABLES&nbsp;</font></a></li><?php
		                        }
		                        ?>
		                        <li><a href="<?php echo base_url().'index.php/admin/proy/proyecto/'.$proyecto[0]['proy_id'].'/3' ?>"><font size="2">&nbsp;CLASIFICACI&Oacute;N&nbsp;</font></a></li>
		                        <li ><a href="<?php echo base_url().'index.php/admin/proy/proyecto/'.$proyecto[0]['proy_id'].'/4?qRegistro='.$proyecto[0]['proy_id'].'' ?>"><font size="2">&nbsp;LOCALIZACI&Oacute;N&nbsp;</font></a></li>
		                        <?php
                                  if($proyecto[0]['tp_id']==1 || $proyecto[0]['tp_id']==2 || $proyecto[0]['tp_id']==3){ ?>
                                    <li><a href="<?php echo base_url().'index.php/admin/proy/proyecto/'.$proyecto[0]['proy_id'].'/5' ?>" title="MARCO LOGICO"><font size="2">&nbsp;OBJETIVOS&nbsp;</font></a></li>
                                    <?php
                                  }
                        		?>
		                        <li><a href="<?php echo base_url().'index.php/admin/proy/proyecto/'.$proyecto[0]['proy_id'].'/7' ?>"><font size="2">&nbsp;RESUMEN T&Eacute;CNICO&nbsp;</font></a></li>
		                        <li><a href='<?php echo site_url("admin").'/proy/proyecto/'.$proyecto[0]['proy_id'].'/8'; ?>'><font size="2">ANEXOS DE LA OPERACI&Oacute;N</font></a></li>
				            	<li class="active"><a href='#' title="FASE ETAPA COMPONENTE"><i class="glyphicon glyphicon-ok"></i><font size="2">&nbsp;FASE / ETAPA&nbsp;</font></a></li>
				            </ul>
				        </div>
					</nav>
				</div>	
				
				<div class="row show-grid">
		            <div class="col-md-2">
		                <b><?php echo strtoupper($proyecto[0]['tipo']);?></b>
		            </div>
		            <div class="col-md-6">
		                <b><?php echo strtoupper($proyecto[0]['proy_nombre']);?></b>
		            </div>
		            <div class="col-md-2">
		                <b>FECHA DE INICIO : <?php echo date('d-m-Y',strtotime($proyecto[0]['f_inicial'])); ?></b>
		            </div>
		            <div class="col-md-2">
		                <b>FECHA DE CONCLUSI&Oacute;N : <?php echo date('d-m-Y',strtotime($proyecto[0]['f_final'])); ?></b>
		            </div>
		        </div>
				<!-- widget grid -->
				<section id="widget-grid" class="">
					<div class="row">
							<!-- Widget ID (each widget will need unique ID)-->
							<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="jarviswidget jarviswidget-color-darken" >
									<header>
										<span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
										<h2 class="font-md"><strong>FASE ETAPA (Nuevo)</strong></h2>				
									</header>
										<!-- widget content -->
									<div class="widget-body">
										<div class="row">
											<form name="form_fase" id="form_fase" method="post" action="<?php echo site_url("").'/programacion/faseetapa/add_fase'?>">
												<div id="" class="col-sm-12">
													<div class="well">
													<input  type="hidden" name="id" id="id" value="<?php echo $proyecto[0]['proy_id']?>">
													<input  type="hidden" name="fi" id="fi" value="<?php echo date('Y-m-d',strtotime($proyecto[0]['f_inicial'])); ?>">	
													<input  type="hidden" name="ff" id="ff" value="<?php echo date('Y-m-d',strtotime($proyecto[0]['f_final'])); ?>">
													<input  type="hidden" name="aper" id="aper" value="<?php echo $proyecto[0]['aper_id']?>">	
													<input  type="hidden" name="g1" id="g1">
													<input  type="hidden" name="g2" id="g2">		
													<input  type="hidden" name="tp" id="tp" value="0">
													<input  type="hidden" name="gi" id="gi" value="<?php echo $proyecto[0]['inicio'] ?>">
													<input  type="hidden" name="gf" id="gf" value="<?php echo $proyecto[0]['fin'] ?>">	
													<div class="row">
														<div class="col-sm-4">
															<div class="form-group">
																<label><font size="1"><b>SELECCIONE FASE </b></font><font color="blue">(Obligatorio)</font></label>
																<select name="fase" id="fase" class="form-control">
																	<option value="">SELECCIONE  FASE</option>
                                                     				<?php  echo $combo_fase;?>
																</select>
															</div>
														</div>

														<div class="col-sm-4">
															<div class="form-group">
																<label><font size="1"><b>SELECCIONE ETAPA </b></font><font color="blue">(Obligatorio)</font></label>
																<select class="form-control" id="etapas" name="etapas" value="<?php echo @set_value('et_id') ?>"> 
			                                                  	</select>
															</div>
														</div>
														<div class="col-sm-4">
															<div class="form-group">
																<label><font size="1"><b>UNIDAD EJECUTORA </b></font></label>
																<select class="select2" id="uni_ejec" name="uni_ejec">
					                                                <?php 
													                    foreach($unidad_org as $row)
													                    {
													                    	if($row['uni_id']==$f_top[0]['uni_ejec'])
													                    	{
													                    		?>
															                     <option value="<?php echo $row['uni_id']; ?>" selected><?php echo $row['uni_unidad']; ?></option>
															                    <?php 
													                    	}
													                    	else
													                    	{
													                    		?>
															                     <option value="<?php echo $row['uni_id']; ?>"><?php echo $row['uni_unidad']; ?></option>
															                    <?php 
													                    	}	
													                    }
													                ?>     
					                                            </select>
															</div>
														</div>
													</div>

													<div class="row">
														<div class="col-sm-12">
															<div class="form-group">
																<label><font size="1"><b>OBSERVACI&Oacute;N</b></font><font color="blue">(Opcional)</font></label>
																<textarea rows="5"  name="desc" id="desc" class="form-control" style="width:100%;"></textarea> 
															</div>
														</div>
													</div>

													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label><font size="1"><b>FECHA DE INICIO DE LA FASE </b></font></label><font color="blue"> (dd/mm/yyyy)</font>
																<div class="input-group">
																	<input type="text" name="f_inicio" id="f_inicio" placeholder="Seleccione Fecha inicial" class="form-control datepicker" data-dateformat="dd/mm/yy" value="<?php echo date('d/m/Y',strtotime($proyecto[0]['f_inicial'])); ?>" onKeyUp="this.value=formateafecha(this.value);">
																	<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
																</div>
															</div>
							
														</div>

														<div class="col-sm-6">
															<div class="form-group">
																<label><font size="1"><b>FECHA DE CONCLUSI&Oacute;N DE LA FASE </b></font></label><font color="blue"> (dd/mm/yyyy)</font>
																<div class="input-group">
																	<input type="text" name="f_final" id="f_final" placeholder="Seleccione Fecha final" class="form-control datepicker" data-dateformat="dd/mm/yy" value="<?php echo date('d/m/Y',strtotime($proyecto[0]['f_final'])); ?>" onKeyUp="this.value=formateafecha(this.value);">
																	<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
																</div>
															</div>
														</div>
													</div>

													<div class="row">
														<div class="col-sm-4">
															<div class="form-group">
																<label><font size="1"><b>FORMA DE EJECUCI&Oacute;N </b></font><font color="blue">(Obligatorio)</font></label>
																<select class="form-control" id="f_ejec" name="f_ejec">
			                                                        <option value=""> - - Seleccione - - </option>
			                                                        <option value="1">DIRECTA</option>
			                                                        <option value="2">DELEGADA</option>
			                                                  	</select>
															</div>
														</div>
														<div class="col-sm-4">
															<div class="form-group">
																<label><font size="1"><b>PRESUPUESTO FASE TOTAL </b></font><font color="blue"> (Obligatorio)</font></label>
																<input class="form-control" type="text" name="monto_total" id="monto_total" value="0" placeholder="0" onkeypress="if (this.value.length < 15) { return numerosDecimales(event);}else{return false; }" onpaste="return false">
															</div>
														</div>
														<div class="col-sm-4">
															<div class="form-group">
																<label><font size="1"><b>PRESUPUESTO TOTAL EJECUTADO </b></font><font color="blue"> (Automatico)</font></label>
																<input class="form-control" type="text" name="monto_ref" id="monto_ref" value="0" placeholder="0" onkeypress="if (this.value.length < 25) { return soloNumeros(event);}else{return false; }"  disabled="true">
															</div>
														</div>
													</div> <!-- end well -->
															
												</div>
												<div class="form-actions">
													<a href="<?php echo base_url().'index.php/admin/proy/fase_etapa/'.$proyecto[0]['proy_id'].'' ?>" class="btn btn-lg btn-default" title="VOLVER A MIS FASES"> CANCELAR </a>
													<input type="button" id="enviar_fase" name="enviar_fase" value="GUARDAR FASE Y GUARDAR ASIGNAR PRESUPUESTO" class="btn btn-primary btn-lg">
												</div>
											</form>
										</div>
				
									</div>
									<!-- end widget content -->
								</div>
								<!-- end widget div -->
							</article>
						</div>
							<!-- end widget -->
					</div>
					<!-- end row -->
				</section>
				<!-- end widget grid -->					
			</div>
			<!-- END MAIN CONTENT -->

		</div>
		<!-- END MAIN PANEL -->
		<!-- PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<script>
			if (!window.jQuery) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-2.0.2.min.js"><\/script>');
			}
		</script>

		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>

		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url(); ?>assets/js/session_time/jquery-idletimer.js"></script>
		<script src="<?php echo base_url();?>/assets/js/app.config.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/mis_js/validacion_form.js"></script>
		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url();?>/assets/js/bootstrap/bootstrap.min.js"></script>
		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url();?>/assets/js/notification/SmartNotification.min.js"></script>
		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url();?>/assets/js/smartwidgets/jarvis.widget.min.js"></script>
		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url();?>/assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<!-- SPARKLINES -->
		<script src="<?php echo base_url();?>/assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/select2/select2.min.js"></script>
		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
		<!-- browser msie issue fix -->
		<script src="<?php echo base_url();?>/assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url();?>/assets/js/plugin/fastclick/fastclick.min.js"></script>
		<!-- Demo purpose only -->
		<script src="<?php echo base_url();?>/assets/js/demo.min.js"></script>
		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url();?>/assets/js/app.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<script src="<?php echo base_url();?>/assets/js/speech/voicecommand.min.js"></script>
		<script type="text/javascript">
		$(document).ready(function() {
			pageSetUp();

			$("#fase").change(function () {
                $("#fase option:selected").each(function () {
                elegido=$(this).val();
                $.post("<?php echo base_url(); ?>index.php/admin/combo_fase_etapas", { elegido: elegido }, function(data){
                $("#etapas").html(data);
                });     
            });
            });    
		})
		</script>
		<script >
		$(function () {
			function reset() {
		            $("#toggleCSS").attr("href", "<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css");
		            alertify.set({
		                labels: {
		                    ok: "ACEPTAR",
		                    cancel: "CANCELAR"
		                },
		                delay: 5000,
		                buttonReverse: false,
		                buttonFocus: "ok"
		            });
		        }
		    $("#enviar_fase").on("click", function (e) {
		        var $validator = $("#form_fase").validate({
		            rules: {
		              fase: {
			        	 required: true, 
				      },
				      etapas: {	
				        required: true, 
				      },
				      uni_ejec: {	
				        required: true, 
				      },
				      f_inicio: {	
				        required: true,
				      },
				      f_final: {
				        required: true,
				      },
				      f_ejec: {	
				        required: true,
				      },
				      monto_total: {	
				        required: true,
				      }
		            },

		            messages: {
				      fase: "Seleccione Fase",
				      etapas: "Seleccione Etapa",
				      f_inicio: "Seleccione Fecha Inicio Fase",
				      f_final: "Seleccione Fecha Final Fase",
				      f_ejec: "Seleccione Forma de Ejecucion",
				      monto_total: "Registre Monto Total",
				    },

		            highlight: function (element) {
		                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		            },
		            unhighlight: function (element) {
		                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		            },
		            errorElement: 'span',
		            errorClass: 'help-block',
		            errorPlacement: function (error, element) {
		                if (element.parent('.input-group').length) {
		                    error.insertAfter(element.parent());
		                } else {
		                    error.insertAfter(element);
		                }
		            }
		        });
		        var $valid = $("#form_fase").valid();
		        if (!$valid) {
		            $validator.focusInvalid();
		        } 
		        else {
		        	var fecha_inicial = document.getElementById("f_inicio").value.split("/")  //fecha inicial
                	var fecha_final = document.getElementById("f_final").value.split("/")  /*fecha final*/

                	if((parseInt(fecha_inicial[2])<document.getElementById("gi").value || parseInt(fecha_inicial[2])>document.getElementById("gf").value)){
                		alertify.alert("ERROR!! LA FECHA INICIAL NOSE ENCUENTRA EN EL TIEMPO ESTIMADO DEL PROYECTO.. VERIFIQUE FECHA");
			            return false;
                	}
                	/*------------------------- Fecha Inicial ----------------------*/
                	if(parseInt(fecha_inicial[0])>'31' || parseInt(fecha_inicial[1])>'12' || parseInt(fecha_inicial[2])<'2008'){
                		alertify.alert("ERROR!! VERIFIQUE LA FECHA INICIAL DE LA FASE");
			            return false;
                	}
                	/*-----------------------------------------------------------------*/

              		if((parseInt(fecha_final[2])<document.getElementById("gi").value || parseInt(fecha_final[2])>document.getElementById("gf").value)){
                		alertify.alert("ERROR!! LA FECHA FINAL NOSE ENCUENTRA EN EL TIEMPO ESTIMADO DEL PROYECTO.. VERIFIQUE FECHA");
			            return false;
                	}
					/*------------------------------ Fecha Final ----------------------*/
                	if(parseInt(fecha_final[2])>'2027' || parseInt(fecha_final[0])>'31' || parseInt(fecha_final[1])>'12'){
                		alertify.alert("ERROR!! VERIFIQUE LA FECHA FINAL DE LA FASE");
			            return false;
                	}

                	document.getElementById('g1').value=fecha_inicial[2];
                  	document.getElementById('g2').value=fecha_final[2];
                    var proy_id = document.getElementById("id").value;
                    var fas_id = document.getElementById("fase").value;
                    var eta_id = document.getElementById("etapas").value;
                    var pfec_id = 0;
                    var url = "<?php echo site_url("admin")?>/proy/verif_fase_existe";
                    $.ajax({
                        type:"post",
                        url:url,
                        data:{proy_id:proy_id,fas_id:fas_id,eta_id:eta_id,pfec_id:pfec_id},
                        success:function(datos){

                            if(datos.trim() =='true'){
                                alertify.alert("LA FASE ETAPA SELECCIONADA YA SE ENCUENTRA REGISTRADA");
                                return false;
                            }else{
                                /*-----------------------------------------------------------------*/
                                reset();
                                alertify.confirm("GUARDAR FASE ETAPA?", function (a) {
                                    if (a) {
                                        document.form_fase.submit();
                                    } else {
                                        alertify.error("OPCI\u00D3N CANCELADA");
                                    }
                                });
                            }

                        }});










                	/*-----------------------------------------------------------------*/


		        }
		    });
		});
		</script>
	</body>
</html>
