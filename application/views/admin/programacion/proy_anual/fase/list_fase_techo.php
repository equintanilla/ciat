<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
		<title><?php echo $this->session->userdata('name')?></title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
	    <meta name="viewport" content="width=device-width">
	    <script>
	      function confirmar()
	      {
	        if(confirm('¿Estas seguro de Eliminar ?'))
	          return true;
	        else
	          return false;
	      }
	    </script>

	</head>
	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->
		<!-- HEADER -->
		<header id="header">
			<!-- pulled right: nav area -->
			<div class="pull-right">
				<!-- collapse menu button -->
				<div id="hide-menu" class="btn-header pull-right">
					<span> <a href="javascript:void(0);" data-action="toggleMenu" title="Menu"><i class="fa fa-reorder"></i></a> </span>
				</div>
				<!-- end collapse menu -->
				<!-- logout button -->
				<div id="logout" class="btn-header transparent pull-right">
					<span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Salir" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
				</div>
				<!-- end logout button -->
				<!-- search mobile button (this is hidden till mobile view port) -->
				<div id="search-mobile" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
				</div>
				<!-- end search mobile button -->
				<!-- fullscreen button -->
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Pantalla Completa"><i class="fa fa-arrows-alt"></i></a> </span>
				</div>
				<!-- end fullscreen button -->
			</div>
			<!-- end pulled right: nav area -->
		</header>
		<!-- END HEADER -->

		<!-- Left panel : Navigation area -->
		<aside id="left-panel">

			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                        <span>
                            <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                        </span>
						<i class="fa fa-angle-down"></i>
					</a> 
					
				</span>
			</div>

			<nav>
				<ul>
					<li class="">
	                <a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MEN&Uacute; PRINCIPAL">
	                <i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
	            	</li>
		            <li class="text-center">
		                <a href="<?php echo base_url().'index.php/admin/proy/list_proy_fin' ?>" title="MIS PROYECTOS"><span class="menu-item-parent">PROGRAMACI&Oacute;N DEL POA</span></a>
		            </li>
					<?php
		                for($i=0;$i<count($enlaces);$i++)
		                {
		                    if(count($subenlaces[$enlaces[$i]['o_child']])>0)
		                    {
		            ?>
		            <li>
		              	<a href="#" >
		              		<i class="<?php echo $enlaces[$i]['o_image']?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
		              	<ul >
		              	<?php
		                foreach ($subenlaces[$enlaces[$i]['o_child']] as $item) {
		                ?>
		                <li><a href="<?php echo base_url($item['o_url']); ?>"><?php echo $item['o_titulo']; ?></a></li>
		                <?php } ?>
		                </ul>
		            </li>
		            <?php 
		                    }
		            	} ?>
				</ul>
			</nav>
			<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>

		</aside>

		<!-- MAIN PANEL -->
		<div id="main" role="main">

			<!-- RIBBON -->
			<div id="ribbon">

				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>

				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li>Mis Operaciones</li><li><a href="<?php echo base_url().'index.php/admin/proy/list_proy_fin' ?>" title="MIS PROYECTOS">T&eacute;cnico Analista Financiero</a></li><li>Presupuesto Asignado</li>
				</ol>
			</div>
			<!-- END RIBBON -->
			<!-- MAIN CONTENT -->
			<div id="content">
				<?php
                  $attributes = array('class' => 'form-horizontal', 'id' => 'formulario','name' =>'formulario','enctype' => 'multipart/form-data');
                  echo validation_errors();
                  echo form_open('admin/proy/actualiza_techo_ptto', $attributes);
                ?>
				<!-- widget grid -->
				<section id="widget-grid" class="">
					<!-- row -->
					<div class="row">
						<!-- NEW WIDGET START -->
						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="jarviswidget jarviswidget-color-darken" >
								<header>
									<span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
									<h2 class="font-md"><strong>RECURSOS ASIGNADOS - <?php echo $this->session->userdata("gestion");?></strong></h2>				
								</header>
									<!-- widget content -->
								<div class="widget-body">
								<form id="formulario" name="formulario" novalidate="novalidate" method="post">
								<input class="form-control" type="hidden" name="id_p" id="id_p" value="<?php echo $proyecto[0]['proy_id'];?>"> <!-- Proy id -->
								<input class="form-control" type="hidden" name="id_f" id="id_f" value="<?php echo $fase[0]['id'];?>"> <!-- Fase Id -->
								<h2 class="alert alert-success"><center><?php echo $titulo.' - '.$proyecto[0]['proy_nombre'] ?></center></h2>
									<center>
									<table class="table table-bordered" style="width:80%;">
										<thead>			                
											<tr>
												<td style="width:20%;"><font size="1"><b>PRESUPUESTO TOTAL FASE</b></font></td>
												<td style="width:16%;"><input class="form-control" type="text" name="pt" id="pt" value="<?php echo number_format($fase[0]['pfec_ptto_fase'], 2, ',', '.') ?>" disabled="true"></td>
												<td style="width:20%;"><font size="1"><b>PRESPUESTO FASE GESTI&Oacute;N <?php echo $this->session->userdata("gestion");?>:</b></font></td>
												<td style="width:16%;"><input class="form-control" type="text" name="pg" id="pg" value="<?php echo number_format($fase_gest[0]['pfecg_ppto_total'], 2, ',', '.') ?>" disabled="true"></td>
											</tr>
										</thead>
									</table>
									</center><br>

									<div class="table-responsive" >
									<center>

										<b><?php echo " NRO. TOTAL DE RESCURSOS ASIGNADOS A LA OPERACI&Oacute;N : ".$nro;?></b><br>
										
						                <table class="table table-bordered" style="width:80%;" >
											<thead>			                
												<tr>
												<th>
													<?php 
									                  if($this->session->flashdata('success')){ ?>
									                    <div class="alert alert-success">
									                      <?php echo $this->session->flashdata('success'); ?>
									                    </div>
									                <?php 
									                    }
									                  elseif($this->session->flashdata('danger')){ ?>
									                      <div class="alert alert-danger">
									                        <?php echo $this->session->flashdata('danger'); ?>
									                      </div>
									                      <?php
									                  }
									                ?>
												</th>
												</tr>
											</thead>
										</table>
										<table class="table table-bordered" style="width:80%;" >
											<thead>			                
												<tr>
													<th style="width:10%;">
														<?php
														if($techo[0]['suma_techo']<$fase_gest[0]['pfecg_ppto_total']){
														?>
														<center><a href="#" data-toggle="modal" data-target="#modal_nuevo_ff" class="btn btn-xs nuevo_ff" title="NUEVO DE REGISTRO PRESUPUESTO"><img src="<?php echo base_url(); ?>assets/ifinal/2.png" WIDTH="35" HEIGHT="35"/><br>NUEVO REGISTRO</a></center>
														<?php
														}
														?>
													</th>
													<th style="width:30%;"><font size="2"><center>FUENTE FINANCIAMIENTO</center></font></th>
													<th style="width:30%;"><font size="2"><center>ORGANISMO FINANCIADOR</center></font></th>
													<th style="width:30%;" colspan="2"><font size="2"><center>IMPORTE</center></font></th>
												</tr>
											</thead>
										</table>

										<?php $nro=1; $monto=0;
			                                foreach($fase_asig as $row)
			                                {
			                                    $ff_ins = $this->model_faseetapa->fuente_insumo($row['ffofet_id']); 
			                                    ?>
			                                    	<table class="table table-bordered" style="width:80%;" >
													<tr>
														<td style="width:10%;">
															<input type="hidden" name="ffofet_id[]" id="ffofet_id[]" value="<?php echo $row['ffofet_id'] ?>"><br>
															<input type="hidden" name="nro[]" id="nro[]" value="<?php echo $nro ?>"><br>
															Asig. <?php echo $nro;?>
								                        </td>
								                        <td style="width:30%;">
								                        	<input type="hidden" name="ff[]" id="ff[]" value="<?php echo $row['ff_id'] ?>"><br>
															<?php echo $row['ff_codigo'].' - '.$row['ff_descripcion'];?>
								                        </td>
														<td style="width:30%;">
															<input type="hidden" name="of[]" id="of[]" value="<?php echo $row['of_id'] ?>"><br>
															<?php echo $row['of_codigo'].' - '.$row['of_descripcion'];?>
														</td>
														<td style="width:20%;">
															<input type="hidden" name="f_monto[]" id="f_monto[]" value="<?php echo $row['ffofet_monto'] ?>"><br>
															<?php echo number_format($row['ffofet_monto'], 2, ',', '.');?> Bs.
														</td>
														<td align="center">
															<a href="#" data-toggle="modal" data-target="#modal_mod_ff" class="btn btn-xs mod_ff" title="MODIFICAR DE REGISTRO METAS" name="<?php echo $row['ffofet_id']; ?>"><img src="<?php echo base_url(); ?>assets/ifinal/modificar.png" WIDTH="30" HEIGHT="30"/><br>Modificar</a><br>
															<?php 
																if($ff_ins==0) /// No tiene registrado en insumos
																{
																	?>
																	<a href="<?php echo base_url().'index.php/admin/proy/delete_recurso/'.$proyecto[0]['proy_id'].'/'.$fase_gest[0]['ptofecg_id'].'/'.$row['ffofet_id'].'/'.$row['nro'].'' ?>" title="ELIMINAR RECURSO" onclick="return confirmar()"><img src="<?php echo base_url(); ?>assets/ifinal/eliminar.png" WIDTH="30" HEIGHT="30"/><br><font size="1">Eliminar</font></a>
																	<?php
																}
															?>
															
														</td>
													</tr>
													</table>
			                                    <?php
			                                  $nro++; $monto=$monto+$row['ffofet_monto'];   	
			                                }

			                        	?>
			                        	<?php echo "<b><font color='blue'>Monto Total Asignado : ".number_format($monto, 2, ',', ' ')." Bs. </font></b>";?>
											<table class="table table-bordered" style="width:80%;" >
											<tr>
												<td style="width:50%;">
													<div align="right">
														<input type="button" value="ACTUALIZAR RECURSOS ASIGANDOS POR GESTIONES" id="btsubmit" class="btn btn-lg btn-default" onclick="valida()" title="GUARDAR REGISTRO">
														<a href="<?php echo base_url().'index.php/admin/proy/list_proy_fin' ?>" class="btn btn-lg btn-default" title="Volver a Mis Operaciones - Analista Financiero ">VOLVER A LISTA DE OPERACIONES</a>
													</div>
						                        </td>
											</tr>
											</table>

									</center>
									</div>
								</form>
								</div>
								<!-- end widget content -->
							</div>
						</article>
						<!-- WIDGET END -->
					</div>
				</section>
				<!-- end widget grid -->					
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN PANEL -->
		<script type="text/javascript">
		function valida(){
			var OK = confirm("ACTUALIZAR RECURSOS POR GESTIONES?");
	          if (OK) {
	              document.formulario.submit();
	              	document.getElementById("btsubmit").value = "ACTUALIZANDO...";
					document.getElementById("btsubmit").disabled = true;
					return true;
	            }
		}
		</script>
		<!-- ================== Modal NUEVO REGISTRO  ========================== -->
    <div class="modal animated fadeInDown" id="modal_nuevo_ff" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close text-danger " data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">
                        <!-- <img src="<?php echo base_url(); ?>assets/img/logo.png" width="150" alt="SmartAdmin"> -->
                    </h4>
                    <h4 class="modal-title text-center text-info">
                        <b><i class="glyphicon glyphicon-pencil"></i> NUEVO RECURSO</b>
                    </h4>
                </div>
                <div class="modal-body no-padding">
                    <div class="row">
                        <form id="form_ff" novalidate="novalidate" method="post">
                        <input class="form-control" type="hidden" name="proy_id" id="proy_id" value="<?php echo $proyecto[0]['proy_id']; ?>">
                        <input class="form-control" type="hidden" name="fase_id" id="fase_id" value="<?php echo $fase[0]['id']; ?>">
                        <input class="form-control" type="hidden" name="id_fg" id="id_fg" value="<?php echo $fase_gest[0]['ptofecg_id'] ?>">
                        <input class="form-control" type="hidden" name="suma_techo_nuevo" id="suma_techo_nuevo">
                        <input class="form-control" type="hidden" name="techo" id="techo" value="<?php echo $techo[0]['suma_techo'] ?>">
                        <input class="form-control" type="hidden" name="ptto_fase" id="ptto_fase" value="<?php echo $fase_gest[0]['pfecg_ppto_total'] ?>">
                            <div id="bootstrap-wizard-1" class="col-sm-12">
                                <div class="well">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <LABEL><b>FUENTE FINANCIAMIENTO</b></label>
                                                <select class="form-control" id="fi" name="fi" title="Seleccione Fuente Financiamiento" onblur="javascript:suma_presupuesto1();" >
					                            <?php 
													foreach($ffi as $row)
													{
													?>
														<option value="<?php echo $row['ff_id']; ?>"><?php echo $row['ff_codigo'].' - '.$row['ff_descripcion']; ?></option>
													<?php 	
													}
												?>       
					                            </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <LABEL><b>ORGANISMO FINANCIADOR</b></label>
                                                <select class="form-control" id="of" name="of" title="Seleccione Organismo Financiador" onblur="javascript:suma_presupuesto1();" >
					                            <?php 
													foreach($fof as $row)
													{
													?>
														<option value="<?php echo $row['of_id']; ?>"><?php echo $row['of_codigo'].' - '.$row['of_descripcion']; ?></option>
													<?php 	
													}
												?>       
					                            </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <LABEL><b>IMPORTE</b></label>
                                               <input class="form-control" type="text" name="importe" id="importe" placeholder="0" value="0" onblur="javascript:suma_presupuesto1();"  onkeypress="if (this.value.length < 20) { return numerosDecimales(event);}else{return false; }" >
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- end well -->
                            </div>
                        </form>
                    </div>
                </div>
                <script type="text/javascript">
						function suma_presupuesto1()
						{ 		
							ptto = parseFloat($('[name="techo"]').val());
							a = parseFloat($('[name="importe"]').val());
							$('[name="suma_techo_nuevo"]').val((ptto+a).toFixed(2) );
						}
				</script>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-3 pull-left">
                            <button class="btn btn-ms btn-danger" data-dismiss="modal">CANCELAR</button>
                        </div>
                        <div class="col-md-3 pull-right ">
                            <button type="submit" name="enviar_ff" id="enviar_ff" class="btn  btn-ms btn-primary"><i class="fa fa-save"></i>
                                GUARDAR
                            </button>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- ================== Modal  MODIFICAR  METAS========================== -->
	    <div class="modal animated fadeInDown" id="modal_mod_ff" tabindex="-1" role="dialog">
	        <div class="modal-dialog">
	            <div class="modal-content">
	                <div class="modal-header">
	                    <button type="button" class="close text-danger " data-dismiss="modal" aria-hidden="true">
	                        &times;
	                    </button>
	                    <h4 class="modal-title">
	                        <!-- <img src="<?php echo base_url(); ?>assets/img/logo.png" width="150" alt="SmartAdmin"> -->
	                    </h4>
	                    <h4 class="modal-title text-center text-info">
	                        <b><i class="glyphicon glyphicon-pencil"></i> MODIFICAR RECURSO</b>
	                    </h4>
	                </div>
	                <div class="modal-body no-padding">
	                    <div class="row">
	                        <form id="mod_formff" novalidate="novalidate" method="post">
	                            <div id="bootstrap-wizard-1" class="col-sm-12">
	                                <div class="well">
	                                	<input class="form-control" type="hidden" name="proy_id" id="proy_id" value="<?php echo $proyecto[0]['proy_id']; ?>">
                        				<input class="form-control" type="hidden" name="fase_id" id="fase_id" value="<?php echo $fase[0]['id']; ?>">
	                                    <input class="form-control" type="hidden" name="ffofet_id" id="ffofet_id" >
	                                    <input class="form-control" type="hidden" name="monto" id="monto" >
	                                    <input class="form-control" type="hidden" name="suma_techo_nuevo" id="suma_techo_nuevo" >
	                                    <input class="form-control" type="hidden" name="techo" id="techo" value="<?php echo $techo[0]['suma_techo'] ?>">
										<input class="form-control" type="hidden" name="ptto_fase" id="ptto_fase" value="<?php echo $fase_gest[0]['pfecg_ppto_total'] ?>">
	                                    <div class="row">
	                                        <div class="col-sm-6">
	                                            <div class="form-group">
	                                                <LABEL><b>FUENTE FINANCIAMIENTO</b></label>
	                                                <select class="form-control" id="ff_id" name="ff_id" title="Seleccione Fuente Financiamiento" onblur="javascript:suma_presupuesto();" >
						                            <?php 
														foreach($ffi as $row)
														{
														?>
															<option value="<?php echo $row['ff_id']; ?>"><?php echo $row['ff_codigo'].' - '.$row['ff_descripcion']; ?></option>
														<?php 	
														}
													?>       
						                            </select>
	                                            </div>
	                                        </div>
	                                        <div class="col-sm-6">
	                                            <div class="form-group">
	                                                <LABEL><b>ORGANISMO FINANCIADOR</b></label>
	                                                <select class="form-control" id="of_id" name="of_id" title="Seleccione Organismo Financiador" onblur="javascript:suma_presupuesto();" >
						                            <?php 
														foreach($fof as $row)
														{
														?>
															<option value="<?php echo $row['of_id']; ?>"><?php echo $row['of_codigo'].' - '.$row['of_descripcion']; ?></option>
														<?php 	
														}
													?>       
						                            </select>
	                                            </div>
	                                        </div>
	                                        <div class="col-sm-12">
	                                            <div class="form-group">
	                                                <LABEL><b>IMPORTE</b></label>
	                                               <input class="form-control" type="text" name="ffofet_monto" id="ffofet_monto" placeholder="0" value="0" onblur="javascript:suma_presupuesto();"  onkeypress="if (this.value.length < 20) { return numerosDecimales(event);}else{return false; }" >
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div> <!-- end well -->
	                            </div>
	                        </form>
	                    </div>
	                </div>
	                <script type="text/javascript">
						function suma_presupuesto()
						{ 		
							ptto = parseFloat($('[name="monto"]').val());
							a = parseFloat($('[name="ffofet_monto"]').val());
							$('[name="suma_techo_nuevo"]').val((ptto+a).toFixed(2) );
						}
					</script>
	                <div class="modal-footer">
	                    <div class="row">
	                        <div class="col-md-3 pull-left">
	                            <button class="btn btn-sm btn-danger" data-dismiss="modal">CANCELAR</button>
	                        </div>
	                        <div class="col-md-3 pull-right ">
	                            <button type="submit" name="mod_ffenviar" id="mod_ffenviar" class="btn  btn-sm btn-primary">
	                                <i class="fa fa-save"></i>
	                                ACEPTAR
	                            </button>
	                        </div>
	                    </div>
	                </div>
	            </div><!-- /.modal-content -->
	        </div><!-- /.modal-dialog -->
	    </div>
	    <!-- /.modal -->
	</div>
		<!-- PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
	
		<!-- END PAGE FOOTER -->
		<!--================================================== -->
		<script>
			if (!window.jQuery) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-2.0.2.min.js"><\/script>');
			}
		</script>

		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>
		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url(); ?>assets/js/session_time/jquery-idletimer.js"></script>
		<script src="<?php echo base_url();?>/assets/js/app.config.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/mis_js/validacion_form.js"></script>
		<script src = "<?php echo base_url(); ?>mis_js/control_session.js"></script>
		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url();?>/assets/js/bootstrap/bootstrap.min.js"></script>
		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url();?>/assets/js/notification/SmartNotification.min.js"></script>
		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url();?>/assets/js/smartwidgets/jarvis.widget.min.js"></script>
		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url();?>/assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<!-- SPARKLINES -->
		<script src="<?php echo base_url();?>/assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/select2/select2.min.js"></script>
		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
		<!-- browser msie issue fix -->
		<script src="<?php echo base_url();?>/assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url();?>/assets/js/plugin/fastclick/fastclick.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
		<!-- Demo purpose only -->
		<script src="<?php echo base_url();?>/assets/js/demo.min.js"></script>
		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url();?>/assets/js/app.min.js"></script>
		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<!-- Voice command : plugin -->
		<script src="<?php echo base_url();?>/assets/js/speech/voicecommand.min.js"></script>

		<!--================= NUEVO FUENTE FINANCIAMIENTO =========================================-->
		<script type="text/javascript">
		    $(function () {
		        var id_p = '';
		        $('#modal_nuevo_ff').on('hidden.bs.modal', function () {
		            document.forms['form_ff'].reset();

		        });
		        $("#enviar_ff").on("click", function (e) {
	            	monto = $(this).attr('id'); 
		            //========================VALIDANDO FORMULARIO===================
		            var $validator = $("#form_ff").validate({
		                //////////////// DATOS GENERALES
		                rules: {
		                    fi: { //// fuente financiamiento
		                        required: true,
		                    },
		                    of: { //// organismo financiador
		                        required: true,
		                    },
		                    importe: { //// importe
		                        required: true,
		                    }
		                },
		                messages: {
		                    fi: "selecione fuente de financiamaiento",
		                    of: "selecione organismo financiador",
		                    importe: "registre  importe",
		                    
		                },
		                highlight: function (element) {
		                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		                },
		                unhighlight: function (element) {
		                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		                },
		                errorElement: 'span',
		                errorClass: 'help-block',
		                errorPlacement: function (error, element) {
		                    if (element.parent('.input-group').length) {
		                        error.insertAfter(element.parent());
		                    } else {
		                        error.insertAfter(element);
		                    }
		                }
		            });
		            var $valid = $("#form_ff").valid();
		            if (!$valid) {
		                $validator.focusInvalid();
		                //return false;
		            } else {
		                //==========================================================
		                var suma = document.getElementById("suma_techo_nuevo").value;
	                    var techo = document.getElementById("ptto_fase").value;

		                var proy_id = document.getElementById("proy_id").value;
		                var fase_id = document.getElementById("fase_id").value;
		                var id_fg = document.getElementById("id_fg").value;
		                var ff_id = document.getElementById("fi").value;
	                    var of_id = document.getElementById("of").value;
	                    var ffofet_monto = document.getElementById("importe").value;
		                //=================== VERIFICAR SI EXISTE EL COD DE PROGRAMA ==============
		                if(parseFloat(suma)<=parseFloat(techo))
	                    {
	                    	var url = "<?php echo site_url("admin")?>/proy/add_ptto_techo";
		                            $.ajax({
		                                type: "post",
		                                url: url,
		                                data: {
		                                    p_id: proy_id,
		                                    f_id: fase_id,
		                                    id_fg: id_fg,
		                                    ff_id: ff_id,
		                                    of_id: of_id,
		                                    ffofet_monto: ffofet_monto
		                                },
		                                success: function (data) {
		                                    if (data == 'true') {
		                                        window.location.reload(true);
		                                    } else {
		                                        alert(data);
		                                    }
		                                }
		                            });
	                    }
	                    else
	                    {
	                    	alert('la suma para techo presupuestario supera al valor supera al monto fase.. verifique el monto')
	                    }
		                
		            }
		        });
		    });
		</script>

		<!-- ======================================== MODIFICAR TECHO PRESUPUESTARIO ==================================== -->
		<script type="text/javascript">
	    $(function () {
	        var id_m = '';
	        $(".mod_ff").on("click", function (e) {
	            //==========================LLENAR MIS DATOS DE FORMULARIO CON LA CLAVE RECIBIDA
	            id_ffofet = $(this).attr('name');
	            monto = $(this).attr('id'); 
	            suma_techo=document.getElementById("techo").value;
	            var url = "<?php echo site_url("admin")?>/proy/get_techo";
	            var codigo = '';
	            var request;
	            if (request) {
	                request.abort();
	            }
	            request = $.ajax({
	                url: url,
	                type: "POST",
	                dataType: 'json',
	                data:{id_ffofet:id_ffofet,suma_techo:suma_techo},

	            });

	            request.done(function (response, textStatus, jqXHR) { 

	                document.getElementById("ffofet_id").value = response.ffofet_id;
	                document.getElementById("ff_id").value = response.ff_id;
	                document.getElementById("of_id").value = response.of_id;
	                document.getElementById("monto").value = response.monto;
	                document.getElementById("ffofet_monto").value = response.ffofet_monto;

	            });
	            request.fail(function (jqXHR, textStatus, thrown) {
	                console.log("ERROR: " + textStatus);
	            });
	            request.always(function () {
	                //console.log("termino la ejecuicion de ajax");
	            });
	            e.preventDefault();
	            // =============================VALIDAR EL FORMULARIO DE MODIFICACION
	            $("#mod_ffenviar").on("click", function (e) {
	                var $validator = $("#mod_formff").validate({
	                   rules: {
	                    ff_id: { //// indicador
	                        required: true,
	                    },
	                    of_id: { //// indicador
	                        required: true,
	                    },
	                    ffofet_monto: { //// indicador
	                        required: true,
	                    }
	                },
	                messages: {
	                    ff_id: "Seleccione Fuente Financiamiento",
	                    of_id: "Selecciones Organismo Financiador",
	                    ffofet_monto: "Registre Importe",
	                    suma_techo_nuevo: {
	                    required: "registre monto",
	                    number: "Dato Inválido",
	                    max: "El dato debe ser menor o igual a <?php echo $fase_gest[0]['pfecg_ppto_total'] ?>"
                		},
	                    
	                },
	                    highlight: function (element) {
	                        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
	                    },
	                    unhighlight: function (element) {
	                        $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
	                    },
	                    errorElement: 'span',
	                    errorClass: 'help-block',
	                    errorPlacement: function (error, element) {
	                        if (element.parent('.input-group').length) {
	                            error.insertAfter(element.parent());
	                        } else {
	                            error.insertAfter(element);
	                        }
	                    }
	                });
	                var $valid = $("#mod_formff").valid();
	                if (!$valid) {
	                    $validator.focusInvalid();
	                } else {
	                    //==========================================================
	                    var suma = document.getElementById("suma_techo_nuevo").value;
	                    var techo = document.getElementById("ptto_fase").value;

	                    var proy_id = document.getElementById("proy_id").value;
		                var fase_id = document.getElementById("fase_id").value;

	                    var ffofet_id = document.getElementById("ffofet_id").value;
	                    var ff_id = document.getElementById("ff_id").value;
	                    var of_id = document.getElementById("of_id").value;
	                    var ffofet_monto = document.getElementById("ffofet_monto").value;
	                    if(parseFloat(suma)<=parseFloat(techo))
	                    {
	                    	var url = "<?php echo site_url("admin")?>/proy/update_techo";
		                    $.ajax({
		                        type: "post",
		                        url: url,
		                        data: {
		                            proy_id: proy_id,
		                            fase_id: fase_id,
		                            ffofet_id: ffofet_id,
		                            ff_id: ff_id,
		                            of_id: of_id,
		                            ffofet_monto: ffofet_monto
		                        },
		                        	success: function (data) {
                                    if (data == 'true') {
                                        window.location.reload(true);
                                    } else {
                                        alert(data);
                                    }
                                }
		                      
		                    });
	                    }
	                    else
	                    {
	                    	alert('la suma para techo presupuestario supera al valor supera al monto fase.. verifique el monto')
	                    }
	                    
	                }
	            });
	        });
	    });
	</script>
	<!-- =================================================================================================================== -->
	</body>

</html>
