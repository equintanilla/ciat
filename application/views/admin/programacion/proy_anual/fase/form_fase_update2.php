<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
		<title><?php echo $this->session->userdata('name')?></title>
		<meta name="description" content="">
		<meta name="author" content="">
			
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.core.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css" id="toggleCSS" />
        <style type="text/css">
            aside{background: #05678B;}
        </style>
		<style>
            .show-grid [class^="col-"] {
                padding-top: 10px;
                padding-bottom: 10px;
                background-color: rgba(61, 106, 124, 0.15);
                border: 1px solid rgba(61, 106, 124, 0.2);
            }
        
            .show-grid {
                margin-bottom: 15px;
            }
        </style>
	</head>
	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->
		<!-- HEADER -->
		<header id="header">
			<!-- pulled right: nav area -->
			<div class="pull-right">
				<!-- collapse menu button -->
				<div id="hide-menu" class="btn-header pull-right">
					<span> <a href="javascript:void(0);" data-action="toggleMenu" title="Menu"><i class="fa fa-reorder"></i></a> </span>
				</div>
				<!-- end collapse menu -->
				<!-- logout button -->
				<div id="logout" class="btn-header transparent pull-right">
					<span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Salir" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
				</div>
				<!-- end logout button -->
				<!-- search mobile button (this is hidden till mobile view port) -->
				<div id="search-mobile" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
				</div>
				<!-- end search mobile button -->
				<!-- fullscreen button -->
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Pantalla Completa"><i class="fa fa-arrows-alt"></i></a> </span>
				</div>
				<!-- end fullscreen button -->
			</div>
			<!-- end pulled right: nav area -->
		</header>
		<!-- END HEADER -->
		<!-- Left panel : Navigation area -->
		<aside id="left-panel">
			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                            <span>
                                <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                            </span>
						<i class="fa fa-angle-down"></i>
					</a> 
					
				</span>
			</div>

			<nav>
                <ul>
                    <li class="">
                        <a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MENÃš PRINCIPAL"><i
                                    class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
                    </li>
                    <li class="text-center">
                        <?php
                        if($proyecto[0]['proy_estado']==1){ ?>
                            <a href="<?php echo base_url().'index.php/admin/proy/list_proy' ?>" title="MIS PROYECTOS"><span class="menu-item-parent">PROGRAMACI&Oacute;N DEL POA</span></a>
                            <?php
                        }
                        elseif ($proyecto[0]['proy_estado']==2){ ?>
                            <a href="<?php echo base_url().'index.php/admin/proy/list_proy_poa' ?>" title="MIS PROYECTOS"><span class="menu-item-parent">PROGRAMACI&Oacute;N DEL POA</span></a>
                            <?php
                        }
                        ?>
                    </li>
                    <?php
                    if($nro_fase==1){
                        for($i=0;$i<count($enlaces);$i++){ ?>
                            <li>
                                <a href="#" >
                                    <i class="<?php echo $enlaces[$i]['o_image'];?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
                                <ul>
                                    <?php
                                    $submenu= $this->menu_modelo->get_Modulos_sub($enlaces[$i]['o_child']);
                                    foreach($submenu as $row) {
                                        ?>
                                        <li><a href="<?php echo base_url($row['o_url'])."/".$mod."/".$id_f[0]['id']."/".$id_f[0]['proy_id']; ?>"><?php echo $row['o_titulo']; ?></a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                            <?php
                        }
                    }
                    ?>
                </ul>
			</nav>
			<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>

		</aside>

		<!-- MAIN PANEL -->
		<div id="main" role="main">

			<!-- RIBBON -->
			<div id="ribbon">

				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>

				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<?php
			            if($proyecto[0]['tp_id']==1)
			            { if($proyecto[0]['proy_estado']==1){ ?>
					            <li>Mis Operaciones</li><li><a href="<?php echo base_url().'index.php/admin/proy/list_proy' ?>" title="MIS PROYECTOS">T&eacute;cnico de Unidad Ejecutora</a></li><li><a href="<?php echo base_url().'index.php/admin/proy/fase_etapa/'.$proyecto[0]['proy_id'] ?>" title="MIS FASES Y ETAPAS">Fases y Etapas</a></li><li>Presupuesto Requerido</li>
					            <?php
					        }
					       elseif ($proyecto[0]['proy_estado']==2){ ?>
					            <li>Mis Operaciones</li><li><a href="<?php echo base_url().'index.php/admin/proy/list_proy_poa' ?>" title="MIS PROYECTOS">T&eacute;cnico Analista POA</a></li><li><a href="<?php echo base_url().'index.php/admin/proy/fase_etapa/'.$proyecto[0]['proy_id'] ?>" title="MIS FASES Y ETAPAS">Fases y Etapas</a></li><li>Presupuesto Requerido</li>
					            <?php
					       }
			            }
			            elseif ($proyecto[0]['tp_id']==2 || $proyecto[0]['tp_id']==3 || $proyecto[0]['tp_id']==4)
			            { if($proyecto[0]['proy_estado']==1){ ?>
				                <li>Mis Operaciones</li><li><a href="<?php echo base_url().'index.php/admin/proy/list_proy' ?>" title="MIS PROYECTOS">T&eacute;cnico de Unidad Ejecutora</a></li><li>Presupuesto Requerido</li>
				                <?php
			        		}
			        		elseif ($proyecto[0]['proy_estado']==2){ ?>
				                <li>Mis Operaciones</li><li><a href="<?php echo base_url().'index.php/admin/proy/list_proy_poa' ?>" title="MIS PROYECTOS">T&eacute;cnico Analista POA</a></li><li>Presupuesto Requerido</li>
				                <?php
			        		}
			            	
			            }
			        ?>
				</ol>
			</div>
			<!-- END RIBBON -->

            <?php
              $attributes = array('class' => 'form-horizontal', 'id' => 'formulario','name' =>'formulario','enctype' => 'multipart/form-data');
              echo validation_errors();
              echo form_open('admin/proy/add_fe2', $attributes);
            ?>
			<!-- MAIN CONTENT -->
			<div id="content">
				<div class="row">
					<nav role="navigation" class="navbar navbar-default navbar-inverse">
				        <div class="navbar-header">
				            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
				                <span class="sr-only">Toggle navigation</span>
				                <span class="icon-bar"></span>
				                <span class="icon-bar"></span>
				                <span class="icon-bar"></span>
				            </button>
				        </div>
				 
				        <div id="navbarCollapse" class="collapse navbar-collapse">
				            <ul class="nav navbar-nav">
				                <li><a href="<?php echo base_url().'index.php/admin/proy/edit/'.$proyecto[0]['proy_id'].'/1' ?>"><font size="2">&nbsp;DATOS GENERALES&nbsp;</font></a></li>
				                <li><a href="<?php echo base_url().'index.php/admin/proy/proyecto/'.$proyecto[0]['proy_id'].'/10' ?>" title="<?php echo strtoupper($titulo);?> DEL PROYECTO"><font size="2">&nbsp;<?php echo strtoupper($titulo);?>&nbsp;</font></a></li>
				                <?php
		                        if($this->session->userdata("rol_id")==1){?>
		                        	<li><a href="<?php echo base_url().'index.php/admin/proy/proyecto/'.$proyecto[0]['proy_id'].'/2' ?>"><font size="2">&nbsp;RESPONSABLES&nbsp;</font></a></li><?php
		                        }
		                        ?>
				                <li><a href="<?php echo base_url().'index.php/admin/proy/proyecto/'.$proyecto[0]['proy_id'].'/3' ?>"><font size="2">&nbsp;CLASIFICACI&Oacute;N&nbsp;</font></a></li>
				                <li><a href="<?php echo base_url().'index.php/admin/proy/proyecto/'.$proyecto[0]['proy_id'].'/4?qRegistro='.$proyecto[0]['proy_id'].'' ?>"><font size="2">&nbsp;LOCALIZACI&Oacute;N&nbsp;</font></a></li>
				                <?php
		                            if($proyecto[0]['tp_id']==1 || $proyecto[0]['tp_id']==2 || $proyecto[0]['tp_id']==3){ ?>
		                                <li><a href="<?php echo base_url().'index.php/admin/proy/proyecto/'.$proyecto[0]['proy_id'].'/5' ?>" title="MARCO LOGICO"><font size="2">&nbsp;OBJETIVOS&nbsp;</font></a></li>
		                                <?php
		                            }
		                        ?>
				                <li><a href="<?php echo base_url().'index.php/admin/proy/proyecto/'.$proyecto[0]['proy_id'].'/7' ?>"><font size="2">&nbsp;RESUMEN T&Eacute;CNICO&nbsp;</font></a></li>
				                <li><a href='<?php echo site_url("admin").'/proy/proyecto/'.$proyecto[0]['proy_id'].'/8'; ?>'><font size="2">ANEXOS DE LA OPERACI&Oacute;N</font></a></li>
				            	<?php
			                        if($proyecto[0]['tp_id']==1){ ?>
			                          <li class="active"><a href='<?php echo site_url("admin").'/proy/fase_etapa/'.$proyecto[0]['proy_id']; ?>' title="FASE ETAPA COMPONENTE"><i class="glyphicon glyphicon-ok"></i><font size="2">&nbsp;FASE / ETAPA&nbsp;</font></a></li>
			                          <?php
			                        }
			                        elseif ($proyecto[0]['tp_id']==2 || $proyecto[0]['tp_id']==3 || $proyecto[0]['tp_id']==4){ ?>
			                          <li class="active"><a href="#" title="MONTO PRESUPUESTO"><i class="glyphicon glyphicon-ok"></i><font size="2">&nbsp;PRESUPUESTO&nbsp;</font></a></li>
			                          <?php
			                        }
			                    ?>
				            </ul>
				        </div>
					</nav>
				</div>	

        <div class="row">
            <div class="col-md-12">
                <h2 class="well well-sm text-justify" style="margin:0 0 7px 0 !important;">
					<?php echo strtoupper($proyecto[0]['tipo']);?>: 
					<span class="text-success"><?php echo strtoupper($proyecto[0]['proy_nombre']);?></span>
					<hr style="margin:2px auto; border-bottom:1px solid #eee;">
	                <small>
						<b class="lead">
						FECHA DE INICIO : <span class="text-primary"><?php echo date('d-m-Y',strtotime($proyecto[0]['f_inicial'])); ?></span>
						&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
						FECHA DE CONCLUSI&Oacute;N : <span class="text-primary"><?php echo date('d-m-Y',strtotime($proyecto[0]['f_final'])); ?></span>
						</b>
					</small>
				</h2>
            </div>
        </div>

				<!-- widget grid -->
					<div class="row">
					<?php 
						if($proyecto[0]['tp_id']==1)
						{
						?>
							<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								
								<div class="jarviswidget jarviswidget-color-darken" >
									<header>
										<span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
										<h2 class="font-md"><strong>DATOS RESUMEN DE FASE DE LA OPERACI&Oacute;N</strong></h2>				
									</header>
										<!-- widget content -->
									<div class="widget-body">
											
												<table class="table table-bordered" style="width:100%;">
												<thead>
													<tr>
														<th style="width:5%;"><font size="1">FASE</font></th>
														<th style="width:5%;"><font size="1">ETAPA</font></th>
														<th style="width:30%;"><font size="1">DESCRIPCI&Oacute;N_OBSERVACI&Oacute;N FASE</font></th>
														<th style="width:10%;"><font size="1">UNIDAD</font></th>
														<th style="width:10%;"><font size="1">F. INICIO</font></th>
														<th style="width:10%;"><font size="1">F. CONCLUSI&Oacute;N</font></th>
														<th style="width:10%;"><font size="1">Nuevo/Continuo</font></th>
														<th style="width:10%;"><font size="1">Anual/Plurianual</font></th>
														<th style="width:10%;"><font size="1">PRESUPUESTO ASIG.</font></th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td><font size="1"><?php echo $fase_proyecto[0]['fase'] ?></font></td>
														<td><font size="1"><?php echo $fase_proyecto[0]['etapa'] ?></font></td>
														<td><font size="1"><?php echo $fase_proyecto[0]['descripcion'] ?></font></td>
														<td><font size="1"><?php echo $fase_proyecto[0]['uni_unidad'] ?></td>
														<td><font size="1"><?php echo date('d-m-Y',strtotime($fase_proyecto[0]['inicio'])) ?></font></td>
														<td><font size="1"><?php echo date('d-m-Y',strtotime($fase_proyecto[0]['final'])) ?></font></td>
														<td><font size="1"><?php echo $this->model_faseetapa->calcula_nc($fase_proyecto[0]['pfec_fecha_inicio'])?></font></td>
														<td><font size="1"><?php echo $this->model_faseetapa->calcula_ap($fase_proyecto[0]['pfec_fecha_inicio'],$fase_proyecto[0]['pfec_fecha_fin']) ?></font></td>
														<td><font size="1"><input class="form-control" type="text" name="montos" id="montos" value="0"></font></td>
													</tr>
												</tbody>
											</table>
												
									</div>
								</div>
							</article>
							<?php
							}

							if($nro_fg!=0)
							{
							?>
							<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="jarviswidget jarviswidget-color-darken" >
									<header>
										<span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
										<h2 class="font-md"><strong>PROGRAMACI&Oacute;N PLURIANUAL : <?php echo $fase_proyecto[0]['pfec_fecha_inicio'] ?> - <?php echo $fase_proyecto[0]['pfec_fecha_fin'] ?></strong></h2>
									</header>
										<!-- widget content -->
									<div class="widget-body">
										<div class="row">
										<?php 
												if($fase_proyecto[0]['pfec_ptto_fase']!=0)
												{
												?>
											<form id="formulario"  name="formulario" novalidate="novalidate" method="post">
												<input class="form-control" type="hidden" name="id_p" value="<?php echo $proyecto[0]['proy_id']?>">
												<input class="form-control" type="hidden" name="tp_id" value="<?php echo $proyecto[0]['tp_id']?>">
												<input class="form-control" type="hidden" name="id_f" value="<?php echo $fase_proyecto[0]['id']?>">
												<input class="form-control" type="hidden" name="gest" id="gest" value="<?php echo $gestiones?>">
												<input class="form-control" type="hidden" name="gestion" id="gestion" value="<?php echo $fecha?>">
												<input class="form-control" type="hidden" name="ppt" id="ppt" value="<?php echo $fase_proyecto[0]['pfec_ptto_fase'] ?>">
												<input class="form-control" type="hidden" name="pe" id="pe" value="<?php echo $fase_proyecto[0]['pfec_ptto_fase_e'] ?>">	
												<input class="form-control" type="hidden" name="tp" id="tp" value="0">
												<input class="form-control" type="hidden" name="montos" id="montos" value="0">
												<input class="form-control" type="hidden" name="estado" id="estado" value="2"> <!-- estado MODIFICADO-->
												<input class="form-control" type="hidden" name="ep" id="ep" value="<?php echo $proyecto[0]['proy_estado'] ?>"><!-- estado del proyecto TOP,POA,FIN -->	
												<input class="form-control" type="hidden" name="gi" id="gi" value="<?php echo $fase_proyecto[0]['pfec_fecha_inicio']?>">
												<input class="form-control" type="hidden" name="gf" id="gf" value="<?php echo $fase_proyecto[0]['pfec_fecha_fin']?>">
												<div id="" class="col-sm-12">
												<?php
												if($proyecto[0]['tp_id']!=1)
												{
													?>
													<div class="well">
														<div class="row">
															<div class="col-sm-12">
																<div class="form-group">
																	<label><font size="1"><b>OBSERVACI&Oacute;N </b></font><font color="blue">(Opcional)</font></label>
																	<textarea rows="5"  name="desc" id="desc" class="form-control" style="width:100%;"><?php echo $fase_proyecto[0]['descripcion']?></textarea> 
																</div>
															</div>
														</div>
													</div><br>
													<?php
												}
												?>
													<h2 class="alert alert-success"><center>PRESUPUESTO REQUERIDO : Gesti&oacute;n <?php echo $fase_proyecto[0]['pfec_fecha_inicio'].' - '.$fase_proyecto[0]['pfec_fecha_fin'] ?></center></h2>
																<div class="well">
																	<div class="row">
																	<div class="col-sm-2">
																		<div class="form-group">
																			<label><font size="1"><b>PRESUPUESTO TOTAL </b></font></label>
																			<input class="form-control" type="text" value="<?php echo number_format($fase_proyecto[0]['pfec_ptto_fase'], 2, ',', '.') ?>" disabled="true">	
																		</div>
																	</div>
																		<?php 
																			if($gestiones>=1)
																			{
																				?>
																				<div class="col-sm-2">
																					<div class="form-group">
																						<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion1[0]['g_id'];?></b></font></label>
																						<input class="form-control" type="text" name="pt1" id="pt1" value="<?php echo number_format(round($fase_gestion1[0]['pfecg_ppto_total'],3),2,',','.') ?>" onblur="javascript:format_numero(this,2,',','.');" onpaste="return false">
																						<input  type="hidden" name="gs1" id="gs1" value="<?php echo $fase_gestion1[0]['g_id'];?>">
																					</div>
																				</div>

																				<?php
																				if($gestiones>=2)
																				{
																					?>
																						<div class="col-sm-2">
																							<div class="form-group">
																								<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion2[0]['g_id'];?></b></font></label>
																								<input class="form-control" type="text" name="pt2" id="pt2" value="<?php echo  number_format(round($fase_gestion2[0]['pfecg_ppto_total'],3),2,',','.') ?>" onblur="javascript:format_numero(this,2,',','.');" onpaste="return false">
											                                                  	<input  type="hidden" name="gs2" id="gs2" value="<?php echo $fase_gestion2[0]['g_id'];?>">
																							</div>
																						</div>
																					<?php
																					if($gestiones>=3)
																					{
																						?>
																							<div class="col-sm-2">
																								<div class="form-group">
																									<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion3[0]['g_id'];?></b></font></label>
																									<input class="form-control" type="text" name="pt3" id="pt3" value="<?php echo number_format(round($fase_gestion3[0]['pfecg_ppto_total'],3),2,',','.') ?>" onblur="javascript:format_numero(this,2,',','.');" onkeypress="if (this.value.length < 25) { return numerosDecimales(event);}else{return false; }" onpaste="return false">
																									<input  type="hidden" name="gs3" id="gs3" value="<?php echo $fase_gestion3[0]['g_id'];?>">
																								</div>
																							</div>
																					
																					<?php
																						if($gestiones>=4)
																						{
																							?>
																								<div class="col-sm-2">
																									<div class="form-group">
																										<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion4[0]['g_id'];?></b></font></label>
																										<input class="form-control" type="text" name="pt4" id="pt4" value="<?php echo number_format(round($fase_gestion4[0]['pfecg_ppto_total'],3),2,',','.') ?>" onblur="javascript:format_numero(this,2,',','.');" onkeypress="if (this.value.length < 25) { return numerosDecimales(event);}else{return false; }" onpaste="return false">
																										<input  type="hidden" name="gs4" id="gs4" value="<?php echo $fase_gestion4[0]['g_id'];?>">
																									</div>
																								</div>
																						
																						<?php
																							if($gestiones>=5)
																							{
																								?>
																									<div class="col-sm-2">
																										<div class="form-group">
																											<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion5[0]['g_id'];?></b></font></label>
																											<input class="form-control" type="text" name="pt5" id="pt5" value="<?php echo number_format(round($fase_gestion5[0]['pfecg_ppto_total'],3),2,',','.') ?>" onblur="javascript:format_numero(this,2,',','.');" onkeypress="if (this.value.length < 25) { return numerosDecimales(event);}else{return false; }" onpaste="return false" >
																											<input  type="hidden" name="gs5" id="gs5" value="<?php echo $fase_gestion5[0]['g_id'];?>">
																										</div>
																									</div>
																							
																							<?php
																								if($gestiones>=6)
																								{
																									?>
																										<div class="col-sm-2">
																											<div class="form-group">
																												<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion6[0]['g_id'];?></b></font></label>
																												<input class="form-control" type="text" name="pt6" id="pt6" value="<?php echo number_format(round($fase_gestion6[0]['pfecg_ppto_total'],3),2,',','.') ?>" onblur="javascript:format_numero(this,2,',','.');" onkeypress="if (this.value.length < 25) { return numerosDecimales(event);}else{return false; }" onpaste="return false">
																												<input  type="hidden" name="gs6" id="gs6" value="<?php echo $fase_gestion6[0]['g_id'];?>">
																											</div>
																										</div>

																								
																								<?php
																									if($gestiones>=7)
																									{
																										?>
																											<div class="col-sm-2">
																												<div class="form-group">
																													<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion7[0]['g_id'];?></b></font></label>
																													<input class="form-control" type="text" name="pt7" id="pt7" value="<?php echo number_format(round($fase_gestion7[0]['pfecg_ppto_total'],3),2,',','.') ?>" onblur="javascript:format_numero(this,2,',','.');" onkeypress="if (this.value.length < 25) { return numerosDecimales(event);}else{return false; }"  onpaste="return false">
																													<input  type="hidden" name="gs7" id="gs7" value="<?php echo $fase_gestion7[0]['g_id'];?>">
																												</div>
																											</div>
																									
																									<?php
																										if($gestiones>=8)
																										{
																											?>
																												<div class="col-sm-2">
																													<div class="form-group">
																														<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion8[0]['g_id'];?></b></font></label>
																														<input class="form-control" type="text" name="pt8" id="pt8" value="<?php echo number_format(round($fase_gestion8[0]['pfecg_ppto_total'],3),2,',','.') ?>" onblur="javascript:format_numero(this,2,',','.');" onkeypress="if (this.value.length < 25) { return numerosDecimales(event);}else{return false; }" onpaste="return false" >
																														<input  type="hidden" name="gs8" id="gs8" value="<?php echo $fase_gestion8[0]['g_id'];?>">
																													</div>
																												</div>
																										
																										<?php
																											if($gestiones>=9)
																											{
																												?>
																													<div class="col-sm-2">
																														<div class="form-group">
																															<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion9[0]['g_id'];?></b></font></label>
																															<input class="form-control" type="text" name="pt9" id="pt9" value="<?php echo number_format(round($fase_gestion9[0]['pfecg_ppto_total'],3),2,',','.') ?>" onblur="javascript:format_numero(this,2,',','.');" onkeypress="if (this.value.length < 25) { return numerosDecimales(event);}else{return false; }" onpaste="return false" >
																															<input  type="hidden" name="gs9" id="gs9" value="<?php echo $fase_gestion9[0]['g_id'];?>">
																														</div>
																													</div>
																											
																											<?php
																												if($gestiones>=10)
																												{
																													?>
																														<div class="col-sm-2">
																															<div class="form-group">
																																<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion10[0]['g_id'];?></b></font></label>
																																<input class="form-control" type="text" name="pt10" id="pt10" value="<?php echo number_format(round($fase_gestion10[0]['pfecg_ppto_total'],3),2,',','.') ?>" onblur="javascript:format_numero(this,2,',','.');" onkeypress="if (this.value.length < 25) { return numerosDecimales(event);}else{return false; }" onpaste="return false" >
																																<input  type="hidden" name="gs10" id="gs10" value="<?php echo $fase_gestion10[0]['g_id'];?>">
																															</div>
																														</div>
																												
																												<?php
																													if($gestiones>=11)
																													{
																														?>
																															<div class="col-sm-2">
																																<div class="form-group">
																																	<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion11[0]['g_id'];?></b></font></label>
																																	<input class="form-control" type="text" name="pt11" id="pt11" value="<?php echo number_format(round($fase_gestion11[0]['pfecg_ppto_total'],3),2,',','.') ?>" onblur="javascript:format_numero(this,2,',','.');" onkeypress="if (this.value.length < 25) { return numerosDecimales(event);}else{return false; }" onpaste="return false" >
																																	<input  type="hidden" name="gs11" id="gs11" value="<?php echo $fase_gestion11[0]['g_id'];?>">
																																</div>
																															</div>
																													
																													<?php
																														if($gestiones>=12)
																														{
																															?>
																																<div class="col-sm-2">
																																	<div class="form-group">
																																		<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion12[0]['g_id'];?></b></font></label>
																																		<input class="form-control" type="text" name="pt12" id="pt12" value="<?php echo number_format(round($fase_gestion12[0]['pfecg_ppto_total'],3),2,',','.') ?>" onblur="javascript:format_numero(this,2,',','.');" onkeypress="if (this.value.length < 25) { return numerosDecimales(event);}else{return false; }"  onpaste="return false">
																																	</div>
																																</div>
																														
																														<?php
																														}
																													}
																												}
																											}
																										}
																									}
																								}
																							}
																						}
																					}
																				}

																			}
																		?>
																		<div class="col-sm-2">
																		<div class="form-group">
																			<label><font size="1"><b>SALDO </b></font></label>
																			<input class="form-control" type="text" value="0" id="saldo" name="saldo" disabled="true">
																		</div>
																	</div>
																	</div>
																</div><br>

															

																<div class="well">
																	<div class="row">
																	<div class="col-sm-2">
																		<div class="form-group">
																			<label><font size="1"><b>PRESUPUESTO TOTAL EJECUTADO </b></font></label>
																			<input class="form-control" type="text" value="<?php echo number_format($fase_proyecto[0]['pfec_ptto_fase_e'], 2, ',', ' ') ?>" id="total" name="total" disabled="true">
																		</div>
																	</div>
																		<?php 
																			if($gestiones>=1)
																			{
																				?>
																				<div class="col-sm-2">
																					<div class="form-group">
																						<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion1[0]['g_id'];?></b></font></label>
																						<input class="form-control" type="text" name="pte1" id="pte1" value="<?php echo number_format(round($fase_gestion1[0]['pfecg_ppto_ejecutado'],3),2,',','.') ?>" onblur="javascript:format_numero(this,2,',','.');" onkeypress="if (this.value.length < 25) { return numerosDecimales(event);}else{return false; }" onpaste="return false" >
																					</div>
																				</div>

																				<?php
																				if($gestiones>=2)
																				{
																					?>
																						<div class="col-sm-2">
																							<div class="form-group">
																								<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion2[0]['g_id'];?></b></font></label>
																								<input class="form-control" type="text" name="pte2" id="pte2" value="<?php echo number_format(round($fase_gestion2[0]['pfecg_ppto_ejecutado'],3),2,',','.') ?>" onblur="javascript:format_numero(this,2,',','.');" onkeypress="if (this.value.length < 25) { return numerosDecimales(event);}else{return false; }" onpaste="return false">
																							</div>
																						</div>
																					<?php
																					if($gestiones>=3)
																					{
																						?>
																							<div class="col-sm-2">
																								<div class="form-group">
																									<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion3[0]['g_id'];?></b></font></label>
																									<input class="form-control" type="text" name="pte3" id="pte3" value="<?php echo number_format(round($fase_gestion3[0]['pfecg_ppto_ejecutado'],3),2,',','.') ?>" onblur="javascript:format_numero(this,2,',','.');" onkeypress="if (this.value.length < 25) { return numerosDecimales(event);}else{return false; }" onpaste="return false" >
																								</div>
																							</div>
																					
																					<?php
																						if($gestiones>=4)
																						{
																							?>
																								<div class="col-sm-2">
																									<div class="form-group">
																										<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion4[0]['g_id'];?></b></font></label>
																										<input class="form-control" type="text" name="pte4" id="pte4" value="<?php echo number_format(round($fase_gestion4[0]['pfecg_ppto_ejecutado'],3),2,',','.') ?>" onblur="javascript:format_numero(this,2,',','.');" onkeypress="if (this.value.length < 25) { return numerosDecimales(event);}else{return false; }" onpaste="return false">
																									</div>
																								</div>
																						
																						<?php
																							if($gestiones>=5)
																							{
																								?>
																									<div class="col-sm-2">
																										<div class="form-group">
																											<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion5[0]['g_id'];?></b></font></label>
																											<input class="form-control" type="text" name="pte5" id="pte5" value="<?php echo number_format(round($fase_gestion5[0]['pfecg_ppto_ejecutado'],3),2,',','.') ?>" onblur="javascript:format_numero(this,2,',','.');" onkeypress="if (this.value.length < 25) { return numerosDecimales(event);}else{return false; }" onpaste="return false">
																										</div>
																									</div>
																							
																							<?php
																								if($gestiones>=6)
																								{
																									?>
																										<div class="col-sm-2">
																											<div class="form-group">
																												<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion6[0]['g_id'];?></b></font></label>
																												<input class="form-control" type="text" name="pte6" id="pte6" value="<?php echo number_format(round($fase_gestion6[0]['pfecg_ppto_ejecutado'],3),2,',','.') ?>" onblur="javascript:format_numero(this,2,',','.');" onkeypress="if (this.value.length < 25) { return numerosDecimales(event);}else{return false; }"  onpaste="return false">
																											</div>
																										</div>

																								
																								<?php
																									if($gestiones>=7)
																									{
																										?>
																											<div class="col-sm-2">
																												<div class="form-group">
																													<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion7[0]['g_id'];?></b></font></label>
																													<input class="form-control" type="text" name="pte7" id="pte7" value="<?php echo number_format(round($fase_gestion7[0]['pfecg_ppto_ejecutado'],3),2,',','.') ?>" onblur="javascript:format_numero(this,2,',','.');" onkeypress="if (this.value.length < 25) { return numerosDecimales(event);}else{return false; }" onpaste="return false">
																												</div>
																											</div>
																									
																									<?php
																										if($gestiones>=8)
																										{
																											?>
																												<div class="col-sm-2">
																													<div class="form-group">
																														<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion8[0]['g_id'];?></b></font></label>
																														<input class="form-control" type="text" name="pte8" id="pte8" value="<?php echo number_format(round($fase_gestion8[0]['pfecg_ppto_ejecutado'],3),2,',','.') ?>" onblur="javascript:format_numero(this,2,',','.');" onkeypress="if (this.value.length < 25) { return numerosDecimales(event);}else{return false; }" onpaste="return false">
																													</div>
																												</div>
																										
																										<?php
																											if($gestiones>=9)
																											{
																												?>
																													<div class="col-sm-2">
																														<div class="form-group">
																															<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion9[0]['g_id'];?></b></font></label>
																															<input class="form-control" type="text" name="pte9" id="pte9" value="<?php echo number_format(round($fase_gestion9[0]['pfecg_ppto_ejecutado'],3),2,',','.') ?>" onblur="javascript:format_numero(this,2,',','.');" onkeypress="if (this.value.length < 25) { return numerosDecimales(event);}else{return false; }" onpaste="return false">
																														</div>
																													</div>
																											
																											<?php
																												if($gestiones>=10)
																												{
																													?>
																														<div class="col-sm-2">
																															<div class="form-group">
																																<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion10[0]['g_id'];?></b></font></label>
																																<input class="form-control" type="text" name="pte10" id="pte10" value="<?php echo number_format(round($fase_gestion10[0]['pfecg_ppto_ejecutado'],3),2,',','.') ?>" onblur="javascript:format_numero(this,2,',','.');" onkeypress="if (this.value.length < 25) { return numerosDecimales(event);}else{return false; }" onpaste="return false">
																															</div>
																														</div>
																												
																												<?php
																													if($gestiones>=11)
																													{
																														?>
																															<div class="col-sm-2">
																																<div class="form-group">
																																	<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion11[0]['g_id'];?></b></font></label>
																																	<input class="form-control" type="text" name="pte11" id="pte11" value="<?php echo number_format(round($fase_gestion11[0]['pfecg_ppto_ejecutado'],3),2,',','.') ?>" onblur="javascript:format_numero(this,2,',','.');" onkeypress="if (this.value.length < 25) { return numerosDecimales(event);}else{return false; }" onpaste="return false">
																																</div>
																															</div>
																													
																													<?php
																														if($gestiones>=12)
																														{
																															?>
																																<div class="col-sm-2">
																																	<div class="form-group">
																																		<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion12[0]['g_id'];?></b></font></label>
																																		<input class="form-control" type="text" name="pte12" id="pte12" value="<?php echo number_format(round($fase_gestion12[0]['pfecg_ppto_ejecutado'],3),2,',','.') ?>" onblur="javascript:format_numero(this,2,',','.');" onkeypress="if (this.value.length < 25) { return numerosDecimales(event);}else{return false; }" onpaste="return false">
																																	</div>
																																</div>
																														
																														<?php
																														}
																													}
																												}
																											}
																										}
																									}
																								}
																							}
																						}
																					}
																				}

																			}
																		?>

																	</div>
																</div>

															</div> <!-- end well -->
															
												</div>
												<div class="form-actions">
													<?php
											            if($proyecto[0]['tp_id']==1){
											            ?>
											                <a href="<?php echo site_url("admin").'/proy/update_f/'.$fase_proyecto[0]['id'].'/'.$proyecto[0]['proy_id'].'/1'; ?>" class="btn btn-lg btn-default" title="VOLVER A MIS FASES">VOLVER ATRAS </a>
											            <?php
											            }
											                elseif ($proyecto[0]['tp_id']==2 || $proyecto[0]['tp_id']==3 || $proyecto[0]['tp_id']==4) {
											            ?>
											                <a href="<?php echo base_url().'index.php/admin/proy/list_proy_poa' ?>" class="btn btn-lg btn-default" title="MIS PROYECTOS"> SALIR A MIS OPERACIONES</a>
											            <?php
											            }
											        ?>
																
														<input type="button" value="GUARDAR PRESUPUESTO" class="btn btn-primary btn-lg" onclick="valida_envia()" title="GUARDAR DATOS">
												</div>
											</form>
											<?php
												}
												elseif($fase_proyecto[0]['pfec_ptto_fase']==0)
												{
													?>
													<center><a href="<?php echo base_url().'index.php/admin/proy/edit/'.$proyecto[0]['proy_id'].'/1' ?>"><font color="blue">Registre Monto presupuesto del programa</font></a></center>
													<?php
												}
											?>
										</div>
				
									</div>
									<!-- end widget content -->
								</div>
								<!-- end widget div -->
							</article>
							<?php
							}
							elseif($nro_fg==0)
							{		
								?>
									<a href="<?php echo site_url("admin").'/proy/update_f/'.$fase_proyecto[0]['id'].'/'.$proyecto[0]['proy_id'].'/1'; ?>" title="VOLVER ATRAS"><font color="red">Error al registrar datos de gestion</font></a>
								<?php
							}
							?>
							</div>
							<!-- end widget -->
					</div>
				
					<!-- end row -->
				</section>
				<!-- end widget grid -->					
			</div>
			<!-- END MAIN CONTENT -->

		</div>
		<!-- END MAIN PANEL -->

		<!-- PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->
		<script type="text/javascript">
		//------------------------------------------
        function sin_puntos(numero){
            var n = numero.toString();
            var num=n.replace(/[.]/g,"");
            num=num.replace(/[,]/g,".");
            console.log('Num    '+num);
            return num;
        }
        function suma_presupuesto()
        {
            ptotal = parseFloat($('[name="ppt"]').val()); ///// Monto total fase
            if("<?php echo $gestiones?>">=1)
            {

                a1 = parseFloat(sin_puntos($('[name="pt1"]').val()));
                $('[name="montos"]').val((a1).toFixed(2) );
                $('[name="saldo"]').val((ptotal-(a1)).toFixed(2) );

                a = parseFloat(sin_puntos($('[name="pte1"]').val()));
                $('[name="total"]').val((a).toFixed(2) );
                $('[name="pe"]').val((a).toFixed(2) );
                if("<?php echo $gestiones?>">=2)
                {
                    b1 = parseFloat(sin_puntos($('[name="pt2"]').val()));
                    $('[name="montos"]').val((a1+b1).toFixed(2) );
                    $('[name="saldo"]').val((ptotal-(a1+b1)).toFixed(2) );

                    b = parseFloat(sin_puntos($('[name="pte2"]').val()));
                    $('[name="total"]').val((a+b).toFixed(2) );
                    $('[name="pe"]').val((a+b).toFixed(2) );

                    if("<?php echo $gestiones?>">=3)
                    {
                        c1 = parseFloat(sin_puntos($('[name="pt3"]').val()));
                        $('[name="montos"]').val((a1+b1+c1).toFixed(2) );
                        $('[name="saldo"]').val((ptotal-(a1+b1+c1)).toFixed(2) );

                        c = parseFloat(sin_puntos($('[name="pte3"]').val()));
                        $('[name="total"]').val((a+b+c).toFixed(2) );
                        $('[name="pe"]').val((a+b+c).toFixed(2) );

                        if("<?php echo $gestiones?>">=4)
                        {
                            d1 = parseFloat(sin_puntos($('[name="pt4"]').val()));
                            $('[name="montos"]').val((a1+b1+c1+d1).toFixed(2) );
                            $('[name="saldo"]').val((ptotal-(a1+b1+c1+d1)).toFixed(2) );

                            d = parseFloat($('[name="pte4"]').val());
                            $('[name="total"]').val((a+b+c+d).toFixed(2) );
                            $('[name="pe"]').val((a+b+c+d).toFixed(2) );

                            if("<?php echo $gestiones?>">=5)
                            {
                                e1 = parseFloat(sin_puntos($('[name="pt5"]').val()));
                                $('[name="montos"]').val((a1+b1+c1+d1+e1).toFixed(2) );
                                $('[name="saldo"]').val((ptotal-(a1+b1+c1+d1+e1)).toFixed(2) );

                                e = parseFloat(sin_puntos($('[name="pte5"]').val()));
                                $('[name="total"]').val((a+b+c+d+e).toFixed(2) );
                                $('[name="pe"]').val((a+b+c+d+e).toFixed(2) );

                                if("<?php echo $gestiones?>">=6)
                                {
                                    f1 = parseFloat(sin_puntos($('[name="pt6"]').val()));
                                    $('[name="montos"]').val((a1+b1+c1+d1+e1+f1).toFixed(2) );
                                    $('[name="saldo"]').val((ptotal-(a1+b1+c1+d1+e1+f1)).toFixed(2) );

                                    f = parseFloat(sin_puntos($('[name="pte6"]').val()));
                                    $('[name="total"]').val((a+b+c+d+e+f).toFixed(2) );
                                    $('[name="pe"]').val((a+b+c+d+e+f).toFixed(2) );


                                    if("<?php echo $gestiones?>">=7)
                                    {
                                        g1 = parseFloat(sin_puntos($('[name="pt7"]').val()));
                                        $('[name="montos"]').val((a1+b1+c1+d1+e1+f1+g1).toFixed(2) );
                                        $('[name="saldo"]').val((ptotal-(a1+b1+c1+d1+e1+f1+g1)).toFixed(2) );

                                        g = parseFloat(sin_puntos($('[name="pte7"]').val()));
                                        $('[name="total"]').val((a+b+c+d+e+f+g).toFixed(2) );
                                        $('[name="pe"]').val((a+b+c+d+e+f+g).toFixed(2) );


                                        if("<?php echo $gestiones?>">=8)
                                        {
                                            h1 = parseFloat(sin_puntos($('[name="pt8"]').val()));
                                            $('[name="montos"]').val((a1+b1+c1+d1+e1+f1+g1+h1).toFixed(2) );
                                            $('[name="saldo"]').val((ptotal-(a1+b1+c1+d1+e1+f1+g1+h1)).toFixed(2) );

                                            h = parseFloat(sin_puntos($('[name="pte8"]').val()));
                                            $('[name="total"]').val((a+b+c+d+e+f+g+h).toFixed(2) );
                                            $('[name="pe"]').val((a+b+c+d+e+f+g+h).toFixed(2) );

                                            if("<?php echo $gestiones?>">=9)
                                            {
                                                i1 = parseFloat(sin_puntos($('[name="pt9"]').val()));
                                                $('[name="montos"]').val((a1+b1+c1+d1+e1+f1+g1+h1+i1).toFixed(2) );
                                                $('[name="saldo"]').val((ptotal-(a1+b1+c1+d1+e1+f1+g1+h1+i1)).toFixed(2) );

                                                i = parseFloat(sin_puntos($('[name="pte9"]').val()));
                                                $('[name="total"]').val((a+b+c+d+e+f+g+h+i).toFixed(2) );
                                                $('[name="pe"]').val((a+b+c+d+e+f+g+h+i).toFixed(2) );

                                                if("<?php echo $gestiones?>">=10)
                                                {
                                                    j1 = parseFloat(sin_puntos($('[name="pt10"]').val()));
                                                    $('[name="montos"]').val((a1+b1+c1+d1+e1+f1+g1+h1+i1+j1).toFixed(2) );
                                                    $('[name="saldo"]').val((ptotal-(a1+b1+c1+d1+e1+f1+g1+h1+i1+j1)).toFixed(2) );

                                                    j = parseFloat(sin_puntos($('[name="pte10"]').val()));
                                                    $('[name="total"]').val((a+b+c+d+e+f+g+h+i+j).toFixed(2) );
                                                    $('[name="pe"]').val((a+b+c+d+e+f+g+h+i+j).toFixed(2) );

                                                    if("<?php echo $gestiones?>">=11)
                                                    {
                                                        k1 = parseFloat(sin_puntos($('[name="pt11"]').val()));
                                                        $('[name="montos"]').val((a1+b1+c1+d1+e1+f1+g1+h1+i1+j1+k1).toFixed(2) );
                                                        $('[name="saldo"]').val((ptotal-(a1+b1+c1+d1+e1+f1+g1+h1+i1+j1+k1)).toFixed(2) );

                                                        k = parseFloat(sin_puntos($('[name="pte11"]').val()));
                                                        $('[name="total"]').val((a+b+c+d+e+f+g+h+i+j+k).toFixed(2) );
                                                        $('[name="pe"]').val((a+b+c+d+e+f+g+h+i+j+k).toFixed(2) );

                                                        if("<?php echo $gestiones?>">=12)
                                                        {
                                                            l1 = parseFloat(sin_puntos($('[name="pt12"]').val()));
                                                            $('[name="montos"]').val((a1+b1+c1+d1+e1+f1+g1+h1+i1+j1+k1+l1).toFixed(2) );
                                                            $('[name="saldo"]').val((ptotal-(a1+b1+c1+d1+e1+f1+g1+h1+i1+j1+k1+l1)).toFixed(2) );

                                                            l = parseFloat(sin_puntos($('[name="pte12"]').val()));
                                                            $('[name="total"]').val((a+b+c+d+e+f+g+h+i+j+k+l).toFixed(2) );
                                                            $('[name="pe"]').val((a+b+c+d+e+f+g+h+i+j+k+l).toFixed(2) );

                                                        }

                                                    }

                                                }
                                            }

                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }
            format_totales(saldo,2,',','.');
            format_totales(total,2,',','.');
        }
		</script>
        <script type="text/javascript">
        function format_numero(input, decimales, separadorDecimal, separadorMiles) {
            suma_presupuesto();
            var partes, array;
            var numero = input.value;

            if ( !isFinite(numero) || isNaN(numero = parseFloat(numero)) ) {
                return "";
            }
            if (typeof separadorDecimal==="undefined") {
                separadorDecimal = ",";
            }
            if (typeof separadorMiles==="undefined") {
                separadorMiles = "";
            }

            // Redondeamos
            if ( !isNaN(parseInt(decimales)) ) {
                if (decimales >= 0) {
                    numero = numero.toFixed(decimales);
                } else {
                    numero = (
                        Math.round(numero / Math.pow(10, Math.abs(decimales))) * Math.pow(10, Math.abs(decimales))
                    ).toFixed();
                }
            } else {
                numero = numero.toString();
            }

            // Damos formato
            partes = numero.split(".", 2);
            array = partes[0].split("");
            for (var i=array.length-3; i>0 && array[i-1]!=="-"; i-=3) {
                array.splice(i, 0, separadorMiles);
            }
            numero = array.join("");

            if (partes.length>1) {
                numero += separadorDecimal + partes[1];
            }
            input.value = numero;

        }
        </script>
        <script type="text/javascript">
            function format_totales(input,decimales, separadorDecimal, separadorMiles) {
                var partes, array;
                var numero = input.value;
                if ( !isFinite(numero) || isNaN(numero = parseFloat(numero)) ) {
                    return "";
                }
                if (typeof separadorDecimal==="undefined") {
                    separadorDecimal = ",";
                }
                if (typeof separadorMiles==="undefined") {
                    separadorMiles = "";
                }

                // Redondeamos
                if ( !isNaN(parseInt(decimales)) ) {
                    if (decimales >= 0) {
                        numero = numero.toFixed(decimales);
                    } else {
                        numero = (
                            Math.round(numero / Math.pow(10, Math.abs(decimales))) * Math.pow(10, Math.abs(decimales))
                        ).toFixed();
                    }
                } else {
                    numero = numero.toString();
                }

                // Damos formato
                partes = numero.split(".", 2);
                array = partes[0].split("");
                for (var i=array.length-3; i>0 && array[i-1]!=="-"; i-=3) {
                    array.splice(i, 0, separadorMiles);
                }
                numero = array.join("");

                if (partes.length>1) {
                    numero += separadorDecimal + partes[1];
                }
                input.value=numero;

            }
        </script>
		<script>
			if (!window.jQuery) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-2.0.2.min.js"><\/script>');
			}
		</script>
		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>
		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url(); ?>assets/js/session_time/jquery-idletimer.js"></script>
		<script src="<?php echo base_url();?>/assets/js/app.config.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/mis_js/validacion_form.js"></script>
		<SCRIPT src="<?php echo base_url(); ?>mis_js/programacion/programacion/datos_generales.js" type="text/javascript"></SCRIPT>
		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url();?>/assets/js/bootstrap/bootstrap.min.js"></script>
		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url();?>/assets/js/notification/SmartNotification.min.js"></script>
		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url();?>/assets/js/smartwidgets/jarvis.widget.min.js"></script>
		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url();?>/assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<!-- SPARKLINES -->
		<script src="<?php echo base_url();?>/assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/select2/select2.min.js"></script>
		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
		<!-- browser msie issue fix -->
		<script src="<?php echo base_url();?>/assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url();?>/assets/js/plugin/fastclick/fastclick.min.js"></script>
		<!-- Demo purpose only -->
		<script src="<?php echo base_url();?>/assets/js/demo.min.js"></script>
		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url();?>/assets/js/app.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<!-- Voice command : plugin -->
		<script src="<?php echo base_url();?>/assets/js/speech/voicecommand.min.js"></script>

	</body>
</html>
