<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><?php echo $this->session->userdata('name')?></title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.core.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css" id="toggleCSS" />
	</head>
	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->
		<!-- HEADER -->
		<header id="header">
			<!-- pulled right: nav area -->
			<div class="pull-right">
				<!-- collapse menu button -->
				<div id="hide-menu" class="btn-header pull-right">
					<span> <a href="javascript:void(0);" data-action="toggleMenu" title="Menu"><i class="fa fa-reorder"></i></a> </span>
				</div>
				<!-- end collapse menu -->
				<!-- logout button -->
				<div id="logout" class="btn-header transparent pull-right">
					<span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Salir" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
				</div>
				<!-- end logout button -->
				<!-- search mobile button (this is hidden till mobile view port) -->
				<div id="search-mobile" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
				</div>
				<!-- end search mobile button -->
				<!-- fullscreen button -->
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Pantalla Completa"><i class="fa fa-arrows-alt"></i></a> </span>
				</div>
				<!-- end fullscreen button -->
			</div>
			<!-- end pulled right: nav area -->
		</header>
		<!-- END HEADER -->

		<!-- Left panel : Navigation area --> 
		<aside id="left-panel">
			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                              <span>
                                  <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                              </span>
						<i class="fa fa-angle-down"></i>
					</a> 
					
				</span>
			</div>

			<nav>
				<ul>
					<li class="">
	                <a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MENÚ PRINCIPAL"><i
	                        class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
	            	</li>
		            <li class="text-center">
		                <a href="<?php echo base_url().'index.php/admin/proy/list_proy' ?>" title="MIS PROYECTOS"><span class="menu-item-parent">PROGRAMACI&Oacute;N DEL POA</span></a>
		            </li>
					<?php
		                for($i=0;$i<count($enlaces);$i++)
		                {
		                    if(count($subenlaces[$enlaces[$i]['o_child']])>0)
		                    {
		            ?>
		            <li>
		              	<a href="#" >
		              		<i class="<?php echo $enlaces[$i]['o_image']?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
		              	<ul >
		              	<?php
		                foreach ($subenlaces[$enlaces[$i]['o_child']] as $item) {
		                ?>
		                <li><a href="<?php echo base_url($item['o_url']); ?>"><?php echo $item['o_titulo']; ?></a></li>
		                <?php } ?>
		                </ul>
		            </li>
		            	<?php 
		                    }
		            } ?>
				</ul>
			</nav>
			<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>

		</aside>

		<!-- MAIN PANEL -->
		<div id="main" role="main">
			<!-- RIBBON -->
			<div id="ribbon">
				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>
				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url().'index.php/admin/proy/list_proy' ?>" title="MIS PROYECTOS">Mis Operaciones</a></li><li><a href="<?php echo base_url().'index.php/admin/proy/list_proy' ?>">T&eacute;cnico de Planificaci&oacute;n</a></li><li>Datos Generales</li>
				</ol>
			</div>
			<!-- END RIBBON -->

			<!-- MAIN CONTENT -->
			<div id="content">
				<div class="row">
					<nav role="navigation" class="navbar navbar-default navbar-inverse">
				        <div class="navbar-header">
				            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
				                <span class="sr-only">Toggle navigation</span>
				                <span class="icon-bar"></span>
				                <span class="icon-bar"></span>
				                <span class="icon-bar"></span>
				            </button>
				        </div>
				 
				        <div id="navbarCollapse" class="collapse navbar-collapse">
				            <ul class="nav navbar-nav">
				                <li class="active"><a href="#" title="DATOS GENERALES"><i class="glyphicon glyphicon-ok"></i>&nbsp;DATOS GENERALES&nbsp;</a></li>
				                <li><a href="#" title="METAS DEL PROYECTO">&nbsp;METAS&nbsp;</a></li>
				                <li><a href="#" title="RESPONSABLES POR REGISTRAR">&nbsp;RESPONSABLES&nbsp;</a></li>
				                <li><a href="#" title="CLASIFICACION POR REGISTRAR">&nbsp;CLASIFICACI&Oacute;N&nbsp;</a></li>
				                <li><a href="#" title="LOCALIZACION POR REGISTRAR">&nbsp;LOCALIZACI&Oacute;N&nbsp;</a></li>
				                <li><a href="#" title="MARCO LOGICO POR REGISTRAR">&nbsp;OBJETIVOS&nbsp;</a></li>
				                <li><a href="#" title="RESUMEN TECNICO POR REGISTRAR">&nbsp;RESUMEN T&Eacute;CNICO&nbsp;</a></li>
				                <li><a href='#' title="SUBIR ARCHIVOS DOUMENTOS DEL PROYECTO">ANEXOS DE LA OPERACI&Oacute;N</a></li>
				            </ul>
				        </div>
					</nav>
				</div>

				<!-- widget grid -->
<!--				<section id="widget-grid" class=""> // Incluir el ID widget-grid elimina elementos DOM dentro de header-->
				<section id="" class="">
					<!-- row -->
					<div class="row">
							<!-- Widget ID (each widget will need unique ID)-->
							<div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" data-widget-fullscreenbutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false">
								<header>
									<span class="widget-icon"> <i class="fa fa-check"></i> </span>
									<h2 class="font-md"><strong>DATOS GENERALES DE LA OPERACI&Oacute;N</strong></h2>
								</header>
								<!-- widget div-->
								<div>
									<!-- widget edit box -->
<!--									<div class="jarviswidget-editbox"></div>-->
									<!-- end widget edit box -->
									<!-- widget content -->
									<div class="widget-body">
										<div class="row">
											<form name="form_nuevo_obj" id="form_nuevo_obj" method="post" action="<?php echo site_url("") . '/programacion/proyecto/valida'?>">
											<input class="form-control" type="hidden" name="gestion" id="gestion" value="<?php echo $this->session->userdata("gestion") ?>">
												<div id="bootstrap-wizard-1" class="col-sm-8">
													<div class="well">
														<div class="row">
															<div class="col-sm-12">
																<div class="form-group">
																	<label><b>NOMBRE DEL PROYECTO, PROGRAMA U OPERACI&Oacute;N DE FUNCIONAMIENTO </b><span class="text-info">(Registro Obligatorio)</span></label>
																	<textarea rows="5" class="form-control" name="nom_proy" id="nom_proy" style="width:100%;"  maxlength="200" title="Ingrese Nombre del Proyecto, Programa " ></textarea> 
																</div>
															</div>
														</div>

														<div class="row">
															<div class="col-sm-4">
																<div class="form-group">
																	<label><b>TIPO DE OPERACI&Oacute;N </b><span class="text-info">(Obligatorio)</span></label>
																	<select class="form-control" id="tp_id" name="tp_id" title="Seleccione Tipo de Proyecto">
																		<option value="">Seleccione</option>
				                                                        <?php 
														                    foreach($tp_proy as $row){ ?>
																                <option value="<?php echo $row['tp_id']; ?>"><?php echo $row['tp_tipo']; ?></option>
																                <?php 	
														                    }
														                ?>       
				                                                  	</select>
																</div>
															</div>
															<div class="col-sm-4">
																<div class="form-group">
																	<label><b>PONDERACI&Oacute;N - % </b><span class="text-info">(Autom&aacute;tico)</span></label>
																	<input class="form-control" type="text" value="0" disabled="true" >
																	<input class="form-control" type="hidden" name="pn_cion" id="pn_cion" value="0" placeholder="99%" >
																</div>
															</div>
															<div id="pi" style="display:none;">  <!-- Relativo -->
																<div class="col-sm-4">
																	<div class="form-group"> 
																		<label><b>C&Oacute;DIGO SISIN</b><span class="text-info">(Opcional)</span></label>
																		<input class="form-control" type="text" name="cod_sisin" id="cod_sisin" value="9999-99999-99999" data-mask="9999-99999-99999" data-mask-placeholder= "X" title="Ingrese Codigo SISIN del proyecto">
																	</div>
																</div>
															</div>
															<div id="pr" style="display:none;">  <!-- Relativo -->
																<div class="col-sm-4">
																	<div class="form-group"> 
																		<label><b>COSTO TOTAL PROY/PROG</b><span class="text-info">(Opcional)</span></label>
																		<input class="form-control" type="text" name="mtotal" id="mtotal" placeholder="0" value="0"  onkeypress="if (this.value.length < 20) { return numerosDecimales(event);}else{return false; }" onpaste="return false">
																	</div>
																</div>
															</div>
														</div>

														<div class="row">
															<div class="col-sm-6">
																<div class="form-group">
																	<label><b>FECHA INICIAL DEL PROYECTO </b><span class="text-info">(dd/mm/yyyy)</span></label>
																	<div class="input-group">
																		<input type="text" name="ini" id="f_ini" placeholder="Seleccione Fecha inicial" value="<?php echo date('d/m/Y') ?>" class="form-control datepicker" data-dateformat="dd/mm/yy"
							                                                   onKeyUp="this.value=formateafecha(this.value);">
																		<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
																	</div>
																</div>
															</div>

															<div class="col-sm-6">
																<div class="form-group">
																	<label><b>FECHA FINAL DEL PROYECTO </b><span class="text-info">(dd/mm/yyyy)</span></label>
																	<div class="input-group">
																		<input type="text" name="fin" id="f_final" placeholder="Seleccione Fecha final" value="<?php echo date('d/m/Y') ?>" class="form-control datepicker" data-dateformat="dd/mm/yy"
							                                                   onKeyUp="this.value=formateafecha(this.value);">
																		<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
																	</div>
																</div>
															</div>
														</div>
													</div> <!-- end well -->
															
												</div>
												<div id="bootstrap-wizard-1" class="col-sm-4">
													<div class="well">
														<div class="row">
															<input type="hidden" name="form" value="1"><input type="hidden" name="dur">
															<div class="col-sm-12">
															<center><b>CATEGORIA PROGRAM&Aacute;TICA</b></center>
															<center><b>GESTI&Oacute;N  <?php echo $this->session->userData('gestion') ?></b></center>
																<div class="form-group">
																	<label><b>PROGRAMA </b><span class="text-info">(Obligatorio)</span></label>
																	<select id="prog" name="prog" title="Seleccione Apertura Programatica" class="form-control">
				                                                        <option value="">Programa</option>
				                                                        <?php 
																	        foreach($programas as $row)
																	        {
																	            ?>
																			        <option value="<?php echo $row['aper_programa']; ?>"><?php echo $row['aper_programa'].' - '.$row['aper_descripcion']; ?></option>
																			    <?php
																	        }
																	    ?>        
				                                                  	</select>
																</div>
															</div>

															<div id="pi1" style="display:none;">
																<div class="col-sm-12">
																	<div class="form-group">
																		<label><b>PROYECTO</b><span class="text-info">(Opcional)</span></label>
																		<input class="form-control" type="text" name="proy1" id="proy1" value="9999" maxlength="14" placeholder="Programa" title="Ingrese Proyecto">
																	</div>
																</div>
																<div class="col-sm-12">
																	<div class="form-group">
																		<label><b>ACTIVIDAD</b></label>
																		<input class="form-control" type="text" name="act" id="act" value="000" disabled="true">
																	</div>
																</div>
															</div>

															<div id="pr1" style="display:none;">
																<div class="col-sm-12">
																	<div class="form-group">
																		<label><b>PROYECTO</b></label>
																		<input class="form-control" type="text" name="proy" id="proy" value="0000" disabled="true">
																	</div>
																</div>
																<div class="col-sm-12">
																	<div class="form-group">
																		<label><b>ACTIVIDAD </b><span class="text-info">(Opcional)</span></label>
																		<input class="form-control" type="text" name="act1" id="act1" value="999" data-mask="999" data-mask-placeholder= "9" title="Ingrese Actividad">
																	</div>
																</div>
															</div>
														</div>
													</div> <!-- end well -->
													<div id="tg" style="display:none;">
														<br>
														<div class="well">
														<div class="row">
															<div class="col-sm-12">
																<center><b>TIPO DE GASTO </b> <span class="text-info">(Obligatorio)</span></center>
																<div class="form-group">
																	<select class="form-control" id="tg" name="tg" title="Seleccione Tipo de Gasto">
				                                                        <option value="">Seleccione</option>
				                                                        <?php 
																	        foreach($tp_gasto as $row)
																	        {
																	            ?>
																			        <option value="<?php echo $row['tg_id']; ?>"><?php echo $row['tg_descripcion']; ?></option>
																			    <?php
																	        }
																	    ?>        
				                                                  	</select>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-sm-12">
																<center><b>TIPO </b> <span class="text-info">(Obligatorio)</span></center>
																<div class="form-group">
																	<select class="select2" id="tp_cap" name="tp_cap" title="Seleccione Tipo">
																		<option value="">Seleccione</option>
				                                                        <?php 
														                    foreach($list_cap as $row)
														                    {
														                    	?>
																                    <option value="<?php echo $row['cp_id']; ?>"><?php echo $row['cp_descripcion']; ?></option>
																                <?php 	
														                    }
														                ?>       
				                                                  	</select>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-sm-12">
																<center><b id="titulo_comp"></b>REQUIEREN DE UNIDADES EJECUTORAS ?<span class="text-info">(Obligatorio)</span></center>
																<div class="form-group">
																	<select class="select2" id="comp" name="comp" title="Seleccione Tipo">
																		<option value="">Seleccione</option>
				                                                        <option value="1">SI</option>   
				                                                        <option value="0">NO</option>   
				                                                  	</select>
																</div>
															</div>
														</div>
														</div>
													</div>

												</div>
												<div  class="col-sm-12">
													<div class="form-actions">
														<a href="<?php echo base_url().'index.php/admin/proy/list_proy' ?>" class="btn btn-lg btn-default" title="Volver a Mis Proyectos ">CANCELAR</a>
														<input type="button" id="enviar_obj" name="enviar_obj" value="GUARDAR PROYECTO" class="btn btn-primary btn-lg">
													</div>
												</div>
											</form>
										</div>
									</div>
									<!-- end widget content -->
								</div>
								<!-- end widget div -->
							</div>
						<!-- end widget -->
					</div>
					<!-- end row -->
				</section>
				<!-- end widget grid -->					
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN PANEL -->
		<!-- PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->
		<!--================================================== -->
		<script src="<?php echo base_url();?>/assets/js/libs/jquery-2.0.2.min.js"></script>
		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>
		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url(); ?>assets/js/session_time/jquery-idletimer.js"></script>
		<script src="<?php echo base_url();?>/assets/js/app.config.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/mis_js/validacion_form.js"></script>
		<SCRIPT src="<?php echo base_url(); ?>mis_js/programacion/ejecucion/abm_ejecucion.js" type="text/javascript"></SCRIPT>
		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url();?>/assets/js/bootstrap/bootstrap.min.js"></script>
		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url();?>/assets/js/notification/SmartNotification.min.js"></script>
		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url();?>/assets/js/smartwidgets/jarvis.widget.min.js"></script>
		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url();?>/assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<!-- SPARKLINES -->
		<script src="<?php echo base_url();?>/assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/select2/select2.min.js"></script>
		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
		<!-- browser msie issue fix -->
		<script src="<?php echo base_url();?>/assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url();?>/assets/js/plugin/fastclick/fastclick.min.js"></script>
		<!-- Demo purpose only -->
		<script src="<?php echo base_url();?>/assets/js/demo.min.js"></script>
		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url();?>/assets/js/app.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<!-- Voice command : plugin -->
		<script src="<?php echo base_url();?>/assets/js/speech/voicecommand.min.js"></script>
		<script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
		<script src="<?php echo base_url();?>/assets/js/plugin/fuelux/wizard/wizard.min.js"></script>
		<script type="text/javascript">
		$(document).ready(function() {
			pageSetUp();
		})
		</script>
		<script>
		$(function () {
			function reset() {
	            $("#toggleCSS").attr("href", "<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css");
	            alertify.set({
	                labels: {
	                    ok: "ACEPTAR",
	                    cancel: "CANCELAR"
	                },
	                delay: 5000,
	                buttonReverse: false,
	                buttonFocus: "ok"
	            });
	        }

		    $("#enviar_obj").on("click", function (e) {
		        var $validator = $("#form_nuevo_obj").validate({
		            rules: {
		                nom_proy: {
		                    required: true,
		                },
		                tp_id: {
		                    required: true,
		                },
		                ini: {
		                    required: true,
		                },
		                fin: {
		                    required: true,
		                },
		                prog: {
		                    required: true,
		                },
		                tg: {
		                    required: true,
		                },
		                tp_cap: {
		                    required: true,
		                },
		                comp: {
		                    required: true,
		                }

		            },
		            messages: {
		                nom_proy: {required: "Campo Requerido"},
		                tp_id: {required: "Campo Requerido"},
		                ini: {required: "Campo Requerido"},
		                fin: {required: "Campo Requerido"},
		                prog: {required: "Campo Requerido"},
		                tp_cap: {required: "Campo Requerido"},
		                comp: {required: "Campo Requerido"}
		            },
		            highlight: function (element) {
		                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		            },
		            unhighlight: function (element) {
		                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		            },
		            errorElement: 'span',
		            errorClass: 'help-block',
		            errorPlacement: function (error, element) {
		                if (element.parent('.input-group').length) {
		                    error.insertAfter(element.parent());
		                } else {
		                    error.insertAfter(element);
		                }
		            }
		        });
		        var $valid = $("#form_nuevo_obj").valid();
		        if (!$valid) {
		            $validator.focusInvalid();
		        } 
		        else {
		        		var fecha_inicial = document.getElementById("f_ini").value.split("/")  //fecha inicial
	                	var fecha_final = document.getElementById("f_final").value.split("/")  /*fecha final*/

	                	if(document.getElementById("gestion").value<parseInt(fecha_inicial[2])){
			                alertify.alert("NO PUEDE REGISTRAR PROYECTOS,PROGRAMAS,OPERACIONES DE GESTIONES POSTERIORES A LA GESTION VIGENTE DEL SISTEMA");
			                return false;
	                	}

	                	if(parseInt(fecha_final[2])<parseInt(fecha_inicial[2])){
	                		alertify.alert("ERROR !!! VERIFIQUE LAS FECHAS DE INICIO Y FINAL DEL PROYECTO,PROGRAMA, OPERACIÓN");
			                return false;
	                	}

	                	 /*------------------------- Fecha Inicial ----------------------*/
	                	if(parseInt(fecha_inicial[0])>'31' || parseInt(fecha_inicial[1])>'12' || parseInt(fecha_inicial[2])<'2008'){
	                		alertify.alert("ERROR !!! VERIFIQUE LA FECHA INICIAL REGISTRADO ");
			                return false;
	                	}
	                	/*-----------------------------------------------------------------*/
	                	/*------------------------------ Fecha Final ----------------------*/
	                	if(parseInt(fecha_final[2])>'2027' || parseInt(fecha_final[0])>'31' || parseInt(fecha_final[1])>'12'){
	                		alertify.alert("ERROR !!! VERIFIQUE LA FECHA FINAL REGISTRADO ");
			                return false;
	                	}
		            
		            	if(document.getElementById("tp_id").value==1)
		              	{
		              		prog=document.getElementById("prog").value
		              		proy=document.getElementById("proy1").value
		              		act='000'

		              		if(document.getElementById("proy1").value=='9999')
		              		{   
		              			reset();
				                alertify.confirm("GUARDAR PROYECTO,PROGRAMA,OPERACI\u00D3N?", function (a) {
				                    if (a) {
				                        //============= GUARDAR DESPUES DE LA VALIDACION ===============
				                        document.getElementById('enviar_obj').disabled = true;
				                        document.form_nuevo_obj.submit();
				                    } else {
				                        alertify.error("OPCI\u00D3N CANCELADA");
				                    }
				                });
		              		}
		              		else
		              		{
		              			var url = "<?php echo site_url("admin")?>/proy/verif";
								$.ajax({
									type:"post",
									url:url,
									data:{prog:prog,proy:proy,act:act},
									success:function(datos){
										
										if(datos.trim() =='true'){
											alertify.alert("LA CATEGORIA PROGRAMATICA SELECCIONADO YA SE ENCUENTRA REGISTRADO");
			                				return false;
										}else{
										    reset();
							                alertify.confirm("GUARDAR PROYECTO,PROGRAMA,OPERACI\u00D3N?", function (a) {
							                    if (a) {
							                        //============= GUARDAR DESPUES DE LA VALIDACION ===============
							                        document.getElementById('enviar_obj').disabled = true;
							                        document.form_nuevo_obj.submit();
							                    } else {
							                        alertify.error("OPCI\u00D3N CANCELADA");
							                    }
							                });
										}
								}});
		              		}	
		              	}
		              	else
		              	{	
		              		prog=document.getElementById("prog").value 
		              		proy='0000'
		              		act=document.getElementById("act1").value
		              		if(document.getElementById("act1").value=='999')
		              		{	
				                reset();
				                alertify.confirm("GUARDAR PROYECTO,PROGRAMA,OPERACI\u00D3N?", function (a) {
				                    if (a) {
				                        //============= GUARDAR DESPUES DE LA VALIDACION ===============
				                        document.getElementById('enviar_obj').disabled = true;
				                        document.form_nuevo_obj.submit();
				                    } else {
				                        alertify.error("OPCI\u00D3N CANCELADA");
				                    }
				                });
		              		}
		              		else
		              		{
		              			var url = "<?php echo site_url("admin")?>/proy/verif";
								$.ajax({
									type:"post",
									url:url,
									data:{prog:prog,proy:proy,act:act},
									success:function(datos){

										if(datos.trim() =='true'){
											alertify.alert("LA CATEGORIA PROGRAMATICA SELECCIONADO YA SE ENCUENTRA REGISTRADO");
	            							return false;
										}else{
											reset(); 
							                alertify.confirm("GUARDAR PROYECTO,PROGRAMA,OPERACI\u00D3N?", function (a) {
							                    if (a) {
							                        //============= GUARDAR DESPUES DE LA VALIDACION ===============
							                        document.getElementById('enviar_obj').disabled = true;
							                        document.form_nuevo_obj.submit();
							                    } else {
							                        alertify.error("OPCI\u00D3N CANCELADA");
							                    }
							                });
										}

								}});
		              		}	
		              	}

		        }
		    });
		});
		</script>
	</body>
</html>
