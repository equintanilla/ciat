<!DOCTYPE html>
<html lang="en-us">
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    <title><?php echo $this->session->userdata('name')?></title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- Basic Styles -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
    <!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
    <!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
    <!-- FAVICONS -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
    <!--//////////////fin css-->
    <!--para las alertas-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.core.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css" id="toggleCSS"/>
    <meta name="viewport" content="width=device-width">
    <style type="text/css">
        aside{background: #05678B;}
    </style>
        <style>
            .show-grid [class^="col-"] {
                padding-top: 10px;
                padding-bottom: 10px;
                background-color: rgba(61, 106, 124, 0.15);
                border: 1px solid rgba(61, 106, 124, 0.2);
            }
        
            .show-grid {
                margin-bottom: 15px;
            }
        </style>
      <style type="text/css">
          table{font-size: 10px;
            width: 100%;
            max-width:1550px;;
            overflow-x: scroll;
            }
          th{
              padding: 1.4px;
              text-align: center;
              font-size: 10px;
            }
      </style>
</head>
<body class="">
<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->

<!-- HEADER -->
        <header id="header">
            <!-- pulled right: nav area -->
            <div class="pull-right">
                <!-- collapse menu button -->
                <div id="hide-menu" class="btn-header pull-right">
                    <span> <a href="javascript:void(0);" data-action="toggleMenu" title="Menu"><i class="fa fa-reorder"></i></a> </span>
                </div>
                <!-- end collapse menu -->
                <!-- logout button -->
                <div id="logout" class="btn-header transparent pull-right">
                    <span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Salir" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
                </div>
                <!-- end logout button -->
                <!-- search mobile button (this is hidden till mobile view port) -->
                <div id="search-mobile" class="btn-header transparent pull-right">
                    <span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
                </div>
                <!-- end search mobile button -->
                <!-- fullscreen button -->
                <div id="fullscreen" class="btn-header transparent pull-right">
                    <span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Pantalla Completa"><i class="fa fa-arrows-alt"></i></a> </span>
                </div>
                <!-- end fullscreen button -->
            </div>
            <!-- end pulled right: nav area -->
        </header>
<!-- END HEADER -->

<!-- Left panel : Navigation area -->
<aside id="left-panel">

    <!-- User info -->
    <div class="login-info">
		<span> <!-- User image size is adjusted inside CSS, it should stay as is -->
			<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
				<span>
					<?php echo $this->session->userdata("user_name"); ?>
				</span>
                <i class="fa fa-angle-down"></i>
            </a>
		</span>
    </div>

    <nav>
        <ul>
            <li class="">
                <a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MENÚ PRINCIPAL"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
            </li>
            <li class="text-center">
                <?php
                if($proyecto[0]['proy_estado']==1){ ?>
                    <a href="<?php echo base_url().'index.php/admin/proy/list_proy' ?>" title="MIS PROYECTOS"><span class="menu-item-parent">PROGRAMACI&Oacute;N DEL POA</span></a>
                    <?php
                }
                elseif ($proyecto[0]['proy_estado']==2){ ?>
                    <a href="<?php echo base_url().'index.php/admin/proy/list_proy_poa' ?>" title="MIS PROYECTOS"><span class="menu-item-parent">PROGRAMACI&Oacute;N DEL POA</span></a>
                    <?php
                }
                ?>
            </li>
            <?php
            if($nro_fase==1){
                for($i=0;$i<count($enlaces);$i++){ ?>
                    <li>
                        <a href="#" >
                            <i class="<?php echo $enlaces[$i]['o_image'];?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
                        <ul>
                            <?php
                            $submenu= $this->menu_modelo->get_Modulos_sub($enlaces[$i]['o_child']);
                            foreach($submenu as $row) {
                                ?>
                                <li><a href="<?php echo base_url($row['o_url'])."/".$mod."/".$id_f[0]['id']."/".$id_f[0]['proy_id']; ?>"><?php echo $row['o_titulo']; ?></a></li>
                            <?php } ?>
                        </ul>
                    </li>
                    <?php
                }
            }
            ?>
        </ul>
    </nav>
    <span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>

</aside>

<!-- MAIN PANEL -->
<div id="main" role="main">

    <!-- RIBBON -->
	<div id="ribbon">
		<span class="ribbon-button-alignment"> 
			<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
				<i class="fa fa-refresh"></i>
			</span> 
		</span>
		<!-- breadcrumb -->
		<ol class="breadcrumb">
            <?php
                if($proyecto[0]['proy_estado']==1)
                {
                    ?>
                    <li>Mis Operaciones</li><li><a href="<?php echo base_url().'index.php/admin/proy/list_proy' ?>">T&eacute;cnico de Unidad Ejecutora</a></li><li><?php echo $titulo;?></li>
                    <?php
                }
                elseif ($proyecto[0]['proy_estado']==2) 
                {
                    ?>
                    <li>Mis Operaciones</li><li><a href="<?php echo base_url().'index.php/admin/proy/list_proy_poa' ?>">T&eacute;cnico Analista POA</a></li><li><?php echo $titulo;?></li>
                    <?php
                }
                elseif ($proyecto[0]['proy_estado']==3) 
                {
                    ?>
                    <li>Mis Operaciones</li><li><a href="<?php echo base_url().'index.php/admin/proy/list_proy_fin' ?>">T&eacute;cnico Analista Financiero</a></li><li><?php echo $titulo;?></li>
                    <?php
                }
                ?>
			
		</ol>
	</div>
    <!-- END RIBBON -->

    <!-- MAIN CONTENT -->
    <div id="content">

        <div class="row">
            <nav role="navigation" class="navbar navbar-default navbar-inverse">
		        <div class="navbar-header">
		            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
		                <span class="sr-only">Toggle navigation</span>
		                <span class="icon-bar"></span>
		                <span class="icon-bar"></span>
		                <span class="icon-bar"></span>
		            </button>
		        </div>
		 
		        <div id="navbarCollapse" class="collapse navbar-collapse">
		            <ul class="nav navbar-nav">
		                <li><a href="<?php echo base_url().'index.php/admin/proy/edit/'.$proyecto[0]['proy_id'].'/1' ?>" title="DATOS GENERALES"><font size="2">&nbsp;DATOS GENERALES&nbsp;</font></a></li>
		                <li class="active"><a href="#" title="<?php echo strtoupper($titulo);?> DE LA OPERACION"><i class="glyphicon glyphicon-ok"></i><font size="2">&nbsp;<?php echo strtoupper($titulo);?>&nbsp;</font></a></li>
		                <?php
                        if($this->session->userdata("rol_id")==1){ ?>
                            <li><a href="<?php echo base_url().'index.php/admin/proy/proyecto/'.$proyecto[0]['proy_id'].'/2' ?>" title="RESPONSABES"><font size="2">&nbsp;RESPONSABLES&nbsp;</font></a></li><?php
                        }
                        ?>
		                <li><a href="<?php echo base_url().'index.php/admin/proy/proyecto/'.$proyecto[0]['proy_id'].'/3' ?>"><font size="2">&nbsp;CLASIFICACI&Oacute;N&nbsp;</font></a></li>
                        <li><a href="<?php echo base_url().'index.php/admin/proy/proyecto/'.$proyecto[0]['proy_id'].'/4?qRegistro='.$proyecto[0]['proy_id'].'' ?>"><font size="2">&nbsp;LOCALIZACI&Oacute;N&nbsp;</font></a></li>
                        <?php
                          if($proyecto[0]['tp_id']==1 || $proyecto[0]['tp_id']==2 || $proyecto[0]['tp_id']==3){ ?>
                            <li><a href="<?php echo base_url().'index.php/admin/proy/proyecto/'.$proyecto[0]['proy_id'].'/5' ?>" title="MARCO LOGICO"><font size="2">&nbsp;OBJETIVOS&nbsp;</font></a></li>
                            <?php
                          }
                        ?>
                        <li><a href="<?php echo base_url().'index.php/admin/proy/proyecto/'.$proyecto[0]['proy_id'].'/7' ?>"><font size="2">&nbsp;RESUMEN T&Eacute;CNICO&nbsp;</font></a></li>
                        <li><a href='<?php echo site_url("admin").'/proy/proyecto/'.$proyecto[0]['proy_id'].'/8'; ?>'><font size="2">ANEXOS DE LA OPERACI&Oacute;N</font></a></li>
		            	<?php
                            if($proyecto[0]['proy_estado']!=4){
                                if($proyecto[0]['tp_id']==1){ ?>
                                  <li><a href='<?php echo site_url("admin").'/proy/fase_etapa/'.$proyecto[0]['proy_id']; ?>' title="FASE ETAPA COMPONENTE"><font size="2">&nbsp;FASE / ETAPA&nbsp;</font></a></li>
                                  <?php
                                }
                                elseif ($proyecto[0]['tp_id']==2 || $proyecto[0]['tp_id']==3 || $proyecto[0]['tp_id']==4) { ?>
                                  <li><a href="<?php echo base_url().'index.php/admin/proy/proyecto/'.$proyecto[0]['proy_id'].'/9' ?>" title="MONTO PRESUPUESTO"><font size="2">&nbsp;PRESUPUESTO&nbsp;</font></a></li>
                                  <?php
                                }
                            }
	                      ?>
		            </ul>
		        </div>
			</nav>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h2 class="well well-sm text-justify" style="margin:0 0 7px 0 !important;">
					<?php echo strtoupper($proyecto[0]['tipo']);?>: 
					<span class="text-success"><?php echo strtoupper($proyecto[0]['proy_nombre']);?></span>
					<hr style="margin:2px auto; border-bottom:1px solid #eee;">
	                <small>
						<b class="lead">
						FECHA DE INICIO : <span class="text-primary"><?php echo date('d-m-Y',strtotime($proyecto[0]['f_inicial'])); ?></span>
						&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
						FECHA DE CONCLUSI&Oacute;N : <span class="text-primary"><?php echo date('d-m-Y',strtotime($proyecto[0]['f_final'])); ?></span>
						</b>
					</small>
				</h2>
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- row -->
            <div class="row">
                <!-- NEW WIDGET START -->
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-color-darken" >
                            <header>
                                <span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
                               <h2 class="font-md"><strong><?php echo strtoupper($titulo);?> DE LA OPERACI&Oacute;N</strong></h2>               
                            </header>
                        <div>
                            <div class="widget-body no-padding">
                                <div class="table-responsive">
                                    <table id="dt_basic" class="table table table-bordered" width="100%">
                                        <thead>
                                        <tr>
                                            <th width="5%"><center><a href="#" data-toggle="modal" data-target="#modal_nuevo_ff" class="btn btn-xs nuevo_ff" title="NUEVO DE REGISTRO METAS"><img src="<?php echo base_url(); ?>assets/ifinal/2.png" WIDTH="35" HEIGHT="35"/></a><br>NUEVO REGISTRO</center></th>
                                            <th style="width:30%;">INDICADORES</th>
                                            <th style="width:10%;">META</th>
                                            <th style="width:10%;">EJECUCI&Oacute;N</th>
                                            <th style="width:10%;">EFICACIA</th>
                                            <th style="width:10%;">RESULTADO/PRODUCTO</th>
                                            <th style="width:5%;">MODIFICAR</th>
                                            <th style="width:5%;">ELIMINAR</th>
                                        </tr>
                                        </thead>
                                        <tbody id="tabla_ff">
                                            <?php
                                                $nro=1;
                                               foreach($metas as $row)
                                               {
                                                ?>
                                                <tr>
                                                    <td align="center"><?php echo $nro;?></td>
                                                    <td><font size="1"><?php echo $row['meta_descripcion'];?></font></td>
                                                    <td align="center"><font size="1"><?php echo $row['meta_meta'];?></font></td>
                                                    <td align="center"><font size="1"><?php echo $row['meta_ejec'];?></font></td>
                                                    <td align="center"><font size="1"><?php echo $row['meta_efic'];?></font></td>
                                                    <td align="center">
                                                    <font size="1">
                                                    <?php 
                                                        if($row['meta_rp']==0){echo "RESULTADO";}else {echo "PRODUCTO";};
                                                    ?>
                                                    </font>
                                                    </td>
                                                    <td align="center"><a href="#" data-toggle="modal" data-target="#modal_mod_ff" class="btn btn-xs mod_ff" title="MODIFICAR REGISTRO METAS" name="<?php echo $row['meta_id']; ?>"><img src="<?php echo base_url(); ?>assets/ifinal/modificar.png" WIDTH="45" HEIGHT="45"/></a></td>
                                                    <td align="center"><a href="#" data-toggle="modal" data-target="#modal_del_ff" class="btn btn-xs del_ff" title="ELIMINAR REGISTRO METAS" name="<?php echo $row['meta_id']; ?>"><img src="<?php echo base_url(); ?>assets/ifinal/eliminar.png" WIDTH="45" HEIGHT="45"/></a></td>
                                                </tr>
                                                <?php
                                                $nro++;
                                               }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- end widget content -->
                        </div>
                        <div class="form-actions">
                            <?php
                                if($proyecto[0]['proy_estado']!=4){
                                ?>
                                <a href="<?php echo base_url().'index.php/admin/proy/list_proy' ?>" class="btn btn-lg btn-default" title="Volver a Mis Proyectos ">CANCELAR</a>
                                <?php
                                if($this->session->userdata("rol_id")==1){
                                    ?>
                                    <a href="<?php echo base_url().'index.php/admin/proy/proyecto/'.$proyecto[0]['proy_id'].'/2' ?>" class="btn btn-primary btn-lg" title="GUARDAR Y REGISTRAR RESPONSABLES DEL PROYECTO" >PASAR A RESPONSABLES </a>
                                    <?php
                                }
                                else{
                                    ?>
                                    <a href="<?php echo base_url().'index.php/admin/proy/proyecto/'.$proyecto[0]['proy_id'].'/3' ?>" class="btn btn-primary btn-lg" title="GUARDAR Y REGISTRAR RESPONSABLES DEL PROYECTO" >PASAR A CLASIFICACI&Oacute;N </a>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                        <!-- end widget div -->
                    </div>
                    <!-- end widget -->
                </article>
                <!-- WIDGET END -->
            </div>
        </section>
        <!-- end widget grid -->
    </div>
    <!-- END MAIN CONTENT -->

    <!-- ================== Modal NUEVO METAS  ========================== -->
    <div class="modal animated fadeInDown" id="modal_nuevo_ff" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close text-danger " data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">
                    </h4>
                    <h4 class="modal-title text-center text-info">
                    <b><i class="glyphicon glyphicon-pencil"></i> NUEVO REGISTRO (<?php echo strtoupper($titulo);?>)</b>
                    </h4>
                </div>
                <div class="modal-body no-padding">
                    <div class="row">
                        <form id="form_ff" novalidate="novalidate" method="post">
                        <input type="hidden" name="id_pr" id="id_pr" value="<?php echo $proyecto[0]['proy_id'];?>">
                            <div id="bootstrap-wizard-1" class="col-sm-12">
                                <div class="well">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label><font size="1"><b>INDICADOR </b></font><font color="blue">(Obligatorio)</font></label>
                                                <textarea rows="5" class="form-control" name="ind" id="ind" style="width:100%;" title="Indicador" maxlength="200"></textarea> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <LABEL><b>META </b><font color="blue">(Obligatorio)</font></label>
                                                <input class="form-control" type="text" name="meta" id="meta" value="0" onkeypress="if (this.value.length < 6) { return numerosDecimales(event);}else{return false; }" onpaste="return false">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                 <LABEL><b>EJECUCI&Oacute;N </b><font color="blue">(Obligatorio)</font></label>
                                                <input class="form-control" type="text" name="ejec" id="ejec" value="0" onkeypress="if (this.value.length < 6) { return numerosDecimales(event);}else{return false; }" onpaste="return false">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <LABEL><b>RESULTADO / PRODUCTO</b> <font color="blue">(Obligatorio)</font></label>
                                                <select class="form-control" id="rp" name="rp" title="Seleccione Resultado Producto">
                                                    <option value="">Seleccione</option>
                                                    <option value="0">Resultado</option>  
                                                    <option value="1">Producto</option>     
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- end well -->
                            </div>
                        </form>
                        <div class="modal-footer">
                            <div class="form-actions">
                                <button class="btn btn-lg btn-danger" data-dismiss="modal">CANCELAR</button>
                                <button type="submit" name="enviar_ff" id="enviar_ff" class="btn  btn-lg btn-primary"><i class="fa fa-save"></i>GUARDAR</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- ================== Modal  MODIFICAR  METAS========================== -->
    <div class="modal animated fadeInDown" id="modal_mod_ff" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close text-danger " data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">
                    </h4>
                    <h4 class="modal-title text-center text-info">
                    <b><i class="glyphicon glyphicon-pencil"></i> MODIFICAR REGISTRO (<?php echo strtoupper($titulo);?>)</b>
                    </h4>
                </div>
                <div class="modal-body no-padding">
                    <div class="row">
                        <form id="mod_formff" novalidate="novalidate" method="post">
                            <div id="bootstrap-wizard-1" class="col-sm-12">
                                <div class="well">
                                    <input class="form-control" type="hidden" name="meta_id" id="meta_id" >
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label><font size="1"><b>INDICADOR </b></font></label>
                                                <textarea rows="5" class="form-control" name="indi" id="indi" style="width:100%;" title="Indicador" maxlength="200"></textarea> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <LABEL><b>META </b></label>
                                                <input class="form-control" type="text" name="metas" id="metas"  onkeypress="if (this.value.length < 6) { return numerosDecimales(event);}else{return false; }" onpaste="return false">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                 <LABEL><b>EJECUCI&Oacute;N </b></label>
                                                <input class="form-control" type="text" name="ejecs" id="ejecs"  onkeypress="if (this.value.length < 6) { return numerosDecimales(event);}else{return false; }" onpaste="return false">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <LABEL><b>RESULTADO / PRODUCTO</b></label>
                                                <select class="form-control" id="repr" name="repr" title="Seleccione Resultado Producto">
                                                    <option value="0">Resultado</option>  
                                                    <option value="1">Producto</option>     
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- end well -->
                            </div>
                        </form>
                        <div class="modal-footer">
                            <div class="form-actions">
                                <button class="btn btn-lg btn-danger" data-dismiss="modal">CANCELAR</button>
                                <button type="submit" name="mod_ffenviar" id="mod_ffenviar" class="btn  btn-lg btn-primary"><i class="fa fa-save"></i>MODIFICAR</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
</div>
<!-- END MAIN PANEL -->

<!-- PAGE FOOTER -->
<div class="page-footer">
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
        </div>
    </div>
</div>
<!-- END PAGE FOOTER -->

<!--================================================== -->

<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
<script data-pace-options='{ "restartOnRequestAfter": true }'
        src="<?php echo base_url(); ?>assets/js/plugin/pace/pace.min.js"></script>
<script>
    if (!window.jQuery) {
        document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-2.0.2.min.js"><\/script>');
    }
</script>
<script>
    if (!window.jQuery.ui) {
        document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
    }
</script>
<!-- IMPORTANT: APP CONFIG -->
<script src="<?php echo base_url(); ?>assets/js/session_time/jquery-idletimer.js"></script>
<script src="<?php echo base_url(); ?>assets/js/app.config.js"></script>
<script src="<?php echo base_url(); ?>mis_js/registro_ejecucion/mis_operaciones.js"></script>
<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script>
<!-- BOOTSTRAP JS -->
<script src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.min.js"></script>
<!-- CUSTOM NOTIFICATION -->
<script src="<?php echo base_url(); ?>assets/js/notification/SmartNotification.min.js"></script>
<!-- JQUERY VALIDATE -->
<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
<!-- JQUERY MASKED INPUT -->
<script src="<?php echo base_url(); ?>assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
<!-- JQUERY SELECT2 INPUT -->
<script src="<?php echo base_url(); ?>assets/js/plugin/select2/select2.min.js"></script>
<!-- JQUERY UI + Bootstrap Slider -->
<script src="<?php echo base_url(); ?>assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
<!-- browser msie issue fix -->
<script src="<?php echo base_url(); ?>assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
<!-- FastClick: For mobile devices -->
<script src="<?php echo base_url(); ?>assets/js/plugin/fastclick/fastclick.min.js"></script>
<!-- Demo purpose only -->
<script src="<?php echo base_url(); ?>assets/js/demo.min.js"></script>
<!-- MAIN APP JS FILE -->
<script src="<?php echo base_url(); ?>assets/js/app.min.js"></script>
<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
<!-- Voice command : plugin -->
<script src="<?php echo base_url(); ?>assets/js/speech/voicecommand.min.js"></script>
<!--alertas -->
<script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
<!-- ------------  mis validaciones js --------------------- -->
<SCRIPT src="<?php echo base_url(); ?>assets/js/mis_js/validacion_form.js" type="text/javascript"></SCRIPT>
<!-- PAGE RELATED PLUGIN(S) -->
<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
<!--================= ELIMINACION DE LAS METAS =========================================-->
<script type="text/javascript">
    $(function () {
        function reset() {
            $("#toggleCSS").attr("href", "<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css");
            alertify.set({
                labels: {
                    ok: "ACEPTAR",
                    cancel: "CANCELAR"
                },
                delay: 5000,
                buttonReverse: false,
                buttonFocus: "ok"
            });
        }

        // =====================================================================
        $(".del_ff").on("click", function (e) {
            reset();
            var name = $(this).attr('name');
            var request;
            // confirm dialog
            alertify.confirm("REALMENTE DESEA ELIMINAR ESTE REGISTRO?", function (a) {
                if (a) { 
                    url = "<?php echo site_url("admin")?>/proy/del_meta";
                    if (request) {
                        request.abort();
                    }
                    request = $.ajax({
                        url: url,
                        type: "POST",
                        data: "meta_id=" + name

                    });
                    window.location.reload(true);
                    request.done(function (response, textStatus, jqXHR) {
                        $('#tr' + response).html("");
                    });
                    request.fail(function (jqXHR, textStatus, thrown) {
                        console.log("ERROR: " + textStatus);
                    });
                    request.always(function () {
                        //console.log("termino la ejecuicion de ajax");
                    });

                    e.preventDefault();
                    alertify.success("Se eliminó el registro correctamente");

                } else {
                    // user clicked "cancel"
                    alertify.error("Opcion cancelada");
                }
            });
            return false;
        });
    });

</script>
<!--================= MODIFICAR META =========================================-->
<script type="text/javascript">
    $(function () {
        var id_m = '';
        $(".mod_ff").on("click", function (e) {
            //==========================LLENAR MIS DATOS DE FORMULARIO CON LA CLAVE RECIBIDA
            id_m = $(this).attr('name');
            var url = "<?php echo site_url("admin")?>/proy/get_meta";
            var codigo = '';
            var request;
            if (request) {
                request.abort();
            }
            request = $.ajax({
                url: url,
                type: "POST",
                dataType: 'json',
                data: "id_m=" + id_m
            });

            request.done(function (response, textStatus, jqXHR) {

                document.getElementById("meta_id").value = response.meta_id;
                document.getElementById("indi").value = response.meta_descripcion;
                document.getElementById("metas").value = response.meta_meta;
                document.getElementById("ejecs").value = response.meta_ejec;
                var combo = document.forms["mod_formff"].repr;
                var cantidad = combo.length;
                for (i = 0; i < cantidad; i++) {
                    if (combo[i].value == response.meta_rp) {
                        combo[i].selected = true;
                    }
                }

            });
            request.fail(function (jqXHR, textStatus, thrown) {
                console.log("ERROR: " + textStatus);
            });
            request.always(function () {
                //console.log("termino la ejecuicion de ajax");
            });
            e.preventDefault();
            // =============================VALIDAR EL FORMULARIO DE MODIFICACION
            $("#mod_ffenviar").on("click", function (e) {
                var $validator = $("#mod_formff").validate({
                   rules: {
                    indi: { //// indicador
                        required: true,
                    },
                    repr: { //// Resultado Ejecutado
                        required: true,
                    },
                    metas: { //// meta
                        required: true,
                        number: true,
                        min: 1,
                    }
                },
                messages: {
                    indi: "Describa el indicador",
                    repr: "Selecciones Resultado Producto",
                    metas: {required: "Ingrese el meta", number: "Dato Inválido", min: "Dato Inválido"},
                    
                },
                    highlight: function (element) {
                        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                    },
                    unhighlight: function (element) {
                        $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                    },
                    errorElement: 'span',
                    errorClass: 'help-block',
                    errorPlacement: function (error, element) {
                        if (element.parent('.input-group').length) {
                            error.insertAfter(element.parent());
                        } else {
                            error.insertAfter(element);
                        }
                    }
                });
                var $valid = $("#mod_formff").valid();
                if (!$valid) {
                    $validator.focusInvalid();
                } else {
                    //==========================================================
                    var meta_id = document.getElementById("meta_id").value;
                    var indis = document.getElementById("indi").value;
                    var metas = document.getElementById("metas").value;
                    var ejecs = document.getElementById("ejecs").value;
                    var rp = document.getElementById("repr").value;

                    var url = "<?php echo site_url("admin")?>/proy/update_meta";
                    $.ajax({
                        type: "post",
                        url: url,
                        data: {
                            indis: indis,
                            metas: metas,
                            ejecs: ejecs,
                            meta_id: meta_id,
                            rp: rp
                        },
                        success: function (data) {
                            window.location.reload(true);
                        }
                    });
                }
            });
        });
    });
</script>
<!--================= NUEVO FUENTE FINANCIAMIENTO =========================================-->

<script type="text/javascript">
    $(function () {
        var id_p = '';
        $('#modal_nuevo_ff').on('hidden.bs.modal', function () {
            document.forms['form_ff'].reset();

        });
        $("#enviar_ff").on("click", function (e) {

            //========================VALIDANDO FORMULARIO===================
            var $validator = $("#form_ff").validate({
                //////////////// DATOS GENERALES
                rules: {
                    ind: { //// indicador
                        required: true,
                    },
                    rp: { //// Resultado Producto
                        required: true,
                    },
                    meta: { //// meta
                        required: true,
                        number: true,
                        min: 1,
                    }
                },
                messages: {
                    ind: "Describa el indicador",
                    rp: "Seleccione Resultado Producto",
                    meta: {required: "Ingrese el meta", number: "Dato Inválido", min: "Dato Inválido"},
                    
                },
                highlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                },
                unhighlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function (error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
            var $valid = $("#form_ff").valid();
            if (!$valid) {
                $validator.focusInvalid();
                //return false;
            } else {
                //==========================================================
                var id_pr = document.getElementById("id_pr").value;
                var ind = document.getElementById("ind").value;
                var meta = document.getElementById("meta").value;
                var ejec = document.getElementById("ejec").value;
                var rp = document.getElementById("rp").value;
                //=================== VERIFICAR SI EXISTE EL COD DE PROGRAMA ==============
                
                var url = "<?php echo site_url("admin")?>/proy/add_meta";
                            $.ajax({
                                type: "post",
                                url: url,
                                data: {
                                    id_pr: id_pr,
                                    ind: ind,
                                    meta: meta,
                                    ejec: ejec,
                                    rp: rp
                                },
                                success: function (data) {
                                    if (data == 'true') {
                                        window.location.reload(true);
                                    } else {
                                        alert(data);
                                    }
                                }
                            });
            }
        });
    });
</script>
</body>

</html>
