<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
		<title><?php echo $this->session->userdata('name')?></title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
        <link href="<?php echo base_url(); ?>assets/file/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
        <script src="<?php echo base_url(); ?>assets/file/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/file/js/fileinput.min.js" type="text/javascript"></script>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.core.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css" id="toggleCSS" />
        <style type="text/css">
            aside{background: #05678B;}
        </style>
        <style>
            .show-grid [class^="col-"] {
                padding-top: 10px;
                padding-bottom: 10px;
                background-color: rgba(61, 106, 124, 0.15);
                border: 1px solid rgba(61, 106, 124, 0.2);
            }
        
            .show-grid {
                margin-bottom: 15px;
                margin-bottom: 15px;
            }
            table{font-size: 10px;
            width: 100%;
            max-width:1550px;;
			overflow-x: scroll;
            }
            th{
              padding: 1.4px;
              text-align: center;
              font-size: 10px;
            }
        </style>	
	</head>
	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->
		<!-- HEADER -->
		<header id="header">
			<!-- pulled right: nav area -->
			<div class="pull-right">
				<!-- collapse menu button -->
				<div id="hide-menu" class="btn-header pull-right">
					<span> <a href="javascript:void(0);" data-action="toggleMenu" title="Menu"><i class="fa fa-reorder"></i></a> </span>
				</div>
				<!-- end collapse menu -->
				<!-- logout button -->
				<div id="logout" class="btn-header transparent pull-right">
					<span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Salir" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
				</div>
				<!-- end logout button -->
				<!-- search mobile button (this is hidden till mobile view port) -->
				<div id="search-mobile" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
				</div>
				<!-- end search mobile button -->
				<!-- fullscreen button -->
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Pantalla Completa"><i class="fa fa-arrows-alt"></i></a> </span>
				</div>
				<!-- end fullscreen button -->
			</div>
			<!-- end pulled right: nav area -->
		</header>
		<!-- END HEADER -->

		<!-- Left panel : Navigation area -->
		<aside id="left-panel">

			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                        <span>
                            <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                        </span>
						<i class="fa fa-angle-down"></i>
					</a> 
					
				</span>
			</div>

			<nav>
                <ul>
                    <li class="">
                        <a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MENÃš PRINCIPAL"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
                    </li>
                    <li class="text-center">
                        <?php
                        if($proyecto[0]['proy_estado']==1){ ?>
                            <a href="<?php echo base_url().'index.php/admin/proy/list_proy' ?>" title="MIS PROYECTOS"><span class="menu-item-parent">PROGRAMACI&Oacute;N DEL POA</span></a>
                            <?php
                        }
                        elseif ($proyecto[0]['proy_estado']==2){ ?>
                            <a href="<?php echo base_url().'index.php/admin/proy/list_proy_poa' ?>" title="MIS PROYECTOS"><span class="menu-item-parent">PROGRAMACI&Oacute;N DEL POA</span></a>
                            <?php
                        }
                        ?>
                    </li>
                    <?php
                    if($nro_fase==1){
                        for($i=0;$i<count($enlaces);$i++){ ?>
                            <li>
                                <a href="#" >
                                    <i class="<?php echo $enlaces[$i]['o_image'];?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
                                <ul>
                                    <?php
                                    $submenu= $this->menu_modelo->get_Modulos_sub($enlaces[$i]['o_child']);
                                    foreach($submenu as $row) {
                                        ?>
                                        <li><a href="<?php echo base_url($row['o_url'])."/".$mod."/".$id_f[0]['id']."/".$id_f[0]['proy_id']; ?>"><?php echo $row['o_titulo']; ?></a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                            <?php
                        }
                    }
                    ?>
                </ul>
			</nav>
			<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>

		</aside>

		<!-- MAIN PANEL -->
		<div id="main" role="main">
			<!-- RIBBON -->
			<div id="ribbon">
				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>

				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<?php
					if($proyecto[0]['proy_estado']==1){ ?>
						<li><a href="<?php echo base_url().'index.php/admin/proy/list_proy' ?>" title="MIS PROYECTOS">Mis Operaciones</a></li><li><a href="<?php echo base_url().'index.php/admin/proy/list_proy' ?>">T&eacute;cnico de Unidad Ejecutora</a></li><li>Archivos Adjuntos</li>
						<?php
					}
					elseif ($proyecto[0]['proy_estado']==2){ ?>
						<li><a href="<?php echo base_url().'index.php/admin/proy/list_proy_poa' ?>" title="MIS PROYECTOS">Mis Operaciones</a></li><li><a href="<?php echo base_url().'index.php/admin/proy/list_proy' ?>">T&eacute;cnico Analista POA</a></li><li>Archivos Adjuntos</li>
						<?php
					}
					?>
				</ol>
			</div>
			<!-- END RIBBON -->

			<!-- MAIN CONTENT -->
			<div id="content">
				
				<div class="row">
					<nav role="navigation" class="navbar navbar-default navbar-inverse">
				        <div class="navbar-header">
				            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
				                <span class="sr-only">Toggle navigation</span>
				                <span class="icon-bar"></span>
				                <span class="icon-bar"></span>
				                <span class="icon-bar"></span>
				            </button>
				        </div>
				 
				        <div id="navbarCollapse" class="collapse navbar-collapse">
				            <ul class="nav navbar-nav">
				                <li><a href="<?php echo base_url().'index.php/admin/proy/edit/'.$proyecto[0]['proy_id'].'/1' ?>" title="DATOS GENERALES"><font size="2">&nbsp;DATOS GENERALES&nbsp;</font></a></li>
				                <li><a href="<?php echo base_url().'index.php/admin/proy/proyecto/'.$proyecto[0]['proy_id'].'/10' ?>" title="<?php echo strtoupper($titulo);?> DEL PROYECTO"><font size="2">&nbsp;<?php echo strtoupper($titulo);?>&nbsp;</font></a></li>
				                <?php
		                        if($this->session->userdata("rol_id")==1){
		                            ?><li><a href="<?php echo base_url().'index.php/admin/proy/proyecto/'.$proyecto[0]['proy_id'].'/2' ?>"><font size="2">&nbsp;RESPONSABLES&nbsp;</font></a></li><?php
		                        }
		                        ?>
				                <li><a href="<?php echo base_url().'index.php/admin/proy/proyecto/'.$proyecto[0]['proy_id'].'/3' ?>" title="CLASIFICACION"><font size="2">&nbsp;CLASIFICACI&Oacute;N&nbsp;</font></a></li>
				                <li><a href="<?php echo base_url().'index.php/admin/proy/proyecto/'.$proyecto[0]['proy_id'].'/4?qRegistro='.$proyecto[0]['proy_id'].'' ?>" title="LOCALIZACION"><font size="2">&nbsp;LOCALIZACI&Oacute;N&nbsp;</font></a></li>
				                
				                <?php
		                            if($proyecto[0]['tp_id']==1 || $proyecto[0]['tp_id']==2 || $proyecto[0]['tp_id']==3){ ?>
		                                <li><a href="<?php echo base_url().'index.php/admin/proy/proyecto/'.$proyecto[0]['proy_id'].'/5' ?>" title="MARCO LOGICO"><font size="2">&nbsp;OBJETIVOS&nbsp;</font></a></li>
		                                <?php
		                            }
		                        ?>
				                <li><a href="<?php echo base_url().'index.php/admin/proy/proyecto/'.$proyecto[0]['proy_id'].'/7' ?>" title="RESUMEN TECNICO"><font size="2">&nbsp;RESUMEN T&Eacute;CNICO&nbsp;</font></a></li>
				                <li class="active"><a href="#"><i class="glyphicon glyphicon-ok"></i><font size="2">ANEXOS DE LA OPERACI&Oacute;N</font></a></li>
				            	<?php
				            		if($proyecto[0]['proy_estado']!=4){
				            			if($proyecto[0]['tp_id']==1){ ?>
				            				<li><a href='<?php echo site_url("admin").'/proy/fase_etapa/'.$proyecto[0]['proy_id']; ?>' title="FASE ETAPA COMPONENTE"><font size="2">&nbsp;FASE / ETAPA&nbsp;</font></a></li>
				            			<?php
					            		}
					            		elseif ($proyecto[0]['tp_id']==2 || $proyecto[0]['tp_id']==3 || $proyecto[0]['tp_id']==4) { ?>
					            			<li><a href="<?php echo base_url().'index.php/admin/proy/proyecto/'.$proyecto[0]['proy_id'].'/9' ?>" title="MONTO PRESUPUESTO"><font size="2">&nbsp;PRESUPUESTO&nbsp;</font></a></li>
					            			<?php
					            		}
				            		}
				            	?>
				            </ul>
				        </div>
					</nav>
				</div>

        <div class="row">
            <div class="col-md-12">
                <h2 class="well well-sm text-justify" style="margin:0 0 7px 0 !important;">
					<?php echo strtoupper($proyecto[0]['tipo']);?>: 
					<span class="text-success"><?php echo strtoupper($proyecto[0]['proy_nombre']);?></span>
					<hr style="margin:2px auto; border-bottom:1px solid #eee;">
	                <small>
						<b class="lead">
						FECHA DE INICIO : <span class="text-primary"><?php echo date('d-m-Y',strtotime($proyecto[0]['f_inicial'])); ?></span>
						&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
						FECHA DE CONCLUSI&Oacute;N : <span class="text-primary"><?php echo date('d-m-Y',strtotime($proyecto[0]['f_final'])); ?></span>
						</b>
					</small>
				</h2>
            </div>
        </div>
				<!-- widget grid -->
				<section id="widget-grid" class="">
					<!-- row -->
					<div class="row">
					<?php  
					if($this->uri->segment(6)==1 && $this->uri->segment(7)==0){ ?>
						<h4 class="alert alert-danger">Error al subir Archivo.. vuelva a subir el documento...</h4>
						<?php
					}
					elseif ($this->uri->segment(6)==1 && $this->uri->segment(7)==1){ ?>
						<h4 class="alert alert-danger">Error al subir Archivo.. El archivo debe ser menor o igual a 10 MG</h4>
						<?php
					}
					?>
						<!-- end widget -->
						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
							<?php 
			                  if($this->session->flashdata('success')){ ?>
			                    <div class="alert alert-success">
			                      <?php echo $this->session->flashdata('success'); ?>
			                    </div>
			                <?php 
			                    }
			                  elseif($this->session->flashdata('danger')){ ?>
			                      <div class="alert alert-danger">
			                        <?php echo $this->session->flashdata('danger'); ?>
			                      </div>
			                      <?php
			                  }
			                ?>
			                <?php
					            $attributes = array('class' => 'form-horizontal', 'id' => 'wizard-1','name' =>'wizard-1','enctype' => 'multipart/form-data');
					            echo validation_errors();
					            echo form_open('admin/proy/add_arch', $attributes);
					        ?>
							<form id="wizard-1" novalidate="novalidate" method="post" enctype="multipart/form-data" >
								<input class="form-control" type="hidden" name="id" value="<?php echo $proyecto[0]['proy_id'] ?>">
								<input class="form-control"  name="tp_doc" type="hidden" id="tp_doc" >

								<div class="jarviswidget jarviswidget-color-darken" >
										<header>
											<span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
											<h2 class="font-md"><strong>ANEXOS DE LA OPERACI&Oacute;N</strong></h2>				
										</header>
											
										<div class="panel-body">
											<div class="col-sm-12">
												<label class="control-label">Seleccione Documento/Archivo <font color="blue">(Obligatorio)</font></label>
												<input id="file1" name="file1" type="file" class="file" multiple data-show-upload="false" data-show-caption="true" title="SELECCIONE EL ARCHIVO, DOCUMENTO">
											</div>	
        									<div class="col-sm-12">
												<small class="text-info">El archivo no debe superar los <b>20 MB</b> de tamaño</small>
												<br>
												<label class="control-label">Descripci&oacute;n del Documento/Archivo <font color="blue">(Obligatorio)</font></label>
												<textarea rows="3" class="form-control" name="doc" id="doc" style="width:100%;" title="Descripcion del documento" maxlength="50"></textarea> 
        									</div>
        									<div class="col-sm-12"><hr></div>
        									<div class="col-sm-12">
												<input type="button" name="Submit" value="SUBIR ARCHIVO" id="btsubmit" class="btn btn-success btn-lg" onclick="comprueba_extension(this.form, this.form.file1.value,this.form.doc.value)" style="width:100%;">
        									</div>
        									<div class="col-sm-12"><hr></div>	
										</div>
							                   
										<div class="form-actions">
											<?php
												if($proyecto[0]['proy_estado']==1){ ?>
													<a href="<?php echo base_url().'index.php/admin/proy/list_proy' ?>" class="btn btn-lg btn-default" title="VOLVER A MIS PROYECTOS"> CANCELAR </a>
													<?php
												}
												elseif ($proyecto[0]['proy_estado']==2){ ?>
													<a href="<?php echo base_url().'index.php/admin/proy/list_proy_poa' ?>" class="btn btn-lg btn-default" title="VOLVER A MIS PROYECTOS"> CANCELAR </a>
													<?php
												}

							            		if($proyecto[0]['tp_id']==1){ ?>
							            			<a href='<?php echo site_url("admin").'/proy/fase_etapa/'.$proyecto[0]['proy_id']; ?>' class="btn btn-primary btn-lg" title="REGISTRAR FASE Y ETAPAS DEL PROYECTO" >PASAR A REGISTRAR FASES DE LA OPERACI&Oacute;N</a>
							            			<?php
							            		}
							            		elseif ($proyecto[0]['tp_id']==2 || $proyecto[0]['tp_id']==3 || $proyecto[0]['tp_id']==4) { ?>
							            			<a href="<?php echo base_url().'index.php/admin/proy/proyecto/'.$proyecto[0]['proy_id'].'/9' ?>" class="btn btn-primary btn-lg" title="MONTO DEL PROYECTO" >PASAR A REGISTRAR PRESUPUESTO</a>
							            			<?php
							            		}
							            	?>
										</div>
							        </div>	
							</form>
						</article>
						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                            <div class="jarviswidget jarviswidget-color-darken" >
								<header>
									<span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
									<h2 class="font-md"><strong>ANEXOS DE LA OPERACI&Oacute;N</strong></h2>				
								</header>
                            </div>
								<div>
									<!-- widget content -->
								<div class="widget-body">
									<div class="well">
									<div class="table-responsive">
										<table class="table table-bordered">
											<thead>			                
												<tr>
													<th style="width:1%;">Nro</th>
													<th style="width:15%;">ARCHIVO / DOCUMENTO</th>
													<th style="width:5%;">FECHA</th>
													<th style="width:5%;">FUNCIONARIO</th>
													<th style="width:5%;">VER ARCHIVO</th>
													<th style="width:5%;">ELIMINAR ARCHIVO</th>
												</tr>
											</thead>
											<?php $num=1;
		                                      foreach($arch as $row)
		                                      {
		                                       echo '<tr>';
		                                        echo '<td><font size="1">'.$num.'</font></td>';
		                                        echo '<td><font size="1">'.$row['documento'].'</font></td>';
		                                        echo '<td><font size="1">'.date('d-m-Y',strtotime($row['fecha_creacion'])).'</font></td>';
		                                        echo '<td><font size="1">'.$row['fun_nombre'].' '.$row['fun_paterno'].' '.$row['fun_materno'].'</font></td>';
		                                        echo '<td align="center">';
		                                        if(file_exists("archivos/documentos/".$row['adj_adjunto'])) ////// Existe Archivo Almacenado
		                                        {
		                                        	if($row['tp_doc']==1) ///// imagen
		                                        	{
		                                        		?>
			                                           <a href="<?php echo base_url(); ?>archivos/documentos/<?php echo $row['adj_adjunto'] ?>" target="_blank" title="VER ARCHIVO/DOCUMENTO - IMAGEN"><img src="<?php echo base_url(); ?>assets/ifinal/img.jpg" WIDTH="35" HEIGHT="35"/>
			                                        	<?php
		                                        	}
		                                        	elseif ($row['tp_doc']==2) ///// pdf
		                                        	{
		                                        		?>
			                                           <a href="<?php echo base_url(); ?>archivos/documentos/<?php echo $row['adj_adjunto'] ?>" target="_blank" title="VER ARCHIVO/DOCUMENTO - PDF"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="35" HEIGHT="35"/>
			                                        	<?php
		                                        	}
		                                        	elseif ($row['tp_doc']==3) ///// word
		                                        	{
		                                        		?>
			                                           <a href="<?php echo base_url(); ?>archivos/documentos/<?php echo $row['adj_adjunto'] ?>" target="_blank" title="VER ARCHIVO/DOCUMENTO - WORD"><img src="<?php echo base_url(); ?>assets/ifinal/word.png" WIDTH="35" HEIGHT="35"/>
			                                        	<?php
		                                        	}
		                                        	elseif ($row['tp_doc']==4) ///// excel
		                                        	{
		                                        		?>
			                                           <a href="<?php echo base_url(); ?>archivos/documentos/<?php echo $row['adj_adjunto'] ?>" target="_blank" title="VER ARCHIVO/DOCUMENTO - EXCEL"><img src="<?php echo base_url(); ?>assets/ifinal/excel.jpg" WIDTH="35" HEIGHT="35"/>
			                                        	<?php
		                                        	}
		                                        	
		                                        }
		                                        else ////// No Existe Archivo Almacenado
		                                        {
		                                        	?>
		                                           <a href="#" target="_blank" class="btn btn-labeled btn-success" title="NOSE ENCUENTRA ARCHIVO/DOCUMENTO FISICO"><i class="glyphicon glyphicon-file"></i>
		                                        	<?php
		                                        }
		                                        echo '</td>';
		                                        ?>
		                                           <td align="center"><a href="#" data-toggle="modal" data-target="#modal_del_ff" class="btn btn-xs del_ff" title="ELIMINAR ARCHIVO"  name="<?php echo $row['proy_id'];?>" id="<?php echo $row['adj_id'];?>"><img src="<?php echo base_url(); ?>assets/ifinal/eliminar.png" WIDTH="35" HEIGHT="35"/></a></td>
		                                        <?php 
		                                        
		                                      echo '</tr>';
		                                      $num=$num+1;
		                                      }
		                                     ?>
											</table>
									</div>
								</div>
								</div>
							</div>
							</form>
						</article>
					</div>
					<!-- end row -->
				</section>
				<!-- end widget grid -->					
			</div>
			<!-- END MAIN CONTENT -->

		</div>
		<!-- END MAIN PANEL -->

		<!-- PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->
		<!--================================================== -->
		<script language="javascript">
		
		</script>
		<script>
			if (!window.jQuery) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-2.0.2.min.js"><\/script>');
			}
		</script>
		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>
		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url(); ?>assets/js/session_time/jquery-idletimer.js"></script>
		<script src="<?php echo base_url();?>/assets/js/app.config.js"></script>
		<SCRIPT src="<?php echo base_url(); ?>mis_js/programacion/programacion/datos_generales.js" type="text/javascript"></SCRIPT>
		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url();?>/assets/js/bootstrap/bootstrap.min.js"></script>
		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url();?>/assets/js/notification/SmartNotification.min.js"></script>
		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url();?>/assets/js/smartwidgets/jarvis.widget.min.js"></script>
		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url();?>/assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<!-- SPARKLINES -->
		<script src="<?php echo base_url();?>/assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/select2/select2.min.js"></script>
		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
		<!-- browser msie issue fix -->
		<script src="<?php echo base_url();?>/assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url();?>/assets/js/plugin/fastclick/fastclick.min.js"></script>
		<!-- Demo purpose only -->
		<script src="<?php echo base_url();?>/assets/js/demo.min.js"></script>
		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url();?>/assets/js/app.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
		<!-- Voice command : plugin -->
		<script src="<?php echo base_url();?>/assets/js/speech/voicecommand.min.js"></script>
		<!-- PAGE RELATED PLUGIN(S) -->
		<script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
		<script src="<?php echo base_url();?>/assets/js/plugin/fuelux/wizard/wizard.min.js"></script>
		<script type="text/javascript">
		    $(function () {
		        function reset() {
		            $("#toggleCSS").attr("href", "<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css");
		            alertify.set({
		                labels: {
		                    ok: "ACEPTAR",
		                    cancel: "CANCELAR"
		                },
		                delay: 5000,
		                buttonReverse: false,
		                buttonFocus: "ok"
		            });
		        }

		        // =====================================================================
		        $(".del_ff").on("click", function (e) {
		            reset();
		            var name = $(this).attr('name'); 
		            var id = $(this).attr('id'); 
		            var request;
		            // confirm dialog
		            alertify.confirm("REALMENTE DESEA ELIMINAR ESTE ARCHIVO ?", function (a) {
		                if (a) { 
		                    url = "<?php echo site_url("")?>/proy/delete_arch";
		                    if (request) {
		                        request.abort();
		                    }
		                    request = $.ajax({
		                        url: url,
		                        type: "POST",
		                        dataType: "json",
                    			data: "proy_id="+name+"&adj_id="+id

		                    });

		                    request.done(function (response, textStatus, jqXHR) { 
			                    reset();
			                    if (response.respuesta == 'correcto') {
			                        alertify.alert("EL ARCHIVO SE ELIMIN\u00D3 CORRECTAMENTE", function (e) {
			                            if (e) {
			                                window.location.reload(true);
			                            }
			                        });
			                    } else {
			                        alertify.alert("ERROR AL ELIMINAR EL ARCHIVO!!!", function (e) {
			                            if (e) {
			                                window.location.reload(true);
			                            }
			                        });
			                    }
			                });
		                    request.fail(function (jqXHR, textStatus, thrown) {
		                        console.log("ERROR: " + textStatus);
		                    });
		                    request.always(function () {
		                        //console.log("termino la ejecuicion de ajax");
		                    });

		                    e.preventDefault();

		                } else {
		                    // user clicked "cancel"
		                    alertify.error("Opcion cancelada");
		                }
		            });
		            return false;
		        });
		    });

		</script>
	</body>
</html>
