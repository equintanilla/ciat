<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
		<title><?php echo $this->session->userdata('name')?></title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css"> 
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<!--estiloh-->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css"> 
		    <!--para las alertas-->
    	<meta name="viewport" content="width=device-width">
		<!--fin de stiloh-->
			<script>
			function abreVentana(PDF)
            {             
                 var direccion;
                 direccion = '' + PDF;
                 window.open(direccion, "Reporte de Proyectos" , "width=1000,height=650,scrollbars=SI") ; 
                                                                                
            }
            function abreVentanaFis(PDF)
            {             
                 var direccion;
                 direccion = '' + PDF;
                 window.open(direccion, "Reporte de Proyectos" , "width=1140,height=650,scrollbars=SI") ; 
                                                                                
            } 
            function abreVentanaFin(PDF)
            {             
                 var direccion;
                 direccion = '' + PDF;
                 window.open(direccion, "Reporte de Proyectos" , "width=1280,height=650,scrollbars=SI") ; 
                                                                                
            }                                            
          </script>
			<style>
			/*////scroll tablas/////*/
			table{font-size: 12px;
            width: 100%;
            max-width:1550px;;
			overflow-x: scroll;
            }
            th{
              padding: 1.4px;
              text-align: center;
              font-size: 12px;
            }
			</style>
	</head>
	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->
		<!-- HEADER -->
<header id="header">
	<div id="logo-group" class="col-md-2">
		<span id="logo"> <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="<?php echo $this->session->userdata('name')?>"> </span>
	</div>
	<div class="col-md-6 no-print" style="font-size:15px; margin-top:15px; margin-bottom:-10px;">
		<div class="text-center">
			<span>
				<div class="badge bg-color-blue" style="padding:7px;">
					<span style="font-size:1.6em;">Gesti&oacute;n de trabajo: <?php echo $this->session->userdata('desc_mes').'/'.$this->session->userdata('gestion');?></span>
				</div>
			</span>
			<div class="project-context hidden-xs">
				<span class="project-selector" style="font-size:0.7em;">
					<i class="fa fa-lg fa-fw fa-calendar txt-color-blue"></i>
					<a href="<?php echo base_url();?>index.php/cambiar_gestion">Cambiar Gestión</a>
				</span>
	<!--			<ul class="dropdown-menu">
					<li>
						<a href="<?php echo base_url();?>index.php/cambiar_gestion">Cambiar Gestión</a>
					</li>
				</ul>-->
			</div>
		</div>
	</div>
	<!-- pulled right: nav area -->
	<div class="pull-right col-md-2 no-print">
		<!-- logout button -->
		<div id="logout" class="btn-header transparent pull-right">
            <span> 
				<a href="<?php echo base_url(); ?>index.php/admin/logout" title="Salir" data-action="userLogout" data-logout-msg="Seguro de Salir?">
					Salir <i class="fa fa-sign-out"></i>
				</a> 
			</span>
		</div>
		<!-- end logout button -->

		<!-- fullscreen button -->
		<div id="fullscreen" class="btn-header transparent pull-right">
            <span> 
				<a href="javascript:void(0);" data-action="launchFullscreen" title="Pantalla Completa">
					<i class="fa fa-arrows-alt"></i>
				</a> 
			</span>
		</div>
		<!-- end fullscreen button -->
		<!-- collapse menu button -->
		<div id="hide-menu" class="btn-header pull-right">
            <span> 
				<a href="javascript:void(0);" data-action="toggleMenu" title="Menu">
					<i class="fa fa-reorder"></i>
				</a>
			</span>
		</div>
		<!-- end collapse menu -->

	</div>
	<!-- end pulled right: nav area -->

</header>
		<!-- END HEADER -->

		<!-- Left panel : Navigation area -->
		<aside id="left-panel">

			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                        <span>
                            <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                        </span>
						<i class="fa fa-angle-down"></i>
					</a> 
					
				</span>
			</div>

			<nav>
				<ul>
					<li class="">
	                <a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MENÚ PRINCIPAL"><i
	                        class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
	            	</li>
		            <li class="text-center">
		                <a href="#" title="PROGRAMACION"> <span class="menu-item-parent">PROGRAMACI&Oacute;N</span></a>
		            </li>
					<?php
		                for($i=0;$i<count($enlaces);$i++)
		                {
		                    if(count($subenlaces[$enlaces[$i]['o_child']])>0)
		                    {
			            ?>
			            <li>
			              	<a href="#" >
			              		<i class="<?php echo $enlaces[$i]['o_image']?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
			              	<ul >
			              	<?php
			                foreach ($subenlaces[$enlaces[$i]['o_child']] as $item) {
			                ?>
			                <li><a href="<?php echo base_url($item['o_url']); ?>"><?php echo $item['o_titulo']; ?></a></li>
			                <?php } ?>
			                </ul>
			            </li>
			            <?php 
			            }
			        } ?>
				</ul>
			</nav>
			<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>
		</aside>

		<!-- MAIN PANEL -->
		<div id="main" role="main">
			<!-- RIBBON -->
			<div id="ribbon">
				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>
				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li>Mis Operaciones</li><li>Donde Estan mis Operaciones ?</li>
				</ol>
			</div>

			<!-- MAIN CONTENT -->
			<div id="content">
				<section id="widget-grid" class="">
					<div class="row">
						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="jarviswidget jarviswidget-color-darken" >
                              <header>
                                  <span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
                                  <h2 class="font-md"><strong>DONDE ESTAN MIS OPERACIONES <?php echo $this->session->userdata("gestion")?> ?</strong></h2>  
                              </header>
								<div>
									<div class="widget-body no-padding">
									<div class="table-responsive">
										<table id="dt_basic" class="table table-bordered" style="width:100%;" font-size: "7px";>
											<thead>
												<tr>
													<th style="width:1%;"></th>
													<th>APERTURA PROGRAMATICA <?php echo $this->session->userdata("gestion");?></center></th>
													<th>PROYECTO_PROGRAMA_OPERACI&Oacute;N DE FUNCIONAMIENTO</th>
													<th>TIPO DE OPERACI&Oacute;N</th>
													<th>C&Oacute;DIGO_SISIN</th>
													<th>RESPONSABLE (UE)</th>
													<th>UNIDAD_EJECUTORA</th>
													<th>UNIDAD_RESPONSABLE</th>
													<th>FASE_ETAPA</th>
													<th>NUEVO_CONTINUIDAD</th>
													<th>ANUAL_PLURIANUAL</th>
													<th>TOTAL PRESUPUESTO ETAPA</th>
													<th>PRESUPUESTO REQUERIDO&nbsp;<?php echo $this->session->userdata("gestion");?></th>
													<th>TECHO PRESUPUESTO ASIGNADO</th>
													<th>PROVINCIA_MUNICIPIO</th>
												</tr>
											</thead>
											<tbody>
											<?php $nro=1;
			                                    foreach($lista_aper_padres  as $rowa)
			                                    {
			                                    	$proyectos=$this->model_proyecto->proy_actividades($rowa['aper_programa'],$this->session->userdata('gestion'));
			                                    	if(count($proyectos)!=0)
			                                    	{
			                                    		echo '<tr bgcolor="#99DDF0">';
			                                    	 	echo '<td></td>';
			                                    	 	echo '<td><center>' .$rowa['aper_programa'].''.$rowa['aper_proyecto'].''.$rowa['aper_actividad'].'</td></td>';
											            echo '<td>'.$rowa['aper_descripcion'].'</td>';
											            echo '<td>'.$rowa['aper_sisin'].'</td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
			                                    		echo '</tr>';

			                                    		foreach($proyectos  as $row)
				                                    	{
				                                    	if($row['tp_id']!=1){$color='#D7FCF9';}else{$color='#ffffff';}
				                                    	$fase= $this->model_faseetapa->get_id_fase($row['proy_id']); ////// datos fase activa
				                                    	$nro_f = $this->model_faseetapa->nro_fase($row['proy_id']); //// nro de fases 
				                                    	echo '<tr bgcolor='.$color.'>';
				                                    	echo '<td>';
														echo  '<div class="btn-group-vertical dropup"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-list"></i> ACCIONES <span class="caret"></span></button><ul class="dropdown-menu" role="menu">';
				                                       	?>
				                                       	<li><a href="javascript:abreVentana('<?php echo site_url("admin").'/dictamen_proyecto/'.$row['proy_id']; ?>');" title="DICTAMEN DEL PROYECTO"><i class="glyphicon glyphicon-file"></i> Resumen de la operaci&oacute;n en PDF</a></li>
				                                       	<?php
				                                       	if($this->session->userdata('rol_id')==1)
														{
															?>
															<li><a data-toggle="modal" data-target="#modal_mod_aper2" class="mod_aper2" name="<?php echo $row['proy_id'];?>" id="3" id="OBSERVACION AL PROYECTO"><i class="glyphicon glyphicon-eye-open"></i> Observar operaci&oacute;n</a></li>
															<?php
														}
														echo "</ul>";
														if($nro_f!=0) 
				                                        {
				                                        	?>
				                                        	<button class="btn btn-default" data-toggle="modal" data-target="#<?php echo $nro;?>" title="MIS REPORTES" ><i class="glyphicon glyphicon-stats"></i> Mis reportes</button>
				                                        	<div class="modal fade bs-example-modal-lg" tabindex="-1" id="<?php echo $nro;?>"  role="dialog" aria-labelledby="myLargeModalLabel">
											                    <div class="modal-dialog modal-lg" role="document">
											                     	<div class="modal-content">
																		<div class="modal-header">
																			<button type="button" class="close text-danger" data-dismiss="modal" aria-hidden="true">
																				&times;
																			</button>
																		    <h4 class="modal-title">
																	            <img src="<?php echo base_url(); ?>assets/img/logo.png" width="150" alt="SmartAdmin">
																	        </h4>
																		</div>
																		<div class="modal-body no-padding">
																			<div class="well">
																				<table class="table table-hover">
																					<thead>
																					<tr>
																						<th colspan="3"><center><?php echo $row['proy_nombre'];?></center></th>
																					</tr>
																					</thead>
																					<tr>
																						<td><b>IDENTIFICACI&Oacute;N DE LA ETAPA</b></td>
																						<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/prog/identificacion_proy/'.$row['proy_id']; ?>');" title="IDENTIFICACION DE LA ETAPA"><i style="font-size:30px;" class="glyphicon glyphicon-print"></i><br>Identificación</a></center></td>
																						<td><!--<center><a href="javascript:abreVentana('<?php echo site_url("admin").'/prog/identificacion_proy/'.$row['proy_id']; ?>');" title="IDENTIFICACION DE LA ETAPA"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center>--></td>
																					</tr>
																					<tr>
																						<td><b>PROGRAMACI&Oacute;N Y EJECUCI&Oacute;N F&Iacute;SICA DE LA ETAPA</b></td>
																						<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/prog/ejecucion_pfisico/1/'.$fase[0]['id'].'/'.$row['proy_id']; ?>');" title="PROGRAMACION Y EJECUCION FISICA ANUAL DE LA ETAPA"><i style="font-size:30px;" class="glyphicon glyphicon-print"></i><br>Anual</a></center></td>
																						<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/prog/ejecucion_pfisico_m/4/'.$fase[0]['id'].'/'.$row['proy_id']; ?>');" title="PROGRAMACION Y EJECUCION FISICA PLURI-ANUAL DE LA ETAPA"><i style="font-size:30px;" class="glyphicon glyphicon-print"></i><br>Pluri Anual</a></center></td>
																					</tr>
																					<tr>
																						<td><b>PROGRAMACI&Oacute;N Y EJECUCI&Oacute;N FINANCIERA DE LA ETAPA</b></td>
																						<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/prog/rep_financiero/1/'.$fase[0]['id'].'/'.$row['proy_id']; ?>');" title="PROGRAMACION Y EJECUCION FINANCIERA ANUAL DE LA ETAPA"><i style="font-size:30px;" class="glyphicon glyphicon-print"></i><br>Anual</a></center></td>
																						<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/prog/rep_financiero/4/'.$fase[0]['id'].'/'.$row['proy_id']; ?>');" title="PROGRAMACION Y EJECUCION FINANCIERA PLURI-ANUAL DE LA ETAPA"><i style="font-size:30px;" class="glyphicon glyphicon-print"></i><br>Pluri Anual</a></center></td>
																					</tr>
																					<tr>
																						<td><b>REGISTRO DE CONTRATOS</b></td>
																						<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/prog/reporte_contrato/'.$row['proy_id'].'/'.$fase[0]['id'].''; ?>');" title="REPORTE DE CONTRATOS"><i style="font-size:30px;" class="glyphicon glyphicon-print"></i><br>Contratos</a></center></td>
																						<td></td>
																					</tr>
																					<tr>
																						<td><b>INFORMES DE SUPERVISI&Oacute;N Y EVALUACI&Oacute;N</b></td>
																						<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/prog/reporte_supervision/'.$row['proy_id'].'/'.$fase[0]['id'].''; ?>');" title="REPORTE DE SUPERVISIÓN"><i style="font-size:30px;" class="glyphicon glyphicon-print"></i><br>Supervisión</a></center></td>
																						<td></td>
																					</tr>
																					<tr>
																						<td><b>CURVA "S" AVANCE F&Iacute;SICO </b></td>
																						<td><center><a href="javascript:abreVentanaFis('<?php echo site_url("admin").'/imprimir_curva_fis/1/'.$fase[0]['id'].'/'.$row['proy_id']; ?>');" title="GRAFICO AVANCE FISICO ANUAL"><i style="font-size:30px;" class="glyphicon glyphicon-print"></i><br>Anual</a></center></td>
																						<td><center><a href="javascript:abreVentanaFis('<?php echo site_url("admin").'/imprimir_curva_fis/4/'.$fase[0]['id'].'/'.$row['proy_id']; ?>');" title="GRAFICO AVANCE FISICO PLURI-ANUAL"><i style="font-size:30px;" class="glyphicon glyphicon-print"></i><br>Pluri Anual</a></center></td>
																					</tr>
																					<tr>
																						<td><b>CURVA "S" AVANCE FINANCIERO</b></td>
																						<td><center><a href="javascript:abreVentanaFin('<?php echo site_url("").'/prog/rep_curva_fin/1/'.$fase[0]['id'].'/'.$row['proy_id']; ?>');" title="GRAFICO AVANCE FINANCIERO ANUAL"><i style="font-size:30px;" class="glyphicon glyphicon-print"></i><br>Anual</a></center></td>
																						<td><center><a href="javascript:abreVentanaFin('<?php echo site_url("").'/prog/rep_curva_fin/4/'.$fase[0]['id'].'/'.$row['proy_id']; ?>');" title="GRAFICO AVANCE FINANCIERO PLURI-ANUAL"><i style="font-size:30px;" class="glyphicon glyphicon-print"></i><br>Pluri Anual</a></center></td>
																					</tr>
																				</table>
																			</div>
																		</div>
																	</div><!-- /.modal-content -->
			                                                    </div>
			                                                </div>
				                                        	<?php
				                                        }
														echo '</ul></div>';
				                                       	echo '</td>';
				                                       	echo '<td>';
				                                        echo '<center>'.$row['aper_programa'].''.$row['aper_proyecto'].''.$row['aper_actividad'].'</center><br>';
				                                        if($row['proy_estado']==1)echo '<button class="btn btn-block btn-default" disabled><i class="glyphicon glyphicon-wrench"></i> T&Eacute;CNICO DE<br>UNIDAD<br> EJECUTORA</button>';
				                                        if($row['proy_estado']==2)echo '<button class="btn btn-block btn-default" disabled><i class="glyphicon glyphicon-align-left"></i> VALIDADOR POA</button>';
				                                        if($row['proy_estado']==3)echo '<button class="btn btn-block btn-default" disabled><i class="glyphicon glyphicon-usd"></i> VALIDADOR<br>FINANCIERO</button>';
				                                        if($row['proy_estado']==4)echo '<button class="btn btn-block btn-success" disabled><i class="glyphicon glyphicon-ok"></i> APROBADO</button>';
				                                        if($row['proy_estado']==5)echo '<button class="btn btn-block btn-success" disabled><i class="glyphicon glyphicon-wrench"></i> EN EJECUCIÓN</button>';
				                                        echo'</td>';
				                                        echo '<td>'.$row['proy_nombre'].'</td>';
				                                        echo '<td>'.$row['tp_tipo'].'</td>';
				                                        echo '<td>'.$row['proy_sisin'].'</td>';
				                                        echo '<td>'.$row['fun_nombre'].' '.$row['fun_paterno'].' '.$row['fun_materno'].'</td>';
				                                        echo '<td>'.$row['ue'].'</td>';
				                                        echo '<td>'.$row['ur'].'</td>';
				                                        /*==============================================================================================================*/
				                                      	if($nro_f!=0) //// tiene fase
				                                        {
				                                        	$data['fase'] = $this->model_faseetapa->get_id_fase($row['proy_id']); ////// datos fase activa
				                                        	$nc=$this->model_faseetapa->calcula_nc($data['fase'][0]['pfec_fecha_inicio']); //// calcula nuevo/continuo
				                                        	$ap=$this->model_faseetapa->calcula_ap($data['fase'][0]['pfec_fecha_inicio'],$data['fase'][0]['pfec_fecha_fin']); //// calcula Anual/Plurianual
				                                        	echo '<td>* '.$data['fase'][0]['fase'].'<br>* '.$data['fase'][0]['etapa'].'</td>';
				                                        	echo '<td>'.$nc.'</td>';
				                                        	echo '<td>'.$ap.'</td>';
				                                        	echo '<td>'.number_format($data['fase'][0]['pfec_ptto_fase'], 2, ',', ' ').' Bs.</td>';
				                                        	$fase_gest = $this->model_faseetapa->fase_etapa_gestion($data['fase'][0]['id'],$this->session->userdata("gestion")); ////// datos fase activa presupuesto gestion actual
				                                        	$nro_fg_act = $this->model_faseetapa->nro_fasegestion_actual($data['fase'][0]['id'],$this->session->userdata('gestion'));
				                                        	echo '<td>';
				                                        	if($nro_fg_act!=0 && ($fase_gest[0]['estado']==1 || $fase_gest[0]['estado']==2))
				                                        	{
				                                        		echo ''.number_format($fase_gest[0]['pfecg_ppto_total'], 2, ',', ' ').' Bs.';
				                                        	}
				                                        	elseif ($nro_fg_act==0) {
				                                        		echo '<font color="red">la gestion no esta en curso</font>';
				                                        	}
				                                        	echo '</td>';
				                                        	echo '<td>';
				                                        	if($this->model_faseetapa->verif_fase_etapa_gestion($fase[0]['id'],$this->session->userdata("gestion"))!=0)
				                                        	{	
				                                        		if($this->model_faseetapa->nro_ffofet($fase_gest[0]['ptofecg_id'])!=0)
				                                        		{
				                                        			$techo=$this->model_faseetapa->techo_presupuestario($fase_gest[0]['ptofecg_id']);
																	echo ''.number_format($techo[0]['suma_techo'], 2, ',', ' ').' Bs.';

				                                        		}
				                                        		else{echo "<font color=red>S/T</font>";}
				                                        	}
				                                        	echo '</td>';

				                                        } 
				                                        elseif($nro_f==0) /// no tiene fase
				                                        {
				                                        	echo '<td><center><font color="red"><b>N/R</b></font></center></td>';
				                                        	echo '<td><center><font color="red"><b>N/R</b></font></center></td>';
				                                        	echo '<td><center><font color="red"><b>N/R</b></font></center></td>';
				                                        	echo '<td><center><font color="red"><b>N/R</b></font></center></td>';
				                                        	echo '<td><center><font color="red"><b>N/R</b></font></center></td>';
				                                        	echo '<td><center><font color="red"><b>N/R</b></font></center></td>';
				                                        }
				                                        /*==============================================================================================================*/
				                                      	/*==================================== UBICACION GEOGRAFICA =====================================================*/
				                                        	echo '<td>';
												                $prov=$this->model_proyecto->proy_prov($row['proy_id']);
												                if(count($prov)!=0){
												                    foreach($prov as $locali)
												                    {
												                      echo '<li><b>'.strtoupper($locali['prov_provincia']).' </b>';
												                        $muni=$this->model_proyecto->provincia_municipio($row['proy_id'],$locali['prov_id']);
												                        echo '(';
												                        foreach($muni as $muni)
												                        {
												                          echo ''.$muni['muni_municipio'].',';
												                        }
												                        echo ')';
												                      echo '</li>';
												                    }
												                }
												                
												              echo '</td>';
						                                    /*==============================================================================================================*/
				                                    	echo '</tr>';
				                                    	$nro++;
				                                    	}
			                                    	}
			                                    }
			                                ?>
											</tbody>
										</table>
										</div>
									</div>
									<!-- end widget content -->
								</div>
								<!-- end widget div -->
							</div>
							<!-- end widget -->
						</article>
						<!-- WIDGET END -->
					</div>
				</section>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN PANEL -->
		<!-- ================================= OBSERVAR PROYECTO AL TOP =================================================== -->
		<div  class="modal animated fadeInDown" id="modal_mod_aper2" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close text-danger" data-dismiss="modal" aria-hidden="true">
						&times;
					</button>
				    <h4 class="modal-title">
			            <img src="<?php echo base_url(); ?>assets/img/logo.png" width="150" alt="SmartAdmin">
			        </h4>
						<center><font size="2"><b>PROGRAMA OBSERVADO</b></font></center>
				</div>
				<div class="modal-body no-padding">
					<div class="row">
						<form id="mod_formaper2" name="mod_formaper2" novalidate="novalidate" method="post">
							<div id="bootstrap-wizard-1" class="col-sm-12">
								<div class="well">
								<input class="form-control" type="hidden" name="id" id="id">
								<input class="form-control" type="hidden" name="tpo" id="tpo">
									<div class="row">
										<div class="col-sm-3">
											<div class="form-group">
												
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<LABEL><b style="color: #568a89;"></b><br>
                                                    <b><font size="3" id="responsable_poa"></font></b></label>
											</div>
										</div>
										<div class="col-sm-3">
											<div class="form-group">
												<img src="<?php echo base_url(); ?>assets/ifinal/archivo1.png" WIDTH="70" HEIGHT="70"/>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="form-group">
												<LABEL><b>DESCRIBA LA OBSERVACI&Oacute;N AL PROYECTO</b></label>
												<textarea rows="6" class="form-control" name="observacion" id="observacion" style="width:100%;"></textarea> 
											</div>
										</div>
									</div>

								</div> <!-- end well -->
							</div>
						</form>
					</div>
				</div>
				<div class="modal-footer">
					<div class="row">
						<div class="col-md-3 pull-left">
							<button  class="btn btn-lg btn-danger" data-dismiss="modal">CANCELAR </button>
						</div>
						<div class="col-md-3 pull-right ">
							<button type="submit" name="mod_aperenviar2" id="mod_aperenviar2" class="btn  btn-lg btn-primary"><i class="fa fa-save"></i>
								ENVIAR
							</button>
						</div>
					</div>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>
		<!-- ========================================================================================================= -->
		<!-- PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->
		<!--================================================== -->
		<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
		<script data-pace-options='{ "restartOnRequestAfter": true }' src="<?php echo base_url(); ?>assets/js/plugin/pace/pace.min.js"></script>
		<script>
			if (!window.jQuery) {
				document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-2.0.2.min.js"><\/script>');
			}
		</script>

		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>

		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url(); ?>assets/js/session_time/jquery-idletimer.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/app.config.js"></script>
		<script src = "<?php echo base_url(); ?>mis_js/control_session.js"></script>
		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.min.js"></script>
		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url(); ?>assets/js/notification/SmartNotification.min.js"></script>
		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url(); ?>assets/js/smartwidgets/jarvis.widget.min.js"></script>
		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<!-- SPARKLINES -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/select2/select2.min.js"></script>
		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
		<!-- browser msie issue fix -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/fastclick/fastclick.min.js"></script>
		<!-- Demo purpose only -->
		<script src="<?php echo base_url(); ?>assets/js/demo.min.js"></script>
		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url(); ?>assets/js/app.min.js"></script>
		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<!-- Voice command : plugin -->
		<script src="<?php echo base_url(); ?>assets/js/speech/voicecommand.min.js"></script>
		<!-- PAGE RELATED PLUGIN(S) -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.colVis.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.tableTools.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>

		<!-- ======================================= PROYECTO OBSERVADO ======================================= -->
		<script type="text/javascript">
			$(function(){
				//limpiar variable
				var id_aper ='';
				$(".mod_aper2").on("click",function(e){
					tp = $(this).attr('id');
					id_p = $(this).attr('name');
					document.getElementById("id").value=id_p; 
					document.getElementById("tpo").value=tp;
					var url = "<?php echo site_url("admin")?>/proy/get_resp";
					var codigo ='';
					var request;
					if(request){
						request.abort();
					}
					request = $.ajax({
						url:url,
						type:"POST",
						dataType:'json',
						data: "id_p="+id_p+"&tp="+tp
					});

					request.done(function(response,textStatus,jqXHR){
						$('#responsable_poa').html(response.responsable);
						//document.mod_formaper.modunidad_o.selectedIndex=response.uni_id;
					});
					request.fail(function(jqXHR,textStatus,thrown){
						console.log("ERROR: "+ textStatus);
					});
					request.always(function(){
						//console.log("termino la ejecuicion de ajax");
					});
					e.preventDefault();
					// =============================VALIDAR EL FORMULARIO DE MODIFICACION
					$("#mod_aperenviar2").on("click",function(e){
						var $validator = $("#mod_formaper2").validate({
							rules: {
								
								observacion: {
									required: true,
								}
							},

							messages: {
								observacion: {required:"Describa la Observacion"},
							},

							highlight: function (element) {
								$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
							},
							unhighlight: function (element) {
								$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
							},
							errorElement: 'span',
							errorClass: 'help-block',
							errorPlacement: function (error, element) {
								if (element.parent('.input-group').length) {
									error.insertAfter(element.parent());
								} else {
									error.insertAfter(element);
								}
							}
						});
						var $valid = $("#mod_formaper2").valid();
						if (!$valid) {
							$validator.focusInvalid();
						} else {
							//==========================================================
							//var aper_programa = document.getElementById("modaper_programa").value

							var id_p = document.getElementById("id").value
							var tp = document.getElementById("tpo").value
							var observacion = document.getElementById("observacion").value
							var url = "<?php echo site_url("admin")?>/proy/add_obs";
							$.ajax({
								type:"post",
								url:url,
								data:{id:id_p,observacion:observacion,tp:tp},
								success:function(data){
									window.location.reload(true);
								}
							});
						}
					});
				});
			});
		</script>
		<!-- ====================================================================================================== -->
		<script type="text/javascript">
		// DO NOT REMOVE : GLOBAL FUNCTIONS!
		$(document).ready(function() {
			pageSetUp();
			/* BASIC ;*/
				var responsiveHelper_dt_basic = undefined;
				var responsiveHelper_datatable_fixed_column = undefined;
				var responsiveHelper_datatable_col_reorder = undefined;
				var responsiveHelper_datatable_tabletools = undefined;
				var breakpointDefinition = {
					tablet : 1024,
					phone : 480
				};
	
				$('#dt_basic').dataTable({
					"ordering": false,
					"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
					"autoWidth" : true,
					"preDrawCallback" : function() {
						// Initialize the responsive datatables helper once.
						if (!responsiveHelper_dt_basic) {
							responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
						}
					},
					"rowCallback" : function(nRow) {
						responsiveHelper_dt_basic.createExpandIcon(nRow);
					},
					"drawCallback" : function(oSettings) {
						responsiveHelper_dt_basic.respond();
					}
				});
			})
		</script>
	</body>
</html>
