<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
		<title><?php echo $this->session->userdata('name')?></title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.core.css"/>
    	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css" id="toggleCSS"/>
		<script>
	    function confirmar()
	    {
	      if(confirm('¿Estas seguro de Eliminar?'))
	        return true;
	      else
	        return false;
	    }
	    </script>
		<style type="text/css">
			aside{background: #05678B;}
		</style>
		<style>
            th{
            padding: 1.4px;
            text-align: center;
            font-size: 9px;
            }
            td{
            padding: 1.4px;
            text-align: center;
/*            font-size: 9px;*/
            }
		</style>
	</head>
	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->
		<!-- HEADER -->
<header id="header">
	<div id="logo-group" class="col-md-2">
		<span id="logo"> <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="<?php echo $this->session->userdata('name')?>"> </span>
	</div>
	<div class="col-md-6 no-print" style="font-size:15px; margin-top:15px; margin-bottom:-10px;">
		<div class="text-center">
			<span>
				<div class="badge bg-color-blue" style="padding:7px;">
					<span style="font-size:1.6em;">Gesti&oacute;n de trabajo: <?php echo $this->session->userdata('desc_mes').'/'.$this->session->userdata('gestion');?></span>
				</div>
			</span>
			<div class="project-context hidden-xs">
				<span class="project-selector" style="font-size:0.7em;">
					<i class="fa fa-lg fa-fw fa-calendar txt-color-blue"></i>
					<a href="<?php echo base_url();?>index.php/cambiar_gestion">Cambiar Gestión</a>
				</span>
	<!--			<ul class="dropdown-menu">
					<li>
						<a href="<?php echo base_url();?>index.php/cambiar_gestion">Cambiar Gestión</a>
					</li>
				</ul>-->
			</div>
		</div>
	</div>
	<!-- pulled right: nav area -->
	<div class="pull-right col-md-2 no-print">
		<!-- logout button -->
		<div id="logout" class="btn-header transparent pull-right">
            <span> 
				<a href="<?php echo base_url(); ?>index.php/admin/logout" title="Salir" data-action="userLogout" data-logout-msg="Seguro de Salir?">
					Salir <i class="fa fa-sign-out"></i>
				</a> 
			</span>
		</div>
		<!-- end logout button -->

		<!-- fullscreen button -->
		<div id="fullscreen" class="btn-header transparent pull-right">
            <span> 
				<a href="javascript:void(0);" data-action="launchFullscreen" title="Pantalla Completa">
					<i class="fa fa-arrows-alt"></i>
				</a> 
			</span>
		</div>
		<!-- end fullscreen button -->
		<!-- collapse menu button -->
		<div id="hide-menu" class="btn-header pull-right">
            <span> 
				<a href="javascript:void(0);" data-action="toggleMenu" title="Menu">
					<i class="fa fa-reorder"></i>
				</a>
			</span>
		</div>
		<!-- end collapse menu -->

	</div>
	<!-- end pulled right: nav area -->

</header>
		<!-- END HEADER -->
		<!-- Left panel : Navigation area -->
		<aside id="left-panel">
			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                        <span>
                            <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                        </span>
					</a> 
				</span>
			</div>

			<nav>
				<ul>
					<li>
						<a href='<?php echo site_url("admin").'/dashboard'; ?>' title="MENU PRINCIPAL"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
					</li>
					<li class="text-center">
		                <a href="<?php echo base_url().'index.php/admin/proy/mis_proyectos/'.$mod ?>" title="PROGRAMACION -> MIS PROYECTOS"> <span class="menu-item-parent">PROGRAMACI&Oacute;N F&Iacute;SICA</span></a>
		            </li>
					<?php
					if($nro_fase==1){
					    for($i=0;$i<count($enlaces);$i++)
					    {
					    	?>
					    	 <li>
				              	<a href="#" >
				              		<i class="<?php echo $enlaces[$i]['o_image'];?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
				              	<ul >
				              	<?php
				                $submenu= $this->menu_modelo->get_Modulos_sub($enlaces[$i]['o_child']);
					      		foreach($submenu as $row) {
				                ?>
				                <li><a href="<?php echo base_url($row['o_url'])."/".$mod."/".$id_f[0]['id']."/".$id_f[0]['proy_id']; ?>"><?php echo $row['o_titulo']; ?></a></li>
				                <?php } ?>
				                </ul>
				            </li>
					    	<?php
					    }
					}
					?>
				</ul>
			</nav>
			<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>
		</aside>

		<!-- MAIN PANEL -->
		<div id="main" role="main">
			<!-- RIBBON -->
			<div id="ribbon">
				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<?php 
                      if($proyecto[0]['tp_id']==1){
                      	if($mod==1){ ?>
                           <li><a href="<?php echo base_url().'index.php/admin/proy/list_proy' ?>" title="VOLVER A MIS PROYECTOS">Mis Operaciones</a></li><li><a href="<?php echo base_url().'index.php/admin/prog/prog_fisica/'.$mod.'/'.$id_f[0]['id']."/".$id_f[0]['proy_id'] ?>" title="PROGRAMACION FISICA">Programaci&oacute;n Fisica</a></li><li><a href="#" >Mis Componentes</a></li>
                           <?php
                      	}
                      	elseif ($mod==4){ ?>
                           <li><a href="<?php echo base_url().'index.php/admin/sgp/list_proy' ?>" title="VOLVER A MIS PROYECTOS">Gerencia de Proyectos</a></li><li><a href="<?php echo base_url().'index.php/admin/prog/prog_fisica/'.$mod.'/'.$id_f[0]['id']."/".$id_f[0]['proy_id'] ?>" title="PROGRAMACION FISICA">Programaci&oacute;n Fisica</a></li><li><a href="#" >Mis Componentes</a></li>
                           <?php
                      	}
                           
                      }
                      elseif ($proyecto[0]['tp_id']==2 || $proyecto[0]['tp_id']==3){
                      	if($mod==1){ ?>
                           <li><a href="<?php echo base_url().'index.php/admin/proy/list_proy' ?>" title="VOLVER A MIS PROYECTOS">Mis Operaciones</a></li><li><a href="#" >Mis Componentes</a></li>
                           <?php
                      	}
                      	elseif ($mod==4){ ?>
                           <li><a href="<?php echo base_url().'index.php/admin/sgp/list_proy' ?>" title="VOLVER A MIS PROYECTOS">Gerencia de Proyectos</a></li><li><a href="#" >Mis Componentes</a></li>
                           <?php
                      	}
                           
                      }
                 	?>
				</ol>
			</div>
			<!-- END RIBBON -->

			<!-- MAIN CONTENT -->
			<div id="content">

                      <section id="widget-grid" class="well">
                          <div class="">
                            <h1> <?php echo $titulo_proy;?> : <small><?php echo $proyecto[0]['proy_nombre']?></small></h1>
                            <h1> CATEGORIA PROGRAM&Aacute;TICA : <small><?php echo $proyecto[0]['aper_programa'].''.$proyecto[0]['aper_proyecto'].''.$proyecto[0]['aper_actividad']?></small>
                          </div>
                      </section>
	
					<section id="widget-grid" class="">
						<div class="row">
							<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<?php 
				                  if($this->session->flashdata('success'))
				                    { ?>
				                    <div class="alert alert-success">
				                      <?php echo $this->session->flashdata('success'); ?>
				                    </div>
				                <?php 
				                    }
				                  elseif($this->session->flashdata('danger'))
				                  {
				                    {
				                      ?>
				                      <div class="alert alert-danger">
				                        <?php echo $this->session->flashdata('danger'); ?>
				                      </div>
				                      <?php
				                    }
				                  }
				                ?>
								<!-- Widget ID (each widget will need unique ID)-->
								<div class="jarviswidget jarviswidget-color-darken" >
									<header>
										<span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
										<?php
										if($mod==1){ ?>
											<h2 class="font-md"><strong><?php echo $titulo_com;?> <?php echo $this->session->userdata("gestion");?></strong></h2>				
											<?php
										}
										elseif ($mod==4){ ?>
											<h2 class="font-md"><strong><?php echo $titulo_com;?> <?php echo $id_f[0]['pfec_fecha_inicio'].' - '.$id_f[0]['pfec_fecha_fin'];?></strong></h2>				
											<?php
										}
										?>
									</header>
									<div>
										<div class="widget-body no-padding">
											<div class="table-responsive">
												<table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
													<thead>
													<tr>
														<th style="width:1%;"><center><a href="#" data-toggle="modal" data-target="#modal_nuevo_ff" class="btn btn-xs nuevo_ff" title="NUEVO DE REGISTRO <?php echo $titulo_com;?>"><img src="<?php echo base_url(); ?>assets/ifinal/2.png" WIDTH="35" HEIGHT="35"/><BR><font size="1">NUEVO <?php echo $titulo_com;?></font></a></center></th>
														<th style="width:2%;"></th>
														<th style="width:2%;"></th>
														<th style="width:20%;">DESCRIPCI&Oacute;N <?php echo $titulo_com;?></th>
														<th style="width:20%;">T&Eacute;CNICO RESPONSABLE</th>
														<th style="width:20%;">UNIDAD EJECUTORA RESPONABLE</th>
														<th style="width:5%;">% PONDERACI&Oacute;N</th>
														<th style="width:5%;">REGISTRO DE PRODUCTOS</th>
													</tr>
													</thead>
													<tbody id="bdi">
													<?php echo $componentes;?>
													</tbody>
												</table>
											</div>
										</div>
										<!-- end widget content -->
									</div>
									<!-- end widget div -->
								</div>
								<!-- end widget -->
							</article>
							</form>
						<!-- WIDGET END -->
						</div>
				</section>

			</div>
			<!-- END MAIN CONTENT -->

		</div>
		<!-- END MAIN PANEL -->

    <!-- ============================================ Modal NUEVO COMPONENTE  =============================================== -->
    <div class="modal animated fadeInDown" id="modal_nuevo_ff" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close text-danger " data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">
                              <img src="<?php echo base_url(); ?>assets/img/logo.png" width="150" alt="SmartAdmin">
                    </h4>
                    <h4 class="modal-title text-center text-info">
                        <b>NUEVO REGISTRO (<?php echo $titulo_com;?>)</b>
                    </h4>
                </div>
                <div class="modal-body no-padding">
                    <div class="row">
                        <form id="form_ff" novalidate="novalidate" method="post">
                        <input type="hidden" name="id_f" id="id_f" value="<?php echo $id_f[0]['id'];?>">
                        <input type="hidden" name="gestiones" id="gestiones" value="<?php echo $gest;?>">
                        <input type="hidden" name="fecha" id="fecha" value="<?php echo $fecha;?>">
                        <input type="hidden" name="vcomp" id="vcomp" value="<?php echo $proyecto[0]['comp'];?>">
                        
                            <div id="bootstrap-wizard-1" class="col-sm-12">
                                <div class="well">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label><font size="1"><b>NOMBRE DEL COMPONENTE</b></font></label>
                                                <textarea rows="6" class="form-control" name="comp" id="comp" style="width:100%;" title="<?php echo $titulo_com;?> DE LA OPERACION" maxlength="100"></textarea> 
                                            </div>
                                            <div class="form-group">
                                                <label><font size="1"><b>MODALIDAD DE EJECUCIÓN</b></font></label>
												<select class="form-control" name="modalidad" id="modalidad">
													<option value="No seleccionado">No seleccionado</option>
													<option value="Administración propia">Administración propia</option>
													<option value="Contratación de terceros">Contratación de terceros</option>
												</select>
                                            </div>
                                        </div>
                                        <?php
                                        if($proyecto[0]['comp']==1){
                                        	?>
                                        	<div class="col-sm-12">
												<div class="form-group">
													<label><b><font size="1">T&Eacute;CNICO RESPONSABLE </font></b></label>
														<select class="select2" name="resp" id="resp">
														<?php 
										                    foreach($fun1 as $row)
										                    {
										                    	if($row['fun_id']==$responsable[0]['fun_id'])
										                    	{
										                    		?>
												                     <option value="<?php echo $row['fun_id']?>" selected="true"><?php echo $row['fun_nombre'].' '.$row['fun_paterno'].' '.$row['fun_materno']; ?></option>
												                    <?php 
										                    	}
										                    	else
										                    	{
										                    		?>
												                    <option value="<?php echo $row['fun_id']?>"><?php echo $row['fun_nombre'].' '.$row['fun_paterno'].' '.$row['fun_materno']; ?></option>
												                    <?php 
										                    	}
										                    }
										                ?>    
		                                                 </select>
												</div>
											</div>
											<div class="col-sm-12">
												<div class="form-group">
													<label><b><font size="1">UNIDAD EJECUTORA</font></b></label>
		                                                <select class="select2" name="uni_ejec" id="uni_ejec">
		                                                <?php 
										                    foreach($unidades as $row)
										                    {
										                    	if($row['uni_id']==$responsable[0]['uni_ejec'])
										                    	{
										                    		?>
												                     <option value="<?php echo $row['uni_id']?>" selected <?php if(@$_POST['pais']==$row['uni_id']){ echo "selected";} ?> ><?php echo $row['uni_unidad']; ?></option>
												                    <?php 
										                    	}
										                    	else
										                    	{
										                    		?>
												                     <option value="<?php echo $row['uni_id']?>" <?php if(@$_POST['pais']==$row['uni_id']){ echo "selected";} ?> ><?php echo $row['uni_unidad']; ?></option>
												                    <?php 
										                    	}
										                    }
										                ?>
		                                                </select>
												</div>
											</div>
                                        	<?php
                                        }
                                        ?>
                                        
                                    </div>
                                </div> <!-- end well -->
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-3 pull-left">
                            <button class="btn btn-ms btn-danger" data-dismiss="modal">CANCELAR</button>
                        </div>
                        <div class="col-md-3 pull-right ">
                            <button type="submit" name="enviar_ff" id="enviar_ff" class="btn  btn-ms btn-primary"><i class="fa fa-save"></i>
                                GUARDAR
                            </button>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <!-- =========================================================================================================================================== -->
	 <!-- ================== Modal  MODIFICAR  COMPONENTE========================== -->
    <div class="modal animated fadeInDown" id="modal_mod_ff" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close text-danger " data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">
                        <img src="<?php echo base_url(); ?>assets/img/logo.png" width="150" alt="SmartAdmin">
                    </h4>
                    <h4 class="modal-title text-center text-info">
                        <b><i class="glyphicon glyphicon-pencil"></i> MODIFICAR REGISTRO (<?php echo $titulo_com;?>)</b>
                    </h4>
                </div>
                <div class="modal-body no-padding">
                    <div class="row">
                        <form id="mod_formff" novalidate="novalidate" method="post">
                            <div id="bootstrap-wizard-1" class="col-sm-12">
                                <div class="well">
                                    <input class="form-control" type="hidden" name="com_id" id="com_id" >
                                    <input type="hidden" name="vcomp" id="vcomp" value="<?php echo $proyecto[0]['comp'];?>">
                                    <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label><font size="1"><b>DESCRIPCI&Oacute;N <?php echo $titulo_com;?></b></font></label>
                                            <textarea rows="6" class="form-control" name="componente" id="componente" style="width:100%;" title="COMPONENTE DE LA OPERACION" maxlength="100"></textarea> 
                                        </div>
                                            <div class="form-group">
                                                <label><font size="1"><b>MODALIDAD DE EJECUCIÓN</b></font></label>
												<select class="form-control" name="mod_modalidad" id="mod_modalidad">
													<option value="No seleccionado">No seleccionado</option>
													<option value="Administración propia">Administración propia</option>
													<option value="Contratación de terceros">Contratación de terceros</option>
												</select>
                                            </div>
                                    </div>
                                    <?php 
                                    if($proyecto[0]['comp']==1){
                                    	?>
                                    	<div class="col-sm-12">
											<div class="form-group">
												<label><b><font size="1">T&Eacute;CNICO RESPONSABLE </font></b></label>
													<select class="form-control" name="resp_id" id="resp_id">
													<?php 
									                    foreach($fun1 as $row)
									                    {
									                    	?>
									                    	<option value="<?php echo $row['fun_id']?>"><?php echo $row['fun_nombre'].' '.$row['fun_paterno'].' '.$row['fun_materno']; ?></option>
									                    	<?php
									                    }
									                ?>    
	                                                 </select>
											</div>
										</div>
										<div class="col-sm-12">
											<div class="form-group">
												<label><b><font size="1">UNIDAD EJECUTORA</font></b></label>
	                                                <select class="form-control" name="uni_id" id="uni_id">
	                                                <?php 
									                    foreach($unidades as $row)
									                    {
									                    	?>
												            <option value="<?php echo $row['uni_id']?>" <?php if(@$_POST['pais']==$row['uni_id']){ echo "selected";} ?> ><?php echo $row['uni_unidad']; ?></option>
												            <?php 
									                    }
									                ?>
	                                                </select>
											</div>
										</div>
                                    	<?php
                                    }
                                    ?>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <LABEL><b>PONDERACI&Oacute;N</b></label>
                                                <input class="form-control" type="text" name="ponderacion" id="ponderacion"  onkeypress="if (this.value.length < 8) { return soloNumeros(event);}else{return false; }" disabled="true">
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- end well -->
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-3 pull-left">
                            <button class="btn btn-sm btn-danger" data-dismiss="modal">CANCELAR</button>
                        </div>
                        <div class="col-md-3 pull-right ">
                            <button type="submit" name="mod_ffenviar" id="mod_ffenviar" class="btn  btn-sm btn-primary">
                                <i class="fa fa-save"></i>
                                GUARDAR
                            </button>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>	<!-- PAGE FOOTER -->
<!-- =========================================================================================================================================== -->

		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->

		<!--================================================== -->
		<script>
			if (!window.jQuery) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-2.0.2.min.js"><\/script>');
			}
		</script>

		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>
		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url(); ?>assets/js/session_time/jquery-idletimer.js"></script>
		<script src = "<?php echo base_url(); ?>mis_js/control_session.js"></script>
		<script src="<?php echo base_url();?>/assets/js/app.config.js"></script>
		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url();?>/assets/js/bootstrap/bootstrap.min.js"></script>
		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url();?>/assets/js/notification/SmartNotification.min.js"></script>
		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url();?>/assets/js/smartwidgets/jarvis.widget.min.js"></script>
		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url();?>/assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<!-- SPARKLINES -->
		<script src="<?php echo base_url();?>/assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/select2/select2.min.js"></script>
		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
		<!-- browser msie issue fix -->
		<script src="<?php echo base_url();?>/assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url();?>/assets/js/plugin/fastclick/fastclick.min.js"></script>
		<!-- Demo purpose only -->
		<script src="<?php echo base_url();?>/assets/js/demo.min.js"></script>
		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url();?>/assets/js/app.min.js"></script>
		<SCRIPT src="<?php echo base_url(); ?>assets/js/mis_js/validacion_form.js" type="text/javascript"></SCRIPT>
		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<!-- Voice command : plugin -->
		<script src="<?php echo base_url(); ?>assets/js/speech/voicecommand.min.js"></script>
		<!--alertas -->
		<script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
		<!-- PAGE RELATED PLUGIN(S) -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.colVis.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.tableTools.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
		<!--================= NUEVO NUEVO COMPONENTE =========================================-->
		<script type="text/javascript">
		    $(function () {
		        var id_p = '';
		        $('#modal_nuevo_ff').on('hidden.bs.modal', function () {
		            document.forms['form_ff'].reset();
		        });
		        $("#enviar_ff").on("click", function (e) {

		            //========================VALIDANDO FORMULARIO===================
		            var $validator = $("#form_ff").validate({
		                //////////////// DATOS GENERALES
		                rules: {
		                	id_f: { //// id componente
		                        required: true,
		                    },
		                    comp: { //// componente
		                        required: true,
		                    },
		                    modalidad: { //// componente
		                        required: true,
		                    }
		                },
		                messages: {
		                    id_f: "id de la fase etapa",
		                    comp: "Describa Componente",		                    
		                    modalidad: "Modalidad ejecucion componente",		                    
		                },
		                highlight: function (element) {
		                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		                },
		                unhighlight: function (element) {
		                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		                },
		                errorElement: 'span',
		                errorClass: 'help-block',
		                errorPlacement: function (error, element) {
		                    if (element.parent('.input-group').length) {
		                        error.insertAfter(element.parent());
		                    } else {
		                        error.insertAfter(element);
		                    }
		                }
		            });
		            var $valid = $("#form_ff").valid();
		            if (!$valid) {
		                $validator.focusInvalid();
		                //return false;
		            } else {
		                //==========================================================
		                var id_f = document.getElementById("id_f").value;
		                var gestiones = document.getElementById("gestiones").value;
		                var fecha = document.getElementById("fecha").value;
		                var comp = document.getElementById("comp").value;
		                var mod = document.getElementById("modalidad").value;
		               //	alert(document.getElementById("comp").value)
		               	if(document.getElementById("vcomp").value==1)
		               	{
		               		var resp_id = document.getElementById("resp").value;
		                	var uni_id = document.getElementById("uni_ejec").value;
		               	}
		               	else{
		               		var resp_id = 0;
		                	var uni_id = 0;
		               	}
		                var url = "<?php echo site_url("admin")?>/prog/add_comp";
		                            $.ajax({
		                                type: "post",
		                                url: url,
		                                data: {
		                                    id_f: id_f,
		                                    comp: comp,
		                                    modalidad: mod,
		                                    fecha: fecha,
		                                    gestiones: gestiones,
		                                    resp_id: resp_id,
		                                    uni_id: uni_id
		                                },
		                                success: function (data) { 
		                                    window.location.reload(true);
		                                }
		                            });
		            }
		        });
		    });
		</script>
		<!--=====================================================================================================-->
		<!--================= MODIFICAR COMPONENTE  =========================================-->
			<script type="text/javascript">
			    $(function () {
			        var id_c = '';var ponderacion = '';
			        $("#dt_basic").on("click", ".mod_ff", function (e) {
			            //==========================LLENAR MIS DATOS DE FORMULARIO CON LA CLAVE RECIBIDA
			            id_c = $(this).attr('name'); 
			            ponderacion = $(this).attr('id'); 
			            var url = "<?php echo site_url("admin")?>/prog/get_comp";
			            var codigo = '';
			            var request;
			            if (request) {
			                request.abort();
			            }
			            request = $.ajax({
			                url: url,
			                type: "POST",
			                dataType: 'json',
			                data: "id_c=" + id_c
			            });

			            request.done(function (response, textStatus, jqXHR) {

			                document.getElementById("com_id").value = id_c;
			                document.getElementById("ponderacion").value = ponderacion;
			                document.getElementById("componente").value = response.com_componente;
							$.each($('#mod_modalidad').children('option'), function(){
								if($(this).val() == response.modalidad){
									console.log("Valor encontrado: "+$(this).val());
									$(this).attr('selected','true');
								}
							});
			                document.getElementById("uni_id").value = response.uni_id;
			                document.getElementById("resp_id").value = response.resp_id;
			            });
			            request.fail(function (jqXHR, textStatus, thrown) {
			                console.log("ERROR: " + textStatus);
			            });
			            request.always(function () {
			                //console.log("termino la ejecuicion de ajax");
			            });
			            e.preventDefault();
			            // =============================VALIDAR EL FORMULARIO DE MODIFICACION
			            $("#mod_ffenviar").on("click", function (e) {
			                var $validator = $("#mod_formff").validate({
			                   rules: {
				                	com_id: { //// id componente
				                        required: true,
				                    },
				                    componente: { //// componente
				                        required: true,
				                    },
				                    modalidad: { //// modalida de ejecucion
				                        required: true,
				                    }
				                },
				                messages: {
				                    com_id: "id del componente",
				                    componente: "Describa Componente",		                    
				                    modalidad: "Describa Componente",		                    
				                },
			                    highlight: function (element) {
			                        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
			                    },
			                    unhighlight: function (element) {
			                        $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
			                    },
			                    errorElement: 'span',
			                    errorClass: 'help-block',
			                    errorPlacement: function (error, element) {
			                        if (element.parent('.input-group').length) {
			                            error.insertAfter(element.parent());
			                        } else {
			                            error.insertAfter(element);
			                        }
			                    }
			                });
			                var $valid = $("#mod_formff").valid();
			                if (!$valid) {
			                    $validator.focusInvalid();
			                } else {
			                    //==========================================================
			                    var com_id = document.getElementById("com_id").value;
			                    var ponderacion = document.getElementById("ponderacion").value;
			                    var componente = document.getElementById("componente").value;
			                    var mod = document.getElementById("mod_modalidad").value;
			                    if(document.getElementById("vcomp").value==1)
				               	{
				               		var resp_id = document.getElementById("resp_id").value;
				                	var uni_id = document.getElementById("uni_id").value;
				               	}
				               	else{
				               		var resp_id = 0;
				                	var uni_id = 0;
				               	}

			                    var url = "<?php echo site_url("admin")?>/prog/update_comp";
			                    $.ajax({
			                        type: "post",
			                        url: url,
			                        data: {
			                            com_id: com_id,
			                            ponderacion: ponderacion,
			                            resp_id: resp_id,
			                            uni_id: uni_id,
			                            componente: componente,
			                            modalidad: mod
			                        },
			                        success: function (data) {
			                            window.location.reload(true);
			                        }
			                    });
			                }
			            });
			        });
			    });
			</script>
	<!-- =============================================================================================== -->
		<script type="text/javascript">
          // DO NOT REMOVE : GLOBAL FUNCTIONS!
          $(document).ready(function() {
               
               pageSetUp();
     
               /* BASIC ;*/
                    var responsiveHelper_dt_basic = undefined;
                    var responsiveHelper_datatable_fixed_column = undefined;
                    var responsiveHelper_datatable_col_reorder = undefined;
                    var responsiveHelper_datatable_tabletools = undefined;
                    
                    var breakpointDefinition = {
                         tablet : 1024,
                         phone : 480
                    };
     
                    $('#dt_basic').dataTable({
                         "ordering": false,
                         "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
                              "t"+
                              "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                         "autoWidth" : true,
                         "preDrawCallback" : function() {
                              // Initialize the responsive datatables helper once.
                              if (!responsiveHelper_dt_basic) {
                                   responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                              }
                         },
                         "rowCallback" : function(nRow) {
                              responsiveHelper_dt_basic.createExpandIcon(nRow);
                         },
                         "drawCallback" : function(oSettings) {
                              responsiveHelper_dt_basic.respond();
                         }
                    });

          })
          </script>
	</body>
</html>
