<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
		<title><?php echo $this->session->userdata('name')?></title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.core.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css" id="toggleCSS" />
		<script>
	    function confirmar()
	    {
	      if(confirm('¿Estas seguro de Eliminar?'))
	        return true;
	      else
	        return false;
	    }
	    </script>
		<style type="text/css">
			aside{background: #05678B;}
		</style>
	</head>
	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->
		<!-- HEADER -->
		<header id="header">
			<!-- pulled right: nav area -->
			<div class="pull-right">
				<!-- collapse menu button -->
				<div id="hide-menu" class="btn-header pull-right">
					<span> <a href="javascript:void(0);" data-action="toggleMenu" title="Menu"><i class="fa fa-reorder"></i></a> </span>
				</div>
				<!-- end collapse menu -->
				<!-- logout button -->
				<div id="logout" class="btn-header transparent pull-right">
					<span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Salir" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
				</div>
				<!-- end logout button -->
				<!-- search mobile button (this is hidden till mobile view port) -->
				<div id="search-mobile" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
				</div>
				<!-- end search mobile button -->
				<!-- fullscreen button -->
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Pantalla Completa"><i class="fa fa-arrows-alt"></i></a> </span>
				</div>
				<!-- end fullscreen button -->
			</div>
			<!-- end pulled right: nav area -->
		</header>
		<!-- END HEADER -->
		<!-- Left panel : Navigation area -->
		<aside id="left-panel">
			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                        <span>
                            <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                        </span>
					</a> 
				</span>
			</div>

			<nav>
				<ul>
					<li>
						<a href='<?php echo site_url("admin").'/dashboard'; ?>' title="MENU PRINCIPAL"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
					</li>
					<li class="text-center">
		                <a href="<?php echo base_url().'index.php/admin/proy/mis_proyectos/'.$mod ?>" title="PROGRAMACION -> MIS PROYECTOS"> <span class="menu-item-parent">PROGRAMACI&Oacute;N F&Iacute;SICA</span></a>
		            </li>
					<?php
					if($nro_fase==1){
					    for($i=0;$i<count($enlaces);$i++)
					    {
					    	?>
					    	 <li>
				              	<a href="#" >
				              		<i class="<?php echo $enlaces[$i]['o_image'];?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
				              	<ul >
				              	<?php
				                $submenu= $this->menu_modelo->get_Modulos_sub($enlaces[$i]['o_child']);
					      		foreach($submenu as $row) {
				                ?>
				                <li><a href="<?php echo base_url($row['o_url'])."/".$mod."/".$id_f[0]['id']."/".$id_f[0]['proy_id']; ?>"><?php echo $row['o_titulo']; ?></a></li>
				                <?php } ?>
				                </ul>
				            </li>
					    	<?php
					    }
					}
					?>
				</ul>
			</nav>
			<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>
		</aside>

		<!-- MAIN PANEL -->
		<div id="main" role="main">
			<!-- RIBBON -->
			<div id="ribbon">
				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<?php 
                          if($proyecto[0]['tp_id']==1)
                          {
                          	if($mod==1){ ?>
                               <li><a href="<?php echo base_url().'index.php/admin/proy/list_proy' ?>" title="VOLVER A MIS PROYECTOS">Mis Operaciones</a></li><li><a href="<?php echo base_url().'index.php/admin/prog/prog_fisica/'.$mod.'/'.$id_f[0]['id']."/".$id_f[0]['proy_id'] ?>" title="PROGRAMACION FISICA">Programaci&oacute;n Fisica</a></li><li><a href="#" >Mis Componentes</a></li>
                               <?php
                          	}
                          	elseif ($mod==4){ ?>
                               <li><a href="<?php echo base_url().'index.php/admin/sgp/list_proy' ?>" title="VOLVER A MIS PROYECTOS">Gerencia de Proyectos</a></li><li><a href="<?php echo base_url().'index.php/admin/prog/prog_fisica/'.$mod.'/'.$id_f[0]['id']."/".$id_f[0]['proy_id'] ?>" title="PROGRAMACION FISICA">Programaci&oacute;n Fisica</a></li><li><a href="#" >Mis Componentes</a></li>
                               <?php
                          	}   
                          }
                          elseif ($proyecto[0]['tp_id']==2 || $proyecto[0]['tp_id']==3){
                          	if($mod==1){ ?>
                               <li><a href="<?php echo base_url().'index.php/admin/proy/list_proy' ?>" title="VOLVER A MIS PROYECTOS">Mis Operaciones</a></li><li><a href="#" >Mis Componentes</a></li>
                               <?php
                          	}
                          	elseif ($mod==4){ ?>
                               <li><a href="<?php echo base_url().'index.php/admin/sgp/list_proy' ?>" title="VOLVER A MIS PROYECTOS">Gerencia de Proyectos</a></li><li><a href="#" >Mis Componentes</a></li>
                               <?php
                          	}
                               
                          }
                     ?>
				</ol>
			</div>
			<!-- END RIBBON -->
			<!-- MAIN CONTENT -->
			<div id="content">

                      <section id="widget-grid" class="well">
                          <div class="">
                            <h1> <?php echo $titulo_proy;?> : <small><?php echo $proyecto[0]['proy_nombre']?></small></h1>
                            <h1> CATEGORIA PROGRAM&Aacute;TICA : <small><?php echo $proyecto[0]['aper_programa'].''.$proyecto[0]['aper_proyecto'].''.$proyecto[0]['aper_actividad']?></small>
                          </div>
                      </section>
	
					<section id="widget-grid" class="">
						<div class="row">
							<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<?php 
				                  if($this->session->flashdata('success'))
				                    { ?>
				                    <div class="alert alert-success">
				                      <?php echo $this->session->flashdata('success'); ?>
				                    </div>
				                <?php 
				                    }
				                  elseif($this->session->flashdata('danger'))
				                  {
				                    {
				                      ?>
				                      <div class="alert alert-danger">
				                        <?php echo $this->session->flashdata('danger'); ?>
				                      </div>
				                      <?php
				                    }
				                  }
				                ?>
								<!-- Widget ID (each widget will need unique ID)-->
								<div class="jarviswidget jarviswidget-color-darken" >
									<header>
											<span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
											<?php
											if($mod==1){ ?>
												<h2 class="font-md"><strong>COMPONENTES <?php echo $this->session->userdata("gestion");?></strong></h2>				
												<?php
											}
											elseif ($mod==4){ ?>
												<h2 class="font-md"><strong>COMPONENTES <?php echo $id_f[0]['pfec_fecha_inicio'].' - '.$id_f[0]['pfec_fecha_fin'];?></strong></h2>				
												<?php
											}
											?>
									</header>
									<div>
										<div class="widget-body no-padding">
											<div class="table-responsive">
												<table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
													<thead>
													<tr>
														<th style="width:1%;"><center><a href="#" data-toggle="modal" data-target="#modal_nuevo_ff" class="btn btn-xs nuevo_ff" title="NUEVO DE REGISTRO COMPONENTES"><img src="<?php echo base_url(); ?>assets/ifinal/2.png" WIDTH="45" HEIGHT="45"/><BR> NUEVO COMPONENTES</a></center></th>
														<th style="width:2%;"></th>
														<th style="width:2%;"></th>
														<th style="width:25%;"><font size="1">DESCRIPCI&Oacute;N COMPONENTE</font></th>
														<th style="width:5%;"><font size="1">% PONDERACI&Oacute;N</font></th>
														<th style="width:5%;"><font size="1">REGISTRO DE PRODUCTOS</font></th>
													</tr>
													</thead>
													<tbody id="bdi">
													<?php $num=1; $ponderacion=0;
				                                      foreach($comp as $row)
				                                      { 
				                                       echo '<tr>';
				                                        echo '<td><font size="2"><center>'.$num.'</center></font></td>';
				                                        ?>
				                                        <td align="center"><a href="#" data-toggle="modal" data-target="#modal_mod_ff" class="btn btn-xs mod_ff" title="MODIFICAR DE REGISTRO COMPONENTES" name="<?php echo $row['com_id']; ?>" id="<?php echo $row['com_ponderacion']; ?>"><img src="<?php echo base_url(); ?>assets/ifinal/modificar.png" WIDTH="35" HEIGHT="35"/><br>MODIFICAR</a></td>
				                                        <?php 
				                                        ?>
				                                        <td>
					                                        <center>
					                                        	<!-- <a href="#" data-toggle="modal" data-target="#modal_del_ff" class="btn btn-xs del_ff" title="ELIMINAR COMPONENTE"  name="<?php echo $id_f[0]['proy_id'];?>" id="<?php echo $row['com_id'];?>"><img src="<?php echo base_url(); ?>assets/ifinal/eliminar.png" WIDTH="35" HEIGHT="35"/><br>Eliminar</a> -->
					                                        	 <a href='<?php echo site_url("admin").'/prog/delete_comp/'.$mod.'/'.$id_f[0]['id']."/".$id_f[0]['proy_id'].'/'.$row['com_id'];?>' title="ELIMINAR <?php echo $titulo_com;?>" onclick="return confirmar()"><img src="<?php echo base_url(); ?>assets/ifinal/eliminar.png" WIDTH="35" HEIGHT="35"/><br>ELIMINAR</a><br>
					                                        </center> 
				                                        </td>
				                                        <?php 
				                                        echo '<td><font size="1">'.$row['com_componente'].'</font></td>';
				                                        echo '<td><center><font size="1">'.$row['com_ponderacion'].' %</font></center></td>';
				                                       ?>
				                                        <td>
				                                           <center><a href='<?php echo site_url("admin").'/prog/list_prod/'.$mod.'/'.$id_f[0]['id'].'/'.$id_f[0]['proy_id'].'/'.$row['com_id']; ?>'  title="INGRESAR A PRODUCTOS"><img src="<?php echo base_url(); ?>assets/ifinal/archivo.png" WIDTH="45" HEIGHT="45"/><br>REGISTRO DE<br>PRODUCTOS</a></center>
				                                        </td>
				                                        <?php 
				                                       
				                                      echo '</tr>';
				                                      $num=$num+1;
				                                      $ponderacion=$ponderacion+$row['com_ponderacion'];
				                                      }
				                                     ?>
				                                     <tr>
														<th style="width:3%;"></th>
														<th style="width:2%;"></th>
														<th style="width:2%;"></th>
														<th style="width:25%;"></th>
														<th style="width:5%;"><center><?php echo $ponderacion/100; ?></center></th>
														<th style="width:5%;"></th>
													</tr>
													</tbody>
												</table>
											</div>
										</div>
										<!-- end widget content -->
									</div>
									<!-- end widget div -->
								</div>
								<!-- end widget -->
							</article>
							</form>
						<!-- WIDGET END -->
						</div>
				</section>

			</div>
			<!-- END MAIN CONTENT -->

		</div>
		<!-- END MAIN PANEL -->

    <!-- ============================================ Modal NUEVO COMPONENTE  =============================================== -->
    <div class="modal animated fadeInDown" id="modal_nuevo_ff" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close text-danger " data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">
                              <img src="<?php echo base_url(); ?>assets/img/logo.png" width="150" alt="SmartAdmin">
                    </h4>
                    <h4 class="modal-title text-center text-info">
                        <b>NUEVO REGISTRO (Componente)</b>
                    </h4>
                </div>
                <div class="modal-body no-padding">
                    <div class="row">
                        <form id="form_ff" novalidate="novalidate" method="post">
                        <input type="hidden" name="id_f" id="id_f" value="<?php echo $id_f[0]['id'];?>">
                        <input type="hidden" name="gestiones" id="gestiones" value="<?php echo $gest;?>">
                        <input type="hidden" name="fecha" id="fecha" value="<?php echo $fecha;?>">
                            <div id="bootstrap-wizard-1" class="col-sm-12">
                                <div class="well">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label><font size="1"><b>DESCRIPCI&Oacute;N COMPONENTE</b></font></label>
                                                <textarea rows="6" class="form-control" name="comp" id="comp" style="width:100%;" title="COMPONENTE DE LA OPERACION" maxlength="100"></textarea> 
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- end well -->
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-3 pull-left">
                            <button class="btn btn-ms btn-danger" data-dismiss="modal">CANCELAR</button>
                        </div>
                        <div class="col-md-3 pull-right ">
                            <button type="submit" name="enviar_ff" id="enviar_ff" class="btn  btn-ms btn-primary"><i class="fa fa-save"></i>
                                GUARDAR
                            </button>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <!-- =========================================================================================================================================== -->
	 <!-- ================== Modal  MODIFICAR  COMPONENTE========================== -->
    <div class="modal animated fadeInDown" id="modal_mod_ff" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close text-danger " data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">
                        <img src="<?php echo base_url(); ?>assets/img/logo.png" width="150" alt="SmartAdmin">
                    </h4>
                    <h4 class="modal-title text-center text-info">
                        <b><i class="glyphicon glyphicon-pencil"></i> MODIFICAR REGISTRO (Componente)</b>
                    </h4>
                </div>
                <div class="modal-body no-padding">
                    <div class="row">
                        <form id="mod_formff" novalidate="novalidate" method="post">
                            <div id="bootstrap-wizard-1" class="col-sm-12">
                                <div class="well">
                                    <input class="form-control" type="hidden" name="com_id" id="com_id" >
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label><font size="1"><b>DESCRIPCI&Oacute;N COMPONENTE</b></font></label>
                                                <textarea rows="6" class="form-control" name="componente" id="componente" style="width:100%;" title="COMPONENTE DE LA OPERACION" maxlength="100"></textarea> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <LABEL><b>PONDERACI&Oacute;N</b></label>
                                                <input class="form-control" type="text" name="ponderacion" id="ponderacion"  onkeypress="if (this.value.length < 8) { return soloNumeros(event);}else{return false; }" disabled="true">
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- end well -->
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-3 pull-left">
                            <button class="btn btn-sm btn-danger" data-dismiss="modal">CANCELAR</button>
                        </div>
                        <div class="col-md-3 pull-right ">
                            <button type="submit" name="mod_ffenviar" id="mod_ffenviar" class="btn  btn-sm btn-primary">
                                <i class="fa fa-save"></i>
                                GUARDAR
                            </button>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>	<!-- PAGE FOOTER -->
<!-- =========================================================================================================================================== -->

		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->
		<!--================================================== -->
		<script>
			if (!window.jQuery) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-2.0.2.min.js"><\/script>');
			}
		</script>

		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>
		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url(); ?>assets/js/session_time/jquery-idletimer.js"></script>
		<script src = "<?php echo base_url(); ?>mis_js/control_session.js"></script>
		<script src="<?php echo base_url();?>/assets/js/app.config.js"></script>
		<script src="<?php echo base_url(); ?>mis_js/registro_ejecucion/mis_operaciones.js"></script>
		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url();?>/assets/js/bootstrap/bootstrap.min.js"></script>
		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url();?>/assets/js/notification/SmartNotification.min.js"></script>
		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url();?>/assets/js/smartwidgets/jarvis.widget.min.js"></script>
		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url();?>/assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<!-- SPARKLINES -->
		<script src="<?php echo base_url();?>/assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/select2/select2.min.js"></script>
		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
		<!-- browser msie issue fix -->
		<script src="<?php echo base_url();?>/assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url();?>/assets/js/plugin/fastclick/fastclick.min.js"></script>
		<!-- Demo purpose only -->
		<script src="<?php echo base_url();?>/assets/js/demo.min.js"></script>
		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url();?>/assets/js/app.min.js"></script>
		<SCRIPT src="<?php echo base_url(); ?>assets/js/mis_js/validacion_form.js" type="text/javascript"></SCRIPT>
		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<script src="<?php echo base_url(); ?>assets/js/speech/voicecommand.min.js"></script>
		<!--alertas -->
		<script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
		<!-- PAGE RELATED PLUGIN(S) -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.colVis.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.tableTools.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
		<!--================= NUEVO NUEVO COMPONENTE =========================================-->
		<script type="text/javascript">
		    $(function () {
		        var id_p = '';
		        $('#modal_nuevo_ff').on('hidden.bs.modal', function () {
		            document.forms['form_ff'].reset();
		        });
		        $("#enviar_ff").on("click", function (e) {

		            //========================VALIDANDO FORMULARIO===================
		            var $validator = $("#form_ff").validate({
		                //////////////// DATOS GENERALES
		                rules: {
		                	id_f: { //// id componente
		                        required: true,
		                    },
		                    comp: { //// componente
		                        required: true,
		                    }
		                },
		                messages: {
		                    id_f: "id de la fase etapa",
		                    comp: "Describa Componente",		                    
		                },
		                highlight: function (element) {
		                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		                },
		                unhighlight: function (element) {
		                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		                },
		                errorElement: 'span',
		                errorClass: 'help-block',
		                errorPlacement: function (error, element) {
		                    if (element.parent('.input-group').length) {
		                        error.insertAfter(element.parent());
		                    } else {
		                        error.insertAfter(element);
		                    }
		                }
		            });
		            var $valid = $("#form_ff").valid();
		            if (!$valid) {
		                $validator.focusInvalid();
		                //return false;
		            } else {
		                //==========================================================
		                var id_f = document.getElementById("id_f").value;
		                var gestiones = document.getElementById("gestiones").value;
		                var fecha = document.getElementById("fecha").value;
		                var comp = document.getElementById("comp").value;
		                
		                var url = "<?php echo site_url("admin")?>/prog/add_comp";
		                            $.ajax({
		                                type: "post",
		                                url: url,
		                                data: {
		                                    id_f: id_f,
		                                    comp: comp,
		                                    fecha: fecha,
		                                    gestiones: gestiones
		                                },
		                                success: function (data) { 
		                                    window.location.reload(true);
		                                }
		                            });
		            }
		        });
		    });
		</script>
		<!--=====================================================================================================-->
		<!--================= MODIFICAR COMPONENTE  =========================================-->
			<script type="text/javascript">
			    $(function () {
			        var id_c = '';var ponderacion = '';
			        $(".mod_ff").on("click", function (e) {
			            //==========================LLENAR MIS DATOS DE FORMULARIO CON LA CLAVE RECIBIDA
			            id_c = $(this).attr('name'); 
			            ponderacion = $(this).attr('id'); 
			            var url = "<?php echo site_url("admin")?>/prog/get_comp";
			            var codigo = '';
			            var request;
			            if (request) {
			                request.abort();
			            }
			            request = $.ajax({
			                url: url,
			                type: "POST",
			                dataType: 'json',
			                data: "id_c=" + id_c
			            });

			            request.done(function (response, textStatus, jqXHR) {

			                document.getElementById("com_id").value = id_c;
			                document.getElementById("ponderacion").value = ponderacion;
			                document.getElementById("componente").value = response.com_componente;

			            });
			            request.fail(function (jqXHR, textStatus, thrown) {
			                console.log("ERROR: " + textStatus);
			            });
			            request.always(function () {
			                //console.log("termino la ejecuicion de ajax");
			            });
			            e.preventDefault();
			            // =============================VALIDAR EL FORMULARIO DE MODIFICACION
			            $("#mod_ffenviar").on("click", function (e) {
			                var $validator = $("#mod_formff").validate({
			                   rules: {
				                	com_id: { //// id componente
				                        required: true,
				                    },
				                    componente: { //// componente
				                        required: true,
				                    }
				                },
				                messages: {
				                    com_id: "id del componente",
				                    componente: "Describa Componente",		                    
				                },
			                    highlight: function (element) {
			                        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
			                    },
			                    unhighlight: function (element) {
			                        $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
			                    },
			                    errorElement: 'span',
			                    errorClass: 'help-block',
			                    errorPlacement: function (error, element) {
			                        if (element.parent('.input-group').length) {
			                            error.insertAfter(element.parent());
			                        } else {
			                            error.insertAfter(element);
			                        }
			                    }
			                });
			                var $valid = $("#mod_formff").valid();
			                if (!$valid) {
			                    $validator.focusInvalid();
			                } else {
			                    //==========================================================
			                    var com_id = document.getElementById("com_id").value;
			                    var ponderacion = document.getElementById("ponderacion").value;
			                    var componente = document.getElementById("componente").value;

			                    var url = "<?php echo site_url("admin")?>/prog/update_comp";
			                    $.ajax({
			                        type: "post",
			                        url: url,
			                        data: {
			                            com_id: com_id,
			                            ponderacion: ponderacion,
			                            componente: componente
			                        },
			                        success: function (data) {
			                            window.location.reload(true);
			                        }
			                    });
			                }
			            });
			        });
			    });
			</script>
	<!-- =============================================================================================== -->
			<script type="text/javascript">
		    $(function () {
		        function reset() {
		            $("#toggleCSS").attr("href", "<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css");
		            alertify.set({
		                labels: {
		                    ok: "ACEPTAR",
		                    cancel: "CANCELAR"
		                },
		                delay: 5000,
		                buttonReverse: false,
		                buttonFocus: "ok"
		            });
		        }

		        // =====================================================================
		        $(".del_ff").on("click", function (e) {
		            reset();
		            var name = $(this).attr('name'); // proy id
		            var id = $(this).attr('id'); // com id
		            alert(name+" "+id)
		            var request;
		            // confirm dialog
		            alertify.confirm("DESEA ELIMINAR EL COMPONENTE SELECCIONADO ?", function (a) {
		                if (a) { 
		                    url = "<?php echo site_url("admin")?>/prog/delete_comp";
		                    if (request) {
		                        request.abort();
		                    }
		                    request = $.ajax({
		                        url: url,
		                        type: "POST",
		                        dataType: "json",
                    			data: "proy_id="+name+"&com_id="+id

		                    });

		                    request.done(function (response, textStatus, jqXHR) { alert(response.respuesta)
			                    reset();
			                    if (response.respuesta == 'correcto') {
			                        alertify.alert("LA PROGRAMACIÓN FÍSICA SE CERRO CORRECTAMENTE ", function (e) {
			                            if (e) {
			                                window.location.reload(true);
			                            }
			                        });
			                    } else {
			                        alertify.alert("ERROR AL REALIZAR LA ACCION!!!", function (e) {
			                            /*if (e) {
			                                window.location.reload(true);
			                            }*/
			                        });
			                    }
			                });
		                    request.fail(function (jqXHR, textStatus, thrown) {
		                        console.log("ERROR: " + textStatus);
		                    });
		                    request.always(function () {
		                        //console.log("termino la ejecuicion de ajax");
		                    });

		                    e.preventDefault();

		                } else {
		                    // user clicked "cancel"
		                    alertify.error("Opcion cancelada");
		                }
		            });
		            return false;
		        });

		    });

		</script>
	</body>
</html>
