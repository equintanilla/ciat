<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta charset="utf-8">
    <link rel="STYLESHEET" href="<?php echo base_url(); ?>assets/print_static.css" type="text/css" />
    <title><?php echo $this->session->userData('sistema');?></title>
</head>
<style type="text/css">
        @page {size: letter;}
      </style>
<style>
#areaImprimir{background:#fff;}
.button-container {
    margin: 0 auto;
    padding: 5px; 0;
    width: 50%;
    border: thin solid gray;
    display: flex;
    justify-content: space-around;
}
</style>
  <script type="text/javascript">
    function printDiv(nombreDiv) {
     var contenido= document.getElementById(nombreDiv).innerHTML;
     var contenidoOriginal= document.body.innerHTML;
     document.body.innerHTML = contenido;
     window.print();
     document.body.innerHTML = contenidoOriginal;
}
  </script>
<body>
  <table style="width:100%;">
    <tr bgcolor="#504c49">
      <td style="width:80%;"></td>
      <td style="width:20%;" align="center" bgcolor="#504c49"><input type="button" name="name" onclick="printDiv('areaImprimir')" title="IMPRIMIR REPORTE" value="IMPRIMIR REPORTE"/></td>
    </tr>
  </table><br>
  
<div id="areaImprimir">
  <?php echo $pfinanciero; ?>
</div>
</body>
</html>
