<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta charset="utf-8">
    <link rel="STYLESHEET" href="<?php echo base_url(); ?>assets/print_static.css" type="text/css" />
    <style>
        table{
            font-size: 9px;
            width: 100%;
            background-color:#fff;
        }
  </style>
</head>
<body>

<div id="body">
<div id="section_header">
</div>
  
<div class="page" style="font-size: 7pt" align="center">
      <table style="width: 90%;" class="header">
          <tr>
              <td width=20%; align="center">
                  <img src="<?php echo base_url(); ?>assets/img/logo.jpg" width="150px"/>
              </td>
              <td width=60%; class="titulo_pdf">
                  <b>ENTIDAD : </b><?php echo $this->session->userdata('entidad');?><br>
                  <b>POA - PLAN OPERATIVO ANUAL : </b><?php echo $this->session->userdata("gestion")?><br>
                  <b><?php echo $this->session->userdata('sistema');?></b><br>
                  <b>GR&Aacute;FICO  - CURVA S : </b> AVANCE F&Iacute;SICO ANUAL DE LA OPERACI&Oacute;N <br>
              </td>
              <td width=20%; align="center">
                  <img src="<?php echo base_url(); ?>assets/img/escudo.jpg" class="img-responsive" alt="Cinque Terre" alt="" width="90px" >
              </td>
          </tr>
      </table>
      <table style="width: 90%;" style="table-layout:fixed;" border="1">
        <tbody>
            <tr bgcolor="#ffffff">
                <td colspan="4" align="center">
                  <b><?php echo $proyecto[0]['proy_nombre'];?></b><br>Proyecto/Programa/Operaci&oacte;n
                </td>
            </tr>
            <tr align="center">
                <td style="width:25%;">
                  <b><?php  
                      if($proyecto[0]['tp_id']==1){echo $proyecto[0]['proy_sisin'];}else{echo "N/A";}?></b><br>SISIN
                </td>
                <td style="width:25%;">
                  <b><?php echo $proyecto[0]['aper_programa'].''.$proyecto[0]['aper_proyecto'].''.$proyecto[0]['aper_actividad'];?></b><br>APERTURA PROGRAMATICA
                </td>
                <td style="width:25%;">
                  <b><?php echo $unidad_responsable[0]['uejec'];?></b><br>Unidad Ejecutora
                </td>
                <td style="width:25%;">
                  <b><?php echo $unidad_ejecutora[0]['uresp'];?></b><br>Unidad Responsable
                </td>
            </tr>
        </tbody>
      </table>
        <?php
            $p=100;
            $fase = $this->model_faseetapa->get_id_fase($proyecto[0]['proy_id']);
            $años=$fase[0]['pfec_fecha_fin']-$fase[0]['pfec_fecha_inicio']+1;
            $meses=$años*12;
            for($p=1;$p<=$meses;$p++){$cpp[$p]=0; $cpe[$p]=0;} //// vector vacio para sumar a nivel de productos programacion , ejecutado
            for($p=1;$p<=$meses;$p++){$cef[$p]=0;} //// vector eficacia
            for($p=1;$p<=$meses;$p++){$ef_menor[$p]=0;$ef_entre[$p]=0;$ef_mayor[$p]=0;} //// vector eficacia
            $componentes = $this->model_componente->componentes_id($fase[0]['id']);

            foreach ($componentes as $rowc)
            {
              for($p=1;$p<=$meses;$p++){$ppp[$p]=0; $ppe[$p]=0;} //// vector vacio para sumar a nivel de productos programacion , ejecutado
              $productos = $this->model_producto->list_prod($rowc['com_id']);
              
              foreach ($productos as $rowp)
              {
                $actividad = $this->model_actividad->list_act_anual($rowp['prod_id']);
                for($p=1;$p<=$meses;$p++){$app[$p]=0; $aee[$p]=0;} //// vector vacio para sumar a nivel de actividades programacion , ejecutado
                
                foreach ($actividad as $rowa)
                {   
                  $pa=0; $pe=0; $gestion=$fase[0]['pfec_fecha_inicio']; $a=1;$e=1;
                  for($k=1;$k<=$años;$k++)
                  { 
                    $programado=$this->model_actividad->act_prog_mensual($rowa['act_id'],$gestion); /// programado
                    $ejecutado=$this->model_actividad->act_ejec_mensual($rowa['act_id'],$gestion); /// ejecutado
                    $nro=0;
                    foreach($programado as $row)
                    {
                      $nro++;
                      $matriz [1][$nro]=$row['m_id'];
                      $matriz [2][$nro]=$row['pg_fis'];
                    }
                    /*---------------- llenando la matriz vacia --------------*/
                    for($j = 1; $j<=12; $j++)
                    {
                      $matriz_r[1][$j]=$j;
                      $matriz_r[2][$j]='0';
                      $matriz_r[3][$j]='0';
                      $matriz_r[4][$j]='0';
                      $matriz_r[5][$j]='0';
                      $matriz_r[6][$j]='0';
                      $matriz_r[7][$j]='0';
                      $matriz_r[8][$j]='0';
                      $matriz_r[9][$j]='0';
                      $matriz_r[10][$j]='0';
                    }
                    /*--------------------------------------------------------*/
                   /*--------------------ejecutado gestion ------------------*/
                    $nro_e=0;
                    foreach($ejecutado as $row)
                    {
                      $nro_e++;
                      $matriz_e [1][$nro_e]=$row['m_id'];
                      $matriz_e [2][$nro_e]=$row['ejec_fis'];
                      $matriz_e [3][$nro_e]=$row['ejec_fis_a'];
                      $matriz_e [4][$nro_e]=$row['ejec_fis_b'];
                    }
                    /*--------------------------------------------------------*/
                    /*------- asignando en la matriz P, PA, %PA ----------*/
                    for($i = 1 ;$i<=$nro ;$i++)
                    {
                      for($j = 1 ;$j<=12 ;$j++)
                      {
                        if($matriz[1][$i]==$matriz_r[1][$j])
                        {
                            $matriz_r[2][$j]=round($matriz[2][$i],2);
                        }
                      }
                    }

                    for($j = 1 ;$j<=12 ;$j++){
                      $pa=$pa+$matriz_r[2][$j];
                      $matriz_r[3][$j]=$pa+$rowa['act_linea_base'];
                      if($rowa['act_meta']!=0)
                      {
                        $matriz_r[4][$j]=round(((($pa+$rowa['act_linea_base'])/$rowa['act_meta'])*100),2);
                      }
                      
                    }

                    if($rowa['indi_id']==1)
                    {
                      for($i = 1 ;$i<=$nro_e ;$i++){
                        for($j = 1 ;$j<=12 ;$j++)
                        {
                          if($matriz_e[1][$i]==$matriz_r[1][$j])
                          {
                              $matriz_r[7][$j]=round($matriz_e[2][$i],2);
                          }
                        }
                      }
                    }
                    elseif ($rowa['indi_id']==2) 
                    {
                      for($i = 1 ;$i<=$nro_e ;$i++){
                          for($j = 1 ;$j<=12 ;$j++)
                          {
                            if($matriz_e[1][$i]==$matriz_r[1][$j])
                            {
                              $matriz_r[5][$j]=round($matriz_e[3][$i],2);
                              $matriz_r[6][$j]=round($matriz_e[4][$i],2);
                              $matriz_r[7][$j]=round($matriz_e[2][$i],2);
                            }
                          }
                        }
                      /*--------------------------------------------------------*/
                      }
                      /*--------------------matriz E,AE,%AE gestion ------------------*/
                        for($j = 1 ;$j<=12 ;$j++){
                          $pe=$pe+$matriz_r[7][$j];
                          $matriz_r[8][$j]=$pe+$rowa['act_linea_base'];
                          if($rowa['act_meta']!=0)
                          {
                            $matriz_r[9][$j]=round(((($pe+$rowa['act_linea_base'])/$rowa['act_meta'])*100),2);
                          }
                          
                          if($matriz_r[4][$j]==0)
                            {$matriz_r[10][$j]=round((($matriz_r[9][$j])*100),2);}
                            else
                            {
                            $matriz_r[10][$j]=round((($matriz_r[9][$j]/$matriz_r[4][$j])*100),2);
                            }
                        }
                      
                       // $ap[$n]=$matriz_r[2][1]*$rowa['act_ponderacion']; $n++;
                        $ap[$a]=(($matriz_r[4][1]*$rowa['act_ponderacion'])/100); $a++;
                        $ae[$e]=(($matriz_r[9][1]*$rowa['act_ponderacion'])/100); $e++;

                        $ap[$a]=(($matriz_r[4][2]*$rowa['act_ponderacion'])/100); $a++;
                        $ae[$e]=(($matriz_r[9][2]*$rowa['act_ponderacion'])/100); $e++;

                        $ap[$a]=(($matriz_r[4][3]*$rowa['act_ponderacion'])/100); $a++;
                        $ae[$e]=(($matriz_r[9][3]*$rowa['act_ponderacion'])/100); $e++;

                        $ap[$a]=(($matriz_r[4][4]*$rowa['act_ponderacion'])/100); $a++;
                        $ae[$e]=(($matriz_r[9][4]*$rowa['act_ponderacion'])/100); $e++;

                        $ap[$a]=(($matriz_r[4][5]*$rowa['act_ponderacion'])/100); $a++;
                        $ae[$e]=(($matriz_r[9][5]*$rowa['act_ponderacion'])/100); $e++;

                        $ap[$a]=(($matriz_r[4][6]*$rowa['act_ponderacion'])/100); $a++;
                        $ae[$e]=(($matriz_r[9][6]*$rowa['act_ponderacion'])/100); $e++;

                        $ap[$a]=(($matriz_r[4][7]*$rowa['act_ponderacion'])/100); $a++;
                        $ae[$e]=(($matriz_r[9][7]*$rowa['act_ponderacion'])/100); $e++;

                        $ap[$a]=(($matriz_r[4][8]*$rowa['act_ponderacion'])/100); $a++;
                        $ae[$e]=(($matriz_r[9][8]*$rowa['act_ponderacion'])/100); $e++;

                        $ap[$a]=(($matriz_r[4][9]*$rowa['act_ponderacion'])/100); $a++;
                        $ae[$e]=(($matriz_r[9][9]*$rowa['act_ponderacion'])/100); $e++;

                        $ap[$a]=(($matriz_r[4][10]*$rowa['act_ponderacion'])/100); $a++;
                        $ae[$e]=(($matriz_r[9][10]*$rowa['act_ponderacion'])/100); $e++;

                        $ap[$a]=(($matriz_r[4][11]*$rowa['act_ponderacion'])/100); $a++;
                        $ae[$e]=(($matriz_r[9][11]*$rowa['act_ponderacion'])/100); $e++;

                        $ap[$a]=(($matriz_r[4][12]*$rowa['act_ponderacion'])/100); $a++;
                        $ae[$e]=(($matriz_r[9][12]*$rowa['act_ponderacion'])/100); $e++;

                    $gestion++;

                  }

                  for($k=1;$k<=$meses;$k++)
                  {
                    $app[$k]=round(($app[$k]+$ap[$k]),1); //// sumando a nivel de las actividades
                    $aee[$k]=round(($aee[$k]+$ae[$k]),1); //// sumando a nivel de las actividades
                  }

                }

                for($kp=1;$kp<=$meses;$kp++)
                {
                  $ppp[$kp]=round(($ppp[$kp]+($app[$kp]*$rowp['prod_ponderacion'])/100),1); //// sumando a nivel de los productos
                  $ppe[$kp]=round(($ppe[$kp]+($aee[$kp]*$rowp['prod_ponderacion'])/100),1); //// sumando a nivel de los productos
                } 
              }
              
              for($k=1;$k<=$meses;$k++)
                {
                  $cpp[$k]=round(($cpp[$k]+($ppp[$k]*$rowc['com_ponderacion'])/100),0); //// sumando a nivel de los productos
                  $cpe[$k]=round(($cpe[$k]+($ppe[$k]*$rowc['com_ponderacion'])/100),2); //// sumando a nivel de los productos
                  if($cpp[$k]==0)
                  {$cef[$k]=$cpe[$k];}
                  else{$cef[$k]=round((($cpe[$k]/$cpp[$k])*100),1);}

                  if($cef[$k]<=75){$ef_menor[$k] = $cef[$k];}else{$ef_menor[$k] = 0;}
                  if ($cef[$k] >= 76 && $cef[$k] <= 90.9) {$ef_entre[$k] = $cef[$k];}else {$ef_entre[$k] = 0;}
                  if($cef[$k] >= 91){$ef_mayor[$k] = $cef[$k];}else{$ef_mayor[$k] = 0;}

                }
            }  
        ?>

        <?php 
                        $i=1;$meses=12;$gestion=$fase[0]['pfec_fecha_inicio'];$nro=1;$a=1;
                        for($kp=1;$kp<=$años;$kp++)
                        {   
                          $variable=0;
                          for($pos=$i;$pos<=$meses;$pos++)
                          {
                            $variable++;
                            $prog[$variable]=$cpp[$pos];
                            $ejec[$variable]=$cpe[$pos];
                            $menor[$variable]=$ef_menor[$pos];
                            $entre[$variable]=$ef_entre[$pos];
                            $mayor[$variable]=$ef_mayor[$pos];
                          }
                          
                          if($gestion==$this->session->userdata('gestion'))
                          {
                            ?>
                                <script src="<?php echo base_url(); ?>assets/highcharts/js/highcharts.js"></script> 
                                <!--=================================== GRAFICO ================================= -->
                                <table class="change_order_items" style="width: 90%;">
                                <tbody>
                                  <tr>
                                    <td style="width: 50%;"  colspan="7"><div id="<?php echo 'graf'.$a;?>"></div></td>
                                    <td style="width: 50%;"  colspan="7"><div id="<?php echo 'graf_eficacia'.$a;?>"></div></td>
                                  </tr>
       
                                                            
                                <script type="text/javascript">
                                  var a=<?php echo $a;?>;
                                  var gestion=<?php echo $gestion;?>;
                                    var chart1 = new Highcharts.Chart({
                                          chart: {
                                              renderTo: 'graf'+a, // div contenedor
                                              type: 'line' // tipo de grafico
                                          },

                                          title: {
                                              text: 'PROGRAMACI\u00D3N Y EJECUCI\u00D3N F\u00CDSICA DE OPERACION : '+gestion, // t�tulo  del gr�fico
                                              x: -20 //center
                                          },
                                          subtitle: {
                                              text: ''
                                          },
                                          xAxis: {
                                              categories: ['ENERO', 'FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO', 'JULIO', 'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE', 'NOVIEMBRE', 'DICIEMBRE']
                                          },
                                          yAxis: {
                                              title: {
                                                  text: 'PORCENTAJES (%)'
                                              }
                                          },
                                          plotOptions: {
                                              line: {
                                                  dataLabels: {
                                                      enabled: true
                                                  },
                                                  enableMouseTracking: false
                                              }
                                          },
                                          series: [
                                              {
                                                  name: 'PROGRAMACIÓN ACUMULADA EN %',
                                                  data: [ <?php echo $prog[1];?>, <?php echo $prog[2];?>, <?php echo $prog[3];?>, <?php echo $prog[4];?>, <?php echo $prog[5];?>, <?php echo $prog[6];?>, <?php echo $prog[7];?>, <?php echo $prog[8];?>, <?php echo $prog[9];?>, <?php echo $prog[10];?>, <?php echo $prog[11];?>, <?php echo $prog[12];?>]
                                              },
                                              {
                                                  name: 'EJECUCIÓN ACUMULADA EN %',
                                                  data: [ <?php echo $ejec[1];?>, <?php echo $ejec[2];?>, <?php echo $ejec[3];?>, <?php echo $ejec[4];?>, <?php echo $ejec[5];?>, <?php echo $ejec[6];?>, <?php echo $ejec[7];?>, <?php echo $ejec[8];?>, <?php echo $ejec[9];?>, <?php echo $ejec[10];?>, <?php echo $ejec[11];?>, <?php echo $ejec[12];?>]
                                              }
                                          ]
                                      });
                                    </script>
                                    <script type="text/javascript">
                                      var a=<?php echo $a;?>;
                                      var gestion=<?php echo $gestion;?>;                        
                                      Highcharts.chart('graf_eficacia'+a, {
                                          chart: {
                                              type: 'column'
                                          },
                                          title: {
                                              text: 'EFICACIA : '+gestion
                                          },
                                          xAxis: {
                                              categories: ['ENERO', 'FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO', 'JULIO', 'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE', 'NOVIEMBRE', 'DICIEMBRE']
                                          },
                                          yAxis: {
                                              min: 0,
                                              title: {
                                                  text: 'PORCENTAJES (%)'
                                              },
                                              stackLabels: {
                                                  enabled: true,
                                                  style: {
                                                      fontWeight: 'bold',
                                                      color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                                                  }
                                              }
                                          },
                                          legend: {
                                              align: 'right',
                                              x: -30,
                                              verticalAlign: 'top',
                                              y: 25,
                                              floating: true,
                                              backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                                              borderColor: '#CCC',
                                              borderWidth: 1,
                                              shadow: false
                                          },
                                          tooltip: {
                                              headerFormat: '<b>{point.x}</b><br/>',
                                              pointFormat: '{series.name}: <br/> TOTAL:   {point.y}'
                                          },
                                          plotOptions: {
                                              column: {
                                                  stacking: 'normal',
                                                  dataLabels: {
                                                      enabled: false,
                                                      color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                                                  }
                                              }
                                          },
                                          series: [{

                                              name: '<b style="color: #FF0000;">MENOR A 75%</b>',
                                              data: [{y: <?php echo $menor[1]?>, color: 'red'},{y: <?php echo $menor[2]?>, color: 'red'},{y: <?php echo $menor[3]?>, color: 'red'},{y: <?php echo $menor[4]?>, color: 'red'},{y: <?php echo $menor[5]?>, color: 'red'},{y: <?php echo $menor[6]?>, color: 'red'},{y: <?php echo $menor[7]?>, color: 'red'},{y: <?php echo $menor[8]?>, color: 'red'},{y: <?php echo $menor[9]?>, color: 'red'},{y: <?php echo $menor[10]?>, color: 'red'},{y: <?php echo $menor[11]?>, color: 'red'},{y: <?php echo $menor[12]?>, color: 'red'}] 

                                          }, {
                                              name: '<b style="color: #d6d21f;">ENTRE 76% Y 90%</b>',
                                              data: [{y: <?php echo $entre[1]?>, color: 'yellow'},{y: <?php echo $entre[2]?>, color: 'yellow'},{y: <?php echo $entre[3]?>, color: 'yellow'},{y: <?php echo $entre[4]?>, color: 'yellow'},{y: <?php echo $entre[5]?>, color: 'yellow'},{y: <?php echo $entre[6]?>, color: 'yellow'},{y: <?php echo $entre[7]?>, color: 'yellow'},{y: <?php echo $entre[8]?>, color: 'yellow'},{y: <?php echo $entre[9]?>, color: 'yellow'},{y: <?php echo $entre[10]?>, color: 'yellow'},{y: <?php echo $entre[11]?>, color: 'yellow'},{y: <?php echo $entre[12]?>, color: 'yellow'}] 
                                          }, {
                                              name: '<b style="color: green;">MAYOR A 91%</b>',
                                              data: [{y: <?php echo $mayor[1]?>, color: 'green'},{y: <?php echo $mayor[2]?>, color: 'green'},{y: <?php echo $mayor[3]?>, color: 'green'},{y: <?php echo $mayor[4]?>, color: 'green'},{y: <?php echo $mayor[5]?>, color: 'green'},{y: <?php echo $mayor[6]?>, color: 'green'},{y: <?php echo $mayor[7]?>, color: 'green'},{y: <?php echo $mayor[8]?>, color: 'green'},{y: <?php echo $mayor[9]?>, color: 'green'},{y: <?php echo $mayor[10]?>, color: 'green'},{y: <?php echo $mayor[11]?>, color: 'green'},{y: <?php echo $mayor[12]?>, color: 'green'}] 
                                          }]

                                      });
                                      </script>


                                    <tr class="even_row">
                                      <th colspan="14"> GESTI&Oacute;N : <?php echo $gestion;?></th>
                                    </tr>
                                    <tr>
                                      <th></th>
                                      <th>Enero</th>
                                      <th>Febrero</th>
                                      <th>Marzo</th>
                                      <th>Abril</th>
                                      <th>Mayo</th>
                                      <th>Junio</th>
                                      <th>Julio</th>
                                      <th>Agosto</th>
                                      <th>Sept.</th>
                                      <th>Octubre</th>
                                      <th>Nov.</th>
                                      <th>Dic.</th>
                                      <th></th>
                                    </tr>
                                    <tr class="even_row">
                                        <td>PROGRAMACI&Oacute;N ACUMULADA EN %</td>
                                        <?php 
                                        for($pos=$i;$pos<=$meses;$pos++)
                                        {
                                            echo '<td style="text-align: center">'.$cpp[$pos].'%</td>';
                                        }
                                        ?>
                                        <td bgcolor="blue"></td>
                                    </tr>
                                    <tr class="even_row">
                                        <td>EJECUCI&Oacute;N ACUMULADA EN %</td>
                                        <?php
                                        for($pos=$i;$pos<=$meses;$pos++)
                                        {
                                            echo '<td style="text-align: center">'.$cpe[$pos].'%</td>';
                                        }
                                        ?>
                                        <td></td>
                                    </tr>
                                    <tr class="even_row">
                                        <td>EFICACIA</td>
                                        <?php
                                        for($pos=$i;$pos<=$meses;$pos++)
                                        {
                                            echo '<td style="text-align: center">'.$cef[$pos].'%</td>';
                                        }
                                        
                                        ?>
                                        <td></td>
                                    </tr>
                                    </tbody>
                                </table>
                                
                          <?php
                          }
                          
                          $i=$meses+1;
                          $meses=$meses+12;
                          $gestion++; $nro++; $a++;          
                        } 
                      ?>
                      <div align="center">
                        <table style="width: 80%;">
                            <tr>    
                              <td colspan="3" height="80"><b>FIRMAS</b></td>
                            </tr>
                            <tr>    
                              <td style="width: 33.3%;">RESPONSABLE UNIDAD EJECUTORA</td>
                              <td style="width: 33.3%;">ANALISTA POA</td>
                              <td style="width: 33.3%;">ANALISTA FINANCIERO</td>
                            </tr>
                        </table>
                      </div>

</div>
</div>
</div>


</body>
</html>