<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta charset="utf-8">
    <link rel="STYLESHEET" href="<?php echo base_url(); ?>assets/print_static.css" type="text/css" />
    <title><?php echo $this->session->userData('sistema');?></title>
</head>
<style type="text/css">
        @page {size: letter;}
      </style>
<style>
#areaImprimir{background:#fff;}
.button-container {
    margin: 0 auto;
    padding: 5px; 0;
    width: 50%;
    border: thin solid gray;
    display: flex;
    justify-content: space-around;
}
</style>
  <script type="text/javascript">
    function printDiv(nombreDiv) {
     var contenido= document.getElementById(nombreDiv).innerHTML;
     var contenidoOriginal= document.body.innerHTML;
     document.body.innerHTML = contenido;
     window.print();
     document.body.innerHTML = contenidoOriginal;
}
  </script>
<body>
  <table style="width:100%;">
    <tr bgcolor="#504c49">
      <td style="width:80%;"></td>
      <td style="width:20%;" align="center" bgcolor="#504c49"><input type="button" name="name" onclick="printDiv('areaImprimir')" title="IMPRIMIR REPORTE" value="IMPRIMIR REPORTE"/></td>
    </tr>
  </table><br>
  
<div id="areaImprimir">
  <?php echo $dato_proyecto; ?>
    <table class="change_order_items" style="width:100%;" align="center">
      <tbody>
        <tr class="modo1">
          <td style="width: 50%;"><div id="graf" style="width: 450px; height: 320px; margin: 1 auto"></div></td>
          <td style="width: 50%;"><div id="graf_eficacia" style="width: 450px; height: 320px; margin: 1 auto"></div></td>
        </tr>
      </tbody>
    </table><hr>
  <?php echo $tabla_insumo; ?>
</div>
</body>
<script src="<?php echo base_url();?>/assets/js/libs/jquery-2.0.2.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/highcharts/js/highcharts.js"></script>
      <script type="text/javascript">
        $(function () {
            Highcharts.chart('graf_eficacia', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'EFICACIA'
                },
                xAxis: {
                    categories: ['ENERO', 'FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO', 'JULIO', 'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE', 'NOVIEMBRE', 'DICIEMBRE']
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'PORCENTAJES (%)'
                    },
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                        }
                    }
                },
                legend: {
                    align: 'right',
                    x: -30,
                    verticalAlign: 'top',
                    y: 25,
                    floating: true,
                    backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                    borderColor: '#CCC',
                    borderWidth: 1,
                    shadow: false
                },
                tooltip: {
                    headerFormat: '<b>{point.x}</b><br/>',
                    pointFormat: '{series.name}: <br/> TOTAL:   {point.y}'
                },
                plotOptions: {
                    column: {
                        stacking: 'normal',
                        dataLabels: {
                            enabled: false,
                            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                        }
                    }
                },
                series: [{

                    name: '<b style="color: #FF0000;">MENOR A 75%</b>',
                    data: [
                    <?php 
                      for ($i=1; $i <=12 ; $i++) 
                      {
                        if($i==12){
                          ?>
                          {y: <?php echo $total[9][$i]?>, color: 'red'}
                          <?php
                        }
                        else{
                          ?>
                          {y: <?php echo $total[9][$i]?>, color: 'red'},
                          <?php
                        }
                      }
                    ?>
                   ] 
                }, {
                    name: '<b style="color: #d6d21f;">ENTRE 76% Y 90%</b>',
                    data: [ 
                      <?php 
                        for ($i=1; $i <=12 ; $i++) 
                        {
                          if($i==12){
                            ?>
                            {y: <?php echo $total[10][$i]?>, color: 'yellow'}
                            <?php
                          }
                          else{
                            ?>
                            {y: <?php echo $total[10][$i]?>, color: 'yellow'},
                            <?php
                          }
                        }
                      ?>] 
                }, {
                    name: '<b style="color: green;">MAYOR A 91%</b>',
                    data: [ 
                      <?php 
                        for ($i=1; $i <=12 ; $i++) 
                        {
                          if($i==12){
                            ?>
                            {y: <?php echo $total[11][$i]?>, color: 'green'}
                            <?php
                          }
                          else{
                            ?>
                            {y: <?php echo $total[11][$i]?>, color: 'green'},
                            <?php
                          }
                        }
                      ?>]
                }]
            });
        });
      </script>
      <script type="text/javascript">
      var chart1 = new Highcharts.Chart({
          chart: {
              renderTo: 'graf', // div contenedor
              type: 'line' // tipo de grafico
          },

          title: {
              text: 'PROGRAMACI\u00D3N Y EJECUCI\u00D3N PRESUPUESTARIA DE LA OPERACI\u00D3N', // t�tulo  del gr�fico
              x: -20 //center
          },
          subtitle: {
              text: ''
          },
          xAxis: {
              categories: ['ENERO', 'FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO', 'JULIO', 'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE', 'NOVIEMBRE', 'DICIEMBRE']
          },
          yAxis: {
              title: {
                  text: 'PORCENTAJES (%)'
              }
          },
          plotOptions: {
              line: {
                  dataLabels: {
                      enabled: true
                  },
                  enableMouseTracking: false
              }
          },
          series: [
              {
                  name: 'PROGRAMACIÓN ACUMULADA EN %',
                  data: [
                  <?php 
                    for ($i=1; $i <=12 ; $i++) 
                    {
                      if($i==12){
                        ?>
                        <?php echo $total[4][$i];?>
                        <?php
                      }
                      else{
                        ?>
                        <?php echo $total[4][$i];?>,
                        <?php
                      }
                    }
                  ?>
                  ]
              },
              {
                  name: 'EJECUCIÓN ACUMULADA EN %',
                  data: [
                  <?php 
                    for ($i=1; $i <=12 ; $i++) 
                    {
                      if($i==12){
                        ?>
                        <?php echo $total[7][$i];?>
                        <?php
                      }
                      else{
                        ?>
                        <?php echo $total[7][$i];?>,
                        <?php
                      }
                    }
                  ?>
                  ]
              }
          ]
      });
  </script>
</html>
