<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta charset="utf-8">
    <title> </title>
    <link rel="STYLESHEET" href="<?php echo base_url(); ?>assets/print_static.css" type="text/css" />
  <style>
    table{
        font-size: 9px;
        width: 100%;
        background-color:#fff;
    }

    @page {
    margin: 40px 40px;
    }
  </style>
  <script type="text/javascript">
      function imprimir() {
          if (window.print) {
              window.print();
          } else {
              alert("La función de impresion no esta soportada por su navegador.");
          }
      }
  </script>
</head>
<body>

<body onload="imprimir();">

<div id="section_header">
</div>
    <div class="page" style="font-size: 7pt" align="center">
      <table style="width: 90%;">
          <tr>
              <td width=20%;>
                <img src="<?php echo base_url(); ?>assets/img/logo.jpg" width="180px"/>
              </td>
              <td width=60%;>
                  <b>ENTIDAD : </b><?php echo $this->session->userdata('entidad');?><br>
                  <b>POA - PLAN OPERATIVO ANUAL : </b><?php echo $this->session->userdata("gestion")?><br>
                  <b><?php echo $this->session->userdata('sistema');?></b><br>
                  <b>GR&Aacute;FICO - CURVA S : </b> AVANCE PRESUPUESTARIO MULTI-ANUAL DE LA OPERACI&Oacute;N <br>
              </td>
              <td width=20%; align="center">
                  <img src="<?php echo base_url(); ?>assets/img/escudo.jpg" class="img-responsive" alt="Cinque Terre" alt="" width="80px" >
              </td>
          </tr>
      </table>
      <table style="width: 90%;" style="table-layout:fixed;" border="1">
        <tbody>
            <tr bgcolor="#ffffff">
                <td colspan="4" align="center">
                  <b><?php echo $proyecto[0]['proy_nombre'];?></b><br>Proyecto/Programa/Operaci&oacute;n
                </td>
            </tr>
            <tr align="center">
                <td style="width:25%;">
                  <b><?php  
                      if($proyecto[0]['tp_id']==1){echo $proyecto[0]['proy_sisin'];}else{echo "N/A";}?></b><br>SISIN
                </td>
                <td style="width:25%;">
                  <b><?php echo $proyecto[0]['aper_programa'].''.$proyecto[0]['aper_proyecto'].''.$proyecto[0]['aper_actividad'];?></b><br>APERTURA PROGRAMATICA
                </td>
                <td style="width:25%;">
                  <b><?php echo $unidad_responsable[0]['uejec'];?></b><br>Unidad Ejecutora
                </td>
                <td style="width:25%;">
                  <b><?php echo $unidad_ejecutora[0]['uresp'];?></b><br>Unidad Responsable
                </td>
            </tr>
        </tbody>
      </table><br>
      <table style="width: 90%; border-top: 1px solid black; border-bottom: 1px solid black; font-size: 8pt;">
        <tr>
        <td><strong>PRESUPUESTO TOTAL FASE : </strong> <?php echo number_format($fase[0]['pfec_ptto_fase'], 2, ',', '.');?> Bs.</td>
        </tr>
      </table>
        <?php
         $meses=$años*12;

          for ($i=1; $i <=$meses; $i++)
          {
            $prog_a[$i]=0;$prog_a2[$i]=0; $ejec_a[$i]=0;$ejec_a2[$i]=0; $efi[$i]=0;
            $ef_menor[$i]=0;$ef_entre[$i]=0;$ef_mayor[$i]=0;
            $menor[$i]=0;$entre[$i]=0;$mayor[$i]=0;
          }

          $suma_prog=0;
          for($i=1;$i<=$meses;$i++)
          {
            $suma_prog=$suma_prog+$programado[$i];
            $prog_a[$i]=$suma_prog; ///// Programado acumulado
            $prog_a2[$i]=round((($prog_a[$i]/$fase[0]['pfec_ptto_fase'])*100),2); ////// Programado Acumulado %
          }

          $suma_ejec=0;
          for($i=1;$i<=$meses;$i++)
          {
            $suma_ejec=$suma_ejec+$ejecutado[$i];
            $ejec_a[$i]=$suma_ejec; ///// Programado acumulado
            $ejec_a2[$i]=round((($ejec_a[$i]/$fase[0]['pfec_ptto_fase'])*100),2); ////// Programado Acumulado %
          }

          for($i=1;$i<=$meses;$i++)
          {
            if($prog_a2[$i]!=0)
            {
              $efi[$i]=round((($ejec_a2[$i]/$prog_a2[$i])*100),2);
            }
            if($efi[$i]<=75){$ef_menor[$i] = $efi[$i];}else{$ef_menor[$i] = 0;}
            if ($efi[$i] >= 76 && $efi[$i] <= 90.9) {$ef_entre[$i] = $efi[$i];}else {$ef_entre[$i] = 0;}
            if($efi[$i] >= 91){$ef_mayor[$i] = $efi[$i];}else{$ef_mayor[$i] = 0;}
          }

         $i=1;$meses=12;$gestion=$fase[0]['pfec_fecha_inicio'];$nro=1;$a=1;
          for($kp=1;$kp<=$años;$kp++)
          { 
            $ptto_gestion = $this->model_faseetapa->fase_gestion($fase[0]['id'],$gestion);

            $variable=0;
            for($pos=$i;$pos<=$meses;$pos++)
            {
              $variable++;
              $prog[$variable]=$programado[$pos];
              $prog2[$variable]=$prog_a[$pos];
              $prog3[$variable]=$prog_a2[$pos];

              $ejec[$variable]=$ejecutado[$pos];
              $ejec2[$variable]=$ejec_a[$pos];
              $ejec3[$variable]=$ejec_a2[$pos];

              $menor[$variable]=$ef_menor[$pos];
              $entre[$variable]=$ef_entre[$pos];
              $mayor[$variable]=$ef_mayor[$pos];
            }
            
            ?>

                <script src="<?php echo base_url(); ?>assets/highcharts/js/highcharts.js"></script>
                <!--=================================== GRAFICO ================================= -->
                <table class="change_order_items" style="width: 80%; font-size: 7pt;">
                  <tbody>
                    <tr>
                      <td style="width: 50%;"  colspan="7"><div id="<?php echo 'graf'.$a;?>"></div></td>
                      <td style="width: 50%;"  colspan="7"><div id="<?php echo 'graf_eficacia'.$a;?>"></div></td>
                    </tr>                                            
                <script type="text/javascript">
                  var a=<?php echo $a;?>;
                  var gestion=<?php echo $gestion;?>;
                    var chart1 = new Highcharts.Chart({
                          chart: {
                              renderTo: 'graf'+a, // div contenedor
                              type: 'line' // tipo de grafico
                          },

                          title: {
                              text: 'PROGRAMACI\u00D3N Y EJECUCI\u00D3N F\u00CDSICA DE ACCION : '+gestion, // t�tulo  del gr�fico
                              x: -20 //center
                          },
                          subtitle: {
                              text: ''
                          },
                          xAxis: {
                              categories: ['ENERO', 'FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO', 'JULIO', 'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE', 'NOVIEMBRE', 'DICIEMBRE']
                          },
                          yAxis: {
                              title: {
                                  text: 'PORCENTAJES (%)'
                              }
                          },
                          plotOptions: {
                              line: {
                                  dataLabels: {
                                      enabled: true
                                  },
                                  enableMouseTracking: false
                              }
                          },
                          series: [
                              {
                                  name: 'PROGRAMACIÓN ACUMULADA EN %',
                                  data: [ <?php echo $prog3[1];?>, <?php echo $prog3[2];?>, <?php echo $prog3[3];?>, <?php echo $prog3[4];?>, <?php echo $prog3[5];?>, <?php echo $prog3[6];?>, <?php echo $prog3[7];?>, <?php echo $prog3[8];?>, <?php echo $prog3[9];?>, <?php echo $prog3[10];?>, <?php echo $prog3[11];?>, <?php echo $prog3[12];?>]
                              },
                              {
                                  name: 'EJECUCIÓN ACUMULADA EN %',
                                  data: [ <?php echo $ejec3[1];?>, <?php echo $ejec3[2];?>, <?php echo $ejec3[3];?>, <?php echo $ejec3[4];?>, <?php echo $ejec3[5];?>, <?php echo $ejec3[6];?>, <?php echo $ejec3[7];?>, <?php echo $ejec3[8];?>, <?php echo $ejec3[9];?>, <?php echo $ejec3[10];?>, <?php echo $ejec3[11];?>, <?php echo $ejec3[12];?>]
                              }
                          ]
                      });
                    </script>
                    <script type="text/javascript">
                      var a=<?php echo $a;?>;
                      var gestion=<?php echo $gestion;?>;                        
                      Highcharts.chart('graf_eficacia'+a, {
                          chart: {
                              type: 'column'
                          },
                          title: {
                              text: 'EFICACIA : '+gestion
                          },
                          xAxis: {
                              categories: ['ENERO', 'FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO', 'JULIO', 'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE', 'NOVIEMBRE', 'DICIEMBRE']
                          },
                          yAxis: {
                              min: 0,
                              title: {
                                  text: 'PORCENTAJES (%)'
                              },
                              stackLabels: {
                                  enabled: true,
                                  style: {
                                      fontWeight: 'bold',
                                      color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                                  }
                              }
                          },
                          legend: {
                              align: 'right',
                              x: -30,
                              verticalAlign: 'top',
                              y: 25,
                              floating: true,
                              backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                              borderColor: '#CCC',
                              borderWidth: 1,
                              shadow: false
                          },
                          tooltip: {
                              headerFormat: '<b>{point.x}</b><br/>',
                              pointFormat: '{series.name}: <br/> TOTAL:   {point.y}'
                          },
                          plotOptions: {
                              column: {
                                  stacking: 'normal',
                                  dataLabels: {
                                      enabled: false,
                                      color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                                  }
                              }
                          },
                          series: [{

                              name: '<b style="color: #FF0000;">MENOR A 75%</b>',
                              data: [{y: <?php echo $menor[1]?>, color: 'red'},{y: <?php echo $menor[2]?>, color: 'red'},{y: <?php echo $menor[3]?>, color: 'red'},{y: <?php echo $menor[4]?>, color: 'red'},{y: <?php echo $menor[5]?>, color: 'red'},{y: <?php echo $menor[6]?>, color: 'red'},{y: <?php echo $menor[7]?>, color: 'red'},{y: <?php echo $menor[8]?>, color: 'red'},{y: <?php echo $menor[9]?>, color: 'red'},{y: <?php echo $menor[10]?>, color: 'red'},{y: <?php echo $menor[11]?>, color: 'red'},{y: <?php echo $menor[12]?>, color: 'red'}] 

                          }, {
                              name: '<b style="color: #d6d21f;">ENTRE 76% Y 90%</b>',
                              data: [{y: <?php echo $entre[1]?>, color: 'yellow'},{y: <?php echo $entre[2]?>, color: 'yellow'},{y: <?php echo $entre[3]?>, color: 'yellow'},{y: <?php echo $entre[4]?>, color: 'yellow'},{y: <?php echo $entre[5]?>, color: 'yellow'},{y: <?php echo $entre[6]?>, color: 'yellow'},{y: <?php echo $entre[7]?>, color: 'yellow'},{y: <?php echo $entre[8]?>, color: 'yellow'},{y: <?php echo $entre[9]?>, color: 'yellow'},{y: <?php echo $entre[10]?>, color: 'yellow'},{y: <?php echo $entre[11]?>, color: 'yellow'},{y: <?php echo $entre[12]?>, color: 'yellow'}] 
                          }, {
                              name: '<b style="color: green;">MAYOR A 91%</b>',
                              data: [{y: <?php echo $mayor[1]?>, color: 'green'},{y: <?php echo $mayor[2]?>, color: 'green'},{y: <?php echo $mayor[3]?>, color: 'green'},{y: <?php echo $mayor[4]?>, color: 'green'},{y: <?php echo $mayor[5]?>, color: 'green'},{y: <?php echo $mayor[6]?>, color: 'green'},{y: <?php echo $mayor[7]?>, color: 'green'},{y: <?php echo $mayor[8]?>, color: 'green'},{y: <?php echo $mayor[9]?>, color: 'green'},{y: <?php echo $mayor[10]?>, color: 'green'},{y: <?php echo $mayor[11]?>, color: 'green'},{y: <?php echo $mayor[12]?>, color: 'green'}] 
                          }]

                      });

                      </script>

                      <tr class="even_row">
                        <th colspan="14"> PRESUPUESTO GESTI&Oacute;N <?php echo $gestion;?> : <?php echo number_format($ptto_gestion[0]['pfecg_ppto_total'], 2, ',', '.');?> Bs.</th>
                      </tr>
                      <tr>
                        <th>PROGRAMACI&Oacute;N / EJECUCI&Oacute;N</th>
                        <th>Enero</th>
                        <th>Febrero</th>
                        <th>Marzo</th>
                        <th>Abril</th>
                        <th>Mayo</th>
                        <th>Junio</th>
                        <th>Julio</th>
                        <th>Agosto</th>
                        <th>Sept.</th>
                        <th>Octubre</th>
                        <th>Nov.</th>
                        <th>Dic.</th>
                        <th></th>
                      </tr>
                      <tr class="odd_row">
                          <td>PROGRAMADO</td>
                          <?php 
                          for($pos=$i;$pos<=$meses;$pos++)
                          {
                              echo '<td>'.number_format($programado[$pos], 2, ',', '.').'</td>';
                          }
                          ?>
                          <td></td>
                      </tr>
                      <tr class="odd_row">
                          <td>PROGRAMACI&Oacute;N ACUMULADA</td>
                          <?php 
                          for($pos=$i;$pos<=$meses;$pos++)
                          {
                              echo '<td>'.number_format($prog_a[$pos], 2, ',', '.').'</td>';
                          }
                          ?>
                          <td></td>
                      </tr>
                      <tr class="even_row">
                          <td>PROGRAMACI&Oacute;N ACUMULADA EN %</td>
                          <?php 
                          for($pos=$i;$pos<=$meses;$pos++)
                          {
                              echo '<td>'.$prog_a2[$pos].'%</td>';
                          }
                          ?>
                          <td></td>
                      </tr>
                      <tr class="odd_row">
                          <td>EJECUCI&Oacute;N</td>
                          <?php
                          for($pos=$i;$pos<=$meses;$pos++)
                          {
                              echo '<td>'.number_format($ejecutado[$pos], 2, ',', '.').'</td>';
                          }
                          ?>
                          <td></td>
                      </tr>
                      <tr class="odd_row">
                          <td>EJECUCI&Oacute;N ACUMULADA</td>
                          <?php
                          for($pos=$i;$pos<=$meses;$pos++)
                          {
                              echo '<td>'.number_format($ejec_a[$pos], 2, ',', '.').'</td>';
                          }
                          ?>
                          <td></td>
                      </tr>
                      <tr class="even_row">
                          <td>EJECUCI&Oacute;N ACUMULADA EN %</td>
                          <?php
                          for($pos=$i;$pos<=$meses;$pos++)
                          {
                              echo '<td>'.$ejec_a2[$pos].'</td>';
                          }
                          ?>
                          <td></td>
                      </tr>
                      <tr class="even_row">
                          <td>EFICACIA</td>
                          <?php
                          for($pos=$i;$pos<=$meses;$pos++)
                          {
                              echo '<td>'.$efi[$pos].'%</td>';
                          }
                          ?>
                          <td></td>
                      </tr>
                      </tbody>
                    </table><br>
            <?php

              if($kp==2){echo "<br><br><br><br><br><br>";}
              elseif($kp==3){echo "<br><br><br><br><br><br>";}
              elseif($kp==4){echo "<br><br><br><br><br><br><br><br><br><br><br>";}
              elseif($kp==5){echo "<br><br><br><br><br><br>";}
              elseif($kp==6){echo "<br><br><br><br><br><br><br><br><br><br><br><br>";}
              elseif($kp==7){echo "<br><br><br><br><br>";}
              elseif($kp==8){echo "<br><br><br><br><br><br><br><br><br><br><br><br><br>";}
              elseif($kp==9){echo "<br><br><br><br><br><br><br><br>";}
              elseif($kp==10){echo "<br><br><br><br><br><br>";}


            $i=$meses+1;
            $meses=$meses+12;
            $gestion++; $nro++; $a++; 
          }

         ?>
        <div align="center">
          <table style="width: 80%;">
              <tr>    
                <td colspan="3" height="80"><b>FIRMAS</b></td>
              </tr>
              <tr>    
                <td style="width: 33.3%;">RESPONSABLE UNIDAD EJECUTORA</td>
                <td style="width: 33.3%;">ANALISTA POA</td>
                <td style="width: 33.3%;">ANALISTA FINANCIERO</td>
              </tr>
          </table>
        </div>
  </body>
</html>