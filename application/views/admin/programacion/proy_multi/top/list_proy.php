<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
		<title><?php echo $this->session->userdata('name')?></title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css"> 
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<!--estiloh-->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css"> 
		    <!--para las alertas-->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.core.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css" id="toggleCSS" />
	    <meta name="viewport" content="width=device-width">
		<!--fin de stiloh-->
          <script>
		  	function abreVentana(PDF)
			{
				var direccion;
				direccion = '' + PDF;
				window.open(direccion, "Reporte de Proyectos" , "width=800,height=650,scrollbars=SI") ;
			}                                                  
          </script>
			<style>
			/*////scroll tablas/////*/
			table{font-size: 10px;
            width: 100%;
            max-width:1550px;;
			overflow-x: scroll;
            }
            th{
              padding: 1.4px;
              text-align: center;
              font-size: 10px;
            }
			</style>
	</head>
	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->
		<!-- HEADER -->
		<header id="header">
			<!-- pulled right: nav area -->
			<div class="pull-right">
				<!-- collapse menu button -->
				<div id="hide-menu" class="btn-header pull-right">
					<span> <a href="javascript:void(0);" data-action="toggleMenu" title="Menu"><i class="fa fa-reorder"></i></a> </span>
				</div>
				<!-- end collapse menu -->
				<!-- logout button -->
				<div id="logout" class="btn-header transparent pull-right">
					<span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Salir" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
				</div>
				<!-- end logout button -->
				<!-- search mobile button (this is hidden till mobile view port) -->
				<div id="search-mobile" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
				</div>
				<!-- end search mobile button -->
				<!-- fullscreen button -->
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Pantalla Completa"><i class="fa fa-arrows-alt"></i></a> </span>
				</div>
				<!-- end fullscreen button -->
			</div>
			<!-- end pulled right: nav area -->
		</header>
		<!-- END HEADER -->
		<!-- Left panel : Navigation area -->
		<aside id="left-panel">
			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                            <span>
                                <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                            </span>
						<i class="fa fa-angle-down"></i>
					</a>
				</span>
			</div>
			<nav>
				<ul>
					<li class="">
	                <a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MEN� PRINCIPAL"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
	            	</li>
		            <li class="text-center">
		                <a href="#" title="PROGRAMACION"> <span class="menu-item-parent">GERENCIA DE PROYECTOS</span></a>
		            </li>
				<?php
		            for($i=0;$i<count($enlaces);$i++)
		            {
		                if(count($subenlaces[$enlaces[$i]['o_child']])>0)
		                {
		            ?>
			            <li>
			              	<a href="#" >
			              		<i class="<?php echo $enlaces[$i]['o_image']?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
			              	<ul >
			              	<?php
			                foreach ($subenlaces[$enlaces[$i]['o_child']] as $item) {
			                ?>
			                <li><a href="<?php echo base_url($item['o_url']); ?>"><?php echo $item['o_titulo']; ?></a></li>
			                <?php } ?>
			                </ul>
			            </li>
		            <?php 
		            }
		        } ?>
				</ul>
			</nav>
			<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>
		</aside>

		<!-- MAIN PANEL -->
        <div id="main" role="main">

            <!-- RIBBON -->
            <div id="ribbon">
				<span class="ribbon-button-alignment">
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span>
				</span>
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li>Gerencia de Proyectos</li><li>T&eacute;cnico de Planificaci&oacute;n</li><li>Mis Operaciones</li>
                </ol>
            </div>
            <!-- MAIN CONTENT -->
            <div id="content">
                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <div class="row">
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <?php
                            if($this->session->flashdata('success')){ ?>
                                <div class="alert alert-success">
                                    <?php echo $this->session->flashdata('success'); ?>
                                </div>
                            <?php }
                            elseif($this->session->flashdata('danger')){ ?>
                                <div class="alert alert-danger">
                                <?php echo $this->session->flashdata('danger'); ?>
                                </div><?php }
                            ?>
                            <div class="well well-sm well-light">
                                <h3>GERENCIA DE PROYECTOS PLURIANUALES</h3>
                                <div id="tabs">
                                    <ul>
                                        <li>
                                            <a href="#tabs-c">PROYECTOS DE INVERSI&Oacute;N PUBLICA</a>
                                        </li>
                                        <li>
                                            <a href="#tabs-d">P. RECURRENTES</a>
                                        </li>
                                        <li>
                                            <a href="#tabs-b">P. NO RECURRENTES</a>
                                        </li>
                                        <li>
                                            <a href="#tabs-a">OPERACI&Oacute;N DE FUNCIONAMIENTO</a>
                                        </li>
                                    </ul>
                                    <div id="tabs-c">
                                        <div class="row">
                                            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="jarviswidget jarviswidget-color-darken" >
                                                    <header>
                                                        <span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
                                                        <h2 class="font-md"><strong>PROYECTOS DE INVERSI&Oacute;N</strong></h2>
                                                    </header>
                                                    <div>
                                                        <div class="widget-body no-padding">
                                                            <table id="dt_basic3" class="table1 table-bordered" style="width:100%;" >
                                                            <thead>
                                                            <tr>
                                                                <th style="width:3%;">E/B</th>
                                                                <th>CATEGORIA PROGRAM&Aacute;TICA <?php echo $this->session->userdata("gestion");?></th>
                                                                <th>PROYECTO_PROGRAMA_OPERACI&Oacute;N DE FUNCIONAMIENTO</th>
                                                                <th>TIPO DE OPERACI&Oacute;N</th>
                                                                <th>C&Oacute;DIGO_SISIN</th>
                                                                <th>RESPONSABLE (UE)</th>
                                                                <th>UNIDAD_EJECUTORA</th>
                                                                <th>UNIDAD_RESPONSABLE</th>
                                                                <th>OBSERVACI&Oacute;N</th>
                                                                <th>FASE_ETAPA</th>
                                                                <th>NUEVO_CONTINUIDAD</th>
                                                                <th>ANUAL_PLURIANUAL</th>
                                                                <th>COSTO TOTAL DEL PROYECTO</th>
                                                                <th>PRESUPUESTO REQUERIDO&nbsp;<?php echo $this->session->userdata("gestion");?></th>
                                                                <th>TECHO ASIGNADO TOTAL</th>
                                                                <th>PRIORIDAD</th>
                                                                <th>UBICACI&Oacute;N</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php echo $proyectos;?>
                                                            </tbody>
                                                            </table>
                                                        </div>
                                                        <!-- end widget content -->
                                                    </div>
                                                    <!-- end widget div -->
                                                </div>
                                                <!-- end widget -->
                                            </article>
                                        </div>
                                    </div>

                                    <div id="tabs-d">
                                        <div class="row">
                                            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="jarviswidget jarviswidget-color-darken" >
                                                    <header>
                                                        <span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
                                                        <h2 class="font-md"><strong>P. RECURRENTES</strong></h2>
                                                    </header>
                                                    <div>
                                                        <div class="widget-body no-padding">
                                                            <table id="dt_basic4" class="table1 table-bordered" style="width:100%;" >
                                                            <thead>
                                                            <tr>
                                                                <th style="width:3%;">E/B</th>
                                                                <th>CATEGORIA PROGRAM&Aacute;TICA <?php echo $this->session->userdata("gestion");?></th>
                                                                <th>PROYECTO_PROGRAMA_OPERACI&Oacute;N DE FUNCIONAMIENTO</th>
                                                                <th>TIPO DE OPERACI&Oacute;N</th>
                                                                <th>C&Oacute;DIGO_SISIN</th>
                                                                <th>RESPONSABLE (UE)</th>
                                                                <th>UNIDAD_EJECUTORA</th>
                                                                <th>UNIDAD_RESPONSABLE</th>
                                                                <th>OBSERVACI&Oacute;N</th>
                                                                <th>FASE_ETAPA</th>
                                                                <th>NUEVO_CONTINUIDAD</th>
                                                                <th>ANUAL_PLURIANUAL</th>
                                                                <th>COSTO TOTAL DEL PROYECTO</th>
                                                                <th>PRESUPUESTO REQUERIDO&nbsp;<?php echo $this->session->userdata("gestion");?></th>
                                                                <th>TECHO ASIGNADO TOTAL</th>
                                                                <th>PRIORIDAD</th>
                                                                <th>UBICACI&Oacute;N</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php echo $precurrentes;?>
                                                            </tbody>
                                                            </table>
                                                        </div>
                                                        <!-- end widget content -->
                                                    </div>
                                                    <!-- end widget div -->
                                                </div>
                                                <!-- end widget -->
                                            </article>
                                        </div>
                                    </div>

                                    <div id="tabs-b">
                                        <div class="row">
                                            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="jarviswidget jarviswidget-color-darken" >
                                                    <header>
                                                        <span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
                                                        <h2 class="font-md"><strong>P. NO RECURRENTES</strong></h2>
                                                    </header>
                                                    <div>
                                                        <div class="widget-body no-padding">
                                                            <table id="dt_basic5" class="table1 table-bordered" style="width:100%;" >
                                                            <thead>
                                                            <tr>
                                                                <th style="width:3%;">E/B</th>
                                                                <th>CATEGORIA PROGRAM&Aacute;TICA <?php echo $this->session->userdata("gestion");?></th>
                                                                <th>PROYECTO_PROGRAMA_OPERACI&Oacute;N DE FUNCIONAMIENTO</th>
                                                                <th>TIPO DE OPERACI&Oacute;N</th>
                                                                <th>C&Oacute;DIGO_SISIN</th>
                                                                <th>RESPONSABLE (UE)</th>
                                                                <th>UNIDAD_EJECUTORA</th>
                                                                <th>UNIDAD_RESPONSABLE</th>
                                                                <th>OBSERVACI&Oacute;N</th>
                                                                <th>FASE_ETAPA</th>
                                                                <th>NUEVO_CONTINUIDAD</th>
                                                                <th>ANUAL_PLURIANUAL</th>
                                                                <th>COSTO TOTAL DEL PROYECTO</th>
                                                                <th>PRESUPUESTO REQUERIDO&nbsp;<?php echo $this->session->userdata("gestion");?></th>
                                                                <th>TECHO ASIGNADO TOTAL</th>
                                                                <th>PRIORIDAD</th>
                                                                <th>UBICACI&Oacute;N</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php echo $pnrecurrentes;?>
                                                            </tbody>
                                                            </table>
                                                        </div>
                                                        <!-- end widget content -->
                                                    </div>
                                                    <!-- end widget div -->
                                                </div>
                                                <!-- end widget -->
                                            </article>
                                        </div>
                                    </div>

                                    <div id="tabs-a">
                                        <div class="row">
                                            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="jarviswidget jarviswidget-color-darken" >
                                                    <header>
                                                        <span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
                                                        <h2 class="font-md"><strong>OPERACI&Oacute;N DE FUNCIONAMIENTO</strong></h2>
                                                    </header>
                                                    <div>
                                                        <div class="widget-body no-padding">
                                                            <table id="dt_basic" class="table table-bordered" style="width:100%;">
                                                            <thead>
                                                            <tr>
                                                                <th style="width:3%;">E/B</th>
                                                                <th>CATEGORIA PROGRAM&Aacute;TICA <?php echo $this->session->userdata("gestion");?></th>
                                                                <th>PROYECTO_PROGRAMA_OPERACI&Oacute;N DE FUNCIONAMIENTO</th>
                                                                <th>TIPO DE OPERACI&Oacute;N</th>
                                                                <th>C&Oacute;DIGO_SISIN</th>
                                                                <th>RESPONSABLE (UE)</th>
                                                                <th>UNIDAD_EJECUTORA</th>
                                                                <th>UNIDAD_RESPONSABLE</th>
                                                                <th>OBSERVACI&Oacute;N</th>
                                                                <th>FASE_ETAPA</th>
                                                                <th>NUEVO_CONTINUIDAD</th>
                                                                <th>ANUAL_PLURIANUAL</th>
                                                                <th>COSTO TOTAL DEL PROYECTO</th>
                                                                <th>PRESUPUESTO REQUERIDO&nbsp;<?php echo $this->session->userdata("gestion");?></th>
                                                                <th>TECHO ASIGNADO TOTAL</th>
                                                                <th>PRIORIDAD</th>
                                                                <th>UBICACI&Oacute;N</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php echo $operaciones;?>
                                                            </tbody>
                                                            </table>
                                                        </div>
                                                        <!-- end widget content -->
                                                    </div>
                                                    <!-- end widget div -->
                                                </div>
                                                <!-- end widget -->
                                            </article>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </article>
                    </div>
                </section>
            </div>
            <!-- END MAIN CONTENT -->
            <script>
                function doSelectAlert(event,dato,id) {
                    var option = event.srcElement.children[event.srcElement.selectedIndex];
                    if (option.dataset.noAlert !== undefined) {
                        return;
                    }
                    if(dato==1){valor='BAJA'}
                    if(dato==2){valor='MEDIA'}
                    if(dato==3){valor='ALTA'}
                    var OK = confirm("Prioridad "+valor+ "  GUARDAR INFORMACION ?");
                    if (OK) {
                        var url = "<?php echo site_url("admin")?>/proy/prioridad";
                        $.ajax({
                            type: "post",
                            url: url,
                            data:{id:id,pr:dato},
                            success: function (data) {
                                window.location.reload(true);
                            }
                        });
                    }
                }
            </script>
        </div>
		<!-- END MAIN PANEL -->
		<!-- ========================================================================================================= -->
		<!-- PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->
		<!--================================================== -->
		<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
		<script data-pace-options='{ "restartOnRequestAfter": true }' src="<?php echo base_url(); ?>assets/js/plugin/pace/pace.min.js"></script>
		<script>
			if (!window.jQuery) {
				document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-2.0.2.min.js"><\/script>');
			}
		</script>
		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>
        <script src="<?php echo base_url(); ?>assets/js/session_time/jquery-idletimer.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/app.config.js"></script>
        <script src = "<?php echo base_url(); ?>mis_js/programacion/programacion/tablas.js"></script>
        <!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
        <script src="<?php echo base_url(); ?>assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script>
        <!-- BOOTSTRAP JS -->
        <script src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.min.js"></script>
        <!-- CUSTOM NOTIFICATION -->
        <script src="<?php echo base_url(); ?>assets/js/notification/SmartNotification.min.js"></script>
        <!-- JARVIS WIDGETS -->
        <script src="<?php echo base_url(); ?>assets/js/smartwidgets/jarvis.widget.min.js"></script>
        <!-- EASY PIE CHARTS -->
        <script src="<?php echo base_url(); ?>assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
        <!-- SPARKLINES -->
        <script src="<?php echo base_url(); ?>assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
        <!-- JQUERY VALIDATE -->
        <script src="<?php echo base_url(); ?>assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
        <!-- JQUERY MASKED INPUT -->
        <script src="<?php echo base_url(); ?>assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
        <!-- JQUERY SELECT2 INPUT -->
        <script src="<?php echo base_url(); ?>assets/js/plugin/select2/select2.min.js"></script>
        <!-- JQUERY UI + Bootstrap Slider -->
        <script src="<?php echo base_url(); ?>assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
        <!-- browser msie issue fix -->
        <script src="<?php echo base_url(); ?>assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
        <!-- FastClick: For mobile devices -->
        <script src="<?php echo base_url(); ?>assets/js/plugin/fastclick/fastclick.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
        <!-- Demo purpose only -->
        <script src="<?php echo base_url(); ?>assets/js/demo.min.js"></script>
        <!-- MAIN APP JS FILE -->
        <script src="<?php echo base_url(); ?>assets/js/app.min.js"></script>
        <!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
        <!-- Voice command : plugin -->
        <script src="<?php echo base_url(); ?>assets/js/speech/voicecommand.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
        <!-- PAGE RELATED PLUGIN(S) -->
        <script src="<?php echo base_url(); ?>assets/js/plugin/datatables/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.colVis.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.tableTools.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>

		<!--================= ELIMINAR PROYECTO  =========================================-->
		<script type="text/javascript">
		    $(function () {
                function reset() {
                    $("#toggleCSS").attr("href", "<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css");
                    alertify.set({
                        labels: {
                            ok: "ACEPTAR",
                            cancel: "CANCELAR"
                        },
                        delay: 5000,
                        buttonReverse: false,
                        buttonFocus: "ok"
                    });
                }

                // =====================================================================
                $("#dt_basic").on("click",".del_ff", function (e) {
                    reset();
                    var name = $(this).attr('name');
                    var request;
                    // confirm dialog
                    alertify.confirm("REALMENTE DESEA ELIMINAR ESTE PROYECTO ?", function (a) {
                        if (a) {
                            url = "<?php echo site_url("admin")?>/proy/delete";
                            if (request) {
                                request.abort();
                            }
                            request = $.ajax({
                                url: url,
                                type: "POST",
                                data: {
                                    proy_id: name
                                },

                            });
                            location.reload(true);
                            request.done(function (response, textStatus, jqXHR) {
                                $('#tr' + response).html("");
                            });
                            request.fail(function (jqXHR, textStatus, thrown) {
                                console.log("ERROR: " + textStatus);
                            });
                            request.always(function () {
                                //console.log("termino la ejecuicion de ajax");
                            });

                            e.preventDefault();
                            alertify.success("Se eliminó el Proyecto correctamente");

                        } else {
                            // user clicked "cancel"
                            alertify.error("Opcion cancelada");
                        }
                    });
                    return false;
                });
                $("#dt_basic3").on("click",".del_ff", function (e) {
                    reset();
                    var name = $(this).attr('name');
                    var request;
                    // confirm dialog
                    alertify.confirm("REALMENTE DESEA ELIMINAR ESTE PROYECTO ?", function (a) {
                        if (a) {
                            url = "<?php echo site_url("admin")?>/proy/delete";
                            if (request) {
                                request.abort();
                            }
                            request = $.ajax({
                                url: url,
                                type: "POST",
                                data: {
                                    proy_id: name
                                },

                            });
                            location.reload(true);
                            request.done(function (response, textStatus, jqXHR) {
                                $('#tr' + response).html("");
                            });
                            request.fail(function (jqXHR, textStatus, thrown) {
                                console.log("ERROR: " + textStatus);
                            });
                            request.always(function () {
                                //console.log("termino la ejecuicion de ajax");
                            });

                            e.preventDefault();
                            alertify.success("Se eliminó el Proyecto correctamente");

                        } else {
                            // user clicked "cancel"
                            alertify.error("Opcion cancelada");
                        }
                    });
                    return false;
                });
                $("#dt_basic4").on("click",".del_ff", function (e) {
                    reset();
                    var name = $(this).attr('name');
                    var request;
                    // confirm dialog
                    alertify.confirm("REALMENTE DESEA ELIMINAR ESTE PROYECTO ?", function (a) {
                        if (a) {
                            url = "<?php echo site_url("admin")?>/proy/delete";
                            if (request) {
                                request.abort();
                            }
                            request = $.ajax({
                                url: url,
                                type: "POST",
                                data: {
                                    proy_id: name
                                },

                            });
                            location.reload(true);
                            request.done(function (response, textStatus, jqXHR) {
                                $('#tr' + response).html("");
                            });
                            request.fail(function (jqXHR, textStatus, thrown) {
                                console.log("ERROR: " + textStatus);
                            });
                            request.always(function () {
                                //console.log("termino la ejecuicion de ajax");
                            });

                            e.preventDefault();
                            alertify.success("Se eliminó el Proyecto correctamente");

                        } else {
                            // user clicked "cancel"
                            alertify.error("Opcion cancelada");
                        }
                    });
                    return false;
                });
                $("#dt_basic5").on("click",".del_ff", function (e) {
                    reset();
                    var name = $(this).attr('name');
                    var request;
                    // confirm dialog
                    alertify.confirm("REALMENTE DESEA ELIMINAR ESTE PROYECTO ?", function (a) {
                        if (a) {
                            url = "<?php echo site_url("admin")?>/proy/delete";
                            if (request) {
                                request.abort();
                            }
                            request = $.ajax({
                                url: url,
                                type: "POST",
                                data: {
                                    proy_id: name
                                },

                            });
                            location.reload(true);
                            request.done(function (response, textStatus, jqXHR) {
                                $('#tr' + response).html("");
                            });
                            request.fail(function (jqXHR, textStatus, thrown) {
                                console.log("ERROR: " + textStatus);
                            });
                            request.always(function () {
                                //console.log("termino la ejecuicion de ajax");
                            });

                            e.preventDefault();
                            alertify.success("Se eliminó el Proyecto correctamente");

                        } else {
                            // user clicked "cancel"
                            alertify.error("Opcion cancelada");
                        }
                    });
                    return false;
                });
		    });

		</script>
		<!-- ====================================================================================================== -->
        <script type="text/javascript">
            // DO NOT REMOVE : GLOBAL FUNCTIONS!
            $(document).ready(function() {
                pageSetUp();
                // menu
                $("#menu").menu();
                // Modal Link
                /*
                 * Remove focus from buttons
                 */
                $('.ui-dialog :button').blur();

                $('#tabs').tabs();
            })
        </script>
	</body>
</html>
