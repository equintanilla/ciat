<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

		<title> SIGRE </title>
		<meta name="description" content="">
		<meta name="author" content="">
			
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">

		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">


		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">

		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
		    <script>
                    function abreVentana(PDF)
                    {             
                         var direccion;
                         direccion = '' + PDF;
                         window.open(direccion, "Reporte de Proyectos" , "width=800,height=650,scrollbars=SI") ;
                                                                                        
                    }
                                                                  
          </script>

	</head>
	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->

		<!-- HEADER -->
		<header id="header">
			<div id="logo-group">
				<span id="logo"> <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="SmartAdmin"> </span>
			</div>

			<!-- pulled right: nav area -->
			<div class="pull-right">
				
				<!-- collapse menu button -->
				<div id="hide-menu" class="btn-header pull-right">
					<span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
				</div>
				<!-- end collapse menu -->

				<!-- logout button -->
				<div id="logout" class="btn-header transparent pull-right">
					<span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Sign Out" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
				</div>
				<!-- end logout button -->

				<!-- search mobile button (this is hidden till mobile view port) -->
				<div id="search-mobile" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
				</div>
				<!-- end search mobile button -->

				<!-- fullscreen button -->
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen"><i class="fa fa-arrows-alt"></i></a> </span>
				</div>
				<!-- end fullscreen button -->

			</div>
			<!-- end pulled right: nav area -->

		</header>
		<!-- END HEADER -->

		<!-- Left panel : Navigation area -->
		<aside id="left-panel">

			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
					
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                              <span>
                                  <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                              </span>
						<i class="fa fa-angle-down"></i>
					</a> 
					
				</span>
			</div>

			<nav>
				<ul>
					<li class="">
	                <a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MENÚ PRINCIPAL">
	                <i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
	            	</li>
		            <li class="text-center">
		                <a href="<?php echo base_url().'index.php/admin/sgp/list_proy' ?>" title="MIS PROYECTOS"><span class="menu-item-parent">GERENCIA DE PROYECTOS</span></a>
		            </li>
				<?php
		                for($i=0;$i<count($enlaces);$i++)
		                {
		                    if(count($subenlaces[$enlaces[$i]['o_child']])>0)
		                    {
		            ?>
		            <li>
		              	<a href="#" >
		              		<i class="<?php echo $enlaces[$i]['o_image']?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
		              	<ul >
		              	<?php
		                foreach ($subenlaces[$enlaces[$i]['o_child']] as $item) {
		                ?>
		                <li><a href="<?php echo base_url($item['o_url']); ?>"><?php echo $item['o_titulo']; ?></a></li>
		                <?php } ?>
		                </ul>
		            </li>
		            <?php 
		                    }
		            	} ?>
				</ul>
			</nav>
			<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>

		</aside>

		<!-- MAIN PANEL -->
		<div id="main" role="main">
			<!-- RIBBON -->
			<div id="ribbon">
				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>

				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url().'index.php/admin/sgp/list_proy' ?>" title="MIS PROYECTOS">Gerencia de Proyectos</a></li><li><a href="<?php echo base_url().'index.php/admin/sgp/list_proy' ?>">Cat&aacute;logo de Proyectos</a></li><li>Archivos Adjuntos</li>
				</ol>
			</div>
			<!-- END RIBBON -->

			<!-- MAIN CONTENT -->
			<div id="content">
				<!-- widget grid -->
				<section id="widget-grid" class="">
					<!-- row -->
					<div class="row">
							<!-- end widget -->
							<article class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
								<div class="well">
								<b>PROYECTO : </b><?php echo $proyecto[0]['proy_nombre'] ?><br>
								<b>TIPO DE PROYECTO : </b><?php echo strtoupper($proyecto[0]['tipo']) ?><br>
								<b>FECHA DE INICIO: </b><?php echo date('d/m/Y',strtotime($proyecto[0]['f_inicial'])); ?><br>
								<b>FECHA DE CONCLUSI&Oacute;N : </b><?php echo date('d/m/Y',strtotime($proyecto[0]['f_final'])); ?><br>
								</div>
								
									<div class="jarviswidget jarviswidget-color-darken" >
										<header>
											<span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
											<h2 class="font-md"><strong>ARCHIVOS DEL PROYECTO</strong></h2>				
										</header>
									<div>
									<!-- widget content -->
									<div class="widget-body">
									<div class="well">
										<div class="table-responsive">
											<table class="table table-bordered">
												<thead>			                
													<tr>
														<th style="width:1%;">Nro</th>
														<th style="width:15%;"><font size="1">DOCUMENTO / ARCHIVO</font></th>
														<th style="width:5%;"><font size="1">VER DOCUMENTO</font></th>
													</tr>
												</thead>
												<?php $num=1;
								                foreach($arch as $row)
								                {
								                    echo '<tr>';
								                    echo '<td><font size="1">'.$num.'</font></td>';
								                    echo '<td><font size="1">'.$row['documento'].'</font></td>';
								                	echo '<td><center>';
								                	if(file_exists("archivos/documentos/".$row['adj_adjunto'])) ////// Existe Archivo Almacenado
								                    {
								                        if($row['tp_doc']==1) ///// imagen
								                        {
								                        ?>
									                        <a href='<?php echo site_url("admin").'/sgp/get_arch/'.$id.'/'.$row['adj_id']; ?>' title="VER DOCUMENTO"><img src="<?php echo base_url(); ?>assets/ifinal/img.jpg" WIDTH="35" HEIGHT="35"/>
									                    <?php
								                        }
								                        elseif ($row['tp_doc']==2) ///// pdf
								                        {
								                        ?>
									                        <a href='<?php echo site_url("admin").'/sgp/get_arch/'.$id.'/'.$row['adj_id']; ?>' title="VER DOCUMENTO"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="35" HEIGHT="35"/>
									                    <?php
								                        }
								                        elseif ($row['tp_doc']==3) ///// word
								                        {
								                        ?>
									                        <a href='<?php echo site_url("admin").'/sgp/get_arch/'.$id.'/'.$row['adj_id']; ?>' title="VER DOCUMENTO"><img src="<?php echo base_url(); ?>assets/ifinal/word.png" WIDTH="35" HEIGHT="35"/>
									                    <?php
								                        }
								                        elseif ($row['tp_doc']==4) ///// excel
								                        {
								                        ?>
									                        <a href='<?php echo site_url("admin").'/sgp/get_arch/'.$id.'/'.$row['adj_id']; ?>' title="VER DOCUMENTO"><img src="<?php echo base_url(); ?>assets/ifinal/excel.jpg" WIDTH="35" HEIGHT="35"/>
									                    <?php
								                        }
								                                        	
								                    }
								                    else ////// No Existe Archivo Almacenado
								                    {
								                        ?>
								                        <a href="#" class="btn btn-labeled btn-success" title="NOSE ENCUENTRA ARCHIVO/DOCUMENTO FISICO"><i class="glyphicon glyphicon-file"></i>
								                    <?php
								                    }
								                    echo '</center></td>';
								                
								                    echo '</tr>';
								                    $num=$num+1;
								                }
								                ?>
											</table>
										</div>
									</div>
									</div>
									</div>
								
							</article>
							<article class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
								<div class="jarviswidget jarviswidget-color-darken" >
									<header>
										<span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
											<h2 class="font-md"><strong>DOCUMENTO SELECCIONADO</strong></h2>				
									</header>
								<div>
								<!-- widget content -->
								<div class="widget-body">
								<?php 
									if($if=='true')
									{
										?>
										<iframe id="ipdf" width="100%"  height="550px" src="<?php echo base_url(); ?>archivos/documentos/<?php echo $adj ?>" ></iframe>
										<?php
									}
									elseif ($if=='false') 
									{
										?>
										<iframe id="ipdf" width="100%"  height="550px" src="" ></iframe>
										<?php
									}
								?>
															
								</div>
									<div class="form-actions">
											<a href="<?php echo base_url().'index.php/admin/sgp/list_proy' ?>" class="btn btn-lg btn-default" title="VOLVER A MIS PROYECTOS"> VOLVER </a>
									</div>
								</div>
								</div>
								
							</article>
					</div>
					<!-- end row -->
				</section>
				<!-- end widget grid -->					
			</div>
			<!-- END MAIN CONTENT -->

		</div>
		<!-- END MAIN PANEL -->

	<!-- /.modal -->
		<!-- PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white">SIGRE <?php echo $this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->

		<!--================================================== -->
	
		<script>
			if (!window.jQuery) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-2.0.2.min.js"><\/script>');
			}
		</script>

		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>

		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url(); ?>assets/js/session_time/jquery-idletimer.js"></script>
		<script src="<?php echo base_url();?>/assets/js/app.config.js"></script>
		<script src = "<?php echo base_url(); ?>mis_js/control_session.js"></script>
		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 

		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url();?>/assets/js/bootstrap/bootstrap.min.js"></script>

		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url();?>/assets/js/notification/SmartNotification.min.js"></script>

		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url();?>/assets/js/smartwidgets/jarvis.widget.min.js"></script>

		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url();?>/assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

		<!-- SPARKLINES -->
		<script src="<?php echo base_url();?>/assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>

		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>

		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>

		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/select2/select2.min.js"></script>

		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>

		<!-- browser msie issue fix -->
		<script src="<?php echo base_url();?>/assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>

		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url();?>/assets/js/plugin/fastclick/fastclick.min.js"></script>

		<!-- Demo purpose only -->
		<script src="<?php echo base_url();?>/assets/js/demo.min.js"></script>

		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url();?>/assets/js/app.min.js"></script>

		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<!-- Voice command : plugin -->
		<script src="<?php echo base_url();?>/assets/js/speech/voicecommand.min.js"></script>

		<!-- PAGE RELATED PLUGIN(S) -->

			<!--================= MODIFICAR APERTURA=========================================-->

	</body>

</html>
