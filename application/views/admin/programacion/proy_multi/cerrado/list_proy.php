<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
		<title><?php echo $this->session->userdata('name')?></title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css"> 
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<!--estiloh-->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css"> 
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.core.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css" id="toggleCSS" />
	    <meta name="viewport" content="width=device-width">
		<!--fin de stiloh-->
          <script>
            function abreVentana(PDF)
            {             
                var direccion;
                direccion = '' + PDF;
                window.open(direccion, "Reporte de Proyectos" , "width=800,height=650,scrollbars=SI") ;                                                               
            }                                             
          </script>
			<style>
			/*////scroll tablas/////*/
			table{font-size: 10px;
            width: 100%;
            max-width:1550px;;
			overflow-x: scroll;
            }
            th{
              padding: 1.4px;
              text-align: center;
              font-size: 10px;
            }
			</style>
	</head>
	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->

		<!-- HEADER -->
		<header id="header">
			<!-- pulled right: nav area -->
			<div class="pull-right">
				<!-- collapse menu button -->
				<div id="hide-menu" class="btn-header pull-right">
					<span> <a href="javascript:void(0);" data-action="toggleMenu" title="Menu"><i class="fa fa-reorder"></i></a> </span>
				</div>
				<!-- end collapse menu -->
				<!-- logout button -->
				<div id="logout" class="btn-header transparent pull-right">
					<span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Salir" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
				</div>
				<!-- end logout button -->
				<!-- search mobile button (this is hidden till mobile view port) -->
				<div id="search-mobile" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
				</div>
				<!-- end search mobile button -->
				<!-- fullscreen button -->
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Pantalla Completa"><i class="fa fa-arrows-alt"></i></a> </span>
				</div>
				<!-- end fullscreen button -->
			</div>
			<!-- end pulled right: nav area -->
		</header>
		<!-- END HEADER -->

		<!-- Left panel : Navigation area -->
		<aside id="left-panel">

			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                        <span>
                            <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                        </span>
						<i class="fa fa-angle-down"></i>
					</a> 
				</span>
			</div>
			<nav>
				<ul>
					<li class="">
	                <a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MEN� PRINCIPAL"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
	            	</li>
		            <li class="text-center">
		                <a href="#" title="PROGRAMACION"> <span class="menu-item-parent">GERENCIA DE PROYECTOS</span></a>
		            </li>
					<?php
			                for($i=0;$i<count($enlaces);$i++)
			                {
			                    if(count($subenlaces[$enlaces[$i]['o_child']])>0)
			                    {
			            ?>
			            <li>
			              	<a href="#" >
			              		<i class="<?php echo $enlaces[$i]['o_image']?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
			              	<ul >
			              	<?php
			                foreach ($subenlaces[$enlaces[$i]['o_child']] as $item) {
			                ?>
			                <li><a href="<?php echo base_url($item['o_url']); ?>"><?php echo $item['o_titulo']; ?></a></li>
			                <?php } ?>
			                </ul>
			            </li>
			            <?php 
			            }
			        } ?>
				</ul>
			</nav>
			<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>

		</aside>

		<!-- MAIN PANEL -->
		<div id="main" role="main">

			<!-- RIBBON -->
			<div id="ribbon">

				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>

				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li>Gerencia de Proyectos</li><li>Cat&aacute;logo de Proyectos</li><li>Lista de Operaciones Concluidas</li>
				</ol>
			</div>

			<!-- MAIN CONTENT -->
			<div id="content">
				<!-- widget grid -->
				<section id="widget-grid" class="">
					<div class="row">
						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="jarviswidget jarviswidget-color-darken" >
                              <header>
                                  <span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
                                  <h2 class="font-md"><strong>LISTA DE OPERACIONES CONCLUIDAS <?php echo $this->session->userdata("gestion")?></strong></h2>  
                              </header>
								<div>
									<div class="widget-body no-padding">
									<div class="table-responsive">
										<table id="dt_basic" class="table table-bordered" style="width:100%;" font-size: "7px";>
											<thead>
												<tr>
													<th style="width:1%;"></th>
													<th>CATEGORIA PROGRAM&Aacute;TICA <?php echo $this->session->userdata("gestion");?></th> 
													<th>&nbsp;PROYECTO_PROGRAMA_OPERACI&Oacute;N_DE_FUNCIONAMIENTO&nbsp;</th>
													<th>&nbsp;TIPO_DE_OPERACI&Oacute;N&nbsp;</th>
													<th>&nbsp;C&Oacute;DIGO_SISIN&nbsp;</th>
													<th>&nbsp;RESPONSABLE (UE)&nbsp;</th>
													<th>&nbsp;UNIDAD_EJECUTORA&nbsp;</th>
													<th>&nbsp;UNIDAD_RESPONSABLE&nbsp;</th>
													<th>&nbsp;FASE_ETAPA&nbsp;</th>
													<th><center>&nbsp;NUEVO_CONTINUIDAD&nbsp;</th>
													<th>&nbsp;ANUAL_PLURIANUAL&nbsp;</th>
													<th>&nbsp;COSTO_TOTAL_DEL_PROYECTO&nbsp;</th>
													<th>PROVINCIA_MUNICIPIO</th>
												</tr>
											</thead>
											<tbody>
											<?php $nro=1;
			                                    foreach($lista_aper_padres  as $rowa)
			                                    {
			                                    	$proyectos=$this->model_proyecto->list_proyectos($rowa['aper_programa'],5,1);
			                                    	if(count($proyectos)!=0)
			                                    	{
			                                    		echo '<tr bgcolor="#99DDF0">';
			                                    	 	echo '<td></td>';
			                                    	 	echo '<td><center>' .$rowa['aper_programa'].''.$rowa['aper_proyecto'].''.$rowa['aper_actividad'].'</td></td>';
											            echo '<td>' . $rowa['aper_descripcion'] . '</td>';
											            echo '<td>' . $rowa['aper_sisin'] . '</td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
			                                    	echo '</tr>';
			                                    	
			                                    	foreach($proyectos as $row)
			                                    	{
			                                    	if($row['tp_id']!=1){$color='#D7FCF9';}else{$color='#ffffff';}
			                                    	$fase = $this->model_faseetapa->get_id_fase($row['proy_id']); ////// datos fase activa
			                                    	$fase_gest = $this->model_faseetapa->fase_etapa_gestion($fase[0]['id'],$this->session->userdata("gestion"));
			                                    	echo '<tr bgcolor='.$color.'>';
			                                    	echo '<td>';
			                                    	?>
			                                    		<center><a href="javascript:abreVentana('<?php echo site_url("admin").'/dictamen_proyecto/'.$row['proy_id'].'/'.$fase[0]['pfec_ejecucion'].'' ?>');" title="DICTAMEN DEL PROYECTO"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Registro de Proyecto</a></center>
			                                    		<br><center><a data-toggle="modal" data-target="#<?php echo $nro;?>" title="MIS REPORTES" ><img src="<?php echo base_url(); ?>assets/ifinal/doc.jpg" WIDTH="40" HEIGHT="40"/><br>Mis reportes</a></center>
			                                        	<div class="modal fade bs-example-modal-lg" tabindex="-1" id="<?php echo $nro;?>"  role="dialog" aria-labelledby="myLargeModalLabel">
										                    <div class="modal-dialog modal-lg" role="document">
										                     	<div class="modal-content">
																	<div class="modal-header">
																		<button type="button" class="close text-danger" data-dismiss="modal" aria-hidden="true">
																			&times;
																		</button>
																	    <h4 class="modal-title">
																            <img src="<?php echo base_url(); ?>assets/img/logo.png" width="150" alt="SmartAdmin">
																        </h4>
																	</div>
																	<div class="modal-body no-padding">
																		<div class="well">
																			<table class="table table-hover">
																				<thead>
																				<tr>
																					<th colspan="3"><center><?php echo $row['proy_nombre'];?></center></th>
																				</tr>
																				</thead>
																				<tr>
																					<td><b>IDENTIFICACI&Oacute;N DE LA ETAPA</b></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/identificacion/'.$row['proy_id']; ?>');" title="IDENTIFICACION DE LA ETAPA"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/identificacion/'.$row['proy_id']; ?>');" title="IDENTIFICACION DE LA ETAPA"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
																				</tr>
																				<tr>
																					<td><b>PROGRAMACI&Oacute;N Y EJECUCI&Oacute;N F&Iacute;SICA DE LA ETAPA</b></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/rep_programacion/1/'.$fase[0]['id'].'/'.$row['proy_id']; ?>');" title="PROGRAMACION Y EJECUCION FISICA ANUAL DE LA ETAPA"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/rep_programacion/4/'.$fase[0]['id'].'/'.$row['proy_id']; ?>');" title="PROGRAMACION Y EJECUCION FISICA PLURI-ANUAL DE LA ETAPA"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
																				</tr>
																				<tr>
																					<td><b>PROGRAMACI&Oacute;N Y EJECUCI&Oacute;N FINANCIERA DE LA ETAPA</b></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/rep_financiero/1/'.$fase[0]['id'].'/'.$row['proy_id']; ?>');" title="PROGRAMACION Y EJECUCION FINANCIERA ANUAL DE LA ETAPA"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/rep_financiero/4/'.$fase[0]['id'].'/'.$row['proy_id']; ?>');" title="PROGRAMACION Y EJECUCION FINANCIERA PLURI-ANUAL DE LA ETAPA"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
																				</tr>
																				<tr>
																					<td><b>REGISTRO DE CONTRATOS</b></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/contratos/'.$row['proy_id'].'/'.$fase[0]['id'].''; ?>');" title="REPORTE DE CONTRATOS"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/contratos/'.$row['proy_id'].'/'.$fase[0]['id'].''; ?>');" title="REPORTE DE CONTRATOS"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
																				</tr>
																				<tr>
																					<td><b>INFORMES DE SUPERVISI&Oacute;N Y EVALUACI&Oacute;N</b></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/supervision_evaluacion/'.$row['proy_id'].'/'.$fase[0]['id'].''; ?>');" title="REPORTE DE CONTRATOS"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/supervision_evaluacion/'.$row['proy_id'].'/'.$fase[0]['id'].''; ?>');" title="REPORTE DE CONTRATOS"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
																				</tr>
																				<tr>
																					<td><b>CURVA "S" AVANCE F&Iacute;SICO </b></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/rep_curva_fis/1/'.$fase[0]['id'].'/'.$row['proy_id']; ?>');" title="GRAFICO AVANCE FISICO ANUAL"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/rep_curva_fis/4/'.$fase[0]['id'].'/'.$row['proy_id']; ?>');" title="GRAFICO AVANCE FISICO PLURI-ANUAL"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
																				</tr>
																				<tr>
																					<td><b>CURVA "S" AVANCE FINANCIERO</b></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/rep_curva_fin/1/'.$fase[0]['id'].'/'.$row['proy_id']; ?>');" title="GRAFICO AVANCE FINANCIERO ANUAL"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/rep_curva_fin/4/'.$fase[0]['id'].'/'.$row['proy_id']; ?>');" title="GRAFICO AVANCE FINANCIERO PLURI-ANUAL"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
																				</tr>
																			</table>
																		</div>
																	</div>
																</div><!-- /.modal-content -->
		                                                    </div>
		                                                </div><br>
				                                    	<?php
				                                       	echo '</td>';
				                                        echo '<td><center>'.$row['aper_programa'].''.$row['aper_proyecto'].''.$row['aper_actividad'].'</center></td>';
				                                        echo '<td>'.$row['proy_nombre'].'</td>';
				                                        echo '<td>'.$row['tp_tipo'].'</td>';
				                                        echo '<td>'.$row['proy_sisin'].'</td>';
				                                        echo '<td>'.$row['fun_nombre'].' '.$row['fun_paterno'].' '.$row['fun_materno'].'</td>';
				                                        echo '<td>'.$row['ue'].'</td>';
				                                        echo '<td>'.$row['ur'].'</td>';
				                                        $nc=$this->model_faseetapa->calcula_nc($fase[0]['pfec_fecha_inicio']); //// calcula nuevo/continuo
				                                        $ap=$this->model_faseetapa->calcula_ap($fase[0]['pfec_fecha_inicio'],$fase[0]['pfec_fecha_fin']); //// calcula Anual/Plurianual
				                                        echo '<td>* '.$fase[0]['fase'].'<br>* '.$fase[0]['etapa'].'</td>';
				                                        echo '<td>'.$nc.'</td>';
				                                        echo '<td>'.$ap.'</td>';
				                                        echo '<td>'.number_format($fase[0]['pfec_ptto_fase'], 2, ',', ' ').' Bs.</td>';
				                                        
				                                      	/*==================================== UBICACION GEOGRAFICA =====================================================*/
				                                        	echo '<td>';
						                                        $loc=$this->model_proyecto->localizacion($row['proy_id']);
												                if(count($loc)!=0){
												                  echo '<table class="table table-bordered">';
												                    echo '<tr bgcolor="#f5f5f5">';
												                    echo '<th>PROVINCIA</th><th>MUNICIPIO</th>';
												                    echo '</tr>';
												                    foreach($loc as $locali)
												                    {
												                      echo '<tr>';
												                      echo '<td>'.$locali['prov_provincia'].'</td>';
												                      echo '<td>'.$locali['muni_municipio'].'</td>';
												                      echo '</tr>';
												                    }
												                  
												                  echo '</table>';
												                }
						                                    echo'</td>';
				                                    	echo '</tr>';
				                                    	}
			                                    	}
			                                    }
			                                ?>
											</tbody>
										</table>
										</div>

									</div>
									<!-- end widget content -->
				
								</div>
								<!-- end widget div -->
							</div>
							<!-- end widget -->
						</article>
						<!-- WIDGET END -->
					</div>
				</section>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN PANEL -->
		<!-- PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->
		<!--================================================== -->
		<script>
		function doSelectAlert(event,dato,id) {
		    var option = event.srcElement.children[event.srcElement.selectedIndex];
		    if (option.dataset.noAlert !== undefined) {
		        return;
		    }
		    if(dato==1){valor='BAJA'}
		    if(dato==2){valor='MEDIA'}
		    if(dato==3){valor='ALTA'}
		    var OK = confirm("Prioridad "+valor+ "  GUARDAR INFORMACION ?");
				if (OK) {
					var url = "<?php echo site_url("admin")?>/proy/prioridad";
				        $.ajax({
				            type: "post",
				            url: url,
				            data:{id:id,pr:dato},
				                success: function (data) {
				                window.location.reload(true);
				            }
				        });
					}
		}
		</script>
		<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
		<script data-pace-options='{ "restartOnRequestAfter": true }' src="<?php echo base_url(); ?>assets/js/plugin/pace/pace.min.js"></script>
		<script>
			if (!window.jQuery) {
				document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-2.0.2.min.js"><\/script>');
			}
		</script>
		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>
		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url(); ?>assets/js/session_time/jquery-idletimer.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/app.config.js"></script>
		<script src = "<?php echo base_url(); ?>mis_js/control_session.js"></script>
		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.min.js"></script>
		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url(); ?>assets/js/notification/SmartNotification.min.js"></script>
		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url(); ?>assets/js/smartwidgets/jarvis.widget.min.js"></script>
		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<!-- SPARKLINES -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/select2/select2.min.js"></script>
		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
		<!-- browser msie issue fix -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/fastclick/fastclick.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
		<!-- Demo purpose only -->
		<script src="<?php echo base_url(); ?>assets/js/demo.min.js"></script>
		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url(); ?>assets/js/app.min.js"></script>
		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<!-- Voice command : plugin -->
		<script src="<?php echo base_url(); ?>assets/js/speech/voicecommand.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
		<!-- PAGE RELATED PLUGIN(S) -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.colVis.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.tableTools.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
		<!--================= ELIMINAR PROYECTO  =========================================-->
		<script type="text/javascript">
		    $(function () {
		        function reset() {
		            $("#toggleCSS").attr("href", "<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css");
		            alertify.set({
		                labels: {
		                    ok: "ACEPTAR",
		                    cancel: "CANCELAR"
		                },
		                delay: 5000,
		                buttonReverse: false,
		                buttonFocus: "ok"
		            });
		        }

		        // =====================================================================
		        $(".del_ff").on("click", function (e) {
		            reset();
		            var name = $(this).attr('name');
		            var request;
		            // confirm dialog
		            alertify.confirm("REALMENTE DESEA ELIMINAR ESTA OPERACION ?", function (a) {
		                if (a) { 
		                    url = "<?php echo site_url("admin")?>/proy/delete";
		                    if (request) {
		                        request.abort();
		                    }
		                    request = $.ajax({
		                        url: url,
		                        type: "POST",
		                        data: {
		                            proy_id: name
		                        },

		                    });
		                    window.location.reload(true);
		                    request.done(function (response, textStatus, jqXHR) {
		                        $('#tr' + response).html("");
		                    });
		                    request.fail(function (jqXHR, textStatus, thrown) {
		                        console.log("ERROR: " + textStatus);
		                    });
		                    request.always(function () {
		                        //console.log("termino la ejecuicion de ajax");
		                    });

		                    e.preventDefault();
		                    alertify.success("Se elimin� el Proyecto correctamente");

		                } else {
		                    // user clicked "cancel"
		                    alertify.error("Opcion cancelada");
		                }
		            });
		            return false;
		        });
		    });

		</script>
		<script type="text/javascript">
		$(document).ready(function() {
			pageSetUp();
			/* BASIC ;*/
				var responsiveHelper_dt_basic = undefined;
				var responsiveHelper_datatable_fixed_column = undefined;
				var responsiveHelper_datatable_col_reorder = undefined;
				var responsiveHelper_datatable_tabletools = undefined;
				var breakpointDefinition = {
					tablet : 1024,
					phone : 480
				};
	
				$('#dt_basic').dataTable({
					"ordering": false,
					"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
					"autoWidth" : true,
					"preDrawCallback" : function() {
						// Initialize the responsive datatables helper once.
						if (!responsiveHelper_dt_basic) {
							responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
						}
					},
					"rowCallback" : function(nRow) {
						responsiveHelper_dt_basic.createExpandIcon(nRow);
					},
					"drawCallback" : function(oSettings) {
						responsiveHelper_dt_basic.respond();
					}
				});
		})

		</script>
	</body>
</html>
