<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

		<title><?php echo $this->session->userdata('name')?></title>
		<meta name="description" content="">
		<meta name="author" content="">
			
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<!--///////////////css-->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
		<!--//////////////fin css-->
		<style type="text/css">
			aside{background: #05678B;}
		</style>
	</head>
	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->

		<!-- HEADER -->
		<header id="header">
			<div id="logo-group">
				<!-- <span id="logo"> <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="SmartAdmin"> </span> -->
			</div>

			<!-- pulled right: nav area -->
			<div class="pull-right">
				<!-- collapse menu button -->
				<div id="hide-menu" class="btn-header pull-right">
					<span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
				</div>
				<!-- end collapse menu -->
				<!-- logout button -->
				<div id="logout" class="btn-header transparent pull-right">
					<span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Sign Out" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
				</div>
				<!-- end logout button -->
				<!-- search mobile button (this is hidden till mobile view port) -->
				<div id="search-mobile" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
				</div>
				<!-- end search mobile button -->
				<!-- fullscreen button -->
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen"><i class="fa fa-arrows-alt"></i></a> </span>
				</div>
				<!-- end fullscreen button -->
			</div>
			<!-- end pulled right: nav area -->
		</header>
		<!-- END HEADER -->

		<!-- Left panel : Navigation area -->
		<aside id="left-panel">
			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                        <span>
                            <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                        </span>
					</a> 
				</span>
			</div>

			<nav>
				<ul>
					<li>
						<a href='<?php echo site_url("admin").'/dashboard'; ?>' title="MENU PRINCIPAL"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
					</li>
					<li class="text-center">
		                <a href="<?php echo base_url().'index.php/admin/proy/mis_proyectos/'.$mod ?>" title="PROGRAMACION -> PROGRAMACI&Oacute;N FISICA"> <span class="menu-item-parent">PROGRAMACI&Oacute;N F&Iacute;SICA</span></a>
		            </li>
					<?php
					if($nro_fase==1){
					    for($i=0;$i<count($enlaces);$i++)
					    {
					    	?>
					    	 <li>
				              	<a href="#" >
				              		<i class="<?php echo $enlaces[$i]['o_image'];?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
				              	<ul >
				              	<?php
				                $submenu= $this->menu_modelo->get_Modulos_sub($enlaces[$i]['o_child']);
					      		foreach($submenu as $row) {
				                ?>
				                <li><a href="<?php echo base_url($row['o_url'])."/".$mod."/".$id_f[0]['id']."/".$id_f[0]['proy_id']; ?>"><?php echo $row['o_titulo']; ?></a></li>
				                <?php } ?>
				                </ul>
				            </li>
					    	<?php
					    }
					}
					?>
				</ul>
			</nav>
			<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>
		</aside>

		<!-- MAIN PANEL -->
		<div id="main" role="main">
			<!-- RIBBON -->
			<div id="ribbon">

				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>

				<!-- breadcrumb -->
				<ol class="breadcrumb">
				<?php 
				if($mod==1)
				{
					?>
					<li><a href="<?php echo base_url().'index.php/admin/proy/list_proy' ?>" title="MIS PROYECTOS">Mis Operaciones</a></li><li><a href="<?php echo base_url().'index.php/admin/proy/prog/'.$mod.'/'.$proyecto[0]['proy_id'].'' ?>" title="DATOS FASE ETAPA">Datos Generales</a></li><li>Fase Etapa</li>
					<?php
				}
				elseif ($mod==4) 
				{
					?>
					<li><a href="<?php echo base_url().'index.php/admin/sgp/list_proy' ?>" title="MIS PROYECTOS">Gerencia de Proyectos</a></li><li><a href="<?php echo base_url().'index.php/admin/proy/prog//'.$mod.'/'.$proyecto[0]['proy_id'].'' ?>" title="DATOS FASE ETAPA">Datos Generales</a></li><li>Fase Etapa</li>	
					<?php
				}
				?>
					
				</ol>
			</div>
			<!-- END RIBBON -->
					<?php
						$attributes = array('class' => 'form-horizontal', 'id' => 'formulario','name' =>'formulario','enctype' => 'multipart/form-data');
						echo validation_errors();
						echo form_open('admin/proy/fase_update', $attributes);
					?>
			<!-- MAIN CONTENT -->
			<div id="content">
				<div class="row">
					<nav role="navigation" class="navbar navbar-default navbar-inverse">
				        <div class="navbar-header">
				            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
				                <span class="sr-only">Toggle navigation</span>
				                <span class="icon-bar"></span>
				                <span class="icon-bar"></span>
				                <span class="icon-bar"></span>
				            </button>
				        </div>
				 
				        <div id="navbarCollapse" class="collapse navbar-collapse">
				           <ul class="nav navbar-nav">
				                <li><a href="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/1' ?>"><font size="2">&nbsp;DATOS GENERALES&nbsp;</font></a></li>
				                <?php
		                          if($proyecto[0]['tp_id']==1 || $proyecto[0]['tp_id']==2 || $proyecto[0]['tp_id']==3)
		                          {
		                              ?>
		                          <li><a href="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/10' ?>"><font size="2">&nbsp;METAS&nbsp;</font></a></li>
		                              <?php
		                          }
		                        ?>
				                <li><a href="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/2' ?>"><font size="2">&nbsp;RESPONSABLES&nbsp;</font></a></li>
				                <li><a href="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/3' ?>"><font size="2">&nbsp;CLASIFICACI&Oacute;N&nbsp;</font></a></li>
				                <li><a href="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/4?qRegistro='.$proyecto[0]['proy_id'].'' ?>"><font size="2">&nbsp;LOCALIZACI&Oacute;N&nbsp;</font></a></li>
				                
				                <?php
                                    if($proyecto[0]['tp_id']==1 || $proyecto[0]['tp_id']==2 || $proyecto[0]['tp_id']==3)
                                    {
                                        ?>
                                    <li><a href="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/5' ?>"><font size="2">&nbsp;OBJETIVOS&nbsp;</font></a></li>
                                        <?php
                                    }
                                ?>
				                <li><a href="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/8' ?>"><font size="2">&nbsp;ARCHIVOS&nbsp;</font></a></li>
				            	<?php
			                        if($proyecto[0]['tp_id']==1){
			                          ?>
			                          <li><li class="active"><a href="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/9' ?>" title="FASE ETAPA COMPONENTE"><i class="glyphicon glyphicon-ok"></i><font size="2">&nbsp;FASE / ETAPA&nbsp;</font></a></li>
			                          <?php
			                        }
			                        elseif ($proyecto[0]['tp_id']==2 || $proyecto[0]['tp_id']==3 || $proyecto[0]['tp_id']==4) {
			                          if($mod==1)
			                          {
				                          ?>
				                          <li><li class="active"><a href="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/9' ?>" title="MONTO PRESUPUESTO"><i class="glyphicon glyphicon-ok"></i><font size="2">&nbsp;PRESUPUESTO&nbsp;</font></a></li>
				                          <?php
			                          }
			                          elseif($mod==4)
			                          {
			                          	  ?>
				                          <li><li class="active"><a href="<?php echo base_url().'index.php/admin/sgp/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/9' ?>" title="MONTO PRESUPUESTO"><i class="glyphicon glyphicon-ok"></i><font size="2">&nbsp;PRESUPUESTO&nbsp;</font></a></li>
				                          <?php
			                          }
			                        }
			                      ?>
				            </ul>
				        </div>
					</nav>
				</div>
				<div class="well">
					<u><b>PROYECTO</b></u> :		<?php echo $proyecto[0]['proy_nombre'] ?><br>
					<u><b>GESTI&Oacute;N ACTUAL</b></u> : <?php echo $this->session->userdata("gestion");?>
				</div>

				<section id="widget-grid" class="">
					<!-- row -->
					<div class="row">
							<!-- Widget ID (each widget will need unique ID)-->
							<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								
								<div class="jarviswidget jarviswidget-color-darken" >
									<header>
										<span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
										<h2 class="font-md"><strong>FASE ETAPA (Modificar)</strong></h2>				
									</header>
										<!-- widget content -->
									<div class="widget-body">
										<div class="row">
											<form id="formulario"  name="formulario" novalidate="novalidate" method="post">
												<div id="bootstrap-wizard-1" class="col-sm-12">
													<div class="well">
																<input class="form-control" type="hidden" name="id" value="<?php echo $fase_proyecto[0]['id']?>">
																<input class="form-control" type="hidden" name="id_p" value="<?php echo $fase_proyecto[0]['proy_id']?>">	
																<input class="form-control" type="hidden" name="tp" id="tp" value="1">
																<input class="form-control" type="hidden" name="nro" id="nro" value="1">
																<input class="form-control" type="hidden" name="mod" id="mod" value="<?php echo $mod;?>">	
																<div class="row">
																	<div class="col-sm-4">
																		<div class="form-group">
																			<label><font size="1"><b>FASE</b></font></label>
																			<select name="fase" id="fase" class="form-control" >
																				<?php 
																                    foreach($fases as $row)
																                    {
																                    	if($row['fas_id']==$fase_proyecto[0]['fas_id'])
																                    	{
																                    		?>
																		                     <option value="<?php echo $row['fas_id']; ?>" selected><?php echo $row['fas_fase']; ?></option>
																		                    <?php 
																                    	}
																                    	else
																                    	{
																                    		?>
																		                     <option value="<?php echo $row['fas_id']; ?>"><?php echo $row['fas_fase']; ?></option>
																		                    <?php 
																                    	}	
																                    }
																                ?>
																			</select>
																		</div>
																	</div>

																	<div class="col-sm-4">
																		<div class="form-group">
																			<label><font size="1"><b>ETAPA</b></font></label>
																			<select class="form-control" id="etapas" name="etapas"> 
						                                                  	<option value="<?php echo $fase_proyecto[0]['eta_id'] ?>"><?php echo $fase_proyecto[0]['etapa'] ?></option>
						                                                  	</select>
																		</div>
																	</div>

																	<div class="col-sm-4">
																		<div class="form-group">
																			<label><font size="1"><b>UNIDAD ORGANIZACIONAL</b></font></label>
																			<select class="form-control" id="uni_ejec" name="uni_ejec">
								                                                <?php 
																                    foreach($unidad_org as $row)
																                    {
																                    	if($row['uni_id']==$f_top[0]['uni_ejec'])
																                    	{
																                    		?>
																		                     <option value="<?php echo $row['uni_id']; ?>" selected><?php echo $row['uni_unidad']; ?></option>
																		                    <?php 
																                    	}
																                    	else
																                    	{
																                    		?>
																		                     <option value="<?php echo $row['uni_id']; ?>"><?php echo $row['uni_unidad']; ?></option>
																		                    <?php 
																                    	}	
																                    }
																                ?>      
								                                            </select>
																		</div>
																	</div>
																</div>

																<div class="row">
																	<div class="col-sm-12">
																		<div class="form-group">
																			<label><font size="1"><b>DESCRIPCION DE LA ETAPA</b></font></label>
																			<textarea rows="4"  name="desc" id="desc" class="form-control" style="width:100%;" ><?php echo $fase_proyecto[0]['descripcion'] ?></textarea> 
																		</div>
																	</div>
																	
																</div>

																<div class="row">
																		<div class="col-sm-6">
																			<div class="form-group">
																				<label><font size="1"><b>FECHA INICIAL </b></font></label>
																				<div class="input-group">
																						<input type="text" name="f_inicio" id="f_inicio" value="<?php echo date('d/m/Y',strtotime($fase_proyecto[0]['inicio'])) ?>" placeholder="Seleccione Fecha inicial" class="form-control datepicker"  data-dateformat="dd/mm/yy" title="Seleccione fecha Inicial del Proyecto">
																						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>

																				</div>
																			</div>
																		</div>

																		<div class="col-sm-6">
																			<div class="form-group">
																				<label><font size="1"><b>FECHA DE CONCLUSI&Oacute;N </b></font></label>
																				<div class="input-group">
																						<input type="text" name="f_final" id="f_final" value="<?php echo date('d/m/Y',strtotime($fase_proyecto[0]['final'])) ?>"placeholder="Seleccione Fecha inicial" class="form-control datepicker"  data-dateformat="dd/mm/yy" title="Seleccione fecha de conclusión del Proyecto">
																						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>

																				</div>
																			</div>
																		</div>
																</div>

																<div class="row">
																	<div class="col-sm-4">
																		<div class="form-group">
																			<label><font size="1"><b>FORMA DE EJECUCION</b></font></label>
																			<select class="form-control" id="f_ejec" name="f_ejec">
						                                                        <option value="<?php echo $fase_proyecto[0]['pfec_ejecucion'] ?>"><?php echo $fase_proyecto[0]['ejec'] ?></option>
						                                                        <option value="">------------------------</option>
						                                                        <option value="1">DIRECTA</option>
						                                                        <option value="2">DELEGADA</option>
						                                                  	</select>
																		</div>
																	</div>
																	<div class="col-sm-4">
																		<div class="form-group">
																			<label><font size="1"><b>PRESUPUESTO FASE/ETAPA TOTAL</b></font></label>
																			<input class="form-control" type="number" name="monto_total" id="monto_total" value="<?php echo round($fase_proyecto[0]['pfec_ptto_fase'],1) ?>" placeholder="0" onkeypress="if (this.value.length < 10) { return soloNumeros(event);}else{return false; }"  onpaste="return false">
																		</div>
																	</div>
																	<div class="col-sm-4">
																		<div class="form-group">
																			<label><font size="1"><b>TOTAL FASE/ETAPA EJECUCI&Oacute;N</b></font></label>
																			<input class="form-control" type="number" value="<?php echo round($fase_proyecto[0]['pfec_ptto_fase_e'],1) ?>" disabled="true">
																			<input class="form-control" type="hidden" name="monto_ref" id="monto_ref" value="<?php echo round($fase_proyecto[0]['pfec_ptto_fase_e'],1) ?>">
																		</div>
																	</div>
																</div>
																

															</div> <!-- end well -->
															<div class="form-actions">
																<a href="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/9' ?>" class="btn btn-lg btn-default"> CANCELAR </a>
																<input type="button" value="SIGUIENTE" class="btn btn-primary btn-lg" onclick="valida_envia()" title="GUARDAR INFORMACION Y VER PRESUPUESTOS FASE">
															</div>
												</div>
											</form>
										</div>
				
									</div>
									<!-- end widget content -->
								</div>
								<!-- end widget div -->
							</div>
							<!-- end widget -->
					</div>
				
					<!-- end row -->
				</section>
				<!-- end widget grid -->					
			</div>
			<!-- END MAIN CONTENT -->

		</div>
		<!-- END MAIN PANEL -->

		<!-- PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->

		<!--================================================== -->
		<script>
            function valida_envia()
            { 
               
                if (document.formulario.fase.value=="") /////// Fase
                  { 
                      alert("Seleccione campo FASE") 
                      document.formulario.fase.focus() 
                      return 0; 
                  }

                if (document.formulario.etapas.value=="") /////// Etapas
                  { 
                      alert("Ingrese Campo ETAPAS") 
                      document.formulario.etapas.focus() 
                      return 0; 
                  }

				if (document.formulario.f_ejec.value=="") /////// Forma de ejecucion
                  { 
                      alert("Seleccione FORMA DE EJECUCION") 
                      document.formulario.f_ejec.focus() 
                      return 0; 
                  }

                  if (document.formulario.monto_total.value=="") /////// monto total
                  { 
                      alert("Registre Presupuesto Total") 
                      document.formulario.monto_total.focus() 
                      return 0; 
                  }

                  var OK = confirm("MODIFICACION CORRECTA...GUARDAR INFORMACION ?");
		            if (OK) {
		                document.formulario.submit(); 
		            }
            }
          </script>
		<!--================================================== -->

		<script>
			if (!window.jQuery) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-2.0.2.min.js"><\/script>');
			}
		</script>

		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>

		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url(); ?>assets/js/session_time/jquery-idletimer.js"></script>
		<script src = "<?php echo base_url(); ?>mis_js/control_session.js"></script>
		<script src="<?php echo base_url();?>/assets/js/app.config.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/mis_js/validacion_form.js"></script>
		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url();?>/assets/js/bootstrap/bootstrap.min.js"></script>
		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url();?>/assets/js/notification/SmartNotification.min.js"></script>
		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url();?>/assets/js/smartwidgets/jarvis.widget.min.js"></script>
		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url();?>/assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<!-- SPARKLINES -->
		<script src="<?php echo base_url();?>/assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/select2/select2.min.js"></script>
		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
		<!-- browser msie issue fix -->
		<script src="<?php echo base_url();?>/assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url();?>/assets/js/plugin/fastclick/fastclick.min.js"></script>
		<!-- Demo purpose only -->
		<script src="<?php echo base_url();?>/assets/js/demo.min.js"></script>
		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url();?>/assets/js/app.min.js"></script>
		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<!-- Voice command : plugin -->
		<script src="<?php echo base_url();?>/assets/js/speech/voicecommand.min.js"></script>
		<!-- PAGE RELATED PLUGIN(S) -->
		<script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
		<script src="<?php echo base_url();?>/assets/js/plugin/fuelux/wizard/wizard.min.js"></script>
		<script type="text/javascript">
		$(document).ready(function() {
			
			pageSetUp();

			$("#fase").change(function () {
                $("#fase option:selected").each(function () {
                elegido=$(this).val();
                $.post("<?php echo base_url(); ?>index.php/admin/combo_fase_etapas", { elegido: elegido }, function(data){
                $("#etapas").html(data);
                });     
            });
            });    
		})

		</script>
	</body>
</html>
