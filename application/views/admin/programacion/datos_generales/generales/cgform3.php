<!DOCTYPE html>
<html lang="en-us">
    <head>
        <meta charset="utf-8">
        <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

        <title><?php echo $this->session->userdata('name')?></title>
        <meta name="description" content="">
        <meta name="author" content="">
            
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <!-- Basic Styles -->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
        <!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css">
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
        <!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
        <!-- FAVICONS -->
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
        <!--///////////////css-->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
        <!--//////////////fin css-->
        <style type="text/css">
            aside{background: #05678B;}
        </style>
    </head>
    <body class="">
        <!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->

        <!-- HEADER -->
        <header id="header">
            <div id="logo-group">
                <!-- <span id="logo"> <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="SmartAdmin"> </span> -->
            </div>

            <!-- pulled right: nav area -->
            <div class="pull-right">
                <!-- collapse menu button -->
                <div id="hide-menu" class="btn-header pull-right">
                    <span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
                </div>
                <!-- end collapse menu -->
                <!-- logout button -->
                <div id="logout" class="btn-header transparent pull-right">
                    <span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Sign Out" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
                </div>
                <!-- end logout button -->
                <!-- search mobile button (this is hidden till mobile view port) -->
                <div id="search-mobile" class="btn-header transparent pull-right">
                    <span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
                </div>
                <!-- end search mobile button -->
                <!-- fullscreen button -->
                <div id="fullscreen" class="btn-header transparent pull-right">
                    <span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen"><i class="fa fa-arrows-alt"></i></a> </span>
                </div>
                <!-- end fullscreen button -->
            </div>
            <!-- end pulled right: nav area -->
        </header>
        <!-- END HEADER -->

        <!-- Left panel : Navigation area -->
        <aside id="left-panel">
            <!-- User info -->
            <div class="login-info">
                <span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
                    <a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                        <span>
                            <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                        </span>
                    </a> 
                </span>
            </div>

            <nav>
                <ul>
                    <li>
                        <a href='<?php echo site_url("admin").'/dashboard'; ?>' title="MENU PRINCIPAL"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
                    </li>
                    <li class="text-center">
                        <a href="<?php echo base_url().'index.php/admin/proy/mis_proyectos/'.$mod ?>" title="PROGRAMACION -> PROGRAMACI&Oacute;N FISICA"> <span class="menu-item-parent">PROGRAMACI&Oacute;N F&Iacute;SICA</span></a>
                    </li>
                    <?php
                    if($nro_fase==1){
                        for($i=0;$i<count($enlaces);$i++)
                        {
                            ?>
                             <li>
                                <a href="#" >
                                    <i class="<?php echo $enlaces[$i]['o_image'];?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
                                <ul >
                                <?php
                                $submenu= $this->menu_modelo->get_Modulos_sub($enlaces[$i]['o_child']);
                                foreach($submenu as $row) {
                                ?>
                                <li><a href="<?php echo base_url($row['o_url'])."/".$mod."/".$id_f[0]['id']."/".$id_f[0]['proy_id']; ?>"><?php echo $row['o_titulo']; ?></a></li>
                                <?php } ?>
                                </ul>
                            </li>
                            <?php
                        }
                    }
                    ?>
                </ul>
            </nav>
            <span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>
        </aside>

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <span class="ribbon-button-alignment"> 
                    <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                        <i class="fa fa-refresh"></i>
                    </span> 
                </span>

                <!-- breadcrumb -->
                <ol class="breadcrumb">
                <?php 
                if($mod==1)
                {
                    ?>
                    <li><a href="<?php echo base_url().'index.php/admin/proy/list_proy' ?>" title="MIS PROYECTOS">Mis Operaciones</a></li><li><a href="<?php echo base_url().'index.php/admin/proy/prog/'.$mod.'/'.$proyecto[0]['proy_id'].'' ?>" title="DATOS GENERALES DEL PROYECTO">Datos Generales</a></li><li>Localizaci&oacute;n</li>
                    <?php
                }
                elseif ($mod==4) 
                {
                    ?>
                    <li><a href="<?php echo base_url().'index.php/admin/sgp/list_proy' ?>" title="MIS PROYECTOS">Gerencia de Proyectos</a></li><li><a href="<?php echo base_url().'index.php/admin/proy/prog/'.$mod.'/'.$proyecto[0]['proy_id'].'' ?>" title="DATOS GENERALES DEL PROYECTO">Datos Generales</a></li><li>Localizaci&oacute;n</li>
                    <?php
                }
                ?>
                </ol>
            </div>
            <!-- END RIBBON -->

      <!-- MAIN CONTENT -->
      <div id="content">

        <div class="row">
          <nav role="navigation" class="navbar navbar-default navbar-inverse">
                <div class="navbar-header">
                    <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
         
                 <div id="navbarCollapse" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/1' ?>"><font size="2">&nbsp;DATOS GENERALES&nbsp;</font></a></li>
                        <?php
                          if($proyecto[0]['tp_id']==1 || $proyecto[0]['tp_id']==2 || $proyecto[0]['tp_id']==3)
                          {
                              ?>
                          <li><a href="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/10' ?>"><font size="2">&nbsp;METAS&nbsp;</font></a></li>
                              <?php
                          }
                        ?>
                        <li><a href="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/2' ?>"><font size="2">&nbsp;RESPONSABLES&nbsp;</font></a></li>
                        <li><a href="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/3' ?>"><font size="2">&nbsp;CLASIFICACI&Oacute;N&nbsp;</font></a></li>
                        <li class="active"><a href="#"><i class="glyphicon glyphicon-ok"></i><font size="2">&nbsp;LOCALIZACI&Oacute;N&nbsp;</font></a></li>
                        <?php
                            if($proyecto[0]['tp_id']==1 || $proyecto[0]['tp_id']==2 || $proyecto[0]['tp_id']==3)
                            {
                                ?>
                            <li><a href="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/5' ?>"><font size="2">&nbsp;OBJETIVOS&nbsp;</font></a></li>
                                <?php
                            }
                        ?>
                        <li><a href="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/7' ?>"><font size="2">&nbsp;RESUMEN T&Eacute;CNICO&nbsp;</font></a></li>
                        
                        <?php
                              if($proyecto[0]['tp_id']==1){
                                ?>
                                <li><a href="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/9' ?>" title="FASE ETAPA COMPONENTE"><font size="2">&nbsp;FASE / ETAPA&nbsp;</font></a></li>
                                <?php
                              }
                              elseif ($proyecto[0]['tp_id']==2 || $proyecto[0]['tp_id']==3 || $proyecto[0]['tp_id']==4) {
                                if($mod==1)
                                {
                                  ?>
                                  <li><a href="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/9' ?>" title="MONTO PRESUPUESTO"><font size="2">&nbsp;PRESUPUESTO&nbsp;</font></a></li>
                                  <?php
                                }
                                elseif($mod==4)
                                {
                                    ?>
                                  <li><a href="<?php echo base_url().'index.php/admin/sgp/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/9' ?>" title="MONTO PRESUPUESTO"><font size="2">&nbsp;PRESUPUESTO&nbsp;</font></a></li>
                                  <?php
                                }
                              }
                      ?>
                    </ul>
                </div>
          </nav>
        </div>

          <div class="row">
              <!-- Widget ID (each widget will need unique ID)-->
              <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <!-- widget div-->

<?php 
function saiDepartamentos()
{ 
  extract($link);


  $sSQL="SELECT dep_id, dep_departamento FROM _Departamentos WHERE dep_estado = 1 ORDER BY dep_departamento ASC";
  
  $MatrixBD=pg_query($sSQL);
  $txtmat=pg_fetch_array($MatrixBD);
  if (gettype($txtmat)==8)
  {

    $cgregqty=-1;
  }
    else
  {

    $cgregqty = pg_num_rows($MatrixBD);
  } 

// Los Registros
  $cgr=0;
  $varX=" ";

  while($rT=pg_fetch_object($MatrixBD)){
    $varX.="<option value=\"".trim($rT->dep_id)."\">".trim($rT->dep_departamento)."</option>"; 

  }
  

  $function_ret=$varX;
  return $function_ret;
} 

//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

function saiProyDepartamentos($vProyecto)
{

  $sSQL="";
  $sSQL=$sSQL." SELECT _ProyectosDepartamentos.pd_id, _Departamentos.dep_departamento";
  $sSQL=$sSQL." FROM _Departamentos RIGHT OUTER JOIN";
  $sSQL=$sSQL." _ProyectosDepartamentos ON _Departamentos.dep_id = _ProyectosDepartamentos.dep_id";
  $sSQL=$sSQL." WHERE (_ProyectosDepartamentos.proy_id = ".$vProyecto.")";
  $sSQL=$sSQL." ORDER BY _Departamentos.dep_departamento";
  
  $MatrixBD=pg_query($sSQL);
  
  $cgregqty=pg_num_rows($MatrixBD);
  $varX="";
  while($txtmat=pg_fetch_array($MatrixBD)){
  if (gettype($txtmat)==8)
  {

    $cgregqty=-1;
  }
    else
  {

    $cgregqty=pg_num_rows($MatrixBD);
  } 

 
  
    $varY=$txtmat[0];
    $varX.="<a href=\"?qRegistro=".$vProyecto."&cgcmd=cgDelDepartamento&qRegistro_dep=".$varY."\"><img src=\"".base_url()."/assets/Iconos/page_delete.png\" width=\"16\" height=\"16\" border=\"0\" align=\"absmiddle\" alt=\"Borrar\" title=\"Borrar\"></a> ".trim($txtmat[1])."<BR>";
  }

  $function_ret=$varX;
  return $function_ret;
} 

//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

function funProyDepartamentos($vProyecto)
{
  extract($GLOBALS);


  $sSQL="";
  $sSQL=$sSQL." SELECT _ProyectosDepartamentos.pd_id, _Departamentos.dep_departamento";
  
  
  $sSQL=$sSQL." FROM _Departamentos RIGHT OUTER JOIN";
  $sSQL=$sSQL." _ProyectosDepartamentos ON _Departamentos.dep_id = _ProyectosDepartamentos.dep_id";
  $sSQL=$sSQL." WHERE (_ProyectosDepartamentos.proy_id = ".$vProyecto.")";
  $sSQL=$sSQL." ORDER BY _Departamentos.dep_departamento";
  
  $MatrixBD=pg_query($sSQL);
  $txtmat=pg_fetch_object($MatrixBD);

  if (gettype($txtmat)==8)
  {

    $cgregqty=-1;
  }
    else
  {

    $cgregqty=count($txtmat);
  } 

  $varX="";
  for ($cgr=0; $cgr<=$cgregqty; $cgr=$cgr+1)
  {
    $varY=$txtmat[0][$cgr];
    if (!doubleval($varY)>0)
    {
      $varY=0;
    } 
    $varX=$VarX."<img src=\"".base_url()."/assets/Iconos/bullet_gray.png\" width=\"16\" height=\"16\" border=\"0\" align=\"absmiddle\">".trim($txtmat[1])."<BR>";

  }

  $function_ret=$varX;
  return $function_ret;
} 

//'''''''''''''''''''''''''''''''''''''''MUESTRA LISTADO DE PROVINCIAS REGISTRADOS''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

function saiProyProvincias($vProyecto)
{
  extract($GLOBALS);


  $sSQL="";
 
  
  $sSQL=$sSQL." SELECT _ProyectosProvincias.pp_id, _Provincias.prov_provincia,(SELECT _Departamentos.dep_departamento FROM _Departamentos WHERE _Departamentos.dep_id = _Provincias.dep_id)  as dep_departamento ";
  $sSQL=$sSQL." FROM _Provincias RIGHT OUTER JOIN";
  $sSQL=$sSQL." _ProyectosProvincias ON _Provincias.prov_id = _ProyectosProvincias.prov_id";
  $sSQL=$sSQL." WHERE (_ProyectosProvincias.proy_id = ".$vProyecto.")";
  $sSQL=$sSQL." ORDER BY _Provincias.prov_provincia";
 
  $MatrixBD=pg_query($sSQL);
   $varX="";
  
  while($txtmat=pg_fetch_array($MatrixBD)){
  if (gettype($txtmat)==8)
  {

    $cgregqty=-1;
  }
    else
  {

    $cgregqty=count($txtmat);
  } 


   $varY=$txtmat[0];
    if (!doubleval($varY)>0)
    {
      $varY=0;
    } 
    $varX.="<a href=\"?qRegistro=".$vProyecto."&cgcmd=cgDelProvincia&qRegistro_prov=".$varY."\"><img src=\"".base_url()."/assets/Iconos/page_delete.png\" width=\"16\" height=\"16\" border=\"0\" align=\"absmiddle\" alt=\"Borrar\" title=\"Borrar\"></a> ".$txtmat[2].' - '.trim($txtmat[1])."<BR>";
  }

  $function_ret=$varX;
  return $function_ret;
} 

//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

function funProyProvincias($vProyecto)
{
  extract($GLOBALS);

  $varX="";

  $sSQL="";
  $sSQL=$sSQL." SELECT _ProyectosProvincias.pp_id, _Provincias.prov_provincia,(SELECT _Departamentos.dep_departamento FROM _Departamentos WHERE _Departamentos.dep_id = _Provincias.dep_id)  as dep_departamento";
  $sSQL=$sSQL." FROM _Provincias RIGHT OUTER JOIN";
  $sSQL=$sSQL." _ProyectosProvincias ON _Provincias.prov_id = _ProyectosProvincias.prov_id";
  $sSQL=$sSQL." WHERE (_ProyectosProvincias.proy_id = ".$vProyecto.")";
  $sSQL=$sSQL." ORDER BY _Provincias.prov_provincia";
  
  $MatrixBD=pg_query($sSQL);
  $txtmat=pg_fetch_array($MatrixBD);
  if (gettype($txtmat)==8)
  {

    $cgregqty=-1;
  }
    else
  {

    $cgregqty=count($txtmat);
  } 

  
  for ($cgr=0; $cgr<=$cgregqty; $cgr=$cgr+1)
  {    $varY=$txtmat[0][$cgr];
    if (!doubleval($varY)>0)
    {
      $varY=0;
    } 
    $varX=$VarX."<img src=\"".base_url()."/assets/Iconos/bullet_gray.png\" width=\"16\" height=\"16\" border=\"0\" align=\"absmiddle\">".trim($txtmat[1])."<BR>";

  }

  $function_ret=$varX;
  return $function_ret;
} 


//'''''''''''''''''''''''''''''''''''LISTADO PARA EL COMBO DE PROVINCIAS''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

function saiProvincias($vProyecto)
{
  extract($GLOBALS);



  if (!$vProyecto>0)
  {
    $vProyecto=0;
  } 
  $sSQL="";
  $sSQL=$sSQL." SELECT _Provincias.prov_id, _Provincias.prov_provincia,(SELECT _Departamentos.dep_departamento FROM _Departamentos WHERE _Departamentos.dep_id = _Provincias.dep_id)  as dep_departamento";
  $sSQL=$sSQL." FROM _Provincias INNER JOIN";
  $sSQL=$sSQL." _ProyectosDepartamentos ON _Provincias.dep_id = _ProyectosDepartamentos.dep_id";
  $sSQL=$sSQL." WHERE (_ProyectosDepartamentos.proy_id = ".$vProyecto.")";
  $sSQL=$sSQL." ORDER BY 3";

  $MatrixBD=pg_query($sSQL);
   $varX="";
  
  while($txtmat=pg_fetch_array($MatrixBD)){

  if (gettype($txtmat)==8)
  {

    $cgregqty=-1;
  }
    else
  {

    $cgregqty=count($txtmat);
  } 

   
    $varX.="<option value=\"".trim($txtmat[0])."\">".$txtmat[2].' - '.trim($txtmat[1] )."</option>";

 }
  $function_ret=$varX;
  return $function_ret;
} 

//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

function saiProyMunicipios($vProyecto)
{
  extract($GLOBALS);


  $sSQL="";
  $sSQL=$sSQL." SELECT _ProyectosMunicipios.pm_id, _Municipios.muni_municipio, _ProyectosMunicipios.pm_pondera,(SELECT _regiones.reg_region FROM _regiones WHERE _regiones.reg_id = _Municipios.reg_id) as region,(SELECT _Provincias.prov_provincia FROM _Provincias WHERE _Provincias.prov_id = _Municipios.prov_id) as prov_provincia,_Municipios.muni_poblacion_hombres,_Municipios.muni_polacion_mujeres ";
  $sSQL=$sSQL." FROM _Municipios RIGHT OUTER JOIN";
  $sSQL=$sSQL." _ProyectosMunicipios ON _Municipios.muni_id = _ProyectosMunicipios.muni_id";
  $sSQL=$sSQL." WHERE (_ProyectosMunicipios.proy_id = ".$vProyecto.")";
  $sSQL=$sSQL." ORDER BY _Municipios.muni_municipio";
  
  $MatrixBD=pg_query($sSQL);
   $varX="";
  
  while($txtmat=pg_fetch_array($MatrixBD)){
  if (gettype($txtmat)==8)
  {

    $cgregqty=-1;
  }
    else
  {

    $cgregqty=count($txtmat);
  } 


    $varY=$txtmat[0];

    $varX.="<a href=\"?qRegistro=".$vProyecto."&cgcmd=cgDelMunicipio&qRegistro_mun=".$varY."\"><img src=\"".base_url()."/assets/Iconos/page_delete.png\" width=\"16\" height=\"16\" border=\"0\" alt=\"Borrar\" title=\"Borrar\" align=\"absmiddle\"></a><img src=\"".base_url()."/assets/Iconos/page_edit.png\" width=\"16\" height=\"16\" border=\"0\" style=\"cursor:pointer\" align=\"absmiddle\" alt=\"Editar Ponderación\"  title=\"Editar Ponderación\" onclick=\"javascript:vV1=jsDato('Ponderación del Municipio','0');NewWindow('?qRegistro=".$vProyecto."&cgcmd=cmd_UpdMunicipio&qRegistro_mun_pnd=".$varY."&qPonderacion='+vV1,'_self',0,0,0)\"> [".number_format($txtmat[2],2)." %]<br> Region : ".$txtmat[3] ."<br> Provincia : ".$txtmat[4] ."<br> Municipio : ".$txtmat[1]."<br> Poblacion Masculina : ".$txtmat[5]."<br> Poblacion Femenina : ".$txtmat[6]."<BR>";
   
  }
  $function_ret=$varX;
  return $function_ret;
} 

//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

function funProyMunicipios($vProyecto)
{
  extract($GLOBALS);


  $sSQL="";
  $sSQL=$sSQL." SELECT _ProyectosMunicipios.pm_id, _Municipios.muni_municipio, _ProyectosMunicipios.pm_pondera";
  $sSQL=$sSQL." FROM _Municipios RIGHT OUTER JOIN";
  $sSQL=$sSQL." _ProyectosMunicipios ON _Municipios.muni_id = _ProyectosMunicipios.muni_id";
  $sSQL=$sSQL." WHERE (_ProyectosMunicipios.proy_id = ".$vProyecto.")";
  $sSQL=$sSQL." ORDER BY _Municipios.muni_municipio";
  $txtmat=$MatrixBD[$sSQL][1];
  if (gettype($txtmat)==8)
  {

    $cgregqty=-1;
  }
    else
  {

    $cgregqty=count($txtmat);
  } 

  $varX="";
  for ($cgr=0; $cgr<=$cgregqty; $cgr=$cgr+1)
  {
    $varY=$txtmat[0][$cgr];
    if (!doubleval($varY)>0)
    {
      $varY=0;
    } 
    $varX=$VarX."<img src=\"".base_url()."/assets/Iconos/bullet_gray.png\" width=\"16\" height=\"16\" border=\"0\" align=\"absmiddle\"> [".number_format($txtmat[2][$cgr],2)." %] ".$txtmat[1][$cgr]."<BR>";

  }

  $function_ret=$varX;
  return $function_ret;
} 

//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

function saiMunicipios($vProyecto)
{
  extract($GLOBALS);


  if (!$vProyecto>0)
  {
    $vProyecto=0;
  } 

  $sSQL="";
  $sSQL=$sSQL." SELECT _Municipios.muni_id, _Municipios.muni_municipio,(SELECT _Provincias.prov_provincia FROM _Provincias WHERE _Provincias.prov_id = _Municipios.prov_id) as prov_provincia  ";
  $sSQL=$sSQL." FROM _Municipios INNER JOIN";
  $sSQL=$sSQL." _ProyectosProvincias ON _Municipios.prov_id = _ProyectosProvincias.prov_id";
  $sSQL=$sSQL." WHERE (_ProyectosProvincias.proy_id = ".$vProyecto.")";
  
  $sSQL=$sSQL." ORDER BY 3";
  
  $MatrixBD=pg_query($sSQL);
   $varX="none";
  //$txtmat=pg_fetch_array($MatrixBD);
  while($txtmat=pg_fetch_array($MatrixBD)){
  if (gettype($txtmat)==8)
  {

    $cgregqty=-1;
  }
    else
  {

    $cgregqty=count($txtmat);
  } 

//  
    $varX.="<option value=\"".trim($txtmat[0])."\">".$txtmat[2].' - '.trim($txtmat[1])."</option>";

  }
  $function_ret=$varX;
  return $function_ret;
} 

//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

function saiProyCantones($vProyecto)
{
  extract($GLOBALS);


  $sSQL="";
  $sSQL=$sSQL." SELECT _ProyectosCantones.pc_id, _Cantones.can_canton";
  $sSQL=$sSQL." FROM _Cantones RIGHT OUTER JOIN";
  $sSQL=$sSQL." _ProyectosCantones ON _Cantones.can_id = _ProyectosCantones.can_id";
  $sSQL=$sSQL." WHERE (_ProyectosCantones.proy_id = ".$vProyecto.")";
  $sSQL=$sSQL." ORDER BY _Cantones.can_canton";
  
   $MatrixBD=pg_query($sSQL);
   $varX="";
  
  while($txtmat=pg_fetch_array($MatrixBD)){
  if (gettype($txtmat)==8)
  {

    $cgregqty=-1;
  }
    else
  {

    $cgregqty=count($txtmat);
  } 

// Los Registros

    $varY=$txtmat[0];

    $varX.="<a href=\"?qRegistro=".$vProyecto."&cgcmd=cgDelCanton&qRegistro_canton=".$varY."\"><img src=\"".base_url()."/assets/Iconos/page_delete.png\" width=\"16\" height=\"16\" border=\"0\" align=\"absmiddle\" alt=\"Borrar\" title=\"Borrar\"></a> ".trim($txtmat[1] )."<BR>";

 
  }
  $function_ret=$varX;
  return $function_ret;
} 

//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

function funProyCantones($vProyecto)
{
  extract($GLOBALS);


  $sSQL="";
  $sSQL=$sSQL." SELECT _ProyectosCantones.pc_id, _Cantones.can_canton";
  $sSQL=$sSQL." FROM _Cantones RIGHT OUTER JOIN";
  $sSQL=$sSQL." _ProyectosCantones ON _Cantones.can_id = _ProyectosCantones.can_id";
  $sSQL=$sSQL." WHERE (_ProyectosCantones.proy_id = ".$vProyecto.")";
  $sSQL=$sSQL." ORDER BY _Cantones.can_canton";
  $MatrixBD=pg_query($sSQL);
   $varX="";
  
  while($txtmat=pg_fetch_array($MatrixBD)){
  if (gettype($txtmat)==8)
  {

    $cgregqty=-1;
  }
    else
  {

    $cgregqty=count($txtmat);
  } 

// Los Registros
  $cgr=0;
  
    $varY=$txtmat[0];
    if (!doubleval($varY)>0)
    {
      $varY=0;
    } 
    $varX.="<img src=\"".base_url()."/assets/Iconos/bullet_gray.png\" width=\"16\" height=\"16\" border=\"0\" align=\"absmiddle\"> ".trim($txtmat[1])."<BR>";

  }

  $function_ret=$varX;
  return $function_ret;
} 

//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

function saiCantones($vProyecto)
{
  extract($GLOBALS);


  if (!$vProyecto>0)
  {
    $vProyecto=0;
  } 

  $sSQL="";
  $sSQL=$sSQL." SELECT _Cantones.can_id, _Cantones.can_canton";
  $sSQL=$sSQL." FROM _ProyectosMunicipios INNER JOIN";
  $sSQL=$sSQL." _Cantones ON _ProyectosMunicipios.muni_id = _Cantones.muni_id";
  $sSQL=$sSQL." WHERE (_ProyectosMunicipios.proy_id = ".$vProyecto.")";
  $sSQL=$sSQL." ORDER BY _Cantones.can_canton";
  $MatrixBD=pg_query($sSQL);
   $varX="";
  
  while($txtmat=pg_fetch_array($MatrixBD)){
  if (gettype($txtmat)==8)
  {

    $cgregqty=-1;
  }
    else
  {

    $cgregqty=count($txtmat);
  } 

 
    $varX.="<option value=\"".trim($txtmat[0])."\">".trim($txtmat[1] )."</option>";

  }

  $function_ret=$varX;
  return $function_ret;
} 

//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

function saiMuniEnProy($vProyecto)
{
  extract($GLOBALS);


  if (!$vProyecto>0)
  {
    $vProyecto=0;
  } 

  $sSQL="";
  $sSQL=$sSQL." SELECT _Municipios.muni_id, _Municipios.muni_municipio";
  $sSQL=$sSQL." FROM _ProyectosMunicipios INNER JOIN";
  $sSQL=$sSQL." _Municipios ON _ProyectosMunicipios.muni_id = _Municipios.muni_id";
  $sSQL=$sSQL." WHERE (_ProyectosMunicipios.proy_id = ".$vProyecto.")";
  $sSQL=$sSQL." ORDER BY _Municipios.muni_municipio";

  $MatrixBD=pg_query($sSQL);
   $varX="";
  
  while($txtmat=pg_fetch_array($MatrixBD)){
  
    $varX.="<option value=\"".trim($txtmat[0])."\">".trim($txtmat[1])."</option>";
  
  }

  $function_ret=$varX;
  return $function_ret;
} 
?>

      <div id="content">

        <section id="widget-grid" class="">
          <!-- START ROW -->
          <div class="row">
            <!-- NEW COL START -->
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-darken" >
                      <header>
                        <span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
                        <h2 class="font-md"><strong>LOCALIZACI&Oacute;N</strong></h2>        
                      </header>
                      <div>
                        <!-- widget content -->
                        <div class="widget-body">
                        <div class="well">

    <!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
    <!-- JavaScript -->
<script src="//cdn.jsdelivr.net/alertifyjs/1.8.0/alertify.min.js"></script>

<!-- CSS -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.8.0/css/alertify.min.css"/>
<!-- Default theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.8.0/css/themes/default.min.css"/>
<!-- Semantic UI theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.8.0/css/themes/semantic.min.css"/>

    <script type="text/javascript">
    $(document).ready(function() {
      
      
    //alertify.alert('Ready!');

    })
          
    

    </script>
                  <!-- widget content -->
                  
<script language="javascript" type="text/javascript">   
var win = null;
function arriba(){
  scrollTo(0,0)
}

function MsgBox(elmensaje){
  alert(elmensaje)
}

function NewWindow(mypage,myname,w,h,scroll){
LeftPosition = (screen.width) ? (screen.width-w)/2 : 0;
TopPosition = (screen.height) ? (screen.height-h)/2 : 0;
settings = 'height='+h+',width='+w+',top='+TopPosition+',left='+LeftPosition+',scrollbars='+scroll+',resizable'
win = window.open(mypage,myname,settings)
if(win.window.focus){win.window.focus();}
}

function jsConfirmar(lapregunta){
  question = confirm(lapregunta)
  if (question != "0"){
    return true ;
  } else {
    return false
  }
}

function winconfirm(mymsg,mypage,myname,w,h,scroll){
  question = confirm(mymsg)
  if (question != "0"){
    NewWindow(mypage,myname,w,h,scroll)
  }
}

function disableGroup(formName, groupName, booleanDisabled) {
  for (var i=0; i<formName.elements.length; i++) {
    if (formName.elements[i].name == groupName) {
      formName.elements[i].disabled = booleanDisabled;
    }
  }
}

function jsRedondar(elmonto) {
// returns the amount in the .99 format
    elmonto -= 0;
    return (elmonto== Math.floor(elmonto)) ? elmonto + '.00' : (  (elmonto*10 == Math.floor(elmonto*10)) ? elmonto + '0' : elmonto);
}

function FormatNumber(num)
{
  num = num.toString().replace(/ |,/g,'');
  if(isNaN(num)) 
  num = "0";
  cents = Math.floor((num*100+0.5)%100);
  num = Math.floor((num*100+0.5)/100).toString();
  if(cents < 10) 
  cents = "0" + cents;
  for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
  num = num.substring(0,num.length-(4*i+3))+','+num.substring(num.length-(4*i+3));
  return (' ' + num + '.' + cents);
}

function cgMiReloj(){ 
    AhoraEs = new Date() 
    hora = AhoraEs.getHours() 
    minuto = AhoraEs.getMinutes() 
    segundo = AhoraEs.getSeconds() 

    mostrarhora = hora + ":" + minuto + ":" + segundo 

    this.parent.elreloj.value = mostrarhora

    setTimeout("cgMiReloj()",1.000) 
} 

function cDentro(qobjeto,qcolor) { 
    qobjeto.bgColor=qcolor;qobjeto.style.cursor="hand"; 
} 
function cFuera(qobjeto,qcolor) { 
    qobjeto.bgColor=qcolor;qobjeto.style.cursor="default"; 
} 

function visi(nr)
{
  if (document.layers)
  {
    vista = (document.layers[nr].visibility == 'hide') ? 'show' : 'hide'
    document.layers[nr].visibility = vista;
  }
  else if (document.all)
  {
    vista = (document.all[nr].style.visibility == 'hidden') ? 'visible' : 'hidden';
    document.all[nr].style.visibility = vista;
  }
  else if (document.getElementById)
  {
    vista = (document.getElementById(nr).style.visibility == 'hidden') ? 'visible' : 'hidden';
    document.getElementById(nr).style.visibility = vista;

  }
}

function js_show(nr)
{
  document.getElementById(nr).style.display = 'block';
}

function js_hide(nr)
{
  document.getElementById(nr).style.display = 'none';
}


function blocking(nr)
{
  if (document.layers)
  {
    current = (document.layers[nr].display == 'none') ? 'block' : 'none';
    document.layers[nr].display = current;
  }
  else if (document.all)
  {
    current = (document.all[nr].style.display == 'none') ? 'block' : 'none';
    document.all[nr].style.display = current;
  }
  else if (document.getElementById)
  {
    vista = (document.getElementById(nr).style.display == 'none') ? 'block' : 'none';
    document.getElementById(nr).style.display = vista;
  }
}

// FUNCIONES PARA VALIDAR CAMPOS

function ValidaVacio(entered, alertbox)
{
// Emptyfield Validation by Henrik Petersen / NetKontoret (modificado por Marcelo Demestri)
// Explained at www.echoecho.com/jsforms.htm
// Please do not remove this line and the two lines above.
with (entered)
{
if (value==null || value=="")
{if (alertbox!="") {alert(alertbox);} entered.focus(); return false;}
else {return true;}
}
}

// SOLICITA DATO
function jsDato(vMensaje, vValor)
{
var Name = prompt (vMensaje,vValor);
if ((Name == "") || (Name == null)) {
  Name = "0"; 
}
return Name;
}
</script>

<?php  
//// Recuperar la gestion de trabajo
$qGestion=2015;//$_SESSION['auth_granted_poa_gestion'];

//// Recuperar los parametros WhereToGo y WhatToDo
$WhereToGo=@$_GET["WhereToGo"];
if ($WhereToGo=="")
{
  $WhereToGo=0;
} 
$WhatToDo=@$_GET["WhatToDo"];
if ($WhatToDo=="")
{
  $WhatToDo=0;
} 

//// Recuperar los parametros iniciales
$qcmd=@$_GET["cgcmd"];
if (strlen($qcmd)==0)
{
  $qcmd="0";
} 
$qcmd=trim($qcmd);

if ($qcmd=="0")
{

  $qcmd=@$_POST["cgcmd"];
  if (strlen($qcmd)==0)
  {
    $qcmd="0";
  } 
  $qcmd=trim($qcmd);
} 


//// Comando para redirigir
if ($WhatToDo=="cmdCallProject")
{

  $qid=@$_GET["qRegistro"];
  setcookie("auth_granted_poa_cookie_localizacion_proy_id",$qid,0,"","",0);
} 


$qid1=@$_GET["cg1"];
$qid2=@$_GET["cg2"];
$qid3=@$_GET["cg3"];
$qid4=@$_GET["cg4"];
$qid5=@$_GET["cg5"];
$qid6=@$_GET["cg6"];
$qid22=@$_GET["cg22"]; // Ponderacion del Municipio
if (strlen($qid1)==0)
{
  $qid1=0;
} 
if (strlen($qid2)==0)
{
  $qid2=0;
} 
if (strlen($qid3)==0)
{
  $qid3=0;
} 
if (strlen($qid4)==0)
{
  $qid4=0;
} 
if (strlen($qid5)==0)
{
  $qid5=0;
} 
if (strlen($qid6)==0)
{
  $qid6=0;
} 
if (strlen($qid22)==0)
{
  $qid22=0;
} 
if (!$qid1>0)
{
  $qid1=0;
} 
if (!$qid2>0)
{
  $qid2=0;
} 
if (!$qid3>0)
{
  $qid3=0;
} 
if (!$qid4>0)
{
  $qid4=0;
} 
if (!$qid5>0)
{
  $qid5=0;
} 
if (!$qid6>0)
{
  $qid6=0;
} 
if (!$qid22>0)
{
  $qid22=0;
} 
if ($qid1>0)
{
  $qcmd="cgNuevo1";
} 
if ($qid2>0)
{
  $qcmd="cgNuevo2";
} 
if ($qid3>0)
{
  $qcmd="cgNuevo3";
} 
if ($qid4>0)
{
  $qcmd="cgNuevo4";
} 
if ($qid5>0)
{
  $qcmd="cgNuevo5";
} 
if ($qid6>0)
{
  $qcmd="cgNuevo6";
} 

// ********************************************
// **** Agregar Departamento al Proyecto
// ********************************************
if ($qcmd=="cgNuevo1")
{

  $qid=@$_GET["qRegistro"];
  if ($qid>0 && $qid1>0)
  {

    $sSQL="SELECT pd_id FROM _ProyectosDepartamentos WHERE proy_id = ".$qid." AND dep_id = ".$qid1;
    //if (!$IfExistBD[$sSQL][1]){

// Puego agregar el registro 
      $sSQL="INSERT INTO _ProyectosDepartamentos(proy_id, dep_id) VALUES(".$qid.", ".$qid1.")";
      //$Resultado=$ExecuteBD[$sSQL][1];
       $Resultado=pg_fetch_array(pg_query($sSQL));
           //$dato=pg_fetch_array($rs);
    //} 

  } 

} 

// ********************************************
// **** Agregar Provincia al Proyecto
// ********************************************
if ($qcmd=="cgNuevo3")
{

  $qid=@$_GET["qRegistro"];//21;//$_COOKIE["auth_granted_poa_cookie_localizacion_proy_id"];
  if ($qid>0 && $qid3>0)
  {

    $sSQL="SELECT pp_id FROM _ProyectosProvincias WHERE proy_id = ".$qid." AND prov_id = ".$qid3;
    //if (!$IfExistBD[$sSQL][1]){

// Puego agregar el registro 
      $sSQL="INSERT INTO _ProyectosProvincias(proy_id, prov_id) VALUES(".$qid.", ".$qid3.")";
      //$Resultado=$ExecuteBD[$sSQL][1];
      $Resultado=pg_query($sSQL);
    // } 

  } 

} 

// ********************************************
// **** Agregar Municipio al Proyecto
// ********************************************
if ($qcmd=="cgNuevo4")
{

  $qid=@$_GET["qRegistro"];//21;//$_COOKIE["auth_granted_poa_cookie_localizacion_proy_id"];
  $qid22=@$_GET["cg22"];
  /*if (!$qid22>=0)
  {
    $qid22=0;
  }*/ 

  if ($qid>0 && $qid4>0 && $qid22>=0)
  {

// Actualizar la ponderacion
    $sSQL="SELECT pm_id FROM _ProyectosMunicipios WHERE proy_id = ".$qid." AND muni_id=".$qid4;
    $IfExistBD=pg_fetch_array(pg_query($sSQL));
    //if (!$IfExistBD[1]){

// Puego agregar el registro 
      $sSQL="SELECT SUM(pm_pondera) AS eltotal FROM _ProyectosMunicipios WHERE proy_id = ".$qid;
      $RecordBD=pg_fetch_array(pg_query($sSQL));
      $varTotal=$RecordBD["eltotal"];
     if (!isset($varTotal))
      {
        $varTotal=0;
      } 
      if (!doubleval($varTotal)>=0)
      {
        $varTotal=0;
      } 
      //$varTotal=doubleval($varTotal)+doubleval($qid22);
      $varTotal=floatval($varTotal)+floatval($qid22);
      if ($varTotal>=101)
      {
        $qid22=0;
      }  /**/
      else{
         $sSQL="INSERT INTO _ProyectosMunicipios(proy_id, muni_id, pm_pondera) VALUES(".$qid.", ".$qid4.", ".$qid22.")";
      }
     
      //$Resultado=$ExecuteBD[$sSQL][1];
      $Resultado=pg_fetch_array(pg_query($sSQL));
    //} 

  } 

} 

// ********************************************
// **** Agregar Canton al Proyecto
// ********************************************
if ($qcmd=="cgNuevo5")
{

  $qid=@$_GET["qRegistro"];//21;//$_COOKIE["auth_granted_poa_cookie_localizacion_proy_id"];
  if ($qid>0 && $qid5>0)
  {

    $sSQL="SELECT pc_id FROM _ProyectosCantones WHERE proy_id = ".$qid." AND can_id = ".$qid5;
    //$varX=$RecordBD[$sSQL]["pc_id"][1];
    $RecordBD=pg_fetch_array(pg_query($sSQL));
      $varX=$RecordBD["pc_id"];
    if ($varX=="none")
    {
      $varX=0;
    } 
    if (!$varX>0)
    {

// Puego agregar el registro 
      $sSQL="INSERT INTO _ProyectosCantones(proy_id, can_id) VALUES(".$qid.", ".$qid5.")";
      //$Resultado=$ExecuteBD[$sSQL][1];
      $Resultado=pg_fetch_array(pg_query($sSQL));
    } 

  } 

} 

// ********************************************
// **** Borrar Departamento
// ********************************************
if ($qcmd=="cgDelDepartamento")
{

  $qid=@$_GET["qRegistro"];//$_COOKIE["auth_granted_poa_cookie_localizacion_proy_id"];
// Capturar el ID del Registro
  $qRegistro=@$_GET["qRegistro_dep"];
  if (strlen($qRegistro)==0)
  {
    $qRegistro=0;
  } 
  if (!$qRegistro>0)
  {
    $qRegistro=0;
  } 
  if ($qRegistro>0)
  {

    $sSQL="SELECT pp_id FROM _ProyectosProvincias WHERE proy_id = ".$qid;
    //if (!$IfExistBD[$sSQL][1]){

      $sSQL="DELETE FROM _ProyectosDepartamentos WHERE pd_id = ".$qRegistro;
      //$Resultado=$ExecuteBD[$sSQL][1];
      $Resultado=pg_fetch_array(pg_query($sSQL));

      setcookie("poa_cookies_llave_mensaje","DONE",0,"","",0);
      setcookie("poa_cookies_ruta_siguiente","?WhatToDo=cmdCallProject&qRegistro=".$_COOKIE["auth_granted_poa_cookie_localizacion_proy_id"],0,"","",0);
      setcookie("poa_cookies_texto_mensaje","El registro ha sido eliminado satisfactoriamente.",0,"","",0);
      //header("Location: "."Mensaje.asp");
    /*}
      else
    {

      setcookie("poa_cookies_llave_mensaje","NONE",0,"","",0);
      setcookie("poa_cookies_ruta_siguiente","?WhatToDo=cmdCallProject&qRegistro=".$_COOKIE["auth_granted_poa_cookie_localizacion_proy_id"],0,"","",0);
      setcookie("poa_cookies_texto_mensaje","Existe un error al intentar eliminar el registro, verifique e int&eacute;ntelo nuevamente.",0,"","",0);
      header("Location: "."Mensaje.asp");
    } */

  } 

} 

// ********************************************
// **** Borrar Provincia
// ********************************************
if ($qcmd=="cgDelProvincia")
{

  $qid=$_GET["qRegistro"];//$_COOKIE["auth_granted_poa_cookie_localizacion_proy_id"];
// Capturar el ID del Registro
  $qRegistro=$_GET["qRegistro_prov"];
  if (strlen($qRegistro)==0)
  {
    $qRegistro=0;
  } 
  if (!$qRegistro>0)
  {
    $qRegistro=0;
  } 
  if ($qRegistro>0)
  {


// Obtengo la provincia
    $qProvincia=0;
    $sSQL="SELECT prov_id FROM _ProyectosProvincias WHERE pp_id = ".$qRegistro;
    $rs=pg_query($sSQL);
    $RecordBD=pg_fetch_array($rs);
    $qProvincia=$RecordBD["prov_id"];//$qProvincia=$RecordBD[$sSQL]["prov_id"][1];
    if ($qProvincia=="none")
    {
      $qProvincia=0;
    } 
    if (!doubleval($qProvincia)>0)
    {
      $qProvincia=0;
    } 
// Verfico que no existan Municipios vinculados a la Provincia
    $sSQL="";
    $sSQL=$sSQL." SELECT _Municipios.prov_id, _ProyectosMunicipios.proy_id";
    $sSQL=$sSQL." FROM _Municipios INNER JOIN";
    $sSQL=$sSQL." _ProyectosMunicipios ON _Municipios.muni_id = _ProyectosMunicipios.muni_id";
    $sSQL=$sSQL." WHERE (_ProyectosMunicipios.proy_id = ".$qid.") AND (_Municipios.prov_id = ".$qProvincia.")";

    //if (!$IfExistBD[$sSQL][1] && (doubleval($qProvincia)>0)){

      $sSQL="DELETE FROM _ProyectosProvincias WHERE pp_id = ".$qRegistro;
      //$Resultado=$ExecuteBD[$sSQL][1];
      $Resultado=pg_fetch_array(pg_query($sSQL));

      setcookie("poa_cookies_llave_mensaje","DONE",0,"","",0);
      setcookie("poa_cookies_ruta_siguiente","?WhatToDo=cmdCallProject&qRegistro=".$_COOKIE["auth_granted_poa_cookie_localizacion_proy_id"],0,"","",0);
      setcookie("poa_cookies_texto_mensaje","El registro ha sido eliminado satisfactoriamente.",0,"","",0);
      /*header("Location: "."Mensaje.asp");
    }
      else
    {

      setcookie("poa_cookies_llave_mensaje","NONE",0,"","",0);
      setcookie("poa_cookies_ruta_siguiente","?WhatToDo=cmdCallProject&qRegistro=".$_COOKIE["auth_granted_poa_cookie_localizacion_proy_id"],0,"","",0);
      setcookie("poa_cookies_texto_mensaje","Existe un error al intentar eliminar el registro, verifique e int&eacute;ntelo nuevamente.",0,"","",0);
      header("Location: "."Mensaje.asp");
    } */

  } 

} 

// ********************************************
// **** Borrar Municipio
// ********************************************
if ($qcmd=="cgDelMunicipio")
{

  $qid=21;//$_COOKIE["auth_granted_poa_cookie_localizacion_proy_id"];
// Capturar el ID del Registro
  $qRegistro=$_GET["qRegistro_mun"];
  if (strlen($qRegistro)==0)
  {
    $qRegistro=0;
  } 
  if (!$qRegistro>0)
  {
    $qRegistro=0;
  } 

  if ($qRegistro>0)
  {

// Obtengo el municipio
    $qMunicipio=0;
    $sSQL="SELECT muni_id FROM _ProyectosMunicipios WHERE pm_id = ".$qRegistro;
   /* $qMunicipio=$RecordBD[$sSQL]["muni_id"][1];
    if ($qMunicipio=="none")
    {
      $qMunicipio=0;
    } 
    if (!doubleval($qMunicipio)>0)
    {
      $qMunicipio=0;
    } */
// Verfico que no existan Cantones vinculados al Municipio
    $sSQL="";
    $sSQL=$sSQL." SELECT _Cantones.can_id, _ProyectosCantones.proy_id";
    $sSQL=$sSQL." FROM _Cantones INNER JOIN";
    $sSQL=$sSQL." _ProyectosCantones ON _Cantones.can_id = _ProyectosCantones.can_id";
    $sSQL=$sSQL." WHERE (_ProyectosCantones.proy_id = ".$qid.") AND (_Cantones.muni_id = ".$qMunicipio.")";
    //if (!$IfExistBD[$sSQL][1] && (doubleval($qMunicipio)>0)){

      $sSQL="DELETE FROM _ProyectosMunicipios WHERE pm_id = ".$qRegistro;
      //$Resultado=$ExecuteBD[$sSQL][1];
      $Resultado=pg_fetch_array(pg_query($sSQL));

      setcookie("poa_cookies_llave_mensaje","DONE",0,"","",0);
      setcookie("poa_cookies_ruta_siguiente","?WhatToDo=cmdCallProject&qRegistro=".$_COOKIE["auth_granted_poa_cookie_localizacion_proy_id"],0,"","",0);
      setcookie("poa_cookies_texto_mensaje","El registro ha sido eliminado satisfactoriamente.",0,"","",0);


  } 

} 

// ********************************************
// **** Actualizar la Ponderacion del Municipio
// ********************************************
if ($qcmd=="cmd_UpdMunicipio")
{

  $qRegistro=@$_GET["qRegistro_mun_pnd"];
  if (!$qRegistro>0){
    $qRegistro=0;
  } /**/

  $qPondera=@$_GET["qPonderacion"];
  /*if (!$qPondera>=0){
    $qPondera=0;
  } */

// Actualizar la ponderacion del Municipio
  if ($qRegistro>0 && $qPondera>0)
  {

    $sSQL="UPDATE _ProyectosMunicipios SET pm_pondera = '".$qPondera."' WHERE pm_id = ".$qRegistro;
    //$Resultado=$ExecuteBD[$sSQL][1];
    $Resultado=pg_fetch_array(pg_query($sSQL));
  } 

} 

// ********************************************
// **** Borrar Canton
// ********************************************
if ($qcmd=="cgDelCanton")
{

// Capturar el ID del Registro
  $qRegistro=$_GET["qRegistro_canton"];
  if (strlen($qRegistro)==0)
  {
    $qRegistro=0;
  } 
  if (!$qRegistro>0)
  {
    $qRegistro=0;
  } 

  if ($qRegistro>0)
  {

    $sSQL="DELETE FROM _ProyectosCantones WHERE pc_id = ".$qRegistro; 
    //$Resultado=$ExecuteBD[$sSQL][1];
    $Resultado=pg_fetch_array(pg_query($sSQL));
  } 

} 

// ********************************************
// * Adicionar Canton
// ********************************************
if ($qcmd=="cmd_Canton")
{

  $WhereToGo=1;
} 

// ********************************************
// **** Grabar Registro
// ********************************************
if ($qcmd=="cmd_Canton_Save")
{

  $qid=@$_GET["qRegistro"];//$_COOKIE["auth_granted_poa_cookie_localizacion_proy_id"];
  $var1=@$_POST["var1"];
  if (strlen($var1)==0)
  {
    $var1="0";
  } 
  $var1=trim($var1);
  $var2=@$_POST["var2"];
  if (strlen($var2)==0)
  {
    $var2=0;
  } 
  if (!$var2>0)
  {
    $var2=0;
  } 
  $var3=@$_POST["var3"];
  if (strlen($var3)==0)
  {
    $var3=0;
  } 
  if (!$var3>0)
  {
    $var3=0;
  } 

  if (strlen($var1)>0 && $var2>0)
  {


    $sSQL="INSERT INTO _Cantones(can_canton, muni_id, idtipo) VALUES('".$var1."', ".$var2.", ".$var3.")";
    //$Resultado=$ExecuteBD[$sSQL][1];
    $Resultado=pg_fetch_array(pg_query($sSQL));

// Vincular el registro creado al Proyecto
    $sSQL="SELECT can_id FROM _Cantones WHERE can_canton = '".$var1."' AND muni_id = ".$var2;
    //$varX=$RecordBD[$sSQL]["can_id"][1];
    $RecordBD=pg_fetch_array(pg_query($sSQL));
    $varX=$RecordBD["can_id"];
    if ($varX=="none")
    {
      $varX=0;
    } 
    if (!doubleval($varX)>0)
    {
      $varX=0;
    } 
    if (doubleval($varX)>0 && $qid>0)
    {

      $sSQL="INSERT INTO _ProyectosCantones(can_id, proy_id) VALUES(".$varX.", ".$qid.")";
      //$Resultado=$ExecuteBD[$sSQL][1];
      $Resultado=pg_fetch_array(pg_query($sSQL));
    } 

    $cgmsg=$varX;
  }
    else
  {

    $cgmsg=$varX;
  } 

} 


?>
<!-- Pagina Inicial -->
<?php  if ($WhereToGo==0)
{
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">

  <tr>
    <td>

        <form name="form" method="post" action="SGPLocalizacion">
        <table width="80%" border="0" cellspacing="0" cellpadding="3" align="center">
          <tr>
            <td style="border-right:1px solid #999;" width="25%" align="center"><font face="Tahoma, Geneva, sans-serif" size="3"><b>DEPARTAMENTO</b></font></td>
            <td width="20">&nbsp;</td>
            <td style="border-right:1px solid #999;" width="25%" align="center"><font face="Tahoma, Geneva, sans-serif" size="3"><b>PROVINCIA</b></font></td>
            <td width="20">&nbsp;</td>
            <td style="border-right:1px solid #999;" width="25%" align="center"><font face="Tahoma, Geneva, sans-serif" size="3"><b>MUNICIPIO</b></font></td>
            <td width="20">&nbsp;</td>
            <td width="25%" align="center"><font face="Tahoma, Geneva, sans-serif" size="3"><b>LOCALIZACI&Oacute;N ESPECIFICA</b></font></td>
          </tr>
          <tr>
            <td style="border-right:1px solid #999;">&nbsp;</td>
            <td>&nbsp;</td>
            <td style="border-right:1px solid #999;">&nbsp;</td>
            <td>&nbsp;</td>
            <td style="border-right:1px solid #999;">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td style="border-right:1px solid #999;" valign="top"><?php    echo saiProyDepartamentos($_GET["qRegistro"]);?></td>
            <td>&nbsp;</td>
            <td style="border-right:1px solid #999;" valign="top"><?php    echo saiProyProvincias($_GET["qRegistro"]);?></td>
            <td>&nbsp;</td>
            <td style="border-right:1px solid #999;" valign="top"><?php    echo saiProyMunicipios($_GET["qRegistro"]);?></td>
            <td>&nbsp;</td>
            <td valign="top"><?php    echo saiProyCantones($_GET["qRegistro"]);?></td>
          </tr>
          <tr>
            <td style="border-right:1px solid #999;">&nbsp;</td>
            <td>&nbsp;</td>
            <td style="border-right:1px solid #999;">&nbsp;</td>
            <td>&nbsp;</td>
            <td style="border-right:1px solid #999;">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td style="border-right:1px solid #999;">&nbsp;</td>
            <td>&nbsp;</td>
            <td style="border-right:1px solid #999;">&nbsp;</td>
            <td>&nbsp;</td>
            <td style="border-right:1px solid #999;">
                <?php  
  $sSQL="SELECT SUM(pm_pondera) AS eltotal FROM _ProyectosMunicipios WHERE (proy_id = ".$_GET["qRegistro"].")";
  $MatrixBD=pg_query($sSQL);
  $rs_MatrixBD=pg_fetch_array($MatrixBD);
  $varX=$rs_MatrixBD["eltotal"];
  if (!isset($varX))
  {
    $varX=0;
  } 
  if (!doubleval($varX)>0)
  {
    $varX=0;
  } 
  print number_format($varX,2)."% Ponderado";
  if (!doubleval($varX)==100)
  {
    print "<img src=\"".base_url()."/assets/Iconos/bullet_red.png\" width=\"16\" height=\"16\" border=\"0\" align=\"absmiddle\">";
  } ?>
            </td>
            <td>&nbsp;</td>
            <td><div align="center"><img src="<?php echo base_url()?>/assets/Iconos/page_add.png" width="16" height="16" border="0" style="cursor:pointer" onClick="javascript:NewWindow('?cgcmd=cmd_Canton&qRegistro=<?php echo $_GET['qRegistro']?>','_self',0,0,0);" alt="Nuevo" title="Nuevo"></div></td>
          </tr>
          <tr>
            <td style="border-right:1px solid #999;">&nbsp;</td>
            <td>&nbsp;</td>
            <td style="border-right:1px solid #999;">&nbsp;</td>
            <td>&nbsp;</td>
            <td style="border-right:1px solid #999;">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td style="border-right:1px solid #999;">
            <select name="var1o" class="form-control" id="var1o" onChange="javascript:this.form.var1.value=this.form.var1o.value;winconfirm('Agregar '+this.form.var1o[this.form.var1o.selectedIndex].text+'?','?WhatToDo=cmdCallProject&qRegistro=<?php echo $_GET['qRegistro']?>&cg1='+this.form.var1.value,'_self',0,0,0)" title="SELECCIONE DEPARTAMENTO">
              <option value="0">--Seleccione--</option>
              <option value="6">TARIJA</option>
            </select></td>
            <td>&nbsp;</td>
            <td style="border-right:1px solid #999;"><select name="var3o" class="form-control" id="var3o" onChange="javascript:this.form.var3.value=this.form.var3o.value;winconfirm('Agregar '+this.form.var3o[this.form.var3o.selectedIndex].text+'?','?qRegistro=<?php echo $_GET['qRegistro']?>&cg3='+this.form.var3.value,'_self',0,0,0)" title="SELECCIONE PROVINCIA">
                <option value="0">--Seleccione--</option>
                <?php  print saiProvincias($_GET["qRegistro"]); ?>
            </select></td>
            <td>&nbsp;</td>
            <td style="border-right:1px solid #999;"><select name="var4o" class="form-control" id="var4o" onChange="javascript:this.form.var4.value=this.form.var4o.value;vV1=jsDato('Ponderacion del Municipio','0');winconfirm('Agregar '+this.form.var4o[this.form.var4o.selectedIndex].text+'?','?qRegistro=<?php echo $_GET['qRegistro']?>&cg22='+vV1+'&cg4='+this.form.var4.value,'_self',0,0,0)" title="SELECCIONE MUNICIPIO">
                <option value="0">--Seleccione--</option>
                <?php  print saiMunicipios($_GET["qRegistro"]); ?>
              </select></td>
              <td>&nbsp;</td>
            <td><select name="var5o" class="form-control" id="var5o" onChange="javascript:this.form.var5.value=this.form.var5o.value;winconfirm('Agregar '+this.form.var5o[this.form.var5o.selectedIndex].text+'?','?qRegistro=<?php echo $_GET['qRegistro']?>&cg5='+this.form.var5.value,'_self',0,0,0)" title="REGISTRE CANTON">
                <option value="0">--Seleccione--</option>
                <?php  print saiCantones($_GET["qRegistro"]); ?>
              </select></td>
          </tr>
          <tr>
            <td style="border-right:1px solid #999;"><input name="var1" type="hidden" id="var1" value="0" size="10"></td>
            <td>&nbsp;</td>
            <td style="border-right:1px solid #999;"><input name="var3" type="hidden" id="var3" value="0" size="10"></td>
            <td>&nbsp;</td>
            <td style="border-right:1px solid #999;"><input name="var4" type="hidden" id="var4" value="0" size="10"></td>
            <td>&nbsp;</td>
            <td><input name="var5" type="hidden" id="var5" value="0" size="10"></td>
          </tr>
        </table>
        </form>
        
     </td>
  </tr>
</table>
<?php  } ?>

<!-- Adicionar Canton -->
<?php  if ($WhereToGo==1)
{
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td class="TituloSeccion">Cant&oacute;n / Distrito (Adicionar)</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>
  <form action="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/4' ?>?cgcmd=cmd_Canton_Save&qRegistro=<?php echo $_GET["qRegistro"];?>" method="post" name="frmNew">
  <table width="60%" border="0" cellspacing="1" cellpadding="3" align="center">

      <tr>
    <td width="40%" class="EncabezadoTabla">Municipio</td>
    <td width="60%"><select name="var2" class="form-control" id="var2">
            <option value="0">--Seleccione--</option>
            <?php  
  print saiMuniEnProy($_GET["qRegistro"]);
    //print saiMuniEnProy($_COOKIE["auth_granted_poa_cookie_localizacion_proy_id"]);
?>
          </select></td>
    </tr>
    <tr>
    <td width="40%" class="EncabezadoTabla">Cant&oacute;n / Distrito</td>
    <td width="60%"><input name="var1" type="text" class="form-control" id="var1"></td>
    </tr>
      <tr>
    <td width="40%" class="EncabezadoTabla">Tipo</td>
    <td width="60%"><select name="var3" class="form-control" id="var3">
            <option value="0">--Seleccione--</option>
            <option value="1">Cant&oacute;n</option>
            <option value="2">Distrito</option>
            <option value="3">Comunidad</option>
            <option value="4">Junta Vecinal</option>
            <option value="5">Otro</option>
          </select></td>
    </tr>
      <tr>
    <td colspan="2">&nbsp;</td>
      </tr>
    <tr>
        <td colspan="2" align="center"><input name="Aceptar"  class="btn btn-primary" value="   Aceptar   " type="submit" >&nbsp;<input name="Cancelar" value="   Cancelar   " type="button" class="btn btn-default" onClick="javascript:window.location.href='?qRegistro=<?php echo $_GET["qRegistro"];?>'"></td>
      </tr>
  </table>
  </form>
  </td>
  </tr>
</table>
</div>
</div>
</div>
</article >
<?php  } ?>

          <?php
            $attributes = array('class' => 'form-horizontal', 'id' => 'formulario','name' =>'formulario','enctype' => 'multipart/form-data');
            echo validation_errors();
            echo form_open('admin/proy/update_prog', $attributes);
          ?>
              <!-- Widget ID (each widget will need unique ID)-->
              <div class="">
                              <form id="wizard-1" novalidate="novalidate" method="post">
                              <input class="form-control" type="hidden" name="id" value="<?php echo $proyecto[0]['proy_id'] ?>"><input class="form-control" type="hidden" name="cod" value="<?php echo $proyecto[0]['proy_codigo']?>">    
                              <input class="form-control" type="hidden" name="mod" id="mod" value="<?php echo $mod?>">
                              <input type="hidden" name="tp" id="tp" value="<?php echo $proyecto[0]['tp_id'] ?>">  
                                <div class="row">
                                  <div class="col-sm-9">
                                    <div class="form-group">
                                      <label><font size="1"><b>POBLACI&Oacute;N BENEFICIARIA</b></font></label>
                                      <textarea rows="5" style="width:100%;" name="pob_bene" id="pob_bene"><?php echo $proyecto[0]['proy_poblac_beneficiaria'] ?></textarea> 
                                    </div>
                                  </div>
                                  <div class="col-sm-3">
                                    <div class="form-group">
                                      <label><font size="1"><b>UBICACI&Oacute;N GEOREFERENCIAL</b></font></label>
                                      <style type="text/css">#graf{font-size: 90px;}</style> 
                                      <center><a onclick="abrir('<?php echo base_url().'assets/geogoblp/index.php?id='.$_GET['qRegistro'] ?>',1200,800);" title="GEO VISOR"><span class="glyphicon glyphicon-globe" id="graf"></span></a></center>

                                    </div>
                                  </div>  
                                </div>

                                <div class="row">
                                  <div class="col-sm-2">
                                    <div class="form-group">
                                      <label><font size="1"><b>&Aacute;REA DE INFLUENCIA</b></font></label>
                                              <select class="form-control" id="area_id" name="area_id" >
                                                  <option value="<?php echo $proyecto[0]['area_id'] ?>"><?php if($proyecto[0]['area_id'] ==1) {echo "Urbano";}elseif ($proyecto[0]['area_id'] ==2) {echo "Rural";}elseif ($proyecto[0]['area_id'] ==3) {echo "Urbano-Rural";}elseif ($proyecto[0]['area_id'] ==0) {echo "No seleccionado";} ?></option>
                                                  <option value="1">Urbano</option>
                                                  <option value="2">Rural</option>
                                                  <option value="3">Urbano-Rural</option>        
                                              </select>
                                    </div>
                                  </div>
                                  <?php $dato=553373; ?>
                                  <div class="col-sm-2">
                                    <div class="form-group">
                                      <label><font size="1"><b>POBLACI&Oacute;N SEGUN CENSO</b></font></label>
                                      <input class="form-control" type="text" value="<?php echo number_format($dato);?>" disabled="true">
                                      <input class="form-control" type="hidden" name="pob_c" id="pob_c" value="553373" placeholder="0" onblur="javascript:calcula_poblacion_beneficiada();" onkeypress="if (this.value.length < 10) { return soloNumeros(event);}else{return false; }">
                                    </div>
                                  </div>
                                  <div class="col-sm-2">
                                    <div class="form-group">
                                      <label><font size="1"><b>POBLACI&Oacute;N BENEFICIADA</b></font></label>
                                      <input class="form-control" type="text" name="num_pob" id="num_pob" value="<?php echo $proyecto[0]['proy_poblac_beneficiada'] ?>" onkeyup="javascript:calcula_poblacion_beneficiada();" onkeypress="if (this.value.length < 10) { return soloNumeros(event);}else{return false; }">
                                    </div>
                                  </div>
                                  <div class="col-sm-2">
                                      <div class="form-group">
                                        <label><font size="1"><b>Nro. DE HOMBRES</b></font></label>
                                        <input class="form-control" type="text" name="nro_h" id="nro_h" value="<?php echo $proyecto[0]['proy_nro_hombres'] ?>"  placeholder="0" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }">
                                      </div>
                                    </div>
                                    <div class="col-sm-2">
                                      <div class="form-group">
                                        <label><font size="1"><b>Nro. DE MUJERES</b></font></label>
                                        <input class="form-control" type="text" name="nro_m" id="nro_m" value="<?php echo $proyecto[0]['proy_nro_mujeres'] ?>"  placeholder="0" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }">
                                      </div>
                                  </div>
                                  <div class="col-sm-2">
                                    <div class="form-group">
                                      <label><font size="1"><b>% POBLACI&Oacute;N BENEFICIADA</b></font></label>
                                      <input class="form-control" type="text" name="porc_pob" id="porc_pob" value="<?php echo $proyecto[0]['proy_porcent_poblac_beneficiada'] ?>" placeholder="0" onkeypress="if (this.value.length < 10) { return soloNumeros(event);}else{return false; }">
                                    </div>
                                  </div>

                                </div>

                            <div class="form-actions">
                              <?php
                              if($proyecto[0]['proy_estado']==1)
                              {
                                ?>
                                <a href="<?php echo base_url().'index.php/admin/proy/list_proy' ?>" class="btn btn-lg btn-default" title="VOLVER A MIS PROYECTOS"> CANCELAR </a>
                                <?php
                              }
                              elseif ($proyecto[0]['proy_estado']==2) 
                              {
                                ?>
                                <a href="<?php echo base_url().'index.php/admin/proy/list_proy_poa' ?>" class="btn btn-lg btn-default" title="VOLVER A MIS PROYECTOS"> CANCELAR </a>
                                <?php
                              }
                              ?>
                              <button type="submit" name="form" value="4" class="btn btn-primary btn-lg" title="MODIFICAR Y GUARDAR LOCALIZACION"> SIGUIENTE </button>
                            </div>
                            </div>

        <script type="text/javascript">
        function abrir(url,wid,hei)
        {
          if(wid==0 && hei==0)
          {
            wid = screen.width ;
            hei = screen.heigh;
          }
          var x = (screen.width - wid)/2;
          var y = (screen.height - hei)/2;
          window.open(url,'','top='+y+'px,left='+x+'px,width='+wid+'px,height='+hei+'px');
        }
      </script>
                    
        </section>
        
      </div>
      <!-- END MAIN CONTENT -->

    </div>
    <!-- END MAIN PANEL -->

    <!-- PAGE FOOTER -->
  
    <!-- END PAGE FOOTER -->

    <!--================================================== -->

    <script type="text/javascript">
//------------------------------------------
      function calcula_poblacion_beneficiada()
      { 
          a = parseInt($('[name="pob_c"]').val());
            b = parseInt($('[name="num_pob"]').val());
            if (a!=0 && a>0 ) {
              
              if ( b!=0 && b>0) {
                if(b>a)
                {
                  alert('valor no valido')
                  $('[name="num_pob"]').focus();//foco de inicio
                }
                else
                {
                  $('[name="porc_pob"]').val(((b/a)*100).toFixed(2) );xx
                }
                
              }else{
                $('[name="num_pob"]').focus();//foco de inicio
              }
            }else{
              //alert('ingrese a')
              $('[name="pob_c"]').focus();//foco de inicio
            }
      }
      </script>
    <script>
      if (!window.jQuery) {
        document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-2.0.2.min.js"><\/script>');
      }
    </script>

    <script>
      if (!window.jQuery.ui) {
        document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
      }
    </script>

    <!-- IMPORTANT: APP CONFIG -->
    <script src="<?php echo base_url(); ?>assets/js/session_time/jquery-idletimer.js"></script>
    <script src = "<?php echo base_url(); ?>mis_js/control_session.js"></script>
    <script src="<?php echo base_url();?>/assets/js/app.config.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/mis_js/validacion_form.js"></script>
    <!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
    <script src="<?php echo base_url();?>/assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
    <!-- BOOTSTRAP JS -->
    <script src="<?php echo base_url();?>/assets/js/bootstrap/bootstrap.min.js"></script>
    <!-- CUSTOM NOTIFICATION -->
    <script src="<?php echo base_url();?>/assets/js/notification/SmartNotification.min.js"></script>
    <!-- JARVIS WIDGETS -->
    <script src="<?php echo base_url();?>/assets/js/smartwidgets/jarvis.widget.min.js"></script>
    <!-- EASY PIE CHARTS -->
    <script src="<?php echo base_url();?>/assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
    <!-- SPARKLINES -->
    <script src="<?php echo base_url();?>/assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
    <!-- JQUERY VALIDATE -->
    <script src="<?php echo base_url();?>/assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
    <!-- JQUERY MASKED INPUT -->
    <script src="<?php echo base_url();?>/assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
    <!-- JQUERY SELECT2 INPUT -->
    <script src="<?php echo base_url();?>/assets/js/plugin/select2/select2.min.js"></script>
    <!-- JQUERY UI + Bootstrap Slider -->
    <script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
    <!-- browser msie issue fix -->
    <script src="<?php echo base_url();?>/assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
    <!-- FastClick: For mobile devices -->
    <script src="<?php echo base_url();?>/assets/js/plugin/fastclick/fastclick.min.js"></script>
    <!-- Demo purpose only -->
    <script src="<?php echo base_url();?>/assets/js/demo.min.js"></script>
    <!-- MAIN APP JS FILE -->
    <script src="<?php echo base_url();?>/assets/js/app.min.js"></script>
    <!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
    <!-- Voice command : plugin -->
    <script src="<?php echo base_url();?>/assets/js/speech/voicecommand.min.js"></script>
    <!-- PAGE RELATED PLUGIN(S) -->
    <script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
    <script src="<?php echo base_url();?>/assets/js/plugin/fuelux/wizard/wizard.min.js"></script>
    <script type="text/javascript">
    
    // DO NOT REMOVE : GLOBAL FUNCTIONS!
    
    $(document).ready(function() {
      
      pageSetUp();

         var $validator = $("#wizard-1").validate({
          
          rules: {
            /////////////// LOCALIZACION
            pob_bene: {
              required: true, ///// poblacion beneficiaria texto
            },
            area_id: {
              required: true,////// area de influencia
            },
            pob_c: {
              required: true, ///// poblacion segun censo
            },
            num_pob: {
              required: true,////// numero de poblacion
            },
            porc_pob: {
              required: true,////// porcentaje de poblacion beneficiada
            }

          },
          
          messages: {
            pob_bene: "Escriba la poblacion beneficiaria",
            area_id: "Seleccione area de influencia",
            pob_c: "Ingrese Poblacion segun Censo",
            num_pob: "Ingrese nro de poblacion beneficiaria",
            porc_pob: "Ingrese % de la poblacion beneficiaria",

          },
          
          highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
          },
          unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
          },
          errorElement: 'span',
          errorClass: 'help-block',
          errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
              error.insertAfter(element.parent());
            } else {
              error.insertAfter(element);
            }
          }
        });
        
        $('#bootstrap-wizard-1').bootstrapWizard({
          'tabClass': 'form-wizard',
          'onNext': function (tab, navigation, index) {
            var $valid = $("#wizard-1").valid();
            if (!$valid) {
              $validator.focusInvalid();
              return false;
            } else {
              $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).addClass(
                'complete');
              $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).find('.step')
              .html('<i class="fa fa-check"></i>');
            }
          }
        });
        
    
      // fuelux wizard
        var wizard = $('.wizard').wizard();
        
        wizard.on('finished', function (e, data) {
          //$("#fuelux-wizard").submit();
          //console.log("submitted!");
          $.smallBox({
            title: "Congratulations! Your form was submitted",
            content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
            color: "#5F895F",
            iconSmall: "fa fa-check bounce animated",
            timeout: 4000
          });
          
        });
    })

    </script>
  </body>
</html>
