<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

		<title><?php echo $this->session->userdata('name')?></title>
		<meta name="description" content="">
		<meta name="author" content="">
			
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<!--///////////////css-->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
		<!--//////////////fin css-->
		<style type="text/css">
			aside{background: #05678B;}
		</style>
	</head>
	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->

		<!-- HEADER -->
		<header id="header">
			<div id="logo-group">
				<!-- <span id="logo"> <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="SmartAdmin"> </span> -->
			</div>

			<!-- pulled right: nav area -->
			<div class="pull-right">
				<!-- collapse menu button -->
				<div id="hide-menu" class="btn-header pull-right">
					<span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
				</div>
				<!-- end collapse menu -->
				<!-- logout button -->
				<div id="logout" class="btn-header transparent pull-right">
					<span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Sign Out" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
				</div>
				<!-- end logout button -->
				<!-- search mobile button (this is hidden till mobile view port) -->
				<div id="search-mobile" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
				</div>
				<!-- end search mobile button -->
				<!-- fullscreen button -->
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen"><i class="fa fa-arrows-alt"></i></a> </span>
				</div>
				<!-- end fullscreen button -->
			</div>
			<!-- end pulled right: nav area -->
		</header>
		<!-- END HEADER -->

		<!-- Left panel : Navigation area -->
		<aside id="left-panel">
			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                        <span>
                            <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                        </span>
					</a> 
				</span>
			</div>

			<nav>
				<ul>
					<li>
						<a href='<?php echo site_url("admin").'/dashboard'; ?>' title="MENU PRINCIPAL"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
					</li>
					<li class="text-center">
		                <a href="<?php echo base_url().'index.php/admin/proy/mis_proyectos/'.$mod ?>" title="PROGRAMACION -> PROGRAMACI&Oacute;N FISICA"> <span class="menu-item-parent">PROGRAMACI&Oacute;N F&Iacute;SICA</span></a>
		            </li>
					<?php
					if($nro_fase==1){
					    for($i=0;$i<count($enlaces);$i++)
					    {
					    	?>
					    	 <li>
				              	<a href="#" >
				              		<i class="<?php echo $enlaces[$i]['o_image'];?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
				              	<ul >
				              	<?php
				                $submenu= $this->menu_modelo->get_Modulos_sub($enlaces[$i]['o_child']);
					      		foreach($submenu as $row) {
				                ?>
				                <li><a href="<?php echo base_url($row['o_url'])."/".$mod."/".$id_f[0]['id']."/".$id_f[0]['proy_id']; ?>"><?php echo $row['o_titulo']; ?></a></li>
				                <?php } ?>
				                </ul>
				            </li>
					    	<?php
					    }
					}
					?>
				</ul>
			</nav>
			<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>
		</aside>

		<!-- MAIN PANEL -->
		<div id="main" role="main">

			<!-- RIBBON -->
			<div id="ribbon">

				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>

				<!-- breadcrumb -->
				<ol class="breadcrumb">
				<?php 
				if($mod==1)
				{
					?>
					<li><a href="<?php echo base_url().'index.php/admin/proy/list_proy' ?>" title="MIS PROYECTOS">Mis Operaciones</a></li><li><a href="<?php echo base_url().'index.php/admin/proy/prog/'.$mod.'/'.$proyecto[0]['proy_id'].'' ?>" title="DATOS GENERALES DEL PROYECTO">Datos Generales</a></li><li>Datos Generales</li>
					<?php
				}
				elseif ($mod==4) 
				{
					?>
					<li><a href="<?php echo base_url().'index.php/admin/sgp/list_proy' ?>" title="MIS PROYECTOS">Gerencia de Proyectos</a></li><li><a href="<?php echo base_url().'index.php/admin/proy/prog//'.$mod.'/'.$proyecto[0]['proy_id'].'' ?>" title="DATOS GENERALES DEL PROYECTO">Datos Generales</a></li><li>Datos Generales</li>	
					<?php
				}
				?>
					
				</ol>
			</div>
			<!-- END RIBBON -->

			<!-- MAIN CONTENT -->
			<div id="content">
					<?php
						$attributes = array('class' => 'form-horizontal', 'id' => 'formulario','name' =>'formulario','enctype' => 'multipart/form-data');
						echo validation_errors();
						echo form_open('admin/proy/update_prog', $attributes);
					?>

				<section id="widget-grid" class="">
				<div class="row">
					<nav role="navigation" class="navbar navbar-default navbar-inverse">
				        <div class="navbar-header">
				            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
				                <span class="sr-only">Toggle navigation</span>
				                <span class="icon-bar"></span>
				                <span class="icon-bar"></span>
				                <span class="icon-bar"></span>
				            </button>
				        </div>
				 
				        <div id="navbarCollapse" class="collapse navbar-collapse">
				            <ul class="nav navbar-nav">
				                <li class="active"><a href="#"><i class="glyphicon glyphicon-ok"></i><font size="2">&nbsp;DATOS GENERALES&nbsp;</font></a></li>
				                <?php
			                        if($proyecto[0]['tp_id']==1 || $proyecto[0]['tp_id']==2 || $proyecto[0]['tp_id']==3)
			                        {
			                        	?>
			                        <li><a href="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/10' ?>"><font size="2">&nbsp;METAS&nbsp;</font></a></li>
			                        	<?php
			                        }
			                    ?>
				                <li><a href="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/2' ?>"><font size="2">&nbsp;RESPONSABLES&nbsp;</font></a></li>
				                <li><a href="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/3' ?>"><font size="2">&nbsp;CLASIFICACI&Oacute;N&nbsp;</font></a></li>
				                <li><a href="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/4?qRegistro='.$proyecto[0]['proy_id'].'' ?>"><font size="2">&nbsp;LOCALIZACI&Oacute;N&nbsp;</font></a></li>
				                <?php
			                        if($proyecto[0]['tp_id']==1 || $proyecto[0]['tp_id']==2 || $proyecto[0]['tp_id']==3)
			                        {
			                        	?>
			                        <li><a href="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/5' ?>"><font size="2">&nbsp;OBJETIVOS&nbsp;</font></a></li>
			                        	<?php
			                        }
			                    ?>
				                <li><a href="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/7' ?>"><font size="2">&nbsp;RESUMEN T&Eacute;CNICO&nbsp;</font></a></li>
				            	<?php
			                        if($proyecto[0]['tp_id']==1){
			                          ?>
			                          <li><a href="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/9' ?>" title="FASE ETAPA COMPONENTE"><font size="2">&nbsp;FASE / ETAPA&nbsp;</font></a></li>
			                          <?php
			                        }
			                        elseif ($proyecto[0]['tp_id']==2 || $proyecto[0]['tp_id']==3 || $proyecto[0]['tp_id']==4) {
			                          if($mod==1)
			                          {
				                          ?>
				                          <li><a href="<?php echo base_url().'index.php/admin/proy/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/9' ?>" title="MONTO PRESUPUESTO"><font size="2">&nbsp;PRESUPUESTO&nbsp;</font></a></li>
				                          <?php
			                          }
			                          elseif($mod==4)
			                          {
			                          	  ?>
				                          <li><a href="<?php echo base_url().'index.php/admin/sgp/edit_prog/'.$mod.'/'.$proyecto[0]['proy_id'].'/9' ?>" title="MONTO PRESUPUESTO"><font size="2">&nbsp;PRESUPUESTO&nbsp;</font></a></li>
				                          <?php
			                          }
			                        }
			                      ?>
				            </ul>
				        </div>
					</nav>
				</div>
					<!-- row -->
					<div class="row">
							<!-- Widget ID (each widget will need unique ID)-->
							<input type="hidden" name="id" id="id" value="<?php echo $proyecto[0]['proy_id'] ?>"> 
							<input type="hidden" name="form" id="form" value="1">
							<input type="hidden" name="tp" id="tp" value="<?php echo $proyecto[0]['tp_id'] ?>">
							<input type="hidden" name="mod" id="mod" value="<?php echo $mod ?>">
							<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<form id="formulario" name="formulario" novalidate="novalidate" method="post">
								<div class="jarviswidget jarviswidget-color-darken" >
									<header>
										<span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
										<h2 class="font-md"><strong>DATOS GENERALES</strong></h2>				
									</header>
										<!-- widget content -->
											<div class="widget-body">
												<div id="bootstrap-wizard-1" class="col-sm-8">
													<div class="well">
														<div class="row">
															<div class="col-sm-12">
																<div class="form-group">
																	<label><font size="1"><b>NOMBRE DE PROYECTO, PROGRAMA U OPERACI&Oacute;N</b></font></label>
																	<textarea rows="4" class="form-control" name="nom_proy" id="nom_proy" style="width:100%;" ><?php echo $proyecto[0]['proy_nombre'] ?></textarea>
																</div>
															</div>
														</div>

														<div class="row">
															<div class="col-sm-6">
																<div class="form-group">
																	<?php 
																		if($proyecto[0]['tp_id']==1)
																		{
																			?>
																			<label><font size="1"><b>C&Oacute;DIGO SISIN</b></font></label>
																			<input class="form-control" type="text" name="cod_sisin" id="cod_sisin" value="<?php echo $proyecto[0]['proy_sisin'] ?>" data-mask="9999-99999-99999" data-mask-placeholder= "X" placeholder="Codigo Proyecto">
																			<?php
																		}
																		elseif ($proyecto[0]['tp_id']==2 ||$proyecto[0]['tp_id']==3 ||$proyecto[0]['tp_id']==4) 
																		{
																			?>
																			<label><font size="1"><b>MONTO PRESUPUESTO</b></font></label>
																			<input <input class="form-control" type="text" name="mtotal" id="mtotal" placeholder="0" value="<?php echo $id_f[0]['pfec_ptto_fase'] ?>"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"  onpaste="return false">
																			<?php
																		}
																	?>
																</div>
															</div>
															<div class="col-sm-6">
																<div class="form-group">
																	<label><font size="1"><b>PONDERACI&Oacute;N</b></font></label>
																	<input class="form-control" type="number" name="pn_cion" id="pn_cion" value="<?php echo $proyecto[0]['proy_ponderacion'] ?>" placeholder="XX %" onkeypress="if (this.value.length < 10) { return soloNumeros(event);}else{return false; }"  onpaste="return false" disabled="true">
																</div>
															</div>
														</div>

														<div class="row">
															<div class="col-sm-6">
																<div class="form-group">
																	<label><font size="1"><b>FECHA INICIAL</b></font></label>
																	<div class="input-group">
																			<input type="text" name="f_ini" id="f_ini" placeholder="Seleccione Fecha inicial" class="form-control datepicker" value="<?php echo date('d/m/Y',strtotime($proyecto[0]['f_inicial'])); ?>" data-dateformat="dd/mm/yy" disabled="true">
																			<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
																	</div>
																</div>
															</div>

															<div class="col-sm-6">
																<div class="form-group">
																	<label><font size="1"><b>FECHA FINAL</b></font></label> 
																	<div class="input-group">
																			<input type="text" name="f_final" id="f_final" placeholder="Seleccione Fecha final" class="form-control datepicker" value="<?php echo date('d/m/Y',strtotime($proyecto[0]['f_final'])); ?>" data-dateformat="dd/mm/yy" disabled="true">
																			<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
																	</div>
																</div>
															</div>
														</div>

													</div> <!-- well -->
													</div><!-- id="bootstrap-wizard-1" class="col-sm-8" -->
													<div id="bootstrap-wizard-1" class="col-sm-4">
														<div class="well">
															<div class="row">
																<center><b><?php echo strtoupper($proyecto[0]['tipo']) ?></b></center><br>

																<div class="col-sm-12">
																	<div class="form-group">
																		<label><font size="1"><b>PROGRAMA</b></font></label>
																		<input class="form-control" type="text" value="<?php echo $proyecto[0]['aper_programa'].' - '.$aper[0]['aper_descripcion'] ?>" disabled="true">
																		<input class="form-control" type="hidden" name="prog" id="prog" value="<?php echo $proyecto[0]['aper_programa'] ?>" >
																		<input class="form-control" type="hidden" name="proy_a" id="proy_a" value="<?php echo $proyecto[0]['aper_proyecto'] ?>" >
																		<input class="form-control" type="hidden" name="act_a" id="act_a" value="<?php echo $proyecto[0]['aper_actividad'] ?>" >
																		<input class="form-control" type="hidden" name="tp_id" id="tp_id" value="<?php echo $proyecto[0]['tp_id'] ?>" >
																	</div>
																</div>
																<div class="col-sm-12">
																	<div class="form-group">
																		<label><font size="1"><b>PROYECTO</b></font></label>
																		<input class="form-control" type="text" value="<?php echo $proyecto[0]['aper_proyecto'] ?>" data-mask="9999" data-mask-placeholder= "9" disabled="true">
																		
																	</div>
																</div>
																<div class="col-sm-12">
																	<div class="form-group">
																		<label><font size="1"><b>ACTIVIDAD</b></font></label>
																		<input class="form-control" type="text"value="<?php echo $proyecto[0]['aper_actividad'] ?>" data-mask="999" data-mask-placeholder= "9" disabled="true">
																	</div>
																</div>
															</div>
														</div>
													</div> <!-- id="bootstrap-wizard-1" class="col-sm-4" -->

												</div><!-- class="widget-body" -->
												<div class="form-actions">
												<?php  
												if($mod==1)
												{
													?>
													<a href="<?php echo base_url().'index.php/admin/proy/list_proy' ?>" class="btn btn-lg btn-default" title="VOLVER A MIS PROYECTOS"> CANCELAR </a>
													<?php
												}
												elseif($mod==4)
												{
													?>
													<a href="<?php echo base_url().'index.php/admin/sgp/list_proy' ?>" class="btn btn-lg btn-default" title="VOLVER A MIS PROYECTOS"> CANCELAR </a>
													<?php
												}
												?>
											
											<input type="button" value="SIGUIENTE" id="btsubmit" class="btn btn-primary btn-lg" onclick="valida_envia()" title="GUARDAR Y REGISTRAR METAS DEL PROYECTO">
										</div>
										</div> <!-- class="jarviswidget jarviswidget-color-darken" -->
										
									</form>
								</article>
							<!-- end widget -->
					</div>
				
					<!-- end row -->
				</section>
				
			</div>
			<!-- END MAIN CONTENT -->

		</div>
		<!-- END MAIN PANEL -->

		<!-- PAGE FOOTER -->
		<div class="page-footer"> 
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->

		<!--================================================== -->
		<script>
            function valida_envia()
            { 
                if (document.formulario.nom_proy.value=="") /////// Nombre de Proyecto
                  { 
                      alert("Ingrese Campo PROYECTO / ACTIVIDAD") 
                      document.formulario.nom_proy.focus() 
                      return 0; 
                  }

                  if("<?php echo  $proyecto[0]['tp_id']?>"==2 || "<?php echo  $proyecto[0]['tp_id']?>"==3 || "<?php echo  $proyecto[0]['tp_id']?>"==4 )
                  {
                  	  if (document.formulario.mtotal.value=="") /////// Presupuesto
	                  { 
	                      alert("Ingrese Campo MONTO PRESUPUESTO") 
                      	  document.formulario.mtotal.focus() 
	                      return 0; 
	                  }
                  }
                	var OK = confirm("GUARDAR MODIFICACIONES DEL PROYECTO ?");
						if (OK) {
							document.formulario.submit();
							document.getElementById("btsubmit").value = "SIGUIENTE...";
							document.getElementById("btsubmit").disabled = true;
							return true;  
					}
            }
        </script>

		<script>
			if (!window.jQuery) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-2.0.2.min.js"><\/script>');
			}
		</script>

		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>

		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url(); ?>assets/js/session_time/jquery-idletimer.js"></script>
		<script src = "<?php echo base_url(); ?>mis_js/control_session.js"></script>
		<script src="<?php echo base_url();?>/assets/js/app.config.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/mis_js/validacion_form.js"></script>
		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url();?>/assets/js/bootstrap/bootstrap.min.js"></script>
		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url();?>/assets/js/notification/SmartNotification.min.js"></script>
		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url();?>/assets/js/smartwidgets/jarvis.widget.min.js"></script>
		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url();?>/assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<!-- SPARKLINES -->
		<script src="<?php echo base_url();?>/assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/select2/select2.min.js"></script>
		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
		<!-- browser msie issue fix -->
		<script src="<?php echo base_url();?>/assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url();?>/assets/js/plugin/fastclick/fastclick.min.js"></script>
		<!-- Demo purpose only -->
		<script src="<?php echo base_url();?>/assets/js/demo.min.js"></script>
		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url();?>/assets/js/app.min.js"></script>
		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<!-- Voice command : plugin -->
		<script src="<?php echo base_url();?>/assets/js/speech/voicecommand.min.js"></script>
		<!-- PAGE RELATED PLUGIN(S) -->
		<script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
		<script src="<?php echo base_url();?>/assets/js/plugin/fuelux/wizard/wizard.min.js"></script>
		<script type="text/javascript">
		// DO NOT REMOVE : GLOBAL FUNCTIONS!
		
		$(document).ready(function() {
			
			pageSetUp();
			//Bootstrap Wizard Validations
		})
		</script>
	</body>
</html>
