<!DOCTYPE html>
<html lang="en-us">
  <head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    <title><?php echo $this->session->userdata('name')?></title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- Basic Styles -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
    <!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
    <!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
    <!-- FAVICONS -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
    <style type="text/css">
      aside{background: #05678B;}
      
    </style>
  </head>
  <body class="">
    <!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->
    <!-- HEADER -->
    <header id="header">
      <div id="logo-group">
        <!-- <span id="logo"> <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="SmartAdmin"> </span> -->
      </div>

      <!-- pulled right: nav area -->
      <div class="pull-right">
        
        <!-- collapse menu button -->
        <div id="hide-menu" class="btn-header pull-right">
          <span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
        </div>
        <!-- end collapse menu -->
        <!-- logout button -->
        <div id="logout" class="btn-header transparent pull-right">
          <span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Sign Out" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
        </div>
        <!-- end logout button -->
        <!-- search mobile button (this is hidden till mobile view port) -->
        <div id="search-mobile" class="btn-header transparent pull-right">
          <span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
        </div>
        <!-- end search mobile button -->
        <!-- fullscreen button -->
        <div id="fullscreen" class="btn-header transparent pull-right">
          <span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen"><i class="fa fa-arrows-alt"></i></a> </span>
        </div>
        <!-- end fullscreen button -->
      </div>
      <!-- end pulled right: nav area -->

    </header>
    <!-- END HEADER -->
    <!-- Left panel : Navigation area -->
    <aside id="left-panel">
      <!-- User info -->
      <div class="login-info">
        <span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
          <a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
              <span>
                  <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
              </span>
          </a> 
        </span>
      </div>

      <nav>
        <ul>
          <li>
            <a href='<?php echo site_url("admin").'/dashboard'; ?>' title="MENU PRINCIPAL"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
          </li>
          <li class="text-center">
            <a href="<?php echo base_url().'index.php/admin/proy/mis_proyectos/'.$mod ?>" title="PROGRAMACION -> MIS PROYECTOS"> <span class="menu-item-parent">PROGRAMACI&Oacute;N F&Iacute;SICA</span></a>
          </li>
          <?php
          if($nro_fase==1){
              for($i=0;$i<count($enlaces);$i++)
              {
                ?>
                 <li>
                        <a href="#" >
                          <i class="<?php echo $enlaces[$i]['o_image'];?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
                        <ul >
                        <?php
                        $submenu= $this->menu_modelo->get_Modulos_sub($enlaces[$i]['o_child']);
                    foreach($submenu as $row) {
                        ?>
                        <li><a href="<?php echo base_url($row['o_url'])."/".$mod."/".$id_f[0]['id']."/".$id_f[0]['proy_id']; ?>"><?php echo $row['o_titulo']; ?></a></li>
                        <?php } ?>
                        </ul>
                    </li>
                <?php
              }
          }
          ?>
        </ul>
      </nav>
      <span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>
    </aside>

    <!-- MAIN PANEL -->
    <div id="main" role="main">
      <!-- RIBBON -->
      <div id="ribbon">
        <!-- breadcrumb -->
          <ol class="breadcrumb">
          <?php
          if($mod==1)
            {
            ?>
            <li><a href="<?php echo base_url().'index.php/admin/proy/list_proy' ?>" title="VOLVER A MIS PROYECTOS">Mis Operaciones</a></li><li><a href="#" title="MIS COMPONENTES">Ejecuci&oacute;n Fisica (Anual)</a></li><li>Productos (Ejecutar)</li>
            <?php
            }
          elseif ($mod==4) 
            {
            ?>
            <li><a href="<?php echo base_url().'index.php/admin/sgp/list_proy' ?>" title="VOLVER A MIS PROYECTOS">Gerencia de Proyectos</a></li><li><a href="#" title="MIS COMPONENTES">Ejecuci&oacute;n Fisica (Multi Anual)</a></li><li>Productos (Ejecutar)</li>
            <?php
            }
          ?>                         
          </ol>
      </div>
      <!-- END RIBBON -->

				<?php
					$attributes = array('class' => 'form-horizontal', 'id' => 'formulario','name' =>'formulario','enctype' => 'multipart/form-data');
					echo validation_errors();
					echo form_open('admin/prog/valida_ejec_prod', $attributes);
				?>			
			<!-- MAIN CONTENT -->
			<div id="content">
        <section id="widget-grid" class="well">
          <div class="">
            <h1> PRODUCTO : <small><?php echo $producto[0]['prod_producto'] ?></small></h1>
            <h1> INDICADOR : <small><?php echo $producto[0]['indi_descripcion'] ?></small></h1>
            <h1> LINEA BASE : <small><?php echo round($producto[0]['prod_linea_base'],2) ?></small></h1>
            <h1> META : <small><?php echo round($producto[0]['prod_meta'],2) ?></small></h1>
          </div>
        </section>
				<section id="widget-grid" class="">
					<div class="row">

						<form  name="formulario" id="formulario" method="post">

						<input class="form-control" type="hidden" name="id_f" id="id_f" value="<?php echo $id_f[0]['id'];?>">
						<input class="form-control" type="hidden" name="id_p" id="id_p" value="<?php echo $id_f[0]['proy_id'];?>">
						<input class="form-control" type="hidden" name="id_pr" id="id_pr" value="<?php echo $producto[0]['prod_id'];?>">			
						<input class="form-control" type="hidden" name="gest" id="gest"value="<?php echo $gestion ?>">			
						<input class="form-control" type="hidden" name="mod" id="mod" value="<?php echo $mod ?>">	

							<?php 
								$nro=0;
								foreach($programado as $row)
                {
                    $nro++;
                    $matriz [1][$nro]=$row['m_id'];
                    $matriz [2][$nro]=$row['pg_fis'];
                }

                                $nro_e=0;
                                foreach($ejecutado as $row)
                                {
                                    $nro_e++;
                                    $matriz_e [1][$nro_e]=$row['m_id'];
                                    $matriz_e [2][$nro_e]=$row['pejec_fis'];
                                    $matriz_e [3][$nro_e]=$row['pejec_fis_a'];
                                    $matriz_e [4][$nro_e]=$row['pejec_fis_b'];
                                }

                                for($j = 1 ;$j<=12 ;$j++){
                                    $matriz_r[1][$j]=$j;
                                    $matriz_r[2][$j]='0'; /// programado
                                    $matriz_r[3][$j]='0'; /// ejecutado relativo a
                                    $matriz_r[4][$j]='0'; /// ejecutado relativo b
                                    $matriz_r[5][$j]='0'; /// ejecutado
                                }

                                for($i = 1 ;$i<=$nro ;$i++){
                                    for($j = 1 ;$j<=12 ;$j++)
                                    {
                                        if($matriz[1][$i]==$matriz_r[1][$j])
                                        {
                                            $matriz_r[2][$j]=round($matriz[2][$i],1);
                                        }
                                    }
                                }

                                if($producto[0]['indi_id']==1)
                                {
                                	for($i = 1 ;$i<=$nro_e ;$i++){
	                                    for($j = 1 ;$j<=12 ;$j++)
	                                    {
	                                        if($matriz_e[1][$i]==$matriz_r[1][$j])
	                                        {
	                                            $matriz_r[5][$j]=round($matriz_e[2][$i],1);
	                                        }
	                                    }
	                                }
                                }
                                elseif ($producto[0]['indi_id']==2) 
                                {
                                	if($producto[0]['prod_denominador']==0)
                                	{
                                		for($i = 1 ;$i<=$nro_e ;$i++){
		                                    for($j = 1 ;$j<=12 ;$j++)
		                                    {
		                                        if($matriz_e[1][$i]==$matriz_r[1][$j])
		                                        {
		                                            $matriz_r[3][$j]=round($matriz_e[3][$i],1);
		                                            $matriz_r[4][$j]=round($matriz_e[4][$i],1);
		                                            $matriz_r[5][$j]=round((($matriz_r[4][$j]/$matriz_r[3][$j])*$matriz_r[2][$j]),1);
		                                        }
		                                    }
		                                }
                                	}
                                	if ($producto[0]['prod_denominador']==1)
                                	{
                                		for($i = 1 ;$i<=$nro_e ;$i++){
		                                    for($j = 1 ;$j<=12 ;$j++)
		                                    {
		                                        if($matriz_e[1][$i]==$matriz_r[1][$j])
		                                        {
		                                            $matriz_r[3][$j]=round($matriz_e[3][$i],1);
		                                            $matriz_r[4][$j]=round($matriz_e[4][$i],1);
		                                            $matriz_r[5][$j]=round((($matriz_r[4][$j]/$matriz_r[3][$j])*100),1);
		                                        }
		                                    }
		                                }
                                	}
                                	
                                }
                                	
                                /*for($j = 1 ;$j<=5 ;$j++){
						            for($i=1;$i<=12;$i++){
						                echo "[".$matriz_r[$j][$i]."]";
						            }
						            echo'<br>';
						        }*/
                                
							?>
							<!-- well -->
							<div class="well">
								<!-- row -->
								<div class="row">
										<!-- row -->
										<p class="alert alert-info">
											<strong>GESTI&Oacute;N <?php echo $gestion?></strong>
										</p>
											<div class="col-md-4">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th ></th>
															<th >Enero</th>
															<th >Febrero</th>
															<th >Marzo</th>
															<th >Abril</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td><b>P</b></td>
															<input  name="p1" type="hidden" value="<?php echo $matriz_r[2][1]; ?>" >
															<td><input  class="form-control" type="text" value="<?php echo $matriz_r[2][1]; ?>" disabled="true"></td>
															
															<input  name="p2" class="form-control" type="hidden" value="<?php echo $matriz_r[2][2]; ?>" >
															<td><input  class="form-control" type="text" value="<?php echo $matriz_r[2][2]; ?>" disabled="true"></td>
															
															<input  name="p3" class="form-control" type="hidden" value="<?php echo $matriz_r[2][3]; ?>">
															<td><input class="form-control" type="text" value="<?php echo $matriz_r[2][3]; ?>" disabled="true"></td>
															
															<input  name="p4" class="form-control" type="hidden" value="<?php echo $matriz_r[2][4]; ?>">
															<td><input  class="form-control" type="text" value="<?php echo $matriz_r[2][4]; ?>" disabled="true"></td>
														</tr>
														<?php 
															if($producto[0]['indi_id']==2)
															{
																?>
															<tr>
																<td><b>E:A</b></td>
																<td><input  name="a1" class="form-control" type="text" value="<?php echo $matriz_r[3][1]; ?>" onkeydown="return onlynumber(event);"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }" ></td>
																<td><input  name="a2" class="form-control" type="text" value="<?php echo $matriz_r[3][2]; ?>" onkeydown="return onlynumber(event);"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }" ></td>
																<td><input  name="a3" class="form-control" type="text" value="<?php echo $matriz_r[3][3]; ?>" onkeydown="return onlynumber(event);"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }" ></td>
																<td><input  name="a4" class="form-control" type="text" value="<?php echo $matriz_r[3][4]; ?>" onkeydown="return onlynumber(event);"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }" ></td>
															</tr>
															<tr>
																<td><b>E:B</b></td>
																<td><input  name="b1" class="form-control" type="text" value="<?php echo $matriz_r[4][1]; ?>" onkeydown="return onlynumber(event);"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }" ></td>
																<td><input  name="b2" class="form-control" type="text" value="<?php echo $matriz_r[4][2]; ?>" onkeydown="return onlynumber(event);"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }" ></td>
																<td><input  name="b3" class="form-control" type="text" value="<?php echo $matriz_r[4][3]; ?>" onkeydown="return onlynumber(event);"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }" ></td>
																<td><input  name="b4" class="form-control" type="text" value="<?php echo $matriz_r[4][4]; ?>" onkeydown="return onlynumber(event);"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }" ></td>
															</tr>
																<?php
															}
															elseif ($producto[0]['indi_id']==1)
															{
																?>
															<tr>
																<td><b>E</b></td>
																<td><input  name="e1" class="form-control" type="text" value="<?php echo $matriz_r[5][1]; ?>" onkeydown="return onlynumber(event);"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }" ></td>
																<td><input  name="e2" class="form-control" type="text" value="<?php echo $matriz_r[5][2]; ?>" onkeydown="return onlynumber(event);"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }" ></td>
																<td><input  name="e3" class="form-control" type="text" value="<?php echo $matriz_r[5][3]; ?>" onkeydown="return onlynumber(event);"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }" ></td>
																<td><input  name="e4" class="form-control" type="text" value="<?php echo $matriz_r[5][4]; ?>" onkeydown="return onlynumber(event);"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }" ></td>
															</tr>
																<?php
															}
														?>
													</tbody>
												</table>
											</div>
				
											<div class="col-md-4">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th ></th>
															<th >Mayo</th>
															<th >Junio</th>
															<th >Julio</th>
															<th >Agosto</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td><b>P</b></td>
															<input  name="p5" class="form-control" type="hidden" value="<?php echo $matriz_r[2][5]; ?>" >
															<td><input class="form-control" type="text" value="<?php echo $matriz_r[2][5]; ?>" disabled="true"></td>
															
															<input  name="p6" class="form-control" type="hidden" value="<?php echo $matriz_r[2][6]; ?>" >
															<td><input  class="form-control" type="text" value="<?php echo $matriz_r[2][6]; ?>" disabled="true"></td>
															
															<input  name="p7" class="form-control" type="hidden" value="<?php echo $matriz_r[2][7]; ?>" >
															<td><input  class="form-control" type="text" value="<?php echo $matriz_r[2][7]; ?>" disabled="true"></td>
															
															<input  name="p8" class="form-control" type="hidden" value="<?php echo $matriz_r[2][8]; ?>" >
															<td><input class="form-control" type="text" value="<?php echo $matriz_r[2][8]; ?>" disabled="true"></td>
														</tr>
														<?php 
															if($producto[0]['indi_id']==2)
															{
																?>
															<tr>
																<td><b>E:A</b></td>
																<td><input  name="a5" class="form-control" type="text" value="<?php echo $matriz_r[3][5]; ?>" onkeydown="return onlynumber(event);"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"  ></td>
																<td><input  name="a6" class="form-control" type="text" value="<?php echo $matriz_r[3][6]; ?>" onkeydown="return onlynumber(event);"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"  ></td>
																<td><input  name="a7" class="form-control" type="text" value="<?php echo $matriz_r[3][7]; ?>" onkeydown="return onlynumber(event);"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"  ></td>
																<td><input  name="a8" class="form-control" type="text" value="<?php echo $matriz_r[3][8]; ?>" onkeydown="return onlynumber(event);"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"  ></td>
															</tr>
															<tr>
																<td><b>E:B</b></td>
																<td><input  name="b5" class="form-control" type="text" value="<?php echo $matriz_r[4][5]; ?>" onkeydown="return onlynumber(event);"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"  ></td>
																<td><input  name="b6" class="form-control" type="text" value="<?php echo $matriz_r[4][6]; ?>" onkeydown="return onlynumber(event);"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"  ></td>
																<td><input  name="b7" class="form-control" type="text" value="<?php echo $matriz_r[4][7]; ?>" onkeydown="return onlynumber(event);"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"  ></td>
																<td><input  name="b8" class="form-control" type="text" value="<?php echo $matriz_r[4][8]; ?>" onkeydown="return onlynumber(event);"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"  ></td>
															</tr>
																<?php
															}
															elseif ($producto[0]['indi_id']==1)
															{
																?>
															<tr>
																<td><b>E</b></td>
																<td><input  name="e5" class="form-control" type="text" value="<?php echo $matriz_r[5][5]; ?>" onkeydown="return onlynumber(event);"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"  ></td>
																<td><input  name="e6" class="form-control" type="text" value="<?php echo $matriz_r[5][6]; ?>" onkeydown="return onlynumber(event);"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"  ></td>
																<td><input  name="e7" class="form-control" type="text" value="<?php echo $matriz_r[5][7]; ?>" onkeydown="return onlynumber(event);"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"  ></td>
																<td><input  name="e8" class="form-control" type="text" value="<?php echo $matriz_r[5][8]; ?>" onkeydown="return onlynumber(event);"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"  ></td>
															</tr>
																<?php
															}
														?>
													</tbody>
												</table>
											</div>

											<div class="col-md-4">	
												<table class="table table-bordered">
													<thead>
														<tr>
															<th ></th>
															<th >Septiembre</th>
															<th >Octubre</th>
															<th >Noviembre</th>
															<th >Diciembre</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td><b>P</b></td>
															<input  name="p9" class="form-control" type="hidden" value="<?php echo $matriz_r[2][9]; ?>" >
															<td><input class="form-control" type="text" value="<?php echo $matriz_r[2][9]; ?>" disabled="true"></td>

															<input  name="p10" class="form-control" type="hidden" value="<?php echo $matriz_r[2][10]; ?>" >
															<td><input  class="form-control" type="text" value="<?php echo $matriz_r[2][10]; ?>" disabled="true"></td>

															<input  name="p11" class="form-control" type="hidden" value="<?php echo $matriz_r[2][11]; ?>" >
															<td><input class="form-control" type="text" value="<?php echo $matriz_r[2][11]; ?>" disabled="true"></td>
															
															<input  name="p12" class="form-control" type="hidden" value="<?php echo $matriz_r[2][12]; ?>" >
															<td><input class="form-control" type="text" value="<?php echo $matriz_r[2][12]; ?>" disabled="true"></td>
														</tr>
														<?php 
															if($producto[0]['indi_id']==2)
															{
																?>
															<tr>
																<td><b>E:A</b></td>
																<td><input  name="a9" class="form-control" type="text" value="<?php echo $matriz_r[3][9]; ?>" onkeydown="return onlynumber(event);"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"  ></td>
																<td><input  name="a10" class="form-control" type="text" value="<?php echo $matriz_r[3][10]; ?>" onkeydown="return onlynumber(event);"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"  ></td>
																<td><input  name="a11" class="form-control" type="text" value="<?php echo $matriz_r[3][11]; ?>" onkeydown="return onlynumber(event);"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"  ></td>
																<td><input  name="a12" class="form-control" type="text" value="<?php echo $matriz_r[3][12]; ?>" onkeydown="return onlynumber(event);"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"  ></td>
															</tr>
															<tr>
																<td><b>E:B</b></td>
																<td><input  name="b9" class="form-control" type="text" value="<?php echo $matriz_r[4][9]; ?>" onkeydown="return onlynumber(event);"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"  ></td>
																<td><input  name="b10" class="form-control" type="text" value="<?php echo $matriz_r[4][10]; ?>" onkeydown="return onlynumber(event);"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"  ></td>
																<td><input  name="b11" class="form-control" type="text" value="<?php echo $matriz_r[4][11]; ?>" onkeydown="return onlynumber(event);"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"  ></td>
																<td><input  name="b12" class="form-control" type="text" value="<?php echo $matriz_r[4][12]; ?>" onkeydown="return onlynumber(event);"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"  ></td>
															</tr>
																<?php
															}
															elseif ($producto[0]['indi_id']==1)
															{
																?>
															<tr>
																<td><b>E</b></td>
																<td><input  name="e9" class="form-control" type="text" value="<?php echo $matriz_r[5][9]; ?>" onkeydown="return onlynumber(event);"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"  ></td>
																<td><input  name="e10" class="form-control" type="text" value="<?php echo $matriz_r[5][10]; ?>" onkeydown="return onlynumber(event);"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"  ></td>
																<td><input  name="e11" class="form-control" type="text" value="<?php echo $matriz_r[5][11]; ?>" onkeydown="return onlynumber(event);"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"  ></td>
																<td><input  name="e12" class="form-control" type="text" value="<?php echo $matriz_r[5][12]; ?>" onkeydown="return onlynumber(event);"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"  ></td>
															</tr>
																<?php
															}
														?>
													</tbody>
												</table>
											</div>
											
									</div>
									<div class="form-actions">
										<a href="<?php echo base_url().'index.php/admin/prog/efisica/'.$mod.'/'.$id_f[0]['id']."/".$id_f[0]['proy_id'] ?>" class="btn btn-lg btn-default"> CANCELAR </a>
										<input type="button" value="VALIDAR EJECUCION" class="btn btn-primary btn-lg" onclick="valida_envia()" title="GUARDAR DATOS">
									</div>
								</form>
								<!-- end row -->
							</div>
							<!-- end well -->		
							<!-- end widget -->
					</div>
				</section>
				<!-- end widget grid -->					
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN PANEL -->

		<!-- PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->
		<script>
            function valida_envia()
            { 
              if(<?php echo $producto[0]['indi_id'] ?>==1)
              {
                if (document.formulario.e1.value=="" || document.formulario.e2.value=="" || document.formulario.e3.value=="" || document.formulario.e4.value=="" || document.formulario.e5.value=="" || document.formulario.e6.value=="" ||
                    document.formulario.e7.value=="" || document.formulario.e8.value=="" || document.formulario.e9.value=="" || document.formulario.e10.value=="" || document.formulario.e11.value=="" || document.formulario.e12.value=="")
                  { 
                      alert("Existe un campo vacio en la fila Ejecucion !!!!!!.. por favor verifique dato") 
                      return 0; 
                  }
              }

              if(<?php echo $producto[0]['indi_id'] ?>==2)
              {
                 if (document.formulario.a1.value=="" || document.formulario.a2.value=="" || document.formulario.a3.value=="" || document.formulario.a4.value=="" || document.formulario.a5.value=="" || document.formulario.a6.value=="" ||
                    document.formulario.a7.value=="" || document.formulario.a8.value=="" || document.formulario.a9.value=="" || document.formulario.a10.value=="" || document.formulario.a11.value=="" || document.formulario.a12.value=="")
                  { 
                      alert("Existe un campo vacio en la fila Ejecucion A !!!!!!.. por favor verifique dato") 
                      return 0; 
                  }
                  if (document.formulario.b1.value=="" || document.formulario.b2.value=="" || document.formulario.b3.value=="" || document.formulario.b4.value=="" || document.formulario.b5.value=="" || document.formulario.b6.value=="" ||
                    document.formulario.b7.value=="" || document.formulario.b8.value=="" || document.formulario.b9.value=="" || document.formulario.b10.value=="" || document.formulario.b11.value=="" || document.formulario.b12.value=="")
                  { 
                      alert("Existe un campo vacio en la fila Ejecucion B !!!!!!.. por favor verifique el dato ") 
                      return 0; 
                  }
              }

                var OK = confirm("TODO CORRECTO..VALIDAR EJECUCION EN EL PRODUCTO ?");
		            if (OK) {
		            document.formulario.submit(); 
		        }
            }
          </script>
		<script src="<?php echo base_url();?>/assets/js/libs/jquery-2.0.2.min.js"></script>

		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>

		<!-- IMPORTANT: APP CONFIG -->
    <script src="<?php echo base_url(); ?>assets/js/session_time/jquery-idletimer.js"></script>
    <script src = "<?php echo base_url(); ?>mis_js/control_session.js"></script>
		<script src="<?php echo base_url();?>/assets/js/app.config.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/mis_js/validacion_form.js"></script>
		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url();?>/assets/js/bootstrap/bootstrap.min.js"></script>
		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url();?>/assets/js/notification/SmartNotification.min.js"></script>
		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url();?>/assets/js/smartwidgets/jarvis.widget.min.js"></script>
		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url();?>/assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<!-- SPARKLINES -->
		<script src="<?php echo base_url();?>/assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/select2/select2.min.js"></script>
		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
		<!-- browser msie issue fix -->
		<script src="<?php echo base_url();?>/assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url();?>/assets/js/plugin/fastclick/fastclick.min.js"></script>
		<!-- Demo purpose only -->
		<script src="<?php echo base_url();?>/assets/js/demo.min.js"></script>
		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url();?>/assets/js/app.min.js"></script>
		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<!-- Voice command : plugin -->
		<script src="<?php echo base_url();?>/assets/js/speech/voicecommand.min.js"></script>
	</body>

</html>
