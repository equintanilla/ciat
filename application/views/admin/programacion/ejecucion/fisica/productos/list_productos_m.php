<!DOCTYPE html>
<html lang="en-us">
     <head>
          <meta charset="utf-8">
          <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

          <title><?php echo $this->session->userdata('name')?></title>
          <meta name="description" content="">
          <meta name="author" content="">
               
          <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
          <!-- Basic Styles -->
          <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
          <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
          <!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
          <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css">
          <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
          <!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
          <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
          <!-- FAVICONS -->
          <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
          <link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
          <!--estiloh-->
          <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
          <!--fin de stiloh-->
          <style type="text/css">
               aside{background: #05678B;}
          </style>
          <style>
      /*////scroll tablas/////*/
      table{font-size: 10px;
            width: 100%;
            max-width:1550px;;
            overflow-x: scroll;
            }
            th{
              padding: 1.4px;
              text-align: center;
              font-size: 10px;
            }
            td{
              padding: 1.4px;
              text-align: center;
              font-size: 10px;
            }
      </style>
     </head>
     <body class="">
          <!-- HEADER -->
          <header id="header">
               <div id="logo-group">
                    <!-- <span id="logo"> <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="SmartAdmin"> </span> -->
               </div>

               <!-- pulled right: nav area -->
               <div class="pull-right">
                    
                    <!-- collapse menu button -->
                    <div id="hide-menu" class="btn-header pull-right">
                         <span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
                    </div>
                    <!-- end collapse menu -->
                    <!-- logout button -->
                    <div id="logout" class="btn-header transparent pull-right">
                         <span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Sign Out" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
                    </div>
                    <!-- end logout button -->
                    <!-- search mobile button (this is hidden till mobile view port) -->
                    <div id="search-mobile" class="btn-header transparent pull-right">
                         <span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
                    </div>
                    <!-- end search mobile button -->
                    <!-- fullscreen button -->
                    <div id="fullscreen" class="btn-header transparent pull-right">
                         <span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen"><i class="fa fa-arrows-alt"></i></a> </span>
                    </div>
                    <!-- end fullscreen button -->
               </div>
               <!-- end pulled right: nav area -->
          </header>
          <!-- END HEADER -->
          <!-- Left panel : Navigation area -->
    <aside id="left-panel">
      <!-- User info -->
      <div class="login-info">
        <span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
          <a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
            <span>
                <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
            </span>
          </a> 
        </span>
      </div>

      <nav>
        <ul>
          <li>
            <a href='<?php echo site_url("admin").'/dashboard'; ?>' title="MENU PRINCIPAL"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
          </li>
          <li class="text-center">
            <a href="<?php echo base_url().'index.php/admin/proy/mis_proyectos/'.$mod ?>" title="PROGRAMACION -> MIS PROYECTOS"> <span class="menu-item-parent">PROGRAMACI&Oacute;N F&Iacute;SICA</span></a>
          </li>
          <?php
          if($nro_fase==1){
              for($i=0;$i<count($enlaces);$i++)
              {
                ?>
                 <li>
                        <a href="#" >
                          <i class="<?php echo $enlaces[$i]['o_image'];?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
                        <ul >
                        <?php
                        $submenu= $this->menu_modelo->get_Modulos_sub($enlaces[$i]['o_child']);
                    foreach($submenu as $row) {
                        ?>
                        <li><a href="<?php echo base_url($row['o_url'])."/".$mod."/".$id_f[0]['id']."/".$id_f[0]['proy_id']; ?>"><?php echo $row['o_titulo']; ?></a></li>
                        <?php } ?>
                        </ul>
                    </li>
                <?php
              }
          }
          ?>
        </ul>
      </nav>
      <span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>
    </aside>

      <!-- MAIN PANEL -->
      <div id="main" role="main">

               <!-- RIBBON -->
               <div id="ribbon">

                    <span class="ribbon-button-alignment"> 
                         <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                              <i class="fa fa-refresh"></i>
                         </span> 
                    </span>

                    <!-- breadcrumb -->
                    <ol class="breadcrumb">                         
                      <li><a href="<?php echo base_url().'index.php/admin/sgp/list_proy' ?>" title="VOLVER A MIS PROYECTOS">Gerencia de Proyectos</a></li><li><a href="#" title="MIS COMPONENTES">Ejecuci&oacute;n Fisica (Multi Anual)</a></li><li>Productos</li>
                    </ol>
               </div>
               <!-- END RIBBON -->
        <!-- MAIN CONTENT -->
        <div id="content">
              <section id="widget-grid" class="well">
                <div class="">
                  <h1> <?php echo $titulo_proy;?> : <small><?php echo $proyecto[0]['proy_nombre']?></small></h1>
                  <h1> CATEGORIA PROGRAM&Aacute;TICA : <small><?php echo $proyecto[0]['aper_programa'].''.$proyecto[0]['aper_proyecto'].''.$proyecto[0]['aper_actividad']?></small>
                  <h1><small>EJECUCI&Oacute;N F&Iacute;SICA DE PRODUCTOS</small></h1>
                  </div>
              </section>

                        <?php 
                        if($this->model_componente->componentes_nro($id_f[0]['id'])!=0)
                        {
                          $comp = $this->model_componente->componentes_id($id_f[0]['id']);
                            foreach($comp as $rowc)
                            {   
                              $prod= $this->model_producto->list_prod($rowc['com_id']);
                                ?>
                                <section id="widget-grid" class="">
                                  <div class="well">
                                  <font size="2"><b>
                                      <?php 
                                        if($proyecto[0]['tp_id']==1 || $proyecto[0]['tp_id']==2 || $proyecto[0]['tp_id']==3)
                                        {
                                          ?>
                                          <h2 class="alert alert-success">COMPONENTE : <?php echo $rowc['com_componente'] ?></h2>
                                          <?php
                                        }
                                      ?> PRODUCTOS GESTI&Oacute;N : <?php echo $this->session->userdata("gestion");?>
                                  </b></font>
                                    <table  class="table  table-bordered table-hover" style="width:100%;">
                                        <thead>                             
                                            <tr>
                                              <td style="width:1%;" bgcolor="#568a89"><font size="1" color="#ffffff"><b>#</b></font></td>
                                              <td style="width:2%;" bgcolor="#568a89"><font size="1" color="#ffffff"><b>ACTIVIDADES</b></font></td>
                                              <td style="width:4%;" bgcolor="#568a89"><font size="1" color="#ffffff"><b>OBJETIVO DEL PRODUCTO</b></font></td>
                                              <td style="width:1%;" bgcolor="#568a89"><font size="1" color="#ffffff"><b>TIPO DE INDICADOR</b></font></td>
                                              <td style="width:1%;" bgcolor="#568a89"><font size="1" color="#ffffff"><b>LINEA BASE</b></font></td>
                                              <td style="width:1%;" bgcolor="#568a89"><font size="1" color="#ffffff"><b>META TOTAL</b></font></td>
                                              <td style="width:2%;" bgcolor="#568a89"><font size="1" color="#ffffff"><b>PONDERACI&Oacute;N</b></font></td>
                                              <td style="width:25%;" bgcolor="#568a89"><font size="1" color="#ffffff"><b>TEMPORALIZACI&Oacute;N DE LAS METAS</b></font></td>

                                            </tr>
                                          </thead>
                                          <tbody>
                                          <?php $nrop=1; $ti='';
                                            foreach($prod as $rowp)
                                            { if($rowp['indi_id']==2){$ti='%';} 
                                              $años=$id_f[0]['pfec_fecha_fin']-$id_f[0]['pfec_fecha_inicio']+1;
                                              echo '<tr>';
                                                  echo '<td>'.$nrop.'</td>';
                                                  echo '<td>';
                                                  ?>
                                                   <center><a href='<?php echo site_url("admin").'/prog/efisica_a/'.$mod.'/'.$id_f[0]['id'].'/'.$id_f[0]['proy_id'].'/'.$rowc['com_id'].'/'.$rowp['prod_id']; ?>' title="EJECUTAR ACTIVIDADES"><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span></i><br>EJECUTAR ACTIVIDAD</a></center>
                                                  <?php 
                                                  echo '</td>';
                                                  echo '<td>'.$rowp['prod_producto'].'</td>';
                                                  echo '<td>'.$rowp['indi_abreviacion'].'</td>';
                                                  echo '<td>'.round($rowp['prod_linea_base'],1).' '.$ti.'</td>';
                                                  echo '<td>'.round($rowp['prod_meta'],1).' '.$ti.'</td>';
                                                  echo '<td>'.round($rowp['prod_ponderacion'],1).' %</td>';
                                                  echo '<td>';
                                                  $pa=0; $pe=0; $gestion=$id_f[0]['pfec_fecha_inicio'];
                                                            for($k=1;$k<=$años;$k++)
                                                            { 
                                                              $programado=$this->model_producto->prod_prog_mensual($rowp['prod_id'],$gestion); /// programado
                                                              $ejecutado=$this->model_producto->prod_ejec_mensual($rowp['prod_id'],$gestion); /// ejecutado
                                                              
                                                        /*------------------- programado gestion ------------------*/
                                                              $nro=0;
                                                              foreach($programado as $row)
                                                              {
                                                                $nro++;
                                                                $matriz [1][$nro]=$row['m_id'];
                                                                $matriz [2][$nro]=$row['pg_fis'];
                                                              }
                                                        /*--------------------------------------------------------*/
                                                        /*---------------- llenando la matriz vacia --------------*/
                                                              for($j = 1; $j<=12; $j++)
                                                              {
                                                                $matriz_r[1][$j]=$j;
                                                                $matriz_r[2][$j]='0';
                                                                $matriz_r[3][$j]='0';
                                                                $matriz_r[4][$j]='0';
                                                                $matriz_r[5][$j]='0';
                                                                $matriz_r[6][$j]='0';
                                                                $matriz_r[7][$j]='0';
                                                                $matriz_r[8][$j]='0';
                                                                $matriz_r[9][$j]='0';
                                                                $matriz_r[10][$j]='0';
                                                              }
                                                        /*--------------------------------------------------------*/
                                                        /*--------------------ejecutado gestion ------------------*/
                                                              $nro_e=0;
                                                              foreach($ejecutado as $row)
                                                              {
                                                                  $nro_e++;
                                                                  $matriz_e [1][$nro_e]=$row['m_id'];
                                                                  $matriz_e [2][$nro_e]=$row['pejec_fis'];
                                                                  $matriz_e [3][$nro_e]=$row['pejec_fis_a'];
                                                                  $matriz_e [4][$nro_e]=$row['pejec_fis_b'];
                                                              }
                                                        /*--------------------------------------------------------*/
                                                        /*------- asignando en la matriz P, PA, %PA ----------*/
                                                              for($i = 1 ;$i<=$nro ;$i++)
                                                              {
                                                                for($j = 1 ;$j<=12 ;$j++)
                                                                {
                                                                  if($matriz[1][$i]==$matriz_r[1][$j])
                                                                  {
                                                                    $matriz_r[2][$j]=round($matriz[2][$i],2);
                                                                  }
                                                                }
                                                              }

                                                              for($j = 1 ;$j<=12 ;$j++){
                                                                $pa=$pa+$matriz_r[2][$j];
                                                                $matriz_r[3][$j]=$pa+$rowp['prod_linea_base'];
                                                                $matriz_r[4][$j]=round(((($pa+$rowp['prod_linea_base'])/$rowp['prod_meta'])*100),2);
                                                              }

                                                              if($rowp['indi_id']==1)
                                                              {
                                                                for($i = 1 ;$i<=$nro_e ;$i++){
                                                                    for($j = 1 ;$j<=12 ;$j++)
                                                                    {
                                                                        if($matriz_e[1][$i]==$matriz_r[1][$j])
                                                                        {
                                                                            $matriz_r[7][$j]=round($matriz_e[2][$i],2);
                                                                        }
                                                                    }
                                                                }
                                                              }
                                                              elseif ($rowp['indi_id']==2) 
                                                              {
                                                                for($i = 1 ;$i<=$nro_e ;$i++){
                                                                      for($j = 1 ;$j<=12 ;$j++)
                                                                      {
                                                                          if($matriz_e[1][$i]==$matriz_r[1][$j])
                                                                          {
                                                                              $matriz_r[5][$j]=round($matriz_e[3][$i],2);
                                                                              $matriz_r[6][$j]=round($matriz_e[4][$i],2);
                                                                              $matriz_r[7][$j]=round($matriz_e[2][$i],2);
                                                                          }
                                                                      }
                                                                  }
                                                              /*--------------------------------------------------------*/
                                                              }
                                                              /*--------------------matriz E,AE,%AE gestion ------------------*/
                                                              for($j = 1 ;$j<=12 ;$j++){
                                                                $pe=$pe+$matriz_r[7][$j];
                                                                $matriz_r[8][$j]=$pe+$rowp['prod_linea_base'];
                                                                $matriz_r[9][$j]=round(((($pe+$rowp['prod_linea_base'])/$rowp['prod_meta'])*100),2);
                                                                if($matriz_r[4][$j]!=0)
                                                                {$matriz_r[10][$j]=round((($matriz_r[9][$j]/$matriz_r[4][$j])*100),2);}
                                                                
                                                                }
                                                              /*--------------------------------------------------------*/
                                                            echo '<table class="table  table-bordered table-hover">
                                                                        <thead>                        
                                                                            <tr>';
                                                                            $v='GESTI&Oacute;N'; $color='#008080';
                                                                            if($gestion==$this->session->userdata("gestion")){$v='GESTI&Oacute;N ACTUAL'; $color='#000000';}
                                                                              ?>
                                                                                <td style="width:1%;" bgcolor="<?php echo $color;?>" colspan='13'><font color="#ffffff"><?php echo $v.' '.$gestion ?></font>
                                                                                    <a href='<?php echo site_url("admin").'/prog/ejec_prod/'.$mod.'/'.$id_f[0]['id']."/".$id_f[0]['proy_id'].'/'.$rowp['prod_id'].'/'.$gestion; ?>' title="ACTUALIZAR EJECUCION"><img src="<?php echo base_url(); ?>assets/Iconos/table_refresh.png" WIDTH="20" HEIGHT="20"/></a>
                                                                                </td>
                                                                              <?php 
                                                                              echo'
                                                                            </tr>
                                                                            <tr bgcolor="#F5F5DC">
                                                                                <td style="width:1%;"></td>
                                                                                <td style="width:1%;"><center>Enero</center></td>
                                                                                <td style="width:1%;"><center>Febrero</center></td>
                                                                                <td style="width:1%;"><center>Marzo</center></td>
                                                                                <td style="width:1%;"><center>&nbsp;&nbsp;Abril&nbsp;&nbsp;</center></td>
                                                                                <td style="width:1%;"><center>&nbsp;Mayo&nbsp;</center></td>
                                                                                <td style="width:1%;"><center>&nbsp;Junio&nbsp;</center></td>
                                                                                <td style="width:1%;"><center>&nbsp;&nbsp;Julio&nbsp;&nbsp;</center></td>
                                                                                <td style="width:1%;"><center>&nbsp;Agosto&nbsp;</center></td>
                                                                                <td style="width:1%;"><center>Septiembre</center></td>
                                                                                <td style="width:1%;"><center>Octubre</center></td>
                                                                                <td style="width:1%;"><center>Noviembre</center></td>
                                                                                <td style="width:1%;"><center>Diciembre</center></td>
                                                                            </tr>
                                                                            <tr bgcolor="#F5F5DC">
                                                                                <td style="width:1%;">PROGRAMADO</td>';
                                                                                for($i = 1 ;$i<=12 ;$i++)
                                                                                {
                                                                                  echo '<td><center>'.$matriz_r[2][$i].'</center></td>';
                                                                                }
                                                                            echo'
                                                                            </tr>
                                                                           
                                                                            <tr bgcolor="#F5F5DC">
                                                                                <td style="width:1%;">PROGRAMADO<BR>ACUMULADO</td>';
                                                                                for($i = 1 ;$i<=12 ;$i++)
                                                                                {
                                                                                  echo '<td><center>'.$matriz_r[3][$i].' '.$ti.'</center></td>';
                                                                                }
                                                                            echo'
                                                                            </tr>
                                                                            <tr bgcolor="#F5F5DC">
                                                                                <td style="width:1%;">%PROGRAMADO<BR>ACUMULADO</td>';
                                                                                for($i = 1 ;$i<=12 ;$i++)
                                                                                {
                                                                                  echo '<td><center>'.$matriz_r[4][$i].' %</center></td>';
                                                                                }
                                                                            echo'
                                                                            </tr>';
                                                                            if($rowp['indi_id']==2)
                                                                            {
                                                                            echo'<tr bgcolor="#98FB98">
                                                                                <td bgcolor="#98FB98"style="width:1%;">A</td>';
                                                                                for($i = 1 ;$i<=12 ;$i++)
                                                                                {
                                                                                  echo '<td><center>'.$matriz_r[5][$i].' '.$ti.'</center></td>';
                                                                                }
                                                                            echo'
                                                                            </tr>
                                                                            <tr bgcolor="#F5F5DC">
                                                                                <td bgcolor="#98FB98" style="width:1%;">B</td>';
                                                                                for($i = 1 ;$i<=12 ;$i++)
                                                                                {
                                                                                  echo '<td><center>'.$matriz_r[6][$i].' '.$ti.'</center></td>';
                                                                                }
                                                                            echo'
                                                                            </tr>';
                                                                            }
                                                                            echo'<tr bgcolor="#F5F5DC">
                                                                                <td bgcolor="#98FB98" style="width:1%;">EJECUTADO</td>';
                                                                                for($i = 1 ;$i<=12 ;$i++)
                                                                                {
                                                                                  echo '<td bgcolor="#98FB98"><center>'.$matriz_r[7][$i].' '.$ti.'</center></td>';
                                                                                }
                                                                            echo'
                                                                            </tr>
                                                                            <tr bgcolor="#F5F5DC">
                                                                                <td bgcolor="#98FB98" style="width:1%;">EJECUTADO<BR>ACUMULADO</td>
                                                                                ';
                                                                                for($i = 1 ;$i<=12 ;$i++)
                                                                                {
                                                                                  echo '<td bgcolor="#98FB98"><center>'.$matriz_r[8][$i].' '.$ti.'</center></td>';
                                                                                }
                                                                            echo'
                                                                            </tr>
                                                                            <tr bgcolor="#F5F5DC">
                                                                                <td bgcolor="#98FB98" style="width:1%;">%EJECUTADO<BR>ACUMULADO</td>';
                                                                                for($i = 1 ;$i<=12 ;$i++)
                                                                                {
                                                                                  echo '<td bgcolor="#98FB98"><center>'.$matriz_r[9][$i].' %</center></td>';
                                                                                }
                                                                            echo'
                                                                            </tr>
                                                                            <tr bgcolor="#F5F5DC">
                                                                                <td bgcolor="#B0E0E6" style="width:1%;">EFICACIA</td>';
                                                                                for($i = 1 ;$i<=12 ;$i++)
                                                                                {
                                                                                  echo '<td bgcolor="#B0E0E6"><center>'.$matriz_r[10][$i].' %</center></td>';
                                                                                }
                                                                            echo'
                                                                            </tr>
                                                                            
                                                              </thead>';
                                                              echo'</table><br>';
                                                              $gestion++;
                                                            }
                                                  echo '</td>';
                                              echo '</tr>';
                                              $nrop++;      
                                            }
                                          ?>            
                                          </tbody>
                                    </table>
                                  </div> 
                                </section>
                                <?php
                            }
                        }
                        else
                        {
                          echo "No se tiene componentes registrados";
                        } 

                        ?> 
                         

        <!--///////////fin de tabla///////-->
    </div>
    <!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->

<!-- PAGE FOOTER -->
<div class="page-footer">
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
        </div>
    </div>
</div>
<!-- END PAGE FOOTER -->
<!--================================================== -->

<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
<script data-pace-options='{ "restartOnRequestAfter": true }'
        src="<?php echo base_url(); ?>assets/js/plugin/pace/pace.min.js"></script>
<script>
    if (!window.jQuery) {
        document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-2.0.2.min.js"><\/script>');
    }
</script>
<script>
    if (!window.jQuery.ui) {
        document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
    }
</script>
<!-- IMPORTANT: APP CONFIG -->
<script src="<?php echo base_url(); ?>assets/js/session_time/jquery-idletimer.js"></script>
<script src = "<?php echo base_url(); ?>mis_js/control_session.js"></script>
<script src="<?php echo base_url(); ?>assets/js/app.config.js"></script>

<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script>
<!--tablas-->
<!-- FastClick: For mobile devices -->
<script src="<?php echo base_url(); ?>assets/js/tabla/dataTables.bootstrap.min.js"></script>
<!-- FastClick: For mobile devices -->
<script src="<?php echo base_url(); ?>assets/js/tabla/jquery.dataTables.min.js"></script>
<!-- FastClick: For mobile devices -->
<script src="<?php echo base_url(); ?>assets/js/tabla/jquery-1.12.3.js"></script>
<!--fin tablas-->
<!-- BOOTSTRAP JS -->
<script src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.min.js"></script>
<!-- CUSTOM NOTIFICATION -->
<script src="<?php echo base_url(); ?>assets/js/notification/SmartNotification.min.js"></script>
<!--  JARVIS WIDGETS -->
<script src="<?php echo base_url(); ?>assets/js/smartwidgets/jarvis.widget.min.js"></script>
<!-- JQUERY VALIDATE -->
<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
<!-- JQUERY MASKED INPUT -->
<script src="<?php echo base_url(); ?>assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
<!-- JQUERY SELECT2 INPUT -->
<script src="<?php echo base_url(); ?>assets/js/plugin/select2/select2.min.js"></script>
<!-- JQUERY UI + Bootstrap Slider -->
<script src="<?php echo base_url(); ?>assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
<!-- browser msie issue fix -->
<script src="<?php echo base_url(); ?>assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
<!-- FastClick: For mobile devices -->
<script src="<?php echo base_url(); ?>assets/js/plugin/fastclick/fastclick.min.js"></script>
<!-- Demo purpose only -->
<script src="<?php echo base_url(); ?>assets/js/demo.min.js"></script>
<!-- MAIN APP JS FILE -->
<script src="<?php echo base_url(); ?>assets/js/app.min.js"></script>
<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
<!-- Voice command : plugin -->
<script src="<?php echo base_url(); ?>assets/js/speech/voicecommand.min.js"></script>
<!-- ///// mis validaciones js ///// -->
<SCRIPT src="<?php echo base_url(); ?>assets/js/mis_js/validacion_form.js" type="text/javascript"></SCRIPT>
<!--alertas -->
<script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
<!-- PAGE RELATED PLUGIN(S) -->
<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>

<!--================================       CASO ADICONAR CARPETA ================== -->

 <script type="text/javascript">
          // DO NOT REMOVE : GLOBAL FUNCTIONS!
          
          $(document).ready(function() {
               
               pageSetUp();
     
               /* BASIC ;*/
                    var responsiveHelper_dt_basic = undefined;
                    var responsiveHelper_datatable_fixed_column = undefined;
                    var responsiveHelper_datatable_col_reorder = undefined;
                    var responsiveHelper_datatable_tabletools = undefined;
                    
                    var breakpointDefinition = {
                         tablet : 1024,
                         phone : 480
                    };
     
                    $('#dt_basic').dataTable({
                         "ordering": false,
                         "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
                              "t"+
                              "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                         "autoWidth" : true,
                         "preDrawCallback" : function() {
                              // Initialize the responsive datatables helper once.
                              if (!responsiveHelper_dt_basic) {
                                   responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                              }
                         },
                         "rowCallback" : function(nRow) {
                              responsiveHelper_dt_basic.createExpandIcon(nRow);
                         },
                         "drawCallback" : function(oSettings) {
                              responsiveHelper_dt_basic.respond();
                         }
                    });

          })


          </script>
</body>
</html>
