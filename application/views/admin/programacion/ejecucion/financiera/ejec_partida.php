<!DOCTYPE html>
<html lang="en-us">
     <head>
          <meta charset="utf-8">
          <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
          <title><?php echo $this->session->userdata('name')?></title>
          <meta name="description" content="">
          <meta name="author" content="">
          <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
          <!-- Basic Styles -->
          <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
          <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
          <!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
          <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css">
          <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
          <!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
          <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
          <!-- FAVICONS -->
          <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
          <link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
          <!--estiloh-->
          <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
          <style type="text/css">
               aside{background: #05678B;}
          </style>
	          <style>
	          	table{
		        font-size: 9px;
		        width: 100%;
		        max-width:1550px;;
		        overflow-x: scroll;
		        }
		    	th{
		        padding: 1.4px;
		        text-align: center;
		        font-size: 9px;
		        }
	      </style>
     </head>
     <body class="">
          <!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->
          <!-- HEADER -->
		<header id="header">
			<!-- pulled right: nav area -->
			<div class="pull-right">
				<!-- collapse menu button -->
				<div id="hide-menu" class="btn-header pull-right">
					<span> <a href="javascript:void(0);" data-action="toggleMenu" title="Menu"><i class="fa fa-reorder"></i></a> </span>
				</div>
				<!-- end collapse menu -->
				<!-- logout button -->
				<div id="logout" class="btn-header transparent pull-right">
					<span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Salir" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
				</div>
				<!-- end logout button -->
				<!-- search mobile button (this is hidden till mobile view port) -->
				<div id="search-mobile" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
				</div>
				<!-- end search mobile button -->
				<!-- fullscreen button -->
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Pantalla Completa"><i class="fa fa-arrows-alt"></i></a> </span>
				</div>
				<!-- end fullscreen button -->
			</div>
			<!-- end pulled right: nav area -->
		</header>
          <!-- END HEADER -->
          <!-- Left panel : Navigation area -->
        <aside id="left-panel">
      <!-- User info -->
      <div class="login-info">
        <span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
          <a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
            <span>
              <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
            </span>
          </a> 
        </span>
      </div>

      <nav>
        <ul>
          <li>
            <a href='<?php echo site_url("admin").'/dashboard'; ?>' title="MENU PRINCIPAL"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
          </li>
          <li class="text-center">
            <a href="<?php echo base_url().'index.php/admin/proy/mis_proyectos/'.$mod ?>" title="PROGRAMACION -> MIS PROYECTOS"> <span class="menu-item-parent">PROGRAMACI&Oacute;N F&Iacute;SICA</span></a>
          </li>
          <?php
          if($nro_fase==1){
              for($i=0;$i<count($enlaces);$i++)
              {
                ?>
             	<li>
                    <a href="#" >
                      <i class="<?php echo $enlaces[$i]['o_image'];?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
                    <ul >
                    <?php
                    $submenu= $this->menu_modelo->get_Modulos_sub($enlaces[$i]['o_child']);
                	foreach($submenu as $row) {
                    ?>
                    <li><a href="<?php echo base_url($row['o_url'])."/".$mod."/".$id_f[0]['id']."/".$id_f[0]['proy_id']; ?>"><?php echo $row['o_titulo']; ?></a></li>
                    <?php } ?>
                    </ul>
                </li>
                <?php
              }
          }
          ?>
        </ul>
      </nav>
      <span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>
    </aside>

		<!-- MAIN PANEL -->
		<div id="main" role="main">
			<!-- RIBBON -->
			<div id="ribbon">
				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>
				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url().'index.php/admin/proy/list_proy'?>" title="MIS OPERACIONES">Mis Operaciones</a></li><li>Ejecucion Presupuestaria de la Operaci&oacute;n</li><li>Ejecutar Partida</li>
				</ol>
			</div>
			<!-- END RIBBON -->
			<!-- MAIN CONTENT -->
			<div id="content">
				<!-- widget grid -->
				<section id="widget-grid" class="">
					<!-- row -->
					<div class="row">
						<article class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
				            <section id="widget-grid" class="well">
				                <div class="">
				                  <h1> CATEGORIA PROGRAM&Aacute;TICA : <small><?php echo $proyecto[0]['aper_programa'].''.$proyecto[0]['aper_proyecto'].''.$proyecto[0]['aper_actividad']?></small></h1>
				                  <h1> <?php echo $titulo_proy;?> : <small><?php echo $proyecto[0]['proy_nombre']?></small></h1>
				                </div>
				            </section>
				        </article>

				        <article class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
				            <section id="widget-grid" class="well">
				              <style type="text/css">#graf{font-size: 80px;}</style> 
				              <center>
				                <div class="dropdown">
				                <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
				                  OPCIONES INSUMO
				                  <span class="caret"></span>
				                </button>
				                	<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
				                  		<li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url().'index.php/admin/dashboard' ?>">SALIR A MENU PRINCIPAL</a></li>
				                  		<!-- <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url().'index.php/admin/proy/operacion/'.$proyecto[0]['proy_id'].'/'.$fase[0]['id']; ?>">MI OPERACI&Oacute;N </a></li> -->
				                  		<li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url().'index.php/admin/proy/mis_proyectos/1'; ?>">LISTA DE OPERACIONES</a></li>
				                  		<!-- <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url().'index.php/'.$atras.''; ?>">VOLVER ATRAS</a></li> -->
				                	</ul>
				              </div>
				              </center>
				            </section>
				        </article>
				        
				        <div >
							<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false">
								<header>
									<span class="widget-icon"> <i class="fa fa-table"></i> </span>
									<h2></h2>
								</header>
								<!-- widget div-->
								<div>
									<!-- widget edit box -->
									<div class="jarviswidget-editbox">
										<!-- This area used as dropdown edit box -->
									</div>
									<!-- end widget edit box -->
									<!-- widget content -->
									<div class="widget-body">
										<div class="table-responsive">
										<h2 class="alert alert-success"><center><?php echo 'PARTIDA : '.$ppartida[0]['codigo2'].' - '.strtoupper($ppartida[0]['partida2']);?></center></h2>
										<form action="<?php echo site_url("admin") . '/prog/valida_ejecucion' ?>" id="ins_form_prog" name="ins_form_prog" novalidate="novalidate" method="post">
		                            	<input type="hidden" name="mod" id="mod" value="<?php echo $mod;?>"> <!-- mod -->
                                        <input type="hidden" name="gestion" id="gestion" value="<?php echo $gestion;?>"> <!-- mod -->

                                        <input type="hidden" name="proy_id" id="proy_id" value="<?php echo $proyecto[0]['proy_id'];?>"> <!-- proy  -->
		                            	<input type="hidden" name="pr_id" id="pr_id" value="<?php echo $ppartida[0]['pr_id'];?>"> <!-- proy  -->
		                            	<input type="hidden" name="fase_id" id="fase_id" value="<?php echo $id_f[0]['id'];?>"> <!-- proy  -->
		                            	<input type="hidden" name="par_id" id="par_id" value="<?php echo $ppartida[0]['par_id'];?>"> <!-- par -->
		                            	<input type="hidden" name="ff_id" id="ff_id" value="<?php echo $ppartida[0]['ff_id'];?>"> <!-- ff -->
		                            	<input type="hidden" name="of_id" id="of_id" value="<?php echo $ppartida[0]['of_id'];?>"> <!-- of -->
		                            	<table class="table table-bordered table-hover" style="width:100%;" >
											    <thead>
											        <tr>
											            <th style="width:16.6%;"><center>ENERO</center></th>
											            <th style="width:16.6%;"><center>FEBRERO</center></th>
											            <th style="width:16.6%;"><center>MARZO</center></th>
											            <th style="width:16.6%;"><center>ABRIL</center></th>
											            <th style="width:16.6%;"><center>MAYO</center></th>
											            <th style="width:16.6%;"><center>JUNIO</center></th>
											        </tr>
											    </thead>
											    <tbody>
											        <tr>
											            <td>
											            	<table class="table table-bordered">
											            		<tr bgcolor="#9ceceb">
											            			<td style="width:20%;">P</td><td style="width:80%;"><input class="form-control" type="text" style="width:100%;" value="<?php echo $ppartida[0]['enero'];?>" title="PROGRAMACION FINANCIERA MES DE ENERO" disabled="true"></td>
											            		</tr>
											            		<tr>
											            			<td style="width:20%;">E</td><td style="width:80%;"><input  name="m1"  class="form-control" type="text" value="<?php echo round($ejec[1],2);?>" onkeypress="if (this.value.length < 15) { return numerosDecimales(event);}else{return false; }" onpaste="return false" required="true" title="EJECUCION FINANCIERA MES DE ENERO"></td>
											            		</tr>
											            	</table>
											            </td>
											            <td>
											            	<table class="table table-bordered">
											            		<tr bgcolor="#9ceceb">
											            			<td style="width:20%;">P</td><td style="width:80%;"><input class="form-control" type="text" style="width:100%;" value="<?php echo $ppartida[0]['febrero'];?>" title="PROGRAMACION FINANCIERA MES DE FEBRERO" disabled="true"></td>
											            		</tr>
											            		<tr>
											            			<td style="width:20%;">E</td><td style="width:80%;"><input  name="m2" class="form-control" type="text" value="<?php echo round($ejec[2],2);?>" onkeypress="if (this.value.length < 15) { return numerosDecimales(event);}else{return false; }" onpaste="return false" required="true" title="EJECUCION FINANCIERA MES DE FEBRERO"></td>
											            		</tr>
											            	</table>
											            </td>
											            <td>
											            	<table class="table table-bordered">
											            		<tr bgcolor="#9ceceb">
											            			<td style="width:20%;">P</td><td style="width:80%;"><input class="form-control" type="text" style="width:100%;" value="<?php echo $ppartida[0]['marzo'];?>" title="PROGRAMACION FINANCIERA MES DE MARZO" disabled="true"></td>
											            		</tr>
											            		<tr>
											            			<td style="width:20%;">E</td><td style="width:80%;"><input  name="m3" class="form-control" type="text" value="<?php echo round($ejec[3],2);?>" onkeypress="if (this.value.length < 15) { return numerosDecimales(event);}else{return false; }" onpaste="return false" required="true" title="EJECUCION FINANCIERA MES DE MARZO"></td>
											            		</tr>
											            	</table>
														</td>
											            <td>
											            	<table class="table table-bordered">
											            		<tr bgcolor="#9ceceb">
											            			<td style="width:20%;">P</td><td style="width:80%;"><input class="form-control" type="text" style="width:100%;" value="<?php echo $ppartida[0]['abril'];?>" title="PROGRAMACION FINANCIERA MES DE ABRIL" disabled="true"></td>
											            		</tr>
											            		<tr>
											            			<td style="width:20%;">E</td><td style="width:80%;"><input  name="m4" class="form-control" type="text" value="<?php echo round($ejec[4],2);?>" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }" onpaste="return false" required="true" title="EJECUCION FINANCIERA MES DE ABRIL"></td>
											            		</tr>
											            	</table>
											            </td>
											        	<td>
											        		<table class="table table-bordered">
											            		<tr bgcolor="#9ceceb">
											            			<td style="width:20%;">P</td><td style="width:80%;"><input class="form-control" type="text" style="width:100%;" value="<?php echo $ppartida[0]['mayo'];?>" title="PROGRAMACION FINANCIERA MES DE MAYO" disabled="true"></td>
											            		</tr>
											            		<tr>
											            			<td style="width:20%;">E</td><td style="width:80%;"><input  name="m5" class="form-control" type="text" value="<?php echo round($ejec[5],2);?>" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }" onpaste="return false" required="true" title="EJECUCION FINANCIERA MES DE MAYO "></td>
											            		</tr>
											            	</table>
											        	</td>
											            <td>
											            	<table class="table table-bordered">
											            		<tr bgcolor="#9ceceb">
											            			<td style="width:20%;">P</td><td style="width:80%;"><input class="form-control" type="text" style="width:100%;" value="<?php echo $ppartida[0]['junio'];?>" title="PROGRAMACION FINANCIERA MES DE JUNIO" disabled="true"></td>
											            		</tr>
											            		<tr>
											            			<td style="width:20%;">E</td><td style="width:80%;"><input  name="m6" class="form-control" type="text" value="<?php echo round($ejec[6],2);?>" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }" onpaste="return false" required="true" title="EJECUCION FINANCIERA MES DE JUNIO"></td>
											            		</tr>
											            	</table>
											            </td>
											        </tr>
											    </tbody>
											</table>
											<table class="table table-bordered table-hover" style="width:100%;" >
											    <thead>
											        <tr>
											            <th style="width:16.6%;" ><center>JULIO</center></th>
											            <th style="width:16.6%;"><center>AGOSTO</center></th>
											            <th style="width:16.6%;"><center>SEPTIEMBRE</center></th>
											            <th style="width:16.6%;"><center>OCTUBRE</center></th>
											            <th style="width:16.6%;"><center>NOVIEMBRE</center></th>
											            <th style="width:16.6%;"><center>DICIEMBRE </center></th>
											        </tr>
											    </thead>
											    <tbody>
											        <tr>
											            <td>
											            	<table class="table table-bordered">
											            		<tr bgcolor="#9ceceb">
											            			<td style="width:20%;">P</td><td style="width:80%;"><input class="form-control" type="text" style="width:100%;" value="<?php echo $ppartida[0]['julio'];?>" title="PROGRAMACION FINANCIERA MES DE JULIO" disabled="true"></td>
											            		</tr>
											            		<tr>
											            			<td style="width:20%;">E</td><td style="width:80%;"><input  name="m7" class="form-control" type="text" value="<?php echo round($ejec[7],2);?>" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }" onpaste="return false" required="true" title="EJECUCION FINANCIERA MES DE JULIO"></td>
											            		</tr>
											            	</table>
											            </td>
											            <td>
											            	<table class="table table-bordered">
											            		<tr bgcolor="#9ceceb">
											            			<td style="width:20%;">P</td><td style="width:80%;"><input class="form-control" type="text" style="width:100%;" value="<?php echo $ppartida[0]['agosto'];?>" title="PROGRAMACION FINANCIERA MES DE AGOSTO" disabled="true"></td>
											            		</tr>
											            		<tr>
											            			<td style="width:20%;">E</td><td style="width:80%;"><input  name="m8" class="form-control" type="text" value="<?php echo round($ejec[8],2);?>" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }" onpaste="return false" required="true" title="EJECUCION FINANCIERA MES DE AGOSTO"></td>
											            		</tr>
											            	</table>
											            </td>
											        	<td>
											        		<table class="table table-bordered">
											            		<tr bgcolor="#9ceceb">
											            			<td style="width:20%;">P</td><td style="width:80%;"><input class="form-control" type="text" style="width:100%;" value="<?php echo $ppartida[0]['septiembre'];?>" title="PROGRAMACION FINANCIERA MES DE SEPTIEMBRE" disabled="true"></td>
											            		</tr>
											            		<tr>
											            			<td style="width:20%;">E</td><td style="width:80%;"><input  name="m9" class="form-control" type="text" value="<?php echo round($ejec[9],2);?>" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }" onpaste="return false" required="true" title="EJECUCION FINANCIERA MES DE SEPTIEMBRE"></td>
											            		</tr>
											            	</table>
											        	</td>
											            <td>
											            	<table class="table table-bordered">
											            		<tr bgcolor="#9ceceb">
											            			<td style="width:20%;">P</td><td style="width:80%;"><input class="form-control" type="text" style="width:100%;" value="<?php echo $ppartida[0]['octubre'];?>" title="PROGRAMACION FINANCIERA MES DE OCTUBRE" disabled="true"></td>
											            		</tr>
											            		<tr>
											            			<td style="width:20%;">E</td><td style="width:80%;"><input  name="m10" class="form-control" type="text" value="<?php echo round($ejec[10],2);?>" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }" onpaste="return false" required="true" title="EJECUCION FINANCIERA MES DE OCTUBRE"></td>
											            		</tr>
											            	</table>
											            </td>
											            <td>
											            	<table class="table table-bordered">
											            		<tr bgcolor="#9ceceb">
											            			<td style="width:20%;">P</td><td style="width:80%;"><input class="form-control" type="text" style="width:100%;" value="<?php echo $ppartida[0]['noviembre'];?>" title="PROGRAMACION FINANCIERA MES DE NOVIEMBRE" disabled="true"></td>
											            		</tr>
											            		<tr>
											            			<td style="width:20%;">E</td><td style="width:80%;"><input  name="m11" class="form-control" type="text" value="<?php echo round($ejec[11],2);?>" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }" onpaste="return false" required="true" title="EJECUCION FINANCIERA MES DE NOVIEMBRE"></td>
											            		</tr>
											            	</table>
											            </td>
											            <td>
											            	<table class="table table-bordered">
											            		<tr bgcolor="#9ceceb">
											            			<td style="width:20%;">P</td><td style="width:80%;"><input class="form-control" type="text" style="width:100%;" value="<?php echo $ppartida[0]['diciembre'];?>" title="PROGRAMACION FINANCIERA MES DE DICIEMBRE" disabled="true"></td>
											            		</tr>
											            		<tr>
											            			<td style="width:20%;">E</td><td style="width:80%;"><input  name="m12" class="form-control" type="text"  value="<?php echo round($ejec[12],2);?>" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }" onpaste="return false" required="true" title="EJECUCION FINANCIERA MES DE DICIEMBRE"></td>
											            		</tr>
											            	</table>
											            </td>
											        </tr>
											    </tbody>
											</table>
                                    	</form> 

							            <div class="form-actions">
											<a href="<?php echo base_url().'index.php/admin/prog/efinanciero/'.$mod.'/'.$id_f[0]['id'].'/'.$proyecto[0]['proy_id'].''; ?>" class="btn btn-lg btn-default" title="REQUERIMIENTOS DE LA OPERACION"> CANCELAR </a>
											<input type="button" value="EJECUTAR PROGRAMACION FINANCIERA" id="btsubmit" class="btn btn-primary btn-lg" onclick="valida_envia_ejecutado()" title="EJECUTAR PROGRAMACION FINANCIERA">
										</div>
										</div>
									</div>
									<!-- end widget content -->
								</div>
								<!-- end widget div -->
								</div>
							</article>
				        </div>

					</div>
				</section>
				<!-- end widget grid -->
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN PANEL -->
		<!-- PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->
		<!--================================================== -->
		<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
		<script data-pace-options='{ "restartOnRequestAfter": true }' src="<?php echo base_url(); ?>assets/js/plugin/pace/pace.min.js"></script>

		<script>
			if (!window.jQuery) {document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-2.0.2.min.js"><\/script>');}
		</script>
		<script>
			if (!window.jQuery.ui) {document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');}
		</script>
		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url(); ?>assets/js/session_time/jquery-idletimer.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/app.config.js"></script>
		<script src = "<?php echo base_url(); ?>mis_js/control_session.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/mis_js/validacion_form.js"></script>
		<script src="<?php echo base_url(); ?>mis_js/programacion/insumos/insumos_componentes.js"></script>
		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.min.js"></script>
		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url(); ?>assets/js/notification/SmartNotification.min.js"></script>
		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url(); ?>assets/js/smartwidgets/jarvis.widget.min.js"></script>
		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<!-- SPARKLINES -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/select2/select2.min.js"></script>
		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
		<!-- browser msie issue fix -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/fastclick/fastclick.min.js"></script>
		<!-- Demo purpose only -->
		<script src="<?php echo base_url(); ?>assets/js/demo.min.js"></script>
		<!-- MAIN APP JS FILE -->

</html>
