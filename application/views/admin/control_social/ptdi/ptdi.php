<!DOCTYPE html>
<html lang="en-us">
  <head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    <title><?php echo $this->session->userdata('name')?></title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- Basic Styles -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
    <!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
    <!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
    <!-- FAVICONS -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.core.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css" id="toggleCSS"/>
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
    <style>
      table{font-size: 8px;
            width: 100%;
            max-width:1550px;;
            overflow-x: scroll;
            }
      th{
          padding: 1.4px;
          text-align: center;
          font-size: 8px;
        }
    </style>
    <script>
        function abreVentana(PDF)
      {
        var direccion;
        direccion = '' + PDF;
        window.open(direccion, "Reporte de Proyectos" , "width=800,height=650,scrollbars=SI") ;
      }                                                  
    </script>
  </head>
  <body class="">
    <!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->
    <!-- HEADER -->
      <header id="header">
        <!-- pulled right: nav area -->
        <div class="pull-right">
          <!-- logout button -->
          <div id="logout" class="btn-header transparent pull-right">
            <span> <a href="<?php echo site_url("user/logout_control_social"); ?>" title="SALIR DE CONTROL SOCIAL" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a></span>
          </div>
          <!-- end logout button -->
          <!-- fullscreen button -->
          <div id="fullscreen" class="btn-header transparent pull-right">
            <span> <a href="javascript:void(0);" data-action="launchFullscreen" title="EXPANDIR PANTALLA"><i class="fa fa-arrows-alt"></i></a> </span>
          </div>
          <!-- end fullscreen button -->
        </div>
        <!-- end pulled right: nav area -->
      </header>
    <!-- END HEADER -->
    <!-- MAIN PANEL -->
    <div >
      <!-- RIBBON -->
      <div id="ribbon">
        <span class="ribbon-button-alignment"> 
          <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
            <i class="fa fa-refresh"></i>
          </span> 
        </span>
        <!-- breadcrumb -->
        <ol class="breadcrumb">
          <li>Control Social <?php echo $this->session->userdata("gestion")?></li><li>Ejecuci&oacute;n Presupuestaria por Componentes Estrategicos</li>
        </ol>
      </div>
      <!-- END RIBBON -->

      <!-- MAIN CONTENT -->
      <div id="content">
      <div class="well">
        <section id="widget-grid" class="">
          <!-- row -->
          <div class="row">
          <article class="col-xs-12 col-sm-12 col-md-12 col-lg-9">
                <section id="widget-grid" class="well">
                <div align="center">
                  <font size="2">EJECUCI&Oacute;N PRESUPUESTARIA POR COMPONENTES ESTRATEGICOS (Del 1 de Enero Al de <?php echo $dias.' de '.$mes.' de '.$this->session->userdata("gestion");?>) (Expresado en Bolivianos) </font>
                </div>
            </section>
            </article>
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
                <section id="widget-grid" class="well">
                    <div align="center">
                      <a href="#" data-toggle="modal" data-target="#modal_nuevo_ff" class="btn btn-primary" style="width:100%;">GR&Aacute;FICO COMPONENTE</a><br><br>
                      <div class="dropdown">
                        <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenu1" style="width:100%;" data-toggle="dropdown" aria-expanded="true">
                          OPCIONES DE IMPRESI&Oacute;N
                          <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                          <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:abreVentana('<?php echo site_url("").'/rep/ejec/rep_comp_ptdi' ?>');">IMPRIMIR COMPONENTES ESTRATEGICOS.</a></li>
                          <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:abreVentana('<?php echo site_url("").'/rep/ejec/rep_grafico_comp_ptdi' ?>');">IMPRIMIR GR&Aacute;FICO</a></li>
                          <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url().'index.php/admin/control_social'?>">VOLVER ATRAS</a></li>
                        </ul>
                      </div>
                    </div>
                </section>
            </article>

           <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <!-- Widget ID (each widget will need unique ID)-->
              <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false">
                <header>
                  <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                  <h2></h2>
                </header>
                <!-- widget div-->
                <div>
                  <!-- widget edit box -->
                  <div class="jarviswidget-editbox">
                    <!-- This area used as dropdown edit box -->
                  </div>
                  <!-- end widget edit box -->
                  <!-- widget content -->
                  <div class="widget-body">
                    <p></p>
                    <div class="table-responsive">
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <tr>
                            <th>COMPONENTE ESTRATEGICO</th>
                            <th>PRESUPUESTO INICIAL</th>
                            <th>MODIFICACIONES</th>
                            <th>PRESUPUESTO VIGENTE</th>
                            <th>EJECUCI&Oacute;N</th>
                            <th>% EJECUCI&Oacute;N</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php echo $tabla;?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <!-- end widget content -->
                </div>
                <!-- end widget div -->
              </div>

            </article>
          </div>
          <!-- end row -->
        </section>
        </div>
      </div>
      <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN PANEL -->

    <!-- PAGE FOOTER -->
    <div class="page-footer"> 
      <div class="row">
        <div class="col-xs-12 col-sm-6">
          <span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
        </div>
      </div>
    </div>
    <!-- END PAGE FOOTER -->
    <!-- ============================================ Modal NUEVO COMPONENTE  =============================================== -->
    
    <div class="modal fade bs-example-modal-lg" id="modal_nuevo_ff" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close text-danger " data-dismiss="modal" aria-hidden="true">
                      &times;
                  </button>
                  <h4 class="modal-title">
                    <img src="<?php echo base_url(); ?>assets/img/logo.png" width="150" alt="SmartAdmin">
                  </h4>
                  <h4 class="modal-title text-center text-info">
                      <b></b>
                  </h4>
                </div>
                <div class="modal-body no-padding">
                  <div class="row">
                    <div class="jarviswidget jarviswidget-color-darken" id="wid-id-1" data-widget-editbutton="false">
                        <header>
                          <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                          <h2></h2>
                        </header>
                        <!-- widget div-->
                        <div>
                          <!-- widget edit box -->
                          <div class="jarviswidget-editbox">
                            <!-- This area used as dropdown edit box -->
                          </div>
                          <!-- end widget edit box -->
                          <!-- widget content -->
                          <div class="widget-body no-padding">
                            <div id="container" style="width: 800px; height: 400px; margin: 0 auto"></div> 
                          </div>
                          <!-- end widget content -->
                        </div>
                        <!-- end widget div -->
                    </div>
                  </div>
                </div>           
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <!-- =========================================================================================================================================== -->
    <!--================================================== -->
    <script>
      if (!window.jQuery) {
        document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-2.0.2.min.js"><\/script>');
      }
    </script>

    <script>
      if (!window.jQuery.ui) {
        document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
      }
    </script>

    <!-- IMPORTANT: APP CONFIG -->
    <script src="<?php echo base_url();?>/assets/js/app.config.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/mis_js/validacion_form.js"></script>
    <script src = "<?php echo base_url(); ?>mis_js/control_session.js"></script>
    <!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
    <script src="<?php echo base_url();?>/assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
    <!-- BOOTSTRAP JS -->
    <script src="<?php echo base_url();?>/assets/js/bootstrap/bootstrap.min.js"></script>
    <!-- CUSTOM NOTIFICATION -->
    <script src="<?php echo base_url();?>/assets/js/notification/SmartNotification.min.js"></script>
    <!-- JARVIS WIDGETS -->
    <script src="<?php echo base_url();?>/assets/js/smartwidgets/jarvis.widget.min.js"></script>
    <!-- EASY PIE CHARTS -->
    <script src="<?php echo base_url();?>/assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
    <!-- SPARKLINES -->
    <script src="<?php echo base_url();?>/assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
    <!-- JQUERY VALIDATE -->
    <script src="<?php echo base_url();?>/assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
    <!-- JQUERY MASKED INPUT -->
    <script src="<?php echo base_url();?>/assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
    <!-- JQUERY SELECT2 INPUT -->
    <script src="<?php echo base_url();?>/assets/js/plugin/select2/select2.min.js"></script>
    <!-- JQUERY UI + Bootstrap Slider -->
    <script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
    <!-- browser msie issue fix -->
    <script src="<?php echo base_url();?>/assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
    <!-- FastClick: For mobile devices -->
    <script src="<?php echo base_url();?>/assets/js/plugin/fastclick/fastclick.min.js"></script>
    <!-- Demo purpose only -->
    <script src="<?php echo base_url();?>/assets/js/demo.min.js"></script>
    <!-- MAIN APP JS FILE -->
    <script src="<?php echo base_url();?>/assets/js/app.min.js"></script>
    <!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
    <!-- Voice command : plugin -->
    <script src="<?php echo base_url();?>/assets/js/speech/voicecommand.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/highcharts/js/highcharts.js"></script>
    <script src="<?php echo base_url(); ?>assets/highcharts/js/highcharts-3d.js"></script>
    <script src="<?php echo base_url(); ?>assets/highcharts/js/modules/exporting.js"></script> 
    <!-- PAGE RELATED PLUGIN(S) -->
    
    <script type="text/javascript">
      var chart;

      $(document).ready(function() {
        chart = new Highcharts.Chart({
          chart: {
            renderTo: 'container',
            defaultSeriesType: 'column'
          },
          title: {
            text: 'RELACI\u00D3N DEL PRESUPUESTO VIGENTE Vs EJECUCI\u00D3N POR COMPONENTE ESTRATEGICO'
          },
          subtitle: {
            text: '(Al de <?php echo $dias.' de '.$mes.' de '.$this->session->userdata("gestion");?>) (Expresado en MM de Bolivianos)'
          },
          xAxis: {
            categories: [
              <?php 
              for ($i=1; $i <=$nro-1 ; $i++) 
              { 
                if($i==$nro-1){
                  ?>
                  '<?php echo $ptdi[$i][2];?>'
                  <?php
                }
                else{
                  ?>
                  '<?php echo $ptdi[$i][2];?>',
                  <?php
                }
              } 
            ?>
            ]
          },
          yAxis: {
            min: 0,
            title: {
              text: 'Rainfall (mm)'
            }
          },
          legend: {
            layout: 'vertical',
            backgroundColor: '#FFFFFF',
            align: 'left',
            verticalAlign: 'top',
            x: 100,
            y: 70,
            floating: true,
            shadow: true
          },
          tooltip: {
            formatter: function() {
              return ''+
                this.x +': '+ this.y +' mm';
            }
          },
          plotOptions: {
            column: {
              pointPadding: 0.2,
              borderWidth: 0
            }
          },
          series: [{
            name: 'PRESUPUESTO VIGENTE',color: '#FE2E2E',
            data: [
            <?php 
              for ($i=1; $i <=$nro-1 ; $i++) 
              {
                if($i==$nro-1){
                  ?>
                  {y: <?php echo $ptdi[$i][5]?>, color: '#FE2E2E'}
                  <?php
                }
                else{
                  ?>
                  {y: <?php echo $ptdi[$i][5]?>, color: '#FE2E2E'},
                  <?php
                }
              }
            ?>
            ]
        
          }, {
            name: 'PRESUPUESTO EJECUTADO',color: '#04B404',
            data: [
            <?php 
              for ($i=1; $i <=$nro-1 ; $i++) 
              {
                if($i==$nro-1){
                  ?>
                  {y: <?php echo $ptdi[$i][6]?>, color: '#04B404'}
                  <?php
                }
                else{
                  ?>
                  {y: <?php echo $ptdi[$i][6]?>, color: '#04B404'},
                  <?php
                }
              }
            ?>
            ]
          }]
        });
      });
    </script>
  </body>
</html>
