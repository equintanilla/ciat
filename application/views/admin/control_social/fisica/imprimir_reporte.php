<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<link rel="STYLESHEET" href="<?php echo base_url(); ?>assets/print_static.css" type="text/css" />
  <style>
        table{
            font-size: 9px;
            width: 100%;
            background-color:#fff;
        }
  </style>
  <script type="text/javascript">
      function imprimir() {
          if (window.print) {
              window.print();
          } else {
              alert("La función de impresion no esta soportada por su navegador.");
          }
      }
  </script>
</head>
<body>

<body onload="imprimir();">

<div id="section_header">
</div>
    <div class="page" style="font-size: 7pt" align="center">
      <table style="width: 90%;">
          <tr>
              <td width=20%;>
                <img src="<?php echo base_url(); ?>assets/img/logoTarija.JPG" width="100px"/>
              </td>
              <td width=60%;>
                  <b>ENTIDAD : </b><?php echo $this->session->userdata('entidad');?><br>
                  <b>POA - PLAN OPERATIVO ANUAL : </b><?php echo $this->session->userdata("gestion")?><br>
                  <b><?php echo $this->session->userdata('sistema');?></b><br>
                  <b>REPORTE : </b> RESUMEN DE EJECUCI&Oacute;N DE INDICADORES DE EFICACIA F&Iacute;SICA AL <?php echo $dias;?> DE <?php echo $mes;?> DEL <?php echo $this->session->userdata("gestion")?><br>
              </td>
              <td width=20%; align="center">
                <img src="<?php echo base_url(); ?>assets/img/ltarija2.JPG" width="150px" >
              </td>
          </tr>
      </table>
    

      <table class="change_order_items" style="width: 80%; font-size: 7pt;">
          <tbody>
            <tr class="even_row">
              <td colspan="2">
                <center><b>RESUMEN DE INDICADORES DE EFICACIA F&Iacute;SICA AL <?php echo $dias;?> DE <?php echo $mes;?> DEL <?php echo $this->session->userdata("gestion")?></b></center>
              </td>
            </tr>
            <tr>
              <td style="width: 50%;">
                <div id="container2" style="width: 520px; height: 500px; margin: 1 auto"></div>
              </td>
              <td style="width: 50%;">
                  <table class="change_order_items">
                      <tr class="even_row">
                        <th></th>
                        <th>EJECUCI&Oacute;N</th>
                        <th>CANTIDAD</th>
                      </tr>
                      <tbody>
                      <tr class="modo1">
                        <td>Baja Ejecuci&oacute;n</td>
                        <td>0 - 25 %</td>
                        <td><?php echo $avance_financiero[1];?></td>
                        </tr>
                      <tr class="modo1">
                        <td>En Riesgo</td>
                        <td>26 - 50 %</td>
                        <td><?php echo $avance_financiero[2];?></td>
                        </tr>
                      <tr class="modo1">
                        <td>Mediana Ejecuci&oacute;n</td>
                        <td>51 - 75 %</td>
                        <td><?php echo $avance_financiero[3];?></td>
                        </tr>
                      <tr class="modo1">
                        <td>Ejecuci&oacute;n a Tiempo</td>
                        <td>76 - 100 %</td>
                        <td><?php echo $avance_financiero[4];?></td>
                        </tr>
                      <tr class="modo1">
                        <td><b>TOTAL</b></td>
                        <td></td>
                        <td><?php echo $avance_financiero[6];?></td>
                      </tr>
                    </tbody>
                  </table>
              </td>
            </tr>
          </tbody>
        </table>
        
        <br><br><br><br><br>
        <div align="center">
          <table border="0" cellpadding="0" cellspacing="0" class="tabla">
              <tr class="modo1">    
                <td colspan="3" height="40"><b>FIRMAS</b></td>
              </tr>
              <tr class="modo1" align="center">    
                <td style="width: 33.3%;"><br><br><br><br>RESPONSABLE UNIDAD EJECUTORA</td>
                <td style="width: 33.3%;"><br><br><br><br>ANALISTA POA</td>
                <td style="width: 33.3%;"><br><br><br><br>ANALISTA FINANCIERO</td>
              </tr>
          </table>
        </div>
    <!--================================================== -->
    <script src="<?php echo base_url();?>/assets/js/libs/jquery-2.0.2.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/highcharts/js/highcharts.js"></script>
    <script type="text/javascript">
    var entidad='<?php echo $this->session->userdata("entidad");?>';
    var mes='<?php echo $mes;?>';
    var dias='<?php echo $dias;?>';
    var gestion='<?php echo $this->session->userdata("gestion");?>';
    Highcharts.chart('container2', {
      
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: '',
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: 'SIGRE V1.0',
            colorByPoint: true,
            data: [{
                name: '(0 - 25 %)',
                color: '#EF1919',
                y: <?php echo $avance_financiero[1];?>
            },{
                name: '(26 - 50 %)',
                color: '#F8912F',
                y: <?php echo $avance_financiero[2];?>
            },{
                name: '(51 - 75 %)',
                color: '#F5F50A',
                y: <?php echo $avance_financiero[3];?>
            }, {
                name: '(76 - 100 %)',
                color: '#26F50A',
                y: <?php echo $avance_financiero[4];?>,
                sliced: true,
                selected: true
            }]
        }]
    });
    </script>
    </body>
    </html>

