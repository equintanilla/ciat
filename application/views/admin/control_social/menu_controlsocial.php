<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
		<title><?php echo $this->session->userdata('name')?></title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css"> 
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<!--estiloh-->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css"> 
		<meta name="viewport" content="width=device-width">
		<style>
		body{
				background-image: linear-gradient(rgba(0, 0, 0, 0.4), rgba(255, 255, 255, 0.4)),
                 url("<?php echo base_url('assets/img_v1.1/bg-green-img.jpg');?>");
				background-size: 100% 100%;
		}
		.box_menu{
            background: rgba(255, 255, 255, 0.8);
			background-size: 100% 100%;
			-moz-border-radius: 10px 10px 10px 10px; 
			-webkit-border-radius: 10px 10px 10px 10px; 
			border-radius: 10px 10px 10px 10px;
			padding:10px !important;
		}
		.box_item{
            background: rgba(255, 255, 255, 0.5);
			border:1px solid #aaa;
			-moz-border-radius: 7px; 
			-webkit-border-radius: 7px; 
			border-radius: 7px;
			margin:5px;
		}
		.box_item:hover{
            background: rgba(255, 255, 255, 0.8);
			border:1px solid #777;
			color:#000;
		}
		.hr_white{
			background-color:#777; border-color:#777;
		}	
		
		</style>
	</head>
	<body class="">

<!-- HEADER -->
<header id="header">
	<div id="logo-group" class="col-md-2">
		<span id="logo"> <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="<?php echo $this->session->userdata('name')?>"> </span>
	</div>
	<div class="col-md-6 no-print" style="font-size:15px; margin-top:15px; margin-bottom:-10px;">
	</div>
	<!-- pulled right: nav area -->
	<div class="pull-right col-md-2 no-print">
		<!-- logout button -->
		<div id="logout" class="btn-header transparent pull-right">
            <span> 
				<a href="<?php echo base_url(); ?>index.php/admin/logout" title="Salir" data-action="userLogout" data-logout-msg="Seguro de Salir?">
					Salir <i class="fa fa-sign-out"></i>
				</a> 
			</span>
		</div>
		<!-- end logout button -->

		<!-- fullscreen button -->
		<div id="fullscreen" class="btn-header transparent pull-right">
            <span> 
				<a href="javascript:void(0);" data-action="launchFullscreen" title="Pantalla Completa">
					<i class="fa fa-arrows-alt"></i>
				</a> 
			</span>
		</div>
		<!-- end fullscreen button -->
		<!-- collapse menu button -->
		<div id="hide-menu" class="btn-header pull-right">
            <span> 
				<a href="javascript:void(0);" data-action="toggleMenu" title="Menu">
					<i class="fa fa-reorder"></i>
				</a>
			</span>
		</div>
		<!-- end collapse menu -->

	</div>
	<!-- end pulled right: nav area -->

</header>
<!-- END HEADER -->
		<!-- MAIN PANEL -->
		<div id="" role="">
			<!-- RIBBON -->
			<!-- MAIN CONTENT -->
			<div id="content" style="">
				<!-- widget grid -->
								<div class="row">
									<div class="col-md-8 col-md-offset-2" align="center">
									<img style="width:90%;" src="<?php echo base_url('assets/img_v1.1/head_gobs.png');?>">
									</div>
								</div>
					<br>
					<div class="row">
			                <div class="col-md-8 col-md-offset-2">
			                  <div class="widget-body no-padding box_menu">
								<h2 class="text-center text-primary" style="font-size:2em;">
								SISTEMA INTEGRADO DE PLANIFICACIÓN, SEGUIMIENTO Y EVALUACIÓN
								<br>
								<span style="color:#777; font-size:1.5em;">DATOS ABIERTOS</span>
								</h2>

								<hr class="hr_white">

			                    <div class="row">
	                                <a href="<?php echo site_url("admin") . '/mis_operaciones'?>" title="Lista de Operaciones Institucionales - Proyectos de Inversión, Programas Recurrentes, No Recurrentes, Operación de Funcionamiento">
										<div class="col-sm-6 animated rotateInDownLeft">
											<div class="row box_item">
												<div class="col-md-5">
													<img src="<?php echo base_url('assets/img_v1.1/control_social/lista.png');?>" style="width:90%; margin:4px auto;">
												</div>
												<div class="col-md-7">
													<div class="text-center">
														  <h5 style="color:#777; font-size:2em; line-height:2em;">DATOS GRALES. DE<br> PROYECTOS<br> Y PROGRAMAS</h5>
													</div>
												</div>
											</div>
										</div>
	                                </a>
	                                <a href="<?php echo site_url("admin") . '/select_ejecfis'?>" title="Resumen de Ejecución Fisica a nivel de Operaciones Institucionales - Proyectos de Inversión, Programas Recurrentes, No Recurrentes, Operación de Funcionamiento">
			                        <div class="col-sm-6 animated rotateInDownRight">
											<div class="row box_item">
												<div class="col-md-5">
													<img src="<?php echo base_url('assets/img_v1.1/control_social/rfisico.png');?>" style="width:90%; margin:4px auto;">
												</div>
												<div class="col-md-7">
													<div class="text-center">
														  <h5 style="color:#777; font-size:2em; line-height:2em;">DATOS<br> EJECUCIÓN<br> FÍSICA</h5>
													</div>
												</div>
											</div>
			                        </div>
	                                </a>
			                    </div>

			                    <div class="row">
	                                <a href="<?php echo site_url("admin") . '/select_ejecfin'?>" title="Resumen de Ejecución Presupuestaria a nivel de Operaciones Institucionales - Proyectos de Inversión, Programas Recurrentes, No Recurrentes, Operación de Funcionamiento">
			                        <div class="col-sm-6 animated bounceInLeft">
											<div class="row box_item">
												<div class="col-md-5">
													<img src="<?php echo base_url('assets/img_v1.1/control_social/rfinanciero.png');?>" style="width:90%; margin:4px auto;">
												</div>
												<div class="col-md-7">
													<div class="text-center">
														  <h5 style="color:#777; font-size:2em; line-height:2em;">DATOS<br> EJECUCIÓN<br> FINANCIERA</h5>
													</div>
												</div>
											</div>
			                        </div>
	                                </a>
	                                <a href="<?php echo site_url("admin") . '/comp_est'?>" title="Resumen de Ejecución Presupuestaria a nivel de Componentes Estrategicos PTDI">
			                        <div class="col-sm-6 animated bounceInRight">
											<div class="row box_item">
												<div class="col-md-5">
													<img src="<?php echo base_url('assets/img_v1.1/control_social/estrategico.png');?>" style="width:90%; margin:4px auto;">
												</div>
												<div class="col-md-7">
													<div class="text-center">
														  <h5 style="color:#777; font-size:2em; line-height:2em;">DATOS POR<br> COMPONENTES<br> ESTRATÉGICOS</h5>
													</div>
												</div>
											</div>
			                        </div>
									</a>
			                    </div>
								
								
								<hr>
								<div class="row">
									<div class="col-md-12 text-center">
										<a href="<?= site_url();?>" class="btn btn-lg btn-primary">
											<i class="glyphicon glyphicon-backward"></i>
											Volver a página inicial
										</a>
									</div>
								</div>
								
								<br>
										<div class="well well-sm text-center lead">
											Si aprender más sobre cómo generar reportes para Control Social, consulta ésta 
											<a href="<?php echo base_url(); ?>assets/video/csocial.pdf" style="cursor: pointer;" download>
											Guía Rapida.
											</a>
											<br>
											Si tiene alguna consulta, sugerencia o reclamo, tambien puedes enviarnos un e-mail a 
			                                <a href="#" class="ruta" title="Realize sus Consultas, Reclamos, Sugerencias ">
			                                  ciat.soporte@santacruz.gob.bo
			                                </a>
			                            </div>

			                    </div>
			                    <!-- end widget content -->
			                  </div>
			                  <!-- end widget div -->
					</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div><br><br><br><br>
		<!-- END MAIN PANEL -->
		<!-- PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->
		<!--================================================== -->
		<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
		<script data-pace-options='{ "restartOnRequestAfter": true }' src="<?php echo base_url(); ?>assets/js/plugin/pace/pace.min.js"></script>
		<script>
			if (!window.jQuery) {
				document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-2.0.2.min.js"><\/script>');
			}
		</script>

		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>

		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url(); ?>assets/js/session_time/jquery-idletimer.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/app.config.js"></script>
		<script src="<?php echo base_url(); ?>mis_js/programacion/programacion/tablas.js"></script>
		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.min.js"></script>
		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url(); ?>assets/js/notification/SmartNotification.min.js"></script>
		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url(); ?>assets/js/smartwidgets/jarvis.widget.min.js"></script>
		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<!-- SPARKLINES -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/select2/select2.min.js"></script>
		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
		<!-- browser msie issue fix -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/fastclick/fastclick.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
		<!-- Demo purpose only -->
		<script src="<?php echo base_url(); ?>assets/js/demo.min.js"></script>
		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url(); ?>assets/js/app.min.js"></script>
		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<!-- Voice command : plugin -->
		<script src="<?php echo base_url(); ?>assets/js/speech/voicecommand.min.js"></script>
	</body>
</html>
