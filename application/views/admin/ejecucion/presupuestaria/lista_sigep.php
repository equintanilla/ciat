<!DOCTYPE html>
<html lang="en-us">
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    <title><?php echo $this->session->userdata('name')?></title>
    <meta name="description" content="">
    <meta name="author" content="">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- Basic Styles -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
    <!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
    <!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
    <!-- FAVICONS -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.core.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css" id="toggleCSS"/>
    <link href="<?php echo base_url(); ?>assets/file/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
    <script src="<?php echo base_url(); ?>assets/file/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/file/js/fileinput.min.js" type="text/javascript"></script> 
    <meta name="viewport" content="width=device-width">
    <style>
        table{font-size: 9px;
        width: 100%;
        max-width:1550px;;
        overflow-x: scroll;
        }
        th{
          padding: 1.4px;
          text-align: center;
          font-size: 9px;
        }
    </style>
</head>
<body class="">
<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->
<!-- HEADER -->
<header id="header">
    <!-- pulled right: nav area -->
    <div class="pull-right col-md-4">
        <!-- collapse menu button -->
        <div id="hide-menu" class="btn-header pull-right">
            <span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
        </div>
        <!-- end collapse menu -->
        <!-- logout button -->
        <div id="logout" class="btn-header transparent pull-right">
            <span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Sign Out" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
        </div>
        <!-- end logout button -->
        <!-- search mobile button (this is hidden till mobile view port) -->
        <div id="search-mobile" class="btn-header transparent pull-right">
            <span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
        </div>
        <!-- end search mobile button -->
        <!-- fullscreen button -->
        <div id="fullscreen" class="btn-header transparent pull-right">
            <span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen"><i  class="fa fa-arrows-alt"></i></a> </span>
        </div>
        <!-- end fullscreen button -->
    </div>
    <!-- end pulled right: nav area -->

</header>
<!-- END HEADER -->
<!-- Left panel : Navigation area -->
<aside id="left-panel">

    <!-- User info -->
    <div class="login-info">
		<span> <!-- User image size is adjusted inside CSS, it should stay as is -->
			<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
				<span>
					<?php echo $this->session->userdata("user_name"); ?>
				</span>
                <i class="fa fa-angle-down"></i>
            </a>
		</span>
    </div>

    <nav>
        <ul>
          <li class="">
            <a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MENÚ PRINCIPAL"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
          </li>
          <li class="text-center">
          <a href="<?php echo base_url().'index.php/admin/ejec/list_aper'; ?>" title="Ejecuci&oacute;n"><span class="menu-item-parent">REGISTRO DE EJECUCI&Oacute;N</span></a>
        </li>
			<?php
	                for($i=0;$i<count($enlaces);$i++)
	                {
	                    if(count($subenlaces[$enlaces[$i]['o_child']])>0)
	                    {
	            ?>
	            <li>
	              	<a href="#" >
	              		<i class="<?php echo $enlaces[$i]['o_image']?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
	              	<ul >
	              	<?php
	                foreach ($subenlaces[$enlaces[$i]['o_child']] as $item) {
	                ?>
	                <li><a href="<?php echo base_url($item['o_url']); ?>"><?php echo $item['o_titulo']; ?></a></li>
	                <?php } ?>
	                </ul>
	            </li>
	            <?php 
	                    }
	         } ?>
        </ul>
    </nav>
    <span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>

</aside>

<!-- MAIN PANEL -->
<div id="main" role="main">

    <!-- RIBBON -->
	<div id="ribbon">
		<span class="ribbon-button-alignment"> 
			<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
				<i class="fa fa-refresh"></i>
			</span> 
		</span>
		<!-- breadcrumb -->
		<ol class="breadcrumb">
        <li>Registro Ejecuci&oacute;n POA</li><li>Registro Ejecuci&oacute;n Presupuestaria</li><li>Cargar Archivo SIGEP</li>
		</ol>
	</div>
    <!-- END RIBBON -->

    <!-- MAIN CONTENT -->
    <div id="content">
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- row -->
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                    <section id="widget-grid" class="well">
                        <div class="">
                          <h1><small><?php echo $this->session->userdata("entidad"); ?></small></h1>
                          <h1> CARGAR ARCHIVO SIGEP A NIVEL DE CATEGORIAS PROGRAMATICAS <?php echo $this->session->userdata("gestion"); ?></h1>
                        </div>
                    </section>
                </article>
                <article class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                    <section id="widget-grid" class="well">
                        <div align="center">
                            <a href="<?php echo site_url("admin").'/ejec/list_aper' ?>" class="btn btn-success" title="VOLVER ATRAS" style="width:100%;">VOLVER ATRAS</a>
                        </div>
                    </section>
                </article>

                <article class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <?php 
                        if($this->session->flashdata('success')){ ?>
                          <div class="alert alert-success">
                              <?php echo $this->session->flashdata('success'); ?>
                          </div>
                          <script type="text/javascript">alertify.success("<?php echo '<font size=2>'.$this->session->flashdata('success').'</font>'; ?>")</script>
                      <?php 
                          }
                        elseif($this->session->flashdata('danger')){ ?>
                            <div class="alert alert-danger">
                              <?php echo $this->session->flashdata('danger'); ?>
                            </div>
                            <script type="text/javascript">alertify.error("<?php echo '<font size=2>'.$this->session->flashdata('danger').'</font>'; ?>")</script>
                          <?php
                        }
                      ?>
                    <div class="jarviswidget" id="wid-id-2" data-widget-editbutton="false">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-bar-chart-o"></i> </span>
                            <h2>SUBIR ARCHIVO .CSV</h2>
                        </header>

                        <div>
                        <div class="jarviswidget-editbox"></div>
                            <div class="widget-body no-padding">
                                <div class="">
                                <br>
                                <div class="col-sm-12">
                                    <p class="alert alert-info">
                                      <i class="fa fa-info"></i> Por favor guardar el archivo (Excel.xls) a extension (.csv) delimitado por (; "Punto y comas"). verificar el archivo .csv para su correcta importaci&oacute;n
                                    </p>
                                    <!-- row -->
                                    <div class="row">
                                      <form action="<?php echo site_url() . '/ejecucion/ejecucion/subir_sigep' ?>" method="post" enctype="multipart/form-data" id="form_subir_sigep" name="form_subir_sigep">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label><font size="1"><b>SELECCIONE MES </b></font><font color="blue">(Obligatorio)</font></label>
                                                    <select class="select2" id="mes_id" name="mes_id" title="Seleccione Mes" required="true">
                                                        <option value="">Seleccione</option>
                                                        <?php 
                                                        foreach($meses as $row)
                                                        {
                                                            if($row['m_id']==$this->session->userdata("mes")){
                                                                ?>
                                                                <option value="<?php echo $row['m_id']; ?>" selected ><?php echo $row['m_descripcion']; ?></option>
                                                                <?php
                                                            }
                                                            else{
                                                               ?>
                                                                <option value="<?php echo $row['m_id']; ?>"><?php echo $row['m_descripcion']; ?></option>
                                                               <?php   
                                                            }
                                                        }
                                                        ?>       
                                                    </select>
                                                </div>
                                            </div>
                                             <table class="table table-bordered">
                                              <thead>
                                                <tr>
                                                  <th style="width:50%">
                                                      <input id="archivo" accept=".csv" name="archivo" type="file" class="file">
                                                      <input name="MAX_FILE_SIZE" type="hidden" value="20000" />
                                                  </th>
                                                </tr>
                                              </thead>
                                              <tbody>
                                                <tr>
                                                  <td>
                                                    <button type="button" name="subir_archivo" id="subir_archivo" class="btn btn-success" style="width:100%;" value="Importar Archivo (<?php echo $mes; ?>.CSV)" >SUBIR ARCHIVO.CSV</button>
                                                    <center><img id="load" style="display: none" src="<?php echo base_url() ?>/assets/img/loading.gif" width="50" height="50"></center>
                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table>
                                      </form> 
                                    </div>
                                    <!-- end row -->
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>

                <article class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                  <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false">
                    <header>
                      <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                      <h2>ARCHIVOS IMPORTADOS </h2>
                    </header>
                    <!-- widget div-->
                    <div>
                      <!-- widget edit box -->
                      <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->
                      </div>
                      <!-- end widget edit box -->
                      <!-- widget content -->
                      <div class="widget-body">
                        <p></p>
                        <div class="table-responsive">
                          <table class="table table-bordered">
                            <thead>
                              <tr>
                                <tr>
                                <th>MES IMPORTADO</th>
                                <th>ESTADO</th>
                                <th>FECHA DE IMPORTACI&Oacute;N</th>
                                <th></th>
                              </tr>
                            </thead>
                            <tbody>
                                <?php echo $meses_sigep;?>
                            </tbody>
                          </table>
                        </div>
                      </div>
                      <!-- end widget content -->
                    </div>
                    <!-- end widget div -->
                  </div>

                </article>
            </div>
        </section>
        <!-- end widget grid -->
    </div>
    <!-- END MAIN CONTENT -->
<!-- PAGE FOOTER -->
</div>
<div class="page-footer">
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
        </div>
    </div>
</div>
<!-- END PAGE FOOTER -->
<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
<script data-pace-options='{ "restartOnRequestAfter": true }' src="<?php echo base_url(); ?>assets/js/plugin/pace/pace.min.js"></script>
<script>
    if (!window.jQuery) {
        document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-2.0.2.min.js"><\/script>');
    }
</script>
<script>
    if (!window.jQuery.ui) {
        document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
    }
</script>
<!-- IMPORTANT: APP CONFIG -->
<script src="<?php echo base_url(); ?>assets/js/session_time/jquery-idletimer.js"></script>
<script src="<?php echo base_url(); ?>assets/js/app.config.js"></script> 
<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script>
<!-- BOOTSTRAP JS -->
<script src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.min.js"></script>
<!-- CUSTOM NOTIFICATION -->
<script src="<?php echo base_url(); ?>assets/js/notification/SmartNotification.min.js"></script>
<!-- JQUERY VALIDATE -->
<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
<!-- JQUERY MASKED INPUT -->
<script src="<?php echo base_url(); ?>assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
<!-- JQUERY SELECT2 INPUT -->
<script src="<?php echo base_url(); ?>assets/js/plugin/select2/select2.min.js"></script>
<!-- JQUERY UI + Bootstrap Slider -->
<script src="<?php echo base_url(); ?>assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
<!-- browser msie issue fix -->
<script src="<?php echo base_url(); ?>assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
<!-- FastClick: For mobile devices -->
<script src="<?php echo base_url(); ?>assets/js/plugin/fastclick/fastclick.min.js"></script>
<!-- Demo purpose only -->
<script src="<?php echo base_url(); ?>assets/js/demo.min.js"></script>
<!-- MAIN APP JS FILE -->
<script src="<?php echo base_url(); ?>assets/js/app.min.js"></script>
<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
<!-- Voice command : plugin -->
<script src="<?php echo base_url(); ?>assets/js/speech/voicecommand.min.js"></script>
<!--alertas -->
<script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
<!-- ------------  mis validaciones js --------------------- -->
<SCRIPT src="<?php echo base_url(); ?>assets/js/mis_js/validacion_form.js" type="text/javascript"></SCRIPT>
<!-- PAGE RELATED PLUGIN(S) -->
<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
<!--================= ELIMINACION DE LAS METAS =========================================-->
  <script type="text/javascript">
    $(function () {
        //SUBIR ARCHIVO
        $("#subir_archivo").on("click", function () {

            var $valid = $("#form_subir_sigep").valid();
            if (!$valid) {
                $validator.focusInvalid();
            } else {
              if(document.getElementById('mes_id').value==''){
                alertify.alert('SELECCIONE MES');
                return false;
              }

              if(document.getElementById('archivo').value==''){
                alertify.alert('POR FAVOR SELECCIONE ARCHIVO .CSV');
                return false;
              }
                archivo = document.getElementById('archivo').value;
                //si el valor es diferente de 0 no existe fallas
                    alertify.confirm("REALMENTE DESEA SUBIR ESTE ARCHIVO?", function (a) {
                        if (a) {
                            document.getElementById("load").style.display = 'block';
                            document.getElementById('subir_archivo').disabled = true;
                            document.forms['form_subir_sigep'].submit();
                        } else {
                            alertify.error("OPCI\u00D3N CANCELADA");
                        }
                    });
               
            }
        });
      });
    </script>
<script type="text/javascript">
    // TABLA
    $(document).ready(function () {
        pageSetUp();
        /* BASIC ;*/
        var responsiveHelper_dt_basic = undefined;

        var breakpointDefinition = {
            tablet: 1024,
            phone: 480
        };

        $('#dt_basic').dataTable({
            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
            "t" +
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
            "autoWidth": true,
            "preDrawCallback": function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper_dt_basic) {
                    responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                }
            },
            "rowCallback": function (nRow) {
                responsiveHelper_dt_basic.createExpandIcon(nRow);
            },
            "drawCallback": function (oSettings) {
                responsiveHelper_dt_basic.respond();
            }
        });

        /* END BASIC */
    })
</script>
</body>

</html>
