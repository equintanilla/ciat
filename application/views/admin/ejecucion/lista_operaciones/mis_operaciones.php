<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
		<title><?php echo $this->session->userdata('name')?></title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css"> 
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
        <!-- FAVICONS -->
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css"> 
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.core.css"/>
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css" id="toggleCSS"/>
		<script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
		<!--para las alertas-->
    	<meta name="viewport" content="width=device-width">
		<!--fin de stiloh-->
		<style>
			.table1{
	          display: inline-block;
	          width:100%;
	          max-width:1550px;
	          overflow-x: scroll;
	          }
			table{font-size: 9px;
            width: 100%;
            max-width:1550px;;
			overflow-x: scroll;
            }
			div.table-responsive table table td{
				font-size:9px !important;
			}
            th{
              padding: 4px;
              text-align: center;
              font-size: 14px;
              background-color: #fafafa;
            }
            td{
			  padding:4px;		
              font-size: 14px;
            }
		</style>
	</head>
	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->
		<!-- HEADER -->
<header id="header">
	<div id="logo-group" class="col-md-2">
		<span id="logo"> <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="<?php echo $this->session->userdata('name')?>"> </span>
	</div>
	<div class="col-md-6 no-print" style="font-size:15px; margin-top:15px; margin-bottom:-10px;">
		<div class="text-center">
			<span>
				<div class="badge bg-color-blue" style="padding:7px;">
					<span style="font-size:1.6em;">Gesti&oacute;n de trabajo: <?php echo $this->session->userdata('desc_mes').'/'.$this->session->userdata('gestion');?></span>
				</div>
			</span>
			<div class="project-context hidden-xs">
				<span class="project-selector" style="font-size:0.7em;">
					<i class="fa fa-lg fa-fw fa-calendar txt-color-blue"></i>
					<a href="<?php echo base_url();?>index.php/cambiar_gestion">Cambiar Gestión</a>
				</span>
	<!--			<ul class="dropdown-menu">
					<li>
						<a href="<?php echo base_url();?>index.php/cambiar_gestion">Cambiar Gestión</a>
					</li>
				</ul>-->
			</div>
		</div>
	</div>
	<!-- pulled right: nav area -->
	<div class="pull-right col-md-2 no-print">
		<!-- logout button -->
		<div id="logout" class="btn-header transparent pull-right">
            <span> 
				<a href="<?php echo base_url(); ?>index.php/admin/logout" title="Salir" data-action="userLogout" data-logout-msg="Seguro de Salir?">
					Salir <i class="fa fa-sign-out"></i>
				</a> 
			</span>
		</div>
		<!-- end logout button -->

		<!-- fullscreen button -->
		<div id="fullscreen" class="btn-header transparent pull-right">
            <span> 
				<a href="javascript:void(0);" data-action="launchFullscreen" title="Pantalla Completa">
					<i class="fa fa-arrows-alt"></i>
				</a> 
			</span>
		</div>
		<!-- end fullscreen button -->
		<!-- collapse menu button -->
		<div id="hide-menu" class="btn-header pull-right">
            <span> 
				<a href="javascript:void(0);" data-action="toggleMenu" title="Menu">
					<i class="fa fa-reorder"></i>
				</a>
			</span>
		</div>
		<!-- end collapse menu -->

	</div>
	<!-- end pulled right: nav area -->

</header>
		<!-- END HEADER -->

		<!-- Left panel : Navigation area -->
		<aside id="left-panel">
			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                        <span>
                           <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                        </span>
						<i class="fa fa-angle-down"></i>
					</a> 
				</span>
			</div>

			<nav>
				<ul>
					<li class="">
	                <a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MENÚ PRINCIPAL"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
	            	</li>
		            <li class="text-center">
		                <a href="<?php echo base_url().'index.php/admin/dm/3/' ?>" title="REGISTRO DE EJECUCION"> <span class="menu-item-parent">REGISTRO DE EJECUCI&Oacute;N</span></a>
		            </li>
		            <?php echo $menu;?>
				</ul>
			</nav>
			<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>
		</aside>

		<!-- MAIN PANEL -->
		<div id="main" role="main">
			<!-- RIBBON -->
			<div id="ribbon">
				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>
				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li>Registro de Ejecuci&oacute;n</li><li>Registro Ejecuci&oacute;n POA</li><li>Mis Operaciones</li>
				</ol>
			</div>

			<!-- MAIN CONTENT -->
			<div id="content">
				<section id="widget-grid" class="">
					<div class="row">
						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="well well-sm well-light">
								<h2><i class="glyphicon glyphicon-th-list"></i> OPERACIONES APROBADAS</h2>
								<div id="tabs">
									<ul>
										<li>
											<a href="#tabs-c">PROYECTOS DE INVERSI&Oacute;N PUBLICA</a>
										</li>
										<li>
											<a href="#tabs-d">P. RECURRENTES</a>
										</li>
										<li>
											<a href="#tabs-b">P. NO RECURRENTES</a>
										</li>
										<li>
											<a href="#tabs-a">OPERACI&Oacute;N DE FUNCIONAMIENTO</a>
										</li>
									</ul>
									<div id="tabs-c">
										<div class="row">
											<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<div class="jarviswidget jarviswidget-color-darken" data-widget-editbutton="false" data-widget-fullscreenbutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false">
				                              <header>
				                                  <span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
				                                  <h2 class="font-md"><strong>PROYECTOS DE INVERSI&Oacute;N</strong></h2>  
				                              </header>
												<div>
													<div class="widget-body no-padding">
														<table id="dt_basic3" class="table1 table table-hover table-bordered" style="width:100%;" font-size: "7px";>
															<thead>
																<tr>
																	<th style="width:2%;">Nro.</th>
																	<th style="width:9%;">REGISTRO DE EJECUCI&Oacute;N</th>
																	<th style="width:1%;">C&Oacute;DIGO_SISIN</th>
																	<th style="width:5%;"><center>CATEGORIA PROGRAMATICA <?php echo $this->session->userdata("gestion");?></center></th>
																	<th style="width:15%;">PROGRAMA_RECURRENTE</th>
																	<th style="width:10%;">RESPONSABLE (UE)</th>
																	<th style="width:10%;">UNIDAD_EJECUTORA</th>
																	<th style="width:3%;">NUEVO_CONTINUO</th>
																	<th style="width:5%;">ANUAL_PLURIANUAL</th>
																	<th style="width:5%;"><center>PRESUPUESTO DE LA GESTI&Oacute;N <?php echo $this->session->userdata("gestion");?></center></th>
																</tr>
															</thead>
															<tbody>
																<?php echo $proyectos;?>
															</tbody>
														</table>
													</div>
													<!-- end widget content -->
												</div>
												<!-- end widget div -->
											</div>
											<!-- end widget -->
											</article>
										</div>
									</div>

									<div id="tabs-d">
										<div class="row">
											<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<div class="jarviswidget jarviswidget-color-darken"  data-widget-editbutton="false" data-widget-fullscreenbutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false">
				                              <header>
				                                  <span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
				                                  <h2 class="font-md"><strong>P. RECURRENTES</strong></h2>  
				                              </header>
												<div>
													<div class="widget-body no-padding">
														<table id="dt_basic4" class="table1 table table-hover table-bordered" style="width:100%;" font-size: "7px";>
															<thead>
																<tr>
																	<th style="width:2%;">Nro.</th>
																	<th style="width:9%;">REGISTRO DE EJECUCI&Oacute;N</th>
																	<th style="width:1%;">C&Oacute;DIGO_SISIN</th>
																	<th style="width:5%;"><center>CATEGORIA PROGRAMATICA <?php echo $this->session->userdata("gestion");?></center></th>
																	<th style="width:15%;">PROGRAMA_RECURRENTE</th>
																	<th style="width:10%;">RESPONSABLE (UE)</th>
																	<th style="width:10%;">UNIDAD_EJECUTORA</th>
																	<th style="width:3%;">NUEVO_CONTINUO</th>
																	<th style="width:5%;">ANUAL_PLURIANUAL</th>
																	<th style="width:5%;"><center>PRESUPUESTO DE LA GESTI&Oacute;N <?php echo $this->session->userdata("gestion");?></center></th>
																</tr>
															</thead>
															<tbody>
																<?php echo $precurrentes;?>
															</tbody>
														</table>
													</div>
													<!-- end widget content -->
												</div>
												<!-- end widget div -->
											</div>
											<!-- end widget -->
											</article>
										</div>
									</div>

									<div id="tabs-b">
										<div class="row">
											<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<div class="jarviswidget jarviswidget-color-darken"  data-widget-editbutton="false" data-widget-fullscreenbutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false">
				                              <header>
				                                  <span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
				                                  <h2 class="font-md"><strong>P. NO RECURRENTES</strong></h2>  
				                              </header>
												<div>
													<div class="widget-body no-padding">
														<table id="dt_basic5" class="table1 table table-hover table-bordered" style="width:100%;" font-size: "7px";>
															<thead>
																<tr>
																	<th style="width:2%;">Nro.</th>
																	<th style="width:9%;"> REGISTRO DE EJECUCI&Oacute;N</th>
																	<th style="width:1%;">C&Oacute;DIGO_SISIN</th>
																	<th style="width:5%;"><center>CATEGORIA PROGRAMATICA <?php echo $this->session->userdata("gestion");?></center></th>
																	<th style="width:15%;">PROGRAMA_RECURRENTE</th>
																	<th style="width:10%;">RESPONSABLE (UE)</th>
																	<th style="width:10%;">UNIDAD_EJECUTORA</th>
																	<th style="width:3%;">NUEVO_CONTINUO</th>
																	<th style="width:5%;">ANUAL_PLURIANUAL</th>
																	<th style="width:5%;"><center>PRESUPUESTO DE LA GESTI&Oacute;N <?php echo $this->session->userdata("gestion");?></center></th>
																</tr>
															</thead>
															<tbody>
																<?php echo $pnrecurrentes;?>
															</tbody>
														</table>
													</div>
													<!-- end widget content -->
												</div>
												<!-- end widget div -->
											</div>
											<!-- end widget -->
											</article>
										</div>
									</div>
									
									<div id="tabs-a">
										<div class="row">
											<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<div class="jarviswidget jarviswidget-color-darken"  data-widget-editbutton="false" data-widget-fullscreenbutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false">
				                              <header>
				                                  <span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
				                                  <h2 class="font-md"><strong>OPERACI&Oacute;N DE FUNCIONAMIENTO</strong></h2>  
				                              </header>
												<div>
													<div class="widget-body no-padding">
														<table id="dt_basic" class="table table-hover table-bordered" style="width:100%;" font-size: "7px";>
															<thead>
																<tr>
																	<th style="width:2%;">Nro.</th>
																	<th style="width:9%;">REGISTRO DE EJECUCI&Oacute;N</th>
																	<th style="width:1%;">C&Oacute;DIGO_SISIN</th>
																	<th style="width:5%;"><center>CATEGORIA PROGRAMATICA <?php echo $this->session->userdata("gestion");?></center></th>
																	<th style="width:15%;">PROGRAMA_RECURRENTE</th>
																	<th style="width:10%;">RESPONSABLE (UE)</th>
																	<th style="width:10%;">UNIDAD_EJECUTORA</th>
																	<th style="width:3%;">NUEVO_CONTINUO</th>
																	<th style="width:5%;">ANUAL_PLURIANUAL</th>
																	<th style="width:5%;"><center>PRESUPUESTO DE LA GESTI&Oacute;N <?php echo $this->session->userdata("gestion");?></center></th>
																</tr>
															</thead>
															<tbody>
																<?php echo $operaciones;?>
															</tbody>
														</table>
													</div>
													<!-- end widget content -->
												</div>
												<!-- end widget div -->
											</div>
											<!-- end widget -->
											</article>
										</div>
									</div>
									
								</div>
							</div>
						</article>
					</div>
				</section>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN PANEL -->
		<!-- ========================================================================================================= -->
		<!-- PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->
		<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
		<script data-pace-options='{ "restartOnRequestAfter": true }' src="<?php echo base_url(); ?>assets/js/plugin/pace/pace.min.js"></script>
		<script>
			if (!window.jQuery) {
				document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-2.0.2.min.js"><\/script>');
			}
		</script>
		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>
		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url(); ?>assets/js/app.config.js"></script>
		<script src = "<?php echo base_url(); ?>mis_js/programacion/programacion/tablas.js"></script>
		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.min.js"></script>
		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url(); ?>assets/js/notification/SmartNotification.min.js"></script>
		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url(); ?>assets/js/smartwidgets/jarvis.widget.min.js"></script>
		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<!-- SPARKLINES -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/select2/select2.min.js"></script>
		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
		<!-- browser msie issue fix -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/fastclick/fastclick.min.js"></script>
		<!-- Demo purpose only -->
		<script src="<?php echo base_url(); ?>assets/js/demo.min.js"></script>
		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url(); ?>assets/js/app.min.js"></script>
		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<!-- Voice command : plugin -->
		<script src="<?php echo base_url(); ?>assets/js/speech/voicecommand.min.js"></script>
		<!-- PAGE RELATED PLUGIN(S) -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.colVis.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.tableTools.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function() {
				pageSetUp();
				// menu
				$("#menu").menu();
				$('.ui-dialog :button').blur();
				$('#tabs').tabs();
			})

            function valida_vatos(id){
                var iden='_'+id;
                $(document).ready(function(){
                    var cont='<strong style="font-size:20px">Procesando</strong><img src="<?php echo base_url(); ?>assets/img_v1.1/preloader.gif" alt="<?php echo $this->session->userdata('name')?>">';
                    $('#'+iden).html(cont);
                    $.post("<?php echo base_url(); ?>index.php/admin/cargar_partida_proy", { id: id }, function(data){
                        $('#'+iden).html(data);
                    });
                });
            }
		</script>
	</body>
</html>
