<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
		<title><?php echo $this->session->userdata('name')?></title>
		<meta name="description" content="">
		<meta name="author" content="">
			
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.core.css"/>
    	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css" id="toggleCSS"/>
          <!--fin de stiloh-->
        <style type="text/css">
		/*////scroll tablas/////*/
		    .table{
		     display: inline-block;
		     width:100%;
		     overflow-x: scroll;
		     }
		     th, td {
	                padding: 1.4px;
	                text-align: center;
	                font-size: 9px;
	            }
		/*///fin de scroll//*/
          </style>
	</head>
	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->

		<!-- HEADER -->
		<header id="header">
			<!-- pulled right: nav area -->
			<div class="pull-right">
				<!-- collapse menu button -->
				<div id="hide-menu" class="btn-header pull-right">
					<span> <a href="javascript:void(0);" data-action="toggleMenu" title="Menu"><i class="fa fa-reorder"></i></a> </span>
				</div>
				<!-- end collapse menu -->
				<!-- logout button -->
				<div id="logout" class="btn-header transparent pull-right">
					<span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Salir" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
				</div>
				<!-- end logout button -->
				<!-- search mobile button (this is hidden till mobile view port) -->
				<div id="search-mobile" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
				</div>
				<!-- end search mobile button -->
				<!-- fullscreen button -->
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Pantalla Completa"><i class="fa fa-arrows-alt"></i></a> </span>
				</div>
				<!-- end fullscreen button -->
			</div>
			<!-- end pulled right: nav area -->
		</header>
		<!-- END HEADER -->

		<!-- Left panel : Navigation area -->
		<aside id="left-panel">
			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                        <span>
                            <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                        </span>
						<i class="fa fa-angle-down"></i>
					</a> 
				</span>
			</div>
			<nav>
				<ul>
					<li class="">
	                <a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MENÚ PRINCIPAL">
	                <i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
	            	</li>
		            <li class="text-center">
		                <a href="<?php echo base_url().'index.php/admin/dm/3/' ?>" title="REGISTRO DE EJECUCION"> <span class="menu-item-parent">REGISTRO DE EJECUCI&Oacute;N</span></a>
		            </li>
					<?php
			                for($i=0;$i<count($enlaces);$i++)
			                {
			                    if(count($subenlaces[$enlaces[$i]['o_child']])>0)
			                    {
			            ?>
			            <li>
			              	<a href="#" >
			              		<i class="<?php echo $enlaces[$i]['o_image']?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
			              	<ul >
			              	<?php
			                foreach ($subenlaces[$enlaces[$i]['o_child']] as $item) {
			                ?>
			                <li><a href="<?php echo base_url($item['o_url']); ?>"><?php echo $item['o_titulo']; ?></a></li>
			                <?php } ?>
			                </ul>
			            </li>
			            <?php 
			            }
			        } ?>
				</ul>
			</nav>
			<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>
		</aside>

		<!-- MAIN PANEL -->
		<div id="main" role="main">

			<!-- RIBBON -->
			<div id="ribbon">

				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>

				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li>Registro de Ejecuci&oacute;n</li><li><a href="<?php echo base_url().'index.php/admin/ejec/prog/'.$tipo.'' ?>" title="MIS OPERACIONES">Registro de Ejecuci&oacute;n POA</a></li><li>Programa Recurrente</li><li>Ejecutaci&oacute;n F&iacute;sica Mes <?php echo $mes ?> (Borrador)</li>
				</ol>
			</div>
			<!-- END RIBBON -->
			<!-- MAIN CONTENT -->
			<div id="content">
						<?php
						    $attributes = array('class' => 'form-horizontal', 'id' => 'formulario','name' =>'formulario','enctype' => 'multipart/form-data');
						    echo validation_errors();
						    echo form_open('admin/ejec/valida', $attributes);
						?> 
				<section id="widget-grid" class="">
					<div class="row">
					<article class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
		                  <section id="widget-grid" class="well">
	                        <div class="">
	                            <h1> FORMULARIO DE REGISTRO DE EJECUCI&Oacute;N DE LA OPERACI&Oacute;N</h1>
	                            <h1> CATEGORIA PROGRAM&Aacute;TICA : <small><?php echo $proyecto[0]['aper_programa'].''.$proyecto[0]['aper_proyecto'].''.$proyecto[0]['aper_actividad']?></small>
								<h1> <?php echo $titulo_proy;?> : <small><?php echo $proyecto[0]['proy_nombre']?></small></h1>
      							<h1> COMPONENTE : <small><?php echo $componente[0]['com_componente']?></small></h1>
      							<h1> GESTI&Oacute;N ACTUAL : <small><?php echo $this->session->userdata("gestion")?></small>&nbsp;&nbsp;MES : <small><?php echo $mes?></small></h1>
	                        </div>
		                  </section>
		                 
			                <section id="widget-grid" class="well">
			                <center><b>PERIODO DEL INFORME</b></center><br>
			                	<div class="row">
			                    <center>
			                    <?php
			                    	if($fase_ejec[0]['f_inicio_pinforme']!='')
			                    	{
			                    		?>
			                    		<div class="col-sm-6">
											<label><b>FECHA INICIO </b></font></label>
											<div class="input-group">
												<input type="text" name="fpi" id="fpi" placeholder="Seleccione Fecha inicial del Periodo de Informe" value="<?php echo date('d/m/Y',strtotime($fase_ejec[0]['f_inicio_pinforme'])); ?>" class="form-control datepicker" data-dateformat="dd/mm/yy" >
												<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
											</div>
				                       </div>
				                       <div class="col-sm-6">
											<label><b>FECHA FINAL </b></font></label>
											<div class="input-group">
												<input type="text" name="fpf" id="fpf" placeholder="Seleccione Fecha Final del Periodo de Informe" value="<?php echo date('d/m/Y',strtotime($fase_ejec[0]['f_final_pinforme'])); ?>" class="form-control datepicker" data-dateformat="dd/mm/yy" >
												<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
											</div>
				                       </div>
			                    		<?php
			                    	}
			                    	else
			                    	{
			                    		?>
			                    		<div class="col-sm-6">
											<label><b>FECHA INICIO </b></font></label>
											<div class="input-group">
												<input type="text" name="fpi" id="fpi" placeholder="Seleccione Fecha inicial del Periodo de Informe" value="<?php echo date('d/m/Y'); ?>" class="form-control datepicker" data-dateformat="dd/mm/yy" >
												<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
											</div>
				                       </div>
				                       <div class="col-sm-6">
											<label><b>FECHA FINAL </b></font></label>
											<div class="input-group">
												<input type="text" name="fpf" id="fpf" placeholder="Seleccione Fecha Final del Periodo de Informe" value="<?php echo date('d/m/Y'); ?>" class="form-control datepicker" data-dateformat="dd/mm/yy" >
												<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
											</div>
				                       </div>
			                    		<?php
			                    	}
			                    ?>
			                       
			                    </center>
			                    </div>
			                </section>
						</article>
			            <article class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
			            	<section id="widget-grid" class="well">
								<a href='<?php echo site_url("admin").'/ejec/archivos/'.$tipo.'/'.$proyecto[0]['proy_id'].'/'.$fase[0]['id'].'/'.$componente[0]['com_id'].'/'.$mes_id.''; ?>'title="ARCHIVOS/DOCUMENTOS" class="btn btn-primary" style="width:100%;">ANEXOS DE LA EJECUCI&Oacute;N</a>
							</center>
			                </section>
			                <section id="widget-grid" class="well">
			                	<div class="row">
			                    <center>
			                        <center><b>ESTADO DE LA OPERACI&Oacute;N</b></center><br>
									<div class="col-sm-6">
										<select class="form-control" id="est" name="est" title="Seleccione Estado del Proyecto">
								        	<option value="">Seleccione estado </option>
								            <?php 
											foreach($proy_estado as $row)
											{
												if($row['ep_id']==$fase_ejec[0]['estado'])
												{
												?>
												<option value="<?php echo $row['ep_id']; ?>" selected><?php echo $row['ep_descripcion']; ?></option>
												<?php 
												}
												else
												{
												?>
													<option value="<?php echo $row['ep_id']; ?>"><?php echo $row['ep_descripcion']; ?></option>
												<?php 
												}
											}
											?>     
								        </select>
									</div>

									<div class="col-sm-6">
										<select class="form-control" id="dep" name="dep">
										<?php 
											foreach($dependecias as $row)
											{
												if($row['st_id']==$fase_ejec[0]['st_id'])
												{
												?>
													<option value="<?php echo $row['st_id']; ?>" selected><?php echo $row['st_descripcion']; ?></option>
												<?php 
												}
												else
												{
												?>
													<option value="<?php echo $row['st_id']; ?>"><?php echo $row['st_descripcion']; ?></option>
												<?php 
												}
												
											}
										?> 
				                    	</select>
									</div><br>
				                    <?php
									if($fase_ejec[0]['st_id']==8)
									{
										?>
										<div id="contrato">
								        <div class="form-actions" align="center">
									        <center><a href="<?php echo base_url().'index.php/admin/ejec/list_contratos/'.$tipo.'/'.$proyecto[0]['proy_id'].'/'.$id_f.'/'.$componente[0]['com_id'].'/'.$proyecto[0]['aper_programa'].'/'.$mes_id ?>" class="btn btn-primary" style="width:100%;">REGISTRO DE CONTRATO</a></center>
										</div>
										</div>
										<?php
									}
									else
									{
										?>
										<div id="contrato" style="display:none;">
								        <div class="form-actions" align="center">
									        <center><a href="<?php echo base_url().'index.php/admin/ejec/list_contratos/'.$tipo.'/'.$proyecto[0]['proy_id'].'/'.$id_f.'/'.$componente[0]['com_id'].'/'.$proyecto[0]['aper_programa'].'/'.$mes_id ?>" class="btn btn-primary" style="width:100%;">REGISTRO DE CONTRATO</a></center>
										</div>
										</div>
										<?php
									}
									?>
			                    </center>
			                    </div>
			                </section>
			            </article>

			            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			            	<input type="hidden" name="id_proy" id="id_proy" value="<?php echo $proyecto[0]['proy_id'] ?>"><!-- id proyecto -->
							<input type="hidden" name="id_f" id="id_f" value="<?php echo $id_f ?>"><!-- id fase -->
							<input type="hidden" name="tp" id="tp" value="<?php echo $tipo ?>"><!-- tp proyecto -->
							<input type="hidden" name="id_c" id="id_c" value="<?php echo $componente[0]['com_id'] ?>"><!-- id componente -->
							<input type="hidden" name="mes_id" id="mes_id" value="<?php echo $mes_id ?>"><!-- Mes Activo -->
							<input type="hidden" name="prog" id="prog" value="<?php echo $proyecto[0]['aper_programa'] ?>"><!-- prog -->
							<input type="hidden" name="accion" id="accion" value="2"><!-- tipo de accion -->

							<section id="widget-grid" class="">
								<!-- row -->
								<?php
								if($fase_ejec[0]['st_id']==1 || $fase_ejec[0]['st_id']==2)
								{
									?>
									<div id="ejec" >
									<div class="well"><!-- well -->
										<h2 class="alert alert-success"><center><?php echo $titulo_proy ?></center></h2>
										<?php $idc=1;
										    echo "<style>table{font-size: 9px;width: 100%;}</style>";
										    $prod=$this->model_producto->list_prod($componente[0]['com_id']);
										    $nrop=1;
										    foreach($prod as $rowp)
										    {	$ti='';
			                                    if($rowp['indi_id']==2){ $ti='%';}
										    	/*=========================================== TABLA DE PRODUCTOS ===========================================*/
										    	?>
										    	<input class="form-control" type="hidden" name="producto[]" value="<?php echo $rowp['prod_id'];?>">
										    	<input class="form-control" type="hidden" name="indi[]" value="<?php echo $rowp['indi_id'];?>">
										    	<?php
										    	echo'<table  class="table table-bordered" style="width:100%;">
			                                        <thead>                             
			                                            <tr>
			                                                <td style="width:2%;" bgcolor="#3B3B3F"><font color="#ffffff"><b>Nro.</b></font></td>
			                                                <td style="width:15%;" bgcolor="#3B3B3F"><font color="#ffffff"><b>PRODUCTO DE LA OPERACI&Oacute;N</b></font></td>
			                                                <td style="width:15%;" bgcolor="#3B3B3F"><font color="#ffffff"><b>ACTIVIDADES DE LA OPERACI&Oacute;N</b></font></td>
			                                                <td style="width:5%;" bgcolor="#3B3B3F"><font color="#ffffff"><b>COSTO ACTIVIDAD</b></font></td>
			                                                <td style="width:5%;" bgcolor="#3B3B3F"><font color="#ffffff"><b>TIPO DE INDICADOR</b></font></td>
			                                                <td style="width:10%;" bgcolor="#3B3B3F"><font color="#ffffff"><b>INDICADOR</b></font></td>
			                                                <td style="width:5%;" bgcolor="#3B3B3F"><font color="#ffffff"><b>LINEA BASE</b></font></td>
			                                                <td style="width:5%;" bgcolor="#3B3B3F"><font color="#ffffff"><b>META</b></font></td>
			                                                <td style="width:5%;" bgcolor="#3B3B3F"><font color="#ffffff"><b>%PONDERACI&Oacute;N</b></font></td>
			                                                <td style="width:15%;" bgcolor="#3B3B3F"><font color="#ffffff"><b>VERIFICACI&Oacute;N</b></font></td>
			                                                <td style="width:20%;" bgcolor="#3B3B3F"><font color="#ffffff"><b>EJECUCI&Oacute;N PRODUCTO '.$mes.' '.$this->session->userdata("gestion").'</b></font></td>
			                                            </tr>
							                        </thead>
							                        <tbody>
							                        <tr bgcolor="#e8e8e8">
							                        	<td>'.$nrop.'</td>
							                        	<td>'.$rowp['prod_producto'].'</td>
							                        	<td></td>
							                        	<td></td>
			                                            <td>'.$rowp['indi_abreviacion'].'</td>
			                                            <td>'.$rowp['prod_indicador'].'</td>
			                                            <td>'.round($rowp['prod_linea_base'],2).' '.$ti.'</td>
			                                            <td>'.round($rowp['prod_meta'],2).' '.$ti.'</td>
			                                            <td>'.$rowp['prod_ponderacion'].' %</td>
			                                            <td>'.$rowp['prod_fuente_verificacion'].'</td>';

			                                            $programado = $this->model_producto->list_prodgest_anual($rowp['prod_id']);
			                                            $ejecutado=$this->model_producto->prod_ejec_mensual($rowp['prod_id'],$this->session->userdata("gestion")); /// ejecutado
	                                               			$n=0;   
	                                                          /*------------------- programado gestion ------------------*/
	                                                              $nro=0;
	                                                              foreach($programado as $row)
	                                                              {
	                                                                $nro++;
	                                                                $matriz [1][$nro]=$row['m_id'];
	                                                                $matriz [2][$nro]=$row['pg_fis'];
	                                                              }
	                                                        /*--------------------------------------------------------*/

	                                                        /*---------------- llenando la matriz vacia --------------*/
	                                                              for($j = 1; $j<=12; $j++)
	                                                              {
	                                                                $matriz_r[1][$j]=$j;
	                                                                $matriz_r[2][$j]='0';
	                                                                $matriz_r[3][$j]='0';
	                                                                $matriz_r[4][$j]='0';
	                                                                $matriz_r[5][$j]='0';
	                                                                $matriz_r[6][$j]='0';
	                                                                $matriz_r[7][$j]='0';
	                                                                $matriz_r[8][$j]='0';
	                                                                $matriz_r[9][$j]='0';
	                                                                $matriz_r[10][$j]='0';
	                                                              }
	                                                        /*--------------------------------------------------------*/
	                                                        /*--------------------ejecutado gestion ------------------*/
	                                                              $nro_e=0;
	                                                              foreach($ejecutado as $row)
	                                                              {
	                                                                  $nro_e++;
	                                                                  $matriz_e [1][$nro_e]=$row['m_id'];
	                                                                  $matriz_e [2][$nro_e]=$row['pejec_fis'];
	                                                                  $matriz_e [3][$nro_e]=$row['pejec_fis_a'];
	                                                                  $matriz_e [4][$nro_e]=$row['pejec_fis_b'];
	                                                              }
	                                                        /*--------------------------------------------------------*/
	                                                            /*------- asignando en la matriz P, PA, %PA ----------*/
	                                                              for($i = 1 ;$i<=$nro ;$i++)
	                                                              {
	                                                                for($j = 1 ;$j<=12 ;$j++)
	                                                                {
	                                                                  if($matriz[1][$i]==$matriz_r[1][$j])
	                                                                  {
	                                                                    $matriz_r[2][$j]=round($matriz[2][$i],2);
	                                                                  }
	                                                                }
	                                                              }

	                                                              $pa=0;        
	                                                              for($j = 1 ;$j<=12 ;$j++){
	                                                                $pa=$pa+$matriz_r[2][$j];
	                                                                $matriz_r[3][$j]=$pa+$rowp['prod_linea_base'];
	                                                                if($rowp['prod_meta']!=0)
	                                                                	$matriz_r[4][$j]=round(((($pa+$rowp['prod_linea_base'])/$rowp['prod_meta'])*100),2);
	                                                              }

	                                                              if($rowp['indi_id']==1)
	                                                              {
	                                                                for($i = 1 ;$i<=$nro_e ;$i++){
	                                                                    for($j = 1 ;$j<=12 ;$j++)
	                                                                    {
	                                                                        if($matriz_e[1][$i]==$matriz_r[1][$j])
	                                                                        {
	                                                                            $matriz_r[7][$j]=round($matriz_e[2][$i],2);
	                                                                        }
	                                                                    }
	                                                                }
	                                                              }
	                                                              elseif ($rowp['indi_id']==2) 
	                                                              {
	                                                                for($i = 1 ;$i<=$nro_e ;$i++){
	                                                                      for($j = 1 ;$j<=12 ;$j++)
	                                                                      {
	                                                                          if($matriz_e[1][$i]==$matriz_r[1][$j])
	                                                                          {
	                                                                              $matriz_r[5][$j]=round($matriz_e[3][$i],2);
	                                                                              $matriz_r[6][$j]=round($matriz_e[4][$i],2);
	                                                                              $matriz_r[7][$j]=round($matriz_e[2][$i],2);
	                                                                          }
	                                                                      }
	                                                                  }
	                                                              /*--------------------------------------------------------*/
	                                                              }
	                                                              /*--------------------matriz E,AE,%AE gestion ------------------*/
	                                                              $pe=0; 
	                                                              for($j = 1 ;$j<=12 ;$j++){
	                                                                $pe=$pe+$matriz_r[7][$j];
	                                                                $matriz_r[8][$j]=$pe+$rowp['prod_linea_base'];
	                                                                if($rowp['prod_meta']!=0)
	                                                                	$matriz_r[9][$j]=round(((($pe+$rowp['prod_linea_base'])/$rowp['prod_meta'])*100),2);
	                                                                if($matriz_r[4][$j]!=0)
	                                                                	$matriz_r[10][$j]=round((($matriz_r[9][$j]/$matriz_r[4][$j])*100),2);
	                                                                }
															echo '<td>';
															?>
															<table class="table-bordered" border="1">
																<tr bgcolor="#3B3B3F">
																	<td><font color="#ffffff"><b>PROGRAMADO:</b></font></td>
																		<input type="hidden" name="pp[]" id="pp" value="<?php echo $matriz_r[2][$mes_id];?>">
																	<td><input class="form-control" type="text" value="<?php echo $matriz_r[2][$mes_id];?>" disabled="true"></td>
																</tr>
															<?php 
															if($rowp['indi_id']==1)
															{
															?>	<input class="form-control" type="hidden" name="den[]" value="<?php echo $rowp['prod_denominador'];?>">
																<input class="form-control" type="hidden" name="pa[]" value="<?php echo $matriz_r[5][$mes_id];?>"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }">
																<input class="form-control" type="hidden" name="pb[]" value="<?php echo $matriz_r[6][$mes_id];?>"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }">
																<tr bgcolor="#3B3B3F">
																	<td><font color="#ffffff"><b>EJECUTADO:</b></font></td>
																	<td><input class="form-control" type="text" name="pe[]" id="pe" value="<?php echo $matriz_r[7][$mes_id];?>"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"></td>
																</tr>
															<?php
															}
															if($rowp['indi_id']==2)
															{
															?>	<input  type="hidden" name="den[]" value="<?php echo $rowp['prod_denominador'];?>">
																<tr bgcolor="#3B3B3F">
																	<td><font color="#ffffff"><b>A-NUMERADOR</b></td>
																	<td><input class="form-control" type="text" name="pa[]" value="<?php echo $matriz_r[5][$mes_id];?>"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"></td>
																</tr>
																<tr bgcolor="#3B3B3F">
																	<td><font color="#ffffff"><b>B-DENOMINADOR</b></td>
																	<td><input class="form-control" type="text" name="pb[]" value="<?php echo $matriz_r[6][$mes_id];?>"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"></td>
																</tr>
																<input class="form-control" type="hidden" name="pe[]" id="pe" value="<?php echo $matriz_r[7][$mes_id];?>"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }">
																
															<?php	
															}
															echo '</table>';
															echo '</td>';
					                        echo '</tr>';
					                        /*=================================================== END PRODUCTOS =====================================================================*/
					                        $act=$this->model_actividad->list_act_anual($rowp['prod_id']);
					                        $nroa=1;
					                        foreach($act as $rowa)
										    {	$ti='';
			                                    if($rowa['indi_id']==2){ $ti='%';}
			                                ?>
									    	<input class="form-control" type="hidden" name="actividad[]" value="<?php echo $rowa['act_id'];?>">
									    	<input class="form-control" type="hidden" name="prod[]" value="<?php echo $rowp['prod_id'];?>">
									    	<input class="form-control" type="hidden" name="cost_u[]" value="<?php echo $rowa['act_costo_uni'];?>">
									    	<input class="form-control" type="hidden" name="india[]" value="<?php echo $rowa['indi_id'];?>">
									    	<?php
										    /*=========================================== TABLA DE PRODUCTOS ===========================================*/
										   	echo'<tr bgcolor="#b0f1ef">
							                        	<td>'.$nroa.'</td>
							                        	<td></td>
							                        	<td>'.$rowa['act_actividad'].'</td>
			                                            <td>'.number_format($rowa['act_costo_uni'], 2, ',', ' ').'Bs.</td>
			                                            <td>'.$rowa['indi_abreviacion'].'</td>
			                                            <td>'.$rowa['act_indicador'].'</td>
			                                            <td>'.round($rowa['act_linea_base'],2).' '.$ti.'</td>
			                                            <td>'.round($rowa['act_meta'],2).' '.$ti.'</td>
			                                            <td>'.$rowa['act_ponderacion'].' %</td>
			                                            <td>'.$rowa['act_fuente_verificacion'].'</td>';
			                                            echo'<td>';
			                                                $programado=$this->model_actividad->act_prog_mensual($rowa['act_id'],$this->session->userdata("gestion")); /// programado
				                                            $ejecutado=$this->model_actividad->act_ejec_mensual($rowa['act_id'],$this->session->userdata("gestion")); /// ejecutado
				                                            $nro=0;
				                                            foreach($programado as $row)
				                                            {
				                                              $nro++;
				                                              $matriz [1][$nro]=$row['m_id'];
				                                              $matriz [2][$nro]=$row['pg_fis'];
				                                            }

				                                            /*---------------- llenando la matriz vacia --------------*/
				                                            for($j = 1; $j<=12; $j++)
				                                            {
				                                              $matriz_r[1][$j]=$j;
				                                              $matriz_r[2][$j]='0';
				                                              $matriz_r[3][$j]='0';
				                                              $matriz_r[4][$j]='0';
				                                              $matriz_r[5][$j]='0';
				                                              $matriz_r[6][$j]='0';
				                                              $matriz_r[7][$j]='0';
				                                              $matriz_r[8][$j]='0';
				                                              $matriz_r[9][$j]='0';
				                                              $matriz_r[10][$j]='0';
				                                            }
				                                            /*--------------------------------------------------------*/

				                                            /*--------------------ejecutado gestion ------------------*/
				                                            $nro_e=0;
				                                            foreach($ejecutado as $row)
				                                            {
				                                              $nro_e++;
				                                              $matriz_e [1][$nro_e]=$row['m_id'];
				                                              $matriz_e [2][$nro_e]=$row['ejec_fis'];
				                                              $matriz_e [3][$nro_e]=$row['ejec_fis_a'];
				                                              $matriz_e [4][$nro_e]=$row['ejec_fis_b'];
				                                            }
				                                            /*--------------------------------------------------------*/
				                                            /*------- asignando en la matriz P, PA, %PA ----------*/
				                                            for($i = 1 ;$i<=$nro ;$i++)
				                                            {
				                                              for($j = 1 ;$j<=12 ;$j++)
				                                              {
				                                                if($matriz[1][$i]==$matriz_r[1][$j])
				                                                {
				                                                    $matriz_r[2][$j]=round($matriz[2][$i],2);
				                                                }
				                                              }
				                                            }
				                                            $pa=0;
				                                            for($j = 1 ;$j<=12 ;$j++){
				                                              $pa=$pa+$matriz_r[2][$j];
				                                              $matriz_r[3][$j]=$pa+$rowa['act_linea_base'];
				                                              if($rowa['act_meta']!=0)
				                                              	$matriz_r[4][$j]=round(((($pa+$rowa['act_linea_base'])/$rowa['act_meta'])*100),2);
				                                            }

				                                            if($rowa['indi_id']==1)
				                                            {
				                                              for($i = 1 ;$i<=$nro_e ;$i++){
				                                                for($j = 1 ;$j<=12 ;$j++)
				                                                {
				                                                  if($matriz_e[1][$i]==$matriz_r[1][$j])
				                                                  {
				                                                      $matriz_r[7][$j]=round($matriz_e[2][$i],2);
				                                                  }
				                                                }
				                                              }
				                                            }
				                                            elseif ($rowa['indi_id']==2) 
				                                            {
				                                              for($i = 1 ;$i<=$nro_e ;$i++){
				                                                  for($j = 1 ;$j<=12 ;$j++)
				                                                  {
				                                                    if($matriz_e[1][$i]==$matriz_r[1][$j])
				                                                      {
				                                                          $matriz_r[5][$j]=round($matriz_e[3][$i],2);
				                                                          $matriz_r[6][$j]=round($matriz_e[4][$i],2);
				                                                          $matriz_r[7][$j]=round($matriz_e[2][$i],2);
				                                                      }
				                                                  }
				                                                }
				                                              /*--------------------------------------------------------*/
				                                              }
				                                              /*--------------------matriz E,AE,%AE gestion ------------------*/
				                                                $pe=0;
				                                                for($j = 1 ;$j<=12 ;$j++){
				                                                  $pe=$pe+$matriz_r[7][$j];
				                                                  $matriz_r[8][$j]=$pe+$rowa['act_linea_base'];
				                                                  if($rowa['act_meta']!=0)
				                                                  	$matriz_r[9][$j]=round(((($pe+$rowa['act_linea_base'])/$rowa['act_meta'])*100),2);
				                                                  if($matriz_r[4][$j]!=0)
				                                                    $matriz_r[10][$j]=round((($matriz_r[9][$j]/$matriz_r[4][$j])*100),2);
				                                                }
															        ?>
																	<table class="table-bordered" border="1">
																		<tr bgcolor="#568A89">
																			<td><font color="#ffffff"><b>PROGRAMADO: </b></font></td>
																			<input class="form-control" type="hidden" name="ap[]" value="<?php echo $matriz_r[2][$mes_id];?>">
																			<td><input class="form-control" type="text"  value="<?php echo $matriz_r[2][$mes_id];?>" disabled="true"></td>
																		</tr>
																	<?php 
																	if($rowa['indi_id']==2)
																	{
																	?>	<input  type="hidden" name="dena[]" value="<?php echo $rowa['act_denominador'];?>">
																		<tr bgcolor="#568A89">
																			<td><font color="#ffffff"><b>A-NUMERADOR</b></font></td>
																			<td><input class="form-control" type="text" name="aa[]" id="aa[]" value="<?php echo $matriz_r[5][$mes_id];?>"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"></td>
																		</tr>
																		<tr bgcolor="#568A89">
																			<td><font color="#ffffff"><b>B-DENOMINADOR</b></font></td>
																			<td><input class="form-control" type="text" name="ab[]" id="ab[]" value="<?php echo $matriz_r[6][$mes_id];?>"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"></td>
																		</tr>
																		<input class="form-control" type="hidden" name="ae[]" id="ae[]" value="<?php echo $matriz_r[7][$mes_id];?>">
																	<?php
																	}
																	elseif($rowa['indi_id']==1)
																	{
																	?>	<input  type="hidden" name="dena[]" value="<?php echo $rowa['act_denominador'];?>">
																		<input class="form-control" type="hidden" name="aa[]" id="aa[]" value="<?php echo $matriz_r[5][$mes_id];?>">
																		<input class="form-control" type="hidden" name="ab[]" id="ab[]" value="<?php echo $matriz_r[6][$mes_id];?>">
																		<tr bgcolor="#568A89">
																			<td><font color="#ffffff"><b>EJECUTADO:</b></font></td>
																			<td><input class="form-control" type="text" name="ae[]" id="ae[]" value="<?php echo $matriz_r[7][$mes_id];?>"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"></td>
																		</tr>
																	<?php
																	}
														echo '</table>';			
			                                            echo'</td>';
			                                        echo '</tr>';
			                                    $nroa++;
			                                }
										    $nrop++;
										    } 
										echo "</tbody>
					                        </table>";
									?>		
									</div>
								</div><!-- well -->
									<?php
								}
								else
								{
									?>
									<div id="ejec" style="display:none;">
									<div class="well"><!-- well -->
										<h2 class="alert alert-success"><center><?php echo $titulo_proy ?></center></h2>
										<?php $idc=1;
									    echo "<style>table{font-size: 9px;width: 100%;}</style>";
									    $prod=$this->model_producto->list_prod($componente[0]['com_id']);
									    $nrop=1;
									    foreach($prod as $rowp)
									    {	$ti='';
		                                    if($rowp['indi_id']==2){ $ti='%';}
									    	/*=========================================== TABLA DE PRODUCTOS ===========================================*/
									    	?>
									    	<input class="form-control" type="hidden" name="producto[]" value="<?php echo $rowp['prod_id'];?>">
									    	<input class="form-control" type="hidden" name="indi[]" value="<?php echo $rowp['indi_id'];?>">
									    	<?php
									    	echo'<table  class="table table-bordered" style="width:100%;">
		                                        <thead>                             
		                                            <tr>
		                                                <td style="width:2%;" bgcolor="#3B3B3F"><font color="#ffffff"><b>Nro.</b></font></td>
		                                                <td style="width:15%;" bgcolor="#3B3B3F"><font color="#ffffff"><b>PRODUCTO DE LA OPERACI&Oacute;N</b></font></td>
		                                                <td style="width:15%;" bgcolor="#3B3B3F"><font color="#ffffff"><b>ACTIVIDADES DE LA OPERACI&Oacute;N</b></font></td>
		                                                <td style="width:5%;" bgcolor="#3B3B3F"><font color="#ffffff"><b>COSTO ACTIVIDAD</b></font></td>
		                                                <td style="width:5%;" bgcolor="#3B3B3F"><font color="#ffffff"><b>TIPO DE INDICADOR</b></font></td>
		                                                <td style="width:10%;" bgcolor="#3B3B3F"><font color="#ffffff"><b>INDICADOR</b></font></td>
		                                                <td style="width:5%;" bgcolor="#3B3B3F"><font color="#ffffff"><b>LINEA BASE</b></font></td>
		                                                <td style="width:5%;" bgcolor="#3B3B3F"><font color="#ffffff"><b>META</b></font></td>
		                                                <td style="width:5%;" bgcolor="#3B3B3F"><font color="#ffffff"><b>%PONDERACI&Oacute;N</b></font></td>
		                                                <td style="width:15%;" bgcolor="#3B3B3F"><font color="#ffffff"><b>MEDIOS DE VERIFICACI&Oacute;N</b></font></td>
		                                                <td style="width:20%;" bgcolor="#3B3B3F"><font color="#ffffff"><b>EJECUCI&Oacute;N PRODUCTO '.$mes.' '.$this->session->userdata("gestion").'</b></font></td>
		                                            </tr>
						                        </thead>
						                        <tbody>
						                        <tr bgcolor="#e8e8e8">
						                        	<td>'.$nrop.'</td>
						                        	<td>'.$rowp['prod_producto'].'</td>
						                        	<td></td>
						                        	<td></td>
		                                            <td>'.$rowp['indi_abreviacion'].'</td>
		                                            <td>'.$rowp['prod_indicador'].'</td>
		                                            <td>'.round($rowp['prod_linea_base'],2).' '.$ti.'</td>
		                                            <td>'.round($rowp['prod_meta'],2).' '.$ti.'</td>
		                                            <td>'.$rowp['prod_ponderacion'].' %</td>
		                                            <td>'.$rowp['prod_fuente_verificacion'].'</td>';

		                                            $programado = $this->model_producto->list_prodgest_anual($rowp['prod_id']);
		                                            $ejecutado=$this->model_producto->prod_ejec_mensual($rowp['prod_id'],$this->session->userdata("gestion")); /// ejecutado
                                               			$n=0;   
                                                          /*------------------- programado gestion ------------------*/
                                                              $nro=0;
                                                              foreach($programado as $row)
                                                              {
                                                                $nro++;
                                                                $matriz [1][$nro]=$row['m_id'];
                                                                $matriz [2][$nro]=$row['pg_fis'];
                                                              }
                                                        /*--------------------------------------------------------*/

                                                        /*---------------- llenando la matriz vacia --------------*/
                                                              for($j = 1; $j<=12; $j++)
                                                              {
                                                                $matriz_r[1][$j]=$j;
                                                                $matriz_r[2][$j]='0';
                                                                $matriz_r[3][$j]='0';
                                                                $matriz_r[4][$j]='0';
                                                                $matriz_r[5][$j]='0';
                                                                $matriz_r[6][$j]='0';
                                                                $matriz_r[7][$j]='0';
                                                                $matriz_r[8][$j]='0';
                                                                $matriz_r[9][$j]='0';
                                                                $matriz_r[10][$j]='0';
                                                              }
                                                        /*--------------------------------------------------------*/
                                                        /*--------------------ejecutado gestion ------------------*/
                                                              $nro_e=0;
                                                              foreach($ejecutado as $row)
                                                              {
                                                                  $nro_e++;
                                                                  $matriz_e [1][$nro_e]=$row['m_id'];
                                                                  $matriz_e [2][$nro_e]=$row['pejec_fis'];
                                                                  $matriz_e [3][$nro_e]=$row['pejec_fis_a'];
                                                                  $matriz_e [4][$nro_e]=$row['pejec_fis_b'];
                                                              }
                                                        /*--------------------------------------------------------*/
                                                            /*------- asignando en la matriz P, PA, %PA ----------*/
                                                              for($i = 1 ;$i<=$nro ;$i++)
                                                              {
                                                                for($j = 1 ;$j<=12 ;$j++)
                                                                {
                                                                  if($matriz[1][$i]==$matriz_r[1][$j])
                                                                  {
                                                                    $matriz_r[2][$j]=round($matriz[2][$i],2);
                                                                  }
                                                                }
                                                              }

                                                              $pa=0;        
                                                              for($j = 1 ;$j<=12 ;$j++){
                                                                $pa=$pa+$matriz_r[2][$j];
                                                                $matriz_r[3][$j]=$pa+$rowp['prod_linea_base'];
                                                                if($rowp['prod_meta']!=0)
                                                                	$matriz_r[4][$j]=round(((($pa+$rowp['prod_linea_base'])/$rowp['prod_meta'])*100),2);
                                                              }

                                                              if($rowp['indi_id']==1)
                                                              {
                                                                for($i = 1 ;$i<=$nro_e ;$i++){
                                                                    for($j = 1 ;$j<=12 ;$j++)
                                                                    {
                                                                        if($matriz_e[1][$i]==$matriz_r[1][$j])
                                                                        {
                                                                            $matriz_r[7][$j]=round($matriz_e[2][$i],2);
                                                                        }
                                                                    }
                                                                }
                                                              }
                                                              elseif ($rowp['indi_id']==2) 
                                                              {
                                                                if($rowp['prod_denominador']==0)
                                                                {
                                                                  for($i = 1 ;$i<=$nro_e ;$i++){
                                                                      for($j = 1 ;$j<=12 ;$j++)
                                                                      {
                                                                          if($matriz_e[1][$i]==$matriz_r[1][$j])
                                                                          {
                                                                              $matriz_r[5][$j]=round($matriz_e[3][$i],2);
                                                                              $matriz_r[6][$j]=round($matriz_e[4][$i],2);
                                                                              $matriz_r[7][$j]=round($matriz_e[2][$i],2);
                                                                          }
                                                                      }
                                                                  }
                                                                }
                                                                if ($rowp['prod_denominador']==1)
                                                                {
                                                                  for($i = 1 ;$i<=$nro_e ;$i++){
                                                                      for($j = 1 ;$j<=12 ;$j++)
                                                                      {
                                                                          if($matriz_e[1][$i]==$matriz_r[1][$j])
                                                                          {
                                                                              $matriz_r[5][$j]=round($matriz_e[3][$i],2);
                                                                              $matriz_r[6][$j]=round($matriz_e[4][$i],2);
                                                                              $matriz_r[7][$j]=round($matriz_e[2][$i],2);
                                                                          }
                                                                      }
                                                                  }
                                                                }
                                                              /*--------------------------------------------------------*/
                                                              }
                                                              /*--------------------matriz E,AE,%AE gestion ------------------*/
                                                              $pe=0; 
                                                              for($j = 1 ;$j<=12 ;$j++){
                                                                $pe=$pe+$matriz_r[7][$j];
                                                                $matriz_r[8][$j]=$pe+$rowp['prod_linea_base'];
                                                                if($rowp['prod_meta']!=0)
                                                                	$matriz_r[9][$j]=round(((($pe+$rowp['prod_linea_base'])/$rowp['prod_meta'])*100),2);
                                                                if($matriz_r[4][$j]!=0)
                                                                $matriz_r[10][$j]=round((($matriz_r[9][$j]/$matriz_r[4][$j])*100),2);
                                                                }
																echo '<td>';
																?>
																<table class="table-bordered" border="1">
																	<tr bgcolor="#3B3B3F">
																		<td><font color="#ffffff"><b>PROGRAMADO:</b></font></td>
																			<input type="hidden" name="pp[]" id="pp" value="<?php echo $matriz_r[2][$this->session->userdata('mes')];?>">
																		<td><input class="form-control" type="text" value="<?php echo $matriz_r[2][$this->session->userdata('mes')];?>" disabled="true"></td>
																	</tr>
																<?php 
																if($rowp['indi_id']==1)
																{
																?>	<input class="form-control" type="hidden" name="den[]" value="<?php echo $rowp['prod_denominador'];?>">
																	<input class="form-control" type="hidden" name="pa[]" value="<?php echo $matriz_r[5][$this->session->userdata('mes')];?>"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"  onpaste="return false">
																	<input class="form-control" type="hidden" name="pb[]" value="<?php echo $matriz_r[6][$this->session->userdata('mes')];?>"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"  onpaste="return false">
																	<tr bgcolor="#3B3B3F">
																		<td><font color="#ffffff"><b>EJECUTADO:</b></font></td>
																		<td><input class="form-control" type="text" name="pe[]" id="pe" value="<?php echo $matriz_r[7][$this->session->userdata('mes')];?>"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"  onpaste="return false"></td>
																	</tr>
																<?php
																}
																if($rowp['indi_id']==2)
																{
																?>	<input  type="hidden" name="den[]" value="<?php echo $rowp['prod_denominador'];?>">
																	<tr bgcolor="#3B3B3F">
																		<td><font color="#ffffff"><b>A-NUMERADOR</b></td>
																		<td><input class="form-control" type="text" name="pa[]" value="<?php echo $matriz_r[5][$this->session->userdata('mes')];?>"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"  onpaste="return false"></td>
																	</tr>
																	<tr bgcolor="#3B3B3F">
																		<td><font color="#ffffff"><b>B-DENOMINADOR</b></td>
																		<td><input class="form-control" type="text" name="pb[]" value="<?php echo $matriz_r[6][$this->session->userdata('mes')];?>"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"  onpaste="return false"></td>
																	</tr>
																	<input class="form-control" type="hidden" name="pe[]" id="pe" value="<?php echo $matriz_r[7][$this->session->userdata('mes')];?>"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"  onpaste="return false">
																	
																<?php	
																}
																echo '</table>';
																echo '</td>';
						                        echo '</tr>';
						                        /*=================================================== END PRODUCTOS =====================================================================*/
						                        $act=$this->model_actividad->list_act_anual($rowp['prod_id']);
						                        $nroa=1;
						                        foreach($act as $rowa)
											    {	$ti='';
				                                    if($rowa['indi_id']==2){ $ti='%';}
				                                ?>
										    	<input class="form-control" type="hidden" name="actividad[]" value="<?php echo $rowa['act_id'];?>">
										    	<input class="form-control" type="hidden" name="prod[]" value="<?php echo $rowp['prod_id'];?>">
										    	<input class="form-control" type="hidden" name="cost_u[]" value="<?php echo $rowa['act_costo_uni'];?>">
										    	<input class="form-control" type="hidden" name="india[]" value="<?php echo $rowa['indi_id'];?>">
										    	<?php
											    /*=========================================== TABLA DE PRODUCTOS ===========================================*/
											   	echo'<tr bgcolor="#b0f1ef">
								                        	<td>'.$nroa.'</td>
								                        	<td></td>
								                        	<td>'.$rowa['act_actividad'].'</td>
				                                            <td>'.number_format($rowa['act_costo_uni'], 2, ',', '.').'Bs.</td>
				                                            <td>'.$rowa['indi_abreviacion'].'</td>
				                                            <td>'.$rowa['act_indicador'].'</td>
				                                            <td>'.round($rowa['act_linea_base'],2).' '.$ti.'</td>
				                                            <td>'.round($rowa['act_meta'],2).' '.$ti.'</td>
				                                            <td>'.$rowa['act_ponderacion'].' %</td>
				                                            <td>'.$rowa['act_fuente_verificacion'].'</td>';
				                                            echo'<td>';
				                                                $programado=$this->model_actividad->act_prog_mensual($rowa['act_id'],$this->session->userdata("gestion")); /// programado
					                                            $ejecutado=$this->model_actividad->act_ejec_mensual($rowa['act_id'],$this->session->userdata("gestion")); /// ejecutado
					                                            $nro=0;
					                                            foreach($programado as $row)
					                                            {
					                                              $nro++;
					                                              $matriz [1][$nro]=$row['m_id'];
					                                              $matriz [2][$nro]=$row['pg_fis'];
					                                            }

					                                            /*---------------- llenando la matriz vacia --------------*/
					                                            for($j = 1; $j<=12; $j++)
					                                            {
					                                              $matriz_r[1][$j]=$j;
					                                              $matriz_r[2][$j]='0';
					                                              $matriz_r[3][$j]='0';
					                                              $matriz_r[4][$j]='0';
					                                              $matriz_r[5][$j]='0';
					                                              $matriz_r[6][$j]='0';
					                                              $matriz_r[7][$j]='0';
					                                              $matriz_r[8][$j]='0';
					                                              $matriz_r[9][$j]='0';
					                                              $matriz_r[10][$j]='0';
					                                            }
					                                            /*--------------------------------------------------------*/

					                                            /*--------------------ejecutado gestion ------------------*/
					                                            $nro_e=0;
					                                            foreach($ejecutado as $row)
					                                            {
					                                              $nro_e++;
					                                              $matriz_e [1][$nro_e]=$row['m_id'];
					                                              $matriz_e [2][$nro_e]=$row['ejec_fis'];
					                                              $matriz_e [3][$nro_e]=$row['ejec_fis_a'];
					                                              $matriz_e [4][$nro_e]=$row['ejec_fis_b'];
					                                            }
					                                            /*--------------------------------------------------------*/
					                                            /*------- asignando en la matriz P, PA, %PA ----------*/
					                                            for($i = 1 ;$i<=$nro ;$i++)
					                                            {
					                                              for($j = 1 ;$j<=12 ;$j++)
					                                              {
					                                                if($matriz[1][$i]==$matriz_r[1][$j])
					                                                {
					                                                    $matriz_r[2][$j]=round($matriz[2][$i],2);
					                                                }
					                                              }
					                                            }
					                                            $pa=0;
					                                            for($j = 1 ;$j<=12 ;$j++){
					                                              $pa=$pa+$matriz_r[2][$j];
					                                              $matriz_r[3][$j]=$pa+$rowa['act_linea_base'];
					                                              if($rowa['act_meta']!=0)
					                                              	$matriz_r[4][$j]=round(((($pa+$rowa['act_linea_base'])/$rowa['act_meta'])*100),2);
					                                            }

					                                            if($rowa['indi_id']==1)
					                                            {
					                                              for($i = 1 ;$i<=$nro_e ;$i++){
					                                                for($j = 1 ;$j<=12 ;$j++)
					                                                {
					                                                  if($matriz_e[1][$i]==$matriz_r[1][$j])
					                                                  {
					                                                      $matriz_r[7][$j]=round($matriz_e[2][$i],2);
					                                                  }
					                                                }
					                                              }
					                                            }
					                                            elseif ($rowa['indi_id']==2) 
					                                            {
					                                              for($i = 1 ;$i<=$nro_e ;$i++){
					                                                  for($j = 1 ;$j<=12 ;$j++)
					                                                  {
					                                                    if($matriz_e[1][$i]==$matriz_r[1][$j])
					                                                      {
					                                                          $matriz_r[5][$j]=round($matriz_e[3][$i],2);
					                                                          $matriz_r[6][$j]=round($matriz_e[4][$i],2);
					                                                          $matriz_r[7][$j]=round($matriz_e[2][$i],2);
					                                                      }
					                                                  }
					                                                }
					                                              /*--------------------------------------------------------*/
					                                              }
					                                              /*--------------------matriz E,AE,%AE gestion ------------------*/
					                                                $pe=0;
					                                                for($j = 1 ;$j<=12 ;$j++){
					                                                  $pe=$pe+$matriz_r[7][$j];
					                                                  $matriz_r[8][$j]=$pe+$rowa['act_linea_base'];
					                                                  if($rowa['act_meta']!=0)
					                                                  	$matriz_r[9][$j]=round(((($pe+$rowa['act_linea_base'])/$rowa['act_meta'])*100),2);
					                                                  if($matriz_r[4][$j]!=0)
					                                                    $matriz_r[10][$j]=round((($matriz_r[9][$j]/$matriz_r[4][$j])*100),2);
					                                                }
																        ?>
																		<table class="table-bordered" border="1">
																			<tr bgcolor="#568A89">
																				<td><font color="#ffffff"><b>PROGRAMADO:</b></font></td>
																				<input class="form-control" type="hidden" name="ap[]" value="<?php echo $matriz_r[2][$this->session->userdata('mes')];?>">
																				<td><input class="form-control" type="text"  value="<?php echo $matriz_r[2][$this->session->userdata('mes')];?>" disabled="true"></td>
																			</tr>
																		<?php 
																		if($rowa['indi_id']==2)
																		{
																		?>	<input  type="hidden" name="dena[]" value="<?php echo $rowa['act_denominador'];?>">
																			<tr bgcolor="#568A89">
																				<td><font color="#ffffff"><b>A-NUMERADOR</b></font></td>
																				<td><input class="form-control" type="text" name="aa[]" id="aa[]" value="<?php echo $matriz_r[5][$this->session->userdata('mes')];?>"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"  onpaste="return false"></td>
																			</tr>
																			<tr bgcolor="#568A89">
																				<td><font color="#ffffff"><b>B-DENOMINADOR</b></font></td>
																				<td><input class="form-control" type="text" name="ab[]" id="ab[]" value="<?php echo $matriz_r[6][$this->session->userdata('mes')];?>"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"  onpaste="return false"></td>
																			</tr>
																			<input class="form-control" type="hidden" name="ae[]" id="ae[]" value="<?php echo $matriz_r[7][$this->session->userdata('mes')];?>">
																		<?php
																		}
																		elseif($rowa['indi_id']==1)
																		{
																		?>	<input  type="hidden" name="dena[]" value="<?php echo $rowa['act_denominador'];?>">
																			<input class="form-control" type="hidden" name="aa[]" id="aa[]" value="<?php echo $matriz_r[5][$this->session->userdata('mes')];?>">
																			<input class="form-control" type="hidden" name="ab[]" id="ab[]" value="<?php echo $matriz_r[6][$this->session->userdata('mes')];?>">
																			<tr bgcolor="#568A89">
																				<td><font color="#ffffff"><b>EJECUTADO:</b></font></td>
																				<td><input class="form-control" type="text" name="ae[]" id="ae[]" value="<?php echo $matriz_r[7][$this->session->userdata('mes')];?>"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"  onpaste="return false" required></td>
																			</tr>
																		<?php
																		}
															echo '</table>';			
				                                            echo'</td>';
				                                        echo '</tr>';
				                                    $nroa++;
					                                }
										    $nrop++;
										    }
										    echo "</tbody>
					                            </table>"; 
									?>		
									</div>
								</div><!-- well -->
									<?php
								}
								?>
								<!-- end row -->
							</section>
			            </article>

			            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			            	<section id="widget-grid" class="">
			            	<div id="observaciones">
								<div class="well">
									<div class="row">
										<div class="col-sm-12">
											<div class="form-group">
												<h2><b>OBSERVACIONES</b></h2>
													<a href="#" data-toggle="modal" data-target="#modal_nuevo_ff" class="btn btn-lg btn-primary nuevo_ff" title="NUEVA OBSERVACIONES"><i class="glyphicon glyphicon-zoom-in"></i> Registrar observación</a>
													<br><br>
													<?php if(count($observaciones) > 0):?>
													<table class="table table-bordered"  border="1">
														<thead>
															<tr bgcolor="#e8e8e8">
																<th style="width:1%;"></th>
																<th style="width:15%;">FECHA INICIAL</th>
																<th style="width:15%;">FECHA FINAL</th>
																<th style="width:60%;">OBSERVACI&Oacute;N</th>
																<th style="width:5%;"></th>
															</tr>
														</thead>
														<tbody>
														<?php $nro=1;
						                                    foreach($observaciones  as $row)
						                                    {
						                                    	?>
						                                    	<tr>
						                                    	<td align="center"><?php echo $nro;?></td>
						                                    	<td>
						                                    		<div class="input-group">
																		<input type="text" class="form-control datepicker" value="<?php echo date('d/m/Y',strtotime($row['fecha_inicio'])); ?>" data-dateformat="dd/mm/yy" disabled="true">
																		<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
																	</div>
																</td>
						                                    	<td>
						                                    		<div class="input-group">
																		<input type="text" class="form-control datepicker" value="<?php echo date('d/m/Y',strtotime($row['fecha_final'])); ?>" data-dateformat="dd/mm/yy" disabled="true">
																		<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
																	</div>
						                                    	</td>
						                                    	<td>
						                                    	<textarea rows="3" class="form-control" disabled="true"><?php echo $row['observacion'];?></textarea>
						                                    	</td>
						                                    	<td align="center">
						                                    		<center>
						                                    		<a href="#" data-toggle="modal" data-target="#modal_mod_ff" class="btn btn-xs mod_ff" title="MODIFICAR DE REGISTRO OBSERVACION" name="<?php echo $row['fo_id']; ?>"><img src="<?php echo base_url(); ?>assets/ifinal/modificar.png" WIDTH="30" HEIGHT="30"/></a><br>
						                                    		<a href="#" data-toggle="modal" data-target="#modal_del_ff" class="btn btn-xs del_ff" title="ELIMINAR CONTRATO" name="<?php echo $row['fo_id']; ?>"><img src="<?php echo base_url(); ?>assets/ifinal/eliminar.png" WIDTH="30" HEIGHT="30"/></a>
						                                    		</center>
						                                    	</td>
						                                    	</tr>
						                                    	<?php
						                                    	$nro++;
						                                    }
						                                ?>
						                        		</tbody>
							                    </table>
												<?php else:?>
													<div class="well well-sm" style="background-color:#fff6a5; font-size:15px;">
														El proyecto o programa no tiene observaciones registradas hasta el momento.
													</div>
												<?php endif;?>
											</div>
										</div>

										<div class="row">
											<div class="col-md-12">
												<h2><b>PROBLEMAS</b></h2>
											</div>
											<div class="col-sm-12">
												<button type="button" id="botoncito" class="btn btn-lg btn-primary">
													<i class="glyphicon glyphicon-fire"></i>
													¿La ejecución presenta algun problema?
												</button>
											</div>
										</div>
<div class="cajita" style="display:block; clear:both; margin-top:15px !important;">
										<div class="col-sm-12">
											<p class="lead"><b>Escriba el(los) problema(s) y plantee la(s) alternativa(s) de solución</b></p>
											<div class="form-group">
												<label><b>PROBLEMAS</b></label>
												<textarea rows="3" class="form-control" name="prob" id="prob" style="width:100%;" maxlength="500"><?php echo $fase_ejec[0]['ejec_prob'] ?></textarea> 
											</div>
										</div>
<!--										<div class="col-sm-12">
											<div class="form-group">
												<label><font size="1" ><b>EFECTOS</b></font></label>
												<textarea rows="3" class="form-control" name="efectos" id="efectos" style="width:100%;" maxlength="500"><?php echo $fase_ejec[0]['ejec_efect'] ?></textarea> 
											</div>
										</div>
										<div class="col-sm-12">
											<div class="form-group">
												<label><font size="1" ><b>CAUSAS</b></font></label>
												<textarea rows="3" class="form-control" name="causas" id="causas" style="width:100%;" maxlength="500"><?php echo $fase_ejec[0]['ejec_causas'] ?></textarea> 
											</div>
										</div>-->
										<input type="hidden" name="efectos" id="efectos" value="">
										<input type="hidden" name="causas" id="causas" value="">

										<div class="col-sm-12">
											<div class="form-group">
												<label><b>SOLUCIONES</b></label>
												<textarea rows="3" class="form-control" name="sol" id="sol" style="width:100%;" maxlength="500"><?php echo $fase_ejec[0]['ejec_sol'] ?></textarea> 
											</div>
										</div>
</div>
																
									</div>
									<input class="form-control" type="hidden"  name="bton_reg" id="bton_reg" value="0">
									<div class="form-actions">
										<a href="<?php echo base_url().'index.php/admin/'.$atras;?>" class="btn btn-danger" title="VOLVER A MIS EJECUCIONES"> CANCELAR </a>
										<input type="button" value="GUARDAR BORRADOR" name="boton" class="btn btn-primary" onclick="valida_envia()" title="GUARDAR EN BORRADOR">
										<input type="button" value="ENVIAR SGP" name="boton1" class="btn btn-success" id="valida_envia1" title="ENVIAR SGP">
									</div>
								</div>
								

								</form>
								</div>
								<div id="cerrado" style="display:none;">
									<div class="well">
									<?php
									    $attributes = array('class' => 'form-horizontal', 'id' => 'formulario2','name' =>'formulario2','enctype' => 'multipart/form-data');
									    echo validation_errors();
									    echo form_open('admin/ejec/update_proy', $attributes);
									?> 
									<form id="formulario2" name="formulario2" novalidate="novalidate" method="post" >	
									<input type="hidden" name="proy_id" id="proy_id" value="<?php echo $proyecto[0]['proy_id'] ?>">
									<input type="hidden" name="id_f" id="id_f" value="<?php echo $id_f ?>"><!-- id fase -->
									<input type="hidden" name="tp" id="tp" value="<?php echo $tipo ?>"><!-- tipo de proyecto -->
									<input type="hidden" name="mes_id" id="mes_id" value="<?php echo $mes_id ?>"><!-- Mes Activo -->
									<input type="hidden" name="id_c" id="id_c" value="<?php echo $componente[0]['com_id'] ?>"><!-- id componente -->
									<input type="hidden" name="prog" id="prog" value="<?php echo $proyecto[0]['aper_programa'] ?>"><!-- prog -->
									<input type="hidden" name="accion" id="accion" value="1"><!-- tipo de accion -->
									<div class="row">
										<div class="col-sm-12">
											<div class="form-group">
												<label><font size="1"><b>CONCLUSIONES</b></font></label>
												<textarea rows="5" class="form-control" name="conclusiones" id="conclusiones" style="width:100%;" maxlength="500"></textarea> 
											</div>
										</div>
										<div class="col-sm-12">
											<div class="form-group">
												<label><font size="1" ><b>RECOMENDACIONES</b></font></label>
												<textarea rows="5" class="form-control" name="recomendaciones" id="recomendaciones" style="width:100%;" maxlength="500"></textarea> 
											</div>
										</div>
										<div class="col-sm-12">
											<div class="form-group">
												<label><font size="1" ><b>LECCIONES APRENDIDAS</b></font></label>
												<textarea rows="5" class="form-control" name="lecciones" id="lecciones" style="width:100%;" maxlength="500"></textarea> 
											</div>
										</div>
										<div class="form-actions">
											<a href="<?php echo base_url().'index.php/admin/'.$atras;?>" class="btn btn-success" title="VOLVER ATRAS">ATRAS</a>
											<a href="<?php echo base_url().'index.php/admin/ejec/prog/'.$tipo.'' ?>" class="btn  btn-success" title="VOLVER A MIS PROYECTOS DE INVERSION"> CANCELAR </a>
											<input type="button" value="CERRAR" id="btsubmit" class="btn btn-primary" onclick="validar_cerrado()" title="VALIDAR Y CERRAR EL PROYECTO">					
										</div>
										
									</div>
									</form>
									
									</div>
								</div>
			            	</section>
			            </article>

				</div> <!-- row -->
				</section>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN PANEL -->
		    <!-- ================== Modal NUEVO METAS  ========================== -->
	    <div class="modal animated fadeInDown" id="modal_nuevo_ff" tabindex="-1" role="dialog">
	        <div class="modal-dialog">
	            <div class="modal-content">
	                <div class="modal-header">
	                    <button type="button" class="close text-danger " data-dismiss="modal" aria-hidden="true">
	                        &times;
	                    </button>
	                    <h4 class="modal-title">
	                              <img src="<?php echo base_url(); ?>assets/img/logo.png" width="150" alt="SmartAdmin">
	                    </h4>
	                    <h4 class="modal-title text-center text-info">
	                        <b><i class="glyphicon glyphicon-pencil"></i> NUEVO REGISTRO (Observaci&oacute;n)</b>
	                    </h4>
	                </div>
	                <div class="modal-body no-padding">
	                    <div class="row">
	                        <form id="form_ff" novalidate="novalidate" method="post">
	                        <input type="hidden" name="id_pr" id="id_pr" value="<?php echo $proyecto[0]['proy_id'];?>">
	                        <input type="hidden" name="id_m" id="id_m" value="<?php echo $mes_id;?>">
	                        <input type="hidden" name="id_f" id="id_f" value="<?php echo $id_f;?>">
	                        <input type="hidden" name="id_c" id="id_c" value="<?php echo $componente[0]['com_id'];?>">
	                            <div id="bootstrap-wizard-1" class="col-sm-12">
	                                <div class="well">
	                                	<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label><font size="1"><b>FECHA INICIAL</b></font></label>
													<div class="input-group">
														<input class="form-control" type="date" name="f1" id="f1" title="Fecha Inicial">
														<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
													</div>
												</div>
											</div>

											<div class="col-sm-6">
												<div class="form-group">
													<label><font size="1"><b>FECHA FINAL</b></font></label>
													<div class="input-group">
														<input class="form-control" type="date" name="f2" id="f2" title="Fecha Final">
														<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
													</div>
												</div>
											</div>
										</div>
	                                    <div class="row">
	                                        <div class="col-sm-12">
	                                            <div class="form-group">
	                                                <label><font size="1"><b>DESCRIPCI&Oacute;N DE LA OBSERVACI&Oacute;N</b></font></label>
	                                                <textarea rows="5" class="form-control" name="obs" id="obs" style="width:100%;" title="Observacion" maxlength="200"></textarea> 
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div> <!-- end well -->
	                            </div>
	                        </form>
	                    </div>
	                </div>
	                <div class="modal-footer">
	                    <div class="row">
	                        <div class="col-md-3 pull-left">
	                            <button class="btn btn-danger" data-dismiss="modal">CANCELAR</button>
	                        </div>
	                        <div class="col-md-3 pull-right ">
	                            <button type="submit" name="enviar_ff" id="enviar_ff" class="btn  btn-primary"><i class="fa fa-save"></i>
	                                GUARDAR
	                            </button>
	                        </div>
	                    </div>
	                </div>
	            </div><!-- /.modal-content -->
	        </div><!-- /.modal-dialog -->
	    </div>
	    <!-- /.modal -->
		<div  class="modal animated fadeInDown" id="modal_mod_ff" tabindex="-1" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
	                    <button type="button" class="close text-danger " data-dismiss="modal" aria-hidden="true">
	                        &times;
	                    </button>
	                    <h4 class="modal-title">
	                        <img src="<?php echo base_url(); ?>assets/img/logo.png" width="150" alt="SmartAdmin">
	                    </h4>
	                    <h4 class="modal-title text-center text-info">
	                        <b><i class="glyphicon glyphicon-pencil"></i> MODIFICAR REGISTRO (Observaci&oacute;n)</b>
	                    </h4>
	                </div>
					<div class="modal-body no-padding">
						<div class="row">
							<form id="mod_formff" novalidate="novalidate" method="post">
							<input type="hidden" name="id_f" id="id_f" value="<?php echo $id_f;?>">
	                        <input type="hidden" name="fo_id" id="fo_id" >
								<div id="bootstrap-wizard-1" class="col-sm-12">
									<div class="well">
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label><b>FECHA INICIAL</b></label>
													<input type="text" name="fi" id="fi" placeholder="Ingrese Fecha" class="form-control datepicker" data-dateformat="yy-mm-dd">
													<span class="input-group-btn"><i class="fa fa-calendar"></i></span>
												</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group">
													<label><b>FECHA FINAL</b></label>
													<input type="text" name="ff" id="ff" placeholder="Ingrese Fecha" class="form-control datepicker" data-dateformat="yy-mm-dd">
													<span class="input-group-btn"><i class="fa fa-calendar"></i></span>
												</div>
											</div>
										</div>
										<div class="row">
	                                        <div class="col-sm-12">
	                                            <div class="form-group">
	                                                <label><font size="1"><b>DESCRIPCI&Oacute;N DE LA OBSERVACI&Oacute;N</b></font></label>
	                                                <textarea rows="5" class="form-control" name="obser" id="obser" style="width:100%;" title="Observacion" maxlength="200"></textarea> 
	                                            </div>
	                                        </div>
	                                    </div>
									</div> <!-- end well -->
								</div>
							</form>
						</div>
					</div>
					<div class="modal-footer">
	                    <div class="row">
	                        <div class="col-md-3 pull-left">
	                            <button class="btn btn-danger" data-dismiss="modal">CANCELAR</button>
	                        </div>
	                        <div class="col-md-3 pull-right ">
	                            <button type="submit" name="mod_ffenviar" id="mod_ffenviar" class="btn btn-primary">
	                                <i class="fa fa-save"></i>
	                                ACEPTAR
	                            </button>
	                        </div>
	                    </div>
	                </div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
		<!-- PAGE FOOTER -->
		<div class="page-footer"> 
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->
		


		

		<!--================================================== -->
	<script type="text/javascript">
		
	function valida_envia()
    { 
        if (document.formulario.est.value=="") /////// Programa
        { 
            alertify.alert("Seleccione Estado del proyecto") 
            document.formulario.est.focus() 
            return 0; 
        }

        if (document.formulario.dep.value=="") /////// Dependencia de Estado
        { 
            alertify.alert("Seleccione Estado") 
            document.formulario.dep.focus() 
            return 0; 
        }

        if (document.formulario.fpi.value=="") /////// Fecha de Periodo Inicial
        { 
            alertify.alert("SELECCIONE FECHA INICIO DE PERIODO DEL INFORME") 
            document.formulario.fpi.focus() 
            return 0; 
        }

        if (document.formulario.fpf.value=="") /////// Fecha de Periodo Inicial
        { 
            alertify.alert("SELECCIONE FECHA FINAL DE PERIODO DEL INFORME") 
            document.formulario.fpf.focus() 
            return 0; 
        }

        document.getElementById('bton_reg').value='0';
        alertify.confirm("GUARDAR EJECUCI\u00D3N DE LA OPERACION?", function (a) {
                    if (a) {
                        //============= GUARDAR DESPUES DE LA VALIDACION ===============
                        document.formulario.submit();
                        document.getElementById("btsubmit").value = "GUARDANDO EJECUCION...";
						document.getElementById("btsubmit").disabled = true;
						return true; 
                    } else {
                        alertify.error("OPCI\u00D3N CANCELADA");
                    }
                });

    }
	</script>
	<script type="text/javascript">
/*	function valida_envia1()
    { 
    }*/

        function validar_cerrado()
	    { 
       	 	if (document.formulario.fpi.value=="") /////// Fecha de Periodo Inicial
	        { 
	            alertify.alert("SELECCIONE FECHA INICIO DE PERIODO DEL INFORME") 
	            document.formulario.fpi.focus() 
	            return 0; 
	        }

	        if (document.formulario.fpf.value=="") /////// Fecha de Periodo Inicial
	        { 
	            alertify.alert("SELECCIONE FECHA FINAL DE PERIODO DEL INFORME") 
	            document.formulario.fpf.focus() 
	            return 0; 
	        }
	        
       	 	if (document.formulario2.conclusiones.value=="") /////// CONCLUSIONES
            { 
                alertify.alert("REGISTRE CONCLUSIONES") 
                document.formulario2.conclusiones.focus() 
                return 0; 
            }
            if (document.formulario2.recomendaciones.value=="") /////// RECOMENDACIONES
            { 
                alertify.alert("REGISTRE RECOMENDACIONES") 
                document.formulario2.recomendaciones.focus() 
                return 0; 
            }
            if (document.formulario2.lecciones.value=="") /////// LECCIONES
            { 
                alertify.alert("REGISTRE LECCIONES APRENDIDAS") 
                document.formulario2.lecciones.focus() 
                return 0; 
            }

            alertify.confirm("ESTA SEGURO DE CERRAR LA OPERACI\u00D3N ?", function (a) {
                    if (a) {
                        //============= GUARDAR DESPUES DE LA VALIDACION ===============
                        document.formulario2.submit();
                        document.getElementById("btsubmit").value = "CERRANDO OPERACION...";
						document.getElementById("btsubmit").disabled = true;
						return true; 
                    } else {
                        alertify.error("OPCI\u00D3N CANCELADA");
                    }
                });

        }
	</script>

		<script>
			if (!window.jQuery) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-2.0.2.min.js"><\/script>');
			}
		</script>

		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>

		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url();?>/assets/js/app.config.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/mis_js/validacion_form.js"></script>
		<SCRIPT src="<?php echo base_url(); ?>mis_js/programacion/ejecucion/abm_ejecucion.js" type="text/javascript"></SCRIPT>
		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url();?>/assets/js/bootstrap/bootstrap.min.js"></script>
		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url();?>/assets/js/notification/SmartNotification.min.js"></script>
		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url();?>/assets/js/smartwidgets/jarvis.widget.min.js"></script>
		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url();?>/assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<!-- SPARKLINES -->
		<script src="<?php echo base_url();?>/assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/select2/select2.min.js"></script>
		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
		<!-- browser msie issue fix -->
		<script src="<?php echo base_url();?>/assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url();?>/assets/js/plugin/fastclick/fastclick.min.js"></script>
		<!-- Demo purpose only -->
		<script src="<?php echo base_url();?>/assets/js/demo.min.js"></script>
		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url();?>/assets/js/app.min.js"></script>
		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<!-- Voice command : plugin -->
		<script src="<?php echo base_url();?>/assets/js/speech/voicecommand.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
		<!-- PAGE RELATED PLUGIN(S) -->
		<script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
		<script src="<?php echo base_url();?>/assets/js/plugin/fuelux/wizard/wizard.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
	<!--================= NUEVO FUENTE FINANCIAMIENTO =========================================-->

		<script type="text/javascript">
		    $(function () {

	$('#valida_envia1').click(function(e){
		if (document.formulario.est.value=="") /////// Programa
        { 
            alertify.alert("Seleccione Estado del proyecto") 
            document.formulario.est.focus() 
            return 0; 
        }
		if (document.formulario.dep.value=="") /////// Dependencia de Estado
        { 
            alertify.alert("Seleccione Estado") 
            document.formulario.dep.focus() 
            return 0; 
        }

        if (document.formulario.fpi.value=="") /////// Fecha de Periodo Inicial
        { 
            alertify.alert("SELECCIONE FECHA INICIO DE PERIODO DEL INFORME") 
            document.formulario.fpi.focus() 
            return 0; 
        }

        if (document.formulario.fpf.value=="") /////// Fecha de Periodo Inicial
        { 
            alertify.alert("SELECCIONE FECHA FINAL DE PERIODO DEL INFORME") 
            document.formulario.fpf.focus() 
            return 0; 
        }

        document.getElementById('bton_reg').value='1'; 
	        alertify.confirm("GUARDAR Y CERRAR EJECUCI\u00D3N DE LA OPERACION?", function (a) {
			if (a) {
                //============= GUARDAR DESPUES DE LA VALIDACION ===============
				
                document.formulario.submit();//ENVIA LOS DATOS DEL FORMULARIO
                document.getElementById("btsubmit").value = "GUARDANDO EJECUCION...";
				document.getElementById("btsubmit").disabled = true;
				return true; 
            } else {
                alertify.error("OPCI\u00D3N CANCELADA");
            }
        });
	});

			


			
				var problema = $('#prob').val();
				if(problema.length == 0){
					$('.cajita').hide();
				}
				$('#botoncito').click(function(e){
					e.stopPropagation();
					e.preventDefault();
					$('.cajita').slideToggle();
				});
				
				
		        var id_p = '';
		        $('#modal_nuevo_ff').on('hidden.bs.modal', function () {
		            document.forms['form_ff'].reset();

		        });
		        $("#enviar_ff").on("click", function (e) {

		            //========================VALIDANDO FORMULARIO===================
		            var $validator = $("#form_ff").validate({
		                //////////////// DATOS GENERALES
		                rules: {
		                    f1: { //// indicador
		                        required: true,
		                    },
		                    f2: { //// indicador
		                        required: true,
		                    },
		                    obs: { //// meta
		                        required: true,
		                    }
		                },
		                messages: {
		                    f1: "Seleccione Fecha Inicial",
		                    f2: "Seleccione Fecha Final",
		                    obs: "Describa Observacion",
		                    
		                },
		                highlight: function (element) {
		                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		                },
		                unhighlight: function (element) {
		                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		                },
		                errorElement: 'span',
		                errorClass: 'help-block',
		                errorPlacement: function (error, element) {
		                    if (element.parent('.input-group').length) {
		                        error.insertAfter(element.parent());
		                    } else {
		                        error.insertAfter(element);
		                    }
		                }
		            });
		            var $valid = $("#form_ff").valid();
		            if (!$valid) {
		                $validator.focusInvalid();
		                //return false;
		            } else {
		                //==========================================================
		                var id_pr = document.getElementById("id_pr").value;
		                var id_f = document.getElementById("id_f").value;
		                var id_m = document.getElementById("id_m").value;
		                var id_c = document.getElementById("id_c").value;
		                var f1 = document.getElementById("f1").value;
		                var f2 = document.getElementById("f2").value;
		                var obs = document.getElementById("obs").value;

		              //  alert('proy_id : '+id_pr+' - fase_id: '+id_f+' - mes_id: '+id_m+' -com_id: '+id_c+' - fecha 1: '+f1+' -fecha 2: '+f2+' - Observacion : '+obs)
		                //=================== VERIFICAR SI EXISTE EL COD DE PROGRAMA ==============
		                
		                var url = "<?php echo site_url("admin")?>/ejec/valida_obs";
		                            $.ajax({
		                                type: "post",
		                                url: url,
		                                data: {
		                                    id_pr: id_pr,
		                                    id_f: id_f,
		                                    id_m: id_m,
		                                    id_c: id_c,
		                                    f1: f1,
		                                    f2: f2,
		                                    obs: obs
		                                },
		                                success: function (data) {
		                                    if (data.trim() == 'true') {
		                                        window.location.reload(true);
		                                    } else {
		                                        alert(data);
		                                    }
		                                }
		                            });
		            }
		        });
		    });
		</script>
		<!--============================== MODIFICAR OBSERVACION =========================================-->
		<script type="text/javascript">
		    $(function () {
		        var id_fo = '';
		        $(".mod_ff").on("click", function (e) {
		            //==========================LLENAR MIS DATOS DE FORMULARIO CON LA CLAVE RECIBIDA
		            id_fo = $(this).attr('name'); 
		            var url = "<?php echo site_url("admin")?>/ejec/get_obs";
		            var codigo = '';
		            var request;
		            if (request) {
		                request.abort();
		            }
		            request = $.ajax({
		                url: url,
		                type: "POST",
		                dataType: 'json',
		                data: "id_fo=" + id_fo
		            });

		            request.done(function (response, textStatus, jqXHR) {

		                document.getElementById("fo_id").value = response.fo_id;
		                document.getElementById("fi").value = response.fecha_inicio;
		                document.getElementById("ff").value = response.fecha_final;
		                document.getElementById("obser").value = response.observacion;

		            });
		            request.fail(function (jqXHR, textStatus, thrown) {
		                console.log("ERROR: " + textStatus);
		            });
		            request.always(function () {
		                //console.log("termino la ejecuicion de ajax");
		            });
		            e.preventDefault();
		            // =============================VALIDAR EL FORMULARIO DE MODIFICACION
		            $("#mod_ffenviar").on("click", function (e) {
		                var $validator = $("#mod_formff").validate({
		                   rules: {
		                    fi: { //// f inicio
		                        required: true,
		                    },
		                    ff: { //// f final
		                        required: true,
		                    },
		                    obser: { //// meta
		                        required: true,
		                    }
		                },
		                messages: {
		                    f1: "Seleccione Fecha Inicial",
		                    f2: "Seleccione Fecha Final",
		                    obser: "Describa Observacion",
		                    
		                },
		                    highlight: function (element) {
		                        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		                    },
		                    unhighlight: function (element) {
		                        $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		                    },
		                    errorElement: 'span',
		                    errorClass: 'help-block',
		                    errorPlacement: function (error, element) {
		                        if (element.parent('.input-group').length) {
		                            error.insertAfter(element.parent());
		                        } else {
		                            error.insertAfter(element);
		                        }
		                    }
		                });
		                var $valid = $("#mod_formff").valid();
		                if (!$valid) {
		                    $validator.focusInvalid();
		                } else {
		                    //==========================================================
		                    var fo_id = document.getElementById("fo_id").value;
		                    var fi = document.getElementById("fi").value;
		                    var ff = document.getElementById("ff").value;
		                    var obser = document.getElementById("obser").value;

		                    var url = "<?php echo site_url("admin")?>/ejec/update_obs";
		                    $.ajax({
		                        type: "post",
		                        url: url,
		                        data: {
		                            fo_id: fo_id,
		                            fi: fi,
		                            ff: ff,
		                            obser: obser
		                        },
		                        success: function (data) {
		                            window.location.reload(true);
		                        }
		                    });
		                }
		            });
		        });
		    });
		</script>
		<script type="text/javascript">
		    $(function () {
		        function reset() {
		            $("#toggleCSS").attr("href", "<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css");
		            alertify.set({
		                labels: {
		                    ok: "ACEPTAR",
		                    cancel: "CANCELAR"
		                },
		                delay: 5000,
		                buttonReverse: false,
		                buttonFocus: "ok"
		            });
		        }

		        // =====================================================================
		        $(".del_ff").on("click", function (e) {
		            reset();
		            var name = $(this).attr('name');
		            var request;
		            // confirm dialog
		            alertify.confirm("REALMENTE DESEA ELIMINAR ESTE REGISTRO?", function (a) {
		                if (a) { 
		                    url = "<?php echo site_url("admin")?>/ejec/delete_obs";
		                    if (request) {
		                        request.abort();
		                    }
		                    request = $.ajax({
		                        url: url,
		                        type: "POST",
		                        data: "fo_id=" + name

		                    });
		                    window.location.reload(true);
		                    request.done(function (response, textStatus, jqXHR) {
		                        $('#tr' + response).html("");
		                    });
		                    request.fail(function (jqXHR, textStatus, thrown) {
		                        console.log("ERROR: " + textStatus);
		                    });
		                    request.always(function () {
		                        //console.log("termino la ejecuicion de ajax");
		                    });

		                    e.preventDefault();
		                    alertify.success("Se eliminó el registro correctamente");

		                } else {
		                    // user clicked "cancel"
		                    alertify.error("Opcion cancelada");
		                }
		            });
		            return false;
		        });
		    });

		</script>

		<script type="text/javascript">
		$(document).ready(function() {
			
			pageSetUp();

			$("#est").change(function () {
                $("#est option:selected").each(function () {
                elegido=$(this).val();
                $.post("<?php echo base_url(); ?>index.php/admin/combo_estado", { elegido: elegido }, function(data){ 
                $("#dep").html(data);
                });     
            });
            });    
		})
		</script>

	</body>
</html>
