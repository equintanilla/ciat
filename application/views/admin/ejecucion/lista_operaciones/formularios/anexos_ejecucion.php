<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
		<title><?php echo $this->session->userdata('name')?></title>
		<meta name="description" content="">
		<meta name="author" content="">
			
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
        <link href="<?php echo base_url(); ?>assets/file/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
        <script src="<?php echo base_url(); ?>assets/file/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/file/js/fileinput.min.js" type="text/javascript"></script>	
        <script>
		  	function abreVentana(PDF)
			{
				var direccion;
				direccion = '' + PDF;
				window.open(direccion, "Reporte de Proyectos" , "width=800,height=650,scrollbars=SI") ;
			}                                                  
        </script>
	</head>
	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->
		<!-- HEADER -->
		<header id="header">
			<!-- pulled right: nav area -->
			<div class="pull-right">
				<!-- collapse menu button -->
				<div id="hide-menu" class="btn-header pull-right">
					<span> <a href="javascript:void(0);" data-action="toggleMenu" title="Menu"><i class="fa fa-reorder"></i></a> </span>
				</div>
				<!-- end collapse menu -->
				<!-- logout button -->
				<div id="logout" class="btn-header transparent pull-right">
					<span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Salir" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
				</div>
				<!-- end logout button -->
				<!-- search mobile button (this is hidden till mobile view port) -->
				<div id="search-mobile" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
				</div>
				<!-- end search mobile button -->
				<!-- fullscreen button -->
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Pantalla Completa"><i class="fa fa-arrows-alt"></i></a> </span>
				</div>
				<!-- end fullscreen button -->
			</div>
			<!-- end pulled right: nav area -->
		</header>
		<!-- END HEADER -->

		<!-- Left panel : Navigation area -->
		<aside id="left-panel">
			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                        <span>
                            <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                        </span>
						<i class="fa fa-angle-down"></i>
					</a> 
				</span>
			</div>
			<nav>
				<ul>
					<li class="">
	                <a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MENÚ PRINCIPAL">
	                <i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
	            	</li>
		            <li class="text-center">
		                <a href='<?php echo site_url("admin").'/ejec/prog/'.$tipo.''; ?>' title="REGISTRO DE EJECUCION"> <span class="menu-item-parent">REGISTRO DE EJECUCI&Oacute;N</span></a>
		            </li>
				<?php
		                for($i=0;$i<count($enlaces);$i++)
		                {
		                    if(count($subenlaces[$enlaces[$i]['o_child']])>0)
		                    {
		            ?>
		            <li>
		              	<a href="#" >
		              		<i class="<?php echo $enlaces[$i]['o_image']?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
		              	<ul >
		              	<?php
		                foreach ($subenlaces[$enlaces[$i]['o_child']] as $item) {
		                ?>
		                <li><a href="<?php echo base_url($item['o_url']); ?>"><?php echo $item['o_titulo']; ?></a></li>
		                <?php } ?>
		                </ul>
		            </li>
		            <?php 
		            }
		        } ?>
				</ul>
			</nav>
			<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>
		</aside>

		<!-- MAIN PANEL -->
		<div id="main" role="main">
			<!-- RIBBON -->
			<div id="ribbon">
				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>
				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li>Registro de Ejecuci&oacute;n</li><li><a href="#" title="MIS OPERACIONES">Registro de Ejecuci&oacute;n POA</a></li><li>Programa Recurrente</li><li>Ejecutaci&oacute;n F&iacute;sica Mes <?php echo $mes ?></li><li>Anexos de la Ejecuci&oacute;n</li>
				</ol>
			</div>
			<!-- END RIBBON -->
			<!-- MAIN CONTENT -->
			<div id="content">
				
					<?php
						$attributes = array('class' => 'form-horizontal', 'id' => 'wizard-1','name' =>'wizard-1','enctype' => 'multipart/form-data');
						echo validation_errors();
						echo form_open('admin/ejec/valida_arch', $attributes);
					?>

				<!-- widget grid -->
				<section id="widget-grid" class="">
					<form id="wizard-1" novalidate="novalidate" method="post" enctype="multipart/form-data" >
					<!-- row -->
					<div class="row">
						<article class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
		                  <section id="widget-grid" class="well">
	                        <div class="">
	                            <h1> ANEXOS DE EJECUCI&Oacute;N</h1>
	                            <h1> CATEGORIA PROGRAM&Aacute;TICA : <small><?php echo $proyecto[0]['aper_programa'].''.$proyecto[0]['aper_proyecto'].''.$proyecto[0]['aper_actividad']?></small>
								<h1> <?php echo $titulo_proy;?> : <small><?php echo $proyecto[0]['proy_nombre']?></small></h1>
      							<h1> COMPONENTE : <small><?php echo $componente[0]['com_componente']?></small></h1>
      							<h1> GESTI&Oacute;N ACTUAL : <small><?php echo $this->session->userdata("gestion")?></small>&nbsp;&nbsp;MES : <small><?php echo $mes?></small></h1>
	                        </div>
		                  </section>
						</article>
						<article class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
			                <section id="widget-grid" class="well">
			                    <center>
			                        <a href="<?php echo base_url().'index.php/admin/ejec/meses_operacion/'.$tipo.'/'.$proyecto[0]['proy_id'].'/'.$fase[0]['id'].'/'.$componente[0]['com_id'].'/'.$this->session->userdata("mes").'' ?>" class="btn btn-success" title="Volver atras" style="width:100%;">VOLVER ATRAS</a><br>
			                    </center>
			                </section>
			            </article>

						<article class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
							
							<input type="hidden" name="id_p" id="id_p" value="<?php echo $proyecto[0]['proy_id'] ?>"><!-- id proyecto -->
							<input type="hidden" name="id_f" id="id_f" value="<?php echo $fase[0]['id'] ?>"><!-- id proyecto -->
							<input type="hidden" name="id_c" id="id_c" value="<?php echo $componente[0]['com_id'] ?>"><!-- id proyecto -->
							<input type="hidden" name="prog" id="prog" value="<?php echo $proyecto[0]['aper_programa'] ?>"><!-- prog -->
							<input type="hidden" name="id_mes" id="id_mes" value="<?php echo $id_mes ?>"><!-- id mes -->
							<input type="hidden" name="tp" id="tp" value="<?php echo $tipo ?>"><!-- tp -->
							<input class="form-control"  name="tp_doc" type="hidden" id="tp_doc" >
								<div class="jarviswidget jarviswidget-color-darken" >
									<header>
										<span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
										<h2 class="font-md"><strong>ANEXOS DE LA EJECUCI&Oacute;N</strong></h2>				
									</header>
								<div class="panel-body">
									<div class="col-sm-12">
										<label class="control-label">Seleccione Documento/Archivo</label>
										<input id="file1" name="file1" type="file" class="file" multiple data-show-upload="false" data-show-caption="true" title="SELECCIONE EL ARCHIVO, DOCUMENTO">
									</div>	
									<div class="col-sm-12">
										<label class="control-label">Descripci&oacute;n del Documento/Archivo</label>
										<textarea rows="3" class="form-control" name="doc" id="doc" style="width:100%;" title="Descripcion del documento" ></textarea> 
									</div>
									<div class="col-sm-12"><hr></div>
									<div class="col-sm-12">
										<input type="button" name="Submit" value="SUBIR ARCHIVO" id="btsubmit" class="btn btn-success btn-lg" onclick="comprueba_extension(this.form, this.form.file1.value,this.form.doc.value)" style="width:100%;">
									</div>
									<div class="col-sm-12"><hr></div>	
								</div>
						        </div>				
							</form>
						</article>

							<article class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
								<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false">
								<header>
									<span class="widget-icon"> <i class="fa fa-table"></i> </span>
									<h2>ANEXOS DE LA EJECUCI&Oacute;N MES DE : <?php echo $mes;?></h2>
								</header>
								<!-- widget div-->
								<div>
									<!-- widget edit box -->
									<div class="jarviswidget-editbox">
										<!-- This area used as dropdown edit box -->
									</div>
									<!-- end widget edit box -->
									<!-- widget content -->
									<div class="widget-body">
										<div class="table-responsive">
											<table class="table table-bordered">
												<thead>
													<tr>
														<th>Nro.</th>
														<th>ANEXOS DE LA EJECUCI&Oacute;N</th>
														<th>VER ANEXO</th>
														<th>ELIMINAR</th>
													</tr>
												</thead>
												<tbody>
													<?php echo $anexos; ?>
												</tbody>
											</table>
											
										</div>
									</div>
								</div>
								<!-- end widget div -->
							</div>
						</article>
					</div>
				
					<!-- end row -->
				</section>
				<!-- end widget grid -->					
			</div>
			<!-- END MAIN CONTENT -->

		</div>
		<!-- END MAIN PANEL -->

		<!-- PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->

		<!--================================================== -->
		<script>
			if (!window.jQuery) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-2.0.2.min.js"><\/script>');
			}
		</script>

		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>

		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url();?>/assets/js/app.config.js"></script>

		<script src="<?php echo base_url(); ?>mis_js/registro_ejecucion/mis_operaciones.js"></script>
		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url();?>/assets/js/bootstrap/bootstrap.min.js"></script>
		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url();?>/assets/js/notification/SmartNotification.min.js"></script>
		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url();?>/assets/js/smartwidgets/jarvis.widget.min.js"></script>
		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url();?>/assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<!-- SPARKLINES -->
		<script src="<?php echo base_url();?>/assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/select2/select2.min.js"></script>
		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
		<!-- browser msie issue fix -->
		<script src="<?php echo base_url();?>/assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url();?>/assets/js/plugin/fastclick/fastclick.min.js"></script>
		<!-- Demo purpose only -->
		<script src="<?php echo base_url();?>/assets/js/demo.min.js"></script>
		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url();?>/assets/js/app.min.js"></script>
		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<!-- Voice command : plugin -->
		<script src="<?php echo base_url();?>/assets/js/speech/voicecommand.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
	</body>
</html>
