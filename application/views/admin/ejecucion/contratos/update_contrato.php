<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
		<title><?php echo $this->session->userdata('name')?></title>
		<meta name="description" content="">
		<meta name="author" content="">
			
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.core.css"/>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css" id="toggleCSS"/>
	</head>
	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->

		<!-- HEADER -->
		<header id="header">
			<div id="logo-group">
				<!-- <span id="logo"> <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="SmartAdmin"> </span> -->
			</div>

			<!-- pulled right: nav area -->
			<div class="pull-right">
				
				<!-- collapse menu button -->
				<div id="hide-menu" class="btn-header pull-right">
					<span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
				</div>
				<!-- end collapse menu -->
				<!-- logout button -->
				<div id="logout" class="btn-header transparent pull-right">
					<span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Sign Out" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
				</div>
				<!-- end logout button -->
				<!-- search mobile button (this is hidden till mobile view port) -->
				<div id="search-mobile" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
				</div>
				<!-- end search mobile button -->
				<!-- fullscreen button -->
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen"><i class="fa fa-arrows-alt"></i></a> </span>
				</div>
				<!-- end fullscreen button -->
			</div>
			<!-- end pulled right: nav area -->

		</header>
		<!-- END HEADER -->

		<!-- Left panel : Navigation area -->
		<aside id="left-panel">
			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                              <span>
                                  <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                              </span>
						<i class="fa fa-angle-down"></i>
					</a> 
				</span>
			</div>
			<nav>
				<ul>
					<li class="">
	                <a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MENÚ PRINCIPAL">
	                <i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
	            	</li>
		            <li class="text-center">
		                <a href='<?php echo site_url("admin").'/ejec/prog/'.$tipo.''; ?>' title="REGISTRO DE EJECUCION"> <span class="menu-item-parent">REGISTRO DE EJECUCI&Oacute;N</span></a>
		            </li>
				<?php
		                for($i=0;$i<count($enlaces);$i++)
		                {
		                    if(count($subenlaces[$enlaces[$i]['o_child']])>0)
		                    {
		            ?>
		            <li>
		              	<a href="#" >
		              		<i class="<?php echo $enlaces[$i]['o_image']?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
		              	<ul >
		              	<?php
		                foreach ($subenlaces[$enlaces[$i]['o_child']] as $item) {
		                ?>
		                <li><a href="<?php echo base_url($item['o_url']); ?>"><?php echo $item['o_titulo']; ?></a></li>
		                <?php } ?>
		                </ul>
		            </li>
		            <?php 
		            }
		        } ?>
				</ul>
			</nav>
			<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>
		</aside>

		<!-- MAIN PANEL -->
		<div id="main" role="main">
			<!-- RIBBON -->
			<div id="ribbon">
				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>
				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li>Registro de Ejecuci&oacute;n</li><li><a href="<?php echo base_url().'index.php/admin/ejec/prog/'.$tipo.'' ?>" title="MIS OPERACIONES">Registro de Ejecuci&oacute;n POA</a></li><li>Programa Recurrente</li><li>Ejecutaci&oacute;n F&iacute;sica</li><li>Mis Contratos (Modificar)</li>
				</ol>
			</div>
			<!-- END RIBBON -->
			<!-- MAIN CONTENT -->
			<div id="content">
				<!-- widget grid -->
				<section id="widget-grid" class="">
					<!-- row -->
					<div class="row">
						<div class="alert alert-block alert-success">
							<a class="close" data-dismiss="alert" href="#">×</a>
							<h4 class="alert-heading"><i class="glyphicon glyphicon-user"></i><?php echo " FUNCIONARIO RESPONSABLE : ".$this->session->userdata("funcionario")."   ||  "; ?><i class="glyphicon glyphicon-calendar"></i><?php echo "FECHA ACTUAL : " . date("d") . " del " . date("m") . " de " . date("Y");?></h4>
						</div>
						<article class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
			                <section id="widget-grid" class="well">
			                    <div>
			                        <h1><b> CATEGORIA PROGRAM&Aacute;TICA  : </b><small><?php echo $proyecto[0]['aper_programa'].''.$proyecto[0]['aper_proyecto'].''.$proyecto[0]['aper_actividad'] ?></small></h1>
			                        <h1><b> <?php echo $titulo_proy;?> : </b><small><?php echo $proyecto[0]['proy_nombre'] ?></small></h1>
			                        <h1><b> COMPONENTE : </b><small><?php echo $componente[0]['com_componente']?></small></h1>
			                    </div>
			                </section>
			            </article>
			            <article class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
			                <section id="widget-grid" class="well">
			                    <div>
									<a href="<?php echo base_url().'index.php/admin/ejec/list_contratos/'.$tipo.'/'.$proyecto[0]['proy_id'].'/'.$fase[0]['id'].'/'.$componente[0]['com_id'].'/'.$aper.'' ?>" class="btn btn-success" title="Volver atras" style="width:100%;">VOLVER ATRAS</a>
								</div>
			                </section>
			            </article>
							<!-- Widget ID (each widget will need unique ID)-->
							<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<form id="formulario" name="formulario" novalidate="novalidate" method="post" action="<?php echo site_url("admin").'/ejec/update_contrato' ?>">
								<div class="jarviswidget jarviswidget-color-darken" >
									<header>
										<span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
										<h2 class="font-md"><strong>REGISTRO DE CONTRATO (MODIFICAR)</strong></h2>
									</header>
								<!-- widget div-->
								<div>
									<!-- widget edit box -->
									<div class="jarviswidget-editbox"></div>
									<!-- end widget edit box -->
									<!-- widget content -->
									<div class="widget-body">
										<div class="row">
												<div id="bootstrap-wizard-1" class="col-sm-12">
													<div class="well">
														<div class="row">
														<input class="form-control" type="hidden" name="ctto_id" id="ctto_id" value="<?php echo $ctto_id; ?>" >
														<input class="form-control" type="hidden" name="id_p" id="id_p" value="<?php echo $proyecto[0]['proy_id']?>" >
														<input class="form-control" type="hidden" name="id_f" id="id_f" value="<?php echo $fase[0]['id']?>" >
														<input class="form-control" type="hidden" name="tp" id="tp" value="<?php echo $tipo?>" >
														<input class="form-control" type="hidden" name="id_c" id="id_c" value="<?php echo $componente[0]['com_id']?>" >
														<input class="form-control" type="hidden" name="aper" id="aper" value="<?php echo $aper; ?>" >
                                                            <div class="col-sm-8">
                                                                <div class="form-group">
                                                                    <label><font size="1"><b>TIPO DE CONTRATO</b></font></label>
                                                                    <select class="form-control" id="ctto_tip_id" name="ctto_tip_id" title="Seleccione Tipo contrato">
                                                                        <option value="0">Seleccione una opción</option>
																		<?php
																			foreach($tipo_ctto as $row)
																			{
																				if($row['ctto_tip_id']==$contrato[0]['ctto_tip_id']) {
																					?>
                                                                                    <option value="<?php echo $row['ctto_tip_id']; ?>" selected><?php echo $row['ctto_tip_nombre']; ?></option>
																					<?php
																				}
																				else {
																					?>
                                                                                    <option value="<?php echo $row['ctto_tip_id']; ?>"><?php echo $row['ctto_tip_nombre']; ?></option>
																					<?php
																				}
																			}
																		?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-8">
																<div class="form-group">
																	<label><font size="1"><b>OBJETO CONTRATO</b></font></label>
																	<textarea rows="5" class="form-control" name="obj" id="obj" style="width:100%;" title="Objeto de contrato"><?php echo $contrato[0]['ctto_objeto']?></textarea> 
																</div>
															</div>
															<div class="col-sm-4">
																<div class="form-group">
																	<label><font size="1"><b>MODALIDAD</b></font></label>
																		<select class="form-control" id="mod_id" name="mod_id" title="Seleccione Tipo de modalidad">
                                                                            <option value="0">Seleccione una opción</option>
						                                                    <?php 
																                foreach($mod as $row)
																                {
																                	if($row['mod_id']==$contrato[0]['mod_id'])
																                	{
 																					?>
																		                <option value="<?php echo $row['mod_id']; ?>" selected><?php echo $row['mod_modalidad'].' - '.$row['mod_nivel']; ?></option>
																		            <?php 
																                	}
																                	else
																                	{
																					?>
																		                <option value="<?php echo $row['mod_id']; ?>"><?php echo $row['mod_modalidad'].' - '.$row['mod_nivel']; ?></option>
																		            <?php 
																                	}
																                }
																            ?>       
						                                                </select>
																</div>
															</div>
															<div class="col-sm-4">
																<div class="form-group">
																	<label><font size="1"><b>FECHA DE ADJUDICACI&Oacute;N</b></font></label>
																		<div class="input-group">
																			<input type="text" name="f_adj" id="f_adj" placeholder="Seleccione Fecha adjudicacion" class="form-control datepicker" value="<?php echo date('d/m/Y',strtotime($contrato[0]['ctto_fecha_adjudicacion'])); ?>" data-dateformat="dd/mm/yy" >
																			<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
																		</div>
																</div>
															</div>
														</div>

														<div class="row">
															<div>
																<div class="col-sm-3">
																	<div class="form-group"> 
																		<label><font size="1"><b>NUMERO DE CONTRATO</b></font></label>
																		<input class="form-control" type="text" name="nro_ctto" id="nro_ctto" value="<?php echo $contrato[0]['ctto_numero']?>" title="Ingrese el numero de contrato">
																	</div>
																</div>
															</div>
															<div class="col-sm-3">
																<div class="form-group">
																	<label><font size="1"><b>FECHA DE SUSCRIPCI&Oacute;N</b></font></label>
																		<div class="input-group">
																			<input type="text" name="f_sus" id="f_sus" placeholder="Seleccione Fecha suscripcion" class="form-control datepicker" value="<?php echo date('d/m/Y',strtotime($contrato[0]['ctto_fecha_suscripcion'])); ?>" data-dateformat="dd/mm/yy" >
																			<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
																		</div>
																</div>
															</div>
															
															<div> 
																<div class="col-sm-2">
																	<div class="form-group"> 
																		<label><font size="1"><b>PLAZO (Dias)</b></font></label>
																		<input class="form-control" type="text" name="pl_dd" id="pl_dd" value="<?php echo $contrato[0]['ctto_plazo_dd']?>"  onkeypress="if (this.value.length < 20) { return numerosDecimales(event);}else{return false; }">
																	</div>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-sm-8">
																<div class="form-group">
																	<label><font size="1"><b>OBSERVACIONES</b></font></label>
																	<textarea rows="5" class="form-control" name="obs" id="obs" style="width:100%;" title="Observaciones" ><?php echo $contrato[0]['ctto_observaciones']?></textarea> 
																</div>
															</div>
															<div class="col-sm-3">
																<div class="form-group">
																	<label><font size="1"><b>CONTRATISTA</b></font></label>
																		<select class="form-control" id="ctta_id" name="ctta_id" title="Seleccione Contratista">
						                                                <option value="0">Seleccione</option>
						                                                    <?php 
																                foreach($contratista as $row)
																                {
																                    if($row['ctta_id']==$contrato[0]['ctta_id'])
																                	{
 																					?>
																		                <option value="<?php echo $row['ctta_id']; ?>" selected><?php echo $row['ctta_nombre'].' - '.$row['ctta_sigla']; ?></option>
																		            <?php 
																                	}
																                	else
																                	{
																					?>
																		                <option value="<?php echo $row['ctta_id']; ?>"><?php echo $row['ctta_nombre'].' - '.$row['ctta_sigla']; ?></option>
																		            <?php 
																                	} 	
																                }
																            ?>       
						                                                </select>
																</div>
															</div>
                                                            <div class="col-sm-1">
                                                                <div class="form-group">
                                                                    <label><font size="1"><b>AGREGAR</b></font></label>
                                                                    <a href="#" data-toggle="modal" data-target="#modal_nuevo_ff" class="btn btn-xs nuevo_ff" title="AGREGAR NUEVO CONTRATISTA"><img src="<?php echo base_url(); ?>assets/ifinal/2.png" WIDTH="35" HEIGHT="35"/></a>
                                                                </div>
                                                            </div>
															<div class="col-sm-4">
																<div class="form-group">
																	<label><font size="1"><b>FECHA ORDEN DE INICIO</b></font></label>
																		<div class="input-group">
																			<input type="text" name="f_orden" id="f_orden" placeholder="Seleccione Fecha adjudicacion" class="form-control datepicker" value="<?php echo date('d/m/Y',strtotime($contrato[0]['ctto_fecha_orden_inicio'])); ?>" data-dateformat="dd/mm/yy" >
																			<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
																		</div>
																</div>
															</div>
														</div>
														<div class="row">
															
															<div class="col-sm-2">
																<div class="form-group">
																	<label><font size="1"><b>MONTO (Bolivianos)</b></font></label>
																	<input class="form-control" type="text" name="bol" id="bol" value="<?php echo $contrato[0]['ctto_bolivianos']?>" onkeypress="if (this.value.length < 20) { return numerosDecimales(event);}else{return false; }">
																</div>
															</div>
															<div class="col-sm-2">
																<div class="form-group">
																	<label><font size="1"><b>ANTICIPO</b></font></label>
																	<input class="form-control" type="text" name="ant" id="ant" value="<?php echo $contrato[0]['ctto_anticipo']?>" onkeypress="if (this.value.length < 20) { return numerosDecimales(event);}else{return false; }">
																</div>
															</div>
															<div class="col-sm-3">
																<div class="form-group">
																	<label><font size="1"><b>MODENA DE PAGO</b></font></label>
																	<input class="form-control" type="text" name="moneda" id="moneda" value="<?php echo $contrato[0]['ctto_moneda']?>" placeholder="BOLIVIANO">
																</div>
															</div>
															<div class="col-sm-3">
																<div class="form-group">
																	<label><font size="1"><b>RESOLUCION</b></font></label>
																	<input class="form-control" type="text" name="res" id="res" value="<?php echo $contrato[0]['ctto_resolucion']?>">
																</div>
															</div>
														</div>
													</div> <!-- end well -->
															
												</div>
												<div  class="col-sm-12">
													<div class="form-actions">
														<a href="<?php echo base_url().'index.php/admin/ejec/list_contratos/'.$tipo.'/'.$proyecto[0]['proy_id'].'/'.$fase[0]['id'].'/'.$componente[0]['com_id'].'/'.$aper.'' ?>" class="btn btn-lg btn-success"> CANCELAR </a>
														<input type="button" id="enviar_obj" value="MODIFICAR" class="btn btn-primary btn-lg" onclick="valida_envia()" title="Guardar y modificar informacion contratos">
													</div>
												</div>
											</form>
										</div>
									</div>
									<!-- end widget content -->
								</div>
								<!-- end widget div -->
							</div>
							<!-- end widget -->
					</div>
					<!-- end row -->
				</section>
				<!-- end widget grid -->					
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN PANEL -->
        <!-- ================== Modal nuevo contratista  ========================== -->
        <div class="modal animated fadeInDown" id="modal_nuevo_ff" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close text-danger " data-dismiss="modal" aria-hidden="true">
                            &times;
                        </button>
                        <h4 class="modal-title">
                            <img src="<?php echo base_url(); ?>assets/img/logo.png" width="150" alt="SmartAdmin">
                        </h4>
                        <h4 class="modal-title text-center text-info">
                            <b><i class="glyphicon glyphicon-pencil"></i> REGISTRO DE CONTRATISTA </b>
                        </h4>
                    </div>
                    <div class="modal-body no-padding">
                        <div class="row">
                            <form id="form_ff" novalidate="novalidate" method="post">
                                <div id="bootstrap-wizard-1" class="col-sm-12">
                                    <div class="well">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label><font size="1"><b>NOMBRE DEL CONTRATISTA</b></font></label>
                                                    <textarea rows="3" class="form-control" name="ctta_nombre" id="ctta_nombre" style="width:100%;" title="NOMBRE DEL CONTRATISTA" ></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <LABEL><b>SIGLA</b></label>
                                                    <input class="form-control" type="text" name="ctta_sigla" id="ctta_sigla" placeholder="SIGLA" title="SIGLA" >
                                                </div>
                                            </div>
                                            <div class="col-sm-8">
                                                <div class="form-group">
                                                    <LABEL><b>DIRECCI&Oacute;N</b></label>
                                                    <textarea rows="2" class="form-control" name="ctta_direccion" id="ctta_direccion" style="width:100%;" placeholder="DIRECCION" title="DIRECCION" ></textarea>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <LABEL><b>NIT CONTRATISTA</b></label>
                                                    <input class="form-control" type="text" name="ctta_nit" id="ctta_nit" placeholder="XXXXXXXXXX" title="NIT CONTRATISTA">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <LABEL><b>PAIS</b></label>
                                                    <input class="form-control" type="text" name="ctta_pais" id="ctta_pais" placeholder="PAIS" title="PAIS" >
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <LABEL><b>CIUDAD</b></label>
                                                    <input class="form-control" type="text" name="ctta_ciudad" id="ctta_ciudad" placeholder="CIUDAD" title="CIUDAD" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <LABEL><b>CASILLA</b></label>
                                                    <input class="form-control" type="text" name="ctta_casilla" id="ctta_casilla" placeholder="CASILLA" title="CASILLA" >
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <LABEL><b>TELEFONO</b></label>
                                                    <input class="form-control" type="text" name="ctta_fono" id="ctta_fono" value="0" placeholder="XXXXXXX" onkeypress="if (this.value.length < 11) { return soloNumeros(event);}else{return false; }">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <LABEL><b>FAX</b></label>
                                                    <input class="form-control" type="text" name="ctta_fax" id="ctta_fax" placeholder="FAX" title="FAX">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <LABEL><b>EMAIL</b></label>
                                                    <input class="form-control"  type="email" name="ctta_email" id="ctta_email">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <LABEL><b>REPRESENTANTE</b></label>
                                                    <input class="form-control" type="text" name="ctta_rep" id="ctta_rep" placeholder="REPRESENTANTE" title="REPRESENTANTE">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <LABEL><b>CARGO</b></label>
                                                    <input class="form-control" type="text" name="ctta_cargo" id="ctta_cargo" placeholder="CARGO" title="CARGO">
                                                </div>
                                            </div>
                                        </div>
                                    </div> <!-- end well -->
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="row">
                            <div class="col-md-3 pull-left">
                                <button class="btn btn-ms btn-danger" data-dismiss="modal">CANCELAR</button>
                            </div>
                            <div class="col-md-3 pull-right ">
                                <button type="submit" name="enviar_ff" id="enviar_ff" class="btn  btn-ms btn-primary"><i class="fa fa-save"></i>
                                    GUARDAR
                                </button>
                            </div>
                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
		<!-- PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->
		<!--================================================== -->
		<script src="<?php echo base_url();?>/assets/js/libs/jquery-2.0.2.min.js"></script>
		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>
		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url();?>/assets/js/app.config.js"></script>
		<script src = "<?php echo base_url(); ?>mis_js/control_session.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/mis_js/validacion_form.js"></script>
		<SCRIPT src="<?php echo base_url(); ?>mis_js/programacion/ejecucion/abm_ejecucion.js" type="text/javascript"></SCRIPT>
		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url();?>/assets/js/bootstrap/bootstrap.min.js"></script>
		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url();?>/assets/js/notification/SmartNotification.min.js"></script>
		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url();?>/assets/js/smartwidgets/jarvis.widget.min.js"></script>
		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url();?>/assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<!-- SPARKLINES -->
		<script src="<?php echo base_url();?>/assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/select2/select2.min.js"></script>
		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
		<!-- browser msie issue fix -->
		<script src="<?php echo base_url();?>/assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url();?>/assets/js/plugin/fastclick/fastclick.min.js"></script>
		<!-- Demo purpose only -->
		<script src="<?php echo base_url();?>/assets/js/demo.min.js"></script>
		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url();?>/assets/js/app.min.js"></script>
		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<!-- Voice command : plugin -->
		<script src="<?php echo base_url();?>/assets/js/speech/voicecommand.min.js"></script>
		<script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
		<script src="<?php echo base_url();?>/assets/js/plugin/fuelux/wizard/wizard.min.js"></script>
        <script src="<?php echo base_url();?>/assets/lib_alerta/alertify.min.js"></script>
		<script>
		$(document).ready(function() {
			pageSetUp();
			//Bootstrap Wizard Validations
		})
            function valida_envia()
            {
                if (document.formulario.ctto_tip_id.value=="0") /////// Tipo de contrato
                { alertify.alert("Seleccione el tipo de contrato");
                    document.formulario.ctto_tip_id.focus()
                    return 0;
                }
                if (document.formulario.obj.value=="") /////// Programa
                { alertify.alert("Registre el objeto del contrato");
                    document.formulario.obj.focus()
                    return 0;
                }
                if (document.formulario.mod_id.value=="0") /////// Programa
                { alertify.alert("Seleccione modalidad del contrato")
                    document.formulario.mod_id.focus()
                    return 0;
                }
                if (document.formulario.f_adj.value=="") /////// Programa
                { alertify.alert("Seleccione fecha de adjudicacion")
                    document.formulario.f_adj.focus()
                    return 0;
                }
                if (document.formulario.f_sus.value=="") /////// Programa
                { alertify.alert("Seleccione fecha suscripcion del contrato")
                    document.formulario.f_sus.focus()
                    return 0;
                }
                if (document.formulario.f_orden.value=="") /////// Programa
                { alertify.alert("Seleccione fecha Orden de inicio")
                    document.formulario.f_orden.focus()
                    return 0;
                }
                if (document.formulario.ctta_id.value=="0") /////// Programa
                { alertify.alert("Seleccione Contratista")
                    document.formulario.ctta_id.focus()
                    return 0;
                }

                alertify.confirm("¿ESTÁ SEGURO DE GUARDAR LAS MODIFICACIONES HECHAS?", function (a) {
                    if (a) {
                        document.formulario.submit();
                        document.getElementById('enviar_obj').disabled = true;
                    } else {
                        alertify.error("OPCI\u00D3N CANCELADA");
                    }
                });

            }
          </script>
        <!--================= NUEVO CONTRATISTA =========================================-->
        <script type="text/javascript">
            $(function () {
                var id_p = '';
                $('#modal_nuevo_ff').on('hidden.bs.modal', function () {
                    document.forms['form_ff'].reset();

                });
                $("#enviar_ff").on("click", function (e) {

                    //========================VALIDANDO FORMULARIO===================
                    var $validator = $("#form_ff").validate({
                        //////////////// DATOS GENERALES
                        rules: {
                            ctta_nombre: { //// nombre
                                required: true,
                            },
                            ctta_sigla: { //// sigla
                                required: true,
                            },
                            ctta_direccion: { //// direccion
                                required: true,
                            },
                            ctta_pais: { //// ciudad
                                required: true,
                            },
                            ctta_ciudad: { //// pais
                                required: true,
                            },
                            ctta_nit: { //// pais
                                required: true,
                            },
                            ctta_email: { //// email
                                required : true,
                                email : true
                            },
                            ctta_representante: { //// representante
                                required: true,
                            }
                        },
                        messages: {
                            ctta_nombre: "registre este campo",
                            ctta_sigla: "registre este campo",
                            ctta_direccion: "registre este campo",
                            ctta_pais: "registre este campo",
                            ctta_nit: "registre este campo",
                            ctta_cuidad: "registre este campo",
                            ctta_email : {
                                required : 'Please enter your email address',
                                email : '<i class="fa fa-warning"></i><strong>Please enter a VALID email addres</strong>'
                            },
                            ctta_representante:"registre este campo",


                        },
                        highlight: function (element) {
                            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                        },
                        unhighlight: function (element) {
                            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                        },
                        errorElement: 'span',
                        errorClass: 'help-block',
                        errorPlacement: function (error, element) {
                            if (element.parent('.input-group').length) {
                                error.insertAfter(element.parent());
                            } else {
                                error.insertAfter(element);
                            }
                        }
                    });
                    var $valid = $("#form_ff").valid();
                    if (!$valid) {
                        $validator.focusInvalid();
                        //return false;
                    } else {
                        //==========================================================
                        var ctta_nombre = document.getElementById("ctta_nombre").value;
                        var ctta_sigla = document.getElementById("ctta_sigla").value;
                        var ctta_direccion = document.getElementById("ctta_direccion").value;
                        var ctta_ciudad = document.getElementById("ctta_ciudad").value;
                        var ctta_pais = document.getElementById("ctta_pais").value;
                        var ctta_casilla = document.getElementById("ctta_casilla").value;
                        var ctta_nit = document.getElementById("ctta_nit").value;
                        var ctta_fono = document.getElementById("ctta_fono").value;
                        var ctta_fax = document.getElementById("ctta_fax").value;
                        var ctta_email = document.getElementById("ctta_email").value;
                        var ctta_rep = document.getElementById("ctta_rep").value;
                        var ctta_cargo = document.getElementById("ctta_cargo").value;
                        //=================== VERIFICAR SI EXISTE EL COD DE PROGRAMA ==============

                        var url = "<?php echo site_url("admin")?>/ejec/add_ctta";
                        $.ajax({
                            type: "post",
                            url: url,
                            data: {
                                ctta_nombre: ctta_nombre,
                                ctta_sigla: ctta_sigla,
                                ctta_direccion: ctta_direccion,
                                ctta_ciudad: ctta_ciudad,
                                ctta_pais: ctta_pais,
                                ctta_casilla: ctta_casilla,
                                ctta_fono: ctta_fono,
                                ctta_fax: ctta_fax,
                                ctta_email: ctta_email,
                                ctta_nit: ctta_nit,
                                ctta_rep: ctta_rep,
                                ctta_cargo: ctta_cargo
                            },
                            success: function (data) {
                                window.location.reload(true);
                            }
                        });
                    }
                });
            });
        </script>
	</body>
</html>
