<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

		<title><?php echo $this->session->userdata('name')?></title>
		<meta name="description" content="">
		<meta name="author" content="">
			
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.core.css"/>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css" id="toggleCSS"/>
	</head>
	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->

		<!-- HEADER -->
		<header id="header">
			<div id="logo-group">
				<!-- <span id="logo"> <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="SmartAdmin"> </span> -->
			</div>

			<!-- pulled right: nav area -->
			<div class="pull-right">
				<!-- collapse menu button -->
				<div id="hide-menu" class="btn-header pull-right">
					<span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
				</div>
				<!-- end collapse menu -->
				<!-- logout button -->
				<div id="logout" class="btn-header transparent pull-right">
					<span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Sign Out" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
				</div>
				<!-- end logout button -->
				<!-- search mobile button (this is hidden till mobile view port) -->
				<div id="search-mobile" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
				</div>
				<!-- end search mobile button -->
				<!-- fullscreen button -->
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen"><i class="fa fa-arrows-alt"></i></a> </span>
				</div>
				<!-- end fullscreen button -->
			</div>
			<!-- end pulled right: nav area -->

		</header>
		<!-- END HEADER -->

		<!-- Left panel : Navigation area -->
		<aside id="left-panel">
			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                        <span>
                            <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                        </span>
						<i class="fa fa-angle-down"></i>
					</a> 
				</span>
			</div>
			<nav>
				<ul>
					<li class="">
	                <a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MENÚ PRINCIPAL">
	                <i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
	            	</li>
		            <li class="text-center">
		                <a href='<?php echo site_url("admin").'/ejec/prog/'.$tipo.''; ?>' title="REGISTRO DE EJECUCION"> <span class="menu-item-parent">REGISTRO DE EJECUCI&Oacute;N</span></a>
		            </li>
				<?php
		                for($i=0;$i<count($enlaces);$i++)
		                {
		                    if(count($subenlaces[$enlaces[$i]['o_child']])>0)
		                    {
		            ?>
		            <li>
		              	<a href="#" >
		              		<i class="<?php echo $enlaces[$i]['o_image']?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
		              	<ul >
		              	<?php
		                foreach ($subenlaces[$enlaces[$i]['o_child']] as $item) {
		                ?>
		                <li><a href="<?php echo base_url($item['o_url']); ?>"><?php echo $item['o_titulo']; ?></a></li>
		                <?php } ?>
		                </ul>
		            </li>
		            <?php 
		            }
		        } ?>
				</ul>
			</nav>
			<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>
		</aside>

		<!-- MAIN PANEL -->
		<div id="main" role="main">
			<!-- RIBBON -->
			<div id="ribbon">
				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>
				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li>Registro de Ejecuci&oacute;n</li><li><a href="<?php echo base_url().'index.php/admin/ejec/garantias/'.$tipo.'/'.$contrato[0]['ctto_id'].'/'.$id_p.'/'.$id_f.'/'.$componente[0]['com_id'].'/'.$proyecto[0]['aper_programa'].'' ?>"  title="MIS GARANTIAS">Mis Garantias</a></li><li>Garantia (Modificar)</li>
				</ol>
			</div>
			<!-- END RIBBON -->
			<!-- MAIN CONTENT -->
			<div id="content">
				<!-- widget grid -->
				<section id="widget-grid" class="">
					<!-- row -->
					<div class="row">
						<div class="alert alert-block alert-success">
							<a class="close" data-dismiss="alert" href="#">×</a>
							<h4 class="alert-heading"><i class="glyphicon glyphicon-user"></i><?php echo " FUNCIONARIO RESPONSABLE : ".$this->session->userdata("funcionario")."   ||  "; ?><i class="glyphicon glyphicon-calendar"></i><?php echo "FECHA ACTUAL : " . date("d") . " del " . date("m") . " de " . date("Y");?></h4>
						</div>
						<article class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
			                <section id="widget-grid" class="well">
			                    <div>
			                        <h1><b> CATEGORIA PROGRAM&Aacute;TICA  : </b><small><?php echo $proyecto[0]['aper_programa'].''.$proyecto[0]['aper_proyecto'].''.$proyecto[0]['aper_actividad'] ?></small></h1>
			                        <h1><b> <?php echo $titulo_proy;?> : </b><small><?php echo $proyecto[0]['proy_nombre'] ?></small></h1>
			                        <h1><b> COMPONENTE : </b><small><?php echo $componente[0]['com_componente']?></small></h1>
			                        <h1><b> CONTRATO : </b><small><?php echo $contrato[0]['ctto_objeto'];?></small></h1>
			                    </div>
			                </section>
			            </article>
			            <article class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
			                <section id="widget-grid" class="well">
			                    <div>
									<a href="<?php echo base_url().'index.php/admin/ejec/garantias/'.$tipo.'/'.$contrato[0]['ctto_id'].'/'.$id_p.'/'.$id_f.'/'.$componente[0]['com_id'].'/'.$proyecto[0]['aper_programa'].'' ?>" class="btn btn-success" title="Volver atras" style="width:100%;">VOLVER ATRAS</a>
								</div>
			                </section>
			            </article>
							<!-- Widget ID (each widget will need unique ID)-->
							<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<?php
									if($this->uri->segment(10)=='false')
									{
										?>
										<div class="alert alert-danger">
										  <a href="#" class="alert-link">Error!!</a>
										</div>
										<?php
									}
									
								?>
								<form id="formulario" name="formulario" novalidate="novalidate" method="post" action="<?php echo site_url("admin").'/ejec/update_garantia' ?>">
								<div class="jarviswidget jarviswidget-color-darken" >
									<header>
										<span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
										<h2 class="font-md"><strong>GARANTIA (Modificar)</strong></h2>				
									</header>
								<!-- widget div-->
								<div>
									<!-- widget edit box -->
									<div class="jarviswidget-editbox"></div>
									<!-- end widget edit box -->
									<!-- widget content -->
									<div class="widget-body">
										<div class="row">
												<div id="bootstrap-wizard-1" class="col-sm-12">
													<div class="well">
														<div class="row">
														<input class="form-control" type="hidden" name="id_p" id="id_p" value="<?php echo $proyecto[0]['proy_id']?>" >
														<input class="form-control" type="hidden" name="id_f" id="id_f" value="<?php echo $fase[0]['id']?>" >
														<input class="form-control" type="hidden" name="id_gta" id="id_gta" value="<?php echo $garantia[0]['gtia_id']?>" >
														<input class="form-control" type="hidden" name="tp" id="tp" value="<?php echo $tipo?>" >
														<input class="form-control" type="hidden" name="id_c" id="id_c" value="<?php echo $componente[0]['com_id']?>" >
														<input class="form-control" type="hidden" name="id_ctto" id="id_ctto" value="<?php echo $contrato[0]['ctto_id']?>" >
														<input class="form-control" type="hidden" name="aper" id="aper" value="<?php echo $aper; ?>" >
                                                            <div class="col-sm-2">
                                                                <div class="form-group">
                                                                    <label><font size="1"><b>TIPO</b></font></label>
                                                                    <select class="form-control" id="gtia_tip_id" name="gtia_tip_id" title="Seleccione tipo">
                                                                        <option value="0">Seleccione</option>
																		<?php
																			foreach($garantia_tipo as $row)
																			{
																				if($row['gtia_tip_id']==$garantia[0]['gtia_tip_id'])
																				{
																					?>
                                                                                    <option value="<?php echo $row['gtia_tip_id']; ?>" selected><?php echo $row['gtia_tip_nombre']; ?></option>
																					<?php
																				}
																				else
																				{
																					?>
                                                                                    <option value="<?php echo $row['gtia_tip_id']; ?>"><?php echo $row['gtia_tip_nombre']; ?></option>
																					<?php
																				}
																			}
																		?>
                                                                    </select>
                                                                </div>
                                                            </div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label><font size="1"><b>DETALLE</b></font></label>
																	<textarea rows="4" class="form-control" name="detalle" id="detalle" style="width:100%;" title="Detalle"><?php echo $garantia[0]['gtia_detalle']?></textarea> 
																</div>
															</div>
															<div class="col-sm-2">
																<div class="form-group">
																	<label><font size="1"><b>PLAZO</b></font></label>
																		<input class="form-control" type="text" name="plazo" id="plazo" value="<?php echo $garantia[0]['gtia_plzo']?>" onkeypress="if (this.value.length < 20) { return numerosDecimales(event);}else{return false; }" title="Plazo">
																</div>
															</div>
															<div class="col-sm-3">
																<div class="form-group">
																	<label><font size="1"><b>FECHA DE VENCIMIENTO</b></font></label>
																		<div class="input-group">
																			<input type="text" name="f_venc" id="f_venc" placeholder="Seleccione Fecha de vencimiento" class="form-control datepicker" value="<?php echo date('d/m/Y',strtotime($garantia[0]['gtia_fecha_vencimiento'])); ?>" data-dateformat="dd/mm/yy" >
																			<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
																		</div>
																</div>
															</div>
															<div class="col-sm-3">
																<div class="form-group">
																	<label><font size="1"><b>FECHA DE EMISI&Oacute;N</b></font></label>
																		<div class="input-group">
																			<input type="text" name="f_ent" id="f_ent" placeholder="Seleccione Fecha de emision" class="form-control datepicker" value="<?php echo date('d/m/Y',strtotime($garantia[0]['gtia_fecha_emision'])); ?>" data-dateformat="dd/mm/yy" >
																			<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
																		</div>
																</div>
															</div>
															<div class="col-sm-4">
																<div class="form-group">
																	<label><font size="1"><b>EMISOR</b></font></label>
																		<input class="form-control" type="text" name="emisor" id="emisor" value="<?php echo $garantia[0]['gtia_emisor']?>" title="Emisor">
																</div>
															</div>
														</div>

														<div class="row">
															<div class="col-sm-12">
																<div class="form-group">
																	<label><font size="1"><b>OBSERVACIONES</b></font></label>
																	<textarea rows="4" class="form-control" name="obs" id="obs" style="width:100%;" title="Observaciones" ><?php echo $garantia[0]['gtia_obs']?></textarea> 
																</div>
															</div>
															
														</div>
														
													</div> <!-- end well -->
															
												</div>
												<div  class="col-sm-12">
													<div class="form-actions">
														<a href="<?php echo base_url().'index.php/admin/ejec/prog/'.$tipo.'' ?>" class="btn btn-lg btn-success" title="Volver a Mis acciones"> CANCELAR </a>
														<input type="button" id="enviar_obj" value="MODIFICAR" class="btn btn-primary btn-lg" onclick="valida_envia()" title="Guardar y Garantia">
													</div>
												</div>
											</form>
										</div>
									</div>
									<!-- end widget content -->
								</div>
								<!-- end widget div -->
							</div>
							<!-- end widget -->
					</div>
					<!-- end row -->
				</section>
				<!-- end widget grid -->					
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN PANEL -->

		<!-- PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->
		<!--================================================== -->
		<script src="<?php echo base_url();?>/assets/js/libs/jquery-2.0.2.min.js"></script>
		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>
		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url();?>/assets/js/app.config.js"></script>
		<script src = "<?php echo base_url(); ?>mis_js/control_session.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/mis_js/validacion_form.js"></script>
		<SCRIPT src="<?php echo base_url(); ?>mis_js/programacion/ejecucion/abm_ejecucion.js" type="text/javascript"></SCRIPT>
		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url();?>/assets/js/bootstrap/bootstrap.min.js"></script>
		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url();?>/assets/js/notification/SmartNotification.min.js"></script>
		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url();?>/assets/js/smartwidgets/jarvis.widget.min.js"></script>
		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url();?>/assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<!-- SPARKLINES -->
		<script src="<?php echo base_url();?>/assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/select2/select2.min.js"></script>
		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
		<!-- browser msie issue fix -->
		<script src="<?php echo base_url();?>/assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url();?>/assets/js/plugin/fastclick/fastclick.min.js"></script>
		<!-- Demo purpose only -->
		<script src="<?php echo base_url();?>/assets/js/demo.min.js"></script>
		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url();?>/assets/js/app.min.js"></script>
		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<!-- Voice command : plugin -->
		<script src="<?php echo base_url();?>/assets/js/speech/voicecommand.min.js"></script>
		<script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
		<script src="<?php echo base_url();?>/assets/js/plugin/fuelux/wizard/wizard.min.js"></script>
        <script src="<?php echo base_url();?>/assets/lib_alerta/alertify.min.js"></script>
		<script>
		$(document).ready(function() {
			pageSetUp();
			//Bootstrap Wizard Validations
		})
            function valida_envia()
            {
                if (document.formulario.gtia_tip_id.value=="0") /////// Programa
                { alertify.alert("Seleccciones el tipo de garantía")
                    document.formulario.detalle.focus()
                    return 0;
                }
                if (document.formulario.detalle.value=="") /////// Programa
                { alertify.alert("Registre campo Detalle")
                    document.formulario.detalle.focus()
                    return 0;
                }
                if (document.formulario.plazo.value=="") /////// Programa
                { alertify.alert("Seleccione campo Plazo")
                    document.formulario.plazo.focus()
                    return 0;
                }
                if (document.formulario.f_venc.value=="") /////// Programa
                { alertify.alert("Seleccione campo fecha de vencimiento")
                    document.formulario.f_venc.focus()
                    return 0;
                }
                if (document.formulario.f_ent.value=="") /////// Programa
                { alertify.alert("Seleccione campo fecha de entrega")
                    document.formulario.f_ent.focus()
                    return 0;
                }
                alertify.confirm("¿GUARDAR MODIFICACIONES?", function (a) {
                    if (a) {
                        document.formulario.submit();
                        document.getElementById('enviar_obj').disabled = true;
                    } else {
                        alertify.error("OPCI\u00D3N CANCELADA");
                    }
                });

            }
          </script>
	</body>
</html>
