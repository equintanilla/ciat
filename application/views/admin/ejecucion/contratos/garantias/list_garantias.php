<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
		<title><?php echo $this->session->userdata('name')?></title>
		<meta name="description" content="">
		<meta name="author" content="">
			
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.core.css"/>
    	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css" id="toggleCSS"/>
    	<style>
		table{
         	font-size: 9px;
            width: 100%;
            max-width:1550px;;
          	overflow-x: scroll;
            }
            th{
                padding: 1.4px;
                text-align: center;
                font-size: 9px;
            }
            td {
                padding: 1.4px;
                font-size: 9px;
            }
		</style>
	</head>
	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->

		<!-- HEADER -->
		<header id="header">
			<div id="logo-group">
				<!-- <span id="logo"> <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="SmartAdmin"> </span> -->
			</div>

			<!-- pulled right: nav area -->
			<div class="pull-right">
				
				<!-- collapse menu button -->
				<div id="hide-menu" class="btn-header pull-right">
					<span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
				</div>
				<!-- end collapse menu -->
				<!-- logout button -->
				<div id="logout" class="btn-header transparent pull-right">
					<span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Sign Out" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
				</div>
				<!-- end logout button -->
				<!-- search mobile button (this is hidden till mobile view port) -->
				<div id="search-mobile" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
				</div>
				<!-- end search mobile button -->
				<!-- fullscreen button -->
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen"><i class="fa fa-arrows-alt"></i></a> </span>
				</div>
				<!-- end fullscreen button -->
			</div>
			<!-- end pulled right: nav area -->

		</header>
		<!-- END HEADER -->

		<!-- Left panel : Navigation area -->
		<aside id="left-panel">
			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                              <span>
                                  <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                              </span>
						<i class="fa fa-angle-down"></i>
					</a> 
				</span>
			</div>
			<nav>
				<ul>
					<li class="">
	                <a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MENÚ PRINCIPAL">
	                <i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
	            	</li>
		            <li class="text-center">
		                <a href='<?php echo site_url("admin").'/ejec/prog/'.$tipo.''; ?>' title="REGISTRO DE EJECUCION"> <span class="menu-item-parent">REGISTRO DE EJECUCI&Oacute;N</span></a>
		            </li>
				<?php
		                for($i=0;$i<count($enlaces);$i++)
		                {
		                    if(count($subenlaces[$enlaces[$i]['o_child']])>0)
		                    {
		            ?>
		            <li>
		              	<a href="#" >
		              		<i class="<?php echo $enlaces[$i]['o_image']?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
		              	<ul >
		              	<?php
		                foreach ($subenlaces[$enlaces[$i]['o_child']] as $item) {
		                ?>
		                <li><a href="<?php echo base_url($item['o_url']); ?>"><?php echo $item['o_titulo']; ?></a></li>
		                <?php } ?>
		                </ul>
		            </li>
		            <?php 
		            }
		        } ?>
				</ul>
			</nav>
			<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>
		</aside>
		<!-- MAIN PANEL -->
		<div id="main" role="main">

			<!-- RIBBON -->
			<div id="ribbon">

				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>

				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li>Registro de Ejecuci&oacute;n</li><li><a href="<?php echo base_url().'index.php/admin/ejec/prog/'.$tipo.'' ?>" title="MIS OPERACIONES">Registro de Ejecuci&oacute;n POA</a></li><li>Programa Recurrente</li><li>Ejecutaci&oacute;n F&iacute;sica</li><li>Mis Contratos</li><li>Garantias</li>
				</ol>
			</div>

			<!-- MAIN CONTENT -->
			<div id="content">
				<!-- widget grid -->
				<section id="widget-grid" class="">
					<div class="row">
						<div class="alert alert-block alert-success" >
							<a class="close" data-dismiss="alert" href="#">×</a>
							<h4 class="alert-heading"><i class="glyphicon glyphicon-user"></i><?php echo " FUNCIONARIO RESPONSABLE : ".$this->session->userdata("funcionario")."   ||  "; ?><i class="glyphicon glyphicon-calendar"></i><?php echo "FECHA ACTUAL : " . date("d") . " del " . date("m") . " de " . date("Y");?></h4>
						</div>
						<article class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
			                <section id="widget-grid" class="well">
			                    <div>
			                        <h1><b> CATEGORIA PROGRAM&Aacute;TICA  : </b><small><?php echo $proyecto[0]['aper_programa'].''.$proyecto[0]['aper_proyecto'].''.$proyecto[0]['aper_actividad'] ?></small></h1>
			                        <h1><b> <?php echo $titulo_proy;?> : </b><small><?php echo $proyecto[0]['proy_nombre'] ?></small></h1>
			                        <h1><b> COMPONENTE : </b><small><?php echo $componente[0]['com_componente']?></small></h1>
			                        <h1><b> CONTRATO : </b><small><?php echo $contrato[0]['ctto_objeto'];?></small></h1>
			                    </div>
			                </section>
			            </article>
			            <article class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
			                <section id="widget-grid" class="well">
			                    <div>
									<a href="<?php echo base_url().'index.php/admin/ejec/list_contratos/'.$tipo.'/'.$id_p.'/'.$id_f.'/'.$componente[0]['com_id'].'/'.$proyecto[0]['aper_programa'].'' ?>" class="btn btn-success" title="Volver atras" style="width:100%;">VOLVER ATRAS</a>
								</div>
			                </section>
			            </article>
						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<?php 
									if($this->uri->segment(10)=='true')
									{
										?>
										<div class="alert alert-info">
										  <a href="#" class="alert-link">Se registro Correctamente !!</a>
										</div>
										<?php
									}
									elseif($this->uri->segment(10)=='true2')
									{
										?>
										<div class="alert alert-info">
										  <a href="#" class="alert-link">Se Modifico Correctamente !!</a>
										</div>
										<?php
									}
									
								?>
									<div class="jarviswidget jarviswidget-color-darken" >
										<header>
											<span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
											<h2 class="font-md"><strong>GARANTIAS DEL CONTRATO</strong></h2>				
										</header>
											<!-- widget content -->
										<div class="widget-body">
										<div class="table-responsive">
											<table class="table table-bordered">
												<thead>
													<tr>
														<th style="width:10%;"><a href='<?php echo site_url("admin").'/ejec/new_garantia/'.$tipo.'/'.$contrato[0]['ctto_id'].'/'.$id_p.'/'.$id_f.'/'.$componente[0]['com_id'].'/'.$aper.''; ?>' title="NUEVO GARANTIA"><img src="<?php echo base_url(); ?>assets/ifinal/2.png" WIDTH="40" HEIGHT="40"/><br>REGISTRO DE GARANTIAS</a></center></th>
														<th style="width:10%;">TIPO</th>
														<th style="width:25%;">&nbsp;DETALLE GARANTIA&nbsp;</th>
														<th style="width:5%;">&nbsp;PLAZO DE GARANTIA&nbsp;&nbsp;</th>
														<th style="width:10%;">FECHA DE VENCIMIENTO</th>
														<th style="width:10%;">FECHA DE ENTREGA</th>
														<th style="width:10%;">EMISOR</th>
														<th>OBSERVACI&Oacute;N</th>
													</tr>
												</thead>
												<tbody>
												<?php

												$nro=1;
				                                    foreach($garantias  as $row)
				                                    {
				                                    	echo '<tr>';
				                                    	?>
				                                           	<td>
                                                                <div class="btn-group">
                                                                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-list"></i> ACCIONES <span class="caret"></span></button>
                                                                    <ul class="dropdown-menu" role="menu">
                                                                      <li><a href='<?php echo site_url("admin").'/ejec/edit_garantia/'.$tipo.'/'.$row['gtia_id'].'/'.$row['ctto_id'].'/'.$id_p.'/'.$id_f.'/'.$componente[0]['com_id'].'/'.$aper.''; ?>' title="MODIFICAR CONTRATO"><i class="glyphicon glyphicon-pencil"></i> Modificar</a></li>
                                                                      <li><a href="#" data-toggle="modal" data-target="#modal_del_ff" class="del_ff" title="ELIMINAR GARANTIA" name="<?php echo $row['gtia_id']; ?>"><i class="glyphicon glyphicon-trash"></i> Eliminar</a></li>
                                                                    </ul>
                                                                </div>
				                                           	</td>
				                                        <?php
					                                        echo '<td>'.$row['gtia_tip_nombre'].'</td>';
				                                    	 	echo '<td>'.$row['gtia_detalle'].'</td>';
												            echo '<td>'.$row['gtia_plzo'].'</td>';											            
												            echo '<td>'.date('d/m/Y',strtotime($row['gtia_fecha_vencimiento'])).'</td>';
												            echo '<td>'.date('d/m/Y',strtotime($row['gtia_fecha_emision'])).'</td>';
												            echo '<td>'.$row['gtia_emisor'].'</td>';	
												            echo '<td>'.$row['gtia_obs'].'</td>';
												        echo '</tr>';
				                                    	$nro++;
				                                    }
				                                ?>
												</tbody>
											</table>
											</div>
										</div>
										<!-- end widget content -->
									</div>
							<!-- end widget -->
						</article>
						<!-- WIDGET END -->
					</div>
				</section>
			</div>
			<!-- END MAIN CONTENT -->
		</div>

		<!-- PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->

		<!--================================================== -->

		<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
		<script data-pace-options='{ "restartOnRequestAfter": true }' src="<?php echo base_url(); ?>assets/js/plugin/pace/pace.min.js"></script>

		<script>
			if (!window.jQuery) {
				document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-2.0.2.min.js"><\/script>');
			}
		</script>

		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>

		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url(); ?>assets/js/app.config.js"></script>
		<script src = "<?php echo base_url(); ?>mis_js/control_session.js"></script>
		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.min.js"></script>
		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url(); ?>assets/js/notification/SmartNotification.min.js"></script>
		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url(); ?>assets/js/smartwidgets/jarvis.widget.min.js"></script>
		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<!-- SPARKLINES -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/select2/select2.min.js"></script>
		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
		<!-- browser msie issue fix -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/fastclick/fastclick.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
		<!-- Demo purpose only -->
		<script src="<?php echo base_url(); ?>assets/js/demo.min.js"></script>
		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url(); ?>assets/js/app.min.js"></script>
		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<!-- Voice command : plugin -->
		<script src="<?php echo base_url(); ?>assets/js/speech/voicecommand.min.js"></script>
		<!-- PAGE RELATED PLUGIN(S) -->
		<script type="text/javascript">
		    $(function () {
		        function reset() {
		            $("#toggleCSS").attr("href", "<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css");
		            alertify.set({
		                labels: {
		                    ok: "ACEPTAR",
		                    cancel: "CANCELAR"
		                },
		                delay: 5000,
		                buttonReverse: false,
		                buttonFocus: "ok"
		            });
		        }

		        // =====================================================================
		        $(".del_ff").on("click", function (e) {
		            reset();
		            var name = $(this).attr('name');
		            var request;
		            // confirm dialog
		            alertify.confirm("REALMENTE DESEA ELIMINAR ESTE REGISTRO?", function (a) {
		                if (a) { 
		                    url = "<?php echo site_url("admin")?>/ejec/delete_garantia";
		                    if (request) {
		                        request.abort();
		                    }
		                    request = $.ajax({
		                        url: url,
		                        type: "POST",
		                        data: "gtia_id=" + name

		                    });
		                    window.location.reload(true);
		                    request.done(function (response, textStatus, jqXHR) {
		                        $('#tr' + response).html("");
		                    });
		                    request.fail(function (jqXHR, textStatus, thrown) {
		                        console.log("ERROR: " + textStatus);
		                    });
		                    request.always(function () {
		                        //console.log("termino la ejecuicion de ajax");
		                    });

		                    e.preventDefault();
		                    alertify.success("Se eliminó el registro correctamente");

		                } else {
		                    // user clicked "cancel"
		                    alertify.error("Opcion cancelada");
		                }
		            });
		            return false;
		        });
		    });

		</script>
	</body>
</html>