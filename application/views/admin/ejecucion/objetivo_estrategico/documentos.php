<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

		<title><?php echo $this->session->userdata('name')?></title>
		<meta name="description" content="">
		<meta name="author" content="">
			
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
        <link href="<?php echo base_url(); ?>assets/file/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
        <script src="<?php echo base_url(); ?>assets/file/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/file/js/fileinput.min.js" type="text/javascript"></script>	
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.core.css"/>
    	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css" id="toggleCSS"/>
          <!--fin de stiloh-->
	</head>
	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->

		<!-- HEADER -->
		<header id="header">
			<div id="logo-group">
				<!-- <span id="logo"> <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="SmartAdmin"> </span> -->
			</div>

			<!-- pulled right: nav area -->
			<div class="pull-right">
				<!-- collapse menu button -->
				<div id="hide-menu" class="btn-header pull-right">
					<span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
				</div>
				<!-- end collapse menu -->
				<!-- logout button -->
				<div id="logout" class="btn-header transparent pull-right">
					<span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Sign Out" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
				</div>
				<!-- end logout button -->
				<!-- search mobile button (this is hidden till mobile view port) -->
				<div id="search-mobile" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
				</div>
				<!-- end search mobile button -->
				<!-- fullscreen button -->
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen"><i class="fa fa-arrows-alt"></i></a> </span>
				</div>
				<!-- end fullscreen button -->
			</div>
			<!-- end pulled right: nav area -->
		</header>
		<!-- END HEADER -->

		<!-- Left panel : Navigation area -->
		<aside id="left-panel">
			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
					
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                              <span>
                                  <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                              </span>
						<i class="fa fa-angle-down"></i>
					</a> 
					
				</span>
			</div>
			<nav>
				<ul>
					<li class="">
	                <a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MENÚ PRINCIPAL">
	                <i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
	            	</li>
		            <li class="text-center">
		                <a href='<?php echo site_url("admin").'/ejec/prog'; ?>' title="REGISTRO DE EJECUCION"> <span class="menu-item-parent">REGISTRO DE EJECUCI&Oacute;N</span></a>
		            </li>
				<?php
		                for($i=0;$i<count($enlaces);$i++)
		                {
		                    if(count($subenlaces[$enlaces[$i]['o_child']])>0)
		                    {
		            ?>
		            <li>
		              	<a href="#" >
		              		<i class="<?php echo $enlaces[$i]['o_image']?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
		              	<ul >
		              	<?php
		                foreach ($subenlaces[$enlaces[$i]['o_child']] as $item) {
		                ?>
		                <li><a href="<?php echo base_url($item['o_url']); ?>"><?php echo $item['o_titulo']; ?></a></li>
		                <?php } ?>
		                </ul>
		            </li>
		            <?php 
		            }
		        } ?>
				</ul>
			</nav>
			<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>
		</aside>

		<!-- MAIN PANEL -->
		<div id="main" role="main">

			<!-- RIBBON -->
			<div id="ribbon">
				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>
				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li>Registro de Ejecuci&oacute;n</li><li>Registro de Ejecuci&oacute;n P.E.I.</li><li>Resultados de Mediano Plazo</li><li>Ejecutaci&oacute;n F&iacute;sica</li><li>Anexos</li>
				</ol>
			</div>
			<!-- END RIBBON -->

			<!-- MAIN CONTENT -->
			<div id="content">
				
					<?php
						$attributes = array('class' => 'form-horizontal', 'id' => 'wizard-1','name' =>'wizard-1','enctype' => 'multipart/form-data');
						echo validation_errors();
						echo form_open('admin/ejec/valida_arch_oe', $attributes);
					?>

				<!-- widget grid -->
				<section id="widget-grid" class="">
					<form id="wizard-1" novalidate="novalidate" method="post" enctype="multipart/form-data" >
					<!-- row -->
					<div class="row">
							<!-- end widget -->
						<!-- Widget ID (each widget will need unique ID)-->
					<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<section id="widget-grid" class="well">
						    <div class="">
						        <h1> C&Oacute;DIGO : <small><?php echo $objetivo[0]['obje_codigo']?></small><br>
						        <h1> RESULTADO DE MEDIANO PLAZO : <small><?php echo $objetivo[0]['obje_objetivo']?></small></h1>
						    </div>
						</section>
					</article>


					<article class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<input type="hidden" name="obje_id" id="obje_id" value="<?php echo $objetivo[0]['obje_id']?>"><!-- id proyecto -->
						<input class="form-control"  name="tp_doc" type="hidden" id="tp_doc" >
							
							<div class="jarviswidget jarviswidget-color-darken" >
											<header>
												<span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
												<h2 class="font-md"><strong>ANEXOS A LA EJECUCI&Oacute;N</strong></h2>				
											</header>
										<div class="panel-body">
											<div class="col-sm-12">
												<label class="control-label">Seleccione Documento/Archivo</label>
												<input id="file1" name="file1" type="file" class="file" multiple data-show-upload="false" data-show-caption="true" title="SELECCIONE EL ARCHIVO, DOCUMENTO">
											</div>	
        									<div class="col-sm-12">
												<label class="control-label">Descripci&oacute;n del Documento/Archivo</label>
												<textarea rows="3" class="form-control" name="doc" id="doc" style="width:100%;" title="Descripcion del documento" ></textarea> 
        									</div>
        									<div class="col-sm-12"><hr></div>
        									<div class="col-sm-12">
												<input type="button" name="Submit" value="SUBIR ARCHIVO" id="btsubmit" class="btn btn-success btn-lg" onclick="comprueba_extension(this.form, this.form.file1.value,this.form.doc.value)" style="width:100%;">
        									</div>
        									<div class="col-sm-12"><hr></div>	
										</div>
											<div class="form-actions">
												<a href="<?php echo base_url().'index.php/admin/ejec/obj_est/'.$objetivo[0]['obje_id'].'' ?>" class="btn btn-lg btn-default" title="Atras"> ATRAS </a>
									<a href="<?php echo base_url().'index.php/admin/ejec/redobj' ?>" class="btn btn-lg btn-default" title="Volver a Mis proyectos"> CANCELAR </a>
											</div>
								        </div>					
						</form>
					</article>
					<article class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
						<div class="jarviswidget jarviswidget-color-teal" >
							<header>
								<span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
									<h2 class="font-md"><strong>LISTA DE ARCHIVOS / DOCUMENTOS :
									<?php echo $this->session->userdata("gestion");?></strong></h2>				
							</header>
						<div>
						<div class="widget-body no-padding">
							<div class="table-responsive">
								<table id="dt_basic" class="table table-bordered" font-size: "7px"; style="width:100%;">
									<thead>			                
										<tr>
											<th style="width:1%;"><font size="1">Nro</font></th>
											<th style="width:15%;"><font size="1">DOCUMENTO</font></th>
											<th style="width:5%;"><font size="1">&nbsp;VER&nbsp;</font></th>
											<th style="width:5%;"><font size="1">&nbsp;ELIMINAR&nbsp;</font></th>
										</tr>
									</thead>
									<tbody>
										<?php $num=1;
								        	foreach($archivos as $row)
								            {
								                echo '<tr>';
								                echo '<td><font size="1">'.$num.'</font></td>';
								                echo '<td><font size="1">'.$row['documento'].'</font></td>';
								                                        
								                if(file_exists("archivos/oestrategico_adjuntos/".$row['archivo']))
								                { 
								                    if($row['tip_doc']==1)
								                    {
								                        ?>
									                    <td><center><a href="<?php echo base_url(); ?>archivos/oestrategico_adjuntos/<?php echo $row['archivo'] ?>" target="_blank" title="VER ARCHIVO"><img src="<?php echo base_url(); ?>assets/ifinal/img.jpg" WIDTH="45" HEIGHT="45"/></a></center></td> 
									                    <?php
								                    }
								                    elseif ($row['tip_doc']==2) 
								                    {
								                        ?>
									                    <td><center><a href="<?php echo base_url(); ?>archivos/oestrategico_adjuntos/<?php echo $row['archivo'] ?>" target="_blank" title="VER ARCHIVO"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="45" HEIGHT="45"/></a></center></td> 
									                    <?php
								                    }
								                }
								                else
								                {
								                    ?>
								                        <td><center><a href="#" title=" NO EXISTE EL ARCHIVO"><img src="<?php echo base_url(); ?>assets/img/not.png" WIDTH="45" HEIGHT="45"/></a></center></td> 
								                    <?php
								                }
								                 echo '<td>';
								                    if($this->session->userdata("fun_id")==$row['fun_id'] || $this->session->userdata("rol")==0)
								                    {
								                    ?>
								                        <a href="#" data-toggle="modal" data-target="#modal_del_ff" class="btn btn-xs del_ff" title="ELIMINAR DE REGISTRO METAS" name="<?php echo $row['oe_id']; ?>" id="<?php echo $row['obje_id']; ?>"><img src="<?php echo base_url(); ?>assets/ifinal/eliminar.png" WIDTH="35" HEIGHT="35"/></a>
								                    <?php
								                    }
								                    else 
								                    {
								                    ?>
								                        <center><a href="#" title="NO PUEDE ELIMINAR EL ARCHIVO"><img src="<?php echo base_url(); ?>assets/img/delete.jpg" WIDTH="40" HEIGHT="40"/></a></center>
								                    <?php
								                    }
								                echo '</td>';
								                echo '</tr>';
								                $num=$num+1;
								                }
								                ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</article>
					</div>
				
					<!-- end row -->
				</section>
				<!-- end widget grid -->					
			</div>
			<!-- END MAIN CONTENT -->

		</div>
		<!-- END MAIN PANEL -->

		<!-- PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->

		<!--================================================== -->
		<script language="javascript">
			function comprueba_extension(formulario, archivo,doc) {
			extensiones_permitidas = new Array(".gif", ".jpg", ".docx",".doc",".PNG",".xlsx",".xls",".pdf",".png",".JPEG",".jpeg");
			mierror = "";
			if (!archivo) {
				//Si no tengo archivo, es que no se ha seleccionado un archivo en el formulario
				mierror = "No has seleccionado ningun archivo";
			}else{
				//recupero la extensiÃ³n de este nombre de archivo
				extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();

				if(extension=='.gif' || extension=='.jpg' ||extension=='.jpeg' || extension=='.png' ||extension=='.PNG' || extension=='.JPEG')
				{
					ext=1;
				}
				if(extension=='.pdf')
				{
					ext=2;
				}
				if(extension=='.docx' || extension=='.doc')
				{
					ext=3;
				}
				if(extension=='.xlsx' || extension=='.xls')
				{
					ext=4;
				}
				
				//compruebo si la extensiÃ³n estÃ¡ entre las permitidas
				permitida = false;
				for (var i = 0; i < extensiones_permitidas.length; i++) {
					if (extensiones_permitidas[i] == extension) { 
						permitida = true;
						break;
					}
				}
				if (!permitida) {
					mierror = "Comprueba la extensiÃ³n de los archivos a subir. \nSolo se pueden subir archivos con extensiones: " + extensiones_permitidas.join();
				}else{
					document.getElementById('tp_doc').value=ext;
					if(doc.length==0)
					{
						alert("ingrese el nombre del documento")
						document.formulario.doc.focus() 
		                return 0;
					}
					//submito!
					var OK = confirm("subir archivo ?");

						if (OK) {
							formulario.submit();
							document.getElementById("btsubmit").value = "SUBIENDO ARCHIVO...";
							document.getElementById("btsubmit").disabled = true;
							return true;  
						}
					//formulario.submit();
					return 1;
				}
			}
			//si estoy aqui es que no se ha podido submitir
			alert (mierror);
			return 0;
		}
		</script>
		<script>
			if (!window.jQuery) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-2.0.2.min.js"><\/script>');
			}
		</script>

		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>

		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url();?>/assets/js/app.config.js"></script>
		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url();?>/assets/js/bootstrap/bootstrap.min.js"></script>
		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url();?>/assets/js/notification/SmartNotification.min.js"></script>
		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url();?>/assets/js/smartwidgets/jarvis.widget.min.js"></script>
		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url();?>/assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<!-- SPARKLINES -->
		<script src="<?php echo base_url();?>/assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/select2/select2.min.js"></script>
		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
		<!-- browser msie issue fix -->
		<script src="<?php echo base_url();?>/assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url();?>/assets/js/plugin/fastclick/fastclick.min.js"></script>
		<!-- Demo purpose only -->
		<script src="<?php echo base_url();?>/assets/js/demo.min.js"></script>
		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url();?>/assets/js/app.min.js"></script>
		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<!-- Voice command : plugin -->
		<script src="<?php echo base_url();?>/assets/js/speech/voicecommand.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
		<!-- PAGE RELATED PLUGIN(S) -->
		<script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
		<script src="<?php echo base_url();?>/assets/js/plugin/fuelux/wizard/wizard.min.js"></script>
		<script type="text/javascript">
    $(function () {
        function reset() {
            $("#toggleCSS").attr("href", "<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css");
            alertify.set({
                labels: {
                    ok: "ACEPTAR",
                    cancel: "CANCELAR"
                },
                delay: 5000,
                buttonReverse: false,
                buttonFocus: "ok"
            });
        }

        // =====================================================================
        $(".del_ff").on("click", function (e) {
            reset();
            var name = $(this).attr('name');
            var id = $(this).attr('id'); 
            var request;
            // confirm dialog
            alertify.confirm("REALMENTE DESEA ELIMINAR ESTE REGISTRO?", function (a) {
                if (a) { 
                    url = "<?php echo site_url("admin")?>/ejec/delete";
                    if (request) {
                        request.abort();
                    }
                    request = $.ajax({
                        url: url,
                        type: "POST",
                        data: {
                            oe_id: name,
                            obje_id: id
                        },

                    });
                    window.location.reload(true);
                    request.done(function (response, textStatus, jqXHR) {
                        $('#tr' + response).html("");
                    });
                    request.fail(function (jqXHR, textStatus, thrown) {
                        console.log("ERROR: " + textStatus);
                    });
                    request.always(function () {
                        //console.log("termino la ejecuicion de ajax");
                    });

                    e.preventDefault();
                    alertify.success("Se eliminó el documento correctamente");

                } else {
                    // user clicked "cancel"
                    alertify.error("Opcion cancelada");
                }
            });
            return false;
        });
    });

</script>
	</body>
</html>
