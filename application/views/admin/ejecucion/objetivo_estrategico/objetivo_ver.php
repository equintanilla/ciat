<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

		<title><?php echo $this->session->userdata('name')?></title>
		<meta name="description" content="">
		<meta name="author" content="">
			
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">

		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.core.css"/>
    	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css" id="toggleCSS"/>
        <style type="text/css">
		/*////scroll tablas/////*/
		    .table{
		     display: inline-block;
		     width:100%;
		     overflow-x: scroll;
		     }
		     th {
	         padding: 1.4px;
	         text-align: center;
	         font-size: 9px;
	         color: #ffffff;
	         }
	         td {
	         padding: 1.4px;
	         font-size: 9px;
	        }
		/*///fin de scroll//*/
          </style>
	</head>
	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->

		<!-- HEADER -->
		<header id="header">
			<div id="logo-group">
				<!-- <span id="logo"> <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="SmartAdmin"> </span> -->
			</div>

			<!-- pulled right: nav area -->
			<div class="pull-right">
				
				<!-- collapse menu button -->
				<div id="hide-menu" class="btn-header pull-right">
					<span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
				</div>
				<!-- end collapse menu -->
				<!-- logout button -->
				<div id="logout" class="btn-header transparent pull-right">
					<span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Sign Out" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
				</div>
				<!-- end logout button -->
				<!-- search mobile button (this is hidden till mobile view port) -->
				<div id="search-mobile" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
				</div>
				<!-- end search mobile button -->
				<!-- fullscreen button -->
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen"><i class="fa fa-arrows-alt"></i></a> </span>
				</div>
				<!-- end fullscreen button -->
			</div>
			<!-- end pulled right: nav area -->

		</header>
		<!-- END HEADER -->

		<!-- Left panel : Navigation area -->
		<aside id="left-panel">
			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
					
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                              <span>
                                  <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                              </span>
						<i class="fa fa-angle-down"></i>
					</a> 
					
				</span>
			</div>
			<nav>
				<ul>
					<li class="">
	                <a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MENÚ PRINCIPAL">
	                <i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
	            	</li>
		            <li class="text-center">
		                <a href='<?php echo site_url("admin").'/ejec/redobj'; ?>' title="REGISTRO DE EJECUCION"> <span class="menu-item-parent">REGISTRO DE EJECUCI&Oacute;N</span></a>
		            </li>
				<?php
		                for($i=0;$i<count($enlaces);$i++)
		                {
		                    if(count($subenlaces[$enlaces[$i]['o_child']])>0)
		                    {
		            ?>
		            <li>
		              	<a href="#" >
		              		<i class="<?php echo $enlaces[$i]['o_image']?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
		              	<ul >
		              	<?php
		                foreach ($subenlaces[$enlaces[$i]['o_child']] as $item) {
		                ?>
		                <li><a href="<?php echo base_url($item['o_url']); ?>"><?php echo $item['o_titulo']; ?></a></li>
		                <?php } ?>
		                </ul>
		            </li>
		            <?php 
		            }
		        } ?>
				</ul>
			</nav>
			<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>
		</aside>

		<!-- MAIN PANEL -->
		<div id="main" role="main">

			<!-- RIBBON -->
			<div id="ribbon">

				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>

				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li>Registro de Ejecuci&oacute;n</li><li>Registro de Ejecuci&oacute;n P.E.I.</li><li>Resultados de Mediano Plazo</li><li>Ejecutaci&oacute;n F&iacute;sica (Cerrado)</li>
				</ol>
			</div>
			<!-- END RIBBON -->
			<!-- MAIN CONTENT -->
			<div id="content">
					<?php
						$attributes = array('class' => 'form-horizontal', 'id' => 'formulario','name' =>'formulario','enctype' => 'multipart/form-data');
						echo validation_errors();
						echo form_open('admin/ejec/valida_oe', $attributes);
					?> 
				<div class="well">
					<div class="">
						<div class="col-sm-12">
							<div class="form-group">
							<form id="formulario" name="formulario" novalidate="novalidate" method="post">
								<input type="hidden" name="obje_id" id="obje_id" value="<?php echo $objetivo[0]['obje_id']; ?>"><!-- id obje -->
								<input type="hidden" name="id_g" id="id_g" value="<?php echo $this->session->userdata("gestion"); ?>"><!-- id gestion -->
								<input  type="hidden" name="indi_id" value="<?php echo $oe[0]['indi_id'];?>">
								<input  type="hidden" name="den" value="<?php echo $oe[0]['obje_denominador'];?>">
							
							<label><font size="2"><b>REGISTRO DE EJECUCI&Oacute;N DE RESULTADO DE MEDIANO PLAZO</b></font></label>
							<table class="table table table-bordered">
								<thead>                             
	                                <tr>
	                                	<th style="width:5%;" bgcolor="#3B3B3F"><b>C&Oacute;DIGO</b></th>
	                                    <th style="width:10%;" bgcolor="#3B3B3F"><b>RESULTADO DE MEDIANO PLAZO</b></th>
	                                    <th style="width:3%;" bgcolor="#3B3B3F"><b>TIPO DE INDICADOR</b></th>
	                                    <th style="width:2%;" bgcolor="#3B3B3F"><b>LINEA BASE</b></th>
	                                    <th style="width:2%;" bgcolor="#3B3B3F"><b>META</b></th>
	                                    <th style="width:5%;" bgcolor="#3B3B3F"><b>% PONDERACI&Oacute;N</b></th>
	                                    <th style="width:10%;" bgcolor="#3B3B3F"><b>EJECUCI&Oacute;N OBJETIVO <?php echo $this->session->userdata("gestion");?></b></th>
	                                    <th style="width:2%;" bgcolor="#3B3B3F"><b>ARCHIVOS</b></th>
	                                    <th style="width:3%;" bgcolor="#3B3B3F"><b>OBSERVACI&Oacute;N</b></th>
	                                </tr>
					            </thead>
					                <tbody>
					                <tr>
					                <tr>
	                                	<td><?php echo $objetivo[0]['obje_codigo'] ?></td>
	                                	<td><?php echo $objetivo[0]['obje_objetivo'];?></td>
	                                	<td><?php echo $objetivo[0]['indicador'];?></td>
	                                	<td><?php echo $objetivo[0]['obje_linea_base'];?></td>
	                                	<td><?php echo $objetivo[0]['obje_meta'];?></td>
	                                	<td><?php echo $objetivo[0]['obje_ponderacion'];?>%</td>
	                                	<?php
	                                		if($nro_p!=0){$p=$programado[0]['opm_programado'];}
	                                		else{$p=0;}
	                                		if($nro_e!=0){$e=$ejecutado[0]['oem_ejecutado'];$ea=$ejecutado[0]['oem_ejecutado_a'];$eb=$ejecutado[0]['oem_ejecutado_b'];}
	                                		else{$e=0;$ea=0;$eb=0;}
	                                	?>
	                                	<td>
	                                		<table class="table table table-bordered" border="1">
												<tr bgcolor="#568A89">
													<td><font color="#ffffff"><b>PROGRAMADO: </b></font></td>
													<input type="hidden" name="p" value="<?php echo round($p,1);?>">
													<td><input class="form-control" type="text" value="<?php echo round($p,1);?>" disabled="true"></td>
												</tr>
												<?php 
												if($objetivo[0]['indi_id']==2)
												{
												?>	
												<tr bgcolor="#568A89">
													<td><font color="#ffffff"><b>A-NUMERADOR:</b></font></td>
													<td><input class="form-control" type="text" name="ea" value="<?php echo round($ea,1);?>"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }" disabled="true"></td>
												</tr>
												<tr bgcolor="#568A89">
													<td><font color="#ffffff"><b>B-DENOMINADOR:</b></font></td>
													<td><input class="form-control" type="text" name="eb" value="<?php echo round($eb,1);?>"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }" disabled="true"></td>
												</tr>
													<input class="form-control" type="hidden" name="e" value="<?php echo $e;?>">
												<?php
												}
												elseif($objetivo[0]['indi_id']==1)
												{
												?>	
													<input class="form-control" type="hidden" name="ea" value="<?php echo round($ea,1);?>">
													<input class="form-control" type="hidden" name="eb" value="<?php echo round($eb,1);?>">
												<tr bgcolor="#568A89">
													<td><font color="#ffffff"><b>E : </b></font></td>
													<td><input class="form-control" type="text" name="e" value="<?php echo round($e,1);?>"  onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }" disabled="true"></td>
												</tr>
												<?php
												}
												?>
												</table>
	                                	</td>
	                                	<td>
	                                		<center>
												<a href='<?php echo site_url("admin").'/ejec/archivos_oe/'.$objetivo[0]['obje_id'].''; ?>'title="ARCHIVOS/DOCUMENTOS" disabled="true"><img src="<?php echo base_url(); ?>assets/ifinal/doc.jpg" WIDTH="50" HEIGHT="40"/></a>
											</center>
	                                	</td>
	                                   	<td>
	                                   		<center><a href="#" data-toggle="modal" data-target="#modal_nuevo_ff" class="btn btn-xs nuevo_ff" title="NUEVO DE REGISTRO OBSERVACIONES" disabled="true"><img src="<?php echo base_url(); ?>assets/ifinal/2.png" WIDTH="50" HEIGHT="45"/></a></center>
	                                   	</td>
	                                </tr>
					                </tr>
					                </tbody>
					            </table>
								<?php
					            if(count($observaciones)!=0)
					            {
					            	?>
									<label><font size="1"><b>OBSERVACIONES</b></font></label>
									<table class="table table table-bordered">
										<thead>
											<tr>
												<th style="width:1%;" bgcolor="#3B3B3F">NRO.</th>
												<th style="width:15%;" bgcolor="#3B3B3F">FECHA INICIAL</th>
												<th style="width:15%;" bgcolor="#3B3B3F">FECHA FINAL</th>
												<th style="width:60%;" bgcolor="#3B3B3F">OBSERVACI&Oacute;N</th>
												<th style="width:5%;" bgcolor="#3B3B3F"></th>
											</tr>
										</thead>
										<tbody>
										<?php $nro=1;
		                                    foreach($observaciones  as $row)
		                                    {
		                                    	?>
		                                    	<tr>
		                                    	<td align="center"><?php echo $nro;?></td>
		                                    	<td>
		                                    		<div class="input-group">
														<input type="text" class="form-control datepicker" value="<?php echo date('d/m/Y',strtotime($row['fecha_inicio'])); ?>" data-dateformat="dd/mm/yy" disabled="true">
														<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
													</div>
												</td>
		                                    	<td>
		                                    		<div class="input-group">
														<input type="text" class="form-control datepicker" value="<?php echo date('d/m/Y',strtotime($row['fecha_final'])); ?>" data-dateformat="dd/mm/yy" disabled="true">
														<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
													</div>
		                                    	</td>
		                                    	<td>
		                                    	<textarea rows="3" class="form-control" disabled="true"><?php echo $row['observacion'];?></textarea>
		                                    	</td>
		                                    	<td align="center">
		                                    		<center>
		                                    		<a href="#" data-toggle="modal" data-target="#modal_mod_ff" class="btn btn-xs mod_ff" title="MODIFICAR DE REGISTRO OBSERVACION" name="<?php echo $row['obs_id']; ?>"><img src="<?php echo base_url(); ?>assets/ifinal/modificar.png" WIDTH="30" HEIGHT="30"/></a><br>
		                                    		<a href="#" data-toggle="modal" data-target="#modal_del_ff" class="btn btn-xs del_ff" title="ELIMINAR CONTRATO" name="<?php echo $row['obs_id']; ?>"><img src="<?php echo base_url(); ?>assets/ifinal/eliminar.png" WIDTH="30" HEIGHT="30"/></a>
		                                    		</center>
		                                    	</td>
		                                    	</tr>
		                                    	<?php
		                                    	$nro++;
		                                    }
		                                ?>
		                        		</tbody>
			                        </table>
					            	<?php
					            }
					            ?>
							</div>
						</div>
						<div class="col-sm-12">
							<input class="form-control" type="hidden"  name="oe_id" id="oe_id" value="<?php echo $o_ejec[0]['oe_id'];?>">
							<div class="form-group">
								<label><font size="1"><b>PROBLEMAS</b></font></label>
								<textarea rows="5" class="form-control" name="prob" id="prob" style="width:100%;" disabled="true"><?php echo $o_ejec[0]['ejec_prob'];?></textarea> 
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
								<label><font size="1" ><b>CAUSAS</b></font></label>
								<textarea rows="5" class="form-control" name="causas" id="causas" style="width:100%;" disabled="true"><?php echo $o_ejec[0]['ejec_causas'];?></textarea> 
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
								<label><font size="1" ><b>SOLUCIONES</b></font></label>
								<textarea rows="5" class="form-control" name="sol" id="sol" style="width:100%;" disabled="true"><?php echo $o_ejec[0]['ejec_sol'];?></textarea> 
							</div>
						</div>
												
					</div>
					<input class="form-control" type="hidden" name="bton_reg" id="bton_reg" value="0">
					<input class="form-control" type="hidden"  name="estado" id="estado" value="2">
					<div class="form-actions">
						<a href="<?php echo base_url().'index.php/admin/ejec/redobj' ?>"  class="btn btn-lg btn-default" title="Volver a Mis Proyectos ">ATRAS</a>
						<a href="<?php echo base_url().'index.php/admin/ejec/redobj' ?>" class="btn btn-lg btn-default" title="Volver a Mis proyectos"> CANCELAR </a>

					</div>
				</div>
				
				</form>
			</div>
			<!-- END MAIN CONTENT -->

		</div>

		<!-- PAGE FOOTER -->
		<div class="page-footer"> 
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->
		<script>
			if (!window.jQuery) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-2.0.2.min.js"><\/script>');
			}
		</script>

		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>

		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url();?>/assets/js/app.config.js"></script>
		<script src = "<?php echo base_url(); ?>mis_js/control_session.js"></script>

		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script>
		<SCRIPT src="<?php echo base_url(); ?>mis_js/programacion/ejecucion/abm_ejecucion.js" type="text/javascript"></SCRIPT>
		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.min.js"></script>
		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url(); ?>assets/js/notification/SmartNotification.min.js"></script>
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/select2/select2.min.js"></script>
		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
		<!-- browser msie issue fix -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/fastclick/fastclick.min.js"></script>
		<!-- Demo purpose only -->
		<script src="<?php echo base_url(); ?>assets/js/demo.min.js"></script>
		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url(); ?>assets/js/app.min.js"></script>
		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<!-- Voice command : plugin -->
		<script src="<?php echo base_url(); ?>assets/js/speech/voicecommand.min.js"></script>
		<!--alertas -->
		<script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
		<!-- PAGE RELATED PLUGIN(S) -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
	<!--================= NUEVO FUENTE FINANCIAMIENTO =========================================-->
	</body>
</html>
