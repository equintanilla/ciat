<?php if($this->session->userdata('usuario')){?>
<!DOCTYPE html>
<html lang="en-us" xmlns="http://www.w3.org/1999/html">
<head>
	<meta charset="utf-8">
	<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

	<title> <?php echo $this->session->userdata('name')?> </title>
	<meta name="description" content="">
	<meta name="author" content="">

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

	<!-- Basic Styles -->
	<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">

	<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
	<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css">
	<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">

	<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
	<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">

	<!-- FAVICONS -->
	<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
	<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
	<!--///////////////css-->
	<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
	<!--//////////////fin css-->
	<!--para las alertas-->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.core.css" />
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css" id="toggleCSS" />
	<meta name="viewport" content="width=device-width">


</head>
<body class="">
<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->

<!-- HEADER -->
<header id="header">
	<div id="logo-group" class="col-md-1">
		<span id="logo"> <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="SmartAdmin"> </span>
	</div>
	<div class="col-md-4 " >
	<a href="<?php echo base_url(); ?>index.php/admin/dm/1"><i id="social-fb" class="fa fa-code fa-3x social red-tooltip" rel="tooltip" data-placement="bottom" data-original-title="PROGRAMACI&Oacute;N"></i></a>
	<a href="<?php echo base_url(); ?>index.php/admin/dm/2"><i id="social-tw" class="fa fa-refresh fa-3x social red-tooltip" rel="tooltip" data-placement="bottom" data-original-title="MODIFICACIONES"></i></a>
	<a href="<?php echo base_url(); ?>index.php/admin/dm/3"><i id="social-gp" class="fa fa-pencil-square-o fa-3x social red-tooltip" rel="tooltip" data-placement="bottom" data-original-title="REGISTRO DE EJECUCION"></i></a>
	<a href="#"><i id="social-em" class="fa fa-sitemap fa-3x social red-tooltip" rel="tooltip" data-placement="bottom" data-original-title="GERENCIA DE PROYECTOS"></i></a>
	<a href="#"><i id="social-fb" class="fa fa-globe fa-3x social red-tooltip" rel="tooltip" data-placement="bottom" data-original-title="SIG"></i></a>
	<a href="#"><i id="social-tw" class="fa fa-users fa-3x social red-tooltip" rel="tooltip" data-placement="bottom" data-original-title="CONTROL SOCIAL"></i></a>
	<a href="#"><i id="social-gp" class="fa fa-question-circle fa-3x social red-tooltip" rel="tooltip" data-placement="bottom" data-original-title="CONSULTAS"></i></a>
	<a href="<?php echo base_url(); ?>index.php/admin/dm/7" class="color"><i id="social-em" class="fa fa-file-archive-o fa-3x social red-tooltip" rel="tooltip" data-placement="bottom" data-original-title="REPORTES"></i></a>
	<a href="<?php echo base_url(); ?>index.php/admin/dm/9"><i id="social-em" class="fa fa-cogs fa-3x social red-tooltip" rel="tooltip" data-placement="bottom" data-original-title="MANTENIMIENTO"></i></a>
</div>

	<!-- pulled right: nav area -->
	<div class="pull-right col-md-4">

		<!-- collapse menu button -->
		<div id="hide-menu" class="btn-header pull-right">
			<span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
		</div>
		<!-- end collapse menu -->

		<!-- logout button -->
		<div id="logout" class="btn-header transparent pull-right">
			<span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Sign Out" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
		</div>
		<!-- end logout button -->

		<!-- search mobile button (this is hidden till mobile view port) -->
		<div id="search-mobile" class="btn-header transparent pull-right">
			<span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
		</div>
		<!-- end search mobile button -->

		<!-- fullscreen button -->
		<div id="fullscreen" class="btn-header transparent pull-right">
			<span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen"><i class="fa fa-arrows-alt"></i></a> </span>
		</div>
		<!-- end fullscreen button -->

	</div>
	<!-- end pulled right: nav area -->

</header>
<!-- END HEADER -->

<!-- Left panel : Navigation area -->
<aside id="left-panel">

	<!-- User info -->
	<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is -->

					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
						<img src="<?php echo base_url(); ?>assets/img/avatars/sunny.png" alt="me" class="online" />
						<span>
							<?php echo $this->session->userdata("user_name");?>
						</span>
						<i class="fa fa-angle-down"></i>
					</a>

				</span>
	</div>

	<nav>

		<ul>
			<li class="">
				<a href="<?php echo site_url("admin").'/dashboard'; ?>" title="MENÚ PRINCIPAL"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MENÚ PRINCIPAL</span></a>
			</li>
			<li class="text-center">
				<a href="<?php //echo site_url("admin").'/dashboard'; ?>" title="<?=$titulo?>"> <span class="menu-item-parent"><?=$titulo?></span></a>
			</li>

			<?php
			for($i=0;$i<count($enlaces);$i++)
			{
				if(count($subenlaces[$enlaces[$i]['idchild']])>0)
				{
					?>
					<li>
						<a href="#" >
							<i class="<?php echo $enlaces[$i]['oimage']?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['otitulo']; ?></span></a>
						<ul >
							<?php
							foreach ($subenlaces[$enlaces[$i]['idchild']] as $item) {
								?>
								<li><a href="<?php echo base_url($item['ourl']); ?>"><?php echo $item['otitulo']; ?></a></li>
							<?php } ?>
						</ul>
					</li>
					<?php
				}
			} ?>
		</ul>
	</nav>
	<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>

</aside>

<!-- MAIN PANEL -->
<div id="main" role="main">

	<!-- RIBBON -->
	<div id="">

			
		<!-- breadcrumb -->
		<ol class="breadcrumb">
			<li>Mantenimiento</li><li> Creación de carpeta POA</li>
		</ol>
	</div>
	<!-- END RIBBON -->

	<!-- MAIN CONTENT -->
	<div id="content">
	<div class="row">
					<div class="col-xs-12 col-sm-7 col-md-7 col-lg-12 animated fadeInDown">
						<h1 class="page-title txt-color-blueDark"><i class="fa fa-pencil-square-o fa-fw "></i> LISTA CARPETA POA</h1>
					</div>
	</div>
		

		<div class="row">
			<div class="col-xs-12 col-sm-7 col-md-7 col-lg-12">
				<div class="form-group">
					<a data-toggle="modal" href="#modal_poa_nuevo" class="btn btn-labeled btn-success pull-left nuevo_poa">
						<span class="btn-label"><i class="glyphicon glyphicon-file"></i></span><b>NUEVO</b></a><br><br>	
				</div>
				<div id="respuesta">

				</div>
			</div>
		</div>
		<!-- widget grid -->
		<section id="widget-grid" class="">

			<!-- row -->
			<div class="row">

				<!-- NEW WIDGET START -->
				<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

					<!-- Widget ID (each widget will need unique ID)-->
					<div class="jarviswidget jarviswidget-color-teal" id="wid-id-0" data-widget-editbutton="false">

						<header>
							<span class="widget-icon"> <i class="fa fa-table"></i></span>
						</header>

						<div>
							<div class="widget-body no-padding">
								<div class="table-responsive">
									<table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
										<thead>
										<tr>
											<th> # </th>
											<th>CÓDIGO</th>
											<th>APERTURA PROGRAMÁTICA</th>
											<th>UNIDAD ORGANIZACIONAL</th>
											<th>FECHA DE CREACIÓN</th>
											<th></th>
											
										</tr>
										</thead>
										<tbody id="bdi">
										<?php
										$cont = 1;
										foreach($lista_poa as $row)
										{
											echo '<tr id="tr'.$row['poa_id'].'">';
											echo'<td>'.$cont.'</td>';
											echo '<td><font size="1">'.$row['poa_codigo'].'</font></td>';
											echo '<td><font size="1">'.$row['aper_programa'].$row['aper_proyecto'].$row['aper_actividad']." - ".$row['aper_descripcion'].'</font></td>';
											echo '<td><font size="1">'.$row['uni_unidad'].'</font></td>';
											echo '<td><font size="1">'.$row['poa_fecha_creacion'].'</font></td>';
											?>
											<input type="hidden" name="id_mod" id="id_mod" value="<?php echo $row['poa_id']?>">
											<td>
								<div class="btnapp">
                                                                    <div class="hover-btn">
									<BUTTON  data-toggle="modal" data-target="#modal_mod_poa" class="btn btn-xs botones dos mod_poa"name="<?php echo $row['poa_id']?>" id="enviar_mod">
                                                                            <div class="btn-hover-postion2">
                                                                                <span class="glyphicon glyphicon-pencil"></span>
                                                                            </div>
                                                                        </BUTTON>
                                                                        <button class="btn btn-xs botones uno eliminar_poa" name="<?php echo  $row['poa_id']?>" id="eliminar" > 
                                                                            <div class="btn-hover-postion1">
                                                                                            <span
                                                                                                class="glyphicon glyphicon-trash"></span>
                                                                            </div>
                                                                        </button>
                                                                    </div>
                                                                </div>

											</td>
											
											<?php
											echo '</tr>';
											$cont++;
										}
										?>
										</tbody>
									</table>
								</div>
							</div>
							<!-- end widget content -->
						</div>
						<!-- end widget div -->
					</div>
					<!-- end widget -->
				</article>
				<!-- WIDGET END -->
			</div>
		</section>
		<!-- end widget grid -->

	</div>
	<!-- END MAIN CONTENT -->
	<!-- ================== Modal NUEVO  POA========================== -->
	<div  class="modal animated fadeInDown" id="modal_poa_nuevo" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close text-danger" data-dismiss="modal" aria-hidden="true">
						&times;
					</button>
					<h4 class="modal-title text-center text-info">
						<b><i class="glyphicon glyphicon-pencil"></i> NUEVA CARPETA POA</b>
					</h4>
				</div>
				<div class="modal-body no-padding">
					<div class="row">
						<form id="form_poa" novalidate="novalidate" method="post">
							<div id="bootstrap-wizard-1" class="col-sm-12">
								<div class="well">
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<LABEL><b>Código</b></label>
												<input disabled="disabled" class="form-control" type="text" value="AUTOMÁTICO">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="form-group">
												<LABEL><b>Apertura Programática</b></label>
												<select name="aper_programatica" id="aper_programatica" class="form-control">
													<option value=""> Seleccione </option>
													<?php
													foreach($list_aper as $row){
														echo'<option value="'.$row['aper_id'].'">'.$row['aper_programa'].$row['aper_proyecto'].$row['aper_actividad']." - ".$row['aper_descripcion'].'</option>';
													}
													?>
												</select>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-10">
											<div class="form-group">
												<LABEL><b>Unidad Organizacional</b></label>
												<input disabled="disabled" class="form-control" type="text" id="unidad" name="unidad">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-4">
											<div class="form-group">
											  <label><b>Fecha Creación</b></label>
													<input type="text" value="<?php echo date('d/m/Y')?>" name="poa_fecha" id="poa_fecha" placeholder="Ingrese Fecha" class="form-control datepicker" data-dateformat="dd/mm/yy">
													<span class="input-group-btn"><i class="fa fa-calendar"></i></span>
											</div>
										</div>
									</div>
								</div> <!-- end well -->
							</div>
						</form>
					</div>
				</div>
				<div class="modal-footer">
					<div class="row">
						<div class="col-md-3 pull-left">
							<button  class="btn btn-lg btn-danger" data-dismiss="modal">CANCELAR </button>
						</div>
						<div class="col-md-3 pull-right ">
							<button type="submit" name="enviar_poa" id="enviar_poa" class="btn  btn-lg btn-primary"><i class="fa fa-save"></i>
								GUARDAR
							</button>
						</div>
					</div>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	<!-- ================== Modal MODIFICAR  POA========================== -->
	<div  class="modal animated fadeInDown" id="modal_mod_poa" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close text-danger" data-dismiss="modal" aria-hidden="true">
						&times;
					</button>
					<h4 class="modal-title text-center text-info">
						<b><i class="glyphicon glyphicon-pencil"></i> MODIFICAR CARPETA POA</b>
					</h4>
				</div>
				<div class="modal-body no-padding">
					<div class="row">
						<form id="mod_formpoa" novalidate="novalidate" method="post">
							<div id="bootstrap-wizard-1" class="col-sm-12">
								<div class="well">
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<LABEL><b>Código</b></label>
												<input disabled="disabled" class="form-control" type="text" name="modpoa_codigo" id="modpoa_codigo"
													   placeholder="Ingrese el código">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="form-group">
												<LABEL><b>Apertura Programática</b></label>
												<input class="form-control" disabled="disabled" type="text" name="modpoa_aper" id="modpoa_aper">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-4">
											<div class="form-group">
												<label><b>Fecha Creación</b></label>
												<input type="text" name="modpoa_fecha" id="modpoa_fecha" placeholder="Ingrese Fecha" class="form-control datepicker" data-dateformat="dd/mm/yy">
												<span class="input-group-btn"><i class="fa fa-calendar"></i></span>
											</div>
										</div>
									</div>
								</div> <!-- end well -->
							</div>
						</form>
					</div>
				</div>
				<div class="modal-footer">
					<div class="row">
						<div class="col-md-3 pull-left">
							<button  class="btn btn-lg btn-danger" data-dismiss="modal">CANCELAR </button>
						</div>
						<div class="col-md-3 pull-right ">
							<button type="submit" name="mod_poaenviar" id="mod_poaenviar" class="btn  btn-lg btn-primary"><i class="fa fa-save"></i>
								GUARDAR
							</button>
						</div>
					</div>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
</div>
<!-- END MAIN PANEL -->

<!-- PAGE FOOTER -->
<div class="page-footer">
	<div class="row">
		<div class="col-xs-12 col-sm-6">
			<span class="txt-color-white"><?php echo $this->session->userdata('name')?> © 2013-2014</span>
		</div>
	</div>
</div>
<!-- END PAGE FOOTER -->

<!--================================================== -->

<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
<script data-pace-options='{ "restartOnRequestAfter": true }' src="<?php echo base_url(); ?>assets/js/plugin/pace/pace.min.js"></script>
<script>
	if (!window.jQuery) {
		document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-2.0.2.min.js"><\/script>');
	}
</script>
<script>
	if (!window.jQuery.ui) {
		document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
	}
</script>
<!-- IMPORTANT: APP CONFIG -->
<script src="<?php echo base_url(); ?>assets/js/app.config.js"></script>

<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script>

<!-- BOOTSTRAP JS -->
<script src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.min.js"></script>

<!-- CUSTOM NOTIFICATION -->
<script src="<?php echo base_url(); ?>assets/js/notification/SmartNotification.min.js"></script>

<!-- JQUERY VALIDATE -->
<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>

<!-- JQUERY MASKED INPUT -->
<script src="<?php echo base_url(); ?>assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>

<!-- JQUERY SELECT2 INPUT -->
<script src="<?php echo base_url(); ?>assets/js/plugin/select2/select2.min.js"></script>

<!-- JQUERY UI + Bootstrap Slider -->
<script src="<?php echo base_url(); ?>assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>

<!-- browser msie issue fix -->
<script src="<?php echo base_url(); ?>assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>

<!-- FastClick: For mobile devices -->
<script src="<?php echo base_url(); ?>assets/js/plugin/fastclick/fastclick.min.js"></script>

<!-- Demo purpose only -->
<script src="<?php echo base_url(); ?>assets/js/demo.min.js"></script>

<!-- MAIN APP JS FILE -->
<script src="<?php echo base_url(); ?>assets/js/app.min.js"></script>

<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
<!-- Voice command : plugin -->
<script src="<?php echo base_url(); ?>assets/js/speech/voicecommand.min.js"></script>
<!--alertas -->
<script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
<!-- PAGE RELATED PLUGIN(S) -->
<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
<!--================= NUEVA CARPETA POA =========================================-->
<script type="text/javascript">
	$(function(){
		$('#modal_poa_nuevo').on('hidden.bs.modal', function(){
			document.forms['form_poa'].reset();
		});
		$("#aper_programatica").on("click",function(e){
			//=================== AXTUALIZAR UNIDAD ORGANIZACIONAL MEDIANTE EL SELECT ==================
			var unidad = document.getElementById("aper_programatica").value
			if(unidad.length == 0){
				unidad = 0;
			}
			var url = "<?php echo site_url("admin")?>/mantenimiento/actualiza_unidad";
			$.ajax({
				type:"post",
				url:url,
				data:{unidad:unidad},
				success:function(datos){
					document.getElementById("unidad").value = datos;
				}
			});
		});
	});
</script>
<script type="text/javascript">
	$(function(){
		$("#enviar_poa").on("click",function(e){
			//========================VALIDANDO FORMULARIO===================
			var $validator = $("#form_poa").validate({
				rules: {
					aper_programatica: {
						required: true,
					},
					poa_fecha: {
						required: true,
					}
				},
				messages: {
					aper_programatica: "Seleccione una opcion",
					poa_fecha: {required:"Ingrese la gestión"},
				},
				highlight: function (element) {
					$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
				},
				unhighlight: function (element) {
					$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
				},
				errorElement: 'span',
				errorClass: 'help-block',
				errorPlacement: function (error, element) {
					if (element.parent('.input-group').length) {
						error.insertAfter(element.parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
			var $valid = $("#form_poa").valid();
			if (!$valid) {
				$validator.focusInvalid();
				//return false;
			} else {
				//==========================================================

				var aper_programatica = document.getElementById("aper_programatica").value;
				var poa_fecha = document.getElementById("poa_fecha").value;
				//============= GUARDAR DESPUES DE LA VALIDACION ===============
				var url = "<?php echo site_url("admin")?>/mantenimiento/add_poa";
					$.ajax({
						type:"post",
						url:url,
						data:{aper_programatica:aper_programatica,poa_fecha:poa_fecha},
						success:function(data){
							if(data =='true'){
								window.location.reload(true);
							}else{
								alert(data);
							}
						}
					});


			}
		});
	});
</script>
<!--================= ELIMINACION CARPETA POA =========================================-->
<script type="text/javascript">
	$(function(){
		function reset () {
			$("#toggleCSS").attr("href", "<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css");
			alertify.set({
				labels : {
					ok     : "ACEPTAR",
					cancel : "CANCELAR"
				},
				delay : 5000,
				buttonReverse : false,
				buttonFocus   : "ok"
			});
		}
		// =====================================================================
		$(".eliminar_poa").on("click",function(e){
			reset();
			var name = $(this).attr('name');
			var request;
			// confirm dialog
			alertify.confirm("REALMENTE DESEA ELIMINAR ESTE REGISTRO?", function (a) {
				if (a) {
					url = "<?php echo site_url("admin")?>/mantenimiento/del_poa";
					if(request){
						request.abort();
					}
					request = $.ajax({
						url:url,
						type:"POST",
						data:"poa_id="+name
					});
					request.done(function(response,textStatus,jqXHR){
						//console.log("response: "+response);
						window.location.reload(true);
						//$('#tr'+ response).html("");
					});
					request.fail(function(jqXHR,textStatus,thrown){
						console.log("ERROR: "+ textStatus);
					});
					request.always(function(){
						//console.log("termino la ejecuicion de ajax");
					});
					e.preventDefault();
					alertify.success("Se eliminó el registro correctamente");
				} else {
					// user clicked "cancel"
					alertify.error("Opcion cancelada");
				}
			});
			return false;
		});
	});

</script>
<!--================= MODIFICAR CARPETA POA    =========================================-->
<script type="text/javascript">
	$(function(){
		//limpiar variable
		var id_poa ='';
		$(".mod_poa").on("click",function(e){
			//==========================LLENAR MIS DATOS DE FORMULARIO CON LA CLAVE RECIBIDA
			id_poa = $(this).attr('name');
			var url = "<?php echo site_url("admin")?>/mantenimiento/get_poa";
			var codigo ='';
			var request;
			if(request){
				request.abort();
			}
			request = $.ajax({
				url:url,
				type:"POST",
				dataType:'json',
				data:"id_poa="+id_poa
			});
			request.done(function(response,textStatus,jqXHR){
				document.getElementById("modpoa_codigo").value=response.poa_codigo;
				document.getElementById("modpoa_aper").value=response.aper_descripcion;
				document.getElementById("modpoa_fecha").value=response.fecha;
			});
			request.fail(function(jqXHR,textStatus,thrown){
				console.log("ERROR: "+ textStatus);
			});
			request.always(function(){
				//console.log("termino la ejecuicion de ajax");
			});
			e.preventDefault();
			// =============================VALIDAR EL FORMULARIO DE MODIFICACION
			$("#mod_poaenviar").on("click",function(e){
				var $validator = $("#mod_formpoa").validate({
					rules: {
						modpoa_fecha: {
							required: true,
						}
					},
					messages: {
						modpoa_fecha: {required:"Ingrese la gestión"},
					},
					highlight: function (element) {
						$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
					},
					unhighlight: function (element) {
						$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
					},
					errorElement: 'span',
					errorClass: 'help-block',
					errorPlacement: function (error, element) {
						if (element.parent('.input-group').length) {
							error.insertAfter(element.parent());
						} else {
							error.insertAfter(element);
						}
					}
				});
				var $valid = $("#mod_formpoa").valid();
				if (!$valid) {
					$validator.focusInvalid();
				} else {
					//==========================================================
					var poafecha= document.getElementById("modpoa_fecha").value;
					var url = "<?php echo site_url("admin")?>/mantenimiento/add_poa";
					$.ajax({
						type:"post",
						url:url,
						data:{poa_fecha:poafecha,modificar:id_poa},
						success:function(data){
							window.location.reload(true);
						}
					});
				}
			});
		});
	});
</script>

<script type="text/javascript">
	// TABLA
	$(document).ready(function() {
		pageSetUp();
		/* BASIC ;*/
		var responsiveHelper_dt_basic = undefined;

		var breakpointDefinition = {
			tablet : 1024,
			phone : 480
		};

		$('#dt_basic').dataTable({
			"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
			"t"+
			"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
			"autoWidth" : true,
			"preDrawCallback" : function() {
				// Initialize the responsive datatables helper once.
				if (!responsiveHelper_dt_basic) {
					responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
				}
			},
			"rowCallback" : function(nRow) {
				responsiveHelper_dt_basic.createExpandIcon(nRow);
			},
			"drawCallback" : function(oSettings) {
				responsiveHelper_dt_basic.respond();
			}
		});

		/* END BASIC */
	})
</script>
</body>
</html>
<?php }else{?>
<script>
function redireccionar(){window.location="<?php echo base_url(); ?>index.php/admin/logins";} 
setTimeout ("redireccionar()", 2000);
</script>
<style>
.espacio{height: 40%;}
</style>
<div class="container-fluid imagenn">
<div class="espacio">
</div>
		
		<div class="tour-dos col-md-12">
			<center> <img src="<?php echo base_url(); ?>assets/img/logo.png" class="salir"/></center>
		      <center><h4 class="error-text box animated"><?php echo $this->session->userdata('name')?> -- Cerrar  sesi�n</h4></center>
		     <center> <img src="<?php echo base_url(); ?>assets/img/salir.gif" class="salir"/></center>
		     
		</div>
</div>
<?php 
} ?>