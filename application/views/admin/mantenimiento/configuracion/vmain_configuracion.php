<!DOCTYPE html>
<html lang="en-us">
    <head>
        <meta charset="utf-8">
        <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
        <title><?php echo $this->session->userdata('name')?></title>
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <!-- Basic Styles -->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
        <!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css"> 
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
        <!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
        <!-- FAVICONS -->
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
        <!--estiloh-->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.core.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css" id="toggleCSS" /> 
        <meta name="viewport" content="width=device-width">
          <style>
            th{
            padding: 1.4px;
            text-align: center;
            font-size: 9px;
            }
            td{
            padding: 1.4px;
            font-size: 9px;
            }
        </style>
    </head>
    <body class="">
        <!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->

        <!-- HEADER -->
        <header id="header">
            <div id="logo-group">
            </div>
            <div class="pull-right">
                <div id="hide-menu" class="btn-header pull-right">
                    <span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
                </div>
                <div id="logout" class="btn-header transparent pull-right">
                    <span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="SALIR DEL SISTEMA" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
                </div>
                <div id="search-mobile" class="btn-header transparent pull-right">
                    <span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
                </div>
                <div id="fullscreen" class="btn-header transparent pull-right">
                    <span> <a href="javascript:void(0);" data-action="launchFullscreen" title="EXPANDIR"><i class="fa fa-arrows-alt"></i></a> </span>
                </div>
            </div>
        </header>
        <!-- END HEADER -->
        <!-- Left panel : Navigation area -->
        <aside id="left-panel">
            <!-- User info -->
            <div class="login-info">
                <span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
                    <a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                            <span>
                                <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                            </span>
                        <i class="fa fa-angle-down"></i>
                    </a>
                </span>
            </div>
            <nav>
                <ul>
                    <li class="">
                    <a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MENÚ PRINCIPAL"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
                    </li>
                    <li class="text-center">
                        <a href="#" title="REPORTE GERENCIAL"> <span class="menu-item-parent">MANTENIMIENTO</span></a>
                    </li>
                    <?php echo $menu;?>
                </ul>
            </nav>
            <span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>
        </aside>

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <span class="ribbon-button-alignment"> 
                    <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                        <i class="fa fa-refresh"></i>
                    </span> 
                </span>
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li>Mantenimiento</li><li>Configuraci&oacute;n</li><li>Main Configuraci&oacute;n</li>
                </ol>
            </div>
            <!-- MAIN CONTENT -->
            <div id="content">
                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <div class="row">
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <section id="widget-grid" class="well">
                                <div class="">
                                  <h1>CONFIGURACI&Oacute;N BASE DE DATOS
                                </div>
                            </section>
                        </article>

                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <?php 
                          if($this->session->flashdata('success')){ ?>
                            <div class="alert alert-success">
                              <?php echo $this->session->flashdata('success'); ?>
                            </div>
                        <?php 
                            }
                          elseif($this->session->flashdata('danger')){?>
                            <div class="alert alert-danger">
                                <?php echo $this->session->flashdata('danger'); ?>
                            </div>
                            <?php
                          }
                        ?>
                        </article>

                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="well">
                            <div class="row">
                                <header><h2 class="alert alert-success"><center>CONFIGURACI&Oacute;N <?php echo $this->session->userdata("name");?></center></h2></header>
                                 
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 animated fadeInDown">
                                            <div class="panel panel-darken">
                                                <div class="panel-heading">
                                                    <h4>MES ACTIVA : <?php echo $conf[0]['m_descripcion'];?></h4>
                                                </div>
                                                <div class="panel-body">
                                                    <form name="form_mes" id="form_mes" method="post" action="<?php echo site_url("") . '/mantenimiento/cconfiguracion/update_conf'?>">
                                                    <input type="hidden" name="ide" id="ide" value="<?php echo$conf[0]['ide'] ?>">
                                                    <input type="hidden" name="tp" id="tp" value="2">
                                                    <input type="hidden" name="mes_id1" id="mes_id1" value="<?php echo$conf[0]['conf_mes'] ?>">
                                                    <input type="hidden" name="mes" id="mes" value="<?php echo$conf[0]['m_descripcion'] ?>">
                                                    <div class="form-group">
                                                        <div class="col-md-8">
                                                            <select class="select2" id="mes_id" name="mes_id" title="Meses">
                                                            <?php 
                                                                foreach($mes as $row){
                                                                    if($row['m_id']==$conf[0]['conf_mes']){ ?>
                                                                         <option value="<?php echo $row['m_id']; ?>" selected><?php echo $row['m_descripcion']; ?></option>
                                                                        <?php 
                                                                    }
                                                                    else{ ?>
                                                                         <option value="<?php echo $row['m_id']; ?>"><?php echo $row['m_descripcion']; ?></option>
                                                                        <?php 
                                                                    }   
                                                                }
                                                            ?>        
                                                            </select>
                                                        </div>
                                                        <label class="col-md-4 control-label"><input style="width:100%;" type="button" value="MODIFICAR" id="btsubmit" class="btn btn-primary" onclick="valida_mes()" title="MODIFICAR MES"></label>
                                                    </div>
                                                    </form>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 animated fadeInDown">
                                            <div class="panel panel-darken">
                                                <div class="panel-heading">
                                                    <h4>GESTI&Oacute;N ACTIVA : <?php echo $conf[0]['conf_gestion'];?></h4>
                                                </div>
                                                <div class="panel-body">
                                                    <form name="form_gest" id="form_gest" method="post" action="<?php echo site_url("") . '/mantenimiento/cconfiguracion/update_conf'?>">
                                                    <input type="hidden" name="ide" id="ide" value="<?php echo $conf[0]['ide'] ?>">
                                                    <input type="hidden" name="tp" id="tp" value="1">
                                                    <input type="hidden" name="gest1" id="gest1" value="<?php echo $conf[0]['conf_gestion'] ?>">
                                                    <div class="form-group">
                                                        <div class="col-md-9">
                                                            <select class="select2" id="gest_id" name="gest_id" title="GESTIONES ">
                                                            <?php 
                                                                foreach($gestion as $row){
                                                                    if($row['g_id']==$conf[0]['conf_gestion']){ ?>
                                                                         <option value="<?php echo $row['g_id']; ?>" selected><?php echo $row['g_descripcion']; ?></option>
                                                                        <?php 
                                                                    }
                                                                    else{ ?>
                                                                         <option value="<?php echo $row['g_id']; ?>"><?php echo $row['g_descripcion']; ?></option>
                                                                        <?php 
                                                                    }   
                                                                }
                                                            ?>        
                                                            </select>
                                                        </div>
                                                        <label class="col-md-3 control-label"><input type="button" value="MODIFICAR" id="btsubmit" class="btn btn-primary" onclick="valida_gestion()" title="MODIFICAR GESTION"></label>
                                                    </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 animated fadeInDown">
                                            <div class="panel panel-darken">
                                                <div class="panel-heading">
                                                    <h4>OBJETIVOS ESTRATEGICOS</h4>
                                                </div>
                                                <div class="panel-body">
                                                    <form name="form_obj" id="form_obj" method="post" action="<?php echo site_url("") . '/mantenimiento/cconfiguracion/update_conf'?>" class="form-horizontal">
                                                        <input type="hidden" name="ide" id="ide" value="<?php echo $conf[0]['ide'] ?>">
                                                        <input type="hidden" name="tp" id="tp" value="5">
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">GESTI&Oacute;N INICIO</label>
                                                            <div class="col-md-9">
                                                                <input class="form-control" placeholder="GESTION INICIO" name="gi" id="gi" type="text" value="<?php echo $conf[0]['conf_gestion_desde'] ?>" onkeypress="if (this.value.length < 20) { return soloNumeros(event);}else{return false; }" onpaste="return false">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">GESTI&Oacute;N FINAL</label>
                                                            <div class="col-md-9">
                                                                <input class="form-control" placeholder="GESTION FINAL" name="gf" id="gf" type="text" value="<?php echo $conf[0]['conf_gestion_hasta'] ?>" onkeypress="if (this.value.length < 20) { return soloNumeros(event);}else{return false; }" onpaste="return false">
                                                            </div>
                                                        </div>
                                                        <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <input type="button" value="MODIFICAR" id="btsubmit" class="btn btn-primary" onclick="valida_obj()" title="MODIFICAR GESTIONES OBJETIVOS ESTRATEGICOS">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                        </article>

                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="well">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-5">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 animated fadeInDown">
                                            <div class="panel panel-darken">
                                                <div class="panel-heading">
                                                    <h4>SIGLA ENTIDAD</h4>
                                                </div>
                                                <div class="panel-body">
                                                    <form name="form_sigla" id="form_sigla" method="post" action="<?php echo site_url("") . '/mantenimiento/cconfiguracion/update_conf'?>">
                                                        <input type="hidden" name="ide" id="ide" value="<?php echo $conf[0]['ide'] ?>">
                                                        <input type="hidden" name="tp" id="tp" value="3">
                                                        <div class="col-md-9">
                                                            <input class="form-control" name="sigla" id="sigla" placeholder="SIGLA ENTIDAD" type="text" value="<?php echo $conf[0]['conf_sigla_entidad'];?>">
                                                        </div>
                                                        <label class="col-md-3 control-label"><input type="button" value="MODIFICAR" id="btsubmit" class="btn btn-primary" onclick="valida_sigla()" title="MODIFICAR SIGLA"></label>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-7">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 animated fadeInDown">
                                            <div class="panel panel-darken">
                                                <div class="panel-heading">
                                                    <h4>NOMBRE ENTIDAD</h4>
                                                </div>
                                                <div class="panel-body">
                                                    <form name="form_entidad" id="form_entidad" method="post" action="<?php echo site_url("") . '/mantenimiento/cconfiguracion/update_conf'?>">
                                                        <input type="hidden" name="ide" id="ide" value="<?php echo $conf[0]['ide'] ?>">
                                                        <input type="hidden" name="tp" id="tp" value="4">
                                                        <div class="col-md-10">
                                                            <input class="form-control" name="entidad" id="entidad" placeholder="NOMBRE ENTIDAD" type="text" value="<?php echo $conf[0]['conf_nombre_entidad'];?>">
                                                        </div>
                                                        <label class="col-md-2 control-label"><input type="button" value="MODIFICAR" id="btsubmit" class="btn btn-primary" onclick="valida_entidad()" title="MODIFICAR NOMBRE ENTIDAD"></label>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </article>

                    </div>
                </section>
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->
    </div>
    <!-- ========================================================================================================= -->
        <!-- PAGE FOOTER -->
        <div class="page-footer">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
                </div>
            </div>
        </div>
        <!-- END PAGE FOOTER -->
        <!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
        <script>
            function valida_mes(){ 
                mes_descripcion=document.form_mes.mes.value;
                mesantiguo=document.form_mes.mes_id1.value;
                mesnuevo=document.form_mes.mes_id.value;

                var m = ['ENERO','FEBRERO','MARZO','ABRIL','MAYO','JUNIO','JULIO','AGOSTO','SEPTIEMBRE','OCTUBRE','NOVIEMBRE','DICIEMBRE'];

                if(mesantiguo!=mesnuevo){
                   alertify.confirm('DESEA MODIFICAR '+mes_descripcion+' POR '+m[mesnuevo]+' ?', function (a) {
                        if (a) {
                            //============= GUARDAR DESPUES DE LA VALIDACION ===============
                            document.form_mes.submit();
                        } else {
                            alertify.error("OPCI\u00D3N CANCELADA");
                        }
                    }); 
                }
                else{
                    alertify.success("MES DE "+mes_descripcion+" SE MANTIENE EN LA CONFIGURACION");
                }   
            }

            function valida_gestion(){ 
                gestantiguo=document.form_gest.gest1.value;
                gestnuevo=document.form_gest.gest_id.value;

                if(gestantiguo!=gestnuevo){
                   alertify.confirm('DESEA MODIFICAR LA GESTION '+gestantiguo+' POR '+gestnuevo+' ?', function (a) {
                        if (a) {
                            document.form_gest.submit();
                        } else {
                            alertify.error("OPCI\u00D3N CANCELADA");
                        }
                    }); 
                }
                else{
                    alertify.success("GESTION "+gestantiguo+" SE MANTIENE EN LA CONFIGURACION");
                }
            }

            function valida_sigla(){ 
                if (document.form_sigla.sigla.value==""){ 
                      alertify.alert("REGISTRE SIGLA INSTITUCIONAL") 
                      document.form_sigla.sigla.focus() 
                      return 0; 
                  }

                alertify.confirm('MODIFICAR SIGLA INSTITUCIONAL ?', function (a) {
                        if (a) {
                            document.form_sigla.submit();
                        } else {
                            alertify.error("OPCI\u00D3N CANCELADA");
                        }
                    }); 
            }

            function valida_entidad(){ 
                if (document.form_entidad.entidad.value==""){ 
                      alertify.alert("REGISTRE NOMBRE DE LA INSTITUCION") 
                      document.form_entidad.entidad.focus() 
                      return 0; 
                  }

                alertify.confirm('MODIFICAR NOMBRE DE LA ENTIDAD ?', function (a) {
                        if (a) {
                            document.form_entidad.submit();
                        } else {
                            alertify.error("OPCI\u00D3N CANCELADA");
                        }
                    }); 
            }

            function valida_obj(){ 
                if (document.form_obj.gi.value==""){ 
                      alertify.alert("REGISTRE GESTION INICIO") 
                      document.form_obj.gi.focus() 
                      return 0; 
                  }

                  if (document.form_obj.gf.value==""){ 
                      alertify.alert("REGISTRE GESTION FINAL") 
                      document.form_obj.gf.focus() 
                      return 0; 
                  }

                 // alert(document.form_obj.gf.value+'--'+document.form_obj.gi.value)
                  if(document.form_obj.gf.value<document.form_obj.gi.value){
                        alertify.error("LA GESTION INICIAL NO PUEDE SER POSTERIOR A LA GESTION FINAL");
                        document.form_obj.gi.focus() 
                      return 0; 
                  }

                  if((document.form_obj.gf.value-document.form_obj.gi.value)==4){
                        alertify.confirm('MODIFICAR GESTIONES DEL OBJETIVO ESTRATEGICO ?', function (a) {
                        if (a) {
                            document.form_obj.submit();
                        } else {
                            alertify.error("OPCI\u00D3N CANCELADA");
                        }
                    }); 
                  }
                  else{
                     alertify.error("LA DIFERENCIA DE GESTIONES DEBE SER DE 5 AÑOS, VERIFIQUE GESTIONES");
                        document.form_obj.gf.focus() 
                      return 0;
                  }
            }
        </script>
        <script data-pace-options='{ "restartOnRequestAfter": true }' src="<?php echo base_url(); ?>assets/js/plugin/pace/pace.min.js"></script>
        <script>
            if (!window.jQuery) {
                document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-2.0.2.min.js"><\/script>');
            }
        </script>

        <script>
            if (!window.jQuery.ui) {
                document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
            }
        </script>

        <!-- IMPORTANT: APP CONFIG -->
        <script src="<?php echo base_url(); ?>assets/js/session_time/jquery-idletimer.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/app.config.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/mis_js/validacion_form.js"></script>
        <!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
        <script src="<?php echo base_url(); ?>assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
        <!-- BOOTSTRAP JS -->
        <script src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.min.js"></script>
        <!-- CUSTOM NOTIFICATION -->
        <script src="<?php echo base_url(); ?>assets/js/notification/SmartNotification.min.js"></script>
        <!-- JARVIS WIDGETS -->
        <script src="<?php echo base_url(); ?>assets/js/smartwidgets/jarvis.widget.min.js"></script>
        <!-- EASY PIE CHARTS -->
        <script src="<?php echo base_url(); ?>assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
        <!-- SPARKLINES -->
        <script src="<?php echo base_url(); ?>assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
        <!-- JQUERY VALIDATE -->
        <script src="<?php echo base_url(); ?>assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
        <!-- JQUERY MASKED INPUT -->
        <script src="<?php echo base_url(); ?>assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
        <!-- JQUERY SELECT2 INPUT -->
        <script src="<?php echo base_url(); ?>assets/js/plugin/select2/select2.min.js"></script>
        <!-- JQUERY UI + Bootstrap Slider -->
        <script src="<?php echo base_url(); ?>assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
        <!-- browser msie issue fix -->
        <script src="<?php echo base_url(); ?>assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
        <!-- FastClick: For mobile devices -->
        <script src="<?php echo base_url(); ?>assets/js/plugin/fastclick/fastclick.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
        <!-- Demo purpose only -->
        <script src="<?php echo base_url(); ?>assets/js/demo.min.js"></script>
        <!-- MAIN APP JS FILE -->
        <script src="<?php echo base_url(); ?>assets/js/app.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/plugin/datatables/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
        <script type="text/javascript">
            $(function () {
                function reset() {
                    $("#toggleCSS").attr("href", "<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css");
                    alertify.set({
                        labels: {
                            ok: "ACEPTAR",
                            cancel: "CANCELAR"
                        },
                        delay: 5000,
                        buttonReverse: false,
                        buttonFocus: "ok"
                    });
                }

                // =====================================================================
                $(".act_ff").on("click", function (e) {
                    reset();
                    var name = $(this).attr('name');
                    var id = $(this).attr('id');
                    if(id==0){
                        tit="DESEA INACTIVAR AL RESPONSABLE ?";
                    }
                    if(id==1){
                        tit="DESEA ACTIVAR AL RESPONSABLE ?";
                    }
                    if(id==3){
                        tit="DESEA ELIMINAR AL RESPONSABLE ?";
                    }
                    var request;
                    // confirm dialog
                    alertify.confirm(tit, function (a) {
                        if (a) { 
                            url = "<?php echo site_url("")?>/funcionario/activar";
                            if (request) {
                                request.abort();
                            }
                            request = $.ajax({
                                url: url,
                                type: "POST",
                                dataType: "json",
                                data: "fun_id="+name+"&tp="+id

                            });

                            request.done(function (response, textStatus, jqXHR) { 
                                reset();
                                if (response.respuesta == 'correcto') {
                                    if(id==0){
                                        resp="SE ACTIVO AL RESPONSABLE ";
                                    }
                                    if(id==1){
                                        resp="SE INACTIVO AL RESPONSABLE ";
                                    }
                                    if(id==3){
                                        resp="SE ELIMINO CORRECTAMENTE AL RESPONSABLE ";
                                    }
                                    alertify.alert(resp, function (e) {
                                        if (e) {
                                            window.location.reload(true);
                                        }
                                    });
                                } else {
                                    alertify.alert("ERROR !!!", function (e) {
                                        if (e) {
                                            window.location.reload(true);
                                        }
                                    });
                                }
                            });
                            request.fail(function (jqXHR, textStatus, thrown) {
                                console.log("ERROR: " + textStatus);
                            });
                            request.always(function () {
                                //console.log("termino la ejecuicion de ajax");
                            });

                            e.preventDefault();

                        } else {
                            // user clicked "cancel"
                            alertify.error("Opcion cancelada");
                        }
                    });
                    return false;
                });

            });
        </script>
        <script type="text/javascript">
          $(document).ready(function() {
              pageSetUp();
              /* BASIC ;*/
              var responsiveHelper_dt_basic = undefined;
              var responsiveHelper_datatable_fixed_column = undefined;
              var responsiveHelper_datatable_col_reorder = undefined;
              var responsiveHelper_datatable_tabletools = undefined;
              
              var breakpointDefinition = {
                   tablet : 1024,
                   phone : 480
              };

              $('#dt_basic').dataTable({
                   "ordering": false,
                   "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
                        "t"+
                        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                   "autoWidth" : true,
                   "preDrawCallback" : function() {
                        // Initialize the responsive datatables helper once.
                        if (!responsiveHelper_dt_basic) {
                             responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                        }
                   },
                   "rowCallback" : function(nRow) {
                        responsiveHelper_dt_basic.createExpandIcon(nRow);
                   },
                   "drawCallback" : function(oSettings) {
                        responsiveHelper_dt_basic.respond();
                   }
              });

          })
      </script>
    </body>
</html>
