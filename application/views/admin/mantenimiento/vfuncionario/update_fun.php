<!DOCTYPE html>
<html lang="en-us">
    <head>
        <meta charset="utf-8">
        <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
        <title><?php echo $this->session->userdata('name')?></title>
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <!-- Basic Styles -->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
        <!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css"> 
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
        <!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
        <!-- FAVICONS -->
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
        <!--estiloh-->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.core.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css" id="toggleCSS" />
        <script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>  
        <meta name="viewport" content="width=device-width">
    </head>
    <body class="">
        <!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->
        <!-- HEADER -->
        <header id="header">
            <div id="logo-group">
            </div>
            <div class="pull-right">
                <div id="hide-menu" class="btn-header pull-right">
                    <span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
                </div>
                <div id="logout" class="btn-header transparent pull-right">
                    <span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Sign Out" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
                </div>
                <div id="search-mobile" class="btn-header transparent pull-right">
                    <span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
                </div>
                <div id="fullscreen" class="btn-header transparent pull-right">
                    <span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen"><i class="fa fa-arrows-alt"></i></a> </span>
                </div>
            </div>
        </header>
        <!-- END HEADER -->
        <!-- Left panel : Navigation area -->
        <aside id="left-panel">
            <!-- User info -->
            <div class="login-info">
                <span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
                    <a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                            <span>
                                <i class="fa fa-user" aria-hidden="true"></i><?php echo $this->session->userdata("user_name");?>
                            </span>
                        <i class="fa fa-angle-down"></i>
                    </a>
                </span>
            </div>
            <nav>
                <ul>
                    <li class="">
                    <a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MENÚ PRINCIPAL"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
                    </li>
                    <li class="text-center">
                        <a href="#" title="REPORTE GERENCIAL"> <span class="menu-item-parent">MANTENIMIENTO</span></a>
                    </li>
                    <?php echo $menu;?>
                </ul>
            </nav>
            <span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>
        </aside>

        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <span class="ribbon-button-alignment"> 
                    <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                        <i class="fa fa-refresh"></i>
                    </span> 
                </span>
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li>Mantenimiento</li><li>Funcionarios</li><li>Modificar Registro</li>
                </ol>
            </div>
            <!-- MAIN CONTENT -->
            <div id="content">
                <!-- widget grid -->
                <section id="widget-grid" class="">
                    <div class="row">
                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <section id="widget-grid" class="well">
                                <div class="">
                                  <h1> REGISTRO DE USUARIOS RESPONSABLES : <?php echo $this->session->userdata('name')?> </h1>
                                </div>
                            </section>
                        </article>
                        <article class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                        </article>
                        <article class="col-sm-12 col-md-8 col-lg-8">
                            <?php 
                              if($this->session->flashdata('danger'))
                                { ?>
                                <div class="alert alert-danger">
                                  <?php echo $this->session->flashdata('danger'); ?>
                                </div>
                            <?php 
                                }
                            ?>
                            <!-- Widget ID (each widget will need unique ID)-->
                                <div class="jarviswidget" id="wid-id-8" data-widget-editbutton="false" data-widget-custombutton="false">
                                <header>
                                    <h2></h2> 
                                </header>
                                <!-- widget div-->
                                <div>
                                    <!-- widget edit box -->
                                    <div class="">
                                        <!-- This area used as dropdown edit box -->
                                    </div>
                                    <div class="widget-body no-padding">
                                        <form action="<?php echo site_url("admin").'/funcionario/add_update_fun' ?>" method="post" id="resp_form" name="resp_form" class="smart-form">
                                            <input class="form-control" type="hidden" name="fun_id"  value="<?php echo $fun[0]['fun_id']?>">
                                            <?php
                                                if($fun[0]['fun_estado']==1 || $fun[0]['fun_estado']==2){
                                                    ?>
                                                    <header><h2 class="alert alert-success"><center>RESPONSABLE : <?php echo $this->session->userdata('name')?> (Activo)</center></h2></header>
                                                    <?php
                                                }
                                                elseif($fun[0]['fun_estado']==0){
                                                    ?>
                                                    <header><h2 class="alert alert-danger"><center>RESPONSABLE : <?php echo $this->session->userdata('name')?> (Inactivo)</center></h2></header>
                                                    <?php
                                                }
                                            ?>
                                            
                                            <fieldset>
                                                <div class="well">
                                                    <div class="row">
                                                        <section class="col col-4">
                                                            <label class="label">NOMBRE COMPLETO</label>
                                                            <input class="form-control" type="text" name="nombre" id="nombre" maxlength="50" value="<?php echo $fun[0]['fun_nombre']?>" onkeyup="javascript:fun_usuario();">
                                                        </section>
                                                        <section class="col col-4">
                                                            <label class="label">PRIMER APELLIDO</label>
                                                            <input class="form-control" type="text" name="ap" id="ap" maxlength="50" value="<?php echo $fun[0]['fun_paterno']?>" onkeyup="javascript:fun_usuario();">
                                                        </section> 
                                                        <section class="col col-4">
                                                            <label class="label">SEGUNDO APELLIDO</label>
                                                            <input class="form-control" type="text" name="am" id="am" maxlength="50" value="<?php echo $fun[0]['fun_materno']?>">
                                                        </section>
                                                    </div>   
                                                </div><br>

                                                <div class="well">
                                                    <div class="row">
                                                        <section class="col col-4">
                                                            <label class="label">CARNET</label>
                                                            <input type="hidden" name="ci1" id="ci1" value="<?php echo $fun[0]['fun_ci']?>">
                                                            <input class="form-control" type="text" name="ci" id="ci" value="<?php echo $fun[0]['fun_ci']?>" onblur="javascript:fun_ci();"  onkeypress="if (this.value.length < 10) { return soloNumeros(event);}else{return false; }"  onpaste="return false">
                                                        </section>
                                                        <section class="col col-4">
                                                            <label class="label">TELEFONO / CELULAR</label>
                                                            <input class="form-control" type="text" name="fono" id="fono" value="<?php echo $fun[0]['fun_telefono']?>" onkeypress="if (this.value.length < 10) { return soloNumeros(event);}else{return false; }"  onpaste="return false">
                                                        </section> 
                                                        <section class="col col-4">
                                                            <label class="label">CARGO ADMINISTRATIVO</label>
                                                            <input class="form-control" type="text" name="crgo" id="crgo" value="<?php echo $fun[0]['fun_cargo']?>" maxlength="50">
                                                        </section>
                                                    </div>   
                                                </div><br>

                                                <div class="well">
                                                    <div class="row">
                                                        <section class="col col-5">
                                                            <label class="label">DOMICILIO</label>
                                                            <input class="form-control" type="text" name="domicilio" id="domicilio" value="<?php echo $fun[0]['fun_domicilio']?>" maxlength="100">
                                                        </section>
                                                        <section class="col col-3">
                                                            <label class="label"><font color="blue" size="1">USUARIO</font></label>
                                                            <input type="hidden" name="usuario1" value="<?php echo $fun[0]['fun_usuario']?>">
                                                            <input class="form-control" type="text" name="usuario" id="usuario" placeholder="Usuario" value="<?php echo $fun[0]['fun_usuario']?>" style="width:100%;" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();" maxlength="15">
                                                        </section>
                                                        <section class="col col-2">
                                                            <label class="label"><font color="blue" size="1">PASSWORD</font></label>
                                                            <input class="form-control" type="password" name="password" id="password" placeholder="Password" value="<?php echo $edit_pass ?>" maxlength="6" onblur="javascript:verif_password();">
                                                        </section>
                                                        <section class="col col-2">
                                                            <label class="label"><font color="blue" size="1">REPETIR PASSWORD</font></label>
                                                            <input class="form-control" type="password" name="password2" id="password2" placeholder="Password" value="<?php echo $edit_pass ?>" maxlength="6" onblur="javascript:verif_password();">
                                                            <div id="pas"></div>
                                                        </section>
                                                    </div>    
                                                </div><br>

                                                <div class="well">
                                                    <div class="row">
                                                        <section class="col col-6">
                                                        <label class="label">ESCALA SALARIAL</label>
                                                            <label class="input">
                                                                <select class="select2" id="esc_sal" name="esc_sal" title="Seleccione Unidad Organizacional">
                                                                    <option value="">Seleccione</option>
                                                                    <?php 
                                                                        foreach($list_escala as $row)
                                                                        {
                                                                            if($row['car_id']==$fun[0]['car_id']){
                                                                                ?>
                                                                                <option value="<?php echo $row['car_id']; ?>" selected="true"><?php echo $row['car_cargo']; ?></option>
                                                                                <?php
                                                                            }
                                                                            else{
                                                                                ?>
                                                                                <option value="<?php echo $row['car_id']; ?>"><?php echo $row['car_cargo']; ?></option>
                                                                                <?php
                                                                            }  
                                                                        }
                                                                    ?>        
                                                                </select> 
                                                            </label>
                                                        </section>
                                                        <section class="col col-6">
                                                        <label class="label">UNIDAD ORGANIZACIONAL</label>
                                                            <label class="input">
                                                                <select class="select2" id="uni_id" name="uni_id" title="Seleccione Unidad Organizacional">
                                                                    <option value="">Seleccione</option>
                                                                    <?php 
                                                                        foreach($uni_org as $row)
                                                                        {
                                                                            if($row['uni_id']==$fun[0]['uni_id']){
                                                                                ?>
                                                                                <option value="<?php echo $row['uni_id']; ?>" selected="true"><?php echo $row['uni_unidad']; ?></option>
                                                                                <?php
                                                                            }
                                                                            else{
                                                                                ?>
                                                                                <option value="<?php echo $row['uni_id']; ?>"><?php echo $row['uni_unidad']; ?></option>
                                                                                <?php
                                                                            }  
                                                                        }
                                                                    ?>        
                                                                </select> 
                                                            </label>
                                                        </section>
                                                    </div>
                                                    <div class="row">
                                                        <section class="col col-4">
                                                        </section>
                                                        <section class="col col-6">
                                                            <label class="label">SELECCIONE ROL : <b id="titulo"></b></label>
                                                                <div class="row">
                                                                    <div class="col col-2"></div>
                                                                    <div class="col col-10">
                                                                        <?php
                                                                        if($fun[0]['fun_id']==0){
                                                                            ?>
                                                                            <div class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" checked required="true" disabled="true">
                                                                                    <i></i>ADMINISTRADOR
                                                                                </label>
                                                                            </div>
                                                                            <?php  
                                                                        }
                                                                        else{
                                                                        $c = 1; 
                                                                            foreach ($listas_rol as $row) {
                                                                                $rol=$this->model_funcionario->verif_rol($fun[0]['fun_id'],$row['r_id']);
                                                                                if(count($rol)!=0){
                                                                                   if($row['r_id']==1){
                                                                                    ?>
                                                                                    <div class="checkbox">
                                                                                        <label>
                                                                                            <input name="rol<?php echo $row['r_id']?>" id="rol<?php echo $row['r_id']?>" type="checkbox" value="<?php echo $row['r_id']?>" checked class="checar1">
                                                                                            <i></i><?php echo $row['r_nombre'];?>
                                                                                        </label>
                                                                                    </div>
                                                                                    <?php
                                                                                   }
                                                                                   else{
                                                                                    ?>
                                                                                    <div class="checkbox">
                                                                                        <label>
                                                                                            <input name="rol<?php echo $row['r_id']?>" id="rol<?php echo $row['r_id']?>" type="checkbox" value="<?php echo $row['r_id']?>" checked class="checar">
                                                                                            <i></i><?php echo $row['r_nombre'];?>
                                                                                        </label>
                                                                                    </div>
                                                                                    <?php
                                                                                   }
                                                                                }
                                                                                else{
                                                                                    if($row['r_id']==1){
                                                                                        ?>
                                                                                        <div class="checkbox">
                                                                                            <label>
                                                                                                <input name="rol<?php echo $row['r_id']?>" id="rol<?php echo $row['r_id']?>" type="checkbox" value="<?php echo $row['r_id']?>" class="checar1">
                                                                                                <i></i><?php echo $row['r_nombre'];?>
                                                                                            </label>
                                                                                        </div>
                                                                                        <?php
                                                                                    }
                                                                                    else{
                                                                                        ?>
                                                                                        <div class="checkbox">
                                                                                            <label>
                                                                                                <input name="rol<?php echo $row['r_id']?>" id="rol<?php echo $row['r_id']?>" type="checkbox" value="<?php echo $row['r_id']?>" class="checar">
                                                                                                <i></i><?php echo $row['r_nombre'];?>
                                                                                            </label>
                                                                                        </div>
                                                                                        <?php
                                                                                    }
                                                                                } 
                                                                            }       
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                        </section>
                                                    </div>
                                                </div>
                                            </fieldset>

                                            <footer>
                                                
                                                <?php
                                                if($this->input->post('fun_id')==0){
                                                    ?>
                                                    <input type="button" value="GUARDAR" id="btsubmit" class="btn btn-primary" onclick="valida_envia()" title="MODIFICAR RESPONSABLE">
                                                    <?php
                                                }
                                                else{
                                                    ?>
                                                    <input type="button" value="GUARDAR" id="btsubmit" class="btn btn-primary" onclick="valida_envia()" title="MODIFICAR RESPONSABLE" disabled="true">
                                                    <?php
                                                }
                                                ?>
                                                <a href="<?php echo base_url().'index.php/admin/mnt/list_usu'; ?>" class="btn btn-default" title="MODIFICAR INFORMACION"> CANCELAR </a>                                          
                                            </footer>
                                        </form>   
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </section>
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->
    </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script>
            // Si se hace click sobre el input de tipo checkbox con id checkb
            $('#rol1').click(function() {
                // Si esta seleccionado (si la propiedad checked es igual a true)
                if ($(this).prop('checked')) {
                    // Selecciona cada input que tenga la clase .checar
                    $('.checar').prop('checked', false);
                }
            });

            var paso;
            for (paso = 2; paso <=6; paso++) {
                $('#rol'+paso).click(function() {
                    // Si esta seleccionado (si la propiedad checked es igual a true)
                    if ($(this).prop('checked')) {
                        // Selecciona cada input que tenga la clase .checar
                        $('.checar1').prop('checked', false);
                    }
                });
            };
        </script>

        <script type="text/javascript">
        function fun_usuario()
        { 
            a = $('[id="nombre"]').val();
            b = $('[id="ap"]').val();
            c = $('[id="am"]').val();
            
            $('[id="usuario"]').val((a[0]+'.'+b) );
        }

        function verif_password()
        { 
            a = $('[id="password"]').val();
            b = $('[id="password2"]').val();
            
            if(a!=b){
                $('#pas').html('<font color=red> INCORRECTOS </font>');
                document.resp_form.password2.focus() 
                return 0; 
            }
            else{
                $('#pas').html('');
            }
            
         
        }

        function fun_ci()
        { 
            ci = $('[id="ci"]').val();
            ci1 = $('[id="ci1"]').val();
            usuario = $('[id="usuario"]').val();
            
            if(ci1!=ci){
                var url = "<?php echo site_url("admin")?>/funcionario/verif_ci";
                    $.ajax({
                        type:"post",
                        url:url,
                        data:{ci:ci},
                        success:function(datos){

                            if(datos.trim() =='false'){
                                alertify.error('LA CEDULA DE IDENTIDAD YA SE ENCUENTRA REGISTRADO !!!')
                                document.resp_form.ci.focus() 
                                return 0; 
                            }
                        }});
            }
            
        }
        </script>
    <!-- ========================================================================================================= -->
        <!-- PAGE FOOTER -->
        <div class="page-footer">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
                </div>
            </div>
        </div>
        <!-- END PAGE FOOTER -->
        <!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
        <script data-pace-options='{ "restartOnRequestAfter": true }' src="<?php echo base_url(); ?>assets/js/plugin/pace/pace.min.js"></script>
        <script>
            if (!window.jQuery) {
                document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-2.0.2.min.js"><\/script>');
            }
        </script>

        <script>
            if (!window.jQuery.ui) {
                document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
            }
        </script>

        <!-- IMPORTANT: APP CONFIG -->
        <script src="<?php echo base_url(); ?>assets/js/session_time/jquery-idletimer.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/app.config.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/mis_js/validacion_form.js"></script>
        <script src="<?php echo base_url(); ?>mis_js/mantenimiento/funcionario.js"></script>
        <!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
        <script src="<?php echo base_url(); ?>assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
        <!-- BOOTSTRAP JS -->
        <script src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.min.js"></script>
        <!-- EASY PIE CHARTS -->
        <script src="<?php echo base_url(); ?>assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
        <!-- SPARKLINES -->
        <script src="<?php echo base_url(); ?>assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
        <!-- JQUERY VALIDATE -->
        <script src="<?php echo base_url(); ?>assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
        <!-- JQUERY MASKED INPUT -->
        <script src="<?php echo base_url(); ?>assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
        <!-- JQUERY SELECT2 INPUT -->
        <script src="<?php echo base_url(); ?>assets/js/plugin/select2/select2.min.js"></script>
        <!-- FastClick: For mobile devices -->
        <script src="<?php echo base_url(); ?>assets/js/plugin/fastclick/fastclick.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
        <!-- Demo purpose only -->
        <script src="<?php echo base_url(); ?>assets/js/demo.min.js"></script>
        <!-- MAIN APP JS FILE -->
        <script src="<?php echo base_url(); ?>assets/js/app.min.js"></script>
        <script type="text/javascript">
        function valida_envia()
        { 
            if (document.resp_form.nombre.value==""){ 
                alertify.alert("REGISTRE NOMBRE DEL RESPONSABLE") 
                document.resp_form.nombre.focus() 
                return 0; 
            }

            if (document.resp_form.ap.value==""){ 
                alertify.alert("REGISTRE PRIMER APELLIDO") 
                document.resp_form.prog.focus() 
                return 0; 
            }

            if (document.resp_form.ci.value==""){ 
                alertify.alert("REGISTRE CEDULA DE IDENTIDAD") 
                document.resp_form.ci.focus() 
                return 0; 
            }

            if (document.resp_form.crgo.value==""){ 
                alertify.alert("REGISTRE CARGO ADMINISTRATIVO") 
                document.resp_form.crgo.focus() 
                return 0; 
            }

            if (document.resp_form.usuario.value==""){ 
                alertify.alert("REGISTRE USUARIO") 
                document.resp_form.usuario.focus() 
                return 0; 
            }

            if (document.resp_form.password.value==""){ 
                alertify.alert("REGISTRE PASSWORD") 
                document.resp_form.password.focus() 
                return 0; 
            }

            if (document.resp_form.esc_sal.value==""){
                alertify.alert("SELECCIONE LA ESCALA SALARIAL") 
                document.resp_form.esc_sal.focus() 
                return 0;
            }

            if (document.resp_form.uni_id.value==""){
                alertify.alert("SELECCIONE UNIDAD ORGANIZACIONAL") 
                document.resp_form.uni_id.focus() 
                return 0;
            }

            if (document.resp_form.password.value!=document.resp_form.password2.value){
                alertify.error("POR FAVOR VERIFIQUE LAS CONSTRASEÑAS") 
                document.resp_form.password2.focus() 
                return 0;
            }

            
            if(document.resp_form.fun_id.value!=0){
               if(document.getElementById('rol1').checked==false && document.getElementById('rol2').checked==false && document.getElementById('rol3').checked==false && document.getElementById('rol4').checked==false && document.getElementById('rol5').checked==false&& document.getElementById('rol6').checked==false&& document.getElementById('rol10').checked==false)
                {
                    alertify.alert('SELECCIONE EL TIPO DE ROL')
                    document.resp_form.rol1.focus() 
                    return 0; 
                } 
            }
            
            usuario1=document.resp_form.usuario1.value.trim();
            usuario=document.resp_form.usuario.value.trim();
            ci1=document.resp_form.ci1.value;
            ci=document.resp_form.ci.value;
            if(usuario1!=usuario){
                var url = "<?php echo site_url("")?>/funcionario/verif_usuario";
                $.ajax({
                    type:"post",
                    url:url,
                    data:{user:usuario},
                    success:function(datos){
                        
                        if(datos.trim() =='true'){

                            if(ci1!=ci){
                                
                                var url = "<?php echo site_url("admin")?>/funcionario/verif_ci";
                                $.ajax({
                                    type:"post",
                                    url:url,
                                    data:{ci:ci},
                                    success:function(datos){

                                        if(datos.trim() =='false'){
                                            alertify.error('LA CEDULA DE IDENTIDAD YA SE ENCUENTRA REGISTRADO !!!')
                                            document.resp_form.ci.focus() 
                                            return 0; 
                                        }
                                        else{
                                            alertify.confirm("GUARDAR RESPONSABLE ?", function (a) {
                                                if (a) {
                                                    //============= GUARDAR DESPUES DE LA VALIDACION ===============
                                                    document.resp_form.submit();
                                                    document.getElementById("btsubmit").value = "MODIFICANDO...";
                                                    document.getElementById("btsubmit").disabled = true;
                                                } else {
                                                    alertify.error("OPCI\u00D3N CANCELADA");
                                                }
                                            });
                                        }

                                    }});

                            }
                            else{
                                alertify.confirm("GUARDAR RESPONSABLE ?", function (a) {
                                    if (a) {
                                        //============= GUARDAR DESPUES DE LA VALIDACION ===============
                                        document.resp_form.submit();
                                        document.getElementById("btsubmit").value = "MODIFICANDO...";
                                        document.getElementById("btsubmit").disabled = true;
                                    } else {
                                        alertify.error("OPCI\u00D3N CANCELADA");
                                    }
                                });
                            }

                            
                        }else{
                            alertify.error("EL USUARIO YA ESE ENCUENTRA REGISTRADO")
                            document.resp_form.usuario.focus() 
                            return 0;  
                        }
                }}); 
            }
            else{
                if(ci1!=ci){
                                
                    var url = "<?php echo site_url("admin")?>/funcionario/verif_ci";
                    $.ajax({
                        type:"post",
                        url:url,
                        data:{ci:ci},
                        success:function(datos){

                            if(datos.trim() =='false'){
                                alertify.error('LA CEDULA DE IDENTIDAD YA SE ENCUENTRA REGISTRADO !!!')
                                document.resp_form.ci.focus() 
                                return 0; 
                            }
                            else{
                                alertify.confirm("GUARDAR RESPONSABLE ?", function (a) {
                                    if (a) {
                                        //============= GUARDAR DESPUES DE LA VALIDACION ===============
                                        document.resp_form.submit();
                                        document.getElementById("btsubmit").value = "MODIFICANDO...";
                                        document.getElementById("btsubmit").disabled = true;
                                    } else {
                                        alertify.error("OPCI\u00D3N CANCELADA");
                                    }
                                });
                            }

                        }});

                }
                else{
                    alertify.confirm("GUARDAR RESPONSABLE ?", function (a) {
                        if (a) {
                            //============= GUARDAR DESPUES DE LA VALIDACION ===============
                            document.resp_form.submit();
                            document.getElementById("btsubmit").value = "MODIFICANDO...";
                            document.getElementById("btsubmit").disabled = true;
                        } else {
                            alertify.error("OPCI\u00D3N CANCELADA");
                        }
                    });
                }
            }
            
        }
        $(document).ready(function() {
            pageSetUp();
            $("#dep_id").change(function () {
                $("#dep_id option:selected").each(function () {
                    elegido=$(this).val();
                    $.post("<?php echo base_url(); ?>index.php/admin/proy/combo_distrital", { elegido: elegido,accion:'distrital' }, function(data){
                        $("#dist_id").html(data);
                    });     
                });
            });
        })
        </script>
    </body>
</html>
