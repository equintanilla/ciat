<!DOCTYPE html>
<html lang="en-us">
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    <title><?php echo $this->session->userdata('name')?></title>
    <meta name="description" content="">
    <meta name="author" content="">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- Basic Styles -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
    <!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
    <!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
    <!-- FAVICONS -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.core.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css" id="toggleCSS"/>
    <meta name="viewport" content="width=device-width">
      <style type="text/css">
          table{font-size: 10px;
            width: 100%;
            max-width:1550px;;
            overflow-x: scroll;
            }
          th{
              padding: 1.4px;
              text-align: center;
              font-size: 10px;
            }
      </style>
</head>
<body class="">
<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->
<!-- HEADER -->
<header id="header">
    <!-- pulled right: nav area -->
    <div class="pull-right col-md-4">
        <!-- collapse menu button -->
        <div id="hide-menu" class="btn-header pull-right">
            <span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
        </div>
        <!-- end collapse menu -->
        <!-- logout button -->
        <div id="logout" class="btn-header transparent pull-right">
            <span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Sign Out" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
        </div>
        <!-- end logout button -->
        <!-- search mobile button (this is hidden till mobile view port) -->
        <div id="search-mobile" class="btn-header transparent pull-right">
            <span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
        </div>
        <!-- end search mobile button -->
        <!-- fullscreen button -->
        <div id="fullscreen" class="btn-header transparent pull-right">
            <span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen"><i  class="fa fa-arrows-alt"></i></a> </span>
        </div>
        <!-- end fullscreen button -->
    </div>
    <!-- end pulled right: nav area -->

</header>
<!-- END HEADER -->
<!-- Left panel : Navigation area -->
<aside id="left-panel">

    <!-- User info -->
    <div class="login-info">
		<span> <!-- User image size is adjusted inside CSS, it should stay as is -->
			<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
				<span>
					<?php echo $this->session->userdata("user_name"); ?>
				</span>
                <i class="fa fa-angle-down"></i>
            </a>
		</span>
    </div>

    <nav>
        <ul>
            <li class="">
            <a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MENÚ PRINCIPAL"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
            </li>
            <li class="text-center">
                <a href="#" title="REPORTE GERENCIAL"> <span class="menu-item-parent">MANTENIMIENTO</span></a>
            </li>
            <?php echo $menu;?>
        </ul>
    </nav>
    <span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>

</aside>

<!-- MAIN PANEL -->
<div id="main" role="main">

    <!-- RIBBON -->
	<div id="ribbon">
		<span class="ribbon-button-alignment"> 
			<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
				<i class="fa fa-refresh"></i>
			</span> 
		</span>
		<!-- breadcrumb -->
		<ol class="breadcrumb">
        <li>Mantenimiento</li><li>Escala Salarial</li>
		</ol>
	</div>
    <!-- END RIBBON -->

    <!-- MAIN CONTENT -->
    <div id="content">
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- row -->
            <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                <section id="widget-grid" class="well">
                <div class="">
                  <h1><small><?php echo $this->session->userdata("entidad"); ?></small></h1>
                  <h1> ESCALA SALARIAL  <?php echo $this->session->userdata("gestion"); ?></h1>
                </div>
            </section>
            </article>
            <article class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                <section id="widget-grid" class="well">
                    <div align="center">
                        <a href="javascript:abreVentana('<?php echo site_url("admin").'/rep_escala_salarial' ?>');" class="btn btn-success" title="REPORTE ESCALA SALARIAL" style="width:100%;">IMPRIMIR ESCALA</a>
                    </div>
                </section>
            </article>
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-color-darken" >
                            <header>
                                <span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
                               <h2 class="font-md"><strong></strong></h2>               
                            </header>
                        <div>
                            <div class="widget-body no-padding">
                                <div class="table-responsive">
                                    <table id="dt_basic" class="table table table-bordered" width="100%">
                                        <thead>
                                        <tr>
                                            <th style="width:3%;">
                                                <a data-toggle="modal" href="#modal_nuevo_car" style="text-decoration: none" title="NUEVO ESCALA SALARIAL">
                                                    <img src="<?php echo base_url(); ?>assets/ifinal/2.png" width="35" height="35"><br>NUEVO
                                                </a>
                                            </th>
                                            <th style="width:3%;">C&Oacute;DIGO</th>
                                            <th style="width:15%;">DEPENDE</th>
                                            <th style="width:15%;">CARGO</th>
                                            <th style="width:5%;">SUELDO</th>
                                            <th style="width:5%;"></th>
                                        </tr>
                                        </thead>
                                        <tbody id="tabla_ff">
                                           <?php echo $escala;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- end widget content -->
                        </div>
                        <!-- end widget div -->
                    </div>
                    <!-- end widget -->
                </article>
            </div>
        </section>
        <!-- end widget grid -->
    </div>
    <!-- END MAIN CONTENT -->
<!-- PAGE FOOTER -->
</div>
<div class="page-footer">
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
        </div>
    </div>
</div>
<!-- END PAGE FOOTER -->

<!-- ================== Modal NUEVO CARGO ========================== -->
<div class="modal animated fadeInDown" id="modal_nuevo_car" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close text-danger " data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title text-center text-info">
                    <b><i class="glyphicon glyphicon-pencil"></i> NUEVA ESCALA SALARIAL</b>
                </h4>
            </div>
            <div class="modal-body no-padding">
                <div class="row">
                    <form id="form_car" novalidate="novalidate" method="post">
                        <div id="bootstrap-wizard-1" class="col-sm-12">
                            <div class="well">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <LABEL><b>Ingrese el nombre del Cargo</b></label>
                                            <input class="form-control" type="text" name="car_nombre" id="car_nombre"
                                                   placeholder="Nombre de Cargo" style="width:100%;"
                                                   style="text-transform:uppercase;"
                                                   onkeyup="javascript:this.value=this.value.toUpperCase();"
                                                   onkeypress="if (this.value.length < 100) { return soloLetras(event);}else{return false; }">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <LABEL><b>Es dependiente? </b></LABEL><br>
                                            <input type="radio" id="depndiente" value="si" name="dependiente"
                                                   class="car_si"/> SI
                                            <input type="radio" id="dependiente" value="no" name="dependiente"
                                                   class="car_no"/> NO

                                        </div>
                                    </div>
                                </div>
                                <div id="content_parent" class="row" style="display: none;">
                                    <div class="col-md-1">
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <select name="padre" id="padre" class="form-control ">
                                                <option value="">Seleccione un Cargo</option>
                                                <?php
                                                //imprimir todos 
                                                foreach ($cargos as $row) {
                                                    echo '<option value="' . $row['car_id'] . '">' . $row['car_cargo'] . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <LABEL><b>Ingrese el Sueldo</b></label>
                                            <input class="form-control" type="text" name="car_sueldo" id="car_sueldo"
                                                   placeholder="Ingrese Sueldo"
                                                   onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <LABEL><b>Ingrese el Código</b></label>
                                            <input class="form-control" type="text" name="car_codigo" id="car_codigo"
                                                   placeholder="Ingrese Código"
                                                   onkeypress="if (this.value.length < 8) { return soloNumeros(event);}else{return false; }">
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- end well -->
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-3 pull-left">
                        <button class="btn btn-default btn-danger" data-dismiss="modal">CANCELAR</button>
                    </div>
                    <div class="col-md-3 pull-right ">
                        <button type="submit" name="enviar_car" id="enviar_car" class="btn  btn-default btn-primary"><i
                                class="fa fa-save"></i>
                            GUARDAR
                        </button>
                    </div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<div class="modal animated fadeInDown" id="modal_mod_car" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close text-danger " data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title text-center text-info">
                    <b><i class="glyphicon glyphicon-pencil"></i> MODIFICAR REGISTRO</b>
                </h4>
            </div>
            <div class="modal-body no-padding">
                <div class="row">
                    <form id="mod_formcar" name="mod_formcar" novalidate="novalidate" method="post">
                        <div id="bootstrap-wizard-1" class="col-sm-12">
                            <div class="well">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <LABEL><b>Ingrese el nombre del Cargo</b></label>
                                            <input class="form-control" type="text" name="modcar_nombre"
                                                   id="modcar_nombre" placeholder="Ingrese el nombre del Cargo"
                                                   style="width:100%;" style="text-transform:uppercase;"
                                                   onkeyup="javascript:this.value=this.value.toUpperCase();"
                                                   onkeypress="if (this.value.length < 100) { return soloLetras(event);}else{return false; }">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <LABEL><b>Es dependiente? </b></LABEL><br>
                                            <input type="radio" id="modcar_si" value="si" name="modcar_si"
                                                   class="modcar_si custom-controls-stacked"/> SI
                                            <input type="radio" id="modcar_no" value="no" name="modcar_no"
                                                   class="modcar_no custom-controls-stacked"/> NO

                                        </div>
                                    </div>
                                </div>
                                <div id="modcontent_parent" class="row" style="display: none;">
                                    <div class="col-md-1">
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <select name="modcar_padre" id="modcar_padre" class="form-control ">
                                                <option value="">Seleccione un Cargo</option>
                                                <?php
                                                foreach ($list_car_padre as $row) {
                                                    echo '<option value="' . $row['car_id'] . '">' . $row['car_cargo'] . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <LABEL><b>Sueldo</b></label>
                                            <input class="form-control" type="text" name="modcar_sueldo"
                                                   id="modcar_sueldo"
                                                   onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <LABEL><b>Ingrese el Código</b></label>
                                            <input class="form-control" type="text" name="modcar_codigo"
                                                   id="modcar_codigo" placeholder="Ingrese Código">
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- end well -->
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-3 pull-left">
                        <button class="btn btn-lg btn-danger" data-dismiss="modal">CANCELAR</button>
                    </div>
                    <div class="col-md-3 pull-right ">
                        <button type="submit" name="mod_parenviar" id="mod_parenviar" class="btn  btn-lg btn-primary"><i
                                class="fa fa-save"></i>
                            ACEPTAR
                        </button>
                    </div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
<script data-pace-options='{ "restartOnRequestAfter": true }' src="<?php echo base_url(); ?>assets/js/plugin/pace/pace.min.js"></script>
<script>
    if (!window.jQuery) {
        document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-2.0.2.min.js"><\/script>');
    }
</script>
<script>
    if (!window.jQuery.ui) {
        document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
    }
</script>
<!-- IMPORTANT: APP CONFIG -->
<script src="<?php echo base_url(); ?>assets/js/session_time/jquery-idletimer.js"></script>
<script src="<?php echo base_url(); ?>assets/js/app.config.js"></script>
<script src = "<?php echo base_url(); ?>mis_js/control_session.js"></script>
<script src = "<?php echo base_url(); ?>mis_js/mantenimiento/estructura_org.js"></script>   
<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script>
<!-- BOOTSTRAP JS -->
<script src="<?php echo base_url(); ?>mis_js/mantenimiento/escala_salarial.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.min.js"></script>
<!-- CUSTOM NOTIFICATION -->
<script src="<?php echo base_url(); ?>assets/js/notification/SmartNotification.min.js"></script>
<!-- JQUERY VALIDATE -->
<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
<!-- JQUERY MASKED INPUT -->
<script src="<?php echo base_url(); ?>assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
<!-- JQUERY SELECT2 INPUT -->
<script src="<?php echo base_url(); ?>assets/js/plugin/select2/select2.min.js"></script>
<!-- JQUERY UI + Bootstrap Slider -->
<script src="<?php echo base_url(); ?>assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
<!-- browser msie issue fix -->
<script src="<?php echo base_url(); ?>assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
<!-- FastClick: For mobile devices -->
<script src="<?php echo base_url(); ?>assets/js/plugin/fastclick/fastclick.min.js"></script>
<!-- Demo purpose only -->
<script src="<?php echo base_url(); ?>assets/js/demo.min.js"></script>
<!-- MAIN APP JS FILE -->
<script src="<?php echo base_url(); ?>assets/js/app.min.js"></script>
<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
<!-- Voice command : plugin -->
<script src="<?php echo base_url(); ?>assets/js/speech/voicecommand.min.js"></script>
<!--alertas -->
<script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
<!-- ------------  mis validaciones js --------------------- -->
<SCRIPT src="<?php echo base_url(); ?>assets/js/mis_js/validacion_form.js" type="text/javascript"></SCRIPT>
<!-- PAGE RELATED PLUGIN(S) -->
<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <script>
        $(function () {
            function reset() {
                $("#toggleCSS").attr("href", "<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css");
                alertify.set({
                    labels: {
                        ok: "ACEPTAR",
                        cancel: "CANCELAR"
                    },
                    delay: 5000,
                    buttonReverse: false,
                    buttonFocus: "ok"
                });
            }

            $("#enviar_car").on("click", function (e) {
                var $validator = $("#form_car").validate({
                    rules: {
                        car_nombre: { //// Programa
                        required: true,
                        },
                        dependiente: { //// Programa
                            required: true,
                        },
                        padre: { //// Programa
                            required: true,
                        },
                        car_sueldo: { //// Programa
                            required: true,
                            number: true,
                        },
                        car_codigo: {
                            required: true,
                            number: true,
                        }
                    },
                    messages: {
                        car_nombre: "Ingrese el Nombre de Cargo",
                        dependiente: "Elija una Opcion",
                        padre: "Seleccione una Opcion",
                        car_sueldo: {required: "Ingrese el Sueldo", number: "Dato Inválido"},
                        car_codigo: {required: "Ingrese el Código", number: "Dato Inválido"},
                    },
                    highlight: function (element) {
                        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                    },
                    unhighlight: function (element) {
                        $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                    },
                    errorElement: 'span',
                    errorClass: 'help-block',
                    errorPlacement: function (error, element) {
                        if (element.parent('.input-group').length) {
                            error.insertAfter(element.parent());
                        } else {
                            error.insertAfter(element);
                        }
                    }
                });
                var $valid = $("#form_car").valid();
                if (!$valid) {
                    $validator.focusInvalid();
                } 
                else {
                    var car_nombre = document.getElementById("car_nombre").value;
                    var car_sueldo = document.getElementById("car_sueldo").value;
                    var padre = document.getElementById("padre").value;
                    var car_codigo = document.getElementById("car_codigo").value;


                    var url = "<?php echo site_url("admin")?>/escala_salarial_ver";
                        $.ajax({
                            type:"post",
                            url:url,
                            data:{car_codigo:car_codigo},
                            success:function(datos){
                                if(datos.trim() =='true'){
                                  var url = "<?php echo site_url("admin")?>/escala_salarial_add";
                                        $.ajax({
                                            type:"post",
                                            url:url,
                                            data:{car_nombre:car_nombre,car_sueldo:car_sueldo,padre:padre,car_codigo:car_codigo},
                                            success: function (data) {
                                                if (datos.trim() == 'true') {

                                                    alertify.alert("EL REGISTRO SE GUARD\u00D3 CORRECTAMENTE", function (e) {
                                                    if (e) {
                                                        window.location.reload(true);
                                                    }
                                                });

                                                } else {
                                                    alertify.error("ERROR !!! AL REGISTRAR ESCALA");
                                                }
                                            }
                                    }); 

                                }
                                else{
                                   alertify.error("CODIGO EXISTENTE PARA ESTA GESTION");
                                }
                        }});

                }
            });
        });
        </script>
        <script type="text/javascript">
                $(function () {
                    $(".mod_car").on("click", function (e) {
                        //==========================LLENAR MIS DATOS DE FORMULARIO CON LA CLAVE RECIBIDA
                        id_car = $(this).attr('name');
                        ponderacion = $(this).attr('id'); 
                        var url = "<?php echo site_url("admin")?>/escala_salarial_mod";
                        var request;
                        if (request) {
                            request.abort();
                        }
                        request = $.ajax({
                            url: url,
                            type: "POST",
                            dataType: 'json',
                            data: "id_car=" + id_car
                        });

                        request.done(function (response, textStatus, jqXHR) {

                            document.getElementById("modcar_nombre").value = response.car_cargo;
                            document.getElementById("modcar_sueldo").value = response.car_sueldo;
                            document.getElementById("modcar_codigo").value = response.car_codigo;
                            document.getElementById("modcar_codigo").disabled = true;
                            document.getElementById('modcar_si').disabled = false;
                            document.getElementById('modcar_no').disabled = false;
                            if (response.padre == 'NINGUNO') {
                                document.getElementById('modcar_no').checked = true;
                                document.getElementById('modcar_si').disabled = true;
                                $('#modcontent_parent').slideUp();
                            } else {
                                document.getElementById('modcar_si').checked = true;
                                document.getElementById('modcar_no').disabled = true;
                                $('#modcontent_parent').slideDown();
                                $("#modcar_padre option[value="+ response.car_depende +"]").attr("selected",true);
                            }

                        });
                        request.fail(function (jqXHR, textStatus, thrown) {
                            console.log("ERROR: " + textStatus);
                        });
                        request.always(function () {
                            //console.log("termino la ejecuicion de ajax");
                        });
                        e.preventDefault();
                        // =============================VALIDAR EL FORMULARIO DE MODIFICACION
                        $("#mod_parenviar").on("click", function (e) {
                            var $validator = $("#mod_formcar").validate({
                               rules: {
                                    modcar_nombre: { //// Programa
                                        required: true,
                                    },
                                    modcar_padre: { //// Programa
                                        required: true,
                                    },
                                    modcar_sueldo: { //// Programa
                                        required: true,
                                        number: true,
                                    },
                                    modcar_codigo: {
                                        required: true,
                                        number: true,
                                    }
                                },
                                messages: {
                                    modcar_nombre: "Ingrese el Nombre de Cargo",
                                    modcar_padre: "Seleccione una Opcion",
                                    modcar_sueldo: {required: "Ingrese el Sueldo", number: "Dato Inválido"},
                                    modcar_codigo: {required: "Ingrese el Código", number: "Dato Inválido"},                         
                                },
                                highlight: function (element) {
                                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                                },
                                unhighlight: function (element) {
                                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                                },
                                errorElement: 'span',
                                errorClass: 'help-block',
                                errorPlacement: function (error, element) {
                                    if (element.parent('.input-group').length) {
                                        error.insertAfter(element.parent());
                                    } else {
                                        error.insertAfter(element);
                                    }
                                }
                            });
                            var $valid = $("#mod_formcar").valid();
                            if (!$valid) {
                                $validator.focusInvalid();
                            } else {
                                //==========================================================
                                var modcar_nombre = document.getElementById("modcar_nombre").value;
                                var modcar_sueldo = document.getElementById("modcar_sueldo").value;
                                var modcar_padre = document.getElementById("modcar_padre").value;
                                var modcar_codigo = document.getElementById("modcar_codigo").value;

                                alertify.confirm("MODIFICAR ESCALA ?", function (a) {
                                    if (a) {
                                        var url = "<?php echo site_url("admin")?>/escala_salarial_update";
                                            $.ajax({
                                                type:"post",
                                                url:url,
                                                data:{car_nombre:modcar_nombre,car_sueldo:modcar_sueldo,padre:modcar_padre,car_codigo:modcar_codigo},
                                                success: function (data) {
                                                    window.location.reload(true);
                                                    alertify.success("SE MODIFICO CORRECTAMENTE");
                                                }
                                        }); 
                                    } else {
                                        alertify.error("OPCI\u00D3N CANCELADA");
                                    }
                                });
                            }
                        });
                    });
                });
            </script>


        <script type="text/javascript">
            $(function () {
                function reset() {
                    $("#toggleCSS").attr("href", "<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css");
                    alertify.set({
                        labels: {
                            ok: "ACEPTAR",
                            cancel: "CANCELAR"
                        },
                        delay: 5000,
                        buttonReverse: false,
                        buttonFocus: "ok"
                    });
                }

                // =====================================================================
                $(".act_ff").on("click", function (e) {
                    reset();
                    var name = $(this).attr('name');

                    var request;
                    // confirm dialog
                    alertify.confirm("ELIMINAR ESCALA SALARIAL ?", function (a) {
                        if (a) { 
                            url = "<?php echo site_url("admin")?>/escala_salarial_del";
                            if (request) {
                                request.abort();
                            }
                            request = $.ajax({
                                url: url,
                                type: "POST",
                                dataType: "json",
                                data: "car_id="+name

                            });

                            request.done(function (response, textStatus, jqXHR) { 
                                reset();
                                if (response.respuesta == 'correcto') {
                                    alertify.alert("SE ELIMINO CORRECTAMENTE LA ESCALA", function (e) {
                                        if (e) {
                                            window.location.reload(true);
                                        }
                                    });
                                } else {
                                    alertify.alert("ERROR !!!", function (e) {
                                        if (e) {
                                            window.location.reload(true);
                                        }
                                    });
                                }
                            });
                            request.fail(function (jqXHR, textStatus, thrown) {
                                console.log("ERROR: " + textStatus);
                            });
                            request.always(function () {
                                //console.log("termino la ejecuicion de ajax");
                            });

                            e.preventDefault();

                        } else {
                            // user clicked "cancel"
                            alertify.error("Opcion cancelada");
                        }
                    });
                    return false;
                });

            });
        </script>
<script type="text/javascript">
    // TABLA
    $(document).ready(function () {
        pageSetUp();
        /* BASIC ;*/
        var responsiveHelper_dt_basic = undefined;

        var breakpointDefinition = {
            tablet: 1024,
            phone: 480
        };

        $('#dt_basic').dataTable({
            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
            "t" +
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
            "autoWidth": true,
            "preDrawCallback": function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper_dt_basic) {
                    responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                }
            },
            "rowCallback": function (nRow) {
                responsiveHelper_dt_basic.createExpandIcon(nRow);
            },
            "drawCallback": function (oSettings) {
                responsiveHelper_dt_basic.respond();
            }
        });

        /* END BASIC */
    })
</script>
</body>

</html>
