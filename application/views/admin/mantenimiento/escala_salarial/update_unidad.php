<!DOCTYPE html>
<html lang="en-us">
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    <title><?php echo $this->session->userdata('name')?></title>
    <meta name="description" content="">
    <meta name="author" content="">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- Basic Styles -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
    <!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
    <!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
    <!-- FAVICONS -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.core.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css" id="toggleCSS"/>
    <meta name="viewport" content="width=device-width">
</head>
<body class="">
<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->
<!-- HEADER -->
<header id="header">
    <!-- pulled right: nav area -->
    <div class="pull-right col-md-4">
        <!-- collapse menu button -->
        <div id="hide-menu" class="btn-header pull-right">
            <span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
        </div>
        <!-- end collapse menu -->
        <!-- logout button -->
        <div id="logout" class="btn-header transparent pull-right">
            <span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Sign Out" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
        </div>
        <!-- end logout button -->
        <!-- search mobile button (this is hidden till mobile view port) -->
        <div id="search-mobile" class="btn-header transparent pull-right">
            <span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
        </div>
        <!-- end search mobile button -->
        <!-- fullscreen button -->
        <div id="fullscreen" class="btn-header transparent pull-right">
            <span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen"><i  class="fa fa-arrows-alt"></i></a> </span>
        </div>
        <!-- end fullscreen button -->
    </div>
    <!-- end pulled right: nav area -->

</header>
<!-- END HEADER -->
<!-- Left panel : Navigation area -->
<aside id="left-panel">

    <!-- User info -->
    <div class="login-info">
		<span> <!-- User image size is adjusted inside CSS, it should stay as is -->
			<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
				<span>
					<?php echo $this->session->userdata("user_name"); ?>
				</span>
                <i class="fa fa-angle-down"></i>
            </a>
		</span>
    </div>

    <nav>
        <ul>
          <li class="">
            <a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MENÚ PRINCIPAL"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
          </li>
          <li class="text-center">
          <a href="#" title="MIS PROYECTOS"><span class="menu-item-parent">MANTENIMIENTO</span></a>
        </li>
				<?php
		                for($i=0;$i<count($enlaces);$i++)
		                {
		                    if(count($subenlaces[$enlaces[$i]['o_child']])>0)
		                    {
		            ?>
		            <li>
		              	<a href="#" >
		              		<i class="<?php echo $enlaces[$i]['o_image']?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
		              	<ul >
		              	<?php
		                foreach ($subenlaces[$enlaces[$i]['o_child']] as $item) {
		                ?>
		                <li><a href="<?php echo base_url($item['o_url']); ?>"><?php echo $item['o_titulo']; ?></a></li>
		                <?php } ?>
		                </ul>
		            </li>
		            <?php 
		                    }
		         } ?>
        </ul>
    </nav>
    <span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>

</aside>

<!-- MAIN PANEL -->
<div id="main" role="main">

    <!-- RIBBON -->
	<div id="ribbon">
		<span class="ribbon-button-alignment"> 
			<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
				<i class="fa fa-refresh"></i>
			</span> 
		</span>
		<!-- breadcrumb -->
		<ol class="breadcrumb">
        <li>Mantenimiento</li><li>Unidad Organizacional</li>
		</ol>
	</div>
    <!-- END RIBBON -->

    <!-- MAIN CONTENT -->
    <div id="content">
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- row -->
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                    <section id="widget-grid" class="well">
                    <div class="">
                      <h1><small><?php echo $this->session->userdata("entidad"); ?></small></h1>
                      <h1> ESTRUCTURA ORGANIZACIONAL </h1>
                    </div>
                </section>
                </article>
                <article class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                    <section id="widget-grid" class="well">
                        <div align="center">
                            <a href="<?php echo site_url("admin").'/add_unidad' ?>" class="btn btn-success btn-lg" title="VOLVER ATRAS" style="width:100%;">VOLVER ATRAS</a>
                        </div>
                    </section>
                </article>
            </div>
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                </article>
                <article class="col-sm-12 col-md-8 col-lg-8">
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget" id="wid-id-8" data-widget-editbutton="false" data-widget-custombutton="false">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                            <h2></h2> 
                        </header>
                        <!-- widget div-->
                        <div>
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <!-- end widget edit box -->
                            
                            <!-- widget content -->
                            <div class="widget-body no-padding">
                                
                                <form action="<?php echo site_url("admin") . '/update_unidad' ?>" method="post" id="uni_form" name="uni_form" class="smart-form">
                                    <input type="hidden" name="uni_id" id="uni_id" value="<?php echo $unidad[0]['uni_id'];?>">
                                    <header>REGISTRO UNIDAD ORGANIZACIONAL (Modificar)</header>
                                    <fieldset>
                                        <div class="row">
                                            <section class="col col-2">
                                                <label class="label">C&oacute;digo </label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-tag"></i>
                                                    <input type="text" value="<?php echo $unidad[0]['uni_id'];?>" disabled="true">
                                                </label>
                                            </section>
                                            <section class="col col-10">
                                                <label class="label">Unidad Organizacional <font color="red">(*)</font></label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-tag"></i>
                                                    <input type="text" name="unidad" id="unidad" maxlength="100" value="<?php echo $unidad[0]['uni_unidad'];?>">
                                                </label>
                                            </section>   
                                        </div>             
                                        <div class="row">
                                            <section class="col col-6">
                                                <label class="label">Dependencia ? <font color="red">(*)</font></label>
                                                <label class="input">
                                                    <select class="form-control" id="dep" name="dep">
                                                        <option value="">Seleccione</option>
                                                        <?php
                                                            if($unidad[0]['uni_depende']!=0){
                                                                ?>
                                                                <option value="1" selected="true">Si</option>
                                                                <option value="0">No</option>
                                                                <?php
                                                            }
                                                            elseif($unidad[0]['uni_depende']==0){
                                                                ?>
                                                                <option value="1">Si</option>
                                                                <option value="0" selected="true">No</option>
                                                                <?php
                                                            }
                                                        ?>
                                                    </select>
                                                </label>
                                            </section>
                                            <section class="col col-6">
                                                <label class="label">Tipo(s) de Unidad</label>
                                                <div class="row">
                                                        <div class="col col-2"></div>
                                                        <div class="col col-10">
                                                            <label class="checkbox">
                                                                <input type="checkbox" name="tunidad1" <?php echo $cheked1;?>>
                                                                <i></i>UNIDAD RESPONSABLE</label>
                                                            <label class="checkbox">
                                                                <input type="checkbox" name="tunidad2" id="tunidad2" onchange="javascript:showContent()" <?php echo $cheked2;?>>
                                                                <i></i>UNIDAD EJECUTORA</label>
                                                            <label class="checkbox">
                                                                <input type="checkbox" name="tunidad3" <?php echo $cheked3;?>>
                                                                <i></i>DIRECCI&Oacute;N ADMINISTRATIVA</label>
                                                        </div>
                                                    </div>
                                            </section>
                                        </div>
                                        
                                        <?php
                                            if($unidad[0]['uni_depende']!=0){
                                                ?>
                                                <div id="dependencia">
                                                    <section class="col col-6">
                                                        <label class="label">Unidad Dependiente<font color="red">(*)</font></label>
                                                        <label class="input">
                                                            <select class="select2" id="unidad_dep" name="unidad_dep">
                                                                <option value="">Seleccione</option>
                                                                <?php 
                                                                foreach($estructura_organizacional as $row)
                                                                {
                                                                    if($row['uni_id']==$unidad[0]['uni_depende']){
                                                                        ?>
                                                                        <option value="<?php echo $row['uni_id'];?>" selected="true"><?php echo $row['uni_id'].' - '.$row['uni_unidad'];?></option>
                                                                        <?php
                                                                    }
                                                                    else{
                                                                        ?>
                                                                        <option value="<?php echo $row['uni_id'];?>"><?php echo $row['uni_id'].' - '.$row['uni_unidad'];?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </label>
                                                    </section>
                                                </div>
                                                <?php
                                            }
                                            elseif($unidad[0]['uni_depende']==0){
                                                ?>
                                                <div id="dependencia" style="display:none;">
                                                    <section class="col col-6">
                                                        <label class="label">Unidad Dependiente <font color="red">(*)</font></label>
                                                        <label class="input">
                                                            <select class="select2" id="unidad_dep" name="unidad_dep">
                                                                <option value="">Seleccione</option>
                                                                <?php 
                                                                foreach($estructura_organizacional as $row)
                                                                {
                                                                    ?>
                                                                    <option value="<?php echo $row['uni_id'];?>"><?php echo $row['uni_id'].' - '.$row['uni_unidad'];?></option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </label>
                                                    </section>
                                                </div>
                                                <?php
                                            }
                                        
                                            if($unidad[0]['uni_ejecutora']==1)
                                            {   
                                                ?>
                                                <div id="unidad_ejecutora">
                                                    <div class="">
                                                        <section class="col col-6">
                                                        <label class="label">Tipo de Unidad Ejecutora <font color="red">(*)</font></label>
                                                        <label class="input">
                                                            <select class="select2" id="tp_ue" name="tp_ue">
                                                                <?php 
                                                                foreach($list_tipo_ue as $row)
                                                                {
                                                                    if($row['tue_id']==$unidad[0]['tp_ue'])
                                                                    {
                                                                        ?>
                                                                        <option value="<?php echo $row['tue_id'];?>" selected><?php echo $row['tue_unidad'];?></option>
                                                                        <?php
                                                                    }
                                                                    else
                                                                    {
                                                                        ?>
                                                                        <option value="<?php echo $row['tue_id'];?>"><?php echo $row['tue_unidad'];?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </label>
                                                        </section>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                            else
                                            {
                                                ?>
                                                <div id="unidad_ejecutora" style="display:none;">
                                                    <div class="">
                                                        <section class="col col-6">
                                                        <label class="label">Tipo de Unidad Ejecutora <font color="red">(*)</font></label>
                                                        <label class="input">
                                                            <select class="select2" id="tp_ue" name="tp_ue">
                                                                <?php 
                                                                foreach($list_tipo_ue as $row)
                                                                {
                                                                    ?>
                                                                    <option value="<?php echo $row['tue_id'];?>"><?php echo $row['tue_unidad'];?></option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </label>
                                                        </section>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                    </fieldset>
                                    
                                    <footer>
                                        <input type="button" value="MODIFICAR UNIDAD" id="btsubmit" class="btn btn-primary" onclick="valida_envia_update()" title="GUARDAR UNIDAD">
                                        <a href="<?php echo base_url().'index.php/estructura_org'; ?>" class="btn btn-default" title="REQUERIMIENTOS DE LA OPERACION"> CANCELAR </a>
                                    </footer>
                                    
                                    <div class="message">
                                        <i class="fa fa-thumbs-up"></i>
                                        <p>Your message was successfully sent!</p>
                                    </div>
                                </form>   
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </section>
        <!-- end widget grid -->
    </div>
    <!-- END MAIN CONTENT -->
<!-- PAGE FOOTER -->
</div>
<div class="page-footer">
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
        </div>
    </div>
</div>
<!-- END PAGE FOOTER -->

<!--================================================== -->

<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
<script data-pace-options='{ "restartOnRequestAfter": true }' src="<?php echo base_url(); ?>assets/js/plugin/pace/pace.min.js"></script>
<script>
    if (!window.jQuery) {
        document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-2.0.2.min.js"><\/script>');
    }
</script>
<script>
    if (!window.jQuery.ui) {
        document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
    }
</script>
<!-- IMPORTANT: APP CONFIG -->
<script src="<?php echo base_url(); ?>assets/js/session_time/jquery-idletimer.js"></script>
<script src="<?php echo base_url(); ?>assets/js/app.config.js"></script>
<script src = "<?php echo base_url(); ?>mis_js/mantenimiento/estructura_org.js"></script>
<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script>
<!-- BOOTSTRAP JS -->
<script src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.min.js"></script>
<!-- CUSTOM NOTIFICATION -->
<script src="<?php echo base_url(); ?>assets/js/notification/SmartNotification.min.js"></script>
<!-- JQUERY VALIDATE -->
<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
<!-- JQUERY MASKED INPUT -->
<script src="<?php echo base_url(); ?>assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
<!-- JQUERY SELECT2 INPUT -->
<script src="<?php echo base_url(); ?>assets/js/plugin/select2/select2.min.js"></script>
<!-- JQUERY UI + Bootstrap Slider -->
<script src="<?php echo base_url(); ?>assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
<!-- browser msie issue fix -->
<script src="<?php echo base_url(); ?>assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
<!-- FastClick: For mobile devices -->
<script src="<?php echo base_url(); ?>assets/js/plugin/fastclick/fastclick.min.js"></script>
<!-- Demo purpose only -->
<script src="<?php echo base_url(); ?>assets/js/demo.min.js"></script>
<!-- MAIN APP JS FILE -->
<script src="<?php echo base_url(); ?>assets/js/app.min.js"></script>
<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
<!-- Voice command : plugin -->
<script src="<?php echo base_url(); ?>assets/js/speech/voicecommand.min.js"></script>
<!--alertas -->
<script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
<!-- ------------  mis validaciones js --------------------- -->
<SCRIPT src="<?php echo base_url(); ?>assets/js/mis_js/validacion_form.js" type="text/javascript"></SCRIPT>
<!-- PAGE RELATED PLUGIN(S) -->
<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
<!--================= ELIMINACION DE LAS METAS =========================================-->
<script type="text/javascript">
    // TABLA
    $(document).ready(function () {
        pageSetUp();
        /* BASIC ;*/
        var responsiveHelper_dt_basic = undefined;

        var breakpointDefinition = {
            tablet: 1024,
            phone: 480
        };

        $('#dt_basic').dataTable({
            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
            "t" +
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
            "autoWidth": true,
            "preDrawCallback": function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper_dt_basic) {
                    responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                }
            },
            "rowCallback": function (nRow) {
                responsiveHelper_dt_basic.createExpandIcon(nRow);
            },
            "drawCallback": function (oSettings) {
                responsiveHelper_dt_basic.respond();
            }
        });

        /* END BASIC */
    })
</script>
</body>

</html>
