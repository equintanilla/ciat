<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
		<title><?php echo $this->session->userdata('name')?></title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css"> 
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<!--estiloh-->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css"> 
		    <!--para las alertas-->
    	<meta name="viewport" content="width=device-width">
		<!--fin de stiloh-->
          	<style>
			/*////scroll tablas/////*/
			table{font-size: 10px;
            width: 100%;
            max-width:1550px;;
			overflow-x: scroll;
            }
            th{
              padding: 1.4px;
              text-align: center;
              font-size: 10px;
            }
			</style>
	</head>
	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->

		<!-- HEADER -->
		<header id="header">
			<div id="logo-group">
				<span id="logo"> <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="SmartAdmin"> </span>
			</div>
			<!-- pulled right: nav area -->
			<div class="pull-right">
				<!-- collapse menu button -->
				<div id="hide-menu" class="btn-header pull-right">
					<span> <a href="javascript:void(0);" data-action="toggleMenu" title="Menu"><i class="fa fa-reorder"></i></a> </span>
				</div>
				<!-- end collapse menu -->
				<!-- logout button -->
				<div id="logout" class="btn-header transparent pull-right">
					<span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Salir" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
				</div>
				<!-- end logout button -->
				<!-- search mobile button (this is hidden till mobile view port) -->
				<div id="search-mobile" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
				</div>
				<!-- end search mobile button -->
				<!-- fullscreen button -->
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Pantalla Completa"><i class="fa fa-arrows-alt"></i></a> </span>
				</div>
				<!-- end fullscreen button -->
			</div>
			<!-- end pulled right: nav area -->
		</header>
		<!-- END HEADER -->
		<!-- Left panel : Navigation area -->
		<aside id="left-panel">

			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
					
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                              <span>
                                  <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                              </span>
						<i class="fa fa-angle-down"></i>
					</a> 
					
				</span>
			</div>

			<nav>
				<ul>
					<li class="">
	                <a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MENÚ PRINCIPAL"><i
	                        class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
	            	</li>
		            <li class="text-center">
		                <a href="#" title="PROGRAMACION"> <span class="menu-item-parent">MODIFICACIONES</span></a>
		            </li>
				<?php
		                for($i=0;$i<count($enlaces);$i++)
		                {
		                    if(count($subenlaces[$enlaces[$i]['o_child']])>0)
		                    {
		            ?>
		            <li>
		              	<a href="#" >
		              		<i class="<?php echo $enlaces[$i]['o_image']?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
		              	<ul >
		              	<?php
		                foreach ($subenlaces[$enlaces[$i]['o_child']] as $item) {
		                ?>
		                <li><a href="<?php echo base_url($item['o_url']); ?>"><?php echo $item['o_titulo']; ?></a></li>
		                <?php } ?>
		                </ul>
		            </li>
		            <?php 
		            }
		        } ?>
				</ul>
			</nav>
			<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>

		</aside>

		<!-- MAIN PANEL -->
		<div id="main" role="main">

			<!-- RIBBON -->
			<div id="ribbon">

				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>

				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li>Modificaciones</li><li>Modificaciones del POA</li><li>Modificaciones de la Operaci&oacute;n</li>
				</ol>
			</div>

			<!-- MAIN CONTENT -->
			<div id="content">
				<section id="widget-grid" class="">
					<div class="row">
						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="jarviswidget jarviswidget-color-darken" >
                              <header>
                                  <span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
                                  <h2 class="font-md"><strong>LISTA DE OPERACIONES <?php echo $this->session->userdata("gestion")?></strong></h2>  
                              </header>
								<div>
									<div class="widget-body no-padding">
									<div class="table-responsive">
										<table id="dt_basic" class="table table-bordered" style="width:100%;" font-size: "7px";>
											<thead>
												<tr>
													<th style="width:1%;">ACCI&Oacute;N</th>
													<th>CATEGORIA PROGRAM&Aacute;TICA <?php echo $this->session->userdata("gestion");?></th>
													<th>PROYECTO_PROGRAMA_OPERACI&Oacute;N</th>
													<th>TIPO DE OPERACI&Oacute;N</th>
													<th>C&Oacute;DIGO_SISIN</th>
													<th>RESPONSABLE (UNIDAD_EJECUTORA)</th>
													<th>UNIDAD_EJECUTORA</th>
													<th>UNIDAD_RESPONSABLE</th>
													<th>FASE_ETAPA</th>
													<th>NUEVO_CONTINUIDAD</th>
													<th>ANUAL_PLURIANUAL</th>
													<th>TOTAL_PRESUPUESTO_ETAPA</th>
													<th>PRESUPUESTO_REQUERIDO&nbsp;<?php echo $this->session->userdata("gestion");?></th>
													<th>PROVINCIA_MUNICIPIO</th>
												</tr>
											</thead>
											<tbody>
											<?php $nro=1;
			                                    foreach($lista_aper_padres  as $rowa)
			                                    {
			                                    	$proyectos=$this->model_proyecto->list_proyectos($rowa['aper_programa'],5,1);
			                                    	if(count($proyectos)!=0)
			                                    	{
			                                    		echo '<tr bgcolor="#99DDF0" title="CATEGORIA PROGRAMATICA PADRE : ' .$rowa['aper_programa'].''.$rowa['aper_proyecto'].''.$rowa['aper_actividad'].'">';
			                                    	 	echo '<td></td>';
			                                    	 	echo '<td><center>' .$rowa['aper_programa'].''.$rowa['aper_proyecto'].''.$rowa['aper_actividad'].'</td></td>';
											            echo '<td>' . $rowa['aper_descripcion'] . '</td>';
											            echo '<td>' . $rowa['aper_sisin'] . '</td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
			                                    	echo '</tr>';
				                                    	foreach($proyectos  as $row)
				                                    	{
				                                    	$fase= $this->model_faseetapa->get_id_fase($row['proy_id']); ////// datos fase activa
				                                    	$fase_gest = $this->model_faseetapa->fase_etapa_gestion($fase[0]['id'],$this->session->userdata("gestion")); ////// datos fase activa presupuesto gestion actual
				                                    	echo '<tr title="'.$row['tp_tipo'].'">';
				                                    	echo '<td>';
				                                       	?>
				                                       	<center><a href="<?php echo base_url().'index.php/admin/mod/proyecto/'.$row['proy_id'].'' ?>" class="btn btn-primary" title="MODIFICAR OPERACI&Oacute;N OPERACI&Oacute;N INSTITUCIONAL">MODIFICAR OPERACI&Oacute;N</a></center>
				                                       	<?php
				                                       	echo '</td>';
				                                       	echo '<td>';
				                                        echo '<center>'.$row['aper_programa'].''.$row['aper_proyecto'].''.$row['aper_actividad'].'</center>';
				                                        echo'</td>';
				                                        echo '<td>'.$row['proy_nombre'].'</td>';
				                                        echo '<td>'.$row['tp_tipo'].'</td>';
				                                        echo '<td>'.$row['proy_sisin'].'</td>';
				                                        $nro_r = $this->model_proyecto->nro_resp($row['proy_id']); //// nro de responsables 
				                                        /*================================================== RESPONSABLES ==============================================*/
				                                        $data['resp'] = $this->model_proyecto->responsable_proy($row['proy_id'],'1');
				                                        echo '<td>'.$data['resp'][0]['fun_nombre'].' '.$data['resp'][0]['fun_paterno'].' '.$data['resp'][0]['fun_materno'].'</td>';
				                                        echo '<td>'.$data['resp'][0]['uejec'].'</td>';
				                                        echo '<td>'.$data['resp'][0]['uresp'].'</td>';
				                                        /*==============================================================================================================*/
				                                      	$data['fase'] = $this->model_faseetapa->get_id_fase($row['proy_id']); ////// datos fase activa
				                                        	$nc=$this->model_faseetapa->calcula_nc($data['fase'][0]['pfec_fecha_inicio']); //// calcula nuevo/continuo
				                                        	$ap=$this->model_faseetapa->calcula_ap($data['fase'][0]['pfec_fecha_inicio'],$data['fase'][0]['pfec_fecha_fin']); //// calcula Anual/Plurianual
				                                        	echo '<td>* '.$data['fase'][0]['fase'].'<br>* '.$data['fase'][0]['etapa'].'</td>';
				                                        	echo '<td>'.$nc.'</td>';
				                                        	echo '<td>'.$ap.'</td>';
				                                        	echo '<td>'.number_format($data['fase'][0]['pfec_ptto_fase'], 2, ',', ' ').' Bs.</td>';
				                                        	$data['fase_gest'] = $this->model_faseetapa->fase_etapa_gestion($data['fase'][0]['id'],$this->session->userdata("gestion")); ////// datos fase activa presupuesto gestion actual
				                                        	$nro_fg_act = $this->model_faseetapa->nro_fasegestion_actual($data['fase'][0]['id'],$this->session->userdata('gestion'));
				                                        	echo '<td>';
				                                        	if($nro_fg_act==1 && ($data['fase_gest'][0]['estado']==1 || $data['fase_gest'][0]['estado']==2))
				                                        	{
				                                        		echo ''.number_format($data['fase_gest'][0]['pfecg_ppto_total'], 2, ',', ' ').' Bs.';
				                                        	}
				                                        	elseif ($nro_fg_act==0) {
				                                        		echo '<font color="red">la gestion no esta en curso</font>';
				                                        	}
				                                        	echo '</td>';
				                                        /*==============================================================================================================*/
				                                      	/*==================================== UBICACION GEOGRAFICA =====================================================*/
				                                        	echo '<td>';
						                                        $loc=$this->model_proyecto->localizacion($row['proy_id']);
												                if(count($loc)!=0){
												                  echo '<table class="table table-bordered">';
												                    echo '<tr bgcolor="#f5f5f5">';
												                    echo '<th>PROVINCIA</th><th>MUNICIPIO</th>';
												                    echo '</tr>';
												                    foreach($loc as $locali)
												                    {
												                      echo '<tr>';
												                      echo '<td>'.$locali['prov_provincia'].'</td>';
												                      echo '<td>'.$locali['muni_municipio'].'</td>';
												                      echo '</tr>';
												                    }
												                  
												                  echo '</table>';
												                }
						                                    echo'</td>';
						                                    /*==============================================================================================================*/
				                                    	echo '</tr>';
				                                    	}
			                                    	}
			                                    }
			                                ?>
											</tbody>
										</table>
										</div>

									</div>
									<!-- end widget content -->
				
								</div>
								<!-- end widget div -->
							</div>
							<!-- end widget -->
						</article>
						<!-- WIDGET END -->
					</div>
				</section>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN PANEL -->
		<!-- ========================================================================================================= -->
		<!-- PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->
		<!--================================================== -->
		<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
		<script data-pace-options='{ "restartOnRequestAfter": true }' src="<?php echo base_url(); ?>assets/js/plugin/pace/pace.min.js"></script>
		<script>
			if (!window.jQuery) {
				document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-2.0.2.min.js"><\/script>');
			}
		</script>
		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>
		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url(); ?>assets/js/app.config.js"></script>
		<script src="<?php echo base_url(); ?>mis_js/registro_ejecucion/mis_operaciones.js"></script>
		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.min.js"></script>
		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url(); ?>assets/js/notification/SmartNotification.min.js"></script>
		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url(); ?>assets/js/smartwidgets/jarvis.widget.min.js"></script>
		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<!-- SPARKLINES -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/select2/select2.min.js"></script>
		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
		<!-- browser msie issue fix -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/fastclick/fastclick.min.js"></script>
		<!-- Demo purpose only -->
		<script src="<?php echo base_url(); ?>assets/js/demo.min.js"></script>
		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url(); ?>assets/js/app.min.js"></script>
		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<script src="<?php echo base_url(); ?>assets/js/speech/voicecommand.min.js"></script>
		<!-- PAGE RELATED PLUGIN(S) -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.colVis.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.tableTools.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
		<!-- ====================================================================================================== -->
	</body>
</html>
