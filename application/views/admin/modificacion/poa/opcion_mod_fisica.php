<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
		<title><?php echo $this->session->userdata('name')?></title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
		<style type="text/css">
			aside{background: #05678B;}
		</style>
	</head>
	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->
		<!-- HEADER -->
		<header id="header">
			<div id="logo-group">
			</div>
			<!-- pulled right: nav area -->
			<div class="pull-right">
				<!-- collapse menu button -->
				<div id="hide-menu" class="btn-header pull-right">
					<span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
				</div>
				<!-- end collapse menu -->
				<!-- logout button -->
				<div id="logout" class="btn-header transparent pull-right">
					<span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Sign Out" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
				</div>
				<!-- end logout button -->
				<!-- search mobile button (this is hidden till mobile view port) -->
				<div id="search-mobile" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
				</div>
				<!-- end search mobile button -->
				<!-- fullscreen button -->
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen"><i class="fa fa-arrows-alt"></i></a> </span>
				</div>
				<!-- end fullscreen button -->
			</div>
			<!-- end pulled right: nav area -->

		</header>
		<!-- END HEADER -->
		<!-- Left panel : Navigation area -->
		<aside id="left-panel">
			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                        <span>
                            <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                        </span>
					</a> 
				</span>
			</div>

			<nav>
				<ul>
					<li>
						<a href='<?php echo site_url("admin").'/dashboard'; ?>' title="MENU PRINCIPAL"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
					</li>
					<li class="text-center">
		                <a href="<?php echo base_url().'index.php/admin/proy/mis_proyectos/'.$mod ?>" title="PROGRAMACION -> MIS PROYECTOS"> <span class="menu-item-parent">PROGRAMACI&Oacute;N F&Iacute;SICA</span></a>
		            </li>
					<?php
					if($nro_fase==1){
					    for($i=0;$i<count($enlaces);$i++){ ?>
					    	 <li>
				              	<a href="#" >
				              		<i class="<?php echo $enlaces[$i]['o_image'];?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
				              	<ul >
				              	<?php
				                $submenu= $this->menu_modelo->get_Modulos_sub($enlaces[$i]['o_child']);
					      		foreach($submenu as $row) {
				                ?>
				                <li><a href="<?php echo base_url($row['o_url'])."/".$mod."/".$id_f[0]['id']."/".$id_f[0]['proy_id']; ?>"><?php echo $row['o_titulo']; ?></a></li>
				                <?php } ?>
				                </ul>
				            </li>
					    	<?php
					    }
					}
					?>
				</ul>
			</nav>
			<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>
		</aside>

		<!-- MAIN PANEL -->
		<div id="main" role="main">
			<!-- RIBBON -->
			<div id="ribbon">
				<!-- breadcrumb -->
				<ol class="breadcrumb">
				<li>Mis Operaciones</li><li>Modificaci&oacute;n</li><li>F&iacute;sica</li><li>Operaci&oacute;n - Proyecto de Inversi&oacute;n</li>
				</ol>
			</div>
			<!-- END RIBBON -->
			<!-- MAIN CONTENT -->
			<div id="content">
				<section id="widget-grid" class="well">
					<div class="">
						<h1> CATEGORIA PROGRAMATICA : <small><?php echo $proyecto[0]['aper_programa'].''.$proyecto[0]['aper_proyecto'].''.$proyecto[0]['aper_actividad'].' - '.$proyecto[0]['proy_nombre']?></small></h1>
					</div>
				</section>
				<section id="widget-grid" class="">
						<div class="row">
							<div class="container">
								<div id="content"><br>
									<?php
										if ($proyecto[0]['proy_estado']==4) { ?>
											<div class="row">
												<div class="col-sm-12">
													<div class="well well-sm">
														<h3 class="text-primary">MODIFICAR PLAZO</h3>
														<table class="table table-bordered">
															<tbody>
																<!-- new tr -->
																<tr>
																	<td style="width:70%;">
																	<p style="font-size: 14px; font-family: Arial;">
																		Opci&oacute;n para realizar la modificaci&oacute;n del plazo de la Fase Activa
																	</p>
																	</td>

																	<td style="width:30%;">
																		<a href="<?php echo base_url().'index.php/mod/opciones/1/'.$id_f[0]['id'].'/'.$id_f[0]['proy_id'].'/1';?>" style="width:100%;" class="btn btn-primary"> INGRESAR A MODIFICAR</a>
																	</td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-12">
													<div class="well well-sm">
														<h3 class="text-primary">MODIFICAR METAS (PRODUCTOS - ACTIVIDADES TOTAL)</h3>
														<table class="table table-bordered">
															<tbody>
																<!-- new tr -->
																<tr>
																	<td style="width:70%;">
																	<p style="font-size: 14px; font-family: Arial;">
																		Opci&oacute;n para realizar la modificaci&oacute;n de metas de Productos y Actividades
																	</p>
																	</td>

																	<td style="width:30%;">
																		<a href="<?php echo base_url().'index.php/mod/opciones/1/'.$id_f[0]['id'].'/'.$id_f[0]['proy_id'].'/2';?>"   style="width:100%;" class="btn btn-primary"> INGRESAR A MODIFICAR </a>
																	</td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-12">
													<div class="well well-sm">
														<h3 class="text-primary">MODIFICAR METAS (PRODUCTOS - ACTIVIDADES ANUAL)</h3>
														<table class="table table-bordered">
															<tbody>
																<!-- new tr -->
																<tr>
																	<td style="width:70%;">
																	<p style="font-size: 14px; font-family: Arial;">
																		Opci&oacute;n para realizar la modificaci&oacute;n de metas de Productos y Actividades Anual
																	</p>
																	</td>

																	<td style="width:30%;">
																		<a href="<?php echo base_url().'index.php/mod/opciones/1/'.$id_f[0]['id'].'/'.$id_f[0]['proy_id'].'/3';?>" style="width:100%;" class="btn btn-primary"> INGRESAR A MODIFICAR </a>
																	</td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-12">
													<div class="well well-sm">
														<h3 class="text-primary">MODIFICAR PRESUPUESTO TOTAL</h3>
														<table class="table table-bordered">
															<tbody>
																<!-- new tr -->
																<tr>
																	<td style="width:70%;">
																	<p style="font-size: 14px; font-family: Arial;">
																		Opci&oacute;n para realizar la modificaci&oacute;n el Presupuesto total asignado
																	</p>
																	</td>

																	<td style="width:30%;">
																		<a href="<?php echo base_url().'index.php/mod/opciones/1/'.$id_f[0]['id'].'/'.$id_f[0]['proy_id'].'/4';?>" style="width:100%;" class="btn btn-primary"> INGRESAR A MODIFICAR </a>
																	</td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-12">
													<div class="well well-sm">
														<h3 class="text-primary">MODIFICAR PRESUPUESTO ANUAL TRASPASOS</h3>
														<table class="table table-bordered">
															<tbody>
																<!-- new tr -->
																<tr>
																	<td style="width:70%;">
																	<p style="font-size: 14px; font-family: Arial;">
																		Opci&oacute;n para realizar la modificaci&oacute;n de presupuesto Anual y Traspasos
																	</p>
																	</td>

																	<td style="width:30%;">
																		
																	</td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-12">
													<div class="well well-sm">
														<h3 class="text-primary">MODIFICAR PRESUPUESTO ANUAL ADICI&Oacute;N</h3>
														<table class="table table-bordered">
															<tbody>
																<!-- new tr -->
																<tr>
																	<td style="width:70%;">
																	<p style="font-size: 14px; font-family: Arial;">
																		Opci&oacute;n para realizar la modificaci&oacute;n de Presupuesto Anual Adici&oacute;n
																	</p>
																	</td>

																	<td style="width:30%;">
																		<a href="<?php echo base_url().'index.php/mod/opciones/1/'.$id_f[0]['id'].'/'.$id_f[0]['proy_id'].'/6';?>" style="width:100%;" class="btn btn-primary"> INGRESAR A MODIFICAR </a>
																	</td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>
											</div>

											<?php
										}
										else{?>
											<h2 class="alert alert-danger"><center>EL PROYECTO,, ACTIVIDAD NOSE ENCUENTRA APROBADO</center></h2>
											<?php
										}
									?>
								</div>
							</div>
						<!-- WIDGET END -->
						</div>
				</section>
			</div>
			<!-- END MAIN CONTENT -->

		</div>
		<!-- END MAIN PANEL -->

		<!-- PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->

		<!--================================================== -->
		<script>
			if (!window.jQuery) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-2.0.2.min.js"><\/script>');
			}
		</script>
		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>
		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url(); ?>assets/js/session_time/jquery-idletimer.js"></script>
		<script src="<?php echo base_url();?>/assets/js/app.config.js"></script>
		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url();?>/assets/js/bootstrap/bootstrap.min.js"></script>
		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url();?>/assets/js/notification/SmartNotification.min.js"></script>
		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url();?>/assets/js/smartwidgets/jarvis.widget.min.js"></script>
		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url();?>/assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<!-- SPARKLINES -->
		<script src="<?php echo base_url();?>/assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/select2/select2.min.js"></script>
		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
		<!-- browser msie issue fix -->
		<script src="<?php echo base_url();?>/assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url();?>/assets/js/plugin/fastclick/fastclick.min.js"></script>
		<!-- Demo purpose only -->
		<script src="<?php echo base_url();?>/assets/js/demo.min.js"></script>
		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url();?>/assets/js/app.min.js"></script>
		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<!-- Voice command : plugin -->
		<script src="<?php echo base_url();?>/assets/js/speech/voicecommand.min.js"></script>
		<script src="<?php echo base_url(); ?>mis_js/programacion/programacion/tablas.js"></script>
	</body>
</html>
