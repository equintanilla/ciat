<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
		<title><?php echo $this->session->userdata('name')?></title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
		<!-- FAVICONS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.core.css"/>
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css" id="toggleCSS"/>
		<script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
		<style type="text/css">
			aside{background: #05678B;}
		</style>
	</head>
	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->
		<!-- HEADER -->
		<header id="header">
			<div id="logo-group">
			</div>
			<!-- pulled right: nav area -->
			<div class="pull-right">
				<!-- collapse menu button -->
				<div id="hide-menu" class="btn-header pull-right">
					<span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
				</div>
				<!-- end collapse menu -->
				<!-- logout button -->
				<div id="logout" class="btn-header transparent pull-right">
					<span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Sign Out" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
				</div>
				<!-- end logout button -->
				<!-- search mobile button (this is hidden till mobile view port) -->
				<div id="search-mobile" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
				</div>
				<!-- end search mobile button -->
				<!-- fullscreen button -->
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen"><i class="fa fa-arrows-alt"></i></a> </span>
				</div>
				<!-- end fullscreen button -->
			</div>
			<!-- end pulled right: nav area -->

		</header>
		<!-- END HEADER -->
		<!-- Left panel : Navigation area -->
		<aside id="left-panel">
			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                        <span>
                            <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                        </span>
					</a> 
				</span>
			</div>

			<nav>
				<ul>
					<li>
						<a href='<?php echo site_url("admin").'/dashboard'; ?>' title="MENU PRINCIPAL"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
					</li>
					<li class="text-center">
		                <a href="<?php echo base_url().'index.php/admin/proy/mis_proyectos/'.$mod ?>" title="PROGRAMACION -> MIS PROYECTOS"> <span class="menu-item-parent">PROGRAMACI&Oacute;N F&Iacute;SICA</span></a>
		            </li>
					<?php
					if($nro_fase==1){
					    for($i=0;$i<count($enlaces);$i++){ ?>
					    	 <li>
				              	<a href="#" >
				              		<i class="<?php echo $enlaces[$i]['o_image'];?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
				              	<ul >
				              	<?php
				                $submenu= $this->menu_modelo->get_Modulos_sub($enlaces[$i]['o_child']);
					      		foreach($submenu as $row) {
				                ?>
				                <li><a href="<?php echo base_url($row['o_url'])."/".$mod."/".$id_f[0]['id']."/".$id_f[0]['proy_id']; ?>"><?php echo $row['o_titulo']; ?></a></li>
				                <?php } ?>
				                </ul>
				            </li>
					    	<?php
					    }
					}
					?>
				</ul>
			</nav>
			<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>
		</aside>

		<!-- MAIN PANEL -->
		<div id="main" role="main">
			<!-- RIBBON -->
			<div id="ribbon">
				<!-- breadcrumb -->
				<ol class="breadcrumb">
				<li>Mis Operaciones</li><li>Modificaci&oacute;n</li><li>F&iacute;sica</li><li>Operaci&oacute;n - Proyecto de Inversi&oacute;n</li><li>Modificar Plazo</li>

				</ol>
			</div>
			<!-- END RIBBON -->
			<!-- MAIN CONTENT -->
			<div id="content">
				<section id="widget-grid" class="well">
					<div class="">
						<h1> 
							CATEGORIA PROGRAMATICA : <small><?php echo $proyecto[0]['aper_programa'].''.$proyecto[0]['aper_proyecto'].''.$proyecto[0]['aper_actividad'].' - '.$proyecto[0]['proy_nombre']?></small><br>
							FECHA INICIO : <small><?php echo date('d/m/Y',strtotime($id_f[0]['inicio'])); ?></small> - FECHA FINAL : <small><?php echo date('d/m/Y',strtotime($id_f[0]['final'])); ?></small>
						</h1>
					</div>
				</section>
				<section id="widget-grid" class="">
						<div class="row">
							<div class="container">
								<div id="content"><br>
									<div class="row">
										<div class="col-sm-12">
											<div class="well well-sm">
												<h3 class="text-primary">MODIFICAR PLAZO</h3>
												<?php echo $plazos;?>
												<form id="formulario" name="formulario" method="post" action="<?php echo site_url("").'/modificaciones/cmod_opciones/update_plazo'?>" class="form-horizontal">
													<input type="hidden" name="pfec_id" value="<?php echo $id_f[0]['id'];?>">
													<input type="hidden" name="fi" id="fi" value="<?php echo date('d/m/Y',strtotime($proyecto[0]['f_inicial'])); ?>">	
													<input type="hidden" name="ff" id="ff" value="<?php echo date('d/m/Y',strtotime($proyecto[0]['f_final'])); ?>">
													<fieldset>
														<legend>Formulario de Evaluaci&oacute;n de Plazo de la Fase Activa</legend>
														<div class="form-group">
															<label class="col-md-2 control-label">FECHA INICIO</label>
															<div class="col-md-10">
																<input type="text" name="f_inicio" id="f_inicio" placeholder="Seleccione Fecha inicial" class="form-control datepicker" value="<?php echo date('d/m/Y',strtotime($id_f[0]['inicio'])); ?>" onKeyUp="this.value=formateafecha(this.value);" data-dateformat="dd/mm/yy" title="MODIFICAR FECHA DE INICIO">
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">FECHA FINAL</label>
															<div class="col-md-10">
																<input type="text" name="f_final" id="f_final" placeholder="Seleccione Fecha Final" class="form-control datepicker" value="<?php echo date('d/m/Y',strtotime($id_f[0]['final'])); ?>" onKeyUp="this.value=formateafecha(this.value);" data-dateformat="dd/mm/yy" title="MODIFICAR FECHA FINAL">
															</div>
														</div>
													</fieldset>
											
													<div class="form-actions">
														<div class="row">
															<div class="col-md-12" id="but">
																<a href='<?php echo site_url("").'/mod/opcion_fis/1/'.$id_f[0]['id'].'/'.$id_f[0]['proy_id'].'/1'; ?>' title="CANCELAR" class="btn btn-default">CANCELAR</a>
																<input type="button" value="MODIFICAR PLAZO" id="btsubmit" class="btn btn-primary" title="MODIFICAR PLAZO"><br><br>
															</div>
														</div>
													</div>
												</form>
											</div>
										</div>
									</div>
							
								</div>
							</div>
						<!-- WIDGET END -->
						</div>
				</section>
			</div>
			<!-- END MAIN CONTENT -->

		</div>
		<!-- END MAIN PANEL -->

		<!-- PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->

		<!--================================================== -->
		<script>
			if (!window.jQuery) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-2.0.2.min.js"><\/script>');
			}
		</script>
		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>
		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url(); ?>assets/js/session_time/jquery-idletimer.js"></script>
		<script src="<?php echo base_url();?>/assets/js/app.config.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/mis_js/validacion_form.js"></script>
		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url();?>/assets/js/bootstrap/bootstrap.min.js"></script>
		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url();?>/assets/js/notification/SmartNotification.min.js"></script>
		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url();?>/assets/js/smartwidgets/jarvis.widget.min.js"></script>
		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url();?>/assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<!-- SPARKLINES -->
		<script src="<?php echo base_url();?>/assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/select2/select2.min.js"></script>
		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
		<!-- browser msie issue fix -->
		<script src="<?php echo base_url();?>/assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url();?>/assets/js/plugin/fastclick/fastclick.min.js"></script>
		<!-- Demo purpose only -->
		<script src="<?php echo base_url();?>/assets/js/demo.min.js"></script>
		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url();?>/assets/js/app.min.js"></script>
		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<!-- Voice command : plugin -->
		<script src="<?php echo base_url();?>/assets/js/speech/voicecommand.min.js"></script>

		<script type="text/javascript">
		$(document).ready(function() {
			pageSetUp();
		})
		</script>
		<script>
    	function reset() {
            $("#toggleCSS").attr("href", "<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css");
            alertify.set({
                labels: {
                    ok: "ACEPTAR",
                    cancel: "CANCELAR"
                },
                delay: 5000,
                buttonReverse: false,
                buttonFocus: "ok"
            });
        }

		$(function () {
		    $("#btsubmit").on("click", function (e) {
		        var $validator = $("#formulario").validate({
		            rules: {
		                f_inicio: {
		                    required: true,
		                },
		                f_final: {
		                    required: true,
		                }
		            },
		            messages: {
		                f_inicio: {required: "<font color=red size=1>SELECCIONE FECHA INICIAL DE FASE</font>"},
		                f_final: {required: "<font color=red size=1>SELECCIONE FECHA FINAL DE LA FASE</font>"}
		            },
		            highlight: function (element) {
		                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		            },
		            unhighlight: function (element) {
		                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		            },
		            errorElement: 'span',
		            errorClass: 'help-block',
		            errorPlacement: function (error, element) {
		                if (element.parent('.input-group').length) {
		                    error.insertAfter(element.parent());
		                } else {
		                    error.insertAfter(element);
		                }
		            }
		        });
		        var $valid = $("#formulario").valid();
		        if (!$valid) {
		            $validator.focusInvalid();
		        } 
		        else {

		        	reset();
	                alertify.confirm("MODIFICAR PLAZO ?", function (a) {
	                    if (a) {
	                        document.getElementById('btsubmit').disabled = true;
	                        document.formulario.submit();
	                    } else {
	                        alertify.error("OPCI\u00D3N CANCELADA");
	                    }
	                });
		            
		        }
		    });
		});
		</script>
		<script>
		$(document).ready(function () {
        	$("#f_inicio").change(function () {            
            var finicio = $(this).val();
            fi = $('[name="fi"]').val(); /// fecha inicial proyecto
            var fecha_inip = fi.split("/")  //fecha inicial
            var fecha_inif = finicio.split("/")  //fecha inicial fase

	            if(fecha_inif[2]<fecha_inip[2]){
	            	alertify.error("LA FECHA INICIAL NO PUEDE SER ANTES DE LA FECHA INICIAL DEL PROYECTO, VERIFIQUE DATOS");
	            	$('#but').slideUp();
	            }
	            else{
	            	$('#but').slideDown();
	            }
            });

            $("#f_final").change(function () {            
            var ffinal = $(this).val();
            ff = $('[name="ff"]').val(); /// fecha final proyecto
            var fecha_finp = ff.split("/")  //fecha final
            var fecha_finf = ffinal.split("/")  //fecha final fase

	            if(fecha_finf[2]>fecha_finp[2]){
	            	alertify.error("LA FECHA FINAL NO PUEDE SER POSTERIOR A LA FECHA FINAL DEL PROYECTO, VERIFIQUE DATOS");
	            	$('#but').slideUp();
	            }
	            else{
	            	$('#but').slideDown();
	            }
            });
        });
        </script>
	</body>
</html>
