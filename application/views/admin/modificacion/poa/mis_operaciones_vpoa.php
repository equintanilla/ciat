<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
		<title><?php echo $this->session->userdata('name')?></title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css"> 
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css"> 
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.core.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css" id="toggleCSS" />
		<script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
		<!--para las alertas-->
    	<meta name="viewport" content="width=device-width">
		<!--fin de stiloh-->
          	<style>
			.table1{
	          display: inline-block;
	          width:100%;
	          max-width:1550px;
	          overflow-x: scroll;
	          }
			table{font-size: 10px;
            width: 100%;
            max-width:1550px;
			overflow-x: scroll;
            }
            th{
              padding: 1.4px;
              text-align: center;
              font-size: 10px;
            }
			</style>
	</head>
	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->

		<!-- HEADER -->
		<header id="header">
			<div id="logo-group">
				<span id="logo"> <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="SmartAdmin"> </span>
			</div>
			<!-- pulled right: nav area -->
			<div class="pull-right">
				<!-- collapse menu button -->
				<div id="hide-menu" class="btn-header pull-right">
					<span> <a href="javascript:void(0);" data-action="toggleMenu" title="Menu"><i class="fa fa-reorder"></i></a> </span>
				</div>
				<!-- end collapse menu -->
				<!-- logout button -->
				<div id="logout" class="btn-header transparent pull-right">
					<span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Salir" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
				</div>
				<!-- end logout button -->
				<!-- search mobile button (this is hidden till mobile view port) -->
				<div id="search-mobile" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
				</div>
				<!-- end search mobile button -->
				<!-- fullscreen button -->
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Pantalla Completa"><i class="fa fa-arrows-alt"></i></a> </span>
				</div>
				<!-- end fullscreen button -->
			</div>
			<!-- end pulled right: nav area -->
		</header>
		<!-- END HEADER -->
		<!-- Left panel : Navigation area -->
		<aside id="left-panel">

			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
					
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                            <span>
                                  <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                            </span>
						<i class="fa fa-angle-down"></i>
					</a> 
					
				</span>
			</div>

			<nav>
                <ul>
                    <li class="">
                    <a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MENÚ PRINCIPAL"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
                    </li>
                    <li class="text-center">
                        <a href="#" title="REGISTRO DE EJECUCION"> <span class="menu-item-parent">MODIFICACIONES</span></a>
                    </li>
                    <?php echo $menu;?>
                </ul>
            </nav>
			<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>

		</aside>

		<!-- MAIN PANEL -->
		<div id="main" role="main">

			<!-- RIBBON -->
			<div id="ribbon">

				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>

				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li>Modificaciones</li><li>Modificaciones del POA</li><li>Mis Operaciones</li><li>Analista POA</li>
				</ol>
			</div>

			<!-- MAIN CONTENT -->
			<div id="content">
				<section id="widget-grid" class="">
					<div class="row">
						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-10">
                            <section id="widget-grid" class="well">
                                <div class="">
                                    <h1><?php echo $this->session->userdata('entidad')?></h1>
                                    <h1><small>MODIFICAR DATOS DE LA OPERACI&Oacute;N / PROYECTO DE INVERSI&Oacute;N </small></h1>
                                </div>
                            </section>
                        </article>
						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="well well-sm well-light">
		                        <div id="tabs">
		                            <ul>
		                                <li>
		                                    <a href="#tabs-c">PROYECTOS DE INVERSI&Oacute;N PUBLICA</a>
		                                </li>
		                                <li>
		                                    <a href="#tabs-d">PROGRAMAS RECURRENTES</a>
		                                </li>
		                                <li>
		                                    <a href="#tabs-b">PROGRAMAS NO RECURRENTES</a>
		                                </li>
		                                <li>
		                                    <a href="#tabs-a">OPERACI&Oacute;N DE FUNCIONAMIENTO</a>
		                                </li>
		                            </ul>
		                            <div id="tabs-c">
		                                <div class="row">
		                                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		                                        <div class="jarviswidget jarviswidget-color-darken" >
		                                            <header>
		                                                <span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
		                                            </header>
		                                            <div>
		                                                <div class="widget-body ">
		                                                    <table id="dt_basic2" class="table table-bordered" style="width:100%;" font-size: "7px";>
		                                                    <thead>
		                                                    <tr>
		                                                        <th style="height:65px;">#</th>
		                                                        <th></th>
		                                                        <th>CATEGORIA PROGRAMATICA <?php echo $this->session->userdata("gestion");?></th>
		                                                        <th>PROYECTO_PROGRAMA_OPERACI&Oacute;N DE FUNCIONAMIENTO</th>
		                                                        <th>TIPO DE OPERACI&Oacute;N</th>
		                                                        <th>C&Oacute;DIGO_SISIN</th>
		                                                        <th>RESPONSABLE (UE)</th>
		                                                        <th>UNIDAD_EJECUTORA</th>
		                                                        <th>UNIDAD_RESPONSABLE</th>
		                                                        <th>FASE_ETAPA</th>
		                                                        <th>NUEVO_CONTINUIDAD</th>
		                                                        <th>ANUAL_PLURIANUAL</th>
		                                                        <th>COSTO TOTAL DEL PROYECTO</th>
		                                                        <th>PRESUPUESTO REQUERIDO&nbsp;<?php echo $this->session->userdata("gestion");?></th>
		                                                        <th>TECHO ASIGNADO TOTAL</th>
		                                                    </tr>
		                                                    </thead>
		                                                    <tbody>
		                                               			<?php echo $proyecto;?>
		                                                    </tbody>
		                                                    </table>
		                                                </div>
		                                                <!-- end widget content -->
		                                            </div>
		                                            <!-- end widget div -->
		                                        </div>
		                                        <!-- end widget -->
		                                    </article>
		                                </div>
		                            </div>

		                            <div id="tabs-d">
		                                <div class="row">
		                                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		                                        <div class="jarviswidget jarviswidget-color-darken" >
		                                            <header>
		                                                <span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
		                                            </header>
		                                            <div>
		                                                <div class="widget-body no-padding">
		                                                    <table id="dt_basic3" class="table1 table-bordered" style="width:100%;" font-size: "7px";>
		                                                    <thead>
		                                                    <tr>
		                                                        <th style="height:65px;">#</th>
		                                                        <th></th>
		                                                        <th>CATEGORIA PROGRAMATICA <?php echo $this->session->userdata("gestion");?></th>
		                                                        <th>PROYECTO_PROGRAMA_OPERACI&Oacute;N DE FUNCIONAMIENTO</th>
		                                                        <th>TIPO DE OPERACI&Oacute;N</th>
		                                                        <th>C&Oacute;DIGO_SISIN</th>
		                                                        <th>RESPONSABLE (UE)</th>
		                                                        <th>UNIDAD_EJECUTORA</th>
		                                                        <th>UNIDAD_RESPONSABLE</th>
		                                                        <th>FASE_ETAPA</th>
		                                                        <th>NUEVO_CONTINUIDAD</th>
		                                                        <th>ANUAL_PLURIANUAL</th>
		                                                        <th>COSTO TOTAL DEL PROYECTO</th>
		                                                        <th>PRESUPUESTO REQUERIDO&nbsp;<?php echo $this->session->userdata("gestion");?></th>
		                                                        <th>TECHO ASIGNADO TOTAL</th>
		                                                    </tr>
		                                                    </thead>
		                                                    <tbody>
		                                               			<?php echo $precurrente;?>
		                                                    </tbody>
		                                                    </table>
		                                                </div>
		                                                <!-- end widget content -->
		                                            </div>
		                                            <!-- end widget div -->
		                                        </div>
		                                        <!-- end widget -->
		                                    </article>
		                                </div>
		                            </div>

		                            <div id="tabs-b">
		                                <div class="row">
		                                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		                                        <div class="jarviswidget jarviswidget-color-darken" >
		                                            <header>
		                                                <span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
		                                            </header>
		                                            <div>
		                                                <div class="widget-body no-padding">
		                                                    <table id="dt_basic4" class="table1 table-bordered" style="width:100%;" font-size: "7px";>
		                                                    <thead>
		                                                    <tr>
		                                                        <th style="height:65px;">#</th>
		                                                        <th></th>
		                                                        <th>CATEGORIA PROGRAMATICA <?php echo $this->session->userdata("gestion");?></th>
		                                                        <th>PROYECTO_PROGRAMA_OPERACI&Oacute;N DE FUNCIONAMIENTO</th>
		                                                        <th>TIPO DE OPERACI&Oacute;N</th>
		                                                        <th>C&Oacute;DIGO_SISIN</th>
		                                                        <th>RESPONSABLE (UE)</th>
		                                                        <th>UNIDAD_EJECUTORA</th>
		                                                        <th>UNIDAD_RESPONSABLE</th>
		                                                        <th>FASE_ETAPA</th>
		                                                        <th>NUEVO_CONTINUIDAD</th>
		                                                        <th>ANUAL_PLURIANUAL</th>
		                                                        <th>COSTO TOTAL DEL PROYECTO</th>
		                                                        <th>PRESUPUESTO REQUERIDO&nbsp;<?php echo $this->session->userdata("gestion");?></th>
		                                                        <th>TECHO ASIGNADO TOTAL</th>
		                                                    </tr>
		                                                    </thead>
		                                                    <tbody>
		                                             			<?php echo $pnrecurrente;?>
		                                                    </tbody>
		                                                    </table>
		                                                </div>
		                                                <!-- end widget content -->
		                                            </div>
		                                            <!-- end widget div -->
		                                        </div>
		                                        <!-- end widget -->
		                                    </article>
		                                </div>
		                            </div>

		                            <div id="tabs-a">
		                                <div class="row">
		                                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		                                        <div class="jarviswidget jarviswidget-color-darken" >
		                                            <header>
		                                                <span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
		                                            </header>
		                                            <div>
		                                                <div class="widget-body no-padding">
		                                                    <table id="dt_basic" class="table1 table-bordered" style="width:100%;" font-size: "7px";>
		                                                    <thead>
		                                                    <tr>
		                                                        <th style="height:65px;">#</th>
		                                                        <th></th>
		                                                        <th>CATEGORIA PROGRAMATICA <?php echo $this->session->userdata("gestion");?></th>
		                                                        <th>PROYECTO_PROGRAMA_OPERACI&Oacute;N DE FUNCIONAMIENTO</th>
		                                                        <th>TIPO DE OPERACI&Oacute;N</th>
		                                                        <th>C&Oacute;DIGO_SISIN</th>
		                                                        <th>RESPONSABLE (UE)</th>
		                                                        <th>UNIDAD_EJECUTORA</th>
		                                                        <th>UNIDAD_RESPONSABLE</th>
		                                                        <th>FASE_ETAPA</th>
		                                                        <th>NUEVO_CONTINUIDAD</th>
		                                                        <th>ANUAL_PLURIANUAL</th>
		                                                        <th>COSTO TOTAL DEL PROYECTO</th>
		                                                        <th>PRESUPUESTO REQUERIDO&nbsp;<?php echo $this->session->userdata("gestion");?></th>
		                                                        <th>TECHO ASIGNADO TOTAL</th>
		                                                    </tr>
		                                                    </thead>
		                                                    <tbody>
		                                  						<?php echo $ofuncionamiento;?>
		                                                    </tbody>
		                                                    </table>
		                                                </div>
		                                                <!-- end widget content -->
		                                            </div>
		                                            <!-- end widget div -->
		                                        </div>
		                                        <!-- end widget -->
		                                    </article>
		                                </div>
		                            </div>

		                        </div>
		                    </div>
						</article>
						<!-- WIDGET END -->
					</div>
				</section>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN PANEL -->
		<!-- ========================================================================================================= -->
		<!-- PAGE FOOTER -->

		<div class="modal fade" id="modal_derivar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							&times;
						</button>
						<div id="titulo"></div>
					</div>
					
					<div class="modal-body">
						<div id="nombre"></div>
						<form id="mod_formaper" name="mod_formaper" novalidate="novalidate" method="post">
							<input class="form-control" type="hidden" name="id_p" id="id_p">
							<input class="form-control" type="hidden" name="resp" id="resp">
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">
							CANCELAR
						</button>
						<button type="submit" name="mod_aperenviar" id="mod_aperenviar" class="btn btn-primary">
							DERIVAR OPERACI&Oacute;N / PROYECTO
						</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

 
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->

		<!--================================================== -->
		<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
		<script data-pace-options='{ "restartOnRequestAfter": true }' src="<?php echo base_url(); ?>assets/js/plugin/pace/pace.min.js"></script>
		<script>
			if (!window.jQuery) {
				document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-2.0.2.min.js"><\/script>');
			}
		</script>
		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>
		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url(); ?>assets/js/app.config.js"></script>
		<script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.min.js"></script>
		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url(); ?>assets/js/notification/SmartNotification.min.js"></script>
		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url(); ?>assets/js/smartwidgets/jarvis.widget.min.js"></script>
		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<!-- SPARKLINES -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/select2/select2.min.js"></script>
		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
		<!-- browser msie issue fix -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/fastclick/fastclick.min.js"></script>
		<!-- Demo purpose only -->
		<script src="<?php echo base_url(); ?>assets/js/demo.min.js"></script>
		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url(); ?>assets/js/app.min.js"></script>
		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<script src="<?php echo base_url(); ?>assets/js/speech/voicecommand.min.js"></script>
		<!-- PAGE RELATED PLUGIN(S) -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.colVis.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.tableTools.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
		<!-- ====================================================================================================== -->
		<script type="text/javascript">
		    // DO NOT REMOVE : GLOBAL FUNCTIONS!
		    $(document).ready(function() {
		        pageSetUp();
		        // menu
		        $("#menu").menu();
		        $('.ui-dialog :button').blur();
		        $('#tabs').tabs();
		    })
		</script>
		<script type="text/javascript">
			$(function(){
				//limpiar variable
				var id_aper =''; 
				$(".modal_derivar").on("click",function(e){
					//============== ASIGNAR PROYECTO AL VALIDADOR POA ================
					proy_id = $(this).attr('name');
					tp = $(this).attr('id');

					if(tp==1){
						$('#titulo').html('<h4 class="modal-title" id="myModalLabel">DERIVAR A T&Eacute;CNICO DE UNIDAD EJECUTORA</h4>');
						$tit='T&Eacute;CNICO DE UNIDAD EJECUTORA';
					}
					if(tp==0){
						$('#titulo').html('<h4 class="modal-title" id="myModalLabel">APROBAR OPERACI&Oacute;N</h4>');
						$tit='APROBAR OPERACI&Oacute;N';
					}

					document.getElementById("id_p").value=proy_id;
					document.getElementById("resp").value=tp;

					var url = "<?php echo site_url("admin")?>/proy/get_proy";
					var codigo ='';
					var request;
					if(request){
						request.abort();
					}
					request = $.ajax({
						url:url,
						type:"POST",
						dataType:'json',
						data: "proy_id="+proy_id
					});

					request.done(function(response,textStatus,jqXHR){
						$('#nombre').html('<b>OPERACI&Oacute;N / PROYECTO : <b>'+response.proyecto[0]['proy_nombre']);
					});
					request.fail(function(jqXHR,textStatus,thrown){
						console.log("ERROR: "+ textStatus);
					});
					request.always(function(){
						//console.log("termino la ejecuicion de ajax");
					});
					e.preventDefault();
					// =============================VALIDAR EL FORMULARIO DE MODIFICACION
					$("#mod_aperenviar").on("click",function(e){
						var $valid = $("#mod_formaper").valid();
						if (!$valid) {
							$validator.focusInvalid();
						} else {
							//==========================================================
							//var aper_programa = document.getElementById("modaper_programa").value
							var id_p = document.getElementById("id_p").value 
							var tp = document.getElementById("resp").value 

							alertify.confirm(""+$tit+" ?", function (a) {
                                if (a) {
                                   var url = "<?php echo site_url("")?>/mod/derivar_proy";
									$.ajax({
										type:"post",
										url:url,
										data:{id_p:id_p,tp:tp},
										success:function(data){
											window.location.reload(true);
										}
									});
                                } else {
                                    alertify.error("OPCI\u00D3N CANCELADA");
                                }
                            });

						}
					});
				});
			});
		</script>
	</body>
</html>
