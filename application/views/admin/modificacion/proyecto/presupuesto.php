<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
		<title> <?php echo $this->session->userdata('name')?> </title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css"> 
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<!--estiloh-->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css"> 
		    <!--para las alertas-->
    	<meta name="viewport" content="width=device-width">
		<!--fin de stiloh-->
          	<style>
			/*////scroll tablas/////*/
			table{font-size: 10px;
            width: 100%;
            max-width:1550px;;
			    overflow-x: scroll;
            }
			</style>
	</head>
	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->

		<!-- HEADER -->
		<header id="header">
			<div id="logo-group">
				<span id="logo"> <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="SmartAdmin"> </span>
			</div>
			<!-- pulled right: nav area -->
			<div class="pull-right">
				<!-- collapse menu button -->
				<div id="hide-menu" class="btn-header pull-right">
					<span> <a href="javascript:void(0);" data-action="toggleMenu" title="Menu"><i class="fa fa-reorder"></i></a> </span>
				</div>
				<!-- end collapse menu -->
				<!-- logout button -->
				<div id="logout" class="btn-header transparent pull-right">
					<span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Salir" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
				</div>
				<!-- end logout button -->
				<!-- search mobile button (this is hidden till mobile view port) -->
				<div id="search-mobile" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
				</div>
				<!-- end search mobile button -->
				<!-- fullscreen button -->
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Pantalla Completa"><i class="fa fa-arrows-alt"></i></a> </span>
				</div>
				<!-- end fullscreen button -->
			</div>
			<!-- end pulled right: nav area -->
		</header>
		<!-- END HEADER -->

		<!-- Left panel : Navigation area -->
		<aside id="left-panel">

			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
					
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                              <span>
                                  <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                              </span>
						<i class="fa fa-angle-down"></i>
					</a> 
					
				</span>
			</div>

			<nav>
				<ul>
					<li class="">
	                <a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MENÚ PRINCIPAL"><i
	                        class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
	            	</li>
		            <li class="text-center">
		                <a href="#" title="PROGRAMACION"> <span class="menu-item-parent">MODIFICACIONES</span></a>
		            </li>
				<?php
		                for($i=0;$i<count($enlaces);$i++)
		                {
		                    if(count($subenlaces[$enlaces[$i]['o_child']])>0)
		                    {
		            ?>
		            <li>
		              	<a href="#" >
		              		<i class="<?php echo $enlaces[$i]['o_image']?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
		              	<ul >
		              	<?php
		                foreach ($subenlaces[$enlaces[$i]['o_child']] as $item) {
		                ?>
		                <li><a href="<?php echo base_url($item['o_url']); ?>"><?php echo $item['o_titulo']; ?></a></li>
		                <?php } ?>
		                </ul>
		            </li>
		            <?php 
		            }
		        } ?>
				</ul>
			</nav>
			<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>

		</aside>
		<!-- MAIN PANEL -->
		<div id="main" role="main">

			<!-- RIBBON -->
			<div id="ribbon">

				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>

				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li>Modificaciones</li><li>Modificaciones del POA</li><li>Modificaciones de la Operaci&oacute;n</li>
				</ol>
			</div>
			<!-- END RIBBON -->

					<?php
						$attributes = array('class' => 'form-horizontal', 'id' => 'formulario','name' =>'formulario','enctype' => 'multipart/form-data');
						echo validation_errors();
						echo form_open('admin/mod/valida_pr', $attributes);
					?>
			<!-- MAIN CONTENT -->
			<div id="content">
				<!-- widget grid -->
				<section id="widget-grid" class="">

					<div class="row">

						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<section id="widget-grid" class="well">
									<div class="">
										<h1> PROGRAMA : <small><?php echo $proyecto[0]['aper_programa'].''.$proyecto[0]['aper_proyecto'].''.$proyecto[0]['aper_actividad']?></small>
										<h1> <?php echo $titulo_proy;?> : <small><?php echo $proyecto[0]['proy_nombre']?></small></h1>
									</div>
								</section>
								<div class="jarviswidget jarviswidget-color-darken" >
									<header>
										<span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
										<h2 class="font-md"><strong>PRESUPUESTO : <?php echo $gestiones.'---'.$fase_proyecto[0]['pfec_fecha_inicio'] ?> - <?php echo $fase_proyecto[0]['pfec_fecha_fin'] ?></strong></h2>				
									</header>
										<!-- widget content -->
									<div class="widget-body">
										<div class="row">

											<form id="formulario"  name="formulario" novalidate="novalidate" method="post">
												<input class="form-control" type="hidden" name="id_p" value="<?php echo $proyecto[0]['proy_id']?>">
												<input class="form-control" type="hidden" name="id_f" value="<?php echo $fase_proyecto[0]['id']?>">
												<input class="form-control" type="hidden" name="gest" id="gest" value="<?php echo $gestiones?>">
												<input class="form-control" type="hidden" name="gestion" id="gestion" value="<?php echo $fecha?>">
												<input class="form-control" type="hidden" name="montos" id="montos" value="0">
												<input class="form-control" type="hidden" name="v1" id="v1" value="<?php echo $v1; ?>">	
												<input class="form-control" type="hidden" name="v2" id="v2" value="<?php echo $v2; ?>">	
												<input class="form-control" type="hidden" name="v3" id="v3" value="<?php echo $v3; ?>">	
												
												<div id="" class="col-sm-12">
																<div class="well">
																	<div class="row">
																	<div class="col-sm-2">
																		<div class="form-group">
																			<label><font size="1"><b>TOTAL FASE PRESUPUESTO (INICIAL)</b></font></label>
																			<input class="form-control" type="hidden" name="ppt1"  value="<?php echo $fase_proyecto[0]['pfec_ptto_fase'] ?>">
																			<input class="form-control" type="text" value="<?php echo $fase_proyecto[0]['pfec_ptto_fase'] ?>" disabled="true"><br>
																			<label><font size="1"><b>TOTAL FASE PRESUPUESTO</b></font></label>
																			<input class="form-control" type="text" name="ppt"  value="<?php echo round($fase_proyecto[0]['pfec_ptto_fase'],3) ?>" onblur="javascript:suma_presupuesto();" onkeypress="if (this.value.length < 25) { return numerosDecimales(event);}else{return false; }">		
																		</div>
																	</div>
																		<?php 
																			if($gestiones>=1)
																			{
																				?>
																				<div class="col-sm-2">
																					<div class="form-group">
																						<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion1[0]['g_id'];?> (INICIAL)</b></font></label>
																						<input class="form-control" type="hidden" name="pt1a" id="pt1a" value="<?php echo round($fase_gestion1[0]['pfecg_ppto_total'],3) ?>">
																						<input class="form-control" type="text" value="<?php echo round($fase_gestion1[0]['pfecg_ppto_total'],3) ?>" disabled="true"><br>
																						<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion1[0]['g_id'];?></b></font></label>
																						<input class="form-control" type="text" name="pt1" id="pt1" value="<?php echo round($fase_gestion1[0]['pfecg_ppto_total'],3) ?>" onblur="javascript:suma_presupuesto();" onkeypress="if (this.value.length < 25) { return numerosDecimales(event);}else{return false; }">
																					</div>
																				</div>

																				<?php
																				if($gestiones>=2)
																				{
																					?>
																						<div class="col-sm-2">
																							<div class="form-group">
																								<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion2[0]['g_id'];?> (INICIAL)</b></font></label>
																								<input class="form-control" type="hidden" name="pt2a" id="pt2a" value="<?php echo round($fase_gestion2[0]['pfecg_ppto_total'],3) ?>">
																								<input class="form-control" type="text" value="<?php echo round($fase_gestion2[0]['pfecg_ppto_total'],3) ?>" disabled="true"><br>
																								<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion2[0]['g_id'];?></b></font></label>
																								<input class="form-control" type="text" name="pt2" id="pt2" value="<?php echo round($fase_gestion2[0]['pfecg_ppto_total'],3) ?>" onblur="javascript:suma_presupuesto();" onkeypress="if (this.value.length < 25) { return numerosDecimales(event);}else{return false; }">
											                                                  	</select>
																							</div>
																						</div>
																					<?php
																					if($gestiones>=3)
																					{
																						?>
																							<div class="col-sm-2">
																								<div class="form-group">
																									<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion3[0]['g_id'];?> (INICIAL)</b></font></label>
																									<input class="form-control" type="hidden" name="pt3a" id="pt3a" value="<?php echo round($fase_gestion3[0]['pfecg_ppto_total'],3) ?>">
																									<input class="form-control" type="text" value="<?php echo round($fase_gestion3[0]['pfecg_ppto_total'],3) ?>" disabled="true"><br>
																									<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion3[0]['g_id'];?></b></font></label>
																									<input class="form-control" type="text" name="pt3" id="pt3" value="<?php echo round($fase_gestion3[0]['pfecg_ppto_total'],3) ?>" onblur="javascript:suma_presupuesto();" onkeypress="if (this.value.length < 25) { return numerosDecimales(event);}else{return false; }" >
																								</div>
																							</div>
																					
																					<?php
																						if($gestiones>=4)
																						{
																							?>
																								<div class="col-sm-2">
																									<div class="form-group">
																										<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion4[0]['g_id'];?> (INICIAL)</b></font></label>
																										<input class="form-control" type="hidden" name="pt4a" id="pt4a" value="<?php echo round($fase_gestion4[0]['pfecg_ppto_total'],3) ?>">
																										<input class="form-control" type="text" value="<?php echo round($fase_gestion4[0]['pfecg_ppto_total'],3) ?>" disabled="true"><br>
																										<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion4[0]['g_id'];?></b></font></label>
																										<input class="form-control" type="text" name="pt4" id="pt4" value="<?php echo round($fase_gestion4[0]['pfecg_ppto_total'],3) ?>" onblur="javascript:suma_presupuesto();" onkeypress="if (this.value.length < 25) { return numerosDecimales(event);}else{return false; }" >
																									</div>
																								</div>
																						
																						<?php
																							if($gestiones>=5)
																							{
																								?>
																									<div class="col-sm-2">
																										<div class="form-group">
																											<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion5[0]['g_id'];?> (INICIAL)</b></font></label>
																											<input class="form-control" type="hidden" name="pt5a" id="pt5a" value="<?php echo round($fase_gestion5[0]['pfecg_ppto_total'],3) ?>">
																											<input class="form-control" type="text" value="<?php echo round($fase_gestion5[0]['pfecg_ppto_total'],3) ?>" disabeld="true"><br>
																											<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion5[0]['g_id'];?></b></font></label>
																											<input class="form-control" type="text" name="pt5" id="pt5" value="<?php echo round($fase_gestion5[0]['pfecg_ppto_total'],3) ?>" onblur="javascript:suma_presupuesto();" onkeypress="if (this.value.length < 25) { return numerosDecimales(event);}else{return false; }">
																										</div>
																									</div>
																							
																							<?php
																								if($gestiones>=6)
																								{
																									?>
																										<div class="col-sm-2">
																											<div class="form-group">
																												<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion6[0]['g_id'];?> (INICIAL)</b></font></label>
																												<input class="form-control" type="hidden" name="pt6a" id="pt6a" value="<?php echo round($fase_gestion6[0]['pfecg_ppto_total'],3) ?>">
																												<input class="form-control" type="text" value="<?php echo round($fase_gestion6[0]['pfecg_ppto_total'],3) ?>" disabled="true"><br>
																												<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion6[0]['g_id'];?></b></font></label>
																												<input class="form-control" type="text" name="pt6" id="pt6" value="<?php echo round($fase_gestion6[0]['pfecg_ppto_total'],3) ?>" onblur="javascript:suma_presupuesto();" onkeypress="if (this.value.length < 25) { return numerosDecimales(event);}else{return false; }" >
																											</div>
																										</div>

																								
																								<?php
																									if($gestiones>=7)
																									{
																										?>
																											<div class="col-sm-2">
																												<div class="form-group">
																													<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion7[0]['g_id'];?> (INICIAL)</b></font></label>
																													<input class="form-control" type="hidden" name="pt7a" id="pt7a" value="<?php echo round($fase_gestion7[0]['pfecg_ppto_total'],3) ?>">
																													<input class="form-control" type="text" value="<?php echo round($fase_gestion7[0]['pfecg_ppto_total'],3) ?>" disabled="true"><br>
																													<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion7[0]['g_id'];?></b></font></label>
																													<input class="form-control" type="text" name="pt7" id="pt7" value="<?php echo round($fase_gestion7[0]['pfecg_ppto_total'],3) ?>" onblur="javascript:suma_presupuesto();" onkeypress="if (this.value.length < 25) { return numerosDecimales(event);}else{return false; }"  >
																												</div>
																											</div>
																									
																									<?php
																										if($gestiones>=8)
																										{
																											?>
																												<div class="col-sm-2">
																													<div class="form-group">
																														<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion8[0]['g_id'];?> (INICIAL)</b></font></label>
																														<input class="form-control" type="hidden" name="pt8a" id="pt8a" value="<?php echo round($fase_gestion8[0]['pfecg_ppto_total'],3) ?>">
																														<input class="form-control" type="text" value="<?php echo round($fase_gestion8[0]['pfecg_ppto_total'],3) ?>" disabled="true"><br>
																														<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion8[0]['g_id'];?></b></font></label>
																														<input class="form-control" type="text" name="pt8" id="pt8" value="<?php echo round($fase_gestion8[0]['pfecg_ppto_total'],3) ?>" onblur="javascript:suma_presupuesto();" onkeypress="if (this.value.length < 25) { return numerosDecimales(event);}else{return false; }"  >
																													</div>
																												</div>
																										
																										<?php
																											if($gestiones>=9)
																											{
																												?>
																													<div class="col-sm-2">
																														<div class="form-group">
																															<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion9[0]['g_id'];?> (INICIAL)</b></font></label>
																															<input class="form-control" type="hidden" name="pt9a" id="pt9a" value="<?php echo round($fase_gestion9[0]['pfecg_ppto_total'],3) ?>">
																															<input class="form-control" type="text" value="<?php echo round($fase_gestion9[0]['pfecg_ppto_total'],3) ?>" disabled="true"><br>
																															<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion9[0]['g_id'];?></b></font></label>
																															<input class="form-control" type="text" name="pt9" id="pt9" value="<?php echo round($fase_gestion9[0]['pfecg_ppto_total'],3) ?>" onblur="javascript:suma_presupuesto();" onkeypress="if (this.value.length < 25) { return numerosDecimales(event);}else{return false; }"  >
																														</div>
																													</div>
																											
																											<?php
																												if($gestiones>=10)
																												{
																													?>
																														<div class="col-sm-2">
																															<div class="form-group">
																																<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion10[0]['g_id'];?> (INICIAL)</b></font></label>
																																<input class="form-control" type="hidden" name="pt10a" id="pt10a" value="<?php echo round($fase_gestion10[0]['pfecg_ppto_total'],3) ?>">
																																<input class="form-control" type="text" value="<?php echo round($fase_gestion10[0]['pfecg_ppto_total'],3) ?>" disabled="true"><br>
																																<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion10[0]['g_id'];?></b></font></label>
																																<input class="form-control" type="text" name="pt10" id="pt10" value="<?php echo round($fase_gestion10[0]['pfecg_ppto_total'],3) ?>" onblur="javascript:suma_presupuesto();" onkeypress="if (this.value.length < 25) { return numerosDecimales(event);}else{return false; }"  >
																															</div>
																														</div>
																												
																												<?php
																													if($gestiones>=11)
																													{
																														?>
																															<div class="col-sm-2">
																																<div class="form-group">
																																	<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion11[0]['g_id'];?> (INICIAL)</b></font></label>
																																	<input class="form-control" type="hidden" name="pt11a" id="pt11a" value="<?php echo round($fase_gestion11[0]['pfecg_ppto_total'],3) ?>">
																																	<input class="form-control" type="text" value="<?php echo round($fase_gestion11[0]['pfecg_ppto_total'],3) ?>" disabled="true"><br>
																																	<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion11[0]['g_id'];?></b></font></label>
																																	<input class="form-control" type="text" name="pt11" id="pt11" value="<?php echo round($fase_gestion11[0]['pfecg_ppto_total'],3) ?>" onblur="javascript:suma_presupuesto();" onkeypress="if (this.value.length < 25) { return numerosDecimales(event);}else{return false; }"  >
																																</div>
																															</div>
																													
																													<?php
																														if($gestiones>=12)
																														{
																															?>
																																<div class="col-sm-2">
																																	<div class="form-group">
																																		<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion12[0]['g_id'];?> (INICIAL)</b></font></label>
																																		<input class="form-control" type="hidden" name="pt12a" id="pt12a" value="<?php echo round($fase_gestion12[0]['pfecg_ppto_total'],3) ?>">
																																		<input class="form-control" type="text" value="<?php echo round($fase_gestion12[0]['pfecg_ppto_total'],3) ?>" disabled="true"><br>
																																		<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion12[0]['g_id'];?></b></font></label>
																																		<input class="form-control" type="text" name="pt12" id="pt12" value="<?php echo round($fase_gestion12[0]['pfecg_ppto_total'],3) ?>" onblur="javascript:suma_presupuesto();" onkeypress="if (this.value.length < 25) { return numerosDecimales(event);}else{return false; }"  >
																																	</div>
																																</div>
																														
																														<?php
																															if($gestiones>=13)
																															{
																																?>
																																	<div class="col-sm-2">
																																		<div class="form-group">
																																			<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion13[0]['g_id'];?> (INICIAL)</b></font></label>
																																			<input class="form-control" type="hidden" name="pt13a" id="pt13a" value="<?php echo round($fase_gestion13[0]['pfecg_ppto_total'],3) ?>">
																																			<input class="form-control" type="text" value="<?php echo round($fase_gestion13[0]['pfecg_ppto_total'],3) ?>" disabled="true"><br>
																																			<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion13[0]['g_id'];?></b></font></label>
																																			<input class="form-control" type="text" name="pt13" id="pt13" value="<?php echo round($fase_gestion13[0]['pfecg_ppto_total'],3) ?>" onblur="javascript:suma_presupuesto();" onkeypress="if (this.value.length < 25) { return numerosDecimales(event);}else{return false; }"  >
																																		</div>
																																	</div>
																															
																															<?php
																																if($gestiones>=14)
																																{
																																	?>
																																		<div class="col-sm-2">
																																			<div class="form-group">
																																				<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion14[0]['g_id'];?> (INICIAL)</b></font></label>
																																				<input class="form-control" type="hidden" name="pt14a" id="pt14a" value="<?php echo round($fase_gestion14[0]['pfecg_ppto_total'],3) ?>">
																																				<input class="form-control" type="text" value="<?php echo round($fase_gestion14[0]['pfecg_ppto_total'],3) ?>" disabled="true"><br>
																																				<label><font size="1" color="blue"><b>GESTION <?php echo $fase_gestion14[0]['g_id'];?></b></font></label>
																																				<input class="form-control" type="text" name="pt14" id="pt14" value="<?php echo round($fase_gestion14[0]['pfecg_ppto_total'],3) ?>" onblur="javascript:suma_presupuesto();" onkeypress="if (this.value.length < 25) { return numerosDecimales(event);}else{return false; }"  >
																																			</div>
																																		</div>
																																
																																<?php
																																}
																															}
																														}
																													}
																												}
																											}
																										}
																									}
																								}
																							}
																						}
																					}
																				}

																			}
																		?>
																	</div>
																</div>
																<div class="form-actions">
																	<a href="<?php echo base_url().'index.php/admin/mod/proyecto_mod/'.$proyecto[0]['proy_id'].'/'.$v1.'/'.$v2.'/'.$v3.'' ?>" class="btn btn-lg btn-default" title="Volver atras">ATRAS</a>
																	<input type="button" value="MODIFICAR PRESUPUESTO" class="btn btn-primary btn-lg" onclick="valida_envia()" title="VALIDAR MODIFICACION DATOS">
																</div>
															</div> <!-- end well -->
															
												</div>
											</form>
										</div>
				
									</div>
									<!-- end widget content -->

								<!-- end widget div -->
							</article>
							
					</div>
					<!-- end row -->
				</section>
				<!-- end widget grid -->					
			</div>
			<!-- END MAIN CONTENT -->

		</div>
		<!-- END MAIN PANEL -->

		<!-- PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->
		<script type="text/javascript">
		//------------------------------------------
			function suma_presupuesto()
			{ 
					ptotal = parseFloat($('[name="ppt"]').val());
					if("<?php echo $gestiones?>">=1)
					{	
						a1 = parseFloat($('[name="pt1"]').val());
						$('[name="montos"]').val((a1).toFixed(2) );

						if("<?php echo $gestiones?>">=2)
						{
							b1 = parseFloat($('[name="pt2"]').val());
							$('[name="montos"]').val((a1+b1).toFixed(2) );

							if("<?php echo $gestiones?>">=3)
							{
								c1 = parseFloat($('[name="pt3"]').val());
								$('[name="montos"]').val((a1+b1+c1).toFixed(2) );

								if("<?php echo $gestiones?>">=4)
								{	
									d1 = parseFloat($('[name="pt4"]').val());
									$('[name="montos"]').val((a1+b1+c1+d1).toFixed(2) );

									if("<?php echo $gestiones?>">=5)
									{
										e1 = parseFloat($('[name="pt5"]').val());
										$('[name="montos"]').val((a1+b1+c1+d1+e1).toFixed(2) );

										if("<?php echo $gestiones?>">=6)
										{
											f1 = parseFloat($('[name="pt6"]').val());
											$('[name="montos"]').val((a1+b1+c1+d1+e1+f1).toFixed(2) );

											if("<?php echo $gestiones?>">=7)
											{
												g1 = parseFloat($('[name="pt7"]').val());
												$('[name="montos"]').val((a1+b1+c1+d1+e1+f1+g1).toFixed(2) );

												
												if("<?php echo $gestiones?>">=8)
												{
													h1 = parseFloat($('[name="pt8"]').val());
													$('[name="montos"]').val((a1+b1+c1+d1+e1+f1+g1+h1).toFixed(2) );

													if("<?php echo $gestiones?>">=9)
													{
														i1 = parseFloat($('[name="pt9"]').val());
														$('[name="montos"]').val((a1+b1+c1+d1+e1+f1+g1+h1+i1).toFixed(2) );

														if("<?php echo $gestiones?>">=10)
														{
															j1 = parseFloat($('[name="pt10"]').val());
															$('[name="montos"]').val((a1+b1+c1+d1+e1+f1+g1+h1+i1+j1).toFixed(2) );

															if("<?php echo $gestiones?>">=11)
															{
																k1 = parseFloat($('[name="pt11"]').val());
																$('[name="montos"]').val((a1+b1+c1+d1+e1+f1+g1+h1+i1+j1+k1).toFixed(2) );

																if("<?php echo $gestiones?>">=12)
																{
																	l1 = parseFloat($('[name="pt12"]').val());
																	$('[name="montos"]').val((a1+b1+c1+d1+e1+f1+g1+h1+i1+j1+k1+l1).toFixed(2) );

																	if("<?php echo $gestiones?>">=13)
																	{
																		m1 = parseFloat($('[name="pt13"]').val());
																		$('[name="montos"]').val((a1+b1+c1+d1+e1+f1+g1+h1+i1+j1+k1+l1+m1).toFixed(2) );

																		if("<?php echo $gestiones?>">=14)
																		{
																			n1 = parseFloat($('[name="pt14"]').val());
																			$('[name="montos"]').val((a1+b1+c1+d1+e1+f1+g1+h1+i1+j1+k1+l1+m1+n1).toFixed(2) );

																		}
																	}
																}
															}

														}

													}

												}
											}
										}
									}
								}
							}
						}

					}

			}
		</script>

		<!--================================================== -->
		<script>
            function valida_envia()
            { 
               
                if (document.formulario.gest.value>=1) /////// 1 GESTION
                  { 
                      /*-------------------- verificando gestiones 1-----------------*/
                      if (document.formulario.pt1.value=="") /////// gestion 1
	                  { 
	                      alert("Ingrese Campo TOTAL PRESUPUESTO <?php echo $fecha ?> ") 
	                      document.formulario.pt1.focus() 
	                      return 0; 
	                  }

	                  /*-------------------- end verificando gestiones 1-----------------*/
	                  /*-------------------- verificando valores 1-----------------*/
	                  if(parseFloat(document.formulario.pt1.value)>parseFloat(document.formulario.ppt.value))
	                  {
	                  	  alert("Error!! PRESUPUESTO <?php echo $fecha ?>") 
	                      document.formulario.pt1.focus() 
	                      return 0; 
	                  }

	                  /*-------------------- end verificando valores 1-----------------*/

	                  if (document.formulario.gest.value>=2) /////// 2 GESTION
	                  {
	                  	  /*-------------------- verificando gestiones 2-----------------*/
	                  	  if (document.formulario.pt2.value=="") /////// gestion 1
		                  { 
		                      alert("Ingrese Campo TOTAL PRESUPUESTO <?php echo $fecha+1 ?>") 
		                      document.formulario.pt2.focus() 
		                      return 0; 
		                  }
		                  /*-------------------- end verificando gestiones 2-----------------*/
		                  /*-------------------- verificando valores 2-----------------*/
		                  if(parseFloat(document.formulario.pt1.value)+parseFloat(document.formulario.pt2.value)>parseFloat(document.formulario.ppt.value))
		                  {
		                  	  alert("Error!! PRESUPUESTO <?php echo $fecha+1 ?>") 
		                      document.formulario.pt2.focus() 
		                      return 0; 
		                  }
		                  /*-------------------- end verificando valores 2-----------------*/

			                  if (document.formulario.gest.value>=3) /////// 3 GESTION
			                  {
			                  	  /*-------------------- verificando gestiones 3-----------------*/
			                  	  if (document.formulario.pt3.value=="") /////// gestion 1
				                  { 
				                      alert("Ingrese Campo TOTAL PRESUPUESTO <?php echo $fecha+2 ?>") 
				                      document.formulario.pt3.focus() 
				                      return 0; 
				                  }
				                  /*-------------------- end verificando gestiones 3-----------------*/
				                  /*-------------------- verificando valores 3-----------------*/
				                  if(parseFloat(document.formulario.pt1.value)+parseFloat(document.formulario.pt2.value)+parseFloat(document.formulario.pt3.value)>parseFloat(document.formulario.ppt.value))
				                  {
				                  	  alert("Error!! PRESUPUESTO <?php echo $fecha+2 ?>") 
				                      document.formulario.pt3.focus() 
				                      return 0; 
				                  }

				                  /*-------------------- end verificando valores 3-----------------*/

				                  	if (document.formulario.gest.value>=4) /////// 4 GESTION
					                  {
					                  	  /*-------------------- verificando gestiones 4-----------------*/
					                  	  if (document.formulario.pt4.value=="") /////// gestion 1
						                  { 
						                      alert("Ingrese Campo TOTAL PRESUPUESTO <?php echo $fecha+3 ?>") 
						                      document.formulario.pt4.focus() 
						                      return 0; 
						                  }
						                  /*--------------------end verificando gestiones 4-----------------*/
						                  /*-------------------- verificando valores 4-----------------*/
						                  if(parseFloat(document.formulario.pt1.value)+parseFloat(document.formulario.pt2.value)+parseFloat(document.formulario.pt3.value)+parseFloat(document.formulario.pt4.value)>parseFloat(document.formulario.ppt.value))
						                  {
						                  	  alert("Error!! PRESUPUESTO <?php echo $fecha+3 ?>") 
						                      document.formulario.pt4.focus() 
						                      return 0; 
						                  }
						                  /*-------------------- end verificando valores 4-----------------*/

						                  	if (document.formulario.gest.value>=5) /////// 5 GESTION
							                  {
							                  	  /*-------------------- verificando gestiones 5-----------------*/
							                  	  if (document.formulario.pt5.value=="") /////// gestion 1
								                  { 
								                      alert("Ingrese Campo TOTAL PRESUPUESTO <?php echo $fecha+4 ?>") 
								                      document.formulario.pt5.focus() 
								                      return 0; 
								                  }
								                  /*-------------------- end verificando gestiones 5-----------------*/
								                  /*-------------------- verificando valores 5-----------------*/
								                  if(parseFloat(document.formulario.pt1.value)+parseFloat(document.formulario.pt2.value)+parseFloat(document.formulario.pt3.value)+parseFloat(document.formulario.pt4.value)+parseFloat(document.formulario.pt5.value)>parseFloat(document.formulario.ppt.value))
								                  {
								                  	  alert("Error!! PRESUPUESTO <?php echo $fecha+4 ?>") 
								                      document.formulario.pt5.focus() 
								                      return 0; 
								                  }
								                  /*-------------------- end verificando valores 4-----------------*/

								                  	if (document.formulario.gest.value>=6) /////// 6 GESTION
									                  {
									                  	  /*-------------------- verificando gestiones 6-----------------*/
									                  	  if (document.formulario.pt6.value=="") /////// gestion 1
										                  { 
										                      alert("Ingrese Campo TOTAL PRESUPUESTO <?php echo $fecha+5 ?>") 
										                      document.formulario.pt6.focus() 
										                      return 0; 
										                  }
										                  /*--------------------end verificando gestiones 6-----------------*/
										                  /*-------------------- verificando valores 6-----------------*/
										                  if(parseFloat(document.formulario.pt1.value)+parseFloat(document.formulario.pt2.value)+parseFloat(document.formulario.pt3.value)+parseFloat(document.formulario.pt4.value)+parseFloat(document.formulario.pt5.value)+parseFloat(document.formulario.pt6.value)>parseFloat(document.formulario.ppt.value))
										                  {
										                  	  alert("Error!! PRESUPUESTO <?php echo $fecha+5 ?>") 
										                      document.formulario.pt6.focus() 
										                      return 0; 
										                  }
										                  /*-------------------- end verificando valores 6-----------------*/

										                  if (document.formulario.gest.value>=7) /////// 7 GESTION
										                  {
											                  	 /*-------------------- verificando gestiones 6-----------------*/
											                  	  if (document.formulario.pt7.value=="") /////// gestion 7
												                  { 
												                      alert("Ingrese Campo TOTAL PRESUPUESTO <?php echo $fecha+6 ?>") 
												                      document.formulario.pt7.focus() 
												                      return 0; 
												                  }
												                  /*--------------------end verificando gestiones 6-----------------*/
												                  /*-------------------- verificando valores 6-----------------*/
												                  if(parseFloat(document.formulario.pt1.value)+parseFloat(document.formulario.pt2.value)+parseFloat(document.formulario.pt3.value)+parseFloat(document.formulario.pt4.value)+parseFloat(document.formulario.pt5.value)+parseFloat(document.formulario.pt6.value)+parseFloat(document.formulario.pt7.value)>parseFloat(document.formulario.ppt.value))
												                  {
												                  	  alert("Error!! PRESUPUESTO <?php echo $fecha+6 ?>") 
												                      document.formulario.pt7.focus() 
												                      return 0; 
												                  }
												                  /*-------------------- end verificando valores 7-----------------*/
											               			 	if (document.formulario.gest.value>=8) /////// 8 GESTION
														                  {
															                  	 /*-------------------- verificando gestiones 6-----------------*/
															                  	  if (document.formulario.pt8.value=="") /////// gestion 7
																                  { 
																                      alert("Ingrese Campo TOTAL PRESUPUESTO <?php echo $fecha+7 ?>") 
																                      document.formulario.pt8.focus() 
																                      return 0; 
																                  }
																                  /*--------------------end verificando gestiones 6-----------------*/
																                  /*-------------------- verificando valores 6-----------------*/
																                  if(parseFloat(document.formulario.pt1.value)+parseFloat(document.formulario.pt2.value)+parseFloat(document.formulario.pt3.value)+parseFloat(document.formulario.pt4.value)+parseFloat(document.formulario.pt5.value)+parseFloat(document.formulario.pt6.value)+parseFloat(document.formulario.pt7.value)+parseFloat(document.formulario.pt8.value)>parseFloat(document.formulario.ppt.value))
																                  {
																                  	  alert("Error!! PRESUPUESTO <?php echo $fecha+7 ?>") 
																                      document.formulario.pt8.focus() 
																                      return 0; 
																                  }
																                  /*-------------------- end verificando valores 8-----------------*/
																                  if (document.formulario.gest.value>=9) /////// 9 GESTION
																                  {
																	                  	 /*-------------------- verificando gestiones 6-----------------*/
																	                  	  if (document.formulario.pt9.value=="") /////// gestion 7
																		                  { 
																		                      alert("Ingrese Campo TOTAL PRESUPUESTO <?php echo $fecha+8 ?>") 
																		                      document.formulario.pt9.focus() 
																		                      return 0; 
																		                  }
																		                  /*--------------------end verificando gestiones 6-----------------*/
																		                  /*-------------------- verificando valores 6-----------------*/
																		                  if(parseFloat(document.formulario.pt1.value)+parseFloat(document.formulario.pt2.value)+parseFloat(document.formulario.pt3.value)+parseFloat(document.formulario.pt4.value)+parseFloat(document.formulario.pt5.value)+parseFloat(document.formulario.pt6.value)+parseFloat(document.formulario.pt7.value)+parseFloat(document.formulario.pt8.value)+parseFloat(document.formulario.pt9.value)>parseFloat(document.formulario.ppt.value))
																		                  {
																		                  	  alert("Error!! PRESUPUESTO <?php echo $fecha+8 ?>") 
																		                      document.formulario.pt9.focus() 
																		                      return 0; 
																		                  }

																		                  /*-------------------- end verificando valores 9-----------------*/
																		                  if (document.formulario.gest.value>=10) /////// 10 GESTION
																		                  {
																			                  	 /*-------------------- verificando gestiones 10-----------------*/
																			                  	  if (document.formulario.pt10.value=="") /////// gestion 10
																				                  { 
																				                      alert("Ingrese Campo TOTAL PRESUPUESTO <?php echo $fecha+9 ?>") 
																				                      document.formulario.pt10.focus() 
																				                      return 0; 
																				                  }
																				                  /*--------------------end verificando gestiones 10-----------------*/
																				                  /*-------------------- verificando valores 10-----------------*/
																				                  if(parseFloat(document.formulario.pt1.value)+parseFloat(document.formulario.pt2.value)+parseFloat(document.formulario.pt3.value)+parseFloat(document.formulario.pt4.value)+parseFloat(document.formulario.pt5.value)+parseFloat(document.formulario.pt6.value)+parseFloat(document.formulario.pt7.value)+parseFloat(document.formulario.pt8.value)+parseFloat(document.formulario.pt9.value)+parseFloat(document.formulario.pt10.value)>parseFloat(document.formulario.ppt.value))
																				                  {
																				                  	  alert("Error!! PRESUPUESTO <?php echo $fecha+9 ?>") 
																				                      document.formulario.pt10.focus() 
																				                      return 0; 
																				                  }
																				                  /*-------------------- end verificando valores 11-----------------*/
																				                  if (document.formulario.gest.value>=11) /////// 11 GESTION
																				                  {
																					                  	 /*-------------------- verificando gestiones 11-----------------*/
																					                  	  if (document.formulario.pt11.value=="") /////// gestion 11
																						                  { 
																						                      alert("Ingrese Campo TOTAL PRESUPUESTO <?php echo $fecha+10 ?>") 
																						                      document.formulario.pt11.focus() 
																						                      return 0; 
																						                  }
																						                  /*--------------------end verificando gestiones 11-----------------*/
																						                  /*-------------------- verificando valores 11-----------------*/
																						                  if(parseFloat(document.formulario.pt1.value)+parseFloat(document.formulario.pt2.value)+parseFloat(document.formulario.pt3.value)+parseFloat(document.formulario.pt4.value)+parseFloat(document.formulario.pt5.value)+parseFloat(document.formulario.pt6.value)+parseFloat(document.formulario.pt7.value)+parseFloat(document.formulario.pt8.value)+parseFloat(document.formulario.pt9.value)+parseFloat(document.formulario.pt10.value)+parseFloat(document.formulario.pt11.value)>parseFloat(document.formulario.ppt.value))
																						                  {
																						                  	  alert("Error!! PRESUPUESTO <?php echo $fecha+10 ?>") 
																						                      document.formulario.pt11.focus() 
																						                      return 0; 
																						                  }
																						                  /*-------------------- end verificando valores 12-----------------*/
																							               if (document.formulario.gest.value>=12) /////// 12 GESTION
																						                  {
																							                  	 /*-------------------- verificando gestiones 12-----------------*/
																							                  	  if (document.formulario.pt12.value=="") /////// gestion 12
																								                  { 
																								                      alert("Ingrese Campo TOTAL PRESUPUESTO <?php echo $fecha+11 ?>") 
																								                      document.formulario.pt12.focus() 
																								                      return 0; 
																								                  }
																								                  /*--------------------end verificando gestiones 12-----------------*/
																								                  /*-------------------- verificando valores 12-----------------*/
																								                  if(parseFloat(document.formulario.pt1.value)+parseFloat(document.formulario.pt2.value)+parseFloat(document.formulario.pt3.value)+parseFloat(document.formulario.pt4.value)+parseFloat(document.formulario.pt5.value)+parseFloat(document.formulario.pt6.value)+parseFloat(document.formulario.pt7.value)+parseFloat(document.formulario.pt8.value)+parseFloat(document.formulario.pt9.value)+parseFloat(document.formulario.pt10.value)+parseFloat(document.formulario.pt11.value)+parseFloat(document.formulario.pt12.value)>parseFloat(document.formulario.ppt.value))
																								                  {
																								                  	  alert("Error!! PRESUPUESTO <?php echo $fecha+11 ?>") 
																								                      document.formulario.pt12.focus() 
																								                      return 0; 
																								                  }
																								                  /*-------------------- end verificando valores 12-----------------*/
																										               if (document.formulario.gest.value>=13) /////// 13 GESTION
																									                  {
																										                  	 /*-------------------- verificando gestiones 13-----------------*/
																										                  	  if (document.formulario.pt13.value=="") /////// gestion 13
																											                  { 
																											                      alert("Ingrese Campo TOTAL PRESUPUESTO <?php echo $fecha+12 ?>") 
																											                      document.formulario.pt13.focus() 
																											                      return 0; 
																											                  }
																											                  /*--------------------end verificando gestiones 11-----------------*/
																											                  /*-------------------- verificando valores 11-----------------*/
																											                  if(parseFloat(document.formulario.pt1.value)+parseFloat(document.formulario.pt2.value)+parseFloat(document.formulario.pt3.value)+parseFloat(document.formulario.pt4.value)+parseFloat(document.formulario.pt5.value)+parseFloat(document.formulario.pt6.value)+parseFloat(document.formulario.pt7.value)+parseFloat(document.formulario.pt8.value)+parseFloat(document.formulario.pt9.value)+parseFloat(document.formulario.pt10.value)+parseFloat(document.formulario.pt11.value)+parseFloat(document.formulario.pt12.value)+parseFloat(document.formulario.pt13.value)>parseFloat(document.formulario.ppt.value))
																											                  {
																											                  	  alert("Error!! PRESUPUESTO <?php echo $fecha+12 ?>") 
																											                      document.formulario.pt13.focus() 
																											                      return 0; 
																											                  }
																											                  /*-------------------- end verificando valores 10-----------------*/
																										               
																									                  }
																						                  }
																				                  }
																			               
																		                  }
																	               
																                  }
															               
														                  }
										                  }
									                  }
									                  
							                  }
					                  }
			                  }
	                  }
                  }

                 var OK = confirm("VALIDAR INFORMACION ?");
		                if (OK) {
		                      document.formulario.submit(); 
		                    } 
                
             	   	
            }
          </script>

		
		<script>
			if (!window.jQuery) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-2.0.2.min.js"><\/script>');
			}
		</script>

		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>

		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url();?>/assets/js/app.config.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/mis_js/validacion_form.js"></script>
		<script src = "<?php echo base_url(); ?>mis_js/control_session.js"></script>
		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url();?>/assets/js/bootstrap/bootstrap.min.js"></script>
		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url();?>/assets/js/notification/SmartNotification.min.js"></script>
		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url();?>/assets/js/smartwidgets/jarvis.widget.min.js"></script>
		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url();?>/assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<!-- SPARKLINES -->
		<script src="<?php echo base_url();?>/assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/select2/select2.min.js"></script>
		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
		<!-- browser msie issue fix -->
		<script src="<?php echo base_url();?>/assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url();?>/assets/js/plugin/fastclick/fastclick.min.js"></script>
		<!-- Demo purpose only -->
		<script src="<?php echo base_url();?>/assets/js/demo.min.js"></script>
		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url();?>/assets/js/app.min.js"></script>
		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<!-- Voice command : plugin -->
		<script src="<?php echo base_url();?>/assets/js/speech/voicecommand.min.js"></script>

	</body>
</html>
