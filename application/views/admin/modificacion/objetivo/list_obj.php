<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
		<title><?php echo $this->session->userdata('name')?></title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css"> 
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<!--estiloh-->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css"> 
		<script>
		  	function abreVentana(PDF)
			{
				var direccion;
				direccion = '' + PDF;
				window.open(direccion, "Reporte Modificaciones de objetivos Estrategicos" , "width=800,height=650,scrollbars=SI") ;
			}                                                  
        </script>
    	<meta name="viewport" content="width=device-width">
		<!--fin de stiloh-->
          	<style>
			/*////scroll tablas/////*/
			table{font-size: 10px;
            width: 100%;
            max-width:1550px;;
			    overflow-x: scroll;
            }
			</style>
			<?php
				$id=$this->uri->segment(5);
			?>
	</head>
	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->
		<!-- HEADER -->
		<header id="header">
			<div id="logo-group">
				<span id="logo"> <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="SmartAdmin"> </span>
			</div>
			<!-- pulled right: nav area -->
			<div class="pull-right">
				<!-- collapse menu button -->
				<div id="hide-menu" class="btn-header pull-right">
					<span> <a href="javascript:void(0);" data-action="toggleMenu" title="Menu"><i class="fa fa-reorder"></i></a> </span>
				</div>
				<!-- end collapse menu -->
				<!-- logout button -->
				<div id="logout" class="btn-header transparent pull-right">
					<span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Salir" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
				</div>
				<!-- end logout button -->
				<!-- search mobile button (this is hidden till mobile view port) -->
				<div id="search-mobile" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
				</div>
				<!-- end search mobile button -->
				<!-- fullscreen button -->
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Pantalla Completa"><i class="fa fa-arrows-alt"></i></a> </span>
				</div>
				<!-- end fullscreen button -->
			</div>
			<!-- end pulled right: nav area -->
		</header>
		<!-- END HEADER -->
		<!-- Left panel : Navigation area -->
		<aside id="left-panel">

			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
					
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                              <span>
                                  <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                              </span>
						<i class="fa fa-angle-down"></i>
					</a> 
					
				</span>
			</div>

			<nav>
				<ul>
					<li class="">
	                <a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MENÚ PRINCIPAL"><i
	                        class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
	            	</li>
		            <li class="text-center">
		                <a href="#" title="MODIFICACIONES"> <span class="menu-item-parent">MODIFICACIONES</span></a>
		            </li>
					<?php
			                for($i=0;$i<count($enlaces);$i++)
			                {
			                    if(count($subenlaces[$enlaces[$i]['o_child']])>0)
			                    {
			            ?>
			            <li>
			              	<a href="#" >
			              		<i class="<?php echo $enlaces[$i]['o_image']?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
			              	<ul >
			              	<?php
			                foreach ($subenlaces[$enlaces[$i]['o_child']] as $item) {
			                ?>
			                <li><a href="<?php echo base_url($item['o_url']); ?>"><?php echo $item['o_titulo']; ?></a></li>
			                <?php } ?>
			                </ul>
			            </li>
			            <?php 
			            }
			        } ?>
				</ul>
			</nav>
			<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>

		</aside>

		<!-- MAIN PANEL -->
		<div id="main" role="main">
			<!-- RIBBON -->
			<div id="ribbon">
				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>
				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li>Modificaciones</li><li>Modificaciones de Acciones de Corto Plazo y Productos Terminales</li>
				</ol>
			</div>
			<!-- END RIBBON -->
			<!-- MAIN CONTENT -->
			<div id="content">
				<!-- widget grid -->
				<article class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
		            <section id="widget-grid" class="well">
		                <div class="">
		                 <h1>LISTA DE PROGRAMAS PARA REGISTRO DE EJECUCIÓN DE ACCIONES DE CORTO PLAZO Y PRODUCTOS TERMINALES</h1>
		                </div>
		            </section>
		          </article>
		          <article class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
		            <section id="widget-grid" class="well">
		              <center>
		                <div class="dropdown">
		                  <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenu1" style="width:100%;" data-toggle="dropdown" aria-expanded="true">
		                    OPCIONES
		                    <span class="caret"></span>
		                  </button>
		                  <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
		                    <!-- <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:abreVentana('<?php // echo site_url("admin").'/mod/rep_objetivo/'.$poa_id.''?>');">IMPRIMIR PROGRAMA</a></li>  -->
		                    <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url().'index.php/admin/mod/redobj' ?>">VOLVER ATRAS</a></li>
		                  </ul>
		                </div>
		              </center>
		            </section>
		          </article>
		          <section id="widget-grid" class="">
		            <!-- row -->
		            <div class="row">
		              <!-- NEW WIDGET START -->
		              <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		              <div class="well">
					<?php $cont=0; $cont_obj=0;
					if(count(($lista_objetivos)!=0))
					{
					foreach($lista_objetivos as $rowoe)
					{	$cont_obj++;
					    ?>
					    <section id="widget-grid" class="well">
							<div class="">
								<h1><?php echo $cont_obj;?>.- ACCION DE MEDIANO PLAZO : <small><?php echo $rowoe['obje_objetivo']?></small></h1>
								<h1>TIPO DE INDICADOR : <small><?php if($rowoe['indi_id']==1){echo "ABSOLUTO";} else {echo "RELATIVO";}?></small></h1>
								</div>
						</section>
					    <?php
					    $nro=1; 
					    $dato_poa = $this->model_modificacion->dato_poa_id($poa_id);//datos del poa
					    $lista_objgestion = $this->model_modificacion->lista_ogestion_estarategico($rowoe['obje_id']);
					    $nro_og=count($lista_objgestion);
					    $ruta_img_sin_archivos = base_url() . 'assets/ifinal/1.png';
					    $ruta_arc = site_url("") . '/reg/arc/';//ruta de archivos
					    $ruta_mes = site_url("") . '/admin/lmes/';//ruta para el mes
					    $ruta_img_arc = base_url() . 'assets/ifinal/doc.jpg';
					    $ruta_img_mes = base_url() . 'assets/ifinal/formularios.jpg';
					    if($nro_og!=0)
					    {
					    	foreach ($lista_objgestion as $rowo) 
                            {
                            ?>
                            <div class="well">
                            <table class="table  table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th colspan="10" align="center" bgcolor="#555756"><font size="2" color="#ffffff">ACCIONES DE CORTO PLAZO</font></th>
                                </tr>
                                <tr>
                                    <th style="width:1%;" bgcolor="#555756"><font color="#ffffff">Nro.</font></th>
                                    <th bgcolor="#555756"><font size="1" color="#ffffff">MODIFICAR OBJETIVO</font></th>
                                    <th bgcolor="#555756"><font size="1" color="#ffffff">ARCHIVOS DE REGISTRO</font></th>
                                    <th bgcolor="#555756"><font size="1" color="#ffffff">HISTORIAL DE REGISTRO</font></th>
                                    <th style="width:20%;" bgcolor="#555756"><font size="1" color="#ffffff">ACCI&Oacute;N DE CORTO PLAZO</font></th>
                                    <th style="width:15%;" bgcolor="#555756"><font size="1" color="#ffffff">RESPONSABLE</font></th>
                                    <th style="width:15%;" bgcolor="#555756"><font size="1" color="#ffffff">UNIDAD ORGANIZACIONAL </font></th>
                                    <th bgcolor="#555756"><font size="1" color="#ffffff">PROGRAMAS</font></th>
                                    
                                </tr>
                                </thead>
                                    <tbody>
										<tr>
                                        <td>
											<?php 
												echo $nro;
												if($id==$rowo['o_id'])
												{ $cont++;
												?>
													<img src="<?php echo base_url(); ?>assets/Iconos/accept.png"/>
												<?php
												}
											?>
										</td>
                                        <td>
                                            <center><a href="<?php echo base_url().'index.php/admin/mod/mod_obj_gest/'.$poa_id.'/'.$rowoe['obje_id'].'/'.$rowo['o_id'].'' ?>" class="btn btn-primary" >MODIFICAR ACCI&Oacute;N<br> DE CORTO PLAZO</a></center>
                                        </td>
                                        <td><a href="<?php echo base_url(); ?>archivos/modificacion/MO--15972.pdf" title="ARCHIVOS">
											           <?php echo ' <img src="' . $ruta_img_arc . '" width="40" height="40" class="img-responsive"
											            title="REGISTRO SIN MODIFICAR">
											        </a>';?></td>
                                        <td><?php echo '<a href="' . $ruta_mes . $rowo['o_id'] . '" title="MODIFICACIONES">
											            <img src="' . $ruta_img_mes . '" width="40" height="40" class="img-responsive" title="MODIFICACIONES">
											        </a>';?></td>
                                        <td><?php echo $rowo['o_objetivo'];?></td>
                                        <td><?php echo $rowo['fun_nombre'].' '.$rowo['fun_paterno'].' '.$rowo['fun_materno'];?></td>
                                        <td><?php echo $rowo['unidad'];?></td>
                                        <td><?php echo $rowo['aper'];?></td>
                                        </tr>
                                        <?php
                                    $nro++;
                                    ?>
                                    </tbody>
                       				</table>
                       				<table class="table  table-bordered table-hover">
                                        <thead>
                                        <tr>
		                                    <th colspan="10" align="center" bgcolor="##568A89"><font size="2" color="#ffffff">PRODUCTO TERMINAL</font></th>
		                                </tr>
                                        <tr>
                                            <th style="width:1%;" bgcolor="##568A89"><font size="1" color="#ffffff">Nro.</font></th>
                                            <th bgcolor="#568A89"><font size="1" color="#ffffff">MODIFICAR PRODUCTO TERMINAL</font></th>
                                            <th bgcolor="#568A89"><font size="1" color="#ffffff">ARCHIVOS DE REGISTRO</font></th>
                                    		<th bgcolor="#568A89"><font size="1" color="#ffffff">HISTORIAL DE REGISTRO</font></th>
                                            <th style="width:20%;" bgcolor="##568A89"><font size="1" color="#ffffff">OBJ. PRODUCTO TERMINAL</font></th>
                                            <th style="width:15%;" bgcolor="##568A89"><font size="1" color="#ffffff">RESPONSABLE </font></th>
                                            <th style="width:15%;" bgcolor="##568A89"><font size="1" color="#ffffff">UNIDAD ORGANIZACIONAL </font></th>
                                            <th style="width:15%;" bgcolor="##568A89"><font size="1" color="#ffffff">PROGRAMAS </font></th>

                                        </tr>
                                        </thead>
                                        <tbody>
	                                    <?php
	                                    $lista_pterminal = $this->mp_terminal->lista_pterminal($rowo['o_id']);
	                                    $nrop=1;
	                                    foreach ($lista_pterminal as $row)
	                                    {
	                                    	?>
	                                    	<tr>
		                                    	<td>
													<?php 
														echo $nrop;
														if($id==$row['pt_id'])
														{ $cont++;
														?>
															<img src="<?php echo base_url(); ?>assets/Iconos/accept.png"/>
														<?php
														}
													?>
												</td>
	                                            <td>
	                                            <center><a href="<?php echo base_url().'index.php/admin/mod/mod_pt/'.$poa_id.'/'.$rowoe['obje_id'].'/'.$row['pt_id'].'' ?>" class="btn btn-default" >MODIFICAR <br>PRODUCTO TERMINAL.</a></center>
	                                           	</td>
	                                            <td ><?php echo '<a href="" title="ARCHIVOS">
											            <img src="' . $ruta_img_arc . '" width="40" height="40" class="img-responsive"
											            title="REGISTRO SIN MODIFICAR">
											        </a>';?></td>
	                                            <td ><?php echo '<a href="" title="MODIFICACIONES">
											            <img src="' . $ruta_img_mes . '" width="40" height="40" class="img-responsive" title="MODIFICACIONES">
											        </a>';?></td>
	                                            <td ><?php echo $row['pt_objetivo'];?></td>
	                                            <td ><?php echo $row['fun_nombre'].' '.$row['fun_paterno'].' '.$row['fun_materno'];?></td>
	                                            <td ><?php echo $row['unidad'];?></td>
	                                            <td><?php echo $rowo['aper'];?></td>
                                            </tr>
                                            <?php
                                            $nrop++;
	                                    }
	                                    ?>
	                                    </tbody>
	                                    </table>
	                                    </div>
	                                    <?php
                                }
							}
						}
					}
					else
					{
						echo "<center><font color=red>No tiene registro</font></center>";
					}
/*					if($cont>0) 
					{
						?>
						<hr>
						<form  name="formulario" id="formulario" method="post" action="<?php echo site_url("admin").'' ?>">
						<label><b>JUSTIFICACI&Oacute;N</b></label>
						<textarea rows="10" class="form-control" name="just_oe" id="just_oe" style="width:100%;"  title="JUSTIFICACION A LA MODIFICACION"></textarea> 

							<div class="form-actions">
								<a href="<?php echo base_url().'index.php/admin/mod/redobj' ?>" class="btn btn-lg btn-success" title="Volver atras">VOLVER ATRAS</a>
								<input type="button" value="GUARDAR JUSTIFICACI&Oacute;N" id="btsubmit" class="btn btn-primary btn-lg" onclick="valida_envia()" title="Guardar y Registrar Responsables">
							</div>
						</form>
						<?php
					}*/

					?>
					</table>
					</div>
		            </article>
		            </div>
		           </section>
				<!-- end widget grid -->					
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN PANEL -->
		<!-- PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->
		<script>
			if (!window.jQuery) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-2.0.2.min.js"><\/script>');
			}
		</script>
		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>
		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url();?>/assets/js/app.config.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/mis_js/validacion_form.js"></script>
		<script src = "<?php echo base_url(); ?>mis_js/control_session.js"></script>
		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url();?>/assets/js/bootstrap/bootstrap.min.js"></script>
		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url();?>/assets/js/notification/SmartNotification.min.js"></script>
		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url();?>/assets/js/smartwidgets/jarvis.widget.min.js"></script>
		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url();?>/assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<!-- SPARKLINES -->
		<script src="<?php echo base_url();?>/assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/select2/select2.min.js"></script>
		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
		<!-- browser msie issue fix -->
		<script src="<?php echo base_url();?>/assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url();?>/assets/js/plugin/fastclick/fastclick.min.js"></script>
		<!-- Demo purpose only -->
		<script src="<?php echo base_url();?>/assets/js/demo.min.js"></script>
		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url();?>/assets/js/app.min.js"></script>
		<script src="<?php echo base_url();?>/assets/js/speech/voicecommand.min.js"></script>
		<script type="text/javascript">
		    $(function () {
		        var id_p = '';
		        $('#modal_nuevo_ff').on('hidden.bs.modal', function () {
		            document.forms['form_ff'].reset();

		        });
		        $("#enviar_ff").on("click", function (e) {
		            //========================VALIDANDO FORMULARIO===================
		            var $validator = $("#form_ff").validate({
		                //////////////// DATOS GENERALES
		                rules: {
		                    fi2: { //// indicador
		                        required: true,
		                    },
		                    ff2: { //// indicador
		                        required: true,
		                    }
		                },
		                messages: {
		                    fi2: "Seleccione Fecha Inicial a modificar ",
		                    ff2: "Seleccione Fecha Final a modificar",
		                    
		                },
		                highlight: function (element) {
		                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		                },
		                unhighlight: function (element) {
		                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		                },
		                errorElement: 'span',
		                errorClass: 'help-block',
		                errorPlacement: function (error, element) {
		                    if (element.parent('.input-group').length) {
		                        error.insertAfter(element.parent());
		                    } else {
		                        error.insertAfter(element);
		                    }
		                }
		            });
		            var $valid = $("#form_ff").valid();
		            if (!$valid) {
		                $validator.focusInvalid();
		                //return false;
		            } else { 
		                //==========================================================
		                var id_p = document.getElementById("id_p").value;
		                var id_f = document.getElementById("id_f").value;
		                var fi1 = document.getElementById("fi1").value;
		                var ff1 = document.getElementById("ff1").value;
		                var fi2 = document.getElementById("fi2").value;
		                var ff2 = document.getElementById("ff2").value;
		                //=================== VERIFICAR SI EXISTE EL COD DE PROGRAMA ==============
		                
		                var url = "<?php echo site_url("admin")?>/mod/valida_plazo";
		                            $.ajax({
		                                type: "post",
		                                url: url,
		                                data: {
		                                    id_p: id_p,
		                                    id_f: id_f,
		                                    fi1: fi1,
		                                    ff1: ff1,
		                                    fi2: fi2,
		                                    ff2: ff2
		                                },
		                                success: function (data) {
		                                    window.location.reload(true);
		                                }
		                            });
		            }
		        });
		    });
		</script>
		<!--============================== MODIFICAR OBSERVACION =========================================-->
		
		
		<script type="text/javascript">
			// TABLA
			$(document).ready(function() {
				pageSetUp();
				/* BASIC ;*/
				var responsiveHelper_dt_basic = undefined;

				var breakpointDefinition = {
					tablet : 1024,
					phone : 480
				};

				$('#dt_basic').dataTable({
					"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
					"t"+
					"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
					"autoWidth" : true,
					"preDrawCallback" : function() {
						// Initialize the responsive datatables helper once.
						if (!responsiveHelper_dt_basic) {
							responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
						}
					},
					"rowCallback" : function(nRow) {
						responsiveHelper_dt_basic.createExpandIcon(nRow);
					},
					"drawCallback" : function(oSettings) {
						responsiveHelper_dt_basic.respond();
					}
				});

				/* END BASIC */
			})
		</script>
	</body>
</html>
