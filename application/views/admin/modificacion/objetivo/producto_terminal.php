<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
		<title><?php echo $this->session->userdata('name')?></title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css"> 
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<!--estiloh-->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.core.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css" id="toggleCSS" />
		<!--para las alertas-->
    	<meta name="viewport" content="width=device-width">
		<!--fin de stiloh-->
          	<style>
			/*////scroll tablas/////*/
			table{font-size: 10px;
            width: 100%;
            max-width:1550px;;
			    overflow-x: scroll;
            }
			</style>
	</head>
	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->
		<!-- HEADER -->
		<header id="header">
			<div id="logo-group">
				<span id="logo"> <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="SmartAdmin"> </span>
			</div>
			<!-- pulled right: nav area -->
			<div class="pull-right">
				<!-- collapse menu button -->
				<div id="hide-menu" class="btn-header pull-right">
					<span> <a href="javascript:void(0);" data-action="toggleMenu" title="Menu"><i class="fa fa-reorder"></i></a> </span>
				</div>
				<!-- end collapse menu -->
				<!-- logout button -->
				<div id="logout" class="btn-header transparent pull-right">
					<span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Salir" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
				</div>
				<!-- end logout button -->
				<!-- search mobile button (this is hidden till mobile view port) -->
				<div id="search-mobile" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
				</div>
				<!-- end search mobile button -->
				<!-- fullscreen button -->
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Expandir Pantalla"><i class="fa fa-arrows-alt"></i></a> </span>
				</div>
				<!-- end fullscreen button -->
			</div>
			<!-- end pulled right: nav area -->
		</header>
		<!-- END HEADER -->
		<!-- Left panel : Navigation area -->
		<aside id="left-panel">
			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                              <span>
                                  <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                              </span>
						<i class="fa fa-angle-down"></i>
					</a> 
				</span>
			</div>

			<nav>
				<ul>
					<li class="">
	                <a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MENÚ PRINCIPAL"><i
	                        class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
	            	</li>
		            <li class="text-center">
		                <a href="#" title="MODIFICACIONES"> <span class="menu-item-parent">MODIFICACIONES</span></a>
		            </li>
					<?php
			                for($i=0;$i<count($enlaces);$i++)
			                {
			                    if(count($subenlaces[$enlaces[$i]['o_child']])>0)
			                    {
			            ?>
			            <li>
			              	<a href="#" >
			              		<i class="<?php echo $enlaces[$i]['o_image']?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
			              	<ul >
			              	<?php
			                foreach ($subenlaces[$enlaces[$i]['o_child']] as $item) {
			                ?>
			                <li><a href="<?php echo base_url($item['o_url']); ?>"><?php echo $item['o_titulo']; ?></a></li>
			                <?php } ?>
			                </ul>
			            </li>
			            <?php 
			            }
			        } ?>
				</ul>
			</nav>
			<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>

		</aside>

		<!-- MAIN PANEL -->
		<div id="main" role="main">
			<!-- RIBBON -->
			<div id="ribbon">
				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>
				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li>Modificaciones</li><li>Modificaciones del POA</li><li>Modificaciones de Objetivos de Gesti&oacute;n y Productos Terminales</li><li>Producto Terminal</li>
				</ol>
			</div>
			<!-- END RIBBON -->
			<!-- MAIN CONTENT -->
			<div id="content">
				<!-- widget grid -->
				<section id="widget-grid" class="well">
					<div class="">
						<h1>
						C&Oacute;DIGO PT: <small><?php echo $pt[0]['pt_codigo'] ?></small><br>
						OBJETIVO PRODUCTO: <small><?php echo $pt[0]['pt_objetivo'] ?></small>
						</h1>
					</div>
				</section>
				<article class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<div class="jarviswidget jarviswidget-color-darken" id="wid-id-2" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
						<header>
							<h2><strong>MODIFICAR PRODUCTO TERMINAL </strong></h2>		
						</header>
						<div class="">

							<form  name="formulario" id="formulario" method="post" action="<?php echo site_url("admin").'/mod/valida_pt' ?>">
							<input class="form-control" type="hidden" name="poa_id" id="poa_id" value="<?php echo $poa_id;?>">
							<input class="form-control" type="hidden" name="obje_id" id="obje_id" value="<?php echo $obje_id;?>">
							<input class="form-control" type="hidden" name="pt_id" id="pt_id" value="<?php echo $pt_id;?>">
							<input class="form-control" type="hidden" name="pt1" id="pt1" value="<?php echo $pt[0]['pt_objetivo'];?>">
							<input class="form-control" type="hidden" name="lb" id="lb" value="<?php echo round($pt[0]['pt_linea_base'],1);?>">

								<div class="modal-dialog demo-modal">
								<div class="modal-content">
									<div class="modal-header">
										<h6 class="modal-title">OBJETIVO PRODUCTO TERMINAL</h6>
									</div>
									<div class="modal-body">
										<textarea rows="5" class="form-control" name="pt2" id="pt2" style="width:100%;"  title="Producto Terminal"><?php echo $pt[0]['pt_objetivo'];?></textarea> 
									</div>
									<div class="col-sm-6">
									<div class="modal-header">
										<h6 class="modal-title">META INICIAL</h6>
									</div>
									<div class="modal-body">
										<input class="form-control" type="hidden" name="met_i" id="met_i" value="<?php echo round($pt[0]['pt_meta'],2);?>">
										<input class="form-control" value="<?php echo round($pt[0]['pt_meta'],2);?>" disabled="true">
									</div>
									</div>
									<div class="col-sm-6">
									<div class="modal-header">
										<h6 class="modal-title">META MODIFICADO</h6>
									</div>
									<div class="modal-body">
										<input class="form-control" type="text" name="met_m" id="met_m" value="<?php echo round($pt[0]['pt_meta'],2);?>" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }">
									</div>
									</div>
									
									<div class="modal-footer"> 
										<a href="<?php echo base_url().'index.php/admin/mod/objetivo/'.$poa_id.'' ?>" class="btn btn-success" title="Volver atras">ATRAS</a>
										<input type="button" value="MODIFICAR" class="btn btn-primary" onclick="valida_envia()">
									</div>
								</div><!-- /.modal-content -->

						</div><!-- /.modal-dialog -->
							
						</div>
						</div>
						</article>
						<article class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
							<div class="jarviswidget jarviswidget-color-darken" id="wid-id-2" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
								<header>
									<h2><strong>GESTI&Oacute;N <?php echo $this->session->userdata("gestion") ?></strong></h2>		
								</header>
								<div>
									<center><strong>INDICADOR <?php echo $pt[0]['indi_descripcion'];?></strong></center>		
								</div>
								
							<?php 
								$nro=0;
								foreach($programado as $row)
				                {
				                    $nro++;
				                    $matriz [1][$nro]=$row['mes_id'];
				                    $matriz [2][$nro]=$row['ppm_fis'];
				                }

				                for($j = 1 ;$j<=12 ;$j++){
				                    $matriz_r[1][$j]=$j;
				                    $matriz_r[2][$j]='0'; /// programado
				                }

				                for($i = 1 ;$i<=$nro ;$i++){
				                    for($j = 1 ;$j<=12 ;$j++)
				                    {
				                        if($matriz[1][$i]==$matriz_r[1][$j])
				                        {
				                            $matriz_r[2][$j]=round($matriz[2][$i],2);
				                        }
				                    }
				                }
				                
				                $suma=0;
				                for($j = 1 ;$j<=12 ;$j++)
				                {
				                	$suma=$suma+$matriz_r[2][$j];
				                }              
							?>
									<div class="row">
											<table class="table table-bordered table-hover" style="width:100%;" >
											    <thead>
											        <tr>
											            <th style="width:15%;"><center>ENERO</center></th>
											            <th style="width:15%;"><center>FEBRERO</center></th>
											            <th style="width:15%;"><center>MARZO</center></th>
											            <th style="width:15%;"><center>ABRIL</center></th>
											            <th style="width:15%;"><center>MAYO</center></th>
											            <th style="width:15%;"><center>JUNIO</center></th>
											        </tr>
											    </thead>
											    <tbody>
											        <tr>
											            <td><input  name="m1" class="form-control" type="text"  style="width:100%;" value="<?php echo $matriz_r[2][1]; ?>"  onchange="suma_programado();" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"></td>
											            <td><input  name="m2" class="form-control" type="text"  style="width:100%;" value="<?php echo $matriz_r[2][2]; ?>"  onchange="suma_programado();" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"></td>
											            <td><input  name="m3" class="form-control" type="text"  style="width:100%;" value="<?php echo $matriz_r[2][3]; ?>"  onchange="suma_programado();" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"></td>
											            <td><input  name="m4" class="form-control" type="text"  style="width:100%;" value="<?php echo $matriz_r[2][4]; ?>"  onchange="suma_programado();" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"></td>
											        	<td><input  name="m5" class="form-control" type="text"  style="width:100%;" value="<?php echo $matriz_r[2][5]; ?>"  onchange="suma_programado();" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"></td>
											            <td><input  name="m6" class="form-control" type="text"  style="width:100%;" value="<?php echo $matriz_r[2][6]; ?>" onchange="suma_programado();" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"></td>
											        </tr>
											    </tbody>
											</table>
											<table class="table table-bordered table-hover" style="width:100%;" >
											    <thead>
											        <tr>
											            <th style="width:15%;"><center>JULIO</center></th>
											            <th style="width:15%;"><center>AGOSTO</center></th>
											            <th style="width:15%;"><center>SEPTIEMBRE</center></th>
											            <th style="width:15%;"><center>OCTUBRE</center></th>
											            <th style="width:15%;"><center>NOVIEMBRE</center></th>
											            <th style="width:15%;"><center>DICIEMBRE</center></th>
											        </tr>
											    </thead>
											    <tbody>
											        <tr>
											            <td><input  name="m7" class="form-control" type="text" style="width:100%;" value="<?php echo $matriz_r[2][7]; ?>"  onchange="suma_programado();" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"></td>
											            <td><input  name="m8" class="form-control" type="text" style="width:100%;" value="<?php echo $matriz_r[2][8]; ?>" onchange="suma_programado();" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"></td>
											        	<td><input  name="m9" class="form-control" type="text"  style="width:100%;" value="<?php echo $matriz_r[2][9]; ?>"  onchange="suma_programado();" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"></td>
											            <td><input  name="m10" class="form-control" type="text" style="width:100%;" value="<?php echo $matriz_r[2][10]; ?>" onchange="suma_programado();" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"></td>
											            <td><input  name="m11" class="form-control" type="text" style="width:100%;" value="<?php echo $matriz_r[2][11]; ?>"  onchange="suma_programado();" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"></td>
											            <td><input  name="m12" class="form-control" type="text" style="width:100%;" value="<?php echo $matriz_r[2][12]; ?>"  onchange="suma_programado();" onkeypress="if (this.value.length < 10) { return numerosDecimales(event);}else{return false; }"></td>
											        </tr>
											    </tbody>
											</table>		
										</div>

								<div class="col-sm-4">
								<div class="form-group">
								<label><font size="2" color="blue"><b>SUMA TOTAL DE INDICADOR</b></font></label>
								<input class="form-control"name="total" type="text" id="total" value="<?php echo round($suma,2);?>" disabled="true" >
								
								</div>
								</div>
							</div> <!-- div obs1 -->
								</div><!-- row -->
							</div><!-- class="jarviswidget jarviswidget-color-darken" -->
						</article>
					</form>
				<!-- end widget grid -->					
			</div>
			<!-- END MAIN CONTENT -->

		</div>
		<!-- END MAIN PANEL -->
		<script>
			function suma_programado()
            {
                lb = parseFloat($('[name="lb"]').val());
                m1 = parseFloat($('[name="m1"]').val());
                m2 = parseFloat($('[name="m2"]').val());
                m3 = parseFloat($('[name="m3"]').val());
                m4 = parseFloat($('[name="m4"]').val());
                m5 = parseFloat($('[name="m5"]').val());
                m6 = parseFloat($('[name="m6"]').val());
                m7 = parseFloat($('[name="m7"]').val());
                m8 = parseFloat($('[name="m8"]').val());
                m9 = parseFloat($('[name="m9"]').val());
                m10 = parseFloat($('[name="m10"]').val());
                m11 = parseFloat($('[name="m11"]').val());
                m12 = parseFloat($('[name="m12"]').val());

                $('[name="total"]').val((m1+m2+m3+m4+m5+m6+m7+m8+m9+m10+m11+m12+lb).toFixed(2) );
            }
            function valida_envia()
            { 
            	suma=document.getElementById('total').value;

               if(parseFloat(document.formulario.met_m.value)<suma)
               {
               	alertify.error('La suma de las cifras Programadas no puede ser mayor a la Meta')
               	document.formulario.met_m.focus() 
	            return 0; 
               }
               if(parseFloat(document.formulario.met_m.value)>suma)
               {
               	alertify.error('La suma de las cifras Programadas no puede ser menor a la Meta')
               	document.formulario.met_m.focus() 
	            return 0;
               }
               if(parseFloat(document.formulario.met_m.value)==suma)
               {
               	
               	alertify.confirm("GUARDAR MODIFICACI\u00D3N?", function (a) {
                    if (a) {
                        //============= GUARDAR DESPUES DE LA VALIDACION ===============
                        document.formulario.submit();
                    } else {
                        alertify.error("OPCI\u00D3N CANCELADA");
                    }
                });

               }

            }
        </script>
		<!-- PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->
		
		<script>
			if (!window.jQuery) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-2.0.2.min.js"><\/script>');
			}
		</script>

		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>

		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url();?>/assets/js/app.config.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/mis_js/validacion_form.js"></script>
		<script src = "<?php echo base_url(); ?>mis_js/control_session.js"></script>
		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url();?>/assets/js/bootstrap/bootstrap.min.js"></script>
		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url();?>/assets/js/notification/SmartNotification.min.js"></script>
		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url();?>/assets/js/smartwidgets/jarvis.widget.min.js"></script>
		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url();?>/assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<!-- SPARKLINES -->
		<script src="<?php echo base_url();?>/assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/select2/select2.min.js"></script>
		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
		<!-- browser msie issue fix -->
		<script src="<?php echo base_url();?>/assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url();?>/assets/js/plugin/fastclick/fastclick.min.js"></script>
		<!-- Demo purpose only -->
		<script src="<?php echo base_url();?>/assets/js/demo.min.js"></script>
		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url();?>/assets/js/app.min.js"></script>

		<script src="<?php echo base_url();?>/assets/js/speech/voicecommand.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>

	</body>
</html>
