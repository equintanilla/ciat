<?php
$site_url = site_url("");
$atras = $site_url.'/admin/mod/redobj';
?>

<!DOCTYPE html>
<html lang="en-us">
    <head>
        <meta charset="utf-8">
        <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
        <title><?php echo $this->session->userdata('name')?></title>
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <!-- Basic Styles -->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
        <!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css"> 
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
        <!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
        <!-- FAVICONS -->
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
        <!--estiloh-->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css"> 
        <script>
            function abreVentana(PDF)
            {
                var direccion;
                direccion = '' + PDF;
                window.open(direccion, "Reporte Modificaciones de objetivos Estrategicos" , "width=800,height=650,scrollbars=SI") ;
            }                                                  
        </script>
        <meta name="viewport" content="width=device-width">
        <!--fin de stiloh-->
            <style>
            /*////scroll tablas/////*/
            table{font-size: 10px;
            width: 100%;
            max-width:1550px;;
                overflow-x: scroll;
            }
            </style>
            <?php
                $id=$this->uri->segment(5);
            ?>
    </head>
    <body class="">
        <!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->
        <!-- HEADER -->
        <header id="header">
            <div id="logo-group">
                <span id="logo"> <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="SmartAdmin"> </span>
            </div>
            <!-- pulled right: nav area -->
            <div class="pull-right">
                <!-- collapse menu button -->
                <div id="hide-menu" class="btn-header pull-right">
                    <span> <a href="javascript:void(0);" data-action="toggleMenu" title="Menu"><i class="fa fa-reorder"></i></a> </span>
                </div>
                <!-- end collapse menu -->
                <!-- logout button -->
                <div id="logout" class="btn-header transparent pull-right">
                    <span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Salir" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
                </div>
                <!-- end logout button -->
                <!-- search mobile button (this is hidden till mobile view port) -->
                <div id="search-mobile" class="btn-header transparent pull-right">
                    <span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
                </div>
                <!-- end search mobile button -->
                <!-- fullscreen button -->
                <div id="fullscreen" class="btn-header transparent pull-right">
                    <span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Pantalla Completa"><i class="fa fa-arrows-alt"></i></a> </span>
                </div>
                <!-- end fullscreen button -->
            </div>
            <!-- end pulled right: nav area -->
        </header>
        <!-- END HEADER -->
        <!-- Left panel : Navigation area -->
        <aside id="left-panel">

            <!-- User info -->
            <div class="login-info">
                <span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
                    
                    <a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                              <span>
                                  <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                              </span>
                        <i class="fa fa-angle-down"></i>
                    </a> 
                    
                </span>
            </div>

            <nav>
                <ul>
                    <li class="">
                    <a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MENÚ PRINCIPAL"><i
                            class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
                    </li>
                    <li class="text-center">
                        <a href="#" title="MODIFICACIONES"> <span class="menu-item-parent">MODIFICACIONES</span></a>
                    </li>
                    <?php
                            for($i=0;$i<count($enlaces);$i++)
                            {
                                if(count($subenlaces[$enlaces[$i]['o_child']])>0)
                                {
                        ?>
                        <li>
                            <a href="#" >
                                <i class="<?php echo $enlaces[$i]['o_image']?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
                            <ul >
                            <?php
                            foreach ($subenlaces[$enlaces[$i]['o_child']] as $item) {
                            ?>
                            <li><a href="<?php echo base_url($item['o_url']); ?>"><?php echo $item['o_titulo']; ?></a></li>
                            <?php } ?>
                            </ul>
                        </li>
                        <?php 
                        }
                    } ?>
                </ul>
            </nav>
            <span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>

        </aside>
        
<div id="main" role="main">
    <div>
        <ol class="breadcrumb">
            <li> Registro de Modificaci&oacute;n</li>
            <li><a href="<?php echo $atras?>">Modificaciones de Acciones de Corto Plazo y Productos Terminales</a></li>
            <li> Lista de Modificaciones Realizadas</li>
        </ol>
    </div>
    <div id="content">
        <article class="row">
            <article class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                <div class="alert alert-block alert-success">
                    <h4 class="alert-heading">
                        <img src="<?php echo base_url() . 'assets/ifinal/responsable.png' ?>" alt=""> <u>Responsable</u>:
                        <?php echo $this->session->userdata("funcionario"); ?>
                    </h4>
                </div>
            </article>

            <article class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                <section id="widget-grid" class="well">
                    <div class="" style="font-size: 15px">
                        <b>MODIFICACIONES DE ACCIONES DE CORTO PLAZO Y PRODUCTO TERMINAL</b> <br>
                        <b>ACCI&Oacute;N DE CORTO PLAZO: </b>
                        <small class="txt-color-blueLight"><?php echo $obje['o_objetivo'] ?></small>
                        <!-- <b>PROGRAMA: </b> -->
                        <!-- <small class="txt-color-blueLight"><?php // echo $dato_poa['aper_programa'] . $dato_poa['aper_proyecto'] . $dato_poa['aper_actividad'] .                                 " - " . $dato_poa['aper_descripcion'] ?></small> -->
                        <br><b>GESTI&Oacute;N: </b>
                        <small class="txt-color-blueLight"><?php echo $this->session->userData('gestion') ?></small>
                        <br>

                    </div>
                </section>
            </article>
            <article class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                <section id="widget-grid" class="well">
                    <center>
                        <div class="dropdown">
                            <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenu1" style="width:100%;" data-toggle="dropdown" aria-expanded="true">
                                OPCIONES
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                <li role="presentation">
                                    <a role="menuitem" tabindex="-1" href="<?php echo $atras ?>">VOLVER ATRAS</a>
                                </li>
                            </ul>
                        </div>
                    </center>
                </section>
            </article>

    </div>
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget jarviswidget-color-darken">
                <header>
                    <span class="widget-icon"> <span class="glyphicon glyphicon-folder-close"></span></span>
                    <h2>LISTA REGISTRO DE EJECUCIÓN POR MES</h2>
                </header>
                <div>
                    <div class="widget-body no-padding">
                        <div class="table">
                            <table id="dt_basic2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Nro.</th>
                                    <th TITLE="REGISTRO DE EJECUCIÓN">REGISTRO DE<BR>MODIFICACI&Oacute;N</th>
                                   <!--  <th TITLE="LISTA DE ARCHIVOS">ARCHIVOS</th>
                                    <th TITLE="REVERTIR REGISTRO DE EJECUCIÓN">REVERSIONES</th> -->
                                    <th>CODIGO</th>
                                    <th>OBJETIVO</th>
                                    <th>TIPO INDICADOR</th>
                                    <th>INDICADOR DE IMPACTO</th>
                                    <th>FUNCIONARIO</th>
                                    <th>FECHA REGISTRO</th>
                                    <th>ESTADO</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                echo $tabla_ejec_mes;
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </div>
</div>


<!-- PAGE FOOTER -->
        <div class="page-footer">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
                </div>
            </div>
        </div>
        <!-- END PAGE FOOTER -->
        <script>
            if (!window.jQuery) {
                document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-2.0.2.min.js"><\/script>');
            }
        </script>
        <script>
            if (!window.jQuery.ui) {
                document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
            }
        </script>
        <!-- IMPORTANT: APP CONFIG -->
        <script src="<?php echo base_url();?>/assets/js/app.config.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/mis_js/validacion_form.js"></script>
        <script src = "<?php echo base_url(); ?>mis_js/control_session.js"></script>
        <!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
        <script src="<?php echo base_url();?>/assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
        <!-- BOOTSTRAP JS -->
        <script src="<?php echo base_url();?>/assets/js/bootstrap/bootstrap.min.js"></script>
        <!-- CUSTOM NOTIFICATION -->
        <script src="<?php echo base_url();?>/assets/js/notification/SmartNotification.min.js"></script>
        <!-- JARVIS WIDGETS -->
        <script src="<?php echo base_url();?>/assets/js/smartwidgets/jarvis.widget.min.js"></script>
        <!-- EASY PIE CHARTS -->
        <script src="<?php echo base_url();?>/assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
        <!-- SPARKLINES -->
        <script src="<?php echo base_url();?>/assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
        <!-- JQUERY VALIDATE -->
        <script src="<?php echo base_url();?>/assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
        <!-- JQUERY MASKED INPUT -->
        <script src="<?php echo base_url();?>/assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
        <!-- JQUERY SELECT2 INPUT -->
        <script src="<?php echo base_url();?>/assets/js/plugin/select2/select2.min.js"></script>
        <!-- JQUERY UI + Bootstrap Slider -->
        <script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
        <!-- browser msie issue fix -->
        <script src="<?php echo base_url();?>/assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
        <!-- FastClick: For mobile devices -->
        <script src="<?php echo base_url();?>/assets/js/plugin/fastclick/fastclick.min.js"></script>
        <!-- Demo purpose only -->
        <script src="<?php echo base_url();?>/assets/js/demo.min.js"></script>
        <!-- MAIN APP JS FILE -->
        <script src="<?php echo base_url();?>/assets/js/app.min.js"></script>
        <script src="<?php echo base_url();?>/assets/js/speech/voicecommand.min.js"></script>
        <script type="text/javascript">
            $(function () {
                var id_p = '';
                $('#modal_nuevo_ff').on('hidden.bs.modal', function () {
                    document.forms['form_ff'].reset();

                });
                $("#enviar_ff").on("click", function (e) {
                    //========================VALIDANDO FORMULARIO===================
                    var $validator = $("#form_ff").validate({
                        //////////////// DATOS GENERALES
                        rules: {
                            fi2: { //// indicador
                                required: true,
                            },
                            ff2: { //// indicador
                                required: true,
                            }
                        },
                        messages: {
                            fi2: "Seleccione Fecha Inicial a modificar ",
                            ff2: "Seleccione Fecha Final a modificar",
                            
                        },
                        highlight: function (element) {
                            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                        },
                        unhighlight: function (element) {
                            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                        },
                        errorElement: 'span',
                        errorClass: 'help-block',
                        errorPlacement: function (error, element) {
                            if (element.parent('.input-group').length) {
                                error.insertAfter(element.parent());
                            } else {
                                error.insertAfter(element);
                            }
                        }
                    });
                    var $valid = $("#form_ff").valid();
                    if (!$valid) {
                        $validator.focusInvalid();
                        //return false;
                    } else { 
                        //==========================================================
                        var id_p = document.getElementById("id_p").value;
                        var id_f = document.getElementById("id_f").value;
                        var fi1 = document.getElementById("fi1").value;
                        var ff1 = document.getElementById("ff1").value;
                        var fi2 = document.getElementById("fi2").value;
                        var ff2 = document.getElementById("ff2").value;
                        //=================== VERIFICAR SI EXISTE EL COD DE PROGRAMA ==============
                        
                        var url = "<?php echo site_url("admin")?>/mod/valida_plazo";
                                    $.ajax({
                                        type: "post",
                                        url: url,
                                        data: {
                                            id_p: id_p,
                                            id_f: id_f,
                                            fi1: fi1,
                                            ff1: ff1,
                                            fi2: fi2,
                                            ff2: ff2
                                        },
                                        success: function (data) {
                                            window.location.reload(true);
                                        }
                                    });
                    }
                });
            });
        </script>
        <!--============================== MODIFICAR OBSERVACION =========================================-->
        
        
        <script type="text/javascript">
            // TABLA
            $(document).ready(function() {
                pageSetUp();
                /* BASIC ;*/
                var responsiveHelper_dt_basic = undefined;

                var breakpointDefinition = {
                    tablet : 1024,
                    phone : 480
                };

                $('#dt_basic').dataTable({
                    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
                    "t"+
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                    "autoWidth" : true,
                    "preDrawCallback" : function() {
                        // Initialize the responsive datatables helper once.
                        if (!responsiveHelper_dt_basic) {
                            responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                        }
                    },
                    "rowCallback" : function(nRow) {
                        responsiveHelper_dt_basic.createExpandIcon(nRow);
                    },
                    "drawCallback" : function(oSettings) {
                        responsiveHelper_dt_basic.respond();
                    }
                });

                /* END BASIC */
            })
        </script>
    </body>
</html>

