<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
		<title><?php echo $this->session->userdata('name')?></title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css"> 
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<!--estiloh-->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css"> 
		    <!--para las alertas-->
    	<meta name="viewport" content="width=device-width">
		<!--fin de stiloh-->
          	<style>
			/*////scroll tablas/////*/
			table{font-size: 10px;
            width: 100%;
            max-width:1550px;;
			    overflow-x: scroll;
            }
			</style>
			<?php
				$id=$this->uri->segment(6);
			?>
	</head>
	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->

		<!-- HEADER -->
		<header id="header">
			<div id="logo-group">
				<span id="logo"> <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="SmartAdmin"> </span>
			</div>

			<!-- pulled right: nav area -->
			<div class="pull-right">
				<!-- collapse menu button -->
				<div id="hide-menu" class="btn-header pull-right">
					<span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
				</div>
				<!-- end collapse menu -->
				<!-- logout button -->
				<div id="logout" class="btn-header transparent pull-right">
					<span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Sign Out" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
				</div>
				<!-- end logout button -->
				<!-- search mobile button (this is hidden till mobile view port) -->
				<div id="search-mobile" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
				</div>
				<!-- end search mobile button -->
				<!-- fullscreen button -->
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen"><i class="fa fa-arrows-alt"></i></a> </span>
				</div>
				<!-- end fullscreen button -->
			</div>
			<!-- end pulled right: nav area -->
		</header>
		<!-- END HEADER -->
		<!-- Left panel : Navigation area -->
		<aside id="left-panel">

			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
					
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                              <span>
                                  <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                              </span>
						<i class="fa fa-angle-down"></i>
					</a> 
					
				</span>
			</div>

			<nav>
				<ul>
					<li class="">
	                <a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MENÚ PRINCIPAL"><i
	                        class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
	            	</li>
		            <li class="text-center">
		                <a href="#" title="PROGRAMACION"> <span class="menu-item-parent">MODIFICACIONES</span></a>
		            </li>
				<?php
		                for($i=0;$i<count($enlaces);$i++)
		                {
		                    if(count($subenlaces[$enlaces[$i]['o_child']])>0)
		                    {
		            ?>
		            <li>
		              	<a href="#" >
		              		<i class="<?php echo $enlaces[$i]['o_image']?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
		              	<ul >
		              	<?php
		                foreach ($subenlaces[$enlaces[$i]['o_child']] as $item) {
		                ?>
		                <li><a href="<?php echo base_url($item['o_url']); ?>"><?php echo $item['o_titulo']; ?></a></li>
		                <?php } ?>
		                </ul>
		            </li>
		            <?php 
		            }
		        } ?>
				</ul>
			</nav>
			<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>

		</aside>

		<!-- MAIN PANEL -->
		<div id="main" role="main">
			<!-- RIBBON -->
			<div id="ribbon">

				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>

				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li>Modificaciones</li><li>Modificaciones del POA</li><li>Modificaciones de Objetivos de Gesti&oacute;n y Productos Terminales</li>
				</ol>
			</div>
			<!-- END RIBBON -->
			<!-- MAIN CONTENT -->
			<div id="content">
				<!-- widget grid -->
				<section id="widget-grid" class="well">
					<div class="">
						<h1> C&Oacute;DIGO POA : <small><?php echo $poa[0]['poa_codigo']?></small><br>
						 APERTURA PROGRAMATICA : <small><?php echo $poa[0]['aper_programa'].''.$poa[0]['aper_proyecto'].''.$poa[0]['aper_actividad'].' - '.$poa[0]['aper_descripcion']?></small></h1>
					</div>
				</section>
					
						<?php $nro=1;
                            foreach ($lista_objgestion as $rowo) 
                            {
                            ?>
                            <div class="well">
                            <table class="table  table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th colspan="10" align="center" bgcolor="#555756"><font size="2" color="#ffffff">OBJETIVO DE GESTI&Oacute;N</font></th>
                                </tr>
                                <tr>
                                    <th style="width:1%;" bgcolor="#555756"><font color="#ffffff">Nro.</font></th>
                                    <th style="width:5%;" bgcolor="#555756"><font size="1" color="#ffffff">C&Oacute;DIGO </font></th>
                                    <th style="width:20%;" bgcolor="#555756"><font size="1" color="#ffffff">OBJETIVO GESTI&Oacute;N</font></th>
                                    <th style="width:15%;" bgcolor="#555756"><font size="1" color="#ffffff">RESPONSABLE</font></th>
                                    <th style="width:15%;" bgcolor="#555756"><font size="1" color="#ffffff">UNIDAD ORGANIZACIONAL </font></th>
                                    <th bgcolor="#555756"><font size="1" color="#ffffff">T.I</font></th>
                                    <th style="width:10%;" bgcolor="#555756"><font size="1" color="#ffffff">INDICADOR</font></th>
                                    <th bgcolor="#555756"><font size="1" color="#ffffff">LÍNEA BASE</font></th>
                                    <th bgcolor="#555756"><font size="1" color="#ffffff">META</font></th>
                                    <th bgcolor="#555756"></th>
                                </tr>
                                </thead>
                                    <tbody>
										<tr>
                                        <td>
											<?php 
												echo $nro;
												if($id==$rowo['o_id'])
												{
												?>
													<img src="<?php echo base_url(); ?>assets/Iconos/accept.png"/>
												<?php
												}
											?>
										</td>
                                        <td><?php echo $rowo['o_codigo'];?></td>
                                        <td><?php echo $rowo['o_objetivo'];?></td>
                                        <td><?php echo $rowo['fun_nombre'].' '.$rowo['fun_paterno'].' '.$rowo['fun_materno'];?></td>
                                        <td><?php echo $rowo['unidad'];?></td>
                                        <td><?php echo $rowo['indi_abreviacion'];?></td>
                                        <td><?php echo $rowo['o_indicador'];?></td>
                                        <td><?php echo $rowo['o_linea_base'];?></td>
                                        <td><?php echo $rowo['o_meta'];?></td>
                                        <td>
                                            <center><a href="<?php echo base_url().'index.php/admin/mod/mod_obj_gest/'.$poa_id.'/'.$obje_id.'/'.$rowo['o_id'].'' ?>" class="btn btn-default" >MODIFICAR O.G.</a></center>
                                        </td>
                                        </tr>
                                        <?php
                                    $nro++;
                                    ?>
                                    </tbody>
                       				</table>
                       				<table class="table  table-bordered table-hover">
                                        <thead>
                                        <tr>
		                                    <th colspan="10" align="center" bgcolor="##568A89"><font size="1" color="#ffffff">PRODUCTO TERMINAL</font></th>
		                                </tr>
                                        <tr>
                                            <th style="width:1%;" bgcolor="##568A89"><font size="1" color="#ffffff">Nro.</font></th>
                                            <th style="width:5%;" bgcolor="##568A89"><font size="1" color="#ffffff">C&Oacute;DIGO </font></th>
                                            <th style="width:20%;" bgcolor="##568A89"><font size="1" color="#ffffff">OBJ. PRODUCTO TERMINAL</font></th>
                                            <th style="width:15%;" bgcolor="##568A89"><font size="1" color="#ffffff">RESPONSABLE </font></th>
                                            <th style="width:15%;" bgcolor="##568A89"><font size="1" color="#ffffff">UNIDAD ORGANIZACIONAL </font></th>
                                            <th bgcolor="##568A89"><font size="1">T.I</font></th>
                                            <th style="width:10%;" bgcolor="##568A89"><font size="1" color="#ffffff">INDICADOR</font></th>
                                            <th bgcolor="##568A89"><font size="1" color="#ffffff">LÍNEA BASE</font></th>
                                            <th bgcolor="##568A89"><font size="1" color="#ffffff">META</font></th>
                                            <th bgcolor="##568A89"></th>
                                        </tr>
                                        </thead>
                                        <tbody>
	                                    <?php
	                                    $lista_pterminal = $this->mp_terminal->lista_pterminal($rowo['o_id']);
	                                    $nrop=1;
	                                    foreach ($lista_pterminal as $row)
	                                    {
	                                    	?>
	                                    	<tr>
		                                    	<td>
													<?php 
														echo $nrop;
														if($id==$row['pt_id'])
														{
														?>
															<img src="<?php echo base_url(); ?>assets/Iconos/accept.png"/>
														<?php
														}
													?>
												</td>
		                                    	<td ><?php echo $row['pt_codigo'];?></td>
	                                            <td ><?php echo $row['pt_objetivo'];?></td>
	                                            <td ><?php echo $row['fun_nombre'].' '.$row['fun_paterno'].' '.$row['fun_materno'];?></td>
	                                            <td ><?php echo $row['unidad'];?></td>
	                                            <td ><?php echo $row['indi_abreviacion'];?></td>
	                                            <td ><?php echo $row['pt_indicador'];?></td>
	                                            <td ><?php echo $row['pt_linea_base'];?></td>
	                                            <td ><?php echo $row['pt_meta'];?></td>
	                                            <td>
	                                            <center><a href="<?php echo base_url().'index.php/admin/mod/mod_pt/'.$poa_id.'/'.$obje_id.'/'.$row['pt_id'].'' ?>" class="btn btn-default" >MODIFICAR P.T.</a></center>
	                                           	</td>
                                            </tr>
                                            <?php
                                            $nrop++;
	                                    }
	                                    ?>
	                                    </tbody>
	                                    </table>
	                                    </div>
	                                    <?php
                                }

                        	?>

					<div class="form-actions">
							<a href="<?php echo base_url().'index.php/admin/mod/redobj' ?>" class="btn btn-lg btn-default" title="Volver atras">ATRAS</a>
						</div>
				<!-- end widget grid -->					
			</div>
			<!-- END MAIN CONTENT -->

		</div>
		<!-- END MAIN PANEL -->
		<!-- PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->
		<script>
			if (!window.jQuery) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-2.0.2.min.js"><\/script>');
			}
		</script>

		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>
		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url();?>/assets/js/app.config.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/mis_js/validacion_form.js"></script>
		<script src = "<?php echo base_url(); ?>mis_js/control_session.js"></script>
		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url();?>/assets/js/bootstrap/bootstrap.min.js"></script>
		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url();?>/assets/js/notification/SmartNotification.min.js"></script>
		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url();?>/assets/js/smartwidgets/jarvis.widget.min.js"></script>
		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url();?>/assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<!-- SPARKLINES -->
		<script src="<?php echo base_url();?>/assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/select2/select2.min.js"></script>
		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
		<!-- browser msie issue fix -->
		<script src="<?php echo base_url();?>/assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url();?>/assets/js/plugin/fastclick/fastclick.min.js"></script>
		<!-- Demo purpose only -->
		<script src="<?php echo base_url();?>/assets/js/demo.min.js"></script>
		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url();?>/assets/js/app.min.js"></script>

		<script src="<?php echo base_url();?>/assets/js/speech/voicecommand.min.js"></script>
		<script type="text/javascript">
		    $(function () {
		        var id_p = '';
		        $('#modal_nuevo_ff').on('hidden.bs.modal', function () {
		            document.forms['form_ff'].reset();

		        });
		        $("#enviar_ff").on("click", function (e) {
		            //========================VALIDANDO FORMULARIO===================
		            var $validator = $("#form_ff").validate({
		                //////////////// DATOS GENERALES
		                rules: {
		                    fi2: { //// indicador
		                        required: true,
		                    },
		                    ff2: { //// indicador
		                        required: true,
		                    }
		                },
		                messages: {
		                    fi2: "Seleccione Fecha Inicial a modificar ",
		                    ff2: "Seleccione Fecha Final a modificar",
		                    
		                },
		                highlight: function (element) {
		                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		                },
		                unhighlight: function (element) {
		                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		                },
		                errorElement: 'span',
		                errorClass: 'help-block',
		                errorPlacement: function (error, element) {
		                    if (element.parent('.input-group').length) {
		                        error.insertAfter(element.parent());
		                    } else {
		                        error.insertAfter(element);
		                    }
		                }
		            });
		            var $valid = $("#form_ff").valid();
		            if (!$valid) {
		                $validator.focusInvalid();
		                //return false;
		            } else { 
		                //==========================================================
		                var id_p = document.getElementById("id_p").value;
		                var id_f = document.getElementById("id_f").value;
		                var fi1 = document.getElementById("fi1").value;
		                var ff1 = document.getElementById("ff1").value;
		                var fi2 = document.getElementById("fi2").value;
		                var ff2 = document.getElementById("ff2").value;
		                //=================== VERIFICAR SI EXISTE EL COD DE PROGRAMA ==============
		                
		                var url = "<?php echo site_url("admin")?>/mod/valida_plazo";
		                            $.ajax({
		                                type: "post",
		                                url: url,
		                                data: {
		                                    id_p: id_p,
		                                    id_f: id_f,
		                                    fi1: fi1,
		                                    ff1: ff1,
		                                    fi2: fi2,
		                                    ff2: ff2
		                                },
		                                success: function (data) {
		                                    window.location.reload(true);
		                                }
		                            });
		            }
		        });
		    });
		</script>
		<!--============================== MODIFICAR OBSERVACION =========================================-->
		<script type="text/javascript">
			// TABLA
			$(document).ready(function() {
				pageSetUp();
				/* BASIC ;*/
				var responsiveHelper_dt_basic = undefined;

				var breakpointDefinition = {
					tablet : 1024,
					phone : 480
				};

				$('#dt_basic').dataTable({
					"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
					"t"+
					"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
					"autoWidth" : true,
					"preDrawCallback" : function() {
						// Initialize the responsive datatables helper once.
						if (!responsiveHelper_dt_basic) {
							responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
						}
					},
					"rowCallback" : function(nRow) {
						responsiveHelper_dt_basic.createExpandIcon(nRow);
					},
					"drawCallback" : function(oSettings) {
						responsiveHelper_dt_basic.respond();
					}
				});

				/* END BASIC */
			})
		</script>
	</body>
</html>
