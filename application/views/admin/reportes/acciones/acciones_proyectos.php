<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
		<title><?php echo $this->session->userdata('name')?></title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css"> 
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<!--estiloh-->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css"> 
		    <!--para las alertas-->
 			<!--para las alertas-->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.core.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css" id="toggleCSS" />
	    <meta name="viewport" content="width=device-width">
		<!--fin de stiloh-->

	</head>
	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->

		<!-- HEADER -->
		<header id="header">
			<div id="logo-group">
				<span id="logo"> <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="SmartAdmin"> </span>
			</div>

			<!-- pulled right: nav area -->
			<div class="pull-right">
				<!-- collapse menu button -->
				<div id="hide-menu" class="btn-header pull-right">
					<span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
				</div>
				<!-- end collapse menu -->
				<!-- logout button -->
				<div id="logout" class="btn-header transparent pull-right">
					<span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Sign Out" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
				</div>
				<!-- end logout button -->
				<!-- search mobile button (this is hidden till mobile view port) -->
				<div id="search-mobile" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
				</div>
				<!-- end search mobile button -->
				<!-- fullscreen button -->
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen"><i class="fa fa-arrows-alt"></i></a> </span>
				</div>
				<!-- end fullscreen button -->
			</div>
			<!-- end pulled right: nav area -->
		</header>
		<!-- END HEADER -->

		<!-- Left panel : Navigation area -->
		<aside id="left-panel">

			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
					
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                              <span>
                                  <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                              </span>
						<i class="fa fa-angle-down"></i>
					</a> 
					
				</span>
			</div>
			<nav>
				<ul>
					<li class="">
	                <a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MENÚ PRINCIPAL"><i
	                        class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
	            	</li>
		            <li class="text-center">
		                <a href="#" title="REPORTES"> <span class="menu-item-parent">REPORTES</span></a>
		            </li>
				<?php
		                for($i=0;$i<count($enlaces);$i++)
		                {
		                    if(count($subenlaces[$enlaces[$i]['o_child']])>0)
		                    {
		            ?>
		            <li>
		              	<a href="#" >
		              		<i class="<?php echo $enlaces[$i]['o_image']?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
		              	<ul >
		              	<?php
		                foreach ($subenlaces[$enlaces[$i]['o_child']] as $item) {
		                ?>
		                <li><a href="<?php echo base_url($item['o_url']); ?>"><?php echo $item['o_titulo']; ?></a></li>
		                <?php } ?>
		                </ul>
		            </li>
		            <?php 
		            }
		        } ?>
				</ul>
			</nav>
			<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>

		</aside>

		<!-- MAIN PANEL -->
		<div id="main" role="main">

			<!-- RIBBON -->
			<div id="ribbon">

				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>

				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li>Reportes</li><li>Seguimiento</li><li>Operaciones</li><li>Prog. y Ejec. Fisica de las operaciones a Nivel Institucional</li><li>Prog. y Ejec. Fis. de las Acciones a Nivel Programas</li><li>A nivel de Acciones</li>
				</ol>
			</div>

			<!-- MAIN CONTENT -->
			<div id="content">
				<div class="alert alert-block alert-success" >
					<a class="close" data-dismiss="alert" href="#">×</a>
					<h4 class="alert-heading"><i class="glyphicon glyphicon-user"></i><?php echo " FUNCIONARIO RESPONSABLE : ".$this->session->userdata("funcionario")."   ||  "; ?><i class="glyphicon glyphicon-calendar"></i><?php echo "FECHA ACTUAL : " . date("d") . " del " . date("m") . " de " . date("Y");?></h4>
				</div>
				<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		            <section id="widget-grid" class="well">
		              <div align="center">
		                <h1><b> <?php echo $this->session->userdata("entidad");?> </b><br><small>PROGRAMACI&Oacute;N Y EJECUCI&Oacute;N F&Iacute;SICA DE ACCIONES<br> AL MES DE <?php echo $mes;?>  de <?php echo $this->session->userdata("gestion");?></small></h1>
		              </div>
		            </section>
		        </article>
				<!-- widget grid -->
				<section id="widget-grid" class="">
					<div class="row">

					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-color-teal" id="wid-id-21"data-widget-editbutton="false">
                        <header>
                            <span class="widget-icon"> <span class="fa fa-table"></span></span>
                            <h2>PROGRAMA <?php echo $programa;?></h2>
                        </header>
                        <div>
                            <div class="widget-body no-padding">
                                <div class="table-responsive">
                                    <table id="dt_basic" class="table table-bordered " width="100%">
                                        <thead>
                                        <tr>
											<th><font size="1"><center>CATEGORIA PROGRAMATICA <?php echo $this->session->userdata("gestion");?></center></font></th>
											<th style="width:30%;"><font size="1"><center>DESCRIPCI&Oacute;N</center></font></th>
											<th style="width:70%;"><font size="1"><center>PROGRAMACI&Oacute;N Y EJECUCI&Oacute;N F&Iacute;SICA DE ACCIONES</center></font></th>
										</tr>
                                        </thead>
                                        <tbody id="bdi">
                                        <?php $nro=1; $a=1;
										        	for($i=1;$i<=12;$i++){$proy_p[$i]=0; $proy_e[$i]=0;}
										            foreach($proyectos  as $proy)
										            {
										            	$nro_f=$this->model_faseetapa->nro_fase($proy['proy_id']);
										            	/*======================= VERIF FASE ===================*/
										            	if($nro_f>0)
										            	{
										            	echo '<tr>';
											    		echo '<td>' .$proy['aper_programa'].'' .$proy['aper_proyecto'].'' .$proy['aper_actividad'].'</td>';
											    		echo '<td>' .$proy['proy_nombre'].'</td>';
										            	$programa_proyecto=$proy['aper_programa'].'' .$proy['aper_proyecto'].'' .$proy['aper_actividad'];
										            	$fase = $this->model_faseetapa->get_id_fase($proy['proy_id']);
											            $componentes = $this->model_componente->componentes_id($fase[0]['id']);
											            for($i=1;$i<=12;$i++){$com_p[$i]=0; $com_e[$i]=0; $com_efi[$i]=0; $menor[$i]=0; $entre[$i]=0; $mayor[$i]=0;}
											            /*======================= COMPONENTE ====================*/
											            foreach ($componentes as $rowc)
											            {
											              $productos = $this->model_producto->list_prod($rowc['com_id']);
											              for($i=1;$i<=12;$i++){$prod_p[$i]=0; $prod_e[$i]=0;}
											              /*======================== PRODUCTO===========================*/
											              foreach ($productos as $rowp)
											              { 
											              	$actividad = $this->model_actividad->list_act_anual($rowp['prod_id']);
												                for($i=1;$i<=12;$i++){$act_p[$i]=0; $act_e[$i]=0;}
												                foreach ($actividad as $rowa)
												                {   $meta_gestion=$this->model_actividad->meta_act_gest($rowa['act_id']);
												                    $meta_gest=$meta_gestion[0]['meta_gest']+$rowa['act_linea_base'];
												                    if($meta_gest==''){$meta_gest='1';}
												                    $programado=$this->model_actividad->act_prog_mensual($rowa['act_id'],$this->session->userdata("gestion")); /// programado
												                    $ejecutado=$this->model_actividad->act_ejec_mensual($rowa['act_id'],$this->session->userdata("gestion")); /// ejecutado
												                    
												                    /*============================= PROCESO ACTIVIDAD  ============================*/
												                    	$nro=0;
													                    foreach($programado as $prog)
													                    {
													                      $nro++;
													                      $matriz [1][$nro]=$prog['m_id'];
													                      $matriz [2][$nro]=$prog['pg_fis'];
													                    }
													                    /*--------------------ejecutado gestion ------------------*/
													                    $nro_e=0;
													                    foreach($ejecutado as $ejec)
													                    {
													                      $nro_e++;
													                      $matriz_e [1][$nro_e]=$ejec['m_id'];
													                      $matriz_e [2][$nro_e]=$ejec['ejec_fis'];
													                      $matriz_e [3][$nro_e]=$ejec['ejec_fis_a'];
													                      $matriz_e [4][$nro_e]=$ejec['ejec_fis_b'];
													                    }
													                    /*--------------------------------------------------------*/
													                    /*---------------- llenando la matriz vacia --------------*/
													                    for($j = 1; $j<=12; $j++)
													                    {
													                      $matriz_r[1][$j]=$j;
													                      $matriz_r[2][$j]='0';
													                      $matriz_r[3][$j]='0';
													                      $matriz_r[4][$j]='0';
													                      $matriz_r[5][$j]='0';
													                      $matriz_r[6][$j]='0';
													                      $matriz_r[7][$j]='0';
													                      $matriz_r[8][$j]='0';
													                      $matriz_r[9][$j]='0';
													                      $matriz_r[10][$j]='0';
													                    }
													                  /*--------------------------------------------------------*/

													                    /*------- asignando en la matriz P, PA, %PA ----------*/
													                    for($i = 1 ;$i<=$nro ;$i++)
													                    {
													                      for($j = 1 ;$j<=12 ;$j++)
													                      {
													                        if($matriz[1][$i]==$matriz_r[1][$j])
													                        {
													                          $matriz_r[2][$j]=round($matriz[2][$i],1);
													                        }
													                      }
													                    }
													                    $pa=0;
													                    for($j = 1 ;$j<=12 ;$j++){
													                    $pa=$pa+$matriz_r[2][$j];
													                    $matriz_r[3][$j]=$pa+$rowa['act_linea_base'];
													                      if($meta_gest!=0)
													                      	$matriz_r[4][$j]=round(((($pa+$rowa['act_linea_base'])/$meta_gest)*100),2);
													                      else
													                      	$matriz_r[4][$j]=round((($pa+$rowa['act_linea_base'])*100),2);
													                    }

													                    if($rowa['indi_id']==1)
													                    {
													                      for($i = 1 ;$i<=$nro_e ;$i++){
													                        for($j = 1 ;$j<=12 ;$j++)
													                        {
													                          if($matriz_e[1][$i]==$matriz_r[1][$j])
													                          {
													                            $matriz_r[7][$j]=round($matriz_e[2][$i],1);
													                          }
													                        }
													                      }
													                    }
													                    elseif ($rowa['indi_id']==2) 
													                    {
													                      if($rowa['act_denominador']==0)
													                      {
													                        for($i = 1 ;$i<=$nro_e ;$i++){
													                          for($j = 1 ;$j<=12 ;$j++)
													                          {
													                            if($matriz_e[1][$i]==$matriz_r[1][$j])
													                            {
													                              $matriz_r[5][$j]=round($matriz_e[3][$i],1);
													                              $matriz_r[6][$j]=round($matriz_e[4][$i],1);
													                              $matriz_r[7][$j]=round($matriz_e[2][$i],1);
													                            }
													                          }
													                        }
													                    }
													                    if ($rowa['act_denominador']==1)
													                    {
													                      for($i = 1 ;$i<=$nro_e ;$i++){
													                        for($j = 1 ;$j<=12 ;$j++)
													                        {
													                          if($matriz_e[1][$i]==$matriz_r[1][$j])
													                          {
													                            $matriz_r[5][$j]=round($matriz_e[3][$i],1);
													                            $matriz_r[6][$j]=round($matriz_e[4][$i],1);
													                            $matriz_r[7][$j]=round($matriz_e[2][$i],1);
													                          }
													                        }
													                      }
													                    }
													                    /*--------------------------------------------------------*/
													                  }
													                    /*--------------------matriz E,AE,%AE gestion ------------------*/
													                    $pe=0;
													                    for($j = 1 ;$j<=12 ;$j++){
													                      $pe=$pe+$matriz_r[7][$j];
													                      $matriz_r[8][$j]=$pe+$rowa['act_linea_base'];
													                      if($meta_gest!=0)
													                      	{$matriz_r[9][$j]=round(((($pe+$rowa['act_linea_base'])/$meta_gest)*100),1);}
													                      else
													                      	{$matriz_r[9][$j]=round((($pe+$rowa['act_linea_base'])*100),1);}
													                      
													                      if($matriz_r[4][$j]==0)
													                        {$matriz_r[10][$j]=round((($matriz_r[9][$j])*100),1);}
													                      else
													                        {$matriz_r[10][$j]=round((($matriz_r[9][$j]/$matriz_r[4][$j])*100),1);}                                              
													                  }
												                    /*============================= END PROCESO ACTIVIDAD =========================*/
												                	
												                	/*------------------------ sumatoria a nivel producto -------------------------------*/
		                                                  			for($j = 1 ;$j<=12 ;$j++)
		                                                             {
		                                                             	$act_p[$j]=round($act_p[$j]+(($matriz_r[4][$j]*$rowa['act_ponderacion'])/100),2);
		                                                             	$act_e[$j]=round($act_e[$j]+(($matriz_r[9][$j]*$rowa['act_ponderacion'])/100),2);
		                                                             }
		                                                             //echo "PROY ID: ".$proy['proy_id'].' ACT :'.$rowa['act_id'].'  PROGRAMA :'.$proy['aper_programa'].''.$proy['aper_proyecto'].''.$proy['aper_actividad'].'<br>';
		                                                  			/*-------------------------------------------------------------------------------------*/
												                }
												                /*----------------------------- FUERA DEL FOREACH ACTIVIDADES*/
											                
                                                  			/*------------------------ sumatoria a nivel producto -------------------------------*/
                                                  			for($j = 1 ;$j<=12 ;$j++)
                                                             {
                                                             	$prod_p[$j]=round($prod_p[$j]+(($act_p[$j]*$rowp['prod_ponderacion'])/100),2);
	                                                            $prod_e[$j]=round($prod_e[$j]+(($act_e[$j]*$rowp['prod_ponderacion'])/100),2);
                                                             }
                                                  			/*-------------------------------------------------------------------------------------*/
											              }
											              /*===================END PRODUCTO ===================*/
											              	/*------------------------ sumatoria a nivel componente  -------------------------------*/
                                                             for($j = 1 ;$j<=12 ;$j++)
                                                             {
                                                             	$com_p[$j]=round($com_p[$j]+(($prod_p[$j]*$rowc['com_ponderacion'])/100),2);
                                                             	$com_e[$j]=round($com_e[$j]+(($prod_e[$j]*$rowc['com_ponderacion'])/100),2);
                                                             }
                                                            /*-------------------------------------------------------------------------------------*/
											             
											            }
														/*------------------------ sumatoria a nivel proyecto  -------------------------------*/
											            for($j = 1 ;$j<=12 ;$j++)
	                                                    {
	                                                    	if($com_p[$j]==0)
	                                                    	$com_efi[$j]=round($com_e[$j],1);
	                                                    	else
	                                                    	$com_efi[$j]=round((($com_e[$j]/$com_p[$j])*100),1);
														}

														for($j=1;$j<=12;$j++)
														{
															if($com_efi[$j] <=75){$menor[$j]=$com_efi[$j];}
															if($com_efi[$j] >= 76 && $com_efi[$j] <= 90.9){$entre[$j]=$com_efi[$j];}
															if($com_efi[$j] >= 91){$mayor[$j]=$com_efi[$j];}
														}
											            for($j = 1 ;$j<=12 ;$j++)
                                                            {
                                                             	$proy_p[$j]=round($proy_p[$j]+(($com_p[$j]*$proy['proy_ponderacion'])/100),2); //// proyectos programado
                                                             	$proy_e[$j]=round($proy_e[$j]+(($com_e[$j]*$proy['proy_ponderacion'])/100),2); //// proyectos ejecutado
                                                            }
                                                        /*-------------------------------------------------------------------------------------*/
											          echo '<td>';
											          	?>
											          		<table class="table table-bordered">
										          			<tr bgcolor="#008080">
										          				<th><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#<?php echo $a;?>" title="PROGRAMACION 2016-2020"><span class="glyphicon glyphicon-stats" aria-hidden="true"></span></button></th>
										          				<th><font color="#ffffff" size="1">ENE.</font></th>
										          				<th><font color="#ffffff" size="1">FEB.</font></th>
										          				<th><font color="#ffffff" size="1">MAR.</font></th>
										          				<th><font color="#ffffff" size="1">ABR.</font></th>
										          				<th><font color="#ffffff" size="1">MAY.</font></th>
										          				<th><font color="#ffffff" size="1">JUN.</font></th>
										          				<th><font color="#ffffff" size="1">JUL.</font></th>
										          				<th><font color="#ffffff" size="1">AGO.</font></th>
										          				<th><font color="#ffffff" size="1">SEP.</font></th>
										          				<th><font color="#ffffff" size="1">OCT.</font></th>
										          				<th><font color="#ffffff" size="1">NOV.</font></th>
										          				<th><font color="#ffffff" size="1">DIC.</font></th>
										          			</tr>
										          			<tr>
										          				<td>PROGRAMACI&Oacute;N ACUMULADA EN %</td>
										          				<?php
																for($i=1;$i<=12;$i++)
																{
																	echo "<td>".$com_p[$i]."%</td>";
																}
																?>
										          			</tr>
										          			<tr>
										          				<td>EJECUCI&Oacute;N ACUMULADA EN %</td>
										          				<?php
																for($i=1;$i<=12;$i++)
																{
																	echo "<td>".$com_e[$i]."%</td>";
																}
																?>
										          			</tr>
										          			<tr>
										          				<td>EFICACIA</td>
										          				<?php
																for($i=1;$i<=12;$i++)
																{
																	echo "<td>".$com_efi[$i]."%</td>";
																}
																?>
										          			</tr>
										          		</table>
											          	<?php

											          	echo '</td>';
											          	?>
												          	<font size="1">
		                                                   	<script type="text/javascript"></script>
		                                                   	<script src="<?php echo base_url(); ?>assets/highcharts/js/highcharts.js"></script>
		    												<script src="<?php echo base_url(); ?>assets/highcharts/js/highcharts-3d.js"></script>
										                    <script src="<?php echo base_url(); ?>assets/highcharts/js/modules/exporting.js"></script> 
										                     <!--==================================================================== MODAL GRAFICO========================================================= -->
										                     
										                     <div class="modal fade bs-example-modal-lg" id="<?php echo $a;?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
										                     <div class="modal-dialog modal-lg" role="document">
										                     
										                     <div class="modal-content">
		                                                      <div class="row">
		                                                      <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
																<div class="jarviswidget">
																	<header>
																		<span class="widget-icon"> <i class="fa fa-bar-chart-o"></i> </span>
																		<h2>PROGRAMACION,EJECUCI&Oacute;N PRODUCTOS A NIVEL PROYECTOS</h2>
																	</header>

																	<!-- widget div-->
																	<div>
																		<!-- widget edit box -->
																		<div class="jarviswidget-editbox">
																			<!-- This area used as dropdown edit box -->
																		</div>
																		<!-- end widget edit box -->
																		<!-- widget content -->
																		<div class="widget-body no-padding">
																			<div class="col-md-6 col-sm-6 col-xs-6"  id="<?php echo 'graf'.$a;?>" class="chart"></div>
																		</div>
																		<!-- end widget content -->
																	</div>
																	<!-- end widget div -->
																</div>

																<div class="jarviswidget">
																	<header>
																		<span class="widget-icon"> <i class="fa fa-bar-chart-o"></i> </span>
																		<h2>EFICACIA PRODUCTOS A NIVEL PROYECTOS</h2>
																	</header>
																	<!-- widget div-->
																	<div>
																		<!-- widget edit box -->
																		<div class="jarviswidget-editbox">
																			<!-- This area used as dropdown edit box -->
																		</div>
																		<!-- end widget edit box -->
																		<!-- widget content -->
																		<div class="widget-body no-padding">
																			<div class="col-md-6 col-sm-6 col-xs-6"  id="<?php echo 'graf_eficacia'.$a;?>" class="chart"></div>
																		</div>
																		<!-- end widget content -->
																	</div>
																	<!-- end widget div -->
																</div>
																<!-- end widget -->
															</article>
		                                                    </div>
		                                                         <!--fin de graficos-->
		                                                         <script type="text/javascript">
		                                                              var a=<?php echo $a;?>;
		                                                              var programa_proyecto=<?php echo $programa_proyecto;?>;

																	  var chart1 = new Highcharts.Chart({
																	        chart: {
																	            renderTo: 'graf'+a, // div contenedor
																	            type: 'line' // tipo de grafico
																	        },

																	        title: {
																	            text: 'PROGRAMACI\u00D3N Y EJECUCI\u00D3N F\u00CDSICA DE PRODUCTOS : '+programa_proyecto, // t�tulo  del gr�fico
																	            x: -20 //center
																	        },
																	        subtitle: {
																	            text: ''
																	        },
																	        xAxis: {
																	            categories: ['ENERO', 'FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO', 'JULIO', 'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE', 'NOVIEMBRE', 'DICIEMBRE']
																	        },
																	        yAxis: {
																	            title: {
																	                text: 'PORCENTAJES (%)'
																	            }
																	        },
																	        plotOptions: {
																	            line: {
																	                dataLabels: {
																	                    enabled: true
																	                },
																	                enableMouseTracking: false
																	            }
																	        },
																	        series: [
																	            {
																	                name: 'PROGRAMACIÓN ACUMULADA EN %',
																	                data: [ <?php echo $com_p[1];?>, <?php echo $com_p[2];?>, <?php echo $com_p[3];?>, <?php echo $com_p[4];?>, <?php echo $com_p[5];?>, <?php echo $com_p[6];?>, <?php echo $com_p[7];?>, <?php echo $com_p[8];?>, <?php echo $com_p[9];?>, <?php echo $com_p[10];?>, <?php echo $com_p[11];?>, <?php echo $com_p[12];?>]
																	            },
																	            {
																	                name: 'EJECUCIÓN ACUMULADA EN %',
																	                data: [ <?php echo $com_e[1];?>, <?php echo $com_e[2];?>, <?php echo $com_e[3];?>, <?php echo $com_e[4];?>, <?php echo $com_e[5];?>, <?php echo $com_e[6];?>, <?php echo $com_e[7];?>, <?php echo $com_e[8];?>, <?php echo $com_e[9];?>, <?php echo $com_e[10];?>, <?php echo $com_e[11];?>, <?php echo $com_e[12];?>]
																	            }
																	        ]
																	    });
		                                                         </script>
		                                                         <script type="text/javascript">
	                                                              var a=<?php echo $a;?>;
	                                                              var programa_proyecto=<?php echo $programa_proyecto;?>;
	                                                            
															        Highcharts.chart('graf_eficacia'+a, {
															            chart: {
															                type: 'column'
															            },
															            title: {
															                text: ' EFICACIA : '+programa_proyecto,
															            },
															            xAxis: {
															                categories: ['ENERO', 'FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO', 'JULIO', 'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE', 'NOVIEMBRE', 'DICIEMBRE']
															            },
															            yAxis: {
															                min: 0,
															                title: {
															                    text: 'PORCENTAJES (%)'
															                },
															                stackLabels: {
															                    enabled: true,
															                    style: {
															                        fontWeight: 'bold',
															                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
															                    }
															                }
															            },
															            legend: {
															                align: 'right',
															                x: -30,
															                verticalAlign: 'top',
															                y: 25,
															                floating: true,
															                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
															                borderColor: '#CCC',
															                borderWidth: 1,
															                shadow: false
															            },
															            tooltip: {
															                headerFormat: '<b>{point.x}</b><br/>',
															                pointFormat: '{series.name}: <br/> TOTAL:   {point.y}'
															            },
															            plotOptions: {
															                column: {
															                    stacking: 'normal',
															                    dataLabels: {
															                        enabled: false,
															                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
															                    }
															                }
															            },
															            series: [{

															                name: '<b style="color: #FF0000;">MENOR A 75%</b>',
															                data: [{y: <?php echo $menor[1]?>, color: 'red'},{y: <?php echo $menor[2]?>, color: 'red'},{y: <?php echo $menor[3]?>, color: 'red'},{y: <?php echo $menor[4]?>, color: 'red'},{y: <?php echo $menor[5]?>, color: 'red'},{y: <?php echo $menor[6]?>, color: 'red'},{y: <?php echo $menor[7]?>, color: 'red'},{y: <?php echo $menor[8]?>, color: 'red'},{y: <?php echo $menor[9]?>, color: 'red'},{y: <?php echo $menor[10]?>, color: 'red'},{y: <?php echo $menor[11]?>, color: 'red'},{y: <?php echo $menor[12]?>, color: 'red'}] 

															            }, {
															                name: '<b style="color: #d6d21f;">ENTRE 76% Y 90%</b>',
															                data: [{y: <?php echo $entre[1]?>, color: 'yellow'},{y: <?php echo $entre[2]?>, color: 'yellow'},{y: <?php echo $entre[3]?>, color: 'yellow'},{y: <?php echo $entre[4]?>, color: 'yellow'},{y: <?php echo $entre[5]?>, color: 'yellow'},{y: <?php echo $entre[6]?>, color: 'yellow'},{y: <?php echo $entre[7]?>, color: 'yellow'},{y: <?php echo $entre[8]?>, color: 'yellow'},{y: <?php echo $entre[9]?>, color: 'yellow'},{y: <?php echo $entre[10]?>, color: 'yellow'},{y: <?php echo $entre[11]?>, color: 'yellow'},{y: <?php echo $entre[12]?>, color: 'yellow'}] 
															            }, {
															                name: '<b style="color: green;">MAYOR A 91%</b>',
															                data: [{y: <?php echo $mayor[1]?>, color: 'green'},{y: <?php echo $mayor[2]?>, color: 'green'},{y: <?php echo $mayor[3]?>, color: 'green'},{y: <?php echo $mayor[4]?>, color: 'green'},{y: <?php echo $mayor[5]?>, color: 'green'},{y: <?php echo $mayor[6]?>, color: 'green'},{y: <?php echo $mayor[7]?>, color: 'green'},{y: <?php echo $mayor[8]?>, color: 'green'},{y: <?php echo $mayor[9]?>, color: 'green'},{y: <?php echo $mayor[10]?>, color: 'green'},{y: <?php echo $mayor[11]?>, color: 'green'},{y: <?php echo $mayor[12]?>, color: 'green'}] 
															            }]

															        });

	                                                         </script>
		                                                        </div>
		                                                       </div>
		                                                    </div>
		                                                </font>
											          	<?php
											        echo '</tr>';
											          }
											        	/*======================= END VERIF FASE ===================*/
											        	$a++;
											        }
											       /* for($j=1;$j<=12;$j++)
											        {
											        	echo "[".$proy_p[$j]."]";
											        }
											        echo "<br>";*/
										    ?>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                            <!-- end widget content -->
                        </div>
                        <!-- end widget div -->
                        <div class="form-actions">
							<a href="<?php echo base_url().'index.php/admin/rep/list_acciones'?>" title="ACCIONES A NIVEL DE PROGRAMAS" class="btn btn-lg btn-default">ATRAS</a>
                        </div>
                    </div>
                    <!-- end widget -->
                </div>
						
				</section>
			</div>
			<!-- END MAIN CONTENT -->

		</div>
		<!-- END MAIN PANEL -->

		<!-- PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userdata('name')?> <?php echo $this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->

		<!--================================================== -->

		<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
		<script data-pace-options='{ "restartOnRequestAfter": true }' src="<?php echo base_url(); ?>assets/js/plugin/pace/pace.min.js"></script>

		<script>
			if (!window.jQuery) {
				document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-2.0.2.min.js"><\/script>');
			}
		</script>

		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>

		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url(); ?>assets/js/session_time/jquery-idletimer.js"></script>
		<script src = "<?php echo base_url(); ?>mis_js/control_session.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/app.config.js"></script>

		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 

		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.min.js"></script>

		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url(); ?>assets/js/notification/SmartNotification.min.js"></script>

		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url(); ?>assets/js/smartwidgets/jarvis.widget.min.js"></script>

		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

		<!-- SPARKLINES -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>

		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>

		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>

		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/select2/select2.min.js"></script>

		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>

		<!-- browser msie issue fix -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>

		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/fastclick/fastclick.min.js"></script>

		<script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
		<!-- Demo purpose only -->
		<script src="<?php echo base_url(); ?>assets/js/demo.min.js"></script>

		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url(); ?>assets/js/app.min.js"></script>

		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<!-- Voice command : plugin -->
		<script src="<?php echo base_url(); ?>assets/js/speech/voicecommand.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
		<!-- PAGE RELATED PLUGIN(S) -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.colVis.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.tableTools.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>

		<!-- ====================================================================================================== -->
		
		
		<script type="text/javascript">
		$(document).ready(function() {
			
			pageSetUp();
	
			/* BASIC ;*/
				var responsiveHelper_dt_basic = undefined;
				var responsiveHelper_datatable_fixed_column = undefined;
				var responsiveHelper_datatable_col_reorder = undefined;
				var responsiveHelper_datatable_tabletools = undefined;
				
				var breakpointDefinition = {
					tablet : 1024,
					phone : 480
				};
	
				$('#dt_basic').dataTable({
					"ordering": false,
					"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
					"autoWidth" : true,
					"preDrawCallback" : function() {
						// Initialize the responsive datatables helper once.
						if (!responsiveHelper_dt_basic) {
							responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
						}
					},
					"rowCallback" : function(nRow) {
						responsiveHelper_dt_basic.createExpandIcon(nRow);
					},
					"drawCallback" : function(oSettings) {
						responsiveHelper_dt_basic.respond();
					}
				});
		})

		</script>
	</body>
</html>
