<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
		<title> <?php echo $this->session->userdata('name')?>  </title>
		<meta name="description" content="">
		<meta name="author" content="">
			
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">

		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css"> 
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">


		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">

		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<!--estiloh-->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css"> 
		    <!--para las alertas-->
 
    <meta name="viewport" content="width=device-width">
		<!--fin de stiloh-->
          <script>
            function abreVentana(PDF)
            {             
                 var direccion;
                 direccion = '' + PDF;
                 window.open(direccion, "Reporte de Proyectos" , "width=800,height=650,scrollbars=SI") ; 
                                                                                
            }                                                 
          </script>
          	<style>
			/*////scroll tablas/////*/
			table{font-size: 10px;
            width: 100%;
            max-width:1550px;;
			    overflow-x: scroll;
            }
           #dt_basic2,#dt_basic3,#dt_basic4{
			  display: inline-block;
			width:100%;
			max-width:1550px;;
			    overflow-x: scroll;
			}
			/*///fin de scroll//*/
         </style>
	</head>
	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->
		<!-- HEADER -->
		<header id="header">
			<div id="logo-group">
				<span id="logo"> <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="SmartAdmin"> </span>
			</div>
			<!-- pulled right: nav area -->
			<div class="pull-right">
				<!-- collapse menu button -->
				<div id="hide-menu" class="btn-header pull-right">
					<span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
				</div>
				<!-- end collapse menu -->
				<!-- logout button -->
				<div id="logout" class="btn-header transparent pull-right">
					<span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Sign Out" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
				</div>
				<!-- end logout button -->
				<!-- search mobile button (this is hidden till mobile view port) -->
				<div id="search-mobile" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
				</div>
				<!-- end search mobile button -->
				<!-- fullscreen button -->
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen"><i class="fa fa-arrows-alt"></i></a> </span>
				</div>
				<!-- end fullscreen button -->
			</div>
			<!-- end pulled right: nav area -->
		</header>
		<!-- END HEADER -->
		<!-- Left panel : Navigation area -->
		<aside id="left-panel">
			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                        <span>
                            <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                        </span>
						<i class="fa fa-angle-down"></i>
					</a> 
				</span>
			</div>

			<nav>
				<ul>
					<li class="">
	                <a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MENÚ PRINCIPAL"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
	            	</li>
		            <li class="text-center">
		                <a href="#" title="PROGRAMACION"> <span class="menu-item-parent">REPORTES</span></a>
		            </li>
					<?php
		                for($i=0;$i<count($enlaces);$i++)
		                {
		                    if(count($subenlaces[$enlaces[$i]['o_child']])>0)
		                    {
			            ?>
			            <li>
			              	<a href="#" >
			              		<i class="<?php echo $enlaces[$i]['o_image']?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
			              	<ul >
			              	<?php
			                foreach ($subenlaces[$enlaces[$i]['o_child']] as $item) {
			                ?>
			                <li><a href="<?php echo base_url($item['o_url']); ?>"><?php echo $item['o_titulo']; ?></a></li>
			                <?php } ?>
			                </ul>
			            </li>
			            <?php 
			            }
			        } ?>
				</ul>
			</nav>
			<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>
		</aside>

		<!-- MAIN PANEL -->
		<div id="main" role="main">
			<!-- RIBBON -->
			<div id="ribbon">
				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>
				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li>Reportes</li><li>Otros Reportes</li><li>Acciones del POA</li>
				</ol>
			</div>

			<!-- MAIN CONTENT -->
			<div id="content">
				<section id="widget-grid" class="">
					<div class="row">
						<div class="alert alert-block alert-success" >
							<a class="close" data-dismiss="alert" href="#">×</a>
							<h4 class="alert-heading"><i class="glyphicon glyphicon-user"></i><?php echo " FUNCIONARIO RESPONSABLE : ".$this->session->userdata("funcionario")."   ||  "; ?><i class="glyphicon glyphicon-calendar"></i><?php echo "FECHA ACTUAL : " . date("d") . " del " . date("m") . " de " . date("Y");?></h4>
						</div>
						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				            <section id="widget-grid" class="well">
				              <div align="center">
				                <h1><b> <?php echo $this->session->userdata('entidad')?>  </b><br><small>ACCIONES DEL POA<br> AL MES DE <?php echo $mes;?>  de <?php echo $this->session->userdata("gestion");?></small></h1>
				              </div>
				            </section>
				        </article>
						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="jarviswidget jarviswidget-color-darken" >
                              	<header>
                                  	<span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
                                	<h2>PROYECTOS DE INVERSI&Oacute;N</h2>
                                </header>
								<div>
									<div class="widget-body no-padding">
									<div class="table-responsive">
										<table id="dt_basic" class="table table-bordered" style="width:100%;" font-size: "7px";>
											<thead>
												<tr>
													<th style="width:1%;"></th>
													<th><font size="1"><center>CATEGORIA PROGRAMATICA <?php echo $this->session->userdata("gestion");?></center></font></th>
													<th><font size="1"><center>&nbsp;DESCRIPCI&Oacute;N&nbsp;</center></font></th>
													<th><font size="1"><center>TIPO_DE_ACCI&Oacute;N</center></font></th>
													<th><font size="1"><center>SUB_SECTOR</center></font></th>
													<th><font size="1"><center>CODIGO_SISIN</center></font></th>
													<th><font size="1"><center>&nbsp;RESPONSABLE_(UE)&nbsp;</center></font></th>
													<th><font size="1"><center>&nbsp;UNIDAD_EJECUTORA&nbsp;</center></font></th>
													<th><font size="1"><center>&nbsp;FASE_ETAPA&nbsp;</center></font></th>
													<th><font size="1"><center>&nbsp;NUEVO_CONTINUIDAD&nbsp;</center></font></th>
													<th><font size="1"><center>&nbsp;ANUAL_PLURIANUAL&nbsp;</center></font></th>
													<th><font size="1"><center>&nbsp;COSTO TOTAL DEL PROYECTO&nbsp;</center></font></th>
													<th><font size="1"><center>&nbsp;PRESUPUESTO_<?php echo $this->session->userdata("gestion");?></center></font></th>
													<th><font size="1"><center>PROVINCIA/MUNICIPIO</center></font></th>
												</tr>
											</thead>
											<tbody>
											<?php $nro=1;
			                                    foreach($lista_aper_padres  as $rowa)
			                                    {
			                                    	echo '<tr bgcolor="#99DDF0">';
			                                    	 	echo '<td></td>';
			                                    	 	echo '<td><center>' .$rowa['aper_programa'].''.$rowa['aper_proyecto'].''.$rowa['aper_actividad'].'</td></td>';
											            echo '<td>'.$rowa['aper_descripcion'].'</td>';
											            echo '<td>'.$rowa['aper_sisin'].'</td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
			                                    	echo '</tr>';

			                                    	$proyectos=$this->model_proyecto->list_proyectos_poa($rowa['aper_programa'],4,1);
			                                    	foreach($proyectos  as $row)
			                                    	{
			                                    	if($row['tp_id']!=1){$color='#D7FCF9';}else{$color='#ffffff';}
			                                    	$fase= $this->model_faseetapa->get_id_fase($row['proy_id']); ////// datos fase activa
			                                    	$nro_f = $this->model_faseetapa->nro_fase($row['proy_id']); //// nro de fases 
			                                    	echo '<tr bgcolor='.$color.'>';
			                                    	echo '<td>'.$nro.'';
			                                       	?>
			                                       	<center><a href="javascript:abreVentana('<?php echo site_url("admin").'/dictamen_proyecto/'.$row['proy_id'].'/'.$fase[0]['pfec_ejecucion'].'' ?>');" title="DICTAMEN DEL PROYECTO"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Registro de Proyecto</a></center>
			                                       	
			                                        	<center><a data-toggle="modal" data-target="#<?php echo $nro;?>" title="MIS REPORTES" ><img src="<?php echo base_url(); ?>assets/ifinal/doc.jpg" WIDTH="40" HEIGHT="40"/><br>Mis reportes</a></center>
			                                        	<div class="modal fade bs-example-modal-lg" tabindex="-1" id="<?php echo $nro;?>"  role="dialog" aria-labelledby="myLargeModalLabel">
										                    <div class="modal-dialog modal-lg" role="document">
										                     	<div class="modal-content">
																	<div class="modal-header">
																		<button type="button" class="close text-danger" data-dismiss="modal" aria-hidden="true">
																			&times;
																		</button>
																	    <h4 class="modal-title">
																            <img src="<?php echo base_url(); ?>assets/img/logo.png" width="150" alt="SmartAdmin">
																        </h4>
																	</div>
																	<div class="modal-body no-padding">
																		<div class="well">
																			<table class="table table-hover">
																				<thead>
																				<tr>
																					<th colspan="3"><center><?php echo $row['proy_nombre'];?></center></th>
																				</tr>
																				</thead>
																				<tr>
																					<td><b>IDENTIFICACI&Oacute;N DE LA ETAPA</b></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/identificacion/'.$row['proy_id']; ?>');" title="IDENTIFICACION DE LA ETAPA"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/identificacion/'.$row['proy_id']; ?>');" title="IDENTIFICACION DE LA ETAPA"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
																				</tr>
																				<tr>
																					<td><b>PROGRAMACI&Oacute;N Y EJECUCI&Oacute;N F&Iacute;SICA DE LA ETAPA</b></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/rep_programacion/1/'.$fase[0]['id'].'/'.$row['proy_id']; ?>');" title="PROGRAMACION Y EJECUCION FISICA ANUAL DE LA ETAPA"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/rep_programacion/4/'.$fase[0]['id'].'/'.$row['proy_id']; ?>');" title="PROGRAMACION Y EJECUCION FISICA PLURI-ANUAL DE LA ETAPA"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
																				</tr>
																				<tr>
																					<td><b>PROGRAMACI&Oacute;N Y EJECUCI&Oacute;N PRESUPUESTARIA DE LA ETAPA</b></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/rep_financiero/1/'.$fase[0]['id'].'/'.$row['proy_id']; ?>');" title="PROGRAMACION Y EJECUCION FINANCIERA ANUAL DE LA ETAPA"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/rep_financiero/4/'.$fase[0]['id'].'/'.$row['proy_id']; ?>');" title="PROGRAMACION Y EJECUCION FINANCIERA PLURI-ANUAL DE LA ETAPA"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
																				</tr>
																				<tr>
																					<td><b>REGISTRO DE CONTRATOS</b></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/contratos/'.$row['proy_id'].'/'.$fase[0]['id'].''; ?>');" title="REPORTE DE CONTRATOS"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/contratos/'.$row['proy_id'].'/'.$fase[0]['id'].''; ?>');" title="REPORTE DE CONTRATOS"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
																				</tr>
																				<tr>
																					<td><b>INFORMES DE SUPERVISI&Oacute;N Y EVALUACI&Oacute;N</b></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/supervision_evaluacion/'.$row['proy_id'].'/'.$fase[0]['id'].''; ?>');" title="REPORTE DE CONTRATOS"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/supervision_evaluacion/'.$row['proy_id'].'/'.$fase[0]['id'].''; ?>');" title="REPORTE DE CONTRATOS"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
																				</tr>
																				<tr>
																					<td><b>CURVA "S" AVANCE F&Iacute;SICO </b></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/rep_curva_fis/1/'.$fase[0]['id'].'/'.$row['proy_id']; ?>');" title="GRAFICO AVANCE FISICO ANUAL"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/rep_curva_fis/4/'.$fase[0]['id'].'/'.$row['proy_id']; ?>');" title="GRAFICO AVANCE FISICO PLURI-ANUAL"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
																				</tr>
																				<tr>
																					<td><b>CURVA "S" EJECUCI&Oacute;N PRESUPUESTARIA</b></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/rep_curva_fin/1/'.$fase[0]['id'].'/'.$row['proy_id']; ?>');" title="GRAFICO AVANCE FINANCIERO ANUAL"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/rep_curva_fin/4/'.$fase[0]['id'].'/'.$row['proy_id']; ?>');" title="GRAFICO AVANCE FINANCIERO PLURI-ANUAL"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
																				</tr>
																			</table>
																		</div>
																	</div>
																</div><!-- /.modal-content -->
		                                                    </div>
		                                                </div>
			                                        	<?php
			                                       	echo '</td>';
			                                       	echo '<td>';
			                                        echo '<center>'.$row['aper_programa'].''.$row['aper_proyecto'].''.$row['aper_actividad'].'</center><br>';
			                                        if($row['proy_estado']==1)echo '<button class="btn btn-default btn-xs">T&eacute;cnico de<br>Planificaci&oacute;n</button>';
			                                        if($row['proy_estado']==2)echo '<button class="btn btn-default btn-xs">Validador POA</button>';
			                                        if($row['proy_estado']==3)echo '<button class="btn btn-default btn-xs">Validador<br>Financiero</button>';
			                                        if($row['proy_estado']==4)echo '<button class="btn btn-success btn-xs">APROBADO</button>';
			                                        echo'</td>';
			                                        echo '<td>'.$row['proy_nombre'].'</td>';
			                                        echo '<td>'.$row['tp_tipo'].'</td>';
			                                        $subsec=$this->model_proyecto->codigo_sectorial($row['codsectorial']); ////// Clasificador Sectorial
			                                        echo '<td>'.$subsec[0]['desc2'].'</td>';
			                                        echo '<td>'.$row['proy_sisin'].'</td>';
			                                        echo '<td>'.$row['fun_nombre'].' '.$row['fun_paterno'].' '.$row['fun_materno'].'</td>';
			                                        echo '<td>'.$row['ue'].'</td>';
			                                        /*==============================================================================================================*/
			                                        	$nc=$this->model_faseetapa->calcula_nc($fase[0]['pfec_fecha_inicio']); //// calcula nuevo/continuo
			                                        	$ap=$this->model_faseetapa->calcula_ap($fase[0]['pfec_fecha_inicio'],$fase[0]['pfec_fecha_fin']); //// calcula Anual/Plurianual
			                                        	echo '<td>* '.$fase[0]['fase'].'<br>* '.$fase[0]['etapa'].'</td>';
			                                        	echo '<td>'.$nc.'</td>';
			                                        	echo '<td>'.$ap.'</td>';
			                                        	echo '<td>'.number_format($fase[0]['pfec_ptto_fase'], 2, ',', ' ').' Bs.</td>';
			                                        	$fase_gest = $this->model_faseetapa->fase_etapa_gestion($fase[0]['id'],$this->session->userdata("gestion")); ////// datos fase activa presupuesto gestion actual
			                                        	$nro_fg_act = $this->model_faseetapa->nro_fasegestion_actual($fase[0]['id'],$this->session->userdata('gestion'));
			                                        	echo '<td>';
			                                        	if($this->model_faseetapa->verif_fase_etapa_gestion($fase[0]['id'],$this->session->userdata("gestion"))!=0)
			                                        	{	
			                                        		if($this->model_faseetapa->nro_ffofet($fase_gest[0]['ptofecg_id'])!=0)
			                                        		{
			                                        			$techo=$this->model_faseetapa->techo_presupuestario($fase_gest[0]['ptofecg_id']);
																echo ''.number_format($techo[0]['suma_techo'], 2, ',', ' ').' Bs.';

			                                        		}
			                                        		else{echo "<font color=red>S/T</font>";}
			                                        	}
			                                        	echo '</td>';
			                                        /*==============================================================================================================*/
			                                      	/*==================================== UBICACION GEOGRAFICA =====================================================*/
			                                        	echo '<td>';
					                                        echo '<table class="table table-bordered">
					                                        		<thead>';
																		$nro_prov= $this->model_proyecto->nro_proy_prov($row['proy_id']);
																		if($nro_prov!=0)
																		{
																			$prov= $this->model_proyecto->proy_prov($row['proy_id']);
																			foreach ($prov as $rowp)
										                                    {
										                                    	echo '<tr><td bgcolor="#696969"><center><b><font color="#ffffff">'.strtoupper($rowp['prov_provincia']).'</font></b></center></td></tr>';
										                                    	$nro_mun= $this->model_proyecto->nro_proy_mun($row['proy_id']);
										                                    	echo '<tr><td>';
										                                    	if($nro_mun!=0)
										                                    	{	$muni= $this->model_proyecto->proy_mun($row['proy_id']);
										                                    		echo '<table >';
										                                    		foreach ($muni as $rowm)
													                                {
													                                    if($rowm['prov_id']==$rowp['prov_id'])
													                                    {
													                                    	echo '<tr><td> - '.$rowm['muni_municipio'].'</td></tr>';
													                                    }
													                                }
													                                echo '</table>';
										                                    	}
										                                    	elseif ($nro_mun==0) 
										                                    	{
										                                    		echo "<font color='blue'>No tiene municipios</font>";
										                                    	}
										                                    	echo'</td></tr>';

										                                    }
																		}
							                                 			elseif($nro_prov==0) 
							                                 			{
								                                 			echo '<td>';
								                                 			?>
								                                 			<center><img src="<?php echo base_url(); ?>assets/Iconos/map_delete.png" WIDTH="30" HEIGHT="30"/></center>
								                                 			<?php
								                                 			echo '</td>';
							                                 			}
		                            							echo '</thead>
		                            						</table>';
					                                    echo'</td>';
					                                    /*==============================================================================================================*/
			                                    	echo '</tr>';$nro++;
			                                    	}
			                                    }
			                                ?>
											</tbody>
										</table>
										</div>

									</div>
									<!-- end widget content -->
				
								</div>
								<!-- end widget div -->
							</div>
							<!-- end widget -->
						</article>

						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="jarviswidget jarviswidget-color-darken" >
                              	<header>
                                  	<span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
                                	<h2>PROGRAMA RECURRENTE</h2>
                                </header>
								<div>
									<div class="widget-body no-padding">
									<div class="table-responsive">
										<table id="dt_basic2" class="table table-bordered" style="width:100%;" font-size: "7px";>
											<thead>
												<tr>
													<th style="width:1%;"></th>
													<th><font size="1"><center>CATEGORIA PROGRAMATICA <?php echo $this->session->userdata("gestion");?></center></font></th>
													<th><font size="1"><center>&nbsp;DESCRIPCI&Oacute;N&nbsp;</center></font></th>
													<th><font size="1"><center>TIPO_DE_ACCI&Oacute;N</center></font></th>
													<th><font size="1"><center>SUB_SECTOR</center></font></th>
													<th><font size="1"><center>CODIGO_SISIN</center></font></th>
													<th><font size="1"><center>&nbsp;RESPONSABLE_(UE)&nbsp;</center></font></th>
													<th><font size="1"><center>&nbsp;UNIDAD_EJECUTORA&nbsp;</center></font></th>
													<th><font size="1"><center>&nbsp;FASE_ETAPA&nbsp;</center></font></th>
													<th><font size="1"><center>&nbsp;NUEVO_CONTINUIDAD&nbsp;</center></font></th>
													<th><font size="1"><center>&nbsp;ANUAL_PLURIANUAL&nbsp;</center></font></th>
													<th><font size="1"><center>&nbsp;COSTO TOTAL DEL PROYECTO&nbsp;</center></font></th>
													<th><font size="1"><center>&nbsp;PRESUPUESTO_<?php echo $this->session->userdata("gestion");?></center></font></th>
													<th><font size="1"><center>PROVINCIA/MUNICIPIO</center></font></th>
												</tr>
											</thead>
											<tbody>
											<?php $nro=1;
			                                    foreach($lista_aper_padres  as $rowa)
			                                    {
			                                    	echo '<tr bgcolor="#99DDF0">';
			                                    	 	echo '<td></td>';
			                                    	 	echo '<td><center>' .$rowa['aper_programa'].''.$rowa['aper_proyecto'].''.$rowa['aper_actividad'].'</td></td>';
											            echo '<td>'.$rowa['aper_descripcion'].'</td>';
											            echo '<td>'.$rowa['aper_sisin'].'</td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
			                                    	echo '</tr>';

			                                    	$proyectos=$this->model_proyecto->list_proyectos_poa($rowa['aper_programa'],4,2);
			                                    	foreach($proyectos  as $row)
			                                    	{
			                                    	if($row['tp_id']!=1){$color='#D7FCF9';}else{$color='#ffffff';}
			                                    	$fase= $this->model_faseetapa->get_id_fase($row['proy_id']); ////// datos fase activa
			                                    	$nro_f = $this->model_faseetapa->nro_fase($row['proy_id']); //// nro de fases 
			                                    	echo '<tr bgcolor='.$color.'>';
			                                    	echo '<td>'.$nro.'';
			                                       	?>
			                                       	<center><a href="javascript:abreVentana('<?php echo site_url("admin").'/dictamen_proyecto/'.$row['proy_id'].'/'.$fase[0]['pfec_ejecucion'].'' ?>');" title="DICTAMEN DEL PROYECTO"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Registro de Proyecto</a></center>
			                                       	
			                                        	<center><a data-toggle="modal" data-target="#<?php echo $nro;?>" title="MIS REPORTES" ><img src="<?php echo base_url(); ?>assets/ifinal/doc.jpg" WIDTH="40" HEIGHT="40"/><br>Mis reportes</a></center>
			                                        	<div class="modal fade bs-example-modal-lg" tabindex="-1" id="<?php echo $nro;?>"  role="dialog" aria-labelledby="myLargeModalLabel">
										                    <div class="modal-dialog modal-lg" role="document">
										                     	<div class="modal-content">
																	<div class="modal-header">
																		<button type="button" class="close text-danger" data-dismiss="modal" aria-hidden="true">
																			&times;
																		</button>
																	    <h4 class="modal-title">
																            <img src="<?php echo base_url(); ?>assets/img/logo.png" width="150" alt="SmartAdmin">
																        </h4>
																	</div>
																	<div class="modal-body no-padding">
																		<div class="well">
																			<table class="table table-hover">
																				<thead>
																				<tr>
																					<th colspan="3"><center><?php echo $row['proy_nombre'];?></center></th>
																				</tr>
																				</thead>
																				<tr>
																					<td><b>IDENTIFICACI&Oacute;N DE LA ETAPA</b></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/identificacion/'.$row['proy_id']; ?>');" title="IDENTIFICACION DE LA ETAPA"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/identificacion/'.$row['proy_id']; ?>');" title="IDENTIFICACION DE LA ETAPA"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
																				</tr>
																				<tr>
																					<td><b>PROGRAMACI&Oacute;N Y EJECUCI&Oacute;N F&Iacute;SICA DE LA ETAPA</b></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/rep_programacion/1/'.$fase[0]['id'].'/'.$row['proy_id']; ?>');" title="PROGRAMACION Y EJECUCION FISICA ANUAL DE LA ETAPA"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/rep_programacion/4/'.$fase[0]['id'].'/'.$row['proy_id']; ?>');" title="PROGRAMACION Y EJECUCION FISICA PLURI-ANUAL DE LA ETAPA"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
																				</tr>
																				<tr>
																					<td><b>PROGRAMACI&Oacute;N Y EJECUCI&Oacute;N PRESUPUESTARIA DE LA ETAPA</b></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/rep_financiero/1/'.$fase[0]['id'].'/'.$row['proy_id']; ?>');" title="PROGRAMACION Y EJECUCION FINANCIERA ANUAL DE LA ETAPA"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/rep_financiero/4/'.$fase[0]['id'].'/'.$row['proy_id']; ?>');" title="PROGRAMACION Y EJECUCION FINANCIERA PLURI-ANUAL DE LA ETAPA"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
																				</tr>
																				<tr>
																					<td><b>REGISTRO DE CONTRATOS</b></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/contratos/'.$row['proy_id'].'/'.$fase[0]['id'].''; ?>');" title="REPORTE DE CONTRATOS"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/contratos/'.$row['proy_id'].'/'.$fase[0]['id'].''; ?>');" title="REPORTE DE CONTRATOS"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
																				</tr>
																				<tr>
																					<td><b>INFORMES DE SUPERVISI&Oacute;N Y EVALUACI&Oacute;N</b></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/supervision_evaluacion/'.$row['proy_id'].'/'.$fase[0]['id'].''; ?>');" title="REPORTE DE CONTRATOS"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/supervision_evaluacion/'.$row['proy_id'].'/'.$fase[0]['id'].''; ?>');" title="REPORTE DE CONTRATOS"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
																				</tr>
																				<tr>
																					<td><b>CURVA "S" AVANCE F&Iacute;SICO </b></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/rep_curva_fis/1/'.$fase[0]['id'].'/'.$row['proy_id']; ?>');" title="GRAFICO AVANCE FISICO ANUAL"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/rep_curva_fis/4/'.$fase[0]['id'].'/'.$row['proy_id']; ?>');" title="GRAFICO AVANCE FISICO PLURI-ANUAL"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
																				</tr>
																				<tr>
																					<td><b>CURVA "S" EJECUCI&Oacute;N PRESUPUESTARIA</b></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/rep_curva_fin/1/'.$fase[0]['id'].'/'.$row['proy_id']; ?>');" title="GRAFICO AVANCE FINANCIERO ANUAL"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/rep_curva_fin/4/'.$fase[0]['id'].'/'.$row['proy_id']; ?>');" title="GRAFICO AVANCE FINANCIERO PLURI-ANUAL"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
																				</tr>
																			</table>
																		</div>
																	</div>
																</div><!-- /.modal-content -->
		                                                    </div>
		                                                </div>
			                                        	<?php
			                                       	echo '</td>';
			                                       	echo '<td>';
			                                        echo '<center>'.$row['aper_programa'].''.$row['aper_proyecto'].''.$row['aper_actividad'].'</center><br>';
			                                        if($row['proy_estado']==1)echo '<button class="btn btn-default btn-xs">T&eacute;cnico de<br>Planificaci&oacute;n</button>';
			                                        if($row['proy_estado']==2)echo '<button class="btn btn-default btn-xs">Validador POA</button>';
			                                        if($row['proy_estado']==3)echo '<button class="btn btn-default btn-xs">Validador<br>Financiero</button>';
			                                        if($row['proy_estado']==4)echo '<button class="btn btn-success btn-xs">APROBADO</button>';
			                                        echo'</td>';
			                                        echo '<td>'.$row['proy_nombre'].'</td>';
			                                        echo '<td>'.$row['tp_tipo'].'</td>';
			                                        $subsec=$this->model_proyecto->codigo_sectorial($row['codsectorial']); ////// Clasificador Sectorial
			                                        echo '<td>'.$subsec[0]['desc2'].'</td>';
			                                        echo '<td>'.$row['proy_sisin'].'</td>';
			                                        echo '<td>'.$row['fun_nombre'].' '.$row['fun_paterno'].' '.$row['fun_materno'].'</td>';
			                                        echo '<td>'.$row['ue'].'</td>';
			                                        /*==============================================================================================================*/
			                                        	$nc=$this->model_faseetapa->calcula_nc($fase[0]['pfec_fecha_inicio']); //// calcula nuevo/continuo
			                                        	$ap=$this->model_faseetapa->calcula_ap($fase[0]['pfec_fecha_inicio'],$fase[0]['pfec_fecha_fin']); //// calcula Anual/Plurianual
			                                        	echo '<td>* '.$fase[0]['fase'].'<br>* '.$fase[0]['etapa'].'</td>';
			                                        	echo '<td>'.$nc.'</td>';
			                                        	echo '<td>'.$ap.'</td>';
			                                        	echo '<td>'.number_format($fase[0]['pfec_ptto_fase'], 2, ',', ' ').' Bs.</td>';
			                                        	$fase_gest = $this->model_faseetapa->fase_etapa_gestion($fase[0]['id'],$this->session->userdata("gestion")); ////// datos fase activa presupuesto gestion actual
			                                        	$nro_fg_act = $this->model_faseetapa->nro_fasegestion_actual($fase[0]['id'],$this->session->userdata('gestion'));
			                                        	echo '<td>';
			                                        	if($this->model_faseetapa->verif_fase_etapa_gestion($fase[0]['id'],$this->session->userdata("gestion"))!=0)
			                                        	{	
			                                        		if($this->model_faseetapa->nro_ffofet($fase_gest[0]['ptofecg_id'])!=0)
			                                        		{
			                                        			$techo=$this->model_faseetapa->techo_presupuestario($fase_gest[0]['ptofecg_id']);
																echo ''.number_format($techo[0]['suma_techo'], 2, ',', ' ').' Bs.';

			                                        		}
			                                        		else{echo "<font color=red>S/T</font>";}
			                                        	}
			                                        	echo '</td>';
			                                        /*==============================================================================================================*/
			                                      	/*==================================== UBICACION GEOGRAFICA =====================================================*/
			                                        	echo '<td>';
					                                        echo '<table class="table table-bordered">
					                                        		<thead>';
																		$nro_prov= $this->model_proyecto->nro_proy_prov($row['proy_id']);
																		if($nro_prov!=0)
																		{
																			$prov= $this->model_proyecto->proy_prov($row['proy_id']);
																			foreach ($prov as $rowp)
										                                    {
										                                    	echo '<tr><td bgcolor="#696969"><center><b><font color="#ffffff">'.strtoupper($rowp['prov_provincia']).'</font></b></center></td></tr>';
										                                    	$nro_mun= $this->model_proyecto->nro_proy_mun($row['proy_id']);
										                                    	echo '<tr><td>';
										                                    	if($nro_mun!=0)
										                                    	{	$muni= $this->model_proyecto->proy_mun($row['proy_id']);
										                                    		echo '<table >';
										                                    		foreach ($muni as $rowm)
													                                {
													                                    if($rowm['prov_id']==$rowp['prov_id'])
													                                    {
													                                    	echo '<tr><td> - '.$rowm['muni_municipio'].'</td></tr>';
													                                    }
													                                }
													                                echo '</table>';
										                                    	}
										                                    	elseif ($nro_mun==0) 
										                                    	{
										                                    		echo "<font color='blue'>No tiene municipios</font>";
										                                    	}
										                                    	echo'</td></tr>';

										                                    }
																		}
							                                 			elseif($nro_prov==0) 
							                                 			{
								                                 			echo '<td>';
								                                 			?>
								                                 			<center><img src="<?php echo base_url(); ?>assets/Iconos/map_delete.png" WIDTH="30" HEIGHT="30"/></center>
								                                 			<?php
								                                 			echo '</td>';
							                                 			}
		                            							echo '</thead>
		                            						</table>';
					                                    echo'</td>';
					                                    /*==============================================================================================================*/
			                                    	echo '</tr>';$nro++;
			                                    	}
			                                    }
			                                ?>
											</tbody>
										</table>
										</div>
									</div>
									<!-- end widget content -->
								</div>
								<!-- end widget div -->
							</div>
							<!-- end widget -->
						</article>

						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="jarviswidget jarviswidget-color-darken" >
                              	<header>
                                  	<span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
                                	<h2>PROGRAMA NO RECURRENTE</h2>
                                </header>
								<div>
									<div class="widget-body no-padding">
									<div class="table-responsive">
										<table id="dt_basic3" class="table table-bordered" style="width:100%;" font-size: "7px";>
											<thead>
												<tr>
													<th style="width:1%;"></th>
													<th><font size="1"><center>CATEGORIA PROGRAMATICA <?php echo $this->session->userdata("gestion");?></center></font></th>
													<th><font size="1"><center>&nbsp;DESCRIPCI&Oacute;N&nbsp;</center></font></th>
													<th><font size="1"><center>TIPO_DE_ACCI&Oacute;N</center></font></th>
													<th><font size="1"><center>SUB_SECTOR</center></font></th>
													<th><font size="1"><center>CODIGO_SISIN</center></font></th>
													<th><font size="1"><center>&nbsp;RESPONSABLE_(UE)&nbsp;</center></font></th>
													<th><font size="1"><center>&nbsp;UNIDAD_EJECUTORA&nbsp;</center></font></th>
													<th><font size="1"><center>&nbsp;FASE_ETAPA&nbsp;</center></font></th>
													<th><font size="1"><center>&nbsp;NUEVO_CONTINUIDAD&nbsp;</center></font></th>
													<th><font size="1"><center>&nbsp;ANUAL_PLURIANUAL&nbsp;</center></font></th>
													<th><font size="1"><center>&nbsp;COSTO TOTAL DEL PROYECTO&nbsp;</center></font></th>
													<th><font size="1"><center>&nbsp;PRESUPUESTO_<?php echo $this->session->userdata("gestion");?></center></font></th>
													<th><font size="1"><center>PROVINCIA/MUNICIPIO</center></font></th>
												</tr>
											</thead>
											<tbody>
											<?php $nro=1;
			                                    foreach($lista_aper_padres  as $rowa)
			                                    {
			                                    	echo '<tr bgcolor="#99DDF0">';
			                                    	 	echo '<td></td>';
			                                    	 	echo '<td><center>' .$rowa['aper_programa'].''.$rowa['aper_proyecto'].''.$rowa['aper_actividad'].'</td></td>';
											            echo '<td>'.$rowa['aper_descripcion'].'</td>';
											            echo '<td>'.$rowa['aper_sisin'].'</td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
			                                    	echo '</tr>';

			                                    	$proyectos=$this->model_proyecto->list_proyectos_poa($rowa['aper_programa'],4,3);
			                                    	foreach($proyectos  as $row)
			                                    	{
			                                    	if($row['tp_id']!=1){$color='#D7FCF9';}else{$color='#ffffff';}
			                                    	$fase= $this->model_faseetapa->get_id_fase($row['proy_id']); ////// datos fase activa
			                                    	$nro_f = $this->model_faseetapa->nro_fase($row['proy_id']); //// nro de fases 
			                                    	echo '<tr bgcolor='.$color.'>';
			                                    	echo '<td>'.$nro.'';
			                                       	?>
			                                       	<center><a href="javascript:abreVentana('<?php echo site_url("admin").'/dictamen_proyecto/'.$row['proy_id'].'/'.$fase[0]['pfec_ejecucion'].'' ?>');" title="DICTAMEN DEL PROYECTO"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Registro de Proyecto</a></center>
			                                       	
			                                        	<center><a data-toggle="modal" data-target="#<?php echo $nro;?>" title="MIS REPORTES" ><img src="<?php echo base_url(); ?>assets/ifinal/doc.jpg" WIDTH="40" HEIGHT="40"/><br>Mis reportes</a></center>
			                                        	<div class="modal fade bs-example-modal-lg" tabindex="-1" id="<?php echo $nro;?>"  role="dialog" aria-labelledby="myLargeModalLabel">
										                    <div class="modal-dialog modal-lg" role="document">
										                     	<div class="modal-content">
																	<div class="modal-header">
																		<button type="button" class="close text-danger" data-dismiss="modal" aria-hidden="true">
																			&times;
																		</button>
																	    <h4 class="modal-title">
																            <img src="<?php echo base_url(); ?>assets/img/logo.png" width="150" alt="SmartAdmin">
																        </h4>
																	</div>
																	<div class="modal-body no-padding">
																		<div class="well">
																			<table class="table table-hover">
																				<thead>
																				<tr>
																					<th colspan="3"><center><?php echo $row['proy_nombre'];?></center></th>
																				</tr>
																				</thead>
																				<tr>
																					<td><b>IDENTIFICACI&Oacute;N DE LA ETAPA</b></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/identificacion/'.$row['proy_id']; ?>');" title="IDENTIFICACION DE LA ETAPA"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/identificacion/'.$row['proy_id']; ?>');" title="IDENTIFICACION DE LA ETAPA"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
																				</tr>
																				<tr>
																					<td><b>PROGRAMACI&Oacute;N Y EJECUCI&Oacute;N F&Iacute;SICA DE LA ETAPA</b></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/rep_programacion/1/'.$fase[0]['id'].'/'.$row['proy_id']; ?>');" title="PROGRAMACION Y EJECUCION FISICA ANUAL DE LA ETAPA"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/rep_programacion/4/'.$fase[0]['id'].'/'.$row['proy_id']; ?>');" title="PROGRAMACION Y EJECUCION FISICA PLURI-ANUAL DE LA ETAPA"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
																				</tr>
																				<tr>
																					<td><b>PROGRAMACI&Oacute;N Y EJECUCI&Oacute;N PRESUPUESTARIA DE LA ETAPA</b></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/rep_financiero/1/'.$fase[0]['id'].'/'.$row['proy_id']; ?>');" title="PROGRAMACION Y EJECUCION FINANCIERA ANUAL DE LA ETAPA"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/rep_financiero/4/'.$fase[0]['id'].'/'.$row['proy_id']; ?>');" title="PROGRAMACION Y EJECUCION FINANCIERA PLURI-ANUAL DE LA ETAPA"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
																				</tr>
																				<tr>
																					<td><b>REGISTRO DE CONTRATOS</b></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/contratos/'.$row['proy_id'].'/'.$fase[0]['id'].''; ?>');" title="REPORTE DE CONTRATOS"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/contratos/'.$row['proy_id'].'/'.$fase[0]['id'].''; ?>');" title="REPORTE DE CONTRATOS"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
																				</tr>
																				<tr>
																					<td><b>INFORMES DE SUPERVISI&Oacute;N Y EVALUACI&Oacute;N</b></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/supervision_evaluacion/'.$row['proy_id'].'/'.$fase[0]['id'].''; ?>');" title="REPORTE DE CONTRATOS"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/supervision_evaluacion/'.$row['proy_id'].'/'.$fase[0]['id'].''; ?>');" title="REPORTE DE CONTRATOS"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
																				</tr>
																				<tr>
																					<td><b>CURVA "S" AVANCE F&Iacute;SICO </b></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/rep_curva_fis/1/'.$fase[0]['id'].'/'.$row['proy_id']; ?>');" title="GRAFICO AVANCE FISICO ANUAL"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/rep_curva_fis/4/'.$fase[0]['id'].'/'.$row['proy_id']; ?>');" title="GRAFICO AVANCE FISICO PLURI-ANUAL"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
																				</tr>
																				<tr>
																					<td><b>CURVA "S" EJECUCI&Oacute;N PRESUPUESTARIA</b></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/rep_curva_fin/1/'.$fase[0]['id'].'/'.$row['proy_id']; ?>');" title="GRAFICO AVANCE FINANCIERO ANUAL"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/rep_curva_fin/4/'.$fase[0]['id'].'/'.$row['proy_id']; ?>');" title="GRAFICO AVANCE FINANCIERO PLURI-ANUAL"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
																				</tr>
																			</table>
																		</div>
																	</div>
																</div><!-- /.modal-content -->
		                                                    </div>
		                                                </div>
			                                        	<?php
			                                       	echo '</td>';
			                                       	echo '<td>';
			                                        echo '<center>'.$row['aper_programa'].''.$row['aper_proyecto'].''.$row['aper_actividad'].'</center><br>';
			                                        if($row['proy_estado']==1)echo '<button class="btn btn-default btn-xs">T&eacute;cnico de<br>Planificaci&oacute;n</button>';
			                                        if($row['proy_estado']==2)echo '<button class="btn btn-default btn-xs">Validador POA</button>';
			                                        if($row['proy_estado']==3)echo '<button class="btn btn-default btn-xs">Validador<br>Financiero</button>';
			                                        if($row['proy_estado']==4)echo '<button class="btn btn-success btn-xs">APROBADO</button>';
			                                        echo'</td>';
			                                        echo '<td>'.$row['proy_nombre'].'</td>';
			                                        echo '<td>'.$row['tp_tipo'].'</td>';
			                                        $subsec=$this->model_proyecto->codigo_sectorial($row['codsectorial']); ////// Clasificador Sectorial
			                                        echo '<td>'.$subsec[0]['desc2'].'</td>';
			                                        echo '<td>'.$row['proy_sisin'].'</td>';
			                                        echo '<td>'.$row['fun_nombre'].' '.$row['fun_paterno'].' '.$row['fun_materno'].'</td>';
			                                        echo '<td>'.$row['ue'].'</td>';
			                                        /*==============================================================================================================*/
			                                        	$nc=$this->model_faseetapa->calcula_nc($fase[0]['pfec_fecha_inicio']); //// calcula nuevo/continuo
			                                        	$ap=$this->model_faseetapa->calcula_ap($fase[0]['pfec_fecha_inicio'],$fase[0]['pfec_fecha_fin']); //// calcula Anual/Plurianual
			                                        	echo '<td>* '.$fase[0]['fase'].'<br>* '.$fase[0]['etapa'].'</td>';
			                                        	echo '<td>'.$nc.'</td>';
			                                        	echo '<td>'.$ap.'</td>';
			                                        	echo '<td>'.number_format($fase[0]['pfec_ptto_fase'], 2, ',', ' ').' Bs.</td>';
			                                        	$fase_gest = $this->model_faseetapa->fase_etapa_gestion($fase[0]['id'],$this->session->userdata("gestion")); ////// datos fase activa presupuesto gestion actual
			                                        	$nro_fg_act = $this->model_faseetapa->nro_fasegestion_actual($fase[0]['id'],$this->session->userdata('gestion'));
			                                        	echo '<td>';
			                                        	if($this->model_faseetapa->verif_fase_etapa_gestion($fase[0]['id'],$this->session->userdata("gestion"))!=0)
			                                        	{	
			                                        		if($this->model_faseetapa->nro_ffofet($fase_gest[0]['ptofecg_id'])!=0)
			                                        		{
			                                        			$techo=$this->model_faseetapa->techo_presupuestario($fase_gest[0]['ptofecg_id']);
																echo ''.number_format($techo[0]['suma_techo'], 2, ',', ' ').' Bs.';

			                                        		}
			                                        		else{echo "<font color=red>S/T</font>";}
			                                        	}
			                                        	echo '</td>';
			                                        /*==============================================================================================================*/
			                                      	/*==================================== UBICACION GEOGRAFICA =====================================================*/
			                                        	echo '<td>';
					                                        echo '<table class="table table-bordered">
					                                        		<thead>';
																		$nro_prov= $this->model_proyecto->nro_proy_prov($row['proy_id']);
																		if($nro_prov!=0)
																		{
																			$prov= $this->model_proyecto->proy_prov($row['proy_id']);
																			foreach ($prov as $rowp)
										                                    {
										                                    	echo '<tr><td bgcolor="#696969"><center><b><font color="#ffffff">'.strtoupper($rowp['prov_provincia']).'</font></b></center></td></tr>';
										                                    	$nro_mun= $this->model_proyecto->nro_proy_mun($row['proy_id']);
										                                    	echo '<tr><td>';
										                                    	if($nro_mun!=0)
										                                    	{	$muni= $this->model_proyecto->proy_mun($row['proy_id']);
										                                    		echo '<table >';
										                                    		foreach ($muni as $rowm)
													                                {
													                                    if($rowm['prov_id']==$rowp['prov_id'])
													                                    {
													                                    	echo '<tr><td> - '.$rowm['muni_municipio'].'</td></tr>';
													                                    }
													                                }
													                                echo '</table>';
										                                    	}
										                                    	elseif ($nro_mun==0) 
										                                    	{
										                                    		echo "<font color='blue'>No tiene municipios</font>";
										                                    	}
										                                    	echo'</td></tr>';

										                                    }
																		}
							                                 			elseif($nro_prov==0) 
							                                 			{
								                                 			echo '<td>';
								                                 			?>
								                                 			<center><img src="<?php echo base_url(); ?>assets/Iconos/map_delete.png" WIDTH="30" HEIGHT="30"/></center>
								                                 			<?php
								                                 			echo '</td>';
							                                 			}
		                            							echo '</thead>
		                            						</table>';
					                                    echo'</td>';
					                                    /*==============================================================================================================*/
			                                    	echo '</tr>';$nro++;
			                                    	}
			                                    }
			                                ?>
											</tbody>
										</table>
										</div>

									</div>
									<!-- end widget content -->
								</div>
								<!-- end widget div -->
							</div>
							<!-- end widget -->
						</article>

						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="jarviswidget jarviswidget-color-darken" >
                              	<header>
                                  <span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
                                  <h2>ACCI&Oacute;N DE FUNCIONAMIENTO</h2>
                                </header>
								<div>
									<div class="widget-body no-padding">
									<div class="table-responsive">
										<table id="dt_basic4" class="table table-bordered" style="width:100%;" font-size: "7px";>
											<thead>
												<tr>
													<th style="width:1%;"></th>
													<th><font size="1"><center>CATEGORIA PROGRAMATICA <?php echo $this->session->userdata("gestion");?></center></font></th>
													<th><font size="1"><center>&nbsp;DESCRIPCI&Oacute;N&nbsp;</center></font></th>
													<th><font size="1"><center>TIPO_DE_ACCI&Oacute;N</center></font></th>
													<th><font size="1"><center>SUB_SECTOR</center></font></th>
													<th><font size="1"><center>CODIGO_SISIN</center></font></th>
													<th><font size="1"><center>&nbsp;RESPONSABLE_(UE)&nbsp;</center></font></th>
													<th><font size="1"><center>&nbsp;UNIDAD_EJECUTORA&nbsp;</center></font></th>
													<th><font size="1"><center>&nbsp;FASE_ETAPA&nbsp;</center></font></th>
													<th><font size="1"><center>&nbsp;NUEVO_CONTINUIDAD&nbsp;</center></font></th>
													<th><font size="1"><center>&nbsp;ANUAL_PLURIANUAL&nbsp;</center></font></th>
													<th><font size="1"><center>&nbsp;COSTO TOTAL DEL PROYECTO&nbsp;</center></font></th>
													<th><font size="1"><center>&nbsp;PRESUPUESTO_<?php echo $this->session->userdata("gestion");?></center></font></th>
													<th><font size="1"><center>PROVINCIA/MUNICIPIO</center></font></th>
												</tr>
											</thead>
											<tbody>
											<?php $nro=1;
			                                    foreach($lista_aper_padres  as $rowa)
			                                    {
			                                    	echo '<tr bgcolor="#99DDF0">';
			                                    	 	echo '<td></td>';
			                                    	 	echo '<td><center>' .$rowa['aper_programa'].''.$rowa['aper_proyecto'].''.$rowa['aper_actividad'].'</td></td>';
											            echo '<td>'.$rowa['aper_descripcion'].'</td>';
											            echo '<td>'.$rowa['aper_sisin'].'</td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
											            echo '<td></td>';
			                                    	echo '</tr>';

			                                    	$proyectos=$this->model_proyecto->list_proyectos_poa($rowa['aper_programa'],4,4);
			                                    	foreach($proyectos  as $row)
			                                    	{
			                                    	if($row['tp_id']!=1){$color='#D7FCF9';}else{$color='#ffffff';}
			                                    	$fase= $this->model_faseetapa->get_id_fase($row['proy_id']); ////// datos fase activa
			                                    	$nro_f = $this->model_faseetapa->nro_fase($row['proy_id']); //// nro de fases 
			                                    	echo '<tr bgcolor='.$color.'>';
			                                    	echo '<td>'.$nro.'';
			                                       	?>
			                                       	<center><a href="javascript:abreVentana('<?php echo site_url("admin").'/dictamen_proyecto/'.$row['proy_id'].'/'.$fase[0]['pfec_ejecucion'].'' ?>');" title="DICTAMEN DEL PROYECTO"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Registro de Proyecto</a></center>
			                                       	
			                                        	<center><a data-toggle="modal" data-target="#<?php echo $nro;?>" title="MIS REPORTES" ><img src="<?php echo base_url(); ?>assets/ifinal/doc.jpg" WIDTH="40" HEIGHT="40"/><br>Mis reportes</a></center>
			                                        	<div class="modal fade bs-example-modal-lg" tabindex="-1" id="<?php echo $nro;?>"  role="dialog" aria-labelledby="myLargeModalLabel">
										                    <div class="modal-dialog modal-lg" role="document">
										                     	<div class="modal-content">
																	<div class="modal-header">
																		<button type="button" class="close text-danger" data-dismiss="modal" aria-hidden="true">
																			&times;
																		</button>
																	    <h4 class="modal-title">
																            <img src="<?php echo base_url(); ?>assets/img/logo.png" width="150" alt="SmartAdmin">
																        </h4>
																	</div>
																	<div class="modal-body no-padding">
																		<div class="well">
																			<table class="table table-hover">
																				<thead>
																				<tr>
																					<th colspan="3"><center><?php echo $row['proy_nombre'];?></center></th>
																				</tr>
																				</thead>
																				<tr>
																					<td><b>IDENTIFICACI&Oacute;N DE LA ETAPA</b></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/identificacion/'.$row['proy_id']; ?>');" title="IDENTIFICACION DE LA ETAPA"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/identificacion/'.$row['proy_id']; ?>');" title="IDENTIFICACION DE LA ETAPA"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
																				</tr>
																				<tr>
																					<td><b>PROGRAMACI&Oacute;N Y EJECUCI&Oacute;N F&Iacute;SICA DE LA ETAPA</b></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/rep_programacion/1/'.$fase[0]['id'].'/'.$row['proy_id']; ?>');" title="PROGRAMACION Y EJECUCION FISICA ANUAL DE LA ETAPA"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/rep_programacion/4/'.$fase[0]['id'].'/'.$row['proy_id']; ?>');" title="PROGRAMACION Y EJECUCION FISICA PLURI-ANUAL DE LA ETAPA"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
																				</tr>
																				<tr>
																					<td><b>PROGRAMACI&Oacute;N Y EJECUCI&Oacute;N PRESUPUESTARIA DE LA ETAPA</b></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/rep_financiero/1/'.$fase[0]['id'].'/'.$row['proy_id']; ?>');" title="PROGRAMACION Y EJECUCION FINANCIERA ANUAL DE LA ETAPA"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/rep_financiero/4/'.$fase[0]['id'].'/'.$row['proy_id']; ?>');" title="PROGRAMACION Y EJECUCION FINANCIERA PLURI-ANUAL DE LA ETAPA"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
																				</tr>
																				<tr>
																					<td><b>REGISTRO DE CONTRATOS</b></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/contratos/'.$row['proy_id'].'/'.$fase[0]['id'].''; ?>');" title="REPORTE DE CONTRATOS"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/contratos/'.$row['proy_id'].'/'.$fase[0]['id'].''; ?>');" title="REPORTE DE CONTRATOS"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
																				</tr>
																				<tr>
																					<td><b>INFORMES DE SUPERVISI&Oacute;N Y EVALUACI&Oacute;N</b></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/supervision_evaluacion/'.$row['proy_id'].'/'.$fase[0]['id'].''; ?>');" title="REPORTE DE CONTRATOS"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/supervision_evaluacion/'.$row['proy_id'].'/'.$fase[0]['id'].''; ?>');" title="REPORTE DE CONTRATOS"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
																				</tr>
																				<tr>
																					<td><b>CURVA "S" AVANCE F&Iacute;SICO </b></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/rep_curva_fis/1/'.$fase[0]['id'].'/'.$row['proy_id']; ?>');" title="GRAFICO AVANCE FISICO ANUAL"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/rep_curva_fis/4/'.$fase[0]['id'].'/'.$row['proy_id']; ?>');" title="GRAFICO AVANCE FISICO PLURI-ANUAL"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
																				</tr>
																				<tr>
																					<td><b>CURVA "S" EJECUCI&Oacute;N PRESUPUESTARIA</b></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/rep_curva_fin/1/'.$fase[0]['id'].'/'.$row['proy_id']; ?>');" title="GRAFICO AVANCE FINANCIERO ANUAL"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Anual</a></center></td>
																					<td><center><a href="javascript:abreVentana('<?php echo site_url("admin").'/rep_curva_fin/4/'.$fase[0]['id'].'/'.$row['proy_id']; ?>');" title="GRAFICO AVANCE FINANCIERO PLURI-ANUAL"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Pluri Anual</a></center></td>
																				</tr>
																			</table>
																		</div>
																	</div>
																</div><!-- /.modal-content -->
		                                                    </div>
		                                                </div>
			                                        	<?php
			                                       	echo '</td>';
			                                       	echo '<td>';
			                                        echo '<center>'.$row['aper_programa'].''.$row['aper_proyecto'].''.$row['aper_actividad'].'</center><br>';
			                                        if($row['proy_estado']==1)echo '<button class="btn btn-default btn-xs">T&eacute;cnico de<br>Planificaci&oacute;n</button>';
			                                        if($row['proy_estado']==2)echo '<button class="btn btn-default btn-xs">Validador POA</button>';
			                                        if($row['proy_estado']==3)echo '<button class="btn btn-default btn-xs">Validador<br>Financiero</button>';
			                                        if($row['proy_estado']==4)echo '<button class="btn btn-success btn-xs">APROBADO</button>';
			                                        echo'</td>';
			                                        echo '<td>'.$row['proy_nombre'].'</td>';
			                                        echo '<td>'.$row['tp_tipo'].'</td>';
			                                        $subsec=$this->model_proyecto->codigo_sectorial($row['codsectorial']); ////// Clasificador Sectorial
			                                        echo '<td>'.$subsec[0]['desc2'].'</td>';
			                                        echo '<td>'.$row['proy_sisin'].'</td>';
			                                        echo '<td>'.$row['fun_nombre'].' '.$row['fun_paterno'].' '.$row['fun_materno'].'</td>';
			                                        echo '<td>'.$row['ue'].'</td>';
			                                        /*==============================================================================================================*/
			                                        	$nc=$this->model_faseetapa->calcula_nc($fase[0]['pfec_fecha_inicio']); //// calcula nuevo/continuo
			                                        	$ap=$this->model_faseetapa->calcula_ap($fase[0]['pfec_fecha_inicio'],$fase[0]['pfec_fecha_fin']); //// calcula Anual/Plurianual
			                                        	echo '<td>* '.$fase[0]['fase'].'<br>* '.$fase[0]['etapa'].'</td>';
			                                        	echo '<td>'.$nc.'</td>';
			                                        	echo '<td>'.$ap.'</td>';
			                                        	echo '<td>'.number_format($fase[0]['pfec_ptto_fase'], 2, ',', ' ').' Bs.</td>';
			                                        	$fase_gest = $this->model_faseetapa->fase_etapa_gestion($fase[0]['id'],$this->session->userdata("gestion")); ////// datos fase activa presupuesto gestion actual
			                                        	$nro_fg_act = $this->model_faseetapa->nro_fasegestion_actual($fase[0]['id'],$this->session->userdata('gestion'));
			                                        	echo '<td>';
			                                        	if($this->model_faseetapa->verif_fase_etapa_gestion($fase[0]['id'],$this->session->userdata("gestion"))!=0)
			                                        	{	
			                                        		if($this->model_faseetapa->nro_ffofet($fase_gest[0]['ptofecg_id'])!=0)
			                                        		{
			                                        			$techo=$this->model_faseetapa->techo_presupuestario($fase_gest[0]['ptofecg_id']);
																echo ''.number_format($techo[0]['suma_techo'], 2, ',', ' ').' Bs.';

			                                        		}
			                                        		else{echo "<font color=red>S/T</font>";}
			                                        	}
			                                        	echo '</td>';
			                                        /*==============================================================================================================*/
			                                      	/*==================================== UBICACION GEOGRAFICA =====================================================*/
			                                        	echo '<td>';
					                                        echo '<table class="table table-bordered">
					                                        		<thead>';
																		$nro_prov= $this->model_proyecto->nro_proy_prov($row['proy_id']);
																		if($nro_prov!=0)
																		{
																			$prov= $this->model_proyecto->proy_prov($row['proy_id']);
																			foreach ($prov as $rowp)
										                                    {
										                                    	echo '<tr><td bgcolor="#696969"><center><b><font color="#ffffff">'.strtoupper($rowp['prov_provincia']).'</font></b></center></td></tr>';
										                                    	$nro_mun= $this->model_proyecto->nro_proy_mun($row['proy_id']);
										                                    	echo '<tr><td>';
										                                    	if($nro_mun!=0)
										                                    	{	$muni= $this->model_proyecto->proy_mun($row['proy_id']);
										                                    		echo '<table >';
										                                    		foreach ($muni as $rowm)
													                                {
													                                    if($rowm['prov_id']==$rowp['prov_id'])
													                                    {
													                                    	echo '<tr><td> - '.$rowm['muni_municipio'].'</td></tr>';
													                                    }
													                                }
													                                echo '</table>';
										                                    	}
										                                    	elseif ($nro_mun==0) 
										                                    	{
										                                    		echo "<font color='blue'>No tiene municipios</font>";
										                                    	}
										                                    	echo'</td></tr>';

										                                    }
																		}
							                                 			elseif($nro_prov==0) 
							                                 			{
								                                 			echo '<td>';
								                                 			?>
								                                 			<center><img src="<?php echo base_url(); ?>assets/Iconos/map_delete.png" WIDTH="30" HEIGHT="30"/></center>
								                                 			<?php
								                                 			echo '</td>';
							                                 			}
		                            							echo '</thead>
		                            						</table>';
					                                    echo'</td>';
					                                    /*==============================================================================================================*/
			                                    	echo '</tr>';$nro++;
			                                    	}
			                                    }
			                                ?>
											</tbody>
										</table>
										</div>

									</div>
									<!-- end widget content -->
				
								</div>
								<!-- end widget div -->
							</div>
							<!-- end widget -->
						</article>
						<!-- WIDGET END -->
					</div>
				</section>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN PANEL -->
		<!-- ========================================================================================================= -->
		<!-- PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userdata('name')?> <?php echo $this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->

		<!--================================================== -->

		<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
		<script data-pace-options='{ "restartOnRequestAfter": true }' src="<?php echo base_url(); ?>assets/js/plugin/pace/pace.min.js"></script>

		<script>
			if (!window.jQuery) {
				document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-2.0.2.min.js"><\/script>');
			}
		</script>

		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>

		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url(); ?>assets/js/session_time/jquery-idletimer.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/app.config.js"></script>
		<script src = "<?php echo base_url(); ?>mis_js/control_session.js"></script>

		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 

		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.min.js"></script>

		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url(); ?>assets/js/notification/SmartNotification.min.js"></script>

		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url(); ?>assets/js/smartwidgets/jarvis.widget.min.js"></script>

		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

		<!-- SPARKLINES -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>

		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>

		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>

		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/select2/select2.min.js"></script>

		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>

		<!-- browser msie issue fix -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>

		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/fastclick/fastclick.min.js"></script>

		<!--[if IE 8]>

		<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

		<![endif]-->

		<!-- Demo purpose only -->
		<script src="<?php echo base_url(); ?>assets/js/demo.min.js"></script>

		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url(); ?>assets/js/app.min.js"></script>

		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<!-- Voice command : plugin -->
		<script src="<?php echo base_url(); ?>assets/js/speech/voicecommand.min.js"></script>


		<!-- PAGE RELATED PLUGIN(S) -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.colVis.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.tableTools.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>

		<!-- ======================================= PROYECTO OBSERVADO ======================================= -->
		<!-- ====================================================================================================== -->
		<script type="text/javascript">
		$(document).ready(function() {
			
			pageSetUp();
			/* BASIC ;*/
				var responsiveHelper_dt_basic = undefined;
				var responsiveHelper_datatable_fixed_column = undefined;
				var responsiveHelper_datatable_col_reorder = undefined;
				var responsiveHelper_datatable_tabletools = undefined;
				
				var breakpointDefinition = {
					tablet : 1024,
					phone : 480
				};
	
				$('#dt_basic').dataTable({
					"ordering": false,
					"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
					"autoWidth" : true,
					"preDrawCallback" : function() {
						// Initialize the responsive datatables helper once.
						if (!responsiveHelper_dt_basic) {
							responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
						}
					},
					"rowCallback" : function(nRow) {
						responsiveHelper_dt_basic.createExpandIcon(nRow);
					},
					"drawCallback" : function(oSettings) {
						responsiveHelper_dt_basic.respond();
					}
				});
				$('#dt_basic2').dataTable({
					"ordering": false,
					"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
					"autoWidth" : true,
					"preDrawCallback" : function() {
						// Initialize the responsive datatables helper once.
						if (!responsiveHelper_dt_basic) {
							responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic2'), breakpointDefinition);
						}
					},
					"rowCallback" : function(nRow) {
						responsiveHelper_dt_basic.createExpandIcon(nRow);
					},
					"drawCallback" : function(oSettings) {
						responsiveHelper_dt_basic.respond();
					}
				});
				$('#dt_basic3').dataTable({
					"ordering": false,
					"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
					"autoWidth" : true,
					"preDrawCallback" : function() {
						// Initialize the responsive datatables helper once.
						if (!responsiveHelper_dt_basic) {
							responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic2'), breakpointDefinition);
						}
					},
					"rowCallback" : function(nRow) {
						responsiveHelper_dt_basic.createExpandIcon(nRow);
					},
					"drawCallback" : function(oSettings) {
						responsiveHelper_dt_basic.respond();
					}
				});
				$('#dt_basic4').dataTable({
					"ordering": false,
					"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
					"autoWidth" : true,
					"preDrawCallback" : function() {
						// Initialize the responsive datatables helper once.
						if (!responsiveHelper_dt_basic) {
							responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic2'), breakpointDefinition);
						}
					},
					"rowCallback" : function(nRow) {
						responsiveHelper_dt_basic.createExpandIcon(nRow);
					},
					"drawCallback" : function(oSettings) {
						responsiveHelper_dt_basic.respond();
					}
				});

		})

		</script>
	</body>
</html>
