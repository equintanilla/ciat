<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>HTML</title>
    <meta name="description" content="" />
    <meta name="author" content="Swnk" />
    
    <meta name="viewport" content="width=device-width; initial-scale=1.0" />
    <link rel="apple-touch-icon" href="/apple-touch-icon.png" />
    <style>
      /*////scroll tablas/////*/
      table{font-size: 11px;
            width: 100%;
            max-width:1550px;;
          overflow-x: scroll;
            }
      </style>
  </head>
  <body>

  <table style="width:100%;">
    <tr>
      <td style="width:25%;">
        <img src="<?php echo base_url(); ?>assets/img/logo.png" class="superbox-img"/>
      </td>
      <td style="width:50%;">
        <table style="width:100%;" >
          <tr>
            <td><b>ENTIDAD : </b> <?php echo $this->session->userdata('entidad')?> </td>
          </tr>
          <tr>
            <td><b>POA - PLAN OPERATIVO ANUAL : </b> <?php echo $this->session->userdata("gestion")?></td>
          </tr>
          <tr>
            <td><b><?php echo $this->session->userdata('sistema')?> </b></td>
          </tr>
          <tr>
            <td><b>FORMULARIO : </b> EVALUACI&Oacute;N DEL PROYECTO </td>
          </tr>
        </table>
      </td>
      <td style="width:25%;" align="center">
        <img src="<?php echo base_url(); ?>assets/img/escudo.PNG" WIDTH="90" HEIGHT="100" class="superbox-img"/>
      </td>
    </tr>
  </table>

  <table class="table table-striped" border="1"> 
      <tbody>
          <tr>
              <td colspan="2" align="center">
                <b><?php echo $proyecto[0]['proy_nombre'];?></b><br>Proyecto/Actividad
              </td>
          </tr>
          <tr align="center">
              <td style="width:50%;">
                <b><?php  
                    if($proyecto[0]['tp_id']==1){echo $proyecto[0]['proy_sisin'];}else{echo "N/A";}?></b><br>SISIN
              </td>
              <td style="width:50%;">
                <b><?php echo $proyecto[0]['aper_programa'].''.$proyecto[0]['aper_proyecto'].''.$proyecto[0]['aper_actividad'];?></b><br>APERTURA PROGRAMATICA
              </td>
          </tr>
          <tr>
              <td colspan="2" align="center">
                <b><?php echo $unidad_responsable[0]['uejec'];?></b><br>Unidad Responsable
              </td>
          </tr>
          <tr>
              <td colspan="2" align="center">
                <b><?php echo $unidad_ejecutora[0]['uresp'];?></b><br>Unidad Ejecutora
              </td>
          </tr>
      </tbody>
  </table>
  <br>
  <b><u>IDENTIFICACI&Oacute;N METAS</u></b><br>
    <table class="table table-striped" border="1">
        <thead>
        <tr>
            <th width="5%"></th>
            <th width="30%">INDICADORES</th>
            <th width="10%">META</th>
            <th width="10%">EJECUCI&Oacute;N</th>
            <th width="10%">EFICACIA</th>
            <th width="10%"></th>
        </tr>
        </thead>
        <tbody id="tabla_ff">
            <?php
               if(count($metas)!=0)
               {
                  $nro=1;
                 foreach($metas as $row)
                 {
                  ?>
                  <tr align="center">
                      <td><?php echo $nro;?></td>
                      <td><?php echo $row['meta_descripcion'];?></td>
                      <td><?php echo $row['meta_meta'];?></td>
                      <td><?php echo $row['meta_ejec'];?></td>
                      <td><?php echo $row['meta_efic'];?> %</td>
                      <td>
                      <?php 
                          if($row['meta_rp']==0){echo "RESULTADO";}else {echo "PRODUCTO";};
                      ?>
                      </td>
                  </tr>
                  <?php
                  $nro++;
                 }
               }
               else
               {
                ?>
                <tr align="center">
                    <td colspan="6">No tiene metas registrados</td>
                </tr>
                <?php
               }
               
            ?>
        </tbody>
    </table>
    <br>
  <b><u>EJECUCI&Oacute;N PRESUPUESTARIA</u></b><br>
<!--   <?php
if($this->model_reporte->nro_costos_total_etapa($fase[0]['id'])!=0) //// tiene modificaciones Costo Total Etapa
            {
              $ct_etapa = $this->model_reporte->costos_total_etapa($fase[0]['id']);
              $tabla .= '<td bgcolor="#DEFAFA">'.number_format($ct_etapa[0]['inicial'], 2, ',', ' ').' Bs.</td>'; //// Inicial
              $tabla .= '<td bgcolor="#DEFAFA">'.number_format($ct_etapa[0]['diferencia'], 2, ',', ' ').' Bs.</td>'; //// Modificaciones
              $tabla .= '<td bgcolor="#DEFAFA">'.number_format($ct_etapa[0]['vigente'], 2, ',', ' ').' Bs.</td>'; ///// Vigente
              $fase_prog=$ct_etapa[0]['vigente'];
            }
  ?> -->
  </body>
</html>
