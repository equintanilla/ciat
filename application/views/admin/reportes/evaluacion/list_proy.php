<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
		<title><?php echo $this->session->userdata('name')?></title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css"> 
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<!--estiloh-->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css"> 
		<!--para las alertas-->
    <meta name="viewport" content="width=device-width">
		<!--fin de stiloh-->
          <script>
            function abreVentana(PDF){             
                 var direccion;
                 direccion = '' + PDF;
                 window.open(direccion, "Reporte de Proyectos" , "width=1000,height=650,scrollbars=SI") ; 
                                                                                
            }                                           
          </script>
          	<style>
			/*////scroll tablas/////*/
			table{font-size: 10px;
            width: 100%;
            max-width:1550px;;
			overflow-x: scroll;
            }
			</style>
	</head>
	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->

		<!-- HEADER -->
		<header id="header">
			<div id="logo-group">
				<span id="logo"> <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="SmartAdmin"> </span>
			</div>

			<!-- pulled right: nav area -->
			<div class="pull-right">
				
				<!-- collapse menu button -->
				<div id="hide-menu" class="btn-header pull-right">
					<span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
				</div>
				<!-- end collapse menu -->
				<!-- logout button -->
				<div id="logout" class="btn-header transparent pull-right">
					<span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Sign Out" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
				</div>
				<!-- end logout button -->
				<!-- search mobile button (this is hidden till mobile view port) -->
				<div id="search-mobile" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
				</div>
				<!-- end search mobile button -->
				<!-- fullscreen button -->
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen"><i class="fa fa-arrows-alt"></i></a> </span>
				</div>
				<!-- end fullscreen button -->

			</div>
			<!-- end pulled right: nav area -->

		</header>
		<!-- END HEADER -->

		<!-- Left panel : Navigation area -->
		<aside id="left-panel">

			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
					
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                              <span>
                                  <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                              </span>
						<i class="fa fa-angle-down"></i>
					</a> 
					
				</span>
			</div>

			<nav>
				<ul>
					<li class="">
	                <a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MENÚ PRINCIPAL"><i
	                        class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
	            	</li>
		            <li class="text-center">
		                <a href="#" title="REPORTES"> <span class="menu-item-parent">REPORTES</span></a>
		            </li>
				<?php
		                for($i=0;$i<count($enlaces);$i++)
		                {
		                    if(count($subenlaces[$enlaces[$i]['o_child']])>0)
		                    {
		            ?>
		            <li>
		              	<a href="#" >
		              		<i class="<?php echo $enlaces[$i]['o_image']?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
		              	<ul >
		              	<?php
		                foreach ($subenlaces[$enlaces[$i]['o_child']] as $item) {
		                ?>
		                <li><a href="<?php echo base_url($item['o_url']); ?>"><?php echo $item['o_titulo']; ?></a></li>
		                <?php } ?>
		                </ul>
		            </li>
		            <?php 
		            }
		        } ?>
				</ul>
			</nav>
			<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>

		</aside>

		<!-- MAIN PANEL -->
		<div id="main" role="main">

			<!-- RIBBON -->
			<div id="ribbon">

				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>

				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li>Reportes</li><li>Evaluacion de Acciones</li><li>Mis Acciones <?php echo $prog.'0000000';?></li>
				</ol>
			</div>

			<!-- MAIN CONTENT -->
			<div id="content">
				<section id="widget-grid" class="">
					<div class="row">
						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-10">
				            <section id="widget-grid" class="well">
				              <div class="">
				                <h1><b> <?php echo $this->session->userdata('entidad')?>  - </b><small> EVALUACI&Oacute;N DE LA ACCI&Oacute;N : <?php echo $prog.'0000000'?></small></h1>
				              </div>
				            </section>
				        </article>
				        <article class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
				            <section id="widget-grid" class="well">
				              <center>
				                <a href="<?php echo base_url().'index.php/admin/rep/evaluacion_acciones'?>" class="btn btn-success" title="Volver atras" style="width:100%;" >VOLVER ATRAS</a>
				              </center>
				            </section>
				        </article>
						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="jarviswidget jarviswidget-color-darken" >
                              <header>
                                  <span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
                                  <h2 class="font-md"><strong>C&Aacute;TEGORIA PROGRAM&Aacute;TICA : <?php echo $prog.'0000000'?></strong></h2>  
                              </header>
								<div>
									<div class="widget-body no-padding">
									<div class="table-responsive">
										<table id="dt_basic" class="table table-bordered" style="width:100%;" font-size: "7px";>
											<thead>
												<tr>
													<th style="width:1%;"></th>
													<th><font size="1"><center>CATEGORIA_PROGRAMATICA <?php echo $this->session->userdata("gestion");?></center></font></th>
													<th><font size="1"><center>&nbsp;PROYECTO_PROGRAMA_ACTIVIDAD&nbsp;</center></font></th>
													<th><font size="1"><center>TIPO_DE_ACCI&Oacute;N;</center></font></th>
													<th><font size="1"><center>&nbsp;CODIGO_SISIN&nbsp;</center></font></th>
													<th><font size="1"><center>&nbsp;RESPONSABLE (UE)&nbsp;</center></font></th>
													<th><font size="1"><center>&nbsp;UNIDAD_EJECUTORA&nbsp;</center></font></th>
													<th><font size="1"><center>&nbsp;UNIDAD_RESPONSABLE&nbsp;</center></font></th>
													<th><font size="1"><center>&nbsp;FASE_ETAPA&nbsp;</center></font></th>
													<th><font size="1"><center>&nbsp;NUEVO_CONTINUIDAD&nbsp;</center></font></th>
													<th><font size="1"><center>&nbsp;ANUAL_PLURIANUAL&nbsp;</center></font></th>
													<th><font size="1"><center>&nbsp;TOTAL PRESUPUESTO ETAPA&nbsp;</center></font></th>
													<th><font size="1"><center>&nbsp;PRESUPUESTO REQUERIDO&nbsp;<?php echo $this->session->userdata("gestion");?></center></font></th>
													<th><font size="1"><center>TECHO PRESUPUESTO ASIGNADO</center></font></th>
												</tr>
											</thead>
											<tbody>
											<?php $nro=1;
		                                    	foreach($acciones  as $row)
		                                    	{
		                                    		$fase= $this->model_faseetapa->get_id_fase($row['proy_id']); ////// datos fase activa
		                                    		if($row['tp_id']!=1){$color='#D7FCF9';}else{$color='#ffffff';}
		                                        	echo '<tr bgcolor='.$color.'>';
		                                    		echo '<td>'.$nro.'';
		                                        	?>
		                                        	<center><a href="javascript:abreVentana('<?php echo site_url("admin").'/dictamen_proyecto/'.$row['proy_id'].'/'.$row['pfec_ejecucion'].''; ?>');" title="DICTAMEN DEL PROYECTO"><img src="<?php echo base_url(); ?>assets/ifinal/pdf.png" WIDTH="40" HEIGHT="40"/><br>Dictamen</a></center>
		                                        	<?php
		                                        	echo '</td>';
			                                       	echo '<td>';
			                                        echo '<center>'.$row['aper_programa'].''.$row['aper_proyecto'].''.$row['aper_actividad'].'</center><br>';
			                                        if($row['proy_estado']==1)echo '<center><button class="btn btn-default btn-xs">T&eacute;cnico de<br>Planificaci&oacute;n</button></center>';
			                                        if($row['proy_estado']==2)echo '<center><button class="btn btn-default btn-xs">Validador POA</button></center>';
			                                        if($row['proy_estado']==3)echo '<center><button class="btn btn-default btn-xs">Validador<br>Financiero</button></center>';
			                                        if($row['proy_estado']==4)echo '<center><button class="btn btn-success btn-xs">APROBADO</button></center>';
			                                        ?>
			                                        <center><br><a href="<?php echo base_url().'index.php/admin/rep/eficacia/'.$row['proy_id'].'/'.$prog.'' ?>" class="btn btn-primary btn-xs" >EVALUACI&Oacute;N</a></center>
			                                        <?php
			                                        echo'</td>';
			                                        echo '<td>'.$row['proy_nombre'].'</td>';
			                                        echo '<td>'.$row['tp_tipo'].'</td>';
			                                        echo '<td>'.$row['proy_sisin'].'</td>';
			                                        /*================================================== RESPONSABLES ==============================================*/
			                                        echo '<td>'.$row['fun_nombre'].' '.$row['fun_paterno'].' '.$row['fun_materno'].'</td>';
			                                        echo '<td>'.$row['ue'].'</td>';
			                                        echo '<td>'.$row['ur'].'</td>';
			                                        $nc=$this->model_faseetapa->calcula_nc($row['pfec_fecha_inicio']); //// calcula nuevo/continuo
		                                        	$ap=$this->model_faseetapa->calcula_ap($row['pfec_fecha_inicio'],$row['pfec_fecha_fin']); //// calcula Anual/Plurianual
		                                        	echo '<td>* '.$fase[0]['fase'].'<br>* '.$fase[0]['etapa'].'</td>';
		                                        	echo '<td>'.$nc.'</td>';
		                                        	echo '<td>'.$ap.'</td>';
		                                        	echo '<td>'.number_format($fase[0]['pfec_ptto_fase'], 2, ',', ' ').' Bs.</td>';
		                                        	$fase_gest = $this->model_faseetapa->fase_etapa_gestion($fase[0]['id'],$this->session->userdata("gestion")); ////// datos fase activa presupuesto gestion actual
		                                        	$nro_fg_act = $this->model_faseetapa->nro_fasegestion_actual($fase[0]['id'],$this->session->userdata('gestion'));
		                                        	echo '<td>';
		                                        	if($nro_fg_act!=0 && ($fase_gest[0]['estado']==1 || $fase_gest[0]['estado']==2))
		                                        	{
		                                        		echo ''.number_format($fase_gest[0]['pfecg_ppto_total'], 2, ',', ' ').' Bs.';
		                                        	}
		                                        	elseif ($nro_fg_act==0) {
		                                        		echo '<font color="red">la gestion no esta en curso</font>';
		                                        	}
		                                        	echo '</td>';
		                                        	echo '<td>';
		                                        	if($this->model_faseetapa->verif_fase_etapa_gestion($fase[0]['id'],$this->session->userdata("gestion"))!=0)
		                                        	{	
		                                        		if($this->model_faseetapa->nro_ffofet($fase_gest[0]['ptofecg_id'])!=0)
		                                        		{
		                                        			$techo=$this->model_faseetapa->techo_presupuestario($fase_gest[0]['ptofecg_id']);
															echo ''.number_format($techo[0]['suma_techo'], 2, ',', ' ').' Bs.';

		                                        		}
		                                        		else{echo "<font color=red>S/T</font>";}
		                                        	}
		                                        	echo '</td>';$nro++;
		 
		                                        /*==============================================================================================================*/
		                                    	echo '</tr>';
		                                    	}
		                                    	
			                                ?>
											</tbody>
										</table>
										</div>

									</div>
									
								</div>
								<!-- end widget div -->
							</div>
							<!-- end widget -->
						</article>
						<!-- WIDGET END -->
					</div>
				</section>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN PANEL -->
		<!-- ========================================================================================================= -->
		<!-- PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->
		<!--================================================== -->
		<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
		<script data-pace-options='{ "restartOnRequestAfter": true }' src="<?php echo base_url(); ?>assets/js/plugin/pace/pace.min.js"></script>

		<script>
			if (!window.jQuery) {
				document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-2.0.2.min.js"><\/script>');
			}
		</script>

		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>

		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url(); ?>assets/js/session_time/jquery-idletimer.js"></script>
		<script src = "<?php echo base_url(); ?>mis_js/control_session.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/app.config.js"></script>
		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.min.js"></script>
		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url(); ?>assets/js/notification/SmartNotification.min.js"></script>
		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url(); ?>assets/js/smartwidgets/jarvis.widget.min.js"></script>
		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<!-- SPARKLINES -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/select2/select2.min.js"></script>
		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
		<!-- browser msie issue fix -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/fastclick/fastclick.min.js"></script>
		<!-- Demo purpose only -->
		<script src="<?php echo base_url(); ?>assets/js/demo.min.js"></script>
		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url(); ?>assets/js/app.min.js"></script>
		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<!-- Voice command : plugin -->
		<script src="<?php echo base_url(); ?>assets/js/speech/voicecommand.min.js"></script>

		<!-- PAGE RELATED PLUGIN(S) -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.colVis.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.tableTools.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>

		<!-- ====================================================================================================== -->
		<script type="text/javascript">
		// DO NOT REMOVE : GLOBAL FUNCTIONS!
		$(document).ready(function() {
			pageSetUp();
			/* BASIC ;*/
				var responsiveHelper_dt_basic = undefined;
				var responsiveHelper_datatable_fixed_column = undefined;
				var responsiveHelper_datatable_col_reorder = undefined;
				var responsiveHelper_datatable_tabletools = undefined;
				
				var breakpointDefinition = {
					tablet : 1024,
					phone : 480
				};
	
				$('#dt_basic').dataTable({
					"ordering": false,
					"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
					"autoWidth" : true,
					"preDrawCallback" : function() {
						// Initialize the responsive datatables helper once.
						if (!responsiveHelper_dt_basic) {
							responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
						}
					},
					"rowCallback" : function(nRow) {
						responsiveHelper_dt_basic.createExpandIcon(nRow);
					},
					"drawCallback" : function(oSettings) {
						responsiveHelper_dt_basic.respond();
					}
				});

		})

		</script>
	</body>
</html>
