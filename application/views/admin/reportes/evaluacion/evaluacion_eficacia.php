<!DOCTYPE html>
<html lang="en-us">
  <head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    <title><?php echo $this->session->userdata('name')?></title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- Basic Styles -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
    <!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css"> 
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
    <!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
    <!-- FAVICONS -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
    <!--estiloh-->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css"> 
        <!--para las alertas-->
    <meta name="viewport" content="width=device-width">
  </head>
  <body class="">
    <!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->

    <!-- HEADER -->
    <header id="header">
      <div id="logo-group">
        <span id="logo"> <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="SmartAdmin"> </span>
      </div>

      <!-- pulled right: nav area -->
      <div class="pull-right">
        <!-- collapse menu button -->
        <div id="hide-menu" class="btn-header pull-right">
          <span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
        </div>
        <!-- end collapse menu -->
        <!-- logout button -->
        <div id="logout" class="btn-header transparent pull-right">
          <span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Sign Out" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
        </div>
        <!-- end logout button -->
        <!-- search mobile button (this is hidden till mobile view port) -->
        <div id="search-mobile" class="btn-header transparent pull-right">
          <span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
        </div>
        <!-- end search mobile button -->
        <!-- fullscreen button -->
        <div id="fullscreen" class="btn-header transparent pull-right">
          <span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen"><i class="fa fa-arrows-alt"></i></a> </span>
        </div>
        <!-- end fullscreen button -->
      </div>
      <!-- end pulled right: nav area -->
    </header>
    <!-- END HEADER -->

    <!-- Left panel : Navigation area -->
    <aside id="left-panel">

      <!-- User info -->
      <div class="login-info">
        <span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
          
          <a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
              <span>
                  <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
              </span>
            <i class="fa fa-angle-down"></i>
          </a> 
          
        </span>
      </div>

      <nav>
        <ul>
          <li class="">
            <a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MENÚ PRINCIPAL"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
          </li>
          <li class="text-center">
            <a href="#" title="REPORTES"> <span class="menu-item-parent">REPORTES</span></a>
          </li>
          <?php
            for($i=0;$i<count($enlaces);$i++)
            {
              if(count($subenlaces[$enlaces[$i]['o_child']])>0)
              {
                ?>
                <li>
                  <a href="#" >
                    <i class="<?php echo $enlaces[$i]['o_image']?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
                  <ul >
                  <?php
                    foreach ($subenlaces[$enlaces[$i]['o_child']] as $item) {
                  ?>
                    <li><a href="<?php echo base_url($item['o_url']); ?>"><?php echo $item['o_titulo']; ?></a></li>
                    <?php } ?>
                  </ul>
                </li>
              <?php 
              }
            } ?>
        </ul>
      </nav>
      <span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>

    </aside>

		<!-- MAIN PANEL -->
		<div id="main" role="main">

			<!-- RIBBON -->
			<div id="ribbon">

				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>

				<!-- breadcrumb -->
				<ol class="breadcrumb">
				<li>Reportes</li><li>Evaluacion de Acciones</li><li><a href="<?php echo base_url().'index.php/admin/rep/proyectos/'.$prog.''?>">Mis Acciones <?php echo $prog.'0000000';?></a></li><li>Evaluaci&oacute;n Eficacia</li>
				</ol>
			</div>
			<!-- END RIBBON -->
			<!-- MAIN CONTENT -->
      <div id="content">
          <div class="well">
            <section id="widget-grid" class="">
                <!-- row -->
              <div class="row">
                <div class="alert alert-block alert-success" >
                  <a class="close" data-dismiss="alert" href="#">×</a>
                  <h4 class="alert-heading"><i class="glyphicon glyphicon-user"></i><?php echo " FUNCIONARIO RESPONSABLE : ".$this->session->userdata("funcionario")."   ||  "; ?><i class="glyphicon glyphicon-calendar"></i><?php echo "FECHA ACTUAL : " . date("d") . " del " . date("m") . " de " . date("Y");?></h4>
                </div>

                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-10">
                  <section id="widget-grid" class="well">
                    <div class="">
                      <h1><b><?php echo $this->session->userdata('entidad')?> - </b><small> EFICACIA</small></h1>
                    </div>
                  </section>
                </article>
                <article class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                  <section id="widget-grid" class="well">
                    <center>
                      <a href="<?php echo base_url().'index.php/admin/rep/proyectos/'.$prog.'' ?>" class="btn btn-success" title="Volver atras" style="width:100%;" >VOLVER ATRAS</a>
                    </center>
                  </section>
                </article>

                <form id="formulario" name="formulario"  method="post" action="<?php echo site_url("admin").'/rep/valida_eficacia' ?>">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-darken" >
                      <header>
                        <span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
                        <h2 class="font-md"><strong>METAS</strong></h2>        
                      </header>
                        <!-- widget content -->
                      <div class="widget-body">
                      <div class="table-responsive">
                        <input class="form-control" type="hidden" name="proy_id" value="<?php echo $proy_id;?>">
                        <input class="form-control" type="hidden" name="prog" value="<?php echo $prog;?>">
                        <input class="form-control" type="hidden" name="vig" value="<?php echo $vigente;?>">
                        <input class="form-control" type="hidden" name="ejec" value="<?php echo $ejecucion[0]['total'];?>">
                        <table class="table table-bordered">
                          <thead>                     
                            <tr>
                              <th style="width:1%;"><font size="1">Nro.</font></th>
                              <th style="width:10%;"><font size="1">METAS</font></th>
                              <th style="width:5%;"><font size="1">PROGRAMADO</font></th>
                              <th style="width:5%;"><font size="1">EJECUTADO</font></th>
                              <th style="width:5%;"><font size="1">EFICACIA</font></th>
                              <th style="width:5%;"><font size="1"></font></th>
                              <th style="width:5%;"><font size="1">% DE PRESUPUESTO</font></th>
                            </tr>
                          </thead>
                          <tbody>
                          <?php $nro=1;
                          foreach($metas as $row)
                          {
                            ?>
                            <tr>
                            <input class="form-control" type="hidden" name="meta_id[]" value="<?php echo $row['meta_id'];?>">
                              <td><?php echo $nro;?></td>
                              <td><?php echo $row['meta_descripcion'];?></td>
                              <td><?php echo $row['meta_meta'];?></td>
                              <td><?php echo $row['meta_ejec'];?></td>
                              <td><?php echo $row['meta_efic'];?> %</td>
                              <td><?php if($row['meta_rp']==0){echo "RESULTADO";}else {echo "PRODUCTO";};?></td>
                              <td><input class="form-control" type="text" name="ppto[]" id="ppto[]" value="<?php echo $row['pct_ppto'];?>"  onkeypress="if (this.value.length < 20) { return numerosDecimales(event);}else{return false; }" ></td>
                            </tr>
                            <?php
                            $nro++;
                          }
                          ?>                
                          </tbody>
                        </table>
                        </div>
                      </div>
                    </div>
                      <div class="form-actions">
                        <input type="submit" value="GENERAR REPORTE DE EVALUACI&Oacute;N" class="btn btn-primary btn-lg"  title="GENERAR LOS REPORTES SELECCIONADOS">
                      </div>
                    <!-- end widget div -->
                  </div>
                  <!-- end widget -->
                </article>
                </form>
              </div>
              </section>
            </div>
      </div>
			<!-- END MAIN CONTENT -->

		</div>
		<!-- END MAIN PANEL -->

		<!-- PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->

		<!--================================================== -->

		<script>
			if (!window.jQuery) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-2.0.2.min.js"><\/script>');
			}
		</script>

		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>

		<!-- IMPORTANT: APP CONFIG -->
    <script src="<?php echo base_url(); ?>assets/js/session_time/jquery-idletimer.js"></script>
    <script src = "<?php echo base_url(); ?>mis_js/control_session.js"></script>
		<script src="<?php echo base_url();?>/assets/js/app.config.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/mis_js/validacion_form.js"></script>
		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url();?>/assets/js/bootstrap/bootstrap.min.js"></script>
		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url();?>/assets/js/notification/SmartNotification.min.js"></script>
		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url();?>/assets/js/smartwidgets/jarvis.widget.min.js"></script>
		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url();?>/assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<!-- SPARKLINES -->
		<script src="<?php echo base_url();?>/assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url();?>/assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url();?>/assets/js/plugin/select2/select2.min.js"></script>
		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
		<!-- browser msie issue fix -->
		<script src="<?php echo base_url();?>/assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url();?>/assets/js/plugin/fastclick/fastclick.min.js"></script>
		<!-- Demo purpose only -->
		<script src="<?php echo base_url();?>/assets/js/demo.min.js"></script>
		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url();?>/assets/js/app.min.js"></script>
		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<!-- Voice command : plugin -->
		<script src="<?php echo base_url();?>/assets/js/speech/voicecommand.min.js"></script>
		<!-- PAGE RELATED PLUGIN(S) -->
		<script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
		<script src="<?php echo base_url();?>/assets/js/plugin/fuelux/wizard/wizard.min.js"></script>
	</body>

</html>
