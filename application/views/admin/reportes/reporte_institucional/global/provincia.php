<!DOCTYPE html>
<html lang="en-us">
  <head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

    <title> <?php echo $this->session->userdata('name')?> </title>
    <meta name="description" content="">
    <meta name="author" content="">
      
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- Basic Styles -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">

    <!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
    <!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">

    <!-- FAVICONS -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.core.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css" id="toggleCSS"/>
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
    <script>
        function abreVentana(PDF)
      {
        var direccion;
        direccion = '' + PDF;
        window.open(direccion, "Reporte de Proyectos" , "width=800,height=650,scrollbars=SI") ;
      }                                                  
    </script>
      <style type="text/css">
          table{font-size: 10px;
            width: 100%;
            max-width:1550px;;
            overflow-x: scroll;
            }
          th{
              padding: 1.4px;
              text-align: center;
              font-size: 9px;
              color: #ffffff;
            }
      </style>
  </head>
  <body class="">
    <!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->
    <!-- HEADER -->
    <header id="header">
      <div id="logo-group">
        <span id="logo"> <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="SmartAdmin"> </span>
      </div>

      <!-- pulled right: nav area -->
      <div class="pull-right">
        <!-- collapse menu button -->
        <div id="hide-menu" class="btn-header pull-right">
          <span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
        </div>
        <!-- end collapse menu -->
        <!-- logout button -->
        <div id="logout" class="btn-header transparent pull-right">
          <span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Sign Out" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
        </div>
        <!-- end logout button -->
        <!-- search mobile button (this is hidden till mobile view port) -->
        <div id="search-mobile" class="btn-header transparent pull-right">
          <span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
        </div>
        <!-- end search mobile button -->
        <!-- fullscreen button -->
        <div id="fullscreen" class="btn-header transparent pull-right">
          <span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen"><i class="fa fa-arrows-alt"></i></a> </span>
        </div>
        <!-- end fullscreen button -->
      </div>
      <!-- end pulled right: nav area -->
    </header>
    <!-- END HEADER -->

    <!-- Left panel : Navigation area -->
    <aside id="left-panel">
      <!-- User info -->
      <div class="login-info">
        <span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
          
          <a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
            <span>
                <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
            </span>
            <i class="fa fa-angle-down"></i>
          </a> 
          
        </span>
      </div>
      <nav>
        <ul>
          <li class="">
            <a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MENÚ PRINCIPAL">
            <i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
          </li>
          <li class="text-center">
            <a href='<?php echo site_url("admin").'/ejec/prog'; ?>' title="REGISTRO DE EJECUCION"> <span class="menu-item-parent">REPORTES</span></a>
          </li>
          <?php
            for($i=0;$i<count($enlaces);$i++)
            {
              if(count($subenlaces[$enlaces[$i]['o_child']])>0)
              {
                ?>
                <li>
                  <a href="#" >
                    <i class="<?php echo $enlaces[$i]['o_image']?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
                    <ul>
                    <?php
                    foreach ($subenlaces[$enlaces[$i]['o_child']] as $item) {
                    ?>
                    <li><a href="<?php echo base_url($item['o_url']); ?>"><?php echo $item['o_titulo']; ?></a></li>
                    <?php } ?>
                    </ul>
                </li>
                <?php 
              }
            } ?>
        </ul>
      </nav>
      <span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>
    </aside>

    <!-- MAIN PANEL -->
    <div id="main" role="main">
      <!-- RIBBON -->
      <div id="ribbon">
        <span class="ribbon-button-alignment"> 
          <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
            <i class="fa fa-refresh"></i>
          </span> 
        </span>
        <!-- breadcrumb -->
        <ol class="breadcrumb">
          <li>Reportes</li><li>Reportes Gerenciales</li><li>Resumen de Ejecuci&oacute;n Presupuestaria a Nivel Institucional</li><li>Por Provincia</li>
        </ol>
      </div>
      <!-- END RIBBON -->
      <!-- MAIN CONTENT -->
      <div id="content">
      <div class="well">
        <section id="widget-grid" class="">
          <!-- row -->
          <div class="row">
            <div class="alert alert-block alert-success" >
              <a class="close" data-dismiss="alert" href="#">×</a>
              <h4 class="alert-heading"><i class="glyphicon glyphicon-user"></i><?php echo " FUNCIONARIO RESPONSABLE : ".$this->session->userdata("funcionario")."   ||  "; ?><i class="glyphicon glyphicon-calendar"></i><?php echo "FECHA ACTUAL : " . date("d") . " del " . date("m") . " de " . date("Y");?></h4>
            </div>
          
          <article class="col-xs-12 col-sm-12 col-md-12 col-lg-10">
            <section id="widget-grid" class="well">
              <div align="center">
                  <h1><b> <?php echo $this->session->userdata('entidad')?> </b><br><small>RESUMEN DE EJECUCI&Oacute;N PRESUPUESTARIA A NIVEL INSTITUCIONAL<br><?php echo $pr1.''.$pr2.''.$pr3.''.$pr4;?><br>REPORTE POR PROVINCIA <br> AL <?php echo $dias.' de '.$mes;?>  DE <?php echo $this->session->userdata("gestion");?><br>(EXPRESADO EN BOLIVIANOS)</small></h1>
              </div>
            </section>
          </article>
          <article class="col-xs-12 col-sm-12 col-md-12 col-lg-2">
            <section id="widget-grid" class="well">
              <center>
                <a href="javascript:abreVentana('<?php echo site_url("admin").'/rep/reporte_por_provincia/'.$p1.'/'.$p2.'/'.$p3.'/'.$p4.'' ?>');" title="GENERAR REPORTE EN PDF" class="btn btn-primary" style="width:100%;">IMPRIMIR REPORTE</a>
                <a href="<?php echo base_url().'index.php/admin/rep/mis_acciones/'.$p1.'/'.$p2.'/'.$p3.'/'.$p4.'' ?>" class="btn btn-success" title="Volver atras" style="width:100%;" >VOLVER ATRAS</a>
              </center>
            </section>
          </article>

            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div class="jarviswidget jarviswidget-color-darken" >
                  <header>
                    <span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
                    </header>
                <div>
                  <div class="widget-body no-padding">
                    <div class="table-responsive">
                      <table id="dt_basic" class="table table table-bordered" width="100%">
                        <thead>
                          <tr>
                            <th bgcolor="#000000" rowspan="2">MUNICIPIO</th>
                            <th colspan="<?php echo $valor;?>" bgcolor="#000000" style="width:28.3%;" >CANTIDAD</th>
                            <th colspan="4" bgcolor="#000000" style="width:28.3%;">PRESUPUESTO</th>
                            <th colspan="3" bgcolor="#000000" style="width:28.3%;">EJECUCI&Oacute;N PRESUPUESTARIA</th> 
                          </tr>
                          <tr>
                            <?php 
                            if($p1==1){
                              ?>
                            <th bgcolor="#000000">INV.</th>
                            <th bgcolor="#000000">PINV.</th>
                              <?php
                            }
                            if($p2==1){
                              ?>
                            <th bgcolor="#000000">P.R.</th>
                              <?php
                            }
                            if($p3==1){
                              ?>
                            <th bgcolor="#000000">P.N.R.</th>
                              <?php
                            }
                            if($p4==1){
                              ?>
                            <th bgcolor="#000000">A.F.</th>
                              <?php
                            }
                            ?>
                            <th bgcolor="#000000">TOTAL</th>

                            <th bgcolor="#000000">INICIAL</th>
                            <th bgcolor="#000000">MODIFICACIONES</th>
                            <th bgcolor="#000000">VIGENTE</th>
                            <th bgcolor="#000000">% PROPORCI&Oacute;N</th>

                            <th bgcolor="#000000">EJECUTADO</th>
                            <th bgcolor="#000000">% DE EJECUCI&Oacute;N</th>
                            <th bgcolor="#000000">SALDO</th>
                          </tr>
                        </thead>
                         <tbody id="bdi">
                        <?php echo $tprov;?>
                        </tbody>
                         <tr>
                            <th bgcolor="#404040">TOTAL</th>
                            <?php 
                            if($p1==1){
                              ?>
                            <th bgcolor="#404040"><?php echo $inv;?></th>
                            <th bgcolor="#404040"><?php echo $pinv;?></th>
                              <?php
                            }
                            if($p2==1){
                              ?>
                            <th bgcolor="#404040"><?php echo $pr;?></th>
                              <?php
                            }
                            if($p3==1){
                              ?>
                            <th bgcolor="#404040"><?php echo $pnr;?></th>
                              <?php
                            }
                            if($p4==1){
                              ?>
                            <th bgcolor="#404040"><?php echo $af;?></th>
                              <?php
                            }
                            ?>
                            <th bgcolor="#404040"><?php echo $totalp;?></th>

                            <th bgcolor="#404040"><?php echo $ppto_i;?></th>
                            <th bgcolor="#404040"><?php echo $ppto_m;?></th>
                            <th bgcolor="#404040"><?php echo $ppto_v;?></th>
                            <th bgcolor="#404040"><?php echo round($p,1);?> %</th>

                            <th bgcolor="#404040"><?php echo $ppto_e;?></th>
                            <th bgcolor="#404040"><?php echo $e;?> %</th>
                            <th bgcolor="#404040"><?php echo $ppto_s;?></th>
                            
                          </tr>
                        </table>  
                      </div>
                    </div>
                    <!-- end widget content -->
                  </div>
                  <!-- end widget div -->
                </div>
                <!-- end widget -->
              </article>

          </div>
          <!-- end row -->
        </section>
        </div>
      </div>
      <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN PANEL -->

    <!-- PAGE FOOTER -->
    <div class="page-footer"> 
      <div class="row">
        <div class="col-xs-12 col-sm-6">
          <span class="txt-color-white"><?php echo $this->session->userdata('name')?>  <?php echo $this->session->userData('gestion') ?></span>
        </div>
      </div>
    </div>
    <!-- END PAGE FOOTER -->

    <!--================================================== -->
    <script>
      if (!window.jQuery) {
        document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-2.0.2.min.js"><\/script>');
      }
    </script>

    <script>
      if (!window.jQuery.ui) {
        document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
      }
    </script>

    <!-- IMPORTANT: APP CONFIG -->
    <script src="<?php echo base_url();?>/assets/js/app.config.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/mis_js/validacion_form.js"></script>
    <script src = "<?php echo base_url(); ?>mis_js/control_session.js"></script>
    <!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
    <script src="<?php echo base_url();?>/assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 

    <!-- BOOTSTRAP JS -->
    <script src="<?php echo base_url();?>/assets/js/bootstrap/bootstrap.min.js"></script>

    <!-- CUSTOM NOTIFICATION -->
    <script src="<?php echo base_url();?>/assets/js/notification/SmartNotification.min.js"></script>

    <!-- JARVIS WIDGETS -->
    <script src="<?php echo base_url();?>/assets/js/smartwidgets/jarvis.widget.min.js"></script>

    <!-- EASY PIE CHARTS -->
    <script src="<?php echo base_url();?>/assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

    <!-- SPARKLINES -->
    <script src="<?php echo base_url();?>/assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>

    <!-- JQUERY VALIDATE -->
    <script src="<?php echo base_url();?>/assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>

    <!-- JQUERY MASKED INPUT -->
    <script src="<?php echo base_url();?>/assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>

    <!-- JQUERY SELECT2 INPUT -->
    <script src="<?php echo base_url();?>/assets/js/plugin/select2/select2.min.js"></script>

    <!-- JQUERY UI + Bootstrap Slider -->
    <script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>

    <!-- browser msie issue fix -->
    <script src="<?php echo base_url();?>/assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>

    <!-- FastClick: For mobile devices -->
    <script src="<?php echo base_url();?>/assets/js/plugin/fastclick/fastclick.min.js"></script>

    <!-- Demo purpose only -->
    <script src="<?php echo base_url();?>/assets/js/demo.min.js"></script>

    <!-- MAIN APP JS FILE -->
    <script src="<?php echo base_url();?>/assets/js/app.min.js"></script>

    <!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
    <!-- Voice command : plugin -->
    <script src="<?php echo base_url();?>/assets/js/speech/voicecommand.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
    <!-- PAGE RELATED PLUGIN(S) -->
    <script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
    <script src="<?php echo base_url();?>/assets/js/plugin/fuelux/wizard/wizard.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.colVis.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.tableTools.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
    <!--================= ELIMINACION DE LAS METAS =========================================-->
  
    <script type="text/javascript">
    
    $(document).ready(function() {
      
      pageSetUp();
  
      /* BASIC ;*/
        var responsiveHelper_dt_basic = undefined;
        var responsiveHelper_dt_basic2 = undefined;
        var responsiveHelper_dt_basic3 = undefined;
        var responsiveHelper_datatable_fixed_column = undefined;
        var responsiveHelper_datatable_col_reorder = undefined;
        var responsiveHelper_datatable_tabletools = undefined;
        
        var breakpointDefinition = {
          tablet : 1024,
          phone : 480
        };
  
        $('#dt_basic').dataTable({
          "ordering": false,
          "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
            "t"+
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
          "autoWidth" : true,
          "preDrawCallback" : function() {
            // Initialize the responsive datatables helper once.
            if (!responsiveHelper_dt_basic) {
              responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
            }
          },
          "rowCallback" : function(nRow) {
            responsiveHelper_dt_basic.createExpandIcon(nRow);
          },
          "drawCallback" : function(oSettings) {
            responsiveHelper_dt_basic.respond();
          }
        });

    })

    </script>
  </body>
</html>
