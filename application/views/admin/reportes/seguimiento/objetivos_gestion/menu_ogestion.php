<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

		<title> <?php echo $this->session->userdata('name')?> </title>
		<meta name="description" content="">
		<meta name="author" content="">
			
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">

		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">


		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">

		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.core.css"/>
    	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css" id="toggleCSS"/>
	</head>
	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->

		<!-- HEADER -->
		<header id="header">
			<div id="logo-group">
				<span id="logo"> <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="SmartAdmin"> </span>
			</div>

			<!-- pulled right: nav area -->
			<div class="pull-right">
				
				<!-- collapse menu button -->
				<div id="hide-menu" class="btn-header pull-right">
					<span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
				</div>
				<!-- end collapse menu -->

				<!-- logout button -->
				<div id="logout" class="btn-header transparent pull-right">
					<span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Sign Out" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
				</div>
				<!-- end logout button -->

				<!-- search mobile button (this is hidden till mobile view port) -->
				<div id="search-mobile" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
				</div>
				<!-- end search mobile button -->

				<!-- fullscreen button -->
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen"><i class="fa fa-arrows-alt"></i></a> </span>
				</div>
				<!-- end fullscreen button -->
			</div>
			<!-- end pulled right: nav area -->

		</header>
		<!-- END HEADER -->

		<!-- Left panel : Navigation area -->
		<aside id="left-panel">
			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
					
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                              <span>
                                  <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                              </span>
						<i class="fa fa-angle-down"></i>
					</a> 
					
				</span>
			</div>
			<nav>
				<ul>
					<li class="">
	                <a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MENÚ PRINCIPAL">
	                <i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
	            	</li>
		            <li class="text-center">
		                <a href='<?php echo site_url("admin").'/ejec/prog'; ?>' title="REPORTES"> <span class="menu-item-parent">REPORTES</span></a>
		            </li>
				<?php
		                for($i=0;$i<count($enlaces);$i++)
		                {
		                    if(count($subenlaces[$enlaces[$i]['o_child']])>0)
		                    {
		            ?>
		            <li>
		              	<a href="#" >
		              		<i class="<?php echo $enlaces[$i]['o_image']?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
		              	<ul >
		              	<?php
		                foreach ($subenlaces[$enlaces[$i]['o_child']] as $item) {
		                ?>
		                <li><a href="<?php echo base_url($item['o_url']); ?>"><?php echo $item['o_titulo']; ?></a></li>
		                <?php } ?>
		                </ul>
		            </li>
		            <?php 
		            }
		        } ?>
				</ul>
			</nav>
			<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>
		</aside>

		<!-- MAIN PANEL -->
		<div id="main" role="main">

			<!-- RIBBON -->
			<div id="ribbon">
				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>

				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li>Reportes</li><li><a href="#" title="MIS ACCIONES">Seguimiento</a></li><li>Objetivos de Gesti&oacute;n</li>
				</ol>
			</div>
			<!-- END RIBBON -->
			<!-- MAIN CONTENT -->
			<div id="content">

	        <div class="row ">
	        	<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			            <section id="widget-grid" class="well">
	                      <div align="center">
	                          <h1><b> <?php echo $this->session->userdata('entidad')?>  </b><br><small> REPORTES A NIVEL OBJETIVOS DE GESTI&Oacute;N <br>AL <?php echo $dias.' de '.$mes;?>  DE <?php echo $this->session->userdata("gestion");?></small></h1>
	                      </div>
			            </section>
			        </article>
	            <article>
	            <article class="col-xs-8 col-sm-8 col-md-8 col-lg-8 col-xs-offset-2 col-sm-offset-2 col-lg-offset-2 col-md-offset-2 animated fadeInRight">
	                <div class="jarviswidget jarviswidget-color-teal" id="wid-id-0" data-widget-editbutton="false">
	                    <header>
	                        <span class="widget-icon"> <span class="fa fa-table"></span> </span>
	                        <h2></h2>
	                    </header>
	                    <div>
	                        <div class="widget-body no-padding">
	                            <div class="table-responsive">
	                                <div class="row">
	                                    <div class="col-sm-2 text-center"><br>
	                                        <a href="<?php echo site_url("admin") ?>/rep/rep_og_institucion" title="A NIVEL INSTITUCIONAL">
	                                            <p class="alert alert-info">
	                                                <i class="fa fa-file-text fa-5x"></i>
	                                            </p>
	                                        </a>
	                                    </div>
	                                    <div class="col-sm-10 text-align-center">
	                                        <label for=""><br><h5><b>RESUMEN DE LA PROGRAMACI&Oacute;N Y EJECUCI&Oacute;N DE OBJETIVOS DE GESTI&Oacute;N A NIVEL INSTITUCIONAL</b></h5>
	                                        </label>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </article>
	            <article class="col-xs-8 col-sm-8 col-md-8 col-lg-8 col-xs-offset-2 col-sm-offset-2 col-lg-offset-2 col-md-offset-2 animated fadeInRight">
	                <div class="jarviswidget jarviswidget-color-teal" id="wid-id-0" data-widget-editbutton="false">
	                    <header>
	                        <span class="widget-icon"> <span class="fa fa-table"></span> </span>
	                        <h2></h2>
	                    </header>
	                    <div>
	                        <div class="widget-body no-padding">
	                            <div class="table-responsive">
	                                <div class="row">
	                                    <div class="col-sm-2 text-center"><br>
	                                        <a href="#" data-toggle="modal" data-target="#modal_prog" class="btn btn-xs nuevo_ff" title="POR PROGRAMAS">
	                                            <p class="alert alert-info">
	                                                <i class="fa fa-file-text fa-5x"></i>
	                                            </p>
	                                        </a>
	                                    </div>
	                                    <div class="col-sm-10 text-align-center">
	                                        <label for=""><br><h5><b>RESUMEN DE LA PROGRAMACI&Oacute;N Y EJECUCI&Oacute;N DE OBJETIVOS DE GESTI&Oacute;N A NIVEL DE PROGRAMAS</b></h5>
	                                        </label>
	                                    </div>
	                                                    	
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </article>
	            <article class="col-xs-8 col-sm-8 col-md-8 col-lg-8 col-xs-offset-2 col-sm-offset-2 col-lg-offset-2 col-md-offset-2 animated fadeInRight">
	                <div class="jarviswidget jarviswidget-color-teal" id="wid-id-0" data-widget-editbutton="false">
	                    <header>
	                        <span class="widget-icon"> <span class="fa fa-table"></span> </span>
	                        <h2></h2>
	                    </header>
	                    <div>
	                        <div class="widget-body no-padding">
	                            <div class="table-responsive">
	                                <div class="row">
	                                    <div class="col-sm-2 text-center"><br>
	                                        <a href="<?php echo site_url("admin") ?>/rep/rep_og_objetivo" title="POR OBJETIVOS DE GESTION">
	                                            <p class="alert alert-info">
	                                                <i class="fa fa-file-text fa-5x"></i>
	                                            </p>
	                                        </a>
	                                    </div>
	                                    <div class="col-sm-10 text-align-center">
	                                        <label for=""><br><h5><b>RESUMEN DE LA PROGRAMACI&Oacute;N Y EJECUCI&Oacute;N A NIVEL DE OBJETIVOS DE GESTI&Oacute;N</b></h5>
	                                        </label>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </article>
	        </div>
			</div>
			<!-- END MAIN CONTENT -->


		    <!-- ================== Modal NUEVO PROGRAMAS  ========================== -->
		    <div class="modal animated fadeInDown" id="modal_prog" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
		        <div class="modal-dialog">
		            <div class="modal-content">
		                <div class="modal-header">
		                    <button type="button" class="close text-danger " data-dismiss="modal" aria-hidden="true">
		                        &times;
		                    </button>
		                    <h4 class="modal-title">
		                        <img src="<?php echo base_url(); ?>assets/img/logo.png" width="150" alt="SmartAdmin">
		                    </h4>
		                    <h4 class="modal-title text-center text-info">
		                        <b><i class="glyphicon glyphicon-pencil"></i> POR PROGRAMAS </b>
		                    </h4>
		                </div>
		                <div class="modal-body no-padding">
		                    <div class="row">
		                        
		                        <div class="well">
									<table  class="table table table-bordered" width="100%">
										<thead>
										<tr>
											<th><center></center></th>
											<th><center>PROGRAMA</center></th>
											<th><center>DETALLE</center></th>
										</tr>
										</thead>
										<?php 
										foreach ($programas as $row) 
										{
											?>
											<tr>
												<td><a href='<?php echo site_url("admin").'/rep/rep_og_programa/'.$row['poa_id']; ?>'><img src="<?php echo base_url(); ?>assets/ifinal/folder.png"></a></td>
												<td><b><center><?php echo $row['aper_programa'].''.$row['aper_proyecto'].''.$row['aper_actividad']; ?></center></b></td>
												<td><b><?php echo $row['aper_descripcion'];?></b></td>
											</tr>
											<?php
										}
										?>
										
									</table>
								</div>
		                    </div>
		                </div>

		            </div><!-- /.modal-content -->
		        </div><!-- /.modal-dialog -->
		    </div>
		   	<!-- ================== Modal NUEVO OBJETIVO  ========================== -->

		        </div><!-- /.modal-dialog -->
		    </div> -->
		</div>
		<!-- END MAIN PANEL -->
		<!-- PAGE FOOTER -->
		<div class="page-footer"> 
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userdata('name')?> <?php echo $this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->

		<!--================================================== -->
		<script>
			if (!window.jQuery) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-2.0.2.min.js"><\/script>');
			}
		</script>

		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>

		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url();?>/assets/js/app.config.js"></script>
		

		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script>

<SCRIPT src="<?php echo base_url(); ?>mis_js/programacion/ejecucion/abm_ejecucion.js" type="text/javascript"></SCRIPT>
<!-- BOOTSTRAP JS -->
<script src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.min.js"></script>

<!-- CUSTOM NOTIFICATION -->
<script src="<?php echo base_url(); ?>assets/js/notification/SmartNotification.min.js"></script>

<!-- JQUERY VALIDATE -->
<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>

<!-- JQUERY MASKED INPUT -->
<script src="<?php echo base_url(); ?>assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>

<!-- JQUERY SELECT2 INPUT -->
<script src="<?php echo base_url(); ?>assets/js/plugin/select2/select2.min.js"></script>

<!-- JQUERY UI + Bootstrap Slider -->
<script src="<?php echo base_url(); ?>assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>

<!-- browser msie issue fix -->
<script src="<?php echo base_url(); ?>assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>

<!-- FastClick: For mobile devices -->
<script src="<?php echo base_url(); ?>assets/js/plugin/fastclick/fastclick.min.js"></script>

<!-- Demo purpose only -->
<script src="<?php echo base_url(); ?>assets/js/demo.min.js"></script>

<!-- MAIN APP JS FILE -->
<script src="<?php echo base_url(); ?>assets/js/app.min.js"></script>

<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
<!-- Voice command : plugin -->
<script src="<?php echo base_url(); ?>assets/js/speech/voicecommand.min.js"></script>
<!--alertas -->
<script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
<!-- PAGE RELATED PLUGIN(S) -->
<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
	<!--================= NUEVO FUENTE FINANCIAMIENTO =========================================-->

	</body>
</html>
