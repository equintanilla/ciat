<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
		<title> <?php echo $this->session->userdata('name')?> </title>
		<meta name="description" content="">
		<meta name="author" content="">
			
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">

		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">

		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.core.css"/>
    	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css" id="toggleCSS"/>
		<script>
	        function abreVentana(PDF)
	      {
	        var direccion;
	        direccion = '' + PDF;
	        window.open(direccion, "Reporte de Proyectos" , "width=800,height=650,scrollbars=SI") ;
	      }                                                  
	    </script>
      	<style type="text/css">
          table{font-size: 9px;
            width: 100%;
            max-width:1550px;;
            overflow-x: scroll;
            }
          th{
              padding: 1.4px;
              text-align: center;
              font-size: 9px;
              color: #ffffff;
            }
      	</style>
	</head>
	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->

		<!-- HEADER -->
		<header id="header">
			<div id="logo-group">
				<span id="logo"> <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="SmartAdmin"> </span>
			</div>

			<!-- pulled right: nav area -->
			<div class="pull-right">
				
				<!-- collapse menu button -->
				<div id="hide-menu" class="btn-header pull-right">
					<span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
				</div>
				<!-- end collapse menu -->

				<!-- logout button -->
				<div id="logout" class="btn-header transparent pull-right">
					<span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Sign Out" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
				</div>
				<!-- end logout button -->

				<!-- search mobile button (this is hidden till mobile view port) -->
				<div id="search-mobile" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
				</div>
				<!-- end search mobile button -->

				<!-- fullscreen button -->
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen"><i class="fa fa-arrows-alt"></i></a> </span>
				</div>
				<!-- end fullscreen button -->
			</div>
			<!-- end pulled right: nav area -->

		</header>
		<!-- END HEADER -->

		<!-- Left panel : Navigation area -->
		<aside id="left-panel">
			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
					
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                              <span>
                                  <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                              </span>
						<i class="fa fa-angle-down"></i>
					</a> 
					
				</span>
			</div>
			<nav>
				<ul>
					<li class="">
	                <a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MENÚ PRINCIPAL">
	                <i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
	            	</li>
		            <li class="text-center">
		                <a href='<?php echo site_url("admin").'/ejec/prog'; ?>' title="REPORTES"> <span class="menu-item-parent">REPORTES</span></a>
		            </li>
				<?php
		                for($i=0;$i<count($enlaces);$i++)
		                {
		                    if(count($subenlaces[$enlaces[$i]['o_child']])>0)
		                    {
		            ?>
		            <li>
		              	<a href="#" >
		              		<i class="<?php echo $enlaces[$i]['o_image']?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
		              	<ul >
		              	<?php
		                foreach ($subenlaces[$enlaces[$i]['o_child']] as $item) {
		                ?>
		                <li><a href="<?php echo base_url($item['o_url']); ?>"><?php echo $item['o_titulo']; ?></a></li>
		                <?php } ?>
		                </ul>
		            </li>
		            <?php 
		            }
		        } ?>
				</ul>
			</nav>
			<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>
		</aside>

		<!-- MAIN PANEL -->
		<div id="main" role="main">

			<!-- RIBBON -->
			<div id="ribbon">
				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>

				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li>Reportes</li><li><a href="#" title="MIS ACCIONES">Seguimiento</a></li><li>Productos Terminales</li><li>A Nivel Objetivos de Gesti&oacute;n</li>
				</ol>
			</div>
			<!-- END RIBBON -->
			<!-- MAIN CONTENT -->
			<div id="content">
				<div class="row">
			        <div class="alert alert-block alert-success" >
		              <a class="close" data-dismiss="alert" href="#">×</a>
		              <h4 class="alert-heading"><i class="glyphicon glyphicon-user"></i><?php echo " FUNCIONARIO RESPONSABLE : ".$this->session->userdata("funcionario")."   ||  "; ?><i class="glyphicon glyphicon-calendar"></i><?php echo "FECHA ACTUAL : " . date("d") . " del " . date("m") . " de " . date("Y");?></h4>
		            </div>

		          <article class="col-xs-12 col-sm-12 col-md-12 col-lg-10">
		            <section id="widget-grid" class="well">
                      <div align="center">
                          <h1><b> <?php echo $this->session->userdata('entidad')?> </b><br><small>RESUMEN DE LA PROGRAMACI&Oacute;N Y EJECUCI&Oacute;N DE PRODUCTOS TERMINALES A NIVEL DE OBJETIVOS DE GESTI&Oacute;N<br>PROGRAMA : <?php echo $programa[0]['aper_programa'].''.$programa[0]['aper_proyecto'].''.$programa[0]['aper_actividad'].' - '.$programa[0]['aper_descripcion'];?> <br> OBJETIVO DE GESTI&Oacute;N : <?php echo $objetivo[0]['o_objetivo'];?><br>AL <?php echo $dias.' de '.$mes;?>  DE <?php echo $this->session->userdata("gestion");?></small></h1>
                      </div>
		            </section>
		          </article>
		          <article class="col-xs-12 col-sm-12 col-md-12 col-lg-2">
		            <section id="widget-grid" class="well">
		              <center>
		                <a href="<?php echo base_url().'index.php/admin/rep/rep_pt_oprog' ?>" class="btn btn-success" title="Volver atras" style="width:100%;" >VOLVER ATRAS</a>
		              </center>
		            </section>
		          </article>
		          <article class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
			            <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false">
							<header>
								<span class="widget-icon"> <i class="fa fa-table"></i> </span>
								<h2></h2>
							</header>
							<div>
								<div class="jarviswidget-editbox"></div>
								<div class="widget-body">
					                <div id="graf" class="col-md-12 col-sm-12 col-xs-6"></div>
								</div>
							</div>
						</div>
			        </article>
			        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
			            <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false">
							<header>
								<span class="widget-icon"> <i class="fa fa-table"></i> </span>
								<h2></h2>
							</header>
							<div>
								<div class="jarviswidget-editbox"></div>
								<div class="widget-body">
					                <div id="graf_eficacia"></div>
								</div>
							</div>
						</div>
			        </article>
	          		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<!-- Widget ID (each widget will need unique ID)-->
						<div class="jarviswidget jarviswidget-color-darken" id="wid-id-1" data-widget-editbutton="false">
							<header>
								<span class="widget-icon"> <i class="fa fa-table"></i> </span>
								<h2>NRO DE PRODUCTOS TERMINALES : <?php echo $nro_pt;?></h2>
							</header>

							<!-- widget div-->
							<div>
								<!-- widget edit box -->
								<div class="jarviswidget-editbox">
									<!-- This area used as dropdown edit box -->
								</div>
								<!-- end widget edit box -->
								<!-- widget content -->
								<div class="widget-body no-padding">
									<div class="table-responsive">
										<table class="table table-bordered table-striped">
											<thead>
												<tr>
								                  <th style="width:18%" rowspan=2 bgcolor="#000000"> ESTADO </th>
								                  <th colspan=12 bgcolor="#000000"> GESTI&Oacute;N : <?php echo $this->session->userData('gestion');?></th>
								                </tr>
								                <tr >
								                  <th bgcolor="#000000">ENERO</th>
								                  <th bgcolor="#000000">FEBRERO</th>
								                  <th bgcolor="#000000">MARZO</th>
								                  <th bgcolor="#000000">ABRIL</th>
								                  <th bgcolor="#000000">MAYO</th>
								                  <th bgcolor="#000000">JUNIO</th>
								                  <th bgcolor="#000000">JULIO</th>
								                  <th bgcolor="#000000">AGOSTO</th>
								                  <th bgcolor="#000000">SEPTIEMBRE</th>
								                  <th bgcolor="#000000">OCTUBRE</th>
								                  <th bgcolor="#000000">NOVIEMBRE</th>
								                  <th bgcolor="#000000">DICIEMBRE</th>
								                </tr>
											</thead>
											<tbody>
												<tr class="collapse_t" >
								                    <td><b><a href="<?php echo base_url().'index.php/admin/rep/rep_pt_obj/detalles/'.$poa_id.'/'.$o_id ?>" title="DETALLES">PROGRAMACI&Oacute;N ACUMULADA EN %</a></b></td>
								                    <?php 
								                    for($i=1;$i<=12;$i++)
								                    {
								                        echo '<td><b>'.$prog[$i].'%</b></td>';
								                    }
								                    ?>
								                </tr>
								                <tr class="collapse_t">
								                    <td><b><a href="<?php echo base_url().'index.php/admin/rep/rep_pt_obj/detalles/'.$poa_id.'/'.$o_id ?>" title="DETALLES">EJECUCI&Oacute;N ACUMULADA EN %</a></b></td>
								                    <?php
								                    for($i=1;$i<=12;$i++)
								                    {
								                        echo '<td><b>'.$ejec[$i].'%</b></td>';
								                    }
								                    ?>
								                </tr>
								                <tr class="collapse_t">
								                    <td><b><a href="<?php echo base_url().'index.php/admin/rep/rep_pt_obj/detalles/'.$poa_id.'/'.$o_id ?>" title="DETALLES">EFICACIA</a></b></td>
								                    <?php
								                    for($i=1;$i<=12;$i++)
								                    {
								                        echo '<td><b>'.$efi[$i].'%</b></td>';
								                    }
								                    
								                    ?>
								                </tr>
											</tbody>
										</table>
										
									</div>
								</div>
								<!-- end widget content -->
							</div>
							<!-- end widget div -->
						</div>
						<!-- end widget -->
					</article>
				</div>
			</div>
			<!-- END MAIN CONTENT -->

		</div>
		<!-- END MAIN PANEL -->
		<!-- PAGE FOOTER -->
		<div class="page-footer"> 
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userdata('name')?> <?php echo $this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->

		<!--================================================== -->
		<script>
			if (!window.jQuery) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-2.0.2.min.js"><\/script>');
			}
		</script>

		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>

		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url();?>/assets/js/app.config.js"></script>
		

		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script>

<SCRIPT src="<?php echo base_url(); ?>mis_js/programacion/ejecucion/abm_ejecucion.js" type="text/javascript"></SCRIPT>
<!-- BOOTSTRAP JS -->
<script src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.min.js"></script>

<!-- CUSTOM NOTIFICATION -->
<script src="<?php echo base_url(); ?>assets/js/notification/SmartNotification.min.js"></script>

<!-- JQUERY VALIDATE -->
<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>

<!-- JQUERY MASKED INPUT -->
<script src="<?php echo base_url(); ?>assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>

<!-- JQUERY SELECT2 INPUT -->
<script src="<?php echo base_url(); ?>assets/js/plugin/select2/select2.min.js"></script>

<!-- JQUERY UI + Bootstrap Slider -->
<script src="<?php echo base_url(); ?>assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>

<!-- browser msie issue fix -->
<script src="<?php echo base_url(); ?>assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>

<!-- FastClick: For mobile devices -->
<script src="<?php echo base_url(); ?>assets/js/plugin/fastclick/fastclick.min.js"></script>

<!-- Demo purpose only -->
<script src="<?php echo base_url(); ?>assets/js/demo.min.js"></script>

<!-- MAIN APP JS FILE -->
<script src="<?php echo base_url(); ?>assets/js/app.min.js"></script>

<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
<!-- Voice command : plugin -->
<script src="<?php echo base_url(); ?>assets/js/speech/voicecommand.min.js"></script>
<!--alertas -->
<script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
<!-- PAGE RELATED PLUGIN(S) -->
<script src="<?php echo base_url(); ?>assets/highcharts/js/highcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/highcharts/js/highcharts-3d.js"></script>
<script src="<?php echo base_url(); ?>assets/highcharts/js/modules/exporting.js"></script> 
<script type="text/javascript">
  var gestion=<?php echo $this->session->userdata("gestion");?>;
    var chart1 = new Highcharts.Chart({
          chart: {
              renderTo: 'graf', // div contenedor
              type: 'line' // tipo de grafico
          },

          title: {
              text: 'PROGRAMACI\u00D3N Y EJECUCI\u00D3N F\u00CDSICA : '+gestion, // t�tulo  del gr�fico
              x: -20 //center
          },
          subtitle: {
              text: ''
          },
          xAxis: {
              categories: ['ENERO', 'FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO', 'JULIO', 'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE', 'NOVIEMBRE', 'DICIEMBRE']
          },
          yAxis: {
              title: {
                  text: 'PORCENTAJES (%)'
              }
          },
          plotOptions: {
              line: {
                  dataLabels: {
                      enabled: true
                  },
                  enableMouseTracking: false
              }
          },
          series: [
              {
                  name: 'PROGRAMACIÓN ACUMULADA EN %',
                  data: [ <?php echo $prog[1];?>, <?php echo $prog[2];?>, <?php echo $prog[3];?>, <?php echo $prog[4];?>, <?php echo $prog[5];?>, <?php echo $prog[6];?>, <?php echo $prog[7];?>, <?php echo $prog[8];?>, <?php echo $prog[9];?>, <?php echo $prog[10];?>, <?php echo $prog[11];?>, <?php echo $prog[12];?>]
              },
              {
                  name: 'EJECUCIÓN ACUMULADA EN %',
                  data: [ <?php echo $ejec[1];?>, <?php echo $ejec[2];?>, <?php echo $ejec[3];?>, <?php echo $ejec[4];?>, <?php echo $ejec[5];?>, <?php echo $ejec[6];?>, <?php echo $ejec[7];?>, <?php echo $ejec[8];?>, <?php echo $ejec[9];?>, <?php echo $ejec[10];?>, <?php echo $ejec[11];?>, <?php echo $ejec[12];?>]
              }
          ]
      });
    </script>
    <script type="text/javascript">
      var gestion=<?php echo $this->session->userdata("gestion");?>;                       
      Highcharts.chart('graf_eficacia', {
          chart: {
              type: 'column'
          },
          title: {
              text: ' EFICACIA : '+gestion
          },
          xAxis: {
              categories: ['ENERO', 'FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO', 'JULIO', 'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE', 'NOVIEMBRE', 'DICIEMBRE']
          },
          yAxis: {
              min: 0,
              title: {
                  text: 'PORCENTAJES (%)'
              },
              stackLabels: {
                  enabled: true,
                  style: {
                      fontWeight: 'bold',
                      color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                  }
              }
          },
          legend: {
              align: 'right',
              x: -30,
              verticalAlign: 'top',
              y: 25,
              floating: true,
              backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
              borderColor: '#CCC',
              borderWidth: 1,
              shadow: false
          },
          tooltip: {
              headerFormat: '<b>{point.x}</b><br/>',
              pointFormat: '{series.name}: <br/> TOTAL:   {point.y}'
          },
          plotOptions: {
              column: {
                  stacking: 'normal',
                  dataLabels: {
                      enabled: false,
                      color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                  }
              }
          },
          series: [{

              name: '<b style="color: #FF0000;">MENOR A 75%</b>',
              data: [{y: <?php echo $menor[1]?>, color: 'red'},{y: <?php echo $menor[2]?>, color: 'red'},{y: <?php echo $menor[3]?>, color: 'red'},{y: <?php echo $menor[4]?>, color: 'red'},{y: <?php echo $menor[5]?>, color: 'red'},{y: <?php echo $menor[6]?>, color: 'red'},{y: <?php echo $menor[7]?>, color: 'red'},{y: <?php echo $menor[8]?>, color: 'red'},{y: <?php echo $menor[9]?>, color: 'red'},{y: <?php echo $menor[10]?>, color: 'red'},{y: <?php echo $menor[11]?>, color: 'red'},{y: <?php echo $menor[12]?>, color: 'red'}] 

          }, {
              name: '<b style="color: #d6d21f;">ENTRE 76% Y 90%</b>',
              data: [{y: <?php echo $entre[1]?>, color: 'yellow'},{y: <?php echo $entre[2]?>, color: 'yellow'},{y: <?php echo $entre[3]?>, color: 'yellow'},{y: <?php echo $entre[4]?>, color: 'yellow'},{y: <?php echo $entre[5]?>, color: 'yellow'},{y: <?php echo $entre[6]?>, color: 'yellow'},{y: <?php echo $entre[7]?>, color: 'yellow'},{y: <?php echo $entre[8]?>, color: 'yellow'},{y: <?php echo $entre[9]?>, color: 'yellow'},{y: <?php echo $entre[10]?>, color: 'yellow'},{y: <?php echo $entre[11]?>, color: 'yellow'},{y: <?php echo $entre[12]?>, color: 'yellow'}] 
          }, {
              name: '<b style="color: green;">MAYOR A 91%</b>',
              data: [{y: <?php echo $mayor[1]?>, color: 'green'},{y: <?php echo $mayor[2]?>, color: 'green'},{y: <?php echo $mayor[3]?>, color: 'green'},{y: <?php echo $mayor[4]?>, color: 'green'},{y: <?php echo $mayor[5]?>, color: 'green'},{y: <?php echo $mayor[6]?>, color: 'green'},{y: <?php echo $mayor[7]?>, color: 'green'},{y: <?php echo $mayor[8]?>, color: 'green'},{y: <?php echo $mayor[9]?>, color: 'green'},{y: <?php echo $mayor[10]?>, color: 'green'},{y: <?php echo $mayor[11]?>, color: 'green'},{y: <?php echo $mayor[12]?>, color: 'green'}] 
          }]

      });

      </script>
	</body>
</html>
