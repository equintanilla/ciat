<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
		<title><?php echo $this->session->userdata('name')?></title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css"> 
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
		<!--estiloh-->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css"> 
	    <meta name="viewport" content="width=device-width">
		<!--fin de stiloh-->
		<script>
	      function abreVentana(PDF)
	      {             
	           var direccion;
	           direccion = '' + PDF;
	           window.open(direccion, "Reporte de Proyectos" , "width=1200,height=650,scrollbars=NO") ;                                                              
	      }                                                 
	    </script>
		<style type="text/css">
		table{font-size: 11px;
            width: 100%;
			overflow-x: scroll;
        }
        th{
            padding: 1.4px;
            text-align: center;
            font-size: 11px;
        }
		tr.riesgo_alto td { background: #f18888; color: #000000; }
		tr.riesgo td { background: #f3b478; color: #000000; }
		tr.retraso td { background: #f7f778; color: #000000; }
		tr.tiempo td { background: #93f586; color: #000000; }
		tr.efi td { background: #cceacc; color: #000000; }
		</style>
	</head>
	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->
		<!-- HEADER -->
		<header id="header">
			<div id="logo-group">
				<!-- <span id="logo"> <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="SmartAdmin"> </span> -->
			</div>
			<!-- pulled right: nav area -->
			<div class="pull-right">
				<!-- collapse menu button -->
				<div id="hide-menu" class="btn-header pull-right">
					<span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
				</div>
				<!-- end collapse menu -->
				<!-- logout button -->
				<div id="logout" class="btn-header transparent pull-right">
					<span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Sign Out" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
				</div>
				<!-- end logout button -->
				<!-- search mobile button (this is hidden till mobile view port) -->
				<div id="search-mobile" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
				</div>
				<!-- end search mobile button -->
				<!-- fullscreen button -->
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen"><i class="fa fa-arrows-alt"></i></a> </span>
				</div>
				<!-- end fullscreen button -->
			</div>
			<!-- end pulled right: nav area -->
		</header>
		<!-- END HEADER -->

		<!-- Left panel : Navigation area -->
		<aside id="left-panel">

			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                        <span>
                            <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
                        </span>
						<i class="fa fa-angle-down"></i>
					</a> 
				</span>
			</div>
			<nav>
				<ul>
					<li class="">
	                <a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MENÚ PRINCIPAL"><i
	                        class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
	            	</li>
		            <li class="text-center">
		                <a href="#" title="REPORTES"> <span class="menu-item-parent">REPORTES</span></a>
		            </li>
					<?php
			                for($i=0;$i<count($enlaces);$i++)
			                {
			                    if(count($subenlaces[$enlaces[$i]['o_child']])>0)
			                    {
			            ?>
			            <li>
			              	<a href="#" >
			              		<i class="<?php echo $enlaces[$i]['o_image']?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
			              	<ul >
			              	<?php
			                foreach ($subenlaces[$enlaces[$i]['o_child']] as $item) {
			                ?>
			                <li><a href="<?php echo base_url($item['o_url']); ?>"><?php echo $item['o_titulo']; ?></a></li>
			                <?php } ?>
			                </ul>
			            </li>
			            <?php 
			            }
			        } ?>
				</ul>
			</nav>
			<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>

		</aside>

		<!-- MAIN PANEL -->
		<div id="main" role="main">

			<!-- RIBBON -->
			<div id="ribbon">

				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>

				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li>Reportes</li><li>Reportes Gerenciales</li><li>Resumen de Objetivos</li>
				</ol>
			</div>

			<!-- MAIN CONTENT -->
			<div id="content">
				<!-- widget grid -->
				<section id="widget-grid" class="">
					<div class="row">
						<div class="alert alert-block alert-success" >
							<a class="close" data-dismiss="alert" href="#">×</a>
							<h4 class="alert-heading"><i class="glyphicon glyphicon-user"></i><?php echo " FUNCIONARIO RESPONSABLE : ".$this->session->userdata("funcionario")."   ||  "; ?><i class="glyphicon glyphicon-calendar"></i><?php echo "FECHA ACTUAL : " . date("d") . " del " . date("m") . " de " . date("Y");?></h4>
						</div>
						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-9">
				            <section id="widget-grid" class="well">
				                <div align="center">
				                    <font size="2"><b> <?php echo $this->session->userdata("entidad");?> </b><br><small>RESUMEN DE OBJETIVOS DE CORTO PLAZO <br><?php echo $pr1.''.$pr2.''.$pr3?><br>AL <?php echo $dias.' DE '.$mes;?>  DE <?php echo $this->session->userdata("gestion");?></small></font>
				                </div>
				            </section>
				        </article>
				        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
			                <section id="widget-grid" class="well">
			                  <style type="text/css">#graf{font-size: 80px;}</style> 
			                  <center>
			                    <div class="dropdown">
			                    <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true" style="width:100%;">
			                      OPCIONES
			                      <span class="caret"></span>
			                    </button>
			                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
			                      <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:abreVentana('<?php echo site_url("").'/rep/rep_avance_objetivos/'.$p1.'/'.$p2.'/'.$p3.'/'.$tp.'' ?>');">IMPRIMIR AVANCE</a></li>
			                      <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url().'index.php/admin/rep/vpinge' ?>" >VOLVER ATRAS </a></li>
			                    </ul>
			                  </div>
			                  </center>
			                </section>
			            </article>

						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-8">
							<div class="jarviswidget" id="wid-id-2" data-widget-editbutton="false">
								<header>
									<span class="widget-icon"> <i class="fa fa-bar-chart-o"></i> </span>
									<h2>AVANCE FINANCIERO</h2> 
								</header>

								<div>
								<div class="jarviswidget-editbox"></div>
									<div class="widget-body no-padding">
										<div class="col-md-12 col-sm-12 col-xs-12"  id="container2" class="chart"></div>
									</div>
								</div>
							</div>
						</article>
						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
							<section id="widget-grid" class="well">
								<div class="table-responsive">
									<table class="table table-bordered" class="col-md-12 col-sm-12 col-xs-12" >
										<thead>
											<tr>
												<th></th>
												<th>EJECUCI&Oacute;N</th>
												<th>CANTIDAD</th>
												<th></th>
											</tr>
										</thead>
										<tbody>
											<tr class="riesgo_alto">
												<td >Baja Ejecuci&oacute;n</td>
												<td >0 - 25 %</td>
												<td ><?php echo $avance_financiero[1];?></td>
												<td><center><a href="<?php echo base_url().'index.php/admin/rep/ver_objet_grupo/1/'.$p1.'/'.$p2.'/'.$p3.'/'.$p4.'/'.$tp.''?>" class="btn btn-default" title="VER PROYECTOS/PROGRAMAS/OPERACIONES" style="width:100%;">VER</a></center></td>
											</tr>
											<tr class="riesgo">
												<td>En Riesgo</td>
												<td>26 - 50 %</td>
												<td><?php echo $avance_financiero[2];?></td>
												<td><center><a href="<?php echo base_url().'index.php/admin/rep/ver_objet_grupo/2/'.$p1.'/'.$p2.'/'.$p3.'/'.$p4.'/'.$tp.''?>" class="btn btn-default" title="VER PROYECTOS/PROGRAMAS/OPERACIONES" style="width:100%;">VER</a></center></td>
											</tr>
											<tr class="retraso">
												<td>Mediana Ejecuci&oacute;n</td>
												<td>51 - 75 %</td>
												<td><?php echo $avance_financiero[3];?></td>
												<td><center><a href="<?php echo base_url().'index.php/admin/rep/ver_objet_grupo/3/'.$p1.'/'.$p2.'/'.$p3.'/'.$p4.'/'.$tp.''?>" class="btn btn-default " title="VER PROYECTOS/PROGRAMAS/OPERACIONES" style="width:100%;">VER</a></center></td>
											</tr>
											<tr class="tiempo">
												<td>Ejecuci&oacute;n a Tiempo</td>
												<td>76 - 100 %</td>
												<td><?php echo $avance_financiero[4];?></td>
												<td><center><a href="<?php echo base_url().'index.php/admin/rep/ver_objet_grupo/4/'.$p1.'/'.$p2.'/'.$p3.'/'.$p4.'/'.$tp.''?>" class="btn btn-default" title="VER PROYECTOS/PROGRAMAS/OPERACIONES" style="width:100%;">VER</a></center></td>
											</tr>
											<tr>
												<td><b>TOTAL</b></td>
												<td></td>
												<td><?php echo $avance_financiero[6];?></td>
												<td></td>
											</tr>
										</tbody>
									</table>
								</div>
							</section>
				        </article>
					</div>
				</section>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN PANEL -->

		<!-- PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->
		<!--================================================== -->
		<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
		<script data-pace-options='{ "restartOnRequestAfter": true }' src="<?php echo base_url(); ?>assets/js/plugin/pace/pace.min.js"></script>
		<script>
			if (!window.jQuery) {
				document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-2.0.2.min.js"><\/script>');
			}
		</script>

		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url(); ?>assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>

		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url(); ?>assets/js/session_time/jquery-idletimer.js"></script>
		<script src = "<?php echo base_url(); ?>mis_js/control_session.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/app.config.js"></script>
		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.min.js"></script>
		<!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url(); ?>assets/js/notification/SmartNotification.min.js"></script>
		<!-- JARVIS WIDGETS -->
		<script src="<?php echo base_url(); ?>assets/js/smartwidgets/jarvis.widget.min.js"></script>
		<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<!-- SPARKLINES -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/select2/select2.min.js"></script>
		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
		<!-- browser msie issue fix -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url(); ?>assets/js/plugin/fastclick/fastclick.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
		<!-- Demo purpose only -->
		<script src="<?php echo base_url(); ?>assets/js/demo.min.js"></script>
		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url(); ?>assets/js/app.min.js"></script>
		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<!-- Voice command : plugin -->
		<script src="<?php echo base_url(); ?>assets/js/speech/voicecommand.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/highcharts/js/highcharts.js"></script>
		<script src="<?php echo base_url(); ?>assets/highcharts/js/highcharts-3d.js"></script>
		<script src="<?php echo base_url(); ?>assets/highcharts/js/modules/exporting.js"></script>
		<script type="text/javascript">
		var entidad='<?php echo $this->session->userdata("entidad");?>';
		var mes='<?php echo $mes;?>';
		var dias='<?php echo $dias;?>';
		var gestion='<?php echo $this->session->userdata("gestion");?>';
		Highcharts.chart('container2', {
			
		    chart: {
		        plotBackgroundColor: null,
		        plotBorderWidth: null,
		        plotShadow: false,
		        type: 'pie'
		    },
		    title: {
		        text: ''+entidad+'<br>RESUMEN DE OBJETIVOS DE CORTO PLAZO <br> AL '+dias+' DE '+mes+' DE '+gestion,
		    },
		    tooltip: {
		        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
		    },
		    plotOptions: {
		        pie: {
		            allowPointSelect: true,
		            cursor: 'pointer',
		            dataLabels: {
		                enabled: true,
		                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
		                style: {
		                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
		                }
		            }
		        }
		    },
		    series: [{
		        name: 'SIGRE V1.0',
		        colorByPoint: true,
		        data: [{
		            name: 'BAJA EJEC. (0-25 %)',
		            color: '#EF1919',
		            y: <?php echo $avance_financiero[1];?>
		        },{
		            name: 'EN RIESGO (26-50 %)',
		            color: '#F8912F',
		            y: <?php echo $avance_financiero[2];?>
		        },{
		            name: 'MEDIANA EJEC. (51-75 %)',
		            color: '#F5F50A',
		            y: <?php echo $avance_financiero[3];?>
		        }, {
		            name: 'EJEC. A TIEMPO (76-100 %)',
		            color: '#26F50A',
		            y: <?php echo $avance_financiero[4];?>,
		            sliced: true,
		            selected: true
		        }]
		    }]
		});
		</script>
		<!-- ====================================================================================================== -->

	</body>
</html>
