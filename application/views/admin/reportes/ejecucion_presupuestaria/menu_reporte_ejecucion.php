<!DOCTYPE html>
<html lang="en-us">
  <head>
    <meta charset="utf-8">
    <title><?php echo $this->session->userdata('name')?></title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- Basic Styles -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
    <!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
    <!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
    <!-- FAVICONS -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.core.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css" id="toggleCSS"/>
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
      <style type="text/css">
        .ruta {
            text-decoration: none;
            color-link: white;
        }

        .icono {
            transition: 0.3s;
            color: white;
        }

        i:hover {

            transform: scale(1.2);
            letter-spacing: 5px;
            color: whitesmoke;
        }

        .head_proy {
            background-color: #568A89;
            color: #ffffff;
            align-content: center;
            text-align: center;
            vertical-align: middle%;
        }

        td {
            color: black;
            font-weight: bold;
            text-align: right;
        }

        .descripcion {
            text-align: left;
        }
    </style>

  </head>
  <body class="">
    <!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->
    <header id="header">
      <div id="logo-group">
        <!-- <span id="logo"> <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="SmartAdmin"> </span> -->
      </div>
      <!-- pulled right: nav area -->
      <div class="pull-right">
        <!-- collapse menu button -->
        <div id="hide-menu" class="btn-header pull-right">
          <span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
        </div>
        <!-- end collapse menu -->
        <!-- logout button -->
        <div id="logout" class="btn-header transparent pull-right">
          <span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Sign Out" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
        </div>
        <!-- end logout button -->
        <!-- search mobile button (this is hidden till mobile view port) -->
        <div id="search-mobile" class="btn-header transparent pull-right">
          <span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
        </div>
        <!-- end search mobile button -->
        <!-- fullscreen button -->
        <div id="fullscreen" class="btn-header transparent pull-right">
          <span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen"><i class="fa fa-arrows-alt"></i></a> </span>
        </div>
        <!-- end fullscreen button -->
      </div>
      <!-- end pulled right: nav area -->
    </header>
    <!-- END HEADER -->

    <!-- Left panel : Navigation area -->
    <aside id="left-panel">
      <!-- User info -->
      <div class="login-info">
        <span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
          <a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
            <span>
                <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
            </span>
            <i class="fa fa-angle-down"></i>
          </a> 
        </span>
      </div>
      <nav>
        <ul>
          <li class="">
            <a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MENÚ PRINCIPAL">
            <i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
          </li>
          <li class="text-center">
            <a href='#' title="REGISTRO DE EJECUCION"> <span class="menu-item-parent">REPORTES</span></a>
          </li>
          <?php
            for($i=0;$i<count($enlaces);$i++)
            {
              if(count($subenlaces[$enlaces[$i]['o_child']])>0)
              {
                ?>
                <li>
                  <a href="#" >
                    <i class="<?php echo $enlaces[$i]['o_image']?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
                    <ul>
                    <?php
                    foreach ($subenlaces[$enlaces[$i]['o_child']] as $item) {
                    ?>
                    <li><a href="<?php echo base_url($item['o_url']); ?>"><?php echo $item['o_titulo']; ?></a></li>
                    <?php } ?>
                    </ul>
                </li>
                <?php 
              }
            } ?>
        </ul>
      </nav>
      <span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>
    </aside>

    <!-- MAIN PANEL -->
    <div id="main" role="main">
      <!-- RIBBON -->
      <div id="ribbon">
        <span class="ribbon-button-alignment"> 
          <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
            <i class="fa fa-refresh"></i>
          </span> 
        </span>
        <!-- breadcrumb -->
        <ol class="breadcrumb">
          <li>Reportes</li><li>Reportes Gerenciales</li><li>Resumen de Ejecuci&oacute;n Presupuestaria a nivel Institucional</li>
        </ol>
      </div>
      <!-- END RIBBON -->

      <!-- MAIN CONTENT -->
      <div id="content">
      <div class="well">
        <section id="widget-grid" class="">
          <!-- row -->
          <div class="row">
          <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <section id="widget-grid" class="well">
              <div align="center">
                <h1><b> <?php echo $this->session->userdata("entidad");?> </b><br><small>RESUMEN DE EJECUCI&Oacute;N PRESUPUESTARIA A NIVEL INSTITUCIONAL<br> AL <?php echo $dias.' de '.$mes;?>  DE <?php echo $this->session->userdata("gestion");?><br>(EXPRESADO EN BOLIVIANOS)</small></h1>
              </div>
            </section>
          </article>
         
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div class="jarviswidget jarviswidget-color-darken" >
                  <header>
                    <span class="widget-icon"> <i class="fa fa-arrows-v"></i> </span>
                    </header>
                <div>
                  <div class="widget-body no-padding">

                    <div class="row">
                        <div class="well well-sm col-sm-4 animated rotateInDownLeft">
                            <div class="well well-sm bg-color-teal txt-color-white text-center">
                                <a href="<?php echo site_url() . '/rep/ejec/tipo_gasto'?>" class="ruta">
                                  <h5 style="font-weight: bold;font-style: italic;color: white">EJECUCI&Oacute;N PRESUPUESTARIA GENERAL POR RUBRO DE GASTO</h5>
                                  <i class="fa fa-signal fa-5x icono" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                        <div class="well well-sm col-sm-4 animated rotateInDownRight">
                            <div class="well well-sm bg-color-teal txt-color-white text-center">
                                <a href="<?php echo site_url() . '/rep/ejec/unidad_ejec'?>" class="ruta">
                                  <h5 style="font-weight: bold;font-style: italic;color: white">EJECUCI&Oacute;N PRESUPUESTARIA GENERAL POR UNIDAD EJECUTORA</h5>
                                  <i class="fa fa-signal fa-5x icono" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                        <div class="well well-sm col-sm-4 animated bounceInLeft">
                            <div class="well well-sm bg-color-teal txt-color-white text-center">
                                <a href="<?php echo site_url() . '/rep/ejec/unidad_ejec_proy'?>" class="ruta">
                                  <h5 style="font-weight: bold;font-style: italic;color: white">EJECUCI&Oacute;N PRESUPUESTARIA GENERAL DE INVERSI&Oacute;N PUBLICA - SECTORES ECONOMICOS</h5>
                                  <i class="fa fa-signal fa-5x icono" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="well well-sm col-sm-4 animated bounceInRight">
                            <div class="well well-sm bg-color-teal txt-color-white text-center">
                                <a href="<?php echo site_url() . '/rep/ejec/unidad_ejec_prog'?>" class="ruta">
                                  <h5 style="font-weight: bold;font-style: italic;color: white">EJECUCI&Oacute;N PRESUPUESTARIA GENERAL DE PROGRAMAS - SECTORES ECONOMICOS</h5>
                                  <i class="fa fa-signal fa-5x icono" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                        <div class="well well-sm col-sm-4 animated rotateInUpRight">
                            <div class="well well-sm bg-color-teal txt-color-white text-center">
                                <a href="<?php echo site_url() . '/rep/ejec/gasto_corriente'?>" class="ruta">
                                  <h5 style="font-weight: bold;font-style: italic;color: white">EJECUCI&Oacute;N PRESUPUESTARIA DE GASTO CORRIENTE</h5>
                                  <i class="fa fa-signal fa-5x icono" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                        <div class="well well-sm col-sm-4 animated rotateInUpLeft">
                            <div class="well well-sm bg-color-teal txt-color-white text-center">
                                <a href="<?php echo site_url() . '/rep/ejec/transferencias'?>" class="ruta">
                                  <h5 style="font-weight: bold;font-style: italic;color: white">TRANSFERENCIA GOBIERNOS MUNICIPALES - PROGRAMAS CONCURRENTES</h5>
                                  <i class="fa fa-signal fa-5x icono" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="well well-sm col-sm-4 animated rotateInDownRight">
                            <div class="well well-sm bg-color-teal txt-color-white text-center">
                                <a href="<?php echo site_url() . '/rep/ejec/presupuestaria'?>" class="ruta">
                                  <h5 style="font-weight: bold;font-style: italic;color: white">EJECUCI&Oacute;N DEL PRESUPUESTO</h5>
                                  <i class="fa fa-signal fa-5x icono" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                        <div class="well well-sm col-sm-4 animated bounceInRight">
                           
                        </div>
                        <div class="well well-sm col-sm-4 animated bounceInRight">
                           
                        </div>
                    </div>

                    </div>
                    <!-- end widget content -->
                  </div>
                  <!-- end widget div -->
                </div>
                <!-- end widget -->
              </article>

          </div>
          <!-- end row -->
        </section>
        </div>
      </div>
      <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN PANEL -->

    <!-- PAGE FOOTER -->
    <div class="page-footer"> 
      <div class="row">
        <div class="col-xs-12 col-sm-6">
          <span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
        </div>
      </div>
    </div>
    <!-- END PAGE FOOTER -->
    <!--================================================== -->
    <script>
      if (!window.jQuery) {
        document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-2.0.2.min.js"><\/script>');
      }
    </script>
    <script>
      if (!window.jQuery.ui) {
        document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
      }
    </script>
    <!-- IMPORTANT: APP CONFIG -->
    <script src="<?php echo base_url();?>/assets/js/app.config.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/mis_js/validacion_form.js"></script>
    <script src = "<?php echo base_url(); ?>mis_js/control_session.js"></script>
    <!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
    <script src="<?php echo base_url();?>/assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
    <!-- BOOTSTRAP JS -->
    <script src="<?php echo base_url();?>/assets/js/bootstrap/bootstrap.min.js"></script>
    <!-- CUSTOM NOTIFICATION -->
    <script src="<?php echo base_url();?>/assets/js/notification/SmartNotification.min.js"></script>
    <!-- JARVIS WIDGETS -->
    <script src="<?php echo base_url();?>/assets/js/smartwidgets/jarvis.widget.min.js"></script>
    <!-- EASY PIE CHARTS -->
    <script src="<?php echo base_url();?>/assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
    <!-- SPARKLINES -->
    <script src="<?php echo base_url();?>/assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
    <!-- JQUERY VALIDATE -->
    <script src="<?php echo base_url();?>/assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
    <!-- JQUERY MASKED INPUT -->
    <script src="<?php echo base_url();?>/assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
    <!-- JQUERY SELECT2 INPUT -->
    <script src="<?php echo base_url();?>/assets/js/plugin/select2/select2.min.js"></script>
    <!-- JQUERY UI + Bootstrap Slider -->
    <script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
    <!-- browser msie issue fix -->
    <script src="<?php echo base_url();?>/assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
    <!-- FastClick: For mobile devices -->
    <script src="<?php echo base_url();?>/assets/js/plugin/fastclick/fastclick.min.js"></script>
    <!-- Demo purpose only -->
    <script src="<?php echo base_url();?>/assets/js/demo.min.js"></script>
    <!-- MAIN APP JS FILE -->
    <script src="<?php echo base_url();?>/assets/js/app.min.js"></script>
    <!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
    <!-- Voice command : plugin -->
    <script src="<?php echo base_url();?>/assets/js/speech/voicecommand.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
    <!-- PAGE RELATED PLUGIN(S) -->
    <script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
  </body>
</html>
