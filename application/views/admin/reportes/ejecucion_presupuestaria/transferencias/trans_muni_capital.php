<!DOCTYPE html>
<html lang="en-us">
  <head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    <title><?php echo $this->session->userdata('name')?></title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- Basic Styles -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
    <!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-production.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/smartadmin-skins.min.css">
    <!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/demo.min.css">
    <!-- FAVICONS -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.core.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/themes_alerta/alertify.default.css" id="toggleCSS"/>
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/css/estilosh.css">
    <script>
        function abreVentana(PDF)
      {
        var direccion;
        direccion = '' + PDF;
        window.open(direccion, "Reporte de Proyectos" , "width=800,height=650,scrollbars=SI") ;
      }                                                  
    </script>
  </head>
  <body class="">
    <!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->
    <!-- HEADER -->
    <header id="header">
      <div id="logo-group">
        <!-- <span id="logo"> <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="SmartAdmin"> </span> -->
      </div>

      <!-- pulled right: nav area -->
      <div class="pull-right">
        <!-- collapse menu button -->
        <div id="hide-menu" class="btn-header pull-right">
          <span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
        </div>
        <!-- end collapse menu -->
        <!-- logout button -->
        <div id="logout" class="btn-header transparent pull-right">
          <span> <a href="<?php echo base_url(); ?>index.php/admin/logout" title="Sign Out" data-action="userLogout" data-logout-msg="Estas seguro de salir del sistema"><i class="fa fa-sign-out"></i></a> </span>
        </div>
        <!-- end logout button -->
        <!-- search mobile button (this is hidden till mobile view port) -->
        <div id="search-mobile" class="btn-header transparent pull-right">
          <span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
        </div>
        <!-- end search mobile button -->
        <!-- fullscreen button -->
        <div id="fullscreen" class="btn-header transparent pull-right">
          <span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen"><i class="fa fa-arrows-alt"></i></a> </span>
        </div>
        <!-- end fullscreen button -->
      </div>
      <!-- end pulled right: nav area -->
    </header>
    <!-- END HEADER -->

    <!-- Left panel : Navigation area -->
    <aside id="left-panel">
      <!-- User info -->
      <div class="login-info">
        <span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
          
          <a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
            <span>
                <i class="fa fa-user" aria-hidden="true"></i>  <?php echo $this->session->userdata("user_name");?>
            </span>
            <i class="fa fa-angle-down"></i>
          </a> 
          
        </span>
      </div>
      <nav>
        <ul>
          <li class="">
            <a href="<?php echo site_url("admin") . '/dashboard'; ?>" title="MENÚ PRINCIPAL">
            <i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">MEN&Uacute; PRINCIPAL</span></a>
          </li>
          <li class="text-center">
            <a href="<?php echo site_url() . '/rep/ejec/rep_ejecucion_presupuestaria'?>" title="REPORTES"> <span class="menu-item-parent">REPORTES</span></a>
          </li>
          <?php
            for($i=0;$i<count($enlaces);$i++)
            {
              if(count($subenlaces[$enlaces[$i]['o_child']])>0)
              {
                ?>
                <li>
                  <a href="#" >
                    <i class="<?php echo $enlaces[$i]['o_image']?>"></i> <span class="menu-item-parent"><?php echo $enlaces[$i]['o_titulo']; ?></span></a>
                    <ul>
                    <?php
                    foreach ($subenlaces[$enlaces[$i]['o_child']] as $item) {
                    ?>
                    <li><a href="<?php echo base_url($item['o_url']); ?>"><?php echo $item['o_titulo']; ?></a></li>
                    <?php } ?>
                    </ul>
                </li>
                <?php 
              }
            } ?>
        </ul>
      </nav>
      <span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>
    </aside>

    <!-- MAIN PANEL -->
    <div id="main" role="main">
      <!-- RIBBON -->
      <div id="ribbon">
        <span class="ribbon-button-alignment"> 
          <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
            <i class="fa fa-refresh"></i>
          </span> 
        </span>
        <!-- breadcrumb -->
        <ol class="breadcrumb">
          <li>Reportes</li><li>Reportes Gerenciales</li><li>Resumen de Ejecuci&oacute;n Presupuestaria a nivel Institucional</li><li>Transferencias a Gobiernos Municipales Proyectos Concurrentes</li>
        </ol>
      </div>
      <!-- END RIBBON -->

      <!-- MAIN CONTENT -->
      <div id="content">
      <div class="well">
        <section id="widget-grid" class="">
          <!-- row -->
          <div class="row">
          <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <nav role="navigation" class="navbar navbar-default navbar-inverse">
                  <div class="navbar-header">
                      <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                          <span class="sr-only">Toggle navigation</span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                      </button>
                  </div>
           
                  <div id="navbarCollapse" class="collapse navbar-collapse">
                      <ul class="nav navbar-nav">
                          <li class="active"><a href="#" title="TRANSFERENCIAS DE CAPITALES A GOBIERNOS MUNICIPALES"><i class="glyphicon glyphicon-ok"></i><font size="1"> TRANSFERENCIAS DE CAPITALES A GOBIERNOS MUNICIPALES</font></a></li>
                          <li><a href="<?php echo base_url().'index.php/rep/ejec/transferencias_corriente' ?>" title="TRANSFERENCIAS CORRIENTES A GOBIERNOS MUNICIPALES"><font size="1"> TRANSFERENCIAS CORRIENTES A GOBIERNOS MUNICIPALES</font></a></li>
                      </ul>
                  </div>
              </nav>
          </article>
          </div>
          <div class="row">
          <article class="col-xs-12 col-sm-12 col-md-12 col-lg-9">
                <section id="widget-grid" class="well">
                <div align="center">
                  <font size="2">TRANSFERENCIAS DE CAPITAL A GOBIERNOS MUNICIPALES<br>PROYECTOS CONCURRENTES EN EL DEPARTAMENTO DE TARIJA (Al de <?php echo $dias.' de '.$mes.' de '.$this->session->userdata("gestion");?>) (Expresado en Bolivianos) </font>
                </div>
            </section>
            </article>
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
                <section id="widget-grid" class="well">
                    <div align="center">
                      <a href="javascript:abreVentana('<?php echo site_url("").'/rep/ejec/rep_transferencias/8' ?>');" class="btn btn-success" title="REPORTE EJECUCI&Oacute;N " style="width:100%;">IMPRIMIR EJECUCI&Oacute;N</a>
                    </div>
                </section>
            </article>

           <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <!-- Widget ID (each widget will need unique ID)-->
              <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false">
                <header>
                  <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                  <h2></h2>
                </header>
                <!-- widget div-->
                <div>
                  <!-- widget edit box -->
                  <div class="jarviswidget-editbox">
                    <!-- This area used as dropdown edit box -->
                  </div>
                  <!-- end widget edit box -->
                  <!-- widget content -->
                  <div class="widget-body">
                    <p></p>
                    <div class="table-responsive">
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <tr>
                            <th>MUNICIPIO</th>
                            <th>PRESUPUESTO INICIAL</th>
                            <th>MODIFICACIONES</th>
                            <th>PRESUPUESTO VIGENTE</th>
                            <th>TRANSFERENCIAS</th>
                            <th>% EJECUCI&Oacute;N</th>
                            <th>% PART</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php echo $tabla;?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <!-- end widget content -->
                </div>
                <!-- end widget div -->
              </div>
              <!-- end widget -->
              <!-- Widget ID (each widget will need unique ID)-->
              <div class="jarviswidget jarviswidget-color-darken" id="wid-id-1" data-widget-editbutton="false">
                <header>
                  <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                  <h2>GRAFICO N° 7</h2>
                </header>
                <!-- widget div-->
                <div>
                  <!-- widget edit box -->
                  <div class="jarviswidget-editbox">
                    <!-- This area used as dropdown edit box -->
                  </div>
                  <!-- end widget edit box -->
                  <!-- widget content -->
                  <div class="widget-body no-padding">
                    <div id="container" style="width: 800px; height: 400px; margin: 0 auto"></div> 
                  </div>
                  <!-- end widget content -->
                </div>
                <!-- end widget div -->
              </div>

              <!-- end widget -->
            </article>
          </div>
          <!-- end row -->
        </section>
        </div>
      </div>
      <!-- END MAIN CONTENT -->
    </div>
    <!-- END MAIN PANEL -->

    <!-- PAGE FOOTER -->
    <div class="page-footer"> 
      <div class="row">
        <div class="col-xs-12 col-sm-6">
          <span class="txt-color-white"><?php echo $this->session->userData('name').' @ '.$this->session->userData('gestion') ?></span>
        </div>
      </div>
    </div>
    <!-- END PAGE FOOTER -->

    <!--================================================== -->
    <script>
      if (!window.jQuery) {
        document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-2.0.2.min.js"><\/script>');
      }
    </script>

    <script>
      if (!window.jQuery.ui) {
        document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
      }
    </script>
    <!-- IMPORTANT: APP CONFIG -->
    <script src="<?php echo base_url();?>/assets/js/app.config.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/mis_js/validacion_form.js"></script>
    <!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
    <script src="<?php echo base_url();?>/assets/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 
    <!-- BOOTSTRAP JS -->
    <script src="<?php echo base_url();?>/assets/js/bootstrap/bootstrap.min.js"></script>
    <!-- CUSTOM NOTIFICATION -->
    <script src="<?php echo base_url();?>/assets/js/notification/SmartNotification.min.js"></script>
    <!-- JARVIS WIDGETS -->
    <script src="<?php echo base_url();?>/assets/js/smartwidgets/jarvis.widget.min.js"></script>
    <!-- EASY PIE CHARTS -->
    <script src="<?php echo base_url();?>/assets/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
    <!-- SPARKLINES -->
    <script src="<?php echo base_url();?>/assets/js/plugin/sparkline/jquery.sparkline.min.js"></script>
    <!-- JQUERY VALIDATE -->
    <script src="<?php echo base_url();?>/assets/js/plugin/jquery-validate/jquery.validate.min.js"></script>
    <!-- JQUERY MASKED INPUT -->
    <script src="<?php echo base_url();?>/assets/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
    <!-- JQUERY SELECT2 INPUT -->
    <script src="<?php echo base_url();?>/assets/js/plugin/select2/select2.min.js"></script>
    <!-- JQUERY UI + Bootstrap Slider -->
    <script src="<?php echo base_url();?>/assets/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
    <!-- browser msie issue fix -->
    <script src="<?php echo base_url();?>/assets/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
    <!-- FastClick: For mobile devices -->
    <script src="<?php echo base_url();?>/assets/js/plugin/fastclick/fastclick.min.js"></script>
    <!-- Demo purpose only -->
    <script src="<?php echo base_url();?>/assets/js/demo.min.js"></script>
    <!-- MAIN APP JS FILE -->
    <script src="<?php echo base_url();?>/assets/js/app.min.js"></script>
    <!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
    <!-- Voice command : plugin -->
    <script src="<?php echo base_url();?>/assets/js/speech/voicecommand.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/lib_alerta/alertify.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/highcharts/js/highcharts.js"></script>
    <script src="<?php echo base_url(); ?>assets/highcharts/js/highcharts-3d.js"></script>
    <script src="<?php echo base_url(); ?>assets/highcharts/js/modules/exporting.js"></script> 
    <!-- PAGE RELATED PLUGIN(S) -->
    <script type="text/javascript">
      var chart;
      $(document).ready(function() {
        
        var colors = Highcharts.getOptions().colors,
          categories = [

            'PRESUPUESTO VIGENTE', 'TRANSFERENCIAS'
            ],
          name = 'Nivel de Ejecución',
          data = [{ 
              y: <?php echo $muni[$nro][5];?>,
              color: '#04B404',
            }, {
              y: <?php echo $muni[$nro][6];?>,
              color: '#04B404',
            }];
        
        function setChart(name, categories, data, color) {
          chart.xAxis[0].setCategories(categories);
          chart.series[0].remove();
          chart.addSeries({
            name: name,
            data: data,
            color: color || 'white'
          });
        }
        
        chart = new Highcharts.Chart({
          chart: {
            renderTo: 'container', 
            type: 'column'
          },
          title: {
            text: 'NIVEL DE EJECUCI\u00D3N POR TIPO DE GASTO'
          },
          subtitle: {
            text: '(Al de <?php echo $dias.' de '.$mes.' de '.$this->session->userdata("gestion");?>) (Expresado en % de Participacion)'
          },
          xAxis: {
            categories: categories              
          },
          yAxis: {
            title: {
              text: ''
            }
          },
          plotOptions: {
            column: {
              cursor: 'pointer',
              point: {
                events: {
                  click: function() {
                    var drilldown = this.drilldown;
                    if (drilldown) { // drill down
                      setChart(drilldown.name, drilldown.categories, drilldown.data, drilldown.color);
                    } else { // restore
                      setChart(name, categories, data);
                    }
                  }
                }
              },
              dataLabels: {
                enabled: true,
                color: colors[0],
                style: {
                  fontWeight: 'bold'
                },
                formatter: function() {
                  return this.y +'%';
                }
              }         
            }
          },
          tooltip: {
            formatter: function() {
              var point = this.point,
                s = this.x +':<b>'+ this.y +'% </b><br/>';
              if (point.drilldown) {
                s += ''+ point.category +' ';
              } else {
                s += '';
              }
              return s;
            }
          },
          series: [{
            name: name,
            data: data,
            color: 'white'
          }],
          exporting: {
            enabled: false
          }
        });
      });
    </script>
  </body>
</html>
