<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<link rel="STYLESHEET" href="<?php echo base_url(); ?>assets/print_static.css" type="text/css" />
  <style>
        table{
            font-size: 9px;
            width: 100%;
            background-color:#fff;
        }
  </style>
  <script type="text/javascript">
      function imprimir() {
          if (window.print) {
              window.print();
          } else {
              alert("La función de impresion no esta soportada por su navegador.");
          }
      }
  </script>
</head>
<body onload="imprimir();">

<div id="body">
<div id="section_header">
</div>
  
    <div class="page" style="font-size: 7pt" align="center">
      <table style="width: 90%;" class="header">
          <tr>
              <td width=20%; align="center">
                <img src="<?php echo base_url(); ?>assets/img/logo.jpg" class="img-responsive" alt="Cinque Terre" width="150px"/>
              </td>
              <td width=60%; class="titulo_pdf">
                  <b>ENTIDAD : </b><?php echo $this->session->userdata('entidad');?><br>
                  <b>POA - PLAN OPERATIVO ANUAL : </b><?php echo $this->session->userdata("gestion")?><br>
                  <b><?php echo $this->session->userdata('sistema');?></b><br>
                  <b>REPORTE : </b> EJECUCI&Oacute;N PRESUPUESTARIA DE INVERSI&Oacute;N P&Uacute;BLICA-SECTORES ECONOMICOS<br>
              </td>
              <td width=20%; align="center">
                <img src="<?php echo base_url(); ?>assets/img/escudo.jpg" class="img-responsive" alt="Cinque Terre" alt="" width="90px" >
              </td>
          </tr>
      </table>

            <article class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                <section id="widget-grid" class="well">
                    <div class="">
                      <h1>RESUMEN DE EJECUCI&Oacute;N PRESPUESTARIA DE INVERSI&Oacute;N P&Uacute;BLICA POR UNIDAD EJECUTORA (Al de <?php echo $dias.' de '.$mes.' de '.$this->session->userdata("gestion");?>) (Expresado en Bolivianos)</h1>
                    </div>
                </section>
            </article>
            
              <table class="change_order_items" style="width: 80%;" border="1">
                <thead>
                    <tr class="even_row" bgcolor="#e5e8ec">
                    <th>UNIDAD</th>
                    <th>PRESUPUESTO INICIAL</th>
                    <th>MODIFICACIONES</th>
                    <th>PRESUPUESTO VIGENTE</th>
                    <th>EJECUCI&Oacute;N</th>
                    <th>%EJEC.</th>
                  </tr>
                </thead>
                <tbody>
                  <?php echo $tabla;?>
                </tbody>
              </table>
              <table style="width: 80%;" class="change_order_items" border="1">
                <tr class="even_row" bgcolor="#e5e8ec">
                  <th>
                    GR&Aacute;FICO
                  </th>
                </tr>
                <tr>
                  <td>
                    <div id="container" style="width: 600px; height: 350px; margin: 1 auto"></div> 
                  </td>
                </tr>
              </table>
              
      <div align="center">
        <table style="width: 80%;">
            <tr>    
              <td colspan="3" height="80"><b>FIRMAS</b></td>
            </tr>
            <tr>    
              <td style="width: 33.3%;">RESPONSABLE UNIDAD EJECUTORA</td>
              <td style="width: 33.3%;">ANALISTA POA</td>
              <td style="width: 33.3%;">ANALISTA FINANCIERO</td>
            </tr>
        </table>
      </div>

    </div>
  </div>
</div>
</body>
 <script>
    if (!window.jQuery) {
      document.write('<script src="<?php echo base_url();?>/assets/js/libs/jquery-2.0.2.min.js"><\/script>');
    }
    </script>

    <script src="<?php echo base_url(); ?>assets/highcharts/js/highcharts.js"></script>
      <script type="text/javascript">
      var chart;

      $(document).ready(function() {
        chart = new Highcharts.Chart({
          chart: {
            renderTo: 'container',
            defaultSeriesType: 'column'
          },
          title: {
            text: 'RELACI\u00D3N DEL PRESUPUESTO VIGENTE Vs DE EJECUCI\u00D3N DE INVERSI\u00D3N PUBLICA'
          },
          subtitle: {
            text: '(Al de <?php echo $dias.' de '.$mes.' de '.$this->session->userdata("gestion");?>) (Expresado en MM de Bolivianos)'
          },
          xAxis: {
            categories: [
              '<?php echo $mue[1][2];?>', 
              '<?php echo $mue[2][2];?>', 
              '<?php echo $mue[3][2];?>'
            ]
          },
          yAxis: {
            min: 0,
            title: {
              text: 'Rainfall (mm)'
            }
          },
          legend: {
            layout: 'vertical',
            backgroundColor: '#FFFFFF',
            align: 'left',
            verticalAlign: 'top',
            x: 100,
            y: 70,
            floating: true,
            shadow: true
          },
          tooltip: {
            formatter: function() {
              return ''+
                this.x +': '+ this.y +' mm';
            }
          },
          plotOptions: {
            column: {
              pointPadding: 0.2,
              borderWidth: 0
            }
          },
          series: [{
            name: 'PRESUPUESTO VIGENTE',color: '#FE2E2E',
            data: [{y: <?php echo $mue[1][5]?>, color: '#FE2E2E'},{y: <?php echo $mue[2][5]?>, color: '#FE2E2E'},{y: <?php echo $mue[3][5]?>, color: '#FE2E2E'}]
        
          }, {
            name: 'PRESUPUESTO EJECUTADO',color: '#04B404',
            data: [{y: <?php echo $mue[1][6]?>, color: '#04B404'},{y: <?php echo $mue[2][6]?>, color: '#04B404'},{y: <?php echo $mue[3][6]?>, color: '#04B404'}]
        
          }]
        });
      });
    </script>
</html>