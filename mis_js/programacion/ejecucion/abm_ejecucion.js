    function valida_envia_ejecucion()
    {
        var OK = confirm('GUARDAR EJECUCIÓN PRESUPUESTARIA ');
        if (OK) {
            document.uni_form.submit(); 
            document.getElementById("btsubmit").value = "GUARDANDO EJECUCIÓN ...";
            document.getElementById("btsubmit").disabled = true;
            return true;
        }
    }
    $("#est").change(function () {
        $("#est option:selected").each(function () {
            elegido = $(this).val();

            if(elegido == 2 ) /// EJECUCION
            {
                $('#contrato').slideUp();
                $('#ejec').slideUp();
                $('#observaciones').slideDown();
                $('#cerrado').slideUp();
                $("#dep").change(function () {
                    $("#dep option:selected").each(function () {
                    elegido2 = $(this).val();

                    if(elegido2 == 1 ){
                        $('#ejec').slideDown();
                    }
                    else
                    {
                        $('#ejec').slideUp();
                    }
                    });
                });
            }

            if(elegido == 1 ) //PROCESO DE INICIO
            {
                $('#contrato').slideUp();
                $('#ejec').slideUp();
                $('#observaciones').slideDown();
                $('#cerrado').slideUp();
                $("#dep").change(function () {
                    $("#dep option:selected").each(function () {
                    elegido2 = $(this).val();

                    if(elegido2 == 8 ){
                        $('#contrato').slideDown();
                    }
                    else
                    {
                        $('#contrato').slideUp();
                    }
                    });
                });
            }
            if(elegido == 3 ) // EN PROCESO DE CONCLUSION
            {
                $('#contrato').slideUp();
                $('#ejec').slideUp();
                $('#observaciones').slideDown();
                $('#cerrado').slideUp();
                $("#dep").change(function () {
                    $("#dep option:selected").each(function () {
                    elegido2 = $(this).val();

                    if(elegido2 == 2 ){
                        $('#ejec').slideDown();
                    }
                    else
                    {
                        $('#ejec').slideUp();
                    }
                    });
                });
            }
            if(elegido == 4 ) //// Conclusiones
            {
                $('#contrato').slideUp();
                $('#ejec').slideUp();
                $("#dep").change(function () {
                    $("#dep option:selected").each(function () {
                    elegido2 = $(this).val();

                    if(elegido2 == 13 ){
                        $('#cerrado').slideDown();
                        $('#observaciones').slideUp();
                    }
                    else
                    {
                        $('#cerrado').slideUp();
                    }
                    });
                });
                
            }
           
        });
    });


    $("#tipo_i").change(function () {
        $("#tipo_i option:selected").each(function () {
            elegido = $(this).val();
            if(elegido == 2 ){
                //ABSOLUTO
                $('#rel').slideDown();
                $('#rel2').slideDown();
                $('#titulo_indicador').html(' RELATIVO ');
                $('#m1').html(' %');
                $('#m2').html(' %');
                $('#m3').html(' %');
                $('#m4').html(' %');
                $('#m5').html(' %');
                $('#m6').html(' %');
                $('#m7').html(' %');
                $('#m8').html(' %');
                $('#m9').html(' %');
                $('#m10').html(' %');
                $('#m11').html(' %');
                $('#m12').html(' %');
            }
            else
            {
                $('#rel').slideUp();
                $('#rel2').slideUp();
                $('#titulo_indicador').html(' ABSOLUTO ');
                $('#m1').html('');
                $('#m2').html('');
                $('#m3').html('');
                $('#m4').html('');
                $('#m5').html('');
                $('#m6').html('');
                $('#m7').html('');
                $('#m8').html('');
                $('#m9').html('');
                $('#m10').html('');
                $('#m11').html('');
                $('#m12').html('');
            }
        });
    });

/*======================================= PROGRAMACION ===========================*/
$("#tp_id").change(function () {
        $("#tp_id option:selected").each(function () {
            elegido = $(this).val();
            if(elegido == 1 ){
                //ABSOLUTO
                $('#pi').slideDown();
                $('#pr').slideUp();
                $('#pi1').slideDown();
                $('#pr1').slideUp();
                $('#tg').slideDown();
                $('#titulo_comp').html(' COMPONENTES ');
            }
            else
            {
                $('#pr').slideDown();
                $('#pi').slideUp();
                $('#pr1').slideDown();
                $('#pi1').slideUp();
                $('#tg').slideDown();
                $('#titulo_comp').html(' PROCESOS ');
            }
        });
    });

$("#tp_id2").change(function () {
        $("#tp_id2 option:selected").each(function () {
            elegido = $(this).val();
            if(elegido == 1 ){
                //ABSOLUTO
                $('#pi2').slideDown();
                $('#pr2').slideUp();
                $('#titulo_comp').html(' COMPONENTES ');
            }
            else
            {
                $('#pr2').slideDown();
                $('#pi2').slideUp();
                $('#titulo_comp').html(' PROCESOS ');
            }
        });
    });
