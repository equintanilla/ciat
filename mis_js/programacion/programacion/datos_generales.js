/*------------------------------------- SUBIR ARCHIVOS - DATOS GENERALES -------------------------------------*/
function comprueba_extension(formulario, archivo,doc) {
      extensiones_permitidas = new Array(".gif", ".jpg", ".docx",".doc",".DOC",".PNG",".xlsx",".xls",".pdf",".png",".JPEG");
      mierror = "";
      if (!archivo) {
        //Si no tengo archivo, es que no se ha seleccionado un archivo en el formulario
        mierror = "POR FAVOR SELECCIONE UN ARCHIVO";
      }else{
        //recupero la extensiÃ³n de este nombre de archivo
        extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();

        if(extension=='.gif' || extension=='.jpg' || extension=='.png' ||extension=='.PNG' || extension=='.JPEG'){
          ext=1;
        }
        if(extension=='.pdf' || extension=='.PDF'){
          ext=2;
        }
        if(extension=='.docx' || extension=='.doc' || extension=='.DOC' || extension=='.DOCX'){
          ext=3;
        }
        if(extension=='.xlsx' || extension=='.xls'){
          ext=4;
        }
        
        //compruebo si la extensiÃ³n estÃ¡ entre las permitidas
        permitida = false;
        for (var i = 0; i < extensiones_permitidas.length; i++) {
          if (extensiones_permitidas[i] == extension) { 
            permitida = true;
            break;
          }
        }
        if (!permitida) {
          mierror = "<h4><font color=red>COMPRUEBE LA EXTENSIÓN DE LOS ARCHIVOS, SOLO SE PUEDEN SUBIR ARCHIVOS CON LAS SIGUIENTES EXTENSIONES : </font></h4>" + extensiones_permitidas.join();
        }else{
          document.getElementById('tp_doc').value=ext;
          if(doc.length==0)
          {
            alertify.alert('INGRESE NOMBRE,DESCRIPCIÓN DEL ARCHIVO ');
            return false;
          }

          alertify.confirm("GUARDAR ARCHIVO ?", function (a) {
                      if (a) {
                          //============= GUARDAR DESPUES DE LA VALIDACION ===============
                          formulario.submit();
                          document.getElementById("btsubmit").value = "SUBIENDO ARCHIVO...";
                          document.getElementById("btsubmit").disabled = true;
                          return true; 
                      } else {
                          alertify.error("OPCI\u00D3N CANCELADA");
                      }
                  });
          return 1;
        }
      }
      //si estoy aqui es que no se ha podido submitir
      alertify.alert(mierror);
      return false;
    }
/*----------------------------------------------------------------------------------------------------------------*/
            function valida_envia()
            { 
                if (parseInt(document.formulario.gest.value)>="1") /////// 1 GESTION
                  { 
                      /*-------------------- verificando gestiones 1-----------------*/
                    if (document.formulario.pt1.value=="") /////// gestion 1
                    { 
                        alertify.alert("INGRESE TOTAL PRESUPUESTO "+document.formulario.gs1.value+"");
                        return false;
                    }
                    if (document.formulario.pte1.value=="") /////// gestion 1
                    { 
                        alertify.alert("INGRESE PRESUPUESTO EJECUTADO "+document.formulario.gs1.value+"");
                        return false;
                    }
                    /*-------------------- end verificando gestiones 1-----------------*/
                    /*-------------------- verificando valores 1-----------------*/
                    if(parseFloat(document.formulario.pt1.value)>parseFloat(document.formulario.ppt.value))
                    {
                        alertify.alert("ERROR !! PRESUPUESTO "+document.formulario.gs1.value+"");
                        return false; 
                    }
                    if(parseFloat(document.formulario.pte1.value)>parseFloat(document.formulario.pe.value))
                    {
                        alertify.alert("ERROR !! PRESUPUESTO EJECUTADO "+document.formulario.gs1.value+"");
                        return false;
                    }
                    if(parseFloat(sin_puntos(document.formulario.pte1.value))>parseFloat(sin_puntos(document.formulario.pt1.value)))
                    {
                        alertify.alert("ERROR !! VALOR EJECUTADO NO PUEDE SER MAYOR AL PRESUPUESTO "+document.formulario.gs1.value+"");
                        return false; 
                    }
                    /*-------------------- end verificando valores 1-----------------*/

                    if (parseInt(document.formulario.gest.value)>="2") /////// 2 GESTION
                    {
                      /*-------------------- verificando gestiones 2-----------------*/
                      if (document.formulario.pt2.value=="") /////// gestion 1
                      { 
                          alertify.alert("INGRESE TOTAL PRESUPUESTO "+document.formulario.gs2.value+"");
                          return false;
                      }
                      if (document.formulario.pte2.value=="") /////// gestion 1
                      { 
                          alertify.alert("INGRESE PRESUPUESTO EJECUTADO "+document.formulario.gs2.value+"");
                          return false;
                      }
                      /*-------------------- end verificando gestiones 2-----------------*/
                      /*-------------------- verificando valores 2-----------------*/
                      if(parseFloat(document.formulario.pt1.value)+parseFloat(document.formulario.pt2.value)>parseFloat(document.formulario.ppt.value))
                      {
                          alertify.alert("ERROR !! PRESUPUESTO "+document.formulario.gs2.value+"");
                          return false; 
                      }
                      if(parseFloat(document.formulario.pte1.value)+parseFloat(document.formulario.pte2.value)>parseFloat(document.formulario.pe.value))
                      {
                          alertify.alert("ERROR !! PRESUPUESTO EJECUTADO "+(fecha+1)+"");
                          return false; 
                      }
                      if(parseFloat(sin_puntos(document.formulario.pte2.value))>parseFloat(sin_puntos(document.formulario.pt2.value)))
                      {
                          alertify.alert("ERROR !! VALOR EJECUTADO NO PUEDE SER MAYOR AL PRESUPUESTO "+document.formulario.gs2.value+"");
                          return false;  
                      }
                      /*-------------------- end verificando valores 2-----------------*/
                        
                        if (parseInt(document.formulario.gest.value)>="3") /////// 3 GESTION
                        {
                          /*-------------------- verificando gestiones 3-----------------*/
                          if (document.formulario.pt3.value=="") /////// gestion 1
                          { 
                              alertify.alert("INGRESE TOTAL PRESUPUESTO "+document.formulario.gs3.value+"");
                              return false; 
                          }
                          if (document.formulario.pte3.value=="") /////// gestion 1
                          { 
                              alertify.alert("INGRESE PRESUPUESTO EJECUTADO "+document.formulario.gs3.value+"");
                              return false; 
                          }
                          /*-------------------- end verificando gestiones 3-----------------*/
                          /*-------------------- verificando valores 3-----------------*/
                          if(parseFloat(document.formulario.pt1.value)+parseFloat(document.formulario.pt2.value)+parseFloat(document.formulario.pt3.value)>parseFloat(document.formulario.ppt.value)+'00.1')
                          {
                              alertify.alert("ERROR !! PRESUPUESTO "+document.formulario.gs3.value+"");
                              return false; 
                          }
                          if(parseFloat(document.formulario.pte1.value)+parseFloat(document.formulario.pte2.value)+parseFloat(document.formulario.pte3.value)>parseFloat(document.formulario.pe.value)+'00.1')
                          {
                              alertify.alert("ERROR !! PRESUPUESTO EJECUTADO "+document.formulario.gs3.value+"");
                              return false;  
                          }
                          if(parseFloat(sin_puntos(document.formulario.pte3.value))>parseFloat(sin_puntos(document.formulario.pt3.value)))
                          {
                              alertify.alert("ERROR !! VALOR EJECUTADO NO PUEDE SER MAYOR AL PRESUPUESTO "+document.formulario.gs3.value+"");
                              return false; 
                          }
                          /*-------------------- end verificando valores 3-----------------*/

                            if (parseInt(document.formulario.gest.value)>="4") /////// 4 GESTION
                            {
                              /*-------------------- verificando gestiones 4-----------------*/
                              if (document.formulario.pt4.value=="") /////// gestion 1
                              { 
                                  alertify.alert("INGRESE TOTAL PRESUPUESTO "+document.formulario.gs4.value+"");
                                  return false;  
                              }
                              if (document.formulario.pte4.value=="") /////// gestion 1
                              { 
                                  alertify.alert("INGRESE PRESUPUESTO EJECUTADO "+document.formulario.gs4.value+"");
                                  return false; 
                              }
                              /*--------------------end verificando gestiones 4-----------------*/
                              /*-------------------- verificando valores 4-----------------*/
                              if(parseFloat(document.formulario.pt1.value)+parseFloat(document.formulario.pt2.value)+parseFloat(document.formulario.pt3.value)+parseFloat(document.formulario.pt4.value)>parseFloat(document.formulario.ppt.value)+'00.1')
                              {
                                  alertify.alert("ERROR !! PRESUPUESTO "+document.formulario.gs4.value+"");
                                  return false; 
                              }
                              if(parseFloat(document.formulario.pte1.value)+parseFloat(document.formulario.pte2.value)+parseFloat(document.formulario.pte3.value)+parseFloat(document.formulario.pte4.value)>parseFloat(document.formulario.pe.value)+'00.1')
                              {
                                  alertify.alert("ERROR !! PRESUPUESTO EJECUTADO "+document.formulario.gs4.value+"");
                                  return false;
                              }
                              if(parseFloat(sin_puntos(document.formulario.pte4.value))>parseFloat(sin_puntos(document.formulario.pt4.value)))
                              {
                                  alertify.alert("ERROR !! VALOR EJECUTADO NO PUEDE SER MAYOR AL PRESUPUESTO "+document.formulario.gs4.value+"");
                                  return false; 
                              }
                              /*-------------------- end verificando valores 4-----------------*/

                                if (parseInt(document.formulario.gest.value)>="5") /////// 5 GESTION
                                {
                                  /*-------------------- verificando gestiones 5-----------------*/
                                  if (document.formulario.pt5.value=="") /////// gestion 1
                                  { 
                                      alertify.alert("INGRESE TOTAL PRESUPUESTO "+document.formulario.gs5.value+"");
                                      return false;  
                                  }
                                  if (document.formulario.pte5.value=="") /////// gestion 1
                                  { 
                                      alertify.alert("INGRESE PRESUPUESTO EJECUTADO "+document.formulario.gs5.value+"");
                                      return false;  
                                  }
                                  /*-------------------- end verificando gestiones 5-----------------*/
                                  /*-------------------- verificando valores 5-----------------*/
                                  if(parseFloat(document.formulario.pt1.value)+parseFloat(document.formulario.pt2.value)+parseFloat(document.formulario.pt3.value)+parseFloat(document.formulario.pt4.value)+parseFloat(document.formulario.pt5.value)>parseFloat(document.formulario.ppt.value)+'00.1')
                                  {
                                      alertify.alert("ERROR !! PRESUPUESTO "+document.formulario.gs5.value+"");
                                      return false; 
                                  }
                                  if(parseFloat(document.formulario.pte1.value)+parseFloat(document.formulario.pte2.value)+parseFloat(document.formulario.pte3.value)+parseFloat(document.formulario.pte4.value)+parseFloat(document.formulario.pte5.value)>parseFloat(document.formulario.pe.value)+'00.1')
                                  {
                                      alertify.alert("ERROR !! PRESUPUESTO EJECUTADO "+document.formulario.gs5.value+"");
                                      return false;
                                  }
                                  if(parseFloat(sin_puntos(document.formulario.pte5.value))>parseFloat(sin_puntos(document.formulario.pt5.value)))
                                  {
                                      alertify.alert("ERROR !! VALOR EJECUTADO NO PUEDE SER MAYOR AL PRESUPUESTO "+document.formulario.gs5.value+"");
                                      return false;  
                                  }
                                  /*-------------------- end verificando valores 4-----------------*/

                                    if (parseInt(document.formulario.gest.value)>="6") /////// 6 GESTION
                                    {
                                      /*-------------------- verificando gestiones 6-----------------*/
                                      if (document.formulario.pt6.value=="") /////// gestion 1
                                      { 
                                          alertify.alert("INGRESE TOTAL PRESUPUESTO "+document.formulario.gs6.value+"");
                                          return false;  
                                      }
                                      if (document.formulario.pte6.value=="") /////// gestion 1
                                      { 
                                          alertify.alert("INGRESE PRESUPUESTO EJECUTADO "+document.formulario.gs6.value+"");
                                          return false; 
                                      }
                                      /*--------------------end verificando gestiones 6-----------------*/
                                      /*-------------------- verificando valores 6-----------------*/
                                      if(parseFloat(document.formulario.pt1.value)+parseFloat(document.formulario.pt2.value)+parseFloat(document.formulario.pt3.value)+parseFloat(document.formulario.pt4.value)+parseFloat(document.formulario.pt5.value)+parseFloat(document.formulario.pt6.value)>parseFloat(document.formulario.ppt.value)+'00.1')
                                      {
                                          alertify.alert("ERROR !! PRESUPUESTO "+document.formulario.gs6.value+"");
                                          return false; 
                                      }
                                      if(parseFloat(document.formulario.pte1.value)+parseFloat(document.formulario.pte2.value)+parseFloat(document.formulario.pte3.value)+parseFloat(document.formulario.pte4.value)+parseFloat(document.formulario.pte5.value)+parseFloat(document.formulario.pte6.value)>parseFloat(document.formulario.pe.value)+'00.1')
                                      {
                                          alertify.alert("ERROR !! PRESUPUESTO EJECUTADO "+document.formulario.gs6.value+"");
                                          return false; 
                                      }
                                      if(parseFloat(sin_puntos(document.formulario.pte6.value))>parseFloat(sin_puntos(document.formulario.pt6.value)))
                                      {
                                          alertify.alert("ERROR !! VALOR EJECUTADO NO PUEDE SER MAYOR AL PRESUPUESTO "+document.formulario.gs6.value+"");
                                          return false; 
                                      }
                                      /*-------------------- end verificando valores 6-----------------*/

                                      if (parseInt(document.formulario.gest.value)>="7") /////// 7 GESTION
                                      {
                                          /*-------------------- verificando gestiones 6-----------------*/
                                          if (document.formulario.pt7.value=="") /////// gestion 7
                                          { 
                                              alertify.alert("INGRESE TOTAL PRESUPUESTO "+document.formulario.gs7.value+"");
                                              return false; 
                                          }
                                          if (document.formulario.pte7.value=="") /////// gestion 7
                                          { 
                                              alertify.alert("INGRESE PRESUPUESTO EJECUTADO "+document.formulario.gs7.value+"");
                                              return false; 
                                          }
                                          /*--------------------end verificando gestiones 6-----------------*/
                                          /*-------------------- verificando valores 6-----------------*/
                                          if(parseFloat(document.formulario.pt1.value)+parseFloat(document.formulario.pt2.value)+parseFloat(document.formulario.pt3.value)+parseFloat(document.formulario.pt4.value)+parseFloat(document.formulario.pt5.value)+parseFloat(document.formulario.pt6.value)+parseFloat(document.formulario.pt7.value)>parseFloat(document.formulario.ppt.value)+'00.1')
                                          {
                                              alertify.alert("ERROR !! PRESUPUESTO "+document.formulario.gs7.value+"");
                                              return false;  
                                          }
                                          if(parseFloat(document.formulario.pte1.value)+parseFloat(document.formulario.pte2.value)+parseFloat(document.formulario.pte3.value)+parseFloat(document.formulario.pte4.value)+parseFloat(document.formulario.pte5.value)+parseFloat(document.formulario.pte6.value)+parseFloat(document.formulario.pte7.value)>parseFloat(document.formulario.pe.value)+'00.1')
                                          {
                                              alertify.alert("ERROR !! PRESUPUESTO EJECUTADO "+document.formulario.gs7.value+"");
                                              return false; 
                                          }
                                          if(parseFloat(sin_puntos(document.formulario.pte7.value))>parseFloat(sin_puntos(document.formulario.pt7.value)))
                                          {
                                              alertify.alert("ERROR !! VALOR EJECUTADO NO PUEDE SER MAYOR AL PRESUPUESTO "+document.formulario.gs7.value+"");
                                              return false; 
                                          }
                                          /*-------------------- end verificando valores 7-----------------*/
                                            if (parseInt(document.formulario.gest.value)>="8") /////// 8 GESTION
                                              {
                                                  /*-------------------- verificando gestiones 6-----------------*/
                                                  if (document.formulario.pt8.value=="") /////// gestion 7
                                                  { 
                                                      alertify.alert("INGRESE TOTAL PRESUPUESTO "+document.formulario.gs8.value+"");
                                                      return false; 
                                                  }
                                                  if (document.formulario.pte8.value=="") /////// gestion 7
                                                  { 
                                                      alertify.alert("INGRESE PRESUPUESTO EJECUTADO "+document.formulario.gs8.value+"");
                                                      return false; 
                                                  }
                                                  /*--------------------end verificando gestiones 6-----------------*/
                                                  /*-------------------- verificando valores 6-----------------*/
                                                  if(parseFloat(document.formulario.pt1.value)+parseFloat(document.formulario.pt2.value)+parseFloat(document.formulario.pt3.value)+parseFloat(document.formulario.pt4.value)+parseFloat(document.formulario.pt5.value)+parseFloat(document.formulario.pt6.value)+parseFloat(document.formulario.pt7.value)+parseFloat(document.formulario.pt8.value)>parseFloat(document.formulario.ppt.value)+'00.1')
                                                  {
                                                      alertify.alert("ERROR !! PRESUPUESTO "+document.formulario.gs8.value+"");
                                                      return false; 
                                                  }
                                                  if(parseFloat(document.formulario.pte1.value)+parseFloat(document.formulario.pte2.value)+parseFloat(document.formulario.pte3.value)+parseFloat(document.formulario.pte4.value)+parseFloat(document.formulario.pte5.value)+parseFloat(document.formulario.pte6.value)+parseFloat(document.formulario.pte7.value)+parseFloat(document.formulario.pte8.value)>parseFloat(document.formulario.pe.value)+'00.1')
                                                  {
                                                      alertify.alert("ERROR !! PRESUPUESTO EJECUTADO "+document.formulario.gs8.value+"");
                                                      return false; 
                                                  }
                                                  if(parseFloat(sin_puntos(document.formulario.pte8.value))>parseFloat(sin_puntos(document.formulario.pt8.value)))
                                                  {
                                                      alertify.alert("ERROR !! VALOR EJECUTADO NO PUEDE SER MAYOR AL PRESUPUESTO "+document.formulario.gs8.value+"");
                                                      return false; 
                                                  }
                                                  /*-------------------- end verificando valores 8-----------------*/
                                                  if (parseInt(document.formulario.gest.value)>="9") /////// 9 GESTION
                                                  {
                                                      /*-------------------- verificando gestiones 9-----------------*/
                                                      if (document.formulario.pt9.value=="") /////// gestion 9
                                                      { 
                                                          alertify.alert("INGRESE TOTAL PRESUPUESTO "+document.formulario.gs9.value+"");
                                                          return false; 
                                                      }
                                                      if (document.formulario.pte9.value=="") /////// gestion 9
                                                      { 
                                                          alertify.alert("INGRESE PRESUPUESTO EJECUTADO "+document.formulario.gs9.value+"");
                                                          return false; 
                                                      }
                                                      /*--------------------end verificando gestiones 6-----------------*/
                                                      /*-------------------- verificando valores 6-----------------*/
                                                      if(parseFloat(document.formulario.pt1.value)+parseFloat(document.formulario.pt2.value)+parseFloat(document.formulario.pt3.value)+parseFloat(document.formulario.pt4.value)+parseFloat(document.formulario.pt5.value)+parseFloat(document.formulario.pt6.value)+parseFloat(document.formulario.pt7.value)+parseFloat(document.formulario.pt8.value)+parseFloat(document.formulario.pt9.value)>parseFloat(document.formulario.ppt.value)+'00.1')
                                                      {
                                                          alertify.alert("ERROR !! PRESUPUESTO "+document.formulario.gs9.value+"");
                                                          return false; 
                                                      }
                                                      if(parseFloat(document.formulario.pte1.value)+parseFloat(document.formulario.pte2.value)+parseFloat(document.formulario.pte3.value)+parseFloat(document.formulario.pte4.value)+parseFloat(document.formulario.pte5.value)+parseFloat(document.formulario.pte6.value)+parseFloat(document.formulario.pte7.value)+parseFloat(document.formulario.pte8.value)+parseFloat(document.formulario.pte9.value)>parseFloat(document.formulario.pe.value)+'00.1')
                                                      {
                                                          alertify.alert("ERROR !! PRESUPUESTO EJECUTADO "+document.formulario.gs9.value+"");
                                                          return false;  
                                                      }
                                                      if(parseFloat(sin_puntos(document.formulario.pte9.value))>parseFloat(sin_puntos(document.formulario.pt9.value)))
                                                      {
                                                          alertify.alert("ERROR !! VALOR EJECUTADO NO PUEDE SER MAYOR AL PRESUPUESTO "+document.formulario.gs9.value+"");
                                                          return false; 
                                                      }
                                                      /*-------------------- end verificando valores 10-----------------*/
                                                      if (parseInt(document.formulario.gest.value)>="10") /////// 10 GESTION
                                                      { 
                                                          /*-------------------- verificando gestiones 6-----------------*/
                                                          if (document.formulario.pt10.value=="") /////// gestion 7
                                                          { 
                                                              alertify.alert("INGRESE TOTAL PRESUPUESTO "+document.formulario.gs10.value+"");
                                                              return false; 
                                                          }
                                                          if (document.formulario.pte10.value=="") /////// gestion 7
                                                          { 
                                                              alertify.alert("INGRESE PRESUPUESTO EJECUTADO "+document.formulario.gs10.value+"");
                                                              return false; 
                                                          }
                                                          /*--------------------end verificando gestiones 6-----------------*/
                                                          /*-------------------- verificando valores 6-----------------*/
                                                          if(parseFloat(document.formulario.pt1.value)+parseFloat(document.formulario.pt2.value)+parseFloat(document.formulario.pt3.value)+parseFloat(document.formulario.pt4.value)+parseFloat(document.formulario.pt5.value)+parseFloat(document.formulario.pt6.value)+parseFloat(document.formulario.pt7.value)+parseFloat(document.formulario.pt8.value)+parseFloat(document.formulario.pt9.value)+parseFloat(document.formulario.pt10.value)>parseFloat(document.formulario.ppt.value)+'00.1')
                                                          {
                                                              alertify.alert("ERROR !! PRESUPUESTO "+document.formulario.gs10.value+"");
                                                              return false;
                                                          }
                                                          if(parseFloat(document.formulario.pte1.value)+parseFloat(document.formulario.pte2.value)+parseFloat(document.formulario.pte3.value)+parseFloat(document.formulario.pte4.value)+parseFloat(document.formulario.pte5.value)+parseFloat(document.formulario.pte6.value)+parseFloat(document.formulario.pte7.value)+parseFloat(document.formulario.pte8.value)+parseFloat(document.formulario.pte9.value)+parseFloat(document.formulario.pte10.value)>parseFloat(document.formulario.pe.value)+'00.1')
                                                          {
                                                              alertify.alert("ERROR !! PRESUPUESTO EJECUTADO "+document.formulario.gs10.value+"");
                                                              return false; 
                                                          }
                                                          if(parseFloat(sin_puntos(document.formulario.pte10.value))>parseFloat(sin_puntos(document.formulario.pt10.value)))
                                                          {
                                                              alertify.alert("ERROR !! VALOR EJECUTADO NO PUEDE SER MAYOR AL PRESUPUESTO "+document.formulario.gs10.value+"");
                                                              return false; 
                                                          }

                                                      }
                                                  }
                                             
                                              }
                                      }
                                    }
                                    
                                }
                            }
                        }
                    }
                  }

                //  document.getElementById('saldo').value=document.getElementById('montos').value;//al final colocamos la suma en algún input.

                alertify.confirm("GUARDAR PRESUPUESTO "+document.formulario.gi.value+" - "+document.formulario.gf.value+"?", function (a) {
                        if (a) {
                            document.formulario.submit();
                        } else {
                            alertify.error("OPCI\u00D3N CANCELADA");
                        }
                    });
            }