$(function () {
    $('#modal_nuevo_fun').on('hidden.bs.modal', function () {
        document.forms['form_fun'].reset();
    });

    $("#enviar_fun").on("click", function (e) {
        //========================VALIDANDO FORMULARIO===================
        var $validator = $("#form_fun").validate({
            //////////////// DATOS GENERALES
            rules: {
                fun_nombre: { //// Programa
                    required: true,
                },
                fun_paterno: { //// Programa
                    required: true,
                },
                fun_materno: { //// Programa
                    required: true,
                },
                fun_ci: { //// Programa
                    required: true,
                },
                fun_cargo: {
                    required: true,
                    minlength: 2,
                },
                fun_telefono: { //// Programa
                    number: true,
                    minlength: 5,
                },
                fun_usuario: {
                    required: true,
                    minlength: 2,
                },
                fun_password: {
                    required: true,
                    minlength: 4,
                },
                uni_id: {
                    required: true,
                },
                car_id: {
                    required: true,
                },
                rol: {
                    required: true,
                }
            },
            messages: {
                fun_nombre: {required: "Ingrese Nombre"},
                fun_paterno: {required: "Ingrese Apellido Paterno"},
                fun_materno: {required: "Ingrese Apellido Materno"},
                fun_ci: {required: "Ingrese cedula de identidad"},
                fun_cargo: {required: "Ingrese El Cargo", minlength: "Dato Inválido"},
                fun_telefono: {number: "Dato Inválido", minlength: "Número de Teléfono Corto"},
                fun_usuario: {required: "Ingrese el Usuario", minlength: "Dato Inválido"},
                fun_password: {required: "Ingrese su Contraseña", minlength: "Contraseña Corta"},
                uni_id: {required: "Seleccione la Unidad Organizacional"},
                car_id: {required: "Seleccione el Cargo"},
                rol: {required: "Seleccione un Rol"},
            },
            highlight: function (element) {
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });
        var $valid = $("#form_fun").valid();
        if (!$valid) {
            $validator.focusInvalid();
            //return false;
        } else {
            document.form_fun.submit();
        }
    });

});
////////////////////////////////////////////////////////////////////MODIFICAR////////////////////////////////////

$(function () {

    function reset()
    {
        ruta_alerta = base_url + 'assets/themes_alerta/alertify.default.css';
        $("#toggleCSS").attr("href", ruta_alerta);
        alertify.set({
            labels: {
                ok: "ACEPTAR",
                cancel: "CANCELAR"
            },
            delay: 5000,
            buttonReverse: false,
            buttonFocus: "ok"
        });
    }
    // =====================================================================
    $(".mod_fun").on("click", function (e) {
        reset();
        var name = $(this).attr('name');
        var url = base_url+"index.php/admin/mantenimiento/get_fun";
        // console.log(name);
        var request;
        if (request) {
            request.abort();
        }
        request = $.ajax({
            url: url,
            type: "POST",
            dataType: 'json',
            data: "fun_id=" + name
        });
        request.done(function (response, textStatus, jqXHR) {
            document.getElementById("modfun_id").value = response.fun_id;
            document.getElementById("modfun_nombre").value = response.fun_nombre; 
            document.getElementById("modfun_paterno").value = response.fun_paterno;
            document.getElementById("modfun_materno").value = response.fun_materno;
            document.getElementById("modfun_cargo").value = response.fun_cargo;
            document.getElementById("modfun_dni").value = response.fun_dni;
            document.getElementById("modfun_telefono").value = response.fun_telefono;
            document.getElementById("modfun_domicilio").value = response.fun_domicilio;
            document.getElementById("modfun_usuario").value = response.fun_usuario;
            document.getElementById("modfun_password").value = response.fun_password;
            document.getElementById("moduni_id").value = response.uni_id;
            document.getElementById("modcar_id").value = response.car_id;
            for (var i = 0; i < response.fun_roles.length; i++) {
                id_rol = 'rol' + response.fun_roles[i];
                document.getElementById(id_rol).checked  = true;
            }
        });
    });
});
/////////////////////////////////////////validar modificacion//////////////////////////////

$(function () {
    $('#modal_mod_fun').on('hidden.bs.modal', function () {
        document.forms['mod_formfun'].reset();
    });
    $("#modenviar_fun").on("click", function (e) {
        //========================VALIDANDO FORMULARIO===================
        var $validator = $("#mod_formfun").validate({
            //////////////// DATOS GENERALES
            rules: {
                modfun_nombre: { //// Programa
                    required: true,
                },
                modfun_paterno: { //// Programa
                    required: true,
                },
                modfun_materno: { //// Programa
                    required: true,
                },
                modfun_dni: { //// Programa
                    required: true,
                },
                modfun_cargo: {
                    required: true,
                    minlength: 2,
                },
                modfun_telefono: { //// Programa
                    number: true,
                    minlength: 5,
                },
                modfun_usuario: {
                    required: true,
                    minlength: 2,
                },
                moduni_id: {
                    required: true,
                },
                modcar_id: {
                    required: true,
                }

            },
            messages: {
                modfun_nombre: {required: "Ingrese Nombre"},
                modfun_paterno: {required: "Ingrese Apellido Paterno"},
                modfun_materno: {required: "Ingrese Apellido Materno"},
                modfun_dni: {required: "Ingrese cedula de identidad"},
                modfun_cargo: {required: "Ingrese El Cargo", minlength: "Dato Inválido"},
                modfun_telefono: {number: "Dato Inválido", minlength: "Número de Teléfono Corto"},
                modfun_usuario: {required: "Ingrese el Usuario", minlength: "Dato Inválido"},
                fun_password: {required: "Ingrese su Contraseña", minlength: "Contraseña Corta"},
                moduni_id: {required: "Seleccione la Unidad Organizacional"},
                modcar_id: {required: "Seleccione el Cargo"},
                
            },
            highlight: function (element) {
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });
        var $valid = $("#mod_formfun").valid();
        if (!$valid) {
            $validator.focusInvalid();
            //return false;
        } else {
            document.mod_formfun.submit();
        }
    });
});

//////////////////////////////////////////////// ALTAS BAJAS DE FUNCIONARIOS////////////////////////////////
$(function () {

    function reset() {
        ruta_alerta = base_url + 'assets/themes_alerta/alertify.default.css';
                            $("#toggleCSS").attr("href", ruta_alerta);
        alertify.set({
            labels: {
                ok: "ACEPTAR",
                cancel: "CANCELAR"
            },
            delay: 5000,
            buttonReverse: false,
            buttonFocus: "ok"
        });
    }

    // ============================DESACTIVAR==================================
    $(".del_fun").on("click", function (e) {
        reset();
        var name = $(this).attr('name');
        var request;
        alertify.confirm("REALMENTE DESEA DESACTIVAR ESTE FUNCIONARIO?", function (a) {
            if (a) {
                var url =  base_url+"index.php/admin/mantenimiento/del_fun";
                if (request) {
                    request.abort();
                }
                request = $.ajax({
                    url: url,
                    type: "POST",
                    data: "del_fun=" + name
                });
                request.done(function (response, textStatus, jqXHR) {
                    //console.log("response: "+response);
                    location.reload();
                    // $('#tr'+name).html("");
                });
                request.fail(function (jqXHR, textStatus, thrown) {
                    console.log("ERROR: " + textStatus);
                });
                request.always(function () {
                    //console.log("termino la ejecuicion de ajax");
                });
                e.preventDefault();
                alertify.success("Se desactivo el registro correctamente");
            } else {
                // user clicked "cancel"
                alertify.error("Opcion cancelada");
            }
        });
        return false;
    });

    // ============================ACTIVAR===========================
    $(".act_fun").on("click", function (e) {
        reset();
        var name = $(this).attr('name');
        var request;
        alertify.confirm("REALMENTE DESEA ACTIVAR ESTE FUNCIONARIO?", function (a) {
            if (a) {
                var url =  base_url + "index.php/mantenimiento/funcionario/act_fun";
                if (request) {
                    request.abort();
                }
                request = $.ajax({
                    url: url,
                    type: "POST",
                    data: "del_fun=" + name,
                    dataType: "json"
                });
                request.done(function (response, textStatus, jqXHR) {
                    console.log("response: "+response);
                    // $('#tr'+name).html("");
                    if (response.boolean) {
                        location.reload();
                    }
                });
                request.fail(function (jqXHR, textStatus, thrown) {
                    console.log("ERROR: " + textStatus);
                });
                request.always(function () {
                    //console.log("termino la ejecuicion de ajax");
                });
                e.preventDefault();
                alertify.success("Se activo el registro correctamente");
            } else {
                // user clicked "cancel"
                alertify.error("Opcion cancelada");
            }
        });
        return false;
    });

});
 