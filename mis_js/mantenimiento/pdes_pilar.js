//////////////////////////////////////add partidas////////////////////////////////////
 $(function () { 
     
        $('#modal_nuevo_pdes').on('hidden.bs.modal', function(){
            document.forms['add_pdes_form'].reset();
        });

        function reset() {

            ruta_alerta = base_url + 'assets/themes_alerta/alertify.default.css';
                                $("#toggleCSS").attr("href", ruta_alerta);
            alertify.set({
                labels: {
                    ok: "ACEPTAR",
                    cancel: "CANCELAR"
                },
                delay: 5000,
                buttonReverse: false,
                buttonFocus: "ok"
            });
        };
        
        $("#enviar_pdes").on("click", function (e) {

            //========================VALIDANDO FORMULARIO===================
            var $validator = $("#add_pdes_form").validate({
                rules: {
                    pilar_descripcion: { //// Programa
                        required: true,
                    },
                    pilar_codigo: { //// Programa
                        required: true,
                        number: true,
                    },
                    pilar_gestion: { //// Programa
                        required: true,
                        number: true,
                        max:2017,
                        min: 2000,
                    }
                },
                messages: {
                    pilar_descripcion: "Ingrese descripción",
                    pilar_codigo: {required: "Ingrese el código", number: "Dato Inválido"},
                    pilar_gestion: {
                        required: "Ingrese la gestión",
                        number: "Dato Inválido",
                        max: "El dato debe ser menor o igual al año 2017",
                        min: "El dato debe ser mayor o igual al año 2000"
                    },

                },
                highlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                },
                unhighlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function (error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
            var $valid = $("#add_pdes_form").valid();
            if (!$valid) {
                $validator.focusInvalid();
                //return false;
            } else {
                //==========================================================
                var pilar_codigo = document.getElementById("pilar_codigo").value;
                var pilar_gestion = document.getElementById("pilar_gestion").value;
                var pilar_descripcion = document.getElementById("pilar_descripcion").value;
                //var dependiente = document.getElementById("dependiente").value;
                //alert(dependiente)
                //=================== VERIFICAR SI EXISTE EL COD DE PROGRAMA ==============
              // alert(pilar_codigo);
                var url =  base_url+"index.php/verificar_pilar";
                $.ajax({
                    type: "post",
                    url: url,
                    data: {pilar_codigo: pilar_codigo},
                    success: function (datos) {
                        alert(datos);        
                        if (datos == 1) {
                            //============= GUARDAR DESPUES DE LA VALIDACION ===============
                            // validacion de confirmacion
                            alertify.confirm("REALMENTE DESEA GUARDAR ESTE REGISTRO?", function (a) {
                            if (a) {
                            //end validacion de confirmacion
                            var url =  base_url+"index.php/add_pilar";
                            $.ajax({
                                type: "post",
                                url: url,
                                data: {
                                    pdes_codigo: pilar_codigo,
                                    pdes_gestion: pilar_gestion,
                                    pdes_descripcion: pilar_descripcion
                                },
                                success: function (data) {
                                    if (data == 'true') {
                                        window.location.reload(true);
                                        
                                    } else {
                                        /*alert(data);*/
                                        alertify.alert("EL REGISTRO SE GUARDÓ CORRECTAMENTE", function (e) {
                                            if (e) {
                                                window.location.reload(true);
                                            }
                                        });
                                       // window.location.reload(true);
                                    }
                                }
                            });
                            //validar de confirmacion
                              } else {
                                // user clicked "cancel"
                                alertify.error("OPCI\u00D3N CANCELADA");
                            }
                            });
                            //end validar de confirmacion
                        } else {
                            $("#par_codigo").closest('.form-group').removeClass('has-success').addClass('has-error');
                            alertify.error("EL CODIGO DE PARTIDA YA EXISTE");
                        }
                    }
                });
            }
        });
    });
 ///////////////////////////////////////////////////
 $(document).ready(function () {
        $(".par_si").click(function () {
            $('#content_parent').slideDown();
        })
        $(".par_no").click(function () {
            $('#content_parent').slideUp();
        })

    });
 //////////////////////////////////////////////////////
 ////////////////////////////////modificar////////////////////////////////////////////////
  $(function () {
        //limpiar variable
        var id_par = '';
        $(".mod_par").on("click", function (e) {
            //==========================LLENAR MIS DATOS DE FORMULARIO CON LA CLAVE RECIBIDA
            id_par = $(this).attr('name');
            
            var url =  base_url+"index.php/admin/partidas_mod";
            var codigo = '';
            var request;
            if (request) {
                request.abort();
            }
            request = $.ajax({
                url: url,
                type: "POST",
                dataType: 'json',
                data: "id_par=" + id_par
            });
            request.done(function (response, textStatus, jqXHR) {
                document.getElementById("modpar_nombre").value = response.par_nombre;
                document.getElementById("modpar_padre").value = response.padre;
                document.getElementById("modpar_codigo").value = response.par_codigo;
                document.getElementById("modpar_gestion").value = response.par_gestion;

                codigo = response.par_codigo;
            });
            request.fail(function (jqXHR, textStatus, thrown) {
                console.log("ERROR: " + textStatus);
            });
            request.always(function () {
                //console.log("termino la ejecuicion de ajax");
            });
            e.preventDefault();
            // =============================VALIDAR EL FORMULARIO DE MODIFICACION
            $("#mod_parenviar").on("click", function (e) {
                var $validator = $("#mod_formpar").validate({
                    rules: {
                        modpar_nombre: { //// Programa
                            required: true,
                        },
                        modpar_codigo: { //// Programa
                            required: true,
                            number: true,
                        },
                        modpar_gestion: { //// Programa
                            required: true,
                            number: true,
                            max:2017,
                            min: 2000,
                        }
                    },
                    messages: {
                        modpar_nombre: "Ingrese el nombre de partida",
                        modpar_codigo: {required: "Ingrese el código", number: "Dato Inválido"},
                        modpar_gestion: {
                            required: "Ingrese la gestión",
                            number: "Dato Inválido",
                            max: "El dato debe ser menor o igual al año 2017",
                            min: "El dato debe ser mayor o igual al año 2000"
                        },

                    },
                    highlight: function (element) {
                        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                    },
                    unhighlight: function (element) {
                        $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                    },
                    errorElement: 'span',
                    errorClass: 'help-block',
                    errorPlacement: function (error, element) {
                        if (element.parent('.input-group').length) {
                            error.insertAfter(element.parent());
                        } else {
                            error.insertAfter(element);
                        }
                    }
                });
                var $valid = $("#mod_formpar").valid();
                if (!$valid) {
                    $validator.focusInvalid();
                } else {
                    //==========================================================
                    var par_nombre = document.getElementById("modpar_nombre").value;
                    var par_gestion = document.getElementById("modpar_gestion").value;
                    var par_codigo = document.getElementById("modpar_codigo").value;
                            // validacion de confirmacion
                            alertify.confirm("REALMENTE DESEA MODIFICAR ESTE REGISTRO?", function (a) {
                            if (a) {
                            //end validacion de confirmacion
                    var url =  base_url+"index.php/admin/partidas_add";
                    $.ajax({
                        type: "post",
                        url: url,
                        data: {
                            par_nombre: par_nombre,
                            par_gestion: par_gestion,
                            par_codigo: par_codigo,
                            modificar: id_par
                        },
                        success: function (data) {
                                        alertify.alert("EL REGISTRO SE GUARDÓ CORRECTAMENTE", function (e) {
                                            if (e) {
                                                window.location.reload(true);
                                            }
                                        });
                            //window.location.reload(true);
                        }
                    });
                            //validar de confirmacion
                              } else {
                                // user clicked "cancel"
                                alertify.error("OPCI\u00D3N CANCELADA");
                            }
                            });
                            //end validar de confirmacion
                }
            });
        });
    });
////////////////////////////////////////////////eliminar/////////////////////////////////
 $(function () {
        function reset() {
           ruta_alerta = base_url + 'assets/themes_alerta/alertify.default.css';
                                $("#toggleCSS").attr("href", ruta_alerta);
            alertify.set({
                labels: {
                    ok: "ACEPTAR",
                    cancel: "CANCELAR"
                },
                delay: 5000,
                buttonReverse: false,
                buttonFocus: "ok"
            });
        }

        // =====================================================================
        $(".del_par").on("click", function (e) {
            reset();
            var name = $(this).attr('name');
            var request;
            alertify.confirm("REALMENTE DESEA ELIMINAR ESTE REGISTRO?", function (a) {
                if (a) {
                    var url =  base_url+"index.php/admin/partidas_del";
                    
                    if (request) {
                        request.abort();
                    }
                    request = $.ajax({
                        url: url,
                        type: "POST",
                        data: "postid=" + name
                    });
                    request.done(function (response, textStatus, jqXHR) {
                        //console.log("response: "+response);
                        $('#tr' + name).html("");
                    });
                    request.fail(function (jqXHR, textStatus, thrown) {
                        console.log("ERROR: " + textStatus);
                    });
                    request.always(function () {
                        //console.log("termino la ejecuicion de ajax");
                    });
                    e.preventDefault();
                    alertify.success("Se eliminó el registro correctamente");
                } else {
                    // user clicked "cancel"
                    alertify.error("Opcion cancelada");
                }
            });
            return false;
        });
    });
//////////////////////////////////////////////////////////////////NUevo pedes//////////////////////////////////
/*
        $(function () {
            $('#modal_nuevo_car').on('hidden.bs.modal', function () {
                document.forms['form_car'].reset();
            });
              $("#enviar_car").on("click", function (e) {
            //========================VALIDANDO FORMULARIO===================
            var $validator = $("#form_car").validate({
                rules: {
                    car_nombre: { //// Programa
                        required: true,
                    },
                    dependiente: { //// Programa
                        required: true,
                    },
                    padre: { //// Programa
                        required: true,
                    },
                    car_sueldo: { //// Programa
                        required: true,
                        number: true,
                    },
                    car_codigo: {
                        required: true,
                        number: true,
                    }
                },
                messages: {
                    car_nombre: "Ingrese el Nombre de Cargo",
                    dependiente: "Elija una Opcion",
                    padre: "Seleccione una Opcion",
                    car_sueldo: {required: "Ingrese el Sueldo", number: "Dato Inválido"},
                    car_codigo: {required: "Ingrese el Código", number: "Dato Inválido"},

                },
                    highlight: function (element) {
                        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                    },
                    unhighlight: function (element) {
                        $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                    },
                    errorElement: 'span',
                    errorClass: 'help-block',
                    errorPlacement: function (error, element) {
                        if (element.parent('.input-group').length) {
                            error.insertAfter(element.parent());
                        } else {
                            error.insertAfter(element);
                        }
                    }
                });
                var $valid = $("#form_car").valid();
                if (!$valid) {
                    $validator.focusInvalid();
                    //return false;
                } else {
                    document.form_car.submit();
                }
            });
        });*/

///////////////////////////////////adicionar cargo////////////////////////////////
 $(function () {
        $('#modal_nuevo_car').on('hidden.bs.modal', function () {
            document.forms['form_car'].reset();
            $("#content_parent").css("display", "none");
        });
        function reset() {

            ruta_alerta = base_url + 'assets/themes_alerta/alertify.default.css';
                                $("#toggleCSS").attr("href", ruta_alerta);
            alertify.set({
                labels: {
                    ok: "ACEPTAR",
                    cancel: "CANCELAR"
                },
                delay: 5000,
                buttonReverse: false,
                buttonFocus: "ok"
            });
        }

        $("#enviar_car").on("click", function (e) {
            //========================VALIDANDO FORMULARIO===================
            var $validator = $("#form_car").validate({
                rules: {
                    car_nombre: { //// Programa
                        required: true,
                    },
                    dependiente: { //// Programa
                        required: true,
                    },
                    padre: { //// Programa
                        required: true,
                    },
                    car_sueldo: { //// Programa
                        required: true,
                        number: true,
                    },
                    car_codigo: {
                        required: true,
                        number: true,
                    }
                },
                messages: {
                    car_nombre: "Ingrese el Nombre de Cargo",
                    dependiente: "Elija una Opcion",
                    padre: "Seleccione una Opcion",
                    car_sueldo: {required: "Ingrese el Sueldo", number: "Dato Inválido"},
                    car_codigo: {required: "Ingrese el Código", number: "Dato Inválido"},

                },
                highlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                },
                unhighlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function (error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
            var $valid = $("#form_car").valid();
            if (!$valid) {
                $validator.focusInvalid();
                //return false;
            } else {
                //==========================================================
                var car_nombre = document.getElementById("car_nombre").value;
                var car_sueldo = document.getElementById("car_sueldo").value;
                var padre = document.getElementById("padre").value;
                var car_codigo = document.getElementById("car_codigo").value;
                //=================== VERIFICAR SI EXISTE EL COD  ==============

                var url =  base_url+"index.php/admin/escala_salarial_ver";
                $.ajax({
                    type: "post",
                    url: url,
                    data: {car_codigo: car_codigo},
                    success: function (datos) {
                        if (parseInt(datos) == 1) {
                            //============= GUARDAR DESPUES DE LA VALIDACION ===============
                            // validacion de confirmacion
                            alertify.confirm("REALMENTE DESEA GUARDAR ESTE REGISTRO?", function (a) {
                            if (a) {
                            //end validacion de confirmacion
                            var url =  base_url+"index.php/admin/escala_salarial_add";
                            $.ajax({
                                type: "post",
                                url: url,
                                data: {
                                    car_nombre: car_nombre,
                                    car_sueldo: car_sueldo,
                                    padre: padre,
                                    car_codigo: car_codigo
                                },
                                success: function (data) {
                                    //alert(data)
                                    if (data = 'true') {
                                        $("#modal_nuevo_car").css("display", "none");
                                        reset();
                                        //alert
                                        alertify.alert("EL REGISTRO SE GUARDÓ CORRECTAMENTE", function (e) {
                                            if (e) {
                                                window.location.reload(true);
                                            }
                                        });
                                        //end alert
                                    } else {
                                        alert(data);
                                    }
                                }
                            });

                            //validar de confirmacion
                              } else {
                                // user clicked "cancel"
                                alertify.error("OPCI\u00D3N CANCELADA");
                            }
                            });
                            //end validar de confirmacion
                        } else {
                            $("#car_codigo").closest('.form-group').removeClass('has-success').addClass('has-error');
                            alertify.error("EL CODIGO DE ESCALA SALARIAL YA EXISTE");
                        }
                    }
                });


            }
        });
    });
//////////////////////
$(document).ready(function () {
        $(".car_si").click(function () {
            $('#content_parent').slideDown();
        })
        $(".car_no").click(function () {
            $('#content_parent').slideUp();
        })
    });
    //////////////////////////
  ////////////////////////////////////////////////////modificar/////////////////////////////
   $(function () {
        var id_car = '';
        $('#modal_mod_car').on('hidden.bs.modal', function () {
            id_car = '';
            document.forms['mod_formcar'].reset();
        });
        function reset() {
            ruta_alerta = base_url + 'assets/themes_alerta/alertify.default.css';
                                $("#toggleCSS").attr("href", ruta_alerta);
            alertify.set({
                labels: {
                    ok: "ACEPTAR",
                    cancel: "CANCELAR"
                },
                delay: 5000,
                buttonReverse: false,
                buttonFocus: "ok"
            });
        }

        $(".mod_car").on("click", function (e) {
            //==========================LLENAR MIS DATOS DE FORMULARIO CON LA CLAVE RECIBIDA

            id_car = $(this).attr('name');
            var url =  base_url+"index.php/admin/escala_salarial_mod";
            
            var request;
            if (request) {
                request.abort();
            }
            request = $.ajax({
                url: url,
                type: "POST",
                dataType: 'json',
                data: "id_car=" + id_car
            });
            request.done(function (response, textStatus, jqXHR) {
                document.getElementById("modcar_nombre").value = response.car_cargo;
                document.getElementById("modcar_sueldo").value = response.car_sueldo;
                document.getElementById("modcar_codigo").value = response.car_codigo;
                document.getElementById("modcar_codigo").disabled = true;
                document.getElementById('modcar_si').disabled = false;
                document.getElementById('modcar_no').disabled = false;
                if (response.padre == 'NINGUNO') {
                    document.getElementById('modcar_no').checked = true;
                    document.getElementById('modcar_si').disabled = true;
                    $('#modcontent_parent').slideUp();
                } else {
                    document.getElementById('modcar_si').checked = true;
                    document.getElementById('modcar_no').disabled = true;
                    $('#modcontent_parent').slideDown();
                    document.getElementById("modcar_padre").value = response.car_depende;
                }

                //codigo = response.car_codigo;
            });
            request.fail(function (jqXHR, textStatus, thrown) {
                console.log("ERROR: " + textStatus);
            });
            request.always(function () {
                //console.log("termino la ejecuicion de ajax");
            });
            e.preventDefault();
            // =============================VALIDAR EL FORMULARIO DE MODIFICACION
            $("#mod_parenviar").on("click", function (e) {
                var $validator = $("#mod_formcar").validate({
                    rules: {
                        modcar_nombre: { //// Programa
                            required: true,
                        },
                        modcar_padre: { //// Programa
                            required: true,
                        },
                        modcar_sueldo: { //// Programa
                            required: true,
                            number: true,
                        },
                        modcar_codigo: {
                            required: true,
                            number: true,
                        }
                    },
                    messages: {
                        modcar_nombre: "Ingrese el Nombre de Cargo",
                        modcar_padre: "Seleccione una Opcion",
                        modcar_sueldo: {required: "Ingrese el Sueldo", number: "Dato Inválido"},
                        modcar_codigo: {required: "Ingrese el Código", number: "Dato Inválido"},

                    },
                    highlight: function (element) {
                        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                    },
                    unhighlight: function (element) {
                        $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                    },
                    errorElement: 'span',
                    errorClass: 'help-block',
                    errorPlacement: function (error, element) {
                        if (element.parent('.input-group').length) {
                            error.insertAfter(element.parent());
                        } else {
                            error.insertAfter(element);
                        }
                    }
                });
                var $valid = $("#mod_formcar").valid();
                if (!$valid) {
                    $validator.focusInvalid();
                } else {
                    //==========================================================
                    var modcar_nombre = document.getElementById("modcar_nombre").value;
                    var modcar_sueldo = document.getElementById("modcar_sueldo").value;
                    var modcar_padre = document.getElementById("modcar_padre").value;
                    var modcar_codigo = document.getElementById("modcar_codigo").value;
                    // validacion de confirmacion
                            alertify.confirm("REALMENTE DESEA MODIFICAR EL REGISTRO?", function (a) {
                            if (a) {
                            //end validacion de confirmacion

                    var url =  base_url+"index.php/admin/escala_salarial_add";
                    $.ajax({
                        type: "post",
                        url: url,
                        data: {
                            car_nombre: modcar_nombre,
                            car_sueldo: modcar_sueldo,
                            padre: modcar_padre,
                            car_codigo: modcar_codigo,
                            modificar: id_car
                        },
                        success: function (data) {
                            if (data = 'true') {
                                $("#modal_mod_car").css("display", "none");
                                reset();
                                alertify.alert("EL REGISTRO SE MODIFICÓ CORRECTAMENTE", function (e) {
                                    if (e) {
                                        window.location.reload(true);
                                    }
                                });
                            } else {
                                alert(data);
                            }
                        }
                    });
                    //validar de confirmacion
                              } else {
                                // user clicked "cancel"
                                alertify.error("OPCI\u00D3N CANCELADA");
                            }
                        });
                            //end validar de confirmacion
                }
            });
            document.mod_formcar.reset()
        });
    });  
////////////////////////////////////////////////eliminar////////////////////////////////////////
$(function () {
        function reset() {
            ruta_alerta = base_url + 'assets/themes_alerta/alertify.default.css';
                                $("#toggleCSS").attr("href", ruta_alerta);
            alertify.set({
                labels: {
                    ok: "ACEPTAR",
                    cancel: "CANCELAR"
                },
                delay: 5000,
                buttonReverse: false,
                buttonFocus: "ok"
            });
        }

        // =====================================================================
        $(".del_car").on("click", function (e) {
            reset();
            var name = $(this).attr('name');
            var request;
            alertify.confirm("REALMENTE DESEA ELIMINAR ESTE REGISTRO?", function (a) {
                if (a) {
                    var url =  base_url+"index.php/admin/escala_salarial_del";
                    
                    if (request) {
                        request.abort();
                    }
                    request = $.ajax({
                        url: url,
                        type: "POST",
                        data: "postid=" + name
                    });
                    request.done(function (response, textStatus, jqXHR) {
                        //console.log("response: "+response);
                        $('#tr' + response.trim()).html("");
                    });
                    request.fail(function (jqXHR, textStatus, thrown) {
                        console.log("ERROR: " + textStatus);
                    });
                    request.always(function () {
                        //console.log("termino la ejecuicion de ajax");
                    });
                    e.preventDefault();
                    alertify.success("Se eliminó el registro correctamente");
                } else {
                    // user clicked "cancel"
                    alertify.error("Opcion cancelada");
                }
            });
            return false;
        });
    });
